<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID73615CC0-BCA3-D330-D6C0-76BBD74FD7AF">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>FeA_1931_OldManUkukuutFox_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\FeA_1931_OldManUkukuutFox_flk\FeA_1931_OldManUkukuutFox_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">463</ud-information>
            <ud-information attribute-name="# HIAT:w">349</ud-information>
            <ud-information attribute-name="# e">348</ud-information>
            <ud-information attribute-name="# HIAT:u">57</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="FeA">
            <abbreviation>FeA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
         <tli id="T278" time="143.0" type="appl" />
         <tli id="T279" time="143.5" type="appl" />
         <tli id="T280" time="144.0" type="appl" />
         <tli id="T281" time="144.5" type="appl" />
         <tli id="T282" time="145.0" type="appl" />
         <tli id="T283" time="145.5" type="appl" />
         <tli id="T284" time="146.0" type="appl" />
         <tli id="T285" time="146.5" type="appl" />
         <tli id="T286" time="147.0" type="appl" />
         <tli id="T287" time="147.5" type="appl" />
         <tli id="T288" time="148.0" type="appl" />
         <tli id="T289" time="148.5" type="appl" />
         <tli id="T290" time="149.0" type="appl" />
         <tli id="T291" time="149.5" type="appl" />
         <tli id="T292" time="150.0" type="appl" />
         <tli id="T293" time="150.5" type="appl" />
         <tli id="T294" time="151.0" type="appl" />
         <tli id="T295" time="151.5" type="appl" />
         <tli id="T296" time="152.0" type="appl" />
         <tli id="T297" time="152.5" type="appl" />
         <tli id="T298" time="153.0" type="appl" />
         <tli id="T299" time="153.5" type="appl" />
         <tli id="T300" time="154.0" type="appl" />
         <tli id="T301" time="154.5" type="appl" />
         <tli id="T302" time="155.0" type="appl" />
         <tli id="T303" time="155.5" type="appl" />
         <tli id="T304" time="156.0" type="appl" />
         <tli id="T305" time="156.5" type="appl" />
         <tli id="T306" time="157.0" type="appl" />
         <tli id="T307" time="157.5" type="appl" />
         <tli id="T308" time="158.0" type="appl" />
         <tli id="T309" time="158.5" type="appl" />
         <tli id="T310" time="159.0" type="appl" />
         <tli id="T311" time="159.5" type="appl" />
         <tli id="T312" time="160.0" type="appl" />
         <tli id="T313" time="160.5" type="appl" />
         <tli id="T314" time="161.0" type="appl" />
         <tli id="T315" time="161.5" type="appl" />
         <tli id="T316" time="162.0" type="appl" />
         <tli id="T317" time="162.5" type="appl" />
         <tli id="T318" time="163.0" type="appl" />
         <tli id="T319" time="163.5" type="appl" />
         <tli id="T320" time="164.0" type="appl" />
         <tli id="T321" time="164.5" type="appl" />
         <tli id="T322" time="165.0" type="appl" />
         <tli id="T323" time="165.5" type="appl" />
         <tli id="T324" time="166.0" type="appl" />
         <tli id="T325" time="166.5" type="appl" />
         <tli id="T326" time="167.0" type="appl" />
         <tli id="T327" time="167.5" type="appl" />
         <tli id="T328" time="168.0" type="appl" />
         <tli id="T329" time="168.5" type="appl" />
         <tli id="T330" time="169.0" type="appl" />
         <tli id="T331" time="169.5" type="appl" />
         <tli id="T332" time="170.0" type="appl" />
         <tli id="T333" time="170.5" type="appl" />
         <tli id="T334" time="171.0" type="appl" />
         <tli id="T335" time="171.5" type="appl" />
         <tli id="T336" time="172.0" type="appl" />
         <tli id="T337" time="172.5" type="appl" />
         <tli id="T338" time="173.0" type="appl" />
         <tli id="T339" time="173.5" type="appl" />
         <tli id="T340" time="174.0" type="appl" />
         <tli id="T341" time="174.5" type="appl" />
         <tli id="T342" time="175.0" type="appl" />
         <tli id="T343" time="175.5" type="appl" />
         <tli id="T344" time="176.0" type="appl" />
         <tli id="T345" time="176.5" type="appl" />
         <tli id="T346" time="177.0" type="appl" />
         <tli id="T347" time="177.5" type="appl" />
         <tli id="T348" time="178.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="FeA"
                      type="t">
         <timeline-fork end="T5" start="T4">
            <tli id="T4.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T348" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Dʼe</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">biːr</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">ogonnʼor</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">olorbuta</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T4.tx.1" id="Seg_19" n="HIAT:w" s="T4">Ukukuːt</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_22" n="HIAT:w" s="T4.tx.1">Čukukuːt</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_25" n="HIAT:w" s="T5">aːta</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_29" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_31" n="HIAT:w" s="T6">Mantɨŋ</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_34" n="HIAT:w" s="T7">ügüs</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_37" n="HIAT:w" s="T8">bagajɨ</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_40" n="HIAT:w" s="T9">ogoto</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_44" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_46" n="HIAT:w" s="T10">Dʼe</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_49" n="HIAT:w" s="T11">bu</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_52" n="HIAT:w" s="T12">manɨga</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_55" n="HIAT:w" s="T13">hahɨl</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_58" n="HIAT:w" s="T14">kelbite</ts>
                  <nts id="Seg_59" n="HIAT:ip">:</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_62" n="HIAT:u" s="T15">
                  <nts id="Seg_63" n="HIAT:ip">"</nts>
                  <ts e="T16" id="Seg_65" n="HIAT:w" s="T15">Ogonnʼor</ts>
                  <nts id="Seg_66" n="HIAT:ip">,</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_69" n="HIAT:w" s="T16">biːr</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_72" n="HIAT:w" s="T17">ogogun</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_75" n="HIAT:w" s="T18">egel</ts>
                  <nts id="Seg_76" n="HIAT:ip">"</nts>
                  <nts id="Seg_77" n="HIAT:ip">,</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_80" n="HIAT:w" s="T19">diːr</ts>
                  <nts id="Seg_81" n="HIAT:ip">,</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_83" n="HIAT:ip">"</nts>
                  <ts e="T21" id="Seg_85" n="HIAT:w" s="T20">hi͡ekpin</ts>
                  <nts id="Seg_86" n="HIAT:ip">.</nts>
                  <nts id="Seg_87" n="HIAT:ip">"</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_90" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_92" n="HIAT:w" s="T21">Manɨga</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_95" n="HIAT:w" s="T22">bu</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_98" n="HIAT:w" s="T23">ogonnʼor</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_101" n="HIAT:w" s="T24">ogotun</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_104" n="HIAT:w" s="T25">bi͡erbite</ts>
                  <nts id="Seg_105" n="HIAT:ip">,</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_108" n="HIAT:w" s="T26">hi͡etin</ts>
                  <nts id="Seg_109" n="HIAT:ip">.</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_112" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_114" n="HIAT:w" s="T27">Dʼe</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_117" n="HIAT:w" s="T28">ol</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_120" n="HIAT:w" s="T29">barbɨta</ts>
                  <nts id="Seg_121" n="HIAT:ip">,</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_124" n="HIAT:w" s="T30">kas</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_127" n="HIAT:w" s="T31">eme</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_130" n="HIAT:w" s="T32">konon</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_133" n="HIAT:w" s="T33">baran</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_136" n="HIAT:w" s="T34">emi͡e</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_139" n="HIAT:w" s="T35">kelbite</ts>
                  <nts id="Seg_140" n="HIAT:ip">.</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_143" n="HIAT:u" s="T36">
                  <nts id="Seg_144" n="HIAT:ip">"</nts>
                  <ts e="T37" id="Seg_146" n="HIAT:w" s="T36">Ogonnʼor</ts>
                  <nts id="Seg_147" n="HIAT:ip">,</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_150" n="HIAT:w" s="T37">emi͡e</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_153" n="HIAT:w" s="T38">ogoto</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_156" n="HIAT:w" s="T39">egel</ts>
                  <nts id="Seg_157" n="HIAT:ip">"</nts>
                  <nts id="Seg_158" n="HIAT:ip">,</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_161" n="HIAT:w" s="T40">diːr</ts>
                  <nts id="Seg_162" n="HIAT:ip">,</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_164" n="HIAT:ip">"</nts>
                  <ts e="T42" id="Seg_166" n="HIAT:w" s="T41">hi͡ekpin</ts>
                  <nts id="Seg_167" n="HIAT:ip">.</nts>
                  <nts id="Seg_168" n="HIAT:ip">"</nts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_171" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_173" n="HIAT:w" s="T42">Onuga</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_176" n="HIAT:w" s="T43">ogonnʼor</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_179" n="HIAT:w" s="T44">ogotun</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_182" n="HIAT:w" s="T45">emi͡e</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_185" n="HIAT:w" s="T46">bi͡erbite</ts>
                  <nts id="Seg_186" n="HIAT:ip">.</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_189" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_191" n="HIAT:w" s="T47">Dʼe</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_194" n="HIAT:w" s="T48">ol</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_197" n="HIAT:w" s="T49">ogotun</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_200" n="HIAT:w" s="T50">ilpite</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_203" n="HIAT:w" s="T51">hi͡eri</ts>
                  <nts id="Seg_204" n="HIAT:ip">.</nts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_207" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_209" n="HIAT:w" s="T52">Onton</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_212" n="HIAT:w" s="T53">kas</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_215" n="HIAT:w" s="T54">eme</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_218" n="HIAT:w" s="T55">kun</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_221" n="HIAT:w" s="T56">bu͡olan</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_224" n="HIAT:w" s="T57">baran</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_227" n="HIAT:w" s="T58">emi͡e</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_230" n="HIAT:w" s="T59">kelbite</ts>
                  <nts id="Seg_231" n="HIAT:ip">.</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_234" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_236" n="HIAT:w" s="T60">Ol</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_239" n="HIAT:w" s="T61">kelen</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_242" n="HIAT:w" s="T62">emi͡e</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_245" n="HIAT:w" s="T63">diːr</ts>
                  <nts id="Seg_246" n="HIAT:ip">:</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_249" n="HIAT:u" s="T64">
                  <nts id="Seg_250" n="HIAT:ip">"</nts>
                  <ts e="T65" id="Seg_252" n="HIAT:w" s="T64">Ogoto</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_255" n="HIAT:w" s="T65">egel</ts>
                  <nts id="Seg_256" n="HIAT:ip">"</nts>
                  <nts id="Seg_257" n="HIAT:ip">,</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_260" n="HIAT:w" s="T66">diːr</ts>
                  <nts id="Seg_261" n="HIAT:ip">,</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_263" n="HIAT:ip">"</nts>
                  <ts e="T68" id="Seg_265" n="HIAT:w" s="T67">hi͡ekpin</ts>
                  <nts id="Seg_266" n="HIAT:ip">.</nts>
                  <nts id="Seg_267" n="HIAT:ip">"</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_270" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_272" n="HIAT:w" s="T68">Ogoto</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_275" n="HIAT:w" s="T69">elete</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_278" n="HIAT:w" s="T70">bu͡olla</ts>
                  <nts id="Seg_279" n="HIAT:ip">.</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_282" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_284" n="HIAT:w" s="T71">Dʼe</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_287" n="HIAT:w" s="T72">ol</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_290" n="HIAT:w" s="T73">barbɨta</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_293" n="HIAT:w" s="T74">daː</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_296" n="HIAT:w" s="T75">hahɨlɨŋ</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_299" n="HIAT:w" s="T76">dʼɨlɨjbɨta</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_302" n="HIAT:w" s="T77">bu</ts>
                  <nts id="Seg_303" n="HIAT:ip">.</nts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_306" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_308" n="HIAT:w" s="T78">Ogonnʼoruŋ</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_311" n="HIAT:w" s="T79">oloro</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_314" n="HIAT:w" s="T80">hataːn</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_317" n="HIAT:w" s="T81">baran</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_320" n="HIAT:w" s="T82">kaːman</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_323" n="HIAT:w" s="T83">kaːlla</ts>
                  <nts id="Seg_324" n="HIAT:ip">.</nts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_327" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_329" n="HIAT:w" s="T84">Dʼe</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_332" n="HIAT:w" s="T85">bu</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_335" n="HIAT:w" s="T86">kaːman</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_338" n="HIAT:w" s="T87">ihen-ihen</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_341" n="HIAT:w" s="T88">baran</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_344" n="HIAT:w" s="T89">biːr</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_347" n="HIAT:w" s="T90">kɨːl</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_350" n="HIAT:w" s="T91">baːjtahɨnɨn</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_353" n="HIAT:w" s="T92">ölörön</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_356" n="HIAT:w" s="T93">baran</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_359" n="HIAT:w" s="T94">ü͡ölüne</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_362" n="HIAT:w" s="T95">olordo</ts>
                  <nts id="Seg_363" n="HIAT:ip">.</nts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_366" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_368" n="HIAT:w" s="T96">Bu</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_371" n="HIAT:w" s="T97">olordoguna</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_374" n="HIAT:w" s="T98">hahɨl</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_377" n="HIAT:w" s="T99">kelle</ts>
                  <nts id="Seg_378" n="HIAT:ip">:</nts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_381" n="HIAT:u" s="T100">
                  <nts id="Seg_382" n="HIAT:ip">"</nts>
                  <ts e="T101" id="Seg_384" n="HIAT:w" s="T100">Čej</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_387" n="HIAT:w" s="T101">ogonnʼor</ts>
                  <nts id="Seg_388" n="HIAT:ip">,</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_391" n="HIAT:w" s="T102">bihigi</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_394" n="HIAT:w" s="T103">ogo</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_397" n="HIAT:w" s="T104">bu͡ola</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_400" n="HIAT:w" s="T105">oːnnʼu͡ok</ts>
                  <nts id="Seg_401" n="HIAT:ip">"</nts>
                  <nts id="Seg_402" n="HIAT:ip">,</nts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_405" n="HIAT:w" s="T106">di͡ete</ts>
                  <nts id="Seg_406" n="HIAT:ip">.</nts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_409" n="HIAT:u" s="T107">
                  <nts id="Seg_410" n="HIAT:ip">"</nts>
                  <ts e="T108" id="Seg_412" n="HIAT:w" s="T107">Če</ts>
                  <nts id="Seg_413" n="HIAT:ip">,</nts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_416" n="HIAT:w" s="T108">daːrɨm</ts>
                  <nts id="Seg_417" n="HIAT:ip">,</nts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_420" n="HIAT:w" s="T109">ogo</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_423" n="HIAT:w" s="T110">bu͡ola</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_426" n="HIAT:w" s="T111">oːnnʼu͡ok</ts>
                  <nts id="Seg_427" n="HIAT:ip">.</nts>
                  <nts id="Seg_428" n="HIAT:ip">"</nts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_431" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_433" n="HIAT:w" s="T112">Bu</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_436" n="HIAT:w" s="T113">ogonnʼoruŋ</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_439" n="HIAT:w" s="T114">bihik</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_442" n="HIAT:w" s="T115">oŋordo</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_445" n="HIAT:w" s="T116">ol</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_448" n="HIAT:w" s="T117">bajtahɨnnarɨn</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_451" n="HIAT:w" s="T118">tiriːtinen</ts>
                  <nts id="Seg_452" n="HIAT:ip">.</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_455" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_457" n="HIAT:w" s="T119">Bihik</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_460" n="HIAT:w" s="T120">bɨ͡ata</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_463" n="HIAT:w" s="T121">oŋorunnular</ts>
                  <nts id="Seg_464" n="HIAT:ip">.</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_467" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_469" n="HIAT:w" s="T122">Ogonnʼoruŋ</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_472" n="HIAT:w" s="T123">di͡ebit</ts>
                  <nts id="Seg_473" n="HIAT:ip">:</nts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_476" n="HIAT:u" s="T124">
                  <nts id="Seg_477" n="HIAT:ip">"</nts>
                  <ts e="T125" id="Seg_479" n="HIAT:w" s="T124">Če</ts>
                  <nts id="Seg_480" n="HIAT:ip">,</nts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_483" n="HIAT:w" s="T125">doː</ts>
                  <nts id="Seg_484" n="HIAT:ip">,</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_487" n="HIAT:w" s="T126">en</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_490" n="HIAT:w" s="T127">bigen</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_493" n="HIAT:w" s="T128">ogo</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_496" n="HIAT:w" s="T129">bu͡olan</ts>
                  <nts id="Seg_497" n="HIAT:ip">"</nts>
                  <nts id="Seg_498" n="HIAT:ip">,</nts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_501" n="HIAT:w" s="T130">di͡ete</ts>
                  <nts id="Seg_502" n="HIAT:ip">.</nts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_505" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_507" n="HIAT:w" s="T131">Hahɨl</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_510" n="HIAT:w" s="T132">manɨga</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_513" n="HIAT:w" s="T133">hɨtan</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_516" n="HIAT:w" s="T134">kördö</ts>
                  <nts id="Seg_517" n="HIAT:ip">:</nts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_520" n="HIAT:u" s="T135">
                  <nts id="Seg_521" n="HIAT:ip">"</nts>
                  <ts e="T136" id="Seg_523" n="HIAT:w" s="T135">Eː</ts>
                  <nts id="Seg_524" n="HIAT:ip">,</nts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_527" n="HIAT:w" s="T136">dogor</ts>
                  <nts id="Seg_528" n="HIAT:ip">,</nts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_531" n="HIAT:w" s="T137">mi͡ene</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_534" n="HIAT:w" s="T138">kuturugum</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_537" n="HIAT:w" s="T139">ɨ͡aldʼar</ts>
                  <nts id="Seg_538" n="HIAT:ip">,</nts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_541" n="HIAT:w" s="T140">hɨtɨ͡arɨ͡a</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_544" n="HIAT:w" s="T141">hu͡ok</ts>
                  <nts id="Seg_545" n="HIAT:ip">"</nts>
                  <nts id="Seg_546" n="HIAT:ip">,</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_549" n="HIAT:w" s="T142">di͡ete</ts>
                  <nts id="Seg_550" n="HIAT:ip">.</nts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_553" n="HIAT:u" s="T143">
                  <nts id="Seg_554" n="HIAT:ip">"</nts>
                  <ts e="T144" id="Seg_556" n="HIAT:w" s="T143">Če</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_559" n="HIAT:w" s="T144">kihi</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_562" n="HIAT:w" s="T145">kihiginen</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_565" n="HIAT:w" s="T146">en</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_568" n="HIAT:w" s="T147">hɨtan</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_571" n="HIAT:w" s="T148">ogo</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_574" n="HIAT:w" s="T149">bu͡ol</ts>
                  <nts id="Seg_575" n="HIAT:ip">"</nts>
                  <nts id="Seg_576" n="HIAT:ip">,</nts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_579" n="HIAT:w" s="T150">di͡ete</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_582" n="HIAT:w" s="T151">hahɨl</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_585" n="HIAT:w" s="T152">ogonnʼoru</ts>
                  <nts id="Seg_586" n="HIAT:ip">.</nts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_589" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_591" n="HIAT:w" s="T153">Ogonnʼor</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_594" n="HIAT:w" s="T154">dʼe</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_597" n="HIAT:w" s="T155">hɨtta</ts>
                  <nts id="Seg_598" n="HIAT:ip">.</nts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_601" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_603" n="HIAT:w" s="T156">Hahɨl</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_606" n="HIAT:w" s="T157">dʼe</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_609" n="HIAT:w" s="T158">manɨ</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_612" n="HIAT:w" s="T159">kelgijen</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_615" n="HIAT:w" s="T160">keːste</ts>
                  <nts id="Seg_616" n="HIAT:ip">.</nts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_619" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_621" n="HIAT:w" s="T161">Hahɨl</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_624" n="HIAT:w" s="T162">manɨ</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_627" n="HIAT:w" s="T163">bigiː-bigiː</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_630" n="HIAT:w" s="T164">kɨːl</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_633" n="HIAT:w" s="T165">etinen</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_636" n="HIAT:w" s="T166">ɨstaːn</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_639" n="HIAT:w" s="T167">ahata</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_642" n="HIAT:w" s="T168">olordo</ts>
                  <nts id="Seg_643" n="HIAT:ip">.</nts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_646" n="HIAT:u" s="T169">
                  <ts e="T170" id="Seg_648" n="HIAT:w" s="T169">Bu</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_651" n="HIAT:w" s="T170">ogonnʼoruŋ</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_654" n="HIAT:w" s="T171">araj</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_657" n="HIAT:w" s="T172">utujan</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_660" n="HIAT:w" s="T173">kaːlla</ts>
                  <nts id="Seg_661" n="HIAT:ip">.</nts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_664" n="HIAT:u" s="T174">
                  <ts e="T175" id="Seg_666" n="HIAT:w" s="T174">Hahɨl</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_669" n="HIAT:w" s="T175">manɨ</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_672" n="HIAT:w" s="T176">hagɨstaːn</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_675" n="HIAT:w" s="T177">ildʼen</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_678" n="HIAT:w" s="T178">bajaraːktan</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_681" n="HIAT:w" s="T179">tüŋneri</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_684" n="HIAT:w" s="T180">bɨrakta</ts>
                  <nts id="Seg_685" n="HIAT:ip">.</nts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_688" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_690" n="HIAT:w" s="T181">Kojut</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_693" n="HIAT:w" s="T182">ol</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_696" n="HIAT:w" s="T183">ogonnʼor</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_699" n="HIAT:w" s="T184">uhuktubuta</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_702" n="HIAT:w" s="T185">bajaraːk</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_705" n="HIAT:w" s="T186">ihiger</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_708" n="HIAT:w" s="T187">hɨtar</ts>
                  <nts id="Seg_709" n="HIAT:ip">.</nts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_712" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_714" n="HIAT:w" s="T188">Ogonnʼor</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_717" n="HIAT:w" s="T189">kaja</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_720" n="HIAT:w" s="T190">da</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_723" n="HIAT:w" s="T191">di͡eg</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_726" n="HIAT:w" s="T192">kamnɨ͡agar</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_729" n="HIAT:w" s="T193">bert</ts>
                  <nts id="Seg_730" n="HIAT:ip">,</nts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_733" n="HIAT:w" s="T194">kelgillibit</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_736" n="HIAT:w" s="T195">kihi</ts>
                  <nts id="Seg_737" n="HIAT:ip">.</nts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_740" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_742" n="HIAT:w" s="T196">Ogonnʼor</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_745" n="HIAT:w" s="T197">ɨŋɨrar</ts>
                  <nts id="Seg_746" n="HIAT:ip">:</nts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T205" id="Seg_749" n="HIAT:u" s="T198">
                  <nts id="Seg_750" n="HIAT:ip">"</nts>
                  <ts e="T199" id="Seg_752" n="HIAT:w" s="T198">Gornu͡oktar</ts>
                  <nts id="Seg_753" n="HIAT:ip">,</nts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_756" n="HIAT:w" s="T199">kɨrsalar</ts>
                  <nts id="Seg_757" n="HIAT:ip">,</nts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_760" n="HIAT:w" s="T200">kutujaktar</ts>
                  <nts id="Seg_761" n="HIAT:ip">,</nts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_764" n="HIAT:w" s="T201">bɨ͡abɨn</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_767" n="HIAT:w" s="T202">bɨha</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_770" n="HIAT:w" s="T203">kerbeːŋ</ts>
                  <nts id="Seg_771" n="HIAT:ip">"</nts>
                  <nts id="Seg_772" n="HIAT:ip">,</nts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_775" n="HIAT:w" s="T204">diːr</ts>
                  <nts id="Seg_776" n="HIAT:ip">.</nts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_779" n="HIAT:u" s="T205">
                  <ts e="T206" id="Seg_781" n="HIAT:w" s="T205">Oloruŋ</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_784" n="HIAT:w" s="T206">kelenner</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_787" n="HIAT:w" s="T207">bɨ͡atɨn</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_790" n="HIAT:w" s="T208">bɨha</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_793" n="HIAT:w" s="T209">kerbeːbittere</ts>
                  <nts id="Seg_794" n="HIAT:ip">,</nts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_797" n="HIAT:w" s="T210">ogonnʼor</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_800" n="HIAT:w" s="T211">tura</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_803" n="HIAT:w" s="T212">ekkireːte</ts>
                  <nts id="Seg_804" n="HIAT:ip">.</nts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_807" n="HIAT:u" s="T213">
                  <ts e="T214" id="Seg_809" n="HIAT:w" s="T213">Innʼe</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_812" n="HIAT:w" s="T214">gɨnan</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_815" n="HIAT:w" s="T215">bu</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_818" n="HIAT:w" s="T216">bɨ͡atɨn</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_821" n="HIAT:w" s="T217">kerbeːččilerge</ts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_824" n="HIAT:w" s="T218">naju͡omun</ts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_827" n="HIAT:w" s="T219">biːr</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_830" n="HIAT:w" s="T220">kɨːl</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_833" n="HIAT:w" s="T221">baːjtahɨnɨn</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_836" n="HIAT:w" s="T222">ölörön</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_839" n="HIAT:w" s="T223">bi͡erde</ts>
                  <nts id="Seg_840" n="HIAT:ip">.</nts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_843" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_845" n="HIAT:w" s="T224">Baː</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_848" n="HIAT:w" s="T225">ogonnʼoruŋ</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_851" n="HIAT:w" s="T226">otuːtugar</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_854" n="HIAT:w" s="T227">kaːman</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_857" n="HIAT:w" s="T228">kelle</ts>
                  <nts id="Seg_858" n="HIAT:ip">.</nts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T237" id="Seg_861" n="HIAT:u" s="T229">
                  <ts e="T230" id="Seg_863" n="HIAT:w" s="T229">Otuːtun</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_866" n="HIAT:w" s="T230">külün</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_869" n="HIAT:w" s="T231">bütünnüː</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_872" n="HIAT:w" s="T232">hɨ͡aldʼatɨgar</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_875" n="HIAT:w" s="T233">kaːlaːn</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_878" n="HIAT:w" s="T234">baran</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_881" n="HIAT:w" s="T235">kaːman</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_884" n="HIAT:w" s="T236">kaːlla</ts>
                  <nts id="Seg_885" n="HIAT:ip">.</nts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T247" id="Seg_888" n="HIAT:u" s="T237">
                  <ts e="T238" id="Seg_890" n="HIAT:w" s="T237">Dʼe</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_893" n="HIAT:w" s="T238">bu</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_896" n="HIAT:w" s="T239">baran</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_899" n="HIAT:w" s="T240">iste</ts>
                  <nts id="Seg_900" n="HIAT:ip">,</nts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_903" n="HIAT:w" s="T241">baran</ts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_906" n="HIAT:w" s="T242">ihen-ihen</ts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_909" n="HIAT:w" s="T243">araj</ts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_912" n="HIAT:w" s="T244">biːr</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_915" n="HIAT:w" s="T245">dʼi͡ege</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_918" n="HIAT:w" s="T246">tijde</ts>
                  <nts id="Seg_919" n="HIAT:ip">.</nts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T255" id="Seg_922" n="HIAT:u" s="T247">
                  <ts e="T248" id="Seg_924" n="HIAT:w" s="T247">Bu</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_927" n="HIAT:w" s="T248">dʼi͡ege</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_930" n="HIAT:w" s="T249">kelen</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_933" n="HIAT:w" s="T250">ihilleːn</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_936" n="HIAT:w" s="T251">körbüte</ts>
                  <nts id="Seg_937" n="HIAT:ip">,</nts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_940" n="HIAT:w" s="T252">hahɨl</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_943" n="HIAT:w" s="T253">kɨːran</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_946" n="HIAT:w" s="T254">erer</ts>
                  <nts id="Seg_947" n="HIAT:ip">.</nts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T264" id="Seg_950" n="HIAT:u" s="T255">
                  <ts e="T256" id="Seg_952" n="HIAT:w" s="T255">Dʼi͡e</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_955" n="HIAT:w" s="T256">ihiger</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_958" n="HIAT:w" s="T257">kiːrbite</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_961" n="HIAT:w" s="T258">hahɨl</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_964" n="HIAT:w" s="T259">bütünnüː</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_967" n="HIAT:w" s="T260">munnʼustan</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_970" n="HIAT:w" s="T261">baraːn</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_973" n="HIAT:w" s="T262">kɨːra</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_976" n="HIAT:w" s="T263">hɨtallar</ts>
                  <nts id="Seg_977" n="HIAT:ip">.</nts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T265" id="Seg_980" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_982" n="HIAT:w" s="T264">Ojunnaːktar</ts>
                  <nts id="Seg_983" n="HIAT:ip">.</nts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T268" id="Seg_986" n="HIAT:u" s="T265">
                  <ts e="T266" id="Seg_988" n="HIAT:w" s="T265">Ogonnʼor</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_991" n="HIAT:w" s="T266">kiːren</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_994" n="HIAT:w" s="T267">olordo</ts>
                  <nts id="Seg_995" n="HIAT:ip">.</nts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_998" n="HIAT:u" s="T268">
                  <ts e="T269" id="Seg_1000" n="HIAT:w" s="T268">Bu</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1003" n="HIAT:w" s="T269">olordoguna</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1006" n="HIAT:w" s="T270">hahɨlɨŋ</ts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1009" n="HIAT:w" s="T271">tönünne</ts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1012" n="HIAT:w" s="T272">kɨːran</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1015" n="HIAT:w" s="T273">baraːt</ts>
                  <nts id="Seg_1016" n="HIAT:ip">.</nts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T279" id="Seg_1019" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_1021" n="HIAT:w" s="T274">Bu</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1024" n="HIAT:w" s="T275">ogonnʼoruŋ</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1027" n="HIAT:w" s="T276">araj</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1030" n="HIAT:w" s="T277">düŋür</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1033" n="HIAT:w" s="T278">ogusta</ts>
                  <nts id="Seg_1034" n="HIAT:ip">.</nts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T284" id="Seg_1037" n="HIAT:u" s="T279">
                  <ts e="T280" id="Seg_1039" n="HIAT:w" s="T279">Bu</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1042" n="HIAT:w" s="T280">düŋürün</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1045" n="HIAT:w" s="T281">okso-okso</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1048" n="HIAT:w" s="T282">dʼe</ts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1051" n="HIAT:w" s="T283">ekkireːte</ts>
                  <nts id="Seg_1052" n="HIAT:ip">.</nts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T291" id="Seg_1055" n="HIAT:u" s="T284">
                  <ts e="T285" id="Seg_1057" n="HIAT:w" s="T284">Dʼe</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1060" n="HIAT:w" s="T285">manɨga</ts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1063" n="HIAT:w" s="T286">küle</ts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1066" n="HIAT:w" s="T287">dʼe</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1069" n="HIAT:w" s="T288">burgaŋnaːta</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1072" n="HIAT:w" s="T289">baː</ts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1075" n="HIAT:w" s="T290">hɨ͡alɨjatɨttan</ts>
                  <nts id="Seg_1076" n="HIAT:ip">.</nts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T298" id="Seg_1079" n="HIAT:u" s="T291">
                  <ts e="T292" id="Seg_1081" n="HIAT:w" s="T291">Dʼe</ts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1084" n="HIAT:w" s="T292">mantan</ts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1087" n="HIAT:w" s="T293">bu</ts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1090" n="HIAT:w" s="T294">hahɨllarɨŋ</ts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1093" n="HIAT:w" s="T295">bütünnüː</ts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1096" n="HIAT:w" s="T296">dʼe</ts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1099" n="HIAT:w" s="T297">külüstüler</ts>
                  <nts id="Seg_1100" n="HIAT:ip">.</nts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T304" id="Seg_1103" n="HIAT:u" s="T298">
                  <ts e="T299" id="Seg_1105" n="HIAT:w" s="T298">Araj</ts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1108" n="HIAT:w" s="T299">onno</ts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1111" n="HIAT:w" s="T300">ulagaːga</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1114" n="HIAT:w" s="T301">hahɨl</ts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1117" n="HIAT:w" s="T302">külere</ts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1120" n="HIAT:w" s="T303">ihilinne</ts>
                  <nts id="Seg_1121" n="HIAT:ip">.</nts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_1124" n="HIAT:u" s="T304">
                  <ts e="T305" id="Seg_1126" n="HIAT:w" s="T304">Ogonnʼor</ts>
                  <nts id="Seg_1127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1129" n="HIAT:w" s="T305">ihitte</ts>
                  <nts id="Seg_1130" n="HIAT:ip">,</nts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1133" n="HIAT:w" s="T306">düŋürün</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1136" n="HIAT:w" s="T307">bɨraːn</ts>
                  <nts id="Seg_1137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1139" n="HIAT:w" s="T308">keːste</ts>
                  <nts id="Seg_1140" n="HIAT:ip">,</nts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1143" n="HIAT:w" s="T309">ogonnʼor</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1146" n="HIAT:w" s="T310">maː</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1149" n="HIAT:w" s="T311">hahɨlɨ</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1152" n="HIAT:w" s="T312">kaban</ts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1155" n="HIAT:w" s="T313">ɨlla</ts>
                  <nts id="Seg_1156" n="HIAT:ip">,</nts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1159" n="HIAT:w" s="T314">hahɨlɨn</ts>
                  <nts id="Seg_1160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1162" n="HIAT:w" s="T315">u͡okka</ts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1165" n="HIAT:w" s="T316">ɨhaːra-ɨhaːra</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1168" n="HIAT:w" s="T317">dʼe</ts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1171" n="HIAT:w" s="T318">tahɨjda</ts>
                  <nts id="Seg_1172" n="HIAT:ip">.</nts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T322" id="Seg_1175" n="HIAT:u" s="T319">
                  <ts e="T320" id="Seg_1177" n="HIAT:w" s="T319">Tahɨjan-tahɨjan</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1180" n="HIAT:w" s="T320">ölörön</ts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1183" n="HIAT:w" s="T321">keːste</ts>
                  <nts id="Seg_1184" n="HIAT:ip">.</nts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T329" id="Seg_1187" n="HIAT:u" s="T322">
                  <ts e="T323" id="Seg_1189" n="HIAT:w" s="T322">Dʼe</ts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1192" n="HIAT:w" s="T323">ol</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1195" n="HIAT:w" s="T324">hahɨlɨŋ</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1198" n="HIAT:w" s="T325">mannaj</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1201" n="HIAT:w" s="T326">kini͡enin</ts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1204" n="HIAT:w" s="T327">ogolorun</ts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1207" n="HIAT:w" s="T328">hi͡ečči</ts>
                  <nts id="Seg_1208" n="HIAT:ip">.</nts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T336" id="Seg_1211" n="HIAT:u" s="T329">
                  <ts e="T330" id="Seg_1213" n="HIAT:w" s="T329">Hol</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1216" n="HIAT:w" s="T330">hahɨlɨŋ</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1219" n="HIAT:w" s="T331">bihikke</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1222" n="HIAT:w" s="T332">kelgijen</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1225" n="HIAT:w" s="T333">baran</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1228" n="HIAT:w" s="T334">tüŋneri</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1231" n="HIAT:w" s="T335">keːspite</ts>
                  <nts id="Seg_1232" n="HIAT:ip">.</nts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T345" id="Seg_1235" n="HIAT:u" s="T336">
                  <ts e="T337" id="Seg_1237" n="HIAT:w" s="T336">Innʼe</ts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1240" n="HIAT:w" s="T337">gɨnan</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1243" n="HIAT:w" s="T338">baran</ts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1246" n="HIAT:w" s="T339">dʼe</ts>
                  <nts id="Seg_1247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1249" n="HIAT:w" s="T340">iti</ts>
                  <nts id="Seg_1250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1252" n="HIAT:w" s="T341">ogonnʼorun</ts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1255" n="HIAT:w" s="T342">dʼi͡etiger</ts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1258" n="HIAT:w" s="T343">baran</ts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1261" n="HIAT:w" s="T344">olorbuta</ts>
                  <nts id="Seg_1262" n="HIAT:ip">.</nts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T348" id="Seg_1265" n="HIAT:u" s="T345">
                  <ts e="T346" id="Seg_1267" n="HIAT:w" s="T345">Dʼe</ts>
                  <nts id="Seg_1268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1270" n="HIAT:w" s="T346">iti</ts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1273" n="HIAT:w" s="T347">uhuga</ts>
                  <nts id="Seg_1274" n="HIAT:ip">.</nts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T348" id="Seg_1276" n="sc" s="T0">
               <ts e="T1" id="Seg_1278" n="e" s="T0">Dʼe </ts>
               <ts e="T2" id="Seg_1280" n="e" s="T1">biːr </ts>
               <ts e="T3" id="Seg_1282" n="e" s="T2">ogonnʼor </ts>
               <ts e="T4" id="Seg_1284" n="e" s="T3">olorbuta. </ts>
               <ts e="T5" id="Seg_1286" n="e" s="T4">Ukukuːt Čukukuːt </ts>
               <ts e="T6" id="Seg_1288" n="e" s="T5">aːta. </ts>
               <ts e="T7" id="Seg_1290" n="e" s="T6">Mantɨŋ </ts>
               <ts e="T8" id="Seg_1292" n="e" s="T7">ügüs </ts>
               <ts e="T9" id="Seg_1294" n="e" s="T8">bagajɨ </ts>
               <ts e="T10" id="Seg_1296" n="e" s="T9">ogoto. </ts>
               <ts e="T11" id="Seg_1298" n="e" s="T10">Dʼe </ts>
               <ts e="T12" id="Seg_1300" n="e" s="T11">bu </ts>
               <ts e="T13" id="Seg_1302" n="e" s="T12">manɨga </ts>
               <ts e="T14" id="Seg_1304" n="e" s="T13">hahɨl </ts>
               <ts e="T15" id="Seg_1306" n="e" s="T14">kelbite: </ts>
               <ts e="T16" id="Seg_1308" n="e" s="T15">"Ogonnʼor, </ts>
               <ts e="T17" id="Seg_1310" n="e" s="T16">biːr </ts>
               <ts e="T18" id="Seg_1312" n="e" s="T17">ogogun </ts>
               <ts e="T19" id="Seg_1314" n="e" s="T18">egel", </ts>
               <ts e="T20" id="Seg_1316" n="e" s="T19">diːr, </ts>
               <ts e="T21" id="Seg_1318" n="e" s="T20">"hi͡ekpin." </ts>
               <ts e="T22" id="Seg_1320" n="e" s="T21">Manɨga </ts>
               <ts e="T23" id="Seg_1322" n="e" s="T22">bu </ts>
               <ts e="T24" id="Seg_1324" n="e" s="T23">ogonnʼor </ts>
               <ts e="T25" id="Seg_1326" n="e" s="T24">ogotun </ts>
               <ts e="T26" id="Seg_1328" n="e" s="T25">bi͡erbite, </ts>
               <ts e="T27" id="Seg_1330" n="e" s="T26">hi͡etin. </ts>
               <ts e="T28" id="Seg_1332" n="e" s="T27">Dʼe </ts>
               <ts e="T29" id="Seg_1334" n="e" s="T28">ol </ts>
               <ts e="T30" id="Seg_1336" n="e" s="T29">barbɨta, </ts>
               <ts e="T31" id="Seg_1338" n="e" s="T30">kas </ts>
               <ts e="T32" id="Seg_1340" n="e" s="T31">eme </ts>
               <ts e="T33" id="Seg_1342" n="e" s="T32">konon </ts>
               <ts e="T34" id="Seg_1344" n="e" s="T33">baran </ts>
               <ts e="T35" id="Seg_1346" n="e" s="T34">emi͡e </ts>
               <ts e="T36" id="Seg_1348" n="e" s="T35">kelbite. </ts>
               <ts e="T37" id="Seg_1350" n="e" s="T36">"Ogonnʼor, </ts>
               <ts e="T38" id="Seg_1352" n="e" s="T37">emi͡e </ts>
               <ts e="T39" id="Seg_1354" n="e" s="T38">ogoto </ts>
               <ts e="T40" id="Seg_1356" n="e" s="T39">egel", </ts>
               <ts e="T41" id="Seg_1358" n="e" s="T40">diːr, </ts>
               <ts e="T42" id="Seg_1360" n="e" s="T41">"hi͡ekpin." </ts>
               <ts e="T43" id="Seg_1362" n="e" s="T42">Onuga </ts>
               <ts e="T44" id="Seg_1364" n="e" s="T43">ogonnʼor </ts>
               <ts e="T45" id="Seg_1366" n="e" s="T44">ogotun </ts>
               <ts e="T46" id="Seg_1368" n="e" s="T45">emi͡e </ts>
               <ts e="T47" id="Seg_1370" n="e" s="T46">bi͡erbite. </ts>
               <ts e="T48" id="Seg_1372" n="e" s="T47">Dʼe </ts>
               <ts e="T49" id="Seg_1374" n="e" s="T48">ol </ts>
               <ts e="T50" id="Seg_1376" n="e" s="T49">ogotun </ts>
               <ts e="T51" id="Seg_1378" n="e" s="T50">ilpite </ts>
               <ts e="T52" id="Seg_1380" n="e" s="T51">hi͡eri. </ts>
               <ts e="T53" id="Seg_1382" n="e" s="T52">Onton </ts>
               <ts e="T54" id="Seg_1384" n="e" s="T53">kas </ts>
               <ts e="T55" id="Seg_1386" n="e" s="T54">eme </ts>
               <ts e="T56" id="Seg_1388" n="e" s="T55">kun </ts>
               <ts e="T57" id="Seg_1390" n="e" s="T56">bu͡olan </ts>
               <ts e="T58" id="Seg_1392" n="e" s="T57">baran </ts>
               <ts e="T59" id="Seg_1394" n="e" s="T58">emi͡e </ts>
               <ts e="T60" id="Seg_1396" n="e" s="T59">kelbite. </ts>
               <ts e="T61" id="Seg_1398" n="e" s="T60">Ol </ts>
               <ts e="T62" id="Seg_1400" n="e" s="T61">kelen </ts>
               <ts e="T63" id="Seg_1402" n="e" s="T62">emi͡e </ts>
               <ts e="T64" id="Seg_1404" n="e" s="T63">diːr: </ts>
               <ts e="T65" id="Seg_1406" n="e" s="T64">"Ogoto </ts>
               <ts e="T66" id="Seg_1408" n="e" s="T65">egel", </ts>
               <ts e="T67" id="Seg_1410" n="e" s="T66">diːr, </ts>
               <ts e="T68" id="Seg_1412" n="e" s="T67">"hi͡ekpin." </ts>
               <ts e="T69" id="Seg_1414" n="e" s="T68">Ogoto </ts>
               <ts e="T70" id="Seg_1416" n="e" s="T69">elete </ts>
               <ts e="T71" id="Seg_1418" n="e" s="T70">bu͡olla. </ts>
               <ts e="T72" id="Seg_1420" n="e" s="T71">Dʼe </ts>
               <ts e="T73" id="Seg_1422" n="e" s="T72">ol </ts>
               <ts e="T74" id="Seg_1424" n="e" s="T73">barbɨta </ts>
               <ts e="T75" id="Seg_1426" n="e" s="T74">daː </ts>
               <ts e="T76" id="Seg_1428" n="e" s="T75">hahɨlɨŋ </ts>
               <ts e="T77" id="Seg_1430" n="e" s="T76">dʼɨlɨjbɨta </ts>
               <ts e="T78" id="Seg_1432" n="e" s="T77">bu. </ts>
               <ts e="T79" id="Seg_1434" n="e" s="T78">Ogonnʼoruŋ </ts>
               <ts e="T80" id="Seg_1436" n="e" s="T79">oloro </ts>
               <ts e="T81" id="Seg_1438" n="e" s="T80">hataːn </ts>
               <ts e="T82" id="Seg_1440" n="e" s="T81">baran </ts>
               <ts e="T83" id="Seg_1442" n="e" s="T82">kaːman </ts>
               <ts e="T84" id="Seg_1444" n="e" s="T83">kaːlla. </ts>
               <ts e="T85" id="Seg_1446" n="e" s="T84">Dʼe </ts>
               <ts e="T86" id="Seg_1448" n="e" s="T85">bu </ts>
               <ts e="T87" id="Seg_1450" n="e" s="T86">kaːman </ts>
               <ts e="T88" id="Seg_1452" n="e" s="T87">ihen-ihen </ts>
               <ts e="T89" id="Seg_1454" n="e" s="T88">baran </ts>
               <ts e="T90" id="Seg_1456" n="e" s="T89">biːr </ts>
               <ts e="T91" id="Seg_1458" n="e" s="T90">kɨːl </ts>
               <ts e="T92" id="Seg_1460" n="e" s="T91">baːjtahɨnɨn </ts>
               <ts e="T93" id="Seg_1462" n="e" s="T92">ölörön </ts>
               <ts e="T94" id="Seg_1464" n="e" s="T93">baran </ts>
               <ts e="T95" id="Seg_1466" n="e" s="T94">ü͡ölüne </ts>
               <ts e="T96" id="Seg_1468" n="e" s="T95">olordo. </ts>
               <ts e="T97" id="Seg_1470" n="e" s="T96">Bu </ts>
               <ts e="T98" id="Seg_1472" n="e" s="T97">olordoguna </ts>
               <ts e="T99" id="Seg_1474" n="e" s="T98">hahɨl </ts>
               <ts e="T100" id="Seg_1476" n="e" s="T99">kelle: </ts>
               <ts e="T101" id="Seg_1478" n="e" s="T100">"Čej </ts>
               <ts e="T102" id="Seg_1480" n="e" s="T101">ogonnʼor, </ts>
               <ts e="T103" id="Seg_1482" n="e" s="T102">bihigi </ts>
               <ts e="T104" id="Seg_1484" n="e" s="T103">ogo </ts>
               <ts e="T105" id="Seg_1486" n="e" s="T104">bu͡ola </ts>
               <ts e="T106" id="Seg_1488" n="e" s="T105">oːnnʼu͡ok", </ts>
               <ts e="T107" id="Seg_1490" n="e" s="T106">di͡ete. </ts>
               <ts e="T108" id="Seg_1492" n="e" s="T107">"Če, </ts>
               <ts e="T109" id="Seg_1494" n="e" s="T108">daːrɨm, </ts>
               <ts e="T110" id="Seg_1496" n="e" s="T109">ogo </ts>
               <ts e="T111" id="Seg_1498" n="e" s="T110">bu͡ola </ts>
               <ts e="T112" id="Seg_1500" n="e" s="T111">oːnnʼu͡ok." </ts>
               <ts e="T113" id="Seg_1502" n="e" s="T112">Bu </ts>
               <ts e="T114" id="Seg_1504" n="e" s="T113">ogonnʼoruŋ </ts>
               <ts e="T115" id="Seg_1506" n="e" s="T114">bihik </ts>
               <ts e="T116" id="Seg_1508" n="e" s="T115">oŋordo </ts>
               <ts e="T117" id="Seg_1510" n="e" s="T116">ol </ts>
               <ts e="T118" id="Seg_1512" n="e" s="T117">bajtahɨnnarɨn </ts>
               <ts e="T119" id="Seg_1514" n="e" s="T118">tiriːtinen. </ts>
               <ts e="T120" id="Seg_1516" n="e" s="T119">Bihik </ts>
               <ts e="T121" id="Seg_1518" n="e" s="T120">bɨ͡ata </ts>
               <ts e="T122" id="Seg_1520" n="e" s="T121">oŋorunnular. </ts>
               <ts e="T123" id="Seg_1522" n="e" s="T122">Ogonnʼoruŋ </ts>
               <ts e="T124" id="Seg_1524" n="e" s="T123">di͡ebit: </ts>
               <ts e="T125" id="Seg_1526" n="e" s="T124">"Če, </ts>
               <ts e="T126" id="Seg_1528" n="e" s="T125">doː, </ts>
               <ts e="T127" id="Seg_1530" n="e" s="T126">en </ts>
               <ts e="T128" id="Seg_1532" n="e" s="T127">bigen </ts>
               <ts e="T129" id="Seg_1534" n="e" s="T128">ogo </ts>
               <ts e="T130" id="Seg_1536" n="e" s="T129">bu͡olan", </ts>
               <ts e="T131" id="Seg_1538" n="e" s="T130">di͡ete. </ts>
               <ts e="T132" id="Seg_1540" n="e" s="T131">Hahɨl </ts>
               <ts e="T133" id="Seg_1542" n="e" s="T132">manɨga </ts>
               <ts e="T134" id="Seg_1544" n="e" s="T133">hɨtan </ts>
               <ts e="T135" id="Seg_1546" n="e" s="T134">kördö: </ts>
               <ts e="T136" id="Seg_1548" n="e" s="T135">"Eː, </ts>
               <ts e="T137" id="Seg_1550" n="e" s="T136">dogor, </ts>
               <ts e="T138" id="Seg_1552" n="e" s="T137">mi͡ene </ts>
               <ts e="T139" id="Seg_1554" n="e" s="T138">kuturugum </ts>
               <ts e="T140" id="Seg_1556" n="e" s="T139">ɨ͡aldʼar, </ts>
               <ts e="T141" id="Seg_1558" n="e" s="T140">hɨtɨ͡arɨ͡a </ts>
               <ts e="T142" id="Seg_1560" n="e" s="T141">hu͡ok", </ts>
               <ts e="T143" id="Seg_1562" n="e" s="T142">di͡ete. </ts>
               <ts e="T144" id="Seg_1564" n="e" s="T143">"Če </ts>
               <ts e="T145" id="Seg_1566" n="e" s="T144">kihi </ts>
               <ts e="T146" id="Seg_1568" n="e" s="T145">kihiginen </ts>
               <ts e="T147" id="Seg_1570" n="e" s="T146">en </ts>
               <ts e="T148" id="Seg_1572" n="e" s="T147">hɨtan </ts>
               <ts e="T149" id="Seg_1574" n="e" s="T148">ogo </ts>
               <ts e="T150" id="Seg_1576" n="e" s="T149">bu͡ol", </ts>
               <ts e="T151" id="Seg_1578" n="e" s="T150">di͡ete </ts>
               <ts e="T152" id="Seg_1580" n="e" s="T151">hahɨl </ts>
               <ts e="T153" id="Seg_1582" n="e" s="T152">ogonnʼoru. </ts>
               <ts e="T154" id="Seg_1584" n="e" s="T153">Ogonnʼor </ts>
               <ts e="T155" id="Seg_1586" n="e" s="T154">dʼe </ts>
               <ts e="T156" id="Seg_1588" n="e" s="T155">hɨtta. </ts>
               <ts e="T157" id="Seg_1590" n="e" s="T156">Hahɨl </ts>
               <ts e="T158" id="Seg_1592" n="e" s="T157">dʼe </ts>
               <ts e="T159" id="Seg_1594" n="e" s="T158">manɨ </ts>
               <ts e="T160" id="Seg_1596" n="e" s="T159">kelgijen </ts>
               <ts e="T161" id="Seg_1598" n="e" s="T160">keːste. </ts>
               <ts e="T162" id="Seg_1600" n="e" s="T161">Hahɨl </ts>
               <ts e="T163" id="Seg_1602" n="e" s="T162">manɨ </ts>
               <ts e="T164" id="Seg_1604" n="e" s="T163">bigiː-bigiː </ts>
               <ts e="T165" id="Seg_1606" n="e" s="T164">kɨːl </ts>
               <ts e="T166" id="Seg_1608" n="e" s="T165">etinen </ts>
               <ts e="T167" id="Seg_1610" n="e" s="T166">ɨstaːn </ts>
               <ts e="T168" id="Seg_1612" n="e" s="T167">ahata </ts>
               <ts e="T169" id="Seg_1614" n="e" s="T168">olordo. </ts>
               <ts e="T170" id="Seg_1616" n="e" s="T169">Bu </ts>
               <ts e="T171" id="Seg_1618" n="e" s="T170">ogonnʼoruŋ </ts>
               <ts e="T172" id="Seg_1620" n="e" s="T171">araj </ts>
               <ts e="T173" id="Seg_1622" n="e" s="T172">utujan </ts>
               <ts e="T174" id="Seg_1624" n="e" s="T173">kaːlla. </ts>
               <ts e="T175" id="Seg_1626" n="e" s="T174">Hahɨl </ts>
               <ts e="T176" id="Seg_1628" n="e" s="T175">manɨ </ts>
               <ts e="T177" id="Seg_1630" n="e" s="T176">hagɨstaːn </ts>
               <ts e="T178" id="Seg_1632" n="e" s="T177">ildʼen </ts>
               <ts e="T179" id="Seg_1634" n="e" s="T178">bajaraːktan </ts>
               <ts e="T180" id="Seg_1636" n="e" s="T179">tüŋneri </ts>
               <ts e="T181" id="Seg_1638" n="e" s="T180">bɨrakta. </ts>
               <ts e="T182" id="Seg_1640" n="e" s="T181">Kojut </ts>
               <ts e="T183" id="Seg_1642" n="e" s="T182">ol </ts>
               <ts e="T184" id="Seg_1644" n="e" s="T183">ogonnʼor </ts>
               <ts e="T185" id="Seg_1646" n="e" s="T184">uhuktubuta </ts>
               <ts e="T186" id="Seg_1648" n="e" s="T185">bajaraːk </ts>
               <ts e="T187" id="Seg_1650" n="e" s="T186">ihiger </ts>
               <ts e="T188" id="Seg_1652" n="e" s="T187">hɨtar. </ts>
               <ts e="T189" id="Seg_1654" n="e" s="T188">Ogonnʼor </ts>
               <ts e="T190" id="Seg_1656" n="e" s="T189">kaja </ts>
               <ts e="T191" id="Seg_1658" n="e" s="T190">da </ts>
               <ts e="T192" id="Seg_1660" n="e" s="T191">di͡eg </ts>
               <ts e="T193" id="Seg_1662" n="e" s="T192">kamnɨ͡agar </ts>
               <ts e="T194" id="Seg_1664" n="e" s="T193">bert, </ts>
               <ts e="T195" id="Seg_1666" n="e" s="T194">kelgillibit </ts>
               <ts e="T196" id="Seg_1668" n="e" s="T195">kihi. </ts>
               <ts e="T197" id="Seg_1670" n="e" s="T196">Ogonnʼor </ts>
               <ts e="T198" id="Seg_1672" n="e" s="T197">ɨŋɨrar: </ts>
               <ts e="T199" id="Seg_1674" n="e" s="T198">"Gornu͡oktar, </ts>
               <ts e="T200" id="Seg_1676" n="e" s="T199">kɨrsalar, </ts>
               <ts e="T201" id="Seg_1678" n="e" s="T200">kutujaktar, </ts>
               <ts e="T202" id="Seg_1680" n="e" s="T201">bɨ͡abɨn </ts>
               <ts e="T203" id="Seg_1682" n="e" s="T202">bɨha </ts>
               <ts e="T204" id="Seg_1684" n="e" s="T203">kerbeːŋ", </ts>
               <ts e="T205" id="Seg_1686" n="e" s="T204">diːr. </ts>
               <ts e="T206" id="Seg_1688" n="e" s="T205">Oloruŋ </ts>
               <ts e="T207" id="Seg_1690" n="e" s="T206">kelenner </ts>
               <ts e="T208" id="Seg_1692" n="e" s="T207">bɨ͡atɨn </ts>
               <ts e="T209" id="Seg_1694" n="e" s="T208">bɨha </ts>
               <ts e="T210" id="Seg_1696" n="e" s="T209">kerbeːbittere, </ts>
               <ts e="T211" id="Seg_1698" n="e" s="T210">ogonnʼor </ts>
               <ts e="T212" id="Seg_1700" n="e" s="T211">tura </ts>
               <ts e="T213" id="Seg_1702" n="e" s="T212">ekkireːte. </ts>
               <ts e="T214" id="Seg_1704" n="e" s="T213">Innʼe </ts>
               <ts e="T215" id="Seg_1706" n="e" s="T214">gɨnan </ts>
               <ts e="T216" id="Seg_1708" n="e" s="T215">bu </ts>
               <ts e="T217" id="Seg_1710" n="e" s="T216">bɨ͡atɨn </ts>
               <ts e="T218" id="Seg_1712" n="e" s="T217">kerbeːččilerge </ts>
               <ts e="T219" id="Seg_1714" n="e" s="T218">naju͡omun </ts>
               <ts e="T220" id="Seg_1716" n="e" s="T219">biːr </ts>
               <ts e="T221" id="Seg_1718" n="e" s="T220">kɨːl </ts>
               <ts e="T222" id="Seg_1720" n="e" s="T221">baːjtahɨnɨn </ts>
               <ts e="T223" id="Seg_1722" n="e" s="T222">ölörön </ts>
               <ts e="T224" id="Seg_1724" n="e" s="T223">bi͡erde. </ts>
               <ts e="T225" id="Seg_1726" n="e" s="T224">Baː </ts>
               <ts e="T226" id="Seg_1728" n="e" s="T225">ogonnʼoruŋ </ts>
               <ts e="T227" id="Seg_1730" n="e" s="T226">otuːtugar </ts>
               <ts e="T228" id="Seg_1732" n="e" s="T227">kaːman </ts>
               <ts e="T229" id="Seg_1734" n="e" s="T228">kelle. </ts>
               <ts e="T230" id="Seg_1736" n="e" s="T229">Otuːtun </ts>
               <ts e="T231" id="Seg_1738" n="e" s="T230">külün </ts>
               <ts e="T232" id="Seg_1740" n="e" s="T231">bütünnüː </ts>
               <ts e="T233" id="Seg_1742" n="e" s="T232">hɨ͡aldʼatɨgar </ts>
               <ts e="T234" id="Seg_1744" n="e" s="T233">kaːlaːn </ts>
               <ts e="T235" id="Seg_1746" n="e" s="T234">baran </ts>
               <ts e="T236" id="Seg_1748" n="e" s="T235">kaːman </ts>
               <ts e="T237" id="Seg_1750" n="e" s="T236">kaːlla. </ts>
               <ts e="T238" id="Seg_1752" n="e" s="T237">Dʼe </ts>
               <ts e="T239" id="Seg_1754" n="e" s="T238">bu </ts>
               <ts e="T240" id="Seg_1756" n="e" s="T239">baran </ts>
               <ts e="T241" id="Seg_1758" n="e" s="T240">iste, </ts>
               <ts e="T242" id="Seg_1760" n="e" s="T241">baran </ts>
               <ts e="T243" id="Seg_1762" n="e" s="T242">ihen-ihen </ts>
               <ts e="T244" id="Seg_1764" n="e" s="T243">araj </ts>
               <ts e="T245" id="Seg_1766" n="e" s="T244">biːr </ts>
               <ts e="T246" id="Seg_1768" n="e" s="T245">dʼi͡ege </ts>
               <ts e="T247" id="Seg_1770" n="e" s="T246">tijde. </ts>
               <ts e="T248" id="Seg_1772" n="e" s="T247">Bu </ts>
               <ts e="T249" id="Seg_1774" n="e" s="T248">dʼi͡ege </ts>
               <ts e="T250" id="Seg_1776" n="e" s="T249">kelen </ts>
               <ts e="T251" id="Seg_1778" n="e" s="T250">ihilleːn </ts>
               <ts e="T252" id="Seg_1780" n="e" s="T251">körbüte, </ts>
               <ts e="T253" id="Seg_1782" n="e" s="T252">hahɨl </ts>
               <ts e="T254" id="Seg_1784" n="e" s="T253">kɨːran </ts>
               <ts e="T255" id="Seg_1786" n="e" s="T254">erer. </ts>
               <ts e="T256" id="Seg_1788" n="e" s="T255">Dʼi͡e </ts>
               <ts e="T257" id="Seg_1790" n="e" s="T256">ihiger </ts>
               <ts e="T258" id="Seg_1792" n="e" s="T257">kiːrbite </ts>
               <ts e="T259" id="Seg_1794" n="e" s="T258">hahɨl </ts>
               <ts e="T260" id="Seg_1796" n="e" s="T259">bütünnüː </ts>
               <ts e="T261" id="Seg_1798" n="e" s="T260">munnʼustan </ts>
               <ts e="T262" id="Seg_1800" n="e" s="T261">baraːn </ts>
               <ts e="T263" id="Seg_1802" n="e" s="T262">kɨːra </ts>
               <ts e="T264" id="Seg_1804" n="e" s="T263">hɨtallar. </ts>
               <ts e="T265" id="Seg_1806" n="e" s="T264">Ojunnaːktar. </ts>
               <ts e="T266" id="Seg_1808" n="e" s="T265">Ogonnʼor </ts>
               <ts e="T267" id="Seg_1810" n="e" s="T266">kiːren </ts>
               <ts e="T268" id="Seg_1812" n="e" s="T267">olordo. </ts>
               <ts e="T269" id="Seg_1814" n="e" s="T268">Bu </ts>
               <ts e="T270" id="Seg_1816" n="e" s="T269">olordoguna </ts>
               <ts e="T271" id="Seg_1818" n="e" s="T270">hahɨlɨŋ </ts>
               <ts e="T272" id="Seg_1820" n="e" s="T271">tönünne </ts>
               <ts e="T273" id="Seg_1822" n="e" s="T272">kɨːran </ts>
               <ts e="T274" id="Seg_1824" n="e" s="T273">baraːt. </ts>
               <ts e="T275" id="Seg_1826" n="e" s="T274">Bu </ts>
               <ts e="T276" id="Seg_1828" n="e" s="T275">ogonnʼoruŋ </ts>
               <ts e="T277" id="Seg_1830" n="e" s="T276">araj </ts>
               <ts e="T278" id="Seg_1832" n="e" s="T277">düŋür </ts>
               <ts e="T279" id="Seg_1834" n="e" s="T278">ogusta. </ts>
               <ts e="T280" id="Seg_1836" n="e" s="T279">Bu </ts>
               <ts e="T281" id="Seg_1838" n="e" s="T280">düŋürün </ts>
               <ts e="T282" id="Seg_1840" n="e" s="T281">okso-okso </ts>
               <ts e="T283" id="Seg_1842" n="e" s="T282">dʼe </ts>
               <ts e="T284" id="Seg_1844" n="e" s="T283">ekkireːte. </ts>
               <ts e="T285" id="Seg_1846" n="e" s="T284">Dʼe </ts>
               <ts e="T286" id="Seg_1848" n="e" s="T285">manɨga </ts>
               <ts e="T287" id="Seg_1850" n="e" s="T286">küle </ts>
               <ts e="T288" id="Seg_1852" n="e" s="T287">dʼe </ts>
               <ts e="T289" id="Seg_1854" n="e" s="T288">burgaŋnaːta </ts>
               <ts e="T290" id="Seg_1856" n="e" s="T289">baː </ts>
               <ts e="T291" id="Seg_1858" n="e" s="T290">hɨ͡alɨjatɨttan. </ts>
               <ts e="T292" id="Seg_1860" n="e" s="T291">Dʼe </ts>
               <ts e="T293" id="Seg_1862" n="e" s="T292">mantan </ts>
               <ts e="T294" id="Seg_1864" n="e" s="T293">bu </ts>
               <ts e="T295" id="Seg_1866" n="e" s="T294">hahɨllarɨŋ </ts>
               <ts e="T296" id="Seg_1868" n="e" s="T295">bütünnüː </ts>
               <ts e="T297" id="Seg_1870" n="e" s="T296">dʼe </ts>
               <ts e="T298" id="Seg_1872" n="e" s="T297">külüstüler. </ts>
               <ts e="T299" id="Seg_1874" n="e" s="T298">Araj </ts>
               <ts e="T300" id="Seg_1876" n="e" s="T299">onno </ts>
               <ts e="T301" id="Seg_1878" n="e" s="T300">ulagaːga </ts>
               <ts e="T302" id="Seg_1880" n="e" s="T301">hahɨl </ts>
               <ts e="T303" id="Seg_1882" n="e" s="T302">külere </ts>
               <ts e="T304" id="Seg_1884" n="e" s="T303">ihilinne. </ts>
               <ts e="T305" id="Seg_1886" n="e" s="T304">Ogonnʼor </ts>
               <ts e="T306" id="Seg_1888" n="e" s="T305">ihitte, </ts>
               <ts e="T307" id="Seg_1890" n="e" s="T306">düŋürün </ts>
               <ts e="T308" id="Seg_1892" n="e" s="T307">bɨraːn </ts>
               <ts e="T309" id="Seg_1894" n="e" s="T308">keːste, </ts>
               <ts e="T310" id="Seg_1896" n="e" s="T309">ogonnʼor </ts>
               <ts e="T311" id="Seg_1898" n="e" s="T310">maː </ts>
               <ts e="T312" id="Seg_1900" n="e" s="T311">hahɨlɨ </ts>
               <ts e="T313" id="Seg_1902" n="e" s="T312">kaban </ts>
               <ts e="T314" id="Seg_1904" n="e" s="T313">ɨlla, </ts>
               <ts e="T315" id="Seg_1906" n="e" s="T314">hahɨlɨn </ts>
               <ts e="T316" id="Seg_1908" n="e" s="T315">u͡okka </ts>
               <ts e="T317" id="Seg_1910" n="e" s="T316">ɨhaːra-ɨhaːra </ts>
               <ts e="T318" id="Seg_1912" n="e" s="T317">dʼe </ts>
               <ts e="T319" id="Seg_1914" n="e" s="T318">tahɨjda. </ts>
               <ts e="T320" id="Seg_1916" n="e" s="T319">Tahɨjan-tahɨjan </ts>
               <ts e="T321" id="Seg_1918" n="e" s="T320">ölörön </ts>
               <ts e="T322" id="Seg_1920" n="e" s="T321">keːste. </ts>
               <ts e="T323" id="Seg_1922" n="e" s="T322">Dʼe </ts>
               <ts e="T324" id="Seg_1924" n="e" s="T323">ol </ts>
               <ts e="T325" id="Seg_1926" n="e" s="T324">hahɨlɨŋ </ts>
               <ts e="T326" id="Seg_1928" n="e" s="T325">mannaj </ts>
               <ts e="T327" id="Seg_1930" n="e" s="T326">kini͡enin </ts>
               <ts e="T328" id="Seg_1932" n="e" s="T327">ogolorun </ts>
               <ts e="T329" id="Seg_1934" n="e" s="T328">hi͡ečči. </ts>
               <ts e="T330" id="Seg_1936" n="e" s="T329">Hol </ts>
               <ts e="T331" id="Seg_1938" n="e" s="T330">hahɨlɨŋ </ts>
               <ts e="T332" id="Seg_1940" n="e" s="T331">bihikke </ts>
               <ts e="T333" id="Seg_1942" n="e" s="T332">kelgijen </ts>
               <ts e="T334" id="Seg_1944" n="e" s="T333">baran </ts>
               <ts e="T335" id="Seg_1946" n="e" s="T334">tüŋneri </ts>
               <ts e="T336" id="Seg_1948" n="e" s="T335">keːspite. </ts>
               <ts e="T337" id="Seg_1950" n="e" s="T336">Innʼe </ts>
               <ts e="T338" id="Seg_1952" n="e" s="T337">gɨnan </ts>
               <ts e="T339" id="Seg_1954" n="e" s="T338">baran </ts>
               <ts e="T340" id="Seg_1956" n="e" s="T339">dʼe </ts>
               <ts e="T341" id="Seg_1958" n="e" s="T340">iti </ts>
               <ts e="T342" id="Seg_1960" n="e" s="T341">ogonnʼorun </ts>
               <ts e="T343" id="Seg_1962" n="e" s="T342">dʼi͡etiger </ts>
               <ts e="T344" id="Seg_1964" n="e" s="T343">baran </ts>
               <ts e="T345" id="Seg_1966" n="e" s="T344">olorbuta. </ts>
               <ts e="T346" id="Seg_1968" n="e" s="T345">Dʼe </ts>
               <ts e="T347" id="Seg_1970" n="e" s="T346">iti </ts>
               <ts e="T348" id="Seg_1972" n="e" s="T347">uhuga. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_1973" s="T0">FeA_1931_OldManUkukuutFox_flk.001</ta>
            <ta e="T6" id="Seg_1974" s="T4">FeA_1931_OldManUkukuutFox_flk.002</ta>
            <ta e="T10" id="Seg_1975" s="T6">FeA_1931_OldManUkukuutFox_flk.003</ta>
            <ta e="T15" id="Seg_1976" s="T10">FeA_1931_OldManUkukuutFox_flk.004</ta>
            <ta e="T21" id="Seg_1977" s="T15">FeA_1931_OldManUkukuutFox_flk.005</ta>
            <ta e="T27" id="Seg_1978" s="T21">FeA_1931_OldManUkukuutFox_flk.006</ta>
            <ta e="T36" id="Seg_1979" s="T27">FeA_1931_OldManUkukuutFox_flk.007</ta>
            <ta e="T42" id="Seg_1980" s="T36">FeA_1931_OldManUkukuutFox_flk.008</ta>
            <ta e="T47" id="Seg_1981" s="T42">FeA_1931_OldManUkukuutFox_flk.009</ta>
            <ta e="T52" id="Seg_1982" s="T47">FeA_1931_OldManUkukuutFox_flk.010</ta>
            <ta e="T60" id="Seg_1983" s="T52">FeA_1931_OldManUkukuutFox_flk.011</ta>
            <ta e="T64" id="Seg_1984" s="T60">FeA_1931_OldManUkukuutFox_flk.012</ta>
            <ta e="T68" id="Seg_1985" s="T64">FeA_1931_OldManUkukuutFox_flk.013</ta>
            <ta e="T71" id="Seg_1986" s="T68">FeA_1931_OldManUkukuutFox_flk.014</ta>
            <ta e="T78" id="Seg_1987" s="T71">FeA_1931_OldManUkukuutFox_flk.015</ta>
            <ta e="T84" id="Seg_1988" s="T78">FeA_1931_OldManUkukuutFox_flk.016</ta>
            <ta e="T96" id="Seg_1989" s="T84">FeA_1931_OldManUkukuutFox_flk.017</ta>
            <ta e="T100" id="Seg_1990" s="T96">FeA_1931_OldManUkukuutFox_flk.018</ta>
            <ta e="T107" id="Seg_1991" s="T100">FeA_1931_OldManUkukuutFox_flk.019</ta>
            <ta e="T112" id="Seg_1992" s="T107">FeA_1931_OldManUkukuutFox_flk.020</ta>
            <ta e="T119" id="Seg_1993" s="T112">FeA_1931_OldManUkukuutFox_flk.021</ta>
            <ta e="T122" id="Seg_1994" s="T119">FeA_1931_OldManUkukuutFox_flk.022</ta>
            <ta e="T124" id="Seg_1995" s="T122">FeA_1931_OldManUkukuutFox_flk.023</ta>
            <ta e="T131" id="Seg_1996" s="T124">FeA_1931_OldManUkukuutFox_flk.024</ta>
            <ta e="T135" id="Seg_1997" s="T131">FeA_1931_OldManUkukuutFox_flk.025</ta>
            <ta e="T143" id="Seg_1998" s="T135">FeA_1931_OldManUkukuutFox_flk.026</ta>
            <ta e="T153" id="Seg_1999" s="T143">FeA_1931_OldManUkukuutFox_flk.027</ta>
            <ta e="T156" id="Seg_2000" s="T153">FeA_1931_OldManUkukuutFox_flk.028</ta>
            <ta e="T161" id="Seg_2001" s="T156">FeA_1931_OldManUkukuutFox_flk.029</ta>
            <ta e="T169" id="Seg_2002" s="T161">FeA_1931_OldManUkukuutFox_flk.030</ta>
            <ta e="T174" id="Seg_2003" s="T169">FeA_1931_OldManUkukuutFox_flk.031</ta>
            <ta e="T181" id="Seg_2004" s="T174">FeA_1931_OldManUkukuutFox_flk.032</ta>
            <ta e="T188" id="Seg_2005" s="T181">FeA_1931_OldManUkukuutFox_flk.033</ta>
            <ta e="T196" id="Seg_2006" s="T188">FeA_1931_OldManUkukuutFox_flk.034</ta>
            <ta e="T198" id="Seg_2007" s="T196">FeA_1931_OldManUkukuutFox_flk.035</ta>
            <ta e="T205" id="Seg_2008" s="T198">FeA_1931_OldManUkukuutFox_flk.036</ta>
            <ta e="T213" id="Seg_2009" s="T205">FeA_1931_OldManUkukuutFox_flk.037</ta>
            <ta e="T224" id="Seg_2010" s="T213">FeA_1931_OldManUkukuutFox_flk.038</ta>
            <ta e="T229" id="Seg_2011" s="T224">FeA_1931_OldManUkukuutFox_flk.039</ta>
            <ta e="T237" id="Seg_2012" s="T229">FeA_1931_OldManUkukuutFox_flk.040</ta>
            <ta e="T247" id="Seg_2013" s="T237">FeA_1931_OldManUkukuutFox_flk.041</ta>
            <ta e="T255" id="Seg_2014" s="T247">FeA_1931_OldManUkukuutFox_flk.042</ta>
            <ta e="T264" id="Seg_2015" s="T255">FeA_1931_OldManUkukuutFox_flk.043</ta>
            <ta e="T265" id="Seg_2016" s="T264">FeA_1931_OldManUkukuutFox_flk.044</ta>
            <ta e="T268" id="Seg_2017" s="T265">FeA_1931_OldManUkukuutFox_flk.045</ta>
            <ta e="T274" id="Seg_2018" s="T268">FeA_1931_OldManUkukuutFox_flk.046</ta>
            <ta e="T279" id="Seg_2019" s="T274">FeA_1931_OldManUkukuutFox_flk.047</ta>
            <ta e="T284" id="Seg_2020" s="T279">FeA_1931_OldManUkukuutFox_flk.048</ta>
            <ta e="T291" id="Seg_2021" s="T284">FeA_1931_OldManUkukuutFox_flk.049</ta>
            <ta e="T298" id="Seg_2022" s="T291">FeA_1931_OldManUkukuutFox_flk.050</ta>
            <ta e="T304" id="Seg_2023" s="T298">FeA_1931_OldManUkukuutFox_flk.051</ta>
            <ta e="T319" id="Seg_2024" s="T304">FeA_1931_OldManUkukuutFox_flk.052</ta>
            <ta e="T322" id="Seg_2025" s="T319">FeA_1931_OldManUkukuutFox_flk.053</ta>
            <ta e="T329" id="Seg_2026" s="T322">FeA_1931_OldManUkukuutFox_flk.054</ta>
            <ta e="T336" id="Seg_2027" s="T329">FeA_1931_OldManUkukuutFox_flk.055</ta>
            <ta e="T345" id="Seg_2028" s="T336">FeA_1931_OldManUkukuutFox_flk.056</ta>
            <ta e="T348" id="Seg_2029" s="T345">FeA_1931_OldManUkukuutFox_flk.057</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_2030" s="T0">Дьэ биир огонньор олорбута.</ta>
            <ta e="T6" id="Seg_2031" s="T4">Укукуут-Чукукуут аата.</ta>
            <ta e="T10" id="Seg_2032" s="T6">Мантыҥ үгүс багайы огото.</ta>
            <ta e="T15" id="Seg_2033" s="T10">Дьэ бу маныга һаһыл кэлбитэ: </ta>
            <ta e="T21" id="Seg_2034" s="T15">— Огонньор, биир огогун эгэл, — диир, — һиэкпин.</ta>
            <ta e="T27" id="Seg_2035" s="T21">Маныга бу огонньор оготун биэрбитэ, һиэтин.</ta>
            <ta e="T36" id="Seg_2036" s="T27">Дьэ ол барбыта, кас эмэ конон баран эмиэ кэлбитэ.</ta>
            <ta e="T42" id="Seg_2037" s="T36">— Огонньор, эмиэ огото эгэл, — диир, — һиэкпин.</ta>
            <ta e="T47" id="Seg_2038" s="T42">Онуга огонньор оготун эмиэ биэрбитэ.</ta>
            <ta e="T52" id="Seg_2039" s="T47">Дьэ ол оготун илпитэ һиэри.</ta>
            <ta e="T60" id="Seg_2040" s="T52">Онтон кас эмэ кун буолан баран эмиэ кэлбитэ.</ta>
            <ta e="T64" id="Seg_2041" s="T60">Ол кэлэн эмиэ диир: </ta>
            <ta e="T68" id="Seg_2042" s="T64">— Огото эгэл, — диир — һиэкпин.</ta>
            <ta e="T71" id="Seg_2043" s="T68">Огото элэтэ буолла.</ta>
            <ta e="T78" id="Seg_2044" s="T71">Дьэ ол барбыта даа һаһылыҥ дьылыйбыта бу.</ta>
            <ta e="T84" id="Seg_2045" s="T78">Огонньоруҥ олоро һатаан баран кааман каалла.</ta>
            <ta e="T96" id="Seg_2046" s="T84">Дьэ бу кааман иһэн-иһэн баран биир кыыл баайтаһынын өлөрөн баран үөлүнэ олордо.</ta>
            <ta e="T100" id="Seg_2047" s="T96">Бу олордогуна һаһыл кэллэ: </ta>
            <ta e="T107" id="Seg_2048" s="T100">— Чэй огонньор, биһиги ого буола оонньуок, — диэтэ.</ta>
            <ta e="T112" id="Seg_2049" s="T107">— Чэ, даарым, ого [буола] оонньуок.</ta>
            <ta e="T119" id="Seg_2050" s="T112">Бу огонньоруҥ биһик оҥордо ол байтаһыннарын тириитинэн.</ta>
            <ta e="T122" id="Seg_2051" s="T119">Биһик быата оҥоруннулар.</ta>
            <ta e="T124" id="Seg_2052" s="T122">Огонньоруҥ диэбит: </ta>
            <ta e="T131" id="Seg_2053" s="T124">— Чэ, доо, эн бигэн ого буолан, — диэтэ.</ta>
            <ta e="T135" id="Seg_2054" s="T131">һаһыл маныга һытан көрдө: </ta>
            <ta e="T143" id="Seg_2055" s="T135">— Ээ, догор, миэнэ кутуругум ыалдьар, һытыарыа һуок, — диэтэ.</ta>
            <ta e="T153" id="Seg_2056" s="T143">— Чэ киһи киһигинэн эн һытан ого буол, — диэтэ һаһыл огонньору.</ta>
            <ta e="T156" id="Seg_2057" s="T153">Огонньор дьэ һытта.</ta>
            <ta e="T161" id="Seg_2058" s="T156">Һаһыл дьэ маны кэлгийэн кээстэ.</ta>
            <ta e="T169" id="Seg_2059" s="T161">Һаһыл маны бигии-бигии кыыл этинэн ыстаан аһата олордо.</ta>
            <ta e="T174" id="Seg_2060" s="T169">Бу огонньоруҥ арай утуйан каалла.</ta>
            <ta e="T181" id="Seg_2061" s="T174">Һаһыл маны һагыстаан илдьэн байараактан түҥнэри быракта.</ta>
            <ta e="T188" id="Seg_2062" s="T181">Койут ол огонньор уһуктубута байараак иһигэр һытар.</ta>
            <ta e="T196" id="Seg_2063" s="T188">Огонньор кайа да диэг камныагар бэрт, кэлгиллибит киһи.</ta>
            <ta e="T198" id="Seg_2064" s="T196">Огонньор ыҥырар: </ta>
            <ta e="T205" id="Seg_2065" s="T198">— Горнуоктар, кырсалар, кутуйактар, быабын быһа кэрбээҥ! — диир.</ta>
            <ta e="T213" id="Seg_2066" s="T205">Олоруҥ кэлэннэр быатын быһа кэрбээбиттэрэ, огонньор тура эккирээтэ.</ta>
            <ta e="T224" id="Seg_2067" s="T213">Инньэ гынан бу быатын кэрбээччилэргэ найуомун биир кыыл баайтаһынын өлөрөн биэрдэ.</ta>
            <ta e="T229" id="Seg_2068" s="T224">Баа огонньоруҥ отуутугар кааман кэллэ.</ta>
            <ta e="T237" id="Seg_2069" s="T229">Отуутун күлүн бүтүннүү һыалдьатыгар каалаан баран кааман каалла.</ta>
            <ta e="T247" id="Seg_2070" s="T237">Дьэ бу баран истэ, баран иһэн-иһэн арай биир дьиэгэ тийдэ.</ta>
            <ta e="T255" id="Seg_2071" s="T247">Бу дьиэгэ кэлэн иһиллээн көрбүтэ: һаһыл кыыран эрэр.</ta>
            <ta e="T264" id="Seg_2072" s="T255">Дьиэ иһигэр киирбитэ һаһыл бүтүннүү мунньустан бараан кыыра һыталлар.</ta>
            <ta e="T265" id="Seg_2073" s="T264">Ойуннаактар.</ta>
            <ta e="T268" id="Seg_2074" s="T265">Огонньор киирэн олордо.</ta>
            <ta e="T274" id="Seg_2075" s="T268">Бу олордогуна һаһылыҥ төнүннэ кыыран бараат.</ta>
            <ta e="T279" id="Seg_2076" s="T274">Бу огонньоруҥ арай дүҥүр огуста.</ta>
            <ta e="T284" id="Seg_2077" s="T279">Бу дүҥүрүн оксо-оксо дьэ эккирээтэ.</ta>
            <ta e="T291" id="Seg_2078" s="T284">Дьэ маныга күлэ дьэ бургаҥнаата баа һыалыйатыттан.</ta>
            <ta e="T298" id="Seg_2079" s="T291">Дьэ мантан бу һаһылларыҥ бүтүннүү дьэ күлүстүлэр.</ta>
            <ta e="T304" id="Seg_2080" s="T298">Арай онно улагаага һаһыл күлэрэ иһилиннэ.</ta>
            <ta e="T319" id="Seg_2081" s="T304">Огонньор иһиттэ, дүҥүрүн быраан кээстэ, огонньор маа һаһылы кабан ылла, һаһылын уокка ыһаара-ыһаара дьэ таһыйда.</ta>
            <ta e="T322" id="Seg_2082" s="T319">Таһыйан-таһыйан өлөрөн кээстэ.</ta>
            <ta e="T329" id="Seg_2083" s="T322">Дьэ ол һаһылыҥ маннай киниэнин оголорун һиэччи.</ta>
            <ta e="T336" id="Seg_2084" s="T329">Һол һаһылыҥ биһиккэ кэлгийэн баран түҥнэри кээспитэ.</ta>
            <ta e="T345" id="Seg_2085" s="T336">Инньэ гынан баран дьэ ити огонньорун дьиэтигэр баран олорбута.</ta>
            <ta e="T348" id="Seg_2086" s="T345">Дьэ ити уһуга.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_2087" s="T0">Dʼe biːr ogonnʼor olorbuta. </ta>
            <ta e="T6" id="Seg_2088" s="T4">Ukukuːt Čukukuːt aːta. </ta>
            <ta e="T10" id="Seg_2089" s="T6">Mantɨŋ ügüs bagajɨ ogoto. </ta>
            <ta e="T15" id="Seg_2090" s="T10">Dʼe bu manɨga hahɨl kelbite: </ta>
            <ta e="T21" id="Seg_2091" s="T15">"Ogonnʼor, biːr ogogun egel", diːr, "hi͡ekpin." </ta>
            <ta e="T27" id="Seg_2092" s="T21">Manɨga bu ogonnʼor ogotun bi͡erbite, hi͡etin. </ta>
            <ta e="T36" id="Seg_2093" s="T27">Dʼe ol barbɨta, kas eme konon baran emi͡e kelbite. </ta>
            <ta e="T42" id="Seg_2094" s="T36">"Ogonnʼor, emi͡e ogoto egel", diːr, "hi͡ekpin." </ta>
            <ta e="T47" id="Seg_2095" s="T42">Onuga ogonnʼor ogotun emi͡e bi͡erbite. </ta>
            <ta e="T52" id="Seg_2096" s="T47">Dʼe ol ogotun ilpite hi͡eri. </ta>
            <ta e="T60" id="Seg_2097" s="T52">Onton kas eme kun bu͡olan baran emi͡e kelbite. </ta>
            <ta e="T64" id="Seg_2098" s="T60">Ol kelen emi͡e diːr: </ta>
            <ta e="T68" id="Seg_2099" s="T64">"Ogoto egel", diːr, "hi͡ekpin." </ta>
            <ta e="T71" id="Seg_2100" s="T68">Ogoto elete bu͡olla. </ta>
            <ta e="T78" id="Seg_2101" s="T71">Dʼe ol barbɨta daː hahɨlɨŋ dʼɨlɨjbɨta bu. </ta>
            <ta e="T84" id="Seg_2102" s="T78">Ogonnʼoruŋ oloro hataːn baran kaːman kaːlla. </ta>
            <ta e="T96" id="Seg_2103" s="T84">Dʼe bu kaːman ihen-ihen baran biːr kɨːl baːjtahɨnɨn ölörön baran ü͡ölüne olordo. </ta>
            <ta e="T100" id="Seg_2104" s="T96">Bu olordoguna hahɨl kelle: </ta>
            <ta e="T107" id="Seg_2105" s="T100">"Čej ogonnʼor, bihigi ogo bu͡ola oːnnʼu͡ok", di͡ete. </ta>
            <ta e="T112" id="Seg_2106" s="T107">"Če, daːrɨm, ogo [bu͡ola] oːnnʼu͡ok." </ta>
            <ta e="T119" id="Seg_2107" s="T112">Bu ogonnʼoruŋ bihik oŋordo ol bajtahɨnnarɨn tiriːtinen. </ta>
            <ta e="T122" id="Seg_2108" s="T119">Bihik bɨ͡ata oŋorunnular. </ta>
            <ta e="T124" id="Seg_2109" s="T122">Ogonnʼoruŋ di͡ebit: </ta>
            <ta e="T131" id="Seg_2110" s="T124">"Če, doː, en bigen ogo bu͡olan", di͡ete. </ta>
            <ta e="T135" id="Seg_2111" s="T131">Hahɨl manɨga hɨtan kördö: </ta>
            <ta e="T143" id="Seg_2112" s="T135">"Eː, dogor, mi͡ene kuturugum ɨ͡aldʼar, hɨtɨ͡arɨ͡a hu͡ok", di͡ete. </ta>
            <ta e="T153" id="Seg_2113" s="T143">"Če kihi kihiginen en hɨtan ogo bu͡ol", di͡ete hahɨl ogonnʼoru. </ta>
            <ta e="T156" id="Seg_2114" s="T153">Ogonnʼor dʼe hɨtta. </ta>
            <ta e="T161" id="Seg_2115" s="T156">Hahɨl dʼe manɨ kelgijen keːste. </ta>
            <ta e="T169" id="Seg_2116" s="T161">Hahɨl manɨ bigiː-bigiː kɨːl etinen ɨstaːn ahata olordo. </ta>
            <ta e="T174" id="Seg_2117" s="T169">Bu ogonnʼoruŋ araj utujan kaːlla. </ta>
            <ta e="T181" id="Seg_2118" s="T174">Hahɨl manɨ hagɨstaːn ildʼen bajaraːktan tüŋneri bɨrakta. </ta>
            <ta e="T188" id="Seg_2119" s="T181">Kojut ol ogonnʼor uhuktubuta bajaraːk ihiger hɨtar. </ta>
            <ta e="T196" id="Seg_2120" s="T188">Ogonnʼor kaja da di͡eg kamnɨ͡agar bert, kelgillibit kihi. </ta>
            <ta e="T198" id="Seg_2121" s="T196">Ogonnʼor ɨŋɨrar: </ta>
            <ta e="T205" id="Seg_2122" s="T198">"Gornu͡oktar, kɨrsalar, kutujaktar, bɨ͡abɨn bɨha kerbeːŋ", diːr. </ta>
            <ta e="T213" id="Seg_2123" s="T205">Oloruŋ kelenner bɨ͡atɨn bɨha kerbeːbittere, ogonnʼor tura ekkireːte. </ta>
            <ta e="T224" id="Seg_2124" s="T213">Innʼe gɨnan bu bɨ͡atɨn kerbeːččilerge naju͡omun biːr kɨːl baːjtahɨnɨn ölörön bi͡erde. </ta>
            <ta e="T229" id="Seg_2125" s="T224">Baː ogonnʼoruŋ otuːtugar kaːman kelle. </ta>
            <ta e="T237" id="Seg_2126" s="T229">Otuːtun külün bütünnüː hɨ͡aldʼatɨgar kaːlaːn baran kaːman kaːlla. </ta>
            <ta e="T247" id="Seg_2127" s="T237">Dʼe bu baran iste, baran ihen-ihen araj biːr dʼi͡ege tijde. </ta>
            <ta e="T255" id="Seg_2128" s="T247">Bu dʼi͡ege kelen ihilleːn körbüte, hahɨl kɨːran erer. </ta>
            <ta e="T264" id="Seg_2129" s="T255">Dʼi͡e ihiger kiːrbite hahɨl bütünnüː munnʼustan baraːn kɨːra hɨtallar. </ta>
            <ta e="T265" id="Seg_2130" s="T264">Ojunnaːktar. </ta>
            <ta e="T268" id="Seg_2131" s="T265">Ogonnʼor kiːren olordo. </ta>
            <ta e="T274" id="Seg_2132" s="T268">Bu olordoguna hahɨlɨŋ tönünne kɨːran baraːt. </ta>
            <ta e="T279" id="Seg_2133" s="T274">Bu ogonnʼoruŋ araj düŋür ogusta. </ta>
            <ta e="T284" id="Seg_2134" s="T279">Bu düŋürün okso-okso dʼe ekkireːte. </ta>
            <ta e="T291" id="Seg_2135" s="T284">Dʼe manɨga küle dʼe burgaŋnaːta baː hɨ͡alɨjatɨttan. </ta>
            <ta e="T298" id="Seg_2136" s="T291">Dʼe mantan bu hahɨllarɨŋ bütünnüː dʼe külüstüler. </ta>
            <ta e="T304" id="Seg_2137" s="T298">Araj onno ulagaːga hahɨl külere ihilinne. </ta>
            <ta e="T319" id="Seg_2138" s="T304">Ogonnʼor ihitte, düŋürün bɨraːn keːste, ogonnʼor maː hahɨlɨ kaban ɨlla, hahɨlɨn u͡okka ɨhaːra-ɨhaːra dʼe tahɨjda. </ta>
            <ta e="T322" id="Seg_2139" s="T319">Tahɨjan-tahɨjan ölörön keːste. </ta>
            <ta e="T329" id="Seg_2140" s="T322">Dʼe ol hahɨlɨŋ mannaj kini͡enin ogolorun hi͡ečči. </ta>
            <ta e="T336" id="Seg_2141" s="T329">Hol hahɨlɨŋ bihikke kelgijen baran tüŋneri keːspite. </ta>
            <ta e="T345" id="Seg_2142" s="T336">Innʼe gɨnan baran dʼe iti ogonnʼorun dʼi͡etiger baran olorbuta. </ta>
            <ta e="T348" id="Seg_2143" s="T345">Dʼe iti uhuga. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_2144" s="T0">dʼe</ta>
            <ta e="T2" id="Seg_2145" s="T1">biːr</ta>
            <ta e="T3" id="Seg_2146" s="T2">ogonnʼor</ta>
            <ta e="T4" id="Seg_2147" s="T3">olor-but-a</ta>
            <ta e="T5" id="Seg_2148" s="T4">Ukukuːt Čukukuːt</ta>
            <ta e="T6" id="Seg_2149" s="T5">aːt-a</ta>
            <ta e="T7" id="Seg_2150" s="T6">man-tɨ-ŋ</ta>
            <ta e="T8" id="Seg_2151" s="T7">ügüs</ta>
            <ta e="T9" id="Seg_2152" s="T8">bagajɨ</ta>
            <ta e="T10" id="Seg_2153" s="T9">ogo-to</ta>
            <ta e="T11" id="Seg_2154" s="T10">dʼe</ta>
            <ta e="T12" id="Seg_2155" s="T11">bu</ta>
            <ta e="T13" id="Seg_2156" s="T12">manɨ-ga</ta>
            <ta e="T14" id="Seg_2157" s="T13">hahɨl</ta>
            <ta e="T15" id="Seg_2158" s="T14">kel-bit-e</ta>
            <ta e="T16" id="Seg_2159" s="T15">ogonnʼor</ta>
            <ta e="T17" id="Seg_2160" s="T16">biːr</ta>
            <ta e="T18" id="Seg_2161" s="T17">ogo-gu-n</ta>
            <ta e="T19" id="Seg_2162" s="T18">egel</ta>
            <ta e="T20" id="Seg_2163" s="T19">diː-r</ta>
            <ta e="T21" id="Seg_2164" s="T20">h-i͡ek-pi-n</ta>
            <ta e="T22" id="Seg_2165" s="T21">manɨ-ga</ta>
            <ta e="T23" id="Seg_2166" s="T22">bu</ta>
            <ta e="T24" id="Seg_2167" s="T23">ogonnʼor</ta>
            <ta e="T25" id="Seg_2168" s="T24">ogo-tu-n</ta>
            <ta e="T26" id="Seg_2169" s="T25">bi͡er-bit-e</ta>
            <ta e="T27" id="Seg_2170" s="T26">hi͡e-tin</ta>
            <ta e="T28" id="Seg_2171" s="T27">dʼe</ta>
            <ta e="T29" id="Seg_2172" s="T28">ol</ta>
            <ta e="T30" id="Seg_2173" s="T29">bar-bɨt-a</ta>
            <ta e="T31" id="Seg_2174" s="T30">kas</ta>
            <ta e="T32" id="Seg_2175" s="T31">eme</ta>
            <ta e="T33" id="Seg_2176" s="T32">kon-on</ta>
            <ta e="T34" id="Seg_2177" s="T33">baran</ta>
            <ta e="T35" id="Seg_2178" s="T34">emi͡e</ta>
            <ta e="T36" id="Seg_2179" s="T35">kel-bit-e</ta>
            <ta e="T37" id="Seg_2180" s="T36">ogonnʼor</ta>
            <ta e="T38" id="Seg_2181" s="T37">emi͡e</ta>
            <ta e="T39" id="Seg_2182" s="T38">ogo-to</ta>
            <ta e="T40" id="Seg_2183" s="T39">egel</ta>
            <ta e="T41" id="Seg_2184" s="T40">diː-r</ta>
            <ta e="T42" id="Seg_2185" s="T41">h-i͡ek-pi-n</ta>
            <ta e="T43" id="Seg_2186" s="T42">onu-ga</ta>
            <ta e="T44" id="Seg_2187" s="T43">ogonnʼor</ta>
            <ta e="T45" id="Seg_2188" s="T44">ogo-tu-n</ta>
            <ta e="T46" id="Seg_2189" s="T45">emi͡e</ta>
            <ta e="T47" id="Seg_2190" s="T46">bi͡er-bit-e</ta>
            <ta e="T48" id="Seg_2191" s="T47">dʼe</ta>
            <ta e="T49" id="Seg_2192" s="T48">ol</ta>
            <ta e="T50" id="Seg_2193" s="T49">ogo-tu-n</ta>
            <ta e="T51" id="Seg_2194" s="T50">il-pit-e</ta>
            <ta e="T52" id="Seg_2195" s="T51">hi͡e-ri</ta>
            <ta e="T53" id="Seg_2196" s="T52">onton</ta>
            <ta e="T54" id="Seg_2197" s="T53">kas</ta>
            <ta e="T55" id="Seg_2198" s="T54">eme</ta>
            <ta e="T56" id="Seg_2199" s="T55">kun</ta>
            <ta e="T57" id="Seg_2200" s="T56">bu͡ol-an</ta>
            <ta e="T58" id="Seg_2201" s="T57">baran</ta>
            <ta e="T59" id="Seg_2202" s="T58">emi͡e</ta>
            <ta e="T60" id="Seg_2203" s="T59">kel-bit-e</ta>
            <ta e="T61" id="Seg_2204" s="T60">ol</ta>
            <ta e="T62" id="Seg_2205" s="T61">kel-en</ta>
            <ta e="T63" id="Seg_2206" s="T62">emi͡e</ta>
            <ta e="T64" id="Seg_2207" s="T63">diː-r</ta>
            <ta e="T65" id="Seg_2208" s="T64">ogo-to</ta>
            <ta e="T66" id="Seg_2209" s="T65">egel</ta>
            <ta e="T67" id="Seg_2210" s="T66">diː-r</ta>
            <ta e="T68" id="Seg_2211" s="T67">h-i͡ek-pi-n</ta>
            <ta e="T69" id="Seg_2212" s="T68">ogo-to</ta>
            <ta e="T70" id="Seg_2213" s="T69">ele-te</ta>
            <ta e="T71" id="Seg_2214" s="T70">bu͡ol-l-a</ta>
            <ta e="T72" id="Seg_2215" s="T71">dʼe</ta>
            <ta e="T73" id="Seg_2216" s="T72">ol</ta>
            <ta e="T74" id="Seg_2217" s="T73">bar-bɨt-a</ta>
            <ta e="T75" id="Seg_2218" s="T74">daː</ta>
            <ta e="T76" id="Seg_2219" s="T75">hahɨl-ɨ-ŋ</ta>
            <ta e="T77" id="Seg_2220" s="T76">dʼɨlɨj-bɨt-a</ta>
            <ta e="T78" id="Seg_2221" s="T77">bu</ta>
            <ta e="T79" id="Seg_2222" s="T78">ogonnʼor-u-ŋ</ta>
            <ta e="T80" id="Seg_2223" s="T79">olor-o</ta>
            <ta e="T81" id="Seg_2224" s="T80">hataː-n</ta>
            <ta e="T82" id="Seg_2225" s="T81">bar-an</ta>
            <ta e="T83" id="Seg_2226" s="T82">kaːm-an</ta>
            <ta e="T84" id="Seg_2227" s="T83">kaːl-l-a</ta>
            <ta e="T85" id="Seg_2228" s="T84">dʼe</ta>
            <ta e="T86" id="Seg_2229" s="T85">bu</ta>
            <ta e="T87" id="Seg_2230" s="T86">kaːm-an</ta>
            <ta e="T88" id="Seg_2231" s="T87">ih-en-ih-en</ta>
            <ta e="T89" id="Seg_2232" s="T88">bar-an</ta>
            <ta e="T90" id="Seg_2233" s="T89">biːr</ta>
            <ta e="T91" id="Seg_2234" s="T90">kɨːl</ta>
            <ta e="T92" id="Seg_2235" s="T91">baːjtahɨn-ɨ-n</ta>
            <ta e="T93" id="Seg_2236" s="T92">ölör-ön</ta>
            <ta e="T94" id="Seg_2237" s="T93">bar-an</ta>
            <ta e="T95" id="Seg_2238" s="T94">ü͡öl-ü-n-e</ta>
            <ta e="T96" id="Seg_2239" s="T95">olor-d-o</ta>
            <ta e="T97" id="Seg_2240" s="T96">bu</ta>
            <ta e="T98" id="Seg_2241" s="T97">olor-dog-una</ta>
            <ta e="T99" id="Seg_2242" s="T98">hahɨl</ta>
            <ta e="T100" id="Seg_2243" s="T99">kel-l-e</ta>
            <ta e="T101" id="Seg_2244" s="T100">čej</ta>
            <ta e="T102" id="Seg_2245" s="T101">ogonnʼor</ta>
            <ta e="T103" id="Seg_2246" s="T102">bihigi</ta>
            <ta e="T104" id="Seg_2247" s="T103">ogo</ta>
            <ta e="T105" id="Seg_2248" s="T104">bu͡ol-a</ta>
            <ta e="T106" id="Seg_2249" s="T105">oːnnʼ-u͡ok</ta>
            <ta e="T107" id="Seg_2250" s="T106">di͡e-t-e</ta>
            <ta e="T108" id="Seg_2251" s="T107">če</ta>
            <ta e="T109" id="Seg_2252" s="T108">daːrɨm</ta>
            <ta e="T110" id="Seg_2253" s="T109">ogo</ta>
            <ta e="T111" id="Seg_2254" s="T110">bu͡ol-a</ta>
            <ta e="T112" id="Seg_2255" s="T111">oːnnʼ-u͡ok</ta>
            <ta e="T113" id="Seg_2256" s="T112">bu</ta>
            <ta e="T114" id="Seg_2257" s="T113">ogonnʼor-u-ŋ</ta>
            <ta e="T115" id="Seg_2258" s="T114">bihik</ta>
            <ta e="T116" id="Seg_2259" s="T115">oŋor-d-o</ta>
            <ta e="T117" id="Seg_2260" s="T116">ol</ta>
            <ta e="T118" id="Seg_2261" s="T117">bajtahɨn-nar-ɨ-n</ta>
            <ta e="T119" id="Seg_2262" s="T118">tiriː-ti-nen</ta>
            <ta e="T120" id="Seg_2263" s="T119">bihik</ta>
            <ta e="T121" id="Seg_2264" s="T120">bɨ͡a-ta</ta>
            <ta e="T122" id="Seg_2265" s="T121">oŋor-u-n-nu-lar</ta>
            <ta e="T123" id="Seg_2266" s="T122">ogonnʼor-u-ŋ</ta>
            <ta e="T124" id="Seg_2267" s="T123">di͡e-bit</ta>
            <ta e="T125" id="Seg_2268" s="T124">če</ta>
            <ta e="T126" id="Seg_2269" s="T125">doː</ta>
            <ta e="T127" id="Seg_2270" s="T126">en</ta>
            <ta e="T128" id="Seg_2271" s="T127">big-e-n</ta>
            <ta e="T129" id="Seg_2272" s="T128">ogo</ta>
            <ta e="T130" id="Seg_2273" s="T129">bu͡ol-an</ta>
            <ta e="T131" id="Seg_2274" s="T130">di͡e-t-e</ta>
            <ta e="T132" id="Seg_2275" s="T131">hahɨl</ta>
            <ta e="T133" id="Seg_2276" s="T132">manɨ-ga</ta>
            <ta e="T134" id="Seg_2277" s="T133">hɨt-an</ta>
            <ta e="T135" id="Seg_2278" s="T134">kör-d-ö</ta>
            <ta e="T136" id="Seg_2279" s="T135">eː</ta>
            <ta e="T137" id="Seg_2280" s="T136">dogor</ta>
            <ta e="T138" id="Seg_2281" s="T137">mi͡ene</ta>
            <ta e="T139" id="Seg_2282" s="T138">kuturug-u-m</ta>
            <ta e="T140" id="Seg_2283" s="T139">ɨ͡aldʼ-ar</ta>
            <ta e="T141" id="Seg_2284" s="T140">hɨt-ɨ͡ar-ɨ͡a</ta>
            <ta e="T142" id="Seg_2285" s="T141">hu͡ok</ta>
            <ta e="T143" id="Seg_2286" s="T142">di͡e-t-e</ta>
            <ta e="T144" id="Seg_2287" s="T143">če</ta>
            <ta e="T145" id="Seg_2288" s="T144">kihi</ta>
            <ta e="T146" id="Seg_2289" s="T145">kihi-gi-nen</ta>
            <ta e="T147" id="Seg_2290" s="T146">en</ta>
            <ta e="T148" id="Seg_2291" s="T147">hɨt-an</ta>
            <ta e="T149" id="Seg_2292" s="T148">ogo</ta>
            <ta e="T150" id="Seg_2293" s="T149">bu͡ol</ta>
            <ta e="T151" id="Seg_2294" s="T150">di͡e-t-e</ta>
            <ta e="T152" id="Seg_2295" s="T151">hahɨl</ta>
            <ta e="T153" id="Seg_2296" s="T152">ogonnʼor-u</ta>
            <ta e="T154" id="Seg_2297" s="T153">ogonnʼor</ta>
            <ta e="T155" id="Seg_2298" s="T154">dʼe</ta>
            <ta e="T156" id="Seg_2299" s="T155">hɨt-t-a</ta>
            <ta e="T157" id="Seg_2300" s="T156">hahɨl</ta>
            <ta e="T158" id="Seg_2301" s="T157">dʼe</ta>
            <ta e="T159" id="Seg_2302" s="T158">ma-nɨ</ta>
            <ta e="T160" id="Seg_2303" s="T159">kelgij-en</ta>
            <ta e="T161" id="Seg_2304" s="T160">keːs-t-e</ta>
            <ta e="T162" id="Seg_2305" s="T161">hahɨl</ta>
            <ta e="T163" id="Seg_2306" s="T162">ma-nɨ</ta>
            <ta e="T164" id="Seg_2307" s="T163">big-iː-big-iː</ta>
            <ta e="T165" id="Seg_2308" s="T164">kɨːl</ta>
            <ta e="T166" id="Seg_2309" s="T165">et-i-nen</ta>
            <ta e="T167" id="Seg_2310" s="T166">ɨstaː-n</ta>
            <ta e="T168" id="Seg_2311" s="T167">ah-a-t-a</ta>
            <ta e="T169" id="Seg_2312" s="T168">olor-d-o</ta>
            <ta e="T170" id="Seg_2313" s="T169">bu</ta>
            <ta e="T171" id="Seg_2314" s="T170">ogonnʼor-u-ŋ</ta>
            <ta e="T172" id="Seg_2315" s="T171">araj</ta>
            <ta e="T173" id="Seg_2316" s="T172">utuj-an</ta>
            <ta e="T174" id="Seg_2317" s="T173">kaːl-l-a</ta>
            <ta e="T175" id="Seg_2318" s="T174">hahɨl</ta>
            <ta e="T176" id="Seg_2319" s="T175">ma-nɨ</ta>
            <ta e="T177" id="Seg_2320" s="T176">hag-ɨ-s-taː-n</ta>
            <ta e="T178" id="Seg_2321" s="T177">ildʼ-en</ta>
            <ta e="T179" id="Seg_2322" s="T178">bajaraːk-tan</ta>
            <ta e="T180" id="Seg_2323" s="T179">tüŋner-i</ta>
            <ta e="T181" id="Seg_2324" s="T180">bɨrak-t-a</ta>
            <ta e="T182" id="Seg_2325" s="T181">kojut</ta>
            <ta e="T183" id="Seg_2326" s="T182">ol</ta>
            <ta e="T184" id="Seg_2327" s="T183">ogonnʼor</ta>
            <ta e="T185" id="Seg_2328" s="T184">uhukt-u-but-a</ta>
            <ta e="T186" id="Seg_2329" s="T185">bajaraːk</ta>
            <ta e="T187" id="Seg_2330" s="T186">ih-i-ger</ta>
            <ta e="T188" id="Seg_2331" s="T187">hɨt-ar</ta>
            <ta e="T189" id="Seg_2332" s="T188">ogonnʼor</ta>
            <ta e="T190" id="Seg_2333" s="T189">kaja</ta>
            <ta e="T191" id="Seg_2334" s="T190">da</ta>
            <ta e="T192" id="Seg_2335" s="T191">di͡eg</ta>
            <ta e="T193" id="Seg_2336" s="T192">kamn-ɨ͡ag-a-r</ta>
            <ta e="T194" id="Seg_2337" s="T193">bert</ta>
            <ta e="T195" id="Seg_2338" s="T194">kelgi-ll-i-bit</ta>
            <ta e="T196" id="Seg_2339" s="T195">kihi</ta>
            <ta e="T197" id="Seg_2340" s="T196">ogonnʼor</ta>
            <ta e="T198" id="Seg_2341" s="T197">ɨŋɨr-ar</ta>
            <ta e="T199" id="Seg_2342" s="T198">gornu͡ok-tar</ta>
            <ta e="T200" id="Seg_2343" s="T199">kɨrsa-lar</ta>
            <ta e="T201" id="Seg_2344" s="T200">kutujak-tar</ta>
            <ta e="T202" id="Seg_2345" s="T201">bɨ͡a-bɨ-n</ta>
            <ta e="T203" id="Seg_2346" s="T202">bɨh-a</ta>
            <ta e="T204" id="Seg_2347" s="T203">kerbeː-ŋ</ta>
            <ta e="T205" id="Seg_2348" s="T204">diː-r</ta>
            <ta e="T206" id="Seg_2349" s="T205">o-lor-u-ŋ</ta>
            <ta e="T207" id="Seg_2350" s="T206">kel-en-ner</ta>
            <ta e="T208" id="Seg_2351" s="T207">bɨ͡a-tɨ-n</ta>
            <ta e="T209" id="Seg_2352" s="T208">bɨh-a</ta>
            <ta e="T210" id="Seg_2353" s="T209">kerbeː-bit-tere</ta>
            <ta e="T211" id="Seg_2354" s="T210">ogonnʼor</ta>
            <ta e="T212" id="Seg_2355" s="T211">tur-a</ta>
            <ta e="T213" id="Seg_2356" s="T212">ekkireː-t-e</ta>
            <ta e="T214" id="Seg_2357" s="T213">innʼe</ta>
            <ta e="T215" id="Seg_2358" s="T214">gɨn-an</ta>
            <ta e="T216" id="Seg_2359" s="T215">bu</ta>
            <ta e="T217" id="Seg_2360" s="T216">bɨ͡a-tɨ-n</ta>
            <ta e="T218" id="Seg_2361" s="T217">kerbeː-čči-ler-ge</ta>
            <ta e="T219" id="Seg_2362" s="T218">naju͡om-u-n</ta>
            <ta e="T220" id="Seg_2363" s="T219">biːr</ta>
            <ta e="T221" id="Seg_2364" s="T220">kɨːl</ta>
            <ta e="T222" id="Seg_2365" s="T221">baːjtahɨn-ɨ-n</ta>
            <ta e="T223" id="Seg_2366" s="T222">ölör-ön</ta>
            <ta e="T224" id="Seg_2367" s="T223">bi͡er-d-e</ta>
            <ta e="T225" id="Seg_2368" s="T224">baː</ta>
            <ta e="T226" id="Seg_2369" s="T225">ogonnʼor-u-ŋ</ta>
            <ta e="T227" id="Seg_2370" s="T226">otuː-tu-gar</ta>
            <ta e="T228" id="Seg_2371" s="T227">kaːm-an</ta>
            <ta e="T229" id="Seg_2372" s="T228">kel-l-e</ta>
            <ta e="T230" id="Seg_2373" s="T229">otuː-tu-n</ta>
            <ta e="T231" id="Seg_2374" s="T230">kül-ü-n</ta>
            <ta e="T232" id="Seg_2375" s="T231">bütün-nüː</ta>
            <ta e="T233" id="Seg_2376" s="T232">hɨ͡aldʼa-tɨ-gar</ta>
            <ta e="T234" id="Seg_2377" s="T233">kaːlaː-n</ta>
            <ta e="T235" id="Seg_2378" s="T234">bar-an</ta>
            <ta e="T236" id="Seg_2379" s="T235">kaːm-an</ta>
            <ta e="T237" id="Seg_2380" s="T236">kaːl-l-a</ta>
            <ta e="T238" id="Seg_2381" s="T237">dʼe</ta>
            <ta e="T239" id="Seg_2382" s="T238">bu</ta>
            <ta e="T240" id="Seg_2383" s="T239">bar-an</ta>
            <ta e="T241" id="Seg_2384" s="T240">is-t-e</ta>
            <ta e="T242" id="Seg_2385" s="T241">bar-an</ta>
            <ta e="T243" id="Seg_2386" s="T242">ih-en-ih-en</ta>
            <ta e="T244" id="Seg_2387" s="T243">araj</ta>
            <ta e="T245" id="Seg_2388" s="T244">biːr</ta>
            <ta e="T246" id="Seg_2389" s="T245">dʼi͡e-ge</ta>
            <ta e="T247" id="Seg_2390" s="T246">tij-d-e</ta>
            <ta e="T248" id="Seg_2391" s="T247">bu</ta>
            <ta e="T249" id="Seg_2392" s="T248">dʼi͡e-ge</ta>
            <ta e="T250" id="Seg_2393" s="T249">kel-en</ta>
            <ta e="T251" id="Seg_2394" s="T250">ihilleː-n</ta>
            <ta e="T252" id="Seg_2395" s="T251">kör-büt-e</ta>
            <ta e="T253" id="Seg_2396" s="T252">hahɨl</ta>
            <ta e="T254" id="Seg_2397" s="T253">kɨːr-an</ta>
            <ta e="T255" id="Seg_2398" s="T254">er-er</ta>
            <ta e="T256" id="Seg_2399" s="T255">dʼi͡e</ta>
            <ta e="T257" id="Seg_2400" s="T256">ih-i-ger</ta>
            <ta e="T258" id="Seg_2401" s="T257">kiːr-bit-e</ta>
            <ta e="T259" id="Seg_2402" s="T258">hahɨl</ta>
            <ta e="T260" id="Seg_2403" s="T259">bütün-nüː</ta>
            <ta e="T261" id="Seg_2404" s="T260">munnʼus-t-an</ta>
            <ta e="T262" id="Seg_2405" s="T261">baraːn</ta>
            <ta e="T263" id="Seg_2406" s="T262">kɨːr-a</ta>
            <ta e="T264" id="Seg_2407" s="T263">hɨt-al-lar</ta>
            <ta e="T265" id="Seg_2408" s="T264">ojun-naːk-tar</ta>
            <ta e="T266" id="Seg_2409" s="T265">ogonnʼor</ta>
            <ta e="T267" id="Seg_2410" s="T266">kiːr-en</ta>
            <ta e="T268" id="Seg_2411" s="T267">olor-d-o</ta>
            <ta e="T269" id="Seg_2412" s="T268">bu</ta>
            <ta e="T270" id="Seg_2413" s="T269">olor-dog-una</ta>
            <ta e="T271" id="Seg_2414" s="T270">hahɨl-ɨ-ŋ</ta>
            <ta e="T272" id="Seg_2415" s="T271">tönün-n-e</ta>
            <ta e="T273" id="Seg_2416" s="T272">kɨːr-an</ta>
            <ta e="T274" id="Seg_2417" s="T273">bar-aːt</ta>
            <ta e="T275" id="Seg_2418" s="T274">bu</ta>
            <ta e="T276" id="Seg_2419" s="T275">ogonnʼor-u-ŋ</ta>
            <ta e="T277" id="Seg_2420" s="T276">araj</ta>
            <ta e="T278" id="Seg_2421" s="T277">düŋür</ta>
            <ta e="T279" id="Seg_2422" s="T278">ogus-t-a</ta>
            <ta e="T280" id="Seg_2423" s="T279">bu</ta>
            <ta e="T281" id="Seg_2424" s="T280">düŋür-ü-n</ta>
            <ta e="T282" id="Seg_2425" s="T281">oks-o-oks-o</ta>
            <ta e="T283" id="Seg_2426" s="T282">dʼe</ta>
            <ta e="T284" id="Seg_2427" s="T283">ekkireː-t-e</ta>
            <ta e="T285" id="Seg_2428" s="T284">dʼe</ta>
            <ta e="T286" id="Seg_2429" s="T285">manɨ-ga</ta>
            <ta e="T287" id="Seg_2430" s="T286">kül-e</ta>
            <ta e="T288" id="Seg_2431" s="T287">dʼe</ta>
            <ta e="T289" id="Seg_2432" s="T288">burga-ŋnaː-t-a</ta>
            <ta e="T290" id="Seg_2433" s="T289">baː</ta>
            <ta e="T291" id="Seg_2434" s="T290">hɨ͡alɨja-tɨ-ttan</ta>
            <ta e="T292" id="Seg_2435" s="T291">dʼe</ta>
            <ta e="T293" id="Seg_2436" s="T292">man-tan</ta>
            <ta e="T294" id="Seg_2437" s="T293">bu</ta>
            <ta e="T295" id="Seg_2438" s="T294">hahɨl-lar-ɨ-ŋ</ta>
            <ta e="T296" id="Seg_2439" s="T295">bütün-nüː</ta>
            <ta e="T297" id="Seg_2440" s="T296">dʼe</ta>
            <ta e="T298" id="Seg_2441" s="T297">kül-ü-s-tü-ler</ta>
            <ta e="T299" id="Seg_2442" s="T298">araj</ta>
            <ta e="T300" id="Seg_2443" s="T299">onno</ta>
            <ta e="T301" id="Seg_2444" s="T300">ulagaː-ga</ta>
            <ta e="T302" id="Seg_2445" s="T301">hahɨl</ta>
            <ta e="T303" id="Seg_2446" s="T302">kül-er-e</ta>
            <ta e="T304" id="Seg_2447" s="T303">ihilin-n-e</ta>
            <ta e="T305" id="Seg_2448" s="T304">ogonnʼor</ta>
            <ta e="T306" id="Seg_2449" s="T305">ihit-t-e</ta>
            <ta e="T307" id="Seg_2450" s="T306">düŋür-ü-n</ta>
            <ta e="T308" id="Seg_2451" s="T307">bɨraː-n</ta>
            <ta e="T309" id="Seg_2452" s="T308">keːs-t-e</ta>
            <ta e="T310" id="Seg_2453" s="T309">ogonnʼor</ta>
            <ta e="T311" id="Seg_2454" s="T310">maː</ta>
            <ta e="T312" id="Seg_2455" s="T311">hahɨl-ɨ</ta>
            <ta e="T313" id="Seg_2456" s="T312">kab-an</ta>
            <ta e="T314" id="Seg_2457" s="T313">ɨl-l-a</ta>
            <ta e="T315" id="Seg_2458" s="T314">hahɨl-ɨ-n</ta>
            <ta e="T316" id="Seg_2459" s="T315">u͡ok-ka</ta>
            <ta e="T317" id="Seg_2460" s="T316">ɨhaːr-a-ɨhaːr-A</ta>
            <ta e="T318" id="Seg_2461" s="T317">dʼe</ta>
            <ta e="T319" id="Seg_2462" s="T318">tahɨj-d-a</ta>
            <ta e="T320" id="Seg_2463" s="T319">tahɨj-an-tahɨj-an</ta>
            <ta e="T321" id="Seg_2464" s="T320">ölör-ön</ta>
            <ta e="T322" id="Seg_2465" s="T321">keːs-t-e</ta>
            <ta e="T323" id="Seg_2466" s="T322">dʼe</ta>
            <ta e="T324" id="Seg_2467" s="T323">ol</ta>
            <ta e="T325" id="Seg_2468" s="T324">hahɨl-ɨ-ŋ</ta>
            <ta e="T326" id="Seg_2469" s="T325">mannaj</ta>
            <ta e="T327" id="Seg_2470" s="T326">kini͡eni-n</ta>
            <ta e="T328" id="Seg_2471" s="T327">ogo-lor-u-n</ta>
            <ta e="T329" id="Seg_2472" s="T328">hi͡e-čči</ta>
            <ta e="T330" id="Seg_2473" s="T329">hol</ta>
            <ta e="T331" id="Seg_2474" s="T330">hahɨl-ɨ-ŋ</ta>
            <ta e="T332" id="Seg_2475" s="T331">bihik-ke</ta>
            <ta e="T333" id="Seg_2476" s="T332">kelgij-en</ta>
            <ta e="T334" id="Seg_2477" s="T333">bar-an</ta>
            <ta e="T335" id="Seg_2478" s="T334">tüŋner-i</ta>
            <ta e="T336" id="Seg_2479" s="T335">keːs-pit-e</ta>
            <ta e="T337" id="Seg_2480" s="T336">innʼe</ta>
            <ta e="T338" id="Seg_2481" s="T337">gɨn-an</ta>
            <ta e="T339" id="Seg_2482" s="T338">baran</ta>
            <ta e="T340" id="Seg_2483" s="T339">dʼe</ta>
            <ta e="T341" id="Seg_2484" s="T340">iti</ta>
            <ta e="T342" id="Seg_2485" s="T341">ogonnʼor-u-n</ta>
            <ta e="T343" id="Seg_2486" s="T342">dʼi͡e-ti-ger</ta>
            <ta e="T344" id="Seg_2487" s="T343">bar-an</ta>
            <ta e="T345" id="Seg_2488" s="T344">olor-but-a</ta>
            <ta e="T346" id="Seg_2489" s="T345">dʼe</ta>
            <ta e="T347" id="Seg_2490" s="T346">iti</ta>
            <ta e="T348" id="Seg_2491" s="T347">uhug-a</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2492" s="T0">dʼe</ta>
            <ta e="T2" id="Seg_2493" s="T1">biːr</ta>
            <ta e="T3" id="Seg_2494" s="T2">ogonnʼor</ta>
            <ta e="T4" id="Seg_2495" s="T3">olor-BIT-tA</ta>
            <ta e="T5" id="Seg_2496" s="T4">Ukukuːt Čukukuːt</ta>
            <ta e="T6" id="Seg_2497" s="T5">aːt-tA</ta>
            <ta e="T7" id="Seg_2498" s="T6">bu-tI-ŋ</ta>
            <ta e="T8" id="Seg_2499" s="T7">ügüs</ta>
            <ta e="T9" id="Seg_2500" s="T8">bagajɨ</ta>
            <ta e="T10" id="Seg_2501" s="T9">ogo-tA</ta>
            <ta e="T11" id="Seg_2502" s="T10">dʼe</ta>
            <ta e="T12" id="Seg_2503" s="T11">bu</ta>
            <ta e="T13" id="Seg_2504" s="T12">bu-GA</ta>
            <ta e="T14" id="Seg_2505" s="T13">hahɨl</ta>
            <ta e="T15" id="Seg_2506" s="T14">kel-BIT-tA</ta>
            <ta e="T16" id="Seg_2507" s="T15">ogonnʼor</ta>
            <ta e="T17" id="Seg_2508" s="T16">biːr</ta>
            <ta e="T18" id="Seg_2509" s="T17">ogo-GI-n</ta>
            <ta e="T19" id="Seg_2510" s="T18">egel</ta>
            <ta e="T20" id="Seg_2511" s="T19">di͡e-Ar</ta>
            <ta e="T21" id="Seg_2512" s="T20">hi͡e-IAK-BI-n</ta>
            <ta e="T22" id="Seg_2513" s="T21">bu-GA</ta>
            <ta e="T23" id="Seg_2514" s="T22">bu</ta>
            <ta e="T24" id="Seg_2515" s="T23">ogonnʼor</ta>
            <ta e="T25" id="Seg_2516" s="T24">ogo-tI-n</ta>
            <ta e="T26" id="Seg_2517" s="T25">bi͡er-BIT-tA</ta>
            <ta e="T27" id="Seg_2518" s="T26">hi͡e-TIn</ta>
            <ta e="T28" id="Seg_2519" s="T27">dʼe</ta>
            <ta e="T29" id="Seg_2520" s="T28">ol</ta>
            <ta e="T30" id="Seg_2521" s="T29">bar-BIT-tA</ta>
            <ta e="T31" id="Seg_2522" s="T30">kas</ta>
            <ta e="T32" id="Seg_2523" s="T31">eme</ta>
            <ta e="T33" id="Seg_2524" s="T32">kon-An</ta>
            <ta e="T34" id="Seg_2525" s="T33">baran</ta>
            <ta e="T35" id="Seg_2526" s="T34">emi͡e</ta>
            <ta e="T36" id="Seg_2527" s="T35">kel-BIT-tA</ta>
            <ta e="T37" id="Seg_2528" s="T36">ogonnʼor</ta>
            <ta e="T38" id="Seg_2529" s="T37">emi͡e</ta>
            <ta e="T39" id="Seg_2530" s="T38">ogo-TA</ta>
            <ta e="T40" id="Seg_2531" s="T39">egel</ta>
            <ta e="T41" id="Seg_2532" s="T40">di͡e-Ar</ta>
            <ta e="T42" id="Seg_2533" s="T41">hi͡e-IAK-BI-n</ta>
            <ta e="T43" id="Seg_2534" s="T42">ol-GA</ta>
            <ta e="T44" id="Seg_2535" s="T43">ogonnʼor</ta>
            <ta e="T45" id="Seg_2536" s="T44">ogo-tI-n</ta>
            <ta e="T46" id="Seg_2537" s="T45">emi͡e</ta>
            <ta e="T47" id="Seg_2538" s="T46">bi͡er-BIT-tA</ta>
            <ta e="T48" id="Seg_2539" s="T47">dʼe</ta>
            <ta e="T49" id="Seg_2540" s="T48">ol</ta>
            <ta e="T50" id="Seg_2541" s="T49">ogo-tI-n</ta>
            <ta e="T51" id="Seg_2542" s="T50">ilt-BIT-tA</ta>
            <ta e="T52" id="Seg_2543" s="T51">hi͡e-AːrI</ta>
            <ta e="T53" id="Seg_2544" s="T52">onton</ta>
            <ta e="T54" id="Seg_2545" s="T53">kas</ta>
            <ta e="T55" id="Seg_2546" s="T54">eme</ta>
            <ta e="T56" id="Seg_2547" s="T55">kün</ta>
            <ta e="T57" id="Seg_2548" s="T56">bu͡ol-An</ta>
            <ta e="T58" id="Seg_2549" s="T57">baran</ta>
            <ta e="T59" id="Seg_2550" s="T58">emi͡e</ta>
            <ta e="T60" id="Seg_2551" s="T59">kel-BIT-tA</ta>
            <ta e="T61" id="Seg_2552" s="T60">ol</ta>
            <ta e="T62" id="Seg_2553" s="T61">kel-An</ta>
            <ta e="T63" id="Seg_2554" s="T62">emi͡e</ta>
            <ta e="T64" id="Seg_2555" s="T63">di͡e-Ar</ta>
            <ta e="T65" id="Seg_2556" s="T64">ogo-TA</ta>
            <ta e="T66" id="Seg_2557" s="T65">egel</ta>
            <ta e="T67" id="Seg_2558" s="T66">di͡e-Ar</ta>
            <ta e="T68" id="Seg_2559" s="T67">hi͡e-IAK-BI-n</ta>
            <ta e="T69" id="Seg_2560" s="T68">ogo-tA</ta>
            <ta e="T70" id="Seg_2561" s="T69">ele-tA</ta>
            <ta e="T71" id="Seg_2562" s="T70">bu͡ol-TI-tA</ta>
            <ta e="T72" id="Seg_2563" s="T71">dʼe</ta>
            <ta e="T73" id="Seg_2564" s="T72">ol</ta>
            <ta e="T74" id="Seg_2565" s="T73">bar-BIT-tA</ta>
            <ta e="T75" id="Seg_2566" s="T74">da</ta>
            <ta e="T76" id="Seg_2567" s="T75">hahɨl-I-ŋ</ta>
            <ta e="T77" id="Seg_2568" s="T76">dʼɨlɨj-BIT-tA</ta>
            <ta e="T78" id="Seg_2569" s="T77">bu</ta>
            <ta e="T79" id="Seg_2570" s="T78">ogonnʼor-I-ŋ</ta>
            <ta e="T80" id="Seg_2571" s="T79">olor-A</ta>
            <ta e="T81" id="Seg_2572" s="T80">hataː-An</ta>
            <ta e="T82" id="Seg_2573" s="T81">bar-An</ta>
            <ta e="T83" id="Seg_2574" s="T82">kaːm-An</ta>
            <ta e="T84" id="Seg_2575" s="T83">kaːl-TI-tA</ta>
            <ta e="T85" id="Seg_2576" s="T84">dʼe</ta>
            <ta e="T86" id="Seg_2577" s="T85">bu</ta>
            <ta e="T87" id="Seg_2578" s="T86">kaːm-An</ta>
            <ta e="T88" id="Seg_2579" s="T87">is-An-is-An</ta>
            <ta e="T89" id="Seg_2580" s="T88">bar-An</ta>
            <ta e="T90" id="Seg_2581" s="T89">biːr</ta>
            <ta e="T91" id="Seg_2582" s="T90">kɨːl</ta>
            <ta e="T92" id="Seg_2583" s="T91">baːjtahɨn-tI-n</ta>
            <ta e="T93" id="Seg_2584" s="T92">ölör-An</ta>
            <ta e="T94" id="Seg_2585" s="T93">bar-An</ta>
            <ta e="T95" id="Seg_2586" s="T94">ü͡öl-I-n-A</ta>
            <ta e="T96" id="Seg_2587" s="T95">olor-TI-tA</ta>
            <ta e="T97" id="Seg_2588" s="T96">bu</ta>
            <ta e="T98" id="Seg_2589" s="T97">olor-TAK-InA</ta>
            <ta e="T99" id="Seg_2590" s="T98">hahɨl</ta>
            <ta e="T100" id="Seg_2591" s="T99">kel-TI-tA</ta>
            <ta e="T101" id="Seg_2592" s="T100">dʼe</ta>
            <ta e="T102" id="Seg_2593" s="T101">ogonnʼor</ta>
            <ta e="T103" id="Seg_2594" s="T102">bihigi</ta>
            <ta e="T104" id="Seg_2595" s="T103">ogo</ta>
            <ta e="T105" id="Seg_2596" s="T104">bu͡ol-A</ta>
            <ta e="T106" id="Seg_2597" s="T105">oːnnʼoː-IAk</ta>
            <ta e="T107" id="Seg_2598" s="T106">di͡e-TI-tA</ta>
            <ta e="T108" id="Seg_2599" s="T107">dʼe</ta>
            <ta e="T109" id="Seg_2600" s="T108">daːrɨm</ta>
            <ta e="T110" id="Seg_2601" s="T109">ogo</ta>
            <ta e="T111" id="Seg_2602" s="T110">bu͡ol-A</ta>
            <ta e="T112" id="Seg_2603" s="T111">oːnnʼoː-IAk</ta>
            <ta e="T113" id="Seg_2604" s="T112">bu</ta>
            <ta e="T114" id="Seg_2605" s="T113">ogonnʼor-I-ŋ</ta>
            <ta e="T115" id="Seg_2606" s="T114">bihik</ta>
            <ta e="T116" id="Seg_2607" s="T115">oŋor-TI-tA</ta>
            <ta e="T117" id="Seg_2608" s="T116">ol</ta>
            <ta e="T118" id="Seg_2609" s="T117">baːjtahɨn-LAr-tI-n</ta>
            <ta e="T119" id="Seg_2610" s="T118">tiriː-tI-nAn</ta>
            <ta e="T120" id="Seg_2611" s="T119">bihik</ta>
            <ta e="T121" id="Seg_2612" s="T120">bɨ͡a-tA</ta>
            <ta e="T122" id="Seg_2613" s="T121">oŋor-I-n-TI-LAr</ta>
            <ta e="T123" id="Seg_2614" s="T122">ogonnʼor-I-ŋ</ta>
            <ta e="T124" id="Seg_2615" s="T123">di͡e-BIT</ta>
            <ta e="T125" id="Seg_2616" s="T124">dʼe</ta>
            <ta e="T126" id="Seg_2617" s="T125">doː</ta>
            <ta e="T127" id="Seg_2618" s="T126">en</ta>
            <ta e="T128" id="Seg_2619" s="T127">bigeː-A-n</ta>
            <ta e="T129" id="Seg_2620" s="T128">ogo</ta>
            <ta e="T130" id="Seg_2621" s="T129">bu͡ol-An</ta>
            <ta e="T131" id="Seg_2622" s="T130">di͡e-TI-tA</ta>
            <ta e="T132" id="Seg_2623" s="T131">hahɨl</ta>
            <ta e="T133" id="Seg_2624" s="T132">bu-GA</ta>
            <ta e="T134" id="Seg_2625" s="T133">hɨt-An</ta>
            <ta e="T135" id="Seg_2626" s="T134">kör-TI-tA</ta>
            <ta e="T136" id="Seg_2627" s="T135">eː</ta>
            <ta e="T137" id="Seg_2628" s="T136">dogor</ta>
            <ta e="T138" id="Seg_2629" s="T137">mini͡ene</ta>
            <ta e="T139" id="Seg_2630" s="T138">kuturuk-I-m</ta>
            <ta e="T140" id="Seg_2631" s="T139">ɨ͡arɨj-Ar</ta>
            <ta e="T141" id="Seg_2632" s="T140">hɨt-IAr-IAK.[tA]</ta>
            <ta e="T142" id="Seg_2633" s="T141">hu͡ok</ta>
            <ta e="T143" id="Seg_2634" s="T142">di͡e-TI-tA</ta>
            <ta e="T144" id="Seg_2635" s="T143">dʼe</ta>
            <ta e="T145" id="Seg_2636" s="T144">kihi</ta>
            <ta e="T146" id="Seg_2637" s="T145">kihi-GI-nAn</ta>
            <ta e="T147" id="Seg_2638" s="T146">en</ta>
            <ta e="T148" id="Seg_2639" s="T147">hɨt-An</ta>
            <ta e="T149" id="Seg_2640" s="T148">ogo</ta>
            <ta e="T150" id="Seg_2641" s="T149">bu͡ol</ta>
            <ta e="T151" id="Seg_2642" s="T150">di͡e-TI-tA</ta>
            <ta e="T152" id="Seg_2643" s="T151">hahɨl</ta>
            <ta e="T153" id="Seg_2644" s="T152">ogonnʼor-nI</ta>
            <ta e="T154" id="Seg_2645" s="T153">ogonnʼor</ta>
            <ta e="T155" id="Seg_2646" s="T154">dʼe</ta>
            <ta e="T156" id="Seg_2647" s="T155">hɨt-TI-tA</ta>
            <ta e="T157" id="Seg_2648" s="T156">hahɨl</ta>
            <ta e="T158" id="Seg_2649" s="T157">dʼe</ta>
            <ta e="T159" id="Seg_2650" s="T158">bu-nI</ta>
            <ta e="T160" id="Seg_2651" s="T159">kelgij-An</ta>
            <ta e="T161" id="Seg_2652" s="T160">keːs-TI-tA</ta>
            <ta e="T162" id="Seg_2653" s="T161">hahɨl</ta>
            <ta e="T163" id="Seg_2654" s="T162">bu-nI</ta>
            <ta e="T164" id="Seg_2655" s="T163">bigeː-A-bigeː-A</ta>
            <ta e="T165" id="Seg_2656" s="T164">kɨːl</ta>
            <ta e="T166" id="Seg_2657" s="T165">et-I-nAn</ta>
            <ta e="T167" id="Seg_2658" s="T166">ɨstaː-An</ta>
            <ta e="T168" id="Seg_2659" s="T167">ahaː-A-t-A</ta>
            <ta e="T169" id="Seg_2660" s="T168">olor-TI-tA</ta>
            <ta e="T170" id="Seg_2661" s="T169">bu</ta>
            <ta e="T171" id="Seg_2662" s="T170">ogonnʼor-I-ŋ</ta>
            <ta e="T172" id="Seg_2663" s="T171">agaj</ta>
            <ta e="T173" id="Seg_2664" s="T172">utuj-An</ta>
            <ta e="T174" id="Seg_2665" s="T173">kaːl-TI-tA</ta>
            <ta e="T175" id="Seg_2666" s="T174">hahɨl</ta>
            <ta e="T176" id="Seg_2667" s="T175">bu-nI</ta>
            <ta e="T177" id="Seg_2668" s="T176">hak-I-s-TAː-An</ta>
            <ta e="T178" id="Seg_2669" s="T177">ilt-An</ta>
            <ta e="T179" id="Seg_2670" s="T178">bajaraːk-ttAn</ta>
            <ta e="T180" id="Seg_2671" s="T179">tüŋner-I</ta>
            <ta e="T181" id="Seg_2672" s="T180">bɨrak-TI-tA</ta>
            <ta e="T182" id="Seg_2673" s="T181">kojut</ta>
            <ta e="T183" id="Seg_2674" s="T182">ol</ta>
            <ta e="T184" id="Seg_2675" s="T183">ogonnʼor</ta>
            <ta e="T185" id="Seg_2676" s="T184">uhugun-I-BIT-tA</ta>
            <ta e="T186" id="Seg_2677" s="T185">bajaraːk</ta>
            <ta e="T187" id="Seg_2678" s="T186">is-tI-GAr</ta>
            <ta e="T188" id="Seg_2679" s="T187">hɨt-Ar</ta>
            <ta e="T189" id="Seg_2680" s="T188">ogonnʼor</ta>
            <ta e="T190" id="Seg_2681" s="T189">kaja</ta>
            <ta e="T191" id="Seg_2682" s="T190">da</ta>
            <ta e="T192" id="Seg_2683" s="T191">dek</ta>
            <ta e="T193" id="Seg_2684" s="T192">kamnaː-IAK-tA-r</ta>
            <ta e="T194" id="Seg_2685" s="T193">bert</ta>
            <ta e="T195" id="Seg_2686" s="T194">kelgij-LIN-I-BIT</ta>
            <ta e="T196" id="Seg_2687" s="T195">kihi</ta>
            <ta e="T197" id="Seg_2688" s="T196">ogonnʼor</ta>
            <ta e="T198" id="Seg_2689" s="T197">ɨŋɨr-Ar</ta>
            <ta e="T199" id="Seg_2690" s="T198">gornu͡ok-LAr</ta>
            <ta e="T200" id="Seg_2691" s="T199">kɨrsa-LAr</ta>
            <ta e="T201" id="Seg_2692" s="T200">kutujak-LAr</ta>
            <ta e="T202" id="Seg_2693" s="T201">bɨ͡a-BI-n</ta>
            <ta e="T203" id="Seg_2694" s="T202">bɨs-A</ta>
            <ta e="T204" id="Seg_2695" s="T203">kerbeː-ŋ</ta>
            <ta e="T205" id="Seg_2696" s="T204">di͡e-Ar</ta>
            <ta e="T206" id="Seg_2697" s="T205">ol-LAr-I-ŋ</ta>
            <ta e="T207" id="Seg_2698" s="T206">kel-An-LAr</ta>
            <ta e="T208" id="Seg_2699" s="T207">bɨ͡a-tI-n</ta>
            <ta e="T209" id="Seg_2700" s="T208">bɨs-A</ta>
            <ta e="T210" id="Seg_2701" s="T209">kerbeː-BIT-LArA</ta>
            <ta e="T211" id="Seg_2702" s="T210">ogonnʼor</ta>
            <ta e="T212" id="Seg_2703" s="T211">tur-A</ta>
            <ta e="T213" id="Seg_2704" s="T212">ekkireː-TI-tA</ta>
            <ta e="T214" id="Seg_2705" s="T213">innʼe</ta>
            <ta e="T215" id="Seg_2706" s="T214">gɨn-An</ta>
            <ta e="T216" id="Seg_2707" s="T215">bu</ta>
            <ta e="T217" id="Seg_2708" s="T216">bɨ͡a-tI-n</ta>
            <ta e="T218" id="Seg_2709" s="T217">kerbeː-AːččI-LAr-GA</ta>
            <ta e="T219" id="Seg_2710" s="T218">naju͡om-tI-n</ta>
            <ta e="T220" id="Seg_2711" s="T219">biːr</ta>
            <ta e="T221" id="Seg_2712" s="T220">kɨːl</ta>
            <ta e="T222" id="Seg_2713" s="T221">baːjtahɨn-tI-n</ta>
            <ta e="T223" id="Seg_2714" s="T222">ölör-An</ta>
            <ta e="T224" id="Seg_2715" s="T223">bi͡er-TI-tA</ta>
            <ta e="T225" id="Seg_2716" s="T224">bu</ta>
            <ta e="T226" id="Seg_2717" s="T225">ogonnʼor-I-ŋ</ta>
            <ta e="T227" id="Seg_2718" s="T226">otuː-tI-GAr</ta>
            <ta e="T228" id="Seg_2719" s="T227">kaːm-An</ta>
            <ta e="T229" id="Seg_2720" s="T228">kel-TI-tA</ta>
            <ta e="T230" id="Seg_2721" s="T229">otuː-tI-n</ta>
            <ta e="T231" id="Seg_2722" s="T230">kül-tI-n</ta>
            <ta e="T232" id="Seg_2723" s="T231">bütün-LIː</ta>
            <ta e="T233" id="Seg_2724" s="T232">hɨ͡aldʼa-tI-GAr</ta>
            <ta e="T234" id="Seg_2725" s="T233">kaːlaː-An</ta>
            <ta e="T235" id="Seg_2726" s="T234">bar-An</ta>
            <ta e="T236" id="Seg_2727" s="T235">kaːm-An</ta>
            <ta e="T237" id="Seg_2728" s="T236">kaːl-TI-tA</ta>
            <ta e="T238" id="Seg_2729" s="T237">dʼe</ta>
            <ta e="T239" id="Seg_2730" s="T238">bu</ta>
            <ta e="T240" id="Seg_2731" s="T239">bar-An</ta>
            <ta e="T241" id="Seg_2732" s="T240">is-TI-tA</ta>
            <ta e="T242" id="Seg_2733" s="T241">bar-An</ta>
            <ta e="T243" id="Seg_2734" s="T242">is-An-is-An</ta>
            <ta e="T244" id="Seg_2735" s="T243">agaj</ta>
            <ta e="T245" id="Seg_2736" s="T244">biːr</ta>
            <ta e="T246" id="Seg_2737" s="T245">dʼi͡e-GA</ta>
            <ta e="T247" id="Seg_2738" s="T246">tij-TI-tA</ta>
            <ta e="T248" id="Seg_2739" s="T247">bu</ta>
            <ta e="T249" id="Seg_2740" s="T248">dʼi͡e-GA</ta>
            <ta e="T250" id="Seg_2741" s="T249">kel-An</ta>
            <ta e="T251" id="Seg_2742" s="T250">ihilleː-An</ta>
            <ta e="T252" id="Seg_2743" s="T251">kör-BIT-tA</ta>
            <ta e="T253" id="Seg_2744" s="T252">hahɨl</ta>
            <ta e="T254" id="Seg_2745" s="T253">kɨːr-An</ta>
            <ta e="T255" id="Seg_2746" s="T254">er-Ar</ta>
            <ta e="T256" id="Seg_2747" s="T255">dʼi͡e</ta>
            <ta e="T257" id="Seg_2748" s="T256">is-tI-GAr</ta>
            <ta e="T258" id="Seg_2749" s="T257">kiːr-BIT-tA</ta>
            <ta e="T259" id="Seg_2750" s="T258">hahɨl</ta>
            <ta e="T260" id="Seg_2751" s="T259">bütün-LIː</ta>
            <ta e="T261" id="Seg_2752" s="T260">munnʼus-n-An</ta>
            <ta e="T262" id="Seg_2753" s="T261">baran</ta>
            <ta e="T263" id="Seg_2754" s="T262">kɨːr-A</ta>
            <ta e="T264" id="Seg_2755" s="T263">hɨt-Ar-LAr</ta>
            <ta e="T265" id="Seg_2756" s="T264">ojun-LAːK-LAr</ta>
            <ta e="T266" id="Seg_2757" s="T265">ogonnʼor</ta>
            <ta e="T267" id="Seg_2758" s="T266">kiːr-An</ta>
            <ta e="T268" id="Seg_2759" s="T267">olor-TI-tA</ta>
            <ta e="T269" id="Seg_2760" s="T268">bu</ta>
            <ta e="T270" id="Seg_2761" s="T269">olor-TAK-InA</ta>
            <ta e="T271" id="Seg_2762" s="T270">hahɨl-I-ŋ</ta>
            <ta e="T272" id="Seg_2763" s="T271">tönün-TI-tA</ta>
            <ta e="T273" id="Seg_2764" s="T272">kɨːr-An</ta>
            <ta e="T274" id="Seg_2765" s="T273">bar-AːT</ta>
            <ta e="T275" id="Seg_2766" s="T274">bu</ta>
            <ta e="T276" id="Seg_2767" s="T275">ogonnʼor-I-ŋ</ta>
            <ta e="T277" id="Seg_2768" s="T276">agaj</ta>
            <ta e="T278" id="Seg_2769" s="T277">düŋür</ta>
            <ta e="T279" id="Seg_2770" s="T278">ogus-TI-tA</ta>
            <ta e="T280" id="Seg_2771" s="T279">bu</ta>
            <ta e="T281" id="Seg_2772" s="T280">düŋür-tI-n</ta>
            <ta e="T282" id="Seg_2773" s="T281">ogus-A-ogus-A</ta>
            <ta e="T283" id="Seg_2774" s="T282">dʼe</ta>
            <ta e="T284" id="Seg_2775" s="T283">ekkireː-TI-tA</ta>
            <ta e="T285" id="Seg_2776" s="T284">dʼe</ta>
            <ta e="T286" id="Seg_2777" s="T285">bu-GA</ta>
            <ta e="T287" id="Seg_2778" s="T286">kül-tA</ta>
            <ta e="T288" id="Seg_2779" s="T287">dʼe</ta>
            <ta e="T289" id="Seg_2780" s="T288">burgaj-ŋnAː-TI-tA</ta>
            <ta e="T290" id="Seg_2781" s="T289">bu</ta>
            <ta e="T291" id="Seg_2782" s="T290">hɨ͡aldʼa-tI-ttAn</ta>
            <ta e="T292" id="Seg_2783" s="T291">dʼe</ta>
            <ta e="T293" id="Seg_2784" s="T292">bu-ttAn</ta>
            <ta e="T294" id="Seg_2785" s="T293">bu</ta>
            <ta e="T295" id="Seg_2786" s="T294">hahɨl-LAr-I-ŋ</ta>
            <ta e="T296" id="Seg_2787" s="T295">bütün-LIː</ta>
            <ta e="T297" id="Seg_2788" s="T296">dʼe</ta>
            <ta e="T298" id="Seg_2789" s="T297">kül-I-s-TI-LAr</ta>
            <ta e="T299" id="Seg_2790" s="T298">agaj</ta>
            <ta e="T300" id="Seg_2791" s="T299">onno</ta>
            <ta e="T301" id="Seg_2792" s="T300">ulaga-GA</ta>
            <ta e="T302" id="Seg_2793" s="T301">hahɨl</ta>
            <ta e="T303" id="Seg_2794" s="T302">kül-Ar-tA</ta>
            <ta e="T304" id="Seg_2795" s="T303">ihilin-TI-tA</ta>
            <ta e="T305" id="Seg_2796" s="T304">ogonnʼor</ta>
            <ta e="T306" id="Seg_2797" s="T305">ihit-TI-tA</ta>
            <ta e="T307" id="Seg_2798" s="T306">düŋür-tI-n</ta>
            <ta e="T308" id="Seg_2799" s="T307">bɨraː-An</ta>
            <ta e="T309" id="Seg_2800" s="T308">keːs-TI-tA</ta>
            <ta e="T310" id="Seg_2801" s="T309">ogonnʼor</ta>
            <ta e="T311" id="Seg_2802" s="T310">bu</ta>
            <ta e="T312" id="Seg_2803" s="T311">hahɨl-nI</ta>
            <ta e="T313" id="Seg_2804" s="T312">kap-An</ta>
            <ta e="T314" id="Seg_2805" s="T313">ɨl-TI-tA</ta>
            <ta e="T315" id="Seg_2806" s="T314">hahɨl-tI-n</ta>
            <ta e="T316" id="Seg_2807" s="T315">u͡ot-GA</ta>
            <ta e="T317" id="Seg_2808" s="T316">ɨhaːr-A-ɨhaːr-A</ta>
            <ta e="T318" id="Seg_2809" s="T317">dʼe</ta>
            <ta e="T319" id="Seg_2810" s="T318">tahɨj-TI-tA</ta>
            <ta e="T320" id="Seg_2811" s="T319">tahɨj-An-tahɨj-An</ta>
            <ta e="T321" id="Seg_2812" s="T320">ölör-An</ta>
            <ta e="T322" id="Seg_2813" s="T321">keːs-TI-tA</ta>
            <ta e="T323" id="Seg_2814" s="T322">dʼe</ta>
            <ta e="T324" id="Seg_2815" s="T323">ol</ta>
            <ta e="T325" id="Seg_2816" s="T324">hahɨl-I-ŋ</ta>
            <ta e="T326" id="Seg_2817" s="T325">mannaj</ta>
            <ta e="T327" id="Seg_2818" s="T326">kini͡ene-n</ta>
            <ta e="T328" id="Seg_2819" s="T327">ogo-LAr-tI-n</ta>
            <ta e="T329" id="Seg_2820" s="T328">hi͡e-AːččI</ta>
            <ta e="T330" id="Seg_2821" s="T329">hol</ta>
            <ta e="T331" id="Seg_2822" s="T330">hahɨl-I-ŋ</ta>
            <ta e="T332" id="Seg_2823" s="T331">bihik-GA</ta>
            <ta e="T333" id="Seg_2824" s="T332">kelgij-An</ta>
            <ta e="T334" id="Seg_2825" s="T333">bar-An</ta>
            <ta e="T335" id="Seg_2826" s="T334">tüŋner-I</ta>
            <ta e="T336" id="Seg_2827" s="T335">keːs-BIT-tA</ta>
            <ta e="T337" id="Seg_2828" s="T336">innʼe</ta>
            <ta e="T338" id="Seg_2829" s="T337">gɨn-An</ta>
            <ta e="T339" id="Seg_2830" s="T338">baran</ta>
            <ta e="T340" id="Seg_2831" s="T339">dʼe</ta>
            <ta e="T341" id="Seg_2832" s="T340">iti</ta>
            <ta e="T342" id="Seg_2833" s="T341">ogonnʼor-tI-n</ta>
            <ta e="T343" id="Seg_2834" s="T342">dʼi͡e-tI-GAr</ta>
            <ta e="T344" id="Seg_2835" s="T343">bar-An</ta>
            <ta e="T345" id="Seg_2836" s="T344">olor-BIT-tA</ta>
            <ta e="T346" id="Seg_2837" s="T345">dʼe</ta>
            <ta e="T347" id="Seg_2838" s="T346">iti</ta>
            <ta e="T348" id="Seg_2839" s="T347">uhuk-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2840" s="T0">well</ta>
            <ta e="T2" id="Seg_2841" s="T1">one</ta>
            <ta e="T3" id="Seg_2842" s="T2">old.man.[NOM]</ta>
            <ta e="T4" id="Seg_2843" s="T3">live-PST2-3SG</ta>
            <ta e="T5" id="Seg_2844" s="T4">Ukukuut_Chukukuut</ta>
            <ta e="T6" id="Seg_2845" s="T5">name-3SG.[NOM]</ta>
            <ta e="T7" id="Seg_2846" s="T6">this-3SG-2SG.[NOM]</ta>
            <ta e="T8" id="Seg_2847" s="T7">many</ta>
            <ta e="T9" id="Seg_2848" s="T8">very</ta>
            <ta e="T10" id="Seg_2849" s="T9">child-3SG.[NOM]</ta>
            <ta e="T11" id="Seg_2850" s="T10">well</ta>
            <ta e="T12" id="Seg_2851" s="T11">this</ta>
            <ta e="T13" id="Seg_2852" s="T12">this-DAT/LOC</ta>
            <ta e="T14" id="Seg_2853" s="T13">fox.[NOM]</ta>
            <ta e="T15" id="Seg_2854" s="T14">come-PST2-3SG</ta>
            <ta e="T16" id="Seg_2855" s="T15">old.man.[NOM]</ta>
            <ta e="T17" id="Seg_2856" s="T16">one</ta>
            <ta e="T18" id="Seg_2857" s="T17">child-2SG-ACC</ta>
            <ta e="T19" id="Seg_2858" s="T18">get.[IMP.2SG]</ta>
            <ta e="T20" id="Seg_2859" s="T19">say-PRS.[3SG]</ta>
            <ta e="T21" id="Seg_2860" s="T20">eat-PTCP.FUT-1SG-ACC</ta>
            <ta e="T22" id="Seg_2861" s="T21">this-DAT/LOC</ta>
            <ta e="T23" id="Seg_2862" s="T22">this</ta>
            <ta e="T24" id="Seg_2863" s="T23">old.man.[NOM]</ta>
            <ta e="T25" id="Seg_2864" s="T24">child-3SG-ACC</ta>
            <ta e="T26" id="Seg_2865" s="T25">give-PST2-3SG</ta>
            <ta e="T27" id="Seg_2866" s="T26">eat-IMP.3SG</ta>
            <ta e="T28" id="Seg_2867" s="T27">well</ta>
            <ta e="T29" id="Seg_2868" s="T28">that.[NOM]</ta>
            <ta e="T30" id="Seg_2869" s="T29">go-PST2-3SG</ta>
            <ta e="T31" id="Seg_2870" s="T30">how.much</ta>
            <ta e="T32" id="Seg_2871" s="T31">INDEF</ta>
            <ta e="T33" id="Seg_2872" s="T32">overnight-CVB.SEQ</ta>
            <ta e="T34" id="Seg_2873" s="T33">after</ta>
            <ta e="T35" id="Seg_2874" s="T34">again</ta>
            <ta e="T36" id="Seg_2875" s="T35">come-PST2-3SG</ta>
            <ta e="T37" id="Seg_2876" s="T36">old.man.[NOM]</ta>
            <ta e="T38" id="Seg_2877" s="T37">again</ta>
            <ta e="T39" id="Seg_2878" s="T38">child-PART</ta>
            <ta e="T40" id="Seg_2879" s="T39">get.[IMP.2SG]</ta>
            <ta e="T41" id="Seg_2880" s="T40">say-PRS.[3SG]</ta>
            <ta e="T42" id="Seg_2881" s="T41">eat-PTCP.FUT-1SG-ACC</ta>
            <ta e="T43" id="Seg_2882" s="T42">that-DAT/LOC</ta>
            <ta e="T44" id="Seg_2883" s="T43">old.man.[NOM]</ta>
            <ta e="T45" id="Seg_2884" s="T44">child-3SG-ACC</ta>
            <ta e="T46" id="Seg_2885" s="T45">again</ta>
            <ta e="T47" id="Seg_2886" s="T46">give-PST2-3SG</ta>
            <ta e="T48" id="Seg_2887" s="T47">well</ta>
            <ta e="T49" id="Seg_2888" s="T48">that</ta>
            <ta e="T50" id="Seg_2889" s="T49">child-3SG-ACC</ta>
            <ta e="T51" id="Seg_2890" s="T50">carry-PST2-3SG</ta>
            <ta e="T52" id="Seg_2891" s="T51">eat-CVB.PURP</ta>
            <ta e="T53" id="Seg_2892" s="T52">then</ta>
            <ta e="T54" id="Seg_2893" s="T53">how.much</ta>
            <ta e="T55" id="Seg_2894" s="T54">INDEF</ta>
            <ta e="T56" id="Seg_2895" s="T55">day.[NOM]</ta>
            <ta e="T57" id="Seg_2896" s="T56">be-CVB.SEQ</ta>
            <ta e="T58" id="Seg_2897" s="T57">after</ta>
            <ta e="T59" id="Seg_2898" s="T58">again</ta>
            <ta e="T60" id="Seg_2899" s="T59">come-PST2-3SG</ta>
            <ta e="T61" id="Seg_2900" s="T60">that.[NOM]</ta>
            <ta e="T62" id="Seg_2901" s="T61">come-CVB.SEQ</ta>
            <ta e="T63" id="Seg_2902" s="T62">again</ta>
            <ta e="T64" id="Seg_2903" s="T63">say-PRS.[3SG]</ta>
            <ta e="T65" id="Seg_2904" s="T64">child-PART</ta>
            <ta e="T66" id="Seg_2905" s="T65">get.[IMP.2SG]</ta>
            <ta e="T67" id="Seg_2906" s="T66">say-PRS.[3SG]</ta>
            <ta e="T68" id="Seg_2907" s="T67">eat-PTCP.FUT-1SG-ACC</ta>
            <ta e="T69" id="Seg_2908" s="T68">child-3SG.[NOM]</ta>
            <ta e="T70" id="Seg_2909" s="T69">last-3SG.[NOM]</ta>
            <ta e="T71" id="Seg_2910" s="T70">be-PST1-3SG</ta>
            <ta e="T72" id="Seg_2911" s="T71">well</ta>
            <ta e="T73" id="Seg_2912" s="T72">that.[NOM]</ta>
            <ta e="T74" id="Seg_2913" s="T73">go-PST2-3SG</ta>
            <ta e="T75" id="Seg_2914" s="T74">and</ta>
            <ta e="T76" id="Seg_2915" s="T75">fox-EP-2SG.[NOM]</ta>
            <ta e="T77" id="Seg_2916" s="T76">disappear-PST2-3SG</ta>
            <ta e="T78" id="Seg_2917" s="T77">this</ta>
            <ta e="T79" id="Seg_2918" s="T78">old.man-EP-2SG.[NOM]</ta>
            <ta e="T80" id="Seg_2919" s="T79">live-CVB.SIM</ta>
            <ta e="T81" id="Seg_2920" s="T80">can-CVB.SEQ</ta>
            <ta e="T82" id="Seg_2921" s="T81">go-CVB.SEQ</ta>
            <ta e="T83" id="Seg_2922" s="T82">walk-CVB.SEQ</ta>
            <ta e="T84" id="Seg_2923" s="T83">stay-PST1-3SG</ta>
            <ta e="T85" id="Seg_2924" s="T84">well</ta>
            <ta e="T86" id="Seg_2925" s="T85">this</ta>
            <ta e="T87" id="Seg_2926" s="T86">walk-CVB.SEQ</ta>
            <ta e="T88" id="Seg_2927" s="T87">go-CVB.SEQ-go-CVB.SEQ</ta>
            <ta e="T89" id="Seg_2928" s="T88">go-CVB.SEQ</ta>
            <ta e="T90" id="Seg_2929" s="T89">one</ta>
            <ta e="T91" id="Seg_2930" s="T90">wild</ta>
            <ta e="T92" id="Seg_2931" s="T91">female.reindeer-3SG-ACC</ta>
            <ta e="T93" id="Seg_2932" s="T92">kill-CVB.SEQ</ta>
            <ta e="T94" id="Seg_2933" s="T93">go-CVB.SEQ</ta>
            <ta e="T95" id="Seg_2934" s="T94">fry-EP-MED-CVB.SIM</ta>
            <ta e="T96" id="Seg_2935" s="T95">sit.down-PST1-3SG</ta>
            <ta e="T97" id="Seg_2936" s="T96">this</ta>
            <ta e="T98" id="Seg_2937" s="T97">sit-TEMP-3SG</ta>
            <ta e="T99" id="Seg_2938" s="T98">fox.[NOM]</ta>
            <ta e="T100" id="Seg_2939" s="T99">come-PST1-3SG</ta>
            <ta e="T101" id="Seg_2940" s="T100">hey</ta>
            <ta e="T102" id="Seg_2941" s="T101">old.man.[NOM]</ta>
            <ta e="T103" id="Seg_2942" s="T102">1PL.[NOM]</ta>
            <ta e="T104" id="Seg_2943" s="T103">child.[NOM]</ta>
            <ta e="T105" id="Seg_2944" s="T104">be-CVB.SIM</ta>
            <ta e="T106" id="Seg_2945" s="T105">play-IMP.1DU</ta>
            <ta e="T107" id="Seg_2946" s="T106">say-PST1-3SG</ta>
            <ta e="T108" id="Seg_2947" s="T107">well</ta>
            <ta e="T109" id="Seg_2948" s="T108">okay</ta>
            <ta e="T110" id="Seg_2949" s="T109">child.[NOM]</ta>
            <ta e="T111" id="Seg_2950" s="T110">be-CVB.SIM</ta>
            <ta e="T112" id="Seg_2951" s="T111">play-IMP.1DU</ta>
            <ta e="T113" id="Seg_2952" s="T112">this</ta>
            <ta e="T114" id="Seg_2953" s="T113">old.man-EP-2SG.[NOM]</ta>
            <ta e="T115" id="Seg_2954" s="T114">cradle.[NOM]</ta>
            <ta e="T116" id="Seg_2955" s="T115">make-PST1-3SG</ta>
            <ta e="T117" id="Seg_2956" s="T116">that</ta>
            <ta e="T118" id="Seg_2957" s="T117">female.reindeer-PL-3SG-GEN</ta>
            <ta e="T119" id="Seg_2958" s="T118">fur-3SG-INSTR</ta>
            <ta e="T120" id="Seg_2959" s="T119">cradle.[NOM]</ta>
            <ta e="T121" id="Seg_2960" s="T120">strap-3SG.[NOM]</ta>
            <ta e="T122" id="Seg_2961" s="T121">make-EP-MED-PST1-3PL</ta>
            <ta e="T123" id="Seg_2962" s="T122">old.man-EP-2SG.[NOM]</ta>
            <ta e="T124" id="Seg_2963" s="T123">say-PST2.[3SG]</ta>
            <ta e="T125" id="Seg_2964" s="T124">well</ta>
            <ta e="T126" id="Seg_2965" s="T125">well</ta>
            <ta e="T127" id="Seg_2966" s="T126">2SG.[NOM]</ta>
            <ta e="T128" id="Seg_2967" s="T127">swing-EP-REFL.[IMP.2SG]</ta>
            <ta e="T129" id="Seg_2968" s="T128">child.[NOM]</ta>
            <ta e="T130" id="Seg_2969" s="T129">be-CVB.SEQ</ta>
            <ta e="T131" id="Seg_2970" s="T130">say-PST1-3SG</ta>
            <ta e="T132" id="Seg_2971" s="T131">fox.[NOM]</ta>
            <ta e="T133" id="Seg_2972" s="T132">this-DAT/LOC</ta>
            <ta e="T134" id="Seg_2973" s="T133">lie.down-CVB.SEQ</ta>
            <ta e="T135" id="Seg_2974" s="T134">try-PST1-3SG</ta>
            <ta e="T136" id="Seg_2975" s="T135">hey</ta>
            <ta e="T137" id="Seg_2976" s="T136">friend.[NOM]</ta>
            <ta e="T138" id="Seg_2977" s="T137">my</ta>
            <ta e="T139" id="Seg_2978" s="T138">tail-EP-1SG.[NOM]</ta>
            <ta e="T140" id="Seg_2979" s="T139">hurt-PRS.[3SG]</ta>
            <ta e="T141" id="Seg_2980" s="T140">lie-CAUS-FUT.[3SG]</ta>
            <ta e="T142" id="Seg_2981" s="T141">NEG</ta>
            <ta e="T143" id="Seg_2982" s="T142">say-PST1-3SG</ta>
            <ta e="T144" id="Seg_2983" s="T143">well</ta>
            <ta e="T145" id="Seg_2984" s="T144">human.being.[NOM]</ta>
            <ta e="T146" id="Seg_2985" s="T145">human.being-2SG-INSTR</ta>
            <ta e="T147" id="Seg_2986" s="T146">2SG.[NOM]</ta>
            <ta e="T148" id="Seg_2987" s="T147">lie.down-CVB.SEQ</ta>
            <ta e="T149" id="Seg_2988" s="T148">child.[NOM]</ta>
            <ta e="T150" id="Seg_2989" s="T149">be.[IMP.2SG]</ta>
            <ta e="T151" id="Seg_2990" s="T150">say-PST1-3SG</ta>
            <ta e="T152" id="Seg_2991" s="T151">fox.[NOM]</ta>
            <ta e="T153" id="Seg_2992" s="T152">old.man-ACC</ta>
            <ta e="T154" id="Seg_2993" s="T153">old.man.[NOM]</ta>
            <ta e="T155" id="Seg_2994" s="T154">well</ta>
            <ta e="T156" id="Seg_2995" s="T155">lie.down-PST1-3SG</ta>
            <ta e="T157" id="Seg_2996" s="T156">fox.[NOM]</ta>
            <ta e="T158" id="Seg_2997" s="T157">well</ta>
            <ta e="T159" id="Seg_2998" s="T158">this-ACC</ta>
            <ta e="T160" id="Seg_2999" s="T159">tie-CVB.SEQ</ta>
            <ta e="T161" id="Seg_3000" s="T160">throw-PST1-3SG</ta>
            <ta e="T162" id="Seg_3001" s="T161">fox.[NOM]</ta>
            <ta e="T163" id="Seg_3002" s="T162">this-ACC</ta>
            <ta e="T164" id="Seg_3003" s="T163">swing-CVB.SIM-swing-CVB.SIM</ta>
            <ta e="T165" id="Seg_3004" s="T164">wild.reindeer.[NOM]</ta>
            <ta e="T166" id="Seg_3005" s="T165">meat-3SG-INSTR</ta>
            <ta e="T167" id="Seg_3006" s="T166">chew-CVB.SEQ</ta>
            <ta e="T168" id="Seg_3007" s="T167">eat-EP-CAUS-CVB.SIM</ta>
            <ta e="T169" id="Seg_3008" s="T168">sit-PST1-3SG</ta>
            <ta e="T170" id="Seg_3009" s="T169">this</ta>
            <ta e="T171" id="Seg_3010" s="T170">old.man-EP-2SG.[NOM]</ta>
            <ta e="T172" id="Seg_3011" s="T171">only</ta>
            <ta e="T173" id="Seg_3012" s="T172">sleep-CVB.SEQ</ta>
            <ta e="T174" id="Seg_3013" s="T173">stay-PST1-3SG</ta>
            <ta e="T175" id="Seg_3014" s="T174">fox.[NOM]</ta>
            <ta e="T176" id="Seg_3015" s="T175">this-ACC</ta>
            <ta e="T177" id="Seg_3016" s="T176">do.intensively-EP-RECP/COLL-ITER-CVB.SEQ</ta>
            <ta e="T178" id="Seg_3017" s="T177">carry-CVB.SEQ</ta>
            <ta e="T179" id="Seg_3018" s="T178">slope-ABL</ta>
            <ta e="T180" id="Seg_3019" s="T179">upset-ADVZ</ta>
            <ta e="T181" id="Seg_3020" s="T180">throw-PST1-3SG</ta>
            <ta e="T182" id="Seg_3021" s="T181">later</ta>
            <ta e="T183" id="Seg_3022" s="T182">that</ta>
            <ta e="T184" id="Seg_3023" s="T183">old.man.[NOM]</ta>
            <ta e="T185" id="Seg_3024" s="T184">wake.up-EP-PST2-3SG</ta>
            <ta e="T186" id="Seg_3025" s="T185">gorge.[NOM]</ta>
            <ta e="T187" id="Seg_3026" s="T186">inside-3SG-DAT/LOC</ta>
            <ta e="T188" id="Seg_3027" s="T187">lie-PRS.[3SG]</ta>
            <ta e="T189" id="Seg_3028" s="T188">old.man.[NOM]</ta>
            <ta e="T190" id="Seg_3029" s="T189">what.kind.of</ta>
            <ta e="T191" id="Seg_3030" s="T190">NEG</ta>
            <ta e="T192" id="Seg_3031" s="T191">to</ta>
            <ta e="T193" id="Seg_3032" s="T192">move-PTCP.FUT-3SG-DAT/LOC</ta>
            <ta e="T194" id="Seg_3033" s="T193">powerful.[NOM]</ta>
            <ta e="T195" id="Seg_3034" s="T194">tie-PASS/REFL-EP-PTCP.PST.[NOM]</ta>
            <ta e="T196" id="Seg_3035" s="T195">MOD</ta>
            <ta e="T197" id="Seg_3036" s="T196">old.man.[NOM]</ta>
            <ta e="T198" id="Seg_3037" s="T197">call-PRS.[3SG]</ta>
            <ta e="T199" id="Seg_3038" s="T198">ermine-PL.[NOM]</ta>
            <ta e="T200" id="Seg_3039" s="T199">polar.fox-PL.[NOM]</ta>
            <ta e="T201" id="Seg_3040" s="T200">mouse-PL.[NOM]</ta>
            <ta e="T202" id="Seg_3041" s="T201">string-1SG-ACC</ta>
            <ta e="T203" id="Seg_3042" s="T202">cut-CVB.SIM</ta>
            <ta e="T204" id="Seg_3043" s="T203">gnaw-IMP.2PL</ta>
            <ta e="T205" id="Seg_3044" s="T204">say-PRS.[3SG]</ta>
            <ta e="T206" id="Seg_3045" s="T205">that-PL-EP-2SG.[NOM]</ta>
            <ta e="T207" id="Seg_3046" s="T206">come-CVB.SEQ-3PL</ta>
            <ta e="T208" id="Seg_3047" s="T207">string-3SG-ACC</ta>
            <ta e="T209" id="Seg_3048" s="T208">cut-CVB.SIM</ta>
            <ta e="T210" id="Seg_3049" s="T209">gnaw-PST2-3PL</ta>
            <ta e="T211" id="Seg_3050" s="T210">old.man.[NOM]</ta>
            <ta e="T212" id="Seg_3051" s="T211">stand.up-CVB.SIM</ta>
            <ta e="T213" id="Seg_3052" s="T212">jump-PST1-3SG</ta>
            <ta e="T214" id="Seg_3053" s="T213">so</ta>
            <ta e="T215" id="Seg_3054" s="T214">make-CVB.SEQ</ta>
            <ta e="T216" id="Seg_3055" s="T215">this</ta>
            <ta e="T217" id="Seg_3056" s="T216">string-3SG-ACC</ta>
            <ta e="T218" id="Seg_3057" s="T217">gnaw-PTCP.HAB-PL-DAT/LOC</ta>
            <ta e="T219" id="Seg_3058" s="T218">salary-3SG-ACC</ta>
            <ta e="T220" id="Seg_3059" s="T219">one</ta>
            <ta e="T221" id="Seg_3060" s="T220">wild</ta>
            <ta e="T222" id="Seg_3061" s="T221">female.reindeer-3SG-ACC</ta>
            <ta e="T223" id="Seg_3062" s="T222">kill-CVB.SEQ</ta>
            <ta e="T224" id="Seg_3063" s="T223">give-PST1-3SG</ta>
            <ta e="T225" id="Seg_3064" s="T224">this</ta>
            <ta e="T226" id="Seg_3065" s="T225">old.man-EP-2SG.[NOM]</ta>
            <ta e="T227" id="Seg_3066" s="T226">campfire-3SG-DAT/LOC</ta>
            <ta e="T228" id="Seg_3067" s="T227">walk-CVB.SEQ</ta>
            <ta e="T229" id="Seg_3068" s="T228">come-PST1-3SG</ta>
            <ta e="T230" id="Seg_3069" s="T229">campfire-3SG-GEN</ta>
            <ta e="T231" id="Seg_3070" s="T230">ash-3SG-ACC</ta>
            <ta e="T232" id="Seg_3071" s="T231">intact-SIM</ta>
            <ta e="T233" id="Seg_3072" s="T232">trousers-3SG-DAT/LOC</ta>
            <ta e="T234" id="Seg_3073" s="T233">strew-CVB.SEQ</ta>
            <ta e="T235" id="Seg_3074" s="T234">go-CVB.SEQ</ta>
            <ta e="T236" id="Seg_3075" s="T235">walk-CVB.SEQ</ta>
            <ta e="T237" id="Seg_3076" s="T236">stay-PST1-3SG</ta>
            <ta e="T238" id="Seg_3077" s="T237">well</ta>
            <ta e="T239" id="Seg_3078" s="T238">this</ta>
            <ta e="T240" id="Seg_3079" s="T239">go-CVB.SEQ</ta>
            <ta e="T241" id="Seg_3080" s="T240">go-PST1-3SG</ta>
            <ta e="T242" id="Seg_3081" s="T241">go-CVB.SEQ</ta>
            <ta e="T243" id="Seg_3082" s="T242">go-CVB.SEQ-go-CVB.SEQ</ta>
            <ta e="T244" id="Seg_3083" s="T243">only</ta>
            <ta e="T245" id="Seg_3084" s="T244">one</ta>
            <ta e="T246" id="Seg_3085" s="T245">house-DAT/LOC</ta>
            <ta e="T247" id="Seg_3086" s="T246">reach-PST1-3SG</ta>
            <ta e="T248" id="Seg_3087" s="T247">this</ta>
            <ta e="T249" id="Seg_3088" s="T248">house-DAT/LOC</ta>
            <ta e="T250" id="Seg_3089" s="T249">come-CVB.SEQ</ta>
            <ta e="T251" id="Seg_3090" s="T250">listen-CVB.SEQ</ta>
            <ta e="T252" id="Seg_3091" s="T251">try-PST2-3SG</ta>
            <ta e="T253" id="Seg_3092" s="T252">fox.[NOM]</ta>
            <ta e="T254" id="Seg_3093" s="T253">shamanize-CVB.SEQ</ta>
            <ta e="T255" id="Seg_3094" s="T254">be-PRS.[3SG]</ta>
            <ta e="T256" id="Seg_3095" s="T255">house.[NOM]</ta>
            <ta e="T257" id="Seg_3096" s="T256">inside-3SG-DAT/LOC</ta>
            <ta e="T258" id="Seg_3097" s="T257">go.in-PST2-3SG</ta>
            <ta e="T259" id="Seg_3098" s="T258">fox.[NOM]</ta>
            <ta e="T260" id="Seg_3099" s="T259">complete-SIM</ta>
            <ta e="T261" id="Seg_3100" s="T260">gather.oneself-REFL-CVB.SEQ</ta>
            <ta e="T262" id="Seg_3101" s="T261">after</ta>
            <ta e="T263" id="Seg_3102" s="T262">shamanize-CVB.SIM</ta>
            <ta e="T264" id="Seg_3103" s="T263">lie-PRS-3PL</ta>
            <ta e="T265" id="Seg_3104" s="T264">shaman-PROPR-3PL</ta>
            <ta e="T266" id="Seg_3105" s="T265">old.man.[NOM]</ta>
            <ta e="T267" id="Seg_3106" s="T266">go.in-CVB.SEQ</ta>
            <ta e="T268" id="Seg_3107" s="T267">sit.down-PST1-3SG</ta>
            <ta e="T269" id="Seg_3108" s="T268">this</ta>
            <ta e="T270" id="Seg_3109" s="T269">sit-TEMP-3SG</ta>
            <ta e="T271" id="Seg_3110" s="T270">fox-EP-2SG.[NOM]</ta>
            <ta e="T272" id="Seg_3111" s="T271">come.back-PST1-3SG</ta>
            <ta e="T273" id="Seg_3112" s="T272">shamanize-CVB.SEQ</ta>
            <ta e="T274" id="Seg_3113" s="T273">go-CVB.ANT</ta>
            <ta e="T275" id="Seg_3114" s="T274">this</ta>
            <ta e="T276" id="Seg_3115" s="T275">old.man-EP-2SG.[NOM]</ta>
            <ta e="T277" id="Seg_3116" s="T276">suddenly</ta>
            <ta e="T278" id="Seg_3117" s="T277">drum.of.the.shaman.[NOM]</ta>
            <ta e="T279" id="Seg_3118" s="T278">beat-PST1-3SG</ta>
            <ta e="T280" id="Seg_3119" s="T279">this</ta>
            <ta e="T281" id="Seg_3120" s="T280">drum.of.the.shaman-3SG-ACC</ta>
            <ta e="T282" id="Seg_3121" s="T281">beat-CVB.SIM-beat-CVB.SIM</ta>
            <ta e="T283" id="Seg_3122" s="T282">well</ta>
            <ta e="T284" id="Seg_3123" s="T283">jump-PST1-3SG</ta>
            <ta e="T285" id="Seg_3124" s="T284">well</ta>
            <ta e="T286" id="Seg_3125" s="T285">this-DAT/LOC</ta>
            <ta e="T287" id="Seg_3126" s="T286">ash-3SG.[NOM]</ta>
            <ta e="T288" id="Seg_3127" s="T287">well</ta>
            <ta e="T289" id="Seg_3128" s="T288">rise-ITER-PST1-3SG</ta>
            <ta e="T290" id="Seg_3129" s="T289">this</ta>
            <ta e="T291" id="Seg_3130" s="T290">trousers-3SG-ABL</ta>
            <ta e="T292" id="Seg_3131" s="T291">well</ta>
            <ta e="T293" id="Seg_3132" s="T292">this-ABL</ta>
            <ta e="T294" id="Seg_3133" s="T293">this</ta>
            <ta e="T295" id="Seg_3134" s="T294">fox-PL-EP-2SG.[NOM]</ta>
            <ta e="T296" id="Seg_3135" s="T295">intact-SIM</ta>
            <ta e="T297" id="Seg_3136" s="T296">well</ta>
            <ta e="T298" id="Seg_3137" s="T297">laugh-EP-RECP/COLL-PST1-3PL</ta>
            <ta e="T299" id="Seg_3138" s="T298">suddenly</ta>
            <ta e="T300" id="Seg_3139" s="T299">there</ta>
            <ta e="T301" id="Seg_3140" s="T300">corner-DAT/LOC</ta>
            <ta e="T302" id="Seg_3141" s="T301">fox.[NOM]</ta>
            <ta e="T303" id="Seg_3142" s="T302">laugh-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T304" id="Seg_3143" s="T303">be.heard-PST1-3SG</ta>
            <ta e="T305" id="Seg_3144" s="T304">old.man.[NOM]</ta>
            <ta e="T306" id="Seg_3145" s="T305">hear-PST1-3SG</ta>
            <ta e="T307" id="Seg_3146" s="T306">drum.of.the.shaman-3SG-ACC</ta>
            <ta e="T308" id="Seg_3147" s="T307">fall-CVB.SEQ</ta>
            <ta e="T309" id="Seg_3148" s="T308">throw-PST1-3SG</ta>
            <ta e="T310" id="Seg_3149" s="T309">old.man.[NOM]</ta>
            <ta e="T311" id="Seg_3150" s="T310">this</ta>
            <ta e="T312" id="Seg_3151" s="T311">fox-ACC</ta>
            <ta e="T313" id="Seg_3152" s="T312">catch-CVB.SEQ</ta>
            <ta e="T314" id="Seg_3153" s="T313">take-PST1-3SG</ta>
            <ta e="T315" id="Seg_3154" s="T314">fox-3SG-ACC</ta>
            <ta e="T316" id="Seg_3155" s="T315">fire-DAT/LOC</ta>
            <ta e="T317" id="Seg_3156" s="T316">fry-CVB.SIM-fry-CVB.SIM</ta>
            <ta e="T318" id="Seg_3157" s="T317">well</ta>
            <ta e="T319" id="Seg_3158" s="T318">lash-PST1-3SG</ta>
            <ta e="T320" id="Seg_3159" s="T319">lash-CVB.SEQ-lash-CVB.SEQ</ta>
            <ta e="T321" id="Seg_3160" s="T320">kill-CVB.SEQ</ta>
            <ta e="T322" id="Seg_3161" s="T321">throw-PST1-3SG</ta>
            <ta e="T323" id="Seg_3162" s="T322">well</ta>
            <ta e="T324" id="Seg_3163" s="T323">that</ta>
            <ta e="T325" id="Seg_3164" s="T324">fox-EP-2SG.[NOM]</ta>
            <ta e="T326" id="Seg_3165" s="T325">first</ta>
            <ta e="T327" id="Seg_3166" s="T326">his-GEN</ta>
            <ta e="T328" id="Seg_3167" s="T327">child-PL-3SG-ACC</ta>
            <ta e="T329" id="Seg_3168" s="T328">eat-HAB.[3SG]</ta>
            <ta e="T330" id="Seg_3169" s="T329">that.EMPH</ta>
            <ta e="T331" id="Seg_3170" s="T330">fox-EP-2SG.[NOM]</ta>
            <ta e="T332" id="Seg_3171" s="T331">cradle-DAT/LOC</ta>
            <ta e="T333" id="Seg_3172" s="T332">tie-CVB.SEQ</ta>
            <ta e="T334" id="Seg_3173" s="T333">go-CVB.SEQ</ta>
            <ta e="T335" id="Seg_3174" s="T334">upset-ADVZ</ta>
            <ta e="T336" id="Seg_3175" s="T335">throw-PST2-3SG</ta>
            <ta e="T337" id="Seg_3176" s="T336">so</ta>
            <ta e="T338" id="Seg_3177" s="T337">make-CVB.SEQ</ta>
            <ta e="T339" id="Seg_3178" s="T338">after</ta>
            <ta e="T340" id="Seg_3179" s="T339">well</ta>
            <ta e="T341" id="Seg_3180" s="T340">that</ta>
            <ta e="T342" id="Seg_3181" s="T341">old.man-3SG-GEN</ta>
            <ta e="T343" id="Seg_3182" s="T342">house-3SG-DAT/LOC</ta>
            <ta e="T344" id="Seg_3183" s="T343">go-CVB.SEQ</ta>
            <ta e="T345" id="Seg_3184" s="T344">live-PST2-3SG</ta>
            <ta e="T346" id="Seg_3185" s="T345">well</ta>
            <ta e="T347" id="Seg_3186" s="T346">that</ta>
            <ta e="T348" id="Seg_3187" s="T347">end-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gd">
            <ta e="T1" id="Seg_3188" s="T0">doch</ta>
            <ta e="T2" id="Seg_3189" s="T1">eins</ta>
            <ta e="T3" id="Seg_3190" s="T2">alter.Mann.[NOM]</ta>
            <ta e="T4" id="Seg_3191" s="T3">leben-PST2-3SG</ta>
            <ta e="T5" id="Seg_3192" s="T4">Ukukuut_Tschukukuut</ta>
            <ta e="T6" id="Seg_3193" s="T5">Name-3SG.[NOM]</ta>
            <ta e="T7" id="Seg_3194" s="T6">dieses-3SG-2SG.[NOM]</ta>
            <ta e="T8" id="Seg_3195" s="T7">viel</ta>
            <ta e="T9" id="Seg_3196" s="T8">sehr</ta>
            <ta e="T10" id="Seg_3197" s="T9">Kind-3SG.[NOM]</ta>
            <ta e="T11" id="Seg_3198" s="T10">doch</ta>
            <ta e="T12" id="Seg_3199" s="T11">dieses</ta>
            <ta e="T13" id="Seg_3200" s="T12">dieses-DAT/LOC</ta>
            <ta e="T14" id="Seg_3201" s="T13">Fuchs.[NOM]</ta>
            <ta e="T15" id="Seg_3202" s="T14">kommen-PST2-3SG</ta>
            <ta e="T16" id="Seg_3203" s="T15">alter.Mann.[NOM]</ta>
            <ta e="T17" id="Seg_3204" s="T16">eins</ta>
            <ta e="T18" id="Seg_3205" s="T17">Kind-2SG-ACC</ta>
            <ta e="T19" id="Seg_3206" s="T18">holen.[IMP.2SG]</ta>
            <ta e="T20" id="Seg_3207" s="T19">sagen-PRS.[3SG]</ta>
            <ta e="T21" id="Seg_3208" s="T20">essen-PTCP.FUT-1SG-ACC</ta>
            <ta e="T22" id="Seg_3209" s="T21">dieses-DAT/LOC</ta>
            <ta e="T23" id="Seg_3210" s="T22">dieses</ta>
            <ta e="T24" id="Seg_3211" s="T23">alter.Mann.[NOM]</ta>
            <ta e="T25" id="Seg_3212" s="T24">Kind-3SG-ACC</ta>
            <ta e="T26" id="Seg_3213" s="T25">geben-PST2-3SG</ta>
            <ta e="T27" id="Seg_3214" s="T26">essen-IMP.3SG</ta>
            <ta e="T28" id="Seg_3215" s="T27">doch</ta>
            <ta e="T29" id="Seg_3216" s="T28">jenes.[NOM]</ta>
            <ta e="T30" id="Seg_3217" s="T29">gehen-PST2-3SG</ta>
            <ta e="T31" id="Seg_3218" s="T30">wie.viel</ta>
            <ta e="T32" id="Seg_3219" s="T31">INDEF</ta>
            <ta e="T33" id="Seg_3220" s="T32">übernachten-CVB.SEQ</ta>
            <ta e="T34" id="Seg_3221" s="T33">nachdem</ta>
            <ta e="T35" id="Seg_3222" s="T34">wieder</ta>
            <ta e="T36" id="Seg_3223" s="T35">kommen-PST2-3SG</ta>
            <ta e="T37" id="Seg_3224" s="T36">alter.Mann.[NOM]</ta>
            <ta e="T38" id="Seg_3225" s="T37">wieder</ta>
            <ta e="T39" id="Seg_3226" s="T38">Kind-PART</ta>
            <ta e="T40" id="Seg_3227" s="T39">holen.[IMP.2SG]</ta>
            <ta e="T41" id="Seg_3228" s="T40">sagen-PRS.[3SG]</ta>
            <ta e="T42" id="Seg_3229" s="T41">essen-PTCP.FUT-1SG-ACC</ta>
            <ta e="T43" id="Seg_3230" s="T42">jenes-DAT/LOC</ta>
            <ta e="T44" id="Seg_3231" s="T43">alter.Mann.[NOM]</ta>
            <ta e="T45" id="Seg_3232" s="T44">Kind-3SG-ACC</ta>
            <ta e="T46" id="Seg_3233" s="T45">wieder</ta>
            <ta e="T47" id="Seg_3234" s="T46">geben-PST2-3SG</ta>
            <ta e="T48" id="Seg_3235" s="T47">doch</ta>
            <ta e="T49" id="Seg_3236" s="T48">jenes</ta>
            <ta e="T50" id="Seg_3237" s="T49">Kind-3SG-ACC</ta>
            <ta e="T51" id="Seg_3238" s="T50">tragen-PST2-3SG</ta>
            <ta e="T52" id="Seg_3239" s="T51">essen-CVB.PURP</ta>
            <ta e="T53" id="Seg_3240" s="T52">dann</ta>
            <ta e="T54" id="Seg_3241" s="T53">wie.viel</ta>
            <ta e="T55" id="Seg_3242" s="T54">INDEF</ta>
            <ta e="T56" id="Seg_3243" s="T55">Tag.[NOM]</ta>
            <ta e="T57" id="Seg_3244" s="T56">sein-CVB.SEQ</ta>
            <ta e="T58" id="Seg_3245" s="T57">nachdem</ta>
            <ta e="T59" id="Seg_3246" s="T58">wieder</ta>
            <ta e="T60" id="Seg_3247" s="T59">kommen-PST2-3SG</ta>
            <ta e="T61" id="Seg_3248" s="T60">jenes.[NOM]</ta>
            <ta e="T62" id="Seg_3249" s="T61">kommen-CVB.SEQ</ta>
            <ta e="T63" id="Seg_3250" s="T62">wieder</ta>
            <ta e="T64" id="Seg_3251" s="T63">sagen-PRS.[3SG]</ta>
            <ta e="T65" id="Seg_3252" s="T64">Kind-PART</ta>
            <ta e="T66" id="Seg_3253" s="T65">holen.[IMP.2SG]</ta>
            <ta e="T67" id="Seg_3254" s="T66">sagen-PRS.[3SG]</ta>
            <ta e="T68" id="Seg_3255" s="T67">essen-PTCP.FUT-1SG-ACC</ta>
            <ta e="T69" id="Seg_3256" s="T68">Kind-3SG.[NOM]</ta>
            <ta e="T70" id="Seg_3257" s="T69">letzter-3SG.[NOM]</ta>
            <ta e="T71" id="Seg_3258" s="T70">sein-PST1-3SG</ta>
            <ta e="T72" id="Seg_3259" s="T71">doch</ta>
            <ta e="T73" id="Seg_3260" s="T72">jenes.[NOM]</ta>
            <ta e="T74" id="Seg_3261" s="T73">gehen-PST2-3SG</ta>
            <ta e="T75" id="Seg_3262" s="T74">und</ta>
            <ta e="T76" id="Seg_3263" s="T75">Fuchs-EP-2SG.[NOM]</ta>
            <ta e="T77" id="Seg_3264" s="T76">verschwinden-PST2-3SG</ta>
            <ta e="T78" id="Seg_3265" s="T77">dieses</ta>
            <ta e="T79" id="Seg_3266" s="T78">alter.Mann-EP-2SG.[NOM]</ta>
            <ta e="T80" id="Seg_3267" s="T79">leben-CVB.SIM</ta>
            <ta e="T81" id="Seg_3268" s="T80">können-CVB.SEQ</ta>
            <ta e="T82" id="Seg_3269" s="T81">gehen-CVB.SEQ</ta>
            <ta e="T83" id="Seg_3270" s="T82">gehen-CVB.SEQ</ta>
            <ta e="T84" id="Seg_3271" s="T83">bleiben-PST1-3SG</ta>
            <ta e="T85" id="Seg_3272" s="T84">doch</ta>
            <ta e="T86" id="Seg_3273" s="T85">dieses</ta>
            <ta e="T87" id="Seg_3274" s="T86">gehen-CVB.SEQ</ta>
            <ta e="T88" id="Seg_3275" s="T87">gehen-CVB.SEQ-gehen-CVB.SEQ</ta>
            <ta e="T89" id="Seg_3276" s="T88">gehen-CVB.SEQ</ta>
            <ta e="T90" id="Seg_3277" s="T89">eins</ta>
            <ta e="T91" id="Seg_3278" s="T90">wild</ta>
            <ta e="T92" id="Seg_3279" s="T91">Rentierkuh-3SG-ACC</ta>
            <ta e="T93" id="Seg_3280" s="T92">töten-CVB.SEQ</ta>
            <ta e="T94" id="Seg_3281" s="T93">gehen-CVB.SEQ</ta>
            <ta e="T95" id="Seg_3282" s="T94">braten-EP-MED-CVB.SIM</ta>
            <ta e="T96" id="Seg_3283" s="T95">sich.setzen-PST1-3SG</ta>
            <ta e="T97" id="Seg_3284" s="T96">dieses</ta>
            <ta e="T98" id="Seg_3285" s="T97">sitzen-TEMP-3SG</ta>
            <ta e="T99" id="Seg_3286" s="T98">Fuchs.[NOM]</ta>
            <ta e="T100" id="Seg_3287" s="T99">kommen-PST1-3SG</ta>
            <ta e="T101" id="Seg_3288" s="T100">nun</ta>
            <ta e="T102" id="Seg_3289" s="T101">alter.Mann.[NOM]</ta>
            <ta e="T103" id="Seg_3290" s="T102">1PL.[NOM]</ta>
            <ta e="T104" id="Seg_3291" s="T103">Kind.[NOM]</ta>
            <ta e="T105" id="Seg_3292" s="T104">sein-CVB.SIM</ta>
            <ta e="T106" id="Seg_3293" s="T105">spielen-IMP.1DU</ta>
            <ta e="T107" id="Seg_3294" s="T106">sagen-PST1-3SG</ta>
            <ta e="T108" id="Seg_3295" s="T107">doch</ta>
            <ta e="T109" id="Seg_3296" s="T108">in.Ordnung</ta>
            <ta e="T110" id="Seg_3297" s="T109">Kind.[NOM]</ta>
            <ta e="T111" id="Seg_3298" s="T110">sein-CVB.SIM</ta>
            <ta e="T112" id="Seg_3299" s="T111">spielen-IMP.1DU</ta>
            <ta e="T113" id="Seg_3300" s="T112">dieses</ta>
            <ta e="T114" id="Seg_3301" s="T113">alter.Mann-EP-2SG.[NOM]</ta>
            <ta e="T115" id="Seg_3302" s="T114">Wiege.[NOM]</ta>
            <ta e="T116" id="Seg_3303" s="T115">machen-PST1-3SG</ta>
            <ta e="T117" id="Seg_3304" s="T116">jenes</ta>
            <ta e="T118" id="Seg_3305" s="T117">Rentierkuh-PL-3SG-GEN</ta>
            <ta e="T119" id="Seg_3306" s="T118">Haut-3SG-INSTR</ta>
            <ta e="T120" id="Seg_3307" s="T119">Wiege.[NOM]</ta>
            <ta e="T121" id="Seg_3308" s="T120">Riemen-3SG.[NOM]</ta>
            <ta e="T122" id="Seg_3309" s="T121">machen-EP-MED-PST1-3PL</ta>
            <ta e="T123" id="Seg_3310" s="T122">alter.Mann-EP-2SG.[NOM]</ta>
            <ta e="T124" id="Seg_3311" s="T123">sagen-PST2.[3SG]</ta>
            <ta e="T125" id="Seg_3312" s="T124">doch</ta>
            <ta e="T126" id="Seg_3313" s="T125">nun</ta>
            <ta e="T127" id="Seg_3314" s="T126">2SG.[NOM]</ta>
            <ta e="T128" id="Seg_3315" s="T127">schwingen-EP-REFL.[IMP.2SG]</ta>
            <ta e="T129" id="Seg_3316" s="T128">Kind.[NOM]</ta>
            <ta e="T130" id="Seg_3317" s="T129">sein-CVB.SEQ</ta>
            <ta e="T131" id="Seg_3318" s="T130">sagen-PST1-3SG</ta>
            <ta e="T132" id="Seg_3319" s="T131">Fuchs.[NOM]</ta>
            <ta e="T133" id="Seg_3320" s="T132">dieses-DAT/LOC</ta>
            <ta e="T134" id="Seg_3321" s="T133">sich.hinlegen-CVB.SEQ</ta>
            <ta e="T135" id="Seg_3322" s="T134">versuchen-PST1-3SG</ta>
            <ta e="T136" id="Seg_3323" s="T135">hey</ta>
            <ta e="T137" id="Seg_3324" s="T136">Freund.[NOM]</ta>
            <ta e="T138" id="Seg_3325" s="T137">mein</ta>
            <ta e="T139" id="Seg_3326" s="T138">Schwanz-EP-1SG.[NOM]</ta>
            <ta e="T140" id="Seg_3327" s="T139">weh.tun-PRS.[3SG]</ta>
            <ta e="T141" id="Seg_3328" s="T140">sich.hinlegen-CAUS-FUT.[3SG]</ta>
            <ta e="T142" id="Seg_3329" s="T141">NEG</ta>
            <ta e="T143" id="Seg_3330" s="T142">sagen-PST1-3SG</ta>
            <ta e="T144" id="Seg_3331" s="T143">doch</ta>
            <ta e="T145" id="Seg_3332" s="T144">Mensch.[NOM]</ta>
            <ta e="T146" id="Seg_3333" s="T145">Mensch-2SG-INSTR</ta>
            <ta e="T147" id="Seg_3334" s="T146">2SG.[NOM]</ta>
            <ta e="T148" id="Seg_3335" s="T147">sich.hinlegen-CVB.SEQ</ta>
            <ta e="T149" id="Seg_3336" s="T148">Kind.[NOM]</ta>
            <ta e="T150" id="Seg_3337" s="T149">sein.[IMP.2SG]</ta>
            <ta e="T151" id="Seg_3338" s="T150">sagen-PST1-3SG</ta>
            <ta e="T152" id="Seg_3339" s="T151">Fuchs.[NOM]</ta>
            <ta e="T153" id="Seg_3340" s="T152">alter.Mann-ACC</ta>
            <ta e="T154" id="Seg_3341" s="T153">alter.Mann.[NOM]</ta>
            <ta e="T155" id="Seg_3342" s="T154">doch</ta>
            <ta e="T156" id="Seg_3343" s="T155">sich.hinlegen-PST1-3SG</ta>
            <ta e="T157" id="Seg_3344" s="T156">Fuchs.[NOM]</ta>
            <ta e="T158" id="Seg_3345" s="T157">doch</ta>
            <ta e="T159" id="Seg_3346" s="T158">dieses-ACC</ta>
            <ta e="T160" id="Seg_3347" s="T159">anbinden-CVB.SEQ</ta>
            <ta e="T161" id="Seg_3348" s="T160">werfen-PST1-3SG</ta>
            <ta e="T162" id="Seg_3349" s="T161">Fuchs.[NOM]</ta>
            <ta e="T163" id="Seg_3350" s="T162">dieses-ACC</ta>
            <ta e="T164" id="Seg_3351" s="T163">schwingen-CVB.SIM-schwingen-CVB.SIM</ta>
            <ta e="T165" id="Seg_3352" s="T164">wildes.Rentier.[NOM]</ta>
            <ta e="T166" id="Seg_3353" s="T165">Fleisch-3SG-INSTR</ta>
            <ta e="T167" id="Seg_3354" s="T166">kauen-CVB.SEQ</ta>
            <ta e="T168" id="Seg_3355" s="T167">essen-EP-CAUS-CVB.SIM</ta>
            <ta e="T169" id="Seg_3356" s="T168">sitzen-PST1-3SG</ta>
            <ta e="T170" id="Seg_3357" s="T169">dieses</ta>
            <ta e="T171" id="Seg_3358" s="T170">alter.Mann-EP-2SG.[NOM]</ta>
            <ta e="T172" id="Seg_3359" s="T171">nur</ta>
            <ta e="T173" id="Seg_3360" s="T172">schlafen-CVB.SEQ</ta>
            <ta e="T174" id="Seg_3361" s="T173">bleiben-PST1-3SG</ta>
            <ta e="T175" id="Seg_3362" s="T174">Fuchs.[NOM]</ta>
            <ta e="T176" id="Seg_3363" s="T175">dieses-ACC</ta>
            <ta e="T177" id="Seg_3364" s="T176">intensiv.machen-EP-RECP/COLL-ITER-CVB.SEQ</ta>
            <ta e="T178" id="Seg_3365" s="T177">tragen-CVB.SEQ</ta>
            <ta e="T179" id="Seg_3366" s="T178">Abhang-ABL</ta>
            <ta e="T180" id="Seg_3367" s="T179">umwerfen-ADVZ</ta>
            <ta e="T181" id="Seg_3368" s="T180">werfen-PST1-3SG</ta>
            <ta e="T182" id="Seg_3369" s="T181">später</ta>
            <ta e="T183" id="Seg_3370" s="T182">jenes</ta>
            <ta e="T184" id="Seg_3371" s="T183">alter.Mann.[NOM]</ta>
            <ta e="T185" id="Seg_3372" s="T184">aufwachen-EP-PST2-3SG</ta>
            <ta e="T186" id="Seg_3373" s="T185">Schlucht.[NOM]</ta>
            <ta e="T187" id="Seg_3374" s="T186">Inneres-3SG-DAT/LOC</ta>
            <ta e="T188" id="Seg_3375" s="T187">liegen-PRS.[3SG]</ta>
            <ta e="T189" id="Seg_3376" s="T188">alter.Mann.[NOM]</ta>
            <ta e="T190" id="Seg_3377" s="T189">was.für.ein</ta>
            <ta e="T191" id="Seg_3378" s="T190">NEG</ta>
            <ta e="T192" id="Seg_3379" s="T191">zu</ta>
            <ta e="T193" id="Seg_3380" s="T192">sich.bewegen-PTCP.FUT-3SG-DAT/LOC</ta>
            <ta e="T194" id="Seg_3381" s="T193">powerful.[NOM]</ta>
            <ta e="T195" id="Seg_3382" s="T194">anbinden-PASS/REFL-EP-PTCP.PST.[NOM]</ta>
            <ta e="T196" id="Seg_3383" s="T195">MOD</ta>
            <ta e="T197" id="Seg_3384" s="T196">alter.Mann.[NOM]</ta>
            <ta e="T198" id="Seg_3385" s="T197">rufen-PRS.[3SG]</ta>
            <ta e="T199" id="Seg_3386" s="T198">Hermelin-PL.[NOM]</ta>
            <ta e="T200" id="Seg_3387" s="T199">Polarfuchs-PL.[NOM]</ta>
            <ta e="T201" id="Seg_3388" s="T200">Maus-PL.[NOM]</ta>
            <ta e="T202" id="Seg_3389" s="T201">Schnur-1SG-ACC</ta>
            <ta e="T203" id="Seg_3390" s="T202">schneiden-CVB.SIM</ta>
            <ta e="T204" id="Seg_3391" s="T203">nagen-IMP.2PL</ta>
            <ta e="T205" id="Seg_3392" s="T204">sagen-PRS.[3SG]</ta>
            <ta e="T206" id="Seg_3393" s="T205">jenes-PL-EP-2SG.[NOM]</ta>
            <ta e="T207" id="Seg_3394" s="T206">kommen-CVB.SEQ-3PL</ta>
            <ta e="T208" id="Seg_3395" s="T207">Schnur-3SG-ACC</ta>
            <ta e="T209" id="Seg_3396" s="T208">schneiden-CVB.SIM</ta>
            <ta e="T210" id="Seg_3397" s="T209">nagen-PST2-3PL</ta>
            <ta e="T211" id="Seg_3398" s="T210">alter.Mann.[NOM]</ta>
            <ta e="T212" id="Seg_3399" s="T211">aufstehen-CVB.SIM</ta>
            <ta e="T213" id="Seg_3400" s="T212">springen-PST1-3SG</ta>
            <ta e="T214" id="Seg_3401" s="T213">so</ta>
            <ta e="T215" id="Seg_3402" s="T214">machen-CVB.SEQ</ta>
            <ta e="T216" id="Seg_3403" s="T215">dieses</ta>
            <ta e="T217" id="Seg_3404" s="T216">Schnur-3SG-ACC</ta>
            <ta e="T218" id="Seg_3405" s="T217">nagen-PTCP.HAB-PL-DAT/LOC</ta>
            <ta e="T219" id="Seg_3406" s="T218">Lohn-3SG-ACC</ta>
            <ta e="T220" id="Seg_3407" s="T219">eins</ta>
            <ta e="T221" id="Seg_3408" s="T220">wild</ta>
            <ta e="T222" id="Seg_3409" s="T221">Rentierkuh-3SG-ACC</ta>
            <ta e="T223" id="Seg_3410" s="T222">töten-CVB.SEQ</ta>
            <ta e="T224" id="Seg_3411" s="T223">geben-PST1-3SG</ta>
            <ta e="T225" id="Seg_3412" s="T224">dieses</ta>
            <ta e="T226" id="Seg_3413" s="T225">alter.Mann-EP-2SG.[NOM]</ta>
            <ta e="T227" id="Seg_3414" s="T226">Lagerfeuer-3SG-DAT/LOC</ta>
            <ta e="T228" id="Seg_3415" s="T227">gehen-CVB.SEQ</ta>
            <ta e="T229" id="Seg_3416" s="T228">kommen-PST1-3SG</ta>
            <ta e="T230" id="Seg_3417" s="T229">Lagerfeuer-3SG-GEN</ta>
            <ta e="T231" id="Seg_3418" s="T230">Asche-3SG-ACC</ta>
            <ta e="T232" id="Seg_3419" s="T231">ganz-SIM</ta>
            <ta e="T233" id="Seg_3420" s="T232">Hose-3SG-DAT/LOC</ta>
            <ta e="T234" id="Seg_3421" s="T233">streuen-CVB.SEQ</ta>
            <ta e="T235" id="Seg_3422" s="T234">gehen-CVB.SEQ</ta>
            <ta e="T236" id="Seg_3423" s="T235">gehen-CVB.SEQ</ta>
            <ta e="T237" id="Seg_3424" s="T236">bleiben-PST1-3SG</ta>
            <ta e="T238" id="Seg_3425" s="T237">doch</ta>
            <ta e="T239" id="Seg_3426" s="T238">dieses</ta>
            <ta e="T240" id="Seg_3427" s="T239">gehen-CVB.SEQ</ta>
            <ta e="T241" id="Seg_3428" s="T240">gehen-PST1-3SG</ta>
            <ta e="T242" id="Seg_3429" s="T241">gehen-CVB.SEQ</ta>
            <ta e="T243" id="Seg_3430" s="T242">gehen-CVB.SEQ-gehen-CVB.SEQ</ta>
            <ta e="T244" id="Seg_3431" s="T243">nur</ta>
            <ta e="T245" id="Seg_3432" s="T244">eins</ta>
            <ta e="T246" id="Seg_3433" s="T245">Haus-DAT/LOC</ta>
            <ta e="T247" id="Seg_3434" s="T246">ankommen-PST1-3SG</ta>
            <ta e="T248" id="Seg_3435" s="T247">dieses</ta>
            <ta e="T249" id="Seg_3436" s="T248">Haus-DAT/LOC</ta>
            <ta e="T250" id="Seg_3437" s="T249">kommen-CVB.SEQ</ta>
            <ta e="T251" id="Seg_3438" s="T250">zuhören-CVB.SEQ</ta>
            <ta e="T252" id="Seg_3439" s="T251">versuchen-PST2-3SG</ta>
            <ta e="T253" id="Seg_3440" s="T252">Fuchs.[NOM]</ta>
            <ta e="T254" id="Seg_3441" s="T253">schamanisieren-CVB.SEQ</ta>
            <ta e="T255" id="Seg_3442" s="T254">sein-PRS.[3SG]</ta>
            <ta e="T256" id="Seg_3443" s="T255">Haus.[NOM]</ta>
            <ta e="T257" id="Seg_3444" s="T256">Inneres-3SG-DAT/LOC</ta>
            <ta e="T258" id="Seg_3445" s="T257">hineingehen-PST2-3SG</ta>
            <ta e="T259" id="Seg_3446" s="T258">Fuchs.[NOM]</ta>
            <ta e="T260" id="Seg_3447" s="T259">vollständig-SIM</ta>
            <ta e="T261" id="Seg_3448" s="T260">sich.sammeln-REFL-CVB.SEQ</ta>
            <ta e="T262" id="Seg_3449" s="T261">nachdem</ta>
            <ta e="T263" id="Seg_3450" s="T262">schamanisieren-CVB.SIM</ta>
            <ta e="T264" id="Seg_3451" s="T263">liegen-PRS-3PL</ta>
            <ta e="T265" id="Seg_3452" s="T264">Schamane-PROPR-3PL</ta>
            <ta e="T266" id="Seg_3453" s="T265">alter.Mann.[NOM]</ta>
            <ta e="T267" id="Seg_3454" s="T266">hineingehen-CVB.SEQ</ta>
            <ta e="T268" id="Seg_3455" s="T267">sich.setzen-PST1-3SG</ta>
            <ta e="T269" id="Seg_3456" s="T268">dieses</ta>
            <ta e="T270" id="Seg_3457" s="T269">sitzen-TEMP-3SG</ta>
            <ta e="T271" id="Seg_3458" s="T270">Fuchs-EP-2SG.[NOM]</ta>
            <ta e="T272" id="Seg_3459" s="T271">zurückkommen-PST1-3SG</ta>
            <ta e="T273" id="Seg_3460" s="T272">schamanisieren-CVB.SEQ</ta>
            <ta e="T274" id="Seg_3461" s="T273">gehen-CVB.ANT</ta>
            <ta e="T275" id="Seg_3462" s="T274">dieses</ta>
            <ta e="T276" id="Seg_3463" s="T275">alter.Mann-EP-2SG.[NOM]</ta>
            <ta e="T277" id="Seg_3464" s="T276">plötzlich</ta>
            <ta e="T278" id="Seg_3465" s="T277">Schamanentrommel.[NOM]</ta>
            <ta e="T279" id="Seg_3466" s="T278">schlagen-PST1-3SG</ta>
            <ta e="T280" id="Seg_3467" s="T279">dieses</ta>
            <ta e="T281" id="Seg_3468" s="T280">Schamanentrommel-3SG-ACC</ta>
            <ta e="T282" id="Seg_3469" s="T281">schlagen-CVB.SIM-schlagen-CVB.SIM</ta>
            <ta e="T283" id="Seg_3470" s="T282">doch</ta>
            <ta e="T284" id="Seg_3471" s="T283">springen-PST1-3SG</ta>
            <ta e="T285" id="Seg_3472" s="T284">doch</ta>
            <ta e="T286" id="Seg_3473" s="T285">dieses-DAT/LOC</ta>
            <ta e="T287" id="Seg_3474" s="T286">Asche-3SG.[NOM]</ta>
            <ta e="T288" id="Seg_3475" s="T287">doch</ta>
            <ta e="T289" id="Seg_3476" s="T288">sich.erheben-ITER-PST1-3SG</ta>
            <ta e="T290" id="Seg_3477" s="T289">dieses</ta>
            <ta e="T291" id="Seg_3478" s="T290">Hose-3SG-ABL</ta>
            <ta e="T292" id="Seg_3479" s="T291">doch</ta>
            <ta e="T293" id="Seg_3480" s="T292">dieses-ABL</ta>
            <ta e="T294" id="Seg_3481" s="T293">dieses</ta>
            <ta e="T295" id="Seg_3482" s="T294">Fuchs-PL-EP-2SG.[NOM]</ta>
            <ta e="T296" id="Seg_3483" s="T295">ganz-SIM</ta>
            <ta e="T297" id="Seg_3484" s="T296">doch</ta>
            <ta e="T298" id="Seg_3485" s="T297">lachen-EP-RECP/COLL-PST1-3PL</ta>
            <ta e="T299" id="Seg_3486" s="T298">plötzlich</ta>
            <ta e="T300" id="Seg_3487" s="T299">dort</ta>
            <ta e="T301" id="Seg_3488" s="T300">Ecke-DAT/LOC</ta>
            <ta e="T302" id="Seg_3489" s="T301">Fuchs.[NOM]</ta>
            <ta e="T303" id="Seg_3490" s="T302">lachen-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T304" id="Seg_3491" s="T303">gehört.werden-PST1-3SG</ta>
            <ta e="T305" id="Seg_3492" s="T304">alter.Mann.[NOM]</ta>
            <ta e="T306" id="Seg_3493" s="T305">hören-PST1-3SG</ta>
            <ta e="T307" id="Seg_3494" s="T306">Schamanentrommel-3SG-ACC</ta>
            <ta e="T308" id="Seg_3495" s="T307">fallen-CVB.SEQ</ta>
            <ta e="T309" id="Seg_3496" s="T308">werfen-PST1-3SG</ta>
            <ta e="T310" id="Seg_3497" s="T309">alter.Mann.[NOM]</ta>
            <ta e="T311" id="Seg_3498" s="T310">dieses</ta>
            <ta e="T312" id="Seg_3499" s="T311">Fuchs-ACC</ta>
            <ta e="T313" id="Seg_3500" s="T312">fangen-CVB.SEQ</ta>
            <ta e="T314" id="Seg_3501" s="T313">nehmen-PST1-3SG</ta>
            <ta e="T315" id="Seg_3502" s="T314">Fuchs-3SG-ACC</ta>
            <ta e="T316" id="Seg_3503" s="T315">Feuer-DAT/LOC</ta>
            <ta e="T317" id="Seg_3504" s="T316">braten-CVB.SIM-braten-CVB.SIM</ta>
            <ta e="T318" id="Seg_3505" s="T317">doch</ta>
            <ta e="T319" id="Seg_3506" s="T318">schlagen-PST1-3SG</ta>
            <ta e="T320" id="Seg_3507" s="T319">schlagen-CVB.SEQ-schlagen-CVB.SEQ</ta>
            <ta e="T321" id="Seg_3508" s="T320">töten-CVB.SEQ</ta>
            <ta e="T322" id="Seg_3509" s="T321">werfen-PST1-3SG</ta>
            <ta e="T323" id="Seg_3510" s="T322">doch</ta>
            <ta e="T324" id="Seg_3511" s="T323">jenes</ta>
            <ta e="T325" id="Seg_3512" s="T324">Fuchs-EP-2SG.[NOM]</ta>
            <ta e="T326" id="Seg_3513" s="T325">zuerst</ta>
            <ta e="T327" id="Seg_3514" s="T326">sein-GEN</ta>
            <ta e="T328" id="Seg_3515" s="T327">Kind-PL-3SG-ACC</ta>
            <ta e="T329" id="Seg_3516" s="T328">essen-HAB.[3SG]</ta>
            <ta e="T330" id="Seg_3517" s="T329">jenes.EMPH</ta>
            <ta e="T331" id="Seg_3518" s="T330">Fuchs-EP-2SG.[NOM]</ta>
            <ta e="T332" id="Seg_3519" s="T331">Wiege-DAT/LOC</ta>
            <ta e="T333" id="Seg_3520" s="T332">anbinden-CVB.SEQ</ta>
            <ta e="T334" id="Seg_3521" s="T333">go-CVB.SEQ</ta>
            <ta e="T335" id="Seg_3522" s="T334">umwerfen-ADVZ</ta>
            <ta e="T336" id="Seg_3523" s="T335">werfen-PST2-3SG</ta>
            <ta e="T337" id="Seg_3524" s="T336">so</ta>
            <ta e="T338" id="Seg_3525" s="T337">machen-CVB.SEQ</ta>
            <ta e="T339" id="Seg_3526" s="T338">nachdem</ta>
            <ta e="T340" id="Seg_3527" s="T339">doch</ta>
            <ta e="T341" id="Seg_3528" s="T340">dieses</ta>
            <ta e="T342" id="Seg_3529" s="T341">alter.Mann-3SG-GEN</ta>
            <ta e="T343" id="Seg_3530" s="T342">Haus-3SG-DAT/LOC</ta>
            <ta e="T344" id="Seg_3531" s="T343">gehen-CVB.SEQ</ta>
            <ta e="T345" id="Seg_3532" s="T344">leben-PST2-3SG</ta>
            <ta e="T346" id="Seg_3533" s="T345">doch</ta>
            <ta e="T347" id="Seg_3534" s="T346">dieses</ta>
            <ta e="T348" id="Seg_3535" s="T347">Ende-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_3536" s="T0">вот</ta>
            <ta e="T2" id="Seg_3537" s="T1">один</ta>
            <ta e="T3" id="Seg_3538" s="T2">старик.[NOM]</ta>
            <ta e="T4" id="Seg_3539" s="T3">жить-PST2-3SG</ta>
            <ta e="T5" id="Seg_3540" s="T4">Укукуут_Чукукуут</ta>
            <ta e="T6" id="Seg_3541" s="T5">имя-3SG.[NOM]</ta>
            <ta e="T7" id="Seg_3542" s="T6">этот-3SG-2SG.[NOM]</ta>
            <ta e="T8" id="Seg_3543" s="T7">много</ta>
            <ta e="T9" id="Seg_3544" s="T8">очень</ta>
            <ta e="T10" id="Seg_3545" s="T9">ребенок-3SG.[NOM]</ta>
            <ta e="T11" id="Seg_3546" s="T10">вот</ta>
            <ta e="T12" id="Seg_3547" s="T11">этот</ta>
            <ta e="T13" id="Seg_3548" s="T12">этот-DAT/LOC</ta>
            <ta e="T14" id="Seg_3549" s="T13">лиса.[NOM]</ta>
            <ta e="T15" id="Seg_3550" s="T14">приходить-PST2-3SG</ta>
            <ta e="T16" id="Seg_3551" s="T15">старик.[NOM]</ta>
            <ta e="T17" id="Seg_3552" s="T16">один</ta>
            <ta e="T18" id="Seg_3553" s="T17">ребенок-2SG-ACC</ta>
            <ta e="T19" id="Seg_3554" s="T18">приносить.[IMP.2SG]</ta>
            <ta e="T20" id="Seg_3555" s="T19">говорить-PRS.[3SG]</ta>
            <ta e="T21" id="Seg_3556" s="T20">есть-PTCP.FUT-1SG-ACC</ta>
            <ta e="T22" id="Seg_3557" s="T21">этот-DAT/LOC</ta>
            <ta e="T23" id="Seg_3558" s="T22">этот</ta>
            <ta e="T24" id="Seg_3559" s="T23">старик.[NOM]</ta>
            <ta e="T25" id="Seg_3560" s="T24">ребенок-3SG-ACC</ta>
            <ta e="T26" id="Seg_3561" s="T25">давать-PST2-3SG</ta>
            <ta e="T27" id="Seg_3562" s="T26">есть-IMP.3SG</ta>
            <ta e="T28" id="Seg_3563" s="T27">вот</ta>
            <ta e="T29" id="Seg_3564" s="T28">тот.[NOM]</ta>
            <ta e="T30" id="Seg_3565" s="T29">идти-PST2-3SG</ta>
            <ta e="T31" id="Seg_3566" s="T30">сколько</ta>
            <ta e="T32" id="Seg_3567" s="T31">INDEF</ta>
            <ta e="T33" id="Seg_3568" s="T32">ночевать-CVB.SEQ</ta>
            <ta e="T34" id="Seg_3569" s="T33">после</ta>
            <ta e="T35" id="Seg_3570" s="T34">опять</ta>
            <ta e="T36" id="Seg_3571" s="T35">приходить-PST2-3SG</ta>
            <ta e="T37" id="Seg_3572" s="T36">старик.[NOM]</ta>
            <ta e="T38" id="Seg_3573" s="T37">опять</ta>
            <ta e="T39" id="Seg_3574" s="T38">ребенок-PART</ta>
            <ta e="T40" id="Seg_3575" s="T39">приносить.[IMP.2SG]</ta>
            <ta e="T41" id="Seg_3576" s="T40">говорить-PRS.[3SG]</ta>
            <ta e="T42" id="Seg_3577" s="T41">есть-PTCP.FUT-1SG-ACC</ta>
            <ta e="T43" id="Seg_3578" s="T42">тот-DAT/LOC</ta>
            <ta e="T44" id="Seg_3579" s="T43">старик.[NOM]</ta>
            <ta e="T45" id="Seg_3580" s="T44">ребенок-3SG-ACC</ta>
            <ta e="T46" id="Seg_3581" s="T45">опять</ta>
            <ta e="T47" id="Seg_3582" s="T46">давать-PST2-3SG</ta>
            <ta e="T48" id="Seg_3583" s="T47">вот</ta>
            <ta e="T49" id="Seg_3584" s="T48">тот</ta>
            <ta e="T50" id="Seg_3585" s="T49">ребенок-3SG-ACC</ta>
            <ta e="T51" id="Seg_3586" s="T50">носить-PST2-3SG</ta>
            <ta e="T52" id="Seg_3587" s="T51">есть-CVB.PURP</ta>
            <ta e="T53" id="Seg_3588" s="T52">потом</ta>
            <ta e="T54" id="Seg_3589" s="T53">сколько</ta>
            <ta e="T55" id="Seg_3590" s="T54">INDEF</ta>
            <ta e="T56" id="Seg_3591" s="T55">день.[NOM]</ta>
            <ta e="T57" id="Seg_3592" s="T56">быть-CVB.SEQ</ta>
            <ta e="T58" id="Seg_3593" s="T57">после</ta>
            <ta e="T59" id="Seg_3594" s="T58">опять</ta>
            <ta e="T60" id="Seg_3595" s="T59">приходить-PST2-3SG</ta>
            <ta e="T61" id="Seg_3596" s="T60">тот.[NOM]</ta>
            <ta e="T62" id="Seg_3597" s="T61">приходить-CVB.SEQ</ta>
            <ta e="T63" id="Seg_3598" s="T62">опять</ta>
            <ta e="T64" id="Seg_3599" s="T63">говорить-PRS.[3SG]</ta>
            <ta e="T65" id="Seg_3600" s="T64">ребенок-PART</ta>
            <ta e="T66" id="Seg_3601" s="T65">приносить.[IMP.2SG]</ta>
            <ta e="T67" id="Seg_3602" s="T66">говорить-PRS.[3SG]</ta>
            <ta e="T68" id="Seg_3603" s="T67">есть-PTCP.FUT-1SG-ACC</ta>
            <ta e="T69" id="Seg_3604" s="T68">ребенок-3SG.[NOM]</ta>
            <ta e="T70" id="Seg_3605" s="T69">последний-3SG.[NOM]</ta>
            <ta e="T71" id="Seg_3606" s="T70">быть-PST1-3SG</ta>
            <ta e="T72" id="Seg_3607" s="T71">вот</ta>
            <ta e="T73" id="Seg_3608" s="T72">тот.[NOM]</ta>
            <ta e="T74" id="Seg_3609" s="T73">идти-PST2-3SG</ta>
            <ta e="T75" id="Seg_3610" s="T74">да</ta>
            <ta e="T76" id="Seg_3611" s="T75">лиса-EP-2SG.[NOM]</ta>
            <ta e="T77" id="Seg_3612" s="T76">пропасть-PST2-3SG</ta>
            <ta e="T78" id="Seg_3613" s="T77">этот</ta>
            <ta e="T79" id="Seg_3614" s="T78">старик-EP-2SG.[NOM]</ta>
            <ta e="T80" id="Seg_3615" s="T79">жить-CVB.SIM</ta>
            <ta e="T81" id="Seg_3616" s="T80">мочь-CVB.SEQ</ta>
            <ta e="T82" id="Seg_3617" s="T81">идти-CVB.SEQ</ta>
            <ta e="T83" id="Seg_3618" s="T82">идти-CVB.SEQ</ta>
            <ta e="T84" id="Seg_3619" s="T83">оставаться-PST1-3SG</ta>
            <ta e="T85" id="Seg_3620" s="T84">вот</ta>
            <ta e="T86" id="Seg_3621" s="T85">этот</ta>
            <ta e="T87" id="Seg_3622" s="T86">идти-CVB.SEQ</ta>
            <ta e="T88" id="Seg_3623" s="T87">идти-CVB.SEQ-идти-CVB.SEQ</ta>
            <ta e="T89" id="Seg_3624" s="T88">идти-CVB.SEQ</ta>
            <ta e="T90" id="Seg_3625" s="T89">один</ta>
            <ta e="T91" id="Seg_3626" s="T90">дикий</ta>
            <ta e="T92" id="Seg_3627" s="T91">важенка-3SG-ACC</ta>
            <ta e="T93" id="Seg_3628" s="T92">убить-CVB.SEQ</ta>
            <ta e="T94" id="Seg_3629" s="T93">идти-CVB.SEQ</ta>
            <ta e="T95" id="Seg_3630" s="T94">жарить-EP-MED-CVB.SIM</ta>
            <ta e="T96" id="Seg_3631" s="T95">сесть-PST1-3SG</ta>
            <ta e="T97" id="Seg_3632" s="T96">этот</ta>
            <ta e="T98" id="Seg_3633" s="T97">сидеть-TEMP-3SG</ta>
            <ta e="T99" id="Seg_3634" s="T98">лиса.[NOM]</ta>
            <ta e="T100" id="Seg_3635" s="T99">приходить-PST1-3SG</ta>
            <ta e="T101" id="Seg_3636" s="T100">ну</ta>
            <ta e="T102" id="Seg_3637" s="T101">старик.[NOM]</ta>
            <ta e="T103" id="Seg_3638" s="T102">1PL.[NOM]</ta>
            <ta e="T104" id="Seg_3639" s="T103">ребенок.[NOM]</ta>
            <ta e="T105" id="Seg_3640" s="T104">быть-CVB.SIM</ta>
            <ta e="T106" id="Seg_3641" s="T105">играть-IMP.1DU</ta>
            <ta e="T107" id="Seg_3642" s="T106">говорить-PST1-3SG</ta>
            <ta e="T108" id="Seg_3643" s="T107">вот</ta>
            <ta e="T109" id="Seg_3644" s="T108">ладно</ta>
            <ta e="T110" id="Seg_3645" s="T109">ребенок.[NOM]</ta>
            <ta e="T111" id="Seg_3646" s="T110">быть-CVB.SIM</ta>
            <ta e="T112" id="Seg_3647" s="T111">играть-IMP.1DU</ta>
            <ta e="T113" id="Seg_3648" s="T112">этот</ta>
            <ta e="T114" id="Seg_3649" s="T113">старик-EP-2SG.[NOM]</ta>
            <ta e="T115" id="Seg_3650" s="T114">колыбель.[NOM]</ta>
            <ta e="T116" id="Seg_3651" s="T115">делать-PST1-3SG</ta>
            <ta e="T117" id="Seg_3652" s="T116">тот</ta>
            <ta e="T118" id="Seg_3653" s="T117">важенка-PL-3SG-GEN</ta>
            <ta e="T119" id="Seg_3654" s="T118">шкура-3SG-INSTR</ta>
            <ta e="T120" id="Seg_3655" s="T119">колыбель.[NOM]</ta>
            <ta e="T121" id="Seg_3656" s="T120">ремень-3SG.[NOM]</ta>
            <ta e="T122" id="Seg_3657" s="T121">делать-EP-MED-PST1-3PL</ta>
            <ta e="T123" id="Seg_3658" s="T122">старик-EP-2SG.[NOM]</ta>
            <ta e="T124" id="Seg_3659" s="T123">говорить-PST2.[3SG]</ta>
            <ta e="T125" id="Seg_3660" s="T124">вот</ta>
            <ta e="T126" id="Seg_3661" s="T125">ну</ta>
            <ta e="T127" id="Seg_3662" s="T126">2SG.[NOM]</ta>
            <ta e="T128" id="Seg_3663" s="T127">качать-EP-REFL.[IMP.2SG]</ta>
            <ta e="T129" id="Seg_3664" s="T128">ребенок.[NOM]</ta>
            <ta e="T130" id="Seg_3665" s="T129">быть-CVB.SEQ</ta>
            <ta e="T131" id="Seg_3666" s="T130">говорить-PST1-3SG</ta>
            <ta e="T132" id="Seg_3667" s="T131">лиса.[NOM]</ta>
            <ta e="T133" id="Seg_3668" s="T132">этот-DAT/LOC</ta>
            <ta e="T134" id="Seg_3669" s="T133">ложиться-CVB.SEQ</ta>
            <ta e="T135" id="Seg_3670" s="T134">попробовать-PST1-3SG</ta>
            <ta e="T136" id="Seg_3671" s="T135">ээ</ta>
            <ta e="T137" id="Seg_3672" s="T136">друг.[NOM]</ta>
            <ta e="T138" id="Seg_3673" s="T137">мой</ta>
            <ta e="T139" id="Seg_3674" s="T138">хвост-EP-1SG.[NOM]</ta>
            <ta e="T140" id="Seg_3675" s="T139">болить-PRS.[3SG]</ta>
            <ta e="T141" id="Seg_3676" s="T140">лежать-CAUS-FUT.[3SG]</ta>
            <ta e="T142" id="Seg_3677" s="T141">NEG</ta>
            <ta e="T143" id="Seg_3678" s="T142">говорить-PST1-3SG</ta>
            <ta e="T144" id="Seg_3679" s="T143">вот</ta>
            <ta e="T145" id="Seg_3680" s="T144">человек.[NOM]</ta>
            <ta e="T146" id="Seg_3681" s="T145">человек-2SG-INSTR</ta>
            <ta e="T147" id="Seg_3682" s="T146">2SG.[NOM]</ta>
            <ta e="T148" id="Seg_3683" s="T147">ложиться-CVB.SEQ</ta>
            <ta e="T149" id="Seg_3684" s="T148">ребенок.[NOM]</ta>
            <ta e="T150" id="Seg_3685" s="T149">быть.[IMP.2SG]</ta>
            <ta e="T151" id="Seg_3686" s="T150">говорить-PST1-3SG</ta>
            <ta e="T152" id="Seg_3687" s="T151">лиса.[NOM]</ta>
            <ta e="T153" id="Seg_3688" s="T152">старик-ACC</ta>
            <ta e="T154" id="Seg_3689" s="T153">старик.[NOM]</ta>
            <ta e="T155" id="Seg_3690" s="T154">вот</ta>
            <ta e="T156" id="Seg_3691" s="T155">ложиться-PST1-3SG</ta>
            <ta e="T157" id="Seg_3692" s="T156">лиса.[NOM]</ta>
            <ta e="T158" id="Seg_3693" s="T157">вот</ta>
            <ta e="T159" id="Seg_3694" s="T158">этот-ACC</ta>
            <ta e="T160" id="Seg_3695" s="T159">обвязывать-CVB.SEQ</ta>
            <ta e="T161" id="Seg_3696" s="T160">бросать-PST1-3SG</ta>
            <ta e="T162" id="Seg_3697" s="T161">лиса.[NOM]</ta>
            <ta e="T163" id="Seg_3698" s="T162">этот-ACC</ta>
            <ta e="T164" id="Seg_3699" s="T163">качать-CVB.SIM-качать-CVB.SIM</ta>
            <ta e="T165" id="Seg_3700" s="T164">дикий.олень.[NOM]</ta>
            <ta e="T166" id="Seg_3701" s="T165">мясо-3SG-INSTR</ta>
            <ta e="T167" id="Seg_3702" s="T166">жевать-CVB.SEQ</ta>
            <ta e="T168" id="Seg_3703" s="T167">есть-EP-CAUS-CVB.SIM</ta>
            <ta e="T169" id="Seg_3704" s="T168">сидеть-PST1-3SG</ta>
            <ta e="T170" id="Seg_3705" s="T169">этот</ta>
            <ta e="T171" id="Seg_3706" s="T170">старик-EP-2SG.[NOM]</ta>
            <ta e="T172" id="Seg_3707" s="T171">только</ta>
            <ta e="T173" id="Seg_3708" s="T172">спать-CVB.SEQ</ta>
            <ta e="T174" id="Seg_3709" s="T173">оставаться-PST1-3SG</ta>
            <ta e="T175" id="Seg_3710" s="T174">лиса.[NOM]</ta>
            <ta e="T176" id="Seg_3711" s="T175">этот-ACC</ta>
            <ta e="T177" id="Seg_3712" s="T176">делать.настойчиво-EP-RECP/COLL-ITER-CVB.SEQ</ta>
            <ta e="T178" id="Seg_3713" s="T177">носить-CVB.SEQ</ta>
            <ta e="T179" id="Seg_3714" s="T178">отрыв-ABL</ta>
            <ta e="T180" id="Seg_3715" s="T179">опрокинуть-ADVZ</ta>
            <ta e="T181" id="Seg_3716" s="T180">бросать-PST1-3SG</ta>
            <ta e="T182" id="Seg_3717" s="T181">позже</ta>
            <ta e="T183" id="Seg_3718" s="T182">тот</ta>
            <ta e="T184" id="Seg_3719" s="T183">старик.[NOM]</ta>
            <ta e="T185" id="Seg_3720" s="T184">просыпаться-EP-PST2-3SG</ta>
            <ta e="T186" id="Seg_3721" s="T185">овраг.[NOM]</ta>
            <ta e="T187" id="Seg_3722" s="T186">нутро-3SG-DAT/LOC</ta>
            <ta e="T188" id="Seg_3723" s="T187">лежать-PRS.[3SG]</ta>
            <ta e="T189" id="Seg_3724" s="T188">старик.[NOM]</ta>
            <ta e="T190" id="Seg_3725" s="T189">какой</ta>
            <ta e="T191" id="Seg_3726" s="T190">NEG</ta>
            <ta e="T192" id="Seg_3727" s="T191">к</ta>
            <ta e="T193" id="Seg_3728" s="T192">двигаться-PTCP.FUT-3SG-DAT/LOC</ta>
            <ta e="T194" id="Seg_3729" s="T193">сильный.[NOM]</ta>
            <ta e="T195" id="Seg_3730" s="T194">обвязывать-PASS/REFL-EP-PTCP.PST.[NOM]</ta>
            <ta e="T196" id="Seg_3731" s="T195">MOD</ta>
            <ta e="T197" id="Seg_3732" s="T196">старик.[NOM]</ta>
            <ta e="T198" id="Seg_3733" s="T197">звать-PRS.[3SG]</ta>
            <ta e="T199" id="Seg_3734" s="T198">горностай-PL.[NOM]</ta>
            <ta e="T200" id="Seg_3735" s="T199">песец-PL.[NOM]</ta>
            <ta e="T201" id="Seg_3736" s="T200">мышь-PL.[NOM]</ta>
            <ta e="T202" id="Seg_3737" s="T201">веревка-1SG-ACC</ta>
            <ta e="T203" id="Seg_3738" s="T202">резать-CVB.SIM</ta>
            <ta e="T204" id="Seg_3739" s="T203">грызть-IMP.2PL</ta>
            <ta e="T205" id="Seg_3740" s="T204">говорить-PRS.[3SG]</ta>
            <ta e="T206" id="Seg_3741" s="T205">тот-PL-EP-2SG.[NOM]</ta>
            <ta e="T207" id="Seg_3742" s="T206">приходить-CVB.SEQ-3PL</ta>
            <ta e="T208" id="Seg_3743" s="T207">веревка-3SG-ACC</ta>
            <ta e="T209" id="Seg_3744" s="T208">резать-CVB.SIM</ta>
            <ta e="T210" id="Seg_3745" s="T209">грызть-PST2-3PL</ta>
            <ta e="T211" id="Seg_3746" s="T210">старик.[NOM]</ta>
            <ta e="T212" id="Seg_3747" s="T211">вставать-CVB.SIM</ta>
            <ta e="T213" id="Seg_3748" s="T212">прыгать-PST1-3SG</ta>
            <ta e="T214" id="Seg_3749" s="T213">так</ta>
            <ta e="T215" id="Seg_3750" s="T214">делать-CVB.SEQ</ta>
            <ta e="T216" id="Seg_3751" s="T215">этот</ta>
            <ta e="T217" id="Seg_3752" s="T216">веревка-3SG-ACC</ta>
            <ta e="T218" id="Seg_3753" s="T217">грызть-PTCP.HAB-PL-DAT/LOC</ta>
            <ta e="T219" id="Seg_3754" s="T218">наем-3SG-ACC</ta>
            <ta e="T220" id="Seg_3755" s="T219">один</ta>
            <ta e="T221" id="Seg_3756" s="T220">дикий</ta>
            <ta e="T222" id="Seg_3757" s="T221">важенка-3SG-ACC</ta>
            <ta e="T223" id="Seg_3758" s="T222">убить-CVB.SEQ</ta>
            <ta e="T224" id="Seg_3759" s="T223">давать-PST1-3SG</ta>
            <ta e="T225" id="Seg_3760" s="T224">этот</ta>
            <ta e="T226" id="Seg_3761" s="T225">старик-EP-2SG.[NOM]</ta>
            <ta e="T227" id="Seg_3762" s="T226">костер-3SG-DAT/LOC</ta>
            <ta e="T228" id="Seg_3763" s="T227">идти-CVB.SEQ</ta>
            <ta e="T229" id="Seg_3764" s="T228">приходить-PST1-3SG</ta>
            <ta e="T230" id="Seg_3765" s="T229">костер-3SG-GEN</ta>
            <ta e="T231" id="Seg_3766" s="T230">пепел-3SG-ACC</ta>
            <ta e="T232" id="Seg_3767" s="T231">целый-SIM</ta>
            <ta e="T233" id="Seg_3768" s="T232">штаны-3SG-DAT/LOC</ta>
            <ta e="T234" id="Seg_3769" s="T233">насыпать-CVB.SEQ</ta>
            <ta e="T235" id="Seg_3770" s="T234">идти-CVB.SEQ</ta>
            <ta e="T236" id="Seg_3771" s="T235">идти-CVB.SEQ</ta>
            <ta e="T237" id="Seg_3772" s="T236">оставаться-PST1-3SG</ta>
            <ta e="T238" id="Seg_3773" s="T237">вот</ta>
            <ta e="T239" id="Seg_3774" s="T238">этот</ta>
            <ta e="T240" id="Seg_3775" s="T239">идти-CVB.SEQ</ta>
            <ta e="T241" id="Seg_3776" s="T240">идти-PST1-3SG</ta>
            <ta e="T242" id="Seg_3777" s="T241">идти-CVB.SEQ</ta>
            <ta e="T243" id="Seg_3778" s="T242">идти-CVB.SEQ-идти-CVB.SEQ</ta>
            <ta e="T244" id="Seg_3779" s="T243">только</ta>
            <ta e="T245" id="Seg_3780" s="T244">один</ta>
            <ta e="T246" id="Seg_3781" s="T245">дом-DAT/LOC</ta>
            <ta e="T247" id="Seg_3782" s="T246">доезжать-PST1-3SG</ta>
            <ta e="T248" id="Seg_3783" s="T247">этот</ta>
            <ta e="T249" id="Seg_3784" s="T248">дом-DAT/LOC</ta>
            <ta e="T250" id="Seg_3785" s="T249">приходить-CVB.SEQ</ta>
            <ta e="T251" id="Seg_3786" s="T250">слушать-CVB.SEQ</ta>
            <ta e="T252" id="Seg_3787" s="T251">попробовать-PST2-3SG</ta>
            <ta e="T253" id="Seg_3788" s="T252">лиса.[NOM]</ta>
            <ta e="T254" id="Seg_3789" s="T253">камлать-CVB.SEQ</ta>
            <ta e="T255" id="Seg_3790" s="T254">быть-PRS.[3SG]</ta>
            <ta e="T256" id="Seg_3791" s="T255">дом.[NOM]</ta>
            <ta e="T257" id="Seg_3792" s="T256">нутро-3SG-DAT/LOC</ta>
            <ta e="T258" id="Seg_3793" s="T257">входить-PST2-3SG</ta>
            <ta e="T259" id="Seg_3794" s="T258">лиса.[NOM]</ta>
            <ta e="T260" id="Seg_3795" s="T259">полный-SIM</ta>
            <ta e="T261" id="Seg_3796" s="T260">собираться-REFL-CVB.SEQ</ta>
            <ta e="T262" id="Seg_3797" s="T261">после</ta>
            <ta e="T263" id="Seg_3798" s="T262">камлать-CVB.SIM</ta>
            <ta e="T264" id="Seg_3799" s="T263">лежать-PRS-3PL</ta>
            <ta e="T265" id="Seg_3800" s="T264">шаман-PROPR-3PL</ta>
            <ta e="T266" id="Seg_3801" s="T265">старик.[NOM]</ta>
            <ta e="T267" id="Seg_3802" s="T266">входить-CVB.SEQ</ta>
            <ta e="T268" id="Seg_3803" s="T267">сесть-PST1-3SG</ta>
            <ta e="T269" id="Seg_3804" s="T268">этот</ta>
            <ta e="T270" id="Seg_3805" s="T269">сидеть-TEMP-3SG</ta>
            <ta e="T271" id="Seg_3806" s="T270">лиса-EP-2SG.[NOM]</ta>
            <ta e="T272" id="Seg_3807" s="T271">возвращаться-PST1-3SG</ta>
            <ta e="T273" id="Seg_3808" s="T272">камлать-CVB.SEQ</ta>
            <ta e="T274" id="Seg_3809" s="T273">идти-CVB.ANT</ta>
            <ta e="T275" id="Seg_3810" s="T274">этот</ta>
            <ta e="T276" id="Seg_3811" s="T275">старик-EP-2SG.[NOM]</ta>
            <ta e="T277" id="Seg_3812" s="T276">вдруг</ta>
            <ta e="T278" id="Seg_3813" s="T277">шаманский.бубен.[NOM]</ta>
            <ta e="T279" id="Seg_3814" s="T278">бить-PST1-3SG</ta>
            <ta e="T280" id="Seg_3815" s="T279">этот</ta>
            <ta e="T281" id="Seg_3816" s="T280">шаманский.бубен-3SG-ACC</ta>
            <ta e="T282" id="Seg_3817" s="T281">бить-CVB.SIM-бить-CVB.SIM</ta>
            <ta e="T283" id="Seg_3818" s="T282">вот</ta>
            <ta e="T284" id="Seg_3819" s="T283">прыгать-PST1-3SG</ta>
            <ta e="T285" id="Seg_3820" s="T284">вот</ta>
            <ta e="T286" id="Seg_3821" s="T285">этот-DAT/LOC</ta>
            <ta e="T287" id="Seg_3822" s="T286">пепел-3SG.[NOM]</ta>
            <ta e="T288" id="Seg_3823" s="T287">вот</ta>
            <ta e="T289" id="Seg_3824" s="T288">подниматься-ITER-PST1-3SG</ta>
            <ta e="T290" id="Seg_3825" s="T289">этот</ta>
            <ta e="T291" id="Seg_3826" s="T290">штаны-3SG-ABL</ta>
            <ta e="T292" id="Seg_3827" s="T291">вот</ta>
            <ta e="T293" id="Seg_3828" s="T292">этот-ABL</ta>
            <ta e="T294" id="Seg_3829" s="T293">этот</ta>
            <ta e="T295" id="Seg_3830" s="T294">лиса-PL-EP-2SG.[NOM]</ta>
            <ta e="T296" id="Seg_3831" s="T295">целый-SIM</ta>
            <ta e="T297" id="Seg_3832" s="T296">вот</ta>
            <ta e="T298" id="Seg_3833" s="T297">смеяться-EP-RECP/COLL-PST1-3PL</ta>
            <ta e="T299" id="Seg_3834" s="T298">вдруг</ta>
            <ta e="T300" id="Seg_3835" s="T299">там</ta>
            <ta e="T301" id="Seg_3836" s="T300">угол-DAT/LOC</ta>
            <ta e="T302" id="Seg_3837" s="T301">лиса.[NOM]</ta>
            <ta e="T303" id="Seg_3838" s="T302">смеяться-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T304" id="Seg_3839" s="T303">слышаться-PST1-3SG</ta>
            <ta e="T305" id="Seg_3840" s="T304">старик.[NOM]</ta>
            <ta e="T306" id="Seg_3841" s="T305">слышать-PST1-3SG</ta>
            <ta e="T307" id="Seg_3842" s="T306">шаманский.бубен-3SG-ACC</ta>
            <ta e="T308" id="Seg_3843" s="T307">упасть-CVB.SEQ</ta>
            <ta e="T309" id="Seg_3844" s="T308">бросать-PST1-3SG</ta>
            <ta e="T310" id="Seg_3845" s="T309">старик.[NOM]</ta>
            <ta e="T311" id="Seg_3846" s="T310">этот</ta>
            <ta e="T312" id="Seg_3847" s="T311">лиса-ACC</ta>
            <ta e="T313" id="Seg_3848" s="T312">поймать-CVB.SEQ</ta>
            <ta e="T314" id="Seg_3849" s="T313">взять-PST1-3SG</ta>
            <ta e="T315" id="Seg_3850" s="T314">лиса-3SG-ACC</ta>
            <ta e="T316" id="Seg_3851" s="T315">огонь-DAT/LOC</ta>
            <ta e="T317" id="Seg_3852" s="T316">жарить-CVB.SIM-жарить-CVB.SIM</ta>
            <ta e="T318" id="Seg_3853" s="T317">вот</ta>
            <ta e="T319" id="Seg_3854" s="T318">хлестать-PST1-3SG</ta>
            <ta e="T320" id="Seg_3855" s="T319">хлестать-CVB.SEQ-хлестать-CVB.SEQ</ta>
            <ta e="T321" id="Seg_3856" s="T320">убить-CVB.SEQ</ta>
            <ta e="T322" id="Seg_3857" s="T321">бросать-PST1-3SG</ta>
            <ta e="T323" id="Seg_3858" s="T322">вот</ta>
            <ta e="T324" id="Seg_3859" s="T323">тот</ta>
            <ta e="T325" id="Seg_3860" s="T324">лиса-EP-2SG.[NOM]</ta>
            <ta e="T326" id="Seg_3861" s="T325">сначала</ta>
            <ta e="T327" id="Seg_3862" s="T326">его-GEN</ta>
            <ta e="T328" id="Seg_3863" s="T327">ребенок-PL-3SG-ACC</ta>
            <ta e="T329" id="Seg_3864" s="T328">есть-HAB.[3SG]</ta>
            <ta e="T330" id="Seg_3865" s="T329">тот.EMPH</ta>
            <ta e="T331" id="Seg_3866" s="T330">лиса-EP-2SG.[NOM]</ta>
            <ta e="T332" id="Seg_3867" s="T331">колыбель-DAT/LOC</ta>
            <ta e="T333" id="Seg_3868" s="T332">обвязывать-CVB.SEQ</ta>
            <ta e="T334" id="Seg_3869" s="T333">идти-CVB.SEQ</ta>
            <ta e="T335" id="Seg_3870" s="T334">опрокинуть-ADVZ</ta>
            <ta e="T336" id="Seg_3871" s="T335">бросать-PST2-3SG</ta>
            <ta e="T337" id="Seg_3872" s="T336">так</ta>
            <ta e="T338" id="Seg_3873" s="T337">делать-CVB.SEQ</ta>
            <ta e="T339" id="Seg_3874" s="T338">после</ta>
            <ta e="T340" id="Seg_3875" s="T339">вот</ta>
            <ta e="T341" id="Seg_3876" s="T340">тот</ta>
            <ta e="T342" id="Seg_3877" s="T341">старик-3SG-GEN</ta>
            <ta e="T343" id="Seg_3878" s="T342">дом-3SG-DAT/LOC</ta>
            <ta e="T344" id="Seg_3879" s="T343">идти-CVB.SEQ</ta>
            <ta e="T345" id="Seg_3880" s="T344">жить-PST2-3SG</ta>
            <ta e="T346" id="Seg_3881" s="T345">вот</ta>
            <ta e="T347" id="Seg_3882" s="T346">тот</ta>
            <ta e="T348" id="Seg_3883" s="T347">конец-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_3884" s="T0">ptcl</ta>
            <ta e="T2" id="Seg_3885" s="T1">cardnum</ta>
            <ta e="T3" id="Seg_3886" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_3887" s="T3">v-v:tense-v:poss.pn</ta>
            <ta e="T5" id="Seg_3888" s="T4">propr</ta>
            <ta e="T6" id="Seg_3889" s="T5">n-n:(poss)-n:case</ta>
            <ta e="T7" id="Seg_3890" s="T6">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T8" id="Seg_3891" s="T7">quant</ta>
            <ta e="T9" id="Seg_3892" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_3893" s="T9">n-n:(poss)-n:case</ta>
            <ta e="T11" id="Seg_3894" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_3895" s="T11">dempro</ta>
            <ta e="T13" id="Seg_3896" s="T12">dempro-pro:case</ta>
            <ta e="T14" id="Seg_3897" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_3898" s="T14">v-v:tense-v:poss.pn</ta>
            <ta e="T16" id="Seg_3899" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_3900" s="T16">cardnum</ta>
            <ta e="T18" id="Seg_3901" s="T17">n-n:poss-n:case</ta>
            <ta e="T19" id="Seg_3902" s="T18">v-v:mood.pn</ta>
            <ta e="T20" id="Seg_3903" s="T19">v-v:tense-v:pred.pn</ta>
            <ta e="T21" id="Seg_3904" s="T20">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T22" id="Seg_3905" s="T21">dempro-pro:case</ta>
            <ta e="T23" id="Seg_3906" s="T22">dempro</ta>
            <ta e="T24" id="Seg_3907" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_3908" s="T24">n-n:poss-n:case</ta>
            <ta e="T26" id="Seg_3909" s="T25">v-v:tense-v:poss.pn</ta>
            <ta e="T27" id="Seg_3910" s="T26">v-v:mood.pn</ta>
            <ta e="T28" id="Seg_3911" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_3912" s="T28">dempro-pro:case</ta>
            <ta e="T30" id="Seg_3913" s="T29">v-v:tense-v:poss.pn</ta>
            <ta e="T31" id="Seg_3914" s="T30">que</ta>
            <ta e="T32" id="Seg_3915" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_3916" s="T32">v-v:cvb</ta>
            <ta e="T34" id="Seg_3917" s="T33">post</ta>
            <ta e="T35" id="Seg_3918" s="T34">ptcl</ta>
            <ta e="T36" id="Seg_3919" s="T35">v-v:tense-v:poss.pn</ta>
            <ta e="T37" id="Seg_3920" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_3921" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_3922" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_3923" s="T39">v-v:mood.pn</ta>
            <ta e="T41" id="Seg_3924" s="T40">v-v:tense-v:pred.pn</ta>
            <ta e="T42" id="Seg_3925" s="T41">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T43" id="Seg_3926" s="T42">dempro-pro:case</ta>
            <ta e="T44" id="Seg_3927" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_3928" s="T44">n-n:poss-n:case</ta>
            <ta e="T46" id="Seg_3929" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_3930" s="T46">v-v:tense-v:poss.pn</ta>
            <ta e="T48" id="Seg_3931" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_3932" s="T48">dempro</ta>
            <ta e="T50" id="Seg_3933" s="T49">n-n:poss-n:case</ta>
            <ta e="T51" id="Seg_3934" s="T50">v-v:tense-v:poss.pn</ta>
            <ta e="T52" id="Seg_3935" s="T51">v-v:cvb</ta>
            <ta e="T53" id="Seg_3936" s="T52">adv</ta>
            <ta e="T54" id="Seg_3937" s="T53">que</ta>
            <ta e="T55" id="Seg_3938" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_3939" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_3940" s="T56">v-v:cvb</ta>
            <ta e="T58" id="Seg_3941" s="T57">post</ta>
            <ta e="T59" id="Seg_3942" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_3943" s="T59">v-v:tense-v:poss.pn</ta>
            <ta e="T61" id="Seg_3944" s="T60">dempro-pro:case</ta>
            <ta e="T62" id="Seg_3945" s="T61">v-v:cvb</ta>
            <ta e="T63" id="Seg_3946" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_3947" s="T63">v-v:tense-v:pred.pn</ta>
            <ta e="T65" id="Seg_3948" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_3949" s="T65">v-v:mood.pn</ta>
            <ta e="T67" id="Seg_3950" s="T66">v-v:tense-v:pred.pn</ta>
            <ta e="T68" id="Seg_3951" s="T67">v-v:tense-v:(poss)-v:(case)</ta>
            <ta e="T69" id="Seg_3952" s="T68">n-n:(poss)-n:case</ta>
            <ta e="T70" id="Seg_3953" s="T69">adj-n:(poss)-n:case</ta>
            <ta e="T71" id="Seg_3954" s="T70">v-v:tense-v:poss.pn</ta>
            <ta e="T72" id="Seg_3955" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_3956" s="T72">dempro-pro:case</ta>
            <ta e="T74" id="Seg_3957" s="T73">v-v:tense-v:poss.pn</ta>
            <ta e="T75" id="Seg_3958" s="T74">conj</ta>
            <ta e="T76" id="Seg_3959" s="T75">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T77" id="Seg_3960" s="T76">v-v:tense-v:poss.pn</ta>
            <ta e="T78" id="Seg_3961" s="T77">dempro</ta>
            <ta e="T79" id="Seg_3962" s="T78">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T80" id="Seg_3963" s="T79">v-v:cvb</ta>
            <ta e="T81" id="Seg_3964" s="T80">v-v:cvb</ta>
            <ta e="T82" id="Seg_3965" s="T81">v-v:cvb</ta>
            <ta e="T83" id="Seg_3966" s="T82">v-v:cvb</ta>
            <ta e="T84" id="Seg_3967" s="T83">v-v:tense-v:poss.pn</ta>
            <ta e="T85" id="Seg_3968" s="T84">ptcl</ta>
            <ta e="T86" id="Seg_3969" s="T85">dempro</ta>
            <ta e="T87" id="Seg_3970" s="T86">v-v:cvb</ta>
            <ta e="T88" id="Seg_3971" s="T87">v-v:cvb-v-v:cvb</ta>
            <ta e="T89" id="Seg_3972" s="T88">v-v:cvb</ta>
            <ta e="T90" id="Seg_3973" s="T89">cardnum</ta>
            <ta e="T91" id="Seg_3974" s="T90">adj</ta>
            <ta e="T92" id="Seg_3975" s="T91">n-n:poss-n:case</ta>
            <ta e="T93" id="Seg_3976" s="T92">v-v:cvb</ta>
            <ta e="T94" id="Seg_3977" s="T93">v-v:cvb</ta>
            <ta e="T95" id="Seg_3978" s="T94">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T96" id="Seg_3979" s="T95">v-v:tense-v:poss.pn</ta>
            <ta e="T97" id="Seg_3980" s="T96">dempro</ta>
            <ta e="T98" id="Seg_3981" s="T97">v-v:mood-v:temp.pn</ta>
            <ta e="T99" id="Seg_3982" s="T98">n-n:case</ta>
            <ta e="T100" id="Seg_3983" s="T99">v-v:tense-v:poss.pn</ta>
            <ta e="T101" id="Seg_3984" s="T100">interj</ta>
            <ta e="T102" id="Seg_3985" s="T101">n-n:case</ta>
            <ta e="T103" id="Seg_3986" s="T102">pers-pro:case</ta>
            <ta e="T104" id="Seg_3987" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_3988" s="T104">v-v:cvb</ta>
            <ta e="T106" id="Seg_3989" s="T105">v-v:mood.pn</ta>
            <ta e="T107" id="Seg_3990" s="T106">v-v:tense-v:poss.pn</ta>
            <ta e="T108" id="Seg_3991" s="T107">ptcl</ta>
            <ta e="T109" id="Seg_3992" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_3993" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_3994" s="T110">v-v:cvb</ta>
            <ta e="T112" id="Seg_3995" s="T111">v-v:mood.pn</ta>
            <ta e="T113" id="Seg_3996" s="T112">dempro</ta>
            <ta e="T114" id="Seg_3997" s="T113">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T115" id="Seg_3998" s="T114">n-n:case</ta>
            <ta e="T116" id="Seg_3999" s="T115">v-v:tense-v:poss.pn</ta>
            <ta e="T117" id="Seg_4000" s="T116">dempro</ta>
            <ta e="T118" id="Seg_4001" s="T117">n-n:(num)-n:poss-n:case</ta>
            <ta e="T119" id="Seg_4002" s="T118">n-n:poss-n:case</ta>
            <ta e="T120" id="Seg_4003" s="T119">n-n:case</ta>
            <ta e="T121" id="Seg_4004" s="T120">n-n:(poss)-n:case</ta>
            <ta e="T122" id="Seg_4005" s="T121">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T123" id="Seg_4006" s="T122">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T124" id="Seg_4007" s="T123">v-v:tense-v:pred.pn</ta>
            <ta e="T125" id="Seg_4008" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_4009" s="T125">ptcl</ta>
            <ta e="T127" id="Seg_4010" s="T126">pers-pro:case</ta>
            <ta e="T128" id="Seg_4011" s="T127">v-v:(ins)-v&gt;v-v:mood.pn</ta>
            <ta e="T129" id="Seg_4012" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_4013" s="T129">v-v:cvb</ta>
            <ta e="T131" id="Seg_4014" s="T130">v-v:tense-v:poss.pn</ta>
            <ta e="T132" id="Seg_4015" s="T131">n-n:case</ta>
            <ta e="T133" id="Seg_4016" s="T132">dempro-pro:case</ta>
            <ta e="T134" id="Seg_4017" s="T133">v-v:cvb</ta>
            <ta e="T135" id="Seg_4018" s="T134">v-v:tense-v:poss.pn</ta>
            <ta e="T136" id="Seg_4019" s="T135">interj</ta>
            <ta e="T137" id="Seg_4020" s="T136">n-n:case</ta>
            <ta e="T138" id="Seg_4021" s="T137">posspr</ta>
            <ta e="T139" id="Seg_4022" s="T138">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T140" id="Seg_4023" s="T139">v-v:tense-v:pred.pn</ta>
            <ta e="T141" id="Seg_4024" s="T140">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T142" id="Seg_4025" s="T141">ptcl</ta>
            <ta e="T143" id="Seg_4026" s="T142">v-v:tense-v:poss.pn</ta>
            <ta e="T144" id="Seg_4027" s="T143">ptcl</ta>
            <ta e="T145" id="Seg_4028" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_4029" s="T145">n-n:poss-n:case</ta>
            <ta e="T147" id="Seg_4030" s="T146">pers-pro:case</ta>
            <ta e="T148" id="Seg_4031" s="T147">v-v:cvb</ta>
            <ta e="T149" id="Seg_4032" s="T148">n-n:case</ta>
            <ta e="T150" id="Seg_4033" s="T149">v-v:mood.pn</ta>
            <ta e="T151" id="Seg_4034" s="T150">v-v:tense-v:poss.pn</ta>
            <ta e="T152" id="Seg_4035" s="T151">n-n:case</ta>
            <ta e="T153" id="Seg_4036" s="T152">n-n:case</ta>
            <ta e="T154" id="Seg_4037" s="T153">n-n:case</ta>
            <ta e="T155" id="Seg_4038" s="T154">ptcl</ta>
            <ta e="T156" id="Seg_4039" s="T155">v-v:tense-v:poss.pn</ta>
            <ta e="T157" id="Seg_4040" s="T156">n-n:case</ta>
            <ta e="T158" id="Seg_4041" s="T157">ptcl</ta>
            <ta e="T159" id="Seg_4042" s="T158">dempro-pro:case</ta>
            <ta e="T160" id="Seg_4043" s="T159">v-v:cvb</ta>
            <ta e="T161" id="Seg_4044" s="T160">v-v:tense-v:poss.pn</ta>
            <ta e="T162" id="Seg_4045" s="T161">n-n:case</ta>
            <ta e="T163" id="Seg_4046" s="T162">dempro-pro:case</ta>
            <ta e="T164" id="Seg_4047" s="T163">v-v:cvb-v-v:cvb</ta>
            <ta e="T165" id="Seg_4048" s="T164">n-n:case</ta>
            <ta e="T166" id="Seg_4049" s="T165">n-n:poss-n:case</ta>
            <ta e="T167" id="Seg_4050" s="T166">v-v:cvb</ta>
            <ta e="T168" id="Seg_4051" s="T167">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T169" id="Seg_4052" s="T168">v-v:tense-v:poss.pn</ta>
            <ta e="T170" id="Seg_4053" s="T169">dempro</ta>
            <ta e="T171" id="Seg_4054" s="T170">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T172" id="Seg_4055" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_4056" s="T172">v-v:cvb</ta>
            <ta e="T174" id="Seg_4057" s="T173">v-v:tense-v:poss.pn</ta>
            <ta e="T175" id="Seg_4058" s="T174">n-n:case</ta>
            <ta e="T176" id="Seg_4059" s="T175">dempro-pro:case</ta>
            <ta e="T177" id="Seg_4060" s="T176">v-v:(ins)-v&gt;v-v&gt;v-v:cvb</ta>
            <ta e="T178" id="Seg_4061" s="T177">v-v:cvb</ta>
            <ta e="T179" id="Seg_4062" s="T178">n-n:case</ta>
            <ta e="T180" id="Seg_4063" s="T179">v-v&gt;adv</ta>
            <ta e="T181" id="Seg_4064" s="T180">v-v:tense-v:poss.pn</ta>
            <ta e="T182" id="Seg_4065" s="T181">adv</ta>
            <ta e="T183" id="Seg_4066" s="T182">dempro</ta>
            <ta e="T184" id="Seg_4067" s="T183">n-n:case</ta>
            <ta e="T185" id="Seg_4068" s="T184">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T186" id="Seg_4069" s="T185">n-n:case</ta>
            <ta e="T187" id="Seg_4070" s="T186">n-n:poss-n:case</ta>
            <ta e="T188" id="Seg_4071" s="T187">v-v:tense-v:pred.pn</ta>
            <ta e="T189" id="Seg_4072" s="T188">n-n:case</ta>
            <ta e="T190" id="Seg_4073" s="T189">que</ta>
            <ta e="T191" id="Seg_4074" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_4075" s="T191">post</ta>
            <ta e="T193" id="Seg_4076" s="T192">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T194" id="Seg_4077" s="T193">adj-n:case</ta>
            <ta e="T195" id="Seg_4078" s="T194">v-v&gt;v-v:(ins)-v:ptcp-v:(case)</ta>
            <ta e="T196" id="Seg_4079" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_4080" s="T196">n-n:case</ta>
            <ta e="T198" id="Seg_4081" s="T197">v-v:tense-v:pred.pn</ta>
            <ta e="T199" id="Seg_4082" s="T198">n-n:(num)-n:case</ta>
            <ta e="T200" id="Seg_4083" s="T199">n-n:(num)-n:case</ta>
            <ta e="T201" id="Seg_4084" s="T200">n-n:(num)-n:case</ta>
            <ta e="T202" id="Seg_4085" s="T201">n-n:poss-n:case</ta>
            <ta e="T203" id="Seg_4086" s="T202">v-v:cvb</ta>
            <ta e="T204" id="Seg_4087" s="T203">v-v:mood.pn</ta>
            <ta e="T205" id="Seg_4088" s="T204">v-v:tense-v:pred.pn</ta>
            <ta e="T206" id="Seg_4089" s="T205">dempro-pro:(num)-pro:(ins)-pro:(poss)-pro:case</ta>
            <ta e="T207" id="Seg_4090" s="T206">v-v:cvb-v:pred.pn</ta>
            <ta e="T208" id="Seg_4091" s="T207">n-n:poss-n:case</ta>
            <ta e="T209" id="Seg_4092" s="T208">v-v:cvb</ta>
            <ta e="T210" id="Seg_4093" s="T209">v-v:tense-v:poss.pn</ta>
            <ta e="T211" id="Seg_4094" s="T210">n-n:case</ta>
            <ta e="T212" id="Seg_4095" s="T211">v-v:cvb</ta>
            <ta e="T213" id="Seg_4096" s="T212">v-v:tense-v:poss.pn</ta>
            <ta e="T214" id="Seg_4097" s="T213">adv</ta>
            <ta e="T215" id="Seg_4098" s="T214">v-v:cvb</ta>
            <ta e="T216" id="Seg_4099" s="T215">dempro</ta>
            <ta e="T217" id="Seg_4100" s="T216">n-n:poss-n:case</ta>
            <ta e="T218" id="Seg_4101" s="T217">v-v:ptcp-v:(num)-v:(case)</ta>
            <ta e="T219" id="Seg_4102" s="T218">n-n:poss-n:case</ta>
            <ta e="T220" id="Seg_4103" s="T219">cardnum</ta>
            <ta e="T221" id="Seg_4104" s="T220">adj</ta>
            <ta e="T222" id="Seg_4105" s="T221">n-n:poss-n:case</ta>
            <ta e="T223" id="Seg_4106" s="T222">v-v:cvb</ta>
            <ta e="T224" id="Seg_4107" s="T223">v-v:tense-v:poss.pn</ta>
            <ta e="T225" id="Seg_4108" s="T224">dempro</ta>
            <ta e="T226" id="Seg_4109" s="T225">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T227" id="Seg_4110" s="T226">n-n:poss-n:case</ta>
            <ta e="T228" id="Seg_4111" s="T227">v-v:cvb</ta>
            <ta e="T229" id="Seg_4112" s="T228">v-v:tense-v:poss.pn</ta>
            <ta e="T230" id="Seg_4113" s="T229">n-n:poss-n:case</ta>
            <ta e="T231" id="Seg_4114" s="T230">n-n:poss-n:case</ta>
            <ta e="T232" id="Seg_4115" s="T231">adj-adj&gt;adv</ta>
            <ta e="T233" id="Seg_4116" s="T232">n-n:poss-n:case</ta>
            <ta e="T234" id="Seg_4117" s="T233">v-v:cvb</ta>
            <ta e="T235" id="Seg_4118" s="T234">v-v:cvb</ta>
            <ta e="T236" id="Seg_4119" s="T235">v-v:cvb</ta>
            <ta e="T237" id="Seg_4120" s="T236">v-v:tense-v:poss.pn</ta>
            <ta e="T238" id="Seg_4121" s="T237">ptcl</ta>
            <ta e="T239" id="Seg_4122" s="T238">dempro</ta>
            <ta e="T240" id="Seg_4123" s="T239">v-v:cvb</ta>
            <ta e="T241" id="Seg_4124" s="T240">v-v:tense-v:poss.pn</ta>
            <ta e="T242" id="Seg_4125" s="T241">v-v:cvb</ta>
            <ta e="T243" id="Seg_4126" s="T242">v-v:cvb-v-v:cvb</ta>
            <ta e="T244" id="Seg_4127" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_4128" s="T244">cardnum</ta>
            <ta e="T246" id="Seg_4129" s="T245">n-n:case</ta>
            <ta e="T247" id="Seg_4130" s="T246">v-v:tense-v:poss.pn</ta>
            <ta e="T248" id="Seg_4131" s="T247">dempro</ta>
            <ta e="T249" id="Seg_4132" s="T248">n-n:case</ta>
            <ta e="T250" id="Seg_4133" s="T249">v-v:cvb</ta>
            <ta e="T251" id="Seg_4134" s="T250">v-v:cvb</ta>
            <ta e="T252" id="Seg_4135" s="T251">v-v:tense-v:poss.pn</ta>
            <ta e="T253" id="Seg_4136" s="T252">n-n:case</ta>
            <ta e="T254" id="Seg_4137" s="T253">v-v:cvb</ta>
            <ta e="T255" id="Seg_4138" s="T254">v-v:tense-v:pred.pn</ta>
            <ta e="T256" id="Seg_4139" s="T255">n-n:case</ta>
            <ta e="T257" id="Seg_4140" s="T256">n-n:poss-n:case</ta>
            <ta e="T258" id="Seg_4141" s="T257">v-v:tense-v:poss.pn</ta>
            <ta e="T259" id="Seg_4142" s="T258">n-n:case</ta>
            <ta e="T260" id="Seg_4143" s="T259">adj-adj&gt;adv</ta>
            <ta e="T261" id="Seg_4144" s="T260">v-v&gt;v-v:cvb</ta>
            <ta e="T262" id="Seg_4145" s="T261">post</ta>
            <ta e="T263" id="Seg_4146" s="T262">v-v:cvb</ta>
            <ta e="T264" id="Seg_4147" s="T263">v-v:tense-v:pred.pn</ta>
            <ta e="T265" id="Seg_4148" s="T264">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T266" id="Seg_4149" s="T265">n-n:case</ta>
            <ta e="T267" id="Seg_4150" s="T266">v-v:cvb</ta>
            <ta e="T268" id="Seg_4151" s="T267">v-v:tense-v:poss.pn</ta>
            <ta e="T269" id="Seg_4152" s="T268">dempro</ta>
            <ta e="T270" id="Seg_4153" s="T269">v-v:mood-v:temp.pn</ta>
            <ta e="T271" id="Seg_4154" s="T270">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T272" id="Seg_4155" s="T271">v-v:tense-v:poss.pn</ta>
            <ta e="T273" id="Seg_4156" s="T272">v-v:cvb</ta>
            <ta e="T274" id="Seg_4157" s="T273">v-v:cvb</ta>
            <ta e="T275" id="Seg_4158" s="T274">dempro</ta>
            <ta e="T276" id="Seg_4159" s="T275">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T277" id="Seg_4160" s="T276">adv</ta>
            <ta e="T278" id="Seg_4161" s="T277">n-n:case</ta>
            <ta e="T279" id="Seg_4162" s="T278">v-v:tense-v:poss.pn</ta>
            <ta e="T280" id="Seg_4163" s="T279">dempro</ta>
            <ta e="T281" id="Seg_4164" s="T280">n-n:poss-n:case</ta>
            <ta e="T282" id="Seg_4165" s="T281">v-v:cvb-v-v:cvb</ta>
            <ta e="T283" id="Seg_4166" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_4167" s="T283">v-v:tense-v:poss.pn</ta>
            <ta e="T285" id="Seg_4168" s="T284">ptcl</ta>
            <ta e="T286" id="Seg_4169" s="T285">dempro-pro:case</ta>
            <ta e="T287" id="Seg_4170" s="T286">n-n:(poss)-n:case</ta>
            <ta e="T288" id="Seg_4171" s="T287">ptcl</ta>
            <ta e="T289" id="Seg_4172" s="T288">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T290" id="Seg_4173" s="T289">dempro</ta>
            <ta e="T291" id="Seg_4174" s="T290">n-n:poss-n:case</ta>
            <ta e="T292" id="Seg_4175" s="T291">ptcl</ta>
            <ta e="T293" id="Seg_4176" s="T292">dempro-pro:case</ta>
            <ta e="T294" id="Seg_4177" s="T293">dempro</ta>
            <ta e="T295" id="Seg_4178" s="T294">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T296" id="Seg_4179" s="T295">adj-adj&gt;adv</ta>
            <ta e="T297" id="Seg_4180" s="T296">ptcl</ta>
            <ta e="T298" id="Seg_4181" s="T297">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T299" id="Seg_4182" s="T298">adv</ta>
            <ta e="T300" id="Seg_4183" s="T299">adv</ta>
            <ta e="T301" id="Seg_4184" s="T300">n-n:case</ta>
            <ta e="T302" id="Seg_4185" s="T301">n-n:case</ta>
            <ta e="T303" id="Seg_4186" s="T302">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T304" id="Seg_4187" s="T303">v-v:tense-v:poss.pn</ta>
            <ta e="T305" id="Seg_4188" s="T304">n-n:case</ta>
            <ta e="T306" id="Seg_4189" s="T305">v-v:tense-v:poss.pn</ta>
            <ta e="T307" id="Seg_4190" s="T306">n-n:poss-n:case</ta>
            <ta e="T308" id="Seg_4191" s="T307">v-v:cvb</ta>
            <ta e="T309" id="Seg_4192" s="T308">v-v:tense-v:poss.pn</ta>
            <ta e="T310" id="Seg_4193" s="T309">n-n:case</ta>
            <ta e="T311" id="Seg_4194" s="T310">dempro</ta>
            <ta e="T312" id="Seg_4195" s="T311">n-n:case</ta>
            <ta e="T313" id="Seg_4196" s="T312">v-v:cvb</ta>
            <ta e="T314" id="Seg_4197" s="T313">v-v:tense-v:poss.pn</ta>
            <ta e="T315" id="Seg_4198" s="T314">n-n:poss-n:case</ta>
            <ta e="T316" id="Seg_4199" s="T315">n-n:case</ta>
            <ta e="T317" id="Seg_4200" s="T316">v-v:cvb-v-v:cvb</ta>
            <ta e="T318" id="Seg_4201" s="T317">ptcl</ta>
            <ta e="T319" id="Seg_4202" s="T318">v-v:tense-v:poss.pn</ta>
            <ta e="T320" id="Seg_4203" s="T319">v-v:cvb-v-v:cvb</ta>
            <ta e="T321" id="Seg_4204" s="T320">v-v:cvb</ta>
            <ta e="T322" id="Seg_4205" s="T321">v-v:tense-v:poss.pn</ta>
            <ta e="T323" id="Seg_4206" s="T322">ptcl</ta>
            <ta e="T324" id="Seg_4207" s="T323">dempro</ta>
            <ta e="T325" id="Seg_4208" s="T324">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T326" id="Seg_4209" s="T325">adv</ta>
            <ta e="T327" id="Seg_4210" s="T326">posspr-pro:case</ta>
            <ta e="T328" id="Seg_4211" s="T327">n-n:(num)-n:poss-n:case</ta>
            <ta e="T329" id="Seg_4212" s="T328">v-v:mood-v:pred.pn</ta>
            <ta e="T330" id="Seg_4213" s="T329">dempro</ta>
            <ta e="T331" id="Seg_4214" s="T330">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T332" id="Seg_4215" s="T331">n-n:case</ta>
            <ta e="T333" id="Seg_4216" s="T332">v-v:cvb</ta>
            <ta e="T334" id="Seg_4217" s="T333">v-v:cvb</ta>
            <ta e="T335" id="Seg_4218" s="T334">v-v&gt;adv</ta>
            <ta e="T336" id="Seg_4219" s="T335">v-v:tense-v:poss.pn</ta>
            <ta e="T337" id="Seg_4220" s="T336">adv</ta>
            <ta e="T338" id="Seg_4221" s="T337">v-v:cvb</ta>
            <ta e="T339" id="Seg_4222" s="T338">post</ta>
            <ta e="T340" id="Seg_4223" s="T339">ptcl</ta>
            <ta e="T341" id="Seg_4224" s="T340">dempro</ta>
            <ta e="T342" id="Seg_4225" s="T341">n-n:poss-n:case</ta>
            <ta e="T343" id="Seg_4226" s="T342">n-n:poss-n:case</ta>
            <ta e="T344" id="Seg_4227" s="T343">v-v:cvb</ta>
            <ta e="T345" id="Seg_4228" s="T344">v-v:tense-v:poss.pn</ta>
            <ta e="T346" id="Seg_4229" s="T345">ptcl</ta>
            <ta e="T347" id="Seg_4230" s="T346">dempro</ta>
            <ta e="T348" id="Seg_4231" s="T347">n-n:(poss)-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_4232" s="T0">ptcl</ta>
            <ta e="T2" id="Seg_4233" s="T1">cardnum</ta>
            <ta e="T3" id="Seg_4234" s="T2">n</ta>
            <ta e="T4" id="Seg_4235" s="T3">v</ta>
            <ta e="T5" id="Seg_4236" s="T4">propr</ta>
            <ta e="T6" id="Seg_4237" s="T5">n</ta>
            <ta e="T7" id="Seg_4238" s="T6">dempro</ta>
            <ta e="T8" id="Seg_4239" s="T7">quant</ta>
            <ta e="T9" id="Seg_4240" s="T8">ptcl</ta>
            <ta e="T10" id="Seg_4241" s="T9">n</ta>
            <ta e="T11" id="Seg_4242" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_4243" s="T11">dempro</ta>
            <ta e="T13" id="Seg_4244" s="T12">dempro</ta>
            <ta e="T14" id="Seg_4245" s="T13">n</ta>
            <ta e="T15" id="Seg_4246" s="T14">v</ta>
            <ta e="T16" id="Seg_4247" s="T15">n</ta>
            <ta e="T17" id="Seg_4248" s="T16">cardnum</ta>
            <ta e="T18" id="Seg_4249" s="T17">n</ta>
            <ta e="T19" id="Seg_4250" s="T18">v</ta>
            <ta e="T20" id="Seg_4251" s="T19">v</ta>
            <ta e="T21" id="Seg_4252" s="T20">v</ta>
            <ta e="T22" id="Seg_4253" s="T21">dempro</ta>
            <ta e="T23" id="Seg_4254" s="T22">dempro</ta>
            <ta e="T24" id="Seg_4255" s="T23">n</ta>
            <ta e="T25" id="Seg_4256" s="T24">n</ta>
            <ta e="T26" id="Seg_4257" s="T25">v</ta>
            <ta e="T27" id="Seg_4258" s="T26">v</ta>
            <ta e="T28" id="Seg_4259" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_4260" s="T28">dempro</ta>
            <ta e="T30" id="Seg_4261" s="T29">v</ta>
            <ta e="T31" id="Seg_4262" s="T30">que</ta>
            <ta e="T32" id="Seg_4263" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_4264" s="T32">v</ta>
            <ta e="T34" id="Seg_4265" s="T33">post</ta>
            <ta e="T35" id="Seg_4266" s="T34">ptcl</ta>
            <ta e="T36" id="Seg_4267" s="T35">v</ta>
            <ta e="T37" id="Seg_4268" s="T36">n</ta>
            <ta e="T38" id="Seg_4269" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_4270" s="T38">n</ta>
            <ta e="T40" id="Seg_4271" s="T39">v</ta>
            <ta e="T41" id="Seg_4272" s="T40">v</ta>
            <ta e="T42" id="Seg_4273" s="T41">v</ta>
            <ta e="T43" id="Seg_4274" s="T42">dempro</ta>
            <ta e="T44" id="Seg_4275" s="T43">n</ta>
            <ta e="T45" id="Seg_4276" s="T44">n</ta>
            <ta e="T46" id="Seg_4277" s="T45">ptcl</ta>
            <ta e="T47" id="Seg_4278" s="T46">v</ta>
            <ta e="T48" id="Seg_4279" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_4280" s="T48">dempro</ta>
            <ta e="T50" id="Seg_4281" s="T49">n</ta>
            <ta e="T51" id="Seg_4282" s="T50">v</ta>
            <ta e="T52" id="Seg_4283" s="T51">v</ta>
            <ta e="T53" id="Seg_4284" s="T52">adv</ta>
            <ta e="T54" id="Seg_4285" s="T53">que</ta>
            <ta e="T55" id="Seg_4286" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_4287" s="T55">n</ta>
            <ta e="T57" id="Seg_4288" s="T56">cop</ta>
            <ta e="T58" id="Seg_4289" s="T57">post</ta>
            <ta e="T59" id="Seg_4290" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_4291" s="T59">v</ta>
            <ta e="T61" id="Seg_4292" s="T60">dempro</ta>
            <ta e="T62" id="Seg_4293" s="T61">v</ta>
            <ta e="T63" id="Seg_4294" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_4295" s="T63">v</ta>
            <ta e="T65" id="Seg_4296" s="T64">n</ta>
            <ta e="T66" id="Seg_4297" s="T65">v</ta>
            <ta e="T67" id="Seg_4298" s="T66">v</ta>
            <ta e="T68" id="Seg_4299" s="T67">v</ta>
            <ta e="T69" id="Seg_4300" s="T68">n</ta>
            <ta e="T70" id="Seg_4301" s="T69">adj</ta>
            <ta e="T71" id="Seg_4302" s="T70">cop</ta>
            <ta e="T72" id="Seg_4303" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_4304" s="T72">dempro</ta>
            <ta e="T74" id="Seg_4305" s="T73">v</ta>
            <ta e="T75" id="Seg_4306" s="T74">conj</ta>
            <ta e="T76" id="Seg_4307" s="T75">n</ta>
            <ta e="T77" id="Seg_4308" s="T76">v</ta>
            <ta e="T78" id="Seg_4309" s="T77">dempro</ta>
            <ta e="T79" id="Seg_4310" s="T78">n</ta>
            <ta e="T80" id="Seg_4311" s="T79">v</ta>
            <ta e="T81" id="Seg_4312" s="T80">v</ta>
            <ta e="T82" id="Seg_4313" s="T81">aux</ta>
            <ta e="T83" id="Seg_4314" s="T82">v</ta>
            <ta e="T84" id="Seg_4315" s="T83">aux</ta>
            <ta e="T85" id="Seg_4316" s="T84">ptcl</ta>
            <ta e="T86" id="Seg_4317" s="T85">dempro</ta>
            <ta e="T87" id="Seg_4318" s="T86">v</ta>
            <ta e="T88" id="Seg_4319" s="T87">v</ta>
            <ta e="T89" id="Seg_4320" s="T88">aux</ta>
            <ta e="T90" id="Seg_4321" s="T89">cardnum</ta>
            <ta e="T91" id="Seg_4322" s="T90">adj</ta>
            <ta e="T92" id="Seg_4323" s="T91">n</ta>
            <ta e="T93" id="Seg_4324" s="T92">v</ta>
            <ta e="T94" id="Seg_4325" s="T93">aux</ta>
            <ta e="T95" id="Seg_4326" s="T94">v</ta>
            <ta e="T96" id="Seg_4327" s="T95">v</ta>
            <ta e="T97" id="Seg_4328" s="T96">dempro</ta>
            <ta e="T98" id="Seg_4329" s="T97">v</ta>
            <ta e="T99" id="Seg_4330" s="T98">n</ta>
            <ta e="T100" id="Seg_4331" s="T99">v</ta>
            <ta e="T101" id="Seg_4332" s="T100">interj</ta>
            <ta e="T102" id="Seg_4333" s="T101">n</ta>
            <ta e="T103" id="Seg_4334" s="T102">pers</ta>
            <ta e="T104" id="Seg_4335" s="T103">n</ta>
            <ta e="T105" id="Seg_4336" s="T104">cop</ta>
            <ta e="T106" id="Seg_4337" s="T105">v</ta>
            <ta e="T107" id="Seg_4338" s="T106">v</ta>
            <ta e="T108" id="Seg_4339" s="T107">ptcl</ta>
            <ta e="T109" id="Seg_4340" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_4341" s="T109">n</ta>
            <ta e="T111" id="Seg_4342" s="T110">cop</ta>
            <ta e="T112" id="Seg_4343" s="T111">v</ta>
            <ta e="T113" id="Seg_4344" s="T112">dempro</ta>
            <ta e="T114" id="Seg_4345" s="T113">n</ta>
            <ta e="T115" id="Seg_4346" s="T114">n</ta>
            <ta e="T116" id="Seg_4347" s="T115">v</ta>
            <ta e="T117" id="Seg_4348" s="T116">dempro</ta>
            <ta e="T118" id="Seg_4349" s="T117">n</ta>
            <ta e="T119" id="Seg_4350" s="T118">n</ta>
            <ta e="T120" id="Seg_4351" s="T119">n</ta>
            <ta e="T121" id="Seg_4352" s="T120">n</ta>
            <ta e="T122" id="Seg_4353" s="T121">v</ta>
            <ta e="T123" id="Seg_4354" s="T122">n</ta>
            <ta e="T124" id="Seg_4355" s="T123">v</ta>
            <ta e="T125" id="Seg_4356" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_4357" s="T125">ptcl</ta>
            <ta e="T127" id="Seg_4358" s="T126">pers</ta>
            <ta e="T128" id="Seg_4359" s="T127">v</ta>
            <ta e="T129" id="Seg_4360" s="T128">n</ta>
            <ta e="T130" id="Seg_4361" s="T129">cop</ta>
            <ta e="T131" id="Seg_4362" s="T130">v</ta>
            <ta e="T132" id="Seg_4363" s="T131">n</ta>
            <ta e="T133" id="Seg_4364" s="T132">dempro</ta>
            <ta e="T134" id="Seg_4365" s="T133">v</ta>
            <ta e="T135" id="Seg_4366" s="T134">v</ta>
            <ta e="T136" id="Seg_4367" s="T135">interj</ta>
            <ta e="T137" id="Seg_4368" s="T136">n</ta>
            <ta e="T138" id="Seg_4369" s="T137">posspr</ta>
            <ta e="T139" id="Seg_4370" s="T138">n</ta>
            <ta e="T140" id="Seg_4371" s="T139">v</ta>
            <ta e="T141" id="Seg_4372" s="T140">v</ta>
            <ta e="T142" id="Seg_4373" s="T141">ptcl</ta>
            <ta e="T143" id="Seg_4374" s="T142">v</ta>
            <ta e="T144" id="Seg_4375" s="T143">ptcl</ta>
            <ta e="T145" id="Seg_4376" s="T144">n</ta>
            <ta e="T146" id="Seg_4377" s="T145">n</ta>
            <ta e="T147" id="Seg_4378" s="T146">pers</ta>
            <ta e="T148" id="Seg_4379" s="T147">v</ta>
            <ta e="T149" id="Seg_4380" s="T148">n</ta>
            <ta e="T150" id="Seg_4381" s="T149">cop</ta>
            <ta e="T151" id="Seg_4382" s="T150">v</ta>
            <ta e="T152" id="Seg_4383" s="T151">n</ta>
            <ta e="T153" id="Seg_4384" s="T152">n</ta>
            <ta e="T154" id="Seg_4385" s="T153">n</ta>
            <ta e="T155" id="Seg_4386" s="T154">ptcl</ta>
            <ta e="T156" id="Seg_4387" s="T155">v</ta>
            <ta e="T157" id="Seg_4388" s="T156">n</ta>
            <ta e="T158" id="Seg_4389" s="T157">ptcl</ta>
            <ta e="T159" id="Seg_4390" s="T158">dempro</ta>
            <ta e="T160" id="Seg_4391" s="T159">v</ta>
            <ta e="T161" id="Seg_4392" s="T160">aux</ta>
            <ta e="T162" id="Seg_4393" s="T161">n</ta>
            <ta e="T163" id="Seg_4394" s="T162">dempro</ta>
            <ta e="T164" id="Seg_4395" s="T163">v</ta>
            <ta e="T165" id="Seg_4396" s="T164">n</ta>
            <ta e="T166" id="Seg_4397" s="T165">n</ta>
            <ta e="T167" id="Seg_4398" s="T166">v</ta>
            <ta e="T168" id="Seg_4399" s="T167">v</ta>
            <ta e="T169" id="Seg_4400" s="T168">aux</ta>
            <ta e="T170" id="Seg_4401" s="T169">dempro</ta>
            <ta e="T171" id="Seg_4402" s="T170">n</ta>
            <ta e="T172" id="Seg_4403" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_4404" s="T172">v</ta>
            <ta e="T174" id="Seg_4405" s="T173">aux</ta>
            <ta e="T175" id="Seg_4406" s="T174">n</ta>
            <ta e="T176" id="Seg_4407" s="T175">dempro</ta>
            <ta e="T177" id="Seg_4408" s="T176">v</ta>
            <ta e="T178" id="Seg_4409" s="T177">v</ta>
            <ta e="T179" id="Seg_4410" s="T178">n</ta>
            <ta e="T180" id="Seg_4411" s="T179">adv</ta>
            <ta e="T181" id="Seg_4412" s="T180">v</ta>
            <ta e="T182" id="Seg_4413" s="T181">adv</ta>
            <ta e="T183" id="Seg_4414" s="T182">dempro</ta>
            <ta e="T184" id="Seg_4415" s="T183">n</ta>
            <ta e="T185" id="Seg_4416" s="T184">v</ta>
            <ta e="T186" id="Seg_4417" s="T185">n</ta>
            <ta e="T187" id="Seg_4418" s="T186">n</ta>
            <ta e="T188" id="Seg_4419" s="T187">v</ta>
            <ta e="T189" id="Seg_4420" s="T188">n</ta>
            <ta e="T190" id="Seg_4421" s="T189">que</ta>
            <ta e="T191" id="Seg_4422" s="T190">ptcl</ta>
            <ta e="T192" id="Seg_4423" s="T191">post</ta>
            <ta e="T193" id="Seg_4424" s="T192">v</ta>
            <ta e="T194" id="Seg_4425" s="T193">adj</ta>
            <ta e="T195" id="Seg_4426" s="T194">v</ta>
            <ta e="T196" id="Seg_4427" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_4428" s="T196">n</ta>
            <ta e="T198" id="Seg_4429" s="T197">v</ta>
            <ta e="T199" id="Seg_4430" s="T198">n</ta>
            <ta e="T200" id="Seg_4431" s="T199">n</ta>
            <ta e="T201" id="Seg_4432" s="T200">n</ta>
            <ta e="T202" id="Seg_4433" s="T201">n</ta>
            <ta e="T203" id="Seg_4434" s="T202">v</ta>
            <ta e="T204" id="Seg_4435" s="T203">v</ta>
            <ta e="T205" id="Seg_4436" s="T204">v</ta>
            <ta e="T206" id="Seg_4437" s="T205">dempro</ta>
            <ta e="T207" id="Seg_4438" s="T206">v</ta>
            <ta e="T208" id="Seg_4439" s="T207">n</ta>
            <ta e="T209" id="Seg_4440" s="T208">v</ta>
            <ta e="T210" id="Seg_4441" s="T209">v</ta>
            <ta e="T211" id="Seg_4442" s="T210">n</ta>
            <ta e="T212" id="Seg_4443" s="T211">v</ta>
            <ta e="T213" id="Seg_4444" s="T212">v</ta>
            <ta e="T214" id="Seg_4445" s="T213">adv</ta>
            <ta e="T215" id="Seg_4446" s="T214">v</ta>
            <ta e="T216" id="Seg_4447" s="T215">dempro</ta>
            <ta e="T217" id="Seg_4448" s="T216">n</ta>
            <ta e="T218" id="Seg_4449" s="T217">n</ta>
            <ta e="T219" id="Seg_4450" s="T218">n</ta>
            <ta e="T220" id="Seg_4451" s="T219">cardnum</ta>
            <ta e="T221" id="Seg_4452" s="T220">adj</ta>
            <ta e="T222" id="Seg_4453" s="T221">n</ta>
            <ta e="T223" id="Seg_4454" s="T222">v</ta>
            <ta e="T224" id="Seg_4455" s="T223">v</ta>
            <ta e="T225" id="Seg_4456" s="T224">dempro</ta>
            <ta e="T226" id="Seg_4457" s="T225">n</ta>
            <ta e="T227" id="Seg_4458" s="T226">n</ta>
            <ta e="T228" id="Seg_4459" s="T227">v</ta>
            <ta e="T229" id="Seg_4460" s="T228">v</ta>
            <ta e="T230" id="Seg_4461" s="T229">n</ta>
            <ta e="T231" id="Seg_4462" s="T230">n</ta>
            <ta e="T232" id="Seg_4463" s="T231">adv</ta>
            <ta e="T233" id="Seg_4464" s="T232">n</ta>
            <ta e="T234" id="Seg_4465" s="T233">v</ta>
            <ta e="T235" id="Seg_4466" s="T234">aux</ta>
            <ta e="T236" id="Seg_4467" s="T235">v</ta>
            <ta e="T237" id="Seg_4468" s="T236">aux</ta>
            <ta e="T238" id="Seg_4469" s="T237">ptcl</ta>
            <ta e="T239" id="Seg_4470" s="T238">dempro</ta>
            <ta e="T240" id="Seg_4471" s="T239">v</ta>
            <ta e="T241" id="Seg_4472" s="T240">aux</ta>
            <ta e="T242" id="Seg_4473" s="T241">v</ta>
            <ta e="T243" id="Seg_4474" s="T242">v</ta>
            <ta e="T244" id="Seg_4475" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_4476" s="T244">cardnum</ta>
            <ta e="T246" id="Seg_4477" s="T245">n</ta>
            <ta e="T247" id="Seg_4478" s="T246">v</ta>
            <ta e="T248" id="Seg_4479" s="T247">dempro</ta>
            <ta e="T249" id="Seg_4480" s="T248">n</ta>
            <ta e="T250" id="Seg_4481" s="T249">v</ta>
            <ta e="T251" id="Seg_4482" s="T250">v</ta>
            <ta e="T252" id="Seg_4483" s="T251">v</ta>
            <ta e="T253" id="Seg_4484" s="T252">n</ta>
            <ta e="T254" id="Seg_4485" s="T253">v</ta>
            <ta e="T255" id="Seg_4486" s="T254">aux</ta>
            <ta e="T256" id="Seg_4487" s="T255">n</ta>
            <ta e="T257" id="Seg_4488" s="T256">n</ta>
            <ta e="T258" id="Seg_4489" s="T257">v</ta>
            <ta e="T259" id="Seg_4490" s="T258">n</ta>
            <ta e="T260" id="Seg_4491" s="T259">adv</ta>
            <ta e="T261" id="Seg_4492" s="T260">v</ta>
            <ta e="T262" id="Seg_4493" s="T261">post</ta>
            <ta e="T263" id="Seg_4494" s="T262">v</ta>
            <ta e="T264" id="Seg_4495" s="T263">aux</ta>
            <ta e="T265" id="Seg_4496" s="T264">adj</ta>
            <ta e="T266" id="Seg_4497" s="T265">n</ta>
            <ta e="T267" id="Seg_4498" s="T266">v</ta>
            <ta e="T268" id="Seg_4499" s="T267">v</ta>
            <ta e="T269" id="Seg_4500" s="T268">dempro</ta>
            <ta e="T270" id="Seg_4501" s="T269">v</ta>
            <ta e="T271" id="Seg_4502" s="T270">n</ta>
            <ta e="T272" id="Seg_4503" s="T271">v</ta>
            <ta e="T273" id="Seg_4504" s="T272">v</ta>
            <ta e="T274" id="Seg_4505" s="T273">v</ta>
            <ta e="T275" id="Seg_4506" s="T274">dempro</ta>
            <ta e="T276" id="Seg_4507" s="T275">n</ta>
            <ta e="T277" id="Seg_4508" s="T276">adv</ta>
            <ta e="T278" id="Seg_4509" s="T277">n</ta>
            <ta e="T279" id="Seg_4510" s="T278">v</ta>
            <ta e="T280" id="Seg_4511" s="T279">dempro</ta>
            <ta e="T281" id="Seg_4512" s="T280">n</ta>
            <ta e="T282" id="Seg_4513" s="T281">v</ta>
            <ta e="T283" id="Seg_4514" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_4515" s="T283">v</ta>
            <ta e="T285" id="Seg_4516" s="T284">ptcl</ta>
            <ta e="T286" id="Seg_4517" s="T285">dempro</ta>
            <ta e="T287" id="Seg_4518" s="T286">n</ta>
            <ta e="T288" id="Seg_4519" s="T287">ptcl</ta>
            <ta e="T289" id="Seg_4520" s="T288">v</ta>
            <ta e="T290" id="Seg_4521" s="T289">dempro</ta>
            <ta e="T291" id="Seg_4522" s="T290">n</ta>
            <ta e="T292" id="Seg_4523" s="T291">ptcl</ta>
            <ta e="T293" id="Seg_4524" s="T292">dempro</ta>
            <ta e="T294" id="Seg_4525" s="T293">dempro</ta>
            <ta e="T295" id="Seg_4526" s="T294">n</ta>
            <ta e="T296" id="Seg_4527" s="T295">adv</ta>
            <ta e="T297" id="Seg_4528" s="T296">ptcl</ta>
            <ta e="T298" id="Seg_4529" s="T297">v</ta>
            <ta e="T299" id="Seg_4530" s="T298">adv</ta>
            <ta e="T300" id="Seg_4531" s="T299">adv</ta>
            <ta e="T301" id="Seg_4532" s="T300">n</ta>
            <ta e="T302" id="Seg_4533" s="T301">n</ta>
            <ta e="T303" id="Seg_4534" s="T302">v</ta>
            <ta e="T304" id="Seg_4535" s="T303">v</ta>
            <ta e="T305" id="Seg_4536" s="T304">n</ta>
            <ta e="T306" id="Seg_4537" s="T305">v</ta>
            <ta e="T307" id="Seg_4538" s="T306">n</ta>
            <ta e="T308" id="Seg_4539" s="T307">v</ta>
            <ta e="T309" id="Seg_4540" s="T308">aux</ta>
            <ta e="T310" id="Seg_4541" s="T309">n</ta>
            <ta e="T311" id="Seg_4542" s="T310">dempro</ta>
            <ta e="T312" id="Seg_4543" s="T311">n</ta>
            <ta e="T313" id="Seg_4544" s="T312">v</ta>
            <ta e="T314" id="Seg_4545" s="T313">aux</ta>
            <ta e="T315" id="Seg_4546" s="T314">n</ta>
            <ta e="T316" id="Seg_4547" s="T315">n</ta>
            <ta e="T317" id="Seg_4548" s="T316">v</ta>
            <ta e="T318" id="Seg_4549" s="T317">ptcl</ta>
            <ta e="T319" id="Seg_4550" s="T318">v</ta>
            <ta e="T320" id="Seg_4551" s="T319">v</ta>
            <ta e="T321" id="Seg_4552" s="T320">v</ta>
            <ta e="T322" id="Seg_4553" s="T321">aux</ta>
            <ta e="T323" id="Seg_4554" s="T322">ptcl</ta>
            <ta e="T324" id="Seg_4555" s="T323">dempro</ta>
            <ta e="T325" id="Seg_4556" s="T324">n</ta>
            <ta e="T326" id="Seg_4557" s="T325">adv</ta>
            <ta e="T327" id="Seg_4558" s="T326">posspr</ta>
            <ta e="T328" id="Seg_4559" s="T327">n</ta>
            <ta e="T329" id="Seg_4560" s="T328">v</ta>
            <ta e="T330" id="Seg_4561" s="T329">dempro</ta>
            <ta e="T331" id="Seg_4562" s="T330">n</ta>
            <ta e="T332" id="Seg_4563" s="T331">n</ta>
            <ta e="T333" id="Seg_4564" s="T332">v</ta>
            <ta e="T334" id="Seg_4565" s="T333">aux</ta>
            <ta e="T335" id="Seg_4566" s="T334">adv</ta>
            <ta e="T336" id="Seg_4567" s="T335">v</ta>
            <ta e="T337" id="Seg_4568" s="T336">adv</ta>
            <ta e="T338" id="Seg_4569" s="T337">v</ta>
            <ta e="T339" id="Seg_4570" s="T338">post</ta>
            <ta e="T340" id="Seg_4571" s="T339">ptcl</ta>
            <ta e="T341" id="Seg_4572" s="T340">dempro</ta>
            <ta e="T342" id="Seg_4573" s="T341">n</ta>
            <ta e="T343" id="Seg_4574" s="T342">n</ta>
            <ta e="T344" id="Seg_4575" s="T343">v</ta>
            <ta e="T345" id="Seg_4576" s="T344">v</ta>
            <ta e="T346" id="Seg_4577" s="T345">ptcl</ta>
            <ta e="T347" id="Seg_4578" s="T346">dempro</ta>
            <ta e="T348" id="Seg_4579" s="T347">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T3" id="Seg_4580" s="T2">np.h:Th</ta>
            <ta e="T6" id="Seg_4581" s="T5">np:Th</ta>
            <ta e="T7" id="Seg_4582" s="T6">pro.h:Poss</ta>
            <ta e="T10" id="Seg_4583" s="T9">np.h:Th</ta>
            <ta e="T13" id="Seg_4584" s="T12">pro:G</ta>
            <ta e="T14" id="Seg_4585" s="T13">np.h:A</ta>
            <ta e="T18" id="Seg_4586" s="T17">np.h:P</ta>
            <ta e="T19" id="Seg_4587" s="T18">0.2.h:A</ta>
            <ta e="T20" id="Seg_4588" s="T19">0.3.h:A</ta>
            <ta e="T21" id="Seg_4589" s="T20">0.1.h:A</ta>
            <ta e="T22" id="Seg_4590" s="T21">pro:Time</ta>
            <ta e="T24" id="Seg_4591" s="T23">np.h:A</ta>
            <ta e="T25" id="Seg_4592" s="T24">np.h:Th</ta>
            <ta e="T27" id="Seg_4593" s="T26">0.3.h:A</ta>
            <ta e="T29" id="Seg_4594" s="T28">pro.h:A</ta>
            <ta e="T36" id="Seg_4595" s="T35">0.3.h:A</ta>
            <ta e="T39" id="Seg_4596" s="T38">np.h:P</ta>
            <ta e="T40" id="Seg_4597" s="T39">0.2.h:A</ta>
            <ta e="T41" id="Seg_4598" s="T40">0.3.h:A</ta>
            <ta e="T42" id="Seg_4599" s="T41">0.1.h:A</ta>
            <ta e="T43" id="Seg_4600" s="T42">pro:Time</ta>
            <ta e="T44" id="Seg_4601" s="T43">np.h:A</ta>
            <ta e="T45" id="Seg_4602" s="T44">np.h:Th</ta>
            <ta e="T50" id="Seg_4603" s="T49">np.h:P</ta>
            <ta e="T51" id="Seg_4604" s="T50">0.3.h:A</ta>
            <ta e="T53" id="Seg_4605" s="T52">n:Time</ta>
            <ta e="T56" id="Seg_4606" s="T55">np:Th</ta>
            <ta e="T60" id="Seg_4607" s="T59">0.3.h:A</ta>
            <ta e="T61" id="Seg_4608" s="T60">pro.h:A</ta>
            <ta e="T65" id="Seg_4609" s="T64">np.h:P</ta>
            <ta e="T66" id="Seg_4610" s="T65">0.2.h:A</ta>
            <ta e="T67" id="Seg_4611" s="T66">0.3.h:A</ta>
            <ta e="T68" id="Seg_4612" s="T67">0.1.h:A</ta>
            <ta e="T69" id="Seg_4613" s="T68">np.h:Th</ta>
            <ta e="T73" id="Seg_4614" s="T72">pro.h:A</ta>
            <ta e="T76" id="Seg_4615" s="T75">np.h:A</ta>
            <ta e="T79" id="Seg_4616" s="T78">np.h:A</ta>
            <ta e="T92" id="Seg_4617" s="T91">np:P</ta>
            <ta e="T96" id="Seg_4618" s="T95">0.3.h:A</ta>
            <ta e="T99" id="Seg_4619" s="T98">np.h:A</ta>
            <ta e="T103" id="Seg_4620" s="T102">pro.h:A</ta>
            <ta e="T107" id="Seg_4621" s="T106">0.3.h:A</ta>
            <ta e="T112" id="Seg_4622" s="T111">0.1.h:A</ta>
            <ta e="T114" id="Seg_4623" s="T113">np.h:A</ta>
            <ta e="T115" id="Seg_4624" s="T114">np:P</ta>
            <ta e="T118" id="Seg_4625" s="T117">np.h:Poss</ta>
            <ta e="T119" id="Seg_4626" s="T118">np:So</ta>
            <ta e="T120" id="Seg_4627" s="T119">np.h:Poss</ta>
            <ta e="T121" id="Seg_4628" s="T120">np:P</ta>
            <ta e="T122" id="Seg_4629" s="T121">0.3.h:A</ta>
            <ta e="T123" id="Seg_4630" s="T122">np.h:A</ta>
            <ta e="T127" id="Seg_4631" s="T126">pro.h:A</ta>
            <ta e="T131" id="Seg_4632" s="T130">0.3.h:A</ta>
            <ta e="T132" id="Seg_4633" s="T131">np.h:A</ta>
            <ta e="T133" id="Seg_4634" s="T132">pro:Time</ta>
            <ta e="T138" id="Seg_4635" s="T137">pro.h:Poss</ta>
            <ta e="T139" id="Seg_4636" s="T138">np:Th</ta>
            <ta e="T142" id="Seg_4637" s="T140">0.3:Cau</ta>
            <ta e="T143" id="Seg_4638" s="T142">0.3.h:A</ta>
            <ta e="T145" id="Seg_4639" s="T144">0.2.h:Th</ta>
            <ta e="T147" id="Seg_4640" s="T146">pro.h:A</ta>
            <ta e="T152" id="Seg_4641" s="T151">np.h:A</ta>
            <ta e="T153" id="Seg_4642" s="T152">np.h:R</ta>
            <ta e="T154" id="Seg_4643" s="T153">np.h:A</ta>
            <ta e="T157" id="Seg_4644" s="T156">np.h:A</ta>
            <ta e="T159" id="Seg_4645" s="T158">pro.h:P</ta>
            <ta e="T162" id="Seg_4646" s="T161">np.h:A</ta>
            <ta e="T163" id="Seg_4647" s="T162">pro.h:P</ta>
            <ta e="T166" id="Seg_4648" s="T165">np:Ins</ta>
            <ta e="T171" id="Seg_4649" s="T170">np.h:Th</ta>
            <ta e="T175" id="Seg_4650" s="T174">np.h:A</ta>
            <ta e="T176" id="Seg_4651" s="T175">pro.h:Th</ta>
            <ta e="T179" id="Seg_4652" s="T178">np:So</ta>
            <ta e="T182" id="Seg_4653" s="T181">adv:Time</ta>
            <ta e="T184" id="Seg_4654" s="T183">pro.h:Th</ta>
            <ta e="T186" id="Seg_4655" s="T185">np:Poss</ta>
            <ta e="T187" id="Seg_4656" s="T186">np:L</ta>
            <ta e="T189" id="Seg_4657" s="T188">np.h:Th</ta>
            <ta e="T192" id="Seg_4658" s="T189">pp:G</ta>
            <ta e="T195" id="Seg_4659" s="T194">0.3.h:P</ta>
            <ta e="T197" id="Seg_4660" s="T196">np.h:A</ta>
            <ta e="T202" id="Seg_4661" s="T201">0.1.h:Poss np:P</ta>
            <ta e="T205" id="Seg_4662" s="T204">0.3.h:A</ta>
            <ta e="T206" id="Seg_4663" s="T205">pro:A</ta>
            <ta e="T208" id="Seg_4664" s="T207">0.3.h:Poss np:P</ta>
            <ta e="T211" id="Seg_4665" s="T210">np.h:A</ta>
            <ta e="T217" id="Seg_4666" s="T216">np:P</ta>
            <ta e="T218" id="Seg_4667" s="T217">np:R</ta>
            <ta e="T222" id="Seg_4668" s="T221">np:P</ta>
            <ta e="T224" id="Seg_4669" s="T223">0.3.h:A</ta>
            <ta e="T226" id="Seg_4670" s="T225">np.h:A</ta>
            <ta e="T227" id="Seg_4671" s="T226">np:G</ta>
            <ta e="T230" id="Seg_4672" s="T229">np:Poss</ta>
            <ta e="T231" id="Seg_4673" s="T230">np:Th</ta>
            <ta e="T233" id="Seg_4674" s="T232">np:G</ta>
            <ta e="T237" id="Seg_4675" s="T235">0.3.h:A</ta>
            <ta e="T241" id="Seg_4676" s="T240">0.3.h:A</ta>
            <ta e="T246" id="Seg_4677" s="T245">np:G</ta>
            <ta e="T247" id="Seg_4678" s="T246">0.3.h:A</ta>
            <ta e="T249" id="Seg_4679" s="T248">np:G</ta>
            <ta e="T252" id="Seg_4680" s="T250">0.3.h:A</ta>
            <ta e="T253" id="Seg_4681" s="T252">np.h:A</ta>
            <ta e="T256" id="Seg_4682" s="T255">np:Poss</ta>
            <ta e="T257" id="Seg_4683" s="T256">np:G</ta>
            <ta e="T258" id="Seg_4684" s="T257">0.3.h:A</ta>
            <ta e="T259" id="Seg_4685" s="T258">np.h:A</ta>
            <ta e="T265" id="Seg_4686" s="T264">0.3.h:Th</ta>
            <ta e="T266" id="Seg_4687" s="T265">np.h:A</ta>
            <ta e="T271" id="Seg_4688" s="T270">np.h:A</ta>
            <ta e="T276" id="Seg_4689" s="T275">np.h:A</ta>
            <ta e="T278" id="Seg_4690" s="T277">np:P</ta>
            <ta e="T281" id="Seg_4691" s="T280">np:P</ta>
            <ta e="T284" id="Seg_4692" s="T283">0.3.h:A</ta>
            <ta e="T286" id="Seg_4693" s="T285">pro:Time</ta>
            <ta e="T287" id="Seg_4694" s="T286">np:Th</ta>
            <ta e="T291" id="Seg_4695" s="T290">np:So</ta>
            <ta e="T293" id="Seg_4696" s="T292">adv:Time</ta>
            <ta e="T295" id="Seg_4697" s="T294">np.h:A</ta>
            <ta e="T299" id="Seg_4698" s="T298">adv:Time</ta>
            <ta e="T300" id="Seg_4699" s="T299">adv:L</ta>
            <ta e="T301" id="Seg_4700" s="T300">np:L</ta>
            <ta e="T302" id="Seg_4701" s="T301">np.h:A</ta>
            <ta e="T304" id="Seg_4702" s="T303">0.3:Th</ta>
            <ta e="T305" id="Seg_4703" s="T304">np.h:E</ta>
            <ta e="T307" id="Seg_4704" s="T306">np:P</ta>
            <ta e="T309" id="Seg_4705" s="T307">0.3.h:A</ta>
            <ta e="T310" id="Seg_4706" s="T309">np.h:A</ta>
            <ta e="T312" id="Seg_4707" s="T311">np:P</ta>
            <ta e="T315" id="Seg_4708" s="T314">np:P</ta>
            <ta e="T316" id="Seg_4709" s="T315">np:L</ta>
            <ta e="T319" id="Seg_4710" s="T318">0.3.h:A</ta>
            <ta e="T322" id="Seg_4711" s="T320">0.3.h:A</ta>
            <ta e="T325" id="Seg_4712" s="T324">np.h:A</ta>
            <ta e="T327" id="Seg_4713" s="T326">pro.h:Poss</ta>
            <ta e="T328" id="Seg_4714" s="T327">np.h:P</ta>
            <ta e="T331" id="Seg_4715" s="T330">np.h:A</ta>
            <ta e="T332" id="Seg_4716" s="T331">np:G</ta>
            <ta e="T341" id="Seg_4717" s="T340">pro.h:A</ta>
            <ta e="T342" id="Seg_4718" s="T341">np.h:Poss</ta>
            <ta e="T343" id="Seg_4719" s="T342">np:G</ta>
            <ta e="T347" id="Seg_4720" s="T346">pro:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_4721" s="T2">np.h:S</ta>
            <ta e="T4" id="Seg_4722" s="T3">v:pred</ta>
            <ta e="T5" id="Seg_4723" s="T4">n:pred</ta>
            <ta e="T6" id="Seg_4724" s="T5">np:S</ta>
            <ta e="T8" id="Seg_4725" s="T7">adj:pred</ta>
            <ta e="T10" id="Seg_4726" s="T9">np.h:S</ta>
            <ta e="T14" id="Seg_4727" s="T13">np.h:S</ta>
            <ta e="T15" id="Seg_4728" s="T14">v:pred</ta>
            <ta e="T18" id="Seg_4729" s="T17">np.h:O</ta>
            <ta e="T19" id="Seg_4730" s="T18">0.2.h:S v:pred</ta>
            <ta e="T20" id="Seg_4731" s="T19">0.3.h:S v:pred</ta>
            <ta e="T21" id="Seg_4732" s="T20">0.1.h:S v:pred</ta>
            <ta e="T24" id="Seg_4733" s="T23">np.h:S</ta>
            <ta e="T25" id="Seg_4734" s="T24">np.h:O</ta>
            <ta e="T26" id="Seg_4735" s="T25">v:pred</ta>
            <ta e="T27" id="Seg_4736" s="T26">0.3.h:S v:pred</ta>
            <ta e="T29" id="Seg_4737" s="T28">pro.h:S</ta>
            <ta e="T30" id="Seg_4738" s="T29">v:pred</ta>
            <ta e="T34" id="Seg_4739" s="T30">s:temp</ta>
            <ta e="T36" id="Seg_4740" s="T35">0.3.h:S v:pred</ta>
            <ta e="T39" id="Seg_4741" s="T38">np.h:O</ta>
            <ta e="T40" id="Seg_4742" s="T39">0.2.h:S v:pred</ta>
            <ta e="T41" id="Seg_4743" s="T40">0.3.h:S v:pred</ta>
            <ta e="T42" id="Seg_4744" s="T41">0.1.h:S v:pred</ta>
            <ta e="T44" id="Seg_4745" s="T43">np.h:S</ta>
            <ta e="T45" id="Seg_4746" s="T44">np.h:O</ta>
            <ta e="T47" id="Seg_4747" s="T46">v:pred</ta>
            <ta e="T50" id="Seg_4748" s="T49">np.h:O</ta>
            <ta e="T51" id="Seg_4749" s="T50">0.3.h:S v:pred</ta>
            <ta e="T52" id="Seg_4750" s="T51">s:purp</ta>
            <ta e="T58" id="Seg_4751" s="T53">s:temp</ta>
            <ta e="T60" id="Seg_4752" s="T59">0.3.h:S v:pred</ta>
            <ta e="T61" id="Seg_4753" s="T60">pro.h:S</ta>
            <ta e="T62" id="Seg_4754" s="T61">s:adv</ta>
            <ta e="T64" id="Seg_4755" s="T63">v:pred</ta>
            <ta e="T65" id="Seg_4756" s="T64">np.h:O</ta>
            <ta e="T66" id="Seg_4757" s="T65">0.2.h:S v:pred</ta>
            <ta e="T67" id="Seg_4758" s="T66">0.3.h:S v:pred</ta>
            <ta e="T68" id="Seg_4759" s="T67">0.1.h:S v:pred</ta>
            <ta e="T69" id="Seg_4760" s="T68">np.h:S</ta>
            <ta e="T70" id="Seg_4761" s="T69">adj:pred</ta>
            <ta e="T71" id="Seg_4762" s="T70">cop</ta>
            <ta e="T73" id="Seg_4763" s="T72">pro.h:S</ta>
            <ta e="T74" id="Seg_4764" s="T73">v:pred</ta>
            <ta e="T76" id="Seg_4765" s="T75">np.h:S</ta>
            <ta e="T77" id="Seg_4766" s="T76">v:pred</ta>
            <ta e="T79" id="Seg_4767" s="T78">np.h:S</ta>
            <ta e="T82" id="Seg_4768" s="T79">s:adv</ta>
            <ta e="T84" id="Seg_4769" s="T82">v:pred</ta>
            <ta e="T89" id="Seg_4770" s="T84">s:adv</ta>
            <ta e="T94" id="Seg_4771" s="T89">s:adv</ta>
            <ta e="T95" id="Seg_4772" s="T94">s:adv</ta>
            <ta e="T96" id="Seg_4773" s="T95">0.3.h:S v:pred</ta>
            <ta e="T98" id="Seg_4774" s="T96">s:temp</ta>
            <ta e="T99" id="Seg_4775" s="T98">np.h:S</ta>
            <ta e="T100" id="Seg_4776" s="T99">v:pred</ta>
            <ta e="T103" id="Seg_4777" s="T102">pro.h:S</ta>
            <ta e="T105" id="Seg_4778" s="T103">s:adv</ta>
            <ta e="T106" id="Seg_4779" s="T105">v:pred</ta>
            <ta e="T107" id="Seg_4780" s="T106">0.3.h:S v:pred</ta>
            <ta e="T111" id="Seg_4781" s="T109">s:adv</ta>
            <ta e="T112" id="Seg_4782" s="T111">0.1.h:S v:pred</ta>
            <ta e="T114" id="Seg_4783" s="T113">np.h:S</ta>
            <ta e="T115" id="Seg_4784" s="T114">np:O</ta>
            <ta e="T116" id="Seg_4785" s="T115">v:pred</ta>
            <ta e="T121" id="Seg_4786" s="T120">np:O</ta>
            <ta e="T122" id="Seg_4787" s="T121">0.3.h:S v:pred</ta>
            <ta e="T123" id="Seg_4788" s="T122">np.h:S</ta>
            <ta e="T124" id="Seg_4789" s="T123">v:pred</ta>
            <ta e="T127" id="Seg_4790" s="T126">pro.h:S</ta>
            <ta e="T128" id="Seg_4791" s="T127">v:pred</ta>
            <ta e="T130" id="Seg_4792" s="T128">s:adv</ta>
            <ta e="T131" id="Seg_4793" s="T130">0.3.h:S v:pred</ta>
            <ta e="T132" id="Seg_4794" s="T131">np.h:S</ta>
            <ta e="T135" id="Seg_4795" s="T133">v:pred</ta>
            <ta e="T139" id="Seg_4796" s="T138">np:S</ta>
            <ta e="T140" id="Seg_4797" s="T139">v:pred</ta>
            <ta e="T142" id="Seg_4798" s="T140">0.3:S v:pred</ta>
            <ta e="T143" id="Seg_4799" s="T142">0.3.h:S v:pred</ta>
            <ta e="T145" id="Seg_4800" s="T144">n:pred</ta>
            <ta e="T147" id="Seg_4801" s="T146">pro.h:S</ta>
            <ta e="T148" id="Seg_4802" s="T147">s:adv</ta>
            <ta e="T149" id="Seg_4803" s="T148">n:pred</ta>
            <ta e="T150" id="Seg_4804" s="T149">0.2.h:S cop</ta>
            <ta e="T151" id="Seg_4805" s="T150">v:pred</ta>
            <ta e="T152" id="Seg_4806" s="T151">np.h:S</ta>
            <ta e="T153" id="Seg_4807" s="T152">np.h:O</ta>
            <ta e="T154" id="Seg_4808" s="T153">np.h:S</ta>
            <ta e="T156" id="Seg_4809" s="T155">v:pred</ta>
            <ta e="T157" id="Seg_4810" s="T156">np.h:S</ta>
            <ta e="T159" id="Seg_4811" s="T158">pro.h:O</ta>
            <ta e="T161" id="Seg_4812" s="T159">v:pred</ta>
            <ta e="T162" id="Seg_4813" s="T161">np.h:S</ta>
            <ta e="T163" id="Seg_4814" s="T162">pro.h:O</ta>
            <ta e="T164" id="Seg_4815" s="T163">s:adv</ta>
            <ta e="T168" id="Seg_4816" s="T164">s:adv</ta>
            <ta e="T169" id="Seg_4817" s="T168">v:pred</ta>
            <ta e="T171" id="Seg_4818" s="T170">np.h:S</ta>
            <ta e="T174" id="Seg_4819" s="T172">v:pred</ta>
            <ta e="T175" id="Seg_4820" s="T174">np.h:S</ta>
            <ta e="T176" id="Seg_4821" s="T175">pro.h:O</ta>
            <ta e="T178" id="Seg_4822" s="T176">s:adv</ta>
            <ta e="T181" id="Seg_4823" s="T179">v:pred</ta>
            <ta e="T185" id="Seg_4824" s="T184">s:temp</ta>
            <ta e="T188" id="Seg_4825" s="T187">0.3.h:S v:pred</ta>
            <ta e="T189" id="Seg_4826" s="T188">np.h:S</ta>
            <ta e="T193" id="Seg_4827" s="T192">v:pred</ta>
            <ta e="T195" id="Seg_4828" s="T194">0.3.h:S v:pred</ta>
            <ta e="T197" id="Seg_4829" s="T196">np.h:S</ta>
            <ta e="T198" id="Seg_4830" s="T197">v:pred</ta>
            <ta e="T202" id="Seg_4831" s="T201">np:O</ta>
            <ta e="T204" id="Seg_4832" s="T202">0.2.h:S v:pred</ta>
            <ta e="T205" id="Seg_4833" s="T204">0.3.h:S v:pred</ta>
            <ta e="T207" id="Seg_4834" s="T206">s:adv</ta>
            <ta e="T208" id="Seg_4835" s="T207">np:O</ta>
            <ta e="T210" id="Seg_4836" s="T208">0.3.h:S v:pred</ta>
            <ta e="T211" id="Seg_4837" s="T210">np.h:S</ta>
            <ta e="T213" id="Seg_4838" s="T211">v:pred</ta>
            <ta e="T215" id="Seg_4839" s="T213">s:temp</ta>
            <ta e="T218" id="Seg_4840" s="T215">s:rel</ta>
            <ta e="T223" id="Seg_4841" s="T219">s:adv</ta>
            <ta e="T224" id="Seg_4842" s="T223">0.3.h:S v:pred</ta>
            <ta e="T226" id="Seg_4843" s="T225">np.h:S</ta>
            <ta e="T229" id="Seg_4844" s="T227">v:pred</ta>
            <ta e="T235" id="Seg_4845" s="T229">s:adv</ta>
            <ta e="T237" id="Seg_4846" s="T235">0.3.h:S v:pred</ta>
            <ta e="T240" id="Seg_4847" s="T239">s:adv</ta>
            <ta e="T241" id="Seg_4848" s="T240">v:pred</ta>
            <ta e="T243" id="Seg_4849" s="T241">s:adv</ta>
            <ta e="T247" id="Seg_4850" s="T246">0.3.h:S v:pred</ta>
            <ta e="T250" id="Seg_4851" s="T247">s:temp</ta>
            <ta e="T252" id="Seg_4852" s="T250">0.3.h:S v:pred</ta>
            <ta e="T253" id="Seg_4853" s="T252">np.h:S</ta>
            <ta e="T255" id="Seg_4854" s="T253">v:pred</ta>
            <ta e="T258" id="Seg_4855" s="T257">0.3.h:S v:pred</ta>
            <ta e="T262" id="Seg_4856" s="T259">s:temp</ta>
            <ta e="T264" id="Seg_4857" s="T262">0.3.h:S v:pred</ta>
            <ta e="T265" id="Seg_4858" s="T264">0.3.h:S adj:pred</ta>
            <ta e="T266" id="Seg_4859" s="T265">np.h:S</ta>
            <ta e="T267" id="Seg_4860" s="T266">s:adv</ta>
            <ta e="T268" id="Seg_4861" s="T267">v:pred</ta>
            <ta e="T270" id="Seg_4862" s="T268">s:temp</ta>
            <ta e="T271" id="Seg_4863" s="T270">np.h:S</ta>
            <ta e="T272" id="Seg_4864" s="T271">v:pred</ta>
            <ta e="T274" id="Seg_4865" s="T272">s:temp</ta>
            <ta e="T276" id="Seg_4866" s="T275">np.h:S</ta>
            <ta e="T278" id="Seg_4867" s="T277">np:O</ta>
            <ta e="T279" id="Seg_4868" s="T278">v:pred</ta>
            <ta e="T282" id="Seg_4869" s="T279">s:adv</ta>
            <ta e="T284" id="Seg_4870" s="T283">v:pred</ta>
            <ta e="T287" id="Seg_4871" s="T286">np:S</ta>
            <ta e="T289" id="Seg_4872" s="T288">v:pred</ta>
            <ta e="T295" id="Seg_4873" s="T294">np.h:S</ta>
            <ta e="T298" id="Seg_4874" s="T297">v:pred</ta>
            <ta e="T303" id="Seg_4875" s="T301">s:comp</ta>
            <ta e="T304" id="Seg_4876" s="T303">0.3:S v:pred</ta>
            <ta e="T305" id="Seg_4877" s="T304">np.h:S</ta>
            <ta e="T306" id="Seg_4878" s="T305">v:pred</ta>
            <ta e="T307" id="Seg_4879" s="T306">np:O</ta>
            <ta e="T309" id="Seg_4880" s="T307">0.3.h:S v:pred</ta>
            <ta e="T310" id="Seg_4881" s="T309">np.h:S</ta>
            <ta e="T312" id="Seg_4882" s="T311">np.h:O</ta>
            <ta e="T314" id="Seg_4883" s="T312">v:pred</ta>
            <ta e="T315" id="Seg_4884" s="T314">np.h:O</ta>
            <ta e="T317" id="Seg_4885" s="T315">s:adv</ta>
            <ta e="T319" id="Seg_4886" s="T318">0.3.h:S v:pred</ta>
            <ta e="T320" id="Seg_4887" s="T319">s:adv</ta>
            <ta e="T322" id="Seg_4888" s="T320">0.3.h:S v:pred</ta>
            <ta e="T325" id="Seg_4889" s="T324">np.h:S</ta>
            <ta e="T328" id="Seg_4890" s="T327">np.h:O</ta>
            <ta e="T329" id="Seg_4891" s="T328">v:pred</ta>
            <ta e="T331" id="Seg_4892" s="T330">np.h:S</ta>
            <ta e="T334" id="Seg_4893" s="T331">s:adv</ta>
            <ta e="T336" id="Seg_4894" s="T334">v:pred</ta>
            <ta e="T339" id="Seg_4895" s="T336">s:temp</ta>
            <ta e="T341" id="Seg_4896" s="T340">pro.h:S</ta>
            <ta e="T344" id="Seg_4897" s="T341">s:adv</ta>
            <ta e="T345" id="Seg_4898" s="T344">v:pred</ta>
            <ta e="T347" id="Seg_4899" s="T346">pro.h:S</ta>
            <ta e="T348" id="Seg_4900" s="T347">n:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T3" id="Seg_4901" s="T2">new</ta>
            <ta e="T6" id="Seg_4902" s="T5">accs-inf</ta>
            <ta e="T7" id="Seg_4903" s="T6">giv-inactive</ta>
            <ta e="T10" id="Seg_4904" s="T9">new</ta>
            <ta e="T13" id="Seg_4905" s="T12">giv-active</ta>
            <ta e="T14" id="Seg_4906" s="T13">new</ta>
            <ta e="T16" id="Seg_4907" s="T15">giv-active-Q</ta>
            <ta e="T18" id="Seg_4908" s="T17">accs-inf-Q</ta>
            <ta e="T20" id="Seg_4909" s="T19">0.quot-sp</ta>
            <ta e="T21" id="Seg_4910" s="T20">0.giv-active-Q</ta>
            <ta e="T24" id="Seg_4911" s="T23">giv-active</ta>
            <ta e="T25" id="Seg_4912" s="T24">giv-active</ta>
            <ta e="T27" id="Seg_4913" s="T26">0.giv-active</ta>
            <ta e="T29" id="Seg_4914" s="T28">giv-active</ta>
            <ta e="T36" id="Seg_4915" s="T35">0.giv-active</ta>
            <ta e="T37" id="Seg_4916" s="T36">giv-inactive-Q</ta>
            <ta e="T39" id="Seg_4917" s="T38">accs-inf-Q</ta>
            <ta e="T41" id="Seg_4918" s="T40">0.quot-sp</ta>
            <ta e="T42" id="Seg_4919" s="T41">0.giv-active-Q</ta>
            <ta e="T44" id="Seg_4920" s="T43">giv-active</ta>
            <ta e="T45" id="Seg_4921" s="T44">giv-active</ta>
            <ta e="T50" id="Seg_4922" s="T49">giv-active</ta>
            <ta e="T51" id="Seg_4923" s="T50">0.giv-active</ta>
            <ta e="T56" id="Seg_4924" s="T55">new</ta>
            <ta e="T60" id="Seg_4925" s="T59">0.giv-active</ta>
            <ta e="T64" id="Seg_4926" s="T63">0.quot-sp</ta>
            <ta e="T65" id="Seg_4927" s="T64">accs-inf-Q</ta>
            <ta e="T66" id="Seg_4928" s="T65">0.giv-inactive-Q</ta>
            <ta e="T67" id="Seg_4929" s="T66">0.quot-sp</ta>
            <ta e="T68" id="Seg_4930" s="T67">0.giv-active-Q</ta>
            <ta e="T69" id="Seg_4931" s="T68">giv-active</ta>
            <ta e="T76" id="Seg_4932" s="T75">giv-inactive</ta>
            <ta e="T79" id="Seg_4933" s="T78">giv-inactive</ta>
            <ta e="T92" id="Seg_4934" s="T91">new</ta>
            <ta e="T96" id="Seg_4935" s="T95">0.giv-inactive</ta>
            <ta e="T98" id="Seg_4936" s="T97">0.giv-active</ta>
            <ta e="T99" id="Seg_4937" s="T98">giv-inactive</ta>
            <ta e="T102" id="Seg_4938" s="T101">giv-active-Q</ta>
            <ta e="T103" id="Seg_4939" s="T102">accs-aggr-Q</ta>
            <ta e="T104" id="Seg_4940" s="T103">accs-gen-Q</ta>
            <ta e="T107" id="Seg_4941" s="T106">0.quot-sp</ta>
            <ta e="T110" id="Seg_4942" s="T109">giv-active-Q</ta>
            <ta e="T112" id="Seg_4943" s="T111">0.giv-active-Q</ta>
            <ta e="T114" id="Seg_4944" s="T113">giv-inactive</ta>
            <ta e="T115" id="Seg_4945" s="T114">new</ta>
            <ta e="T118" id="Seg_4946" s="T117">giv-inactive</ta>
            <ta e="T119" id="Seg_4947" s="T118">accs-inf</ta>
            <ta e="T120" id="Seg_4948" s="T119">giv-active</ta>
            <ta e="T121" id="Seg_4949" s="T120">new</ta>
            <ta e="T122" id="Seg_4950" s="T121">0.giv-inactive</ta>
            <ta e="T123" id="Seg_4951" s="T122">giv-inactive</ta>
            <ta e="T124" id="Seg_4952" s="T123">quot-sp</ta>
            <ta e="T127" id="Seg_4953" s="T126">giv-inactive-Q</ta>
            <ta e="T129" id="Seg_4954" s="T128">giv-active-Q</ta>
            <ta e="T131" id="Seg_4955" s="T130">0.quot-sp</ta>
            <ta e="T132" id="Seg_4956" s="T131">giv-active</ta>
            <ta e="T133" id="Seg_4957" s="T132">giv-inactive</ta>
            <ta e="T137" id="Seg_4958" s="T136">accs-inf-Q</ta>
            <ta e="T138" id="Seg_4959" s="T137">giv-active-Q</ta>
            <ta e="T139" id="Seg_4960" s="T138">accs-inf-Q</ta>
            <ta e="T142" id="Seg_4961" s="T140">0.giv-active-Q</ta>
            <ta e="T143" id="Seg_4962" s="T142">0.quot-sp</ta>
            <ta e="T147" id="Seg_4963" s="T146">giv-inactive-Q</ta>
            <ta e="T149" id="Seg_4964" s="T148">giv-inactive-Q</ta>
            <ta e="T151" id="Seg_4965" s="T150">quot-sp</ta>
            <ta e="T152" id="Seg_4966" s="T151">giv-active</ta>
            <ta e="T153" id="Seg_4967" s="T152">giv-active</ta>
            <ta e="T154" id="Seg_4968" s="T153">giv-active</ta>
            <ta e="T157" id="Seg_4969" s="T156">giv-inactive</ta>
            <ta e="T159" id="Seg_4970" s="T158">giv-active</ta>
            <ta e="T162" id="Seg_4971" s="T161">giv-active</ta>
            <ta e="T163" id="Seg_4972" s="T162">giv-active</ta>
            <ta e="T165" id="Seg_4973" s="T164">giv-inactive</ta>
            <ta e="T166" id="Seg_4974" s="T165">accs-inf</ta>
            <ta e="T171" id="Seg_4975" s="T170">giv-active</ta>
            <ta e="T175" id="Seg_4976" s="T174">giv-inactive</ta>
            <ta e="T176" id="Seg_4977" s="T175">giv-active</ta>
            <ta e="T179" id="Seg_4978" s="T178">new</ta>
            <ta e="T184" id="Seg_4979" s="T183">giv-active</ta>
            <ta e="T186" id="Seg_4980" s="T185">accs-inf</ta>
            <ta e="T187" id="Seg_4981" s="T186">accs-inf</ta>
            <ta e="T188" id="Seg_4982" s="T187">0.giv-active</ta>
            <ta e="T189" id="Seg_4983" s="T188">giv-active</ta>
            <ta e="T195" id="Seg_4984" s="T194">0.giv-active</ta>
            <ta e="T197" id="Seg_4985" s="T196">giv-active</ta>
            <ta e="T198" id="Seg_4986" s="T197">quot-sp</ta>
            <ta e="T199" id="Seg_4987" s="T198">new-Q</ta>
            <ta e="T200" id="Seg_4988" s="T199">new-Q</ta>
            <ta e="T201" id="Seg_4989" s="T200">new-Q</ta>
            <ta e="T202" id="Seg_4990" s="T201">giv-inactive-Q</ta>
            <ta e="T205" id="Seg_4991" s="T204">0.quot-sp</ta>
            <ta e="T206" id="Seg_4992" s="T205">accs-aggr</ta>
            <ta e="T208" id="Seg_4993" s="T207">giv-inactive</ta>
            <ta e="T211" id="Seg_4994" s="T210">giv-inactive</ta>
            <ta e="T217" id="Seg_4995" s="T216">giv-active</ta>
            <ta e="T218" id="Seg_4996" s="T217">giv-active</ta>
            <ta e="T222" id="Seg_4997" s="T221">new</ta>
            <ta e="T224" id="Seg_4998" s="T223">0.giv-inactive</ta>
            <ta e="T226" id="Seg_4999" s="T225">giv-active</ta>
            <ta e="T227" id="Seg_5000" s="T226">new</ta>
            <ta e="T230" id="Seg_5001" s="T229">giv-active</ta>
            <ta e="T231" id="Seg_5002" s="T230">accs-inf</ta>
            <ta e="T233" id="Seg_5003" s="T232">accs-inf</ta>
            <ta e="T237" id="Seg_5004" s="T236">0.giv-active</ta>
            <ta e="T241" id="Seg_5005" s="T240">0.giv-active</ta>
            <ta e="T246" id="Seg_5006" s="T245">new</ta>
            <ta e="T247" id="Seg_5007" s="T246">0.giv-active</ta>
            <ta e="T249" id="Seg_5008" s="T248">giv-active</ta>
            <ta e="T252" id="Seg_5009" s="T251">giv-active</ta>
            <ta e="T253" id="Seg_5010" s="T252">giv-inactive</ta>
            <ta e="T256" id="Seg_5011" s="T255">giv-active</ta>
            <ta e="T257" id="Seg_5012" s="T256">accs-inf</ta>
            <ta e="T258" id="Seg_5013" s="T257">0.giv-active</ta>
            <ta e="T259" id="Seg_5014" s="T258">accs-inf</ta>
            <ta e="T265" id="Seg_5015" s="T264">0.giv-active</ta>
            <ta e="T266" id="Seg_5016" s="T265">giv-inactive</ta>
            <ta e="T270" id="Seg_5017" s="T269">0.giv-active</ta>
            <ta e="T271" id="Seg_5018" s="T270">giv-inactive</ta>
            <ta e="T276" id="Seg_5019" s="T275">giv-active</ta>
            <ta e="T278" id="Seg_5020" s="T277">accs-inf</ta>
            <ta e="T281" id="Seg_5021" s="T280">giv-active</ta>
            <ta e="T284" id="Seg_5022" s="T283">0.giv-active</ta>
            <ta e="T287" id="Seg_5023" s="T286">giv-inactive</ta>
            <ta e="T291" id="Seg_5024" s="T290">giv-inactive</ta>
            <ta e="T295" id="Seg_5025" s="T294">giv-inactive</ta>
            <ta e="T301" id="Seg_5026" s="T300">accs-inf</ta>
            <ta e="T302" id="Seg_5027" s="T301">accs-inf</ta>
            <ta e="T305" id="Seg_5028" s="T304">giv-inactive</ta>
            <ta e="T307" id="Seg_5029" s="T306">giv-inactive</ta>
            <ta e="T310" id="Seg_5030" s="T309">giv-active</ta>
            <ta e="T312" id="Seg_5031" s="T311">giv-active</ta>
            <ta e="T315" id="Seg_5032" s="T314">giv-active</ta>
            <ta e="T316" id="Seg_5033" s="T315">accs-inf</ta>
            <ta e="T322" id="Seg_5034" s="T321">0.giv-active</ta>
            <ta e="T325" id="Seg_5035" s="T324">giv-inactive</ta>
            <ta e="T327" id="Seg_5036" s="T326">giv-active</ta>
            <ta e="T328" id="Seg_5037" s="T327">giv-inactive</ta>
            <ta e="T331" id="Seg_5038" s="T330">giv-active</ta>
            <ta e="T332" id="Seg_5039" s="T331">giv-inactive</ta>
            <ta e="T342" id="Seg_5040" s="T341">giv-active</ta>
            <ta e="T343" id="Seg_5041" s="T342">giv-inactive</ta>
            <ta e="T345" id="Seg_5042" s="T344">0.giv-active</ta>
            <ta e="T348" id="Seg_5043" s="T347">new</ta>
         </annotation>
         <annotation name="Top" tierref="Top">
            <ta e="T4" id="Seg_5044" s="T3">0.top.int.abstr.</ta>
            <ta e="T5" id="Seg_5045" s="T4">top.ext</ta>
            <ta e="T7" id="Seg_5046" s="T6">top.int.concr</ta>
            <ta e="T13" id="Seg_5047" s="T12">top.int.concr</ta>
            <ta e="T21" id="Seg_5048" s="T20">0.top.int.concr</ta>
            <ta e="T22" id="Seg_5049" s="T21">top.int.concr</ta>
            <ta e="T27" id="Seg_5050" s="T26">0.top.int.concr</ta>
            <ta e="T29" id="Seg_5051" s="T28">top.int.concr</ta>
            <ta e="T34" id="Seg_5052" s="T30">top.int.concr</ta>
            <ta e="T42" id="Seg_5053" s="T41">0.top.int.concr</ta>
            <ta e="T43" id="Seg_5054" s="T42">top.int.concr</ta>
            <ta e="T51" id="Seg_5055" s="T50">0.top.int.concr</ta>
            <ta e="T58" id="Seg_5056" s="T52">top.int.concr</ta>
            <ta e="T61" id="Seg_5057" s="T60">top.int.concr</ta>
            <ta e="T68" id="Seg_5058" s="T67">0.top.int.concr</ta>
            <ta e="T69" id="Seg_5059" s="T68">top.int.concr</ta>
            <ta e="T79" id="Seg_5060" s="T78">top.int.concr</ta>
            <ta e="T96" id="Seg_5061" s="T95">0.top.int.concr</ta>
            <ta e="T98" id="Seg_5062" s="T96">top.int.concr</ta>
            <ta e="T114" id="Seg_5063" s="T113">top.int.concr</ta>
            <ta e="T122" id="Seg_5064" s="T121">0.top.int.concr</ta>
            <ta e="T123" id="Seg_5065" s="T122">top.int.concr</ta>
            <ta e="T132" id="Seg_5066" s="T131">top.int.concr</ta>
            <ta e="T139" id="Seg_5067" s="T138">top.int.concr</ta>
            <ta e="T142" id="Seg_5068" s="T140">0.top.int.concr</ta>
            <ta e="T154" id="Seg_5069" s="T153">top.int.concr</ta>
            <ta e="T157" id="Seg_5070" s="T156">top.int.concr</ta>
            <ta e="T162" id="Seg_5071" s="T161">top.int.concr</ta>
            <ta e="T171" id="Seg_5072" s="T170">top.int.concr</ta>
            <ta e="T175" id="Seg_5073" s="T174">top.int.concr</ta>
            <ta e="T185" id="Seg_5074" s="T181">top.int.concr</ta>
            <ta e="T189" id="Seg_5075" s="T188">top.int.concr</ta>
            <ta e="T195" id="Seg_5076" s="T194">0.top.int.concr</ta>
            <ta e="T197" id="Seg_5077" s="T196">top.int.concr</ta>
            <ta e="T206" id="Seg_5078" s="T205">top.int.concr</ta>
            <ta e="T211" id="Seg_5079" s="T210">top.int.concr</ta>
            <ta e="T215" id="Seg_5080" s="T213">top.int.concr</ta>
            <ta e="T226" id="Seg_5081" s="T225">top.int.concr</ta>
            <ta e="T237" id="Seg_5082" s="T236">0.top.int.concr</ta>
            <ta e="T241" id="Seg_5083" s="T240">0.top.int.concr</ta>
            <ta e="T247" id="Seg_5084" s="T246">0.top.int.concr</ta>
            <ta e="T252" id="Seg_5085" s="T251">0.top.int.concr</ta>
            <ta e="T253" id="Seg_5086" s="T252">top.int.concr</ta>
            <ta e="T258" id="Seg_5087" s="T257">0.top.int.concr</ta>
            <ta e="T259" id="Seg_5088" s="T258">top.int.concr</ta>
            <ta e="T265" id="Seg_5089" s="T264">0.top.int.concr</ta>
            <ta e="T266" id="Seg_5090" s="T265">top.int.concr</ta>
            <ta e="T270" id="Seg_5091" s="T268">top.int.concr</ta>
            <ta e="T276" id="Seg_5092" s="T275">top.int.concr</ta>
            <ta e="T284" id="Seg_5093" s="T283">0.top.int.concr</ta>
            <ta e="T286" id="Seg_5094" s="T284">top.int.concr</ta>
            <ta e="T293" id="Seg_5095" s="T292">top.int.concr</ta>
            <ta e="T301" id="Seg_5096" s="T299">top.int.concr</ta>
            <ta e="T305" id="Seg_5097" s="T304">top.int.concr</ta>
            <ta e="T309" id="Seg_5098" s="T308">0.top.int.concr</ta>
            <ta e="T310" id="Seg_5099" s="T309">top.int.concr</ta>
            <ta e="T319" id="Seg_5100" s="T318">0.top.int.concr</ta>
            <ta e="T322" id="Seg_5101" s="T321">0.top.int.concr</ta>
            <ta e="T325" id="Seg_5102" s="T324">top.int.concr</ta>
            <ta e="T331" id="Seg_5103" s="T330">top.int.concr</ta>
            <ta e="T339" id="Seg_5104" s="T336">top.int.concr</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc">
            <ta e="T4" id="Seg_5105" s="T0">foc.wid</ta>
            <ta e="T6" id="Seg_5106" s="T5">foc.nar</ta>
            <ta e="T10" id="Seg_5107" s="T7">foc.int</ta>
            <ta e="T14" id="Seg_5108" s="T13">foc.nar</ta>
            <ta e="T18" id="Seg_5109" s="T17">foc.nar</ta>
            <ta e="T21" id="Seg_5110" s="T20">foc.int</ta>
            <ta e="T26" id="Seg_5111" s="T21">foc.wid</ta>
            <ta e="T27" id="Seg_5112" s="T26">foc.int</ta>
            <ta e="T30" id="Seg_5113" s="T29">foc.int</ta>
            <ta e="T36" id="Seg_5114" s="T34">foc.wid</ta>
            <ta e="T40" id="Seg_5115" s="T37">foc.int</ta>
            <ta e="T42" id="Seg_5116" s="T41">foc.int</ta>
            <ta e="T47" id="Seg_5117" s="T42">foc.wid</ta>
            <ta e="T50" id="Seg_5118" s="T48">foc.nar</ta>
            <ta e="T60" id="Seg_5119" s="T52">foc.wid</ta>
            <ta e="T64" id="Seg_5120" s="T62">foc.int</ta>
            <ta e="T66" id="Seg_5121" s="T64">foc.int</ta>
            <ta e="T68" id="Seg_5122" s="T67">foc.int</ta>
            <ta e="T71" id="Seg_5123" s="T69">foc.int</ta>
            <ta e="T78" id="Seg_5124" s="T71">foc.wid</ta>
            <ta e="T84" id="Seg_5125" s="T79">foc.int</ta>
            <ta e="T89" id="Seg_5126" s="T84">foc.int</ta>
            <ta e="T94" id="Seg_5127" s="T89">foc.int</ta>
            <ta e="T96" id="Seg_5128" s="T94">foc.int</ta>
            <ta e="T100" id="Seg_5129" s="T98">foc.wid</ta>
            <ta e="T106" id="Seg_5130" s="T102">foc.wid</ta>
            <ta e="T112" id="Seg_5131" s="T109">foc.ver</ta>
            <ta e="T115" id="Seg_5132" s="T114">foc.nar</ta>
            <ta e="T121" id="Seg_5133" s="T119">foc.nar</ta>
            <ta e="T124" id="Seg_5134" s="T123">foc.int</ta>
            <ta e="T130" id="Seg_5135" s="T127">foc.int</ta>
            <ta e="T135" id="Seg_5136" s="T132">foc.int</ta>
            <ta e="T140" id="Seg_5137" s="T139">foc.int</ta>
            <ta e="T142" id="Seg_5138" s="T140">foc.int</ta>
            <ta e="T147" id="Seg_5139" s="T146">foc.contr</ta>
            <ta e="T156" id="Seg_5140" s="T154">foc.int</ta>
            <ta e="T161" id="Seg_5141" s="T157">foc.int</ta>
            <ta e="T169" id="Seg_5142" s="T162">foc.int</ta>
            <ta e="T174" id="Seg_5143" s="T172">foc.int</ta>
            <ta e="T181" id="Seg_5144" s="T175">foc.int</ta>
            <ta e="T188" id="Seg_5145" s="T185">foc.wid</ta>
            <ta e="T194" id="Seg_5146" s="T189">foc.int</ta>
            <ta e="T195" id="Seg_5147" s="T194">foc.int</ta>
            <ta e="T198" id="Seg_5148" s="T197">foc.int</ta>
            <ta e="T204" id="Seg_5149" s="T201">foc.int</ta>
            <ta e="T210" id="Seg_5150" s="T206">foc.int</ta>
            <ta e="T213" id="Seg_5151" s="T211">foc.int</ta>
            <ta e="T224" id="Seg_5152" s="T213">foc.wid</ta>
            <ta e="T229" id="Seg_5153" s="T226">foc.int</ta>
            <ta e="T237" id="Seg_5154" s="T229">foc.int</ta>
            <ta e="T241" id="Seg_5155" s="T237">foc.int</ta>
            <ta e="T246" id="Seg_5156" s="T244">foc.nar</ta>
            <ta e="T252" id="Seg_5157" s="T247">foc.int</ta>
            <ta e="T255" id="Seg_5158" s="T253">foc.int</ta>
            <ta e="T258" id="Seg_5159" s="T255">foc.int</ta>
            <ta e="T264" id="Seg_5160" s="T259">foc.int</ta>
            <ta e="T265" id="Seg_5161" s="T264">foc.int</ta>
            <ta e="T268" id="Seg_5162" s="T266">foc.int</ta>
            <ta e="T274" id="Seg_5163" s="T270">foc.wid</ta>
            <ta e="T279" id="Seg_5164" s="T277">foc.int</ta>
            <ta e="T284" id="Seg_5165" s="T279">foc.int</ta>
            <ta e="T291" id="Seg_5166" s="T284">foc.wid</ta>
            <ta e="T298" id="Seg_5167" s="T291">foc.wid</ta>
            <ta e="T304" id="Seg_5168" s="T298">foc.wid</ta>
            <ta e="T306" id="Seg_5169" s="T305">foc.int</ta>
            <ta e="T309" id="Seg_5170" s="T306">foc.int</ta>
            <ta e="T314" id="Seg_5171" s="T310">foc.int</ta>
            <ta e="T319" id="Seg_5172" s="T314">foc.int</ta>
            <ta e="T322" id="Seg_5173" s="T319">foc.int</ta>
            <ta e="T329" id="Seg_5174" s="T326">foc.int</ta>
            <ta e="T336" id="Seg_5175" s="T331">foc.int</ta>
            <ta e="T345" id="Seg_5176" s="T339">foc.wid</ta>
         </annotation>
         <annotation name="BOR" tierref="TIE0" />
         <annotation name="BOR-Phon" tierref="TIE2" />
         <annotation name="BOR-Morph" tierref="TIE3" />
         <annotation name="CS" tierref="TIE4" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_5177" s="T0">Well, there was one old man.</ta>
            <ta e="T6" id="Seg_5178" s="T4">Ukukuut-Chukukuut was his name.</ta>
            <ta e="T10" id="Seg_5179" s="T6">This man had very many children.</ta>
            <ta e="T15" id="Seg_5180" s="T10">Well, a fox came to him: </ta>
            <ta e="T21" id="Seg_5181" s="T15">"Old man, bring me one child", it said, "I will eat it."</ta>
            <ta e="T27" id="Seg_5182" s="T21">This old man gave a child to it, he shall eat it.</ta>
            <ta e="T36" id="Seg_5183" s="T27">Well, the fox went away, several days later it came back again.</ta>
            <ta e="T42" id="Seg_5184" s="T36">"Old man, bring one more child", it said, "I will eat it."</ta>
            <ta e="T47" id="Seg_5185" s="T42">The old man gave a child [to it] again. </ta>
            <ta e="T52" id="Seg_5186" s="T47">It carried away also this child to eat it.</ta>
            <ta e="T60" id="Seg_5187" s="T52">After several days it came back again.</ta>
            <ta e="T64" id="Seg_5188" s="T60">It came and said again: </ta>
            <ta e="T68" id="Seg_5189" s="T64">"Bring me a child", it said, "I will eat it."</ta>
            <ta e="T71" id="Seg_5190" s="T68">There were no children left.</ta>
            <ta e="T78" id="Seg_5191" s="T71">Well, the fox went away and dissapeared.</ta>
            <ta e="T84" id="Seg_5192" s="T78">The old man lived so and [then] went away.</ta>
            <ta e="T96" id="Seg_5193" s="T84">Well, he walked and walked, he killed a reindeer doe, sat down to fry meat.</ta>
            <ta e="T100" id="Seg_5194" s="T96">He was sitting when the fox came: </ta>
            <ta e="T107" id="Seg_5195" s="T100">"Hey, old man, let us play a child game", it said.</ta>
            <ta e="T112" id="Seg_5196" s="T107">"Well, allright, let us play children."</ta>
            <ta e="T119" id="Seg_5197" s="T112">The old man made a cradle out of the reindeer fur. </ta>
            <ta e="T122" id="Seg_5198" s="T119">They made straps for the cradle.</ta>
            <ta e="T124" id="Seg_5199" s="T122">The old man said: </ta>
            <ta e="T131" id="Seg_5200" s="T124">"Well, swing like a child", he said.</ta>
            <ta e="T135" id="Seg_5201" s="T131">The fox tried to lie down: </ta>
            <ta e="T143" id="Seg_5202" s="T135">"Hey, my friend, my tail hurts, It won't let me lie down", it said.</ta>
            <ta e="T153" id="Seg_5203" s="T143"> "Well, you are a human, lie down, be a child", the fox said to the old man.</ta>
            <ta e="T156" id="Seg_5204" s="T153">Well, the old man lay down.</ta>
            <ta e="T161" id="Seg_5205" s="T156">The fox tied him.</ta>
            <ta e="T169" id="Seg_5206" s="T161">The fox sat swinging, chewing the wild meat and fidding the old man.</ta>
            <ta e="T174" id="Seg_5207" s="T169">The old man fell asleep.</ta>
            <ta e="T181" id="Seg_5208" s="T174">The fox carried him to the slope and threw him down.</ta>
            <ta e="T188" id="Seg_5209" s="T181">As the old man woke up later, he was lying in the gorge.</ta>
            <ta e="T196" id="Seg_5210" s="T188">The old man couldn't move since he was tied up.</ta>
            <ta e="T198" id="Seg_5211" s="T196">The old man called: </ta>
            <ta e="T205" id="Seg_5212" s="T198">"Ermines, polar foxes, mice, gnaw through the strings!", he says.</ta>
            <ta e="T213" id="Seg_5213" s="T205">They came, gnawed through the string, the old man jumped to his feet.</ta>
            <ta e="T224" id="Seg_5214" s="T213">After that he killed a reindeer doe and gave it to those who had gnawed the strings. </ta>
            <ta e="T229" id="Seg_5215" s="T224">The old man came to his campfire.</ta>
            <ta e="T237" id="Seg_5216" s="T229">He strewed the whole ash from his campfire into his trousers and went on.</ta>
            <ta e="T247" id="Seg_5217" s="T237">Well, he went on and on, and came to one house.</ta>
            <ta e="T255" id="Seg_5218" s="T247">As he came up to that house, he tried to hark: the fox began to shamanize.</ta>
            <ta e="T264" id="Seg_5219" s="T255">He came inside the house, there the foxes gathered together and started shamanizing.</ta>
            <ta e="T265" id="Seg_5220" s="T264">They have their shaman.</ta>
            <ta e="T268" id="Seg_5221" s="T265">The old man came in and sat down.</ta>
            <ta e="T274" id="Seg_5222" s="T268">He was sitting as the fox came back from its shamanizing.</ta>
            <ta e="T279" id="Seg_5223" s="T274">The old man suddenly beat the shaman's drum.</ta>
            <ta e="T284" id="Seg_5224" s="T279">He was beating the shaman's drum and jumping.</ta>
            <ta e="T291" id="Seg_5225" s="T284">There the ash started to rise out of his trousers.</ta>
            <ta e="T298" id="Seg_5226" s="T291">All the foxes started to laugh about it.</ta>
            <ta e="T304" id="Seg_5227" s="T298">Suddenly one foxes' laugh was heard out of the corner.</ta>
            <ta e="T319" id="Seg_5228" s="T304">The old man heard it, let the shaman's drum fall, caught the fox and lashed it frying it on the fire.</ta>
            <ta e="T322" id="Seg_5229" s="T319">He killed him while lashing.</ta>
            <ta e="T329" id="Seg_5230" s="T322">Well, that fox had eaten his children first.</ta>
            <ta e="T336" id="Seg_5231" s="T329">It had tied him up in the cradle and threw him down.</ta>
            <ta e="T345" id="Seg_5232" s="T336">After that it had gone to the old man's house and lived there.</ta>
            <ta e="T348" id="Seg_5233" s="T345">Well, that's the end.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_5234" s="T0">Es lebte einmal ein alter Mann.</ta>
            <ta e="T6" id="Seg_5235" s="T4">Ukukuut-Tschukukuut war sein Name.</ta>
            <ta e="T10" id="Seg_5236" s="T6">Dieser hatte sehr viele Kinder.</ta>
            <ta e="T15" id="Seg_5237" s="T10">Da kam zu ihm ein Fuchs: </ta>
            <ta e="T21" id="Seg_5238" s="T15">"Alter Mann, hol ein Kind von dir", sagt er, "ich werde es fressen."</ta>
            <ta e="T27" id="Seg_5239" s="T21">Darauf gab der alte Mann [ihm] ein Kind, er soll es fressen.</ta>
            <ta e="T36" id="Seg_5240" s="T27">Jener ging weg, nach einigen Tagen kam er wieder.</ta>
            <ta e="T42" id="Seg_5241" s="T36">"Alter Mann, hol noch ein Kind", sagt er, "ich werde es fressen."</ta>
            <ta e="T47" id="Seg_5242" s="T42">Darauf gab [ihm] der alte Mann wieder ein Kind.</ta>
            <ta e="T52" id="Seg_5243" s="T47">Auch dieses Kind trug er weg, um es zu fressen.</ta>
            <ta e="T60" id="Seg_5244" s="T52">Nach einigen Tagen kam er dann wieder.</ta>
            <ta e="T64" id="Seg_5245" s="T60">Er kommt und sagt wieder: </ta>
            <ta e="T68" id="Seg_5246" s="T64">"Hol mir ein Kind", sagt er, "ich werde es fressen."</ta>
            <ta e="T71" id="Seg_5247" s="T68">Es waren keine Kinder mehr da.</ta>
            <ta e="T78" id="Seg_5248" s="T71">Da ging der Fuchs weg und verschwand.</ta>
            <ta e="T84" id="Seg_5249" s="T78">Der alte Mann konnte so leben und er ging los.</ta>
            <ta e="T96" id="Seg_5250" s="T84">Er ging und ging, er tötete eine wilde Rentierkuh, setze sich und briet sie.</ta>
            <ta e="T100" id="Seg_5251" s="T96">Als er so saß, kam der Fuchs: </ta>
            <ta e="T107" id="Seg_5252" s="T100">"Hey, alter Mann, lass uns Kinder spielen", sagte er.</ta>
            <ta e="T112" id="Seg_5253" s="T107">"Nun, in Ordnung, spielen wir Kinder."</ta>
            <ta e="T119" id="Seg_5254" s="T112">Der alte Mann machte aus dem Fell von Rentierkühen eine Wiege.</ta>
            <ta e="T122" id="Seg_5255" s="T119">Sie machten Riemen für die Wiege.</ta>
            <ta e="T124" id="Seg_5256" s="T122">Der alte Mann sagte: </ta>
            <ta e="T131" id="Seg_5257" s="T124">"Nun also, schaukel wie ein Kind", sagte er.</ta>
            <ta e="T135" id="Seg_5258" s="T131">Der Fuchs versuchte sich hinzulegen: </ta>
            <ta e="T143" id="Seg_5259" s="T135">"Hey, mein Freund, mein Schwanz tut weh, man kann ihn nicht hinlegen", sagte er.</ta>
            <ta e="T153" id="Seg_5260" s="T143">"Du bist doch ein Mensch, leg du dich hin und sei das Kind", sagte der Fuchs zum alten Mann.</ta>
            <ta e="T156" id="Seg_5261" s="T153">Der alte Mann legte sich also hin.</ta>
            <ta e="T161" id="Seg_5262" s="T156">Da band der Fuchs ihn fest.</ta>
            <ta e="T169" id="Seg_5263" s="T161">Er sitzt, schaukelt den alten Mann, kaut das Fleisch des wilden Rentiers und füttert [ihn].</ta>
            <ta e="T174" id="Seg_5264" s="T169">Dann schlief der alte Mann ein.</ta>
            <ta e="T181" id="Seg_5265" s="T174">Der Fuchs zog ihn [fort] und warf ihn einen Abhang hinunter.</ta>
            <ta e="T188" id="Seg_5266" s="T181">Als der alte Mann später aufwachte, lag er in der Schlucht.</ta>
            <ta e="T196" id="Seg_5267" s="T188">Der alte Mann kann sich nirgendwohin bewegen, er ist doch angebunden.</ta>
            <ta e="T198" id="Seg_5268" s="T196">Der alte Mann ruft: </ta>
            <ta e="T205" id="Seg_5269" s="T198">"Hermeline, Polarfüchse, Mäuse, nagt meine Riemen durch!", sagt er.</ta>
            <ta e="T213" id="Seg_5270" s="T205">Diese kamen und nagten die Riemen durch, der alte Mann sprang auf.</ta>
            <ta e="T224" id="Seg_5271" s="T213">Danach tötete eine wilde Rentierkuh und gab sie denen, die die Riemen durchgenacht hatten.</ta>
            <ta e="T229" id="Seg_5272" s="T224">Der alte Mann kam zu seinem Lagerfeuer.</ta>
            <ta e="T237" id="Seg_5273" s="T229">Er streute die ganze Asche aus dem Lagerfeuer in seine Hose und ging los.</ta>
            <ta e="T247" id="Seg_5274" s="T237">Er ging los, er ging und ging, er kam an zu einem Haus.</ta>
            <ta e="T255" id="Seg_5275" s="T247">Als er zu diesem Haus kam, versuchte er zu lauschen: Der Fuchs fängt an zu schamanisieren.</ta>
            <ta e="T264" id="Seg_5276" s="T255">Er ging ins Haus hinein, dort hatten sich alle Füchse versammelt und fingen an zu schamanisieren.</ta>
            <ta e="T265" id="Seg_5277" s="T264">Sie haben einen Schamanen.</ta>
            <ta e="T268" id="Seg_5278" s="T265">Der alte Mann ging hinein und setzte sich.</ta>
            <ta e="T274" id="Seg_5279" s="T268">Wie er dasaß, kam der Fuchs von seiner Schamanenreise zurück.</ta>
            <ta e="T279" id="Seg_5280" s="T274">Dieser alte Mann schlug plötzlich auf die Schamanentrommel.</ta>
            <ta e="T284" id="Seg_5281" s="T279">Er schlug auf die Schamanentrommel und sprang dabei.</ta>
            <ta e="T291" id="Seg_5282" s="T284">Da flog die Asche aus seiner Hose. </ta>
            <ta e="T298" id="Seg_5283" s="T291">Darauf lachten alle Füchse zusammen.</ta>
            <ta e="T304" id="Seg_5284" s="T298">Plötzlich ist dort aus der Ecke das Lachen eines Fuchses zu hören.</ta>
            <ta e="T319" id="Seg_5285" s="T304">Das hörte der alte Mann, er warf die Schamanentrommel fort, der alte Mann griff diesen Fuchs, er briet ihn auf dem Feuer und schlug ihn.</ta>
            <ta e="T322" id="Seg_5286" s="T319">Er schlug und schlug und erschlug ihn.</ta>
            <ta e="T329" id="Seg_5287" s="T322">Schließlich hatte der Fuchs zuerst seine Kinder gefressen.</ta>
            <ta e="T336" id="Seg_5288" s="T329">Dieser Fuchs hatte ihn in einer Wiege festgebunden und [in eine Schlucht] geworfen.</ta>
            <ta e="T345" id="Seg_5289" s="T336">Danach war der Fuchs in das Haus dieses alten Mannes gegangen und lebte dort.</ta>
            <ta e="T348" id="Seg_5290" s="T345">Das ist das Ende.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_5291" s="T0">Вот живет один старик.</ta>
            <ta e="T6" id="Seg_5292" s="T4">Укукуут-Чукукуут — имя его.</ta>
            <ta e="T10" id="Seg_5293" s="T6">У него много детей.</ta>
            <ta e="T15" id="Seg_5294" s="T10">Вот приходит к нему лиса: </ta>
            <ta e="T21" id="Seg_5295" s="T15">"Старик, принеси одного ребенка", говорит, "съем."</ta>
            <ta e="T27" id="Seg_5296" s="T21">Этот старик дал ребенка на съедение.</ta>
            <ta e="T36" id="Seg_5297" s="T27">Вот ушла, через несколько дней опять приходит.</ta>
            <ta e="T42" id="Seg_5298" s="T36">"Старик, дай еще ребенка", говорит, "съем."</ta>
            <ta e="T47" id="Seg_5299" s="T42">Старик опять дал своего ребенка.</ta>
            <ta e="T52" id="Seg_5300" s="T47">Вот и того ребенка унесла, чтобы съесть.</ta>
            <ta e="T60" id="Seg_5301" s="T52">Через несколько дней опять пришла.</ta>
            <ta e="T64" id="Seg_5302" s="T60">Придя, снова говорит: </ta>
            <ta e="T68" id="Seg_5303" s="T64">"Дай ребенка", говорит, "съем."</ta>
            <ta e="T71" id="Seg_5304" s="T68">Детей больше не осталось.</ta>
            <ta e="T78" id="Seg_5305" s="T71">Вот ушла лиса и пропала.</ta>
            <ta e="T84" id="Seg_5306" s="T78">Старик пожил-пожил, и пошел себе.</ta>
            <ta e="T96" id="Seg_5307" s="T84">Вот так шел-шел, потом, убив важенку дикого оленя, сидел и жарил [мяса].</ta>
            <ta e="T100" id="Seg_5308" s="T96">Когда так сидел, лиса подошла: </ta>
            <ta e="T107" id="Seg_5309" s="T100">"Ну, старик, давай играть в детей", сказала.</ta>
            <ta e="T112" id="Seg_5310" s="T107">"Ну, ладно, поиграем в детей."</ta>
            <ta e="T119" id="Seg_5311" s="T112">Этот старик из шкуры важенки сделал колыбель.</ta>
            <ta e="T122" id="Seg_5312" s="T119">Приготовили ремни для колыбели.</ta>
            <ta e="T124" id="Seg_5313" s="T122">Старик сказал: </ta>
            <ta e="T131" id="Seg_5314" s="T124">"Вот так, ты укачивайся вместо ребенка", сказал.</ta>
            <ta e="T135" id="Seg_5315" s="T131">Лиса попробовала лечь: </ta>
            <ta e="T143" id="Seg_5316" s="T135">"Э-э, приятель, у меня хвост болит, не дает улечься", сказала.</ta>
            <ta e="T153" id="Seg_5317" s="T143">"Ты же человек, ты и ложись, будь ребенком", сказала лиса старику.</ta>
            <ta e="T156" id="Seg_5318" s="T153">Вот старик ложится.</ta>
            <ta e="T161" id="Seg_5319" s="T156">Вот лиса его обвязывает [ремнями].</ta>
            <ta e="T169" id="Seg_5320" s="T161">Сидит и, укачивая, пережевывает мясо дикого [оленя] и кормит старика.</ta>
            <ta e="T174" id="Seg_5321" s="T169">Вот старик уснул.</ta>
            <ta e="T181" id="Seg_5322" s="T174">Лиса подтащила его к буераку и столкнула вниз.</ta>
            <ta e="T188" id="Seg_5323" s="T181">Когда проснулся старик — лежит в буераке.</ta>
            <ta e="T196" id="Seg_5324" s="T188">Старик не может никуда двинуться, он же связанный.</ta>
            <ta e="T198" id="Seg_5325" s="T196">Старик кличет: </ta>
            <ta e="T205" id="Seg_5326" s="T198">"Горностаи, песцы, мыши, перегрызите ремни на мне!", говорит.</ta>
            <ta e="T213" id="Seg_5327" s="T205">Те прибежали и перегрызли на нем ремни, старик вскочил на ноги.</ta>
            <ta e="T224" id="Seg_5328" s="T213">Затем убил одну нагульную важенку дикого [оленя] и отдал тем, кто перегрыз ремни.</ta>
            <ta e="T229" id="Seg_5329" s="T224">Пришел старик к своему кострищу.</ta>
            <ta e="T237" id="Seg_5330" s="T229">Весь пепел от костра насыпал в штаны и пошел.</ta>
            <ta e="T247" id="Seg_5331" s="T237">Вот пошел, шел-шел, тут к одному дому подошел.</ta>
            <ta e="T255" id="Seg_5332" s="T247">Подойдя к этому дому, прислушался: лиса начинает камлать.</ta>
            <ta e="T264" id="Seg_5333" s="T255">Вошел в дом: там все лисы, собравшись, начинают камлать.</ta>
            <ta e="T265" id="Seg_5334" s="T264">У них есть шаман.</ta>
            <ta e="T268" id="Seg_5335" s="T265">Вошел старик и сел.</ta>
            <ta e="T274" id="Seg_5336" s="T268">Когда так сидел, [камлавшая] лиса вернулась из шаманского странствия.</ta>
            <ta e="T279" id="Seg_5337" s="T274">Этот старик вдруг в бубен ударил.</ta>
            <ta e="T284" id="Seg_5338" s="T279">Так ударяя в бубен, стал прыгать.</ta>
            <ta e="T291" id="Seg_5339" s="T284">Тут из его штанов стал выбиваться пепел.</ta>
            <ta e="T298" id="Seg_5340" s="T291">Все лисы засмеялись.</ta>
            <ta e="T304" id="Seg_5341" s="T298">Вдруг там в углу послышался лисий смешок.</ta>
            <ta e="T319" id="Seg_5342" s="T304">Услышал старик, отбросил бубен, схватил ту лису и, поджаривая на огне, начал хлестать.</ta>
            <ta e="T322" id="Seg_5343" s="T319">Хлестал-хлестал и убил.</ta>
            <ta e="T329" id="Seg_5344" s="T322">Ведь та лиса сначала съела его детей.</ta>
            <ta e="T336" id="Seg_5345" s="T329">Та лиса, обвязав его в колыбели, столкнула [в буерак].</ta>
            <ta e="T345" id="Seg_5346" s="T336">Так сделав, [лиса] в дом этого старика вошел и жила там.</ta>
            <ta e="T348" id="Seg_5347" s="T345">На этом вот конец.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_5348" s="T0">Вот живет один старик,</ta>
            <ta e="T6" id="Seg_5349" s="T4">Укукуут-Чукукуут — имя его.</ta>
            <ta e="T10" id="Seg_5350" s="T6">У него много детей.</ta>
            <ta e="T15" id="Seg_5351" s="T10">Вот приходит к нему лиса: </ta>
            <ta e="T21" id="Seg_5352" s="T15">— Старик, дай одного ребенка, — говорит, — съесть хочу.</ta>
            <ta e="T27" id="Seg_5353" s="T21">Этот старик дает ребенка на съедение.</ta>
            <ta e="T36" id="Seg_5354" s="T27">Вот ушла, через несколько дней опять приходит.</ta>
            <ta e="T42" id="Seg_5355" s="T36">— Старик, дай еще ребенка, — говорит, — съесть хочу.</ta>
            <ta e="T47" id="Seg_5356" s="T42">Старик опять дал своего ребенка.</ta>
            <ta e="T52" id="Seg_5357" s="T47">Вот и того ребенка унесла, чтобы съесть.</ta>
            <ta e="T60" id="Seg_5358" s="T52">Через несколько дней опять пришла.</ta>
            <ta e="T64" id="Seg_5359" s="T60">Придя, снова говорит: </ta>
            <ta e="T68" id="Seg_5360" s="T64">— Дай ребенка, — говорит, — съесть хочу.</ta>
            <ta e="T71" id="Seg_5361" s="T68">Детей больше не осталось.</ta>
            <ta e="T78" id="Seg_5362" s="T71">Вот ушла лиса и пропала.</ta>
            <ta e="T84" id="Seg_5363" s="T78">Старик пожил-пожил, пока не надоело, и пошел себе.</ta>
            <ta e="T96" id="Seg_5364" s="T84">Вот так шел-шел, потом, убив жирную нагульную важенку дикого [оленя], сидел и жарил [на рожне] мяса.</ta>
            <ta e="T100" id="Seg_5365" s="T96">Когда так сидел, лиса подошла: </ta>
            <ta e="T107" id="Seg_5366" s="T100">— Ну, старик, давай играть в детей, — сказала.</ta>
            <ta e="T112" id="Seg_5367" s="T107">— Ну, ладно, поиграем в детей.</ta>
            <ta e="T119" id="Seg_5368" s="T112">Этот старик из шкуры важенки сделал колыбель.</ta>
            <ta e="T122" id="Seg_5369" s="T119">Приготовили ремни для колыбели.</ta>
            <ta e="T124" id="Seg_5370" s="T122">Старик сказал: </ta>
            <ta e="T131" id="Seg_5371" s="T124">— Ну, приятель, ты укачивайся вместо ребенка, — сказал.</ta>
            <ta e="T135" id="Seg_5372" s="T131">Лиса попробовала лечь: </ta>
            <ta e="T143" id="Seg_5373" s="T135">— Э-э, приятель, у меня хвост болит, не дает улечься, — сказала.</ta>
            <ta e="T153" id="Seg_5374" s="T143">— Ты же человек, ты и ложись, будь ребенком, — сказала лиса старику.</ta>
            <ta e="T156" id="Seg_5375" s="T153">Вот старик ложится.</ta>
            <ta e="T161" id="Seg_5376" s="T156">Вот лиса его обвязывает [ремнями].</ta>
            <ta e="T169" id="Seg_5377" s="T161">Сидит и, укачивая, пережевывает мясо дикого [оленя] и кормит старика.</ta>
            <ta e="T174" id="Seg_5378" s="T169">Вот старик уснул.</ta>
            <ta e="T181" id="Seg_5379" s="T174">Лиса подтащила его к буераку и столкнула вниз.</ta>
            <ta e="T188" id="Seg_5380" s="T181">Когда проснулся старик — лежит в буераке.</ta>
            <ta e="T196" id="Seg_5381" s="T188">Связанный старик, не может никуда двинуться.</ta>
            <ta e="T198" id="Seg_5382" s="T196">Старик кличет: ‎‎</ta>
            <ta e="T205" id="Seg_5383" s="T198">— Горностаи, песцы, мыши, перегрызите ремни на мне! — говорит.</ta>
            <ta e="T213" id="Seg_5384" s="T205">Те прибежали и перегрызли на нем ремни, старик вскочил на ноги.</ta>
            <ta e="T224" id="Seg_5385" s="T213">Затем убил одну нагульную важенку дикого [оленя] и отдал тем, кто перегрыз ремни.</ta>
            <ta e="T229" id="Seg_5386" s="T224">Пришел старик к своему кострищу.</ta>
            <ta e="T237" id="Seg_5387" s="T229">Весь пепел от костра насыпал в штаны и пошел.</ta>
            <ta e="T247" id="Seg_5388" s="T237">Вот пошел, шел-шел, тут к одному дому подошел.</ta>
            <ta e="T255" id="Seg_5389" s="T247">Подойдя к этому дому, прислушался: лиса начинает камлать.</ta>
            <ta e="T264" id="Seg_5390" s="T255">Вошел в дом: там все лисы, собравшись, слушают камлание.</ta>
            <ta e="T265" id="Seg_5391" s="T264">У них лиса-шаман.</ta>
            <ta e="T268" id="Seg_5392" s="T265">Вошел старик и сел.</ta>
            <ta e="T274" id="Seg_5393" s="T268">Когда так сидел, [камлавшая] лиса вернулась из шаманского странствия.</ta>
            <ta e="T279" id="Seg_5394" s="T274">Этот старик вдруг в бубен ударил.</ta>
            <ta e="T284" id="Seg_5395" s="T279">Так ударяя в бубен, стал прыгать.</ta>
            <ta e="T291" id="Seg_5396" s="T284">Тут из его штанов стал клубами выбиваться пепел.</ta>
            <ta e="T298" id="Seg_5397" s="T291">Все лисы засмеялись.</ta>
            <ta e="T304" id="Seg_5398" s="T298">Вдруг там в углу послышался лисий смешок.</ta>
            <ta e="T319" id="Seg_5399" s="T304">Услыхал старик, отбросил бубен, схватил ту лису и, поджаривая на огне, начал хлестать.</ta>
            <ta e="T322" id="Seg_5400" s="T319">Хлестал-хлестал и убил.</ta>
            <ta e="T329" id="Seg_5401" s="T322">Ведь та лиса сначала съела его детей.</ta>
            <ta e="T336" id="Seg_5402" s="T329">Та лиса, обвязав его в колыбели, столкнула в буерак.</ta>
            <ta e="T345" id="Seg_5403" s="T336">Вот так сделав, этот старик возвратился домой и стал жить.</ta>
            <ta e="T348" id="Seg_5404" s="T345">На этом вот конец.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T36" id="Seg_5405" s="T27">[DCh]: literally 'after having overnighted'</ta>
            <ta e="T264" id="Seg_5406" s="T255">[DCh]: Apparently not all foxes shamanized, but their shaman, and the others listened.</ta>
            <ta e="T274" id="Seg_5407" s="T268">[DCh]: Shamanizing the shaman moves to an upper world.</ta>
            <ta e="T345" id="Seg_5408" s="T336">[DCh]: The given Russian translation implies that not the fox came earlier to the house of this old man, but the old man himself now. This is apparently also logical in the context but can not be derived from the Dolgan text. </ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
