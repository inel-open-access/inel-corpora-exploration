<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID5B10B06B-95F5-8170-B296-8CA8C615BEFC">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>PoNA_2004_SnowOwl_flk</transcription-name>
         <referenced-file url="PoNA_2004_SnowOwl_flk.wav" />
         <referenced-file url="PoNA_2004_SnowOwl_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\PoNA_2004_SnowOwl_flk\PoNA_2004_SnowOwl_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">429</ud-information>
            <ud-information attribute-name="# HIAT:w">307</ud-information>
            <ud-information attribute-name="# e">307</ud-information>
            <ud-information attribute-name="# HIAT:u">53</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PoNA">
            <abbreviation>PoNA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="1.24" type="appl" />
         <tli id="T2" time="2.009" type="appl" />
         <tli id="T3" time="2.778" type="appl" />
         <tli id="T4" time="3.546" type="appl" />
         <tli id="T5" time="4.388332250777491" />
         <tli id="T6" time="5.121" type="appl" />
         <tli id="T7" time="5.928" type="appl" />
         <tli id="T8" time="6.734" type="appl" />
         <tli id="T9" time="7.626664902386585" />
         <tli id="T10" time="8.382" type="appl" />
         <tli id="T11" time="9.224" type="appl" />
         <tli id="T12" time="10.065" type="appl" />
         <tli id="T13" time="10.907" type="appl" />
         <tli id="T14" time="11.749" type="appl" />
         <tli id="T15" time="12.591" type="appl" />
         <tli id="T16" time="13.432" type="appl" />
         <tli id="T17" time="14.274" type="appl" />
         <tli id="T18" time="15.209331481654448" />
         <tli id="T19" time="15.815" type="appl" />
         <tli id="T20" time="16.514" type="appl" />
         <tli id="T21" time="17.212" type="appl" />
         <tli id="T22" time="17.911" type="appl" />
         <tli id="T23" time="18.53666784354973" />
         <tli id="T24" time="19.408" type="appl" />
         <tli id="T25" time="20.205" type="appl" />
         <tli id="T26" time="21.002" type="appl" />
         <tli id="T27" time="21.8" type="appl" />
         <tli id="T28" time="22.598" type="appl" />
         <tli id="T29" time="23.395" type="appl" />
         <tli id="T30" time="24.312" type="appl" />
         <tli id="T31" time="25.229" type="appl" />
         <tli id="T32" time="26.146" type="appl" />
         <tli id="T33" time="27.063" type="appl" />
         <tli id="T34" time="27.913335001928207" />
         <tli id="T35" time="28.699" type="appl" />
         <tli id="T36" time="29.418" type="appl" />
         <tli id="T37" time="30.210332945952462" />
         <tli id="T38" time="30.84" type="appl" />
         <tli id="T39" time="31.542" type="appl" />
         <tli id="T40" time="32.244" type="appl" />
         <tli id="T41" time="32.947" type="appl" />
         <tli id="T42" time="33.649" type="appl" />
         <tli id="T43" time="34.45866376301534" />
         <tli id="T44" time="35.283" type="appl" />
         <tli id="T45" time="36.214" type="appl" />
         <tli id="T46" time="37.178334015847106" />
         <tli id="T47" time="37.901" type="appl" />
         <tli id="T48" time="38.657" type="appl" />
         <tli id="T49" time="39.413" type="appl" />
         <tli id="T50" time="40.169" type="appl" />
         <tli id="T51" time="40.926" type="appl" />
         <tli id="T52" time="41.682" type="appl" />
         <tli id="T53" time="42.438" type="appl" />
         <tli id="T54" time="43.194" type="appl" />
         <tli id="T55" time="43.95" type="appl" />
         <tli id="T56" time="44.81933218618564" />
         <tli id="T57" time="45.478" type="appl" />
         <tli id="T58" time="46.25" type="appl" />
         <tli id="T59" time="47.021" type="appl" />
         <tli id="T60" time="47.87966433442997" />
         <tli id="T61" time="48.618" type="appl" />
         <tli id="T62" time="49.444" type="appl" />
         <tli id="T63" time="50.269" type="appl" />
         <tli id="T64" time="51.094" type="appl" />
         <tli id="T65" time="51.92" type="appl" />
         <tli id="T66" time="52.745" type="appl" />
         <tli id="T67" time="53.594" type="appl" />
         <tli id="T68" time="54.442" type="appl" />
         <tli id="T69" time="55.291" type="appl" />
         <tli id="T70" time="56.14" type="appl" />
         <tli id="T71" time="56.712" type="appl" />
         <tli id="T72" time="57.283" type="appl" />
         <tli id="T73" time="57.855" type="appl" />
         <tli id="T74" time="58.427" type="appl" />
         <tli id="T75" time="58.998" type="appl" />
         <tli id="T76" time="59.57" type="appl" />
         <tli id="T77" time="60.311" type="appl" />
         <tli id="T78" time="61.051" type="appl" />
         <tli id="T79" time="61.792" type="appl" />
         <tli id="T80" time="62.532" type="appl" />
         <tli id="T81" time="63.273" type="appl" />
         <tli id="T82" time="64.013" type="appl" />
         <tli id="T83" time="64.754" type="appl" />
         <tli id="T84" time="65.051" type="appl" />
         <tli id="T85" time="65.348" type="appl" />
         <tli id="T86" time="65.646" type="appl" />
         <tli id="T87" time="65.943" type="appl" />
         <tli id="T88" time="66.24" type="appl" />
         <tli id="T89" time="69.4388947451086" />
         <tli id="T90" time="70.248" type="appl" />
         <tli id="T91" time="70.815" type="appl" />
         <tli id="T92" time="71.383" type="appl" />
         <tli id="T93" time="71.951" type="appl" />
         <tli id="T94" time="72.518" type="appl" />
         <tli id="T95" time="73.086" type="appl" />
         <tli id="T96" time="73.654" type="appl" />
         <tli id="T97" time="74.221" type="appl" />
         <tli id="T98" time="74.789" type="appl" />
         <tli id="T99" time="75.415" type="appl" />
         <tli id="T100" time="76.041" type="appl" />
         <tli id="T101" time="76.668" type="appl" />
         <tli id="T102" time="77.294" type="appl" />
         <tli id="T103" time="77.73333954582472" />
         <tli id="T104" time="78.613" type="appl" />
         <tli id="T105" time="79.306" type="appl" />
         <tli id="T106" time="79.998" type="appl" />
         <tli id="T107" time="80.691" type="appl" />
         <tli id="T108" time="81.384" type="appl" />
         <tli id="T109" time="82.12366680065514" />
         <tli id="T110" time="82.749" type="appl" />
         <tli id="T111" time="83.42" type="appl" />
         <tli id="T112" time="84.092" type="appl" />
         <tli id="T113" time="84.764" type="appl" />
         <tli id="T114" time="85.435" type="appl" />
         <tli id="T115" time="86.107" type="appl" />
         <tli id="T116" time="86.778" type="appl" />
         <tli id="T117" time="87.29667039975456" />
         <tli id="T118" time="88.069" type="appl" />
         <tli id="T119" time="88.688" type="appl" />
         <tli id="T120" time="89.307" type="appl" />
         <tli id="T121" time="89.926" type="appl" />
         <tli id="T122" time="90.545" type="appl" />
         <tli id="T123" time="91.15733160608232" />
         <tli id="T124" time="91.902" type="appl" />
         <tli id="T125" time="92.64" type="appl" />
         <tli id="T126" time="93.377" type="appl" />
         <tli id="T127" time="94.16166529669255" />
         <tli id="T128" time="94.903" type="appl" />
         <tli id="T129" time="95.691" type="appl" />
         <tli id="T130" time="96.51233100628173" />
         <tli id="T131" time="97.50999741814954" />
         <tli id="T132" time="98.345" type="appl" />
         <tli id="T133" time="99.14" type="appl" />
         <tli id="T134" time="99.935" type="appl" />
         <tli id="T135" time="100.73" type="appl" />
         <tli id="T136" time="101.525" type="appl" />
         <tli id="T137" time="102.32" type="appl" />
         <tli id="T138" time="103.115" type="appl" />
         <tli id="T139" time="103.91" type="appl" />
         <tli id="T140" time="104.705" type="appl" />
         <tli id="T141" time="105.5" type="appl" />
         <tli id="T142" time="106.295" type="appl" />
         <tli id="T143" time="107.25666519766608" />
         <tli id="T144" time="107.453" type="appl" />
         <tli id="T145" time="107.816" type="appl" />
         <tli id="T146" time="108.179" type="appl" />
         <tli id="T147" time="108.541" type="appl" />
         <tli id="T148" time="108.904" type="appl" />
         <tli id="T149" time="109.267" type="appl" />
         <tli id="T150" time="111.09823165584052" />
         <tli id="T151" time="111.884" type="appl" />
         <tli id="T152" time="112.683" type="appl" />
         <tli id="T153" time="113.482" type="appl" />
         <tli id="T154" time="114.28" type="appl" />
         <tli id="T155" time="114.76485714285714" type="intp" />
         <tli id="T156" time="115.24971428571428" type="intp" />
         <tli id="T157" time="115.73457142857143" type="intp" />
         <tli id="T158" time="116.21942857142858" type="intp" />
         <tli id="T159" time="116.70428571428572" type="intp" />
         <tli id="T160" time="117.14247138752455" />
         <tli id="T161" time="117.674" type="appl" />
         <tli id="T162" time="118.088" type="appl" />
         <tli id="T163" time="118.502" type="appl" />
         <tli id="T164" time="118.917" type="appl" />
         <tli id="T165" time="119.331" type="appl" />
         <tli id="T166" time="119.7783278702486" />
         <tli id="T167" time="120.28" type="appl" />
         <tli id="T168" time="120.815" type="appl" />
         <tli id="T169" time="121.35" type="appl" />
         <tli id="T170" time="121.885" type="appl" />
         <tli id="T171" time="122.42" type="appl" />
         <tli id="T172" time="122.955" type="appl" />
         <tli id="T173" time="123.49" type="appl" />
         <tli id="T174" time="125.65133334829174" />
         <tli id="T175" time="126.34" type="appl" />
         <tli id="T176" time="126.991" type="appl" />
         <tli id="T177" time="127.641" type="appl" />
         <tli id="T178" time="128.291" type="appl" />
         <tli id="T179" time="128.942" type="appl" />
         <tli id="T180" time="129.592" type="appl" />
         <tli id="T181" time="130.243" type="appl" />
         <tli id="T182" time="131.03965904095514" />
         <tli id="T183" time="131.66" type="appl" />
         <tli id="T184" time="132.428" type="appl" />
         <tli id="T185" time="133.195" type="appl" />
         <tli id="T186" time="133.963" type="appl" />
         <tli id="T187" time="134.73667050833637" />
         <tli id="T188" time="135.52557142857142" type="intp" />
         <tli id="T189" time="136.32114285714286" type="intp" />
         <tli id="T190" time="137.1167142857143" type="intp" />
         <tli id="T191" time="137.91228571428573" type="intp" />
         <tli id="T192" time="138.70785714285716" type="intp" />
         <tli id="T193" time="139.83009204080565" />
         <tli id="T194" time="140.299" type="appl" />
         <tli id="T195" time="140.801" type="appl" />
         <tli id="T196" time="141.303" type="appl" />
         <tli id="T197" time="141.804" type="appl" />
         <tli id="T198" time="142.306" type="appl" />
         <tli id="T199" time="142.808" type="appl" />
         <tli id="T200" time="143.2033326199628" />
         <tli id="T201" time="143.896" type="appl" />
         <tli id="T202" time="144.482" type="appl" />
         <tli id="T203" time="145.068" type="appl" />
         <tli id="T204" time="145.8139941912128" />
         <tli id="T205" time="146.31" type="appl" />
         <tli id="T206" time="146.967" type="appl" />
         <tli id="T207" time="147.624" type="appl" />
         <tli id="T208" time="148.41332781776907" />
         <tli id="T209" time="149.06833333333333" type="intp" />
         <tli id="T210" time="149.85666666666665" type="intp" />
         <tli id="T211" time="150.64499999999998" type="intp" />
         <tli id="T212" time="151.4333333333333" type="intp" />
         <tli id="T213" time="152.35499684928328" />
         <tli id="T214" time="153.01" type="appl" />
         <tli id="T215" time="153.58" type="appl" />
         <tli id="T216" time="154.151" type="appl" />
         <tli id="T217" time="156.33751158482545" />
         <tli id="T218" time="156.907" type="appl" />
         <tli id="T219" time="157.634" type="appl" />
         <tli id="T220" time="158.36" type="appl" />
         <tli id="T221" time="159.053" type="appl" />
         <tli id="T222" time="159.747" type="appl" />
         <tli id="T223" time="160.44" type="appl" />
         <tli id="T224" time="161.133" type="appl" />
         <tli id="T225" time="161.827" type="appl" />
         <tli id="T226" time="162.52" type="appl" />
         <tli id="T227" time="163.084" type="appl" />
         <tli id="T228" time="163.648" type="appl" />
         <tli id="T229" time="164.212" type="appl" />
         <tli id="T230" time="164.776" type="appl" />
         <tli id="T231" time="165.47999627275615" />
         <tli id="T232" time="166.24" type="appl" />
         <tli id="T233" time="167.14" type="appl" />
         <tli id="T234" time="168.04" type="appl" />
         <tli id="T235" time="168.94" type="appl" />
         <tli id="T236" time="169.84" type="appl" />
         <tli id="T237" time="170.74" type="appl" />
         <tli id="T238" time="171.79999984428972" />
         <tli id="T239" time="172.446" type="appl" />
         <tli id="T240" time="173.252" type="appl" />
         <tli id="T241" time="174.058" type="appl" />
         <tli id="T242" time="174.864" type="appl" />
         <tli id="T243" time="175.83666736371845" />
         <tli id="T244" time="176.5882" type="intp" />
         <tli id="T245" time="177.50639999999999" type="intp" />
         <tli id="T246" time="178.4246" type="intp" />
         <tli id="T247" time="179.50946046658197" />
         <tli id="T248" time="180.261" type="appl" />
         <tli id="T249" time="180.862" type="appl" />
         <tli id="T250" time="181.463" type="appl" />
         <tli id="T251" time="182.064" type="appl" />
         <tli id="T252" time="182.664" type="appl" />
         <tli id="T253" time="183.265" type="appl" />
         <tli id="T254" time="183.866" type="appl" />
         <tli id="T255" time="184.467" type="appl" />
         <tli id="T256" time="185.068" type="appl" />
         <tli id="T257" time="185.9623265066846" />
         <tli id="T258" time="186.523" type="appl" />
         <tli id="T259" time="187.377" type="appl" />
         <tli id="T260" time="188.232" type="appl" />
         <tli id="T261" time="189.086" type="appl" />
         <tli id="T262" time="189.94" type="appl" />
         <tli id="T263" time="190.544" type="appl" />
         <tli id="T264" time="191.148" type="appl" />
         <tli id="T265" time="191.752" type="appl" />
         <tli id="T266" time="192.356" type="appl" />
         <tli id="T267" time="193.6835838162097" />
         <tli id="T268" time="193.933" type="appl" />
         <tli id="T269" time="194.906" type="appl" />
         <tli id="T270" time="195.878" type="appl" />
         <tli id="T271" time="196.851" type="appl" />
         <tli id="T272" time="198.37732421038064" />
         <tli id="T273" time="198.568" type="appl" />
         <tli id="T274" time="199.312" type="appl" />
         <tli id="T275" time="200.056" type="appl" />
         <tli id="T276" time="200.801" type="appl" />
         <tli id="T277" time="201.545" type="appl" />
         <tli id="T278" time="202.289" type="appl" />
         <tli id="T279" time="203.2063358889463" />
         <tli id="T280" time="203.644" type="appl" />
         <tli id="T281" time="204.254" type="appl" />
         <tli id="T282" time="204.865" type="appl" />
         <tli id="T283" time="205.475" type="appl" />
         <tli id="T284" time="206.086" type="appl" />
         <tli id="T285" time="206.696" type="appl" />
         <tli id="T286" time="207.307" type="appl" />
         <tli id="T287" time="207.917" type="appl" />
         <tli id="T288" time="208.73465935327326" />
         <tli id="T289" time="209.206" type="appl" />
         <tli id="T290" time="209.883" type="appl" />
         <tli id="T291" time="210.561" type="appl" />
         <tli id="T292" time="211.239" type="appl" />
         <tli id="T293" time="211.917" type="appl" />
         <tli id="T294" time="212.594" type="appl" />
         <tli id="T295" time="213.272" type="appl" />
         <tli id="T296" time="213.95" type="appl" />
         <tli id="T297" time="214.628" type="appl" />
         <tli id="T298" time="215.305" type="appl" />
         <tli id="T299" time="216.08299291110424" />
         <tli id="T300" time="216.746" type="appl" />
         <tli id="T301" time="217.509" type="appl" />
         <tli id="T302" time="218.272" type="appl" />
         <tli id="T303" time="219.035" type="appl" />
         <tli id="T304" time="219.799" type="appl" />
         <tli id="T305" time="220.562" type="appl" />
         <tli id="T306" time="221.325" type="appl" />
         <tli id="T307" time="222.088" type="appl" />
         <tli id="T308" time="222.851" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PoNA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T308" id="Seg_0" n="sc" s="T0">
               <ts e="T1" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Leŋkej</ts>
                  <nts id="Seg_5" n="HIAT:ip">.</nts>
                  <nts id="Seg_6" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T5" id="Seg_8" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_10" n="HIAT:w" s="T1">Ol</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_13" n="HIAT:w" s="T2">aːta</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">dulgaːn-haka</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">oloŋkoto</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_23" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_25" n="HIAT:w" s="T5">Bɨlɨr-bɨlɨr</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">mu͡ora</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">ičiges</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">ete</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_38" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_40" n="HIAT:w" s="T9">Barɨ</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">kötördör</ts>
                  <nts id="Seg_44" n="HIAT:ip">,</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_47" n="HIAT:w" s="T11">čɨːčaːktar</ts>
                  <nts id="Seg_48" n="HIAT:ip">,</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_51" n="HIAT:w" s="T12">kaːstar</ts>
                  <nts id="Seg_52" n="HIAT:ip">,</nts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_55" n="HIAT:w" s="T13">kustar</ts>
                  <nts id="Seg_56" n="HIAT:ip">,</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_59" n="HIAT:w" s="T14">haltaːkiːlar</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_62" n="HIAT:w" s="T15">bu</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_65" n="HIAT:w" s="T16">hirge</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_68" n="HIAT:w" s="T17">olorbuttara</ts>
                  <nts id="Seg_69" n="HIAT:ip">.</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_72" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_74" n="HIAT:w" s="T18">Oloru</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_77" n="HIAT:w" s="T19">gɨtta</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_80" n="HIAT:w" s="T20">biːrge</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">leŋkej</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">olorbuta</ts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_90" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_92" n="HIAT:w" s="T23">Gini</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_95" n="HIAT:w" s="T24">ahɨːr</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25">astara</ts>
                  <nts id="Seg_99" n="HIAT:ip">–</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_102" n="HIAT:w" s="T26">kaːstar</ts>
                  <nts id="Seg_103" n="HIAT:ip">,</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_106" n="HIAT:w" s="T27">kustar</ts>
                  <nts id="Seg_107" n="HIAT:ip">,</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_110" n="HIAT:w" s="T28">kabi͡ekaːttar</ts>
                  <nts id="Seg_111" n="HIAT:ip">.</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_114" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_116" n="HIAT:w" s="T29">Barɨ</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_119" n="HIAT:w" s="T30">kötördör</ts>
                  <nts id="Seg_120" n="HIAT:ip">,</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_123" n="HIAT:w" s="T31">čɨːčaːktar</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_126" n="HIAT:w" s="T32">ulakannara</ts>
                  <nts id="Seg_127" n="HIAT:ip">–</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_130" n="HIAT:w" s="T33">kaːs</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_134" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_136" n="HIAT:w" s="T34">Onton</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_139" n="HIAT:w" s="T35">tɨmnɨːlar</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_142" n="HIAT:w" s="T36">bu͡olbuttara</ts>
                  <nts id="Seg_143" n="HIAT:ip">.</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_146" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_148" n="HIAT:w" s="T37">Hübehit</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_151" n="HIAT:w" s="T38">kaːs</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_154" n="HIAT:w" s="T39">barɨ</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_157" n="HIAT:w" s="T40">kötördörü</ts>
                  <nts id="Seg_158" n="HIAT:ip">,</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_161" n="HIAT:w" s="T41">čɨːčaːktarɨ</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_164" n="HIAT:w" s="T42">komujbuta</ts>
                  <nts id="Seg_165" n="HIAT:ip">.</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_168" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_170" n="HIAT:w" s="T43">Ulakan</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_173" n="HIAT:w" s="T44">hübelehiː</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_176" n="HIAT:w" s="T45">bu͡olbuta</ts>
                  <nts id="Seg_177" n="HIAT:ip">.</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_180" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_182" n="HIAT:w" s="T46">Kötördör</ts>
                  <nts id="Seg_183" n="HIAT:ip">,</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_186" n="HIAT:w" s="T47">čɨːčaːktar</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_189" n="HIAT:w" s="T48">komullan</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_192" n="HIAT:w" s="T49">ki͡eŋ</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_195" n="HIAT:w" s="T50">alɨːga</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_198" n="HIAT:w" s="T51">onno</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_201" n="HIAT:w" s="T52">hu͡opka</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_204" n="HIAT:w" s="T53">annɨtɨgar</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_207" n="HIAT:w" s="T54">tu͡olan</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_210" n="HIAT:w" s="T55">olorbuttara</ts>
                  <nts id="Seg_211" n="HIAT:ip">.</nts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_214" n="HIAT:u" s="T56">
                  <ts e="T57" id="Seg_216" n="HIAT:w" s="T56">Leŋkej</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_219" n="HIAT:w" s="T57">emi͡e</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_222" n="HIAT:w" s="T58">munnʼakka</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_225" n="HIAT:w" s="T59">kelbite</ts>
                  <nts id="Seg_226" n="HIAT:ip">.</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_229" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_231" n="HIAT:w" s="T60">Hübehit</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_234" n="HIAT:w" s="T61">kaːs</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_237" n="HIAT:w" s="T62">östörü</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_240" n="HIAT:w" s="T63">bejetin</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_243" n="HIAT:w" s="T64">dʼonugar</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_246" n="HIAT:w" s="T65">haŋarbɨta</ts>
                  <nts id="Seg_247" n="HIAT:ip">.</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_250" n="HIAT:u" s="T66">
                  <nts id="Seg_251" n="HIAT:ip">"</nts>
                  <ts e="T67" id="Seg_253" n="HIAT:w" s="T66">Körögüt</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_256" n="HIAT:w" s="T67">du͡o</ts>
                  <nts id="Seg_257" n="HIAT:ip">,</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_260" n="HIAT:w" s="T68">kallaːmmɨt</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_263" n="HIAT:w" s="T69">tɨmnɨjda</ts>
                  <nts id="Seg_264" n="HIAT:ip">.</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_267" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_269" n="HIAT:w" s="T70">Manna</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_272" n="HIAT:w" s="T71">bihigi</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_275" n="HIAT:w" s="T72">hajɨn</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_278" n="HIAT:w" s="T73">ire</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_281" n="HIAT:w" s="T74">oloror</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_284" n="HIAT:w" s="T75">bu͡olu͡okput</ts>
                  <nts id="Seg_285" n="HIAT:ip">.</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_288" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_290" n="HIAT:w" s="T76">Min</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_293" n="HIAT:w" s="T77">hiri</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_296" n="HIAT:w" s="T78">bilebin</ts>
                  <nts id="Seg_297" n="HIAT:ip">,</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_300" n="HIAT:w" s="T79">kanna</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_303" n="HIAT:w" s="T80">kɨhɨn</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_306" n="HIAT:w" s="T81">bu͡olaːččɨta</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_309" n="HIAT:w" s="T82">hu͡ok</ts>
                  <nts id="Seg_310" n="HIAT:ip">.</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_313" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_315" n="HIAT:w" s="T83">Ol</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_318" n="HIAT:w" s="T84">olus</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_321" n="HIAT:w" s="T85">ɨraːk</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_324" n="HIAT:w" s="T86">hirinen</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_327" n="HIAT:w" s="T87">bardakka</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_330" n="HIAT:w" s="T88">tijillibet</ts>
                  <nts id="Seg_331" n="HIAT:ip">.</nts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_334" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_336" n="HIAT:w" s="T89">Bihigi</ts>
                  <nts id="Seg_337" n="HIAT:ip">,</nts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_339" n="HIAT:ip">(</nts>
                  <ts e="T92" id="Seg_341" n="HIAT:w" s="T90">kɨna-</ts>
                  <nts id="Seg_342" n="HIAT:ip">)</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_345" n="HIAT:w" s="T92">kɨnattaːktar</ts>
                  <nts id="Seg_346" n="HIAT:ip">,</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_349" n="HIAT:w" s="T93">bu</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_352" n="HIAT:w" s="T94">hirge</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_355" n="HIAT:w" s="T95">togo</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_358" n="HIAT:w" s="T96">toŋo</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_361" n="HIAT:w" s="T97">hɨtɨ͡akpɨtɨj</ts>
                  <nts id="Seg_362" n="HIAT:ip">?</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_365" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_367" n="HIAT:w" s="T98">Dʼe</ts>
                  <nts id="Seg_368" n="HIAT:ip">,</nts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_371" n="HIAT:w" s="T99">ol</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_374" n="HIAT:w" s="T100">hirge</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_377" n="HIAT:w" s="T101">könötük</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_380" n="HIAT:w" s="T102">kötü͡ögüŋ</ts>
                  <nts id="Seg_381" n="HIAT:ip">!</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_384" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_386" n="HIAT:w" s="T103">Bu</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_389" n="HIAT:w" s="T104">tɨmnɨːlar</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_392" n="HIAT:w" s="T105">aːhɨ͡aktarɨgar</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_395" n="HIAT:w" s="T106">di͡eri</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_398" n="HIAT:w" s="T107">onno</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_401" n="HIAT:w" s="T108">oloru͡okput</ts>
                  <nts id="Seg_402" n="HIAT:ip">.</nts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_405" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_407" n="HIAT:w" s="T109">Anɨ</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_410" n="HIAT:w" s="T110">manna</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_413" n="HIAT:w" s="T111">bu</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_416" n="HIAT:w" s="T112">kaːllakpɨtɨna</ts>
                  <nts id="Seg_417" n="HIAT:ip">,</nts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_420" n="HIAT:w" s="T113">barɨbɨt</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_423" n="HIAT:w" s="T114">kam</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_426" n="HIAT:w" s="T115">toŋon</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_429" n="HIAT:w" s="T116">ölü͡ökpüt</ts>
                  <nts id="Seg_430" n="HIAT:ip">.</nts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_433" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_435" n="HIAT:w" s="T117">Ol</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_438" n="HIAT:w" s="T118">hirge</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_441" n="HIAT:w" s="T119">kötön</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_444" n="HIAT:w" s="T120">bardakpɨtɨna</ts>
                  <nts id="Seg_445" n="HIAT:ip">,</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_448" n="HIAT:w" s="T121">kihi</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_451" n="HIAT:w" s="T122">bu͡olu͡okput</ts>
                  <nts id="Seg_452" n="HIAT:ip">.</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_455" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_457" n="HIAT:w" s="T123">Kirdik</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_460" n="HIAT:w" s="T124">haŋarabɨn</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_463" n="HIAT:w" s="T125">du͡o</ts>
                  <nts id="Seg_464" n="HIAT:ip">,</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_467" n="HIAT:w" s="T126">kɨnattaːktar</ts>
                  <nts id="Seg_468" n="HIAT:ip">?</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_471" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_473" n="HIAT:w" s="T127">Kim</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_476" n="HIAT:w" s="T128">ajannɨːrɨ</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_479" n="HIAT:w" s="T129">höbülüːr</ts>
                  <nts id="Seg_480" n="HIAT:ip">?</nts>
                  <nts id="Seg_481" n="HIAT:ip">"</nts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_484" n="HIAT:u" s="T130">
                  <nts id="Seg_485" n="HIAT:ip">"</nts>
                  <ts e="T131" id="Seg_487" n="HIAT:w" s="T130">Kirdik</ts>
                  <nts id="Seg_488" n="HIAT:ip">.</nts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_491" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_493" n="HIAT:w" s="T131">Manna</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_496" n="HIAT:w" s="T132">kaːllakpɨtɨna</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_499" n="HIAT:w" s="T133">ölü͡ökpüt</ts>
                  <nts id="Seg_500" n="HIAT:ip">,</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_503" n="HIAT:w" s="T134">bihigi</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_506" n="HIAT:w" s="T135">barɨbɨt</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_509" n="HIAT:w" s="T136">onno</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_512" n="HIAT:w" s="T137">kötü͡ökpüt</ts>
                  <nts id="Seg_513" n="HIAT:ip">"</nts>
                  <nts id="Seg_514" n="HIAT:ip">,</nts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_517" n="HIAT:w" s="T138">di͡en</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_520" n="HIAT:w" s="T139">barɨ</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_523" n="HIAT:w" s="T140">kötördör</ts>
                  <nts id="Seg_524" n="HIAT:ip">,</nts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_527" n="HIAT:w" s="T141">čɨːčaːktar</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_530" n="HIAT:w" s="T142">ü͡ögülespittere</ts>
                  <nts id="Seg_531" n="HIAT:ip">.</nts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_534" n="HIAT:u" s="T143">
                  <nts id="Seg_535" n="HIAT:ip">"</nts>
                  <ts e="T144" id="Seg_537" n="HIAT:w" s="T143">Min</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_540" n="HIAT:w" s="T144">hin</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_543" n="HIAT:w" s="T145">ol</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_546" n="HIAT:w" s="T146">di͡ek</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_549" n="HIAT:w" s="T147">kötü͡öm</ts>
                  <nts id="Seg_550" n="HIAT:ip">"</nts>
                  <nts id="Seg_551" n="HIAT:ip">,</nts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_554" n="HIAT:w" s="T148">leŋkej</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_557" n="HIAT:w" s="T149">di͡ebite</ts>
                  <nts id="Seg_558" n="HIAT:ip">.</nts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_561" n="HIAT:u" s="T150">
                  <ts e="T151" id="Seg_563" n="HIAT:w" s="T150">Hübehit</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_566" n="HIAT:w" s="T151">kaːs</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_569" n="HIAT:w" s="T152">dogottoruttan</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_572" n="HIAT:w" s="T153">ɨjɨppɨta</ts>
                  <nts id="Seg_573" n="HIAT:ip">:</nts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_576" n="HIAT:u" s="T154">
                  <nts id="Seg_577" n="HIAT:ip">"</nts>
                  <ts e="T155" id="Seg_579" n="HIAT:w" s="T154">Bu</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_582" n="HIAT:w" s="T155">leŋkej</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_585" n="HIAT:w" s="T156">bihigini</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_588" n="HIAT:w" s="T157">gɨtta</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_591" n="HIAT:w" s="T158">barsaːrɨ</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_594" n="HIAT:w" s="T159">gɨnar</ts>
                  <nts id="Seg_595" n="HIAT:ip">.</nts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T166" id="Seg_598" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_600" n="HIAT:w" s="T160">Iti</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_603" n="HIAT:w" s="T161">tuhunan</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_606" n="HIAT:w" s="T162">ehigi</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_609" n="HIAT:w" s="T163">kajtak</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_612" n="HIAT:w" s="T164">diː</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_615" n="HIAT:w" s="T165">hanɨːgɨt</ts>
                  <nts id="Seg_616" n="HIAT:ip">?</nts>
                  <nts id="Seg_617" n="HIAT:ip">"</nts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T174" id="Seg_620" n="HIAT:u" s="T166">
                  <nts id="Seg_621" n="HIAT:ip">"</nts>
                  <ts e="T167" id="Seg_623" n="HIAT:w" s="T166">Bihigi</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_626" n="HIAT:w" s="T167">leŋkeji</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_629" n="HIAT:w" s="T168">bejebitin</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_632" n="HIAT:w" s="T169">kɨtta</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_635" n="HIAT:w" s="T170">ɨlɨ͡akpɨt</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_638" n="HIAT:w" s="T171">hu͡oga</ts>
                  <nts id="Seg_639" n="HIAT:ip">"</nts>
                  <nts id="Seg_640" n="HIAT:ip">,</nts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_643" n="HIAT:w" s="T172">kötördör</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_646" n="HIAT:w" s="T173">di͡ebittere</ts>
                  <nts id="Seg_647" n="HIAT:ip">.</nts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_650" n="HIAT:u" s="T174">
                  <nts id="Seg_651" n="HIAT:ip">"</nts>
                  <ts e="T175" id="Seg_653" n="HIAT:w" s="T174">Ehigi</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_656" n="HIAT:w" s="T175">ginini</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_659" n="HIAT:w" s="T176">togo</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_662" n="HIAT:w" s="T177">ɨlɨmaːrɨ</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_665" n="HIAT:w" s="T178">gɨnagɨt</ts>
                  <nts id="Seg_666" n="HIAT:ip">,</nts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_669" n="HIAT:w" s="T179">gini͡eke</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_672" n="HIAT:w" s="T180">könötük</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_675" n="HIAT:w" s="T181">haŋarɨŋ</ts>
                  <nts id="Seg_676" n="HIAT:ip">"</nts>
                  <nts id="Seg_677" n="HIAT:ip">.</nts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_680" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_682" n="HIAT:w" s="T182">Kötördör</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_685" n="HIAT:w" s="T183">leŋkej</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_688" n="HIAT:w" s="T184">tuhunan</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_691" n="HIAT:w" s="T185">bɨha</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_694" n="HIAT:w" s="T186">haŋarbɨttara</ts>
                  <nts id="Seg_695" n="HIAT:ip">:</nts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_698" n="HIAT:u" s="T187">
                  <nts id="Seg_699" n="HIAT:ip">"</nts>
                  <ts e="T188" id="Seg_701" n="HIAT:w" s="T187">Leŋkej</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_704" n="HIAT:w" s="T188">manna</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_707" n="HIAT:w" s="T189">bihigi</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_710" n="HIAT:w" s="T190">kergetterbitin</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_713" n="HIAT:w" s="T191">hi͡en</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_716" n="HIAT:w" s="T192">oloror</ts>
                  <nts id="Seg_717" n="HIAT:ip">.</nts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_720" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_722" n="HIAT:w" s="T193">Gini</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_725" n="HIAT:w" s="T194">bejetin</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_728" n="HIAT:w" s="T195">mi͡eretin</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_731" n="HIAT:w" s="T196">onno</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_734" n="HIAT:w" s="T197">da</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_737" n="HIAT:w" s="T198">keːhi͡e</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_740" n="HIAT:w" s="T199">hu͡oga</ts>
                  <nts id="Seg_741" n="HIAT:ip">.</nts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_744" n="HIAT:u" s="T200">
                  <ts e="T201" id="Seg_746" n="HIAT:w" s="T200">Bihigi</ts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_749" n="HIAT:w" s="T201">ginini</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_752" n="HIAT:w" s="T202">ɨlɨ͡akpɨt</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_755" n="HIAT:w" s="T203">hu͡oga</ts>
                  <nts id="Seg_756" n="HIAT:ip">"</nts>
                  <nts id="Seg_757" n="HIAT:ip">.</nts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T208" id="Seg_760" n="HIAT:u" s="T204">
                  <ts e="T205" id="Seg_762" n="HIAT:w" s="T204">Hübehit</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_765" n="HIAT:w" s="T205">kaːs</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_768" n="HIAT:w" s="T206">leŋkejge</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_771" n="HIAT:w" s="T207">diːr</ts>
                  <nts id="Seg_772" n="HIAT:ip">:</nts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_775" n="HIAT:u" s="T208">
                  <nts id="Seg_776" n="HIAT:ip">"</nts>
                  <ts e="T209" id="Seg_778" n="HIAT:w" s="T208">Körögün</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_781" n="HIAT:w" s="T209">du͡o</ts>
                  <nts id="Seg_782" n="HIAT:ip">,</nts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_785" n="HIAT:w" s="T210">kɨnattaːk</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_788" n="HIAT:w" s="T211">dʼon</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_791" n="HIAT:w" s="T212">hübeleːbitterin</ts>
                  <nts id="Seg_792" n="HIAT:ip">.</nts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_795" n="HIAT:u" s="T213">
                  <ts e="T214" id="Seg_797" n="HIAT:w" s="T213">Barɨlara</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_800" n="HIAT:w" s="T214">haŋarbɨttara</ts>
                  <nts id="Seg_801" n="HIAT:ip">–</nts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_804" n="HIAT:w" s="T215">iti</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_807" n="HIAT:w" s="T216">hoku͡on</ts>
                  <nts id="Seg_808" n="HIAT:ip">.</nts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_811" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_813" n="HIAT:w" s="T217">En</ts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_816" n="HIAT:w" s="T218">manna</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_819" n="HIAT:w" s="T219">kaːlɨ͡aŋ</ts>
                  <nts id="Seg_820" n="HIAT:ip">"</nts>
                  <nts id="Seg_821" n="HIAT:ip">.</nts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_824" n="HIAT:u" s="T220">
                  <nts id="Seg_825" n="HIAT:ip">"</nts>
                  <ts e="T221" id="Seg_827" n="HIAT:w" s="T220">Kördönön</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_830" n="HIAT:w" s="T221">bütebin</ts>
                  <nts id="Seg_831" n="HIAT:ip">"</nts>
                  <nts id="Seg_832" n="HIAT:ip">,</nts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_835" n="HIAT:w" s="T222">leŋkej</ts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_838" n="HIAT:w" s="T223">di͡ebite</ts>
                  <nts id="Seg_839" n="HIAT:ip">,</nts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_841" n="HIAT:ip">"</nts>
                  <ts e="T225" id="Seg_843" n="HIAT:w" s="T224">manna</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_846" n="HIAT:w" s="T225">kaːlɨ͡am</ts>
                  <nts id="Seg_847" n="HIAT:ip">.</nts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T231" id="Seg_850" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_852" n="HIAT:w" s="T226">Bagar</ts>
                  <nts id="Seg_853" n="HIAT:ip">,</nts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_856" n="HIAT:w" s="T227">min</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_859" n="HIAT:w" s="T228">manna</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_862" n="HIAT:w" s="T229">tɨːnnaːk</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_865" n="HIAT:w" s="T230">oloru͡om</ts>
                  <nts id="Seg_866" n="HIAT:ip">"</nts>
                  <nts id="Seg_867" n="HIAT:ip">.</nts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_870" n="HIAT:u" s="T231">
                  <ts e="T232" id="Seg_872" n="HIAT:w" s="T231">Barɨ</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_875" n="HIAT:w" s="T232">kötördör</ts>
                  <nts id="Seg_876" n="HIAT:ip">,</nts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_879" n="HIAT:w" s="T233">čɨːčaːktar</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_882" n="HIAT:w" s="T234">itiː</ts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_885" n="HIAT:w" s="T235">hiri</ts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_888" n="HIAT:w" s="T236">kördüː</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_891" n="HIAT:w" s="T237">barbɨttara</ts>
                  <nts id="Seg_892" n="HIAT:ip">.</nts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_895" n="HIAT:u" s="T238">
                  <ts e="T239" id="Seg_897" n="HIAT:w" s="T238">Leŋkej</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_900" n="HIAT:w" s="T239">dʼi͡etiger</ts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_903" n="HIAT:w" s="T240">kelen</ts>
                  <nts id="Seg_904" n="HIAT:ip">,</nts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_907" n="HIAT:w" s="T241">emeːksiniger</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_910" n="HIAT:w" s="T242">diːr</ts>
                  <nts id="Seg_911" n="HIAT:ip">:</nts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T247" id="Seg_914" n="HIAT:u" s="T243">
                  <nts id="Seg_915" n="HIAT:ip">"</nts>
                  <ts e="T244" id="Seg_917" n="HIAT:w" s="T243">Bilegin</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_920" n="HIAT:w" s="T244">du͡o</ts>
                  <nts id="Seg_921" n="HIAT:ip">,</nts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_924" n="HIAT:w" s="T245">bihigini</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_927" n="HIAT:w" s="T246">ɨlbattarɨn</ts>
                  <nts id="Seg_928" n="HIAT:ip">?</nts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T257" id="Seg_931" n="HIAT:u" s="T247">
                  <ts e="T248" id="Seg_933" n="HIAT:w" s="T247">Mini͡eke</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_936" n="HIAT:w" s="T248">ičiges</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_939" n="HIAT:w" s="T249">hukuːju</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_942" n="HIAT:w" s="T250">tik</ts>
                  <nts id="Seg_943" n="HIAT:ip">,</nts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_946" n="HIAT:w" s="T251">purgaːga</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_949" n="HIAT:w" s="T252">daː</ts>
                  <nts id="Seg_950" n="HIAT:ip">,</nts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_953" n="HIAT:w" s="T253">tɨmnɨːga</ts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_956" n="HIAT:w" s="T254">daː</ts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_959" n="HIAT:w" s="T255">toŋmot</ts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_962" n="HIAT:w" s="T256">bu͡olu͡okpun</ts>
                  <nts id="Seg_963" n="HIAT:ip">"</nts>
                  <nts id="Seg_964" n="HIAT:ip">.</nts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T262" id="Seg_967" n="HIAT:u" s="T257">
                  <nts id="Seg_968" n="HIAT:ip">"</nts>
                  <ts e="T258" id="Seg_970" n="HIAT:w" s="T257">Tigi͡em</ts>
                  <nts id="Seg_971" n="HIAT:ip">"</nts>
                  <nts id="Seg_972" n="HIAT:ip">,</nts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_975" n="HIAT:w" s="T258">ös</ts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_978" n="HIAT:w" s="T259">kiːrbek</ts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_981" n="HIAT:w" s="T260">emeːksine</ts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_984" n="HIAT:w" s="T261">di͡ebite</ts>
                  <nts id="Seg_985" n="HIAT:ip">.</nts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T267" id="Seg_988" n="HIAT:u" s="T262">
                  <nts id="Seg_989" n="HIAT:ip">"</nts>
                  <ts e="T263" id="Seg_991" n="HIAT:w" s="T262">Hukuːj</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_994" n="HIAT:w" s="T263">kaːr</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_997" n="HIAT:w" s="T264">kördük</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_1000" n="HIAT:w" s="T265">čeːlkeː</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1003" n="HIAT:w" s="T266">bu͡ollun</ts>
                  <nts id="Seg_1004" n="HIAT:ip">"</nts>
                  <nts id="Seg_1005" n="HIAT:ip">.</nts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T272" id="Seg_1008" n="HIAT:u" s="T267">
                  <ts e="T268" id="Seg_1010" n="HIAT:w" s="T267">Leŋkej</ts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1013" n="HIAT:w" s="T268">emeːksine</ts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1016" n="HIAT:w" s="T269">čebis-čeːlkeː</ts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1019" n="HIAT:w" s="T270">hukuːju</ts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1022" n="HIAT:w" s="T271">tikpite</ts>
                  <nts id="Seg_1023" n="HIAT:ip">.</nts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T279" id="Seg_1026" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_1028" n="HIAT:w" s="T272">Leŋkej</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1031" n="HIAT:w" s="T273">hukuːjun</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1034" n="HIAT:w" s="T274">keteːtin</ts>
                  <nts id="Seg_1035" n="HIAT:ip">,</nts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1038" n="HIAT:w" s="T275">ontuta</ts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1041" n="HIAT:w" s="T276">etiger</ts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1044" n="HIAT:w" s="T277">hɨstan</ts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1047" n="HIAT:w" s="T278">kaːlbɨta</ts>
                  <nts id="Seg_1048" n="HIAT:ip">.</nts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T288" id="Seg_1051" n="HIAT:u" s="T279">
                  <ts e="T280" id="Seg_1053" n="HIAT:w" s="T279">Leŋkej</ts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1056" n="HIAT:w" s="T280">čɨskaːn</ts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1059" n="HIAT:w" s="T281">tɨmnɨːga</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1062" n="HIAT:w" s="T282">daː</ts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1065" n="HIAT:w" s="T283">toŋmot</ts>
                  <nts id="Seg_1066" n="HIAT:ip">,</nts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1069" n="HIAT:w" s="T284">küːsteːk</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1072" n="HIAT:w" s="T285">purgaːga</ts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1075" n="HIAT:w" s="T286">daː</ts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1078" n="HIAT:w" s="T287">ɨllarbat</ts>
                  <nts id="Seg_1079" n="HIAT:ip">.</nts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T299" id="Seg_1082" n="HIAT:u" s="T288">
                  <ts e="T289" id="Seg_1084" n="HIAT:w" s="T288">Hol</ts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1087" n="HIAT:w" s="T289">kemten</ts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1090" n="HIAT:w" s="T290">tuːs</ts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1093" n="HIAT:w" s="T291">mu͡oraga</ts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1096" n="HIAT:w" s="T292">leŋkej</ts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1099" n="HIAT:w" s="T293">manna</ts>
                  <nts id="Seg_1100" n="HIAT:ip">,</nts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1103" n="HIAT:w" s="T294">kanna</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1106" n="HIAT:w" s="T295">da</ts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1109" n="HIAT:w" s="T296">köppökkö</ts>
                  <nts id="Seg_1110" n="HIAT:ip">,</nts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1113" n="HIAT:w" s="T297">kɨhɨlɨː</ts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1116" n="HIAT:w" s="T298">oloror</ts>
                  <nts id="Seg_1117" n="HIAT:ip">.</nts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T308" id="Seg_1120" n="HIAT:u" s="T299">
                  <ts e="T300" id="Seg_1122" n="HIAT:w" s="T299">Leŋkej</ts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1125" n="HIAT:w" s="T300">urukkutaːgar</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1128" n="HIAT:w" s="T301">daː</ts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1131" n="HIAT:w" s="T302">ulakannɨk</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1134" n="HIAT:w" s="T303">kɨra</ts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1137" n="HIAT:w" s="T304">čɨːčaːktarɨ</ts>
                  <nts id="Seg_1138" n="HIAT:ip">,</nts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1141" n="HIAT:w" s="T305">kabi͡ekaːttarɨ</ts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1144" n="HIAT:w" s="T306">hiːr</ts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1147" n="HIAT:w" s="T307">bu͡olbuta</ts>
                  <nts id="Seg_1148" n="HIAT:ip">.</nts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T308" id="Seg_1150" n="sc" s="T0">
               <ts e="T1" id="Seg_1152" n="e" s="T0">Leŋkej. </ts>
               <ts e="T2" id="Seg_1154" n="e" s="T1">Ol </ts>
               <ts e="T3" id="Seg_1156" n="e" s="T2">aːta </ts>
               <ts e="T4" id="Seg_1158" n="e" s="T3">dulgaːn-haka </ts>
               <ts e="T5" id="Seg_1160" n="e" s="T4">oloŋkoto. </ts>
               <ts e="T6" id="Seg_1162" n="e" s="T5">Bɨlɨr-bɨlɨr </ts>
               <ts e="T7" id="Seg_1164" n="e" s="T6">mu͡ora </ts>
               <ts e="T8" id="Seg_1166" n="e" s="T7">ičiges </ts>
               <ts e="T9" id="Seg_1168" n="e" s="T8">ete. </ts>
               <ts e="T10" id="Seg_1170" n="e" s="T9">Barɨ </ts>
               <ts e="T11" id="Seg_1172" n="e" s="T10">kötördör, </ts>
               <ts e="T12" id="Seg_1174" n="e" s="T11">čɨːčaːktar, </ts>
               <ts e="T13" id="Seg_1176" n="e" s="T12">kaːstar, </ts>
               <ts e="T14" id="Seg_1178" n="e" s="T13">kustar, </ts>
               <ts e="T15" id="Seg_1180" n="e" s="T14">haltaːkiːlar </ts>
               <ts e="T16" id="Seg_1182" n="e" s="T15">bu </ts>
               <ts e="T17" id="Seg_1184" n="e" s="T16">hirge </ts>
               <ts e="T18" id="Seg_1186" n="e" s="T17">olorbuttara. </ts>
               <ts e="T19" id="Seg_1188" n="e" s="T18">Oloru </ts>
               <ts e="T20" id="Seg_1190" n="e" s="T19">gɨtta </ts>
               <ts e="T21" id="Seg_1192" n="e" s="T20">biːrge </ts>
               <ts e="T22" id="Seg_1194" n="e" s="T21">leŋkej </ts>
               <ts e="T23" id="Seg_1196" n="e" s="T22">olorbuta. </ts>
               <ts e="T24" id="Seg_1198" n="e" s="T23">Gini </ts>
               <ts e="T25" id="Seg_1200" n="e" s="T24">ahɨːr </ts>
               <ts e="T26" id="Seg_1202" n="e" s="T25">astara– </ts>
               <ts e="T27" id="Seg_1204" n="e" s="T26">kaːstar, </ts>
               <ts e="T28" id="Seg_1206" n="e" s="T27">kustar, </ts>
               <ts e="T29" id="Seg_1208" n="e" s="T28">kabi͡ekaːttar. </ts>
               <ts e="T30" id="Seg_1210" n="e" s="T29">Barɨ </ts>
               <ts e="T31" id="Seg_1212" n="e" s="T30">kötördör, </ts>
               <ts e="T32" id="Seg_1214" n="e" s="T31">čɨːčaːktar </ts>
               <ts e="T33" id="Seg_1216" n="e" s="T32">ulakannara– </ts>
               <ts e="T34" id="Seg_1218" n="e" s="T33">kaːs. </ts>
               <ts e="T35" id="Seg_1220" n="e" s="T34">Onton </ts>
               <ts e="T36" id="Seg_1222" n="e" s="T35">tɨmnɨːlar </ts>
               <ts e="T37" id="Seg_1224" n="e" s="T36">bu͡olbuttara. </ts>
               <ts e="T38" id="Seg_1226" n="e" s="T37">Hübehit </ts>
               <ts e="T39" id="Seg_1228" n="e" s="T38">kaːs </ts>
               <ts e="T40" id="Seg_1230" n="e" s="T39">barɨ </ts>
               <ts e="T41" id="Seg_1232" n="e" s="T40">kötördörü, </ts>
               <ts e="T42" id="Seg_1234" n="e" s="T41">čɨːčaːktarɨ </ts>
               <ts e="T43" id="Seg_1236" n="e" s="T42">komujbuta. </ts>
               <ts e="T44" id="Seg_1238" n="e" s="T43">Ulakan </ts>
               <ts e="T45" id="Seg_1240" n="e" s="T44">hübelehiː </ts>
               <ts e="T46" id="Seg_1242" n="e" s="T45">bu͡olbuta. </ts>
               <ts e="T47" id="Seg_1244" n="e" s="T46">Kötördör, </ts>
               <ts e="T48" id="Seg_1246" n="e" s="T47">čɨːčaːktar </ts>
               <ts e="T49" id="Seg_1248" n="e" s="T48">komullan </ts>
               <ts e="T50" id="Seg_1250" n="e" s="T49">ki͡eŋ </ts>
               <ts e="T51" id="Seg_1252" n="e" s="T50">alɨːga </ts>
               <ts e="T52" id="Seg_1254" n="e" s="T51">onno </ts>
               <ts e="T53" id="Seg_1256" n="e" s="T52">hu͡opka </ts>
               <ts e="T54" id="Seg_1258" n="e" s="T53">annɨtɨgar </ts>
               <ts e="T55" id="Seg_1260" n="e" s="T54">tu͡olan </ts>
               <ts e="T56" id="Seg_1262" n="e" s="T55">olorbuttara. </ts>
               <ts e="T57" id="Seg_1264" n="e" s="T56">Leŋkej </ts>
               <ts e="T58" id="Seg_1266" n="e" s="T57">emi͡e </ts>
               <ts e="T59" id="Seg_1268" n="e" s="T58">munnʼakka </ts>
               <ts e="T60" id="Seg_1270" n="e" s="T59">kelbite. </ts>
               <ts e="T61" id="Seg_1272" n="e" s="T60">Hübehit </ts>
               <ts e="T62" id="Seg_1274" n="e" s="T61">kaːs </ts>
               <ts e="T63" id="Seg_1276" n="e" s="T62">östörü </ts>
               <ts e="T64" id="Seg_1278" n="e" s="T63">bejetin </ts>
               <ts e="T65" id="Seg_1280" n="e" s="T64">dʼonugar </ts>
               <ts e="T66" id="Seg_1282" n="e" s="T65">haŋarbɨta. </ts>
               <ts e="T67" id="Seg_1284" n="e" s="T66">"Körögüt </ts>
               <ts e="T68" id="Seg_1286" n="e" s="T67">du͡o, </ts>
               <ts e="T69" id="Seg_1288" n="e" s="T68">kallaːmmɨt </ts>
               <ts e="T70" id="Seg_1290" n="e" s="T69">tɨmnɨjda. </ts>
               <ts e="T71" id="Seg_1292" n="e" s="T70">Manna </ts>
               <ts e="T72" id="Seg_1294" n="e" s="T71">bihigi </ts>
               <ts e="T73" id="Seg_1296" n="e" s="T72">hajɨn </ts>
               <ts e="T74" id="Seg_1298" n="e" s="T73">ire </ts>
               <ts e="T75" id="Seg_1300" n="e" s="T74">oloror </ts>
               <ts e="T76" id="Seg_1302" n="e" s="T75">bu͡olu͡okput. </ts>
               <ts e="T77" id="Seg_1304" n="e" s="T76">Min </ts>
               <ts e="T78" id="Seg_1306" n="e" s="T77">hiri </ts>
               <ts e="T79" id="Seg_1308" n="e" s="T78">bilebin, </ts>
               <ts e="T80" id="Seg_1310" n="e" s="T79">kanna </ts>
               <ts e="T81" id="Seg_1312" n="e" s="T80">kɨhɨn </ts>
               <ts e="T82" id="Seg_1314" n="e" s="T81">bu͡olaːččɨta </ts>
               <ts e="T83" id="Seg_1316" n="e" s="T82">hu͡ok. </ts>
               <ts e="T84" id="Seg_1318" n="e" s="T83">Ol </ts>
               <ts e="T85" id="Seg_1320" n="e" s="T84">olus </ts>
               <ts e="T86" id="Seg_1322" n="e" s="T85">ɨraːk </ts>
               <ts e="T87" id="Seg_1324" n="e" s="T86">hirinen </ts>
               <ts e="T88" id="Seg_1326" n="e" s="T87">bardakka </ts>
               <ts e="T89" id="Seg_1328" n="e" s="T88">tijillibet. </ts>
               <ts e="T90" id="Seg_1330" n="e" s="T89">Bihigi, </ts>
               <ts e="T92" id="Seg_1332" n="e" s="T90">(kɨna-) </ts>
               <ts e="T93" id="Seg_1334" n="e" s="T92">kɨnattaːktar, </ts>
               <ts e="T94" id="Seg_1336" n="e" s="T93">bu </ts>
               <ts e="T95" id="Seg_1338" n="e" s="T94">hirge </ts>
               <ts e="T96" id="Seg_1340" n="e" s="T95">togo </ts>
               <ts e="T97" id="Seg_1342" n="e" s="T96">toŋo </ts>
               <ts e="T98" id="Seg_1344" n="e" s="T97">hɨtɨ͡akpɨtɨj? </ts>
               <ts e="T99" id="Seg_1346" n="e" s="T98">Dʼe, </ts>
               <ts e="T100" id="Seg_1348" n="e" s="T99">ol </ts>
               <ts e="T101" id="Seg_1350" n="e" s="T100">hirge </ts>
               <ts e="T102" id="Seg_1352" n="e" s="T101">könötük </ts>
               <ts e="T103" id="Seg_1354" n="e" s="T102">kötü͡ögüŋ! </ts>
               <ts e="T104" id="Seg_1356" n="e" s="T103">Bu </ts>
               <ts e="T105" id="Seg_1358" n="e" s="T104">tɨmnɨːlar </ts>
               <ts e="T106" id="Seg_1360" n="e" s="T105">aːhɨ͡aktarɨgar </ts>
               <ts e="T107" id="Seg_1362" n="e" s="T106">di͡eri </ts>
               <ts e="T108" id="Seg_1364" n="e" s="T107">onno </ts>
               <ts e="T109" id="Seg_1366" n="e" s="T108">oloru͡okput. </ts>
               <ts e="T110" id="Seg_1368" n="e" s="T109">Anɨ </ts>
               <ts e="T111" id="Seg_1370" n="e" s="T110">manna </ts>
               <ts e="T112" id="Seg_1372" n="e" s="T111">bu </ts>
               <ts e="T113" id="Seg_1374" n="e" s="T112">kaːllakpɨtɨna, </ts>
               <ts e="T114" id="Seg_1376" n="e" s="T113">barɨbɨt </ts>
               <ts e="T115" id="Seg_1378" n="e" s="T114">kam </ts>
               <ts e="T116" id="Seg_1380" n="e" s="T115">toŋon </ts>
               <ts e="T117" id="Seg_1382" n="e" s="T116">ölü͡ökpüt. </ts>
               <ts e="T118" id="Seg_1384" n="e" s="T117">Ol </ts>
               <ts e="T119" id="Seg_1386" n="e" s="T118">hirge </ts>
               <ts e="T120" id="Seg_1388" n="e" s="T119">kötön </ts>
               <ts e="T121" id="Seg_1390" n="e" s="T120">bardakpɨtɨna, </ts>
               <ts e="T122" id="Seg_1392" n="e" s="T121">kihi </ts>
               <ts e="T123" id="Seg_1394" n="e" s="T122">bu͡olu͡okput. </ts>
               <ts e="T124" id="Seg_1396" n="e" s="T123">Kirdik </ts>
               <ts e="T125" id="Seg_1398" n="e" s="T124">haŋarabɨn </ts>
               <ts e="T126" id="Seg_1400" n="e" s="T125">du͡o, </ts>
               <ts e="T127" id="Seg_1402" n="e" s="T126">kɨnattaːktar? </ts>
               <ts e="T128" id="Seg_1404" n="e" s="T127">Kim </ts>
               <ts e="T129" id="Seg_1406" n="e" s="T128">ajannɨːrɨ </ts>
               <ts e="T130" id="Seg_1408" n="e" s="T129">höbülüːr?" </ts>
               <ts e="T131" id="Seg_1410" n="e" s="T130">"Kirdik. </ts>
               <ts e="T132" id="Seg_1412" n="e" s="T131">Manna </ts>
               <ts e="T133" id="Seg_1414" n="e" s="T132">kaːllakpɨtɨna </ts>
               <ts e="T134" id="Seg_1416" n="e" s="T133">ölü͡ökpüt, </ts>
               <ts e="T135" id="Seg_1418" n="e" s="T134">bihigi </ts>
               <ts e="T136" id="Seg_1420" n="e" s="T135">barɨbɨt </ts>
               <ts e="T137" id="Seg_1422" n="e" s="T136">onno </ts>
               <ts e="T138" id="Seg_1424" n="e" s="T137">kötü͡ökpüt", </ts>
               <ts e="T139" id="Seg_1426" n="e" s="T138">di͡en </ts>
               <ts e="T140" id="Seg_1428" n="e" s="T139">barɨ </ts>
               <ts e="T141" id="Seg_1430" n="e" s="T140">kötördör, </ts>
               <ts e="T142" id="Seg_1432" n="e" s="T141">čɨːčaːktar </ts>
               <ts e="T143" id="Seg_1434" n="e" s="T142">ü͡ögülespittere. </ts>
               <ts e="T144" id="Seg_1436" n="e" s="T143">"Min </ts>
               <ts e="T145" id="Seg_1438" n="e" s="T144">hin </ts>
               <ts e="T146" id="Seg_1440" n="e" s="T145">ol </ts>
               <ts e="T147" id="Seg_1442" n="e" s="T146">di͡ek </ts>
               <ts e="T148" id="Seg_1444" n="e" s="T147">kötü͡öm", </ts>
               <ts e="T149" id="Seg_1446" n="e" s="T148">leŋkej </ts>
               <ts e="T150" id="Seg_1448" n="e" s="T149">di͡ebite. </ts>
               <ts e="T151" id="Seg_1450" n="e" s="T150">Hübehit </ts>
               <ts e="T152" id="Seg_1452" n="e" s="T151">kaːs </ts>
               <ts e="T153" id="Seg_1454" n="e" s="T152">dogottoruttan </ts>
               <ts e="T154" id="Seg_1456" n="e" s="T153">ɨjɨppɨta: </ts>
               <ts e="T155" id="Seg_1458" n="e" s="T154">"Bu </ts>
               <ts e="T156" id="Seg_1460" n="e" s="T155">leŋkej </ts>
               <ts e="T157" id="Seg_1462" n="e" s="T156">bihigini </ts>
               <ts e="T158" id="Seg_1464" n="e" s="T157">gɨtta </ts>
               <ts e="T159" id="Seg_1466" n="e" s="T158">barsaːrɨ </ts>
               <ts e="T160" id="Seg_1468" n="e" s="T159">gɨnar. </ts>
               <ts e="T161" id="Seg_1470" n="e" s="T160">Iti </ts>
               <ts e="T162" id="Seg_1472" n="e" s="T161">tuhunan </ts>
               <ts e="T163" id="Seg_1474" n="e" s="T162">ehigi </ts>
               <ts e="T164" id="Seg_1476" n="e" s="T163">kajtak </ts>
               <ts e="T165" id="Seg_1478" n="e" s="T164">diː </ts>
               <ts e="T166" id="Seg_1480" n="e" s="T165">hanɨːgɨt?" </ts>
               <ts e="T167" id="Seg_1482" n="e" s="T166">"Bihigi </ts>
               <ts e="T168" id="Seg_1484" n="e" s="T167">leŋkeji </ts>
               <ts e="T169" id="Seg_1486" n="e" s="T168">bejebitin </ts>
               <ts e="T170" id="Seg_1488" n="e" s="T169">kɨtta </ts>
               <ts e="T171" id="Seg_1490" n="e" s="T170">ɨlɨ͡akpɨt </ts>
               <ts e="T172" id="Seg_1492" n="e" s="T171">hu͡oga", </ts>
               <ts e="T173" id="Seg_1494" n="e" s="T172">kötördör </ts>
               <ts e="T174" id="Seg_1496" n="e" s="T173">di͡ebittere. </ts>
               <ts e="T175" id="Seg_1498" n="e" s="T174">"Ehigi </ts>
               <ts e="T176" id="Seg_1500" n="e" s="T175">ginini </ts>
               <ts e="T177" id="Seg_1502" n="e" s="T176">togo </ts>
               <ts e="T178" id="Seg_1504" n="e" s="T177">ɨlɨmaːrɨ </ts>
               <ts e="T179" id="Seg_1506" n="e" s="T178">gɨnagɨt, </ts>
               <ts e="T180" id="Seg_1508" n="e" s="T179">gini͡eke </ts>
               <ts e="T181" id="Seg_1510" n="e" s="T180">könötük </ts>
               <ts e="T182" id="Seg_1512" n="e" s="T181">haŋarɨŋ". </ts>
               <ts e="T183" id="Seg_1514" n="e" s="T182">Kötördör </ts>
               <ts e="T184" id="Seg_1516" n="e" s="T183">leŋkej </ts>
               <ts e="T185" id="Seg_1518" n="e" s="T184">tuhunan </ts>
               <ts e="T186" id="Seg_1520" n="e" s="T185">bɨha </ts>
               <ts e="T187" id="Seg_1522" n="e" s="T186">haŋarbɨttara: </ts>
               <ts e="T188" id="Seg_1524" n="e" s="T187">"Leŋkej </ts>
               <ts e="T189" id="Seg_1526" n="e" s="T188">manna </ts>
               <ts e="T190" id="Seg_1528" n="e" s="T189">bihigi </ts>
               <ts e="T191" id="Seg_1530" n="e" s="T190">kergetterbitin </ts>
               <ts e="T192" id="Seg_1532" n="e" s="T191">hi͡en </ts>
               <ts e="T193" id="Seg_1534" n="e" s="T192">oloror. </ts>
               <ts e="T194" id="Seg_1536" n="e" s="T193">Gini </ts>
               <ts e="T195" id="Seg_1538" n="e" s="T194">bejetin </ts>
               <ts e="T196" id="Seg_1540" n="e" s="T195">mi͡eretin </ts>
               <ts e="T197" id="Seg_1542" n="e" s="T196">onno </ts>
               <ts e="T198" id="Seg_1544" n="e" s="T197">da </ts>
               <ts e="T199" id="Seg_1546" n="e" s="T198">keːhi͡e </ts>
               <ts e="T200" id="Seg_1548" n="e" s="T199">hu͡oga. </ts>
               <ts e="T201" id="Seg_1550" n="e" s="T200">Bihigi </ts>
               <ts e="T202" id="Seg_1552" n="e" s="T201">ginini </ts>
               <ts e="T203" id="Seg_1554" n="e" s="T202">ɨlɨ͡akpɨt </ts>
               <ts e="T204" id="Seg_1556" n="e" s="T203">hu͡oga". </ts>
               <ts e="T205" id="Seg_1558" n="e" s="T204">Hübehit </ts>
               <ts e="T206" id="Seg_1560" n="e" s="T205">kaːs </ts>
               <ts e="T207" id="Seg_1562" n="e" s="T206">leŋkejge </ts>
               <ts e="T208" id="Seg_1564" n="e" s="T207">diːr: </ts>
               <ts e="T209" id="Seg_1566" n="e" s="T208">"Körögün </ts>
               <ts e="T210" id="Seg_1568" n="e" s="T209">du͡o, </ts>
               <ts e="T211" id="Seg_1570" n="e" s="T210">kɨnattaːk </ts>
               <ts e="T212" id="Seg_1572" n="e" s="T211">dʼon </ts>
               <ts e="T213" id="Seg_1574" n="e" s="T212">hübeleːbitterin. </ts>
               <ts e="T214" id="Seg_1576" n="e" s="T213">Barɨlara </ts>
               <ts e="T215" id="Seg_1578" n="e" s="T214">haŋarbɨttara– </ts>
               <ts e="T216" id="Seg_1580" n="e" s="T215">iti </ts>
               <ts e="T217" id="Seg_1582" n="e" s="T216">hoku͡on. </ts>
               <ts e="T218" id="Seg_1584" n="e" s="T217">En </ts>
               <ts e="T219" id="Seg_1586" n="e" s="T218">manna </ts>
               <ts e="T220" id="Seg_1588" n="e" s="T219">kaːlɨ͡aŋ". </ts>
               <ts e="T221" id="Seg_1590" n="e" s="T220">"Kördönön </ts>
               <ts e="T222" id="Seg_1592" n="e" s="T221">bütebin", </ts>
               <ts e="T223" id="Seg_1594" n="e" s="T222">leŋkej </ts>
               <ts e="T224" id="Seg_1596" n="e" s="T223">di͡ebite, </ts>
               <ts e="T225" id="Seg_1598" n="e" s="T224">"manna </ts>
               <ts e="T226" id="Seg_1600" n="e" s="T225">kaːlɨ͡am. </ts>
               <ts e="T227" id="Seg_1602" n="e" s="T226">Bagar, </ts>
               <ts e="T228" id="Seg_1604" n="e" s="T227">min </ts>
               <ts e="T229" id="Seg_1606" n="e" s="T228">manna </ts>
               <ts e="T230" id="Seg_1608" n="e" s="T229">tɨːnnaːk </ts>
               <ts e="T231" id="Seg_1610" n="e" s="T230">oloru͡om". </ts>
               <ts e="T232" id="Seg_1612" n="e" s="T231">Barɨ </ts>
               <ts e="T233" id="Seg_1614" n="e" s="T232">kötördör, </ts>
               <ts e="T234" id="Seg_1616" n="e" s="T233">čɨːčaːktar </ts>
               <ts e="T235" id="Seg_1618" n="e" s="T234">itiː </ts>
               <ts e="T236" id="Seg_1620" n="e" s="T235">hiri </ts>
               <ts e="T237" id="Seg_1622" n="e" s="T236">kördüː </ts>
               <ts e="T238" id="Seg_1624" n="e" s="T237">barbɨttara. </ts>
               <ts e="T239" id="Seg_1626" n="e" s="T238">Leŋkej </ts>
               <ts e="T240" id="Seg_1628" n="e" s="T239">dʼi͡etiger </ts>
               <ts e="T241" id="Seg_1630" n="e" s="T240">kelen, </ts>
               <ts e="T242" id="Seg_1632" n="e" s="T241">emeːksiniger </ts>
               <ts e="T243" id="Seg_1634" n="e" s="T242">diːr: </ts>
               <ts e="T244" id="Seg_1636" n="e" s="T243">"Bilegin </ts>
               <ts e="T245" id="Seg_1638" n="e" s="T244">du͡o, </ts>
               <ts e="T246" id="Seg_1640" n="e" s="T245">bihigini </ts>
               <ts e="T247" id="Seg_1642" n="e" s="T246">ɨlbattarɨn? </ts>
               <ts e="T248" id="Seg_1644" n="e" s="T247">Mini͡eke </ts>
               <ts e="T249" id="Seg_1646" n="e" s="T248">ičiges </ts>
               <ts e="T250" id="Seg_1648" n="e" s="T249">hukuːju </ts>
               <ts e="T251" id="Seg_1650" n="e" s="T250">tik, </ts>
               <ts e="T252" id="Seg_1652" n="e" s="T251">purgaːga </ts>
               <ts e="T253" id="Seg_1654" n="e" s="T252">daː, </ts>
               <ts e="T254" id="Seg_1656" n="e" s="T253">tɨmnɨːga </ts>
               <ts e="T255" id="Seg_1658" n="e" s="T254">daː </ts>
               <ts e="T256" id="Seg_1660" n="e" s="T255">toŋmot </ts>
               <ts e="T257" id="Seg_1662" n="e" s="T256">bu͡olu͡okpun". </ts>
               <ts e="T258" id="Seg_1664" n="e" s="T257">"Tigi͡em", </ts>
               <ts e="T259" id="Seg_1666" n="e" s="T258">ös </ts>
               <ts e="T260" id="Seg_1668" n="e" s="T259">kiːrbek </ts>
               <ts e="T261" id="Seg_1670" n="e" s="T260">emeːksine </ts>
               <ts e="T262" id="Seg_1672" n="e" s="T261">di͡ebite. </ts>
               <ts e="T263" id="Seg_1674" n="e" s="T262">"Hukuːj </ts>
               <ts e="T264" id="Seg_1676" n="e" s="T263">kaːr </ts>
               <ts e="T265" id="Seg_1678" n="e" s="T264">kördük </ts>
               <ts e="T266" id="Seg_1680" n="e" s="T265">čeːlkeː </ts>
               <ts e="T267" id="Seg_1682" n="e" s="T266">bu͡ollun". </ts>
               <ts e="T268" id="Seg_1684" n="e" s="T267">Leŋkej </ts>
               <ts e="T269" id="Seg_1686" n="e" s="T268">emeːksine </ts>
               <ts e="T270" id="Seg_1688" n="e" s="T269">čebis-čeːlkeː </ts>
               <ts e="T271" id="Seg_1690" n="e" s="T270">hukuːju </ts>
               <ts e="T272" id="Seg_1692" n="e" s="T271">tikpite. </ts>
               <ts e="T273" id="Seg_1694" n="e" s="T272">Leŋkej </ts>
               <ts e="T274" id="Seg_1696" n="e" s="T273">hukuːjun </ts>
               <ts e="T275" id="Seg_1698" n="e" s="T274">keteːtin, </ts>
               <ts e="T276" id="Seg_1700" n="e" s="T275">ontuta </ts>
               <ts e="T277" id="Seg_1702" n="e" s="T276">etiger </ts>
               <ts e="T278" id="Seg_1704" n="e" s="T277">hɨstan </ts>
               <ts e="T279" id="Seg_1706" n="e" s="T278">kaːlbɨta. </ts>
               <ts e="T280" id="Seg_1708" n="e" s="T279">Leŋkej </ts>
               <ts e="T281" id="Seg_1710" n="e" s="T280">čɨskaːn </ts>
               <ts e="T282" id="Seg_1712" n="e" s="T281">tɨmnɨːga </ts>
               <ts e="T283" id="Seg_1714" n="e" s="T282">daː </ts>
               <ts e="T284" id="Seg_1716" n="e" s="T283">toŋmot, </ts>
               <ts e="T285" id="Seg_1718" n="e" s="T284">küːsteːk </ts>
               <ts e="T286" id="Seg_1720" n="e" s="T285">purgaːga </ts>
               <ts e="T287" id="Seg_1722" n="e" s="T286">daː </ts>
               <ts e="T288" id="Seg_1724" n="e" s="T287">ɨllarbat. </ts>
               <ts e="T289" id="Seg_1726" n="e" s="T288">Hol </ts>
               <ts e="T290" id="Seg_1728" n="e" s="T289">kemten </ts>
               <ts e="T291" id="Seg_1730" n="e" s="T290">tuːs </ts>
               <ts e="T292" id="Seg_1732" n="e" s="T291">mu͡oraga </ts>
               <ts e="T293" id="Seg_1734" n="e" s="T292">leŋkej </ts>
               <ts e="T294" id="Seg_1736" n="e" s="T293">manna, </ts>
               <ts e="T295" id="Seg_1738" n="e" s="T294">kanna </ts>
               <ts e="T296" id="Seg_1740" n="e" s="T295">da </ts>
               <ts e="T297" id="Seg_1742" n="e" s="T296">köppökkö, </ts>
               <ts e="T298" id="Seg_1744" n="e" s="T297">kɨhɨlɨː </ts>
               <ts e="T299" id="Seg_1746" n="e" s="T298">oloror. </ts>
               <ts e="T300" id="Seg_1748" n="e" s="T299">Leŋkej </ts>
               <ts e="T301" id="Seg_1750" n="e" s="T300">urukkutaːgar </ts>
               <ts e="T302" id="Seg_1752" n="e" s="T301">daː </ts>
               <ts e="T303" id="Seg_1754" n="e" s="T302">ulakannɨk </ts>
               <ts e="T304" id="Seg_1756" n="e" s="T303">kɨra </ts>
               <ts e="T305" id="Seg_1758" n="e" s="T304">čɨːčaːktarɨ, </ts>
               <ts e="T306" id="Seg_1760" n="e" s="T305">kabi͡ekaːttarɨ </ts>
               <ts e="T307" id="Seg_1762" n="e" s="T306">hiːr </ts>
               <ts e="T308" id="Seg_1764" n="e" s="T307">bu͡olbuta. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T1" id="Seg_1765" s="T0">PoNA_2004_SnowOwl_flk.001 (001.001)</ta>
            <ta e="T5" id="Seg_1766" s="T1">PoNA_2004_SnowOwl_flk.002 (001.002)</ta>
            <ta e="T9" id="Seg_1767" s="T5">PoNA_2004_SnowOwl_flk.003 (001.003)</ta>
            <ta e="T18" id="Seg_1768" s="T9">PoNA_2004_SnowOwl_flk.004 (001.004)</ta>
            <ta e="T23" id="Seg_1769" s="T18">PoNA_2004_SnowOwl_flk.005 (001.005)</ta>
            <ta e="T29" id="Seg_1770" s="T23">PoNA_2004_SnowOwl_flk.006 (001.006)</ta>
            <ta e="T34" id="Seg_1771" s="T29">PoNA_2004_SnowOwl_flk.007 (001.007)</ta>
            <ta e="T37" id="Seg_1772" s="T34">PoNA_2004_SnowOwl_flk.008 (001.008)</ta>
            <ta e="T43" id="Seg_1773" s="T37">PoNA_2004_SnowOwl_flk.009 (001.009)</ta>
            <ta e="T46" id="Seg_1774" s="T43">PoNA_2004_SnowOwl_flk.010 (001.010)</ta>
            <ta e="T56" id="Seg_1775" s="T46">PoNA_2004_SnowOwl_flk.011 (001.011)</ta>
            <ta e="T60" id="Seg_1776" s="T56">PoNA_2004_SnowOwl_flk.012 (001.012)</ta>
            <ta e="T66" id="Seg_1777" s="T60">PoNA_2004_SnowOwl_flk.013 (001.013)</ta>
            <ta e="T70" id="Seg_1778" s="T66">PoNA_2004_SnowOwl_flk.014 (001.014)</ta>
            <ta e="T76" id="Seg_1779" s="T70">PoNA_2004_SnowOwl_flk.015 (001.015)</ta>
            <ta e="T83" id="Seg_1780" s="T76">PoNA_2004_SnowOwl_flk.016 (001.016)</ta>
            <ta e="T89" id="Seg_1781" s="T83">PoNA_2004_SnowOwl_flk.017 (001.017)</ta>
            <ta e="T98" id="Seg_1782" s="T89">PoNA_2004_SnowOwl_flk.018 (001.018)</ta>
            <ta e="T103" id="Seg_1783" s="T98">PoNA_2004_SnowOwl_flk.019 (001.019)</ta>
            <ta e="T109" id="Seg_1784" s="T103">PoNA_2004_SnowOwl_flk.020 (001.020)</ta>
            <ta e="T117" id="Seg_1785" s="T109">PoNA_2004_SnowOwl_flk.021 (001.021)</ta>
            <ta e="T123" id="Seg_1786" s="T117">PoNA_2004_SnowOwl_flk.022 (001.022)</ta>
            <ta e="T127" id="Seg_1787" s="T123">PoNA_2004_SnowOwl_flk.023 (001.023)</ta>
            <ta e="T130" id="Seg_1788" s="T127">PoNA_2004_SnowOwl_flk.024 (001.024)</ta>
            <ta e="T131" id="Seg_1789" s="T130">PoNA_2004_SnowOwl_flk.025 (001.025)</ta>
            <ta e="T143" id="Seg_1790" s="T131">PoNA_2004_SnowOwl_flk.026 (001.026)</ta>
            <ta e="T150" id="Seg_1791" s="T143">PoNA_2004_SnowOwl_flk.027 (001.027)</ta>
            <ta e="T154" id="Seg_1792" s="T150">PoNA_2004_SnowOwl_flk.028 (001.028)</ta>
            <ta e="T160" id="Seg_1793" s="T154">PoNA_2004_SnowOwl_flk.029 (001.029)</ta>
            <ta e="T166" id="Seg_1794" s="T160">PoNA_2004_SnowOwl_flk.030 (001.030)</ta>
            <ta e="T174" id="Seg_1795" s="T166">PoNA_2004_SnowOwl_flk.031 (001.031)</ta>
            <ta e="T182" id="Seg_1796" s="T174">PoNA_2004_SnowOwl_flk.032 (001.032)</ta>
            <ta e="T187" id="Seg_1797" s="T182">PoNA_2004_SnowOwl_flk.033 (001.033)</ta>
            <ta e="T193" id="Seg_1798" s="T187">PoNA_2004_SnowOwl_flk.034 (001.034)</ta>
            <ta e="T200" id="Seg_1799" s="T193">PoNA_2004_SnowOwl_flk.035 (001.035)</ta>
            <ta e="T204" id="Seg_1800" s="T200">PoNA_2004_SnowOwl_flk.036 (001.036)</ta>
            <ta e="T208" id="Seg_1801" s="T204">PoNA_2004_SnowOwl_flk.037 (001.037)</ta>
            <ta e="T213" id="Seg_1802" s="T208">PoNA_2004_SnowOwl_flk.038 (001.038)</ta>
            <ta e="T217" id="Seg_1803" s="T213">PoNA_2004_SnowOwl_flk.039 (001.039)</ta>
            <ta e="T220" id="Seg_1804" s="T217">PoNA_2004_SnowOwl_flk.040 (001.040)</ta>
            <ta e="T226" id="Seg_1805" s="T220">PoNA_2004_SnowOwl_flk.041 (001.041)</ta>
            <ta e="T231" id="Seg_1806" s="T226">PoNA_2004_SnowOwl_flk.042 (001.042)</ta>
            <ta e="T238" id="Seg_1807" s="T231">PoNA_2004_SnowOwl_flk.043 (001.043)</ta>
            <ta e="T243" id="Seg_1808" s="T238">PoNA_2004_SnowOwl_flk.044 (001.044)</ta>
            <ta e="T247" id="Seg_1809" s="T243">PoNA_2004_SnowOwl_flk.045 (001.045)</ta>
            <ta e="T257" id="Seg_1810" s="T247">PoNA_2004_SnowOwl_flk.046 (001.046)</ta>
            <ta e="T262" id="Seg_1811" s="T257">PoNA_2004_SnowOwl_flk.047 (001.047)</ta>
            <ta e="T267" id="Seg_1812" s="T262">PoNA_2004_SnowOwl_flk.048 (001.048)</ta>
            <ta e="T272" id="Seg_1813" s="T267">PoNA_2004_SnowOwl_flk.049 (001.049)</ta>
            <ta e="T279" id="Seg_1814" s="T272">PoNA_2004_SnowOwl_flk.050 (001.050)</ta>
            <ta e="T288" id="Seg_1815" s="T279">PoNA_2004_SnowOwl_flk.051 (001.051)</ta>
            <ta e="T299" id="Seg_1816" s="T288">PoNA_2004_SnowOwl_flk.052 (001.052)</ta>
            <ta e="T308" id="Seg_1817" s="T299">PoNA_2004_SnowOwl_flk.053 (001.053)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T1" id="Seg_1818" s="T0">Лэӈкэй.</ta>
            <ta e="T5" id="Seg_1819" s="T1">Ол аата дулгаан-һака олоӈкото.</ta>
            <ta e="T9" id="Seg_1820" s="T5">Былыр-былыр муора ичигэс этэ. </ta>
            <ta e="T18" id="Seg_1821" s="T9">Бары көтөрдөр: чыычаактар, каастар, кустар, һалтаакиилар бу һиргэ олорбуттара.</ta>
            <ta e="T23" id="Seg_1822" s="T18">Олору гытта бииргэ лэӈкэй олорбута. </ta>
            <ta e="T29" id="Seg_1823" s="T23">Гини аһыыр астара – каастар, кустар, кабиэкааттар.</ta>
            <ta e="T34" id="Seg_1824" s="T29">Бары көтөрдөр, чыычаактар улаканнара – каас.</ta>
            <ta e="T37" id="Seg_1825" s="T34">Онтон тымныылар буолбуттара.</ta>
            <ta e="T43" id="Seg_1826" s="T37">Һүбэһит каас бары көтөрдөрү, чыычаактары комуйбута.</ta>
            <ta e="T46" id="Seg_1827" s="T43">Улакан һүбэлэһии буолбута.</ta>
            <ta e="T56" id="Seg_1828" s="T46">Көтөрдөр, чыычаактар комуллан киэӈ алыыга онно һуопка аннытыгар туолан олорбуттара.</ta>
            <ta e="T60" id="Seg_1829" s="T56">Лэӈкэй эмиэ мунньакка кэлбитэ.</ta>
            <ta e="T66" id="Seg_1830" s="T60">Һүбэһит каас өстөрү бэйэтин дьонугар һаӈарбыта.</ta>
            <ta e="T70" id="Seg_1831" s="T66">"Көрөгүт дуо, каллааммыт тымныйда. </ta>
            <ta e="T76" id="Seg_1832" s="T70">Манна биһиги һайын ирэ олорор буолуокпут.</ta>
            <ta e="T83" id="Seg_1833" s="T76">Мин һири билэбин, канна кыһын буолааччыта һуок.</ta>
            <ta e="T89" id="Seg_1834" s="T83">Ол олус ыраак һиринэн бардакка тийиллибэт. </ta>
            <ta e="T98" id="Seg_1835" s="T89">Биһиги, кына.. кынаттаактар, бу һиргэ того тоӈо һытыакпытый? </ta>
            <ta e="T103" id="Seg_1836" s="T98">Дьэ, ол һиргэ көнөтүк көтүөгүӈ! </ta>
            <ta e="T109" id="Seg_1837" s="T103">Бу тымныылар ааһыактарыгар диэри онно олоруокпут.</ta>
            <ta e="T117" id="Seg_1838" s="T109">Аны манна бу кааллакпытына, барыбыт кам тоӈон өлүөкпүт. </ta>
            <ta e="T123" id="Seg_1839" s="T117">Ол һиргэ көтөн бардакпытына, киһи буолуокпут. </ta>
            <ta e="T127" id="Seg_1840" s="T123">Кирдик һаӈарабын дуо, кынаттаактар? </ta>
            <ta e="T130" id="Seg_1841" s="T127">Ким айанныыры һөбүлүүр?" </ta>
            <ta e="T131" id="Seg_1842" s="T130">"Кирдик. </ta>
            <ta e="T143" id="Seg_1843" s="T131">Манна кааллакпытына өлүөкпүт, биһиги барыбыт онно көтүөкпүт", – диэн бары көтөрдөр, чыычаактар үөгүлэспиттэрэ.</ta>
            <ta e="T150" id="Seg_1844" s="T143">"Мин һин ол диэк көтүөм", – лэӈкэй диэбитэ.</ta>
            <ta e="T154" id="Seg_1845" s="T150">Һүбэһит каас доготторуттан ыйыппыта: </ta>
            <ta e="T160" id="Seg_1846" s="T154">"Бу лэӈкэй биһигини гытта барсаары гынар. </ta>
            <ta e="T166" id="Seg_1847" s="T160">Ити туһунан эһиги кайтак (кайдак) дии һаныыгыт?" </ta>
            <ta e="T174" id="Seg_1848" s="T166">"Биһиги лэӈкэйи бэйэбитин кытта ылыакпыт һуога, – көтөрдөр диэбиттэрэ.</ta>
            <ta e="T182" id="Seg_1849" s="T174">"Эһиги гинини того ылымаары гынагыт, гиниэкэ көнөтүк һаӈарыӈ".</ta>
            <ta e="T187" id="Seg_1850" s="T182">Көтөрдөр лэӈкэй туһунан быһа һаӈарбыттара: </ta>
            <ta e="T193" id="Seg_1851" s="T187">"Лэӈкэй манна биһиги кэргэттэрбитин һиэн олорор.</ta>
            <ta e="T200" id="Seg_1852" s="T193">Гини бэйэтин миэрэтин онно да кээһиэ һуога. </ta>
            <ta e="T204" id="Seg_1853" s="T200">Биһиги гинини ылыакпыт һуога".</ta>
            <ta e="T208" id="Seg_1854" s="T204">Һүбэһит каас лэӈкэйгэ диир: </ta>
            <ta e="T213" id="Seg_1855" s="T208">"Көрөгүн дуо, кынаттаак дьон һүбэлээбиттэрин. </ta>
            <ta e="T217" id="Seg_1856" s="T213">Барылара һаӈарбыттара – ити һокуон.</ta>
            <ta e="T220" id="Seg_1857" s="T217">Эн манна каалыаӈ".</ta>
            <ta e="T226" id="Seg_1858" s="T220">"Көрдөнөн (көрдөһөн) бүтэбин, – лэӈкэй диэбитэ, – манна каалыам.</ta>
            <ta e="T231" id="Seg_1859" s="T226">Багар, мин манна тыыннаак олоруом".</ta>
            <ta e="T238" id="Seg_1860" s="T231">Бары көтөрдөр, чыычаактар итии һири көрдүү барбыттара. </ta>
            <ta e="T243" id="Seg_1861" s="T238">Лэӈкэй дьиэтигэр кэлэн, эмээксинигэр диир: </ta>
            <ta e="T247" id="Seg_1862" s="T243">"Билэгин дуо, биһигини ылбаттарын.</ta>
            <ta e="T257" id="Seg_1863" s="T247">Миниэкэ ичигэс һукууйу тик, пургаага даа, тымныыга даа тоӈмот буолуокпун".</ta>
            <ta e="T262" id="Seg_1864" s="T257">"Тигиэм", – өс киирбэк эмээксинэ диэбитэ.</ta>
            <ta e="T267" id="Seg_1865" s="T262">"Һукууй каар көрдүк чээлкээ буоллун".</ta>
            <ta e="T272" id="Seg_1866" s="T267">Лэӈкэй эмээксинэ чэбис-чээлкээ һукууйу тикпитэ.</ta>
            <ta e="T279" id="Seg_1867" s="T272">Лэӈкэй һукууйун кэтээтин, онтута этигэр һыстан каалбыта.</ta>
            <ta e="T288" id="Seg_1868" s="T279">Лэӈкэй чыыскаан тымныыга даа тоӈмот, күүстээк пургаага даа ылларбат.</ta>
            <ta e="T299" id="Seg_1869" s="T288">Һол кэмтэн туус муорага лэӈкэй манна, канна да көппөккө, кыһылыы олорор.</ta>
            <ta e="T308" id="Seg_1870" s="T299">Лэӈкэй уруккутаагар даа улаканнык кыра чыычаактары, кабиэкааттары һиир буолбута.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T1" id="Seg_1871" s="T0">Leŋkej. </ta>
            <ta e="T5" id="Seg_1872" s="T1">Ol aːta dulgaːn-haka oloŋkoto. </ta>
            <ta e="T9" id="Seg_1873" s="T5">Bɨlɨr-bɨlɨr mu͡ora ičiges ete. </ta>
            <ta e="T18" id="Seg_1874" s="T9">Barɨ kötördör, čɨːčaːktar, kaːstar, kustar, haltaːkiːlar bu hirge olorbuttara. </ta>
            <ta e="T23" id="Seg_1875" s="T18">Oloru gɨtta biːrge leŋkej olorbuta. </ta>
            <ta e="T29" id="Seg_1876" s="T23">Gini ahɨːr astara– kaːstar, kustar, kabi͡ekaːttar. </ta>
            <ta e="T34" id="Seg_1877" s="T29">Barɨ kötördör, čɨːčaːktar ulakannara– kaːs. </ta>
            <ta e="T37" id="Seg_1878" s="T34">Onton tɨmnɨːlar bu͡olbuttara. </ta>
            <ta e="T43" id="Seg_1879" s="T37">Hübehit kaːs barɨ kötördörü, čɨːčaːktarɨ komujbuta. </ta>
            <ta e="T46" id="Seg_1880" s="T43">Ulakan hübelehiː bu͡olbuta. </ta>
            <ta e="T56" id="Seg_1881" s="T46">Kötördör, čɨːčaːktar komullan ki͡eŋ alɨːga onno hu͡opka annɨtɨgar tu͡olan olorbuttara. </ta>
            <ta e="T60" id="Seg_1882" s="T56">Leŋkej emi͡e munnʼakka kelbite. </ta>
            <ta e="T66" id="Seg_1883" s="T60">Hübehit kaːs östörü bejetin dʼonugar haŋarbɨta. </ta>
            <ta e="T70" id="Seg_1884" s="T66">"Körögüt du͡o, kallaːmmɨt tɨmnɨjda. </ta>
            <ta e="T76" id="Seg_1885" s="T70">Manna bihigi hajɨn ire oloror bu͡olu͡okput. </ta>
            <ta e="T83" id="Seg_1886" s="T76">Min hiri bilebin, kanna kɨhɨn bu͡olaːččɨta hu͡ok. </ta>
            <ta e="T89" id="Seg_1887" s="T83">Ol olus ɨraːk hirinen bardakka tijillibet. </ta>
            <ta e="T98" id="Seg_1888" s="T89">Bihigi, (kɨna-) kɨnattaːktar, bu hirge togo toŋo hɨtɨ͡akpɨtɨj? </ta>
            <ta e="T103" id="Seg_1889" s="T98">Dʼe, ol hirge könötük kötü͡ögüŋ! </ta>
            <ta e="T109" id="Seg_1890" s="T103">Bu tɨmnɨːlar aːhɨ͡aktarɨgar di͡eri onno oloru͡okput. </ta>
            <ta e="T117" id="Seg_1891" s="T109">Anɨ manna bu kaːllakpɨtɨna, barɨbɨt kam toŋon ölü͡ökpüt. </ta>
            <ta e="T123" id="Seg_1892" s="T117">Ol hirge kötön bardakpɨtɨna, kihi bu͡olu͡okput. </ta>
            <ta e="T127" id="Seg_1893" s="T123">Kirdik haŋarabɨn du͡o, kɨnattaːktar? </ta>
            <ta e="T130" id="Seg_1894" s="T127">Kim ajannɨːrɨ höbülüːr?" </ta>
            <ta e="T131" id="Seg_1895" s="T130">"Kirdik. </ta>
            <ta e="T143" id="Seg_1896" s="T131">Manna kaːllakpɨtɨna ölü͡ökpüt, bihigi barɨbɨt onno kötü͡ökpüt", di͡en barɨ kötördör, čɨːčaːktar ü͡ögülespittere. </ta>
            <ta e="T150" id="Seg_1897" s="T143">"Min hin ol di͡ek kötü͡öm", leŋkej di͡ebite. </ta>
            <ta e="T154" id="Seg_1898" s="T150">Hübehit kaːs dogottoruttan ɨjɨppɨta: </ta>
            <ta e="T160" id="Seg_1899" s="T154">"Bu leŋkej bihigini gɨtta barsaːrɨ gɨnar. </ta>
            <ta e="T166" id="Seg_1900" s="T160">Iti tuhunan ehigi kajtak diː hanɨːgɨt?" </ta>
            <ta e="T174" id="Seg_1901" s="T166">"Bihigi leŋkeji bejebitin kɨtta ɨlɨ͡akpɨt hu͡oga", kötördör di͡ebittere. </ta>
            <ta e="T182" id="Seg_1902" s="T174">"Ehigi ginini togo ɨlɨmaːrɨ gɨnagɨt, gini͡eke könötük haŋarɨŋ". </ta>
            <ta e="T187" id="Seg_1903" s="T182">Kötördör leŋkej tuhunan bɨha haŋarbɨttara: </ta>
            <ta e="T193" id="Seg_1904" s="T187">"Leŋkej manna bihigi kergetterbitin hi͡en oloror. </ta>
            <ta e="T200" id="Seg_1905" s="T193">Gini bejetin mi͡eretin onno da keːhi͡e hu͡oga. </ta>
            <ta e="T204" id="Seg_1906" s="T200">Bihigi ginini ɨlɨ͡akpɨt hu͡oga". </ta>
            <ta e="T208" id="Seg_1907" s="T204">Hübehit kaːs leŋkejge diːr: </ta>
            <ta e="T213" id="Seg_1908" s="T208">"Körögün du͡o, kɨnattaːk dʼon hübeleːbitterin. </ta>
            <ta e="T217" id="Seg_1909" s="T213">Barɨlara haŋarbɨttara– iti hoku͡on. </ta>
            <ta e="T220" id="Seg_1910" s="T217">En manna kaːlɨ͡aŋ". </ta>
            <ta e="T226" id="Seg_1911" s="T220">"Kördönön bütebin", leŋkej di͡ebite, "manna kaːlɨ͡am. </ta>
            <ta e="T231" id="Seg_1912" s="T226">Bagar, min manna tɨːnnaːk oloru͡om". </ta>
            <ta e="T238" id="Seg_1913" s="T231">Barɨ kötördör, čɨːčaːktar itiː hiri kördüː barbɨttara. </ta>
            <ta e="T243" id="Seg_1914" s="T238">Leŋkej dʼi͡etiger kelen, emeːksiniger diːr: </ta>
            <ta e="T247" id="Seg_1915" s="T243">"Bilegin du͡o, bihigini ɨlbattarɨn? </ta>
            <ta e="T257" id="Seg_1916" s="T247">Mini͡eke ičiges hukuːju tik, purgaːga daː, tɨmnɨːga daː toŋmot bu͡olu͡okpun". </ta>
            <ta e="T262" id="Seg_1917" s="T257">"Tigi͡em", ös kiːrbek emeːksine di͡ebite. </ta>
            <ta e="T267" id="Seg_1918" s="T262">"Hukuːj kaːr kördük čeːlkeː bu͡ollun". </ta>
            <ta e="T272" id="Seg_1919" s="T267">Leŋkej emeːksine čebis-čeːlkeː hukuːju tikpite. </ta>
            <ta e="T279" id="Seg_1920" s="T272">Leŋkej hukuːjun keteːtin, ontuta etiger hɨstan kaːlbɨta. </ta>
            <ta e="T288" id="Seg_1921" s="T279">Leŋkej čɨskaːn tɨmnɨːga daː toŋmot, küːsteːk purgaːga daː ɨllarbat. </ta>
            <ta e="T299" id="Seg_1922" s="T288">Hol kemten tuːs mu͡oraga leŋkej manna, kanna da köppökkö, kɨhɨlɨː oloror. </ta>
            <ta e="T308" id="Seg_1923" s="T299">Leŋkej urukkutaːgar daː ulakannɨk kɨra čɨːčaːktarɨ, kabi͡ekaːttarɨ hiːr bu͡olbuta. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1924" s="T0">leŋkej</ta>
            <ta e="T2" id="Seg_1925" s="T1">ol</ta>
            <ta e="T3" id="Seg_1926" s="T2">aːt-a</ta>
            <ta e="T4" id="Seg_1927" s="T3">dulgaːn-haka</ta>
            <ta e="T5" id="Seg_1928" s="T4">oloŋko-to</ta>
            <ta e="T6" id="Seg_1929" s="T5">bɨlɨr-bɨlɨr</ta>
            <ta e="T7" id="Seg_1930" s="T6">mu͡ora</ta>
            <ta e="T8" id="Seg_1931" s="T7">ičiges</ta>
            <ta e="T9" id="Seg_1932" s="T8">e-t-e</ta>
            <ta e="T10" id="Seg_1933" s="T9">barɨ</ta>
            <ta e="T11" id="Seg_1934" s="T10">kötör-dör</ta>
            <ta e="T12" id="Seg_1935" s="T11">čɨːčaːk-tar</ta>
            <ta e="T13" id="Seg_1936" s="T12">kaːs-tar</ta>
            <ta e="T14" id="Seg_1937" s="T13">kus-tar</ta>
            <ta e="T15" id="Seg_1938" s="T14">haltaːkiː-lar</ta>
            <ta e="T16" id="Seg_1939" s="T15">bu</ta>
            <ta e="T17" id="Seg_1940" s="T16">hir-ge</ta>
            <ta e="T18" id="Seg_1941" s="T17">olor-but-tara</ta>
            <ta e="T19" id="Seg_1942" s="T18">o-lor-u</ta>
            <ta e="T20" id="Seg_1943" s="T19">gɨtta</ta>
            <ta e="T21" id="Seg_1944" s="T20">biːrge</ta>
            <ta e="T22" id="Seg_1945" s="T21">leŋkej</ta>
            <ta e="T23" id="Seg_1946" s="T22">olor-but-a</ta>
            <ta e="T24" id="Seg_1947" s="T23">gini</ta>
            <ta e="T25" id="Seg_1948" s="T24">ahɨː-r</ta>
            <ta e="T26" id="Seg_1949" s="T25">as-tar-a</ta>
            <ta e="T27" id="Seg_1950" s="T26">kaːs-tar</ta>
            <ta e="T28" id="Seg_1951" s="T27">kus-tar</ta>
            <ta e="T29" id="Seg_1952" s="T28">kabi͡ekaːt-tar</ta>
            <ta e="T30" id="Seg_1953" s="T29">barɨ</ta>
            <ta e="T31" id="Seg_1954" s="T30">kötör-dör</ta>
            <ta e="T32" id="Seg_1955" s="T31">čɨːčaːk-tar</ta>
            <ta e="T33" id="Seg_1956" s="T32">ulakan-nara</ta>
            <ta e="T34" id="Seg_1957" s="T33">kaːs</ta>
            <ta e="T35" id="Seg_1958" s="T34">onton</ta>
            <ta e="T36" id="Seg_1959" s="T35">tɨmnɨː-lar</ta>
            <ta e="T37" id="Seg_1960" s="T36">bu͡ol-but-tara</ta>
            <ta e="T38" id="Seg_1961" s="T37">hübehit</ta>
            <ta e="T39" id="Seg_1962" s="T38">kaːs</ta>
            <ta e="T40" id="Seg_1963" s="T39">barɨ</ta>
            <ta e="T41" id="Seg_1964" s="T40">kötör-dör-ü</ta>
            <ta e="T42" id="Seg_1965" s="T41">čɨːčaːk-tar-ɨ</ta>
            <ta e="T43" id="Seg_1966" s="T42">komuj-but-a</ta>
            <ta e="T44" id="Seg_1967" s="T43">ulakan</ta>
            <ta e="T45" id="Seg_1968" s="T44">hübel-e-h-iː</ta>
            <ta e="T46" id="Seg_1969" s="T45">bu͡ol-but-a</ta>
            <ta e="T47" id="Seg_1970" s="T46">kötör-dör</ta>
            <ta e="T48" id="Seg_1971" s="T47">čɨːčaːk-tar</ta>
            <ta e="T49" id="Seg_1972" s="T48">komu-ll-an</ta>
            <ta e="T50" id="Seg_1973" s="T49">ki͡eŋ</ta>
            <ta e="T51" id="Seg_1974" s="T50">alɨː-ga</ta>
            <ta e="T52" id="Seg_1975" s="T51">onno</ta>
            <ta e="T53" id="Seg_1976" s="T52">hu͡opka</ta>
            <ta e="T54" id="Seg_1977" s="T53">annɨ-tɨ-gar</ta>
            <ta e="T55" id="Seg_1978" s="T54">tu͡ol-an</ta>
            <ta e="T56" id="Seg_1979" s="T55">olor-but-tara</ta>
            <ta e="T57" id="Seg_1980" s="T56">leŋkej</ta>
            <ta e="T58" id="Seg_1981" s="T57">emi͡e</ta>
            <ta e="T59" id="Seg_1982" s="T58">munnʼak-ka</ta>
            <ta e="T60" id="Seg_1983" s="T59">kel-bit-e</ta>
            <ta e="T61" id="Seg_1984" s="T60">hübehit</ta>
            <ta e="T62" id="Seg_1985" s="T61">kaːs</ta>
            <ta e="T63" id="Seg_1986" s="T62">ös-tör-ü</ta>
            <ta e="T64" id="Seg_1987" s="T63">beje-ti-n</ta>
            <ta e="T65" id="Seg_1988" s="T64">dʼon-u-gar</ta>
            <ta e="T66" id="Seg_1989" s="T65">haŋar-bɨt-a</ta>
            <ta e="T67" id="Seg_1990" s="T66">kör-ö-güt</ta>
            <ta e="T68" id="Seg_1991" s="T67">du͡o</ta>
            <ta e="T69" id="Seg_1992" s="T68">kallaːm-mɨt</ta>
            <ta e="T70" id="Seg_1993" s="T69">tɨmnɨj-d-a</ta>
            <ta e="T71" id="Seg_1994" s="T70">manna</ta>
            <ta e="T72" id="Seg_1995" s="T71">bihigi</ta>
            <ta e="T73" id="Seg_1996" s="T72">hajɨn</ta>
            <ta e="T74" id="Seg_1997" s="T73">ire</ta>
            <ta e="T75" id="Seg_1998" s="T74">olor-or</ta>
            <ta e="T76" id="Seg_1999" s="T75">bu͡ol-u͡ok-put</ta>
            <ta e="T77" id="Seg_2000" s="T76">min</ta>
            <ta e="T78" id="Seg_2001" s="T77">hir-i</ta>
            <ta e="T79" id="Seg_2002" s="T78">bil-e-bin</ta>
            <ta e="T80" id="Seg_2003" s="T79">kanna</ta>
            <ta e="T81" id="Seg_2004" s="T80">kɨhɨn</ta>
            <ta e="T82" id="Seg_2005" s="T81">bu͡ol-aːččɨ-ta</ta>
            <ta e="T83" id="Seg_2006" s="T82">hu͡ok</ta>
            <ta e="T84" id="Seg_2007" s="T83">ol</ta>
            <ta e="T85" id="Seg_2008" s="T84">olus</ta>
            <ta e="T86" id="Seg_2009" s="T85">ɨraːk</ta>
            <ta e="T87" id="Seg_2010" s="T86">hir-i-nen</ta>
            <ta e="T88" id="Seg_2011" s="T87">bar-dak-ka</ta>
            <ta e="T89" id="Seg_2012" s="T88">tij-i-ll-i-bet</ta>
            <ta e="T90" id="Seg_2013" s="T89">bihigi</ta>
            <ta e="T93" id="Seg_2014" s="T92">kɨnat-taːk-tar</ta>
            <ta e="T94" id="Seg_2015" s="T93">bu</ta>
            <ta e="T95" id="Seg_2016" s="T94">hir-ge</ta>
            <ta e="T96" id="Seg_2017" s="T95">togo</ta>
            <ta e="T97" id="Seg_2018" s="T96">toŋ-o</ta>
            <ta e="T98" id="Seg_2019" s="T97">hɨt-ɨ͡ak-pɨt=ɨj</ta>
            <ta e="T99" id="Seg_2020" s="T98">dʼe</ta>
            <ta e="T100" id="Seg_2021" s="T99">ol</ta>
            <ta e="T101" id="Seg_2022" s="T100">hir-ge</ta>
            <ta e="T102" id="Seg_2023" s="T101">könö-tük</ta>
            <ta e="T103" id="Seg_2024" s="T102">köt-ü͡ögüŋ</ta>
            <ta e="T104" id="Seg_2025" s="T103">bu</ta>
            <ta e="T105" id="Seg_2026" s="T104">tɨmnɨː-lar</ta>
            <ta e="T106" id="Seg_2027" s="T105">aːh-ɨ͡ak-tarɨ-gar</ta>
            <ta e="T107" id="Seg_2028" s="T106">di͡eri</ta>
            <ta e="T108" id="Seg_2029" s="T107">onno</ta>
            <ta e="T109" id="Seg_2030" s="T108">olor-u͡ok-put</ta>
            <ta e="T110" id="Seg_2031" s="T109">anɨ</ta>
            <ta e="T111" id="Seg_2032" s="T110">manna</ta>
            <ta e="T112" id="Seg_2033" s="T111">bu</ta>
            <ta e="T113" id="Seg_2034" s="T112">kaːl-lak-pɨtɨna</ta>
            <ta e="T114" id="Seg_2035" s="T113">barɨ-bɨt</ta>
            <ta e="T115" id="Seg_2036" s="T114">kam</ta>
            <ta e="T116" id="Seg_2037" s="T115">toŋ-on</ta>
            <ta e="T117" id="Seg_2038" s="T116">öl-ü͡ök-püt</ta>
            <ta e="T118" id="Seg_2039" s="T117">ol</ta>
            <ta e="T119" id="Seg_2040" s="T118">hir-ge</ta>
            <ta e="T120" id="Seg_2041" s="T119">köt-ön</ta>
            <ta e="T121" id="Seg_2042" s="T120">bar-dak-pɨtɨna</ta>
            <ta e="T122" id="Seg_2043" s="T121">kihi</ta>
            <ta e="T123" id="Seg_2044" s="T122">bu͡ol-u͡ok-put</ta>
            <ta e="T124" id="Seg_2045" s="T123">kirdik</ta>
            <ta e="T125" id="Seg_2046" s="T124">haŋar-a-bɨn</ta>
            <ta e="T126" id="Seg_2047" s="T125">du͡o</ta>
            <ta e="T127" id="Seg_2048" s="T126">kɨnat-taːk-tar</ta>
            <ta e="T128" id="Seg_2049" s="T127">kim</ta>
            <ta e="T129" id="Seg_2050" s="T128">ajannɨː-r-ɨ</ta>
            <ta e="T130" id="Seg_2051" s="T129">höbülüː-r</ta>
            <ta e="T131" id="Seg_2052" s="T130">kirdik</ta>
            <ta e="T132" id="Seg_2053" s="T131">manna</ta>
            <ta e="T133" id="Seg_2054" s="T132">kaːl-lak-pɨtɨna</ta>
            <ta e="T134" id="Seg_2055" s="T133">öl-ü͡ök-püt</ta>
            <ta e="T135" id="Seg_2056" s="T134">bihigi</ta>
            <ta e="T136" id="Seg_2057" s="T135">barɨ-bɨt</ta>
            <ta e="T137" id="Seg_2058" s="T136">onno</ta>
            <ta e="T138" id="Seg_2059" s="T137">köt-ü͡ök-püt</ta>
            <ta e="T139" id="Seg_2060" s="T138">di͡e-n</ta>
            <ta e="T140" id="Seg_2061" s="T139">barɨ</ta>
            <ta e="T141" id="Seg_2062" s="T140">kötör-dör</ta>
            <ta e="T142" id="Seg_2063" s="T141">čɨːčaːk-tar</ta>
            <ta e="T143" id="Seg_2064" s="T142">ü͡ögül-e-s-pit-tere</ta>
            <ta e="T144" id="Seg_2065" s="T143">min</ta>
            <ta e="T145" id="Seg_2066" s="T144">hin</ta>
            <ta e="T146" id="Seg_2067" s="T145">ol</ta>
            <ta e="T147" id="Seg_2068" s="T146">di͡ek</ta>
            <ta e="T148" id="Seg_2069" s="T147">köt-ü͡ö-m</ta>
            <ta e="T149" id="Seg_2070" s="T148">leŋkej</ta>
            <ta e="T150" id="Seg_2071" s="T149">di͡e-bit-e</ta>
            <ta e="T151" id="Seg_2072" s="T150">hübehit</ta>
            <ta e="T152" id="Seg_2073" s="T151">kaːs</ta>
            <ta e="T153" id="Seg_2074" s="T152">dogot-tor-u-ttan</ta>
            <ta e="T154" id="Seg_2075" s="T153">ɨjɨp-pɨt-a</ta>
            <ta e="T155" id="Seg_2076" s="T154">bu</ta>
            <ta e="T156" id="Seg_2077" s="T155">leŋkej</ta>
            <ta e="T157" id="Seg_2078" s="T156">bihigi-ni</ta>
            <ta e="T158" id="Seg_2079" s="T157">gɨtta</ta>
            <ta e="T159" id="Seg_2080" s="T158">bars-aːrɨ</ta>
            <ta e="T160" id="Seg_2081" s="T159">gɨn-ar</ta>
            <ta e="T161" id="Seg_2082" s="T160">iti</ta>
            <ta e="T162" id="Seg_2083" s="T161">tuh-u-nan</ta>
            <ta e="T163" id="Seg_2084" s="T162">ehigi</ta>
            <ta e="T164" id="Seg_2085" s="T163">kajtak</ta>
            <ta e="T165" id="Seg_2086" s="T164">d-iː</ta>
            <ta e="T166" id="Seg_2087" s="T165">han-ɨː-gɨt</ta>
            <ta e="T167" id="Seg_2088" s="T166">bihigi</ta>
            <ta e="T168" id="Seg_2089" s="T167">leŋkej-i</ta>
            <ta e="T169" id="Seg_2090" s="T168">beje-biti-n</ta>
            <ta e="T170" id="Seg_2091" s="T169">kɨtta</ta>
            <ta e="T171" id="Seg_2092" s="T170">ɨl-ɨ͡ak-pɨt</ta>
            <ta e="T172" id="Seg_2093" s="T171">hu͡og-a</ta>
            <ta e="T173" id="Seg_2094" s="T172">kötör-dör</ta>
            <ta e="T174" id="Seg_2095" s="T173">di͡e-bit-tere</ta>
            <ta e="T175" id="Seg_2096" s="T174">ehigi</ta>
            <ta e="T176" id="Seg_2097" s="T175">gini-ni</ta>
            <ta e="T177" id="Seg_2098" s="T176">togo</ta>
            <ta e="T178" id="Seg_2099" s="T177">ɨl-ɨ-m-aːrɨ</ta>
            <ta e="T179" id="Seg_2100" s="T178">gɨn-a-gɨt</ta>
            <ta e="T180" id="Seg_2101" s="T179">gini͡e-ke</ta>
            <ta e="T181" id="Seg_2102" s="T180">könö-tük</ta>
            <ta e="T182" id="Seg_2103" s="T181">haŋar-ɨ-ŋ</ta>
            <ta e="T183" id="Seg_2104" s="T182">kötör-dör</ta>
            <ta e="T184" id="Seg_2105" s="T183">leŋkej</ta>
            <ta e="T185" id="Seg_2106" s="T184">tuh-u-nan</ta>
            <ta e="T186" id="Seg_2107" s="T185">bɨha</ta>
            <ta e="T187" id="Seg_2108" s="T186">haŋar-bɨt-tara</ta>
            <ta e="T188" id="Seg_2109" s="T187">leŋkej</ta>
            <ta e="T189" id="Seg_2110" s="T188">manna</ta>
            <ta e="T190" id="Seg_2111" s="T189">bihigi</ta>
            <ta e="T191" id="Seg_2112" s="T190">kerget-ter-biti-n</ta>
            <ta e="T192" id="Seg_2113" s="T191">hi͡e-n</ta>
            <ta e="T193" id="Seg_2114" s="T192">olor-or</ta>
            <ta e="T194" id="Seg_2115" s="T193">gini</ta>
            <ta e="T195" id="Seg_2116" s="T194">beje-ti-n</ta>
            <ta e="T196" id="Seg_2117" s="T195">mi͡ere-ti-n</ta>
            <ta e="T197" id="Seg_2118" s="T196">onno</ta>
            <ta e="T198" id="Seg_2119" s="T197">da</ta>
            <ta e="T199" id="Seg_2120" s="T198">keːh-i͡e</ta>
            <ta e="T200" id="Seg_2121" s="T199">hu͡og-a</ta>
            <ta e="T201" id="Seg_2122" s="T200">bihigi</ta>
            <ta e="T202" id="Seg_2123" s="T201">gini-ni</ta>
            <ta e="T203" id="Seg_2124" s="T202">ɨl-ɨ͡ak-pɨt</ta>
            <ta e="T204" id="Seg_2125" s="T203">hu͡og-a</ta>
            <ta e="T205" id="Seg_2126" s="T204">hübehit</ta>
            <ta e="T206" id="Seg_2127" s="T205">kaːs</ta>
            <ta e="T207" id="Seg_2128" s="T206">leŋkej-ge</ta>
            <ta e="T208" id="Seg_2129" s="T207">diː-r</ta>
            <ta e="T209" id="Seg_2130" s="T208">kör-ö-gün</ta>
            <ta e="T210" id="Seg_2131" s="T209">du͡o</ta>
            <ta e="T211" id="Seg_2132" s="T210">kɨnat-taːk</ta>
            <ta e="T212" id="Seg_2133" s="T211">dʼon</ta>
            <ta e="T213" id="Seg_2134" s="T212">hübeleː-bit-teri-n</ta>
            <ta e="T214" id="Seg_2135" s="T213">barɨ-lara</ta>
            <ta e="T215" id="Seg_2136" s="T214">haŋar-bɨt-tara</ta>
            <ta e="T216" id="Seg_2137" s="T215">iti</ta>
            <ta e="T217" id="Seg_2138" s="T216">hoku͡on</ta>
            <ta e="T218" id="Seg_2139" s="T217">en</ta>
            <ta e="T219" id="Seg_2140" s="T218">manna</ta>
            <ta e="T220" id="Seg_2141" s="T219">kaːl-ɨ͡a-ŋ</ta>
            <ta e="T221" id="Seg_2142" s="T220">körd-ö-n-ön</ta>
            <ta e="T222" id="Seg_2143" s="T221">büt-e-bin</ta>
            <ta e="T223" id="Seg_2144" s="T222">leŋkej</ta>
            <ta e="T224" id="Seg_2145" s="T223">di͡e-bit-e</ta>
            <ta e="T225" id="Seg_2146" s="T224">manna</ta>
            <ta e="T226" id="Seg_2147" s="T225">kaːl-ɨ͡a-m</ta>
            <ta e="T227" id="Seg_2148" s="T226">bagar</ta>
            <ta e="T228" id="Seg_2149" s="T227">min</ta>
            <ta e="T229" id="Seg_2150" s="T228">manna</ta>
            <ta e="T230" id="Seg_2151" s="T229">tɨːnnaːk</ta>
            <ta e="T231" id="Seg_2152" s="T230">olor-u͡o-m</ta>
            <ta e="T232" id="Seg_2153" s="T231">barɨ</ta>
            <ta e="T233" id="Seg_2154" s="T232">kötör-dör</ta>
            <ta e="T234" id="Seg_2155" s="T233">čɨːčaːk-tar</ta>
            <ta e="T235" id="Seg_2156" s="T234">itiː</ta>
            <ta e="T236" id="Seg_2157" s="T235">hir-i</ta>
            <ta e="T237" id="Seg_2158" s="T236">körd-üː</ta>
            <ta e="T238" id="Seg_2159" s="T237">bar-bɨt-tara</ta>
            <ta e="T239" id="Seg_2160" s="T238">leŋkej</ta>
            <ta e="T240" id="Seg_2161" s="T239">dʼi͡e-ti-ger</ta>
            <ta e="T241" id="Seg_2162" s="T240">kel-en</ta>
            <ta e="T242" id="Seg_2163" s="T241">emeːksin-i-ger</ta>
            <ta e="T243" id="Seg_2164" s="T242">diː-r</ta>
            <ta e="T244" id="Seg_2165" s="T243">bil-e-gin</ta>
            <ta e="T245" id="Seg_2166" s="T244">du͡o</ta>
            <ta e="T246" id="Seg_2167" s="T245">bihigi-ni</ta>
            <ta e="T247" id="Seg_2168" s="T246">ɨl-bat-tarɨ-n</ta>
            <ta e="T248" id="Seg_2169" s="T247">mini͡e-ke</ta>
            <ta e="T249" id="Seg_2170" s="T248">ičiges</ta>
            <ta e="T250" id="Seg_2171" s="T249">hukuːj-u</ta>
            <ta e="T251" id="Seg_2172" s="T250">tik</ta>
            <ta e="T252" id="Seg_2173" s="T251">purgaː-ga</ta>
            <ta e="T253" id="Seg_2174" s="T252">daː</ta>
            <ta e="T254" id="Seg_2175" s="T253">tɨmnɨː-ga</ta>
            <ta e="T255" id="Seg_2176" s="T254">daː</ta>
            <ta e="T256" id="Seg_2177" s="T255">toŋ-mot</ta>
            <ta e="T257" id="Seg_2178" s="T256">bu͡ol-u͡ok-pu-n</ta>
            <ta e="T258" id="Seg_2179" s="T257">tig-i͡e-m</ta>
            <ta e="T259" id="Seg_2180" s="T258">ös</ta>
            <ta e="T260" id="Seg_2181" s="T259">kiːrbek</ta>
            <ta e="T261" id="Seg_2182" s="T260">emeːksin-e</ta>
            <ta e="T262" id="Seg_2183" s="T261">di͡e-bit-e</ta>
            <ta e="T263" id="Seg_2184" s="T262">hukuːj</ta>
            <ta e="T264" id="Seg_2185" s="T263">kaːr</ta>
            <ta e="T265" id="Seg_2186" s="T264">kördük</ta>
            <ta e="T266" id="Seg_2187" s="T265">čeːlkeː</ta>
            <ta e="T267" id="Seg_2188" s="T266">bu͡ol-lun</ta>
            <ta e="T268" id="Seg_2189" s="T267">leŋkej</ta>
            <ta e="T269" id="Seg_2190" s="T268">emeːksin-e</ta>
            <ta e="T270" id="Seg_2191" s="T269">čebis-čeːlkeː</ta>
            <ta e="T271" id="Seg_2192" s="T270">hukuːj-u</ta>
            <ta e="T272" id="Seg_2193" s="T271">tik-pit-e</ta>
            <ta e="T273" id="Seg_2194" s="T272">leŋkej</ta>
            <ta e="T274" id="Seg_2195" s="T273">hukuːj-u-n</ta>
            <ta e="T275" id="Seg_2196" s="T274">ket-eːt-in</ta>
            <ta e="T276" id="Seg_2197" s="T275">on-tu-ta</ta>
            <ta e="T277" id="Seg_2198" s="T276">et-i-ger</ta>
            <ta e="T278" id="Seg_2199" s="T277">hɨst-an</ta>
            <ta e="T279" id="Seg_2200" s="T278">kaːl-bɨt-a</ta>
            <ta e="T280" id="Seg_2201" s="T279">leŋkej</ta>
            <ta e="T281" id="Seg_2202" s="T280">čɨskaːn</ta>
            <ta e="T282" id="Seg_2203" s="T281">tɨmnɨː-ga</ta>
            <ta e="T283" id="Seg_2204" s="T282">daː</ta>
            <ta e="T284" id="Seg_2205" s="T283">toŋ-mot</ta>
            <ta e="T285" id="Seg_2206" s="T284">küːsteːk</ta>
            <ta e="T286" id="Seg_2207" s="T285">purgaː-ga</ta>
            <ta e="T287" id="Seg_2208" s="T286">daː</ta>
            <ta e="T288" id="Seg_2209" s="T287">ɨl-lar-bat</ta>
            <ta e="T289" id="Seg_2210" s="T288">hol</ta>
            <ta e="T290" id="Seg_2211" s="T289">kem-ten</ta>
            <ta e="T291" id="Seg_2212" s="T290">tuːs</ta>
            <ta e="T292" id="Seg_2213" s="T291">mu͡ora-ga</ta>
            <ta e="T293" id="Seg_2214" s="T292">leŋkej</ta>
            <ta e="T294" id="Seg_2215" s="T293">manna</ta>
            <ta e="T295" id="Seg_2216" s="T294">kanna</ta>
            <ta e="T296" id="Seg_2217" s="T295">da</ta>
            <ta e="T297" id="Seg_2218" s="T296">köp-pökkö</ta>
            <ta e="T298" id="Seg_2219" s="T297">kɨh-ɨ-l-ɨː</ta>
            <ta e="T299" id="Seg_2220" s="T298">olor-or</ta>
            <ta e="T300" id="Seg_2221" s="T299">leŋkej</ta>
            <ta e="T301" id="Seg_2222" s="T300">urukku-taːgar</ta>
            <ta e="T302" id="Seg_2223" s="T301">daː</ta>
            <ta e="T303" id="Seg_2224" s="T302">ulakan-nɨk</ta>
            <ta e="T304" id="Seg_2225" s="T303">kɨra</ta>
            <ta e="T305" id="Seg_2226" s="T304">čɨːčaːk-tar-ɨ</ta>
            <ta e="T306" id="Seg_2227" s="T305">kabi͡ekaːt-tar-ɨ</ta>
            <ta e="T307" id="Seg_2228" s="T306">hiː-r</ta>
            <ta e="T308" id="Seg_2229" s="T307">bu͡ol-but-a</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2230" s="T0">leŋkej</ta>
            <ta e="T2" id="Seg_2231" s="T1">ol</ta>
            <ta e="T3" id="Seg_2232" s="T2">aːt-tA</ta>
            <ta e="T4" id="Seg_2233" s="T3">dulgaːn-haka</ta>
            <ta e="T5" id="Seg_2234" s="T4">oloŋko-tA</ta>
            <ta e="T6" id="Seg_2235" s="T5">bɨlɨr-bɨlɨr</ta>
            <ta e="T7" id="Seg_2236" s="T6">mu͡ora</ta>
            <ta e="T8" id="Seg_2237" s="T7">ičiges</ta>
            <ta e="T9" id="Seg_2238" s="T8">e-TI-tA</ta>
            <ta e="T10" id="Seg_2239" s="T9">barɨ</ta>
            <ta e="T11" id="Seg_2240" s="T10">kötör-LAr</ta>
            <ta e="T12" id="Seg_2241" s="T11">čɨːčaːk-LAr</ta>
            <ta e="T13" id="Seg_2242" s="T12">kaːs-LAr</ta>
            <ta e="T14" id="Seg_2243" s="T13">kus-LAr</ta>
            <ta e="T15" id="Seg_2244" s="T14">haltaːkiː-LAr</ta>
            <ta e="T16" id="Seg_2245" s="T15">bu</ta>
            <ta e="T17" id="Seg_2246" s="T16">hir-GA</ta>
            <ta e="T18" id="Seg_2247" s="T17">olor-BIT-LArA</ta>
            <ta e="T19" id="Seg_2248" s="T18">ol-LAr-nI</ta>
            <ta e="T20" id="Seg_2249" s="T19">kɨtta</ta>
            <ta e="T21" id="Seg_2250" s="T20">biːrge</ta>
            <ta e="T22" id="Seg_2251" s="T21">leŋkej</ta>
            <ta e="T23" id="Seg_2252" s="T22">olor-BIT-tA</ta>
            <ta e="T24" id="Seg_2253" s="T23">gini</ta>
            <ta e="T25" id="Seg_2254" s="T24">ahaː-Ar</ta>
            <ta e="T26" id="Seg_2255" s="T25">as-LAr-tA</ta>
            <ta e="T27" id="Seg_2256" s="T26">kaːs-LAr</ta>
            <ta e="T28" id="Seg_2257" s="T27">kus-LAr</ta>
            <ta e="T29" id="Seg_2258" s="T28">kabɨjakaːn-LAr</ta>
            <ta e="T30" id="Seg_2259" s="T29">barɨ</ta>
            <ta e="T31" id="Seg_2260" s="T30">kötör-LAr</ta>
            <ta e="T32" id="Seg_2261" s="T31">čɨːčaːk-LAr</ta>
            <ta e="T33" id="Seg_2262" s="T32">ulakan-LArA</ta>
            <ta e="T34" id="Seg_2263" s="T33">kaːs</ta>
            <ta e="T35" id="Seg_2264" s="T34">onton</ta>
            <ta e="T36" id="Seg_2265" s="T35">tɨmnɨː-LAr</ta>
            <ta e="T37" id="Seg_2266" s="T36">bu͡ol-BIT-LArA</ta>
            <ta e="T38" id="Seg_2267" s="T37">hübehit</ta>
            <ta e="T39" id="Seg_2268" s="T38">kaːs</ta>
            <ta e="T40" id="Seg_2269" s="T39">barɨ</ta>
            <ta e="T41" id="Seg_2270" s="T40">kötör-LAr-nI</ta>
            <ta e="T42" id="Seg_2271" s="T41">čɨːčaːk-LAr-nI</ta>
            <ta e="T43" id="Seg_2272" s="T42">komuj-BIT-tA</ta>
            <ta e="T44" id="Seg_2273" s="T43">ulakan</ta>
            <ta e="T45" id="Seg_2274" s="T44">hübeleː-A-s-Iː</ta>
            <ta e="T46" id="Seg_2275" s="T45">bu͡ol-BIT-tA</ta>
            <ta e="T47" id="Seg_2276" s="T46">kötör-LAr</ta>
            <ta e="T48" id="Seg_2277" s="T47">čɨːčaːk-LAr</ta>
            <ta e="T49" id="Seg_2278" s="T48">komuj-LIN-An</ta>
            <ta e="T50" id="Seg_2279" s="T49">ki͡eŋ</ta>
            <ta e="T51" id="Seg_2280" s="T50">alɨː-GA</ta>
            <ta e="T52" id="Seg_2281" s="T51">onno</ta>
            <ta e="T53" id="Seg_2282" s="T52">hu͡opka</ta>
            <ta e="T54" id="Seg_2283" s="T53">alɨn-tI-GAr</ta>
            <ta e="T55" id="Seg_2284" s="T54">tu͡ol-An</ta>
            <ta e="T56" id="Seg_2285" s="T55">olor-BIT-LArA</ta>
            <ta e="T57" id="Seg_2286" s="T56">leŋkej</ta>
            <ta e="T58" id="Seg_2287" s="T57">emi͡e</ta>
            <ta e="T59" id="Seg_2288" s="T58">munnʼak-GA</ta>
            <ta e="T60" id="Seg_2289" s="T59">kel-BIT-tA</ta>
            <ta e="T61" id="Seg_2290" s="T60">hübehit</ta>
            <ta e="T62" id="Seg_2291" s="T61">kaːs</ta>
            <ta e="T63" id="Seg_2292" s="T62">ös-LAr-nI</ta>
            <ta e="T64" id="Seg_2293" s="T63">beje-tI-n</ta>
            <ta e="T65" id="Seg_2294" s="T64">dʼon-tI-GAr</ta>
            <ta e="T66" id="Seg_2295" s="T65">haŋar-BIT-tA</ta>
            <ta e="T67" id="Seg_2296" s="T66">kör-A-GIt</ta>
            <ta e="T68" id="Seg_2297" s="T67">du͡o</ta>
            <ta e="T69" id="Seg_2298" s="T68">kallaːn-BIt</ta>
            <ta e="T70" id="Seg_2299" s="T69">tɨmnɨj-TI-tA</ta>
            <ta e="T71" id="Seg_2300" s="T70">manna</ta>
            <ta e="T72" id="Seg_2301" s="T71">bihigi</ta>
            <ta e="T73" id="Seg_2302" s="T72">hajɨn</ta>
            <ta e="T74" id="Seg_2303" s="T73">ere</ta>
            <ta e="T75" id="Seg_2304" s="T74">olor-Ar</ta>
            <ta e="T76" id="Seg_2305" s="T75">bu͡ol-IAK-BIt</ta>
            <ta e="T77" id="Seg_2306" s="T76">min</ta>
            <ta e="T78" id="Seg_2307" s="T77">hir-nI</ta>
            <ta e="T79" id="Seg_2308" s="T78">bil-A-BIn</ta>
            <ta e="T80" id="Seg_2309" s="T79">kanna</ta>
            <ta e="T81" id="Seg_2310" s="T80">kɨhɨn</ta>
            <ta e="T82" id="Seg_2311" s="T81">bu͡ol-AːččI-tA</ta>
            <ta e="T83" id="Seg_2312" s="T82">hu͡ok</ta>
            <ta e="T84" id="Seg_2313" s="T83">ol</ta>
            <ta e="T85" id="Seg_2314" s="T84">olus</ta>
            <ta e="T86" id="Seg_2315" s="T85">ɨraːk</ta>
            <ta e="T87" id="Seg_2316" s="T86">hir-tI-nAn</ta>
            <ta e="T88" id="Seg_2317" s="T87">bar-TAK-GA</ta>
            <ta e="T89" id="Seg_2318" s="T88">tij-I-LIN-I-BAT</ta>
            <ta e="T90" id="Seg_2319" s="T89">bihigi</ta>
            <ta e="T93" id="Seg_2320" s="T92">kɨnat-LAːK-LAr</ta>
            <ta e="T94" id="Seg_2321" s="T93">bu</ta>
            <ta e="T95" id="Seg_2322" s="T94">hir-GA</ta>
            <ta e="T96" id="Seg_2323" s="T95">togo</ta>
            <ta e="T97" id="Seg_2324" s="T96">toŋ-A</ta>
            <ta e="T98" id="Seg_2325" s="T97">hɨt-IAK-BIt=Ij</ta>
            <ta e="T99" id="Seg_2326" s="T98">dʼe</ta>
            <ta e="T100" id="Seg_2327" s="T99">ol</ta>
            <ta e="T101" id="Seg_2328" s="T100">hir-GA</ta>
            <ta e="T102" id="Seg_2329" s="T101">könö-LIk</ta>
            <ta e="T103" id="Seg_2330" s="T102">köt-IAgIŋ</ta>
            <ta e="T104" id="Seg_2331" s="T103">bu</ta>
            <ta e="T105" id="Seg_2332" s="T104">tɨmnɨː-LAr</ta>
            <ta e="T106" id="Seg_2333" s="T105">aːs-IAK-LArI-GAr</ta>
            <ta e="T107" id="Seg_2334" s="T106">di͡eri</ta>
            <ta e="T108" id="Seg_2335" s="T107">onno</ta>
            <ta e="T109" id="Seg_2336" s="T108">olor-IAK-BIt</ta>
            <ta e="T110" id="Seg_2337" s="T109">anɨ</ta>
            <ta e="T111" id="Seg_2338" s="T110">manna</ta>
            <ta e="T112" id="Seg_2339" s="T111">bu</ta>
            <ta e="T113" id="Seg_2340" s="T112">kaːl-TAK-BItInA</ta>
            <ta e="T114" id="Seg_2341" s="T113">barɨ-BIt</ta>
            <ta e="T115" id="Seg_2342" s="T114">kam</ta>
            <ta e="T116" id="Seg_2343" s="T115">toŋ-An</ta>
            <ta e="T117" id="Seg_2344" s="T116">öl-IAK-BIt</ta>
            <ta e="T118" id="Seg_2345" s="T117">ol</ta>
            <ta e="T119" id="Seg_2346" s="T118">hir-GA</ta>
            <ta e="T120" id="Seg_2347" s="T119">köt-An</ta>
            <ta e="T121" id="Seg_2348" s="T120">bar-TAK-BItInA</ta>
            <ta e="T122" id="Seg_2349" s="T121">kihi</ta>
            <ta e="T123" id="Seg_2350" s="T122">bu͡ol-IAK-BIt</ta>
            <ta e="T124" id="Seg_2351" s="T123">kirdik</ta>
            <ta e="T125" id="Seg_2352" s="T124">haŋar-A-BIn</ta>
            <ta e="T126" id="Seg_2353" s="T125">du͡o</ta>
            <ta e="T127" id="Seg_2354" s="T126">kɨnat-LAːK-LAr</ta>
            <ta e="T128" id="Seg_2355" s="T127">kim</ta>
            <ta e="T129" id="Seg_2356" s="T128">ajannaː-Ar-nI</ta>
            <ta e="T130" id="Seg_2357" s="T129">höbüleː-Ar</ta>
            <ta e="T131" id="Seg_2358" s="T130">kirdik</ta>
            <ta e="T132" id="Seg_2359" s="T131">manna</ta>
            <ta e="T133" id="Seg_2360" s="T132">kaːl-TAK-BItInA</ta>
            <ta e="T134" id="Seg_2361" s="T133">öl-IAK-BIt</ta>
            <ta e="T135" id="Seg_2362" s="T134">bihigi</ta>
            <ta e="T136" id="Seg_2363" s="T135">barɨ-BIt</ta>
            <ta e="T137" id="Seg_2364" s="T136">onno</ta>
            <ta e="T138" id="Seg_2365" s="T137">köt-IAK-BIt</ta>
            <ta e="T139" id="Seg_2366" s="T138">di͡e-An</ta>
            <ta e="T140" id="Seg_2367" s="T139">barɨ</ta>
            <ta e="T141" id="Seg_2368" s="T140">kötör-LAr</ta>
            <ta e="T142" id="Seg_2369" s="T141">čɨːčaːk-LAr</ta>
            <ta e="T143" id="Seg_2370" s="T142">ü͡ögüleː-A-s-BIT-LArA</ta>
            <ta e="T144" id="Seg_2371" s="T143">min</ta>
            <ta e="T145" id="Seg_2372" s="T144">hin</ta>
            <ta e="T146" id="Seg_2373" s="T145">ol</ta>
            <ta e="T147" id="Seg_2374" s="T146">dek</ta>
            <ta e="T148" id="Seg_2375" s="T147">köt-IAK-m</ta>
            <ta e="T149" id="Seg_2376" s="T148">leŋkej</ta>
            <ta e="T150" id="Seg_2377" s="T149">di͡e-BIT-tA</ta>
            <ta e="T151" id="Seg_2378" s="T150">hübehit</ta>
            <ta e="T152" id="Seg_2379" s="T151">kaːs</ta>
            <ta e="T153" id="Seg_2380" s="T152">dogor-LAr-tI-ttAn</ta>
            <ta e="T154" id="Seg_2381" s="T153">ɨjɨt-BIT-tA</ta>
            <ta e="T155" id="Seg_2382" s="T154">bu</ta>
            <ta e="T156" id="Seg_2383" s="T155">leŋkej</ta>
            <ta e="T157" id="Seg_2384" s="T156">bihigi-nI</ta>
            <ta e="T158" id="Seg_2385" s="T157">kɨtta</ta>
            <ta e="T159" id="Seg_2386" s="T158">barɨs-AːrI</ta>
            <ta e="T160" id="Seg_2387" s="T159">gɨn-Ar</ta>
            <ta e="T161" id="Seg_2388" s="T160">iti</ta>
            <ta e="T162" id="Seg_2389" s="T161">tus-tI-nAn</ta>
            <ta e="T163" id="Seg_2390" s="T162">ehigi</ta>
            <ta e="T164" id="Seg_2391" s="T163">kajdak</ta>
            <ta e="T165" id="Seg_2392" s="T164">di͡e-A</ta>
            <ta e="T166" id="Seg_2393" s="T165">hanaː-A-GIt</ta>
            <ta e="T167" id="Seg_2394" s="T166">bihigi</ta>
            <ta e="T168" id="Seg_2395" s="T167">leŋkej-nI</ta>
            <ta e="T169" id="Seg_2396" s="T168">beje-BItI-n</ta>
            <ta e="T170" id="Seg_2397" s="T169">kɨtta</ta>
            <ta e="T171" id="Seg_2398" s="T170">ɨl-IAK-BIt</ta>
            <ta e="T172" id="Seg_2399" s="T171">hu͡ok-tA</ta>
            <ta e="T173" id="Seg_2400" s="T172">kötör-LAr</ta>
            <ta e="T174" id="Seg_2401" s="T173">di͡e-BIT-LArA</ta>
            <ta e="T175" id="Seg_2402" s="T174">ehigi</ta>
            <ta e="T176" id="Seg_2403" s="T175">gini-nI</ta>
            <ta e="T177" id="Seg_2404" s="T176">togo</ta>
            <ta e="T178" id="Seg_2405" s="T177">ɨl-I-m-AːrI</ta>
            <ta e="T179" id="Seg_2406" s="T178">gɨn-A-GIt</ta>
            <ta e="T180" id="Seg_2407" s="T179">gini-GA</ta>
            <ta e="T181" id="Seg_2408" s="T180">könö-LIk</ta>
            <ta e="T182" id="Seg_2409" s="T181">haŋar-I-ŋ</ta>
            <ta e="T183" id="Seg_2410" s="T182">kötör-LAr</ta>
            <ta e="T184" id="Seg_2411" s="T183">leŋkej</ta>
            <ta e="T185" id="Seg_2412" s="T184">tus-tI-nAn</ta>
            <ta e="T186" id="Seg_2413" s="T185">bɨha</ta>
            <ta e="T187" id="Seg_2414" s="T186">haŋar-BIT-LArA</ta>
            <ta e="T188" id="Seg_2415" s="T187">leŋkej</ta>
            <ta e="T189" id="Seg_2416" s="T188">manna</ta>
            <ta e="T190" id="Seg_2417" s="T189">bihigi</ta>
            <ta e="T191" id="Seg_2418" s="T190">kergen-LAr-BItI-n</ta>
            <ta e="T192" id="Seg_2419" s="T191">hi͡e-An</ta>
            <ta e="T193" id="Seg_2420" s="T192">olor-Ar</ta>
            <ta e="T194" id="Seg_2421" s="T193">gini</ta>
            <ta e="T195" id="Seg_2422" s="T194">beje-tI-n</ta>
            <ta e="T196" id="Seg_2423" s="T195">mi͡ere-tI-n</ta>
            <ta e="T197" id="Seg_2424" s="T196">onno</ta>
            <ta e="T198" id="Seg_2425" s="T197">da</ta>
            <ta e="T199" id="Seg_2426" s="T198">keːs-IAK.[tA]</ta>
            <ta e="T200" id="Seg_2427" s="T199">hu͡ok-tA</ta>
            <ta e="T201" id="Seg_2428" s="T200">bihigi</ta>
            <ta e="T202" id="Seg_2429" s="T201">gini-nI</ta>
            <ta e="T203" id="Seg_2430" s="T202">ɨl-IAK-BIt</ta>
            <ta e="T204" id="Seg_2431" s="T203">hu͡ok-tA</ta>
            <ta e="T205" id="Seg_2432" s="T204">hübehit</ta>
            <ta e="T206" id="Seg_2433" s="T205">kaːs</ta>
            <ta e="T207" id="Seg_2434" s="T206">leŋkej-GA</ta>
            <ta e="T208" id="Seg_2435" s="T207">di͡e-Ar</ta>
            <ta e="T209" id="Seg_2436" s="T208">kör-A-GIn</ta>
            <ta e="T210" id="Seg_2437" s="T209">du͡o</ta>
            <ta e="T211" id="Seg_2438" s="T210">kɨnat-LAːK</ta>
            <ta e="T212" id="Seg_2439" s="T211">dʼon</ta>
            <ta e="T213" id="Seg_2440" s="T212">hübeleː-BIT-LArI-n</ta>
            <ta e="T214" id="Seg_2441" s="T213">barɨ-LArA</ta>
            <ta e="T215" id="Seg_2442" s="T214">haŋar-BIT-LArA</ta>
            <ta e="T216" id="Seg_2443" s="T215">iti</ta>
            <ta e="T217" id="Seg_2444" s="T216">hoku͡on</ta>
            <ta e="T218" id="Seg_2445" s="T217">en</ta>
            <ta e="T219" id="Seg_2446" s="T218">manna</ta>
            <ta e="T220" id="Seg_2447" s="T219">kaːl-IAK-ŋ</ta>
            <ta e="T221" id="Seg_2448" s="T220">kördöː-A-n-An</ta>
            <ta e="T222" id="Seg_2449" s="T221">büt-A-BIn</ta>
            <ta e="T223" id="Seg_2450" s="T222">leŋkej</ta>
            <ta e="T224" id="Seg_2451" s="T223">di͡e-BIT-tA</ta>
            <ta e="T225" id="Seg_2452" s="T224">manna</ta>
            <ta e="T226" id="Seg_2453" s="T225">kaːl-IAK-m</ta>
            <ta e="T227" id="Seg_2454" s="T226">bagar</ta>
            <ta e="T228" id="Seg_2455" s="T227">min</ta>
            <ta e="T229" id="Seg_2456" s="T228">manna</ta>
            <ta e="T230" id="Seg_2457" s="T229">tɨːnnaːk</ta>
            <ta e="T231" id="Seg_2458" s="T230">olor-IAK-m</ta>
            <ta e="T232" id="Seg_2459" s="T231">barɨ</ta>
            <ta e="T233" id="Seg_2460" s="T232">kötör-LAr</ta>
            <ta e="T234" id="Seg_2461" s="T233">čɨːčaːk-LAr</ta>
            <ta e="T235" id="Seg_2462" s="T234">itiː</ta>
            <ta e="T236" id="Seg_2463" s="T235">hir-nI</ta>
            <ta e="T237" id="Seg_2464" s="T236">kördöː-A</ta>
            <ta e="T238" id="Seg_2465" s="T237">bar-BIT-LArA</ta>
            <ta e="T239" id="Seg_2466" s="T238">leŋkej</ta>
            <ta e="T240" id="Seg_2467" s="T239">dʼi͡e-tI-GAr</ta>
            <ta e="T241" id="Seg_2468" s="T240">kel-An</ta>
            <ta e="T242" id="Seg_2469" s="T241">emeːksin-tI-GAr</ta>
            <ta e="T243" id="Seg_2470" s="T242">di͡e-Ar</ta>
            <ta e="T244" id="Seg_2471" s="T243">bil-A-GIn</ta>
            <ta e="T245" id="Seg_2472" s="T244">du͡o</ta>
            <ta e="T246" id="Seg_2473" s="T245">bihigi-nI</ta>
            <ta e="T247" id="Seg_2474" s="T246">ɨl-BAT-LArI-n</ta>
            <ta e="T248" id="Seg_2475" s="T247">min-GA</ta>
            <ta e="T249" id="Seg_2476" s="T248">ičiges</ta>
            <ta e="T250" id="Seg_2477" s="T249">hokuj-nI</ta>
            <ta e="T251" id="Seg_2478" s="T250">tik</ta>
            <ta e="T252" id="Seg_2479" s="T251">purgaː-GA</ta>
            <ta e="T253" id="Seg_2480" s="T252">da</ta>
            <ta e="T254" id="Seg_2481" s="T253">tɨmnɨː-GA</ta>
            <ta e="T255" id="Seg_2482" s="T254">da</ta>
            <ta e="T256" id="Seg_2483" s="T255">toŋ-BAT</ta>
            <ta e="T257" id="Seg_2484" s="T256">bu͡ol-IAK-BI-n</ta>
            <ta e="T258" id="Seg_2485" s="T257">tik-IAK-m</ta>
            <ta e="T259" id="Seg_2486" s="T258">ös</ta>
            <ta e="T260" id="Seg_2487" s="T259">kiːrbek</ta>
            <ta e="T261" id="Seg_2488" s="T260">emeːksin-tA</ta>
            <ta e="T262" id="Seg_2489" s="T261">di͡e-BIT-tA</ta>
            <ta e="T263" id="Seg_2490" s="T262">hokuj</ta>
            <ta e="T264" id="Seg_2491" s="T263">kaːr</ta>
            <ta e="T265" id="Seg_2492" s="T264">kördük</ta>
            <ta e="T266" id="Seg_2493" s="T265">čeːlkeː</ta>
            <ta e="T267" id="Seg_2494" s="T266">bu͡ol-TIn</ta>
            <ta e="T268" id="Seg_2495" s="T267">leŋkej</ta>
            <ta e="T269" id="Seg_2496" s="T268">emeːksin-tA</ta>
            <ta e="T270" id="Seg_2497" s="T269">[C^1][V^1][C^2]-čeːlkeː</ta>
            <ta e="T271" id="Seg_2498" s="T270">hokuj-nI</ta>
            <ta e="T272" id="Seg_2499" s="T271">tik-BIT-tA</ta>
            <ta e="T273" id="Seg_2500" s="T272">leŋkej</ta>
            <ta e="T274" id="Seg_2501" s="T273">hokuj-tI-n</ta>
            <ta e="T275" id="Seg_2502" s="T274">ket-AːT-In</ta>
            <ta e="T276" id="Seg_2503" s="T275">ol-tI-tA</ta>
            <ta e="T277" id="Seg_2504" s="T276">et-tI-GAr</ta>
            <ta e="T278" id="Seg_2505" s="T277">hɨhɨn-An</ta>
            <ta e="T279" id="Seg_2506" s="T278">kaːl-BIT-tA</ta>
            <ta e="T280" id="Seg_2507" s="T279">leŋkej</ta>
            <ta e="T281" id="Seg_2508" s="T280">čɨskaːn</ta>
            <ta e="T282" id="Seg_2509" s="T281">tɨmnɨː-GA</ta>
            <ta e="T283" id="Seg_2510" s="T282">da</ta>
            <ta e="T284" id="Seg_2511" s="T283">toŋ-BAT</ta>
            <ta e="T285" id="Seg_2512" s="T284">küːsteːk</ta>
            <ta e="T286" id="Seg_2513" s="T285">purgaː-GA</ta>
            <ta e="T287" id="Seg_2514" s="T286">da</ta>
            <ta e="T288" id="Seg_2515" s="T287">ɨl-TAr-BAT</ta>
            <ta e="T289" id="Seg_2516" s="T288">hol</ta>
            <ta e="T290" id="Seg_2517" s="T289">kem-ttAn</ta>
            <ta e="T291" id="Seg_2518" s="T290">tuːs</ta>
            <ta e="T292" id="Seg_2519" s="T291">mu͡ora-GA</ta>
            <ta e="T293" id="Seg_2520" s="T292">leŋkej</ta>
            <ta e="T294" id="Seg_2521" s="T293">manna</ta>
            <ta e="T295" id="Seg_2522" s="T294">kanna</ta>
            <ta e="T296" id="Seg_2523" s="T295">da</ta>
            <ta e="T297" id="Seg_2524" s="T296">köt-BAkkA</ta>
            <ta e="T298" id="Seg_2525" s="T297">kɨs-I-LAː-A</ta>
            <ta e="T299" id="Seg_2526" s="T298">olor-Ar</ta>
            <ta e="T300" id="Seg_2527" s="T299">leŋkej</ta>
            <ta e="T301" id="Seg_2528" s="T300">urukku-TAːgAr</ta>
            <ta e="T302" id="Seg_2529" s="T301">da</ta>
            <ta e="T303" id="Seg_2530" s="T302">ulakan-LIk</ta>
            <ta e="T304" id="Seg_2531" s="T303">kɨra</ta>
            <ta e="T305" id="Seg_2532" s="T304">čɨːčaːk-LAr-nI</ta>
            <ta e="T306" id="Seg_2533" s="T305">kabɨjakaːn-LAr-nI</ta>
            <ta e="T307" id="Seg_2534" s="T306">hi͡e-Ar</ta>
            <ta e="T308" id="Seg_2535" s="T307">bu͡ol-BIT-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2536" s="T0">snow.owl.[NOM]</ta>
            <ta e="T2" id="Seg_2537" s="T1">that.[NOM]</ta>
            <ta e="T3" id="Seg_2538" s="T2">name-3SG.[NOM]</ta>
            <ta e="T4" id="Seg_2539" s="T3">Dolgan-Dolgan</ta>
            <ta e="T5" id="Seg_2540" s="T4">tale-3SG.[NOM]</ta>
            <ta e="T6" id="Seg_2541" s="T5">long.ago-long.ago</ta>
            <ta e="T7" id="Seg_2542" s="T6">tundra.[NOM]</ta>
            <ta e="T8" id="Seg_2543" s="T7">warm.[NOM]</ta>
            <ta e="T9" id="Seg_2544" s="T8">be-PST1-3SG</ta>
            <ta e="T10" id="Seg_2545" s="T9">every</ta>
            <ta e="T11" id="Seg_2546" s="T10">bird-PL.[NOM]</ta>
            <ta e="T12" id="Seg_2547" s="T11">small.bird-PL.[NOM]</ta>
            <ta e="T13" id="Seg_2548" s="T12">goose-PL.[NOM]</ta>
            <ta e="T14" id="Seg_2549" s="T13">duck-PL.[NOM]</ta>
            <ta e="T15" id="Seg_2550" s="T14">small.bird-PL.[NOM]</ta>
            <ta e="T16" id="Seg_2551" s="T15">this</ta>
            <ta e="T17" id="Seg_2552" s="T16">earth-DAT/LOC</ta>
            <ta e="T18" id="Seg_2553" s="T17">live-PST2-3PL</ta>
            <ta e="T19" id="Seg_2554" s="T18">that-PL-ACC</ta>
            <ta e="T20" id="Seg_2555" s="T19">with</ta>
            <ta e="T21" id="Seg_2556" s="T20">together</ta>
            <ta e="T22" id="Seg_2557" s="T21">snow.owl.[NOM]</ta>
            <ta e="T23" id="Seg_2558" s="T22">live-PST2-3SG</ta>
            <ta e="T24" id="Seg_2559" s="T23">3SG.[NOM]</ta>
            <ta e="T25" id="Seg_2560" s="T24">eat-PTCP.PRS</ta>
            <ta e="T26" id="Seg_2561" s="T25">food-PL-3SG.[NOM]</ta>
            <ta e="T27" id="Seg_2562" s="T26">goose-PL.[NOM]</ta>
            <ta e="T28" id="Seg_2563" s="T27">duck-PL.[NOM]</ta>
            <ta e="T29" id="Seg_2564" s="T28">partridge-PL.[NOM]</ta>
            <ta e="T30" id="Seg_2565" s="T29">every</ta>
            <ta e="T31" id="Seg_2566" s="T30">bird-PL.[NOM]</ta>
            <ta e="T32" id="Seg_2567" s="T31">small.bird-PL.[NOM]</ta>
            <ta e="T33" id="Seg_2568" s="T32">big-3PL.[NOM]</ta>
            <ta e="T34" id="Seg_2569" s="T33">goose.[NOM]</ta>
            <ta e="T35" id="Seg_2570" s="T34">then</ta>
            <ta e="T36" id="Seg_2571" s="T35">cold-PL.[NOM]</ta>
            <ta e="T37" id="Seg_2572" s="T36">be-PST2-3PL</ta>
            <ta e="T38" id="Seg_2573" s="T37">leader.[NOM]</ta>
            <ta e="T39" id="Seg_2574" s="T38">goose.[NOM]</ta>
            <ta e="T40" id="Seg_2575" s="T39">every</ta>
            <ta e="T41" id="Seg_2576" s="T40">bird-PL-ACC</ta>
            <ta e="T42" id="Seg_2577" s="T41">small.bird-PL-ACC</ta>
            <ta e="T43" id="Seg_2578" s="T42">gather-PST2-3SG</ta>
            <ta e="T44" id="Seg_2579" s="T43">big</ta>
            <ta e="T45" id="Seg_2580" s="T44">advise-EP-RECP/COLL-NMNZ.[NOM]</ta>
            <ta e="T46" id="Seg_2581" s="T45">be-PST2-3SG</ta>
            <ta e="T47" id="Seg_2582" s="T46">bird-PL.[NOM]</ta>
            <ta e="T48" id="Seg_2583" s="T47">small.bird-PL.[NOM]</ta>
            <ta e="T49" id="Seg_2584" s="T48">gather-PASS/REFL-CVB.SEQ</ta>
            <ta e="T50" id="Seg_2585" s="T49">broad</ta>
            <ta e="T51" id="Seg_2586" s="T50">glade-DAT/LOC</ta>
            <ta e="T52" id="Seg_2587" s="T51">there</ta>
            <ta e="T53" id="Seg_2588" s="T52">hill.[NOM]</ta>
            <ta e="T54" id="Seg_2589" s="T53">lower.part-3SG-DAT/LOC</ta>
            <ta e="T55" id="Seg_2590" s="T54">fill.oneself-CVB.SEQ</ta>
            <ta e="T56" id="Seg_2591" s="T55">sit.down-PST2-3PL</ta>
            <ta e="T57" id="Seg_2592" s="T56">snow.owl.[NOM]</ta>
            <ta e="T58" id="Seg_2593" s="T57">also</ta>
            <ta e="T59" id="Seg_2594" s="T58">gathering-DAT/LOC</ta>
            <ta e="T60" id="Seg_2595" s="T59">come-PST2-3SG</ta>
            <ta e="T61" id="Seg_2596" s="T60">leader.[NOM]</ta>
            <ta e="T62" id="Seg_2597" s="T61">goose.[NOM]</ta>
            <ta e="T63" id="Seg_2598" s="T62">news-PL-ACC</ta>
            <ta e="T64" id="Seg_2599" s="T63">self-3SG-GEN</ta>
            <ta e="T65" id="Seg_2600" s="T64">people-3SG-DAT/LOC</ta>
            <ta e="T66" id="Seg_2601" s="T65">speak-PST2-3SG</ta>
            <ta e="T67" id="Seg_2602" s="T66">see-PRS-2PL</ta>
            <ta e="T68" id="Seg_2603" s="T67">Q</ta>
            <ta e="T69" id="Seg_2604" s="T68">weather-1PL.[NOM]</ta>
            <ta e="T70" id="Seg_2605" s="T69">get.colder-PST1-3SG</ta>
            <ta e="T71" id="Seg_2606" s="T70">here</ta>
            <ta e="T72" id="Seg_2607" s="T71">1PL.[NOM]</ta>
            <ta e="T73" id="Seg_2608" s="T72">in.summer</ta>
            <ta e="T74" id="Seg_2609" s="T73">just</ta>
            <ta e="T75" id="Seg_2610" s="T74">live-PTCP.PRS</ta>
            <ta e="T76" id="Seg_2611" s="T75">be-FUT-1PL</ta>
            <ta e="T77" id="Seg_2612" s="T76">1SG.[NOM]</ta>
            <ta e="T78" id="Seg_2613" s="T77">place-ACC</ta>
            <ta e="T79" id="Seg_2614" s="T78">know-PRS-1SG</ta>
            <ta e="T80" id="Seg_2615" s="T79">where</ta>
            <ta e="T81" id="Seg_2616" s="T80">winter.[NOM]</ta>
            <ta e="T82" id="Seg_2617" s="T81">be-PTCP.HAB-3SG</ta>
            <ta e="T83" id="Seg_2618" s="T82">NEG.[3SG]</ta>
            <ta e="T84" id="Seg_2619" s="T83">that.[NOM]</ta>
            <ta e="T85" id="Seg_2620" s="T84">very</ta>
            <ta e="T86" id="Seg_2621" s="T85">distant.[NOM]</ta>
            <ta e="T87" id="Seg_2622" s="T86">earth-3SG-INSTR</ta>
            <ta e="T88" id="Seg_2623" s="T87">go-PTCP.COND-DAT/LOC</ta>
            <ta e="T89" id="Seg_2624" s="T88">reach-EP-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T90" id="Seg_2625" s="T89">1PL.[NOM]</ta>
            <ta e="T93" id="Seg_2626" s="T92">wing-PROPR-PL.[NOM]</ta>
            <ta e="T94" id="Seg_2627" s="T93">this</ta>
            <ta e="T95" id="Seg_2628" s="T94">place-DAT/LOC</ta>
            <ta e="T96" id="Seg_2629" s="T95">why</ta>
            <ta e="T97" id="Seg_2630" s="T96">freeze-CVB.SIM</ta>
            <ta e="T98" id="Seg_2631" s="T97">lie-FUT-1PL=Q</ta>
            <ta e="T99" id="Seg_2632" s="T98">well</ta>
            <ta e="T100" id="Seg_2633" s="T99">that</ta>
            <ta e="T101" id="Seg_2634" s="T100">place-DAT/LOC</ta>
            <ta e="T102" id="Seg_2635" s="T101">direct-ADVZ</ta>
            <ta e="T103" id="Seg_2636" s="T102">fly-IMP.1PL</ta>
            <ta e="T104" id="Seg_2637" s="T103">this</ta>
            <ta e="T105" id="Seg_2638" s="T104">cold-PL.[NOM]</ta>
            <ta e="T106" id="Seg_2639" s="T105">pass.by-PTCP.FUT-3PL-DAT/LOC</ta>
            <ta e="T107" id="Seg_2640" s="T106">until</ta>
            <ta e="T108" id="Seg_2641" s="T107">there</ta>
            <ta e="T109" id="Seg_2642" s="T108">live-FUT-1PL</ta>
            <ta e="T110" id="Seg_2643" s="T109">now</ta>
            <ta e="T111" id="Seg_2644" s="T110">here</ta>
            <ta e="T112" id="Seg_2645" s="T111">this</ta>
            <ta e="T113" id="Seg_2646" s="T112">stay-TEMP-1PL</ta>
            <ta e="T114" id="Seg_2647" s="T113">every-1PL.[NOM]</ta>
            <ta e="T115" id="Seg_2648" s="T114">strongly</ta>
            <ta e="T116" id="Seg_2649" s="T115">freeze-CVB.SEQ</ta>
            <ta e="T117" id="Seg_2650" s="T116">die-FUT-1PL</ta>
            <ta e="T118" id="Seg_2651" s="T117">that</ta>
            <ta e="T119" id="Seg_2652" s="T118">place-DAT/LOC</ta>
            <ta e="T120" id="Seg_2653" s="T119">fly-CVB.SEQ</ta>
            <ta e="T121" id="Seg_2654" s="T120">go-TEMP-1PL</ta>
            <ta e="T122" id="Seg_2655" s="T121">human.being.[NOM]</ta>
            <ta e="T123" id="Seg_2656" s="T122">be-FUT-1PL</ta>
            <ta e="T124" id="Seg_2657" s="T123">truth.[NOM]</ta>
            <ta e="T125" id="Seg_2658" s="T124">speak-PRS-1SG</ta>
            <ta e="T126" id="Seg_2659" s="T125">Q</ta>
            <ta e="T127" id="Seg_2660" s="T126">wing-PROPR-PL.[NOM]</ta>
            <ta e="T128" id="Seg_2661" s="T127">who.[NOM]</ta>
            <ta e="T129" id="Seg_2662" s="T128">travel-PTCP.PRS-ACC</ta>
            <ta e="T130" id="Seg_2663" s="T129">agree-PRS.[3SG]</ta>
            <ta e="T131" id="Seg_2664" s="T130">truth.[NOM]</ta>
            <ta e="T132" id="Seg_2665" s="T131">here</ta>
            <ta e="T133" id="Seg_2666" s="T132">stay-TEMP-1PL</ta>
            <ta e="T134" id="Seg_2667" s="T133">die-FUT-1PL</ta>
            <ta e="T135" id="Seg_2668" s="T134">1PL.[NOM]</ta>
            <ta e="T136" id="Seg_2669" s="T135">every-1PL.[NOM]</ta>
            <ta e="T137" id="Seg_2670" s="T136">thither</ta>
            <ta e="T138" id="Seg_2671" s="T137">fly-FUT-1PL</ta>
            <ta e="T139" id="Seg_2672" s="T138">say-CVB.SEQ</ta>
            <ta e="T140" id="Seg_2673" s="T139">every</ta>
            <ta e="T141" id="Seg_2674" s="T140">bird-PL.[NOM]</ta>
            <ta e="T142" id="Seg_2675" s="T141">small.bird-PL.[NOM]</ta>
            <ta e="T143" id="Seg_2676" s="T142">shout-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T144" id="Seg_2677" s="T143">1SG.[NOM]</ta>
            <ta e="T145" id="Seg_2678" s="T144">however</ta>
            <ta e="T146" id="Seg_2679" s="T145">that.[NOM]</ta>
            <ta e="T147" id="Seg_2680" s="T146">to</ta>
            <ta e="T148" id="Seg_2681" s="T147">fly-FUT-1SG</ta>
            <ta e="T149" id="Seg_2682" s="T148">snow.owl.[NOM]</ta>
            <ta e="T150" id="Seg_2683" s="T149">say-PST2-3SG</ta>
            <ta e="T151" id="Seg_2684" s="T150">leader.[NOM]</ta>
            <ta e="T152" id="Seg_2685" s="T151">goose.[NOM]</ta>
            <ta e="T153" id="Seg_2686" s="T152">friend-PL-3SG-ABL</ta>
            <ta e="T154" id="Seg_2687" s="T153">ask-PST2-3SG</ta>
            <ta e="T155" id="Seg_2688" s="T154">this</ta>
            <ta e="T156" id="Seg_2689" s="T155">snow.owl.[NOM]</ta>
            <ta e="T157" id="Seg_2690" s="T156">1PL-ACC</ta>
            <ta e="T158" id="Seg_2691" s="T157">with</ta>
            <ta e="T159" id="Seg_2692" s="T158">come.along-CVB.PURP</ta>
            <ta e="T160" id="Seg_2693" s="T159">want-PRS.[3SG]</ta>
            <ta e="T161" id="Seg_2694" s="T160">that.[NOM]</ta>
            <ta e="T162" id="Seg_2695" s="T161">side-3SG-INSTR</ta>
            <ta e="T163" id="Seg_2696" s="T162">2PL.[NOM]</ta>
            <ta e="T164" id="Seg_2697" s="T163">how</ta>
            <ta e="T165" id="Seg_2698" s="T164">think-CVB.SIM</ta>
            <ta e="T166" id="Seg_2699" s="T165">think-PRS-2PL</ta>
            <ta e="T167" id="Seg_2700" s="T166">1PL.[NOM]</ta>
            <ta e="T168" id="Seg_2701" s="T167">snow.owl-ACC</ta>
            <ta e="T169" id="Seg_2702" s="T168">self-1PL-ACC</ta>
            <ta e="T170" id="Seg_2703" s="T169">with</ta>
            <ta e="T171" id="Seg_2704" s="T170">take-FUT-1PL</ta>
            <ta e="T172" id="Seg_2705" s="T171">NEG-3SG</ta>
            <ta e="T173" id="Seg_2706" s="T172">bird-PL.[NOM]</ta>
            <ta e="T174" id="Seg_2707" s="T173">say-PST2-3PL</ta>
            <ta e="T175" id="Seg_2708" s="T174">2PL.[NOM]</ta>
            <ta e="T176" id="Seg_2709" s="T175">3SG-ACC</ta>
            <ta e="T177" id="Seg_2710" s="T176">why</ta>
            <ta e="T178" id="Seg_2711" s="T177">take-EP-NEG-CVB.PURP</ta>
            <ta e="T179" id="Seg_2712" s="T178">want-PRS-2PL</ta>
            <ta e="T180" id="Seg_2713" s="T179">3SG-DAT/LOC</ta>
            <ta e="T181" id="Seg_2714" s="T180">direct-ADVZ</ta>
            <ta e="T182" id="Seg_2715" s="T181">say-EP-IMP.2PL</ta>
            <ta e="T183" id="Seg_2716" s="T182">bird-PL.[NOM]</ta>
            <ta e="T184" id="Seg_2717" s="T183">snow.owl.[NOM]</ta>
            <ta e="T185" id="Seg_2718" s="T184">side-3SG-INSTR</ta>
            <ta e="T186" id="Seg_2719" s="T185">through</ta>
            <ta e="T187" id="Seg_2720" s="T186">speak-PST2-3PL</ta>
            <ta e="T188" id="Seg_2721" s="T187">snow.owl.[NOM]</ta>
            <ta e="T189" id="Seg_2722" s="T188">here</ta>
            <ta e="T190" id="Seg_2723" s="T189">1PL.[NOM]</ta>
            <ta e="T191" id="Seg_2724" s="T190">family-PL-1PL-ACC</ta>
            <ta e="T192" id="Seg_2725" s="T191">eat-CVB.SEQ</ta>
            <ta e="T193" id="Seg_2726" s="T192">sit-PRS.[3SG]</ta>
            <ta e="T194" id="Seg_2727" s="T193">3SG.[NOM]</ta>
            <ta e="T195" id="Seg_2728" s="T194">self-3SG-GEN</ta>
            <ta e="T196" id="Seg_2729" s="T195">custom-3SG-ACC</ta>
            <ta e="T197" id="Seg_2730" s="T196">there</ta>
            <ta e="T198" id="Seg_2731" s="T197">NEG</ta>
            <ta e="T199" id="Seg_2732" s="T198">throw-FUT.[3SG]</ta>
            <ta e="T200" id="Seg_2733" s="T199">NEG-3SG</ta>
            <ta e="T201" id="Seg_2734" s="T200">1PL.[NOM]</ta>
            <ta e="T202" id="Seg_2735" s="T201">3SG-ACC</ta>
            <ta e="T203" id="Seg_2736" s="T202">take-FUT-1PL</ta>
            <ta e="T204" id="Seg_2737" s="T203">NEG-3SG</ta>
            <ta e="T205" id="Seg_2738" s="T204">leader.[NOM]</ta>
            <ta e="T206" id="Seg_2739" s="T205">goose.[NOM]</ta>
            <ta e="T207" id="Seg_2740" s="T206">snow.owl-DAT/LOC</ta>
            <ta e="T208" id="Seg_2741" s="T207">say-PRS.[3SG]</ta>
            <ta e="T209" id="Seg_2742" s="T208">see-PRS-2SG</ta>
            <ta e="T210" id="Seg_2743" s="T209">Q</ta>
            <ta e="T211" id="Seg_2744" s="T210">wing-PROPR</ta>
            <ta e="T212" id="Seg_2745" s="T211">people.[NOM]</ta>
            <ta e="T213" id="Seg_2746" s="T212">advise-PTCP.PST-3PL-ACC</ta>
            <ta e="T214" id="Seg_2747" s="T213">every-3PL.[NOM]</ta>
            <ta e="T215" id="Seg_2748" s="T214">speak-PST2-3PL</ta>
            <ta e="T216" id="Seg_2749" s="T215">that.[NOM]</ta>
            <ta e="T217" id="Seg_2750" s="T216">law.[NOM]</ta>
            <ta e="T218" id="Seg_2751" s="T217">2SG.[NOM]</ta>
            <ta e="T219" id="Seg_2752" s="T218">here</ta>
            <ta e="T220" id="Seg_2753" s="T219">stay-FUT-2SG</ta>
            <ta e="T221" id="Seg_2754" s="T220">beg-EP-MED-CVB.SEQ</ta>
            <ta e="T222" id="Seg_2755" s="T221">stop-PRS-1SG</ta>
            <ta e="T223" id="Seg_2756" s="T222">snow.owl.[NOM]</ta>
            <ta e="T224" id="Seg_2757" s="T223">say-PST2-3SG</ta>
            <ta e="T225" id="Seg_2758" s="T224">here</ta>
            <ta e="T226" id="Seg_2759" s="T225">stay-FUT-1SG</ta>
            <ta e="T227" id="Seg_2760" s="T226">maybe</ta>
            <ta e="T228" id="Seg_2761" s="T227">1SG.[NOM]</ta>
            <ta e="T229" id="Seg_2762" s="T228">here</ta>
            <ta e="T230" id="Seg_2763" s="T229">alive.[NOM]</ta>
            <ta e="T231" id="Seg_2764" s="T230">live-FUT-1SG</ta>
            <ta e="T232" id="Seg_2765" s="T231">every</ta>
            <ta e="T233" id="Seg_2766" s="T232">bird-PL.[NOM]</ta>
            <ta e="T234" id="Seg_2767" s="T233">small.bird-PL.[NOM]</ta>
            <ta e="T235" id="Seg_2768" s="T234">warm</ta>
            <ta e="T236" id="Seg_2769" s="T235">place-ACC</ta>
            <ta e="T237" id="Seg_2770" s="T236">search-CVB.SIM</ta>
            <ta e="T238" id="Seg_2771" s="T237">go-PST2-3PL</ta>
            <ta e="T239" id="Seg_2772" s="T238">snow.owl.[NOM]</ta>
            <ta e="T240" id="Seg_2773" s="T239">house-3SG-DAT/LOC</ta>
            <ta e="T241" id="Seg_2774" s="T240">come-CVB.SEQ</ta>
            <ta e="T242" id="Seg_2775" s="T241">old.woman-3SG-DAT/LOC</ta>
            <ta e="T243" id="Seg_2776" s="T242">say-PRS.[3SG]</ta>
            <ta e="T244" id="Seg_2777" s="T243">know-PRS-2SG</ta>
            <ta e="T245" id="Seg_2778" s="T244">Q</ta>
            <ta e="T246" id="Seg_2779" s="T245">1PL-ACC</ta>
            <ta e="T247" id="Seg_2780" s="T246">take-NEG.PTCP-3PL-ACC</ta>
            <ta e="T248" id="Seg_2781" s="T247">1SG-DAT/LOC</ta>
            <ta e="T249" id="Seg_2782" s="T248">warm</ta>
            <ta e="T250" id="Seg_2783" s="T249">long.coat-ACC</ta>
            <ta e="T251" id="Seg_2784" s="T250">sew.[IMP.2SG]</ta>
            <ta e="T252" id="Seg_2785" s="T251">snowstorm-DAT/LOC</ta>
            <ta e="T253" id="Seg_2786" s="T252">NEG</ta>
            <ta e="T254" id="Seg_2787" s="T253">cold-DAT/LOC</ta>
            <ta e="T255" id="Seg_2788" s="T254">and</ta>
            <ta e="T256" id="Seg_2789" s="T255">freeze-NEG.PTCP</ta>
            <ta e="T257" id="Seg_2790" s="T256">be-PTCP.FUT-1SG-ACC</ta>
            <ta e="T258" id="Seg_2791" s="T257">sew-FUT-1SG</ta>
            <ta e="T259" id="Seg_2792" s="T258">word.[NOM]</ta>
            <ta e="T260" id="Seg_2793" s="T259">submissive</ta>
            <ta e="T261" id="Seg_2794" s="T260">old.woman-3SG.[NOM]</ta>
            <ta e="T262" id="Seg_2795" s="T261">say-PST2-3SG</ta>
            <ta e="T263" id="Seg_2796" s="T262">long.coat.[NOM]</ta>
            <ta e="T264" id="Seg_2797" s="T263">snow.[NOM]</ta>
            <ta e="T265" id="Seg_2798" s="T264">similar</ta>
            <ta e="T266" id="Seg_2799" s="T265">white.[NOM]</ta>
            <ta e="T267" id="Seg_2800" s="T266">be-IMP.3SG</ta>
            <ta e="T268" id="Seg_2801" s="T267">snow.owl.[NOM]</ta>
            <ta e="T269" id="Seg_2802" s="T268">old.woman-3SG.[NOM]</ta>
            <ta e="T270" id="Seg_2803" s="T269">EMPH-white</ta>
            <ta e="T271" id="Seg_2804" s="T270">long.coat-ACC</ta>
            <ta e="T272" id="Seg_2805" s="T271">sew-PST2-3SG</ta>
            <ta e="T273" id="Seg_2806" s="T272">snow.owl.[NOM]</ta>
            <ta e="T274" id="Seg_2807" s="T273">long.coat-3SG-ACC</ta>
            <ta e="T275" id="Seg_2808" s="T274">dress-CVB.ANT-3SG</ta>
            <ta e="T276" id="Seg_2809" s="T275">that-3SG-3SG.[NOM]</ta>
            <ta e="T277" id="Seg_2810" s="T276">body-3SG-DAT/LOC</ta>
            <ta e="T278" id="Seg_2811" s="T277">stick-CVB.SEQ</ta>
            <ta e="T279" id="Seg_2812" s="T278">stay-PST2-3SG</ta>
            <ta e="T280" id="Seg_2813" s="T279">snow.owl.[NOM]</ta>
            <ta e="T281" id="Seg_2814" s="T280">frosty</ta>
            <ta e="T282" id="Seg_2815" s="T281">cold-DAT/LOC</ta>
            <ta e="T283" id="Seg_2816" s="T282">NEG</ta>
            <ta e="T284" id="Seg_2817" s="T283">freeze-NEG.[3SG]</ta>
            <ta e="T285" id="Seg_2818" s="T284">strong</ta>
            <ta e="T286" id="Seg_2819" s="T285">snowstorm-DAT/LOC</ta>
            <ta e="T287" id="Seg_2820" s="T286">NEG</ta>
            <ta e="T288" id="Seg_2821" s="T287">take-CAUS-NEG.[3SG]</ta>
            <ta e="T289" id="Seg_2822" s="T288">that.EMPH</ta>
            <ta e="T290" id="Seg_2823" s="T289">time-ABL</ta>
            <ta e="T291" id="Seg_2824" s="T290">salt.[NOM]</ta>
            <ta e="T292" id="Seg_2825" s="T291">tundra-DAT/LOC</ta>
            <ta e="T293" id="Seg_2826" s="T292">snow.owl.[NOM]</ta>
            <ta e="T294" id="Seg_2827" s="T293">here</ta>
            <ta e="T295" id="Seg_2828" s="T294">whereto</ta>
            <ta e="T296" id="Seg_2829" s="T295">NEG</ta>
            <ta e="T297" id="Seg_2830" s="T296">fly-NEG.CVB.SIM</ta>
            <ta e="T298" id="Seg_2831" s="T297">winter-EP-VBZ-CVB.SIM</ta>
            <ta e="T299" id="Seg_2832" s="T298">sit-PRS.[3SG]</ta>
            <ta e="T300" id="Seg_2833" s="T299">snow.owl.[NOM]</ta>
            <ta e="T301" id="Seg_2834" s="T300">former-COMP</ta>
            <ta e="T302" id="Seg_2835" s="T301">EMPH</ta>
            <ta e="T303" id="Seg_2836" s="T302">big-ADVZ</ta>
            <ta e="T304" id="Seg_2837" s="T303">small</ta>
            <ta e="T305" id="Seg_2838" s="T304">small.bird-PL-ACC</ta>
            <ta e="T306" id="Seg_2839" s="T305">partridge-PL-ACC</ta>
            <ta e="T307" id="Seg_2840" s="T306">eat-PTCP.PRS</ta>
            <ta e="T308" id="Seg_2841" s="T307">become-PST2-3SG</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_2842" s="T0">Schneeeule.[NOM]</ta>
            <ta e="T2" id="Seg_2843" s="T1">jenes.[NOM]</ta>
            <ta e="T3" id="Seg_2844" s="T2">Name-3SG.[NOM]</ta>
            <ta e="T4" id="Seg_2845" s="T3">dolganisch-dolganisch</ta>
            <ta e="T5" id="Seg_2846" s="T4">Märchen-3SG.[NOM]</ta>
            <ta e="T6" id="Seg_2847" s="T5">vor.langer.Zeit-vor.langer.Zeit</ta>
            <ta e="T7" id="Seg_2848" s="T6">Tundra.[NOM]</ta>
            <ta e="T8" id="Seg_2849" s="T7">warm.[NOM]</ta>
            <ta e="T9" id="Seg_2850" s="T8">sein-PST1-3SG</ta>
            <ta e="T10" id="Seg_2851" s="T9">jeder</ta>
            <ta e="T11" id="Seg_2852" s="T10">Vogel-PL.[NOM]</ta>
            <ta e="T12" id="Seg_2853" s="T11">Vögelchen-PL.[NOM]</ta>
            <ta e="T13" id="Seg_2854" s="T12">Gans-PL.[NOM]</ta>
            <ta e="T14" id="Seg_2855" s="T13">Ente-PL.[NOM]</ta>
            <ta e="T15" id="Seg_2856" s="T14">kleiner.Vogel-PL.[NOM]</ta>
            <ta e="T16" id="Seg_2857" s="T15">dieses</ta>
            <ta e="T17" id="Seg_2858" s="T16">Erde-DAT/LOC</ta>
            <ta e="T18" id="Seg_2859" s="T17">leben-PST2-3PL</ta>
            <ta e="T19" id="Seg_2860" s="T18">jenes-PL-ACC</ta>
            <ta e="T20" id="Seg_2861" s="T19">mit</ta>
            <ta e="T21" id="Seg_2862" s="T20">zusammen</ta>
            <ta e="T22" id="Seg_2863" s="T21">Schneeeule.[NOM]</ta>
            <ta e="T23" id="Seg_2864" s="T22">leben-PST2-3SG</ta>
            <ta e="T24" id="Seg_2865" s="T23">3SG.[NOM]</ta>
            <ta e="T25" id="Seg_2866" s="T24">essen-PTCP.PRS</ta>
            <ta e="T26" id="Seg_2867" s="T25">Nahrung-PL-3SG.[NOM]</ta>
            <ta e="T27" id="Seg_2868" s="T26">Gans-PL.[NOM]</ta>
            <ta e="T28" id="Seg_2869" s="T27">Ente-PL.[NOM]</ta>
            <ta e="T29" id="Seg_2870" s="T28">Rebhuhn-PL.[NOM]</ta>
            <ta e="T30" id="Seg_2871" s="T29">jeder</ta>
            <ta e="T31" id="Seg_2872" s="T30">Vogel-PL.[NOM]</ta>
            <ta e="T32" id="Seg_2873" s="T31">Vögelchen-PL.[NOM]</ta>
            <ta e="T33" id="Seg_2874" s="T32">groß-3PL.[NOM]</ta>
            <ta e="T34" id="Seg_2875" s="T33">Gans.[NOM]</ta>
            <ta e="T35" id="Seg_2876" s="T34">dann</ta>
            <ta e="T36" id="Seg_2877" s="T35">Kälte-PL.[NOM]</ta>
            <ta e="T37" id="Seg_2878" s="T36">sein-PST2-3PL</ta>
            <ta e="T38" id="Seg_2879" s="T37">Anführer.[NOM]</ta>
            <ta e="T39" id="Seg_2880" s="T38">Gans.[NOM]</ta>
            <ta e="T40" id="Seg_2881" s="T39">jeder</ta>
            <ta e="T41" id="Seg_2882" s="T40">Vogel-PL-ACC</ta>
            <ta e="T42" id="Seg_2883" s="T41">Vögelchen-PL-ACC</ta>
            <ta e="T43" id="Seg_2884" s="T42">sammeln-PST2-3SG</ta>
            <ta e="T44" id="Seg_2885" s="T43">groß</ta>
            <ta e="T45" id="Seg_2886" s="T44">raten-EP-RECP/COLL-NMNZ.[NOM]</ta>
            <ta e="T46" id="Seg_2887" s="T45">sein-PST2-3SG</ta>
            <ta e="T47" id="Seg_2888" s="T46">Vogel-PL.[NOM]</ta>
            <ta e="T48" id="Seg_2889" s="T47">Vögelchen-PL.[NOM]</ta>
            <ta e="T49" id="Seg_2890" s="T48">sammeln-PASS/REFL-CVB.SEQ</ta>
            <ta e="T50" id="Seg_2891" s="T49">breit</ta>
            <ta e="T51" id="Seg_2892" s="T50">Lichtung-DAT/LOC</ta>
            <ta e="T52" id="Seg_2893" s="T51">dort</ta>
            <ta e="T53" id="Seg_2894" s="T52">Hügel.[NOM]</ta>
            <ta e="T54" id="Seg_2895" s="T53">Unterteil-3SG-DAT/LOC</ta>
            <ta e="T55" id="Seg_2896" s="T54">sich.füllen-CVB.SEQ</ta>
            <ta e="T56" id="Seg_2897" s="T55">sich.setzen-PST2-3PL</ta>
            <ta e="T57" id="Seg_2898" s="T56">Schneeeule.[NOM]</ta>
            <ta e="T58" id="Seg_2899" s="T57">auch</ta>
            <ta e="T59" id="Seg_2900" s="T58">Versammlung-DAT/LOC</ta>
            <ta e="T60" id="Seg_2901" s="T59">kommen-PST2-3SG</ta>
            <ta e="T61" id="Seg_2902" s="T60">Anführer.[NOM]</ta>
            <ta e="T62" id="Seg_2903" s="T61">Gans.[NOM]</ta>
            <ta e="T63" id="Seg_2904" s="T62">Neuigkeit-PL-ACC</ta>
            <ta e="T64" id="Seg_2905" s="T63">selbst-3SG-GEN</ta>
            <ta e="T65" id="Seg_2906" s="T64">Leute-3SG-DAT/LOC</ta>
            <ta e="T66" id="Seg_2907" s="T65">sprechen-PST2-3SG</ta>
            <ta e="T67" id="Seg_2908" s="T66">sehen-PRS-2PL</ta>
            <ta e="T68" id="Seg_2909" s="T67">Q</ta>
            <ta e="T69" id="Seg_2910" s="T68">Wetter-1PL.[NOM]</ta>
            <ta e="T70" id="Seg_2911" s="T69">kälter.werden-PST1-3SG</ta>
            <ta e="T71" id="Seg_2912" s="T70">hier</ta>
            <ta e="T72" id="Seg_2913" s="T71">1PL.[NOM]</ta>
            <ta e="T73" id="Seg_2914" s="T72">im.Sommer</ta>
            <ta e="T74" id="Seg_2915" s="T73">nur</ta>
            <ta e="T75" id="Seg_2916" s="T74">leben-PTCP.PRS</ta>
            <ta e="T76" id="Seg_2917" s="T75">sein-FUT-1PL</ta>
            <ta e="T77" id="Seg_2918" s="T76">1SG.[NOM]</ta>
            <ta e="T78" id="Seg_2919" s="T77">Ort-ACC</ta>
            <ta e="T79" id="Seg_2920" s="T78">wissen-PRS-1SG</ta>
            <ta e="T80" id="Seg_2921" s="T79">wo</ta>
            <ta e="T81" id="Seg_2922" s="T80">Winter.[NOM]</ta>
            <ta e="T82" id="Seg_2923" s="T81">sein-PTCP.HAB-3SG</ta>
            <ta e="T83" id="Seg_2924" s="T82">NEG.[3SG]</ta>
            <ta e="T84" id="Seg_2925" s="T83">jenes.[NOM]</ta>
            <ta e="T85" id="Seg_2926" s="T84">sehr</ta>
            <ta e="T86" id="Seg_2927" s="T85">fern.[NOM]</ta>
            <ta e="T87" id="Seg_2928" s="T86">Erde-3SG-INSTR</ta>
            <ta e="T88" id="Seg_2929" s="T87">gehen-PTCP.COND-DAT/LOC</ta>
            <ta e="T89" id="Seg_2930" s="T88">ankommen-EP-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T90" id="Seg_2931" s="T89">1PL.[NOM]</ta>
            <ta e="T93" id="Seg_2932" s="T92">Flügel-PROPR-PL.[NOM]</ta>
            <ta e="T94" id="Seg_2933" s="T93">dieses</ta>
            <ta e="T95" id="Seg_2934" s="T94">Ort-DAT/LOC</ta>
            <ta e="T96" id="Seg_2935" s="T95">warum</ta>
            <ta e="T97" id="Seg_2936" s="T96">frieren-CVB.SIM</ta>
            <ta e="T98" id="Seg_2937" s="T97">liegen-FUT-1PL=Q</ta>
            <ta e="T99" id="Seg_2938" s="T98">doch</ta>
            <ta e="T100" id="Seg_2939" s="T99">jenes</ta>
            <ta e="T101" id="Seg_2940" s="T100">Ort-DAT/LOC</ta>
            <ta e="T102" id="Seg_2941" s="T101">direkt-ADVZ</ta>
            <ta e="T103" id="Seg_2942" s="T102">fliegen-IMP.1PL</ta>
            <ta e="T104" id="Seg_2943" s="T103">dieses</ta>
            <ta e="T105" id="Seg_2944" s="T104">Kälte-PL.[NOM]</ta>
            <ta e="T106" id="Seg_2945" s="T105">vorbeigehen-PTCP.FUT-3PL-DAT/LOC</ta>
            <ta e="T107" id="Seg_2946" s="T106">bis.zu</ta>
            <ta e="T108" id="Seg_2947" s="T107">dort</ta>
            <ta e="T109" id="Seg_2948" s="T108">leben-FUT-1PL</ta>
            <ta e="T110" id="Seg_2949" s="T109">jetzt</ta>
            <ta e="T111" id="Seg_2950" s="T110">hier</ta>
            <ta e="T112" id="Seg_2951" s="T111">dieses</ta>
            <ta e="T113" id="Seg_2952" s="T112">bleiben-TEMP-1PL</ta>
            <ta e="T114" id="Seg_2953" s="T113">jeder-1PL.[NOM]</ta>
            <ta e="T115" id="Seg_2954" s="T114">heftig</ta>
            <ta e="T116" id="Seg_2955" s="T115">frieren-CVB.SEQ</ta>
            <ta e="T117" id="Seg_2956" s="T116">sterben-FUT-1PL</ta>
            <ta e="T118" id="Seg_2957" s="T117">jenes</ta>
            <ta e="T119" id="Seg_2958" s="T118">Ort-DAT/LOC</ta>
            <ta e="T120" id="Seg_2959" s="T119">fliegen-CVB.SEQ</ta>
            <ta e="T121" id="Seg_2960" s="T120">gehen-TEMP-1PL</ta>
            <ta e="T122" id="Seg_2961" s="T121">Mensch.[NOM]</ta>
            <ta e="T123" id="Seg_2962" s="T122">sein-FUT-1PL</ta>
            <ta e="T124" id="Seg_2963" s="T123">Wahrheit.[NOM]</ta>
            <ta e="T125" id="Seg_2964" s="T124">sprechen-PRS-1SG</ta>
            <ta e="T126" id="Seg_2965" s="T125">Q</ta>
            <ta e="T127" id="Seg_2966" s="T126">Flügel-PROPR-PL.[NOM]</ta>
            <ta e="T128" id="Seg_2967" s="T127">wer.[NOM]</ta>
            <ta e="T129" id="Seg_2968" s="T128">reisen-PTCP.PRS-ACC</ta>
            <ta e="T130" id="Seg_2969" s="T129">einverstanden.sein-PRS.[3SG]</ta>
            <ta e="T131" id="Seg_2970" s="T130">Wahrheit.[NOM]</ta>
            <ta e="T132" id="Seg_2971" s="T131">hier</ta>
            <ta e="T133" id="Seg_2972" s="T132">bleiben-TEMP-1PL</ta>
            <ta e="T134" id="Seg_2973" s="T133">sterben-FUT-1PL</ta>
            <ta e="T135" id="Seg_2974" s="T134">1PL.[NOM]</ta>
            <ta e="T136" id="Seg_2975" s="T135">jeder-1PL.[NOM]</ta>
            <ta e="T137" id="Seg_2976" s="T136">dorthin</ta>
            <ta e="T138" id="Seg_2977" s="T137">fliegen-FUT-1PL</ta>
            <ta e="T139" id="Seg_2978" s="T138">sagen-CVB.SEQ</ta>
            <ta e="T140" id="Seg_2979" s="T139">jeder</ta>
            <ta e="T141" id="Seg_2980" s="T140">Vogel-PL.[NOM]</ta>
            <ta e="T142" id="Seg_2981" s="T141">Vögelchen-PL.[NOM]</ta>
            <ta e="T143" id="Seg_2982" s="T142">schreien-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T144" id="Seg_2983" s="T143">1SG.[NOM]</ta>
            <ta e="T145" id="Seg_2984" s="T144">doch</ta>
            <ta e="T146" id="Seg_2985" s="T145">jenes.[NOM]</ta>
            <ta e="T147" id="Seg_2986" s="T146">zu</ta>
            <ta e="T148" id="Seg_2987" s="T147">fliegen-FUT-1SG</ta>
            <ta e="T149" id="Seg_2988" s="T148">Schneeeule.[NOM]</ta>
            <ta e="T150" id="Seg_2989" s="T149">sagen-PST2-3SG</ta>
            <ta e="T151" id="Seg_2990" s="T150">Anführer.[NOM]</ta>
            <ta e="T152" id="Seg_2991" s="T151">Gans.[NOM]</ta>
            <ta e="T153" id="Seg_2992" s="T152">Freund-PL-3SG-ABL</ta>
            <ta e="T154" id="Seg_2993" s="T153">fragen-PST2-3SG</ta>
            <ta e="T155" id="Seg_2994" s="T154">dieses</ta>
            <ta e="T156" id="Seg_2995" s="T155">Schneeeule.[NOM]</ta>
            <ta e="T157" id="Seg_2996" s="T156">1PL-ACC</ta>
            <ta e="T158" id="Seg_2997" s="T157">mit</ta>
            <ta e="T159" id="Seg_2998" s="T158">mitkommen-CVB.PURP</ta>
            <ta e="T160" id="Seg_2999" s="T159">wollen-PRS.[3SG]</ta>
            <ta e="T161" id="Seg_3000" s="T160">dieses.[NOM]</ta>
            <ta e="T162" id="Seg_3001" s="T161">Seite-3SG-INSTR</ta>
            <ta e="T163" id="Seg_3002" s="T162">2PL.[NOM]</ta>
            <ta e="T164" id="Seg_3003" s="T163">wie</ta>
            <ta e="T165" id="Seg_3004" s="T164">denken-CVB.SIM</ta>
            <ta e="T166" id="Seg_3005" s="T165">denken-PRS-2PL</ta>
            <ta e="T167" id="Seg_3006" s="T166">1PL.[NOM]</ta>
            <ta e="T168" id="Seg_3007" s="T167">Schneeeule-ACC</ta>
            <ta e="T169" id="Seg_3008" s="T168">selbst-1PL-ACC</ta>
            <ta e="T170" id="Seg_3009" s="T169">mit</ta>
            <ta e="T171" id="Seg_3010" s="T170">nehmen-FUT-1PL</ta>
            <ta e="T172" id="Seg_3011" s="T171">NEG-3SG</ta>
            <ta e="T173" id="Seg_3012" s="T172">Vogel-PL.[NOM]</ta>
            <ta e="T174" id="Seg_3013" s="T173">sagen-PST2-3PL</ta>
            <ta e="T175" id="Seg_3014" s="T174">2PL.[NOM]</ta>
            <ta e="T176" id="Seg_3015" s="T175">3SG-ACC</ta>
            <ta e="T177" id="Seg_3016" s="T176">warum</ta>
            <ta e="T178" id="Seg_3017" s="T177">nehmen-EP-NEG-CVB.PURP</ta>
            <ta e="T179" id="Seg_3018" s="T178">wollen-PRS-2PL</ta>
            <ta e="T180" id="Seg_3019" s="T179">3SG-DAT/LOC</ta>
            <ta e="T181" id="Seg_3020" s="T180">direkt-ADVZ</ta>
            <ta e="T182" id="Seg_3021" s="T181">sagen-EP-IMP.2PL</ta>
            <ta e="T183" id="Seg_3022" s="T182">Vogel-PL.[NOM]</ta>
            <ta e="T184" id="Seg_3023" s="T183">Schneeeule.[NOM]</ta>
            <ta e="T185" id="Seg_3024" s="T184">Seite-3SG-INSTR</ta>
            <ta e="T186" id="Seg_3025" s="T185">durch</ta>
            <ta e="T187" id="Seg_3026" s="T186">sprechen-PST2-3PL</ta>
            <ta e="T188" id="Seg_3027" s="T187">Schneeeule.[NOM]</ta>
            <ta e="T189" id="Seg_3028" s="T188">hier</ta>
            <ta e="T190" id="Seg_3029" s="T189">1PL.[NOM]</ta>
            <ta e="T191" id="Seg_3030" s="T190">Familie-PL-1PL-ACC</ta>
            <ta e="T192" id="Seg_3031" s="T191">essen-CVB.SEQ</ta>
            <ta e="T193" id="Seg_3032" s="T192">sitzen-PRS.[3SG]</ta>
            <ta e="T194" id="Seg_3033" s="T193">3SG.[NOM]</ta>
            <ta e="T195" id="Seg_3034" s="T194">selbst-3SG-GEN</ta>
            <ta e="T196" id="Seg_3035" s="T195">Brauch-3SG-ACC</ta>
            <ta e="T197" id="Seg_3036" s="T196">dort</ta>
            <ta e="T198" id="Seg_3037" s="T197">NEG</ta>
            <ta e="T199" id="Seg_3038" s="T198">werfen-FUT.[3SG]</ta>
            <ta e="T200" id="Seg_3039" s="T199">NEG-3SG</ta>
            <ta e="T201" id="Seg_3040" s="T200">1PL.[NOM]</ta>
            <ta e="T202" id="Seg_3041" s="T201">3SG-ACC</ta>
            <ta e="T203" id="Seg_3042" s="T202">nehmen-FUT-1PL</ta>
            <ta e="T204" id="Seg_3043" s="T203">NEG-3SG</ta>
            <ta e="T205" id="Seg_3044" s="T204">Anführer.[NOM]</ta>
            <ta e="T206" id="Seg_3045" s="T205">Gans.[NOM]</ta>
            <ta e="T207" id="Seg_3046" s="T206">Schneeeule-DAT/LOC</ta>
            <ta e="T208" id="Seg_3047" s="T207">sagen-PRS.[3SG]</ta>
            <ta e="T209" id="Seg_3048" s="T208">sehen-PRS-2SG</ta>
            <ta e="T210" id="Seg_3049" s="T209">Q</ta>
            <ta e="T211" id="Seg_3050" s="T210">Flügel-PROPR</ta>
            <ta e="T212" id="Seg_3051" s="T211">Volk.[NOM]</ta>
            <ta e="T213" id="Seg_3052" s="T212">raten-PTCP.PST-3PL-ACC</ta>
            <ta e="T214" id="Seg_3053" s="T213">jeder-3PL.[NOM]</ta>
            <ta e="T215" id="Seg_3054" s="T214">sprechen-PST2-3PL</ta>
            <ta e="T216" id="Seg_3055" s="T215">dieses.[NOM]</ta>
            <ta e="T217" id="Seg_3056" s="T216">Gesetz.[NOM]</ta>
            <ta e="T218" id="Seg_3057" s="T217">2SG.[NOM]</ta>
            <ta e="T219" id="Seg_3058" s="T218">hier</ta>
            <ta e="T220" id="Seg_3059" s="T219">bleiben-FUT-2SG</ta>
            <ta e="T221" id="Seg_3060" s="T220">bitten-EP-MED-CVB.SEQ</ta>
            <ta e="T222" id="Seg_3061" s="T221">aufhören-PRS-1SG</ta>
            <ta e="T223" id="Seg_3062" s="T222">Schneeeule.[NOM]</ta>
            <ta e="T224" id="Seg_3063" s="T223">sagen-PST2-3SG</ta>
            <ta e="T225" id="Seg_3064" s="T224">hier</ta>
            <ta e="T226" id="Seg_3065" s="T225">bleiben-FUT-1SG</ta>
            <ta e="T227" id="Seg_3066" s="T226">vielleicht</ta>
            <ta e="T228" id="Seg_3067" s="T227">1SG.[NOM]</ta>
            <ta e="T229" id="Seg_3068" s="T228">hier</ta>
            <ta e="T230" id="Seg_3069" s="T229">lebendig.[NOM]</ta>
            <ta e="T231" id="Seg_3070" s="T230">leben-FUT-1SG</ta>
            <ta e="T232" id="Seg_3071" s="T231">jeder</ta>
            <ta e="T233" id="Seg_3072" s="T232">Vogel-PL.[NOM]</ta>
            <ta e="T234" id="Seg_3073" s="T233">Vögelchen-PL.[NOM]</ta>
            <ta e="T235" id="Seg_3074" s="T234">warm</ta>
            <ta e="T236" id="Seg_3075" s="T235">Ort-ACC</ta>
            <ta e="T237" id="Seg_3076" s="T236">suchen-CVB.SIM</ta>
            <ta e="T238" id="Seg_3077" s="T237">gehen-PST2-3PL</ta>
            <ta e="T239" id="Seg_3078" s="T238">Schneeeule.[NOM]</ta>
            <ta e="T240" id="Seg_3079" s="T239">Haus-3SG-DAT/LOC</ta>
            <ta e="T241" id="Seg_3080" s="T240">kommen-CVB.SEQ</ta>
            <ta e="T242" id="Seg_3081" s="T241">Alte-3SG-DAT/LOC</ta>
            <ta e="T243" id="Seg_3082" s="T242">sagen-PRS.[3SG]</ta>
            <ta e="T244" id="Seg_3083" s="T243">wissen-PRS-2SG</ta>
            <ta e="T245" id="Seg_3084" s="T244">Q</ta>
            <ta e="T246" id="Seg_3085" s="T245">1PL-ACC</ta>
            <ta e="T247" id="Seg_3086" s="T246">nehmen-NEG.PTCP-3PL-ACC</ta>
            <ta e="T248" id="Seg_3087" s="T247">1SG-DAT/LOC</ta>
            <ta e="T249" id="Seg_3088" s="T248">warm</ta>
            <ta e="T250" id="Seg_3089" s="T249">langer.Mantel-ACC</ta>
            <ta e="T251" id="Seg_3090" s="T250">nähen.[IMP.2SG]</ta>
            <ta e="T252" id="Seg_3091" s="T251">Schneesturm-DAT/LOC</ta>
            <ta e="T253" id="Seg_3092" s="T252">NEG</ta>
            <ta e="T254" id="Seg_3093" s="T253">Kälte-DAT/LOC</ta>
            <ta e="T255" id="Seg_3094" s="T254">und</ta>
            <ta e="T256" id="Seg_3095" s="T255">frieren-NEG.PTCP</ta>
            <ta e="T257" id="Seg_3096" s="T256">sein-PTCP.FUT-1SG-ACC</ta>
            <ta e="T258" id="Seg_3097" s="T257">nähen-FUT-1SG</ta>
            <ta e="T259" id="Seg_3098" s="T258">Wort.[NOM]</ta>
            <ta e="T260" id="Seg_3099" s="T259">gefügig</ta>
            <ta e="T261" id="Seg_3100" s="T260">Alte-3SG.[NOM]</ta>
            <ta e="T262" id="Seg_3101" s="T261">sagen-PST2-3SG</ta>
            <ta e="T263" id="Seg_3102" s="T262">langer.Mantel.[NOM]</ta>
            <ta e="T264" id="Seg_3103" s="T263">Schnee.[NOM]</ta>
            <ta e="T265" id="Seg_3104" s="T264">ähnlich</ta>
            <ta e="T266" id="Seg_3105" s="T265">weiß.[NOM]</ta>
            <ta e="T267" id="Seg_3106" s="T266">sein-IMP.3SG</ta>
            <ta e="T268" id="Seg_3107" s="T267">Schneeeule.[NOM]</ta>
            <ta e="T269" id="Seg_3108" s="T268">Alte-3SG.[NOM]</ta>
            <ta e="T270" id="Seg_3109" s="T269">EMPH-weiß</ta>
            <ta e="T271" id="Seg_3110" s="T270">langer.Mantel-ACC</ta>
            <ta e="T272" id="Seg_3111" s="T271">nähen-PST2-3SG</ta>
            <ta e="T273" id="Seg_3112" s="T272">Schneeeule.[NOM]</ta>
            <ta e="T274" id="Seg_3113" s="T273">langer.Mantel-3SG-ACC</ta>
            <ta e="T275" id="Seg_3114" s="T274">anziehen-CVB.ANT-3SG</ta>
            <ta e="T276" id="Seg_3115" s="T275">jenes-3SG-3SG.[NOM]</ta>
            <ta e="T277" id="Seg_3116" s="T276">Körper-3SG-DAT/LOC</ta>
            <ta e="T278" id="Seg_3117" s="T277">haften-CVB.SEQ</ta>
            <ta e="T279" id="Seg_3118" s="T278">bleiben-PST2-3SG</ta>
            <ta e="T280" id="Seg_3119" s="T279">Schneeeule.[NOM]</ta>
            <ta e="T281" id="Seg_3120" s="T280">frostig</ta>
            <ta e="T282" id="Seg_3121" s="T281">Kälte-DAT/LOC</ta>
            <ta e="T283" id="Seg_3122" s="T282">NEG</ta>
            <ta e="T284" id="Seg_3123" s="T283">frieren-NEG.[3SG]</ta>
            <ta e="T285" id="Seg_3124" s="T284">stark</ta>
            <ta e="T286" id="Seg_3125" s="T285">Schneesturm-DAT/LOC</ta>
            <ta e="T287" id="Seg_3126" s="T286">NEG</ta>
            <ta e="T288" id="Seg_3127" s="T287">nehmen-CAUS-NEG.[3SG]</ta>
            <ta e="T289" id="Seg_3128" s="T288">jenes.EMPH</ta>
            <ta e="T290" id="Seg_3129" s="T289">Zeit-ABL</ta>
            <ta e="T291" id="Seg_3130" s="T290">Salz.[NOM]</ta>
            <ta e="T292" id="Seg_3131" s="T291">Tundra-DAT/LOC</ta>
            <ta e="T293" id="Seg_3132" s="T292">Schneeeule.[NOM]</ta>
            <ta e="T294" id="Seg_3133" s="T293">hier</ta>
            <ta e="T295" id="Seg_3134" s="T294">wohin</ta>
            <ta e="T296" id="Seg_3135" s="T295">NEG</ta>
            <ta e="T297" id="Seg_3136" s="T296">fliegen-NEG.CVB.SIM</ta>
            <ta e="T298" id="Seg_3137" s="T297">Winter-EP-VBZ-CVB.SIM</ta>
            <ta e="T299" id="Seg_3138" s="T298">sitzen-PRS.[3SG]</ta>
            <ta e="T300" id="Seg_3139" s="T299">Schneeeule.[NOM]</ta>
            <ta e="T301" id="Seg_3140" s="T300">früher-COMP</ta>
            <ta e="T302" id="Seg_3141" s="T301">EMPH</ta>
            <ta e="T303" id="Seg_3142" s="T302">groß-ADVZ</ta>
            <ta e="T304" id="Seg_3143" s="T303">klein</ta>
            <ta e="T305" id="Seg_3144" s="T304">Vögelchen-PL-ACC</ta>
            <ta e="T306" id="Seg_3145" s="T305">Rebhuhn-PL-ACC</ta>
            <ta e="T307" id="Seg_3146" s="T306">essen-PTCP.PRS</ta>
            <ta e="T308" id="Seg_3147" s="T307">werden-PST2-3SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_3148" s="T0">полярная.сова.[NOM]</ta>
            <ta e="T2" id="Seg_3149" s="T1">тот.[NOM]</ta>
            <ta e="T3" id="Seg_3150" s="T2">имя-3SG.[NOM]</ta>
            <ta e="T4" id="Seg_3151" s="T3">долганский-долганский</ta>
            <ta e="T5" id="Seg_3152" s="T4">сказка-3SG.[NOM]</ta>
            <ta e="T6" id="Seg_3153" s="T5">давно-давно</ta>
            <ta e="T7" id="Seg_3154" s="T6">тундра.[NOM]</ta>
            <ta e="T8" id="Seg_3155" s="T7">теплый.[NOM]</ta>
            <ta e="T9" id="Seg_3156" s="T8">быть-PST1-3SG</ta>
            <ta e="T10" id="Seg_3157" s="T9">каждый</ta>
            <ta e="T11" id="Seg_3158" s="T10">птица-PL.[NOM]</ta>
            <ta e="T12" id="Seg_3159" s="T11">птичка-PL.[NOM]</ta>
            <ta e="T13" id="Seg_3160" s="T12">гусь-PL.[NOM]</ta>
            <ta e="T14" id="Seg_3161" s="T13">утка-PL.[NOM]</ta>
            <ta e="T15" id="Seg_3162" s="T14">мелкая.птица-PL.[NOM]</ta>
            <ta e="T16" id="Seg_3163" s="T15">этот</ta>
            <ta e="T17" id="Seg_3164" s="T16">земля-DAT/LOC</ta>
            <ta e="T18" id="Seg_3165" s="T17">жить-PST2-3PL</ta>
            <ta e="T19" id="Seg_3166" s="T18">тот-PL-ACC</ta>
            <ta e="T20" id="Seg_3167" s="T19">с</ta>
            <ta e="T21" id="Seg_3168" s="T20">вместе</ta>
            <ta e="T22" id="Seg_3169" s="T21">полярная.сова.[NOM]</ta>
            <ta e="T23" id="Seg_3170" s="T22">жить-PST2-3SG</ta>
            <ta e="T24" id="Seg_3171" s="T23">3SG.[NOM]</ta>
            <ta e="T25" id="Seg_3172" s="T24">есть-PTCP.PRS</ta>
            <ta e="T26" id="Seg_3173" s="T25">пища-PL-3SG.[NOM]</ta>
            <ta e="T27" id="Seg_3174" s="T26">гусь-PL.[NOM]</ta>
            <ta e="T28" id="Seg_3175" s="T27">утка-PL.[NOM]</ta>
            <ta e="T29" id="Seg_3176" s="T28">куропатка-PL.[NOM]</ta>
            <ta e="T30" id="Seg_3177" s="T29">каждый</ta>
            <ta e="T31" id="Seg_3178" s="T30">птица-PL.[NOM]</ta>
            <ta e="T32" id="Seg_3179" s="T31">птичка-PL.[NOM]</ta>
            <ta e="T33" id="Seg_3180" s="T32">большой-3PL.[NOM]</ta>
            <ta e="T34" id="Seg_3181" s="T33">гусь.[NOM]</ta>
            <ta e="T35" id="Seg_3182" s="T34">потом</ta>
            <ta e="T36" id="Seg_3183" s="T35">холод-PL.[NOM]</ta>
            <ta e="T37" id="Seg_3184" s="T36">быть-PST2-3PL</ta>
            <ta e="T38" id="Seg_3185" s="T37">руководитель.[NOM]</ta>
            <ta e="T39" id="Seg_3186" s="T38">гусь.[NOM]</ta>
            <ta e="T40" id="Seg_3187" s="T39">каждый</ta>
            <ta e="T41" id="Seg_3188" s="T40">птица-PL-ACC</ta>
            <ta e="T42" id="Seg_3189" s="T41">птичка-PL-ACC</ta>
            <ta e="T43" id="Seg_3190" s="T42">собирать-PST2-3SG</ta>
            <ta e="T44" id="Seg_3191" s="T43">большой</ta>
            <ta e="T45" id="Seg_3192" s="T44">советовать-EP-RECP/COLL-NMNZ.[NOM]</ta>
            <ta e="T46" id="Seg_3193" s="T45">быть-PST2-3SG</ta>
            <ta e="T47" id="Seg_3194" s="T46">птица-PL.[NOM]</ta>
            <ta e="T48" id="Seg_3195" s="T47">птичка-PL.[NOM]</ta>
            <ta e="T49" id="Seg_3196" s="T48">собирать-PASS/REFL-CVB.SEQ</ta>
            <ta e="T50" id="Seg_3197" s="T49">широкий</ta>
            <ta e="T51" id="Seg_3198" s="T50">поляна-DAT/LOC</ta>
            <ta e="T52" id="Seg_3199" s="T51">там</ta>
            <ta e="T53" id="Seg_3200" s="T52">сопка.[NOM]</ta>
            <ta e="T54" id="Seg_3201" s="T53">нижняя.часть-3SG-DAT/LOC</ta>
            <ta e="T55" id="Seg_3202" s="T54">наполняться-CVB.SEQ</ta>
            <ta e="T56" id="Seg_3203" s="T55">сесть-PST2-3PL</ta>
            <ta e="T57" id="Seg_3204" s="T56">полярная.сова.[NOM]</ta>
            <ta e="T58" id="Seg_3205" s="T57">тоже</ta>
            <ta e="T59" id="Seg_3206" s="T58">сход-DAT/LOC</ta>
            <ta e="T60" id="Seg_3207" s="T59">приходить-PST2-3SG</ta>
            <ta e="T61" id="Seg_3208" s="T60">руководитель.[NOM]</ta>
            <ta e="T62" id="Seg_3209" s="T61">гусь.[NOM]</ta>
            <ta e="T63" id="Seg_3210" s="T62">новость-PL-ACC</ta>
            <ta e="T64" id="Seg_3211" s="T63">сам-3SG-GEN</ta>
            <ta e="T65" id="Seg_3212" s="T64">люди-3SG-DAT/LOC</ta>
            <ta e="T66" id="Seg_3213" s="T65">говорить-PST2-3SG</ta>
            <ta e="T67" id="Seg_3214" s="T66">видеть-PRS-2PL</ta>
            <ta e="T68" id="Seg_3215" s="T67">Q</ta>
            <ta e="T69" id="Seg_3216" s="T68">погода-1PL.[NOM]</ta>
            <ta e="T70" id="Seg_3217" s="T69">похолодать-PST1-3SG</ta>
            <ta e="T71" id="Seg_3218" s="T70">здесь</ta>
            <ta e="T72" id="Seg_3219" s="T71">1PL.[NOM]</ta>
            <ta e="T73" id="Seg_3220" s="T72">летом</ta>
            <ta e="T74" id="Seg_3221" s="T73">только</ta>
            <ta e="T75" id="Seg_3222" s="T74">жить-PTCP.PRS</ta>
            <ta e="T76" id="Seg_3223" s="T75">быть-FUT-1PL</ta>
            <ta e="T77" id="Seg_3224" s="T76">1SG.[NOM]</ta>
            <ta e="T78" id="Seg_3225" s="T77">место-ACC</ta>
            <ta e="T79" id="Seg_3226" s="T78">знать-PRS-1SG</ta>
            <ta e="T80" id="Seg_3227" s="T79">где</ta>
            <ta e="T81" id="Seg_3228" s="T80">зима.[NOM]</ta>
            <ta e="T82" id="Seg_3229" s="T81">быть-PTCP.HAB-3SG</ta>
            <ta e="T83" id="Seg_3230" s="T82">NEG.[3SG]</ta>
            <ta e="T84" id="Seg_3231" s="T83">тот.[NOM]</ta>
            <ta e="T85" id="Seg_3232" s="T84">очень</ta>
            <ta e="T86" id="Seg_3233" s="T85">далекий.[NOM]</ta>
            <ta e="T87" id="Seg_3234" s="T86">земля-3SG-INSTR</ta>
            <ta e="T88" id="Seg_3235" s="T87">идти-PTCP.COND-DAT/LOC</ta>
            <ta e="T89" id="Seg_3236" s="T88">доезжать-EP-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T90" id="Seg_3237" s="T89">1PL.[NOM]</ta>
            <ta e="T93" id="Seg_3238" s="T92">крыло-PROPR-PL.[NOM]</ta>
            <ta e="T94" id="Seg_3239" s="T93">этот</ta>
            <ta e="T95" id="Seg_3240" s="T94">место-DAT/LOC</ta>
            <ta e="T96" id="Seg_3241" s="T95">почему</ta>
            <ta e="T97" id="Seg_3242" s="T96">мерзнуть-CVB.SIM</ta>
            <ta e="T98" id="Seg_3243" s="T97">лежать-FUT-1PL=Q</ta>
            <ta e="T99" id="Seg_3244" s="T98">вот</ta>
            <ta e="T100" id="Seg_3245" s="T99">тот</ta>
            <ta e="T101" id="Seg_3246" s="T100">место-DAT/LOC</ta>
            <ta e="T102" id="Seg_3247" s="T101">прямой-ADVZ</ta>
            <ta e="T103" id="Seg_3248" s="T102">летать-IMP.1PL</ta>
            <ta e="T104" id="Seg_3249" s="T103">этот</ta>
            <ta e="T105" id="Seg_3250" s="T104">холод-PL.[NOM]</ta>
            <ta e="T106" id="Seg_3251" s="T105">проехать-PTCP.FUT-3PL-DAT/LOC</ta>
            <ta e="T107" id="Seg_3252" s="T106">пока</ta>
            <ta e="T108" id="Seg_3253" s="T107">там</ta>
            <ta e="T109" id="Seg_3254" s="T108">жить-FUT-1PL</ta>
            <ta e="T110" id="Seg_3255" s="T109">теперь</ta>
            <ta e="T111" id="Seg_3256" s="T110">здесь</ta>
            <ta e="T112" id="Seg_3257" s="T111">этот</ta>
            <ta e="T113" id="Seg_3258" s="T112">оставаться-TEMP-1PL</ta>
            <ta e="T114" id="Seg_3259" s="T113">каждый-1PL.[NOM]</ta>
            <ta e="T115" id="Seg_3260" s="T114">крепко</ta>
            <ta e="T116" id="Seg_3261" s="T115">мерзнуть-CVB.SEQ</ta>
            <ta e="T117" id="Seg_3262" s="T116">умирать-FUT-1PL</ta>
            <ta e="T118" id="Seg_3263" s="T117">тот</ta>
            <ta e="T119" id="Seg_3264" s="T118">место-DAT/LOC</ta>
            <ta e="T120" id="Seg_3265" s="T119">летать-CVB.SEQ</ta>
            <ta e="T121" id="Seg_3266" s="T120">идти-TEMP-1PL</ta>
            <ta e="T122" id="Seg_3267" s="T121">человек.[NOM]</ta>
            <ta e="T123" id="Seg_3268" s="T122">быть-FUT-1PL</ta>
            <ta e="T124" id="Seg_3269" s="T123">правда.[NOM]</ta>
            <ta e="T125" id="Seg_3270" s="T124">говорить-PRS-1SG</ta>
            <ta e="T126" id="Seg_3271" s="T125">Q</ta>
            <ta e="T127" id="Seg_3272" s="T126">крыло-PROPR-PL.[NOM]</ta>
            <ta e="T128" id="Seg_3273" s="T127">кто.[NOM]</ta>
            <ta e="T129" id="Seg_3274" s="T128">путешествовать-PTCP.PRS-ACC</ta>
            <ta e="T130" id="Seg_3275" s="T129">согласить-PRS.[3SG]</ta>
            <ta e="T131" id="Seg_3276" s="T130">правда.[NOM]</ta>
            <ta e="T132" id="Seg_3277" s="T131">здесь</ta>
            <ta e="T133" id="Seg_3278" s="T132">оставаться-TEMP-1PL</ta>
            <ta e="T134" id="Seg_3279" s="T133">умирать-FUT-1PL</ta>
            <ta e="T135" id="Seg_3280" s="T134">1PL.[NOM]</ta>
            <ta e="T136" id="Seg_3281" s="T135">каждый-1PL.[NOM]</ta>
            <ta e="T137" id="Seg_3282" s="T136">туда</ta>
            <ta e="T138" id="Seg_3283" s="T137">летать-FUT-1PL</ta>
            <ta e="T139" id="Seg_3284" s="T138">говорить-CVB.SEQ</ta>
            <ta e="T140" id="Seg_3285" s="T139">каждый</ta>
            <ta e="T141" id="Seg_3286" s="T140">птица-PL.[NOM]</ta>
            <ta e="T142" id="Seg_3287" s="T141">птичка-PL.[NOM]</ta>
            <ta e="T143" id="Seg_3288" s="T142">кричать-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T144" id="Seg_3289" s="T143">1SG.[NOM]</ta>
            <ta e="T145" id="Seg_3290" s="T144">ведь</ta>
            <ta e="T146" id="Seg_3291" s="T145">тот.[NOM]</ta>
            <ta e="T147" id="Seg_3292" s="T146">к</ta>
            <ta e="T148" id="Seg_3293" s="T147">летать-FUT-1SG</ta>
            <ta e="T149" id="Seg_3294" s="T148">полярная.сова.[NOM]</ta>
            <ta e="T150" id="Seg_3295" s="T149">говорить-PST2-3SG</ta>
            <ta e="T151" id="Seg_3296" s="T150">руководитель.[NOM]</ta>
            <ta e="T152" id="Seg_3297" s="T151">гусь.[NOM]</ta>
            <ta e="T153" id="Seg_3298" s="T152">друг-PL-3SG-ABL</ta>
            <ta e="T154" id="Seg_3299" s="T153">спрашивать-PST2-3SG</ta>
            <ta e="T155" id="Seg_3300" s="T154">этот</ta>
            <ta e="T156" id="Seg_3301" s="T155">полярная.сова.[NOM]</ta>
            <ta e="T157" id="Seg_3302" s="T156">1PL-ACC</ta>
            <ta e="T158" id="Seg_3303" s="T157">с</ta>
            <ta e="T159" id="Seg_3304" s="T158">сопровождать-CVB.PURP</ta>
            <ta e="T160" id="Seg_3305" s="T159">хотеть-PRS.[3SG]</ta>
            <ta e="T161" id="Seg_3306" s="T160">тот.[NOM]</ta>
            <ta e="T162" id="Seg_3307" s="T161">сторона-3SG-INSTR</ta>
            <ta e="T163" id="Seg_3308" s="T162">2PL.[NOM]</ta>
            <ta e="T164" id="Seg_3309" s="T163">как</ta>
            <ta e="T165" id="Seg_3310" s="T164">думать-CVB.SIM</ta>
            <ta e="T166" id="Seg_3311" s="T165">думать-PRS-2PL</ta>
            <ta e="T167" id="Seg_3312" s="T166">1PL.[NOM]</ta>
            <ta e="T168" id="Seg_3313" s="T167">полярная.сова-ACC</ta>
            <ta e="T169" id="Seg_3314" s="T168">сам-1PL-ACC</ta>
            <ta e="T170" id="Seg_3315" s="T169">с</ta>
            <ta e="T171" id="Seg_3316" s="T170">взять-FUT-1PL</ta>
            <ta e="T172" id="Seg_3317" s="T171">NEG-3SG</ta>
            <ta e="T173" id="Seg_3318" s="T172">птица-PL.[NOM]</ta>
            <ta e="T174" id="Seg_3319" s="T173">говорить-PST2-3PL</ta>
            <ta e="T175" id="Seg_3320" s="T174">2PL.[NOM]</ta>
            <ta e="T176" id="Seg_3321" s="T175">3SG-ACC</ta>
            <ta e="T177" id="Seg_3322" s="T176">почему</ta>
            <ta e="T178" id="Seg_3323" s="T177">взять-EP-NEG-CVB.PURP</ta>
            <ta e="T179" id="Seg_3324" s="T178">хотеть-PRS-2PL</ta>
            <ta e="T180" id="Seg_3325" s="T179">3SG-DAT/LOC</ta>
            <ta e="T181" id="Seg_3326" s="T180">прямой-ADVZ</ta>
            <ta e="T182" id="Seg_3327" s="T181">говорить-EP-IMP.2PL</ta>
            <ta e="T183" id="Seg_3328" s="T182">птица-PL.[NOM]</ta>
            <ta e="T184" id="Seg_3329" s="T183">полярная.сова.[NOM]</ta>
            <ta e="T185" id="Seg_3330" s="T184">сторона-3SG-INSTR</ta>
            <ta e="T186" id="Seg_3331" s="T185">сквозь</ta>
            <ta e="T187" id="Seg_3332" s="T186">говорить-PST2-3PL</ta>
            <ta e="T188" id="Seg_3333" s="T187">полярная.сова.[NOM]</ta>
            <ta e="T189" id="Seg_3334" s="T188">здесь</ta>
            <ta e="T190" id="Seg_3335" s="T189">1PL.[NOM]</ta>
            <ta e="T191" id="Seg_3336" s="T190">семья-PL-1PL-ACC</ta>
            <ta e="T192" id="Seg_3337" s="T191">есть-CVB.SEQ</ta>
            <ta e="T193" id="Seg_3338" s="T192">сидеть-PRS.[3SG]</ta>
            <ta e="T194" id="Seg_3339" s="T193">3SG.[NOM]</ta>
            <ta e="T195" id="Seg_3340" s="T194">сам-3SG-GEN</ta>
            <ta e="T196" id="Seg_3341" s="T195">обычай-3SG-ACC</ta>
            <ta e="T197" id="Seg_3342" s="T196">там</ta>
            <ta e="T198" id="Seg_3343" s="T197">NEG</ta>
            <ta e="T199" id="Seg_3344" s="T198">бросать-FUT.[3SG]</ta>
            <ta e="T200" id="Seg_3345" s="T199">NEG-3SG</ta>
            <ta e="T201" id="Seg_3346" s="T200">1PL.[NOM]</ta>
            <ta e="T202" id="Seg_3347" s="T201">3SG-ACC</ta>
            <ta e="T203" id="Seg_3348" s="T202">взять-FUT-1PL</ta>
            <ta e="T204" id="Seg_3349" s="T203">NEG-3SG</ta>
            <ta e="T205" id="Seg_3350" s="T204">руководитель.[NOM]</ta>
            <ta e="T206" id="Seg_3351" s="T205">гусь.[NOM]</ta>
            <ta e="T207" id="Seg_3352" s="T206">полярная.сова-DAT/LOC</ta>
            <ta e="T208" id="Seg_3353" s="T207">говорить-PRS.[3SG]</ta>
            <ta e="T209" id="Seg_3354" s="T208">видеть-PRS-2SG</ta>
            <ta e="T210" id="Seg_3355" s="T209">Q</ta>
            <ta e="T211" id="Seg_3356" s="T210">крыло-PROPR</ta>
            <ta e="T212" id="Seg_3357" s="T211">народ.[NOM]</ta>
            <ta e="T213" id="Seg_3358" s="T212">советовать-PTCP.PST-3PL-ACC</ta>
            <ta e="T214" id="Seg_3359" s="T213">каждый-3PL.[NOM]</ta>
            <ta e="T215" id="Seg_3360" s="T214">говорить-PST2-3PL</ta>
            <ta e="T216" id="Seg_3361" s="T215">тот.[NOM]</ta>
            <ta e="T217" id="Seg_3362" s="T216">закон.[NOM]</ta>
            <ta e="T218" id="Seg_3363" s="T217">2SG.[NOM]</ta>
            <ta e="T219" id="Seg_3364" s="T218">здесь</ta>
            <ta e="T220" id="Seg_3365" s="T219">оставаться-FUT-2SG</ta>
            <ta e="T221" id="Seg_3366" s="T220">попросить-EP-MED-CVB.SEQ</ta>
            <ta e="T222" id="Seg_3367" s="T221">кончать-PRS-1SG</ta>
            <ta e="T223" id="Seg_3368" s="T222">полярная.сова.[NOM]</ta>
            <ta e="T224" id="Seg_3369" s="T223">говорить-PST2-3SG</ta>
            <ta e="T225" id="Seg_3370" s="T224">здесь</ta>
            <ta e="T226" id="Seg_3371" s="T225">оставаться-FUT-1SG</ta>
            <ta e="T227" id="Seg_3372" s="T226">можно</ta>
            <ta e="T228" id="Seg_3373" s="T227">1SG.[NOM]</ta>
            <ta e="T229" id="Seg_3374" s="T228">здесь</ta>
            <ta e="T230" id="Seg_3375" s="T229">живой.[NOM]</ta>
            <ta e="T231" id="Seg_3376" s="T230">жить-FUT-1SG</ta>
            <ta e="T232" id="Seg_3377" s="T231">каждый</ta>
            <ta e="T233" id="Seg_3378" s="T232">птица-PL.[NOM]</ta>
            <ta e="T234" id="Seg_3379" s="T233">птичка-PL.[NOM]</ta>
            <ta e="T235" id="Seg_3380" s="T234">теплый</ta>
            <ta e="T236" id="Seg_3381" s="T235">место-ACC</ta>
            <ta e="T237" id="Seg_3382" s="T236">искать-CVB.SIM</ta>
            <ta e="T238" id="Seg_3383" s="T237">идти-PST2-3PL</ta>
            <ta e="T239" id="Seg_3384" s="T238">полярная.сова.[NOM]</ta>
            <ta e="T240" id="Seg_3385" s="T239">дом-3SG-DAT/LOC</ta>
            <ta e="T241" id="Seg_3386" s="T240">приходить-CVB.SEQ</ta>
            <ta e="T242" id="Seg_3387" s="T241">старуха-3SG-DAT/LOC</ta>
            <ta e="T243" id="Seg_3388" s="T242">говорить-PRS.[3SG]</ta>
            <ta e="T244" id="Seg_3389" s="T243">знать-PRS-2SG</ta>
            <ta e="T245" id="Seg_3390" s="T244">Q</ta>
            <ta e="T246" id="Seg_3391" s="T245">1PL-ACC</ta>
            <ta e="T247" id="Seg_3392" s="T246">взять-NEG.PTCP-3PL-ACC</ta>
            <ta e="T248" id="Seg_3393" s="T247">1SG-DAT/LOC</ta>
            <ta e="T249" id="Seg_3394" s="T248">теплый</ta>
            <ta e="T250" id="Seg_3395" s="T249">сокуй-ACC</ta>
            <ta e="T251" id="Seg_3396" s="T250">шить.[IMP.2SG]</ta>
            <ta e="T252" id="Seg_3397" s="T251">пурга-DAT/LOC</ta>
            <ta e="T253" id="Seg_3398" s="T252">NEG</ta>
            <ta e="T254" id="Seg_3399" s="T253">холод-DAT/LOC</ta>
            <ta e="T255" id="Seg_3400" s="T254">да</ta>
            <ta e="T256" id="Seg_3401" s="T255">мерзнуть-NEG.PTCP</ta>
            <ta e="T257" id="Seg_3402" s="T256">быть-PTCP.FUT-1SG-ACC</ta>
            <ta e="T258" id="Seg_3403" s="T257">шить-FUT-1SG</ta>
            <ta e="T259" id="Seg_3404" s="T258">слово.[NOM]</ta>
            <ta e="T260" id="Seg_3405" s="T259">уступчивый</ta>
            <ta e="T261" id="Seg_3406" s="T260">старуха-3SG.[NOM]</ta>
            <ta e="T262" id="Seg_3407" s="T261">говорить-PST2-3SG</ta>
            <ta e="T263" id="Seg_3408" s="T262">сокуй.[NOM]</ta>
            <ta e="T264" id="Seg_3409" s="T263">снег.[NOM]</ta>
            <ta e="T265" id="Seg_3410" s="T264">подобно</ta>
            <ta e="T266" id="Seg_3411" s="T265">белый.[NOM]</ta>
            <ta e="T267" id="Seg_3412" s="T266">быть-IMP.3SG</ta>
            <ta e="T268" id="Seg_3413" s="T267">полярная.сова.[NOM]</ta>
            <ta e="T269" id="Seg_3414" s="T268">старуха-3SG.[NOM]</ta>
            <ta e="T270" id="Seg_3415" s="T269">EMPH-белый</ta>
            <ta e="T271" id="Seg_3416" s="T270">сокуй-ACC</ta>
            <ta e="T272" id="Seg_3417" s="T271">шить-PST2-3SG</ta>
            <ta e="T273" id="Seg_3418" s="T272">полярная.сова.[NOM]</ta>
            <ta e="T274" id="Seg_3419" s="T273">сокуй-3SG-ACC</ta>
            <ta e="T275" id="Seg_3420" s="T274">одеть-CVB.ANT-3SG</ta>
            <ta e="T276" id="Seg_3421" s="T275">тот-3SG-3SG.[NOM]</ta>
            <ta e="T277" id="Seg_3422" s="T276">тело-3SG-DAT/LOC</ta>
            <ta e="T278" id="Seg_3423" s="T277">прилипать-CVB.SEQ</ta>
            <ta e="T279" id="Seg_3424" s="T278">оставаться-PST2-3SG</ta>
            <ta e="T280" id="Seg_3425" s="T279">полярная.сова.[NOM]</ta>
            <ta e="T281" id="Seg_3426" s="T280">морозный</ta>
            <ta e="T282" id="Seg_3427" s="T281">холод-DAT/LOC</ta>
            <ta e="T283" id="Seg_3428" s="T282">NEG</ta>
            <ta e="T284" id="Seg_3429" s="T283">мерзнуть-NEG.[3SG]</ta>
            <ta e="T285" id="Seg_3430" s="T284">сильный</ta>
            <ta e="T286" id="Seg_3431" s="T285">пурга-DAT/LOC</ta>
            <ta e="T287" id="Seg_3432" s="T286">NEG</ta>
            <ta e="T288" id="Seg_3433" s="T287">взять-CAUS-NEG.[3SG]</ta>
            <ta e="T289" id="Seg_3434" s="T288">тот.EMPH</ta>
            <ta e="T290" id="Seg_3435" s="T289">час-ABL</ta>
            <ta e="T291" id="Seg_3436" s="T290">соль.[NOM]</ta>
            <ta e="T292" id="Seg_3437" s="T291">тундра-DAT/LOC</ta>
            <ta e="T293" id="Seg_3438" s="T292">полярная.сова.[NOM]</ta>
            <ta e="T294" id="Seg_3439" s="T293">здесь</ta>
            <ta e="T295" id="Seg_3440" s="T294">куда</ta>
            <ta e="T296" id="Seg_3441" s="T295">NEG</ta>
            <ta e="T297" id="Seg_3442" s="T296">летать-NEG.CVB.SIM</ta>
            <ta e="T298" id="Seg_3443" s="T297">зима-EP-VBZ-CVB.SIM</ta>
            <ta e="T299" id="Seg_3444" s="T298">сидеть-PRS.[3SG]</ta>
            <ta e="T300" id="Seg_3445" s="T299">полярная.сова.[NOM]</ta>
            <ta e="T301" id="Seg_3446" s="T300">прежний-COMP</ta>
            <ta e="T302" id="Seg_3447" s="T301">EMPH</ta>
            <ta e="T303" id="Seg_3448" s="T302">большой-ADVZ</ta>
            <ta e="T304" id="Seg_3449" s="T303">маленький</ta>
            <ta e="T305" id="Seg_3450" s="T304">птичка-PL-ACC</ta>
            <ta e="T306" id="Seg_3451" s="T305">куропатка-PL-ACC</ta>
            <ta e="T307" id="Seg_3452" s="T306">есть-PTCP.PRS</ta>
            <ta e="T308" id="Seg_3453" s="T307">становиться-PST2-3SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_3454" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_3455" s="T1">dempro-pro:case</ta>
            <ta e="T3" id="Seg_3456" s="T2">n-n:(poss)-n:case</ta>
            <ta e="T4" id="Seg_3457" s="T3">adj-adj</ta>
            <ta e="T5" id="Seg_3458" s="T4">n-n:(poss)-n:case</ta>
            <ta e="T6" id="Seg_3459" s="T5">adv-adv</ta>
            <ta e="T7" id="Seg_3460" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_3461" s="T7">adj-n:case</ta>
            <ta e="T9" id="Seg_3462" s="T8">v-v:tense-v:poss.pn</ta>
            <ta e="T10" id="Seg_3463" s="T9">adj</ta>
            <ta e="T11" id="Seg_3464" s="T10">n-n:(num)-n:case</ta>
            <ta e="T12" id="Seg_3465" s="T11">n-n:(num)-n:case</ta>
            <ta e="T13" id="Seg_3466" s="T12">n-n:(num)-n:case</ta>
            <ta e="T14" id="Seg_3467" s="T13">n-n:(num)-n:case</ta>
            <ta e="T15" id="Seg_3468" s="T14">n-n:(num)-n:case</ta>
            <ta e="T16" id="Seg_3469" s="T15">dempro</ta>
            <ta e="T17" id="Seg_3470" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_3471" s="T17">v-v:tense-v:poss.pn</ta>
            <ta e="T19" id="Seg_3472" s="T18">dempro-pro:(num)-pro:case</ta>
            <ta e="T20" id="Seg_3473" s="T19">post</ta>
            <ta e="T21" id="Seg_3474" s="T20">adv</ta>
            <ta e="T22" id="Seg_3475" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_3476" s="T22">v-v:tense-v:poss.pn</ta>
            <ta e="T24" id="Seg_3477" s="T23">pers-pro:case</ta>
            <ta e="T25" id="Seg_3478" s="T24">v-v:ptcp</ta>
            <ta e="T26" id="Seg_3479" s="T25">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T27" id="Seg_3480" s="T26">n-n:(num)-n:case</ta>
            <ta e="T28" id="Seg_3481" s="T27">n-n:(num)-n:case</ta>
            <ta e="T29" id="Seg_3482" s="T28">n-n:(num)-n:case</ta>
            <ta e="T30" id="Seg_3483" s="T29">adj</ta>
            <ta e="T31" id="Seg_3484" s="T30">n-n:(num)-n:case</ta>
            <ta e="T32" id="Seg_3485" s="T31">n-n:(num)-n:case</ta>
            <ta e="T33" id="Seg_3486" s="T32">adj-n:(poss)-n:case</ta>
            <ta e="T34" id="Seg_3487" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_3488" s="T34">adv</ta>
            <ta e="T36" id="Seg_3489" s="T35">n-n:(num)-n:case</ta>
            <ta e="T37" id="Seg_3490" s="T36">v-v:tense-v:poss.pn</ta>
            <ta e="T38" id="Seg_3491" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_3492" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_3493" s="T39">adj</ta>
            <ta e="T41" id="Seg_3494" s="T40">n-n:(num)-n:case</ta>
            <ta e="T42" id="Seg_3495" s="T41">n-n:(num)-n:case</ta>
            <ta e="T43" id="Seg_3496" s="T42">v-v:tense-v:poss.pn</ta>
            <ta e="T44" id="Seg_3497" s="T43">adj</ta>
            <ta e="T45" id="Seg_3498" s="T44">v-v:(ins)-v&gt;v-v&gt;n-n:case</ta>
            <ta e="T46" id="Seg_3499" s="T45">v-v:tense-v:poss.pn</ta>
            <ta e="T47" id="Seg_3500" s="T46">n-n:(num)-n:case</ta>
            <ta e="T48" id="Seg_3501" s="T47">n-n:(num)-n:case</ta>
            <ta e="T49" id="Seg_3502" s="T48">v-v&gt;v-v:cvb</ta>
            <ta e="T50" id="Seg_3503" s="T49">adj</ta>
            <ta e="T51" id="Seg_3504" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_3505" s="T51">adv</ta>
            <ta e="T53" id="Seg_3506" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_3507" s="T53">n-n:poss-n:case</ta>
            <ta e="T55" id="Seg_3508" s="T54">v-v:cvb</ta>
            <ta e="T56" id="Seg_3509" s="T55">v-v:tense-v:poss.pn</ta>
            <ta e="T57" id="Seg_3510" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_3511" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_3512" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_3513" s="T59">v-v:tense-v:poss.pn</ta>
            <ta e="T61" id="Seg_3514" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_3515" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_3516" s="T62">n-n:(num)-n:case</ta>
            <ta e="T64" id="Seg_3517" s="T63">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T65" id="Seg_3518" s="T64">n-n:poss-n:case</ta>
            <ta e="T66" id="Seg_3519" s="T65">v-v:tense-v:poss.pn</ta>
            <ta e="T67" id="Seg_3520" s="T66">v-v:tense-v:pred.pn</ta>
            <ta e="T68" id="Seg_3521" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_3522" s="T68">n-n:(poss)-n:case</ta>
            <ta e="T70" id="Seg_3523" s="T69">v-v:tense-v:poss.pn</ta>
            <ta e="T71" id="Seg_3524" s="T70">adv</ta>
            <ta e="T72" id="Seg_3525" s="T71">pers-pro:case</ta>
            <ta e="T73" id="Seg_3526" s="T72">adv</ta>
            <ta e="T74" id="Seg_3527" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_3528" s="T74">v-v:ptcp</ta>
            <ta e="T76" id="Seg_3529" s="T75">v-v:tense-v:poss.pn</ta>
            <ta e="T77" id="Seg_3530" s="T76">pers-pro:case</ta>
            <ta e="T78" id="Seg_3531" s="T77">n-n:case</ta>
            <ta e="T79" id="Seg_3532" s="T78">v-v:tense-v:pred.pn</ta>
            <ta e="T80" id="Seg_3533" s="T79">que</ta>
            <ta e="T81" id="Seg_3534" s="T80">n-n:case</ta>
            <ta e="T82" id="Seg_3535" s="T81">v-v:ptcp-v:(poss)</ta>
            <ta e="T83" id="Seg_3536" s="T82">ptcl-ptcl:pred.pn</ta>
            <ta e="T84" id="Seg_3537" s="T83">dempro-pro:case</ta>
            <ta e="T85" id="Seg_3538" s="T84">adv</ta>
            <ta e="T86" id="Seg_3539" s="T85">adj-n:case</ta>
            <ta e="T87" id="Seg_3540" s="T86">n-n:poss-n:case</ta>
            <ta e="T88" id="Seg_3541" s="T87">v-v:ptcp-v:(case)</ta>
            <ta e="T89" id="Seg_3542" s="T88">v-v:(ins)-v&gt;v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T90" id="Seg_3543" s="T89">pers-pro:case</ta>
            <ta e="T93" id="Seg_3544" s="T92">n-n&gt;adj-n:(num)-n:case</ta>
            <ta e="T94" id="Seg_3545" s="T93">dempro</ta>
            <ta e="T95" id="Seg_3546" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_3547" s="T95">que</ta>
            <ta e="T97" id="Seg_3548" s="T96">v-v:cvb</ta>
            <ta e="T98" id="Seg_3549" s="T97">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T99" id="Seg_3550" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_3551" s="T99">dempro</ta>
            <ta e="T101" id="Seg_3552" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_3553" s="T101">adj-adj&gt;adv</ta>
            <ta e="T103" id="Seg_3554" s="T102">v-v:mood.pn</ta>
            <ta e="T104" id="Seg_3555" s="T103">dempro</ta>
            <ta e="T105" id="Seg_3556" s="T104">n-n:(num)-n:case</ta>
            <ta e="T106" id="Seg_3557" s="T105">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T107" id="Seg_3558" s="T106">post</ta>
            <ta e="T108" id="Seg_3559" s="T107">adv</ta>
            <ta e="T109" id="Seg_3560" s="T108">v-v:tense-v:poss.pn</ta>
            <ta e="T110" id="Seg_3561" s="T109">adv</ta>
            <ta e="T111" id="Seg_3562" s="T110">adv</ta>
            <ta e="T112" id="Seg_3563" s="T111">dempro</ta>
            <ta e="T113" id="Seg_3564" s="T112">v-v:mood-v:temp.pn</ta>
            <ta e="T114" id="Seg_3565" s="T113">adj-n:(poss)-n:case</ta>
            <ta e="T115" id="Seg_3566" s="T114">adv</ta>
            <ta e="T116" id="Seg_3567" s="T115">v-v:cvb</ta>
            <ta e="T117" id="Seg_3568" s="T116">v-v:tense-v:poss.pn</ta>
            <ta e="T118" id="Seg_3569" s="T117">dempro</ta>
            <ta e="T119" id="Seg_3570" s="T118">n-n:case</ta>
            <ta e="T120" id="Seg_3571" s="T119">v-v:cvb</ta>
            <ta e="T121" id="Seg_3572" s="T120">v-v:mood-v:temp.pn</ta>
            <ta e="T122" id="Seg_3573" s="T121">n-n:case</ta>
            <ta e="T123" id="Seg_3574" s="T122">v-v:tense-v:poss.pn</ta>
            <ta e="T124" id="Seg_3575" s="T123">n-n:case</ta>
            <ta e="T125" id="Seg_3576" s="T124">v-v:tense-v:pred.pn</ta>
            <ta e="T126" id="Seg_3577" s="T125">ptcl</ta>
            <ta e="T127" id="Seg_3578" s="T126">n-n&gt;adj-n:(num)-n:case</ta>
            <ta e="T128" id="Seg_3579" s="T127">que-pro:case</ta>
            <ta e="T129" id="Seg_3580" s="T128">v-v:ptcp-v:(case)</ta>
            <ta e="T130" id="Seg_3581" s="T129">v-v:tense-v:pred.pn</ta>
            <ta e="T131" id="Seg_3582" s="T130">n-n:case</ta>
            <ta e="T132" id="Seg_3583" s="T131">adv</ta>
            <ta e="T133" id="Seg_3584" s="T132">v-v:mood-v:temp.pn</ta>
            <ta e="T134" id="Seg_3585" s="T133">v-v:tense-v:poss.pn</ta>
            <ta e="T135" id="Seg_3586" s="T134">pers-pro:case</ta>
            <ta e="T136" id="Seg_3587" s="T135">adj-n:(poss)-n:case</ta>
            <ta e="T137" id="Seg_3588" s="T136">adv</ta>
            <ta e="T138" id="Seg_3589" s="T137">v-v:tense-v:poss.pn</ta>
            <ta e="T139" id="Seg_3590" s="T138">v-v:cvb</ta>
            <ta e="T140" id="Seg_3591" s="T139">adj</ta>
            <ta e="T141" id="Seg_3592" s="T140">n-n:(num)-n:case</ta>
            <ta e="T142" id="Seg_3593" s="T141">n-n:(num)-n:case</ta>
            <ta e="T143" id="Seg_3594" s="T142">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T144" id="Seg_3595" s="T143">pers-pro:case</ta>
            <ta e="T145" id="Seg_3596" s="T144">ptcl</ta>
            <ta e="T146" id="Seg_3597" s="T145">dempro-pro:case</ta>
            <ta e="T147" id="Seg_3598" s="T146">post</ta>
            <ta e="T148" id="Seg_3599" s="T147">v-v:tense-v:poss.pn</ta>
            <ta e="T149" id="Seg_3600" s="T148">n-n:case</ta>
            <ta e="T150" id="Seg_3601" s="T149">v-v:tense-v:poss.pn</ta>
            <ta e="T151" id="Seg_3602" s="T150">n-n:case</ta>
            <ta e="T152" id="Seg_3603" s="T151">n-n:case</ta>
            <ta e="T153" id="Seg_3604" s="T152">n-n:(num)-n:poss-n:case</ta>
            <ta e="T154" id="Seg_3605" s="T153">v-v:tense-v:poss.pn</ta>
            <ta e="T155" id="Seg_3606" s="T154">dempro</ta>
            <ta e="T156" id="Seg_3607" s="T155">n-n:case</ta>
            <ta e="T157" id="Seg_3608" s="T156">pers-pro:case</ta>
            <ta e="T158" id="Seg_3609" s="T157">post</ta>
            <ta e="T159" id="Seg_3610" s="T158">v-v:cvb</ta>
            <ta e="T160" id="Seg_3611" s="T159">v-v:tense-v:pred.pn</ta>
            <ta e="T161" id="Seg_3612" s="T160">dempro-pro:case</ta>
            <ta e="T162" id="Seg_3613" s="T161">n-n:poss-n:case</ta>
            <ta e="T163" id="Seg_3614" s="T162">pers-pro:case</ta>
            <ta e="T164" id="Seg_3615" s="T163">que</ta>
            <ta e="T165" id="Seg_3616" s="T164">v-v:cvb</ta>
            <ta e="T166" id="Seg_3617" s="T165">v-v:tense-v:pred.pn</ta>
            <ta e="T167" id="Seg_3618" s="T166">pers-pro:case</ta>
            <ta e="T168" id="Seg_3619" s="T167">n-n:case</ta>
            <ta e="T169" id="Seg_3620" s="T168">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T170" id="Seg_3621" s="T169">post</ta>
            <ta e="T171" id="Seg_3622" s="T170">v-v:tense-v:poss.pn</ta>
            <ta e="T172" id="Seg_3623" s="T171">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T173" id="Seg_3624" s="T172">n-n:(num)-n:case</ta>
            <ta e="T174" id="Seg_3625" s="T173">v-v:tense-v:poss.pn</ta>
            <ta e="T175" id="Seg_3626" s="T174">pers-pro:case</ta>
            <ta e="T176" id="Seg_3627" s="T175">pers-pro:case</ta>
            <ta e="T177" id="Seg_3628" s="T176">que</ta>
            <ta e="T178" id="Seg_3629" s="T177">v-v:(ins)-v:(neg)-v:cvb</ta>
            <ta e="T179" id="Seg_3630" s="T178">v-v:tense-v:pred.pn</ta>
            <ta e="T180" id="Seg_3631" s="T179">pers-pro:case</ta>
            <ta e="T181" id="Seg_3632" s="T180">adj-adj&gt;adv</ta>
            <ta e="T182" id="Seg_3633" s="T181">v-v:(ins)-v:mood.pn</ta>
            <ta e="T183" id="Seg_3634" s="T182">n-n:(num)-n:case</ta>
            <ta e="T184" id="Seg_3635" s="T183">n-n:case</ta>
            <ta e="T185" id="Seg_3636" s="T184">n-n:poss-n:case</ta>
            <ta e="T186" id="Seg_3637" s="T185">adv</ta>
            <ta e="T187" id="Seg_3638" s="T186">v-v:tense-v:poss.pn</ta>
            <ta e="T188" id="Seg_3639" s="T187">n-n:case</ta>
            <ta e="T189" id="Seg_3640" s="T188">adv</ta>
            <ta e="T190" id="Seg_3641" s="T189">pers-pro:case</ta>
            <ta e="T191" id="Seg_3642" s="T190">n-n:(num)-n:poss-n:case</ta>
            <ta e="T192" id="Seg_3643" s="T191">v-v:cvb</ta>
            <ta e="T193" id="Seg_3644" s="T192">v-v:tense-v:pred.pn</ta>
            <ta e="T194" id="Seg_3645" s="T193">pers-pro:case</ta>
            <ta e="T195" id="Seg_3646" s="T194">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T196" id="Seg_3647" s="T195">n-n:poss-n:case</ta>
            <ta e="T197" id="Seg_3648" s="T196">adv</ta>
            <ta e="T198" id="Seg_3649" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_3650" s="T198">v-v:tense-v:poss.pn</ta>
            <ta e="T200" id="Seg_3651" s="T199">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T201" id="Seg_3652" s="T200">pers-pro:case</ta>
            <ta e="T202" id="Seg_3653" s="T201">pers-pro:case</ta>
            <ta e="T203" id="Seg_3654" s="T202">v-v:tense-v:poss.pn</ta>
            <ta e="T204" id="Seg_3655" s="T203">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T205" id="Seg_3656" s="T204">n-n:case</ta>
            <ta e="T206" id="Seg_3657" s="T205">n-n:case</ta>
            <ta e="T207" id="Seg_3658" s="T206">n-n:case</ta>
            <ta e="T208" id="Seg_3659" s="T207">v-v:tense-v:pred.pn</ta>
            <ta e="T209" id="Seg_3660" s="T208">v-v:tense-v:pred.pn</ta>
            <ta e="T210" id="Seg_3661" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_3662" s="T210">n-n&gt;adj</ta>
            <ta e="T212" id="Seg_3663" s="T211">n-n:case</ta>
            <ta e="T213" id="Seg_3664" s="T212">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T214" id="Seg_3665" s="T213">adj-n:(poss)-n:case</ta>
            <ta e="T215" id="Seg_3666" s="T214">v-v:tense-v:poss.pn</ta>
            <ta e="T216" id="Seg_3667" s="T215">dempro-pro:case</ta>
            <ta e="T217" id="Seg_3668" s="T216">n-n:case</ta>
            <ta e="T218" id="Seg_3669" s="T217">pers-pro:case</ta>
            <ta e="T219" id="Seg_3670" s="T218">adv</ta>
            <ta e="T220" id="Seg_3671" s="T219">v-v:tense-v:poss.pn</ta>
            <ta e="T221" id="Seg_3672" s="T220">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T222" id="Seg_3673" s="T221">v-v:tense-v:pred.pn</ta>
            <ta e="T223" id="Seg_3674" s="T222">n-n:case</ta>
            <ta e="T224" id="Seg_3675" s="T223">v-v:tense-v:poss.pn</ta>
            <ta e="T225" id="Seg_3676" s="T224">adv</ta>
            <ta e="T226" id="Seg_3677" s="T225">v-v:tense-v:poss.pn</ta>
            <ta e="T227" id="Seg_3678" s="T226">adv</ta>
            <ta e="T228" id="Seg_3679" s="T227">pers-pro:case</ta>
            <ta e="T229" id="Seg_3680" s="T228">adv</ta>
            <ta e="T230" id="Seg_3681" s="T229">adj-n:case</ta>
            <ta e="T231" id="Seg_3682" s="T230">v-v:tense-v:poss.pn</ta>
            <ta e="T232" id="Seg_3683" s="T231">adj</ta>
            <ta e="T233" id="Seg_3684" s="T232">n-n:(num)-n:case</ta>
            <ta e="T234" id="Seg_3685" s="T233">n-n:(num)-n:case</ta>
            <ta e="T235" id="Seg_3686" s="T234">adj</ta>
            <ta e="T236" id="Seg_3687" s="T235">n-n:case</ta>
            <ta e="T237" id="Seg_3688" s="T236">v-v:cvb</ta>
            <ta e="T238" id="Seg_3689" s="T237">v-v:tense-v:poss.pn</ta>
            <ta e="T239" id="Seg_3690" s="T238">n-n:case</ta>
            <ta e="T240" id="Seg_3691" s="T239">n-n:poss-n:case</ta>
            <ta e="T241" id="Seg_3692" s="T240">v-v:cvb</ta>
            <ta e="T242" id="Seg_3693" s="T241">n-n:poss-n:case</ta>
            <ta e="T243" id="Seg_3694" s="T242">v-v:tense-v:pred.pn</ta>
            <ta e="T244" id="Seg_3695" s="T243">v-v:tense-v:pred.pn</ta>
            <ta e="T245" id="Seg_3696" s="T244">ptcl</ta>
            <ta e="T246" id="Seg_3697" s="T245">pers-pro:case</ta>
            <ta e="T247" id="Seg_3698" s="T246">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T248" id="Seg_3699" s="T247">pers-pro:case</ta>
            <ta e="T249" id="Seg_3700" s="T248">adj</ta>
            <ta e="T250" id="Seg_3701" s="T249">n-n:case</ta>
            <ta e="T251" id="Seg_3702" s="T250">v-v:mood.pn</ta>
            <ta e="T252" id="Seg_3703" s="T251">n-n:case</ta>
            <ta e="T253" id="Seg_3704" s="T252">ptcl</ta>
            <ta e="T254" id="Seg_3705" s="T253">n-n:case</ta>
            <ta e="T255" id="Seg_3706" s="T254">conj</ta>
            <ta e="T256" id="Seg_3707" s="T255">v-v:ptcp</ta>
            <ta e="T257" id="Seg_3708" s="T256">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T258" id="Seg_3709" s="T257">v-v:tense-v:poss.pn</ta>
            <ta e="T259" id="Seg_3710" s="T258">n-n:case</ta>
            <ta e="T260" id="Seg_3711" s="T259">adj</ta>
            <ta e="T261" id="Seg_3712" s="T260">n-n:(poss)-n:case</ta>
            <ta e="T262" id="Seg_3713" s="T261">v-v:tense-v:poss.pn</ta>
            <ta e="T263" id="Seg_3714" s="T262">n-n:case</ta>
            <ta e="T264" id="Seg_3715" s="T263">n-n:case</ta>
            <ta e="T265" id="Seg_3716" s="T264">post</ta>
            <ta e="T266" id="Seg_3717" s="T265">adj-n:case</ta>
            <ta e="T267" id="Seg_3718" s="T266">v-v:mood.pn</ta>
            <ta e="T268" id="Seg_3719" s="T267">n-n:case</ta>
            <ta e="T269" id="Seg_3720" s="T268">n-n:(poss)-n:case</ta>
            <ta e="T270" id="Seg_3721" s="T269">adj&gt;adj-adj</ta>
            <ta e="T271" id="Seg_3722" s="T270">n-n:case</ta>
            <ta e="T272" id="Seg_3723" s="T271">v-v:tense-v:poss.pn</ta>
            <ta e="T273" id="Seg_3724" s="T272">n-n:case</ta>
            <ta e="T274" id="Seg_3725" s="T273">n-n:poss-n:case</ta>
            <ta e="T275" id="Seg_3726" s="T274">v-v:cvb-v:pred.pn</ta>
            <ta e="T276" id="Seg_3727" s="T275">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T277" id="Seg_3728" s="T276">n-n:poss-n:case</ta>
            <ta e="T278" id="Seg_3729" s="T277">v-v:cvb</ta>
            <ta e="T279" id="Seg_3730" s="T278">v-v:tense-v:poss.pn</ta>
            <ta e="T280" id="Seg_3731" s="T279">n-n:case</ta>
            <ta e="T281" id="Seg_3732" s="T280">adj</ta>
            <ta e="T282" id="Seg_3733" s="T281">n-n:case</ta>
            <ta e="T283" id="Seg_3734" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_3735" s="T283">v-v:(neg)-v:pred.pn</ta>
            <ta e="T285" id="Seg_3736" s="T284">adj</ta>
            <ta e="T286" id="Seg_3737" s="T285">n-n:case</ta>
            <ta e="T287" id="Seg_3738" s="T286">ptcl</ta>
            <ta e="T288" id="Seg_3739" s="T287">v-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T289" id="Seg_3740" s="T288">dempro</ta>
            <ta e="T290" id="Seg_3741" s="T289">n-n:case</ta>
            <ta e="T291" id="Seg_3742" s="T290">n-n:case</ta>
            <ta e="T292" id="Seg_3743" s="T291">n-n:case</ta>
            <ta e="T293" id="Seg_3744" s="T292">n-n:case</ta>
            <ta e="T294" id="Seg_3745" s="T293">adv</ta>
            <ta e="T295" id="Seg_3746" s="T294">que</ta>
            <ta e="T296" id="Seg_3747" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_3748" s="T296">v-v:cvb</ta>
            <ta e="T298" id="Seg_3749" s="T297">n-n:(ins)-n&gt;v-v:cvb</ta>
            <ta e="T299" id="Seg_3750" s="T298">v-v:tense-v:pred.pn</ta>
            <ta e="T300" id="Seg_3751" s="T299">n-n:case</ta>
            <ta e="T301" id="Seg_3752" s="T300">adj-n:case</ta>
            <ta e="T302" id="Seg_3753" s="T301">ptcl</ta>
            <ta e="T303" id="Seg_3754" s="T302">adj-adj&gt;adv</ta>
            <ta e="T304" id="Seg_3755" s="T303">adj</ta>
            <ta e="T305" id="Seg_3756" s="T304">n-n:(num)-n:case</ta>
            <ta e="T306" id="Seg_3757" s="T305">n-n:(num)-n:case</ta>
            <ta e="T307" id="Seg_3758" s="T306">v-v:ptcp</ta>
            <ta e="T308" id="Seg_3759" s="T307">v-v:tense-v:poss.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_3760" s="T0">n</ta>
            <ta e="T2" id="Seg_3761" s="T1">dempro</ta>
            <ta e="T3" id="Seg_3762" s="T2">n</ta>
            <ta e="T4" id="Seg_3763" s="T3">adj</ta>
            <ta e="T5" id="Seg_3764" s="T4">n</ta>
            <ta e="T6" id="Seg_3765" s="T5">adv</ta>
            <ta e="T7" id="Seg_3766" s="T6">n</ta>
            <ta e="T8" id="Seg_3767" s="T7">adj</ta>
            <ta e="T9" id="Seg_3768" s="T8">cop</ta>
            <ta e="T10" id="Seg_3769" s="T9">adj</ta>
            <ta e="T11" id="Seg_3770" s="T10">n</ta>
            <ta e="T12" id="Seg_3771" s="T11">n</ta>
            <ta e="T13" id="Seg_3772" s="T12">n</ta>
            <ta e="T14" id="Seg_3773" s="T13">n</ta>
            <ta e="T15" id="Seg_3774" s="T14">n</ta>
            <ta e="T16" id="Seg_3775" s="T15">dempro</ta>
            <ta e="T17" id="Seg_3776" s="T16">n</ta>
            <ta e="T18" id="Seg_3777" s="T17">v</ta>
            <ta e="T19" id="Seg_3778" s="T18">dempro</ta>
            <ta e="T20" id="Seg_3779" s="T19">post</ta>
            <ta e="T21" id="Seg_3780" s="T20">adv</ta>
            <ta e="T22" id="Seg_3781" s="T21">n</ta>
            <ta e="T23" id="Seg_3782" s="T22">v</ta>
            <ta e="T24" id="Seg_3783" s="T23">pers</ta>
            <ta e="T25" id="Seg_3784" s="T24">v</ta>
            <ta e="T26" id="Seg_3785" s="T25">n</ta>
            <ta e="T27" id="Seg_3786" s="T26">n</ta>
            <ta e="T28" id="Seg_3787" s="T27">n</ta>
            <ta e="T29" id="Seg_3788" s="T28">n</ta>
            <ta e="T30" id="Seg_3789" s="T29">adj</ta>
            <ta e="T31" id="Seg_3790" s="T30">n</ta>
            <ta e="T32" id="Seg_3791" s="T31">n</ta>
            <ta e="T33" id="Seg_3792" s="T32">n</ta>
            <ta e="T34" id="Seg_3793" s="T33">n</ta>
            <ta e="T35" id="Seg_3794" s="T34">adv</ta>
            <ta e="T36" id="Seg_3795" s="T35">n</ta>
            <ta e="T37" id="Seg_3796" s="T36">cop</ta>
            <ta e="T38" id="Seg_3797" s="T37">n</ta>
            <ta e="T39" id="Seg_3798" s="T38">n</ta>
            <ta e="T40" id="Seg_3799" s="T39">adj</ta>
            <ta e="T41" id="Seg_3800" s="T40">n</ta>
            <ta e="T42" id="Seg_3801" s="T41">n</ta>
            <ta e="T43" id="Seg_3802" s="T42">v</ta>
            <ta e="T44" id="Seg_3803" s="T43">adj</ta>
            <ta e="T45" id="Seg_3804" s="T44">n</ta>
            <ta e="T46" id="Seg_3805" s="T45">cop</ta>
            <ta e="T47" id="Seg_3806" s="T46">n</ta>
            <ta e="T48" id="Seg_3807" s="T47">n</ta>
            <ta e="T49" id="Seg_3808" s="T48">v</ta>
            <ta e="T50" id="Seg_3809" s="T49">adj</ta>
            <ta e="T51" id="Seg_3810" s="T50">n</ta>
            <ta e="T52" id="Seg_3811" s="T51">adv</ta>
            <ta e="T53" id="Seg_3812" s="T52">n</ta>
            <ta e="T54" id="Seg_3813" s="T53">n</ta>
            <ta e="T55" id="Seg_3814" s="T54">v</ta>
            <ta e="T56" id="Seg_3815" s="T55">v</ta>
            <ta e="T57" id="Seg_3816" s="T56">n</ta>
            <ta e="T58" id="Seg_3817" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_3818" s="T58">n</ta>
            <ta e="T60" id="Seg_3819" s="T59">v</ta>
            <ta e="T61" id="Seg_3820" s="T60">n</ta>
            <ta e="T62" id="Seg_3821" s="T61">n</ta>
            <ta e="T63" id="Seg_3822" s="T62">n</ta>
            <ta e="T64" id="Seg_3823" s="T63">emphpro</ta>
            <ta e="T65" id="Seg_3824" s="T64">n</ta>
            <ta e="T66" id="Seg_3825" s="T65">v</ta>
            <ta e="T67" id="Seg_3826" s="T66">v</ta>
            <ta e="T68" id="Seg_3827" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_3828" s="T68">n</ta>
            <ta e="T70" id="Seg_3829" s="T69">v</ta>
            <ta e="T71" id="Seg_3830" s="T70">adv</ta>
            <ta e="T72" id="Seg_3831" s="T71">pers</ta>
            <ta e="T73" id="Seg_3832" s="T72">adv</ta>
            <ta e="T74" id="Seg_3833" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_3834" s="T74">v</ta>
            <ta e="T76" id="Seg_3835" s="T75">aux</ta>
            <ta e="T77" id="Seg_3836" s="T76">pers</ta>
            <ta e="T78" id="Seg_3837" s="T77">n</ta>
            <ta e="T79" id="Seg_3838" s="T78">v</ta>
            <ta e="T80" id="Seg_3839" s="T79">rel</ta>
            <ta e="T81" id="Seg_3840" s="T80">n</ta>
            <ta e="T82" id="Seg_3841" s="T81">cop</ta>
            <ta e="T83" id="Seg_3842" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_3843" s="T83">dempro</ta>
            <ta e="T85" id="Seg_3844" s="T84">adv</ta>
            <ta e="T86" id="Seg_3845" s="T85">adj</ta>
            <ta e="T87" id="Seg_3846" s="T86">n</ta>
            <ta e="T88" id="Seg_3847" s="T87">v</ta>
            <ta e="T89" id="Seg_3848" s="T88">v</ta>
            <ta e="T90" id="Seg_3849" s="T89">pers</ta>
            <ta e="T93" id="Seg_3850" s="T92">n</ta>
            <ta e="T94" id="Seg_3851" s="T93">dempro</ta>
            <ta e="T95" id="Seg_3852" s="T94">n</ta>
            <ta e="T96" id="Seg_3853" s="T95">que</ta>
            <ta e="T97" id="Seg_3854" s="T96">v</ta>
            <ta e="T98" id="Seg_3855" s="T97">aux</ta>
            <ta e="T99" id="Seg_3856" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_3857" s="T99">dempro</ta>
            <ta e="T101" id="Seg_3858" s="T100">n</ta>
            <ta e="T102" id="Seg_3859" s="T101">adv</ta>
            <ta e="T103" id="Seg_3860" s="T102">v</ta>
            <ta e="T104" id="Seg_3861" s="T103">dempro</ta>
            <ta e="T105" id="Seg_3862" s="T104">n</ta>
            <ta e="T106" id="Seg_3863" s="T105">v</ta>
            <ta e="T107" id="Seg_3864" s="T106">post</ta>
            <ta e="T108" id="Seg_3865" s="T107">adv</ta>
            <ta e="T109" id="Seg_3866" s="T108">v</ta>
            <ta e="T110" id="Seg_3867" s="T109">adv</ta>
            <ta e="T111" id="Seg_3868" s="T110">adv</ta>
            <ta e="T112" id="Seg_3869" s="T111">dempro</ta>
            <ta e="T113" id="Seg_3870" s="T112">v</ta>
            <ta e="T114" id="Seg_3871" s="T113">adj</ta>
            <ta e="T115" id="Seg_3872" s="T114">adv</ta>
            <ta e="T116" id="Seg_3873" s="T115">v</ta>
            <ta e="T117" id="Seg_3874" s="T116">v</ta>
            <ta e="T118" id="Seg_3875" s="T117">dempro</ta>
            <ta e="T119" id="Seg_3876" s="T118">n</ta>
            <ta e="T120" id="Seg_3877" s="T119">v</ta>
            <ta e="T121" id="Seg_3878" s="T120">aux</ta>
            <ta e="T122" id="Seg_3879" s="T121">n</ta>
            <ta e="T123" id="Seg_3880" s="T122">cop</ta>
            <ta e="T124" id="Seg_3881" s="T123">n</ta>
            <ta e="T125" id="Seg_3882" s="T124">v</ta>
            <ta e="T126" id="Seg_3883" s="T125">ptcl</ta>
            <ta e="T127" id="Seg_3884" s="T126">n</ta>
            <ta e="T128" id="Seg_3885" s="T127">que</ta>
            <ta e="T129" id="Seg_3886" s="T128">v</ta>
            <ta e="T130" id="Seg_3887" s="T129">v</ta>
            <ta e="T131" id="Seg_3888" s="T130">n</ta>
            <ta e="T132" id="Seg_3889" s="T131">adv</ta>
            <ta e="T133" id="Seg_3890" s="T132">v</ta>
            <ta e="T134" id="Seg_3891" s="T133">v</ta>
            <ta e="T135" id="Seg_3892" s="T134">pers</ta>
            <ta e="T136" id="Seg_3893" s="T135">adj</ta>
            <ta e="T137" id="Seg_3894" s="T136">adv</ta>
            <ta e="T138" id="Seg_3895" s="T137">v</ta>
            <ta e="T139" id="Seg_3896" s="T138">v</ta>
            <ta e="T140" id="Seg_3897" s="T139">adj</ta>
            <ta e="T141" id="Seg_3898" s="T140">n</ta>
            <ta e="T142" id="Seg_3899" s="T141">n</ta>
            <ta e="T143" id="Seg_3900" s="T142">v</ta>
            <ta e="T144" id="Seg_3901" s="T143">pers</ta>
            <ta e="T145" id="Seg_3902" s="T144">ptcl</ta>
            <ta e="T146" id="Seg_3903" s="T145">dempro</ta>
            <ta e="T147" id="Seg_3904" s="T146">post</ta>
            <ta e="T148" id="Seg_3905" s="T147">v</ta>
            <ta e="T149" id="Seg_3906" s="T148">n</ta>
            <ta e="T150" id="Seg_3907" s="T149">v</ta>
            <ta e="T151" id="Seg_3908" s="T150">n</ta>
            <ta e="T152" id="Seg_3909" s="T151">n</ta>
            <ta e="T153" id="Seg_3910" s="T152">n</ta>
            <ta e="T154" id="Seg_3911" s="T153">v</ta>
            <ta e="T155" id="Seg_3912" s="T154">dempro</ta>
            <ta e="T156" id="Seg_3913" s="T155">n</ta>
            <ta e="T157" id="Seg_3914" s="T156">pers</ta>
            <ta e="T158" id="Seg_3915" s="T157">post</ta>
            <ta e="T159" id="Seg_3916" s="T158">v</ta>
            <ta e="T160" id="Seg_3917" s="T159">v</ta>
            <ta e="T161" id="Seg_3918" s="T160">dempro</ta>
            <ta e="T162" id="Seg_3919" s="T161">n</ta>
            <ta e="T163" id="Seg_3920" s="T162">pers</ta>
            <ta e="T164" id="Seg_3921" s="T163">que</ta>
            <ta e="T165" id="Seg_3922" s="T164">v</ta>
            <ta e="T166" id="Seg_3923" s="T165">v</ta>
            <ta e="T167" id="Seg_3924" s="T166">pers</ta>
            <ta e="T168" id="Seg_3925" s="T167">n</ta>
            <ta e="T169" id="Seg_3926" s="T168">emphpro</ta>
            <ta e="T170" id="Seg_3927" s="T169">post</ta>
            <ta e="T171" id="Seg_3928" s="T170">v</ta>
            <ta e="T172" id="Seg_3929" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_3930" s="T172">n</ta>
            <ta e="T174" id="Seg_3931" s="T173">v</ta>
            <ta e="T175" id="Seg_3932" s="T174">pers</ta>
            <ta e="T176" id="Seg_3933" s="T175">pers</ta>
            <ta e="T177" id="Seg_3934" s="T176">que</ta>
            <ta e="T178" id="Seg_3935" s="T177">v</ta>
            <ta e="T179" id="Seg_3936" s="T178">v</ta>
            <ta e="T180" id="Seg_3937" s="T179">pers</ta>
            <ta e="T181" id="Seg_3938" s="T180">adv</ta>
            <ta e="T182" id="Seg_3939" s="T181">v</ta>
            <ta e="T183" id="Seg_3940" s="T182">n</ta>
            <ta e="T184" id="Seg_3941" s="T183">n</ta>
            <ta e="T185" id="Seg_3942" s="T184">n</ta>
            <ta e="T186" id="Seg_3943" s="T185">adv</ta>
            <ta e="T187" id="Seg_3944" s="T186">v</ta>
            <ta e="T188" id="Seg_3945" s="T187">n</ta>
            <ta e="T189" id="Seg_3946" s="T188">adv</ta>
            <ta e="T190" id="Seg_3947" s="T189">pers</ta>
            <ta e="T191" id="Seg_3948" s="T190">n</ta>
            <ta e="T192" id="Seg_3949" s="T191">v</ta>
            <ta e="T193" id="Seg_3950" s="T192">aux</ta>
            <ta e="T194" id="Seg_3951" s="T193">pers</ta>
            <ta e="T195" id="Seg_3952" s="T194">emphpro</ta>
            <ta e="T196" id="Seg_3953" s="T195">n</ta>
            <ta e="T197" id="Seg_3954" s="T196">adv</ta>
            <ta e="T198" id="Seg_3955" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_3956" s="T198">v</ta>
            <ta e="T200" id="Seg_3957" s="T199">ptcl</ta>
            <ta e="T201" id="Seg_3958" s="T200">pers</ta>
            <ta e="T202" id="Seg_3959" s="T201">pers</ta>
            <ta e="T203" id="Seg_3960" s="T202">v</ta>
            <ta e="T204" id="Seg_3961" s="T203">ptcl</ta>
            <ta e="T205" id="Seg_3962" s="T204">n</ta>
            <ta e="T206" id="Seg_3963" s="T205">n</ta>
            <ta e="T207" id="Seg_3964" s="T206">n</ta>
            <ta e="T208" id="Seg_3965" s="T207">v</ta>
            <ta e="T209" id="Seg_3966" s="T208">v</ta>
            <ta e="T210" id="Seg_3967" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_3968" s="T210">adj</ta>
            <ta e="T212" id="Seg_3969" s="T211">n</ta>
            <ta e="T213" id="Seg_3970" s="T212">v</ta>
            <ta e="T214" id="Seg_3971" s="T213">adj</ta>
            <ta e="T215" id="Seg_3972" s="T214">v</ta>
            <ta e="T216" id="Seg_3973" s="T215">dempro</ta>
            <ta e="T217" id="Seg_3974" s="T216">n</ta>
            <ta e="T218" id="Seg_3975" s="T217">pers</ta>
            <ta e="T219" id="Seg_3976" s="T218">adv</ta>
            <ta e="T220" id="Seg_3977" s="T219">v</ta>
            <ta e="T221" id="Seg_3978" s="T220">v</ta>
            <ta e="T222" id="Seg_3979" s="T221">v</ta>
            <ta e="T223" id="Seg_3980" s="T222">n</ta>
            <ta e="T224" id="Seg_3981" s="T223">v</ta>
            <ta e="T225" id="Seg_3982" s="T224">adv</ta>
            <ta e="T226" id="Seg_3983" s="T225">v</ta>
            <ta e="T227" id="Seg_3984" s="T226">adv</ta>
            <ta e="T228" id="Seg_3985" s="T227">pers</ta>
            <ta e="T229" id="Seg_3986" s="T228">adv</ta>
            <ta e="T230" id="Seg_3987" s="T229">adj</ta>
            <ta e="T231" id="Seg_3988" s="T230">v</ta>
            <ta e="T232" id="Seg_3989" s="T231">adj</ta>
            <ta e="T233" id="Seg_3990" s="T232">n</ta>
            <ta e="T234" id="Seg_3991" s="T233">n</ta>
            <ta e="T235" id="Seg_3992" s="T234">adj</ta>
            <ta e="T236" id="Seg_3993" s="T235">n</ta>
            <ta e="T237" id="Seg_3994" s="T236">v</ta>
            <ta e="T238" id="Seg_3995" s="T237">v</ta>
            <ta e="T239" id="Seg_3996" s="T238">n</ta>
            <ta e="T240" id="Seg_3997" s="T239">n</ta>
            <ta e="T241" id="Seg_3998" s="T240">v</ta>
            <ta e="T242" id="Seg_3999" s="T241">n</ta>
            <ta e="T243" id="Seg_4000" s="T242">v</ta>
            <ta e="T244" id="Seg_4001" s="T243">v</ta>
            <ta e="T245" id="Seg_4002" s="T244">ptcl</ta>
            <ta e="T246" id="Seg_4003" s="T245">pers</ta>
            <ta e="T247" id="Seg_4004" s="T246">v</ta>
            <ta e="T248" id="Seg_4005" s="T247">pers</ta>
            <ta e="T249" id="Seg_4006" s="T248">adj</ta>
            <ta e="T250" id="Seg_4007" s="T249">n</ta>
            <ta e="T251" id="Seg_4008" s="T250">v</ta>
            <ta e="T252" id="Seg_4009" s="T251">n</ta>
            <ta e="T253" id="Seg_4010" s="T252">ptcl</ta>
            <ta e="T254" id="Seg_4011" s="T253">n</ta>
            <ta e="T255" id="Seg_4012" s="T254">conj</ta>
            <ta e="T256" id="Seg_4013" s="T255">v</ta>
            <ta e="T257" id="Seg_4014" s="T256">aux</ta>
            <ta e="T258" id="Seg_4015" s="T257">v</ta>
            <ta e="T259" id="Seg_4016" s="T258">n</ta>
            <ta e="T260" id="Seg_4017" s="T259">adj</ta>
            <ta e="T261" id="Seg_4018" s="T260">n</ta>
            <ta e="T262" id="Seg_4019" s="T261">v</ta>
            <ta e="T263" id="Seg_4020" s="T262">n</ta>
            <ta e="T264" id="Seg_4021" s="T263">n</ta>
            <ta e="T265" id="Seg_4022" s="T264">post</ta>
            <ta e="T266" id="Seg_4023" s="T265">adj</ta>
            <ta e="T267" id="Seg_4024" s="T266">cop</ta>
            <ta e="T268" id="Seg_4025" s="T267">n</ta>
            <ta e="T269" id="Seg_4026" s="T268">n</ta>
            <ta e="T270" id="Seg_4027" s="T269">adj</ta>
            <ta e="T271" id="Seg_4028" s="T270">n</ta>
            <ta e="T272" id="Seg_4029" s="T271">v</ta>
            <ta e="T273" id="Seg_4030" s="T272">n</ta>
            <ta e="T274" id="Seg_4031" s="T273">n</ta>
            <ta e="T275" id="Seg_4032" s="T274">v</ta>
            <ta e="T276" id="Seg_4033" s="T275">dempro</ta>
            <ta e="T277" id="Seg_4034" s="T276">n</ta>
            <ta e="T278" id="Seg_4035" s="T277">v</ta>
            <ta e="T279" id="Seg_4036" s="T278">aux</ta>
            <ta e="T280" id="Seg_4037" s="T279">n</ta>
            <ta e="T281" id="Seg_4038" s="T280">adj</ta>
            <ta e="T282" id="Seg_4039" s="T281">n</ta>
            <ta e="T283" id="Seg_4040" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_4041" s="T283">v</ta>
            <ta e="T285" id="Seg_4042" s="T284">adj</ta>
            <ta e="T286" id="Seg_4043" s="T285">n</ta>
            <ta e="T287" id="Seg_4044" s="T286">ptcl</ta>
            <ta e="T288" id="Seg_4045" s="T287">v</ta>
            <ta e="T289" id="Seg_4046" s="T288">dempro</ta>
            <ta e="T290" id="Seg_4047" s="T289">n</ta>
            <ta e="T291" id="Seg_4048" s="T290">n</ta>
            <ta e="T292" id="Seg_4049" s="T291">n</ta>
            <ta e="T293" id="Seg_4050" s="T292">n</ta>
            <ta e="T294" id="Seg_4051" s="T293">adv</ta>
            <ta e="T295" id="Seg_4052" s="T294">que</ta>
            <ta e="T296" id="Seg_4053" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_4054" s="T296">v</ta>
            <ta e="T298" id="Seg_4055" s="T297">v</ta>
            <ta e="T299" id="Seg_4056" s="T298">aux</ta>
            <ta e="T300" id="Seg_4057" s="T299">n</ta>
            <ta e="T301" id="Seg_4058" s="T300">adj</ta>
            <ta e="T302" id="Seg_4059" s="T301">ptcl</ta>
            <ta e="T303" id="Seg_4060" s="T302">adv</ta>
            <ta e="T304" id="Seg_4061" s="T303">adj</ta>
            <ta e="T305" id="Seg_4062" s="T304">n</ta>
            <ta e="T306" id="Seg_4063" s="T305">n</ta>
            <ta e="T307" id="Seg_4064" s="T306">v</ta>
            <ta e="T308" id="Seg_4065" s="T307">aux</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T29" id="Seg_4066" s="T28">EV:cult</ta>
            <ta e="T53" id="Seg_4067" s="T52">RUS:core</ta>
            <ta e="T153" id="Seg_4068" s="T152">EV:core</ta>
            <ta e="T196" id="Seg_4069" s="T195">RUS:cult</ta>
            <ta e="T217" id="Seg_4070" s="T216">RUS:cult</ta>
            <ta e="T250" id="Seg_4071" s="T249">RUS:cult</ta>
            <ta e="T252" id="Seg_4072" s="T251">RUS:core</ta>
            <ta e="T255" id="Seg_4073" s="T254">RUS:gram</ta>
            <ta e="T263" id="Seg_4074" s="T262">RUS:cult</ta>
            <ta e="T266" id="Seg_4075" s="T265">EV:core</ta>
            <ta e="T270" id="Seg_4076" s="T269">EV:core</ta>
            <ta e="T271" id="Seg_4077" s="T270">RUS:cult</ta>
            <ta e="T274" id="Seg_4078" s="T273">RUS:cult</ta>
            <ta e="T286" id="Seg_4079" s="T285">RUS:core</ta>
            <ta e="T306" id="Seg_4080" s="T305">EV:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T1" id="Seg_4081" s="T0">The snow owl.</ta>
            <ta e="T5" id="Seg_4082" s="T1">That is the name of a Dolgan tale.</ta>
            <ta e="T9" id="Seg_4083" s="T5">Long ago, the tundra was warm.</ta>
            <ta e="T18" id="Seg_4084" s="T9">All birds, small birds, geese, ducks, small birds, were living on this land.</ta>
            <ta e="T23" id="Seg_4085" s="T18">The snow owl was living together with them.</ta>
            <ta e="T29" id="Seg_4086" s="T23">Its food are geese, ducks and partridges.</ta>
            <ta e="T34" id="Seg_4087" s="T29">The leader of all birds and small birds [was] a goose.</ta>
            <ta e="T37" id="Seg_4088" s="T34">Then the cold came.</ta>
            <ta e="T43" id="Seg_4089" s="T37">The leading goose gathered all birds and small birds.</ta>
            <ta e="T46" id="Seg_4090" s="T43">There was a big discussion.</ta>
            <ta e="T56" id="Seg_4091" s="T46">The birds and the small birds gathered on a broad glade and sat down there at the foot of a hill.</ta>
            <ta e="T60" id="Seg_4092" s="T56">The snow owl came to the gathering, too.</ta>
            <ta e="T66" id="Seg_4093" s="T60">The leading goose told its people the news.</ta>
            <ta e="T70" id="Seg_4094" s="T66">"Do you see that the weather is getting colder.</ta>
            <ta e="T76" id="Seg_4095" s="T70">We will be able to live here only in summer.</ta>
            <ta e="T83" id="Seg_4096" s="T76">I know a place where there is no winter.</ta>
            <ta e="T89" id="Seg_4097" s="T83">It is very far away, going along the earth one won't reach it.</ta>
            <ta e="T98" id="Seg_4098" s="T89">We, the winged ones, why should we freeze to death at this place?</ta>
            <ta e="T103" id="Seg_4099" s="T98">Well, let's fly directly to that place!</ta>
            <ta e="T109" id="Seg_4100" s="T103">Until the cold has gone over we will live there.</ta>
            <ta e="T117" id="Seg_4101" s="T109">If we stay here now, then all of us will freeze to death.</ta>
            <ta e="T123" id="Seg_4102" s="T117">If we fly to that place, we'll survive.</ta>
            <ta e="T127" id="Seg_4103" s="T123">Do I speak the truth, winged ones?</ta>
            <ta e="T130" id="Seg_4104" s="T127">Who agrees to travel?"</ta>
            <ta e="T131" id="Seg_4105" s="T130">"That's right.</ta>
            <ta e="T143" id="Seg_4106" s="T131">If we stay here, we'll die, all of us will fly there", all birds and small birds shouted together.</ta>
            <ta e="T150" id="Seg_4107" s="T143">"I, however, will [also] fliege there", the snow owl said.</ta>
            <ta e="T154" id="Seg_4108" s="T150">The leading goose asked its friends:</ta>
            <ta e="T160" id="Seg_4109" s="T154">"This snow owl wants to come with us.</ta>
            <ta e="T166" id="Seg_4110" s="T160">What do you think about that?"</ta>
            <ta e="T174" id="Seg_4111" s="T166">"We won't take the snow owl with us", the birds said.</ta>
            <ta e="T182" id="Seg_4112" s="T174">"Why don't you want to take her, say it directly to her."</ta>
            <ta e="T187" id="Seg_4113" s="T182">The birds said all about the snow owl:</ta>
            <ta e="T193" id="Seg_4114" s="T187">"The snow owl is eating our families here.</ta>
            <ta e="T200" id="Seg_4115" s="T193">She won't stop this behavior there.</ta>
            <ta e="T204" id="Seg_4116" s="T200">We won't take her with [us]."</ta>
            <ta e="T208" id="Seg_4117" s="T204">The leading goose says to the snow owl:</ta>
            <ta e="T213" id="Seg_4118" s="T208">"Do you see that the the winged people has decided.</ta>
            <ta e="T217" id="Seg_4119" s="T213">All did speak – that's law.</ta>
            <ta e="T220" id="Seg_4120" s="T217">You'll stay here."</ta>
            <ta e="T226" id="Seg_4121" s="T220">"I stop begging", the snow owl said, "I'll stay here.</ta>
            <ta e="T231" id="Seg_4122" s="T226">Maybe I'll stay alive here."</ta>
            <ta e="T238" id="Seg_4123" s="T231">All birds and small birds went in order to search the warm place.</ta>
            <ta e="T243" id="Seg_4124" s="T238">The snow owl comes home and says to its old wife:</ta>
            <ta e="T247" id="Seg_4125" s="T243">"Do you know that they don't take us?</ta>
            <ta e="T257" id="Seg_4126" s="T247">Sew a warm fur coat for me, for that I won't freeze in the cold or in a snowstorm."</ta>
            <ta e="T262" id="Seg_4127" s="T257">"I'll sew", the obedient old [owl] said.</ta>
            <ta e="T267" id="Seg_4128" s="T262">"The coat shall be as white as snow."</ta>
            <ta e="T272" id="Seg_4129" s="T267">The old snow owl wife sewed a coat, whiter than white.</ta>
            <ta e="T279" id="Seg_4130" s="T272">After the snow owl had dressed it, the coat sticked immediately to its body.</ta>
            <ta e="T288" id="Seg_4131" s="T279">The snow owl neither freezes in a frosty cold, nor does it let a snowstorm take it.</ta>
            <ta e="T299" id="Seg_4132" s="T288">Since that time the snow owl overwinteres here in the naked tundra not flying anywhere.</ta>
            <ta e="T308" id="Seg_4133" s="T299">The snow owl began to eat even more small birds and partridges than earlier.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T1" id="Seg_4134" s="T0">"Die Schneeeule".</ta>
            <ta e="T5" id="Seg_4135" s="T1">Das ist der Name eines dolganischen Märchens.</ta>
            <ta e="T9" id="Seg_4136" s="T5">Vor langer, langer Zeit war die Tundra warm.</ta>
            <ta e="T18" id="Seg_4137" s="T9">Alle Vögel – Vögelchen, Gänse, Enten, kleine Vögel – lebten in diesem Land.</ta>
            <ta e="T23" id="Seg_4138" s="T18">Mit ihnen zusammen lebte die Schneeeule.</ta>
            <ta e="T29" id="Seg_4139" s="T23">Ihre Nahrung sind Gänse, Enten und Rebhühner.</ta>
            <ta e="T34" id="Seg_4140" s="T29">Der Anführer aller Vögel und Vögelchen [war] eine Gans.</ta>
            <ta e="T37" id="Seg_4141" s="T34">Dann kam die Kälte.</ta>
            <ta e="T43" id="Seg_4142" s="T37">Die Anführergans versammelte alle Vögel und Vögelchen.</ta>
            <ta e="T46" id="Seg_4143" s="T43">Es fand eine große Besprechung statt.</ta>
            <ta e="T56" id="Seg_4144" s="T46">Die Vögel und Vögelchen sammelten sich auf einer Lichtung und setzten sich dort am Fuße eines Hügels.</ta>
            <ta e="T60" id="Seg_4145" s="T56">Die Schneeeule kam auch zur Versammlung.</ta>
            <ta e="T66" id="Seg_4146" s="T60">Die Anführergans teilte ihren Leuten die Neuigkeiten mit.</ta>
            <ta e="T70" id="Seg_4147" s="T66">"Seht ihr, dass das Wetter kälter geworden ist.</ta>
            <ta e="T76" id="Seg_4148" s="T70">Hier werden wir nur im Sommer leben können.</ta>
            <ta e="T83" id="Seg_4149" s="T76">Ich kenne einen Ort, wo es keinen Winter gibt.</ta>
            <ta e="T89" id="Seg_4150" s="T83">Der ist sehr weit entfernt, wenn man über die Erde geht, wird er nicht erreicht.</ta>
            <ta e="T98" id="Seg_4151" s="T89">Wir Gefiederte, warum sollen wir an diesem Ort erfrieren?</ta>
            <ta e="T103" id="Seg_4152" s="T98">Nun, lasst uns direkt an jenen Ort fliegen!</ta>
            <ta e="T109" id="Seg_4153" s="T103">Bis diese Kälte vorbeigeht, werden wir dort leben.</ta>
            <ta e="T117" id="Seg_4154" s="T109">Wenn wir jetzt hier bleiben, dann werden wir alle erfrieren.</ta>
            <ta e="T123" id="Seg_4155" s="T117">Wenn wir an jenen Ort fliegen, dann überleben wir.</ta>
            <ta e="T127" id="Seg_4156" s="T123">Spreche ich die Wahrheit, Gefiederte?</ta>
            <ta e="T130" id="Seg_4157" s="T127">Wer ist einverstanden, zu reisen?"</ta>
            <ta e="T131" id="Seg_4158" s="T130">"Das stimmt.</ta>
            <ta e="T143" id="Seg_4159" s="T131">Wenn wir hier bleiben, dann sterben wir, wir fliegen alle dorthin", schrieen die Vögel und Vögelchen gemeinsam.</ta>
            <ta e="T150" id="Seg_4160" s="T143">"Ich fliege doch wohl [auch] dorthin", sagte die Schneeeule.</ta>
            <ta e="T154" id="Seg_4161" s="T150">Die Anführergans fragte ihre Freunde:</ta>
            <ta e="T160" id="Seg_4162" s="T154">"Diese Schneeeule möchte mit uns kommen.</ta>
            <ta e="T166" id="Seg_4163" s="T160">Wie denkt ihr darüber?"</ta>
            <ta e="T174" id="Seg_4164" s="T166">"Wir nehmen die Schneeeule nicht mit uns mit", sagten die Vögel.</ta>
            <ta e="T182" id="Seg_4165" s="T174">"Warum wollt ihr sie nicht mitnehmen, sagt es ihr direkt."</ta>
            <ta e="T187" id="Seg_4166" s="T182">Die Vögel sagten durchweg über die Schneeeule:</ta>
            <ta e="T193" id="Seg_4167" s="T187">"Die Schneeeule frisst hier unsere Familien.</ta>
            <ta e="T200" id="Seg_4168" s="T193">Dieses Verhalten wird sie dort auch nicht lassen.</ta>
            <ta e="T204" id="Seg_4169" s="T200">Wir werden sie nicht mitnehmen."</ta>
            <ta e="T208" id="Seg_4170" s="T204">Die Anführergans sagt zur Schneeeule:</ta>
            <ta e="T213" id="Seg_4171" s="T208">"Siehst du, dass das geflügelte Volk entschieden hat.</ta>
            <ta e="T217" id="Seg_4172" s="T213">Alle haben es gesagt – das ist Gesetz.</ta>
            <ta e="T220" id="Seg_4173" s="T217">Du wirst hier bleiben."</ta>
            <ta e="T226" id="Seg_4174" s="T220">"Ich höre auf, zu bitten", sagte die Schneeeule, "ich bleibe hier.</ta>
            <ta e="T231" id="Seg_4175" s="T226">Vielleicht bleibe ich hier am Leben."</ta>
            <ta e="T238" id="Seg_4176" s="T231">Alle Vögel und Vögelchen gingen, um den warmen Ort zu suchen.</ta>
            <ta e="T243" id="Seg_4177" s="T238">Die Schneeeule kommt nach Hause und sagt zu ihrer Alten:</ta>
            <ta e="T247" id="Seg_4178" s="T243">"Weißt du, dass sie uns nicht mitnehmen?</ta>
            <ta e="T257" id="Seg_4179" s="T247">Nähe mir einen warmen Pelzmantel, damit ich weder im Schneesturm noch in der Kälte erfriere."</ta>
            <ta e="T262" id="Seg_4180" s="T257">"Nähe ich", sagte die gehorsame Alte.</ta>
            <ta e="T267" id="Seg_4181" s="T262">"Der Mantel soll weiß wie Schnee sein."</ta>
            <ta e="T272" id="Seg_4182" s="T267">Die Schneeeulen-Alte nähte einen Mantel, weißer als weiß.</ta>
            <ta e="T279" id="Seg_4183" s="T272">Als die Schneeeule den Mantel angezogen hatte, blieb jener sofort an ihrem Körper haften.</ta>
            <ta e="T288" id="Seg_4184" s="T279">Die Schneeeule erfriert weder in frostiger Kälte, noch lässt sie sich im starken Schneesturm mitnehmen.</ta>
            <ta e="T299" id="Seg_4185" s="T288">Seit jener Zeit, überwintert die Schneeeule hier in der kahlen Tundra, ohne irgendwohin zu fliegen.</ta>
            <ta e="T308" id="Seg_4186" s="T299">Die Schneeeule fing an, noch mehr kleine Vögelchen und Rebhühner zu fressen als früher.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T1" id="Seg_4187" s="T0">Полярная сова.</ta>
            <ta e="T5" id="Seg_4188" s="T1">То название долганской сказки.</ta>
            <ta e="T9" id="Seg_4189" s="T5">Давным-давно север/тундра тёплым был. </ta>
            <ta e="T18" id="Seg_4190" s="T9">Все летающие: птицы, гуси, утки, птицы-пуночки – на этой земле жили.</ta>
            <ta e="T23" id="Seg_4191" s="T18">С теми вместе полярная сова жила. </ta>
            <ta e="T29" id="Seg_4192" s="T23">Её еда – гуси, утки, куропатки.</ta>
            <ta e="T34" id="Seg_4193" s="T29">Всех летающих, птиц главный – гусь.</ta>
            <ta e="T37" id="Seg_4194" s="T34">Потом холода настали.</ta>
            <ta e="T43" id="Seg_4195" s="T37">Советующий (главный) гусь всех летающих, птиц собрал.</ta>
            <ta e="T46" id="Seg_4196" s="T43">Большое согласование (собрание) было.</ta>
            <ta e="T56" id="Seg_4197" s="T46">Летающие, птицы, собравшись на широкой поляне, там под сопкой разместились.</ta>
            <ta e="T60" id="Seg_4198" s="T56">Полярная сова тоже на собрание пришла.</ta>
            <ta e="T66" id="Seg_4199" s="T60">Советующий (главный) гусь новости своему народу рассказал.</ta>
            <ta e="T70" id="Seg_4200" s="T66">"Видите ли, на улице похолодало. </ta>
            <ta e="T76" id="Seg_4201" s="T70">Здесь мы летом только жить будем.</ta>
            <ta e="T83" id="Seg_4202" s="T76">Я землю знаю, где зима не бывает.</ta>
            <ta e="T89" id="Seg_4203" s="T83">Это очень далеко путём если идти (лететь), не доехать (не долететь). </ta>
            <ta e="T98" id="Seg_4204" s="T89">Мы, кры.. крылатые, на этой земле почему замерзать будем лежать?</ta>
            <ta e="T103" id="Seg_4205" s="T98">Вот, на ту землю прямо полетели!</ta>
            <ta e="T109" id="Seg_4206" s="T103">Эти холода пока пройдут, там жить будем.</ta>
            <ta e="T117" id="Seg_4207" s="T109">Сейчас здесь вот если останемся, все, сильно замёрзнув, помрём".</ta>
            <ta e="T123" id="Seg_4208" s="T117">Если в ту землю полетим, выживем.</ta>
            <ta e="T127" id="Seg_4209" s="T123">Правду говорю я что ли, крылатые? </ta>
            <ta e="T130" id="Seg_4210" s="T127">Кто дальний путь одобряет?"</ta>
            <ta e="T131" id="Seg_4211" s="T130">"Правда. </ta>
            <ta e="T143" id="Seg_4212" s="T131">Здесь если останемся, умрём, мы все туда полетим", – сказав, все летающие, птицы закричали. </ta>
            <ta e="T150" id="Seg_4213" s="T143">"Я тоже туда полечу", – полярная сова сказала.</ta>
            <ta e="T154" id="Seg_4214" s="T150">Главный гусь у друзей спросил: </ta>
            <ta e="T160" id="Seg_4215" s="T154">"Эта полярная сова с нами отправиться хочет. </ta>
            <ta e="T166" id="Seg_4216" s="T160">Об этом вы как думаете?"</ta>
            <ta e="T174" id="Seg_4217" s="T166">"Мы полярную сову вместе с собой не возьмём, – птицы сказали.</ta>
            <ta e="T182" id="Seg_4218" s="T174">"Вы её почему не собираетесь взять, ей прямо скажите".</ta>
            <ta e="T187" id="Seg_4219" s="T182">Птицы о полярной сове все сказали: </ta>
            <ta e="T193" id="Seg_4220" s="T187">"Полярная сова здесь нашими семьями питаясь живёт.</ta>
            <ta e="T200" id="Seg_4221" s="T193">Она свою манеру и там не бросит. </ta>
            <ta e="T204" id="Seg_4222" s="T200">Мы её не возьмём".</ta>
            <ta e="T208" id="Seg_4223" s="T204">Главный гусь полярной сове говорит: </ta>
            <ta e="T213" id="Seg_4224" s="T208">"Видишь ли, крылатый народ что решил. </ta>
            <ta e="T217" id="Seg_4225" s="T213">Все сказали – это закон.</ta>
            <ta e="T220" id="Seg_4226" s="T217">Ты здесь останешься".</ta>
            <ta e="T226" id="Seg_4227" s="T220">"Проситься заканчиваю, – полярная сова сказала, – здесь останусь.</ta>
            <ta e="T231" id="Seg_4228" s="T226">Может, я здесь живой буду".</ta>
            <ta e="T238" id="Seg_4229" s="T231">Все летающие, птицы тёплое место искать отправились. </ta>
            <ta e="T243" id="Seg_4230" s="T238">Полярная домой придя, старухе свой говорит: </ta>
            <ta e="T247" id="Seg_4231" s="T243">"Знаешь ли, нас не берут? </ta>
            <ta e="T257" id="Seg_4232" s="T247">Мне тёплую сокуй сшей, и в пургу, и в холода не замёрз чтобы я".</ta>
            <ta e="T262" id="Seg_4233" s="T257">"Сошью", – наивная старушка сказала.</ta>
            <ta e="T267" id="Seg_4234" s="T262">"Сокуй как снег белый пусть будет".</ta>
            <ta e="T272" id="Seg_4235" s="T267">Полярной совы старушка белый-пребелый сокуй сшила.</ta>
            <ta e="T279" id="Seg_4236" s="T272">Полярная сова сокуй надевши, та к телу сразу прилипла.</ta>
            <ta e="T288" id="Seg_4237" s="T279">Полярная сова в сильный мороз даже не замерзает, сильная пурга даже не одолевает её. </ta>
            <ta e="T299" id="Seg_4238" s="T288">С тех времён в голой (солёной) тундре полярная сова здесь, никуда не улетая, зимует.</ta>
            <ta e="T308" id="Seg_4239" s="T299">Полярная сова чем раньше ещё сильнее маленьких птиц, куропаток есть стала.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
