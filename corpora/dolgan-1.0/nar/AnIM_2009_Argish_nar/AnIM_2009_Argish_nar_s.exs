<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDDB7BE52C-519C-D981-0772-BF3E65C013F1">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>AnIM_2009_Argish_nar</transcription-name>
         <referenced-file url="AnIM_2009_Argish_nar.wav" />
         <referenced-file url="AnIM_2009_Argish_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\AnIM_2009_Argish_nar\AnIM_2009_Argish_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">353</ud-information>
            <ud-information attribute-name="# HIAT:w">293</ud-information>
            <ud-information attribute-name="# e">278</ud-information>
            <ud-information attribute-name="# HIAT:u">45</ud-information>
            <ud-information attribute-name="# sc">22</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AnIM">
            <abbreviation>AnIM</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.6733219565768355" />
         <tli id="T1" time="0.902" type="appl" />
         <tli id="T2" time="1.594" type="appl" />
         <tli id="T3" time="2.286" type="appl" />
         <tli id="T4" time="2.978" type="appl" />
         <tli id="T305" time="3.638963673228014" type="intp" />
         <tli id="T5" time="4.299927346456028" />
         <tli id="T6" time="4.933" type="appl" />
         <tli id="T7" time="5.507" type="appl" />
         <tli id="T8" time="6.299893554109994" />
         <tli id="T9" time="6.763" type="appl" />
         <tli id="T10" time="7.197" type="appl" />
         <tli id="T11" time="7.63" type="appl" />
         <tli id="T12" time="8.063" type="appl" />
         <tli id="T13" time="8.497" type="appl" />
         <tli id="T14" time="9.003331762297247" />
         <tli id="T15" time="9.725" type="appl" />
         <tli id="T16" time="10.42" type="appl" />
         <tli id="T17" time="11.115" type="appl" />
         <tli id="T307" time="11.619803666469544" />
         <tli id="T18" time="11.9997972459238" />
         <tli id="T19" time="12.355" type="appl" />
         <tli id="T20" time="12.8" type="appl" />
         <tli id="T21" time="13.245" type="appl" />
         <tli id="T22" time="13.69" type="appl" />
         <tli id="T23" time="14.135" type="appl" />
         <tli id="T306" time="14.564040001689618" type="intp" />
         <tli id="T24" time="14.993080003379236" />
         <tli id="T25" time="15.034" type="appl" />
         <tli id="T26" time="15.428" type="appl" />
         <tli id="T27" time="15.822" type="appl" />
         <tli id="T28" time="16.216" type="appl" />
         <tli id="T29" time="16.61" type="appl" />
         <tli id="T30" time="17.004" type="appl" />
         <tli id="T31" time="17.398" type="appl" />
         <tli id="T32" time="17.792" type="appl" />
         <tli id="T33" time="18.186" type="appl" />
         <tli id="T34" time="18.753330596065304" />
         <tli id="T35" time="19.247" type="appl" />
         <tli id="T36" time="19.843" type="appl" />
         <tli id="T308" time="20.386322210019433" />
         <tli id="T37" time="20.639651262988934" />
         <tli id="T38" time="21.245" type="appl" />
         <tli id="T39" time="21.97" type="appl" />
         <tli id="T40" time="22.64" type="appl" />
         <tli id="T41" time="23.16" type="appl" />
         <tli id="T42" time="23.733332714687" />
         <tli id="T43" time="23.952" type="appl" />
         <tli id="T44" time="24.154" type="appl" />
         <tli id="T45" time="24.357" type="appl" />
         <tli id="T46" time="24.559" type="appl" />
         <tli id="T47" time="24.761" type="appl" />
         <tli id="T48" time="24.963" type="appl" />
         <tli id="T49" time="25.166" type="appl" />
         <tli id="T50" time="25.368" type="appl" />
         <tli id="T51" time="27.779530624313594" />
         <tli id="T52" time="28.166" type="appl" />
         <tli id="T53" time="28.591" type="appl" />
         <tli id="T54" time="29.017" type="appl" />
         <tli id="T55" time="29.442" type="appl" />
         <tli id="T56" time="29.868" type="appl" />
         <tli id="T57" time="30.293" type="appl" />
         <tli id="T58" time="30.719" type="appl" />
         <tli id="T59" time="31.144" type="appl" />
         <tli id="T60" time="31.57" type="appl" />
         <tli id="T61" time="32.179" type="appl" />
         <tli id="T62" time="32.618" type="appl" />
         <tli id="T63" time="33.057" type="appl" />
         <tli id="T64" time="33.496" type="appl" />
         <tli id="T65" time="33.934" type="appl" />
         <tli id="T66" time="34.373" type="appl" />
         <tli id="T67" time="34.812" type="appl" />
         <tli id="T68" time="35.251" type="appl" />
         <tli id="T69" time="35.66999951687505" />
         <tli id="T70" time="36.272" type="appl" />
         <tli id="T71" time="36.614" type="appl" />
         <tli id="T72" time="36.957" type="appl" />
         <tli id="T73" time="37.299" type="appl" />
         <tli id="T74" time="37.641" type="appl" />
         <tli id="T75" time="37.983" type="appl" />
         <tli id="T76" time="38.326" type="appl" />
         <tli id="T77" time="38.668" type="appl" />
         <tli id="T78" time="39.316659906437444" />
         <tli id="T79" time="39.808" type="appl" />
         <tli id="T80" time="40.316" type="appl" />
         <tli id="T81" time="40.823" type="appl" />
         <tli id="T82" time="41.331" type="appl" />
         <tli id="T83" time="41.839" type="appl" />
         <tli id="T84" time="42.347" type="appl" />
         <tli id="T85" time="42.854" type="appl" />
         <tli id="T86" time="43.362" type="appl" />
         <tli id="T87" time="43.9033337099772" />
         <tli id="T88" time="44.462" type="appl" />
         <tli id="T89" time="44.845" type="appl" />
         <tli id="T90" time="45.228" type="appl" />
         <tli id="T91" time="45.61" type="appl" />
         <tli id="T92" time="45.992" type="appl" />
         <tli id="T93" time="46.375" type="appl" />
         <tli id="T94" time="46.758" type="appl" />
         <tli id="T95" time="47.286665215531805" />
         <tli id="T96" time="47.957" type="appl" />
         <tli id="T97" time="48.464" type="appl" />
         <tli id="T98" time="48.971" type="appl" />
         <tli id="T99" time="49.479" type="appl" />
         <tli id="T100" time="49.986" type="appl" />
         <tli id="T101" time="50.493" type="appl" />
         <tli id="T102" time="50.98000124873279" />
         <tli id="T103" time="51.802" type="appl" />
         <tli id="T104" time="52.335" type="appl" />
         <tli id="T105" time="52.868" type="appl" />
         <tli id="T106" time="53.4" type="appl" />
         <tli id="T107" time="53.933" type="appl" />
         <tli id="T108" time="54.465" type="appl" />
         <tli id="T109" time="54.998" type="appl" />
         <tli id="T110" time="55.499062262397565" />
         <tli id="T111" time="56.664" type="appl" />
         <tli id="T112" time="57.008" type="appl" />
         <tli id="T113" time="57.351" type="appl" />
         <tli id="T114" time="57.695" type="appl" />
         <tli id="T115" time="58.039" type="appl" />
         <tli id="T116" time="58.382" type="appl" />
         <tli id="T117" time="58.726" type="appl" />
         <tli id="T118" time="59.07" type="appl" />
         <tli id="T295" time="59.160666666666664" type="intp" />
         <tli id="T119" time="59.614" type="appl" />
         <tli id="T120" time="60.088" type="appl" />
         <tli id="T121" time="60.562" type="appl" />
         <tli id="T122" time="61.036" type="appl" />
         <tli id="T123" time="61.51" type="appl" />
         <tli id="T124" time="61.984" type="appl" />
         <tli id="T125" time="62.458" type="appl" />
         <tli id="T126" time="62.932" type="appl" />
         <tli id="T127" time="63.406" type="appl" />
         <tli id="T128" time="64.19891526569232" />
         <tli id="T129" time="65.442" type="appl" />
         <tli id="T130" time="66.565" type="appl" />
         <tli id="T131" time="67.687" type="appl" />
         <tli id="T132" time="68.97000001584017" />
         <tli id="T133" time="69.274" type="appl" />
         <tli id="T134" time="69.639" type="appl" />
         <tli id="T135" time="70.003" type="appl" />
         <tli id="T136" time="70.368" type="appl" />
         <tli id="T137" time="70.732" type="appl" />
         <tli id="T138" time="71.097" type="appl" />
         <tli id="T139" time="71.461" type="appl" />
         <tli id="T140" time="71.826" type="appl" />
         <tli id="T141" time="72.19" type="appl" />
         <tli id="T142" time="72.887" type="appl" />
         <tli id="T143" time="73.324" type="appl" />
         <tli id="T144" time="73.761" type="appl" />
         <tli id="T145" time="74.198" type="appl" />
         <tli id="T146" time="74.635" type="appl" />
         <tli id="T147" time="75.072" type="appl" />
         <tli id="T148" time="75.509" type="appl" />
         <tli id="T149" time="75.946" type="appl" />
         <tli id="T150" time="76.383" type="appl" />
         <tli id="T151" time="76.82" type="appl" />
         <tli id="T152" time="77.684" type="appl" />
         <tli id="T153" time="78.239" type="appl" />
         <tli id="T154" time="78.793" type="appl" />
         <tli id="T155" time="79.347" type="appl" />
         <tli id="T156" time="79.901" type="appl" />
         <tli id="T157" time="80.456" type="appl" />
         <tli id="T158" time="81.10333016178086" />
         <tli id="T159" time="81.684" type="appl" />
         <tli id="T160" time="82.238" type="appl" />
         <tli id="T161" time="82.791" type="appl" />
         <tli id="T162" time="83.345" type="appl" />
         <tli id="T163" time="83.899" type="appl" />
         <tli id="T164" time="84.452" type="appl" />
         <tli id="T165" time="85.006" type="appl" />
         <tli id="T166" time="85.65999796189914" />
         <tli id="T167" time="86.19866724571258" />
         <tli id="T168" time="86.544" type="appl" />
         <tli id="T169" time="86.897" type="appl" />
         <tli id="T170" time="87.249" type="appl" />
         <tli id="T171" time="87.601" type="appl" />
         <tli id="T172" time="87.953" type="appl" />
         <tli id="T173" time="88.306" type="appl" />
         <tli id="T174" time="88.658" type="appl" />
         <tli id="T175" time="89.01" type="appl" />
         <tli id="T176" time="89.409" type="appl" />
         <tli id="T177" time="89.78" type="appl" />
         <tli id="T178" time="90.15" type="appl" />
         <tli id="T179" time="90.91846379994931" />
         <tli id="T180" time="93.294" type="appl" />
         <tli id="T181" time="93.778" type="appl" />
         <tli id="T182" time="94.261" type="appl" />
         <tli id="T183" time="94.745" type="appl" />
         <tli id="T184" time="95.23566950029569" />
         <tli id="T296" time="95.62233333333333" type="intp" />
         <tli id="T185" time="95.819" type="appl" />
         <tli id="T186" time="96.178" type="appl" />
         <tli id="T187" time="96.537" type="appl" />
         <tli id="T188" time="96.896" type="appl" />
         <tli id="T189" time="97.255" type="appl" />
         <tli id="T190" time="97.614" type="appl" />
         <tli id="T191" time="97.973" type="appl" />
         <tli id="T192" time="98.332" type="appl" />
         <tli id="T193" time="98.691" type="appl" />
         <tli id="T194" time="99.27666241974318" />
         <tli id="T195" time="99.74" type="appl" />
         <tli id="T196" time="100.29" type="appl" />
         <tli id="T197" time="100.84" type="appl" />
         <tli id="T198" time="101.39" type="appl" />
         <tli id="T199" time="102.99159314015375" />
         <tli id="T200" time="103.594" type="appl" />
         <tli id="T201" time="104.168" type="appl" />
         <tli id="T202" time="104.742" type="appl" />
         <tli id="T203" time="105.317" type="appl" />
         <tli id="T204" time="105.891" type="appl" />
         <tli id="T205" time="106.465" type="appl" />
         <tli id="T309" time="106.88492679733041" type="intp" />
         <tli id="T206" time="107.30485359466081" />
         <tli id="T297" time="107.43481818181819" type="intp" />
         <tli id="T207" time="107.661" type="appl" />
         <tli id="T208" time="108.223" type="appl" />
         <tli id="T209" time="108.784" type="appl" />
         <tli id="T210" time="109.346" type="appl" />
         <tli id="T211" time="109.907" type="appl" />
         <tli id="T212" time="110.469" type="appl" />
         <tli id="T213" time="111.03" type="appl" />
         <tli id="T298" time="111.10571428571428" type="intp" />
         <tli id="T214" time="111.56" type="appl" />
         <tli id="T215" time="112.02" type="appl" />
         <tli id="T216" time="112.48" type="appl" />
         <tli id="T217" time="112.94" type="appl" />
         <tli id="T218" time="113.4" type="appl" />
         <tli id="T219" time="114.53139815831715" />
         <tli id="T220" time="114.786" type="appl" />
         <tli id="T221" time="115.241" type="appl" />
         <tli id="T222" time="115.697" type="appl" />
         <tli id="T223" time="116.152" type="appl" />
         <tli id="T224" time="116.608" type="appl" />
         <tli id="T225" time="117.063" type="appl" />
         <tli id="T226" time="117.519" type="appl" />
         <tli id="T227" time="117.974" type="appl" />
         <tli id="T228" time="118.43" type="appl" />
         <tli id="T229" time="119.523" type="appl" />
         <tli id="T230" time="120.316" type="appl" />
         <tli id="T231" time="121.23666246726367" />
         <tli id="T232" time="121.695" type="appl" />
         <tli id="T233" time="122.2" type="appl" />
         <tli id="T234" time="122.705" type="appl" />
         <tli id="T235" time="123.53124609275997" />
         <tli id="T236" time="123.92" type="appl" />
         <tli id="T237" time="124.35" type="appl" />
         <tli id="T238" time="124.78" type="appl" />
         <tli id="T239" time="125.39665728225057" />
         <tli id="T240" time="126.011" type="appl" />
         <tli id="T241" time="126.411" type="appl" />
         <tli id="T242" time="126.812" type="appl" />
         <tli id="T243" time="127.213" type="appl" />
         <tli id="T244" time="127.614" type="appl" />
         <tli id="T245" time="128.014" type="appl" />
         <tli id="T246" time="128.415" type="appl" />
         <tli id="T247" time="128.816" type="appl" />
         <tli id="T248" time="129.216" type="appl" />
         <tli id="T249" time="129.617" type="appl" />
         <tli id="T250" time="130.018" type="appl" />
         <tli id="T251" time="130.419" type="appl" />
         <tli id="T252" time="130.819" type="appl" />
         <tli id="T253" time="131.39999334713187" />
         <tli id="T254" time="132.305" type="appl" />
         <tli id="T310" time="132.73109064796824" />
         <tli id="T255" time="134.79772239587732" />
         <tli id="T256" time="135.157" type="appl" />
         <tli id="T257" time="135.623" type="appl" />
         <tli id="T301" time="137.6043416406184" />
         <tli id="T258" time="138.96431866182309" />
         <tli id="T259" time="139.46" type="appl" />
         <tli id="T260" time="139.93" type="appl" />
         <tli id="T261" time="140.4" type="appl" />
         <tli id="T262" time="141.00333368885697" />
         <tli id="T299" time="141.20142857142858" type="intp" />
         <tli id="T263" time="141.45" type="appl" />
         <tli id="T264" time="141.95" type="appl" />
         <tli id="T265" time="142.45" type="appl" />
         <tli id="T266" time="142.95" type="appl" />
         <tli id="T302" time="143.30378634789218" type="intp" />
         <tli id="T267" time="143.65757269578438" />
         <tli id="T268" time="143.914" type="appl" />
         <tli id="T269" time="144.309" type="appl" />
         <tli id="T270" time="144.703" type="appl" />
         <tli id="T271" time="145.098" type="appl" />
         <tli id="T272" time="145.492" type="appl" />
         <tli id="T273" time="145.887" type="appl" />
         <tli id="T274" time="146.281" type="appl" />
         <tli id="T275" time="146.676" type="appl" />
         <tli id="T303" time="146.85085207400525" />
         <tli id="T276" time="147.7375037593985" />
         <tli id="T277" time="147.975" type="appl" />
         <tli id="T278" time="148.351" type="appl" />
         <tli id="T279" time="148.726" type="appl" />
         <tli id="T280" time="149.102" type="appl" />
         <tli id="T281" time="149.477" type="appl" />
         <tli id="T282" time="149.853" type="appl" />
         <tli id="T283" time="150.228" type="appl" />
         <tli id="T284" time="150.604" type="appl" />
         <tli id="T285" time="150.979" type="appl" />
         <tli id="T286" time="151.2107784066909" />
         <tli id="T304" time="151.45077435160934" />
         <tli id="T287" time="152.0240979978035" />
         <tli id="T288" time="152.59" type="appl" />
         <tli id="T289" time="153.209" type="appl" />
         <tli id="T290" time="154.43072400101377" />
         <tli id="T291" time="154.825" type="appl" />
         <tli id="T292" time="155.18" type="appl" />
         <tli id="T293" time="155.535" type="appl" />
         <tli id="T294" time="155.89" type="appl" />
         <tli id="T300" time="157.824" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AnIM"
                      type="t">
         <timeline-fork end="T33" start="T26">
            <tli id="T26.tx.1" />
            <tli id="T26.tx.2" />
            <tli id="T26.tx.3" />
            <tli id="T26.tx.4" />
            <tli id="T26.tx.5" />
            <tli id="T26.tx.6" />
         </timeline-fork>
         <timeline-fork end="T128" start="T123">
            <tli id="T123.tx.1" />
            <tli id="T123.tx.2" />
            <tli id="T123.tx.3" />
         </timeline-fork>
         <timeline-fork end="T170" start="T167">
            <tli id="T167.tx.1" />
            <tli id="T167.tx.2" />
         </timeline-fork>
         <timeline-fork end="T207" start="T206">
            <tli id="T206.tx.1" />
         </timeline-fork>
         <timeline-fork end="T231" start="T228">
            <tli id="T228.tx.1" />
            <tli id="T228.tx.2" />
         </timeline-fork>
         <timeline-fork end="T263" start="T262">
            <tli id="T262.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T305" id="Seg_0" n="sc" s="T0">
               <ts e="T305" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Köhöbüt</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">oččogo</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">ittebitin</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">barɨkaːn</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_16" n="HIAT:w" s="T4">terinebit</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T307" id="Seg_19" n="sc" s="T5">
               <ts e="T8" id="Seg_21" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T5">Heee</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">dʼi͡ebitin</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">kahɨnabɨt</ts>
                  <nts id="Seg_30" n="HIAT:ip">.</nts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_33" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_35" n="HIAT:w" s="T8">Kömüleːk</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">že</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">dʼi͡ebit</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">bu͡olokput</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">komulaːk</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">bu͡o</ts>
                  <nts id="Seg_51" n="HIAT:ip">.</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T307" id="Seg_54" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">Dʼi͡elerin</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">kömüleːk</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">kahɨnɨ͡aktarɨn</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_65" n="HIAT:w" s="T17">naːda</ts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T306" id="Seg_68" n="sc" s="T18">
               <ts e="T306" id="Seg_70" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_72" n="HIAT:w" s="T18">Onton</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">dʼaktattar</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">bu͡ollagɨna</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_81" n="HIAT:w" s="T21">dʼi͡e</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_84" n="HIAT:w" s="T22">ihin</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_87" n="HIAT:w" s="T23">ubirajdɨːllar</ts>
                  <nts id="Seg_88" n="HIAT:ip">.</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T308" id="Seg_90" n="sc" s="T24">
               <ts e="T34" id="Seg_92" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_94" n="HIAT:w" s="T24">Bu</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_97" n="HIAT:w" s="T25">ka</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26.tx.1" id="Seg_100" n="HIAT:w" s="T26">eto</ts>
                  <nts id="Seg_101" n="HIAT:ip">,</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26.tx.2" id="Seg_104" n="HIAT:w" s="T26.tx.1">to</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26.tx.3" id="Seg_107" n="HIAT:w" s="T26.tx.2">što</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26.tx.4" id="Seg_110" n="HIAT:w" s="T26.tx.3">posuda</ts>
                  <nts id="Seg_111" n="HIAT:ip">,</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26.tx.5" id="Seg_114" n="HIAT:w" s="T26.tx.4">vse</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26.tx.6" id="Seg_117" n="HIAT:w" s="T26.tx.5">eto</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_120" n="HIAT:w" s="T26.tx.6">ubiraetsja</ts>
                  <nts id="Seg_121" n="HIAT:ip">,</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_124" n="HIAT:w" s="T33">barɨkaːnnara</ts>
                  <nts id="Seg_125" n="HIAT:ip">.</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T308" id="Seg_128" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">Onton</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">ü͡örü</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_136" n="HIAT:w" s="T36">egeleller</ts>
                  <nts id="Seg_137" n="HIAT:ip">.</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T309" id="Seg_139" n="sc" s="T37">
               <ts e="T39" id="Seg_141" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_143" n="HIAT:w" s="T37">Stado</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_146" n="HIAT:w" s="T38">privozjat</ts>
                  <nts id="Seg_147" n="HIAT:ip">.</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_150" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_152" n="HIAT:w" s="T39">Onno</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_155" n="HIAT:w" s="T40">taba</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_158" n="HIAT:w" s="T41">tuttallar</ts>
                  <nts id="Seg_159" n="HIAT:ip">.</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_162" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_164" n="HIAT:w" s="T42">Taba</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_167" n="HIAT:w" s="T43">tuttabɨt</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_170" n="HIAT:w" s="T44">bu͡o</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_173" n="HIAT:w" s="T45">oččogo</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_176" n="HIAT:w" s="T46">bu͡ollagɨna</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_179" n="HIAT:w" s="T47">bolokputugar</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_182" n="HIAT:w" s="T48">agɨs</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_185" n="HIAT:w" s="T49">tabanɨ</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_188" n="HIAT:w" s="T50">kölüjebit</ts>
                  <nts id="Seg_189" n="HIAT:ip">.</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_192" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_194" n="HIAT:w" s="T51">Agɨs</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_197" n="HIAT:w" s="T52">tabanɨ</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_200" n="HIAT:w" s="T53">tutallar</ts>
                  <nts id="Seg_201" n="HIAT:ip">,</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_204" n="HIAT:w" s="T54">ulakan</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_207" n="HIAT:w" s="T55">tabalar</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_210" n="HIAT:w" s="T56">naːda</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_213" n="HIAT:w" s="T57">tutu͡okka</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_216" n="HIAT:w" s="T58">bolokko</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_219" n="HIAT:w" s="T59">ke</ts>
                  <nts id="Seg_220" n="HIAT:ip">.</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_223" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_225" n="HIAT:w" s="T60">Onton</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_228" n="HIAT:w" s="T61">bolok</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_231" n="HIAT:w" s="T62">innitiger</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_234" n="HIAT:w" s="T63">bu͡ollagɨna</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_237" n="HIAT:w" s="T64">bu͡o</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_240" n="HIAT:w" s="T65">össü͡ö</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_243" n="HIAT:w" s="T66">da</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_246" n="HIAT:w" s="T67">hetiː</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_249" n="HIAT:w" s="T68">baːr</ts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_253" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_255" n="HIAT:w" s="T69">Ol</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_258" n="HIAT:w" s="T70">hetiː</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_261" n="HIAT:w" s="T71">bu͡o</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_264" n="HIAT:w" s="T72">itteni</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_267" n="HIAT:w" s="T73">barɨtɨn</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_270" n="HIAT:w" s="T74">ti͡ejeller</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_273" n="HIAT:w" s="T75">bu</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_276" n="HIAT:w" s="T76">ka</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_279" n="HIAT:w" s="T77">gruzittɨːllar</ts>
                  <nts id="Seg_280" n="HIAT:ip">.</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_283" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_285" n="HIAT:w" s="T78">Mastarɨ</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_288" n="HIAT:w" s="T79">bu͡ol</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_291" n="HIAT:w" s="T80">bu͡očkalarɨ</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_294" n="HIAT:w" s="T81">bu͡ol</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_297" n="HIAT:w" s="T82">možno</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_300" n="HIAT:w" s="T83">ugoli</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_303" n="HIAT:w" s="T84">itte</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_306" n="HIAT:w" s="T85">uːrallar</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_309" n="HIAT:w" s="T86">bu͡o</ts>
                  <nts id="Seg_310" n="HIAT:ip">.</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_313" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_315" n="HIAT:w" s="T87">Itte</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_318" n="HIAT:w" s="T88">itte</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_321" n="HIAT:w" s="T89">tu͡ok</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_324" n="HIAT:w" s="T90">kaːlbɨt</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_327" n="HIAT:w" s="T91">kimi</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_330" n="HIAT:w" s="T92">barɨkaːn</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_333" n="HIAT:w" s="T93">onno</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_336" n="HIAT:w" s="T94">ti͡ejeller</ts>
                  <nts id="Seg_337" n="HIAT:ip">.</nts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_340" n="HIAT:u" s="T95">
                  <ts e="T96" id="Seg_342" n="HIAT:w" s="T95">Onno</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_345" n="HIAT:w" s="T96">tü͡ört</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_348" n="HIAT:w" s="T97">tabanɨ</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_351" n="HIAT:w" s="T98">kölüjeller</ts>
                  <nts id="Seg_352" n="HIAT:ip">,</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_355" n="HIAT:w" s="T99">tü͡ört</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_358" n="HIAT:w" s="T100">tabanɨ</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_361" n="HIAT:w" s="T101">kölüjeller</ts>
                  <nts id="Seg_362" n="HIAT:ip">.</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_365" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_367" n="HIAT:w" s="T102">Onton</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_370" n="HIAT:w" s="T103">inniki</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_373" n="HIAT:w" s="T104">hɨrga</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_376" n="HIAT:w" s="T105">bu͡ollagɨna</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_379" n="HIAT:w" s="T106">uže</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_382" n="HIAT:w" s="T107">hozjainɨŋ</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_385" n="HIAT:w" s="T108">bejetin</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_388" n="HIAT:w" s="T109">hɨrgata</ts>
                  <nts id="Seg_389" n="HIAT:ip">.</nts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_392" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_394" n="HIAT:w" s="T110">Onno</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_397" n="HIAT:w" s="T111">otto</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_400" n="HIAT:w" s="T112">emi͡e</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_403" n="HIAT:w" s="T113">be</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_406" n="HIAT:w" s="T114">ugol</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_409" n="HIAT:w" s="T115">možno</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_412" n="HIAT:w" s="T116">uːru͡okkun</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_415" n="HIAT:w" s="T117">bu͡o</ts>
                  <nts id="Seg_416" n="HIAT:ip">.</nts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_419" n="HIAT:u" s="T118">
                  <ts e="T295" id="Seg_421" n="HIAT:w" s="T118">I</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_424" n="HIAT:w" s="T295">onno</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_427" n="HIAT:w" s="T119">nipte</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_430" n="HIAT:w" s="T120">di͡eččibit</ts>
                  <nts id="Seg_431" n="HIAT:ip">,</nts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_434" n="HIAT:w" s="T121">mas</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_437" n="HIAT:w" s="T122">abɨrattaːččɨbɨt</ts>
                  <nts id="Seg_438" n="HIAT:ip">,</nts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123.tx.1" id="Seg_441" n="HIAT:w" s="T123">palka</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123.tx.2" id="Seg_444" n="HIAT:w" s="T123.tx.1">takaja</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123.tx.3" id="Seg_447" n="HIAT:w" s="T123.tx.2">drova</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_450" n="HIAT:w" s="T123.tx.3">rubit</ts>
                  <nts id="Seg_451" n="HIAT:ip">.</nts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_454" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_456" n="HIAT:w" s="T128">Onno</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_459" n="HIAT:w" s="T129">nipteler</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_462" n="HIAT:w" s="T130">nipte</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_465" n="HIAT:w" s="T131">nipte</ts>
                  <nts id="Seg_466" n="HIAT:ip">.</nts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_469" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_471" n="HIAT:w" s="T132">De</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_474" n="HIAT:w" s="T133">onu</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_477" n="HIAT:w" s="T134">onu</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_480" n="HIAT:w" s="T135">itte</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_482" n="HIAT:ip">(</nts>
                  <ts e="T138" id="Seg_484" n="HIAT:w" s="T136">uːr-</ts>
                  <nts id="Seg_485" n="HIAT:ip">)</nts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_488" n="HIAT:w" s="T138">ti͡ejeller</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_491" n="HIAT:w" s="T139">hɨrgaga</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_494" n="HIAT:w" s="T140">ke</ts>
                  <nts id="Seg_495" n="HIAT:ip">.</nts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_498" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_500" n="HIAT:w" s="T141">De</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_503" n="HIAT:w" s="T142">onton</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_506" n="HIAT:w" s="T143">iti</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_509" n="HIAT:w" s="T144">hozjain</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_512" n="HIAT:w" s="T145">ke</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_515" n="HIAT:w" s="T146">kim</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_518" n="HIAT:w" s="T147">kim</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_521" n="HIAT:w" s="T148">hɨrgalaːk</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_524" n="HIAT:w" s="T149">tabata</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_527" n="HIAT:w" s="T150">bu͡olar</ts>
                  <nts id="Seg_528" n="HIAT:ip">.</nts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_531" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_533" n="HIAT:w" s="T151">Onton</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_536" n="HIAT:w" s="T152">dʼaktarɨŋ</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_539" n="HIAT:w" s="T153">emi͡e</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_542" n="HIAT:w" s="T154">dʼaktarɨgar</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_545" n="HIAT:w" s="T155">tuspa</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_548" n="HIAT:w" s="T156">naːda</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_551" n="HIAT:w" s="T157">emi͡e</ts>
                  <nts id="Seg_552" n="HIAT:ip">.</nts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T166" id="Seg_555" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_557" n="HIAT:w" s="T158">Kuččuguj</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_560" n="HIAT:w" s="T159">bolok</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_563" n="HIAT:w" s="T160">baːr</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_566" n="HIAT:w" s="T161">aha</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_569" n="HIAT:w" s="T162">onuga</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_572" n="HIAT:w" s="T163">tü͡ört</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_575" n="HIAT:w" s="T164">tabanɨ</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_578" n="HIAT:w" s="T165">kölüjeller</ts>
                  <nts id="Seg_579" n="HIAT:ip">.</nts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_582" n="HIAT:u" s="T166">
                  <ts e="T167" id="Seg_584" n="HIAT:w" s="T166">Onton</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167.tx.1" id="Seg_587" n="HIAT:w" s="T167">v</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167.tx.2" id="Seg_590" n="HIAT:w" s="T167.tx.1">peredi</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_593" n="HIAT:w" s="T167.tx.2">balka</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_596" n="HIAT:w" s="T170">innige</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_599" n="HIAT:w" s="T171">bu͡o</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_602" n="HIAT:w" s="T172">emi͡e</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_605" n="HIAT:w" s="T173">hɨrga</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_608" n="HIAT:w" s="T174">baːr</ts>
                  <nts id="Seg_609" n="HIAT:ip">.</nts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_612" n="HIAT:u" s="T175">
                  <ts e="T176" id="Seg_614" n="HIAT:w" s="T175">Onno</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_617" n="HIAT:w" s="T176">ikki</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_620" n="HIAT:w" s="T177">tabanɨ</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_623" n="HIAT:w" s="T178">kölüjeller</ts>
                  <nts id="Seg_624" n="HIAT:ip">.</nts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_627" n="HIAT:u" s="T179">
                  <ts e="T180" id="Seg_629" n="HIAT:w" s="T179">Urukku</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_632" n="HIAT:w" s="T180">bu͡ollagɨna</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_635" n="HIAT:w" s="T181">bütej</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_638" n="HIAT:w" s="T182">hetiː</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_641" n="HIAT:w" s="T183">di͡eččiler</ts>
                  <nts id="Seg_642" n="HIAT:ip">.</nts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_645" n="HIAT:u" s="T184">
                  <ts e="T296" id="Seg_647" n="HIAT:w" s="T184">Spetsialno</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_650" n="HIAT:w" s="T296">onno</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_653" n="HIAT:w" s="T185">itte</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_656" n="HIAT:w" s="T186">bütte</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_659" n="HIAT:w" s="T187">taŋastarɨn</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_662" n="HIAT:w" s="T188">kaːlara</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_665" n="HIAT:w" s="T189">bu͡o</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_668" n="HIAT:w" s="T190">bütej</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_671" n="HIAT:w" s="T191">hetiːge</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_674" n="HIAT:w" s="T192">ke</ts>
                  <nts id="Seg_675" n="HIAT:ip">.</nts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_678" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_680" n="HIAT:w" s="T194">Taŋastargɨn</ts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_683" n="HIAT:w" s="T195">üčügej</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_686" n="HIAT:w" s="T196">astargɨn</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_689" n="HIAT:w" s="T197">onno</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_692" n="HIAT:w" s="T198">ugagɨn</ts>
                  <nts id="Seg_693" n="HIAT:ip">.</nts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T309" id="Seg_696" n="HIAT:u" s="T199">
                  <ts e="T200" id="Seg_698" n="HIAT:w" s="T199">Bütej</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_701" n="HIAT:w" s="T200">hetiː</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_704" n="HIAT:w" s="T201">onton</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_707" n="HIAT:w" s="T202">dʼaktarɨn</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_710" n="HIAT:w" s="T203">bejetin</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_713" n="HIAT:w" s="T204">hetiːte</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_716" n="HIAT:w" s="T205">hɨrgata</ts>
                  <nts id="Seg_717" n="HIAT:ip">.</nts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T310" id="Seg_719" n="sc" s="T206">
               <ts e="T213" id="Seg_721" n="HIAT:u" s="T206">
                  <ts e="T206.tx.1" id="Seg_723" n="HIAT:w" s="T206">Smotrja</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_726" n="HIAT:w" s="T206.tx.1">kak</ts>
                  <nts id="Seg_727" n="HIAT:ip">,</nts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_730" n="HIAT:w" s="T207">ɨraːk</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_733" n="HIAT:w" s="T208">köskö</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_736" n="HIAT:w" s="T209">üs</ts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_739" n="HIAT:w" s="T210">tabanɨ</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_742" n="HIAT:w" s="T211">kölüjeller</ts>
                  <nts id="Seg_743" n="HIAT:ip">.</nts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T219" id="Seg_746" n="HIAT:u" s="T213">
                  <ts e="T298" id="Seg_748" n="HIAT:w" s="T213">A</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_751" n="HIAT:w" s="T298">čugas</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_754" n="HIAT:w" s="T214">köskö</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_757" n="HIAT:w" s="T215">ikki</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_760" n="HIAT:w" s="T216">tabanɨ</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_763" n="HIAT:w" s="T217">kölüjeller</ts>
                  <nts id="Seg_764" n="HIAT:ip">.</nts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T228" id="Seg_767" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_769" n="HIAT:w" s="T219">Onton</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_772" n="HIAT:w" s="T220">ol</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_775" n="HIAT:w" s="T221">dʼaktarɨŋ</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_778" n="HIAT:w" s="T222">bu͡ologun</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_781" n="HIAT:w" s="T223">kennitten</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_784" n="HIAT:w" s="T224">bu͡o</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_787" n="HIAT:w" s="T225">güːle</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_790" n="HIAT:w" s="T226">bolok</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_793" n="HIAT:w" s="T227">baːr</ts>
                  <nts id="Seg_794" n="HIAT:ip">.</nts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T231" id="Seg_797" n="HIAT:u" s="T228">
                  <ts e="T228.tx.1" id="Seg_799" n="HIAT:w" s="T228">Vot</ts>
                  <nts id="Seg_800" n="HIAT:ip">,</nts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228.tx.2" id="Seg_803" n="HIAT:w" s="T228.tx.1">malenkij</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_806" n="HIAT:w" s="T228.tx.2">bolok</ts>
                  <nts id="Seg_807" n="HIAT:ip">.</nts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_810" n="HIAT:u" s="T231">
                  <ts e="T232" id="Seg_812" n="HIAT:w" s="T231">Kimneːčči</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_815" n="HIAT:w" s="T232">kepteri</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_818" n="HIAT:w" s="T233">güːle</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_821" n="HIAT:w" s="T234">di͡eččibit</ts>
                  <nts id="Seg_822" n="HIAT:ip">.</nts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T239" id="Seg_825" n="HIAT:u" s="T235">
                  <ts e="T236" id="Seg_827" n="HIAT:w" s="T235">Onno</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_830" n="HIAT:w" s="T236">uže</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_833" n="HIAT:w" s="T237">dva</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_836" n="HIAT:w" s="T238">olenja</ts>
                  <nts id="Seg_837" n="HIAT:ip">.</nts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T253" id="Seg_840" n="HIAT:u" s="T239">
                  <ts e="T240" id="Seg_842" n="HIAT:w" s="T239">Onton</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_845" n="HIAT:w" s="T240">ol</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_848" n="HIAT:w" s="T241">güːle</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_851" n="HIAT:w" s="T242">kenniten</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_854" n="HIAT:w" s="T243">tam</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_857" n="HIAT:w" s="T244">vsjakie</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_860" n="HIAT:w" s="T245">itte</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_863" n="HIAT:w" s="T246">bütte</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_866" n="HIAT:w" s="T247">ke</ts>
                  <nts id="Seg_867" n="HIAT:ip">,</nts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_870" n="HIAT:w" s="T248">araŋahɨ</ts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_873" n="HIAT:w" s="T249">možno</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_876" n="HIAT:w" s="T250">tartaragɨn</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_879" n="HIAT:w" s="T251">baːjagɨn</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_882" n="HIAT:w" s="T252">bu͡o</ts>
                  <nts id="Seg_883" n="HIAT:ip">.</nts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T310" id="Seg_886" n="HIAT:u" s="T253">
                  <ts e="T254" id="Seg_888" n="HIAT:w" s="T253">Ɨttargɨn</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_891" n="HIAT:w" s="T254">baːjagɨn</ts>
                  <nts id="Seg_892" n="HIAT:ip">.</nts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T301" id="Seg_894" n="sc" s="T255">
               <ts e="T301" id="Seg_896" n="HIAT:u" s="T255">
                  <ts e="T256" id="Seg_898" n="HIAT:w" s="T255">Onton</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_901" n="HIAT:w" s="T256">köhö</ts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_904" n="HIAT:w" s="T257">turagɨn</ts>
                  <nts id="Seg_905" n="HIAT:ip">.</nts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T302" id="Seg_907" n="sc" s="T258">
               <ts e="T262" id="Seg_909" n="HIAT:u" s="T258">
                  <ts e="T259" id="Seg_911" n="HIAT:w" s="T258">Ulakan</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_914" n="HIAT:w" s="T259">argiš</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_917" n="HIAT:w" s="T260">bu͡olla</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_919" n="HIAT:ip">(</nts>
                  <ts e="T262" id="Seg_921" n="HIAT:w" s="T261">onton</ts>
                  <nts id="Seg_922" n="HIAT:ip">)</nts>
                  <nts id="Seg_923" n="HIAT:ip">.</nts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T302" id="Seg_926" n="HIAT:u" s="T262">
                  <ts e="T262.tx.1" id="Seg_928" n="HIAT:w" s="T262">Osobenno</ts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_931" n="HIAT:w" s="T262.tx.1">kogda</ts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_934" n="HIAT:w" s="T263">elbek</ts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_937" n="HIAT:w" s="T264">kihi</ts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_940" n="HIAT:w" s="T265">ke</ts>
                  <nts id="Seg_941" n="HIAT:ip">.</nts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T303" id="Seg_943" n="sc" s="T267">
               <ts e="T303" id="Seg_945" n="HIAT:u" s="T267">
                  <ts e="T268" id="Seg_947" n="HIAT:w" s="T267">Üs</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_950" n="HIAT:w" s="T268">itte</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_953" n="HIAT:w" s="T269">ɨ͡al</ts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_956" n="HIAT:w" s="T270">bu͡ollagɨna</ts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_959" n="HIAT:w" s="T271">ke</ts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_962" n="HIAT:w" s="T272">uhun</ts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_965" n="HIAT:w" s="T273">bagajɨ</ts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_968" n="HIAT:w" s="T274">bu͡olaːččɨ</ts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_971" n="HIAT:w" s="T275">ontuŋ</ts>
                  <nts id="Seg_972" n="HIAT:ip">.</nts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T304" id="Seg_974" n="sc" s="T276">
               <ts e="T304" id="Seg_976" n="HIAT:u" s="T276">
                  <ts e="T277" id="Seg_978" n="HIAT:w" s="T276">Onton</ts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_981" n="HIAT:w" s="T277">biːr</ts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_984" n="HIAT:w" s="T278">kihiŋ</ts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_987" n="HIAT:w" s="T279">ü͡ör</ts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_990" n="HIAT:w" s="T280">ü͡örü</ts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_993" n="HIAT:w" s="T281">üːrü͡öteːk</ts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_996" n="HIAT:w" s="T282">bu͡o</ts>
                  <nts id="Seg_997" n="HIAT:ip">,</nts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1000" n="HIAT:w" s="T283">ü͡örü</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1003" n="HIAT:w" s="T284">üːrer</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1006" n="HIAT:w" s="T285">biːr</ts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1009" n="HIAT:w" s="T286">kihi</ts>
                  <nts id="Seg_1010" n="HIAT:ip">.</nts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T294" id="Seg_1012" n="sc" s="T287">
               <ts e="T290" id="Seg_1014" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_1016" n="HIAT:w" s="T287">Ü͡örü</ts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1019" n="HIAT:w" s="T288">üːrer</ts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1022" n="HIAT:w" s="T289">ontuŋ</ts>
                  <nts id="Seg_1023" n="HIAT:ip">.</nts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T294" id="Seg_1026" n="HIAT:u" s="T290">
                  <ts e="T291" id="Seg_1028" n="HIAT:w" s="T290">Bu</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1031" n="HIAT:w" s="T291">aːta</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1034" n="HIAT:w" s="T292">ulakan</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1037" n="HIAT:w" s="T293">argiš</ts>
                  <nts id="Seg_1038" n="HIAT:ip">.</nts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T305" id="Seg_1040" n="sc" s="T0">
               <ts e="T1" id="Seg_1042" n="e" s="T0">Köhöbüt </ts>
               <ts e="T2" id="Seg_1044" n="e" s="T1">oččogo </ts>
               <ts e="T3" id="Seg_1046" n="e" s="T2">ittebitin </ts>
               <ts e="T4" id="Seg_1048" n="e" s="T3">barɨkaːn </ts>
               <ts e="T305" id="Seg_1050" n="e" s="T4">terinebit. </ts>
            </ts>
            <ts e="T307" id="Seg_1051" n="sc" s="T5">
               <ts e="T6" id="Seg_1053" n="e" s="T5">Heee </ts>
               <ts e="T7" id="Seg_1055" n="e" s="T6">dʼi͡ebitin </ts>
               <ts e="T8" id="Seg_1057" n="e" s="T7">kahɨnabɨt. </ts>
               <ts e="T9" id="Seg_1059" n="e" s="T8">Kömüleːk </ts>
               <ts e="T10" id="Seg_1061" n="e" s="T9">že </ts>
               <ts e="T11" id="Seg_1063" n="e" s="T10">dʼi͡ebit </ts>
               <ts e="T12" id="Seg_1065" n="e" s="T11">bu͡olokput </ts>
               <ts e="T13" id="Seg_1067" n="e" s="T12">komulaːk </ts>
               <ts e="T14" id="Seg_1069" n="e" s="T13">bu͡o. </ts>
               <ts e="T15" id="Seg_1071" n="e" s="T14">Dʼi͡elerin </ts>
               <ts e="T16" id="Seg_1073" n="e" s="T15">kömüleːk </ts>
               <ts e="T17" id="Seg_1075" n="e" s="T16">kahɨnɨ͡aktarɨn </ts>
               <ts e="T307" id="Seg_1077" n="e" s="T17">naːda. </ts>
            </ts>
            <ts e="T306" id="Seg_1078" n="sc" s="T18">
               <ts e="T19" id="Seg_1080" n="e" s="T18">Onton </ts>
               <ts e="T20" id="Seg_1082" n="e" s="T19">dʼaktattar </ts>
               <ts e="T21" id="Seg_1084" n="e" s="T20">bu͡ollagɨna </ts>
               <ts e="T22" id="Seg_1086" n="e" s="T21">dʼi͡e </ts>
               <ts e="T23" id="Seg_1088" n="e" s="T22">ihin </ts>
               <ts e="T306" id="Seg_1090" n="e" s="T23">ubirajdɨːllar. </ts>
            </ts>
            <ts e="T308" id="Seg_1091" n="sc" s="T24">
               <ts e="T25" id="Seg_1093" n="e" s="T24">Bu </ts>
               <ts e="T26" id="Seg_1095" n="e" s="T25">ka </ts>
               <ts e="T33" id="Seg_1097" n="e" s="T26">eto, to što posuda, vse eto ubiraetsja, </ts>
               <ts e="T34" id="Seg_1099" n="e" s="T33">barɨkaːnnara. </ts>
               <ts e="T35" id="Seg_1101" n="e" s="T34">Onton </ts>
               <ts e="T36" id="Seg_1103" n="e" s="T35">ü͡örü </ts>
               <ts e="T308" id="Seg_1105" n="e" s="T36">egeleller. </ts>
            </ts>
            <ts e="T309" id="Seg_1106" n="sc" s="T37">
               <ts e="T38" id="Seg_1108" n="e" s="T37">Stado </ts>
               <ts e="T39" id="Seg_1110" n="e" s="T38">privozjat. </ts>
               <ts e="T40" id="Seg_1112" n="e" s="T39">Onno </ts>
               <ts e="T41" id="Seg_1114" n="e" s="T40">taba </ts>
               <ts e="T42" id="Seg_1116" n="e" s="T41">tuttallar. </ts>
               <ts e="T43" id="Seg_1118" n="e" s="T42">Taba </ts>
               <ts e="T44" id="Seg_1120" n="e" s="T43">tuttabɨt </ts>
               <ts e="T45" id="Seg_1122" n="e" s="T44">bu͡o </ts>
               <ts e="T46" id="Seg_1124" n="e" s="T45">oččogo </ts>
               <ts e="T47" id="Seg_1126" n="e" s="T46">bu͡ollagɨna </ts>
               <ts e="T48" id="Seg_1128" n="e" s="T47">bolokputugar </ts>
               <ts e="T49" id="Seg_1130" n="e" s="T48">agɨs </ts>
               <ts e="T50" id="Seg_1132" n="e" s="T49">tabanɨ </ts>
               <ts e="T51" id="Seg_1134" n="e" s="T50">kölüjebit. </ts>
               <ts e="T52" id="Seg_1136" n="e" s="T51">Agɨs </ts>
               <ts e="T53" id="Seg_1138" n="e" s="T52">tabanɨ </ts>
               <ts e="T54" id="Seg_1140" n="e" s="T53">tutallar, </ts>
               <ts e="T55" id="Seg_1142" n="e" s="T54">ulakan </ts>
               <ts e="T56" id="Seg_1144" n="e" s="T55">tabalar </ts>
               <ts e="T57" id="Seg_1146" n="e" s="T56">naːda </ts>
               <ts e="T58" id="Seg_1148" n="e" s="T57">tutu͡okka </ts>
               <ts e="T59" id="Seg_1150" n="e" s="T58">bolokko </ts>
               <ts e="T60" id="Seg_1152" n="e" s="T59">ke. </ts>
               <ts e="T61" id="Seg_1154" n="e" s="T60">Onton </ts>
               <ts e="T62" id="Seg_1156" n="e" s="T61">bolok </ts>
               <ts e="T63" id="Seg_1158" n="e" s="T62">innitiger </ts>
               <ts e="T64" id="Seg_1160" n="e" s="T63">bu͡ollagɨna </ts>
               <ts e="T65" id="Seg_1162" n="e" s="T64">bu͡o </ts>
               <ts e="T66" id="Seg_1164" n="e" s="T65">össü͡ö </ts>
               <ts e="T67" id="Seg_1166" n="e" s="T66">da </ts>
               <ts e="T68" id="Seg_1168" n="e" s="T67">hetiː </ts>
               <ts e="T69" id="Seg_1170" n="e" s="T68">baːr. </ts>
               <ts e="T70" id="Seg_1172" n="e" s="T69">Ol </ts>
               <ts e="T71" id="Seg_1174" n="e" s="T70">hetiː </ts>
               <ts e="T72" id="Seg_1176" n="e" s="T71">bu͡o </ts>
               <ts e="T73" id="Seg_1178" n="e" s="T72">itteni </ts>
               <ts e="T74" id="Seg_1180" n="e" s="T73">barɨtɨn </ts>
               <ts e="T75" id="Seg_1182" n="e" s="T74">ti͡ejeller </ts>
               <ts e="T76" id="Seg_1184" n="e" s="T75">bu </ts>
               <ts e="T77" id="Seg_1186" n="e" s="T76">ka </ts>
               <ts e="T78" id="Seg_1188" n="e" s="T77">gruzittɨːllar. </ts>
               <ts e="T79" id="Seg_1190" n="e" s="T78">Mastarɨ </ts>
               <ts e="T80" id="Seg_1192" n="e" s="T79">bu͡ol </ts>
               <ts e="T81" id="Seg_1194" n="e" s="T80">bu͡očkalarɨ </ts>
               <ts e="T82" id="Seg_1196" n="e" s="T81">bu͡ol </ts>
               <ts e="T83" id="Seg_1198" n="e" s="T82">možno </ts>
               <ts e="T84" id="Seg_1200" n="e" s="T83">ugoli </ts>
               <ts e="T85" id="Seg_1202" n="e" s="T84">itte </ts>
               <ts e="T86" id="Seg_1204" n="e" s="T85">uːrallar </ts>
               <ts e="T87" id="Seg_1206" n="e" s="T86">bu͡o. </ts>
               <ts e="T88" id="Seg_1208" n="e" s="T87">Itte </ts>
               <ts e="T89" id="Seg_1210" n="e" s="T88">itte </ts>
               <ts e="T90" id="Seg_1212" n="e" s="T89">tu͡ok </ts>
               <ts e="T91" id="Seg_1214" n="e" s="T90">kaːlbɨt </ts>
               <ts e="T92" id="Seg_1216" n="e" s="T91">kimi </ts>
               <ts e="T93" id="Seg_1218" n="e" s="T92">barɨkaːn </ts>
               <ts e="T94" id="Seg_1220" n="e" s="T93">onno </ts>
               <ts e="T95" id="Seg_1222" n="e" s="T94">ti͡ejeller. </ts>
               <ts e="T96" id="Seg_1224" n="e" s="T95">Onno </ts>
               <ts e="T97" id="Seg_1226" n="e" s="T96">tü͡ört </ts>
               <ts e="T98" id="Seg_1228" n="e" s="T97">tabanɨ </ts>
               <ts e="T99" id="Seg_1230" n="e" s="T98">kölüjeller, </ts>
               <ts e="T100" id="Seg_1232" n="e" s="T99">tü͡ört </ts>
               <ts e="T101" id="Seg_1234" n="e" s="T100">tabanɨ </ts>
               <ts e="T102" id="Seg_1236" n="e" s="T101">kölüjeller. </ts>
               <ts e="T103" id="Seg_1238" n="e" s="T102">Onton </ts>
               <ts e="T104" id="Seg_1240" n="e" s="T103">inniki </ts>
               <ts e="T105" id="Seg_1242" n="e" s="T104">hɨrga </ts>
               <ts e="T106" id="Seg_1244" n="e" s="T105">bu͡ollagɨna </ts>
               <ts e="T107" id="Seg_1246" n="e" s="T106">uže </ts>
               <ts e="T108" id="Seg_1248" n="e" s="T107">hozjainɨŋ </ts>
               <ts e="T109" id="Seg_1250" n="e" s="T108">bejetin </ts>
               <ts e="T110" id="Seg_1252" n="e" s="T109">hɨrgata. </ts>
               <ts e="T111" id="Seg_1254" n="e" s="T110">Onno </ts>
               <ts e="T112" id="Seg_1256" n="e" s="T111">otto </ts>
               <ts e="T113" id="Seg_1258" n="e" s="T112">emi͡e </ts>
               <ts e="T114" id="Seg_1260" n="e" s="T113">be </ts>
               <ts e="T115" id="Seg_1262" n="e" s="T114">ugol </ts>
               <ts e="T116" id="Seg_1264" n="e" s="T115">možno </ts>
               <ts e="T117" id="Seg_1266" n="e" s="T116">uːru͡okkun </ts>
               <ts e="T118" id="Seg_1268" n="e" s="T117">bu͡o. </ts>
               <ts e="T295" id="Seg_1270" n="e" s="T118">I </ts>
               <ts e="T119" id="Seg_1272" n="e" s="T295">onno </ts>
               <ts e="T120" id="Seg_1274" n="e" s="T119">nipte </ts>
               <ts e="T121" id="Seg_1276" n="e" s="T120">di͡eččibit, </ts>
               <ts e="T122" id="Seg_1278" n="e" s="T121">mas </ts>
               <ts e="T123" id="Seg_1280" n="e" s="T122">abɨrattaːččɨbɨt, </ts>
               <ts e="T128" id="Seg_1282" n="e" s="T123">palka takaja drova rubit. </ts>
               <ts e="T129" id="Seg_1284" n="e" s="T128">Onno </ts>
               <ts e="T130" id="Seg_1286" n="e" s="T129">nipteler </ts>
               <ts e="T131" id="Seg_1288" n="e" s="T130">nipte </ts>
               <ts e="T132" id="Seg_1290" n="e" s="T131">nipte. </ts>
               <ts e="T133" id="Seg_1292" n="e" s="T132">De </ts>
               <ts e="T134" id="Seg_1294" n="e" s="T133">onu </ts>
               <ts e="T135" id="Seg_1296" n="e" s="T134">onu </ts>
               <ts e="T136" id="Seg_1298" n="e" s="T135">itte </ts>
               <ts e="T138" id="Seg_1300" n="e" s="T136">(uːr-) </ts>
               <ts e="T139" id="Seg_1302" n="e" s="T138">ti͡ejeller </ts>
               <ts e="T140" id="Seg_1304" n="e" s="T139">hɨrgaga </ts>
               <ts e="T141" id="Seg_1306" n="e" s="T140">ke. </ts>
               <ts e="T142" id="Seg_1308" n="e" s="T141">De </ts>
               <ts e="T143" id="Seg_1310" n="e" s="T142">onton </ts>
               <ts e="T144" id="Seg_1312" n="e" s="T143">iti </ts>
               <ts e="T145" id="Seg_1314" n="e" s="T144">hozjain </ts>
               <ts e="T146" id="Seg_1316" n="e" s="T145">ke </ts>
               <ts e="T147" id="Seg_1318" n="e" s="T146">kim </ts>
               <ts e="T148" id="Seg_1320" n="e" s="T147">kim </ts>
               <ts e="T149" id="Seg_1322" n="e" s="T148">hɨrgalaːk </ts>
               <ts e="T150" id="Seg_1324" n="e" s="T149">tabata </ts>
               <ts e="T151" id="Seg_1326" n="e" s="T150">bu͡olar. </ts>
               <ts e="T152" id="Seg_1328" n="e" s="T151">Onton </ts>
               <ts e="T153" id="Seg_1330" n="e" s="T152">dʼaktarɨŋ </ts>
               <ts e="T154" id="Seg_1332" n="e" s="T153">emi͡e </ts>
               <ts e="T155" id="Seg_1334" n="e" s="T154">dʼaktarɨgar </ts>
               <ts e="T156" id="Seg_1336" n="e" s="T155">tuspa </ts>
               <ts e="T157" id="Seg_1338" n="e" s="T156">naːda </ts>
               <ts e="T158" id="Seg_1340" n="e" s="T157">emi͡e. </ts>
               <ts e="T159" id="Seg_1342" n="e" s="T158">Kuččuguj </ts>
               <ts e="T160" id="Seg_1344" n="e" s="T159">bolok </ts>
               <ts e="T161" id="Seg_1346" n="e" s="T160">baːr </ts>
               <ts e="T162" id="Seg_1348" n="e" s="T161">aha </ts>
               <ts e="T163" id="Seg_1350" n="e" s="T162">onuga </ts>
               <ts e="T164" id="Seg_1352" n="e" s="T163">tü͡ört </ts>
               <ts e="T165" id="Seg_1354" n="e" s="T164">tabanɨ </ts>
               <ts e="T166" id="Seg_1356" n="e" s="T165">kölüjeller. </ts>
               <ts e="T167" id="Seg_1358" n="e" s="T166">Onton </ts>
               <ts e="T170" id="Seg_1360" n="e" s="T167">v peredi balka </ts>
               <ts e="T171" id="Seg_1362" n="e" s="T170">innige </ts>
               <ts e="T172" id="Seg_1364" n="e" s="T171">bu͡o </ts>
               <ts e="T173" id="Seg_1366" n="e" s="T172">emi͡e </ts>
               <ts e="T174" id="Seg_1368" n="e" s="T173">hɨrga </ts>
               <ts e="T175" id="Seg_1370" n="e" s="T174">baːr. </ts>
               <ts e="T176" id="Seg_1372" n="e" s="T175">Onno </ts>
               <ts e="T177" id="Seg_1374" n="e" s="T176">ikki </ts>
               <ts e="T178" id="Seg_1376" n="e" s="T177">tabanɨ </ts>
               <ts e="T179" id="Seg_1378" n="e" s="T178">kölüjeller. </ts>
               <ts e="T180" id="Seg_1380" n="e" s="T179">Urukku </ts>
               <ts e="T181" id="Seg_1382" n="e" s="T180">bu͡ollagɨna </ts>
               <ts e="T182" id="Seg_1384" n="e" s="T181">bütej </ts>
               <ts e="T183" id="Seg_1386" n="e" s="T182">hetiː </ts>
               <ts e="T184" id="Seg_1388" n="e" s="T183">di͡eččiler. </ts>
               <ts e="T296" id="Seg_1390" n="e" s="T184">Spetsialno </ts>
               <ts e="T185" id="Seg_1392" n="e" s="T296">onno </ts>
               <ts e="T186" id="Seg_1394" n="e" s="T185">itte </ts>
               <ts e="T187" id="Seg_1396" n="e" s="T186">bütte </ts>
               <ts e="T188" id="Seg_1398" n="e" s="T187">taŋastarɨn </ts>
               <ts e="T189" id="Seg_1400" n="e" s="T188">kaːlara </ts>
               <ts e="T190" id="Seg_1402" n="e" s="T189">bu͡o </ts>
               <ts e="T191" id="Seg_1404" n="e" s="T190">bütej </ts>
               <ts e="T192" id="Seg_1406" n="e" s="T191">hetiːge </ts>
               <ts e="T194" id="Seg_1408" n="e" s="T192">ke. </ts>
               <ts e="T195" id="Seg_1410" n="e" s="T194">Taŋastargɨn </ts>
               <ts e="T196" id="Seg_1412" n="e" s="T195">üčügej </ts>
               <ts e="T197" id="Seg_1414" n="e" s="T196">astargɨn </ts>
               <ts e="T198" id="Seg_1416" n="e" s="T197">onno </ts>
               <ts e="T199" id="Seg_1418" n="e" s="T198">ugagɨn. </ts>
               <ts e="T200" id="Seg_1420" n="e" s="T199">Bütej </ts>
               <ts e="T201" id="Seg_1422" n="e" s="T200">hetiː </ts>
               <ts e="T202" id="Seg_1424" n="e" s="T201">onton </ts>
               <ts e="T203" id="Seg_1426" n="e" s="T202">dʼaktarɨn </ts>
               <ts e="T204" id="Seg_1428" n="e" s="T203">bejetin </ts>
               <ts e="T205" id="Seg_1430" n="e" s="T204">hetiːte </ts>
               <ts e="T309" id="Seg_1432" n="e" s="T205">hɨrgata. </ts>
            </ts>
            <ts e="T310" id="Seg_1433" n="sc" s="T206">
               <ts e="T207" id="Seg_1435" n="e" s="T206">Smotrja kak, </ts>
               <ts e="T208" id="Seg_1437" n="e" s="T207">ɨraːk </ts>
               <ts e="T209" id="Seg_1439" n="e" s="T208">köskö </ts>
               <ts e="T210" id="Seg_1441" n="e" s="T209">üs </ts>
               <ts e="T211" id="Seg_1443" n="e" s="T210">tabanɨ </ts>
               <ts e="T213" id="Seg_1445" n="e" s="T211">kölüjeller. </ts>
               <ts e="T298" id="Seg_1447" n="e" s="T213">A </ts>
               <ts e="T214" id="Seg_1449" n="e" s="T298">čugas </ts>
               <ts e="T215" id="Seg_1451" n="e" s="T214">köskö </ts>
               <ts e="T216" id="Seg_1453" n="e" s="T215">ikki </ts>
               <ts e="T217" id="Seg_1455" n="e" s="T216">tabanɨ </ts>
               <ts e="T219" id="Seg_1457" n="e" s="T217">kölüjeller. </ts>
               <ts e="T220" id="Seg_1459" n="e" s="T219">Onton </ts>
               <ts e="T221" id="Seg_1461" n="e" s="T220">ol </ts>
               <ts e="T222" id="Seg_1463" n="e" s="T221">dʼaktarɨŋ </ts>
               <ts e="T223" id="Seg_1465" n="e" s="T222">bu͡ologun </ts>
               <ts e="T224" id="Seg_1467" n="e" s="T223">kennitten </ts>
               <ts e="T225" id="Seg_1469" n="e" s="T224">bu͡o </ts>
               <ts e="T226" id="Seg_1471" n="e" s="T225">güːle </ts>
               <ts e="T227" id="Seg_1473" n="e" s="T226">bolok </ts>
               <ts e="T228" id="Seg_1475" n="e" s="T227">baːr. </ts>
               <ts e="T231" id="Seg_1477" n="e" s="T228">Vot, malenkij bolok. </ts>
               <ts e="T232" id="Seg_1479" n="e" s="T231">Kimneːčči </ts>
               <ts e="T233" id="Seg_1481" n="e" s="T232">kepteri </ts>
               <ts e="T234" id="Seg_1483" n="e" s="T233">güːle </ts>
               <ts e="T235" id="Seg_1485" n="e" s="T234">di͡eččibit. </ts>
               <ts e="T236" id="Seg_1487" n="e" s="T235">Onno </ts>
               <ts e="T237" id="Seg_1489" n="e" s="T236">uže </ts>
               <ts e="T238" id="Seg_1491" n="e" s="T237">dva </ts>
               <ts e="T239" id="Seg_1493" n="e" s="T238">olenja. </ts>
               <ts e="T240" id="Seg_1495" n="e" s="T239">Onton </ts>
               <ts e="T241" id="Seg_1497" n="e" s="T240">ol </ts>
               <ts e="T242" id="Seg_1499" n="e" s="T241">güːle </ts>
               <ts e="T243" id="Seg_1501" n="e" s="T242">kenniten </ts>
               <ts e="T244" id="Seg_1503" n="e" s="T243">tam </ts>
               <ts e="T245" id="Seg_1505" n="e" s="T244">vsjakie </ts>
               <ts e="T246" id="Seg_1507" n="e" s="T245">itte </ts>
               <ts e="T247" id="Seg_1509" n="e" s="T246">bütte </ts>
               <ts e="T248" id="Seg_1511" n="e" s="T247">ke, </ts>
               <ts e="T249" id="Seg_1513" n="e" s="T248">araŋahɨ </ts>
               <ts e="T250" id="Seg_1515" n="e" s="T249">možno </ts>
               <ts e="T251" id="Seg_1517" n="e" s="T250">tartaragɨn </ts>
               <ts e="T252" id="Seg_1519" n="e" s="T251">baːjagɨn </ts>
               <ts e="T253" id="Seg_1521" n="e" s="T252">bu͡o. </ts>
               <ts e="T254" id="Seg_1523" n="e" s="T253">Ɨttargɨn </ts>
               <ts e="T310" id="Seg_1525" n="e" s="T254">baːjagɨn. </ts>
            </ts>
            <ts e="T301" id="Seg_1526" n="sc" s="T255">
               <ts e="T256" id="Seg_1528" n="e" s="T255">Onton </ts>
               <ts e="T257" id="Seg_1530" n="e" s="T256">köhö </ts>
               <ts e="T301" id="Seg_1532" n="e" s="T257">turagɨn. </ts>
            </ts>
            <ts e="T302" id="Seg_1533" n="sc" s="T258">
               <ts e="T259" id="Seg_1535" n="e" s="T258">Ulakan </ts>
               <ts e="T260" id="Seg_1537" n="e" s="T259">argiš </ts>
               <ts e="T261" id="Seg_1539" n="e" s="T260">bu͡olla </ts>
               <ts e="T262" id="Seg_1541" n="e" s="T261">(onton). </ts>
               <ts e="T263" id="Seg_1543" n="e" s="T262">Osobenno kogda </ts>
               <ts e="T264" id="Seg_1545" n="e" s="T263">elbek </ts>
               <ts e="T265" id="Seg_1547" n="e" s="T264">kihi </ts>
               <ts e="T302" id="Seg_1549" n="e" s="T265">ke. </ts>
            </ts>
            <ts e="T303" id="Seg_1550" n="sc" s="T267">
               <ts e="T268" id="Seg_1552" n="e" s="T267">Üs </ts>
               <ts e="T269" id="Seg_1554" n="e" s="T268">itte </ts>
               <ts e="T270" id="Seg_1556" n="e" s="T269">ɨ͡al </ts>
               <ts e="T271" id="Seg_1558" n="e" s="T270">bu͡ollagɨna </ts>
               <ts e="T272" id="Seg_1560" n="e" s="T271">ke </ts>
               <ts e="T273" id="Seg_1562" n="e" s="T272">uhun </ts>
               <ts e="T274" id="Seg_1564" n="e" s="T273">bagajɨ </ts>
               <ts e="T275" id="Seg_1566" n="e" s="T274">bu͡olaːččɨ </ts>
               <ts e="T303" id="Seg_1568" n="e" s="T275">ontuŋ. </ts>
            </ts>
            <ts e="T304" id="Seg_1569" n="sc" s="T276">
               <ts e="T277" id="Seg_1571" n="e" s="T276">Onton </ts>
               <ts e="T278" id="Seg_1573" n="e" s="T277">biːr </ts>
               <ts e="T279" id="Seg_1575" n="e" s="T278">kihiŋ </ts>
               <ts e="T280" id="Seg_1577" n="e" s="T279">ü͡ör </ts>
               <ts e="T281" id="Seg_1579" n="e" s="T280">ü͡örü </ts>
               <ts e="T282" id="Seg_1581" n="e" s="T281">üːrü͡öteːk </ts>
               <ts e="T283" id="Seg_1583" n="e" s="T282">bu͡o, </ts>
               <ts e="T284" id="Seg_1585" n="e" s="T283">ü͡örü </ts>
               <ts e="T285" id="Seg_1587" n="e" s="T284">üːrer </ts>
               <ts e="T286" id="Seg_1589" n="e" s="T285">biːr </ts>
               <ts e="T304" id="Seg_1591" n="e" s="T286">kihi. </ts>
            </ts>
            <ts e="T294" id="Seg_1592" n="sc" s="T287">
               <ts e="T288" id="Seg_1594" n="e" s="T287">Ü͡örü </ts>
               <ts e="T289" id="Seg_1596" n="e" s="T288">üːrer </ts>
               <ts e="T290" id="Seg_1598" n="e" s="T289">ontuŋ. </ts>
               <ts e="T291" id="Seg_1600" n="e" s="T290">Bu </ts>
               <ts e="T292" id="Seg_1602" n="e" s="T291">aːta </ts>
               <ts e="T293" id="Seg_1604" n="e" s="T292">ulakan </ts>
               <ts e="T294" id="Seg_1606" n="e" s="T293">argiš. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T305" id="Seg_1607" s="T0">AnIM_2009_Argish_nar.001 (001)</ta>
            <ta e="T8" id="Seg_1608" s="T5">AnIM_2009_Argish_nar.002 (002)</ta>
            <ta e="T14" id="Seg_1609" s="T8">AnIM_2009_Argish_nar.003 (003)</ta>
            <ta e="T307" id="Seg_1610" s="T14">AnIM_2009_Argish_nar.004 (004)</ta>
            <ta e="T306" id="Seg_1611" s="T18">AnIM_2009_Argish_nar.005 (005)</ta>
            <ta e="T34" id="Seg_1612" s="T24">AnIM_2009_Argish_nar.006 (006)</ta>
            <ta e="T308" id="Seg_1613" s="T34">AnIM_2009_Argish_nar.007 (007)</ta>
            <ta e="T39" id="Seg_1614" s="T37">AnIM_2009_Argish_nar.008 (008)</ta>
            <ta e="T42" id="Seg_1615" s="T39">AnIM_2009_Argish_nar.009 (009)</ta>
            <ta e="T51" id="Seg_1616" s="T42">AnIM_2009_Argish_nar.010 (010)</ta>
            <ta e="T60" id="Seg_1617" s="T51">AnIM_2009_Argish_nar.011 (011)</ta>
            <ta e="T69" id="Seg_1618" s="T60">AnIM_2009_Argish_nar.012 (012)</ta>
            <ta e="T78" id="Seg_1619" s="T69">AnIM_2009_Argish_nar.013 (013)</ta>
            <ta e="T87" id="Seg_1620" s="T78">AnIM_2009_Argish_nar.014 (014)</ta>
            <ta e="T95" id="Seg_1621" s="T87">AnIM_2009_Argish_nar.015 (015)</ta>
            <ta e="T102" id="Seg_1622" s="T95">AnIM_2009_Argish_nar.016 (016)</ta>
            <ta e="T110" id="Seg_1623" s="T102">AnIM_2009_Argish_nar.017 (017)</ta>
            <ta e="T118" id="Seg_1624" s="T110">AnIM_2009_Argish_nar.018 (018)</ta>
            <ta e="T128" id="Seg_1625" s="T118">AnIM_2009_Argish_nar.019 (019)</ta>
            <ta e="T132" id="Seg_1626" s="T128">AnIM_2009_Argish_nar.020 (020)</ta>
            <ta e="T141" id="Seg_1627" s="T132">AnIM_2009_Argish_nar.021 (021)</ta>
            <ta e="T151" id="Seg_1628" s="T141">AnIM_2009_Argish_nar.022 (022)</ta>
            <ta e="T158" id="Seg_1629" s="T151">AnIM_2009_Argish_nar.023 (023)</ta>
            <ta e="T166" id="Seg_1630" s="T158">AnIM_2009_Argish_nar.024 (024)</ta>
            <ta e="T175" id="Seg_1631" s="T166">AnIM_2009_Argish_nar.025 (025)</ta>
            <ta e="T179" id="Seg_1632" s="T175">AnIM_2009_Argish_nar.026 (026)</ta>
            <ta e="T184" id="Seg_1633" s="T179">AnIM_2009_Argish_nar.027 (027)</ta>
            <ta e="T194" id="Seg_1634" s="T184">AnIM_2009_Argish_nar.028 (028)</ta>
            <ta e="T199" id="Seg_1635" s="T194">AnIM_2009_Argish_nar.029 (029)</ta>
            <ta e="T309" id="Seg_1636" s="T199">AnIM_2009_Argish_nar.030 (030)</ta>
            <ta e="T213" id="Seg_1637" s="T206">AnIM_2009_Argish_nar.031 (031)</ta>
            <ta e="T219" id="Seg_1638" s="T213">AnIM_2009_Argish_nar.032 (032)</ta>
            <ta e="T228" id="Seg_1639" s="T219">AnIM_2009_Argish_nar.033 (033)</ta>
            <ta e="T231" id="Seg_1640" s="T228">AnIM_2009_Argish_nar.034 (034)</ta>
            <ta e="T235" id="Seg_1641" s="T231">AnIM_2009_Argish_nar.035 (035)</ta>
            <ta e="T239" id="Seg_1642" s="T235">AnIM_2009_Argish_nar.036 (036)</ta>
            <ta e="T253" id="Seg_1643" s="T239">AnIM_2009_Argish_nar.037 (037)</ta>
            <ta e="T310" id="Seg_1644" s="T253">AnIM_2009_Argish_nar.038 (038)</ta>
            <ta e="T301" id="Seg_1645" s="T255">AnIM_2009_Argish_nar.039 (039)</ta>
            <ta e="T262" id="Seg_1646" s="T258">AnIM_2009_Argish_nar.040 (040)</ta>
            <ta e="T302" id="Seg_1647" s="T262">AnIM_2009_Argish_nar.041 (041)</ta>
            <ta e="T303" id="Seg_1648" s="T267">AnIM_2009_Argish_nar.042 (042)</ta>
            <ta e="T304" id="Seg_1649" s="T276">AnIM_2009_Argish_nar.043 (043)</ta>
            <ta e="T290" id="Seg_1650" s="T287">AnIM_2009_Argish_nar.044 (044)</ta>
            <ta e="T294" id="Seg_1651" s="T290">AnIM_2009_Argish_nar.045 (045)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T305" id="Seg_1652" s="T0">kös-A-BIt oččogo itte-BItIn barɨkaːn terij-(I)n-A-BIt</ta>
            <ta e="T8" id="Seg_1653" s="T5">heee d’i͡e-BItIn kas-(I)n-A-BIt</ta>
            <ta e="T14" id="Seg_1654" s="T8">kömü-LAːk že.R d’i͡e-BIt bu͡olok-BIt kömü-LAːk bu͡o</ta>
            <ta e="T307" id="Seg_1655" s="T14">d’i͡e-LArIN kömü-LAːk kas-(I)n-IAk-LArIN naːda</ta>
            <ta e="T306" id="Seg_1656" s="T18">onton d’aktattar bu͡ol-TAk-InA d’i͡e ihin ubiraj.R-LAː-Ar-LAr</ta>
            <ta e="T34" id="Seg_1657" s="T24">bu ke eto.R to što posuda.R barɨkaːn-LArA</ta>
            <ta e="T308" id="Seg_1658" s="T34">onton ü͡ör-(n)I egel-Ar-LAr</ta>
            <ta e="T39" id="Seg_1659" s="T37">Stado privozjat. </ta>
            <ta e="T42" id="Seg_1660" s="T39">onno taba tut-Ar-LAr</ta>
            <ta e="T51" id="Seg_1661" s="T42">taba tut-A-BIt bu͡o oččogo bu͡ol-TAk-InA bu͡olok-BItIgAr agɨs taba-(n)I kölüj-A-BIt</ta>
            <ta e="T60" id="Seg_1662" s="T51">agɨs taba-(n)I tut-Ar-LAr ulakan taba-LAr naːda tut-IAk-GA bu͡olok-GA ke</ta>
            <ta e="T69" id="Seg_1663" s="T60">onton balok inni-(t)IgAr bu͡ol-TAk-InA bu͡o ješo da hetiː baːr</ta>
            <ta e="T78" id="Seg_1664" s="T69">ol hetiː bu͡o itte-(n)I barɨ-(t)In ti͡ej-Ar-LAr bu ka gruzit.R-LAː-Ar-LAr</ta>
            <ta e="T87" id="Seg_1665" s="T78">mas-LAr-(n)I bu͡ol bočka.R-LAr-(n)I bu͡ol možno.R ugol.R-(n)I itte uːr-Ar-LAr bu͡o</ta>
            <ta e="T95" id="Seg_1666" s="T87">itte itte tu͡ok kaːl-BIt kim-(n)I barɨkaːn onno ti͡ej-Ar-LAr</ta>
            <ta e="T102" id="Seg_1667" s="T95">onno tü͡ört taba-(n)I kölüj-Ar-LAr tü͡ört taba-(n)I kölüj-Ar-LAr</ta>
            <ta e="T110" id="Seg_1668" s="T102">onton inni-GI hɨrga bu͡ol-TAk-InA uže.R xozjain.R-(I)ŋ beje-(t)In hɨrga-(t)A</ta>
            <ta e="T118" id="Seg_1669" s="T110">onno da emi͡e be ugol.R možno.R uːr-IAk-GIn bu͡o</ta>
            <ta e="T128" id="Seg_1670" s="T118">i.R onno nipte di͡e-AːččI-BIt mas abɨrattaː-AːččI-BIt *p-Ar-GA *-TA-GA-ɨj-A *drov-A *r-(n)I-BIt</ta>
            <ta e="T132" id="Seg_1671" s="T128">onno nipte-LAr nipte nipte</ta>
            <ta e="T141" id="Seg_1672" s="T132">d'e ol-(n)I ol-(n)I itte uːr ti͡ej-Ar-LAr hɨrga-GA ke</ta>
            <ta e="T151" id="Seg_1673" s="T141">de onton iti xozjain.R ke kim kim hɨrga-LAːk taba-(t)A bu͡ol-Ar</ta>
            <ta e="T158" id="Seg_1674" s="T151">onton d’aktar-(I)ŋ emi͡e d’aktar-(t)IgAr tuspa naːda emi͡e</ta>
            <ta e="T166" id="Seg_1675" s="T158">küččügüj bu͡olok baːr aha ol-I-GA tü͡ört taba-(n)I kölüj-Ar-LAr</ta>
            <ta e="T175" id="Seg_1676" s="T166">onton v peredi.R balok.GEN.R inni-GA bu͡o emi͡e hɨrga baːr</ta>
            <ta e="T179" id="Seg_1677" s="T175">onno ikki taba-(n)I kölüj-Ar-LAr</ta>
            <ta e="T184" id="Seg_1678" s="T179">urut-GI bu͡ol-TAk-InA bütej hetiː di͡e-AːččI-LAr</ta>
            <ta e="T194" id="Seg_1679" s="T184">spetsialno.R onno itte bütte taŋas-LAr-In kaː-LArA bu͡o bütej hetiː-GA ke</ta>
            <ta e="T199" id="Seg_1680" s="T194">taŋas-LAr-GIn üčügej as-LAr-GIn onno uk-A-GIn</ta>
            <ta e="T309" id="Seg_1681" s="T199">bütej hetiː onton d’aktar-(t)In beje-(t)In hetiː-(t)A hɨrga-(t)A</ta>
            <ta e="T213" id="Seg_1682" s="T206">*smotr-(n)Ij-A kak.R ɨraːk kös-GA üs taba-(n)I kölüj-Ar-LAr</ta>
            <ta e="T219" id="Seg_1683" s="T213">a.R čugas kös-GA ikki taba-(n)I kölüj-Ar-LAr</ta>
            <ta e="T228" id="Seg_1684" s="T219">onton ol d’aktar-(I)ŋ balok.R-(t)In kenni-(t)tAn bu͡o güːle bu͡olok baːr</ta>
            <ta e="T231" id="Seg_1685" s="T228">vot.R *-(I)m-Ar-An-GI-ɨj *bolok</ta>
            <ta e="T235" id="Seg_1686" s="T231">kim-LAː-AːččI kepteri güːle di͡e-AːččI-BIt</ta>
            <ta e="T239" id="Seg_1687" s="T235">onno uže.R dva.R</ta>
            <ta e="T253" id="Seg_1688" s="T239">onton ol güːle kenni-(t)tAn tam.R vsjaki͡e.R itte bütte ke araŋas-(n)I možno.R tartar-A-GIn baːj-A-GIn bu͡o</ta>
            <ta e="T310" id="Seg_1689" s="T253">ɨt-LAr-GIn baːj-A-GIn</ta>
            <ta e="T301" id="Seg_1690" s="T255">onton kös-A tur-A-GIn</ta>
            <ta e="T262" id="Seg_1691" s="T258">ulakan argiš bu͡ol-TA onton</ta>
            <ta e="T302" id="Seg_1692" s="T262">osobenno.R kogda.R elbek kihi ke</ta>
            <ta e="T303" id="Seg_1693" s="T267">üs itte ɨ͡al bu͡ol-TAk-InA ke uhun bagajɨ bu͡ol-AːččI ontu-(I)ŋ</ta>
            <ta e="T304" id="Seg_1694" s="T276">onton biːr kihi-(I)ŋ ü͡ör ü͡ör-(n)I üːr-IAk-LAːk bu͡o ü͡ör-(n)I üːr-Ar biːr kihi</ta>
            <ta e="T290" id="Seg_1695" s="T287">ü͡ör-(n)I üːr-Ar ontu-(I)ŋ</ta>
            <ta e="T294" id="Seg_1696" s="T290">bu aːt-(t)A ulakan argiš</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T305" id="Seg_1697" s="T0">Köhöbüt oččogo ittebitin barɨkaːn terinebit. </ta>
            <ta e="T8" id="Seg_1698" s="T5">Heee dʼi͡ebitin kahɨnabɨt. </ta>
            <ta e="T14" id="Seg_1699" s="T8">Kömüleːk že dʼi͡ebit bu͡olokput komulaːk bu͡o. </ta>
            <ta e="T307" id="Seg_1700" s="T14">Dʼi͡elerin kömüleːk kahɨnɨ͡aktarɨn naːda. </ta>
            <ta e="T306" id="Seg_1701" s="T18">Onton dʼaktattar bu͡ollagɨna dʼi͡e ihin ubirajdɨːllar. </ta>
            <ta e="T34" id="Seg_1702" s="T24">Bu ka eto, to što posuda vse eto ubiraetsja barɨkaːnnara ((NOISE)). </ta>
            <ta e="T308" id="Seg_1703" s="T34">Onton ü͡örü egeleller. </ta>
            <ta e="T39" id="Seg_1704" s="T37">Stado privozjat. </ta>
            <ta e="T42" id="Seg_1705" s="T39">Onno taba tuttallar. </ta>
            <ta e="T51" id="Seg_1706" s="T42">Taba tuttabɨt bu͡o oččogo bu͡ollagɨna bolokputugar agɨs tabanɨ kölüjebit. </ta>
            <ta e="T60" id="Seg_1707" s="T51">Agɨs tabanɨ tutallar, ulakan tabalar naːda tutu͡okka bolokko ke. </ta>
            <ta e="T69" id="Seg_1708" s="T60">Onton bolok innitiger bu͡ollagɨna bu͡o össü͡ö da hetiː baːr. </ta>
            <ta e="T78" id="Seg_1709" s="T69">Ol hetiː bu͡o itteni barɨtɨn ti͡ejeller bu ka gruzittɨːllar. </ta>
            <ta e="T87" id="Seg_1710" s="T78">Mastarɨ bu͡ol bu͡očkalarɨ bu͡ol možno ugoli itte uːrallar bu͡o. </ta>
            <ta e="T95" id="Seg_1711" s="T87">Itte itte tu͡ok kaːlbɨt kimi barɨkaːn onno ti͡ejeller. </ta>
            <ta e="T102" id="Seg_1712" s="T95">Onno tü͡ört tabanɨ kölüjeller, tü͡ört tabanɨ kölüjeller. </ta>
            <ta e="T110" id="Seg_1713" s="T102">Onton inniki hɨrga bu͡ollagɨna uže hozjainɨŋ bejetin hɨrgata. </ta>
            <ta e="T118" id="Seg_1714" s="T110">Onno otto emi͡e be ugol možno uːru͡okkun bu͡o. </ta>
            <ta e="T128" id="Seg_1715" s="T118">I onno nipte di͡eččibit, mas abɨrattaːččɨbɨt, palka takaja drova rubit. </ta>
            <ta e="T132" id="Seg_1716" s="T128">Onno nipteler ((NOISE)) nipte nipte ((…)). </ta>
            <ta e="T141" id="Seg_1717" s="T132">De onu onu itte (uːr-) ti͡ejeller hɨrgaga ke. </ta>
            <ta e="T151" id="Seg_1718" s="T141">De onton iti hozjain ke kim kim hɨrgalaːk tabata bu͡olar. </ta>
            <ta e="T158" id="Seg_1719" s="T151">Onton dʼaktarɨŋ emi͡e dʼaktarɨgar tuspa naːda emi͡e. </ta>
            <ta e="T166" id="Seg_1720" s="T158">Kuččuguj bolok baːr aha onuga tü͡ört tabanɨ kölüjeller. </ta>
            <ta e="T175" id="Seg_1721" s="T166">Onton v peredi balka innige bu͡o emi͡e hɨrga baːr. </ta>
            <ta e="T179" id="Seg_1722" s="T175">Onno ikki tabanɨ kölüjeller. </ta>
            <ta e="T184" id="Seg_1723" s="T179">Urukku bu͡ollagɨna bütej hetiː di͡eččiler. </ta>
            <ta e="T194" id="Seg_1724" s="T184">Spetsialnoonno itte bütte taŋastarɨn kaːlara bu͡o bütej hetiːge ke. </ta>
            <ta e="T199" id="Seg_1725" s="T194">Taŋastargɨn üčügej astargɨn onno ugagɨn. </ta>
            <ta e="T309" id="Seg_1726" s="T199">Bütej hetiː onton dʼaktarɨn bejetin hetiːte hɨrgata. </ta>
            <ta e="T213" id="Seg_1727" s="T206">Smotrja kak, ɨraːk köskö üs tabanɨ kölüjeller. </ta>
            <ta e="T219" id="Seg_1728" s="T213">A čugas köskö ikki tabanɨ kölüjeller. </ta>
            <ta e="T228" id="Seg_1729" s="T219">Onton ol dʼaktarɨŋ bu͡ologun kennitten bu͡o güːle bolok baːr. </ta>
            <ta e="T231" id="Seg_1730" s="T228">Vot, malenkij bolok. </ta>
            <ta e="T235" id="Seg_1731" s="T231">Kimneːčči kepteri güːle di͡eččibit. </ta>
            <ta e="T239" id="Seg_1732" s="T235">Onno uže dva olenja. </ta>
            <ta e="T253" id="Seg_1733" s="T239">Onton ol güːle kenniten tam vsjakie itte bütte ke, araŋahɨ možno tartaragɨn baːjagɨn bu͡o. </ta>
            <ta e="T310" id="Seg_1734" s="T253">Ɨttargɨn baːjagɨn ((NOISE)). </ta>
            <ta e="T301" id="Seg_1735" s="T255">Onton köhö turagɨn ((LAUGH)). </ta>
            <ta e="T262" id="Seg_1736" s="T258">((…)) Ulakan argiš bu͡olla (onton). </ta>
            <ta e="T302" id="Seg_1737" s="T262">Osobenno kogda elbek kihi ke. </ta>
            <ta e="T303" id="Seg_1738" s="T267">Üs itte ɨ͡al bu͡ollagɨna ke uhun bagajɨ bu͡olaːččɨ ontuŋ. </ta>
            <ta e="T304" id="Seg_1739" s="T276">Onton biːr kihiŋ ü͡ör ü͡örü üːrü͡öteːk bu͡o, ü͡örü üːrer biːr kihi. </ta>
            <ta e="T290" id="Seg_1740" s="T287">Ü͡örü üːrer ontuŋ. </ta>
            <ta e="T294" id="Seg_1741" s="T290">Bu aːta ulakan argiš. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1742" s="T0">köh-ö-büt</ta>
            <ta e="T2" id="Seg_1743" s="T1">oččogo</ta>
            <ta e="T3" id="Seg_1744" s="T2">itte-biti-n</ta>
            <ta e="T4" id="Seg_1745" s="T3">barɨ-kaːn</ta>
            <ta e="T305" id="Seg_1746" s="T4">teri-n-e-bit</ta>
            <ta e="T6" id="Seg_1747" s="T5">heː</ta>
            <ta e="T7" id="Seg_1748" s="T6">dʼi͡e-biti-n</ta>
            <ta e="T8" id="Seg_1749" s="T7">kah-ɨ-n-a-bɨt</ta>
            <ta e="T9" id="Seg_1750" s="T8">kömü-leːk</ta>
            <ta e="T11" id="Seg_1751" s="T10">dʼi͡e-bit</ta>
            <ta e="T12" id="Seg_1752" s="T11">bu͡olok-put</ta>
            <ta e="T13" id="Seg_1753" s="T12">komu-laːk</ta>
            <ta e="T14" id="Seg_1754" s="T13">bu͡o</ta>
            <ta e="T15" id="Seg_1755" s="T14">dʼi͡e-leri-n</ta>
            <ta e="T16" id="Seg_1756" s="T15">kömü-leːk</ta>
            <ta e="T17" id="Seg_1757" s="T16">kah-ɨ-n-ɨ͡ak-tarɨ-n</ta>
            <ta e="T307" id="Seg_1758" s="T17">naːda</ta>
            <ta e="T19" id="Seg_1759" s="T18">on-ton</ta>
            <ta e="T20" id="Seg_1760" s="T19">dʼaktat-tar</ta>
            <ta e="T21" id="Seg_1761" s="T20">bu͡ollagɨna</ta>
            <ta e="T22" id="Seg_1762" s="T21">dʼi͡e</ta>
            <ta e="T23" id="Seg_1763" s="T22">ih-i-n</ta>
            <ta e="T306" id="Seg_1764" s="T23">ubiraj-dɨː-l-lar</ta>
            <ta e="T25" id="Seg_1765" s="T24">bu</ta>
            <ta e="T26" id="Seg_1766" s="T25">ka</ta>
            <ta e="T34" id="Seg_1767" s="T33">barɨ-kaːn-nar-a</ta>
            <ta e="T35" id="Seg_1768" s="T34">on-ton</ta>
            <ta e="T36" id="Seg_1769" s="T35">ü͡ör-ü</ta>
            <ta e="T308" id="Seg_1770" s="T36">egel-el-ler</ta>
            <ta e="T40" id="Seg_1771" s="T39">onno</ta>
            <ta e="T41" id="Seg_1772" s="T40">taba</ta>
            <ta e="T42" id="Seg_1773" s="T41">tutt-al-lar</ta>
            <ta e="T43" id="Seg_1774" s="T42">taba</ta>
            <ta e="T44" id="Seg_1775" s="T43">tutt-a-bɨt</ta>
            <ta e="T45" id="Seg_1776" s="T44">bu͡o</ta>
            <ta e="T46" id="Seg_1777" s="T45">oččogo</ta>
            <ta e="T47" id="Seg_1778" s="T46">bu͡ollagɨna</ta>
            <ta e="T48" id="Seg_1779" s="T47">bolok-putu-gar</ta>
            <ta e="T49" id="Seg_1780" s="T48">agɨs</ta>
            <ta e="T50" id="Seg_1781" s="T49">taba-nɨ</ta>
            <ta e="T51" id="Seg_1782" s="T50">kölüj-e-bit</ta>
            <ta e="T52" id="Seg_1783" s="T51">agɨs</ta>
            <ta e="T53" id="Seg_1784" s="T52">taba-nɨ</ta>
            <ta e="T54" id="Seg_1785" s="T53">tut-al-lar</ta>
            <ta e="T55" id="Seg_1786" s="T54">ulagan</ta>
            <ta e="T56" id="Seg_1787" s="T55">taba-lar</ta>
            <ta e="T57" id="Seg_1788" s="T56">naːda</ta>
            <ta e="T58" id="Seg_1789" s="T57">tut-u͡ok-ka</ta>
            <ta e="T59" id="Seg_1790" s="T58">bolok-ko</ta>
            <ta e="T60" id="Seg_1791" s="T59">ke</ta>
            <ta e="T61" id="Seg_1792" s="T60">on-ton</ta>
            <ta e="T62" id="Seg_1793" s="T61">bolok</ta>
            <ta e="T63" id="Seg_1794" s="T62">inni-ti-ger</ta>
            <ta e="T64" id="Seg_1795" s="T63">bu͡ollagɨna</ta>
            <ta e="T65" id="Seg_1796" s="T64">bu͡o</ta>
            <ta e="T66" id="Seg_1797" s="T65">össü͡ö</ta>
            <ta e="T67" id="Seg_1798" s="T66">da</ta>
            <ta e="T68" id="Seg_1799" s="T67">hetiː</ta>
            <ta e="T69" id="Seg_1800" s="T68">baːr</ta>
            <ta e="T70" id="Seg_1801" s="T69">ol</ta>
            <ta e="T71" id="Seg_1802" s="T70">hetiː</ta>
            <ta e="T72" id="Seg_1803" s="T71">bu͡o</ta>
            <ta e="T73" id="Seg_1804" s="T72">itte-ni</ta>
            <ta e="T74" id="Seg_1805" s="T73">barɨ-tɨ-n</ta>
            <ta e="T75" id="Seg_1806" s="T74">ti͡ej-el-ler</ta>
            <ta e="T76" id="Seg_1807" s="T75">bu</ta>
            <ta e="T77" id="Seg_1808" s="T76">ka</ta>
            <ta e="T78" id="Seg_1809" s="T77">gruzit-tɨː-l-lar</ta>
            <ta e="T79" id="Seg_1810" s="T78">mas-tar-ɨ</ta>
            <ta e="T80" id="Seg_1811" s="T79">bu͡ol</ta>
            <ta e="T81" id="Seg_1812" s="T80">bu͡očka-lar-ɨ</ta>
            <ta e="T82" id="Seg_1813" s="T81">bu͡ol</ta>
            <ta e="T84" id="Seg_1814" s="T83">ugol-i</ta>
            <ta e="T85" id="Seg_1815" s="T84">itte</ta>
            <ta e="T86" id="Seg_1816" s="T85">uːr-al-lar</ta>
            <ta e="T87" id="Seg_1817" s="T86">bu͡o</ta>
            <ta e="T88" id="Seg_1818" s="T87">itte</ta>
            <ta e="T89" id="Seg_1819" s="T88">itte</ta>
            <ta e="T90" id="Seg_1820" s="T89">tu͡ok</ta>
            <ta e="T91" id="Seg_1821" s="T90">kaːl-bɨt</ta>
            <ta e="T92" id="Seg_1822" s="T91">kim-i</ta>
            <ta e="T93" id="Seg_1823" s="T92">barɨ-kaːn</ta>
            <ta e="T94" id="Seg_1824" s="T93">onno</ta>
            <ta e="T95" id="Seg_1825" s="T94">ti͡ej-el-ler</ta>
            <ta e="T96" id="Seg_1826" s="T95">onno</ta>
            <ta e="T97" id="Seg_1827" s="T96">tü͡ört</ta>
            <ta e="T98" id="Seg_1828" s="T97">taba-nɨ</ta>
            <ta e="T99" id="Seg_1829" s="T98">kölüj-el-ler</ta>
            <ta e="T100" id="Seg_1830" s="T99">tü͡ört</ta>
            <ta e="T101" id="Seg_1831" s="T100">taba-nɨ</ta>
            <ta e="T102" id="Seg_1832" s="T101">kölüj-el-ler</ta>
            <ta e="T103" id="Seg_1833" s="T102">on-ton</ta>
            <ta e="T104" id="Seg_1834" s="T103">inniki</ta>
            <ta e="T105" id="Seg_1835" s="T104">hɨrga</ta>
            <ta e="T106" id="Seg_1836" s="T105">bu͡ollagɨna</ta>
            <ta e="T108" id="Seg_1837" s="T107">hozjain-ɨ-ŋ</ta>
            <ta e="T109" id="Seg_1838" s="T108">beje-ti-n</ta>
            <ta e="T110" id="Seg_1839" s="T109">hɨrga-ta</ta>
            <ta e="T111" id="Seg_1840" s="T110">onno</ta>
            <ta e="T112" id="Seg_1841" s="T111">otto</ta>
            <ta e="T113" id="Seg_1842" s="T112">emi͡e</ta>
            <ta e="T114" id="Seg_1843" s="T113">be</ta>
            <ta e="T115" id="Seg_1844" s="T114">ugol</ta>
            <ta e="T117" id="Seg_1845" s="T116">uːr-u͡ok-ku-n</ta>
            <ta e="T118" id="Seg_1846" s="T117">bu͡o</ta>
            <ta e="T295" id="Seg_1847" s="T118">i</ta>
            <ta e="T119" id="Seg_1848" s="T295">onno</ta>
            <ta e="T120" id="Seg_1849" s="T119">nipte</ta>
            <ta e="T121" id="Seg_1850" s="T120">di͡e-čči-bit</ta>
            <ta e="T122" id="Seg_1851" s="T121">mas</ta>
            <ta e="T123" id="Seg_1852" s="T122">abɨrat-taː-ččɨ-bɨt</ta>
            <ta e="T129" id="Seg_1853" s="T128">onno</ta>
            <ta e="T130" id="Seg_1854" s="T129">nipte-ler</ta>
            <ta e="T131" id="Seg_1855" s="T130">nipte</ta>
            <ta e="T132" id="Seg_1856" s="T131">nipte</ta>
            <ta e="T133" id="Seg_1857" s="T132">de</ta>
            <ta e="T134" id="Seg_1858" s="T133">o-nu</ta>
            <ta e="T135" id="Seg_1859" s="T134">o-nu</ta>
            <ta e="T136" id="Seg_1860" s="T135">itte</ta>
            <ta e="T139" id="Seg_1861" s="T138">ti͡ej-el-ler</ta>
            <ta e="T140" id="Seg_1862" s="T139">hɨrga-ga</ta>
            <ta e="T141" id="Seg_1863" s="T140">ke</ta>
            <ta e="T142" id="Seg_1864" s="T141">de</ta>
            <ta e="T143" id="Seg_1865" s="T142">on-ton</ta>
            <ta e="T144" id="Seg_1866" s="T143">iti</ta>
            <ta e="T145" id="Seg_1867" s="T144">hozjain</ta>
            <ta e="T146" id="Seg_1868" s="T145">ke</ta>
            <ta e="T147" id="Seg_1869" s="T146">kim</ta>
            <ta e="T148" id="Seg_1870" s="T147">kim</ta>
            <ta e="T149" id="Seg_1871" s="T148">hɨrga-laːk</ta>
            <ta e="T150" id="Seg_1872" s="T149">taba-ta</ta>
            <ta e="T151" id="Seg_1873" s="T150">bu͡ol-ar</ta>
            <ta e="T152" id="Seg_1874" s="T151">on-ton</ta>
            <ta e="T153" id="Seg_1875" s="T152">dʼaktar-ɨ-ŋ</ta>
            <ta e="T154" id="Seg_1876" s="T153">emi͡e</ta>
            <ta e="T155" id="Seg_1877" s="T154">dʼaktar-ɨ-gar</ta>
            <ta e="T156" id="Seg_1878" s="T155">tuspa</ta>
            <ta e="T157" id="Seg_1879" s="T156">naːda</ta>
            <ta e="T158" id="Seg_1880" s="T157">emi͡e</ta>
            <ta e="T159" id="Seg_1881" s="T158">kuččuguj</ta>
            <ta e="T160" id="Seg_1882" s="T159">bolok</ta>
            <ta e="T161" id="Seg_1883" s="T160">baːr</ta>
            <ta e="T163" id="Seg_1884" s="T162">onu-ga</ta>
            <ta e="T164" id="Seg_1885" s="T163">tü͡ört</ta>
            <ta e="T165" id="Seg_1886" s="T164">taba-nɨ</ta>
            <ta e="T166" id="Seg_1887" s="T165">kölüj-el-ler</ta>
            <ta e="T167" id="Seg_1888" s="T166">on-ton</ta>
            <ta e="T171" id="Seg_1889" s="T170">inni-ge</ta>
            <ta e="T172" id="Seg_1890" s="T171">bu͡o</ta>
            <ta e="T173" id="Seg_1891" s="T172">emi͡e</ta>
            <ta e="T174" id="Seg_1892" s="T173">hɨrga</ta>
            <ta e="T175" id="Seg_1893" s="T174">baːr</ta>
            <ta e="T176" id="Seg_1894" s="T175">onno</ta>
            <ta e="T177" id="Seg_1895" s="T176">ikki</ta>
            <ta e="T178" id="Seg_1896" s="T177">taba-nɨ</ta>
            <ta e="T179" id="Seg_1897" s="T178">kölüj-el-ler</ta>
            <ta e="T180" id="Seg_1898" s="T179">urukku</ta>
            <ta e="T181" id="Seg_1899" s="T180">bu͡ollagɨna</ta>
            <ta e="T182" id="Seg_1900" s="T181">bütej</ta>
            <ta e="T183" id="Seg_1901" s="T182">hetiː</ta>
            <ta e="T184" id="Seg_1902" s="T183">di͡e-čči-ler</ta>
            <ta e="T185" id="Seg_1903" s="T296">onno</ta>
            <ta e="T186" id="Seg_1904" s="T185">itte</ta>
            <ta e="T187" id="Seg_1905" s="T186">bütte</ta>
            <ta e="T188" id="Seg_1906" s="T187">taŋas-tar-ɨ-n</ta>
            <ta e="T189" id="Seg_1907" s="T188">kaːl-ar-a</ta>
            <ta e="T190" id="Seg_1908" s="T189">bu͡o</ta>
            <ta e="T191" id="Seg_1909" s="T190">bütej</ta>
            <ta e="T192" id="Seg_1910" s="T191">hetiː-ge</ta>
            <ta e="T194" id="Seg_1911" s="T192">ke</ta>
            <ta e="T195" id="Seg_1912" s="T194">taŋas-tar-gɨ-n</ta>
            <ta e="T196" id="Seg_1913" s="T195">üčügej</ta>
            <ta e="T197" id="Seg_1914" s="T196">as-tar-gɨ-n</ta>
            <ta e="T198" id="Seg_1915" s="T197">onno</ta>
            <ta e="T199" id="Seg_1916" s="T198">ug-a-gɨn</ta>
            <ta e="T200" id="Seg_1917" s="T199">bütej</ta>
            <ta e="T201" id="Seg_1918" s="T200">hetiː</ta>
            <ta e="T202" id="Seg_1919" s="T201">on-ton</ta>
            <ta e="T203" id="Seg_1920" s="T202">dʼaktar-ɨ-n</ta>
            <ta e="T204" id="Seg_1921" s="T203">beje-ti-n</ta>
            <ta e="T205" id="Seg_1922" s="T204">hetiː-te</ta>
            <ta e="T309" id="Seg_1923" s="T205">hɨrga-ta</ta>
            <ta e="T208" id="Seg_1924" s="T207">ɨraːk</ta>
            <ta e="T209" id="Seg_1925" s="T208">kös-kö</ta>
            <ta e="T210" id="Seg_1926" s="T209">üs</ta>
            <ta e="T211" id="Seg_1927" s="T210">taba-nɨ</ta>
            <ta e="T213" id="Seg_1928" s="T211">kölüj-el-ler</ta>
            <ta e="T298" id="Seg_1929" s="T213">a</ta>
            <ta e="T214" id="Seg_1930" s="T298">čugas</ta>
            <ta e="T215" id="Seg_1931" s="T214">kös-kö</ta>
            <ta e="T216" id="Seg_1932" s="T215">ikki</ta>
            <ta e="T217" id="Seg_1933" s="T216">taba-nɨ</ta>
            <ta e="T219" id="Seg_1934" s="T217">kölüj-el-ler</ta>
            <ta e="T220" id="Seg_1935" s="T219">on-ton</ta>
            <ta e="T221" id="Seg_1936" s="T220">ol</ta>
            <ta e="T222" id="Seg_1937" s="T221">dʼaktar-ɨ-ŋ</ta>
            <ta e="T223" id="Seg_1938" s="T222">bu͡olog-u-n</ta>
            <ta e="T224" id="Seg_1939" s="T223">kenn-i-tten</ta>
            <ta e="T225" id="Seg_1940" s="T224">bu͡o</ta>
            <ta e="T226" id="Seg_1941" s="T225">güːle</ta>
            <ta e="T227" id="Seg_1942" s="T226">bolok</ta>
            <ta e="T228" id="Seg_1943" s="T227">baːr</ta>
            <ta e="T232" id="Seg_1944" s="T231">kim-neː-čči</ta>
            <ta e="T233" id="Seg_1945" s="T232">kepteri</ta>
            <ta e="T234" id="Seg_1946" s="T233">güːle</ta>
            <ta e="T235" id="Seg_1947" s="T234">di͡e-čči-bit</ta>
            <ta e="T236" id="Seg_1948" s="T235">onno</ta>
            <ta e="T240" id="Seg_1949" s="T239">on-ton</ta>
            <ta e="T241" id="Seg_1950" s="T240">ol</ta>
            <ta e="T242" id="Seg_1951" s="T241">güːle</ta>
            <ta e="T243" id="Seg_1952" s="T242">kenn-i-ten</ta>
            <ta e="T246" id="Seg_1953" s="T245">itte</ta>
            <ta e="T247" id="Seg_1954" s="T246">bütte</ta>
            <ta e="T248" id="Seg_1955" s="T247">ke</ta>
            <ta e="T249" id="Seg_1956" s="T248">araŋah-ɨ</ta>
            <ta e="T251" id="Seg_1957" s="T250">tar-tar-a-gɨn</ta>
            <ta e="T252" id="Seg_1958" s="T251">baːj-a-gɨn</ta>
            <ta e="T253" id="Seg_1959" s="T252">bu͡o</ta>
            <ta e="T254" id="Seg_1960" s="T253">ɨt-tar-gɨ-n</ta>
            <ta e="T310" id="Seg_1961" s="T254">baːj-a-gɨn</ta>
            <ta e="T256" id="Seg_1962" s="T255">on-ton</ta>
            <ta e="T257" id="Seg_1963" s="T256">köh-ö</ta>
            <ta e="T301" id="Seg_1964" s="T257">tur-a-gɨn</ta>
            <ta e="T259" id="Seg_1965" s="T258">ulagan</ta>
            <ta e="T260" id="Seg_1966" s="T259">argiš</ta>
            <ta e="T261" id="Seg_1967" s="T260">bu͡ol-l-a</ta>
            <ta e="T262" id="Seg_1968" s="T261">on-ton</ta>
            <ta e="T264" id="Seg_1969" s="T263">elbek</ta>
            <ta e="T265" id="Seg_1970" s="T264">kihi</ta>
            <ta e="T302" id="Seg_1971" s="T265">ke</ta>
            <ta e="T268" id="Seg_1972" s="T267">üs</ta>
            <ta e="T269" id="Seg_1973" s="T268">itte</ta>
            <ta e="T270" id="Seg_1974" s="T269">ɨ͡al</ta>
            <ta e="T271" id="Seg_1975" s="T270">bu͡ollagɨna</ta>
            <ta e="T272" id="Seg_1976" s="T271">ke</ta>
            <ta e="T273" id="Seg_1977" s="T272">uhun</ta>
            <ta e="T274" id="Seg_1978" s="T273">bagajɨ</ta>
            <ta e="T275" id="Seg_1979" s="T274">bu͡ol-aːččɨ</ta>
            <ta e="T303" id="Seg_1980" s="T275">on-tu-ŋ</ta>
            <ta e="T277" id="Seg_1981" s="T276">on-ton</ta>
            <ta e="T278" id="Seg_1982" s="T277">biːr</ta>
            <ta e="T279" id="Seg_1983" s="T278">kihi-ŋ</ta>
            <ta e="T280" id="Seg_1984" s="T279">ü͡ör</ta>
            <ta e="T281" id="Seg_1985" s="T280">ü͡ör-ü</ta>
            <ta e="T282" id="Seg_1986" s="T281">üːr-ü͡ö-teːk</ta>
            <ta e="T283" id="Seg_1987" s="T282">bu͡o</ta>
            <ta e="T284" id="Seg_1988" s="T283">ü͡ör-ü</ta>
            <ta e="T285" id="Seg_1989" s="T284">üːr-er</ta>
            <ta e="T286" id="Seg_1990" s="T285">biːr</ta>
            <ta e="T304" id="Seg_1991" s="T286">kihi</ta>
            <ta e="T288" id="Seg_1992" s="T287">ü͡ör-ü</ta>
            <ta e="T289" id="Seg_1993" s="T288">üːr-er</ta>
            <ta e="T290" id="Seg_1994" s="T289">on-tu-ŋ</ta>
            <ta e="T291" id="Seg_1995" s="T290">bu</ta>
            <ta e="T292" id="Seg_1996" s="T291">aːt-a</ta>
            <ta e="T293" id="Seg_1997" s="T292">ulagan</ta>
            <ta e="T294" id="Seg_1998" s="T293">argiš</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1999" s="T0">kös-A-BIt</ta>
            <ta e="T2" id="Seg_2000" s="T1">oččogo</ta>
            <ta e="T3" id="Seg_2001" s="T2">itte-BItI-n</ta>
            <ta e="T4" id="Seg_2002" s="T3">barɨ-kAːN</ta>
            <ta e="T305" id="Seg_2003" s="T4">terij-n-A-BIt</ta>
            <ta e="T6" id="Seg_2004" s="T5">eː</ta>
            <ta e="T7" id="Seg_2005" s="T6">dʼi͡e-BItI-n</ta>
            <ta e="T8" id="Seg_2006" s="T7">kas-I-n-A-BIt</ta>
            <ta e="T9" id="Seg_2007" s="T8">kömük-LAːK</ta>
            <ta e="T11" id="Seg_2008" s="T10">dʼi͡e-BIt</ta>
            <ta e="T12" id="Seg_2009" s="T11">balok-BIt</ta>
            <ta e="T13" id="Seg_2010" s="T12">kömük-LAːK</ta>
            <ta e="T14" id="Seg_2011" s="T13">bu͡o</ta>
            <ta e="T15" id="Seg_2012" s="T14">dʼi͡e-LArI-n</ta>
            <ta e="T16" id="Seg_2013" s="T15">kömük-LAːK</ta>
            <ta e="T17" id="Seg_2014" s="T16">kas-I-n-IAK-LArI-n</ta>
            <ta e="T307" id="Seg_2015" s="T17">naːda</ta>
            <ta e="T19" id="Seg_2016" s="T18">ol-ttAn</ta>
            <ta e="T20" id="Seg_2017" s="T19">dʼaktar-LAr</ta>
            <ta e="T21" id="Seg_2018" s="T20">bu͡ollagɨna</ta>
            <ta e="T22" id="Seg_2019" s="T21">dʼi͡e</ta>
            <ta e="T23" id="Seg_2020" s="T22">is-tI-n</ta>
            <ta e="T306" id="Seg_2021" s="T23">ubiraj-LAː-Ar-LAr</ta>
            <ta e="T25" id="Seg_2022" s="T24">bu</ta>
            <ta e="T26" id="Seg_2023" s="T25">ka</ta>
            <ta e="T34" id="Seg_2024" s="T33">barɨ-kAːN-LAr-tA</ta>
            <ta e="T35" id="Seg_2025" s="T34">ol-ttAn</ta>
            <ta e="T36" id="Seg_2026" s="T35">ü͡ör-nI</ta>
            <ta e="T308" id="Seg_2027" s="T36">egel-Ar-LAr</ta>
            <ta e="T40" id="Seg_2028" s="T39">onno</ta>
            <ta e="T41" id="Seg_2029" s="T40">taba</ta>
            <ta e="T42" id="Seg_2030" s="T41">tutun-Ar-LAr</ta>
            <ta e="T43" id="Seg_2031" s="T42">taba</ta>
            <ta e="T44" id="Seg_2032" s="T43">tutun-A-BIt</ta>
            <ta e="T45" id="Seg_2033" s="T44">bu͡o</ta>
            <ta e="T46" id="Seg_2034" s="T45">oččogo</ta>
            <ta e="T47" id="Seg_2035" s="T46">bu͡ollagɨna</ta>
            <ta e="T48" id="Seg_2036" s="T47">balok-BItI-GAr</ta>
            <ta e="T49" id="Seg_2037" s="T48">agɨs</ta>
            <ta e="T50" id="Seg_2038" s="T49">taba-nI</ta>
            <ta e="T51" id="Seg_2039" s="T50">kölüj-A-BIt</ta>
            <ta e="T52" id="Seg_2040" s="T51">agɨs</ta>
            <ta e="T53" id="Seg_2041" s="T52">taba-nI</ta>
            <ta e="T54" id="Seg_2042" s="T53">tut-Ar-LAr</ta>
            <ta e="T55" id="Seg_2043" s="T54">ulakan</ta>
            <ta e="T56" id="Seg_2044" s="T55">taba-LAr</ta>
            <ta e="T57" id="Seg_2045" s="T56">naːda</ta>
            <ta e="T58" id="Seg_2046" s="T57">tut-IAK-GA</ta>
            <ta e="T59" id="Seg_2047" s="T58">balok-GA</ta>
            <ta e="T60" id="Seg_2048" s="T59">ka</ta>
            <ta e="T61" id="Seg_2049" s="T60">ol-ttAn</ta>
            <ta e="T62" id="Seg_2050" s="T61">balok</ta>
            <ta e="T63" id="Seg_2051" s="T62">ilin-tI-GAr</ta>
            <ta e="T64" id="Seg_2052" s="T63">bu͡ollagɨna</ta>
            <ta e="T65" id="Seg_2053" s="T64">bu͡o</ta>
            <ta e="T66" id="Seg_2054" s="T65">össü͡ö</ta>
            <ta e="T67" id="Seg_2055" s="T66">da</ta>
            <ta e="T68" id="Seg_2056" s="T67">hetiː</ta>
            <ta e="T69" id="Seg_2057" s="T68">baːr</ta>
            <ta e="T70" id="Seg_2058" s="T69">ol</ta>
            <ta e="T71" id="Seg_2059" s="T70">hetiː</ta>
            <ta e="T72" id="Seg_2060" s="T71">bu͡o</ta>
            <ta e="T73" id="Seg_2061" s="T72">itte-nI</ta>
            <ta e="T74" id="Seg_2062" s="T73">barɨ-tI-n</ta>
            <ta e="T75" id="Seg_2063" s="T74">ti͡ej-Ar-LAr</ta>
            <ta e="T76" id="Seg_2064" s="T75">bu</ta>
            <ta e="T77" id="Seg_2065" s="T76">ka</ta>
            <ta e="T78" id="Seg_2066" s="T77">gruzit-LAː-Ar-LAr</ta>
            <ta e="T79" id="Seg_2067" s="T78">mas-LAr-nI</ta>
            <ta e="T80" id="Seg_2068" s="T79">bu͡ol</ta>
            <ta e="T81" id="Seg_2069" s="T80">bu͡očuku-LAr-nI</ta>
            <ta e="T82" id="Seg_2070" s="T81">bu͡ol</ta>
            <ta e="T84" id="Seg_2071" s="T83">ugol-nI</ta>
            <ta e="T85" id="Seg_2072" s="T84">itte</ta>
            <ta e="T86" id="Seg_2073" s="T85">uːr-Ar-LAr</ta>
            <ta e="T87" id="Seg_2074" s="T86">bu͡o</ta>
            <ta e="T88" id="Seg_2075" s="T87">itte</ta>
            <ta e="T89" id="Seg_2076" s="T88">itte</ta>
            <ta e="T90" id="Seg_2077" s="T89">tu͡ok</ta>
            <ta e="T91" id="Seg_2078" s="T90">kaːl-BIT</ta>
            <ta e="T92" id="Seg_2079" s="T91">kim-nI</ta>
            <ta e="T93" id="Seg_2080" s="T92">barɨ-kAːN</ta>
            <ta e="T94" id="Seg_2081" s="T93">onno</ta>
            <ta e="T95" id="Seg_2082" s="T94">ti͡ej-Ar-LAr</ta>
            <ta e="T96" id="Seg_2083" s="T95">onno</ta>
            <ta e="T97" id="Seg_2084" s="T96">tü͡ört</ta>
            <ta e="T98" id="Seg_2085" s="T97">taba-nI</ta>
            <ta e="T99" id="Seg_2086" s="T98">kölüj-Ar-LAr</ta>
            <ta e="T100" id="Seg_2087" s="T99">tü͡ört</ta>
            <ta e="T101" id="Seg_2088" s="T100">taba-nI</ta>
            <ta e="T102" id="Seg_2089" s="T101">kölüj-Ar-LAr</ta>
            <ta e="T103" id="Seg_2090" s="T102">ol-ttAn</ta>
            <ta e="T104" id="Seg_2091" s="T103">inniki</ta>
            <ta e="T105" id="Seg_2092" s="T104">hɨrga</ta>
            <ta e="T106" id="Seg_2093" s="T105">bu͡ollagɨna</ta>
            <ta e="T108" id="Seg_2094" s="T107">küheːjin-I-ŋ</ta>
            <ta e="T109" id="Seg_2095" s="T108">beje-tI-n</ta>
            <ta e="T110" id="Seg_2096" s="T109">hɨrga-tA</ta>
            <ta e="T111" id="Seg_2097" s="T110">onno</ta>
            <ta e="T112" id="Seg_2098" s="T111">otto</ta>
            <ta e="T113" id="Seg_2099" s="T112">emi͡e</ta>
            <ta e="T114" id="Seg_2100" s="T113">be</ta>
            <ta e="T115" id="Seg_2101" s="T114">ugol</ta>
            <ta e="T117" id="Seg_2102" s="T116">uːr-IAK-GI-n</ta>
            <ta e="T118" id="Seg_2103" s="T117">bu͡o</ta>
            <ta e="T295" id="Seg_2104" s="T118">i</ta>
            <ta e="T119" id="Seg_2105" s="T295">onno</ta>
            <ta e="T120" id="Seg_2106" s="T119">nipte</ta>
            <ta e="T121" id="Seg_2107" s="T120">di͡e-AːččI-BIt</ta>
            <ta e="T122" id="Seg_2108" s="T121">mas</ta>
            <ta e="T123" id="Seg_2109" s="T122">abɨrat-TAː-ččI-BIt</ta>
            <ta e="T129" id="Seg_2110" s="T128">onno</ta>
            <ta e="T130" id="Seg_2111" s="T129">nipte-LAr</ta>
            <ta e="T131" id="Seg_2112" s="T130">nipte</ta>
            <ta e="T132" id="Seg_2113" s="T131">nipte</ta>
            <ta e="T133" id="Seg_2114" s="T132">dʼe</ta>
            <ta e="T134" id="Seg_2115" s="T133">ol-nI</ta>
            <ta e="T135" id="Seg_2116" s="T134">ol-nI</ta>
            <ta e="T136" id="Seg_2117" s="T135">itte</ta>
            <ta e="T139" id="Seg_2118" s="T138">ti͡ej-Ar-LAr</ta>
            <ta e="T140" id="Seg_2119" s="T139">hɨrga-GA</ta>
            <ta e="T141" id="Seg_2120" s="T140">ka</ta>
            <ta e="T142" id="Seg_2121" s="T141">dʼe</ta>
            <ta e="T143" id="Seg_2122" s="T142">ol-ttAn</ta>
            <ta e="T144" id="Seg_2123" s="T143">iti</ta>
            <ta e="T145" id="Seg_2124" s="T144">küheːjin</ta>
            <ta e="T146" id="Seg_2125" s="T145">ka</ta>
            <ta e="T147" id="Seg_2126" s="T146">kim</ta>
            <ta e="T148" id="Seg_2127" s="T147">kim</ta>
            <ta e="T149" id="Seg_2128" s="T148">hɨrga-LAːK</ta>
            <ta e="T150" id="Seg_2129" s="T149">taba-tA</ta>
            <ta e="T151" id="Seg_2130" s="T150">bu͡ol-Ar</ta>
            <ta e="T152" id="Seg_2131" s="T151">ol-ttAn</ta>
            <ta e="T153" id="Seg_2132" s="T152">dʼaktar-I-ŋ</ta>
            <ta e="T154" id="Seg_2133" s="T153">emi͡e</ta>
            <ta e="T155" id="Seg_2134" s="T154">dʼaktar-tI-GAr</ta>
            <ta e="T156" id="Seg_2135" s="T155">tuspa</ta>
            <ta e="T157" id="Seg_2136" s="T156">naːda</ta>
            <ta e="T158" id="Seg_2137" s="T157">emi͡e</ta>
            <ta e="T159" id="Seg_2138" s="T158">küččügüj</ta>
            <ta e="T160" id="Seg_2139" s="T159">balok</ta>
            <ta e="T161" id="Seg_2140" s="T160">baːr</ta>
            <ta e="T163" id="Seg_2141" s="T162">ol-GA</ta>
            <ta e="T164" id="Seg_2142" s="T163">tü͡ört</ta>
            <ta e="T165" id="Seg_2143" s="T164">taba-nI</ta>
            <ta e="T166" id="Seg_2144" s="T165">kölüj-Ar-LAr</ta>
            <ta e="T167" id="Seg_2145" s="T166">ol-ttAn</ta>
            <ta e="T171" id="Seg_2146" s="T170">ilin-GA</ta>
            <ta e="T172" id="Seg_2147" s="T171">bu͡o</ta>
            <ta e="T173" id="Seg_2148" s="T172">emi͡e</ta>
            <ta e="T174" id="Seg_2149" s="T173">hɨrga</ta>
            <ta e="T175" id="Seg_2150" s="T174">baːr</ta>
            <ta e="T176" id="Seg_2151" s="T175">onno</ta>
            <ta e="T177" id="Seg_2152" s="T176">ikki</ta>
            <ta e="T178" id="Seg_2153" s="T177">taba-nI</ta>
            <ta e="T179" id="Seg_2154" s="T178">kölüj-Ar-LAr</ta>
            <ta e="T180" id="Seg_2155" s="T179">urukku</ta>
            <ta e="T181" id="Seg_2156" s="T180">bu͡ollagɨna</ta>
            <ta e="T182" id="Seg_2157" s="T181">bütej</ta>
            <ta e="T183" id="Seg_2158" s="T182">hetiː</ta>
            <ta e="T184" id="Seg_2159" s="T183">di͡e-AːččI-LAr</ta>
            <ta e="T185" id="Seg_2160" s="T296">onno</ta>
            <ta e="T186" id="Seg_2161" s="T185">itte</ta>
            <ta e="T187" id="Seg_2162" s="T186">bütte</ta>
            <ta e="T188" id="Seg_2163" s="T187">taŋas-LAr-tI-n</ta>
            <ta e="T189" id="Seg_2164" s="T188">kaːl-Ar-tA</ta>
            <ta e="T190" id="Seg_2165" s="T189">bu͡o</ta>
            <ta e="T191" id="Seg_2166" s="T190">bütej</ta>
            <ta e="T192" id="Seg_2167" s="T191">hetiː-GA</ta>
            <ta e="T194" id="Seg_2168" s="T192">ke</ta>
            <ta e="T195" id="Seg_2169" s="T194">taŋas-LAr-GI-n</ta>
            <ta e="T196" id="Seg_2170" s="T195">üčügej</ta>
            <ta e="T197" id="Seg_2171" s="T196">as-LAr-GI-n</ta>
            <ta e="T198" id="Seg_2172" s="T197">onno</ta>
            <ta e="T199" id="Seg_2173" s="T198">uk-A-GIn</ta>
            <ta e="T200" id="Seg_2174" s="T199">bütej</ta>
            <ta e="T201" id="Seg_2175" s="T200">hetiː</ta>
            <ta e="T202" id="Seg_2176" s="T201">ol-ttAn</ta>
            <ta e="T203" id="Seg_2177" s="T202">dʼaktar-tI-n</ta>
            <ta e="T204" id="Seg_2178" s="T203">beje-tI-n</ta>
            <ta e="T205" id="Seg_2179" s="T204">hetiː-tA</ta>
            <ta e="T309" id="Seg_2180" s="T205">hɨrga-tA</ta>
            <ta e="T208" id="Seg_2181" s="T207">ɨraːk</ta>
            <ta e="T209" id="Seg_2182" s="T208">kös-GA</ta>
            <ta e="T210" id="Seg_2183" s="T209">üs</ta>
            <ta e="T211" id="Seg_2184" s="T210">taba-nI</ta>
            <ta e="T213" id="Seg_2185" s="T211">kölüj-Ar-LAr</ta>
            <ta e="T298" id="Seg_2186" s="T213">a</ta>
            <ta e="T214" id="Seg_2187" s="T298">čugas</ta>
            <ta e="T215" id="Seg_2188" s="T214">kös-GA</ta>
            <ta e="T216" id="Seg_2189" s="T215">ikki</ta>
            <ta e="T217" id="Seg_2190" s="T216">taba-nI</ta>
            <ta e="T219" id="Seg_2191" s="T217">kölüj-Ar-LAr</ta>
            <ta e="T220" id="Seg_2192" s="T219">ol-ttAn</ta>
            <ta e="T221" id="Seg_2193" s="T220">ol</ta>
            <ta e="T222" id="Seg_2194" s="T221">dʼaktar-I-ŋ</ta>
            <ta e="T223" id="Seg_2195" s="T222">balok-tI-n</ta>
            <ta e="T224" id="Seg_2196" s="T223">kelin-tI-ttAn</ta>
            <ta e="T225" id="Seg_2197" s="T224">bu͡o</ta>
            <ta e="T226" id="Seg_2198" s="T225">güːle</ta>
            <ta e="T227" id="Seg_2199" s="T226">balok</ta>
            <ta e="T228" id="Seg_2200" s="T227">baːr</ta>
            <ta e="T232" id="Seg_2201" s="T231">kim-LAː-AːččI</ta>
            <ta e="T233" id="Seg_2202" s="T232">kepteri</ta>
            <ta e="T234" id="Seg_2203" s="T233">güːle</ta>
            <ta e="T235" id="Seg_2204" s="T234">di͡e-AːččI-BIt</ta>
            <ta e="T236" id="Seg_2205" s="T235">onno</ta>
            <ta e="T240" id="Seg_2206" s="T239">ol-ttAn</ta>
            <ta e="T241" id="Seg_2207" s="T240">ol</ta>
            <ta e="T242" id="Seg_2208" s="T241">güːle</ta>
            <ta e="T243" id="Seg_2209" s="T242">kelin-tI-ttAn</ta>
            <ta e="T246" id="Seg_2210" s="T245">itte</ta>
            <ta e="T247" id="Seg_2211" s="T246">bütte</ta>
            <ta e="T248" id="Seg_2212" s="T247">ka</ta>
            <ta e="T249" id="Seg_2213" s="T248">araŋas-nI</ta>
            <ta e="T251" id="Seg_2214" s="T250">tart-TAr-A-GIn</ta>
            <ta e="T252" id="Seg_2215" s="T251">baːj-A-GIn</ta>
            <ta e="T253" id="Seg_2216" s="T252">bu͡o</ta>
            <ta e="T254" id="Seg_2217" s="T253">ɨt-LAr-GI-n</ta>
            <ta e="T310" id="Seg_2218" s="T254">baːj-A-GIn</ta>
            <ta e="T256" id="Seg_2219" s="T255">ol-ttAn</ta>
            <ta e="T257" id="Seg_2220" s="T256">kös-A</ta>
            <ta e="T301" id="Seg_2221" s="T257">tur-A-GIn</ta>
            <ta e="T259" id="Seg_2222" s="T258">ulakan</ta>
            <ta e="T260" id="Seg_2223" s="T259">argiš</ta>
            <ta e="T261" id="Seg_2224" s="T260">bu͡ol-TI-tA</ta>
            <ta e="T262" id="Seg_2225" s="T261">ol-ttAn</ta>
            <ta e="T264" id="Seg_2226" s="T263">elbek</ta>
            <ta e="T265" id="Seg_2227" s="T264">kihi</ta>
            <ta e="T302" id="Seg_2228" s="T265">ke</ta>
            <ta e="T268" id="Seg_2229" s="T267">üs</ta>
            <ta e="T269" id="Seg_2230" s="T268">itte</ta>
            <ta e="T270" id="Seg_2231" s="T269">ɨ͡al</ta>
            <ta e="T271" id="Seg_2232" s="T270">bu͡ollagɨna</ta>
            <ta e="T272" id="Seg_2233" s="T271">ka</ta>
            <ta e="T273" id="Seg_2234" s="T272">uhun</ta>
            <ta e="T274" id="Seg_2235" s="T273">bagajɨ</ta>
            <ta e="T275" id="Seg_2236" s="T274">bu͡ol-AːččI</ta>
            <ta e="T303" id="Seg_2237" s="T275">ol-tI-ŋ</ta>
            <ta e="T277" id="Seg_2238" s="T276">ol-ttAn</ta>
            <ta e="T278" id="Seg_2239" s="T277">biːr</ta>
            <ta e="T279" id="Seg_2240" s="T278">kihi-ŋ</ta>
            <ta e="T280" id="Seg_2241" s="T279">ü͡ör</ta>
            <ta e="T281" id="Seg_2242" s="T280">ü͡ör-nI</ta>
            <ta e="T282" id="Seg_2243" s="T281">üːr-IAK-LAːK</ta>
            <ta e="T283" id="Seg_2244" s="T282">bu͡o</ta>
            <ta e="T284" id="Seg_2245" s="T283">ü͡ör-nI</ta>
            <ta e="T285" id="Seg_2246" s="T284">üːr-Ar</ta>
            <ta e="T286" id="Seg_2247" s="T285">biːr</ta>
            <ta e="T304" id="Seg_2248" s="T286">kihi</ta>
            <ta e="T288" id="Seg_2249" s="T287">ü͡ör-nI</ta>
            <ta e="T289" id="Seg_2250" s="T288">üːr-Ar</ta>
            <ta e="T290" id="Seg_2251" s="T289">ol-tI-ŋ</ta>
            <ta e="T291" id="Seg_2252" s="T290">bu</ta>
            <ta e="T292" id="Seg_2253" s="T291">aːt-tA</ta>
            <ta e="T293" id="Seg_2254" s="T292">ulakan</ta>
            <ta e="T294" id="Seg_2255" s="T293">argiš</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2256" s="T0">nomadize-PRS-1PL</ta>
            <ta e="T2" id="Seg_2257" s="T1">then</ta>
            <ta e="T3" id="Seg_2258" s="T2">different-1PL-ACC</ta>
            <ta e="T4" id="Seg_2259" s="T3">every-INTNS.[NOM]</ta>
            <ta e="T305" id="Seg_2260" s="T4">prepare-REFL-PRS-1PL</ta>
            <ta e="T6" id="Seg_2261" s="T5">AFFIRM</ta>
            <ta e="T7" id="Seg_2262" s="T6">house-1PL-ACC</ta>
            <ta e="T8" id="Seg_2263" s="T7">dig-EP-REFL-PRS-1PL</ta>
            <ta e="T9" id="Seg_2264" s="T8">deep.snow-PROPR</ta>
            <ta e="T11" id="Seg_2265" s="T10">house-1PL.[NOM]</ta>
            <ta e="T12" id="Seg_2266" s="T11">balok-1PL.[NOM]</ta>
            <ta e="T13" id="Seg_2267" s="T12">deep.snow-PROPR</ta>
            <ta e="T14" id="Seg_2268" s="T13">EMPH</ta>
            <ta e="T15" id="Seg_2269" s="T14">house-3PL-ACC</ta>
            <ta e="T16" id="Seg_2270" s="T15">deep.snow-PROPR</ta>
            <ta e="T17" id="Seg_2271" s="T16">dig-EP-REFL-PTCP.FUT-3PL-ACC</ta>
            <ta e="T307" id="Seg_2272" s="T17">need.to</ta>
            <ta e="T19" id="Seg_2273" s="T18">that-ABL</ta>
            <ta e="T20" id="Seg_2274" s="T19">woman-PL.[NOM]</ta>
            <ta e="T21" id="Seg_2275" s="T20">though</ta>
            <ta e="T22" id="Seg_2276" s="T21">house.[NOM]</ta>
            <ta e="T23" id="Seg_2277" s="T22">inside-3SG-ACC</ta>
            <ta e="T306" id="Seg_2278" s="T23">clean.up-VBZ-PRS-3PL</ta>
            <ta e="T25" id="Seg_2279" s="T24">this</ta>
            <ta e="T26" id="Seg_2280" s="T25">well</ta>
            <ta e="T34" id="Seg_2281" s="T33">every-DIM-PL-3SG.[NOM]</ta>
            <ta e="T35" id="Seg_2282" s="T34">that-ABL</ta>
            <ta e="T36" id="Seg_2283" s="T35">herd-ACC</ta>
            <ta e="T308" id="Seg_2284" s="T36">bring-PRS-3PL</ta>
            <ta e="T40" id="Seg_2285" s="T39">there</ta>
            <ta e="T41" id="Seg_2286" s="T40">reindeer.[NOM]</ta>
            <ta e="T42" id="Seg_2287" s="T41">catch-PRS-3PL</ta>
            <ta e="T43" id="Seg_2288" s="T42">reindeer.[NOM]</ta>
            <ta e="T44" id="Seg_2289" s="T43">catch-PRS-1PL</ta>
            <ta e="T45" id="Seg_2290" s="T44">EMPH</ta>
            <ta e="T46" id="Seg_2291" s="T45">then</ta>
            <ta e="T47" id="Seg_2292" s="T46">though</ta>
            <ta e="T48" id="Seg_2293" s="T47">balok-1PL-DAT/LOC</ta>
            <ta e="T49" id="Seg_2294" s="T48">eight</ta>
            <ta e="T50" id="Seg_2295" s="T49">reindeer-ACC</ta>
            <ta e="T51" id="Seg_2296" s="T50">harness-PRS-1PL</ta>
            <ta e="T52" id="Seg_2297" s="T51">eight</ta>
            <ta e="T53" id="Seg_2298" s="T52">reindeer-ACC</ta>
            <ta e="T54" id="Seg_2299" s="T53">grab-PRS-3PL</ta>
            <ta e="T55" id="Seg_2300" s="T54">big</ta>
            <ta e="T56" id="Seg_2301" s="T55">reindeer-PL.[NOM]</ta>
            <ta e="T57" id="Seg_2302" s="T56">need.to</ta>
            <ta e="T58" id="Seg_2303" s="T57">grab-PTCP.FUT-DAT/LOC</ta>
            <ta e="T59" id="Seg_2304" s="T58">balok-DAT/LOC</ta>
            <ta e="T60" id="Seg_2305" s="T59">well</ta>
            <ta e="T61" id="Seg_2306" s="T60">that-ABL</ta>
            <ta e="T62" id="Seg_2307" s="T61">balok.[NOM]</ta>
            <ta e="T63" id="Seg_2308" s="T62">front-3SG-DAT/LOC</ta>
            <ta e="T64" id="Seg_2309" s="T63">though</ta>
            <ta e="T65" id="Seg_2310" s="T64">EMPH</ta>
            <ta e="T66" id="Seg_2311" s="T65">still</ta>
            <ta e="T67" id="Seg_2312" s="T66">and</ta>
            <ta e="T68" id="Seg_2313" s="T67">sledge.[NOM]</ta>
            <ta e="T69" id="Seg_2314" s="T68">there.is</ta>
            <ta e="T70" id="Seg_2315" s="T69">that</ta>
            <ta e="T71" id="Seg_2316" s="T70">sledge.[NOM]</ta>
            <ta e="T72" id="Seg_2317" s="T71">EMPH</ta>
            <ta e="T73" id="Seg_2318" s="T72">different-ACC</ta>
            <ta e="T74" id="Seg_2319" s="T73">every-3SG-ACC</ta>
            <ta e="T75" id="Seg_2320" s="T74">load-PRS-3PL</ta>
            <ta e="T76" id="Seg_2321" s="T75">this</ta>
            <ta e="T77" id="Seg_2322" s="T76">well</ta>
            <ta e="T78" id="Seg_2323" s="T77">load-VBZ-PRS-3PL</ta>
            <ta e="T79" id="Seg_2324" s="T78">wood-PL-ACC</ta>
            <ta e="T80" id="Seg_2325" s="T79">maybe</ta>
            <ta e="T81" id="Seg_2326" s="T80">barrel-PL-ACC</ta>
            <ta e="T82" id="Seg_2327" s="T81">maybe</ta>
            <ta e="T84" id="Seg_2328" s="T83">coal-ACC</ta>
            <ta e="T85" id="Seg_2329" s="T84">different</ta>
            <ta e="T86" id="Seg_2330" s="T85">lay-PRS-3PL</ta>
            <ta e="T87" id="Seg_2331" s="T86">EMPH</ta>
            <ta e="T88" id="Seg_2332" s="T87">different</ta>
            <ta e="T89" id="Seg_2333" s="T88">different</ta>
            <ta e="T90" id="Seg_2334" s="T89">what.[NOM]</ta>
            <ta e="T91" id="Seg_2335" s="T90">stay-PST2.[3SG]</ta>
            <ta e="T92" id="Seg_2336" s="T91">who-ACC</ta>
            <ta e="T93" id="Seg_2337" s="T92">every-INTNS.[NOM]</ta>
            <ta e="T94" id="Seg_2338" s="T93">there</ta>
            <ta e="T95" id="Seg_2339" s="T94">load-PRS-3PL</ta>
            <ta e="T96" id="Seg_2340" s="T95">there</ta>
            <ta e="T97" id="Seg_2341" s="T96">four</ta>
            <ta e="T98" id="Seg_2342" s="T97">reindeer-ACC</ta>
            <ta e="T99" id="Seg_2343" s="T98">harness-PRS-3PL</ta>
            <ta e="T100" id="Seg_2344" s="T99">four</ta>
            <ta e="T101" id="Seg_2345" s="T100">reindeer-ACC</ta>
            <ta e="T102" id="Seg_2346" s="T101">harness-PRS-3PL</ta>
            <ta e="T103" id="Seg_2347" s="T102">that-ABL</ta>
            <ta e="T104" id="Seg_2348" s="T103">front</ta>
            <ta e="T105" id="Seg_2349" s="T104">sledge.[NOM]</ta>
            <ta e="T106" id="Seg_2350" s="T105">though</ta>
            <ta e="T108" id="Seg_2351" s="T107">man.of.the.house-EP-2SG.[NOM]</ta>
            <ta e="T109" id="Seg_2352" s="T108">self-3SG-GEN</ta>
            <ta e="T110" id="Seg_2353" s="T109">sledge-3SG.[NOM]</ta>
            <ta e="T111" id="Seg_2354" s="T110">there</ta>
            <ta e="T112" id="Seg_2355" s="T111">EMPH</ta>
            <ta e="T113" id="Seg_2356" s="T112">also</ta>
            <ta e="T114" id="Seg_2357" s="T113">INTJ</ta>
            <ta e="T115" id="Seg_2358" s="T114">coal.[NOM]</ta>
            <ta e="T117" id="Seg_2359" s="T116">lay-PTCP.FUT-2SG-ACC</ta>
            <ta e="T118" id="Seg_2360" s="T117">EMPH</ta>
            <ta e="T295" id="Seg_2361" s="T118">and</ta>
            <ta e="T119" id="Seg_2362" s="T295">there</ta>
            <ta e="T120" id="Seg_2363" s="T119">place.for.chopping.wood.[NOM]</ta>
            <ta e="T121" id="Seg_2364" s="T120">say-HAB-1PL</ta>
            <ta e="T122" id="Seg_2365" s="T121">wood.[NOM]</ta>
            <ta e="T123" id="Seg_2366" s="T122">chop.wood-ITER-HAB-1PL</ta>
            <ta e="T129" id="Seg_2367" s="T128">there</ta>
            <ta e="T130" id="Seg_2368" s="T129">place.for.chopping.wood-PL.[NOM]</ta>
            <ta e="T131" id="Seg_2369" s="T130">place.for.chopping.wood.[NOM]</ta>
            <ta e="T132" id="Seg_2370" s="T131">place.for.chopping.wood.[NOM]</ta>
            <ta e="T133" id="Seg_2371" s="T132">well</ta>
            <ta e="T134" id="Seg_2372" s="T133">that-ACC</ta>
            <ta e="T135" id="Seg_2373" s="T134">that-ACC</ta>
            <ta e="T136" id="Seg_2374" s="T135">different</ta>
            <ta e="T139" id="Seg_2375" s="T138">load-PRS-3PL</ta>
            <ta e="T140" id="Seg_2376" s="T139">sledge-DAT/LOC</ta>
            <ta e="T141" id="Seg_2377" s="T140">well</ta>
            <ta e="T142" id="Seg_2378" s="T141">well</ta>
            <ta e="T143" id="Seg_2379" s="T142">that-ABL</ta>
            <ta e="T144" id="Seg_2380" s="T143">that.[NOM]</ta>
            <ta e="T145" id="Seg_2381" s="T144">man.of.the.house.[NOM]</ta>
            <ta e="T146" id="Seg_2382" s="T145">well</ta>
            <ta e="T147" id="Seg_2383" s="T146">who.[NOM]</ta>
            <ta e="T148" id="Seg_2384" s="T147">who.[NOM]</ta>
            <ta e="T149" id="Seg_2385" s="T148">sledge-PROPR</ta>
            <ta e="T150" id="Seg_2386" s="T149">reindeer-3SG.[NOM]</ta>
            <ta e="T151" id="Seg_2387" s="T150">be-PRS.[3SG]</ta>
            <ta e="T152" id="Seg_2388" s="T151">that-ABL</ta>
            <ta e="T153" id="Seg_2389" s="T152">woman-EP-2SG.[NOM]</ta>
            <ta e="T154" id="Seg_2390" s="T153">also</ta>
            <ta e="T155" id="Seg_2391" s="T154">woman-3SG-DAT/LOC</ta>
            <ta e="T156" id="Seg_2392" s="T155">individually</ta>
            <ta e="T157" id="Seg_2393" s="T156">need.to</ta>
            <ta e="T158" id="Seg_2394" s="T157">again</ta>
            <ta e="T159" id="Seg_2395" s="T158">small</ta>
            <ta e="T160" id="Seg_2396" s="T159">balok.[NOM]</ta>
            <ta e="T161" id="Seg_2397" s="T160">there.is</ta>
            <ta e="T163" id="Seg_2398" s="T162">that-DAT/LOC</ta>
            <ta e="T164" id="Seg_2399" s="T163">four</ta>
            <ta e="T165" id="Seg_2400" s="T164">reindeer-ACC</ta>
            <ta e="T166" id="Seg_2401" s="T165">harness-PRS-3PL</ta>
            <ta e="T167" id="Seg_2402" s="T166">that-ABL</ta>
            <ta e="T171" id="Seg_2403" s="T170">front-DAT/LOC</ta>
            <ta e="T172" id="Seg_2404" s="T171">EMPH</ta>
            <ta e="T173" id="Seg_2405" s="T172">again</ta>
            <ta e="T174" id="Seg_2406" s="T173">sledge.[NOM]</ta>
            <ta e="T175" id="Seg_2407" s="T174">there.is</ta>
            <ta e="T176" id="Seg_2408" s="T175">there</ta>
            <ta e="T177" id="Seg_2409" s="T176">two</ta>
            <ta e="T178" id="Seg_2410" s="T177">reindeer-ACC</ta>
            <ta e="T179" id="Seg_2411" s="T178">harness-PRS-3PL</ta>
            <ta e="T180" id="Seg_2412" s="T179">former</ta>
            <ta e="T181" id="Seg_2413" s="T180">though</ta>
            <ta e="T182" id="Seg_2414" s="T181">closed</ta>
            <ta e="T183" id="Seg_2415" s="T182">sledge.[NOM]</ta>
            <ta e="T184" id="Seg_2416" s="T183">say-HAB-3PL</ta>
            <ta e="T185" id="Seg_2417" s="T296">there</ta>
            <ta e="T186" id="Seg_2418" s="T185">different</ta>
            <ta e="T187" id="Seg_2419" s="T186">various</ta>
            <ta e="T188" id="Seg_2420" s="T187">clothes-PL-3SG-GEN</ta>
            <ta e="T189" id="Seg_2421" s="T188">stay-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T190" id="Seg_2422" s="T189">EMPH</ta>
            <ta e="T191" id="Seg_2423" s="T190">closed</ta>
            <ta e="T192" id="Seg_2424" s="T191">sledge-DAT/LOC</ta>
            <ta e="T194" id="Seg_2425" s="T192">well</ta>
            <ta e="T195" id="Seg_2426" s="T194">clothes-PL-2SG-ACC</ta>
            <ta e="T196" id="Seg_2427" s="T195">good</ta>
            <ta e="T197" id="Seg_2428" s="T196">food-PL-2SG-ACC</ta>
            <ta e="T198" id="Seg_2429" s="T197">thither</ta>
            <ta e="T199" id="Seg_2430" s="T198">stick-PRS-2SG</ta>
            <ta e="T200" id="Seg_2431" s="T199">closed</ta>
            <ta e="T201" id="Seg_2432" s="T200">sledge.[NOM]</ta>
            <ta e="T202" id="Seg_2433" s="T201">that-ABL</ta>
            <ta e="T203" id="Seg_2434" s="T202">woman-3SG-GEN</ta>
            <ta e="T204" id="Seg_2435" s="T203">self-3SG-GEN</ta>
            <ta e="T205" id="Seg_2436" s="T204">sledge-3SG.[NOM]</ta>
            <ta e="T309" id="Seg_2437" s="T205">sledge-3SG.[NOM]</ta>
            <ta e="T208" id="Seg_2438" s="T207">distant.[NOM]</ta>
            <ta e="T209" id="Seg_2439" s="T208">reindeer.caravan-DAT/LOC</ta>
            <ta e="T210" id="Seg_2440" s="T209">three</ta>
            <ta e="T211" id="Seg_2441" s="T210">reindeer-ACC</ta>
            <ta e="T213" id="Seg_2442" s="T211">harness-PRS-3PL</ta>
            <ta e="T298" id="Seg_2443" s="T213">and</ta>
            <ta e="T214" id="Seg_2444" s="T298">close.[NOM]</ta>
            <ta e="T215" id="Seg_2445" s="T214">reindeer.caravan-DAT/LOC</ta>
            <ta e="T216" id="Seg_2446" s="T215">two</ta>
            <ta e="T217" id="Seg_2447" s="T216">reindeer-ACC</ta>
            <ta e="T219" id="Seg_2448" s="T217">harness-PRS-3PL</ta>
            <ta e="T220" id="Seg_2449" s="T219">that-ABL</ta>
            <ta e="T221" id="Seg_2450" s="T220">that</ta>
            <ta e="T222" id="Seg_2451" s="T221">woman-EP-2SG.[NOM]</ta>
            <ta e="T223" id="Seg_2452" s="T222">balok-3SG-GEN</ta>
            <ta e="T224" id="Seg_2453" s="T223">back-3SG-ABL</ta>
            <ta e="T225" id="Seg_2454" s="T224">EMPH</ta>
            <ta e="T226" id="Seg_2455" s="T225">entryway.[NOM]</ta>
            <ta e="T227" id="Seg_2456" s="T226">balok.[NOM]</ta>
            <ta e="T228" id="Seg_2457" s="T227">there.is</ta>
            <ta e="T232" id="Seg_2458" s="T231">who-VBZ-PTCP.HAB</ta>
            <ta e="T233" id="Seg_2459" s="T232">pantry.balok.[NOM]</ta>
            <ta e="T234" id="Seg_2460" s="T233">entryway.[NOM]</ta>
            <ta e="T235" id="Seg_2461" s="T234">say-HAB-1PL</ta>
            <ta e="T236" id="Seg_2462" s="T235">there</ta>
            <ta e="T240" id="Seg_2463" s="T239">that-ABL</ta>
            <ta e="T241" id="Seg_2464" s="T240">that</ta>
            <ta e="T242" id="Seg_2465" s="T241">entryway.[NOM]</ta>
            <ta e="T243" id="Seg_2466" s="T242">back-3SG-ABL</ta>
            <ta e="T246" id="Seg_2467" s="T245">different</ta>
            <ta e="T247" id="Seg_2468" s="T246">various</ta>
            <ta e="T248" id="Seg_2469" s="T247">well</ta>
            <ta e="T249" id="Seg_2470" s="T248">storage-ACC</ta>
            <ta e="T251" id="Seg_2471" s="T250">pull-CAUS-PRS-2SG</ta>
            <ta e="T252" id="Seg_2472" s="T251">tie-PRS-2SG</ta>
            <ta e="T253" id="Seg_2473" s="T252">EMPH</ta>
            <ta e="T254" id="Seg_2474" s="T253">dog-PL-2SG-ACC</ta>
            <ta e="T310" id="Seg_2475" s="T254">tie-PRS-2SG</ta>
            <ta e="T256" id="Seg_2476" s="T255">that-ABL</ta>
            <ta e="T257" id="Seg_2477" s="T256">nomadize-CVB.SIM</ta>
            <ta e="T301" id="Seg_2478" s="T257">stand.up-PRS-2SG</ta>
            <ta e="T259" id="Seg_2479" s="T258">big</ta>
            <ta e="T260" id="Seg_2480" s="T259">argish.[NOM]</ta>
            <ta e="T261" id="Seg_2481" s="T260">be-PST1-3SG</ta>
            <ta e="T262" id="Seg_2482" s="T261">that-ABL</ta>
            <ta e="T264" id="Seg_2483" s="T263">many</ta>
            <ta e="T265" id="Seg_2484" s="T264">human.being.[NOM]</ta>
            <ta e="T302" id="Seg_2485" s="T265">well</ta>
            <ta e="T268" id="Seg_2486" s="T267">three</ta>
            <ta e="T269" id="Seg_2487" s="T268">different</ta>
            <ta e="T270" id="Seg_2488" s="T269">family.[NOM]</ta>
            <ta e="T271" id="Seg_2489" s="T270">though</ta>
            <ta e="T272" id="Seg_2490" s="T271">well</ta>
            <ta e="T273" id="Seg_2491" s="T272">long</ta>
            <ta e="T274" id="Seg_2492" s="T273">very</ta>
            <ta e="T275" id="Seg_2493" s="T274">be-HAB.[3SG]</ta>
            <ta e="T303" id="Seg_2494" s="T275">that-3SG-2SG.[NOM]</ta>
            <ta e="T277" id="Seg_2495" s="T276">that-ABL</ta>
            <ta e="T278" id="Seg_2496" s="T277">one</ta>
            <ta e="T279" id="Seg_2497" s="T278">human.being-2SG.[NOM]</ta>
            <ta e="T280" id="Seg_2498" s="T279">herd.[NOM]</ta>
            <ta e="T281" id="Seg_2499" s="T280">herd-ACC</ta>
            <ta e="T282" id="Seg_2500" s="T281">drive-FUT-NEC.[3SG]</ta>
            <ta e="T283" id="Seg_2501" s="T282">EMPH</ta>
            <ta e="T284" id="Seg_2502" s="T283">herd-ACC</ta>
            <ta e="T285" id="Seg_2503" s="T284">drive-PRS.[3SG]</ta>
            <ta e="T286" id="Seg_2504" s="T285">one</ta>
            <ta e="T304" id="Seg_2505" s="T286">human.being.[NOM]</ta>
            <ta e="T288" id="Seg_2506" s="T287">herd-ACC</ta>
            <ta e="T289" id="Seg_2507" s="T288">drive-PRS.[3SG]</ta>
            <ta e="T290" id="Seg_2508" s="T289">that-3SG-2SG.[NOM]</ta>
            <ta e="T291" id="Seg_2509" s="T290">this</ta>
            <ta e="T292" id="Seg_2510" s="T291">name-3SG.[NOM]</ta>
            <ta e="T293" id="Seg_2511" s="T292">big</ta>
            <ta e="T294" id="Seg_2512" s="T293">argish.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_2513" s="T0">nomadisieren-PRS-1PL</ta>
            <ta e="T2" id="Seg_2514" s="T1">dann</ta>
            <ta e="T3" id="Seg_2515" s="T2">verschieden-1PL-ACC</ta>
            <ta e="T4" id="Seg_2516" s="T3">jeder-INTNS.[NOM]</ta>
            <ta e="T305" id="Seg_2517" s="T4">vorbereiten-REFL-PRS-1PL</ta>
            <ta e="T6" id="Seg_2518" s="T5">AFFIRM</ta>
            <ta e="T7" id="Seg_2519" s="T6">Haus-1PL-ACC</ta>
            <ta e="T8" id="Seg_2520" s="T7">graben-EP-REFL-PRS-1PL</ta>
            <ta e="T9" id="Seg_2521" s="T8">tiefer.Schnee-PROPR</ta>
            <ta e="T11" id="Seg_2522" s="T10">Haus-1PL.[NOM]</ta>
            <ta e="T12" id="Seg_2523" s="T11">Balok-1PL.[NOM]</ta>
            <ta e="T13" id="Seg_2524" s="T12">tiefer.Schnee-PROPR</ta>
            <ta e="T14" id="Seg_2525" s="T13">EMPH</ta>
            <ta e="T15" id="Seg_2526" s="T14">Haus-3PL-ACC</ta>
            <ta e="T16" id="Seg_2527" s="T15">tiefer.Schnee-PROPR</ta>
            <ta e="T17" id="Seg_2528" s="T16">graben-EP-REFL-PTCP.FUT-3PL-ACC</ta>
            <ta e="T307" id="Seg_2529" s="T17">man.muss</ta>
            <ta e="T19" id="Seg_2530" s="T18">jenes-ABL</ta>
            <ta e="T20" id="Seg_2531" s="T19">Frau-PL.[NOM]</ta>
            <ta e="T21" id="Seg_2532" s="T20">aber</ta>
            <ta e="T22" id="Seg_2533" s="T21">Haus.[NOM]</ta>
            <ta e="T23" id="Seg_2534" s="T22">Inneres-3SG-ACC</ta>
            <ta e="T306" id="Seg_2535" s="T23">aufraeumen-VBZ-PRS-3PL</ta>
            <ta e="T25" id="Seg_2536" s="T24">dieses</ta>
            <ta e="T26" id="Seg_2537" s="T25">nun</ta>
            <ta e="T34" id="Seg_2538" s="T33">jeder-DIM-PL-3SG.[NOM]</ta>
            <ta e="T35" id="Seg_2539" s="T34">jenes-ABL</ta>
            <ta e="T36" id="Seg_2540" s="T35">Herde-ACC</ta>
            <ta e="T308" id="Seg_2541" s="T36">bringen-PRS-3PL</ta>
            <ta e="T40" id="Seg_2542" s="T39">dort</ta>
            <ta e="T41" id="Seg_2543" s="T40">Rentier.[NOM]</ta>
            <ta e="T42" id="Seg_2544" s="T41">ergreifen-PRS-3PL</ta>
            <ta e="T43" id="Seg_2545" s="T42">Rentier.[NOM]</ta>
            <ta e="T44" id="Seg_2546" s="T43">ergreifen-PRS-1PL</ta>
            <ta e="T45" id="Seg_2547" s="T44">EMPH</ta>
            <ta e="T46" id="Seg_2548" s="T45">dann</ta>
            <ta e="T47" id="Seg_2549" s="T46">aber</ta>
            <ta e="T48" id="Seg_2550" s="T47">Balok-1PL-DAT/LOC</ta>
            <ta e="T49" id="Seg_2551" s="T48">acht</ta>
            <ta e="T50" id="Seg_2552" s="T49">Rentier-ACC</ta>
            <ta e="T51" id="Seg_2553" s="T50">einspannen-PRS-1PL</ta>
            <ta e="T52" id="Seg_2554" s="T51">acht</ta>
            <ta e="T53" id="Seg_2555" s="T52">Rentier-ACC</ta>
            <ta e="T54" id="Seg_2556" s="T53">greifen-PRS-3PL</ta>
            <ta e="T55" id="Seg_2557" s="T54">groß</ta>
            <ta e="T56" id="Seg_2558" s="T55">Rentier-PL.[NOM]</ta>
            <ta e="T57" id="Seg_2559" s="T56">man.muss</ta>
            <ta e="T58" id="Seg_2560" s="T57">greifen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T59" id="Seg_2561" s="T58">Balok-DAT/LOC</ta>
            <ta e="T60" id="Seg_2562" s="T59">nun</ta>
            <ta e="T61" id="Seg_2563" s="T60">jenes-ABL</ta>
            <ta e="T62" id="Seg_2564" s="T61">Balok.[NOM]</ta>
            <ta e="T63" id="Seg_2565" s="T62">Vorderteil-3SG-DAT/LOC</ta>
            <ta e="T64" id="Seg_2566" s="T63">aber</ta>
            <ta e="T65" id="Seg_2567" s="T64">EMPH</ta>
            <ta e="T66" id="Seg_2568" s="T65">noch</ta>
            <ta e="T67" id="Seg_2569" s="T66">und</ta>
            <ta e="T68" id="Seg_2570" s="T67">Schlitten.[NOM]</ta>
            <ta e="T69" id="Seg_2571" s="T68">es.gibt</ta>
            <ta e="T70" id="Seg_2572" s="T69">jenes</ta>
            <ta e="T71" id="Seg_2573" s="T70">Schlitten.[NOM]</ta>
            <ta e="T72" id="Seg_2574" s="T71">EMPH</ta>
            <ta e="T73" id="Seg_2575" s="T72">verschieden-ACC</ta>
            <ta e="T74" id="Seg_2576" s="T73">jeder-3SG-ACC</ta>
            <ta e="T75" id="Seg_2577" s="T74">einladen-PRS-3PL</ta>
            <ta e="T76" id="Seg_2578" s="T75">dieses</ta>
            <ta e="T77" id="Seg_2579" s="T76">nun</ta>
            <ta e="T78" id="Seg_2580" s="T77">laden-VBZ-PRS-3PL</ta>
            <ta e="T79" id="Seg_2581" s="T78">Holz-PL-ACC</ta>
            <ta e="T80" id="Seg_2582" s="T79">vielleicht</ta>
            <ta e="T81" id="Seg_2583" s="T80">Fass-PL-ACC</ta>
            <ta e="T82" id="Seg_2584" s="T81">vielleicht</ta>
            <ta e="T84" id="Seg_2585" s="T83">Kohle-ACC</ta>
            <ta e="T85" id="Seg_2586" s="T84">verschieden</ta>
            <ta e="T86" id="Seg_2587" s="T85">legen-PRS-3PL</ta>
            <ta e="T87" id="Seg_2588" s="T86">EMPH</ta>
            <ta e="T88" id="Seg_2589" s="T87">verschieden</ta>
            <ta e="T89" id="Seg_2590" s="T88">verschieden</ta>
            <ta e="T90" id="Seg_2591" s="T89">was.[NOM]</ta>
            <ta e="T91" id="Seg_2592" s="T90">bleiben-PST2.[3SG]</ta>
            <ta e="T92" id="Seg_2593" s="T91">wer-ACC</ta>
            <ta e="T93" id="Seg_2594" s="T92">jeder-INTNS.[NOM]</ta>
            <ta e="T94" id="Seg_2595" s="T93">dort</ta>
            <ta e="T95" id="Seg_2596" s="T94">einladen-PRS-3PL</ta>
            <ta e="T96" id="Seg_2597" s="T95">dort</ta>
            <ta e="T97" id="Seg_2598" s="T96">vier</ta>
            <ta e="T98" id="Seg_2599" s="T97">Rentier-ACC</ta>
            <ta e="T99" id="Seg_2600" s="T98">einspannen-PRS-3PL</ta>
            <ta e="T100" id="Seg_2601" s="T99">vier</ta>
            <ta e="T101" id="Seg_2602" s="T100">Rentier-ACC</ta>
            <ta e="T102" id="Seg_2603" s="T101">einspannen-PRS-3PL</ta>
            <ta e="T103" id="Seg_2604" s="T102">jenes-ABL</ta>
            <ta e="T104" id="Seg_2605" s="T103">vorderer</ta>
            <ta e="T105" id="Seg_2606" s="T104">Schlitten.[NOM]</ta>
            <ta e="T106" id="Seg_2607" s="T105">aber</ta>
            <ta e="T108" id="Seg_2608" s="T107">Hausherr-EP-2SG.[NOM]</ta>
            <ta e="T109" id="Seg_2609" s="T108">selbst-3SG-GEN</ta>
            <ta e="T110" id="Seg_2610" s="T109">Schlitten-3SG.[NOM]</ta>
            <ta e="T111" id="Seg_2611" s="T110">dort</ta>
            <ta e="T112" id="Seg_2612" s="T111">EMPH</ta>
            <ta e="T113" id="Seg_2613" s="T112">auch</ta>
            <ta e="T114" id="Seg_2614" s="T113">INTJ</ta>
            <ta e="T115" id="Seg_2615" s="T114">Kohle.[NOM]</ta>
            <ta e="T117" id="Seg_2616" s="T116">legen-PTCP.FUT-2SG-ACC</ta>
            <ta e="T118" id="Seg_2617" s="T117">EMPH</ta>
            <ta e="T295" id="Seg_2618" s="T118">und</ta>
            <ta e="T119" id="Seg_2619" s="T295">dort</ta>
            <ta e="T120" id="Seg_2620" s="T119">Platz.zum.Holzhacken.[NOM]</ta>
            <ta e="T121" id="Seg_2621" s="T120">sagen-HAB-1PL</ta>
            <ta e="T122" id="Seg_2622" s="T121">Holz.[NOM]</ta>
            <ta e="T123" id="Seg_2623" s="T122">Holz.hacken-ITER-HAB-1PL</ta>
            <ta e="T129" id="Seg_2624" s="T128">dort</ta>
            <ta e="T130" id="Seg_2625" s="T129">Platz.zum.Holzhacken-PL.[NOM]</ta>
            <ta e="T131" id="Seg_2626" s="T130">Platz.zum.Holzhacken.[NOM]</ta>
            <ta e="T132" id="Seg_2627" s="T131">Platz.zum.Holzhacken.[NOM]</ta>
            <ta e="T133" id="Seg_2628" s="T132">doch</ta>
            <ta e="T134" id="Seg_2629" s="T133">jenes-ACC</ta>
            <ta e="T135" id="Seg_2630" s="T134">jenes-ACC</ta>
            <ta e="T136" id="Seg_2631" s="T135">verschieden</ta>
            <ta e="T139" id="Seg_2632" s="T138">einladen-PRS-3PL</ta>
            <ta e="T140" id="Seg_2633" s="T139">Schlitten-DAT/LOC</ta>
            <ta e="T141" id="Seg_2634" s="T140">nun</ta>
            <ta e="T142" id="Seg_2635" s="T141">doch</ta>
            <ta e="T143" id="Seg_2636" s="T142">jenes-ABL</ta>
            <ta e="T144" id="Seg_2637" s="T143">dieses.[NOM]</ta>
            <ta e="T145" id="Seg_2638" s="T144">Hausherr.[NOM]</ta>
            <ta e="T146" id="Seg_2639" s="T145">nun</ta>
            <ta e="T147" id="Seg_2640" s="T146">wer.[NOM]</ta>
            <ta e="T148" id="Seg_2641" s="T147">wer.[NOM]</ta>
            <ta e="T149" id="Seg_2642" s="T148">Schlitten-PROPR</ta>
            <ta e="T150" id="Seg_2643" s="T149">Rentier-3SG.[NOM]</ta>
            <ta e="T151" id="Seg_2644" s="T150">sein-PRS.[3SG]</ta>
            <ta e="T152" id="Seg_2645" s="T151">jenes-ABL</ta>
            <ta e="T153" id="Seg_2646" s="T152">Frau-EP-2SG.[NOM]</ta>
            <ta e="T154" id="Seg_2647" s="T153">auch</ta>
            <ta e="T155" id="Seg_2648" s="T154">Frau-3SG-DAT/LOC</ta>
            <ta e="T156" id="Seg_2649" s="T155">einzeln</ta>
            <ta e="T157" id="Seg_2650" s="T156">man.muss</ta>
            <ta e="T158" id="Seg_2651" s="T157">wieder</ta>
            <ta e="T159" id="Seg_2652" s="T158">klein</ta>
            <ta e="T160" id="Seg_2653" s="T159">Balok.[NOM]</ta>
            <ta e="T161" id="Seg_2654" s="T160">es.gibt</ta>
            <ta e="T163" id="Seg_2655" s="T162">jenes-DAT/LOC</ta>
            <ta e="T164" id="Seg_2656" s="T163">vier</ta>
            <ta e="T165" id="Seg_2657" s="T164">Rentier-ACC</ta>
            <ta e="T166" id="Seg_2658" s="T165">einspannen-PRS-3PL</ta>
            <ta e="T167" id="Seg_2659" s="T166">jenes-ABL</ta>
            <ta e="T171" id="Seg_2660" s="T170">Vorderteil-DAT/LOC</ta>
            <ta e="T172" id="Seg_2661" s="T171">EMPH</ta>
            <ta e="T173" id="Seg_2662" s="T172">wieder</ta>
            <ta e="T174" id="Seg_2663" s="T173">Schlitten.[NOM]</ta>
            <ta e="T175" id="Seg_2664" s="T174">es.gibt</ta>
            <ta e="T176" id="Seg_2665" s="T175">dort</ta>
            <ta e="T177" id="Seg_2666" s="T176">zwei</ta>
            <ta e="T178" id="Seg_2667" s="T177">Rentier-ACC</ta>
            <ta e="T179" id="Seg_2668" s="T178">einspannen-PRS-3PL</ta>
            <ta e="T180" id="Seg_2669" s="T179">früher</ta>
            <ta e="T181" id="Seg_2670" s="T180">aber</ta>
            <ta e="T182" id="Seg_2671" s="T181">geschlossen</ta>
            <ta e="T183" id="Seg_2672" s="T182">Schlitten.[NOM]</ta>
            <ta e="T184" id="Seg_2673" s="T183">sagen-HAB-3PL</ta>
            <ta e="T185" id="Seg_2674" s="T296">dort</ta>
            <ta e="T186" id="Seg_2675" s="T185">verschieden</ta>
            <ta e="T187" id="Seg_2676" s="T186">verschieden</ta>
            <ta e="T188" id="Seg_2677" s="T187">Kleidung-PL-3SG-GEN</ta>
            <ta e="T189" id="Seg_2678" s="T188">bleiben-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T190" id="Seg_2679" s="T189">EMPH</ta>
            <ta e="T191" id="Seg_2680" s="T190">geschlossen</ta>
            <ta e="T192" id="Seg_2681" s="T191">Schlitten-DAT/LOC</ta>
            <ta e="T194" id="Seg_2682" s="T192">nun</ta>
            <ta e="T195" id="Seg_2683" s="T194">Kleidung-PL-2SG-ACC</ta>
            <ta e="T196" id="Seg_2684" s="T195">gut</ta>
            <ta e="T197" id="Seg_2685" s="T196">Nahrung-PL-2SG-ACC</ta>
            <ta e="T198" id="Seg_2686" s="T197">dorthin</ta>
            <ta e="T199" id="Seg_2687" s="T198">stecken-PRS-2SG</ta>
            <ta e="T200" id="Seg_2688" s="T199">geschlossen</ta>
            <ta e="T201" id="Seg_2689" s="T200">Schlitten.[NOM]</ta>
            <ta e="T202" id="Seg_2690" s="T201">jenes-ABL</ta>
            <ta e="T203" id="Seg_2691" s="T202">Frau-3SG-GEN</ta>
            <ta e="T204" id="Seg_2692" s="T203">selbst-3SG-GEN</ta>
            <ta e="T205" id="Seg_2693" s="T204">Schlitten-3SG.[NOM]</ta>
            <ta e="T309" id="Seg_2694" s="T205">Schlitten-3SG.[NOM]</ta>
            <ta e="T208" id="Seg_2695" s="T207">fern.[NOM]</ta>
            <ta e="T209" id="Seg_2696" s="T208">Rentierkarawane-DAT/LOC</ta>
            <ta e="T210" id="Seg_2697" s="T209">drei</ta>
            <ta e="T211" id="Seg_2698" s="T210">Rentier-ACC</ta>
            <ta e="T213" id="Seg_2699" s="T211">einspannen-PRS-3PL</ta>
            <ta e="T298" id="Seg_2700" s="T213">und</ta>
            <ta e="T214" id="Seg_2701" s="T298">nah.[NOM]</ta>
            <ta e="T215" id="Seg_2702" s="T214">Rentierkarawane-DAT/LOC</ta>
            <ta e="T216" id="Seg_2703" s="T215">zwei</ta>
            <ta e="T217" id="Seg_2704" s="T216">Rentier-ACC</ta>
            <ta e="T219" id="Seg_2705" s="T217">einspannen-PRS-3PL</ta>
            <ta e="T220" id="Seg_2706" s="T219">jenes-ABL</ta>
            <ta e="T221" id="Seg_2707" s="T220">jenes</ta>
            <ta e="T222" id="Seg_2708" s="T221">Frau-EP-2SG.[NOM]</ta>
            <ta e="T223" id="Seg_2709" s="T222">Balok-3SG-GEN</ta>
            <ta e="T224" id="Seg_2710" s="T223">Hinterteil-3SG-ABL</ta>
            <ta e="T225" id="Seg_2711" s="T224">EMPH</ta>
            <ta e="T226" id="Seg_2712" s="T225">Diele.[NOM]</ta>
            <ta e="T227" id="Seg_2713" s="T226">Balok.[NOM]</ta>
            <ta e="T228" id="Seg_2714" s="T227">es.gibt</ta>
            <ta e="T232" id="Seg_2715" s="T231">wer-VBZ-PTCP.HAB</ta>
            <ta e="T233" id="Seg_2716" s="T232">Vorratsbalok.[NOM]</ta>
            <ta e="T234" id="Seg_2717" s="T233">Diele.[NOM]</ta>
            <ta e="T235" id="Seg_2718" s="T234">sagen-HAB-1PL</ta>
            <ta e="T236" id="Seg_2719" s="T235">dort</ta>
            <ta e="T240" id="Seg_2720" s="T239">jenes-ABL</ta>
            <ta e="T241" id="Seg_2721" s="T240">jenes</ta>
            <ta e="T242" id="Seg_2722" s="T241">Diele.[NOM]</ta>
            <ta e="T243" id="Seg_2723" s="T242">Hinterteil-3SG-ABL</ta>
            <ta e="T246" id="Seg_2724" s="T245">verschieden</ta>
            <ta e="T247" id="Seg_2725" s="T246">verschieden</ta>
            <ta e="T248" id="Seg_2726" s="T247">nun</ta>
            <ta e="T249" id="Seg_2727" s="T248">Speicher-ACC</ta>
            <ta e="T251" id="Seg_2728" s="T250">ziehen-CAUS-PRS-2SG</ta>
            <ta e="T252" id="Seg_2729" s="T251">binden-PRS-2SG</ta>
            <ta e="T253" id="Seg_2730" s="T252">EMPH</ta>
            <ta e="T254" id="Seg_2731" s="T253">Hund-PL-2SG-ACC</ta>
            <ta e="T310" id="Seg_2732" s="T254">binden-PRS-2SG</ta>
            <ta e="T256" id="Seg_2733" s="T255">jenes-ABL</ta>
            <ta e="T257" id="Seg_2734" s="T256">nomadisieren-CVB.SIM</ta>
            <ta e="T301" id="Seg_2735" s="T257">aufstehen-PRS-2SG</ta>
            <ta e="T259" id="Seg_2736" s="T258">groß</ta>
            <ta e="T260" id="Seg_2737" s="T259">Argisch.[NOM]</ta>
            <ta e="T261" id="Seg_2738" s="T260">sein-PST1-3SG</ta>
            <ta e="T262" id="Seg_2739" s="T261">jenes-ABL</ta>
            <ta e="T264" id="Seg_2740" s="T263">viel</ta>
            <ta e="T265" id="Seg_2741" s="T264">Mensch.[NOM]</ta>
            <ta e="T302" id="Seg_2742" s="T265">nun</ta>
            <ta e="T268" id="Seg_2743" s="T267">drei</ta>
            <ta e="T269" id="Seg_2744" s="T268">verschieden</ta>
            <ta e="T270" id="Seg_2745" s="T269">Familie.[NOM]</ta>
            <ta e="T271" id="Seg_2746" s="T270">aber</ta>
            <ta e="T272" id="Seg_2747" s="T271">nun</ta>
            <ta e="T273" id="Seg_2748" s="T272">lang</ta>
            <ta e="T274" id="Seg_2749" s="T273">sehr</ta>
            <ta e="T275" id="Seg_2750" s="T274">sein-HAB.[3SG]</ta>
            <ta e="T303" id="Seg_2751" s="T275">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T277" id="Seg_2752" s="T276">jenes-ABL</ta>
            <ta e="T278" id="Seg_2753" s="T277">eins</ta>
            <ta e="T279" id="Seg_2754" s="T278">Mensch-2SG.[NOM]</ta>
            <ta e="T280" id="Seg_2755" s="T279">Herde.[NOM]</ta>
            <ta e="T281" id="Seg_2756" s="T280">Herde-ACC</ta>
            <ta e="T282" id="Seg_2757" s="T281">treiben-FUT-NEC.[3SG]</ta>
            <ta e="T283" id="Seg_2758" s="T282">EMPH</ta>
            <ta e="T284" id="Seg_2759" s="T283">Herde-ACC</ta>
            <ta e="T285" id="Seg_2760" s="T284">treiben-PRS.[3SG]</ta>
            <ta e="T286" id="Seg_2761" s="T285">eins</ta>
            <ta e="T304" id="Seg_2762" s="T286">Mensch.[NOM]</ta>
            <ta e="T288" id="Seg_2763" s="T287">Herde-ACC</ta>
            <ta e="T289" id="Seg_2764" s="T288">treiben-PRS.[3SG]</ta>
            <ta e="T290" id="Seg_2765" s="T289">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T291" id="Seg_2766" s="T290">dieses</ta>
            <ta e="T292" id="Seg_2767" s="T291">Name-3SG.[NOM]</ta>
            <ta e="T293" id="Seg_2768" s="T292">groß</ta>
            <ta e="T294" id="Seg_2769" s="T293">Argisch.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2770" s="T0">кочевать-PRS-1PL</ta>
            <ta e="T2" id="Seg_2771" s="T1">тогда</ta>
            <ta e="T3" id="Seg_2772" s="T2">разный-1PL-ACC</ta>
            <ta e="T4" id="Seg_2773" s="T3">каждый-INTNS.[NOM]</ta>
            <ta e="T305" id="Seg_2774" s="T4">готовить-REFL-PRS-1PL</ta>
            <ta e="T6" id="Seg_2775" s="T5">AFFIRM</ta>
            <ta e="T7" id="Seg_2776" s="T6">дом-1PL-ACC</ta>
            <ta e="T8" id="Seg_2777" s="T7">копать-EP-REFL-PRS-1PL</ta>
            <ta e="T9" id="Seg_2778" s="T8">глубокий.снег-PROPR</ta>
            <ta e="T11" id="Seg_2779" s="T10">дом-1PL.[NOM]</ta>
            <ta e="T12" id="Seg_2780" s="T11">балок-1PL.[NOM]</ta>
            <ta e="T13" id="Seg_2781" s="T12">глубокий.снег-PROPR</ta>
            <ta e="T14" id="Seg_2782" s="T13">EMPH</ta>
            <ta e="T15" id="Seg_2783" s="T14">дом-3PL-ACC</ta>
            <ta e="T16" id="Seg_2784" s="T15">глубокий.снег-PROPR</ta>
            <ta e="T17" id="Seg_2785" s="T16">копать-EP-REFL-PTCP.FUT-3PL-ACC</ta>
            <ta e="T307" id="Seg_2786" s="T17">надо</ta>
            <ta e="T19" id="Seg_2787" s="T18">тот-ABL</ta>
            <ta e="T20" id="Seg_2788" s="T19">жена-PL.[NOM]</ta>
            <ta e="T21" id="Seg_2789" s="T20">однако</ta>
            <ta e="T22" id="Seg_2790" s="T21">дом.[NOM]</ta>
            <ta e="T23" id="Seg_2791" s="T22">нутро-3SG-ACC</ta>
            <ta e="T306" id="Seg_2792" s="T23">убирать-VBZ-PRS-3PL</ta>
            <ta e="T25" id="Seg_2793" s="T24">этот</ta>
            <ta e="T26" id="Seg_2794" s="T25">вот</ta>
            <ta e="T34" id="Seg_2795" s="T33">каждый-DIM-PL-3SG.[NOM]</ta>
            <ta e="T35" id="Seg_2796" s="T34">тот-ABL</ta>
            <ta e="T36" id="Seg_2797" s="T35">стадо-ACC</ta>
            <ta e="T308" id="Seg_2798" s="T36">принести-PRS-3PL</ta>
            <ta e="T40" id="Seg_2799" s="T39">там</ta>
            <ta e="T41" id="Seg_2800" s="T40">олень.[NOM]</ta>
            <ta e="T42" id="Seg_2801" s="T41">поймать-PRS-3PL</ta>
            <ta e="T43" id="Seg_2802" s="T42">олень.[NOM]</ta>
            <ta e="T44" id="Seg_2803" s="T43">поймать-PRS-1PL</ta>
            <ta e="T45" id="Seg_2804" s="T44">EMPH</ta>
            <ta e="T46" id="Seg_2805" s="T45">тогда</ta>
            <ta e="T47" id="Seg_2806" s="T46">однако</ta>
            <ta e="T48" id="Seg_2807" s="T47">балок-1PL-DAT/LOC</ta>
            <ta e="T49" id="Seg_2808" s="T48">восемь</ta>
            <ta e="T50" id="Seg_2809" s="T49">олень-ACC</ta>
            <ta e="T51" id="Seg_2810" s="T50">запрячь-PRS-1PL</ta>
            <ta e="T52" id="Seg_2811" s="T51">восемь</ta>
            <ta e="T53" id="Seg_2812" s="T52">олень-ACC</ta>
            <ta e="T54" id="Seg_2813" s="T53">хватать-PRS-3PL</ta>
            <ta e="T55" id="Seg_2814" s="T54">большой</ta>
            <ta e="T56" id="Seg_2815" s="T55">олень-PL.[NOM]</ta>
            <ta e="T57" id="Seg_2816" s="T56">надо</ta>
            <ta e="T58" id="Seg_2817" s="T57">хватать-PTCP.FUT-DAT/LOC</ta>
            <ta e="T59" id="Seg_2818" s="T58">балок-DAT/LOC</ta>
            <ta e="T60" id="Seg_2819" s="T59">вот</ta>
            <ta e="T61" id="Seg_2820" s="T60">тот-ABL</ta>
            <ta e="T62" id="Seg_2821" s="T61">балок.[NOM]</ta>
            <ta e="T63" id="Seg_2822" s="T62">передняя.часть-3SG-DAT/LOC</ta>
            <ta e="T64" id="Seg_2823" s="T63">однако</ta>
            <ta e="T65" id="Seg_2824" s="T64">EMPH</ta>
            <ta e="T66" id="Seg_2825" s="T65">еще</ta>
            <ta e="T67" id="Seg_2826" s="T66">да</ta>
            <ta e="T68" id="Seg_2827" s="T67">нарта.[NOM]</ta>
            <ta e="T69" id="Seg_2828" s="T68">есть</ta>
            <ta e="T70" id="Seg_2829" s="T69">тот</ta>
            <ta e="T71" id="Seg_2830" s="T70">нарта.[NOM]</ta>
            <ta e="T72" id="Seg_2831" s="T71">EMPH</ta>
            <ta e="T73" id="Seg_2832" s="T72">разный-ACC</ta>
            <ta e="T74" id="Seg_2833" s="T73">каждый-3SG-ACC</ta>
            <ta e="T75" id="Seg_2834" s="T74">грузить-PRS-3PL</ta>
            <ta e="T76" id="Seg_2835" s="T75">этот</ta>
            <ta e="T77" id="Seg_2836" s="T76">вот</ta>
            <ta e="T78" id="Seg_2837" s="T77">грузить-VBZ-PRS-3PL</ta>
            <ta e="T79" id="Seg_2838" s="T78">дерево-PL-ACC</ta>
            <ta e="T80" id="Seg_2839" s="T79">может</ta>
            <ta e="T81" id="Seg_2840" s="T80">бочка-PL-ACC</ta>
            <ta e="T82" id="Seg_2841" s="T81">может</ta>
            <ta e="T84" id="Seg_2842" s="T83">уголь-ACC</ta>
            <ta e="T85" id="Seg_2843" s="T84">разный</ta>
            <ta e="T86" id="Seg_2844" s="T85">класть-PRS-3PL</ta>
            <ta e="T87" id="Seg_2845" s="T86">EMPH</ta>
            <ta e="T88" id="Seg_2846" s="T87">разный</ta>
            <ta e="T89" id="Seg_2847" s="T88">разный</ta>
            <ta e="T90" id="Seg_2848" s="T89">что.[NOM]</ta>
            <ta e="T91" id="Seg_2849" s="T90">оставаться-PST2.[3SG]</ta>
            <ta e="T92" id="Seg_2850" s="T91">кто-ACC</ta>
            <ta e="T93" id="Seg_2851" s="T92">каждый-INTNS.[NOM]</ta>
            <ta e="T94" id="Seg_2852" s="T93">там</ta>
            <ta e="T95" id="Seg_2853" s="T94">грузить-PRS-3PL</ta>
            <ta e="T96" id="Seg_2854" s="T95">там</ta>
            <ta e="T97" id="Seg_2855" s="T96">четыре</ta>
            <ta e="T98" id="Seg_2856" s="T97">олень-ACC</ta>
            <ta e="T99" id="Seg_2857" s="T98">запрячь-PRS-3PL</ta>
            <ta e="T100" id="Seg_2858" s="T99">четыре</ta>
            <ta e="T101" id="Seg_2859" s="T100">олень-ACC</ta>
            <ta e="T102" id="Seg_2860" s="T101">запрячь-PRS-3PL</ta>
            <ta e="T103" id="Seg_2861" s="T102">тот-ABL</ta>
            <ta e="T104" id="Seg_2862" s="T103">передний</ta>
            <ta e="T105" id="Seg_2863" s="T104">сани.[NOM]</ta>
            <ta e="T106" id="Seg_2864" s="T105">однако</ta>
            <ta e="T108" id="Seg_2865" s="T107">хозяин-EP-2SG.[NOM]</ta>
            <ta e="T109" id="Seg_2866" s="T108">сам-3SG-GEN</ta>
            <ta e="T110" id="Seg_2867" s="T109">сани-3SG.[NOM]</ta>
            <ta e="T111" id="Seg_2868" s="T110">там</ta>
            <ta e="T112" id="Seg_2869" s="T111">EMPH</ta>
            <ta e="T113" id="Seg_2870" s="T112">тоже</ta>
            <ta e="T114" id="Seg_2871" s="T113">INTJ</ta>
            <ta e="T115" id="Seg_2872" s="T114">уголь.[NOM]</ta>
            <ta e="T117" id="Seg_2873" s="T116">класть-PTCP.FUT-2SG-ACC</ta>
            <ta e="T118" id="Seg_2874" s="T117">EMPH</ta>
            <ta e="T295" id="Seg_2875" s="T118">и</ta>
            <ta e="T119" id="Seg_2876" s="T295">там</ta>
            <ta e="T120" id="Seg_2877" s="T119">место.где.рубят.дрова.[NOM]</ta>
            <ta e="T121" id="Seg_2878" s="T120">говорить-HAB-1PL</ta>
            <ta e="T122" id="Seg_2879" s="T121">дерево.[NOM]</ta>
            <ta e="T123" id="Seg_2880" s="T122">рубить.дрова-ITER-HAB-1PL</ta>
            <ta e="T129" id="Seg_2881" s="T128">там</ta>
            <ta e="T130" id="Seg_2882" s="T129">место.где.рубят.дрова-PL.[NOM]</ta>
            <ta e="T131" id="Seg_2883" s="T130">место.где.рубят.дрова.[NOM]</ta>
            <ta e="T132" id="Seg_2884" s="T131">место.где.рубят.дрова.[NOM]</ta>
            <ta e="T133" id="Seg_2885" s="T132">вот</ta>
            <ta e="T134" id="Seg_2886" s="T133">тот-ACC</ta>
            <ta e="T135" id="Seg_2887" s="T134">тот-ACC</ta>
            <ta e="T136" id="Seg_2888" s="T135">разный</ta>
            <ta e="T139" id="Seg_2889" s="T138">грузить-PRS-3PL</ta>
            <ta e="T140" id="Seg_2890" s="T139">сани-DAT/LOC</ta>
            <ta e="T141" id="Seg_2891" s="T140">вот</ta>
            <ta e="T142" id="Seg_2892" s="T141">вот</ta>
            <ta e="T143" id="Seg_2893" s="T142">тот-ABL</ta>
            <ta e="T144" id="Seg_2894" s="T143">тот.[NOM]</ta>
            <ta e="T145" id="Seg_2895" s="T144">хозяин.[NOM]</ta>
            <ta e="T146" id="Seg_2896" s="T145">вот</ta>
            <ta e="T147" id="Seg_2897" s="T146">кто.[NOM]</ta>
            <ta e="T148" id="Seg_2898" s="T147">кто.[NOM]</ta>
            <ta e="T149" id="Seg_2899" s="T148">сани-PROPR</ta>
            <ta e="T150" id="Seg_2900" s="T149">олень-3SG.[NOM]</ta>
            <ta e="T151" id="Seg_2901" s="T150">быть-PRS.[3SG]</ta>
            <ta e="T152" id="Seg_2902" s="T151">тот-ABL</ta>
            <ta e="T153" id="Seg_2903" s="T152">жена-EP-2SG.[NOM]</ta>
            <ta e="T154" id="Seg_2904" s="T153">тоже</ta>
            <ta e="T155" id="Seg_2905" s="T154">жена-3SG-DAT/LOC</ta>
            <ta e="T156" id="Seg_2906" s="T155">отдельно</ta>
            <ta e="T157" id="Seg_2907" s="T156">надо</ta>
            <ta e="T158" id="Seg_2908" s="T157">опять</ta>
            <ta e="T159" id="Seg_2909" s="T158">маленький</ta>
            <ta e="T160" id="Seg_2910" s="T159">балок.[NOM]</ta>
            <ta e="T161" id="Seg_2911" s="T160">есть</ta>
            <ta e="T163" id="Seg_2912" s="T162">тот-DAT/LOC</ta>
            <ta e="T164" id="Seg_2913" s="T163">четыре</ta>
            <ta e="T165" id="Seg_2914" s="T164">олень-ACC</ta>
            <ta e="T166" id="Seg_2915" s="T165">запрячь-PRS-3PL</ta>
            <ta e="T167" id="Seg_2916" s="T166">тот-ABL</ta>
            <ta e="T171" id="Seg_2917" s="T170">передняя.часть-DAT/LOC</ta>
            <ta e="T172" id="Seg_2918" s="T171">EMPH</ta>
            <ta e="T173" id="Seg_2919" s="T172">опять</ta>
            <ta e="T174" id="Seg_2920" s="T173">сани.[NOM]</ta>
            <ta e="T175" id="Seg_2921" s="T174">есть</ta>
            <ta e="T176" id="Seg_2922" s="T175">там</ta>
            <ta e="T177" id="Seg_2923" s="T176">два</ta>
            <ta e="T178" id="Seg_2924" s="T177">олень-ACC</ta>
            <ta e="T179" id="Seg_2925" s="T178">запрячь-PRS-3PL</ta>
            <ta e="T180" id="Seg_2926" s="T179">прежний</ta>
            <ta e="T181" id="Seg_2927" s="T180">однако</ta>
            <ta e="T182" id="Seg_2928" s="T181">закрытый</ta>
            <ta e="T183" id="Seg_2929" s="T182">нарта.[NOM]</ta>
            <ta e="T184" id="Seg_2930" s="T183">говорить-HAB-3PL</ta>
            <ta e="T185" id="Seg_2931" s="T296">там</ta>
            <ta e="T186" id="Seg_2932" s="T185">разный</ta>
            <ta e="T187" id="Seg_2933" s="T186">разный</ta>
            <ta e="T188" id="Seg_2934" s="T187">одежда-PL-3SG-GEN</ta>
            <ta e="T189" id="Seg_2935" s="T188">оставаться-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T190" id="Seg_2936" s="T189">EMPH</ta>
            <ta e="T191" id="Seg_2937" s="T190">закрытый</ta>
            <ta e="T192" id="Seg_2938" s="T191">нарта-DAT/LOC</ta>
            <ta e="T194" id="Seg_2939" s="T192">вот</ta>
            <ta e="T195" id="Seg_2940" s="T194">одежда-PL-2SG-ACC</ta>
            <ta e="T196" id="Seg_2941" s="T195">хороший</ta>
            <ta e="T197" id="Seg_2942" s="T196">пища-PL-2SG-ACC</ta>
            <ta e="T198" id="Seg_2943" s="T197">туда</ta>
            <ta e="T199" id="Seg_2944" s="T198">вставлять-PRS-2SG</ta>
            <ta e="T200" id="Seg_2945" s="T199">закрытый</ta>
            <ta e="T201" id="Seg_2946" s="T200">нарта.[NOM]</ta>
            <ta e="T202" id="Seg_2947" s="T201">тот-ABL</ta>
            <ta e="T203" id="Seg_2948" s="T202">жена-3SG-GEN</ta>
            <ta e="T204" id="Seg_2949" s="T203">сам-3SG-GEN</ta>
            <ta e="T205" id="Seg_2950" s="T204">нарта-3SG.[NOM]</ta>
            <ta e="T309" id="Seg_2951" s="T205">сани-3SG.[NOM]</ta>
            <ta e="T208" id="Seg_2952" s="T207">далекий.[NOM]</ta>
            <ta e="T209" id="Seg_2953" s="T208">аргиш-DAT/LOC</ta>
            <ta e="T210" id="Seg_2954" s="T209">три</ta>
            <ta e="T211" id="Seg_2955" s="T210">олень-ACC</ta>
            <ta e="T213" id="Seg_2956" s="T211">запрячь-PRS-3PL</ta>
            <ta e="T298" id="Seg_2957" s="T213">а</ta>
            <ta e="T214" id="Seg_2958" s="T298">близкий.[NOM]</ta>
            <ta e="T215" id="Seg_2959" s="T214">аргиш-DAT/LOC</ta>
            <ta e="T216" id="Seg_2960" s="T215">два</ta>
            <ta e="T217" id="Seg_2961" s="T216">олень-ACC</ta>
            <ta e="T219" id="Seg_2962" s="T217">запрячь-PRS-3PL</ta>
            <ta e="T220" id="Seg_2963" s="T219">тот-ABL</ta>
            <ta e="T221" id="Seg_2964" s="T220">тот</ta>
            <ta e="T222" id="Seg_2965" s="T221">жена-EP-2SG.[NOM]</ta>
            <ta e="T223" id="Seg_2966" s="T222">балок-3SG-GEN</ta>
            <ta e="T224" id="Seg_2967" s="T223">задняя.часть-3SG-ABL</ta>
            <ta e="T225" id="Seg_2968" s="T224">EMPH</ta>
            <ta e="T226" id="Seg_2969" s="T225">сени.[NOM]</ta>
            <ta e="T227" id="Seg_2970" s="T226">балок.[NOM]</ta>
            <ta e="T228" id="Seg_2971" s="T227">есть</ta>
            <ta e="T232" id="Seg_2972" s="T231">кто-VBZ-PTCP.HAB</ta>
            <ta e="T233" id="Seg_2973" s="T232">пристройка.[NOM]</ta>
            <ta e="T234" id="Seg_2974" s="T233">сени.[NOM]</ta>
            <ta e="T235" id="Seg_2975" s="T234">говорить-HAB-1PL</ta>
            <ta e="T236" id="Seg_2976" s="T235">там</ta>
            <ta e="T240" id="Seg_2977" s="T239">тот-ABL</ta>
            <ta e="T241" id="Seg_2978" s="T240">тот</ta>
            <ta e="T242" id="Seg_2979" s="T241">сени.[NOM]</ta>
            <ta e="T243" id="Seg_2980" s="T242">задняя.часть-3SG-ABL</ta>
            <ta e="T246" id="Seg_2981" s="T245">разный</ta>
            <ta e="T247" id="Seg_2982" s="T246">разный</ta>
            <ta e="T248" id="Seg_2983" s="T247">вот</ta>
            <ta e="T249" id="Seg_2984" s="T248">лабаз-ACC</ta>
            <ta e="T251" id="Seg_2985" s="T250">тянуть-CAUS-PRS-2SG</ta>
            <ta e="T252" id="Seg_2986" s="T251">связывать-PRS-2SG</ta>
            <ta e="T253" id="Seg_2987" s="T252">EMPH</ta>
            <ta e="T254" id="Seg_2988" s="T253">собака-PL-2SG-ACC</ta>
            <ta e="T310" id="Seg_2989" s="T254">связывать-PRS-2SG</ta>
            <ta e="T256" id="Seg_2990" s="T255">тот-ABL</ta>
            <ta e="T257" id="Seg_2991" s="T256">кочевать-CVB.SIM</ta>
            <ta e="T301" id="Seg_2992" s="T257">вставать-PRS-2SG</ta>
            <ta e="T259" id="Seg_2993" s="T258">большой</ta>
            <ta e="T260" id="Seg_2994" s="T259">аргиш.[NOM]</ta>
            <ta e="T261" id="Seg_2995" s="T260">быть-PST1-3SG</ta>
            <ta e="T262" id="Seg_2996" s="T261">тот-ABL</ta>
            <ta e="T264" id="Seg_2997" s="T263">много</ta>
            <ta e="T265" id="Seg_2998" s="T264">человек.[NOM]</ta>
            <ta e="T302" id="Seg_2999" s="T265">вот</ta>
            <ta e="T268" id="Seg_3000" s="T267">три</ta>
            <ta e="T269" id="Seg_3001" s="T268">разный</ta>
            <ta e="T270" id="Seg_3002" s="T269">семья.[NOM]</ta>
            <ta e="T271" id="Seg_3003" s="T270">однако</ta>
            <ta e="T272" id="Seg_3004" s="T271">вот</ta>
            <ta e="T273" id="Seg_3005" s="T272">длинный</ta>
            <ta e="T274" id="Seg_3006" s="T273">очень</ta>
            <ta e="T275" id="Seg_3007" s="T274">быть-HAB.[3SG]</ta>
            <ta e="T303" id="Seg_3008" s="T275">тот-3SG-2SG.[NOM]</ta>
            <ta e="T277" id="Seg_3009" s="T276">тот-ABL</ta>
            <ta e="T278" id="Seg_3010" s="T277">один</ta>
            <ta e="T279" id="Seg_3011" s="T278">человек-2SG.[NOM]</ta>
            <ta e="T280" id="Seg_3012" s="T279">стадо.[NOM]</ta>
            <ta e="T281" id="Seg_3013" s="T280">стадо-ACC</ta>
            <ta e="T282" id="Seg_3014" s="T281">погонять-FUT-NEC.[3SG]</ta>
            <ta e="T283" id="Seg_3015" s="T282">EMPH</ta>
            <ta e="T284" id="Seg_3016" s="T283">стадо-ACC</ta>
            <ta e="T285" id="Seg_3017" s="T284">погонять-PRS.[3SG]</ta>
            <ta e="T286" id="Seg_3018" s="T285">один</ta>
            <ta e="T304" id="Seg_3019" s="T286">человек.[NOM]</ta>
            <ta e="T288" id="Seg_3020" s="T287">стадо-ACC</ta>
            <ta e="T289" id="Seg_3021" s="T288">погонять-PRS.[3SG]</ta>
            <ta e="T290" id="Seg_3022" s="T289">тот-3SG-2SG.[NOM]</ta>
            <ta e="T291" id="Seg_3023" s="T290">этот</ta>
            <ta e="T292" id="Seg_3024" s="T291">имя-3SG.[NOM]</ta>
            <ta e="T293" id="Seg_3025" s="T292">большой</ta>
            <ta e="T294" id="Seg_3026" s="T293">аргиш.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_3027" s="T0">v-v:tense-v:pred.pn</ta>
            <ta e="T2" id="Seg_3028" s="T1">adv</ta>
            <ta e="T3" id="Seg_3029" s="T2">adj-n:(poss)-n:case</ta>
            <ta e="T4" id="Seg_3030" s="T3">adj-adj&gt;adj-n:case</ta>
            <ta e="T305" id="Seg_3031" s="T4">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T6" id="Seg_3032" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_3033" s="T6">n-n:poss-n:case</ta>
            <ta e="T8" id="Seg_3034" s="T7">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T9" id="Seg_3035" s="T8">n-n&gt;adj</ta>
            <ta e="T11" id="Seg_3036" s="T10">n-n:(poss)-n:case</ta>
            <ta e="T12" id="Seg_3037" s="T11">n-n:(poss)-n:case</ta>
            <ta e="T13" id="Seg_3038" s="T12">n-n&gt;adj</ta>
            <ta e="T14" id="Seg_3039" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_3040" s="T14">n-n:poss-n:case</ta>
            <ta e="T16" id="Seg_3041" s="T15">n-n&gt;adj</ta>
            <ta e="T17" id="Seg_3042" s="T16">v-v:(ins)-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T307" id="Seg_3043" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_3044" s="T18">dempro-pro:case</ta>
            <ta e="T20" id="Seg_3045" s="T19">n-n:(num)-n:case</ta>
            <ta e="T21" id="Seg_3046" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_3047" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_3048" s="T22">n-n:poss-n:case</ta>
            <ta e="T306" id="Seg_3049" s="T23">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T25" id="Seg_3050" s="T24">dempro</ta>
            <ta e="T26" id="Seg_3051" s="T25">ptcl</ta>
            <ta e="T34" id="Seg_3052" s="T33">adj-n&gt;n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T35" id="Seg_3053" s="T34">dempro-pro:case</ta>
            <ta e="T36" id="Seg_3054" s="T35">n-n:case</ta>
            <ta e="T308" id="Seg_3055" s="T36">v-v:tense-v:pred.pn</ta>
            <ta e="T40" id="Seg_3056" s="T39">adv</ta>
            <ta e="T41" id="Seg_3057" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_3058" s="T41">v-v:tense-v:pred.pn</ta>
            <ta e="T43" id="Seg_3059" s="T42">n-n:case</ta>
            <ta e="T44" id="Seg_3060" s="T43">v-v:tense-v:pred.pn</ta>
            <ta e="T45" id="Seg_3061" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_3062" s="T45">adv</ta>
            <ta e="T47" id="Seg_3063" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_3064" s="T47">n-n:poss-n:case</ta>
            <ta e="T49" id="Seg_3065" s="T48">cardnum</ta>
            <ta e="T50" id="Seg_3066" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_3067" s="T50">v-v:tense-v:pred.pn</ta>
            <ta e="T52" id="Seg_3068" s="T51">cardnum</ta>
            <ta e="T53" id="Seg_3069" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_3070" s="T53">v-v:tense-v:pred.pn</ta>
            <ta e="T55" id="Seg_3071" s="T54">adj</ta>
            <ta e="T56" id="Seg_3072" s="T55">n-n:(num)-n:case</ta>
            <ta e="T57" id="Seg_3073" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_3074" s="T57">v-v:ptcp-v:(case)</ta>
            <ta e="T59" id="Seg_3075" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_3076" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_3077" s="T60">dempro-pro:case</ta>
            <ta e="T62" id="Seg_3078" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_3079" s="T62">n-n:poss-n:case</ta>
            <ta e="T64" id="Seg_3080" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_3081" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_3082" s="T65">adv</ta>
            <ta e="T67" id="Seg_3083" s="T66">conj</ta>
            <ta e="T68" id="Seg_3084" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_3085" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_3086" s="T69">dempro</ta>
            <ta e="T71" id="Seg_3087" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_3088" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_3089" s="T72">adj-n:case</ta>
            <ta e="T74" id="Seg_3090" s="T73">adj-n:poss-n:case</ta>
            <ta e="T75" id="Seg_3091" s="T74">v-v:tense-v:pred.pn</ta>
            <ta e="T76" id="Seg_3092" s="T75">dempro</ta>
            <ta e="T77" id="Seg_3093" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_3094" s="T77">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T79" id="Seg_3095" s="T78">n-n:(num)-n:case</ta>
            <ta e="T80" id="Seg_3096" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_3097" s="T80">n-n:(num)-n:case</ta>
            <ta e="T82" id="Seg_3098" s="T81">ptcl</ta>
            <ta e="T84" id="Seg_3099" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_3100" s="T84">adj</ta>
            <ta e="T86" id="Seg_3101" s="T85">v-v:tense-v:pred.pn</ta>
            <ta e="T87" id="Seg_3102" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_3103" s="T87">adj</ta>
            <ta e="T89" id="Seg_3104" s="T88">adj</ta>
            <ta e="T90" id="Seg_3105" s="T89">que-pro:case</ta>
            <ta e="T91" id="Seg_3106" s="T90">v-v:tense-v:pred.pn</ta>
            <ta e="T92" id="Seg_3107" s="T91">que-pro:case</ta>
            <ta e="T93" id="Seg_3108" s="T92">adj-adj&gt;adj-n:case</ta>
            <ta e="T94" id="Seg_3109" s="T93">adv</ta>
            <ta e="T95" id="Seg_3110" s="T94">v-v:tense-v:pred.pn</ta>
            <ta e="T96" id="Seg_3111" s="T95">adv</ta>
            <ta e="T97" id="Seg_3112" s="T96">cardnum</ta>
            <ta e="T98" id="Seg_3113" s="T97">n-n:case</ta>
            <ta e="T99" id="Seg_3114" s="T98">v-v:tense-v:pred.pn</ta>
            <ta e="T100" id="Seg_3115" s="T99">cardnum</ta>
            <ta e="T101" id="Seg_3116" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_3117" s="T101">v-v:tense-v:pred.pn</ta>
            <ta e="T103" id="Seg_3118" s="T102">dempro-pro:case</ta>
            <ta e="T104" id="Seg_3119" s="T103">adj</ta>
            <ta e="T105" id="Seg_3120" s="T104">n-n:case</ta>
            <ta e="T106" id="Seg_3121" s="T105">ptcl</ta>
            <ta e="T108" id="Seg_3122" s="T107">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T109" id="Seg_3123" s="T108">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T110" id="Seg_3124" s="T109">n-n:(poss)-n:case</ta>
            <ta e="T111" id="Seg_3125" s="T110">adv</ta>
            <ta e="T112" id="Seg_3126" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_3127" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_3128" s="T113">interj</ta>
            <ta e="T115" id="Seg_3129" s="T114">n.[n:case]</ta>
            <ta e="T117" id="Seg_3130" s="T116">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T118" id="Seg_3131" s="T117">ptcl</ta>
            <ta e="T295" id="Seg_3132" s="T118">conj</ta>
            <ta e="T119" id="Seg_3133" s="T295">adv</ta>
            <ta e="T120" id="Seg_3134" s="T119">n-n:case</ta>
            <ta e="T121" id="Seg_3135" s="T120">v-v:mood-v:pred.pn</ta>
            <ta e="T122" id="Seg_3136" s="T121">n-n:case</ta>
            <ta e="T123" id="Seg_3137" s="T122">v-v&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T129" id="Seg_3138" s="T128">adv</ta>
            <ta e="T130" id="Seg_3139" s="T129">n-n:(num)-n:case</ta>
            <ta e="T131" id="Seg_3140" s="T130">n-n:case</ta>
            <ta e="T132" id="Seg_3141" s="T131">n-n:case</ta>
            <ta e="T133" id="Seg_3142" s="T132">ptcl</ta>
            <ta e="T134" id="Seg_3143" s="T133">dempro-pro:case</ta>
            <ta e="T135" id="Seg_3144" s="T134">dempro-pro:case</ta>
            <ta e="T136" id="Seg_3145" s="T135">adj</ta>
            <ta e="T139" id="Seg_3146" s="T138">v-v:tense-v:pred.pn</ta>
            <ta e="T140" id="Seg_3147" s="T139">n-n:case</ta>
            <ta e="T141" id="Seg_3148" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_3149" s="T141">ptcl</ta>
            <ta e="T143" id="Seg_3150" s="T142">dempro-pro:case</ta>
            <ta e="T144" id="Seg_3151" s="T143">dempro-pro:case</ta>
            <ta e="T145" id="Seg_3152" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_3153" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_3154" s="T146">que-pro:case</ta>
            <ta e="T148" id="Seg_3155" s="T147">que-pro:case</ta>
            <ta e="T149" id="Seg_3156" s="T148">n-n&gt;adj</ta>
            <ta e="T150" id="Seg_3157" s="T149">n-n:(poss)-n:case</ta>
            <ta e="T151" id="Seg_3158" s="T150">v-v:tense-v:pred.pn</ta>
            <ta e="T152" id="Seg_3159" s="T151">dempro-pro:case</ta>
            <ta e="T153" id="Seg_3160" s="T152">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T154" id="Seg_3161" s="T153">ptcl</ta>
            <ta e="T155" id="Seg_3162" s="T154">n-n:poss-n:case</ta>
            <ta e="T156" id="Seg_3163" s="T155">adv</ta>
            <ta e="T157" id="Seg_3164" s="T156">ptcl</ta>
            <ta e="T158" id="Seg_3165" s="T157">ptcl</ta>
            <ta e="T159" id="Seg_3166" s="T158">adj</ta>
            <ta e="T160" id="Seg_3167" s="T159">n-n:case</ta>
            <ta e="T161" id="Seg_3168" s="T160">ptcl</ta>
            <ta e="T163" id="Seg_3169" s="T162">dempro-pro:case</ta>
            <ta e="T164" id="Seg_3170" s="T163">cardnum</ta>
            <ta e="T165" id="Seg_3171" s="T164">n-n:case</ta>
            <ta e="T166" id="Seg_3172" s="T165">v-v:tense-v:pred.pn</ta>
            <ta e="T167" id="Seg_3173" s="T166">dempro-pro:case</ta>
            <ta e="T171" id="Seg_3174" s="T170">n-n:case</ta>
            <ta e="T172" id="Seg_3175" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_3176" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_3177" s="T173">n-n:case</ta>
            <ta e="T175" id="Seg_3178" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_3179" s="T175">adv</ta>
            <ta e="T177" id="Seg_3180" s="T176">cardnum</ta>
            <ta e="T178" id="Seg_3181" s="T177">n-n:case</ta>
            <ta e="T179" id="Seg_3182" s="T178">v-v:tense-v:pred.pn</ta>
            <ta e="T180" id="Seg_3183" s="T179">adj</ta>
            <ta e="T181" id="Seg_3184" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_3185" s="T181">adj</ta>
            <ta e="T183" id="Seg_3186" s="T182">n-n:case</ta>
            <ta e="T184" id="Seg_3187" s="T183">v-v:mood-v:pred.pn</ta>
            <ta e="T185" id="Seg_3188" s="T296">adv</ta>
            <ta e="T186" id="Seg_3189" s="T185">adj</ta>
            <ta e="T187" id="Seg_3190" s="T186">adj</ta>
            <ta e="T188" id="Seg_3191" s="T187">n-n:(num)-n:poss-n:case</ta>
            <ta e="T189" id="Seg_3192" s="T188">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T190" id="Seg_3193" s="T189">ptcl</ta>
            <ta e="T191" id="Seg_3194" s="T190">adj</ta>
            <ta e="T192" id="Seg_3195" s="T191">n-n:case</ta>
            <ta e="T194" id="Seg_3196" s="T192">ptcl</ta>
            <ta e="T195" id="Seg_3197" s="T194">n-n:(num)-n:poss-n:case</ta>
            <ta e="T196" id="Seg_3198" s="T195">adj</ta>
            <ta e="T197" id="Seg_3199" s="T196">n-n:(num)-n:poss-n:case</ta>
            <ta e="T198" id="Seg_3200" s="T197">adv</ta>
            <ta e="T199" id="Seg_3201" s="T198">v-v:tense-v:pred.pn</ta>
            <ta e="T200" id="Seg_3202" s="T199">adj</ta>
            <ta e="T201" id="Seg_3203" s="T200">n-n:case</ta>
            <ta e="T202" id="Seg_3204" s="T201">dempro-pro:case</ta>
            <ta e="T203" id="Seg_3205" s="T202">n-n:poss-n:case</ta>
            <ta e="T204" id="Seg_3206" s="T203">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T205" id="Seg_3207" s="T204">n-n:(poss)-n:case</ta>
            <ta e="T309" id="Seg_3208" s="T205">n-n:(poss)-n:case</ta>
            <ta e="T208" id="Seg_3209" s="T207">adj-n:case</ta>
            <ta e="T209" id="Seg_3210" s="T208">n-n:case</ta>
            <ta e="T210" id="Seg_3211" s="T209">cardnum</ta>
            <ta e="T211" id="Seg_3212" s="T210">n-n:case</ta>
            <ta e="T213" id="Seg_3213" s="T211">v-v:tense-v:pred.pn</ta>
            <ta e="T298" id="Seg_3214" s="T213">conj</ta>
            <ta e="T214" id="Seg_3215" s="T298">adj-n:case</ta>
            <ta e="T215" id="Seg_3216" s="T214">n-n:case</ta>
            <ta e="T216" id="Seg_3217" s="T215">cardnum</ta>
            <ta e="T217" id="Seg_3218" s="T216">n-n:case</ta>
            <ta e="T219" id="Seg_3219" s="T217">v-v:tense-v:pred.pn</ta>
            <ta e="T220" id="Seg_3220" s="T219">dempro-pro:case</ta>
            <ta e="T221" id="Seg_3221" s="T220">dempro</ta>
            <ta e="T222" id="Seg_3222" s="T221">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T223" id="Seg_3223" s="T222">n-n:poss-n:case</ta>
            <ta e="T224" id="Seg_3224" s="T223">n-n:poss-n:case</ta>
            <ta e="T225" id="Seg_3225" s="T224">ptcl</ta>
            <ta e="T226" id="Seg_3226" s="T225">n-n:case</ta>
            <ta e="T227" id="Seg_3227" s="T226">n-n:case</ta>
            <ta e="T228" id="Seg_3228" s="T227">ptcl</ta>
            <ta e="T232" id="Seg_3229" s="T231">que-v&gt;v-v:ptcp</ta>
            <ta e="T233" id="Seg_3230" s="T232">n-n:case</ta>
            <ta e="T234" id="Seg_3231" s="T233">n-n:case</ta>
            <ta e="T235" id="Seg_3232" s="T234">v-v:mood-v:pred.pn</ta>
            <ta e="T236" id="Seg_3233" s="T235">adv</ta>
            <ta e="T240" id="Seg_3234" s="T239">dempro-pro:case</ta>
            <ta e="T241" id="Seg_3235" s="T240">dempro</ta>
            <ta e="T242" id="Seg_3236" s="T241">n-n:case</ta>
            <ta e="T243" id="Seg_3237" s="T242">n-n:poss-n:case</ta>
            <ta e="T246" id="Seg_3238" s="T245">adj</ta>
            <ta e="T247" id="Seg_3239" s="T246">adj</ta>
            <ta e="T248" id="Seg_3240" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_3241" s="T248">n-n:case</ta>
            <ta e="T251" id="Seg_3242" s="T250">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T252" id="Seg_3243" s="T251">v-v:tense-v:pred.pn</ta>
            <ta e="T253" id="Seg_3244" s="T252">ptcl</ta>
            <ta e="T254" id="Seg_3245" s="T253">n-n:(num)-n:poss-n:case</ta>
            <ta e="T310" id="Seg_3246" s="T254">v-v:tense-v:pred.pn</ta>
            <ta e="T256" id="Seg_3247" s="T255">dempro-pro:case</ta>
            <ta e="T257" id="Seg_3248" s="T256">v-v:cvb</ta>
            <ta e="T301" id="Seg_3249" s="T257">v-v:tense-v:pred.pn</ta>
            <ta e="T259" id="Seg_3250" s="T258">adj</ta>
            <ta e="T260" id="Seg_3251" s="T259">n-n:case</ta>
            <ta e="T261" id="Seg_3252" s="T260">v-v:tense-v:poss.pn</ta>
            <ta e="T262" id="Seg_3253" s="T261">dempro-pro:case</ta>
            <ta e="T264" id="Seg_3254" s="T263">quant</ta>
            <ta e="T265" id="Seg_3255" s="T264">n-n:case</ta>
            <ta e="T302" id="Seg_3256" s="T265">ptcl</ta>
            <ta e="T268" id="Seg_3257" s="T267">cardnum</ta>
            <ta e="T269" id="Seg_3258" s="T268">adj</ta>
            <ta e="T270" id="Seg_3259" s="T269">n-n:case</ta>
            <ta e="T271" id="Seg_3260" s="T270">ptcl</ta>
            <ta e="T272" id="Seg_3261" s="T271">ptcl</ta>
            <ta e="T273" id="Seg_3262" s="T272">adj</ta>
            <ta e="T274" id="Seg_3263" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_3264" s="T274">v-v:mood-v:pred.pn</ta>
            <ta e="T303" id="Seg_3265" s="T275">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T277" id="Seg_3266" s="T276">dempro-pro:case</ta>
            <ta e="T278" id="Seg_3267" s="T277">cardnum</ta>
            <ta e="T279" id="Seg_3268" s="T278">n-n:(poss)-n:case</ta>
            <ta e="T280" id="Seg_3269" s="T279">n-n:case</ta>
            <ta e="T281" id="Seg_3270" s="T280">n-n:case</ta>
            <ta e="T282" id="Seg_3271" s="T281">v-v:tense-v:mood-v:pred.pn</ta>
            <ta e="T283" id="Seg_3272" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_3273" s="T283">n-n:case</ta>
            <ta e="T285" id="Seg_3274" s="T284">v-v:tense-v:pred.pn</ta>
            <ta e="T286" id="Seg_3275" s="T285">cardnum</ta>
            <ta e="T304" id="Seg_3276" s="T286">n-n:case</ta>
            <ta e="T288" id="Seg_3277" s="T287">n-n:case</ta>
            <ta e="T289" id="Seg_3278" s="T288">v-v:tense-v:pred.pn</ta>
            <ta e="T290" id="Seg_3279" s="T289">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T291" id="Seg_3280" s="T290">dempro</ta>
            <ta e="T292" id="Seg_3281" s="T291">n-n:(poss)-n:case</ta>
            <ta e="T293" id="Seg_3282" s="T292">adj</ta>
            <ta e="T294" id="Seg_3283" s="T293">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_3284" s="T0">v</ta>
            <ta e="T2" id="Seg_3285" s="T1">adv</ta>
            <ta e="T3" id="Seg_3286" s="T2">adj</ta>
            <ta e="T4" id="Seg_3287" s="T3">adj</ta>
            <ta e="T305" id="Seg_3288" s="T4">v</ta>
            <ta e="T6" id="Seg_3289" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_3290" s="T6">n</ta>
            <ta e="T8" id="Seg_3291" s="T7">v</ta>
            <ta e="T9" id="Seg_3292" s="T8">adj</ta>
            <ta e="T11" id="Seg_3293" s="T10">n</ta>
            <ta e="T12" id="Seg_3294" s="T11">n</ta>
            <ta e="T13" id="Seg_3295" s="T12">adj</ta>
            <ta e="T14" id="Seg_3296" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_3297" s="T14">n</ta>
            <ta e="T16" id="Seg_3298" s="T15">adj</ta>
            <ta e="T17" id="Seg_3299" s="T16">v</ta>
            <ta e="T307" id="Seg_3300" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_3301" s="T18">dempro</ta>
            <ta e="T20" id="Seg_3302" s="T19">n</ta>
            <ta e="T21" id="Seg_3303" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_3304" s="T21">n</ta>
            <ta e="T23" id="Seg_3305" s="T22">n</ta>
            <ta e="T306" id="Seg_3306" s="T23">v</ta>
            <ta e="T25" id="Seg_3307" s="T24">dempro</ta>
            <ta e="T26" id="Seg_3308" s="T25">ptcl</ta>
            <ta e="T34" id="Seg_3309" s="T33">n</ta>
            <ta e="T35" id="Seg_3310" s="T34">dempro</ta>
            <ta e="T36" id="Seg_3311" s="T35">n</ta>
            <ta e="T308" id="Seg_3312" s="T36">v</ta>
            <ta e="T40" id="Seg_3313" s="T39">adv</ta>
            <ta e="T41" id="Seg_3314" s="T40">n</ta>
            <ta e="T42" id="Seg_3315" s="T41">v</ta>
            <ta e="T43" id="Seg_3316" s="T42">n</ta>
            <ta e="T44" id="Seg_3317" s="T43">v</ta>
            <ta e="T45" id="Seg_3318" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_3319" s="T45">adv</ta>
            <ta e="T47" id="Seg_3320" s="T46">ptcl</ta>
            <ta e="T48" id="Seg_3321" s="T47">n</ta>
            <ta e="T49" id="Seg_3322" s="T48">cardnum</ta>
            <ta e="T50" id="Seg_3323" s="T49">n</ta>
            <ta e="T51" id="Seg_3324" s="T50">v</ta>
            <ta e="T52" id="Seg_3325" s="T51">cardnum</ta>
            <ta e="T53" id="Seg_3326" s="T52">n</ta>
            <ta e="T54" id="Seg_3327" s="T53">v</ta>
            <ta e="T55" id="Seg_3328" s="T54">adj</ta>
            <ta e="T56" id="Seg_3329" s="T55">n</ta>
            <ta e="T57" id="Seg_3330" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_3331" s="T57">v</ta>
            <ta e="T59" id="Seg_3332" s="T58">n</ta>
            <ta e="T60" id="Seg_3333" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_3334" s="T60">dempro</ta>
            <ta e="T62" id="Seg_3335" s="T61">n</ta>
            <ta e="T63" id="Seg_3336" s="T62">n</ta>
            <ta e="T64" id="Seg_3337" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_3338" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_3339" s="T65">adv</ta>
            <ta e="T67" id="Seg_3340" s="T66">conj</ta>
            <ta e="T68" id="Seg_3341" s="T67">n</ta>
            <ta e="T69" id="Seg_3342" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_3343" s="T69">dempro</ta>
            <ta e="T71" id="Seg_3344" s="T70">n</ta>
            <ta e="T72" id="Seg_3345" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_3346" s="T72">adj</ta>
            <ta e="T74" id="Seg_3347" s="T73">adj</ta>
            <ta e="T75" id="Seg_3348" s="T74">v</ta>
            <ta e="T76" id="Seg_3349" s="T75">dempro</ta>
            <ta e="T77" id="Seg_3350" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_3351" s="T77">v</ta>
            <ta e="T79" id="Seg_3352" s="T78">n</ta>
            <ta e="T80" id="Seg_3353" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_3354" s="T80">n</ta>
            <ta e="T82" id="Seg_3355" s="T81">ptcl</ta>
            <ta e="T84" id="Seg_3356" s="T83">n</ta>
            <ta e="T85" id="Seg_3357" s="T84">adj</ta>
            <ta e="T86" id="Seg_3358" s="T85">v</ta>
            <ta e="T87" id="Seg_3359" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_3360" s="T87">adj</ta>
            <ta e="T89" id="Seg_3361" s="T88">adj</ta>
            <ta e="T90" id="Seg_3362" s="T89">que</ta>
            <ta e="T91" id="Seg_3363" s="T90">v</ta>
            <ta e="T92" id="Seg_3364" s="T91">que</ta>
            <ta e="T93" id="Seg_3365" s="T92">adj</ta>
            <ta e="T94" id="Seg_3366" s="T93">adv</ta>
            <ta e="T95" id="Seg_3367" s="T94">v</ta>
            <ta e="T96" id="Seg_3368" s="T95">adv</ta>
            <ta e="T97" id="Seg_3369" s="T96">cardnum</ta>
            <ta e="T98" id="Seg_3370" s="T97">n</ta>
            <ta e="T99" id="Seg_3371" s="T98">v</ta>
            <ta e="T100" id="Seg_3372" s="T99">cardnum</ta>
            <ta e="T101" id="Seg_3373" s="T100">n</ta>
            <ta e="T102" id="Seg_3374" s="T101">v</ta>
            <ta e="T103" id="Seg_3375" s="T102">dempro</ta>
            <ta e="T104" id="Seg_3376" s="T103">adj</ta>
            <ta e="T105" id="Seg_3377" s="T104">n</ta>
            <ta e="T106" id="Seg_3378" s="T105">ptcl</ta>
            <ta e="T108" id="Seg_3379" s="T107">n</ta>
            <ta e="T109" id="Seg_3380" s="T108">emphpro</ta>
            <ta e="T110" id="Seg_3381" s="T109">n</ta>
            <ta e="T111" id="Seg_3382" s="T110">adv</ta>
            <ta e="T112" id="Seg_3383" s="T111">v</ta>
            <ta e="T113" id="Seg_3384" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_3385" s="T113">interj</ta>
            <ta e="T115" id="Seg_3386" s="T114">n</ta>
            <ta e="T117" id="Seg_3387" s="T116">v</ta>
            <ta e="T118" id="Seg_3388" s="T117">ptcl</ta>
            <ta e="T295" id="Seg_3389" s="T118">conj</ta>
            <ta e="T119" id="Seg_3390" s="T295">adv</ta>
            <ta e="T120" id="Seg_3391" s="T119">n</ta>
            <ta e="T121" id="Seg_3392" s="T120">v</ta>
            <ta e="T122" id="Seg_3393" s="T121">n</ta>
            <ta e="T123" id="Seg_3394" s="T122">v</ta>
            <ta e="T129" id="Seg_3395" s="T128">adv</ta>
            <ta e="T130" id="Seg_3396" s="T129">n</ta>
            <ta e="T131" id="Seg_3397" s="T130">n</ta>
            <ta e="T132" id="Seg_3398" s="T131">n</ta>
            <ta e="T133" id="Seg_3399" s="T132">ptcl</ta>
            <ta e="T134" id="Seg_3400" s="T133">dempro</ta>
            <ta e="T135" id="Seg_3401" s="T134">dempro</ta>
            <ta e="T136" id="Seg_3402" s="T135">adj</ta>
            <ta e="T139" id="Seg_3403" s="T138">v</ta>
            <ta e="T140" id="Seg_3404" s="T139">n</ta>
            <ta e="T141" id="Seg_3405" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_3406" s="T141">ptcl</ta>
            <ta e="T143" id="Seg_3407" s="T142">dempro</ta>
            <ta e="T144" id="Seg_3408" s="T143">dempro</ta>
            <ta e="T145" id="Seg_3409" s="T144">n</ta>
            <ta e="T146" id="Seg_3410" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_3411" s="T146">que</ta>
            <ta e="T148" id="Seg_3412" s="T147">que</ta>
            <ta e="T149" id="Seg_3413" s="T148">adj</ta>
            <ta e="T150" id="Seg_3414" s="T149">n</ta>
            <ta e="T151" id="Seg_3415" s="T150">cop</ta>
            <ta e="T152" id="Seg_3416" s="T151">dempro</ta>
            <ta e="T153" id="Seg_3417" s="T152">n</ta>
            <ta e="T154" id="Seg_3418" s="T153">ptcl</ta>
            <ta e="T155" id="Seg_3419" s="T154">n</ta>
            <ta e="T156" id="Seg_3420" s="T155">adv</ta>
            <ta e="T157" id="Seg_3421" s="T156">ptcl</ta>
            <ta e="T158" id="Seg_3422" s="T157">ptcl</ta>
            <ta e="T159" id="Seg_3423" s="T158">adj</ta>
            <ta e="T160" id="Seg_3424" s="T159">n</ta>
            <ta e="T161" id="Seg_3425" s="T160">ptcl</ta>
            <ta e="T163" id="Seg_3426" s="T162">dempro</ta>
            <ta e="T164" id="Seg_3427" s="T163">cardnum</ta>
            <ta e="T165" id="Seg_3428" s="T164">n</ta>
            <ta e="T166" id="Seg_3429" s="T165">v</ta>
            <ta e="T167" id="Seg_3430" s="T166">dempro</ta>
            <ta e="T171" id="Seg_3431" s="T170">n</ta>
            <ta e="T172" id="Seg_3432" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_3433" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_3434" s="T173">n</ta>
            <ta e="T175" id="Seg_3435" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_3436" s="T175">adv</ta>
            <ta e="T177" id="Seg_3437" s="T176">cardnum</ta>
            <ta e="T178" id="Seg_3438" s="T177">n</ta>
            <ta e="T179" id="Seg_3439" s="T178">v</ta>
            <ta e="T180" id="Seg_3440" s="T179">adv</ta>
            <ta e="T181" id="Seg_3441" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_3442" s="T181">n</ta>
            <ta e="T183" id="Seg_3443" s="T182">n</ta>
            <ta e="T184" id="Seg_3444" s="T183">v</ta>
            <ta e="T185" id="Seg_3445" s="T296">adv</ta>
            <ta e="T186" id="Seg_3446" s="T185">adj</ta>
            <ta e="T187" id="Seg_3447" s="T186">adj</ta>
            <ta e="T188" id="Seg_3448" s="T187">n</ta>
            <ta e="T189" id="Seg_3449" s="T188">v</ta>
            <ta e="T190" id="Seg_3450" s="T189">ptcl</ta>
            <ta e="T191" id="Seg_3451" s="T190">adj</ta>
            <ta e="T192" id="Seg_3452" s="T191">n</ta>
            <ta e="T194" id="Seg_3453" s="T192">ptcl</ta>
            <ta e="T195" id="Seg_3454" s="T194">n</ta>
            <ta e="T196" id="Seg_3455" s="T195">adj</ta>
            <ta e="T197" id="Seg_3456" s="T196">n</ta>
            <ta e="T198" id="Seg_3457" s="T197">adv</ta>
            <ta e="T199" id="Seg_3458" s="T198">v</ta>
            <ta e="T200" id="Seg_3459" s="T199">adj</ta>
            <ta e="T201" id="Seg_3460" s="T200">n</ta>
            <ta e="T202" id="Seg_3461" s="T201">dempro</ta>
            <ta e="T203" id="Seg_3462" s="T202">n</ta>
            <ta e="T204" id="Seg_3463" s="T203">emphpro</ta>
            <ta e="T205" id="Seg_3464" s="T204">n</ta>
            <ta e="T309" id="Seg_3465" s="T205">n</ta>
            <ta e="T208" id="Seg_3466" s="T207">adj</ta>
            <ta e="T209" id="Seg_3467" s="T208">n</ta>
            <ta e="T210" id="Seg_3468" s="T209">cardnum</ta>
            <ta e="T211" id="Seg_3469" s="T210">n</ta>
            <ta e="T213" id="Seg_3470" s="T211">v</ta>
            <ta e="T298" id="Seg_3471" s="T213">conj</ta>
            <ta e="T214" id="Seg_3472" s="T298">adj</ta>
            <ta e="T215" id="Seg_3473" s="T214">n</ta>
            <ta e="T216" id="Seg_3474" s="T215">cardnum</ta>
            <ta e="T217" id="Seg_3475" s="T216">n</ta>
            <ta e="T219" id="Seg_3476" s="T217">v</ta>
            <ta e="T220" id="Seg_3477" s="T219">dempro</ta>
            <ta e="T221" id="Seg_3478" s="T220">dempro</ta>
            <ta e="T222" id="Seg_3479" s="T221">n</ta>
            <ta e="T223" id="Seg_3480" s="T222">n</ta>
            <ta e="T224" id="Seg_3481" s="T223">n</ta>
            <ta e="T225" id="Seg_3482" s="T224">ptcl</ta>
            <ta e="T226" id="Seg_3483" s="T225">n</ta>
            <ta e="T227" id="Seg_3484" s="T226">n</ta>
            <ta e="T228" id="Seg_3485" s="T227">ptcl</ta>
            <ta e="T232" id="Seg_3486" s="T231">v</ta>
            <ta e="T233" id="Seg_3487" s="T232">n</ta>
            <ta e="T234" id="Seg_3488" s="T233">n</ta>
            <ta e="T235" id="Seg_3489" s="T234">v</ta>
            <ta e="T236" id="Seg_3490" s="T235">adv</ta>
            <ta e="T240" id="Seg_3491" s="T239">dempro</ta>
            <ta e="T241" id="Seg_3492" s="T240">dempro</ta>
            <ta e="T242" id="Seg_3493" s="T241">n</ta>
            <ta e="T243" id="Seg_3494" s="T242">n</ta>
            <ta e="T246" id="Seg_3495" s="T245">adj</ta>
            <ta e="T247" id="Seg_3496" s="T246">adj</ta>
            <ta e="T248" id="Seg_3497" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_3498" s="T248">n</ta>
            <ta e="T251" id="Seg_3499" s="T250">v</ta>
            <ta e="T252" id="Seg_3500" s="T251">v</ta>
            <ta e="T253" id="Seg_3501" s="T252">ptcl</ta>
            <ta e="T254" id="Seg_3502" s="T253">n</ta>
            <ta e="T310" id="Seg_3503" s="T254">v</ta>
            <ta e="T256" id="Seg_3504" s="T255">dempro</ta>
            <ta e="T257" id="Seg_3505" s="T256">v</ta>
            <ta e="T301" id="Seg_3506" s="T257">aux</ta>
            <ta e="T259" id="Seg_3507" s="T258">adj</ta>
            <ta e="T260" id="Seg_3508" s="T259">n</ta>
            <ta e="T261" id="Seg_3509" s="T260">cop</ta>
            <ta e="T262" id="Seg_3510" s="T261">dempro</ta>
            <ta e="T264" id="Seg_3511" s="T263">quant</ta>
            <ta e="T265" id="Seg_3512" s="T264">n</ta>
            <ta e="T302" id="Seg_3513" s="T265">ptcl</ta>
            <ta e="T268" id="Seg_3514" s="T267">cardnum</ta>
            <ta e="T269" id="Seg_3515" s="T268">adj</ta>
            <ta e="T270" id="Seg_3516" s="T269">n</ta>
            <ta e="T271" id="Seg_3517" s="T270">ptcl</ta>
            <ta e="T272" id="Seg_3518" s="T271">ptcl</ta>
            <ta e="T273" id="Seg_3519" s="T272">adj</ta>
            <ta e="T274" id="Seg_3520" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_3521" s="T274">cop</ta>
            <ta e="T303" id="Seg_3522" s="T275">dempro</ta>
            <ta e="T277" id="Seg_3523" s="T276">dempro</ta>
            <ta e="T278" id="Seg_3524" s="T277">cardnum</ta>
            <ta e="T279" id="Seg_3525" s="T278">n</ta>
            <ta e="T280" id="Seg_3526" s="T279">n</ta>
            <ta e="T281" id="Seg_3527" s="T280">n</ta>
            <ta e="T282" id="Seg_3528" s="T281">v</ta>
            <ta e="T283" id="Seg_3529" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_3530" s="T283">n</ta>
            <ta e="T285" id="Seg_3531" s="T284">v</ta>
            <ta e="T286" id="Seg_3532" s="T285">cardnum</ta>
            <ta e="T304" id="Seg_3533" s="T286">n</ta>
            <ta e="T288" id="Seg_3534" s="T287">n</ta>
            <ta e="T289" id="Seg_3535" s="T288">v</ta>
            <ta e="T290" id="Seg_3536" s="T289">dempro</ta>
            <ta e="T291" id="Seg_3537" s="T290">dempro</ta>
            <ta e="T292" id="Seg_3538" s="T291">n</ta>
            <ta e="T293" id="Seg_3539" s="T292">adj</ta>
            <ta e="T294" id="Seg_3540" s="T293">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_3541" s="T0">0.1.h:A</ta>
            <ta e="T3" id="Seg_3542" s="T2">np:P</ta>
            <ta e="T305" id="Seg_3543" s="T4">0.1.h:A</ta>
            <ta e="T7" id="Seg_3544" s="T6">0.1.h:Poss np:Th</ta>
            <ta e="T8" id="Seg_3545" s="T7">0.1.h:A</ta>
            <ta e="T12" id="Seg_3546" s="T11">np:Th</ta>
            <ta e="T15" id="Seg_3547" s="T14">np:Th</ta>
            <ta e="T20" id="Seg_3548" s="T19">np.h:A</ta>
            <ta e="T23" id="Seg_3549" s="T22">np:Th</ta>
            <ta e="T36" id="Seg_3550" s="T35">np:Th</ta>
            <ta e="T308" id="Seg_3551" s="T36">0.3.h:A</ta>
            <ta e="T41" id="Seg_3552" s="T40">np:Th</ta>
            <ta e="T42" id="Seg_3553" s="T41">0.3.h:A</ta>
            <ta e="T43" id="Seg_3554" s="T42">np:Th</ta>
            <ta e="T44" id="Seg_3555" s="T43">0.1.h:A</ta>
            <ta e="T48" id="Seg_3556" s="T47">np:G</ta>
            <ta e="T50" id="Seg_3557" s="T49">np:Th</ta>
            <ta e="T51" id="Seg_3558" s="T50">0.1.h:A</ta>
            <ta e="T53" id="Seg_3559" s="T52">np:Th</ta>
            <ta e="T54" id="Seg_3560" s="T53">0.3.h:A</ta>
            <ta e="T56" id="Seg_3561" s="T55">np:Th</ta>
            <ta e="T63" id="Seg_3562" s="T62">np:L</ta>
            <ta e="T68" id="Seg_3563" s="T67">np:Th</ta>
            <ta e="T71" id="Seg_3564" s="T70">np:G</ta>
            <ta e="T73" id="Seg_3565" s="T72">np:Th</ta>
            <ta e="T75" id="Seg_3566" s="T74">0.3.h:A</ta>
            <ta e="T78" id="Seg_3567" s="T77">0.3.h:A</ta>
            <ta e="T79" id="Seg_3568" s="T78">np:Th</ta>
            <ta e="T81" id="Seg_3569" s="T80">np:Th</ta>
            <ta e="T84" id="Seg_3570" s="T83">np:Th</ta>
            <ta e="T86" id="Seg_3571" s="T85">0.3.h:A</ta>
            <ta e="T92" id="Seg_3572" s="T91">pro:Th</ta>
            <ta e="T95" id="Seg_3573" s="T94">0.3.h:A</ta>
            <ta e="T96" id="Seg_3574" s="T95">adv:L</ta>
            <ta e="T98" id="Seg_3575" s="T97">np:Th</ta>
            <ta e="T99" id="Seg_3576" s="T98">0.3.h:A</ta>
            <ta e="T101" id="Seg_3577" s="T100">np:Th</ta>
            <ta e="T102" id="Seg_3578" s="T101">0.3.h:A</ta>
            <ta e="T105" id="Seg_3579" s="T104">np:Th</ta>
            <ta e="T108" id="Seg_3580" s="T107">np:Poss</ta>
            <ta e="T121" id="Seg_3581" s="T120">0.1.h:A</ta>
            <ta e="T122" id="Seg_3582" s="T121">np:P</ta>
            <ta e="T123" id="Seg_3583" s="T122">0.1.h:A</ta>
            <ta e="T134" id="Seg_3584" s="T133">pro:Th</ta>
            <ta e="T135" id="Seg_3585" s="T134">pro:Th</ta>
            <ta e="T139" id="Seg_3586" s="T138">0.3.h:A</ta>
            <ta e="T140" id="Seg_3587" s="T139">np:G</ta>
            <ta e="T145" id="Seg_3588" s="T144">np:Poss</ta>
            <ta e="T150" id="Seg_3589" s="T149">np:Th</ta>
            <ta e="T160" id="Seg_3590" s="T159">np:Th</ta>
            <ta e="T163" id="Seg_3591" s="T162">pro:G</ta>
            <ta e="T165" id="Seg_3592" s="T164">np:Th</ta>
            <ta e="T166" id="Seg_3593" s="T165">0.3.h:A</ta>
            <ta e="T171" id="Seg_3594" s="T170">np:L</ta>
            <ta e="T174" id="Seg_3595" s="T173">np:Th</ta>
            <ta e="T178" id="Seg_3596" s="T177">np:Th</ta>
            <ta e="T179" id="Seg_3597" s="T178">0.3.h:A</ta>
            <ta e="T184" id="Seg_3598" s="T183">0.3.h:A</ta>
            <ta e="T195" id="Seg_3599" s="T194">np:Th</ta>
            <ta e="T197" id="Seg_3600" s="T196">np:Th</ta>
            <ta e="T199" id="Seg_3601" s="T198">0.2.h:A</ta>
            <ta e="T203" id="Seg_3602" s="T202">np.h:Poss</ta>
            <ta e="T211" id="Seg_3603" s="T210">np:Th</ta>
            <ta e="T213" id="Seg_3604" s="T211">0.3.h:A</ta>
            <ta e="T217" id="Seg_3605" s="T216">np:Th</ta>
            <ta e="T219" id="Seg_3606" s="T217">0.3.h:A</ta>
            <ta e="T222" id="Seg_3607" s="T221">np.h:Poss</ta>
            <ta e="T224" id="Seg_3608" s="T223">np:L</ta>
            <ta e="T227" id="Seg_3609" s="T226">np:Th</ta>
            <ta e="T235" id="Seg_3610" s="T234">0.1.h:A</ta>
            <ta e="T236" id="Seg_3611" s="T235">adv:L</ta>
            <ta e="T243" id="Seg_3612" s="T242">np:L</ta>
            <ta e="T249" id="Seg_3613" s="T248">np:Th</ta>
            <ta e="T251" id="Seg_3614" s="T250">0.2.h:A</ta>
            <ta e="T252" id="Seg_3615" s="T251">0.2.h:A</ta>
            <ta e="T254" id="Seg_3616" s="T253">np:Th</ta>
            <ta e="T310" id="Seg_3617" s="T254">0.2.h:A</ta>
            <ta e="T301" id="Seg_3618" s="T256">0.2.h:A</ta>
            <ta e="T261" id="Seg_3619" s="T260">0.3:Th</ta>
            <ta e="T303" id="Seg_3620" s="T275">pro:Th</ta>
            <ta e="T279" id="Seg_3621" s="T278">np.h:A</ta>
            <ta e="T281" id="Seg_3622" s="T280">np:Th</ta>
            <ta e="T284" id="Seg_3623" s="T283">np:Th</ta>
            <ta e="T304" id="Seg_3624" s="T286">np.h:A</ta>
            <ta e="T288" id="Seg_3625" s="T287">np:Th</ta>
            <ta e="T290" id="Seg_3626" s="T289">np.h:A</ta>
            <ta e="T292" id="Seg_3627" s="T291">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_3628" s="T0">0.1.h:S v:pred</ta>
            <ta e="T3" id="Seg_3629" s="T2">np:O</ta>
            <ta e="T305" id="Seg_3630" s="T4">0.1.h:S v:pred</ta>
            <ta e="T7" id="Seg_3631" s="T6">np:O</ta>
            <ta e="T8" id="Seg_3632" s="T7">0.1.h:S v:pred</ta>
            <ta e="T12" id="Seg_3633" s="T11">np:S</ta>
            <ta e="T13" id="Seg_3634" s="T12">adj:pred</ta>
            <ta e="T17" id="Seg_3635" s="T14">s:comp</ta>
            <ta e="T307" id="Seg_3636" s="T17">ptcl:pred</ta>
            <ta e="T20" id="Seg_3637" s="T19">np.h:S</ta>
            <ta e="T23" id="Seg_3638" s="T22">np:O</ta>
            <ta e="T306" id="Seg_3639" s="T23">v:pred</ta>
            <ta e="T36" id="Seg_3640" s="T35">np:O</ta>
            <ta e="T308" id="Seg_3641" s="T36">0.3.h:S v:pred</ta>
            <ta e="T41" id="Seg_3642" s="T40">np:O</ta>
            <ta e="T42" id="Seg_3643" s="T41">0.3.h:S v:pred</ta>
            <ta e="T43" id="Seg_3644" s="T42">np:O</ta>
            <ta e="T44" id="Seg_3645" s="T43">0.1.h:S v:pred</ta>
            <ta e="T50" id="Seg_3646" s="T49">np:O</ta>
            <ta e="T51" id="Seg_3647" s="T50">0.1.h:S v:pred</ta>
            <ta e="T53" id="Seg_3648" s="T52">np:O</ta>
            <ta e="T54" id="Seg_3649" s="T53">0.3.h:S v:pred</ta>
            <ta e="T57" id="Seg_3650" s="T56">ptcl:pred</ta>
            <ta e="T58" id="Seg_3651" s="T57">s:comp</ta>
            <ta e="T68" id="Seg_3652" s="T67">np:S</ta>
            <ta e="T69" id="Seg_3653" s="T68">ptcl:pred</ta>
            <ta e="T73" id="Seg_3654" s="T72">np:O</ta>
            <ta e="T75" id="Seg_3655" s="T74">0.3.h:S v:pred</ta>
            <ta e="T78" id="Seg_3656" s="T77">0.3.h:S v:pred</ta>
            <ta e="T79" id="Seg_3657" s="T78">np:O</ta>
            <ta e="T81" id="Seg_3658" s="T80">np:O</ta>
            <ta e="T84" id="Seg_3659" s="T83">np:O</ta>
            <ta e="T86" id="Seg_3660" s="T85">0.3.h:S v:pred</ta>
            <ta e="T92" id="Seg_3661" s="T91">pro:O</ta>
            <ta e="T95" id="Seg_3662" s="T94">0.3.h:S v:pred</ta>
            <ta e="T98" id="Seg_3663" s="T97">np:O</ta>
            <ta e="T99" id="Seg_3664" s="T98">0.3.h:S v:pred</ta>
            <ta e="T101" id="Seg_3665" s="T100">np:O</ta>
            <ta e="T102" id="Seg_3666" s="T101">0.3.h:S v:pred</ta>
            <ta e="T105" id="Seg_3667" s="T104">np:S</ta>
            <ta e="T110" id="Seg_3668" s="T109">n:pred</ta>
            <ta e="T121" id="Seg_3669" s="T120">0.1.h:S v:pred</ta>
            <ta e="T122" id="Seg_3670" s="T121">np:O</ta>
            <ta e="T123" id="Seg_3671" s="T122">0.1.h:S v:pred</ta>
            <ta e="T134" id="Seg_3672" s="T133">pro:O</ta>
            <ta e="T135" id="Seg_3673" s="T134">pro:O</ta>
            <ta e="T139" id="Seg_3674" s="T138">0.3.h:S v:pred</ta>
            <ta e="T150" id="Seg_3675" s="T149">np:S</ta>
            <ta e="T151" id="Seg_3676" s="T150">v:pred</ta>
            <ta e="T160" id="Seg_3677" s="T159">np:S</ta>
            <ta e="T161" id="Seg_3678" s="T160">ptcl:pred</ta>
            <ta e="T165" id="Seg_3679" s="T164">np:O</ta>
            <ta e="T166" id="Seg_3680" s="T165">0.3.h:S v:pred</ta>
            <ta e="T174" id="Seg_3681" s="T173">np:S</ta>
            <ta e="T175" id="Seg_3682" s="T174">ptcl:pred</ta>
            <ta e="T178" id="Seg_3683" s="T177">np:O</ta>
            <ta e="T179" id="Seg_3684" s="T178">0.3.h:S v:pred</ta>
            <ta e="T184" id="Seg_3685" s="T183">0.3.h:S v:pred</ta>
            <ta e="T195" id="Seg_3686" s="T194">np:O</ta>
            <ta e="T197" id="Seg_3687" s="T196">np:O</ta>
            <ta e="T199" id="Seg_3688" s="T198">0.2.h:S v:pred</ta>
            <ta e="T211" id="Seg_3689" s="T210">np:O</ta>
            <ta e="T213" id="Seg_3690" s="T211">0.3.h:S v:pred</ta>
            <ta e="T217" id="Seg_3691" s="T216">np:O</ta>
            <ta e="T219" id="Seg_3692" s="T217">0.3.h:S v:pred</ta>
            <ta e="T227" id="Seg_3693" s="T226">np:S</ta>
            <ta e="T228" id="Seg_3694" s="T227">ptcl:pred</ta>
            <ta e="T235" id="Seg_3695" s="T234">0.1.h:S v:pred</ta>
            <ta e="T249" id="Seg_3696" s="T248">np:O</ta>
            <ta e="T251" id="Seg_3697" s="T250">0.2.h:S v:pred</ta>
            <ta e="T252" id="Seg_3698" s="T251">0.2.h:S v:pred</ta>
            <ta e="T254" id="Seg_3699" s="T253">np:O</ta>
            <ta e="T310" id="Seg_3700" s="T254">0.2.h:S v:pred</ta>
            <ta e="T301" id="Seg_3701" s="T256">0.2.h:S v:pred</ta>
            <ta e="T260" id="Seg_3702" s="T259">n:pred</ta>
            <ta e="T261" id="Seg_3703" s="T260">0.3:S cop</ta>
            <ta e="T273" id="Seg_3704" s="T272">adj:pred</ta>
            <ta e="T275" id="Seg_3705" s="T274">cop</ta>
            <ta e="T303" id="Seg_3706" s="T275">pro:S</ta>
            <ta e="T279" id="Seg_3707" s="T278">np.h:S</ta>
            <ta e="T281" id="Seg_3708" s="T280">np:O</ta>
            <ta e="T282" id="Seg_3709" s="T281">v:pred</ta>
            <ta e="T284" id="Seg_3710" s="T283">np:O</ta>
            <ta e="T285" id="Seg_3711" s="T284">v:pred</ta>
            <ta e="T304" id="Seg_3712" s="T286">np.h:S</ta>
            <ta e="T288" id="Seg_3713" s="T287">np:O</ta>
            <ta e="T289" id="Seg_3714" s="T288">v:pred</ta>
            <ta e="T290" id="Seg_3715" s="T289">np.h:S</ta>
            <ta e="T292" id="Seg_3716" s="T291">np:S</ta>
            <ta e="T294" id="Seg_3717" s="T293">n:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_3718" s="T3">EV:gram (DIM)</ta>
            <ta e="T12" id="Seg_3719" s="T11">RUS:cult</ta>
            <ta e="T307" id="Seg_3720" s="T17">RUS:mod</ta>
            <ta e="T306" id="Seg_3721" s="T23">RUS:cult</ta>
            <ta e="T34" id="Seg_3722" s="T33">EV:gram (DIM)</ta>
            <ta e="T48" id="Seg_3723" s="T47">RUS:cult</ta>
            <ta e="T57" id="Seg_3724" s="T56">RUS:mod</ta>
            <ta e="T59" id="Seg_3725" s="T58">RUS:cult</ta>
            <ta e="T62" id="Seg_3726" s="T61">RUS:cult</ta>
            <ta e="T66" id="Seg_3727" s="T65">RUS:mod</ta>
            <ta e="T67" id="Seg_3728" s="T66">RUS:gram</ta>
            <ta e="T78" id="Seg_3729" s="T77">RUS:core</ta>
            <ta e="T81" id="Seg_3730" s="T80">RUS:cult</ta>
            <ta e="T83" id="Seg_3731" s="T82">RUS:mod</ta>
            <ta e="T84" id="Seg_3732" s="T83">RUS:cult</ta>
            <ta e="T93" id="Seg_3733" s="T92">EV:gram (DIM)</ta>
            <ta e="T107" id="Seg_3734" s="T106">RUS:mod</ta>
            <ta e="T108" id="Seg_3735" s="T107">RUS:core</ta>
            <ta e="T115" id="Seg_3736" s="T114">RUS:cult</ta>
            <ta e="T116" id="Seg_3737" s="T115">RUS:mod</ta>
            <ta e="T295" id="Seg_3738" s="T118">RUS:gram</ta>
            <ta e="T120" id="Seg_3739" s="T119">EV:cult</ta>
            <ta e="T130" id="Seg_3740" s="T129">EV:cult</ta>
            <ta e="T131" id="Seg_3741" s="T130">EV:cult</ta>
            <ta e="T132" id="Seg_3742" s="T131">EV:cult</ta>
            <ta e="T145" id="Seg_3743" s="T144">RUS:core</ta>
            <ta e="T157" id="Seg_3744" s="T156">RUS:mod</ta>
            <ta e="T160" id="Seg_3745" s="T159">RUS:cult</ta>
            <ta e="T298" id="Seg_3746" s="T213">RUS:gram</ta>
            <ta e="T223" id="Seg_3747" s="T222">RUS:cult</ta>
            <ta e="T227" id="Seg_3748" s="T226">RUS:cult</ta>
            <ta e="T260" id="Seg_3749" s="T259">RUS:cult</ta>
            <ta e="T294" id="Seg_3750" s="T293">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T12" id="Seg_3751" s="T11">Vsub</ta>
            <ta e="T59" id="Seg_3752" s="T58">Vsub</ta>
            <ta e="T62" id="Seg_3753" s="T61">Vsub</ta>
            <ta e="T66" id="Seg_3754" s="T65">Vsub Csub Vsub</ta>
            <ta e="T81" id="Seg_3755" s="T80">Vsub</ta>
            <ta e="T160" id="Seg_3756" s="T159">Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T12" id="Seg_3757" s="T11">dir:infl</ta>
            <ta e="T307" id="Seg_3758" s="T17">dir:bare</ta>
            <ta e="T306" id="Seg_3759" s="T23">indir:infl</ta>
            <ta e="T57" id="Seg_3760" s="T56">dir:bare</ta>
            <ta e="T59" id="Seg_3761" s="T58">dir:infl</ta>
            <ta e="T62" id="Seg_3762" s="T61">dir:bare</ta>
            <ta e="T66" id="Seg_3763" s="T65">dir:bare</ta>
            <ta e="T67" id="Seg_3764" s="T66">dir:bare</ta>
            <ta e="T78" id="Seg_3765" s="T77">indir:infl</ta>
            <ta e="T81" id="Seg_3766" s="T80">dir:infl</ta>
            <ta e="T83" id="Seg_3767" s="T82">dir:bare</ta>
            <ta e="T84" id="Seg_3768" s="T83">dir:infl</ta>
            <ta e="T107" id="Seg_3769" s="T106">dir:bare</ta>
            <ta e="T108" id="Seg_3770" s="T107">dir:infl</ta>
            <ta e="T115" id="Seg_3771" s="T114">dir:bare</ta>
            <ta e="T116" id="Seg_3772" s="T115">dir:bare</ta>
            <ta e="T295" id="Seg_3773" s="T118">dir:bare</ta>
            <ta e="T120" id="Seg_3774" s="T119">dir:bare</ta>
            <ta e="T130" id="Seg_3775" s="T129">dir:infl</ta>
            <ta e="T131" id="Seg_3776" s="T130">dir:bare</ta>
            <ta e="T132" id="Seg_3777" s="T131">dir:bare</ta>
            <ta e="T145" id="Seg_3778" s="T144">dir:bare</ta>
            <ta e="T157" id="Seg_3779" s="T156">dir:bare</ta>
            <ta e="T160" id="Seg_3780" s="T159">dir:bare</ta>
            <ta e="T298" id="Seg_3781" s="T213">dir:bare</ta>
            <ta e="T260" id="Seg_3782" s="T259">dir:bare</ta>
            <ta e="T294" id="Seg_3783" s="T293">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T33" id="Seg_3784" s="T26">RUS:int.ins</ta>
            <ta e="T39" id="Seg_3785" s="T37">RUS:ext</ta>
            <ta e="T128" id="Seg_3786" s="T123">RUS:int.ins</ta>
            <ta e="T170" id="Seg_3787" s="T167">RUS:int.ins</ta>
            <ta e="T296" id="Seg_3788" s="T184">RUS:int.alt</ta>
            <ta e="T207" id="Seg_3789" s="T206">RUS:int.ins</ta>
            <ta e="T231" id="Seg_3790" s="T228">RUS:ext</ta>
            <ta e="T239" id="Seg_3791" s="T236">RUS:int.alt</ta>
            <ta e="T245" id="Seg_3792" s="T243">RUS:int.alt</ta>
            <ta e="T250" id="Seg_3793" s="T249">RUS:int.alt</ta>
            <ta e="T263" id="Seg_3794" s="T262">RUS:int.alt</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T305" id="Seg_3795" s="T0">[When] we migrate, we prepare all our things.</ta>
            <ta e="T8" id="Seg_3796" s="T5">Yes, we dig out our house.</ta>
            <ta e="T14" id="Seg_3797" s="T8">Our house, our balok is in the deep snow, is in the deep snow.</ta>
            <ta e="T307" id="Seg_3798" s="T14">The houses that are in the deep snow should be dug out.</ta>
            <ta e="T306" id="Seg_3799" s="T18">Then the women tidy up the inside of the house.</ta>
            <ta e="T34" id="Seg_3800" s="T24">Well, the crockery, it is all packed, everything.</ta>
            <ta e="T308" id="Seg_3801" s="T34">Then they bring the herd.</ta>
            <ta e="T39" id="Seg_3802" s="T37">they bring over the herd</ta>
            <ta e="T42" id="Seg_3803" s="T39">Then they catch the reindeer.</ta>
            <ta e="T51" id="Seg_3804" s="T42">We catch the reindeer and then we harness eight reindeer at the balok.</ta>
            <ta e="T60" id="Seg_3805" s="T51">They catch eight reindeer, one should catch big reindeer for the balok.</ta>
            <ta e="T69" id="Seg_3806" s="T60">Then in front of the balok there is another sleigh.</ta>
            <ta e="T78" id="Seg_3807" s="T69">They load everything onto that sleigh.</ta>
            <ta e="T87" id="Seg_3808" s="T78">Wood, barrels, they may put coal and stuff on there.</ta>
            <ta e="T95" id="Seg_3809" s="T87">Everything that is left they load on there.</ta>
            <ta e="T102" id="Seg_3810" s="T95">There they harness four reindeer.</ta>
            <ta e="T110" id="Seg_3811" s="T102">Then the front sleigh is already the sleigh of the owner himself.</ta>
            <ta e="T118" id="Seg_3812" s="T110">There it's also possible to put coal on. </ta>
            <ta e="T128" id="Seg_3813" s="T118">And there [they load] the [thing] we call nipte, where we chop wood, a stick to chop wood.</ta>
            <ta e="T132" id="Seg_3814" s="T128">There the nipte, nipte, nipte.</ta>
            <ta e="T141" id="Seg_3815" s="T132">This all they load onto the sleigh.</ta>
            <ta e="T151" id="Seg_3816" s="T141">Then comes the reindeer sleigh of the owner.</ta>
            <ta e="T158" id="Seg_3817" s="T151">Then the woman, the woman must [go] separately.</ta>
            <ta e="T166" id="Seg_3818" s="T158">There is a small balok, to it they harness four reindeer.</ta>
            <ta e="T175" id="Seg_3819" s="T166">Then in front of the balok there is also a sleigh.</ta>
            <ta e="T179" id="Seg_3820" s="T175">There they harness two reindeer.</ta>
            <ta e="T184" id="Seg_3821" s="T179">Earlier they called it "the closed sledge".</ta>
            <ta e="T194" id="Seg_3822" s="T184">Especially there are various storage spaces for clothes, on a closed sleigh.</ta>
            <ta e="T199" id="Seg_3823" s="T194">Clothes and the best food you put there.</ta>
            <ta e="T309" id="Seg_3824" s="T199">The closed sleigh and then the sleigh of the woman herself.</ta>
            <ta e="T213" id="Seg_3825" s="T206">It depends, for a far migration they harness three reindeer.</ta>
            <ta e="T219" id="Seg_3826" s="T213">For a close migration they harness two reindeer.</ta>
            <ta e="T228" id="Seg_3827" s="T219">Then after the women's balok comes the entryway balok(?).</ta>
            <ta e="T231" id="Seg_3828" s="T228">Look, a small balok.</ta>
            <ta e="T235" id="Seg_3829" s="T231">They call it pantry, or entryway.</ta>
            <ta e="T239" id="Seg_3830" s="T235">There are already two reindeer.</ta>
            <ta e="T253" id="Seg_3831" s="T239">After the entryway come various things, the storage.</ta>
            <ta e="T310" id="Seg_3832" s="T253">You tie the dogs.</ta>
            <ta e="T301" id="Seg_3833" s="T255">And then you migrate.</ta>
            <ta e="T262" id="Seg_3834" s="T258">That is a big argish then.</ta>
            <ta e="T302" id="Seg_3835" s="T262">Especially when there are many people.</ta>
            <ta e="T303" id="Seg_3836" s="T267">When there are three families, it is very long.</ta>
            <ta e="T304" id="Seg_3837" s="T276">Then one person must drive the herd, one person drives the herd.</ta>
            <ta e="T290" id="Seg_3838" s="T287">S/he drives that herd.</ta>
            <ta e="T294" id="Seg_3839" s="T290">This is called a big argish.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T305" id="Seg_3840" s="T0">Wenn wir nomadisieren, bereiten wie alles vor.</ta>
            <ta e="T8" id="Seg_3841" s="T5">Ja, wir graben unser Haus aus.</ta>
            <ta e="T14" id="Seg_3842" s="T8">Unser Haus, unser Balok ist im tiefen Schnee, ist im tiefen Schnee.</ta>
            <ta e="T307" id="Seg_3843" s="T14">Die Häuser im tiefen Schnee muss man ausgraben.</ta>
            <ta e="T306" id="Seg_3844" s="T18">Dann räumen die Frauen das Innere des Hauses auf.</ta>
            <ta e="T34" id="Seg_3845" s="T24">Nun, das Geschirr, alles wird eingepackt, alles.</ta>
            <ta e="T308" id="Seg_3846" s="T34">Dann bringt man die Herde.</ta>
            <ta e="T39" id="Seg_3847" s="T37">Man bringt die Herde.</ta>
            <ta e="T42" id="Seg_3848" s="T39">Dann fängt man die Rentiere.</ta>
            <ta e="T51" id="Seg_3849" s="T42">Wir fangen die Rentiere und dann spannen wir acht Rentiere an den Balok.</ta>
            <ta e="T60" id="Seg_3850" s="T51">Man fängt acht Rentiere, man soll große Rentiere für den Balok fangen.</ta>
            <ta e="T69" id="Seg_3851" s="T60">Dann vor dem Balok ist noch ein anderer Schlitten.</ta>
            <ta e="T78" id="Seg_3852" s="T69">Auf diesen Schlitten lädt man alles.</ta>
            <ta e="T87" id="Seg_3853" s="T78">Holz, Fässer, vielleicht legt man Kohle und so darauf.</ta>
            <ta e="T95" id="Seg_3854" s="T87">Alles, was übrig geblieben ist, wird darauf geladen.</ta>
            <ta e="T102" id="Seg_3855" s="T95">Dort spannt man vier Rentiere an.</ta>
            <ta e="T110" id="Seg_3856" s="T102">Dann der vorderste Schlitten ist schon der Schlitten des Besitzers.</ta>
            <ta e="T118" id="Seg_3857" s="T110">Da kann man auch Kohle drauflegen.</ta>
            <ta e="T128" id="Seg_3858" s="T118">Und dort [lädt] man [das, was] wir "Nipte" nennen, wo wir Holz hacken, ein Stock zum Holzhacken.</ta>
            <ta e="T132" id="Seg_3859" s="T128">Dort die "Nipte", "Nipte", "Nipte".</ta>
            <ta e="T141" id="Seg_3860" s="T132">Das alles lädt man auf den Schlitten.</ta>
            <ta e="T151" id="Seg_3861" s="T141">Dann kommt der Rentierschlitten des Besitzers.</ta>
            <ta e="T158" id="Seg_3862" s="T151">Dann die Frauen, die Frauen müssen getrennt [fahren].</ta>
            <ta e="T166" id="Seg_3863" s="T158">Es gibt einen kleinen Balok, dort spannt man vier Rentiere an.</ta>
            <ta e="T175" id="Seg_3864" s="T166">Dann vor dem Balok ist auch ein Schlitten.</ta>
            <ta e="T179" id="Seg_3865" s="T175">Dort spannt man zwei Rentiere an.</ta>
            <ta e="T184" id="Seg_3866" s="T179">Früher nannte man das den "geschlossenen Schlitten".</ta>
            <ta e="T194" id="Seg_3867" s="T184">Dort ist extra Platz für Kleidung, auf einem geschlossenen Schlitten.</ta>
            <ta e="T199" id="Seg_3868" s="T194">Kleidung und gutes Essen packt man dorthin.</ta>
            <ta e="T309" id="Seg_3869" s="T199">Der geschlossene Schlitten und dann der Schlitten der Frau selbst.</ta>
            <ta e="T213" id="Seg_3870" s="T206">Es hängt davon ab, für einen weiten Zug spannt man drei Rentiere an.</ta>
            <ta e="T219" id="Seg_3871" s="T213">Und für einen kurzen Zug spannt man zwei Rentiere an.</ta>
            <ta e="T228" id="Seg_3872" s="T219">Dann hinter dem Balok der Frau kommt der Dielenbalok(?). </ta>
            <ta e="T231" id="Seg_3873" s="T228">Schau, ein kleiner Balok.</ta>
            <ta e="T235" id="Seg_3874" s="T231">Man nennt es Vorratsbalok, oder Diele.</ta>
            <ta e="T239" id="Seg_3875" s="T235">Dort sind schon zwei Rentiere.</ta>
            <ta e="T253" id="Seg_3876" s="T239">Nach der Diele da kann man alles mögliche, Vorräte anbinden.</ta>
            <ta e="T310" id="Seg_3877" s="T253">Man bindet die Hunde an.</ta>
            <ta e="T301" id="Seg_3878" s="T255">Und dann nomadisiert man.</ta>
            <ta e="T262" id="Seg_3879" s="T258">Das ist dann ein großer Zug.</ta>
            <ta e="T302" id="Seg_3880" s="T262">Besonders wenn viele Leute da sind.</ta>
            <ta e="T303" id="Seg_3881" s="T267">Wenn drei Familien da sind, ist es sehr lang.</ta>
            <ta e="T304" id="Seg_3882" s="T276">Dann muss eine Person die Herde treiben, eine Person treibt die Herde.</ta>
            <ta e="T290" id="Seg_3883" s="T287">Die treibt die Herde.</ta>
            <ta e="T294" id="Seg_3884" s="T290">Das nennt man einen großen Zug.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T305" id="Seg_3885" s="T0">[Когда] мы кочуем, мы готовим все наши вещи.</ta>
            <ta e="T8" id="Seg_3886" s="T5">Да, мы выкапываем дом.</ta>
            <ta e="T14" id="Seg_3887" s="T8">Наш дом же, наш балок, в глубоком снегу, в глубоком снегу.</ta>
            <ta e="T307" id="Seg_3888" s="T14">Дома, которые в глубоком снегу, надо выкопать.</ta>
            <ta e="T306" id="Seg_3889" s="T18">Потом женщины внутри дома убирают.</ta>
            <ta e="T34" id="Seg_3890" s="T24">Вот это, то, что посуда, всё это убирается, всё.</ta>
            <ta e="T308" id="Seg_3891" s="T34">Потом пригоняют стадо.</ta>
            <ta e="T39" id="Seg_3892" s="T37">Стадо приводят.</ta>
            <ta e="T42" id="Seg_3893" s="T39">Потом ловят оленей.</ta>
            <ta e="T51" id="Seg_3894" s="T42">Ловим оленей и запрягаем восемь оленей в балок.</ta>
            <ta e="T60" id="Seg_3895" s="T51">Восемь оленей ловят, на балок нужно больших оленей ловить.</ta>
            <ta e="T69" id="Seg_3896" s="T60">Потом перед балком ещё нарты есть.</ta>
            <ta e="T78" id="Seg_3897" s="T69">На эти нарты всякое разное грузят.</ta>
            <ta e="T87" id="Seg_3898" s="T78">Может, дрова, может, бочки, можно уголь всякий класть.</ta>
            <ta e="T95" id="Seg_3899" s="T87">Всё, что осталось, всё туда грузят.</ta>
            <ta e="T102" id="Seg_3900" s="T95">Туда запрягают четырёх оленей.</ta>
            <ta e="T110" id="Seg_3901" s="T102">Потом передние сани это уже сани самого хозяина.</ta>
            <ta e="T118" id="Seg_3902" s="T110">Туда тоже можно уголь класть.</ta>
            <ta e="T128" id="Seg_3903" s="T118">И туда [грузят] [то, что] мы называем нипте, где мы дрова рубим, палка такая, дрова рубить.</ta>
            <ta e="T132" id="Seg_3904" s="T128">Туда эти нипте, нипте, нипте.</ta>
            <ta e="T141" id="Seg_3905" s="T132">Всё это грузят на сани.</ta>
            <ta e="T151" id="Seg_3906" s="T141">Потом упряжка хозяина.</ta>
            <ta e="T158" id="Seg_3907" s="T151">Потом жена, жена отдельно должна [ехать].</ta>
            <ta e="T166" id="Seg_3908" s="T158">Это маленький балок, в него запрягают четырёх оленей.</ta>
            <ta e="T175" id="Seg_3909" s="T166">Потом впереди балка ещё сани.</ta>
            <ta e="T179" id="Seg_3910" s="T175">Туда запрягают двух оленей.</ta>
            <ta e="T184" id="Seg_3911" s="T179">Раньше это называли "крытая нарта".</ta>
            <ta e="T194" id="Seg_3912" s="T184">Специально для всякой разной одежды там место, на крытой нарте.</ta>
            <ta e="T199" id="Seg_3913" s="T194">Туда кладешь одежду и хорошую еду.</ta>
            <ta e="T309" id="Seg_3914" s="T199">Крытая нарта и потом нарта самой женщины.</ta>
            <ta e="T213" id="Seg_3915" s="T206">Смотря как, для дальней кочёвки запрягают трёх оленей.</ta>
            <ta e="T219" id="Seg_3916" s="T213">А для ближней кочёвки запрягают двух оленей.</ta>
            <ta e="T228" id="Seg_3917" s="T219">Потом сзади женского балка есть сенечный балок(?).</ta>
            <ta e="T231" id="Seg_3918" s="T228">Вот, маленький балок.</ta>
            <ta e="T235" id="Seg_3919" s="T231">Его называют пристройкой, сенями. </ta>
            <ta e="T239" id="Seg_3920" s="T235">Там уже два оленя.</ta>
            <ta e="T253" id="Seg_3921" s="T239">После этих сеней там всякое разное, припасы можно привязать.</ta>
            <ta e="T310" id="Seg_3922" s="T253">Собак привязываешь.</ta>
            <ta e="T301" id="Seg_3923" s="T255">И потом начинаешь кочевать.</ta>
            <ta e="T262" id="Seg_3924" s="T258">Это был большой аргиш.</ta>
            <ta e="T302" id="Seg_3925" s="T262">Особенно когда много людей.</ta>
            <ta e="T303" id="Seg_3926" s="T267">Когда три разных семьи, он бывает очень длинным.</ta>
            <ta e="T304" id="Seg_3927" s="T276">Тогда один человек гонит стадо, стадо гонит один человек.</ta>
            <ta e="T290" id="Seg_3928" s="T287">Он(a) стадо гонит.</ta>
            <ta e="T294" id="Seg_3929" s="T290">Это называется большой аргиш.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr" />
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T305" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T307" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T306" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T308" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T295" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T296" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T309" />
            <conversion-tli id="T206" />
            <conversion-tli id="T297" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T298" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T310" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T301" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T299" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T302" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T303" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T304" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T300" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
