<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID4CB39396-9BE0-C919-4D02-447C00C2C780">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>PoNA_1990_TojooInVolochanka_nar</transcription-name>
         <referenced-file url="PoNA_19900810_TojooInVolochanka_nar.wav" />
         <referenced-file url="PoNA_19900810_TojooInVolochanka_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\PoNA_19900810_TojooInVolochanka_nar\PoNA_19900810_TojooInVolochanka_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">931</ud-information>
            <ud-information attribute-name="# HIAT:w">718</ud-information>
            <ud-information attribute-name="# e">718</ud-information>
            <ud-information attribute-name="# HIAT:u">104</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PoNA">
            <abbreviation>PoNA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="1.491" type="appl" />
         <tli id="T2" time="2.982" type="appl" />
         <tli id="T3" time="4.472" type="appl" />
         <tli id="T4" time="6.339960172126771" />
         <tli id="T5" time="6.771" type="appl" />
         <tli id="T6" time="7.579" type="appl" />
         <tli id="T7" time="8.388" type="appl" />
         <tli id="T8" time="9.196" type="appl" />
         <tli id="T9" time="10.004" type="appl" />
         <tli id="T10" time="11.266595889478698" />
         <tli id="T11" time="11.592" type="appl" />
         <tli id="T12" time="12.373" type="appl" />
         <tli id="T13" time="13.153" type="appl" />
         <tli id="T14" time="13.933" type="appl" />
         <tli id="T15" time="14.714" type="appl" />
         <tli id="T16" time="15.494" type="appl" />
         <tli id="T17" time="16.274" type="appl" />
         <tli id="T18" time="17.054" type="appl" />
         <tli id="T19" time="17.835" type="appl" />
         <tli id="T20" time="18.615" type="appl" />
         <tli id="T21" time="19.395" type="appl" />
         <tli id="T22" time="20.176" type="appl" />
         <tli id="T23" time="20.956" type="appl" />
         <tli id="T24" time="21.736" type="appl" />
         <tli id="T25" time="22.517" type="appl" />
         <tli id="T26" time="23.579851870465184" />
         <tli id="T27" time="24.065" type="appl" />
         <tli id="T28" time="24.834" type="appl" />
         <tli id="T29" time="25.602" type="appl" />
         <tli id="T30" time="26.37" type="appl" />
         <tli id="T31" time="27.138" type="appl" />
         <tli id="T32" time="27.906" type="appl" />
         <tli id="T33" time="28.675" type="appl" />
         <tli id="T34" time="29.443" type="appl" />
         <tli id="T35" time="30.211" type="appl" />
         <tli id="T36" time="30.98" type="appl" />
         <tli id="T37" time="31.748" type="appl" />
         <tli id="T38" time="32.516" type="appl" />
         <tli id="T39" time="33.284" type="appl" />
         <tli id="T40" time="34.052" type="appl" />
         <tli id="T41" time="34.821" type="appl" />
         <tli id="T42" time="36.626436577985785" />
         <tli id="T43" time="36.687" type="appl" />
         <tli id="T44" time="37.784" type="appl" />
         <tli id="T45" time="38.882" type="appl" />
         <tli id="T46" time="39.98" type="appl" />
         <tli id="T47" time="40.398" type="appl" />
         <tli id="T48" time="40.816" type="appl" />
         <tli id="T49" time="41.234" type="appl" />
         <tli id="T50" time="41.652" type="appl" />
         <tli id="T51" time="42.07" type="appl" />
         <tli id="T52" time="42.488" type="appl" />
         <tli id="T53" time="43.17266498557758" />
         <tli id="T54" time="43.909" type="appl" />
         <tli id="T55" time="44.912" type="appl" />
         <tli id="T56" time="45.915" type="appl" />
         <tli id="T57" time="46.918" type="appl" />
         <tli id="T58" time="48.75302706494539" />
         <tli id="T59" time="48.757" type="appl" />
         <tli id="T60" time="49.593" type="appl" />
         <tli id="T61" time="50.43" type="appl" />
         <tli id="T62" time="51.266" type="appl" />
         <tli id="T63" time="52.102" type="appl" />
         <tli id="T64" time="53.038000146579975" />
         <tli id="T65" time="54.285" type="appl" />
         <tli id="T66" time="55.633" type="appl" />
         <tli id="T67" time="56.98" type="appl" />
         <tli id="T68" time="58.327" type="appl" />
         <tli id="T69" time="59.675" type="appl" />
         <tli id="T70" time="61.022" type="appl" />
         <tli id="T71" time="62.369" type="appl" />
         <tli id="T72" time="63.717" type="appl" />
         <tli id="T73" time="64.95066619617113" />
         <tli id="T74" time="65.743" type="appl" />
         <tli id="T75" time="66.423" type="appl" />
         <tli id="T76" time="67.102" type="appl" />
         <tli id="T77" time="67.781" type="appl" />
         <tli id="T78" time="68.46" type="appl" />
         <tli id="T79" time="69.14" type="appl" />
         <tli id="T80" time="69.819" type="appl" />
         <tli id="T81" time="70.498" type="appl" />
         <tli id="T82" time="71.177" type="appl" />
         <tli id="T83" time="71.857" type="appl" />
         <tli id="T84" time="72.66933515511302" />
         <tli id="T85" time="73.306" type="appl" />
         <tli id="T86" time="74.076" type="appl" />
         <tli id="T87" time="74.846" type="appl" />
         <tli id="T88" time="75.616" type="appl" />
         <tli id="T89" time="76.387" type="appl" />
         <tli id="T90" time="77.157" type="appl" />
         <tli id="T91" time="77.927" type="appl" />
         <tli id="T92" time="78.697" type="appl" />
         <tli id="T93" time="80.57949379652607" />
         <tli id="T94" time="80.585" type="appl" />
         <tli id="T95" time="81.702" type="appl" />
         <tli id="T96" time="82.82" type="appl" />
         <tli id="T97" time="83.95800121844603" />
         <tli id="T98" time="84.612" type="appl" />
         <tli id="T99" time="85.285" type="appl" />
         <tli id="T100" time="85.959" type="appl" />
         <tli id="T101" time="86.633" type="appl" />
         <tli id="T102" time="87.306" type="appl" />
         <tli id="T103" time="88.06666681390101" />
         <tli id="T104" time="88.796" type="appl" />
         <tli id="T105" time="89.611" type="appl" />
         <tli id="T106" time="90.427" type="appl" />
         <tli id="T107" time="91.242" type="appl" />
         <tli id="T108" time="93.24608089120625" />
         <tli id="T109" time="93.258" type="appl" />
         <tli id="T110" time="94.459" type="appl" />
         <tli id="T111" time="95.66" type="appl" />
         <tli id="T112" time="97.686052999131" />
         <tli id="T113" time="97.76" type="appl" />
         <tli id="T114" time="98.66" type="appl" />
         <tli id="T115" time="99.56" type="appl" />
         <tli id="T116" time="100.46" type="appl" />
         <tli id="T117" time="101.36" type="appl" />
         <tli id="T118" time="102.259" type="appl" />
         <tli id="T119" time="103.159" type="appl" />
         <tli id="T120" time="104.059" type="appl" />
         <tli id="T121" time="104.959" type="appl" />
         <tli id="T122" time="106.07900157638912" />
         <tli id="T123" time="106.76" type="appl" />
         <tli id="T124" time="107.661" type="appl" />
         <tli id="T125" time="108.562" type="appl" />
         <tli id="T126" time="109.464" type="appl" />
         <tli id="T127" time="110.365" type="appl" />
         <tli id="T128" time="111.266" type="appl" />
         <tli id="T129" time="112.32700269078954" />
         <tli id="T130" time="112.904" type="appl" />
         <tli id="T131" time="113.641" type="appl" />
         <tli id="T132" time="114.378" type="appl" />
         <tli id="T133" time="115.115" type="appl" />
         <tli id="T134" time="115.852" type="appl" />
         <tli id="T135" time="116.589" type="appl" />
         <tli id="T136" time="117.326" type="appl" />
         <tli id="T137" time="118.063" type="appl" />
         <tli id="T138" time="119.15999361984483" />
         <tli id="T139" time="120.353" type="appl" />
         <tli id="T140" time="121.907" type="appl" />
         <tli id="T141" time="123.53333854215745" />
         <tli id="T142" time="124.088" type="appl" />
         <tli id="T143" time="124.716" type="appl" />
         <tli id="T144" time="125.344" type="appl" />
         <tli id="T145" time="125.972" type="appl" />
         <tli id="T146" time="126.74666471008574" />
         <tli id="T147" time="127.373" type="appl" />
         <tli id="T148" time="128.147" type="appl" />
         <tli id="T149" time="128.92" type="appl" />
         <tli id="T150" time="129.694" type="appl" />
         <tli id="T151" time="130.468" type="appl" />
         <tli id="T152" time="131.241" type="appl" />
         <tli id="T153" time="132.014" type="appl" />
         <tli id="T154" time="132.788" type="appl" />
         <tli id="T155" time="133.562" type="appl" />
         <tli id="T156" time="135.2191505481044" />
         <tli id="T157" time="135.298" type="appl" />
         <tli id="T158" time="136.261" type="appl" />
         <tli id="T159" time="137.224" type="appl" />
         <tli id="T160" time="138.187" type="appl" />
         <tli id="T161" time="139.15" type="appl" />
         <tli id="T162" time="139.272" type="appl" />
         <tli id="T163" time="140.95911448943053" />
         <tli id="T164" time="150.62572043010752" />
         <tli id="T165" />
         <tli id="T166" />
         <tli id="T167" />
         <tli id="T168" />
         <tli id="T169" />
         <tli id="T170" />
         <tli id="T171" />
         <tli id="T172" />
         <tli id="T173" time="151.131" type="appl" />
         <tli id="T174" time="151.941" type="appl" />
         <tli id="T175" time="152.751" type="appl" />
         <tli id="T176" time="153.56" type="appl" />
         <tli id="T177" time="154.37" type="appl" />
         <tli id="T178" time="155.18" type="appl" />
         <tli id="T179" time="155.99" type="appl" />
         <tli id="T180" time="156.548" type="appl" />
         <tli id="T181" time="157.107" type="appl" />
         <tli id="T182" time="157.665" type="appl" />
         <tli id="T183" time="158.224" type="appl" />
         <tli id="T184" time="158.782" type="appl" />
         <tli id="T185" time="159.341" type="appl" />
         <tli id="T186" time="160.1456606254777" />
         <tli id="T187" time="160.733" type="appl" />
         <tli id="T188" time="161.566" type="appl" />
         <tli id="T189" time="162.4" type="appl" />
         <tli id="T190" time="163.233" type="appl" />
         <tli id="T191" time="164.067" type="appl" />
         <tli id="T192" time="165.63229282491022" />
         <tli id="T193" time="166.115" type="appl" />
         <tli id="T194" time="168.13894374469956" />
         <tli id="T195" time="168.66" type="appl" />
         <tli id="T196" time="169.989" type="appl" />
         <tli id="T197" time="171.319" type="appl" />
         <tli id="T198" time="172.649" type="appl" />
         <tli id="T199" time="173.979" type="appl" />
         <tli id="T200" time="175.308" type="appl" />
         <tli id="T201" time="176.55800543916408" />
         <tli id="T202" time="177.445" type="appl" />
         <tli id="T203" time="178.252" type="appl" />
         <tli id="T204" time="179.059" type="appl" />
         <tli id="T205" time="179.866" type="appl" />
         <tli id="T206" time="180.673" type="appl" />
         <tli id="T207" time="181.41333952110227" />
         <tli id="T208" time="182.267" type="appl" />
         <tli id="T209" time="183.054" type="appl" />
         <tli id="T210" time="183.84" type="appl" />
         <tli id="T211" time="184.627" type="appl" />
         <tli id="T212" time="185.9054988011852" />
         <tli id="T213" time="186.126" type="appl" />
         <tli id="T214" time="186.838" type="appl" />
         <tli id="T215" time="187.55" type="appl" />
         <tli id="T216" time="188.262" type="appl" />
         <tli id="T217" time="188.974" type="appl" />
         <tli id="T218" time="189.686" type="appl" />
         <tli id="T219" time="190.398" type="appl" />
         <tli id="T220" time="191.81665958371286" />
         <tli id="T221" time="191.869" type="appl" />
         <tli id="T222" time="192.628" type="appl" />
         <tli id="T223" time="193.387" type="appl" />
         <tli id="T224" time="194.146" type="appl" />
         <tli id="T225" time="194.906" type="appl" />
         <tli id="T226" time="195.665" type="appl" />
         <tli id="T227" time="196.424" type="appl" />
         <tli id="T228" time="197.36300494969166" />
         <tli id="T229" time="197.984" type="appl" />
         <tli id="T230" time="198.785" type="appl" />
         <tli id="T231" time="199.586" type="appl" />
         <tli id="T232" time="200.388" type="appl" />
         <tli id="T233" time="201.189" type="appl" />
         <tli id="T234" time="201.99" type="appl" />
         <tli id="T235" time="202.76433820580874" />
         <tli id="T236" time="203.37801924647422" type="intp" />
         <tli id="T237" time="203.99170028713968" type="intp" />
         <tli id="T238" time="204.60538132780516" />
         <tli id="T239" time="205.7109299382619" type="intp" />
         <tli id="T240" time="206.81647854871864" type="intp" />
         <tli id="T241" time="207.92202715917537" type="intp" />
         <tli id="T242" time="209.0275757696321" type="intp" />
         <tli id="T243" time="210.13312438008887" type="intp" />
         <tli id="T244" time="211.2386729905456" />
         <tli id="T245" time="211.366" type="appl" />
         <tli id="T246" time="212.677" type="appl" />
         <tli id="T247" time="213.988" type="appl" />
         <tli id="T248" time="215.3" type="appl" />
         <tli id="T249" time="216.612" type="appl" />
         <tli id="T250" time="217.923" type="appl" />
         <tli id="T251" time="219.234" type="appl" />
         <tli id="T252" time="220.546" type="appl" />
         <tli id="T253" time="221.29385153961323" type="intp" />
         <tli id="T254" time="222.04170307922647" type="intp" />
         <tli id="T255" time="222.7895546188397" type="intp" />
         <tli id="T256" time="223.53740615845294" type="intp" />
         <tli id="T257" time="224.28525769806618" />
         <tli id="T258" time="224.61983846537746" type="intp" />
         <tli id="T259" time="224.95441923268874" type="intp" />
         <tli id="T260" time="225.289" type="appl" />
         <tli id="T261" time="226.87" type="appl" />
         <tli id="T262" time="228.451" type="appl" />
         <tli id="T263" time="230.71188399241973" />
         <tli id="T264" time="230.714" type="appl" />
         <tli id="T265" time="231.395" type="appl" />
         <tli id="T266" time="232.077" type="appl" />
         <tli id="T267" time="232.758" type="appl" />
         <tli id="T268" time="233.18666011244778" />
         <tli id="T269" time="234.098" type="appl" />
         <tli id="T270" time="234.756" type="appl" />
         <tli id="T271" time="235.414" type="appl" />
         <tli id="T272" time="236.072" type="appl" />
         <tli id="T273" time="236.76" type="appl" />
         <tli id="T274" time="237.448" type="appl" />
         <tli id="T275" time="238.135" type="appl" />
         <tli id="T276" time="238.823" type="appl" />
         <tli id="T277" time="239.511" type="appl" />
         <tli id="T278" time="240.199" type="appl" />
         <tli id="T279" time="240.886" type="appl" />
         <tli id="T280" time="241.574" type="appl" />
         <tli id="T281" time="242.87847422809938" />
         <tli id="T282" time="242.969" type="appl" />
         <tli id="T283" time="243.677" type="appl" />
         <tli id="T284" time="244.384" type="appl" />
         <tli id="T285" time="245.092" type="appl" />
         <tli id="T286" time="245.799" type="appl" />
         <tli id="T287" time="246.507" type="appl" />
         <tli id="T288" time="247.214" type="appl" />
         <tli id="T289" time="247.922" type="appl" />
         <tli id="T290" time="249.50509926605312" />
         <tli id="T291" time="249.877" type="appl" />
         <tli id="T292" time="251.125" type="appl" />
         <tli id="T293" time="252.373" type="appl" />
         <tli id="T294" time="253.62" type="appl" />
         <tli id="T295" time="254.868" type="appl" />
         <tli id="T296" time="256.116" type="appl" />
         <tli id="T297" time="257.364" type="appl" />
         <tli id="T298" time="258.75199430170346" />
         <tli id="T299" time="259.69" type="appl" />
         <tli id="T300" time="260.768" type="appl" />
         <tli id="T301" time="261.846" type="appl" />
         <tli id="T302" time="262.923" type="appl" />
         <tli id="T303" time="264.001" type="appl" />
         <tli id="T304" time="265.079" type="appl" />
         <tli id="T305" time="266.4836644810545" />
         <tli id="T306" time="267.6983183088859" />
         <tli id="T307" time="267.9624925924762" type="intp" />
         <tli id="T308" time="268.26" type="appl" />
         <tli id="T309" time="269.311" type="appl" />
         <tli id="T310" time="270.363" type="appl" />
         <tli id="T311" time="271.84066207818995" />
         <tli id="T312" time="272.448" type="appl" />
         <tli id="T313" time="273.482" type="appl" />
         <tli id="T314" time="274.516" type="appl" />
         <tli id="T315" time="275.55" type="appl" />
         <tli id="T316" time="276.584" type="appl" />
         <tli id="T317" time="277.618" type="appl" />
         <tli id="T318" time="278.291" type="appl" />
         <tli id="T319" time="278.964" type="appl" />
         <tli id="T320" time="279.637" type="appl" />
         <tli id="T321" time="280.31" type="appl" />
         <tli id="T322" time="280.983" type="appl" />
         <tli id="T323" time="281.655" type="appl" />
         <tli id="T324" time="282.328" type="appl" />
         <tli id="T325" time="283.001" type="appl" />
         <tli id="T326" time="283.674" type="appl" />
         <tli id="T327" time="284.347" type="appl" />
         <tli id="T328" time="285.3266711085634" />
         <tli id="T329" time="286.326" type="appl" />
         <tli id="T330" time="287.632" type="appl" />
         <tli id="T331" time="288.938" type="appl" />
         <tli id="T332" time="290.244" type="appl" />
         <tli id="T333" time="291.55" type="appl" />
         <tli id="T334" time="292.49" type="appl" />
         <tli id="T335" time="293.429" type="appl" />
         <tli id="T336" time="294.369" type="appl" />
         <tli id="T337" time="295.309" type="appl" />
         <tli id="T338" time="296.249" type="appl" />
         <tli id="T339" time="297.188" type="appl" />
         <tli id="T340" time="298.45466155469006" />
         <tli id="T341" time="299.021" type="appl" />
         <tli id="T342" time="299.914" type="appl" />
         <tli id="T343" time="300.807" type="appl" />
         <tli id="T344" time="301.57333987969974" />
         <tli id="T345" time="302.713" type="appl" />
         <tli id="T346" time="303.727" type="appl" />
         <tli id="T347" time="304.74" type="appl" />
         <tli id="T348" time="305.754" type="appl" />
         <tli id="T349" time="306.767" type="appl" />
         <tli id="T350" time="307.781" type="appl" />
         <tli id="T351" time="308.73399802117035" />
         <tli id="T352" time="309.493" type="appl" />
         <tli id="T353" time="310.192" type="appl" />
         <tli id="T354" time="310.891" type="appl" />
         <tli id="T355" time="311.591" type="appl" />
         <tli id="T356" time="312.29" type="appl" />
         <tli id="T357" time="312.989" type="appl" />
         <tli id="T358" time="313.688" type="appl" />
         <tli id="T359" time="314.387" type="appl" />
         <tli id="T360" time="315.086" type="appl" />
         <tli id="T361" time="315.786" type="appl" />
         <tli id="T362" time="316.485" type="appl" />
         <tli id="T363" time="317.184" type="appl" />
         <tli id="T364" time="318.4696660279968" />
         <tli id="T365" time="318.888" type="appl" />
         <tli id="T366" time="319.893" type="appl" />
         <tli id="T367" time="320.898" type="appl" />
         <tli id="T368" time="321.903" type="appl" />
         <tli id="T369" time="322.908" type="appl" />
         <tli id="T370" time="323.913" type="appl" />
         <tli id="T371" time="324.918" type="appl" />
         <tli id="T372" time="325.703" type="appl" />
         <tli id="T373" time="326.489" type="appl" />
         <tli id="T374" time="327.274" type="appl" />
         <tli id="T375" time="328.059" type="appl" />
         <tli id="T376" time="328.845" type="appl" />
         <tli id="T377" time="331.0445870318602" />
         <tli id="T378" time="331.048" type="appl" />
         <tli id="T379" time="332.465" type="appl" />
         <tli id="T380" time="333.882" type="appl" />
         <tli id="T381" time="335.1666705117735" />
         <tli id="T382" time="336.641" type="appl" />
         <tli id="T383" time="337.981" type="appl" />
         <tli id="T384" time="339.322" type="appl" />
         <tli id="T385" time="340.662" type="appl" />
         <tli id="T386" time="342.002" type="appl" />
         <tli id="T387" time="343.343" type="appl" />
         <tli id="T388" time="344.684" type="appl" />
         <tli id="T389" time="346.07066451246453" />
         <tli id="T390" time="346.686" type="appl" />
         <tli id="T391" time="347.349" type="appl" />
         <tli id="T392" time="348.011" type="appl" />
         <tli id="T393" time="348.673" type="appl" />
         <tli id="T394" time="349.335" type="appl" />
         <tli id="T395" time="349.998" type="appl" />
         <tli id="T396" time="350.56667793500225" />
         <tli id="T397" time="351.86" type="appl" />
         <tli id="T398" time="353.061" type="appl" />
         <tli id="T399" time="354.261" type="appl" />
         <tli id="T400" time="355.462" type="appl" />
         <tli id="T401" time="356.78867009820857" />
         <tli id="T402" time="358.188" type="appl" />
         <tli id="T403" time="359.714" type="appl" />
         <tli id="T404" time="361.241" type="appl" />
         <tli id="T405" time="363.65104886348166" />
         <tli id="T406" time="363.9507142857143" type="intp" />
         <tli id="T407" time="365.1344285714286" type="intp" />
         <tli id="T408" time="366.3181428571429" type="intp" />
         <tli id="T409" time="367.50185714285715" type="intp" />
         <tli id="T410" time="368.68557142857145" type="intp" />
         <tli id="T411" time="370.0692897938457" />
         <tli id="T412" time="371.053" type="appl" />
         <tli id="T413" time="371.999" type="appl" />
         <tli id="T414" time="372.945" type="appl" />
         <tli id="T415" time="373.891" type="appl" />
         <tli id="T416" time="374.837" type="appl" />
         <tli id="T417" time="375.783" type="appl" />
         <tli id="T418" time="376.729" type="appl" />
         <tli id="T419" time="377.675" type="appl" />
         <tli id="T420" time="378.494341034017" />
         <tli id="T421" time="379.342" type="appl" />
         <tli id="T422" time="380.064" type="appl" />
         <tli id="T423" time="380.785" type="appl" />
         <tli id="T424" time="381.507" type="appl" />
         <tli id="T425" time="382.228" type="appl" />
         <tli id="T426" time="383.52331985844563" />
         <tli id="T427" time="383.849" type="appl" />
         <tli id="T428" time="384.747" type="appl" />
         <tli id="T429" time="385.646" type="appl" />
         <tli id="T430" time="386.544" type="appl" />
         <tli id="T431" time="387.443" type="appl" />
         <tli id="T432" time="388.341" type="appl" />
         <tli id="T433" time="389.9908833956298" />
         <tli id="T434" time="390.134" type="appl" />
         <tli id="T435" time="391.029" type="appl" />
         <tli id="T436" time="391.923" type="appl" />
         <tli id="T437" time="392.817" type="appl" />
         <tli id="T438" time="393.711" type="appl" />
         <tli id="T439" time="394.606" type="appl" />
         <tli id="T440" time="396.69084130623696" />
         <tli id="T441" time="396.697" type="appl" />
         <tli id="T442" time="397.894" type="appl" />
         <tli id="T443" time="399.091" type="appl" />
         <tli id="T444" time="400.289" type="appl" />
         <tli id="T445" time="401.486" type="appl" />
         <tli id="T446" time="402.683" type="appl" />
         <tli id="T447" time="403.77332285548266" />
         <tli id="T448" time="404.396" type="intp" />
         <tli id="T449" time="404.91200000000003" type="intp" />
         <tli id="T450" time="405.58799895561765" />
         <tli id="T451" time="405.944" type="appl" />
         <tli id="T452" time="408.14899849232023" />
         <tli id="T453" time="408.645" type="appl" />
         <tli id="T454" time="409.282" type="appl" />
         <tli id="T455" time="409.918" type="appl" />
         <tli id="T456" time="410.554" type="appl" />
         <tli id="T457" time="411.191" type="appl" />
         <tli id="T458" time="411.827" type="appl" />
         <tli id="T459" time="412.463" type="appl" />
         <tli id="T460" time="413.099" type="appl" />
         <tli id="T461" time="413.736" type="appl" />
         <tli id="T462" time="414.7053375134801" />
         <tli id="T463" time="414.955" type="appl" />
         <tli id="T464" time="415.539" type="appl" />
         <tli id="T465" time="416.122" type="appl" />
         <tli id="T466" time="416.705" type="appl" />
         <tli id="T467" time="417.288" type="appl" />
         <tli id="T468" time="417.872" type="appl" />
         <tli id="T469" time="419.06166431353455" />
         <tli id="T470" time="419.156" type="appl" />
         <tli id="T471" time="419.857" type="appl" />
         <tli id="T472" time="420.557" type="appl" />
         <tli id="T473" time="421.258" type="appl" />
         <tli id="T474" time="421.959" type="appl" />
         <tli id="T475" time="422.66" type="appl" />
         <tli id="T476" time="423.361" type="appl" />
         <tli id="T477" time="424.062" type="appl" />
         <tli id="T478" time="424.762" type="appl" />
         <tli id="T479" time="425.463" type="appl" />
         <tli id="T480" time="426.26398885992194" />
         <tli id="T723" time="426.33349999999996" type="intp" />
         <tli id="T481" time="426.503" type="appl" />
         <tli id="T482" time="426.842" type="appl" />
         <tli id="T483" time="427.18" type="appl" />
         <tli id="T484" time="431.28395732428726" />
         <tli id="T485" time="437.56391787333393" />
         <tli id="T486" />
         <tli id="T487" />
         <tli id="T488" />
         <tli id="T489" />
         <tli id="T490" />
         <tli id="T491" />
         <tli id="T492" />
         <tli id="T494" />
         <tli id="T495" time="438.509" type="appl" />
         <tli id="T496" time="439.393" type="appl" />
         <tli id="T497" time="440.276" type="appl" />
         <tli id="T498" time="441.159" type="appl" />
         <tli id="T499" time="442.042" type="appl" />
         <tli id="T500" time="442.926" type="appl" />
         <tli id="T501" time="443.809" type="appl" />
         <tli id="T502" time="444.692" type="appl" />
         <tli id="T503" time="445.575" type="appl" />
         <tli id="T504" time="446.459" type="appl" />
         <tli id="T505" time="447.4905221806912" />
         <tli id="T506" time="448.431" type="appl" />
         <tli id="T507" time="449.52" type="appl" />
         <tli id="T508" time="450.609" type="appl" />
         <tli id="T509" time="451.698" type="appl" />
         <tli id="T510" time="452.8603321973385" />
         <tli id="T511" time="453.641" type="appl" />
         <tli id="T512" time="454.494" type="appl" />
         <tli id="T513" time="455.348" type="appl" />
         <tli id="T514" time="456.202" type="appl" />
         <tli id="T515" time="457.056" type="appl" />
         <tli id="T516" time="457.909" type="appl" />
         <tli id="T517" time="458.763" type="appl" />
         <tli id="T518" time="459.617" type="appl" />
         <tli id="T519" time="460.471" type="appl" />
         <tli id="T520" time="461.324" type="appl" />
         <tli id="T521" time="462.3179811173582" />
         <tli id="T522" time="463.048" type="appl" />
         <tli id="T523" time="463.917" type="appl" />
         <tli id="T524" time="464.787" type="appl" />
         <tli id="T525" time="465.656" type="appl" />
         <tli id="T526" time="466.97039980735207" />
         <tli id="T527" time="467.295" type="appl" />
         <tli id="T528" time="468.064" type="appl" />
         <tli id="T529" time="468.833" type="appl" />
         <tli id="T530" time="469.603" type="appl" />
         <tli id="T531" time="470.372" type="appl" />
         <tli id="T532" time="471.141" type="appl" />
         <tli id="T533" time="471.91" type="appl" />
         <tli id="T534" time="472.679" type="appl" />
         <tli id="T535" time="473.448" type="appl" />
         <tli id="T536" time="474.218" type="appl" />
         <tli id="T537" time="474.987" type="appl" />
         <tli id="T538" time="475.756" type="appl" />
         <tli id="T539" time="477.33700135062975" />
         <tli id="T540" time="477.667" type="appl" />
         <tli id="T541" time="478.808" type="appl" />
         <tli id="T542" time="479.9033393954623" />
         <tli id="T543" time="481.141" type="appl" />
         <tli id="T544" time="482.333" type="appl" />
         <tli id="T545" time="483.524" type="appl" />
         <tli id="T546" time="484.715" type="appl" />
         <tli id="T547" time="485.907" type="appl" />
         <tli id="T548" time="487.3913131890568" />
         <tli id="T549" time="488.108" type="appl" />
         <tli id="T550" time="489.119" type="appl" />
         <tli id="T551" time="490.129" type="appl" />
         <tli id="T552" time="491.139" type="appl" />
         <tli id="T553" time="492.15" type="appl" />
         <tli id="T554" time="493.8768974463675" />
         <tli id="T555" time="493.94" type="appl" />
         <tli id="T556" time="494.72" type="appl" />
         <tli id="T557" time="495.33334663023106" />
         <tli id="T558" time="496.145" type="appl" />
         <tli id="T559" time="496.789" type="appl" />
         <tli id="T560" time="497.434" type="appl" />
         <tli id="T561" time="498.079" type="appl" />
         <tli id="T562" time="498.723" type="appl" />
         <tli id="T563" time="499.368" type="appl" />
         <tli id="T564" time="500.012" type="appl" />
         <tli id="T565" time="500.657" type="appl" />
         <tli id="T566" time="501.302" type="appl" />
         <tli id="T567" time="501.946" type="appl" />
         <tli id="T568" time="503.0168400288972" />
         <tli id="T569" time="503.649" type="appl" />
         <tli id="T570" time="504.707" type="appl" />
         <tli id="T571" time="505.764" type="appl" />
         <tli id="T572" time="506.822" type="appl" />
         <tli id="T573" time="507.88" type="appl" />
         <tli id="T574" time="509.1379994922051" />
         <tli id="T575" time="509.761" type="appl" />
         <tli id="T576" time="510.584" type="appl" />
         <tli id="T577" time="511.3736625310174" />
         <tli id="T578" time="512.17" type="appl" />
         <tli id="T579" time="512.932" type="appl" />
         <tli id="T580" time="513.695" type="appl" />
         <tli id="T581" time="514.457" type="appl" />
         <tli id="T582" time="515.22" type="appl" />
         <tli id="T583" time="515.982" type="appl" />
         <tli id="T584" time="516.745" type="appl" />
         <tli id="T585" time="517.507" type="appl" />
         <tli id="T586" time="519.6434022468617" />
         <tli id="T587" time="519.8723333333334" type="intp" />
         <tli id="T588" time="522.9167150171185" />
         <tli id="T589" time="523.077" type="appl" />
         <tli id="T590" time="523.994" type="appl" />
         <tli id="T591" time="524.912" type="appl" />
         <tli id="T592" time="525.83" type="appl" />
         <tli id="T593" time="526.698" type="appl" />
         <tli id="T594" time="527.565" type="appl" />
         <tli id="T595" time="528.432" type="appl" />
         <tli id="T596" time="529.3" type="appl" />
         <tli id="T597" time="530.768" type="appl" />
         <tli id="T598" time="532.235" type="appl" />
         <tli id="T599" time="533.703" type="appl" />
         <tli id="T600" time="535.17" type="appl" />
         <tli id="T601" time="536.638" type="appl" />
         <tli id="T602" time="537.262" type="appl" />
         <tli id="T603" time="537.885" type="appl" />
         <tli id="T604" time="538.509" type="appl" />
         <tli id="T605" time="539.132" type="appl" />
         <tli id="T606" time="539.756" type="appl" />
         <tli id="T607" time="540.379" type="appl" />
         <tli id="T608" time="541.003" type="appl" />
         <tli id="T609" time="541.626" type="appl" />
         <tli id="T610" time="542.3766448524254" />
         <tli id="T611" time="542.729" type="appl" />
         <tli id="T612" time="543.208" type="appl" />
         <tli id="T613" time="543.687" type="appl" />
         <tli id="T614" time="544.167" type="appl" />
         <tli id="T615" time="544.646" type="appl" />
         <tli id="T616" time="545.125" type="appl" />
         <tli id="T617" time="545.8240190292217" />
         <tli id="T618" time="546.592" type="appl" />
         <tli id="T619" time="547.581" type="appl" />
         <tli id="T620" time="548.569" type="appl" />
         <tli id="T621" time="549.557" type="appl" />
         <tli id="T622" time="550.546" type="appl" />
         <tli id="T623" time="551.534" type="appl" />
         <tli id="T624" time="552.522" type="appl" />
         <tli id="T625" time="553.511" type="appl" />
         <tli id="T626" time="554.5923493629006" />
         <tli id="T627" time="555.303" type="appl" />
         <tli id="T628" time="556.106" type="appl" />
         <tli id="T629" time="556.6766591858529" />
         <tli id="T630" time="557.782" type="appl" />
         <tli id="T631" time="558.655" type="appl" />
         <tli id="T632" time="559.527" type="appl" />
         <tli id="T633" time="560.399" type="appl" />
         <tli id="T634" time="561.272" type="appl" />
         <tli id="T635" time="562.144" type="appl" />
         <tli id="T636" time="563.016" type="appl" />
         <tli id="T637" time="563.888" type="appl" />
         <tli id="T638" time="564.761" type="appl" />
         <tli id="T639" time="565.5463430494916" />
         <tli id="T640" time="566.664" type="appl" />
         <tli id="T641" time="567.695" type="appl" />
         <tli id="T642" time="568.726" type="appl" />
         <tli id="T643" time="569.757" type="appl" />
         <tli id="T644" time="570.788" type="appl" />
         <tli id="T645" time="571.799012103318" />
         <tli id="T646" time="572.447" type="appl" />
         <tli id="T647" time="573.075" type="appl" />
         <tli id="T648" time="573.704" type="appl" />
         <tli id="T649" time="574.332" type="appl" />
         <tli id="T650" time="575.1399806985582" />
         <tli id="T651" time="576.215" type="appl" />
         <tli id="T652" time="578.1497013747107" />
         <tli id="T653" time="578.522" type="appl" />
         <tli id="T654" time="579.574" type="appl" />
         <tli id="T655" time="580.626" type="appl" />
         <tli id="T656" time="581.678" type="appl" />
         <tli id="T657" time="582.73" type="appl" />
         <tli id="T658" time="583.782" type="appl" />
         <tli id="T659" time="585.5963212614254" />
         <tli id="T660" time="585.602" type="appl" />
         <tli id="T661" time="586.371" type="appl" />
         <tli id="T662" time="587.139" type="appl" />
         <tli id="T663" time="587.907" type="appl" />
         <tli id="T664" time="588.675" type="appl" />
         <tli id="T665" time="589.444" type="appl" />
         <tli id="T666" time="590.212" type="appl" />
         <tli id="T667" time="590.98" type="appl" />
         <tli id="T668" time="591.748" type="appl" />
         <tli id="T669" time="592.517" type="appl" />
         <tli id="T670" time="593.4383553307996" />
         <tli id="T671" time="594.082" type="appl" />
         <tli id="T672" time="594.878" type="appl" />
         <tli id="T673" time="595.675" type="appl" />
         <tli id="T674" time="596.472" type="appl" />
         <tli id="T675" time="597.269" type="appl" />
         <tli id="T676" time="598.065" type="appl" />
         <tli id="T677" time="598.862" type="appl" />
         <tli id="T678" time="599.659" type="appl" />
         <tli id="T679" time="600.456" type="appl" />
         <tli id="T680" time="601.252" type="appl" />
         <tli id="T681" time="602.049" type="appl" />
         <tli id="T682" time="603.3060016751997" />
         <tli id="T683" time="603.722" type="appl" />
         <tli id="T684" time="604.597" type="appl" />
         <tli id="T685" time="605.473" type="appl" />
         <tli id="T686" time="606.348" type="appl" />
         <tli id="T687" time="607.224" type="appl" />
         <tli id="T688" time="608.6495097737434" />
         <tli id="T689" time="609.057" type="appl" />
         <tli id="T690" time="610.014" type="appl" />
         <tli id="T691" time="610.972" type="appl" />
         <tli id="T692" time="611.929" type="appl" />
         <tli id="T693" time="612.887" type="appl" />
         <tli id="T694" time="613.844" type="appl" />
         <tli id="T695" time="614.802" type="appl" />
         <tli id="T696" time="615.759" type="appl" />
         <tli id="T697" time="616.88367679639" />
         <tli id="T698" time="617.119" type="appl" />
         <tli id="T699" time="617.522" type="appl" />
         <tli id="T700" time="617.924" type="appl" />
         <tli id="T701" time="618.327" type="appl" />
         <tli id="T702" time="618.729" type="appl" />
         <tli id="T703" time="619.132" type="appl" />
         <tli id="T704" time="619.534" type="appl" />
         <tli id="T705" time="619.937" type="appl" />
         <tli id="T706" time="623.4627500497326" />
         <tli id="T707" time="623.905" type="appl" />
         <tli id="T708" time="624.638" type="appl" />
         <tli id="T709" time="625.372" type="appl" />
         <tli id="T710" time="626.105" type="appl" />
         <tli id="T711" time="626.839" type="appl" />
         <tli id="T712" time="627.748" type="appl" />
         <tli id="T713" time="628.657" type="appl" />
         <tli id="T714" time="629.566" type="appl" />
         <tli id="T715" time="630.475" type="appl" />
         <tli id="T716" time="631.384" type="appl" />
         <tli id="T717" time="632.293" type="appl" />
         <tli id="T718" time="633.202" type="appl" />
         <tli id="T719" time="634.111" type="appl" />
         <tli id="T720" time="635.02" type="appl" />
         <tli id="T721" time="635.929" type="appl" />
         <tli id="T722" time="636.838" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PoNA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T722" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Ihilleːŋ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">öhü</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_9" n="HIAT:ip">"</nts>
                  <ts e="T3" id="Seg_11" n="HIAT:w" s="T2">Dʼikti</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">tabalar</ts>
                  <nts id="Seg_15" n="HIAT:ip">"</nts>
                  <nts id="Seg_16" n="HIAT:ip">.</nts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_19" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_21" n="HIAT:w" s="T4">Ühüs</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_24" n="HIAT:w" s="T5">künüger</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_27" n="HIAT:w" s="T6">ogoloːk</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_30" n="HIAT:w" s="T7">kös</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_33" n="HIAT:w" s="T8">tiːjbit</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">Valačaŋkaga</ts>
                  <nts id="Seg_37" n="HIAT:ip">.</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_40" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_42" n="HIAT:w" s="T10">Avam</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_45" n="HIAT:w" s="T11">rajonun</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">kiːn</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">ologun</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">nʼuːččalara</ts>
                  <nts id="Seg_55" n="HIAT:ip">,</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">hakalara</ts>
                  <nts id="Seg_59" n="HIAT:ip">,</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">barɨlara</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">komullubuttara</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">keliː</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">ogoloru</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">köröːrü</ts>
                  <nts id="Seg_75" n="HIAT:ip">,</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">kukla</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T22">kördük</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_84" n="HIAT:w" s="T23">taŋastaːktarɨ</ts>
                  <nts id="Seg_85" n="HIAT:ip">,</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_88" n="HIAT:w" s="T24">kihi</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_91" n="HIAT:w" s="T25">taptɨ͡aktarɨ</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_95" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">Olus</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_100" n="HIAT:w" s="T27">aːjdaːna</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_103" n="HIAT:w" s="T28">hu͡ok</ts>
                  <nts id="Seg_104" n="HIAT:ip">,</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">dʼɨlɨgɨr</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">Valačaːnka</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">olok</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_116" n="HIAT:w" s="T32">biːr</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_119" n="HIAT:w" s="T33">duruk</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_122" n="HIAT:w" s="T34">tillibit</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_125" n="HIAT:w" s="T35">kördük</ts>
                  <nts id="Seg_126" n="HIAT:ip">,</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_129" n="HIAT:w" s="T36">taŋara</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_132" n="HIAT:w" s="T37">künün</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_135" n="HIAT:w" s="T38">beli͡etiːr</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_138" n="HIAT:w" s="T39">kördük</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_141" n="HIAT:w" s="T40">bu͡olan</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_144" n="HIAT:w" s="T41">kaːlbɨt</ts>
                  <nts id="Seg_145" n="HIAT:ip">.</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_148" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_150" n="HIAT:w" s="T42">Ogolor</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_153" n="HIAT:w" s="T43">bu͡ollaktarɨna</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_156" n="HIAT:w" s="T44">kihileri</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_159" n="HIAT:w" s="T45">körböttör</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_163" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_165" n="HIAT:w" s="T46">Nʼuːčča</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_168" n="HIAT:w" s="T47">dʼi͡eler</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_171" n="HIAT:w" s="T48">di͡ek</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_174" n="HIAT:w" s="T49">agaj</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_176" n="HIAT:ip">(</nts>
                  <ts e="T52" id="Seg_178" n="HIAT:w" s="T50">kar-</ts>
                  <nts id="Seg_179" n="HIAT:ip">)</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_182" n="HIAT:w" s="T52">karaktara</ts>
                  <nts id="Seg_183" n="HIAT:ip">.</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_186" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_188" n="HIAT:w" s="T53">Ulʼica</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_191" n="HIAT:w" s="T54">üstün</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_194" n="HIAT:w" s="T55">ataralatan</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_197" n="HIAT:w" s="T56">ihenner</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_200" n="HIAT:w" s="T57">uːčagɨnan</ts>
                  <nts id="Seg_201" n="HIAT:ip">.</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_204" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_206" n="HIAT:w" s="T58">Ehelere</ts>
                  <nts id="Seg_207" n="HIAT:ip">,</nts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_210" n="HIAT:w" s="T59">Anʼiːsʼim</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_213" n="HIAT:w" s="T60">ogonnʼor</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_216" n="HIAT:w" s="T61">turbut</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_219" n="HIAT:w" s="T62">asku͡ola</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_222" n="HIAT:w" s="T63">attɨgar</ts>
                  <nts id="Seg_223" n="HIAT:ip">.</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_226" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_228" n="HIAT:w" s="T64">Ginini</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_231" n="HIAT:w" s="T65">tögürüččü</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_234" n="HIAT:w" s="T66">aːjdannaːk</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_237" n="HIAT:w" s="T67">armʼija</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_240" n="HIAT:w" s="T68">ugučaktarɨnan</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_242" n="HIAT:ip">(</nts>
                  <ts e="T71" id="Seg_244" n="HIAT:w" s="T69">ürd-</ts>
                  <nts id="Seg_245" n="HIAT:ip">)</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_248" n="HIAT:w" s="T71">ürdüleritten</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_251" n="HIAT:w" s="T72">ɨstaŋalaːbɨttar</ts>
                  <nts id="Seg_252" n="HIAT:ip">.</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_255" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_257" n="HIAT:w" s="T73">Asku͡ola</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_260" n="HIAT:w" s="T74">attɨgar</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_263" n="HIAT:w" s="T75">turallar</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_266" n="HIAT:w" s="T76">Pʼotr</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_269" n="HIAT:w" s="T77">Kuzʼmʼičʼ</ts>
                  <nts id="Seg_270" n="HIAT:ip">,</nts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_273" n="HIAT:w" s="T78">ü͡öreter</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_276" n="HIAT:w" s="T79">dʼon</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_279" n="HIAT:w" s="T80">hübehittere</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_282" n="HIAT:w" s="T81">onno</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_285" n="HIAT:w" s="T82">intʼernaːt</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_288" n="HIAT:w" s="T83">ülehittere</ts>
                  <nts id="Seg_289" n="HIAT:ip">.</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_292" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_294" n="HIAT:w" s="T84">Barɨlara</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_297" n="HIAT:w" s="T85">ogoloru</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_300" n="HIAT:w" s="T86">kaban</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_303" n="HIAT:w" s="T87">ɨlannar</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_306" n="HIAT:w" s="T88">doroːbolohollor</ts>
                  <nts id="Seg_307" n="HIAT:ip">,</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_310" n="HIAT:w" s="T89">tu͡ogu</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_313" n="HIAT:w" s="T90">ere</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_316" n="HIAT:w" s="T91">ginilerge</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_319" n="HIAT:w" s="T92">haŋarallar</ts>
                  <nts id="Seg_320" n="HIAT:ip">.</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_323" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_325" n="HIAT:w" s="T93">Ogolor</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_328" n="HIAT:w" s="T94">nʼuːčča</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_331" n="HIAT:w" s="T95">tɨlɨn</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_334" n="HIAT:w" s="T96">tu͡olkulaːbattar</ts>
                  <nts id="Seg_335" n="HIAT:ip">.</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_338" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_340" n="HIAT:w" s="T97">Ol</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_343" n="HIAT:w" s="T98">ihin</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_346" n="HIAT:w" s="T99">keliː</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_349" n="HIAT:w" s="T100">kɨra</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_352" n="HIAT:w" s="T101">dʼon</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_355" n="HIAT:w" s="T102">kepseppetter</ts>
                  <nts id="Seg_356" n="HIAT:ip">.</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_359" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_361" n="HIAT:w" s="T103">Oduːluːr</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_364" n="HIAT:w" s="T104">agaj</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_367" n="HIAT:w" s="T105">bu͡olbuttar</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_370" n="HIAT:w" s="T106">učʼitʼellerin</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_373" n="HIAT:w" s="T107">hɨrajdarɨn</ts>
                  <nts id="Seg_374" n="HIAT:ip">.</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_377" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_379" n="HIAT:w" s="T108">Giniler</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_382" n="HIAT:w" s="T109">olus</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_385" n="HIAT:w" s="T110">intiri͡ehi</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_388" n="HIAT:w" s="T111">körbütter</ts>
                  <nts id="Seg_389" n="HIAT:ip">.</nts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_392" n="HIAT:u" s="T112">
                  <ts e="T113" id="Seg_394" n="HIAT:w" s="T112">Olus</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_397" n="HIAT:w" s="T113">ulakan</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_400" n="HIAT:w" s="T114">nʼuːčča</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_403" n="HIAT:w" s="T115">dʼi͡eleri</ts>
                  <nts id="Seg_404" n="HIAT:ip">,</nts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_407" n="HIAT:w" s="T116">atɨn</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_410" n="HIAT:w" s="T117">kihileri</ts>
                  <nts id="Seg_411" n="HIAT:ip">,</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_414" n="HIAT:w" s="T118">nʼuːččalarɨ</ts>
                  <nts id="Seg_415" n="HIAT:ip">,</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_418" n="HIAT:w" s="T119">atɨn</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_421" n="HIAT:w" s="T120">tɨllaːktarɨ</ts>
                  <nts id="Seg_422" n="HIAT:ip">,</nts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_425" n="HIAT:w" s="T121">taŋastaːktarɨ</ts>
                  <nts id="Seg_426" n="HIAT:ip">.</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_429" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_431" n="HIAT:w" s="T122">Ataktarɨgar</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_434" n="HIAT:w" s="T123">bu͡ollagɨna</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_437" n="HIAT:w" s="T124">taba</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_440" n="HIAT:w" s="T125">tunʼaga</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_443" n="HIAT:w" s="T126">kördük</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_446" n="HIAT:w" s="T127">atagɨ</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_449" n="HIAT:w" s="T128">iːlimmitter</ts>
                  <nts id="Seg_450" n="HIAT:ip">.</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_453" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_455" n="HIAT:w" s="T129">Er</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_458" n="HIAT:w" s="T130">kihiler</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_461" n="HIAT:w" s="T131">bastarɨgar</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_464" n="HIAT:w" s="T132">maːmalarɨn</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_467" n="HIAT:w" s="T133">komu͡ohun</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_470" n="HIAT:w" s="T134">kördük</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_473" n="HIAT:w" s="T135">bergeheni</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_476" n="HIAT:w" s="T136">kete</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_479" n="HIAT:w" s="T137">hɨldʼallar</ts>
                  <nts id="Seg_480" n="HIAT:ip">.</nts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_483" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_485" n="HIAT:w" s="T138">Dʼaktattara</ts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_488" n="HIAT:w" s="T139">örüːte</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_491" n="HIAT:w" s="T140">hu͡oktar</ts>
                  <nts id="Seg_492" n="HIAT:ip">.</nts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_495" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_497" n="HIAT:w" s="T141">Astara</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_500" n="HIAT:w" s="T142">bɨhɨllɨbɨttar</ts>
                  <nts id="Seg_501" n="HIAT:ip">,</nts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_504" n="HIAT:w" s="T143">er</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_507" n="HIAT:w" s="T144">kihi</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_510" n="HIAT:w" s="T145">kördük</ts>
                  <nts id="Seg_511" n="HIAT:ip">.</nts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_514" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_516" n="HIAT:w" s="T146">ɨrbaːkɨlara</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_519" n="HIAT:w" s="T147">allara</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_522" n="HIAT:w" s="T148">öttüger</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_525" n="HIAT:w" s="T149">olus</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_528" n="HIAT:w" s="T150">čipčigɨr</ts>
                  <nts id="Seg_529" n="HIAT:ip">,</nts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_532" n="HIAT:w" s="T151">ol</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_535" n="HIAT:w" s="T152">ihin</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_538" n="HIAT:w" s="T153">ginner</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_541" n="HIAT:w" s="T154">araččɨ</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_544" n="HIAT:w" s="T155">kaːmallar</ts>
                  <nts id="Seg_545" n="HIAT:ip">.</nts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_548" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_550" n="HIAT:w" s="T156">Tojoː</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_553" n="HIAT:w" s="T157">olus</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_556" n="HIAT:w" s="T158">oduːluː</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_559" n="HIAT:w" s="T159">turbut</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_562" n="HIAT:w" s="T160">dʼaktattarɨ</ts>
                  <nts id="Seg_563" n="HIAT:ip">.</nts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_566" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_568" n="HIAT:w" s="T161">Ihiger</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_571" n="HIAT:w" s="T162">hanɨːr</ts>
                  <nts id="Seg_572" n="HIAT:ip">:</nts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_575" n="HIAT:u" s="T163">
                  <nts id="Seg_576" n="HIAT:ip">"</nts>
                  <ts e="T164" id="Seg_578" n="HIAT:w" s="T163">Badaga</ts>
                  <nts id="Seg_579" n="HIAT:ip">,</nts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_582" n="HIAT:w" s="T164">itinnik</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_585" n="HIAT:w" s="T165">ɨrbaːkɨnnan</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_588" n="HIAT:w" s="T166">hüːre</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_591" n="HIAT:w" s="T167">hataːbattar</ts>
                  <nts id="Seg_592" n="HIAT:ip">,</nts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_595" n="HIAT:w" s="T168">kɨratɨk</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_598" n="HIAT:w" s="T169">ira</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_601" n="HIAT:w" s="T170">kaːmallar</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_604" n="HIAT:w" s="T171">ebit</ts>
                  <nts id="Seg_605" n="HIAT:ip">"</nts>
                  <nts id="Seg_606" n="HIAT:ip">.</nts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_609" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_611" n="HIAT:w" s="T172">Dogottorugar</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_614" n="HIAT:w" s="T173">tugu</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_617" n="HIAT:w" s="T174">ere</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_620" n="HIAT:w" s="T175">iti</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_623" n="HIAT:w" s="T176">tuhunan</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_626" n="HIAT:w" s="T177">kepseːri</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_629" n="HIAT:w" s="T178">gɨmmɨt</ts>
                  <nts id="Seg_630" n="HIAT:ip">.</nts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_633" n="HIAT:u" s="T179">
                  <ts e="T180" id="Seg_635" n="HIAT:w" s="T179">Anʼagɨn</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_638" n="HIAT:w" s="T180">ire</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_641" n="HIAT:w" s="T181">arɨjbɨt</ts>
                  <nts id="Seg_642" n="HIAT:ip">,</nts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_645" n="HIAT:w" s="T182">tu͡ok</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_648" n="HIAT:w" s="T183">ire</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_651" n="HIAT:w" s="T184">aːjdaːna</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_654" n="HIAT:w" s="T185">bu͡olbut</ts>
                  <nts id="Seg_655" n="HIAT:ip">.</nts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_658" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_660" n="HIAT:w" s="T186">Körbüt</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_663" n="HIAT:w" s="T187">ol</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_666" n="HIAT:w" s="T188">di͡ek</ts>
                  <nts id="Seg_667" n="HIAT:ip">,</nts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_670" n="HIAT:w" s="T189">kantan</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_673" n="HIAT:w" s="T190">aːjdaːn</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_676" n="HIAT:w" s="T191">taksɨbɨt</ts>
                  <nts id="Seg_677" n="HIAT:ip">.</nts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_680" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_682" n="HIAT:w" s="T192">Oː</ts>
                  <nts id="Seg_683" n="HIAT:ip">,</nts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_686" n="HIAT:w" s="T193">hoːspodi</ts>
                  <nts id="Seg_687" n="HIAT:ip">!</nts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T201" id="Seg_690" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_692" n="HIAT:w" s="T194">Tu͡ok</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_695" n="HIAT:w" s="T195">ire</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_698" n="HIAT:w" s="T196">kihi</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_701" n="HIAT:w" s="T197">körbötök</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_704" n="HIAT:w" s="T198">hɨrgalaːga</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_706" n="HIAT:ip">(</nts>
                  <ts e="T200" id="Seg_708" n="HIAT:w" s="T199">tallatan</ts>
                  <nts id="Seg_709" n="HIAT:ip">)</nts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_712" n="HIAT:w" s="T200">ispit</ts>
                  <nts id="Seg_713" n="HIAT:ip">.</nts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_716" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_718" n="HIAT:w" s="T201">Hɨrgata</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_721" n="HIAT:w" s="T202">i͡erikej</ts>
                  <nts id="Seg_722" n="HIAT:ip">,</nts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_725" n="HIAT:w" s="T203">tabata</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_728" n="HIAT:w" s="T204">bu͡ollagɨna</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_731" n="HIAT:w" s="T205">ulakana</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_734" n="HIAT:w" s="T206">bert</ts>
                  <nts id="Seg_735" n="HIAT:ip">.</nts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_738" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_740" n="HIAT:w" s="T207">Olus</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_743" n="HIAT:w" s="T208">uhun</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_746" n="HIAT:w" s="T209">döjbüːr</ts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_749" n="HIAT:w" s="T210">kördük</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_752" n="HIAT:w" s="T211">kuturuktaːk</ts>
                  <nts id="Seg_753" n="HIAT:ip">.</nts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_756" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_758" n="HIAT:w" s="T212">Hɨrgalɨːr</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_761" n="HIAT:w" s="T213">kihite</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_764" n="HIAT:w" s="T214">bu͡ollagɨna</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_767" n="HIAT:w" s="T215">hɨraja</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_770" n="HIAT:w" s="T216">tüːte</ts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_773" n="HIAT:w" s="T217">bert</ts>
                  <nts id="Seg_774" n="HIAT:ip">,</nts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_777" n="HIAT:w" s="T218">köp</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_780" n="HIAT:w" s="T219">bɨtɨktaːk</ts>
                  <nts id="Seg_781" n="HIAT:ip">.</nts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T228" id="Seg_784" n="HIAT:u" s="T220">
                  <ts e="T221" id="Seg_786" n="HIAT:w" s="T220">Tabatɨn</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_789" n="HIAT:w" s="T221">kennitten</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_792" n="HIAT:w" s="T222">hatɨː</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_795" n="HIAT:w" s="T223">kaːman</ts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_798" n="HIAT:w" s="T224">ihen</ts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_801" n="HIAT:w" s="T225">tupput</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_804" n="HIAT:w" s="T226">ikki</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_807" n="HIAT:w" s="T227">nʼu͡oŋunu</ts>
                  <nts id="Seg_808" n="HIAT:ip">.</nts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_811" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_813" n="HIAT:w" s="T228">Tabata</ts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_816" n="HIAT:w" s="T229">kajtak</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_819" n="HIAT:w" s="T230">ire</ts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_822" n="HIAT:w" s="T231">kaːmarɨn</ts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_825" n="HIAT:w" s="T232">aːjɨ</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_828" n="HIAT:w" s="T233">bahɨn</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_831" n="HIAT:w" s="T234">möŋtörör</ts>
                  <nts id="Seg_832" n="HIAT:ip">.</nts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_835" n="HIAT:u" s="T235">
                  <ts e="T236" id="Seg_837" n="HIAT:w" s="T235">Ol</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_840" n="HIAT:w" s="T236">aːjɨ</ts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_843" n="HIAT:w" s="T237">hɨrgahɨt</ts>
                  <nts id="Seg_844" n="HIAT:ip">:</nts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_847" n="HIAT:u" s="T238">
                  <nts id="Seg_848" n="HIAT:ip">"</nts>
                  <ts e="T239" id="Seg_850" n="HIAT:w" s="T238">Noː</ts>
                  <nts id="Seg_851" n="HIAT:ip">,</nts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_854" n="HIAT:w" s="T239">noː</ts>
                  <nts id="Seg_855" n="HIAT:ip">"</nts>
                  <nts id="Seg_856" n="HIAT:ip">,</nts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_859" n="HIAT:w" s="T240">diːr</ts>
                  <nts id="Seg_860" n="HIAT:ip">,</nts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_863" n="HIAT:w" s="T241">oloŋkonu</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_866" n="HIAT:w" s="T242">ihilliːr</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_869" n="HIAT:w" s="T243">kördük</ts>
                  <nts id="Seg_870" n="HIAT:ip">.</nts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T252" id="Seg_873" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_875" n="HIAT:w" s="T244">Küːsteːk</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_878" n="HIAT:w" s="T245">bagajɨtɨk</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_881" n="HIAT:w" s="T246">anʼagɨnan</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_884" n="HIAT:w" s="T247">amsajbɨt</ts>
                  <nts id="Seg_885" n="HIAT:ip">,</nts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_888" n="HIAT:w" s="T248">minnʼiges</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_891" n="HIAT:w" s="T249">hɨ͡anɨ</ts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_894" n="HIAT:w" s="T250">hiːr</ts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_897" n="HIAT:w" s="T251">kördük</ts>
                  <nts id="Seg_898" n="HIAT:ip">.</nts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_901" n="HIAT:u" s="T252">
                  <nts id="Seg_902" n="HIAT:ip">"</nts>
                  <ts e="T253" id="Seg_904" n="HIAT:w" s="T252">Kaja</ts>
                  <nts id="Seg_905" n="HIAT:ip">,</nts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_908" n="HIAT:w" s="T253">iti</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_911" n="HIAT:w" s="T254">tu͡ok</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_914" n="HIAT:w" s="T255">tabata</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_917" n="HIAT:w" s="T256">bu͡olar</ts>
                  <nts id="Seg_918" n="HIAT:ip">"</nts>
                  <nts id="Seg_919" n="HIAT:ip">,</nts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_922" n="HIAT:w" s="T257">körö</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_925" n="HIAT:w" s="T258">turan</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_928" n="HIAT:w" s="T259">ü͡örbüčče</ts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_931" n="HIAT:w" s="T260">Tojoː</ts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_934" n="HIAT:w" s="T261">haŋa</ts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_937" n="HIAT:w" s="T262">allɨbɨt</ts>
                  <nts id="Seg_938" n="HIAT:ip">.</nts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T268" id="Seg_941" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_943" n="HIAT:w" s="T263">Gini</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_946" n="HIAT:w" s="T264">agaj</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_949" n="HIAT:w" s="T265">onu</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_952" n="HIAT:w" s="T266">körön</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_955" n="HIAT:w" s="T267">turbataga</ts>
                  <nts id="Seg_956" n="HIAT:ip">.</nts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T272" id="Seg_959" n="HIAT:u" s="T268">
                  <ts e="T269" id="Seg_961" n="HIAT:w" s="T268">Barɨ</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_964" n="HIAT:w" s="T269">ogolor</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_967" n="HIAT:w" s="T270">oduːgu</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_970" n="HIAT:w" s="T271">bu͡olbuttar</ts>
                  <nts id="Seg_971" n="HIAT:ip">.</nts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T281" id="Seg_974" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_976" n="HIAT:w" s="T272">Beje</ts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_979" n="HIAT:w" s="T273">bejelerin</ts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_982" n="HIAT:w" s="T274">gɨtta</ts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_985" n="HIAT:w" s="T275">kepsete</ts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_988" n="HIAT:w" s="T276">ilikterine</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_991" n="HIAT:w" s="T277">emi͡e</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_994" n="HIAT:w" s="T278">körbütter</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_997" n="HIAT:w" s="T279">haŋa</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1000" n="HIAT:w" s="T280">dʼiktini</ts>
                  <nts id="Seg_1001" n="HIAT:ip">.</nts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T290" id="Seg_1004" n="HIAT:u" s="T281">
                  <ts e="T282" id="Seg_1006" n="HIAT:w" s="T281">Kɨrdʼagas</ts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1009" n="HIAT:w" s="T282">nʼuːčča</ts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1012" n="HIAT:w" s="T283">emeːksine</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1015" n="HIAT:w" s="T284">tu͡ok</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1018" n="HIAT:w" s="T285">ire</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1021" n="HIAT:w" s="T286">kɨra</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1024" n="HIAT:w" s="T287">tabakaːttarɨn</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1027" n="HIAT:w" s="T288">üːren</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1030" n="HIAT:w" s="T289">ispit</ts>
                  <nts id="Seg_1031" n="HIAT:ip">.</nts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T298" id="Seg_1034" n="HIAT:u" s="T290">
                  <ts e="T291" id="Seg_1036" n="HIAT:w" s="T290">Hɨgɨnnʼak</ts>
                  <nts id="Seg_1037" n="HIAT:ip">,</nts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1040" n="HIAT:w" s="T291">kultumak</ts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1043" n="HIAT:w" s="T292">tabakaːttar</ts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1046" n="HIAT:w" s="T293">nʼamčɨhak</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1049" n="HIAT:w" s="T294">ataktarɨnan</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1052" n="HIAT:w" s="T295">hüreleːn</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1055" n="HIAT:w" s="T296">ispitter</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1058" n="HIAT:w" s="T297">bardʼɨgɨnɨː-bardʼɨgɨnɨː</ts>
                  <nts id="Seg_1059" n="HIAT:ip">.</nts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T305" id="Seg_1062" n="HIAT:u" s="T298">
                  <ts e="T299" id="Seg_1064" n="HIAT:w" s="T298">Ontulara</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1067" n="HIAT:w" s="T299">kötön</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1070" n="HIAT:w" s="T300">ihenner</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1073" n="HIAT:w" s="T301">ol-bu</ts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1076" n="HIAT:w" s="T302">hir</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1079" n="HIAT:w" s="T303">di͡ek</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1082" n="HIAT:w" s="T304">butarɨhallar</ts>
                  <nts id="Seg_1083" n="HIAT:ip">.</nts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T311" id="Seg_1086" n="HIAT:u" s="T305">
                  <nts id="Seg_1087" n="HIAT:ip">"</nts>
                  <ts e="T306" id="Seg_1089" n="HIAT:w" s="T305">Paː</ts>
                  <nts id="Seg_1090" n="HIAT:ip">"</nts>
                  <nts id="Seg_1091" n="HIAT:ip">,</nts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1094" n="HIAT:w" s="T306">di͡en</ts>
                  <nts id="Seg_1095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1097" n="HIAT:w" s="T307">ü͡ögüleːbit</ts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1100" n="HIAT:w" s="T308">Tojoː</ts>
                  <nts id="Seg_1101" n="HIAT:ip">,</nts>
                  <nts id="Seg_1102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1103" n="HIAT:ip">"</nts>
                  <ts e="T310" id="Seg_1105" n="HIAT:w" s="T309">hɨgɨnnʼak</ts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1108" n="HIAT:w" s="T310">kɨːllar</ts>
                  <nts id="Seg_1109" n="HIAT:ip">!</nts>
                  <nts id="Seg_1110" n="HIAT:ip">"</nts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T317" id="Seg_1113" n="HIAT:u" s="T311">
                  <ts e="T312" id="Seg_1115" n="HIAT:w" s="T311">Ogolor</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1118" n="HIAT:w" s="T312">ele</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1121" n="HIAT:w" s="T313">isterinen</ts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1124" n="HIAT:w" s="T314">külseller</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1127" n="HIAT:w" s="T315">čömüjelerinen</ts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1130" n="HIAT:w" s="T316">köllörö-köllörö</ts>
                  <nts id="Seg_1131" n="HIAT:ip">.</nts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T328" id="Seg_1134" n="HIAT:u" s="T317">
                  <nts id="Seg_1135" n="HIAT:ip">"</nts>
                  <ts e="T318" id="Seg_1137" n="HIAT:w" s="T317">Kör</ts>
                  <nts id="Seg_1138" n="HIAT:ip">,</nts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1141" n="HIAT:w" s="T318">dogoː</ts>
                  <nts id="Seg_1142" n="HIAT:ip">,</nts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1145" n="HIAT:w" s="T319">munnulara</ts>
                  <nts id="Seg_1146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1148" n="HIAT:w" s="T320">kürej</ts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1151" n="HIAT:w" s="T321">lepsetin</ts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1154" n="HIAT:w" s="T322">kördük</ts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1157" n="HIAT:w" s="T323">iti</ts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1160" n="HIAT:w" s="T324">ire</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1163" n="HIAT:w" s="T325">ikki</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1166" n="HIAT:w" s="T326">ire</ts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1169" n="HIAT:w" s="T327">üːtteːkter</ts>
                  <nts id="Seg_1170" n="HIAT:ip">.</nts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T333" id="Seg_1173" n="HIAT:u" s="T328">
                  <ts e="T329" id="Seg_1175" n="HIAT:w" s="T328">Taba</ts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1178" n="HIAT:w" s="T329">itinnik</ts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1181" n="HIAT:w" s="T330">bu͡olbatak</ts>
                  <nts id="Seg_1182" n="HIAT:ip">,</nts>
                  <nts id="Seg_1183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1185" n="HIAT:w" s="T331">bihi͡ettere</ts>
                  <nts id="Seg_1186" n="HIAT:ip">–</nts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1189" n="HIAT:w" s="T332">bosku͡ojdar</ts>
                  <nts id="Seg_1190" n="HIAT:ip">"</nts>
                  <nts id="Seg_1191" n="HIAT:ip">.</nts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T340" id="Seg_1194" n="HIAT:u" s="T333">
                  <ts e="T334" id="Seg_1196" n="HIAT:w" s="T333">Maŋnaj</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1199" n="HIAT:w" s="T334">kuttana</ts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1202" n="HIAT:w" s="T335">hɨspɨttar</ts>
                  <nts id="Seg_1203" n="HIAT:ip">,</nts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1206" n="HIAT:w" s="T336">onton</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1209" n="HIAT:w" s="T337">čugahaːbɨttar</ts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1212" n="HIAT:w" s="T338">ötü͡ötük</ts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1215" n="HIAT:w" s="T339">köröːrü</ts>
                  <nts id="Seg_1216" n="HIAT:ip">.</nts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T344" id="Seg_1219" n="HIAT:u" s="T340">
                  <ts e="T341" id="Seg_1221" n="HIAT:w" s="T340">Tojoː</ts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1224" n="HIAT:w" s="T341">bejetin</ts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1227" n="HIAT:w" s="T342">allaːgɨn</ts>
                  <nts id="Seg_1228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1230" n="HIAT:w" s="T343">kɨ͡ajbatak</ts>
                  <nts id="Seg_1231" n="HIAT:ip">.</nts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T351" id="Seg_1234" n="HIAT:u" s="T344">
                  <ts e="T345" id="Seg_1236" n="HIAT:w" s="T344">Biːrderiger</ts>
                  <nts id="Seg_1237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1239" n="HIAT:w" s="T345">hüːren</ts>
                  <nts id="Seg_1240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1242" n="HIAT:w" s="T346">kelen</ts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1245" n="HIAT:w" s="T347">ürdütüger</ts>
                  <nts id="Seg_1246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1248" n="HIAT:w" s="T348">ugučaktana</ts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1251" n="HIAT:w" s="T349">gɨna</ts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1254" n="HIAT:w" s="T350">ɨstammɨt</ts>
                  <nts id="Seg_1255" n="HIAT:ip">.</nts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T364" id="Seg_1258" n="HIAT:u" s="T351">
                  <ts e="T352" id="Seg_1260" n="HIAT:w" s="T351">Kɨːla</ts>
                  <nts id="Seg_1261" n="HIAT:ip">,</nts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1264" n="HIAT:w" s="T352">hihiger</ts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1267" n="HIAT:w" s="T353">kihini</ts>
                  <nts id="Seg_1268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1270" n="HIAT:w" s="T354">bilen</ts>
                  <nts id="Seg_1271" n="HIAT:ip">,</nts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1274" n="HIAT:w" s="T355">turbut</ts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1277" n="HIAT:w" s="T356">hiritten</ts>
                  <nts id="Seg_1278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1280" n="HIAT:w" s="T357">ɨstammɨt</ts>
                  <nts id="Seg_1281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1283" n="HIAT:w" s="T358">ürbüčče</ts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1286" n="HIAT:w" s="T359">ele</ts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1289" n="HIAT:w" s="T360">küːhünen</ts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1292" n="HIAT:w" s="T361">köppüt</ts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1295" n="HIAT:w" s="T362">bahɨn</ts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1298" n="HIAT:w" s="T363">kotu</ts>
                  <nts id="Seg_1299" n="HIAT:ip">.</nts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T371" id="Seg_1302" n="HIAT:u" s="T364">
                  <ts e="T365" id="Seg_1304" n="HIAT:w" s="T364">Uguːčaktan</ts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1307" n="HIAT:w" s="T365">tüspet</ts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1310" n="HIAT:w" s="T366">ogo</ts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1313" n="HIAT:w" s="T367">tutta</ts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1316" n="HIAT:w" s="T368">hataːbɨt</ts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1319" n="HIAT:w" s="T369">tabatɨn</ts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1322" n="HIAT:w" s="T370">tüːtütten</ts>
                  <nts id="Seg_1323" n="HIAT:ip">.</nts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T377" id="Seg_1326" n="HIAT:u" s="T371">
                  <ts e="T372" id="Seg_1328" n="HIAT:w" s="T371">Kajtak</ts>
                  <nts id="Seg_1329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1331" n="HIAT:w" s="T372">da</ts>
                  <nts id="Seg_1332" n="HIAT:ip">,</nts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1335" n="HIAT:w" s="T373">kanna</ts>
                  <nts id="Seg_1336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1338" n="HIAT:w" s="T374">da</ts>
                  <nts id="Seg_1339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1341" n="HIAT:w" s="T375">iliːlere</ts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1344" n="HIAT:w" s="T376">iŋnibet</ts>
                  <nts id="Seg_1345" n="HIAT:ip">.</nts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_1348" n="HIAT:u" s="T377">
                  <ts e="T378" id="Seg_1350" n="HIAT:w" s="T377">Tabatɨn</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1353" n="HIAT:w" s="T378">tüːlere</ts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1356" n="HIAT:w" s="T379">katɨː</ts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1359" n="HIAT:w" s="T380">kördükter</ts>
                  <nts id="Seg_1360" n="HIAT:ip">.</nts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T389" id="Seg_1363" n="HIAT:u" s="T381">
                  <ts e="T382" id="Seg_1365" n="HIAT:w" s="T381">Uguːčaga</ts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1368" n="HIAT:w" s="T382">ele</ts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1371" n="HIAT:w" s="T383">tɨːnɨnan</ts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1374" n="HIAT:w" s="T384">ü͡ögüleːbit</ts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1377" n="HIAT:w" s="T385">bahɨn</ts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1380" n="HIAT:w" s="T386">kotu</ts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1383" n="HIAT:w" s="T387">hüːren</ts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1386" n="HIAT:w" s="T388">ispit</ts>
                  <nts id="Seg_1387" n="HIAT:ip">.</nts>
                  <nts id="Seg_1388" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T396" id="Seg_1390" n="HIAT:u" s="T389">
                  <ts e="T390" id="Seg_1392" n="HIAT:w" s="T389">Horok</ts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1395" n="HIAT:w" s="T390">hibinnʼeːler</ts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1398" n="HIAT:w" s="T391">onu</ts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1401" n="HIAT:w" s="T392">körönnör</ts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1404" n="HIAT:w" s="T393">biːr</ts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1407" n="HIAT:w" s="T394">duruk</ts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1410" n="HIAT:w" s="T395">köppütter</ts>
                  <nts id="Seg_1411" n="HIAT:ip">.</nts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T401" id="Seg_1414" n="HIAT:u" s="T396">
                  <ts e="T397" id="Seg_1416" n="HIAT:w" s="T396">Kennileritten</ts>
                  <nts id="Seg_1417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1419" n="HIAT:w" s="T397">kɨrdʼagas</ts>
                  <nts id="Seg_1420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1422" n="HIAT:w" s="T398">emeːksin</ts>
                  <nts id="Seg_1423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1425" n="HIAT:w" s="T399">hüːre</ts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1428" n="HIAT:w" s="T400">hataːbɨt</ts>
                  <nts id="Seg_1429" n="HIAT:ip">.</nts>
                  <nts id="Seg_1430" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T405" id="Seg_1432" n="HIAT:u" s="T401">
                  <ts e="T402" id="Seg_1434" n="HIAT:w" s="T401">Tojoː</ts>
                  <nts id="Seg_1435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1437" n="HIAT:w" s="T402">tuːgu</ts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1440" n="HIAT:w" s="T403">ire</ts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1443" n="HIAT:w" s="T404">ü͡ögülüː-ü͡ögülüː</ts>
                  <nts id="Seg_1444" n="HIAT:ip">.</nts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T411" id="Seg_1447" n="HIAT:u" s="T405">
                  <ts e="T406" id="Seg_1449" n="HIAT:w" s="T405">Hüːrerin</ts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1452" n="HIAT:w" s="T406">aːjɨ</ts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1455" n="HIAT:w" s="T407">Tojoː</ts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1458" n="HIAT:w" s="T408">di͡ek</ts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1461" n="HIAT:w" s="T409">tanʼagɨnan</ts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1464" n="HIAT:w" s="T410">dalajar</ts>
                  <nts id="Seg_1465" n="HIAT:ip">.</nts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T420" id="Seg_1468" n="HIAT:u" s="T411">
                  <ts e="T412" id="Seg_1470" n="HIAT:w" s="T411">Hɨgɨnnʼak</ts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1473" n="HIAT:w" s="T412">ugučaga</ts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1476" n="HIAT:w" s="T413">dʼüdeŋi</ts>
                  <nts id="Seg_1477" n="HIAT:ip">,</nts>
                  <nts id="Seg_1478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1480" n="HIAT:w" s="T414">bileːleːk</ts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1483" n="HIAT:w" s="T415">uːlaːk</ts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1486" n="HIAT:w" s="T416">oŋkuːčakka</ts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1488" n="HIAT:ip">"</nts>
                  <ts e="T418" id="Seg_1490" n="HIAT:w" s="T417">laŋ</ts>
                  <nts id="Seg_1491" n="HIAT:ip">"</nts>
                  <nts id="Seg_1492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1494" n="HIAT:w" s="T418">gɨna</ts>
                  <nts id="Seg_1495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1497" n="HIAT:w" s="T419">tüspüt</ts>
                  <nts id="Seg_1498" n="HIAT:ip">.</nts>
                  <nts id="Seg_1499" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T426" id="Seg_1501" n="HIAT:u" s="T420">
                  <ts e="T421" id="Seg_1503" n="HIAT:w" s="T420">Tojoː</ts>
                  <nts id="Seg_1504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1506" n="HIAT:w" s="T421">hibinnʼeːtin</ts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1509" n="HIAT:w" s="T422">gɨtta</ts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1512" n="HIAT:w" s="T423">tura</ts>
                  <nts id="Seg_1513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1515" n="HIAT:w" s="T424">hataːn</ts>
                  <nts id="Seg_1516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1518" n="HIAT:w" s="T425">hɨppɨt</ts>
                  <nts id="Seg_1519" n="HIAT:ip">.</nts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T433" id="Seg_1522" n="HIAT:u" s="T426">
                  <ts e="T427" id="Seg_1524" n="HIAT:w" s="T426">Hɨraja</ts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1527" n="HIAT:w" s="T427">daː</ts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1530" n="HIAT:w" s="T428">tu͡oga</ts>
                  <nts id="Seg_1531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1533" n="HIAT:w" s="T429">da</ts>
                  <nts id="Seg_1534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1536" n="HIAT:w" s="T430">köstübet</ts>
                  <nts id="Seg_1537" n="HIAT:ip">,</nts>
                  <nts id="Seg_1538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1540" n="HIAT:w" s="T431">bu͡or</ts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1543" n="HIAT:w" s="T432">bu͡olbut</ts>
                  <nts id="Seg_1544" n="HIAT:ip">.</nts>
                  <nts id="Seg_1545" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T440" id="Seg_1547" n="HIAT:u" s="T433">
                  <ts e="T434" id="Seg_1549" n="HIAT:w" s="T433">Tura</ts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1552" n="HIAT:w" s="T434">hataːn</ts>
                  <nts id="Seg_1553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1555" n="HIAT:w" s="T435">elete</ts>
                  <nts id="Seg_1556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1558" n="HIAT:w" s="T436">bu͡olan</ts>
                  <nts id="Seg_1559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1561" n="HIAT:w" s="T437">emeːksin</ts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1564" n="HIAT:w" s="T438">hippit</ts>
                  <nts id="Seg_1565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1567" n="HIAT:w" s="T439">ginini</ts>
                  <nts id="Seg_1568" n="HIAT:ip">.</nts>
                  <nts id="Seg_1569" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T447" id="Seg_1571" n="HIAT:u" s="T440">
                  <ts e="T441" id="Seg_1573" n="HIAT:w" s="T440">Tojoː</ts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1576" n="HIAT:w" s="T441">hɨrajɨn</ts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1579" n="HIAT:w" s="T442">körön</ts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1582" n="HIAT:w" s="T443">külümneːbit</ts>
                  <nts id="Seg_1583" n="HIAT:ip">,</nts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1586" n="HIAT:w" s="T444">iliːtinen</ts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1589" n="HIAT:w" s="T445">agaj</ts>
                  <nts id="Seg_1590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1592" n="HIAT:w" s="T446">hapsɨjbɨt</ts>
                  <nts id="Seg_1593" n="HIAT:ip">.</nts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T452" id="Seg_1596" n="HIAT:u" s="T447">
                  <nts id="Seg_1597" n="HIAT:ip">"</nts>
                  <ts e="T448" id="Seg_1599" n="HIAT:w" s="T447">Hupturuj</ts>
                  <nts id="Seg_1600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1602" n="HIAT:w" s="T448">kantan</ts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1605" n="HIAT:w" s="T449">kelbitiŋ</ts>
                  <nts id="Seg_1606" n="HIAT:ip">"</nts>
                  <nts id="Seg_1607" n="HIAT:ip">,</nts>
                  <nts id="Seg_1608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1610" n="HIAT:w" s="T450">di͡ebit</ts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1613" n="HIAT:w" s="T451">kördük</ts>
                  <nts id="Seg_1614" n="HIAT:ip">.</nts>
                  <nts id="Seg_1615" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T462" id="Seg_1617" n="HIAT:u" s="T452">
                  <ts e="T453" id="Seg_1619" n="HIAT:w" s="T452">Tojoː</ts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1622" n="HIAT:w" s="T453">tura</ts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1625" n="HIAT:w" s="T454">ekkireːt</ts>
                  <nts id="Seg_1626" n="HIAT:ip">,</nts>
                  <nts id="Seg_1627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1629" n="HIAT:w" s="T455">ele</ts>
                  <nts id="Seg_1630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1632" n="HIAT:w" s="T456">küːhünen</ts>
                  <nts id="Seg_1633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1635" n="HIAT:w" s="T457">tɨːna</ts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1638" n="HIAT:w" s="T458">hu͡ok</ts>
                  <nts id="Seg_1639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1641" n="HIAT:w" s="T459">dogottorun</ts>
                  <nts id="Seg_1642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1644" n="HIAT:w" s="T460">di͡ek</ts>
                  <nts id="Seg_1645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1647" n="HIAT:w" s="T461">teppit</ts>
                  <nts id="Seg_1648" n="HIAT:ip">.</nts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T469" id="Seg_1651" n="HIAT:u" s="T462">
                  <nts id="Seg_1652" n="HIAT:ip">"</nts>
                  <ts e="T463" id="Seg_1654" n="HIAT:w" s="T462">Araː</ts>
                  <nts id="Seg_1655" n="HIAT:ip">,</nts>
                  <nts id="Seg_1656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1658" n="HIAT:w" s="T463">araččɨ</ts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1661" n="HIAT:w" s="T464">ölö</ts>
                  <nts id="Seg_1662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1664" n="HIAT:w" s="T465">hɨstɨm</ts>
                  <nts id="Seg_1665" n="HIAT:ip">,</nts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1668" n="HIAT:w" s="T466">araččɨ</ts>
                  <nts id="Seg_1669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1671" n="HIAT:w" s="T467">ölö</ts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1674" n="HIAT:w" s="T468">hɨstɨm</ts>
                  <nts id="Seg_1675" n="HIAT:ip">!</nts>
                  <nts id="Seg_1676" n="HIAT:ip">"</nts>
                  <nts id="Seg_1677" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T480" id="Seg_1679" n="HIAT:u" s="T469">
                  <ts e="T470" id="Seg_1681" n="HIAT:w" s="T469">Učʼiːtʼellere</ts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1684" n="HIAT:w" s="T470">dʼirʼektar</ts>
                  <nts id="Seg_1685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1687" n="HIAT:w" s="T471">bu͡olan</ts>
                  <nts id="Seg_1688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1690" n="HIAT:w" s="T472">tu͡ok</ts>
                  <nts id="Seg_1691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1693" n="HIAT:w" s="T473">da</ts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1696" n="HIAT:w" s="T474">di͡ekterin</ts>
                  <nts id="Seg_1697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1699" n="HIAT:w" s="T475">berditten</ts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1702" n="HIAT:w" s="T476">ele</ts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1705" n="HIAT:w" s="T477">isterinen</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1708" n="HIAT:w" s="T478">küle</ts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1711" n="HIAT:w" s="T479">turbuttar</ts>
                  <nts id="Seg_1712" n="HIAT:ip">.</nts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T484" id="Seg_1715" n="HIAT:u" s="T480">
                  <ts e="T723" id="Seg_1717" n="HIAT:w" s="T480">Pavʼel</ts>
                  <nts id="Seg_1718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1720" n="HIAT:w" s="T723">Kuzʼmʼičʼ</ts>
                  <nts id="Seg_1721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1723" n="HIAT:w" s="T481">ele</ts>
                  <nts id="Seg_1724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1726" n="HIAT:w" s="T482">hanaːtɨn</ts>
                  <nts id="Seg_1727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1729" n="HIAT:w" s="T483">haŋarbɨt</ts>
                  <nts id="Seg_1730" n="HIAT:ip">:</nts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T494" id="Seg_1733" n="HIAT:u" s="T484">
                  <nts id="Seg_1734" n="HIAT:ip">"</nts>
                  <ts e="T485" id="Seg_1736" n="HIAT:w" s="T484">Kajtak</ts>
                  <nts id="Seg_1737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1739" n="HIAT:w" s="T485">da</ts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1742" n="HIAT:w" s="T486">di͡ekke</ts>
                  <nts id="Seg_1743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1745" n="HIAT:w" s="T487">bert</ts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1748" n="HIAT:w" s="T488">olus</ts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1751" n="HIAT:w" s="T489">menik</ts>
                  <nts id="Seg_1752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1754" n="HIAT:w" s="T490">dʼon</ts>
                  <nts id="Seg_1755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1757" n="HIAT:w" s="T491">kelbitter</ts>
                  <nts id="Seg_1758" n="HIAT:ip">,</nts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1761" n="HIAT:w" s="T492">bɨhɨlaːk</ts>
                  <nts id="Seg_1762" n="HIAT:ip">"</nts>
                  <nts id="Seg_1763" n="HIAT:ip">.</nts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T496" id="Seg_1766" n="HIAT:u" s="T494">
                  <ts e="T495" id="Seg_1768" n="HIAT:w" s="T494">Dogottorugar</ts>
                  <nts id="Seg_1769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1771" n="HIAT:w" s="T495">di͡ebit</ts>
                  <nts id="Seg_1772" n="HIAT:ip">:</nts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T505" id="Seg_1775" n="HIAT:u" s="T496">
                  <nts id="Seg_1776" n="HIAT:ip">"</nts>
                  <ts e="T497" id="Seg_1778" n="HIAT:w" s="T496">Anɨ</ts>
                  <nts id="Seg_1779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1781" n="HIAT:w" s="T497">ginileri</ts>
                  <nts id="Seg_1782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1784" n="HIAT:w" s="T498">intʼernaːkka</ts>
                  <nts id="Seg_1785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1787" n="HIAT:w" s="T499">illiŋ</ts>
                  <nts id="Seg_1788" n="HIAT:ip">,</nts>
                  <nts id="Seg_1789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1791" n="HIAT:w" s="T500">onton</ts>
                  <nts id="Seg_1792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1794" n="HIAT:w" s="T501">huːnnarɨŋ</ts>
                  <nts id="Seg_1795" n="HIAT:ip">,</nts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1798" n="HIAT:w" s="T502">astarɨn</ts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1801" n="HIAT:w" s="T503">belemni͡ekteriger</ts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1804" n="HIAT:w" s="T504">di͡eri</ts>
                  <nts id="Seg_1805" n="HIAT:ip">.</nts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T510" id="Seg_1808" n="HIAT:u" s="T505">
                  <ts e="T506" id="Seg_1810" n="HIAT:w" s="T505">Barɨtɨn</ts>
                  <nts id="Seg_1811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1813" n="HIAT:w" s="T506">kɨčɨrɨ͡aktɨk</ts>
                  <nts id="Seg_1814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1816" n="HIAT:w" s="T507">oŋoruŋ</ts>
                  <nts id="Seg_1817" n="HIAT:ip">,</nts>
                  <nts id="Seg_1818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1820" n="HIAT:w" s="T508">ogoloru</ts>
                  <nts id="Seg_1821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1823" n="HIAT:w" s="T509">ürpekke</ts>
                  <nts id="Seg_1824" n="HIAT:ip">"</nts>
                  <nts id="Seg_1825" n="HIAT:ip">.</nts>
                  <nts id="Seg_1826" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T521" id="Seg_1828" n="HIAT:u" s="T510">
                  <ts e="T511" id="Seg_1830" n="HIAT:w" s="T510">Ogoloru</ts>
                  <nts id="Seg_1831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1833" n="HIAT:w" s="T511">honno</ts>
                  <nts id="Seg_1834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1836" n="HIAT:w" s="T512">üːren</ts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1839" n="HIAT:w" s="T513">ilpitter</ts>
                  <nts id="Seg_1840" n="HIAT:ip">,</nts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1843" n="HIAT:w" s="T514">köllörbütter</ts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1846" n="HIAT:w" s="T515">barɨlarɨgar</ts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1849" n="HIAT:w" s="T516">oloror</ts>
                  <nts id="Seg_1850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1852" n="HIAT:w" s="T517">dʼi͡elerin</ts>
                  <nts id="Seg_1853" n="HIAT:ip">,</nts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1856" n="HIAT:w" s="T518">nʼuːččalɨː</ts>
                  <nts id="Seg_1857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1858" n="HIAT:ip">"</nts>
                  <ts e="T520" id="Seg_1860" n="HIAT:w" s="T519">koːmnata</ts>
                  <nts id="Seg_1861" n="HIAT:ip">"</nts>
                  <nts id="Seg_1862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1864" n="HIAT:w" s="T520">aːttaːgɨ</ts>
                  <nts id="Seg_1865" n="HIAT:ip">.</nts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T526" id="Seg_1868" n="HIAT:u" s="T521">
                  <ts e="T522" id="Seg_1870" n="HIAT:w" s="T521">Hamaːr</ts>
                  <nts id="Seg_1871" n="HIAT:ip">,</nts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1874" n="HIAT:w" s="T522">haŋa</ts>
                  <nts id="Seg_1875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1877" n="HIAT:w" s="T523">urbaːkɨlarɨ</ts>
                  <nts id="Seg_1878" n="HIAT:ip">,</nts>
                  <nts id="Seg_1879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1881" n="HIAT:w" s="T524">ataktarɨ</ts>
                  <nts id="Seg_1882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1884" n="HIAT:w" s="T525">bi͡eretteːbitter</ts>
                  <nts id="Seg_1885" n="HIAT:ip">.</nts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T539" id="Seg_1888" n="HIAT:u" s="T526">
                  <ts e="T527" id="Seg_1890" n="HIAT:w" s="T526">Barɨ</ts>
                  <nts id="Seg_1891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1893" n="HIAT:w" s="T527">kelbit</ts>
                  <nts id="Seg_1894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1896" n="HIAT:w" s="T528">dʼon</ts>
                  <nts id="Seg_1897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1899" n="HIAT:w" s="T529">astarɨn</ts>
                  <nts id="Seg_1900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1902" n="HIAT:w" s="T530">kɨrpalaːbɨttar</ts>
                  <nts id="Seg_1903" n="HIAT:ip">,</nts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1906" n="HIAT:w" s="T531">tu͡oktan</ts>
                  <nts id="Seg_1907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1909" n="HIAT:w" s="T532">kɨːs</ts>
                  <nts id="Seg_1910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1912" n="HIAT:w" s="T533">ogolor</ts>
                  <nts id="Seg_1913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1915" n="HIAT:w" s="T534">ɨtɨːllara</ts>
                  <nts id="Seg_1916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1918" n="HIAT:w" s="T535">bert</ts>
                  <nts id="Seg_1919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1921" n="HIAT:w" s="T536">ebit</ts>
                  <nts id="Seg_1922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1924" n="HIAT:w" s="T537">astarɨn</ts>
                  <nts id="Seg_1925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1927" n="HIAT:w" s="T538">ahɨnannar</ts>
                  <nts id="Seg_1928" n="HIAT:ip">.</nts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T542" id="Seg_1931" n="HIAT:u" s="T539">
                  <ts e="T540" id="Seg_1933" n="HIAT:w" s="T539">Tojoːnɨ</ts>
                  <nts id="Seg_1934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1936" n="HIAT:w" s="T540">emi͡e</ts>
                  <nts id="Seg_1937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1939" n="HIAT:w" s="T541">kɨrpalaːbɨttar</ts>
                  <nts id="Seg_1940" n="HIAT:ip">.</nts>
                  <nts id="Seg_1941" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T548" id="Seg_1943" n="HIAT:u" s="T542">
                  <ts e="T543" id="Seg_1945" n="HIAT:w" s="T542">Taŋnar</ts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_1948" n="HIAT:w" s="T543">taŋas</ts>
                  <nts id="Seg_1949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1951" n="HIAT:w" s="T544">bi͡erbitter</ts>
                  <nts id="Seg_1952" n="HIAT:ip">,</nts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1955" n="HIAT:w" s="T545">keter</ts>
                  <nts id="Seg_1956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_1958" n="HIAT:w" s="T546">tunʼaktarɨn</ts>
                  <nts id="Seg_1959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1961" n="HIAT:w" s="T547">tuttarbɨttar</ts>
                  <nts id="Seg_1962" n="HIAT:ip">.</nts>
                  <nts id="Seg_1963" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T554" id="Seg_1965" n="HIAT:u" s="T548">
                  <ts e="T549" id="Seg_1967" n="HIAT:w" s="T548">Kɨrabaːtɨgar</ts>
                  <nts id="Seg_1968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_1970" n="HIAT:w" s="T549">keleːt</ts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_1973" n="HIAT:w" s="T550">olorbut</ts>
                  <nts id="Seg_1974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_1976" n="HIAT:w" s="T551">körülüː</ts>
                  <nts id="Seg_1977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_1979" n="HIAT:w" s="T552">tu͡ogu</ts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_1982" n="HIAT:w" s="T553">tuttarbɨttarɨn</ts>
                  <nts id="Seg_1983" n="HIAT:ip">.</nts>
                  <nts id="Seg_1984" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T557" id="Seg_1986" n="HIAT:u" s="T554">
                  <ts e="T555" id="Seg_1988" n="HIAT:w" s="T554">Barɨta</ts>
                  <nts id="Seg_1989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_1991" n="HIAT:w" s="T555">gini͡eke</ts>
                  <nts id="Seg_1992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_1994" n="HIAT:w" s="T556">höp</ts>
                  <nts id="Seg_1995" n="HIAT:ip">.</nts>
                  <nts id="Seg_1996" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T568" id="Seg_1998" n="HIAT:u" s="T557">
                  <ts e="T558" id="Seg_2000" n="HIAT:w" s="T557">Ol</ts>
                  <nts id="Seg_2001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2003" n="HIAT:w" s="T558">daː</ts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2006" n="HIAT:w" s="T559">bu͡ollar</ts>
                  <nts id="Seg_2007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2009" n="HIAT:w" s="T560">togo</ts>
                  <nts id="Seg_2010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2012" n="HIAT:w" s="T561">ginner</ts>
                  <nts id="Seg_2013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2015" n="HIAT:w" s="T562">bi͡erbittere</ts>
                  <nts id="Seg_2016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2018" n="HIAT:w" s="T563">bu͡olu͡oj</ts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2021" n="HIAT:w" s="T564">ikkiliː</ts>
                  <nts id="Seg_2022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2024" n="HIAT:w" s="T565">urbakɨnɨ</ts>
                  <nts id="Seg_2025" n="HIAT:ip">,</nts>
                  <nts id="Seg_2026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2028" n="HIAT:w" s="T566">ikkiliː</ts>
                  <nts id="Seg_2029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2031" n="HIAT:w" s="T567">hɨ͡alɨjanɨ</ts>
                  <nts id="Seg_2032" n="HIAT:ip">?</nts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T574" id="Seg_2035" n="HIAT:u" s="T568">
                  <ts e="T569" id="Seg_2037" n="HIAT:w" s="T568">Haŋa</ts>
                  <nts id="Seg_2038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2040" n="HIAT:w" s="T569">taŋastarɨn</ts>
                  <nts id="Seg_2041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2043" n="HIAT:w" s="T570">keter</ts>
                  <nts id="Seg_2044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2045" n="HIAT:ip">(</nts>
                  <ts e="T573" id="Seg_2047" n="HIAT:w" s="T571">han-</ts>
                  <nts id="Seg_2048" n="HIAT:ip">)</nts>
                  <nts id="Seg_2049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2051" n="HIAT:w" s="T573">hanaːbɨt</ts>
                  <nts id="Seg_2052" n="HIAT:ip">.</nts>
                  <nts id="Seg_2053" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T577" id="Seg_2055" n="HIAT:u" s="T574">
                  <ts e="T575" id="Seg_2057" n="HIAT:w" s="T574">Ogoloru</ts>
                  <nts id="Seg_2058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2060" n="HIAT:w" s="T575">huːnnara</ts>
                  <nts id="Seg_2061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2063" n="HIAT:w" s="T576">ilpitter</ts>
                  <nts id="Seg_2064" n="HIAT:ip">.</nts>
                  <nts id="Seg_2065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T588" id="Seg_2067" n="HIAT:u" s="T577">
                  <ts e="T578" id="Seg_2069" n="HIAT:w" s="T577">Kanna</ts>
                  <nts id="Seg_2070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2072" n="HIAT:w" s="T578">menik</ts>
                  <nts id="Seg_2073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2075" n="HIAT:w" s="T579">dʼon</ts>
                  <nts id="Seg_2076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2078" n="HIAT:w" s="T580">ele</ts>
                  <nts id="Seg_2079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2081" n="HIAT:w" s="T581">hanaːlarɨnan</ts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2084" n="HIAT:w" s="T582">ü͡örüːleːktik</ts>
                  <nts id="Seg_2085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2087" n="HIAT:w" s="T583">külse-halsa</ts>
                  <nts id="Seg_2088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2089" n="HIAT:ip">"</nts>
                  <ts e="T585" id="Seg_2091" n="HIAT:w" s="T584">ataj</ts>
                  <nts id="Seg_2092" n="HIAT:ip">,</nts>
                  <nts id="Seg_2093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2095" n="HIAT:w" s="T585">ataj</ts>
                  <nts id="Seg_2096" n="HIAT:ip">"</nts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2099" n="HIAT:w" s="T586">dehe</ts>
                  <nts id="Seg_2100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2102" n="HIAT:w" s="T587">olorbuttar</ts>
                  <nts id="Seg_2103" n="HIAT:ip">.</nts>
                  <nts id="Seg_2104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T592" id="Seg_2106" n="HIAT:u" s="T588">
                  <ts e="T589" id="Seg_2108" n="HIAT:w" s="T588">Tojoː</ts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2111" n="HIAT:w" s="T589">huːnan</ts>
                  <nts id="Seg_2112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2114" n="HIAT:w" s="T590">büteːt</ts>
                  <nts id="Seg_2115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2117" n="HIAT:w" s="T591">taŋnɨbɨt</ts>
                  <nts id="Seg_2118" n="HIAT:ip">.</nts>
                  <nts id="Seg_2119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T596" id="Seg_2121" n="HIAT:u" s="T592">
                  <ts e="T593" id="Seg_2123" n="HIAT:w" s="T592">Keppit</ts>
                  <nts id="Seg_2124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2126" n="HIAT:w" s="T593">ürüŋ</ts>
                  <nts id="Seg_2127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2129" n="HIAT:w" s="T594">urbaːkɨtɨn</ts>
                  <nts id="Seg_2130" n="HIAT:ip">,</nts>
                  <nts id="Seg_2131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2133" n="HIAT:w" s="T595">hɨ͡alɨjatɨn</ts>
                  <nts id="Seg_2134" n="HIAT:ip">.</nts>
                  <nts id="Seg_2135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T601" id="Seg_2137" n="HIAT:u" s="T596">
                  <ts e="T597" id="Seg_2139" n="HIAT:w" s="T596">Horoktorun</ts>
                  <nts id="Seg_2140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2142" n="HIAT:w" s="T597">hapaːstaːn</ts>
                  <nts id="Seg_2143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2145" n="HIAT:w" s="T598">huːlaːbɨt</ts>
                  <nts id="Seg_2146" n="HIAT:ip">,</nts>
                  <nts id="Seg_2147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2149" n="HIAT:w" s="T599">kojut</ts>
                  <nts id="Seg_2150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2152" n="HIAT:w" s="T600">keteːri</ts>
                  <nts id="Seg_2153" n="HIAT:ip">.</nts>
                  <nts id="Seg_2154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T610" id="Seg_2156" n="HIAT:u" s="T601">
                  <ts e="T602" id="Seg_2158" n="HIAT:w" s="T601">Etiger</ts>
                  <nts id="Seg_2159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2161" n="HIAT:w" s="T602">keten</ts>
                  <nts id="Seg_2162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2164" n="HIAT:w" s="T603">ürüŋ</ts>
                  <nts id="Seg_2165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2167" n="HIAT:w" s="T604">taŋastarɨn</ts>
                  <nts id="Seg_2168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2170" n="HIAT:w" s="T605">tu͡ok</ts>
                  <nts id="Seg_2171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2173" n="HIAT:w" s="T606">da</ts>
                  <nts id="Seg_2174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2176" n="HIAT:w" s="T607">bu͡olbatak</ts>
                  <nts id="Seg_2177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2179" n="HIAT:w" s="T608">kördük</ts>
                  <nts id="Seg_2180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2182" n="HIAT:w" s="T609">hɨldʼɨbɨt</ts>
                  <nts id="Seg_2183" n="HIAT:ip">.</nts>
                  <nts id="Seg_2184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T617" id="Seg_2186" n="HIAT:u" s="T610">
                  <ts e="T611" id="Seg_2188" n="HIAT:w" s="T610">Dogottoro</ts>
                  <nts id="Seg_2189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2191" n="HIAT:w" s="T611">da</ts>
                  <nts id="Seg_2192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2194" n="HIAT:w" s="T612">tu͡ogu</ts>
                  <nts id="Seg_2195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2197" n="HIAT:w" s="T613">da</ts>
                  <nts id="Seg_2198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2200" n="HIAT:w" s="T614">di͡ebetter</ts>
                  <nts id="Seg_2201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2203" n="HIAT:w" s="T615">ebit</ts>
                  <nts id="Seg_2204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2206" n="HIAT:w" s="T616">gini͡eke</ts>
                  <nts id="Seg_2207" n="HIAT:ip">.</nts>
                  <nts id="Seg_2208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T626" id="Seg_2210" n="HIAT:u" s="T617">
                  <ts e="T618" id="Seg_2212" n="HIAT:w" s="T617">Intʼernaːtka</ts>
                  <nts id="Seg_2213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2215" n="HIAT:w" s="T618">üleliːr</ts>
                  <nts id="Seg_2216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2218" n="HIAT:w" s="T619">biːr</ts>
                  <nts id="Seg_2219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2221" n="HIAT:w" s="T620">dʼaktar</ts>
                  <nts id="Seg_2222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2224" n="HIAT:w" s="T621">tu͡olkulappɨt</ts>
                  <nts id="Seg_2225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2227" n="HIAT:w" s="T622">kajtak</ts>
                  <nts id="Seg_2228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2230" n="HIAT:w" s="T623">gini</ts>
                  <nts id="Seg_2231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2233" n="HIAT:w" s="T624">taŋnar</ts>
                  <nts id="Seg_2234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2236" n="HIAT:w" s="T625">bu͡olu͡ogun</ts>
                  <nts id="Seg_2237" n="HIAT:ip">.</nts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T629" id="Seg_2240" n="HIAT:u" s="T626">
                  <ts e="T627" id="Seg_2242" n="HIAT:w" s="T626">Ahɨːr</ts>
                  <nts id="Seg_2243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2245" n="HIAT:w" s="T627">kemnere</ts>
                  <nts id="Seg_2246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2248" n="HIAT:w" s="T628">kelbit</ts>
                  <nts id="Seg_2249" n="HIAT:ip">.</nts>
                  <nts id="Seg_2250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T639" id="Seg_2252" n="HIAT:u" s="T629">
                  <ts e="T630" id="Seg_2254" n="HIAT:w" s="T629">Ogoloru</ts>
                  <nts id="Seg_2255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2257" n="HIAT:w" s="T630">olortoːbuttar</ts>
                  <nts id="Seg_2258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2259" n="HIAT:ip">(</nts>
                  <ts e="T633" id="Seg_2261" n="HIAT:w" s="T631">uhu-</ts>
                  <nts id="Seg_2262" n="HIAT:ip">)</nts>
                  <nts id="Seg_2263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2265" n="HIAT:w" s="T633">uhun</ts>
                  <nts id="Seg_2266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2268" n="HIAT:w" s="T634">bagajɨ</ts>
                  <nts id="Seg_2269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2271" n="HIAT:w" s="T635">ostollor</ts>
                  <nts id="Seg_2272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2274" n="HIAT:w" s="T636">ikki</ts>
                  <nts id="Seg_2275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2277" n="HIAT:w" s="T637">di͡eki</ts>
                  <nts id="Seg_2278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2280" n="HIAT:w" s="T638">öttüleriger</ts>
                  <nts id="Seg_2281" n="HIAT:ip">.</nts>
                  <nts id="Seg_2282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T645" id="Seg_2284" n="HIAT:u" s="T639">
                  <ts e="T640" id="Seg_2286" n="HIAT:w" s="T639">Ulakan</ts>
                  <nts id="Seg_2287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2289" n="HIAT:w" s="T640">ihitterge</ts>
                  <nts id="Seg_2290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2292" n="HIAT:w" s="T641">uːrullubuttar</ts>
                  <nts id="Seg_2293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2295" n="HIAT:w" s="T642">minnʼiges</ts>
                  <nts id="Seg_2296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2298" n="HIAT:w" s="T643">hɨttaːk</ts>
                  <nts id="Seg_2299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2301" n="HIAT:w" s="T644">kili͡epter</ts>
                  <nts id="Seg_2302" n="HIAT:ip">.</nts>
                  <nts id="Seg_2303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T650" id="Seg_2305" n="HIAT:u" s="T645">
                  <ts e="T646" id="Seg_2307" n="HIAT:w" s="T645">Ogo</ts>
                  <nts id="Seg_2308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2310" n="HIAT:w" s="T646">aːjɨ</ts>
                  <nts id="Seg_2311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2313" n="HIAT:w" s="T647">kutattaːbɨttar</ts>
                  <nts id="Seg_2314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2316" n="HIAT:w" s="T648">balɨk</ts>
                  <nts id="Seg_2317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2319" n="HIAT:w" s="T649">minin</ts>
                  <nts id="Seg_2320" n="HIAT:ip">.</nts>
                  <nts id="Seg_2321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T652" id="Seg_2323" n="HIAT:u" s="T650">
                  <ts e="T651" id="Seg_2325" n="HIAT:w" s="T650">Lu͡oskalarɨ</ts>
                  <nts id="Seg_2326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2328" n="HIAT:w" s="T651">bi͡eretteːbitter</ts>
                  <nts id="Seg_2329" n="HIAT:ip">.</nts>
                  <nts id="Seg_2330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T659" id="Seg_2332" n="HIAT:u" s="T652">
                  <ts e="T653" id="Seg_2334" n="HIAT:w" s="T652">Körbüt</ts>
                  <nts id="Seg_2335" n="HIAT:ip">,</nts>
                  <nts id="Seg_2336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2338" n="HIAT:w" s="T653">ogolor</ts>
                  <nts id="Seg_2339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2341" n="HIAT:w" s="T654">ahaːnnar</ts>
                  <nts id="Seg_2342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2344" n="HIAT:w" s="T655">ketektere</ts>
                  <nts id="Seg_2345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2347" n="HIAT:w" s="T656">agaj</ts>
                  <nts id="Seg_2348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2350" n="HIAT:w" s="T657">hüte</ts>
                  <nts id="Seg_2351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2353" n="HIAT:w" s="T658">hɨtar</ts>
                  <nts id="Seg_2354" n="HIAT:ip">.</nts>
                  <nts id="Seg_2355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T670" id="Seg_2357" n="HIAT:u" s="T659">
                  <ts e="T660" id="Seg_2359" n="HIAT:w" s="T659">Tojoː</ts>
                  <nts id="Seg_2360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2362" n="HIAT:w" s="T660">ahɨː</ts>
                  <nts id="Seg_2363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2365" n="HIAT:w" s="T661">oloron</ts>
                  <nts id="Seg_2366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2368" n="HIAT:w" s="T662">tugu</ts>
                  <nts id="Seg_2369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2371" n="HIAT:w" s="T663">ire</ts>
                  <nts id="Seg_2372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2374" n="HIAT:w" s="T664">hanaːta</ts>
                  <nts id="Seg_2375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2377" n="HIAT:w" s="T665">kelbit</ts>
                  <nts id="Seg_2378" n="HIAT:ip">,</nts>
                  <nts id="Seg_2379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2381" n="HIAT:w" s="T666">maːmatɨn</ts>
                  <nts id="Seg_2382" n="HIAT:ip">,</nts>
                  <nts id="Seg_2383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2385" n="HIAT:w" s="T667">agatɨn</ts>
                  <nts id="Seg_2386" n="HIAT:ip">,</nts>
                  <nts id="Seg_2387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2389" n="HIAT:w" s="T668">Kɨčatɨn</ts>
                  <nts id="Seg_2390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2392" n="HIAT:w" s="T669">öjdöːn</ts>
                  <nts id="Seg_2393" n="HIAT:ip">.</nts>
                  <nts id="Seg_2394" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T682" id="Seg_2396" n="HIAT:u" s="T670">
                  <ts e="T671" id="Seg_2398" n="HIAT:w" s="T670">Ahaːn</ts>
                  <nts id="Seg_2399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2401" n="HIAT:w" s="T671">büteːt</ts>
                  <nts id="Seg_2402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2404" n="HIAT:w" s="T672">ogolor</ts>
                  <nts id="Seg_2405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2407" n="HIAT:w" s="T673">Valačaːnka</ts>
                  <nts id="Seg_2408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2410" n="HIAT:w" s="T674">üstün</ts>
                  <nts id="Seg_2411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2413" n="HIAT:w" s="T675">oːnnʼuː</ts>
                  <nts id="Seg_2414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2416" n="HIAT:w" s="T676">hɨldʼɨbɨttar</ts>
                  <nts id="Seg_2417" n="HIAT:ip">,</nts>
                  <nts id="Seg_2418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2420" n="HIAT:w" s="T677">körülüː</ts>
                  <nts id="Seg_2421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2423" n="HIAT:w" s="T678">nʼuːčča</ts>
                  <nts id="Seg_2424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2426" n="HIAT:w" s="T679">dʼi͡eleri</ts>
                  <nts id="Seg_2427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2429" n="HIAT:w" s="T680">barɨ</ts>
                  <nts id="Seg_2430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2432" n="HIAT:w" s="T681">tergi͡eni</ts>
                  <nts id="Seg_2433" n="HIAT:ip">.</nts>
                  <nts id="Seg_2434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T688" id="Seg_2436" n="HIAT:u" s="T682">
                  <ts e="T683" id="Seg_2438" n="HIAT:w" s="T682">Ginnerge</ts>
                  <nts id="Seg_2439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2441" n="HIAT:w" s="T683">barɨta</ts>
                  <nts id="Seg_2442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2444" n="HIAT:w" s="T684">dʼikti</ts>
                  <nts id="Seg_2445" n="HIAT:ip">,</nts>
                  <nts id="Seg_2446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2448" n="HIAT:w" s="T685">haŋardɨː</ts>
                  <nts id="Seg_2449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2451" n="HIAT:w" s="T686">kelbit</ts>
                  <nts id="Seg_2452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2454" n="HIAT:w" s="T687">kihilerge</ts>
                  <nts id="Seg_2455" n="HIAT:ip">.</nts>
                  <nts id="Seg_2456" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T697" id="Seg_2458" n="HIAT:u" s="T688">
                  <ts e="T689" id="Seg_2460" n="HIAT:w" s="T688">Nöŋü͡ö</ts>
                  <nts id="Seg_2461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2463" n="HIAT:w" s="T689">kün</ts>
                  <nts id="Seg_2464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2466" n="HIAT:w" s="T690">emi͡e</ts>
                  <nts id="Seg_2467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2469" n="HIAT:w" s="T691">ulakan</ts>
                  <nts id="Seg_2470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2472" n="HIAT:w" s="T692">ogoloːk</ts>
                  <nts id="Seg_2473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2475" n="HIAT:w" s="T693">kös</ts>
                  <nts id="Seg_2476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2478" n="HIAT:w" s="T694">kelbit</ts>
                  <nts id="Seg_2479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2481" n="HIAT:w" s="T695">atɨn</ts>
                  <nts id="Seg_2482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2484" n="HIAT:w" s="T696">kalxoːztan</ts>
                  <nts id="Seg_2485" n="HIAT:ip">.</nts>
                  <nts id="Seg_2486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T706" id="Seg_2488" n="HIAT:u" s="T697">
                  <ts e="T698" id="Seg_2490" n="HIAT:w" s="T697">Haŋardɨː</ts>
                  <nts id="Seg_2491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2493" n="HIAT:w" s="T698">keliː</ts>
                  <nts id="Seg_2494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2496" n="HIAT:w" s="T699">ogolor</ts>
                  <nts id="Seg_2497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2499" n="HIAT:w" s="T700">kiːrbikteːge</ts>
                  <nts id="Seg_2500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2502" n="HIAT:w" s="T701">bert</ts>
                  <nts id="Seg_2503" n="HIAT:ip">,</nts>
                  <nts id="Seg_2504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2506" n="HIAT:w" s="T702">barɨttan</ts>
                  <nts id="Seg_2507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2508" n="HIAT:ip">(</nts>
                  <ts e="T704" id="Seg_2510" n="HIAT:w" s="T703">kutana</ts>
                  <nts id="Seg_2511" n="HIAT:ip">)</nts>
                  <nts id="Seg_2512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2514" n="HIAT:w" s="T704">kuttanar</ts>
                  <nts id="Seg_2515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2517" n="HIAT:w" s="T705">kördükter</ts>
                  <nts id="Seg_2518" n="HIAT:ip">.</nts>
                  <nts id="Seg_2519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T711" id="Seg_2521" n="HIAT:u" s="T706">
                  <ts e="T707" id="Seg_2523" n="HIAT:w" s="T706">Ginner</ts>
                  <nts id="Seg_2524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2526" n="HIAT:w" s="T707">olus</ts>
                  <nts id="Seg_2527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2529" n="HIAT:w" s="T708">ör</ts>
                  <nts id="Seg_2530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2532" n="HIAT:w" s="T709">itigirdik</ts>
                  <nts id="Seg_2533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2535" n="HIAT:w" s="T710">bu͡olbataktar</ts>
                  <nts id="Seg_2536" n="HIAT:ip">.</nts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T722" id="Seg_2539" n="HIAT:u" s="T711">
                  <ts e="T712" id="Seg_2541" n="HIAT:w" s="T711">Emi͡e</ts>
                  <nts id="Seg_2542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2544" n="HIAT:w" s="T712">Tojoː</ts>
                  <nts id="Seg_2545" n="HIAT:ip">,</nts>
                  <nts id="Seg_2546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2548" n="HIAT:w" s="T713">Miteː</ts>
                  <nts id="Seg_2549" n="HIAT:ip">,</nts>
                  <nts id="Seg_2550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2552" n="HIAT:w" s="T714">Ujbaːn</ts>
                  <nts id="Seg_2553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2555" n="HIAT:w" s="T715">kördük</ts>
                  <nts id="Seg_2556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2558" n="HIAT:w" s="T716">tollubat</ts>
                  <nts id="Seg_2559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2561" n="HIAT:w" s="T717">mi͡ereni</ts>
                  <nts id="Seg_2562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2564" n="HIAT:w" s="T718">ɨlbɨttar</ts>
                  <nts id="Seg_2565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2567" n="HIAT:w" s="T719">haŋa</ts>
                  <nts id="Seg_2568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2570" n="HIAT:w" s="T720">dogottorun</ts>
                  <nts id="Seg_2571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2573" n="HIAT:w" s="T721">körönnör</ts>
                  <nts id="Seg_2574" n="HIAT:ip">.</nts>
                  <nts id="Seg_2575" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T722" id="Seg_2576" n="sc" s="T0">
               <ts e="T1" id="Seg_2578" n="e" s="T0">Ihilleːŋ </ts>
               <ts e="T2" id="Seg_2580" n="e" s="T1">öhü </ts>
               <ts e="T3" id="Seg_2582" n="e" s="T2">"Dʼikti </ts>
               <ts e="T4" id="Seg_2584" n="e" s="T3">tabalar". </ts>
               <ts e="T5" id="Seg_2586" n="e" s="T4">Ühüs </ts>
               <ts e="T6" id="Seg_2588" n="e" s="T5">künüger </ts>
               <ts e="T7" id="Seg_2590" n="e" s="T6">ogoloːk </ts>
               <ts e="T8" id="Seg_2592" n="e" s="T7">kös </ts>
               <ts e="T9" id="Seg_2594" n="e" s="T8">tiːjbit </ts>
               <ts e="T10" id="Seg_2596" n="e" s="T9">Valačaŋkaga. </ts>
               <ts e="T11" id="Seg_2598" n="e" s="T10">Avam </ts>
               <ts e="T12" id="Seg_2600" n="e" s="T11">rajonun </ts>
               <ts e="T13" id="Seg_2602" n="e" s="T12">kiːn </ts>
               <ts e="T14" id="Seg_2604" n="e" s="T13">ologun </ts>
               <ts e="T15" id="Seg_2606" n="e" s="T14">nʼuːččalara, </ts>
               <ts e="T16" id="Seg_2608" n="e" s="T15">hakalara, </ts>
               <ts e="T17" id="Seg_2610" n="e" s="T16">barɨlara </ts>
               <ts e="T18" id="Seg_2612" n="e" s="T17">komullubuttara </ts>
               <ts e="T19" id="Seg_2614" n="e" s="T18">keliː </ts>
               <ts e="T20" id="Seg_2616" n="e" s="T19">ogoloru </ts>
               <ts e="T21" id="Seg_2618" n="e" s="T20">köröːrü, </ts>
               <ts e="T22" id="Seg_2620" n="e" s="T21">kukla </ts>
               <ts e="T23" id="Seg_2622" n="e" s="T22">kördük </ts>
               <ts e="T24" id="Seg_2624" n="e" s="T23">taŋastaːktarɨ, </ts>
               <ts e="T25" id="Seg_2626" n="e" s="T24">kihi </ts>
               <ts e="T26" id="Seg_2628" n="e" s="T25">taptɨ͡aktarɨ. </ts>
               <ts e="T27" id="Seg_2630" n="e" s="T26">Olus </ts>
               <ts e="T28" id="Seg_2632" n="e" s="T27">aːjdaːna </ts>
               <ts e="T29" id="Seg_2634" n="e" s="T28">hu͡ok, </ts>
               <ts e="T30" id="Seg_2636" n="e" s="T29">dʼɨlɨgɨr </ts>
               <ts e="T31" id="Seg_2638" n="e" s="T30">Valačaːnka </ts>
               <ts e="T32" id="Seg_2640" n="e" s="T31">olok </ts>
               <ts e="T33" id="Seg_2642" n="e" s="T32">biːr </ts>
               <ts e="T34" id="Seg_2644" n="e" s="T33">duruk </ts>
               <ts e="T35" id="Seg_2646" n="e" s="T34">tillibit </ts>
               <ts e="T36" id="Seg_2648" n="e" s="T35">kördük, </ts>
               <ts e="T37" id="Seg_2650" n="e" s="T36">taŋara </ts>
               <ts e="T38" id="Seg_2652" n="e" s="T37">künün </ts>
               <ts e="T39" id="Seg_2654" n="e" s="T38">beli͡etiːr </ts>
               <ts e="T40" id="Seg_2656" n="e" s="T39">kördük </ts>
               <ts e="T41" id="Seg_2658" n="e" s="T40">bu͡olan </ts>
               <ts e="T42" id="Seg_2660" n="e" s="T41">kaːlbɨt. </ts>
               <ts e="T43" id="Seg_2662" n="e" s="T42">Ogolor </ts>
               <ts e="T44" id="Seg_2664" n="e" s="T43">bu͡ollaktarɨna </ts>
               <ts e="T45" id="Seg_2666" n="e" s="T44">kihileri </ts>
               <ts e="T46" id="Seg_2668" n="e" s="T45">körböttör. </ts>
               <ts e="T47" id="Seg_2670" n="e" s="T46">Nʼuːčča </ts>
               <ts e="T48" id="Seg_2672" n="e" s="T47">dʼi͡eler </ts>
               <ts e="T49" id="Seg_2674" n="e" s="T48">di͡ek </ts>
               <ts e="T50" id="Seg_2676" n="e" s="T49">agaj </ts>
               <ts e="T52" id="Seg_2678" n="e" s="T50">(kar-) </ts>
               <ts e="T53" id="Seg_2680" n="e" s="T52">karaktara. </ts>
               <ts e="T54" id="Seg_2682" n="e" s="T53">Ulʼica </ts>
               <ts e="T55" id="Seg_2684" n="e" s="T54">üstün </ts>
               <ts e="T56" id="Seg_2686" n="e" s="T55">ataralatan </ts>
               <ts e="T57" id="Seg_2688" n="e" s="T56">ihenner </ts>
               <ts e="T58" id="Seg_2690" n="e" s="T57">uːčagɨnan. </ts>
               <ts e="T59" id="Seg_2692" n="e" s="T58">Ehelere, </ts>
               <ts e="T60" id="Seg_2694" n="e" s="T59">Anʼiːsʼim </ts>
               <ts e="T61" id="Seg_2696" n="e" s="T60">ogonnʼor </ts>
               <ts e="T62" id="Seg_2698" n="e" s="T61">turbut </ts>
               <ts e="T63" id="Seg_2700" n="e" s="T62">asku͡ola </ts>
               <ts e="T64" id="Seg_2702" n="e" s="T63">attɨgar. </ts>
               <ts e="T65" id="Seg_2704" n="e" s="T64">Ginini </ts>
               <ts e="T66" id="Seg_2706" n="e" s="T65">tögürüččü </ts>
               <ts e="T67" id="Seg_2708" n="e" s="T66">aːjdannaːk </ts>
               <ts e="T68" id="Seg_2710" n="e" s="T67">armʼija </ts>
               <ts e="T69" id="Seg_2712" n="e" s="T68">ugučaktarɨnan </ts>
               <ts e="T71" id="Seg_2714" n="e" s="T69">(ürd-) </ts>
               <ts e="T72" id="Seg_2716" n="e" s="T71">ürdüleritten </ts>
               <ts e="T73" id="Seg_2718" n="e" s="T72">ɨstaŋalaːbɨttar. </ts>
               <ts e="T74" id="Seg_2720" n="e" s="T73">Asku͡ola </ts>
               <ts e="T75" id="Seg_2722" n="e" s="T74">attɨgar </ts>
               <ts e="T76" id="Seg_2724" n="e" s="T75">turallar </ts>
               <ts e="T77" id="Seg_2726" n="e" s="T76">Pʼotr </ts>
               <ts e="T78" id="Seg_2728" n="e" s="T77">Kuzʼmʼičʼ, </ts>
               <ts e="T79" id="Seg_2730" n="e" s="T78">ü͡öreter </ts>
               <ts e="T80" id="Seg_2732" n="e" s="T79">dʼon </ts>
               <ts e="T81" id="Seg_2734" n="e" s="T80">hübehittere </ts>
               <ts e="T82" id="Seg_2736" n="e" s="T81">onno </ts>
               <ts e="T83" id="Seg_2738" n="e" s="T82">intʼernaːt </ts>
               <ts e="T84" id="Seg_2740" n="e" s="T83">ülehittere. </ts>
               <ts e="T85" id="Seg_2742" n="e" s="T84">Barɨlara </ts>
               <ts e="T86" id="Seg_2744" n="e" s="T85">ogoloru </ts>
               <ts e="T87" id="Seg_2746" n="e" s="T86">kaban </ts>
               <ts e="T88" id="Seg_2748" n="e" s="T87">ɨlannar </ts>
               <ts e="T89" id="Seg_2750" n="e" s="T88">doroːbolohollor, </ts>
               <ts e="T90" id="Seg_2752" n="e" s="T89">tu͡ogu </ts>
               <ts e="T91" id="Seg_2754" n="e" s="T90">ere </ts>
               <ts e="T92" id="Seg_2756" n="e" s="T91">ginilerge </ts>
               <ts e="T93" id="Seg_2758" n="e" s="T92">haŋarallar. </ts>
               <ts e="T94" id="Seg_2760" n="e" s="T93">Ogolor </ts>
               <ts e="T95" id="Seg_2762" n="e" s="T94">nʼuːčča </ts>
               <ts e="T96" id="Seg_2764" n="e" s="T95">tɨlɨn </ts>
               <ts e="T97" id="Seg_2766" n="e" s="T96">tu͡olkulaːbattar. </ts>
               <ts e="T98" id="Seg_2768" n="e" s="T97">Ol </ts>
               <ts e="T99" id="Seg_2770" n="e" s="T98">ihin </ts>
               <ts e="T100" id="Seg_2772" n="e" s="T99">keliː </ts>
               <ts e="T101" id="Seg_2774" n="e" s="T100">kɨra </ts>
               <ts e="T102" id="Seg_2776" n="e" s="T101">dʼon </ts>
               <ts e="T103" id="Seg_2778" n="e" s="T102">kepseppetter. </ts>
               <ts e="T104" id="Seg_2780" n="e" s="T103">Oduːluːr </ts>
               <ts e="T105" id="Seg_2782" n="e" s="T104">agaj </ts>
               <ts e="T106" id="Seg_2784" n="e" s="T105">bu͡olbuttar </ts>
               <ts e="T107" id="Seg_2786" n="e" s="T106">učʼitʼellerin </ts>
               <ts e="T108" id="Seg_2788" n="e" s="T107">hɨrajdarɨn. </ts>
               <ts e="T109" id="Seg_2790" n="e" s="T108">Giniler </ts>
               <ts e="T110" id="Seg_2792" n="e" s="T109">olus </ts>
               <ts e="T111" id="Seg_2794" n="e" s="T110">intiri͡ehi </ts>
               <ts e="T112" id="Seg_2796" n="e" s="T111">körbütter. </ts>
               <ts e="T113" id="Seg_2798" n="e" s="T112">Olus </ts>
               <ts e="T114" id="Seg_2800" n="e" s="T113">ulakan </ts>
               <ts e="T115" id="Seg_2802" n="e" s="T114">nʼuːčča </ts>
               <ts e="T116" id="Seg_2804" n="e" s="T115">dʼi͡eleri, </ts>
               <ts e="T117" id="Seg_2806" n="e" s="T116">atɨn </ts>
               <ts e="T118" id="Seg_2808" n="e" s="T117">kihileri, </ts>
               <ts e="T119" id="Seg_2810" n="e" s="T118">nʼuːččalarɨ, </ts>
               <ts e="T120" id="Seg_2812" n="e" s="T119">atɨn </ts>
               <ts e="T121" id="Seg_2814" n="e" s="T120">tɨllaːktarɨ, </ts>
               <ts e="T122" id="Seg_2816" n="e" s="T121">taŋastaːktarɨ. </ts>
               <ts e="T123" id="Seg_2818" n="e" s="T122">Ataktarɨgar </ts>
               <ts e="T124" id="Seg_2820" n="e" s="T123">bu͡ollagɨna </ts>
               <ts e="T125" id="Seg_2822" n="e" s="T124">taba </ts>
               <ts e="T126" id="Seg_2824" n="e" s="T125">tunʼaga </ts>
               <ts e="T127" id="Seg_2826" n="e" s="T126">kördük </ts>
               <ts e="T128" id="Seg_2828" n="e" s="T127">atagɨ </ts>
               <ts e="T129" id="Seg_2830" n="e" s="T128">iːlimmitter. </ts>
               <ts e="T130" id="Seg_2832" n="e" s="T129">Er </ts>
               <ts e="T131" id="Seg_2834" n="e" s="T130">kihiler </ts>
               <ts e="T132" id="Seg_2836" n="e" s="T131">bastarɨgar </ts>
               <ts e="T133" id="Seg_2838" n="e" s="T132">maːmalarɨn </ts>
               <ts e="T134" id="Seg_2840" n="e" s="T133">komu͡ohun </ts>
               <ts e="T135" id="Seg_2842" n="e" s="T134">kördük </ts>
               <ts e="T136" id="Seg_2844" n="e" s="T135">bergeheni </ts>
               <ts e="T137" id="Seg_2846" n="e" s="T136">kete </ts>
               <ts e="T138" id="Seg_2848" n="e" s="T137">hɨldʼallar. </ts>
               <ts e="T139" id="Seg_2850" n="e" s="T138">Dʼaktattara </ts>
               <ts e="T140" id="Seg_2852" n="e" s="T139">örüːte </ts>
               <ts e="T141" id="Seg_2854" n="e" s="T140">hu͡oktar. </ts>
               <ts e="T142" id="Seg_2856" n="e" s="T141">Astara </ts>
               <ts e="T143" id="Seg_2858" n="e" s="T142">bɨhɨllɨbɨttar, </ts>
               <ts e="T144" id="Seg_2860" n="e" s="T143">er </ts>
               <ts e="T145" id="Seg_2862" n="e" s="T144">kihi </ts>
               <ts e="T146" id="Seg_2864" n="e" s="T145">kördük. </ts>
               <ts e="T147" id="Seg_2866" n="e" s="T146">ɨrbaːkɨlara </ts>
               <ts e="T148" id="Seg_2868" n="e" s="T147">allara </ts>
               <ts e="T149" id="Seg_2870" n="e" s="T148">öttüger </ts>
               <ts e="T150" id="Seg_2872" n="e" s="T149">olus </ts>
               <ts e="T151" id="Seg_2874" n="e" s="T150">čipčigɨr, </ts>
               <ts e="T152" id="Seg_2876" n="e" s="T151">ol </ts>
               <ts e="T153" id="Seg_2878" n="e" s="T152">ihin </ts>
               <ts e="T154" id="Seg_2880" n="e" s="T153">ginner </ts>
               <ts e="T155" id="Seg_2882" n="e" s="T154">araččɨ </ts>
               <ts e="T156" id="Seg_2884" n="e" s="T155">kaːmallar. </ts>
               <ts e="T157" id="Seg_2886" n="e" s="T156">Tojoː </ts>
               <ts e="T158" id="Seg_2888" n="e" s="T157">olus </ts>
               <ts e="T159" id="Seg_2890" n="e" s="T158">oduːluː </ts>
               <ts e="T160" id="Seg_2892" n="e" s="T159">turbut </ts>
               <ts e="T161" id="Seg_2894" n="e" s="T160">dʼaktattarɨ. </ts>
               <ts e="T162" id="Seg_2896" n="e" s="T161">Ihiger </ts>
               <ts e="T163" id="Seg_2898" n="e" s="T162">hanɨːr: </ts>
               <ts e="T164" id="Seg_2900" n="e" s="T163">"Badaga, </ts>
               <ts e="T165" id="Seg_2902" n="e" s="T164">itinnik </ts>
               <ts e="T166" id="Seg_2904" n="e" s="T165">ɨrbaːkɨnnan </ts>
               <ts e="T167" id="Seg_2906" n="e" s="T166">hüːre </ts>
               <ts e="T168" id="Seg_2908" n="e" s="T167">hataːbattar, </ts>
               <ts e="T169" id="Seg_2910" n="e" s="T168">kɨratɨk </ts>
               <ts e="T170" id="Seg_2912" n="e" s="T169">ira </ts>
               <ts e="T171" id="Seg_2914" n="e" s="T170">kaːmallar </ts>
               <ts e="T172" id="Seg_2916" n="e" s="T171">ebit". </ts>
               <ts e="T173" id="Seg_2918" n="e" s="T172">Dogottorugar </ts>
               <ts e="T174" id="Seg_2920" n="e" s="T173">tugu </ts>
               <ts e="T175" id="Seg_2922" n="e" s="T174">ere </ts>
               <ts e="T176" id="Seg_2924" n="e" s="T175">iti </ts>
               <ts e="T177" id="Seg_2926" n="e" s="T176">tuhunan </ts>
               <ts e="T178" id="Seg_2928" n="e" s="T177">kepseːri </ts>
               <ts e="T179" id="Seg_2930" n="e" s="T178">gɨmmɨt. </ts>
               <ts e="T180" id="Seg_2932" n="e" s="T179">Anʼagɨn </ts>
               <ts e="T181" id="Seg_2934" n="e" s="T180">ire </ts>
               <ts e="T182" id="Seg_2936" n="e" s="T181">arɨjbɨt, </ts>
               <ts e="T183" id="Seg_2938" n="e" s="T182">tu͡ok </ts>
               <ts e="T184" id="Seg_2940" n="e" s="T183">ire </ts>
               <ts e="T185" id="Seg_2942" n="e" s="T184">aːjdaːna </ts>
               <ts e="T186" id="Seg_2944" n="e" s="T185">bu͡olbut. </ts>
               <ts e="T187" id="Seg_2946" n="e" s="T186">Körbüt </ts>
               <ts e="T188" id="Seg_2948" n="e" s="T187">ol </ts>
               <ts e="T189" id="Seg_2950" n="e" s="T188">di͡ek, </ts>
               <ts e="T190" id="Seg_2952" n="e" s="T189">kantan </ts>
               <ts e="T191" id="Seg_2954" n="e" s="T190">aːjdaːn </ts>
               <ts e="T192" id="Seg_2956" n="e" s="T191">taksɨbɨt. </ts>
               <ts e="T193" id="Seg_2958" n="e" s="T192">Oː, </ts>
               <ts e="T194" id="Seg_2960" n="e" s="T193">hoːspodi! </ts>
               <ts e="T195" id="Seg_2962" n="e" s="T194">Tu͡ok </ts>
               <ts e="T196" id="Seg_2964" n="e" s="T195">ire </ts>
               <ts e="T197" id="Seg_2966" n="e" s="T196">kihi </ts>
               <ts e="T198" id="Seg_2968" n="e" s="T197">körbötök </ts>
               <ts e="T199" id="Seg_2970" n="e" s="T198">hɨrgalaːga </ts>
               <ts e="T200" id="Seg_2972" n="e" s="T199">(tallatan) </ts>
               <ts e="T201" id="Seg_2974" n="e" s="T200">ispit. </ts>
               <ts e="T202" id="Seg_2976" n="e" s="T201">Hɨrgata </ts>
               <ts e="T203" id="Seg_2978" n="e" s="T202">i͡erikej, </ts>
               <ts e="T204" id="Seg_2980" n="e" s="T203">tabata </ts>
               <ts e="T205" id="Seg_2982" n="e" s="T204">bu͡ollagɨna </ts>
               <ts e="T206" id="Seg_2984" n="e" s="T205">ulakana </ts>
               <ts e="T207" id="Seg_2986" n="e" s="T206">bert. </ts>
               <ts e="T208" id="Seg_2988" n="e" s="T207">Olus </ts>
               <ts e="T209" id="Seg_2990" n="e" s="T208">uhun </ts>
               <ts e="T210" id="Seg_2992" n="e" s="T209">döjbüːr </ts>
               <ts e="T211" id="Seg_2994" n="e" s="T210">kördük </ts>
               <ts e="T212" id="Seg_2996" n="e" s="T211">kuturuktaːk. </ts>
               <ts e="T213" id="Seg_2998" n="e" s="T212">Hɨrgalɨːr </ts>
               <ts e="T214" id="Seg_3000" n="e" s="T213">kihite </ts>
               <ts e="T215" id="Seg_3002" n="e" s="T214">bu͡ollagɨna </ts>
               <ts e="T216" id="Seg_3004" n="e" s="T215">hɨraja </ts>
               <ts e="T217" id="Seg_3006" n="e" s="T216">tüːte </ts>
               <ts e="T218" id="Seg_3008" n="e" s="T217">bert, </ts>
               <ts e="T219" id="Seg_3010" n="e" s="T218">köp </ts>
               <ts e="T220" id="Seg_3012" n="e" s="T219">bɨtɨktaːk. </ts>
               <ts e="T221" id="Seg_3014" n="e" s="T220">Tabatɨn </ts>
               <ts e="T222" id="Seg_3016" n="e" s="T221">kennitten </ts>
               <ts e="T223" id="Seg_3018" n="e" s="T222">hatɨː </ts>
               <ts e="T224" id="Seg_3020" n="e" s="T223">kaːman </ts>
               <ts e="T225" id="Seg_3022" n="e" s="T224">ihen </ts>
               <ts e="T226" id="Seg_3024" n="e" s="T225">tupput </ts>
               <ts e="T227" id="Seg_3026" n="e" s="T226">ikki </ts>
               <ts e="T228" id="Seg_3028" n="e" s="T227">nʼu͡oŋunu. </ts>
               <ts e="T229" id="Seg_3030" n="e" s="T228">Tabata </ts>
               <ts e="T230" id="Seg_3032" n="e" s="T229">kajtak </ts>
               <ts e="T231" id="Seg_3034" n="e" s="T230">ire </ts>
               <ts e="T232" id="Seg_3036" n="e" s="T231">kaːmarɨn </ts>
               <ts e="T233" id="Seg_3038" n="e" s="T232">aːjɨ </ts>
               <ts e="T234" id="Seg_3040" n="e" s="T233">bahɨn </ts>
               <ts e="T235" id="Seg_3042" n="e" s="T234">möŋtörör. </ts>
               <ts e="T236" id="Seg_3044" n="e" s="T235">Ol </ts>
               <ts e="T237" id="Seg_3046" n="e" s="T236">aːjɨ </ts>
               <ts e="T238" id="Seg_3048" n="e" s="T237">hɨrgahɨt: </ts>
               <ts e="T239" id="Seg_3050" n="e" s="T238">"Noː, </ts>
               <ts e="T240" id="Seg_3052" n="e" s="T239">noː", </ts>
               <ts e="T241" id="Seg_3054" n="e" s="T240">diːr, </ts>
               <ts e="T242" id="Seg_3056" n="e" s="T241">oloŋkonu </ts>
               <ts e="T243" id="Seg_3058" n="e" s="T242">ihilliːr </ts>
               <ts e="T244" id="Seg_3060" n="e" s="T243">kördük. </ts>
               <ts e="T245" id="Seg_3062" n="e" s="T244">Küːsteːk </ts>
               <ts e="T246" id="Seg_3064" n="e" s="T245">bagajɨtɨk </ts>
               <ts e="T247" id="Seg_3066" n="e" s="T246">anʼagɨnan </ts>
               <ts e="T248" id="Seg_3068" n="e" s="T247">amsajbɨt, </ts>
               <ts e="T249" id="Seg_3070" n="e" s="T248">minnʼiges </ts>
               <ts e="T250" id="Seg_3072" n="e" s="T249">hɨ͡anɨ </ts>
               <ts e="T251" id="Seg_3074" n="e" s="T250">hiːr </ts>
               <ts e="T252" id="Seg_3076" n="e" s="T251">kördük. </ts>
               <ts e="T253" id="Seg_3078" n="e" s="T252">"Kaja, </ts>
               <ts e="T254" id="Seg_3080" n="e" s="T253">iti </ts>
               <ts e="T255" id="Seg_3082" n="e" s="T254">tu͡ok </ts>
               <ts e="T256" id="Seg_3084" n="e" s="T255">tabata </ts>
               <ts e="T257" id="Seg_3086" n="e" s="T256">bu͡olar", </ts>
               <ts e="T258" id="Seg_3088" n="e" s="T257">körö </ts>
               <ts e="T259" id="Seg_3090" n="e" s="T258">turan </ts>
               <ts e="T260" id="Seg_3092" n="e" s="T259">ü͡örbüčče </ts>
               <ts e="T261" id="Seg_3094" n="e" s="T260">Tojoː </ts>
               <ts e="T262" id="Seg_3096" n="e" s="T261">haŋa </ts>
               <ts e="T263" id="Seg_3098" n="e" s="T262">allɨbɨt. </ts>
               <ts e="T264" id="Seg_3100" n="e" s="T263">Gini </ts>
               <ts e="T265" id="Seg_3102" n="e" s="T264">agaj </ts>
               <ts e="T266" id="Seg_3104" n="e" s="T265">onu </ts>
               <ts e="T267" id="Seg_3106" n="e" s="T266">körön </ts>
               <ts e="T268" id="Seg_3108" n="e" s="T267">turbataga. </ts>
               <ts e="T269" id="Seg_3110" n="e" s="T268">Barɨ </ts>
               <ts e="T270" id="Seg_3112" n="e" s="T269">ogolor </ts>
               <ts e="T271" id="Seg_3114" n="e" s="T270">oduːgu </ts>
               <ts e="T272" id="Seg_3116" n="e" s="T271">bu͡olbuttar. </ts>
               <ts e="T273" id="Seg_3118" n="e" s="T272">Beje </ts>
               <ts e="T274" id="Seg_3120" n="e" s="T273">bejelerin </ts>
               <ts e="T275" id="Seg_3122" n="e" s="T274">gɨtta </ts>
               <ts e="T276" id="Seg_3124" n="e" s="T275">kepsete </ts>
               <ts e="T277" id="Seg_3126" n="e" s="T276">ilikterine </ts>
               <ts e="T278" id="Seg_3128" n="e" s="T277">emi͡e </ts>
               <ts e="T279" id="Seg_3130" n="e" s="T278">körbütter </ts>
               <ts e="T280" id="Seg_3132" n="e" s="T279">haŋa </ts>
               <ts e="T281" id="Seg_3134" n="e" s="T280">dʼiktini. </ts>
               <ts e="T282" id="Seg_3136" n="e" s="T281">Kɨrdʼagas </ts>
               <ts e="T283" id="Seg_3138" n="e" s="T282">nʼuːčča </ts>
               <ts e="T284" id="Seg_3140" n="e" s="T283">emeːksine </ts>
               <ts e="T285" id="Seg_3142" n="e" s="T284">tu͡ok </ts>
               <ts e="T286" id="Seg_3144" n="e" s="T285">ire </ts>
               <ts e="T287" id="Seg_3146" n="e" s="T286">kɨra </ts>
               <ts e="T288" id="Seg_3148" n="e" s="T287">tabakaːttarɨn </ts>
               <ts e="T289" id="Seg_3150" n="e" s="T288">üːren </ts>
               <ts e="T290" id="Seg_3152" n="e" s="T289">ispit. </ts>
               <ts e="T291" id="Seg_3154" n="e" s="T290">Hɨgɨnnʼak, </ts>
               <ts e="T292" id="Seg_3156" n="e" s="T291">kultumak </ts>
               <ts e="T293" id="Seg_3158" n="e" s="T292">tabakaːttar </ts>
               <ts e="T294" id="Seg_3160" n="e" s="T293">nʼamčɨhak </ts>
               <ts e="T295" id="Seg_3162" n="e" s="T294">ataktarɨnan </ts>
               <ts e="T296" id="Seg_3164" n="e" s="T295">hüreleːn </ts>
               <ts e="T297" id="Seg_3166" n="e" s="T296">ispitter </ts>
               <ts e="T298" id="Seg_3168" n="e" s="T297">bardʼɨgɨnɨː-bardʼɨgɨnɨː. </ts>
               <ts e="T299" id="Seg_3170" n="e" s="T298">Ontulara </ts>
               <ts e="T300" id="Seg_3172" n="e" s="T299">kötön </ts>
               <ts e="T301" id="Seg_3174" n="e" s="T300">ihenner </ts>
               <ts e="T302" id="Seg_3176" n="e" s="T301">ol-bu </ts>
               <ts e="T303" id="Seg_3178" n="e" s="T302">hir </ts>
               <ts e="T304" id="Seg_3180" n="e" s="T303">di͡ek </ts>
               <ts e="T305" id="Seg_3182" n="e" s="T304">butarɨhallar. </ts>
               <ts e="T306" id="Seg_3184" n="e" s="T305">"Paː", </ts>
               <ts e="T307" id="Seg_3186" n="e" s="T306">di͡en </ts>
               <ts e="T308" id="Seg_3188" n="e" s="T307">ü͡ögüleːbit </ts>
               <ts e="T309" id="Seg_3190" n="e" s="T308">Tojoː, </ts>
               <ts e="T310" id="Seg_3192" n="e" s="T309">"hɨgɨnnʼak </ts>
               <ts e="T311" id="Seg_3194" n="e" s="T310">kɨːllar!" </ts>
               <ts e="T312" id="Seg_3196" n="e" s="T311">Ogolor </ts>
               <ts e="T313" id="Seg_3198" n="e" s="T312">ele </ts>
               <ts e="T314" id="Seg_3200" n="e" s="T313">isterinen </ts>
               <ts e="T315" id="Seg_3202" n="e" s="T314">külseller </ts>
               <ts e="T316" id="Seg_3204" n="e" s="T315">čömüjelerinen </ts>
               <ts e="T317" id="Seg_3206" n="e" s="T316">köllörö-köllörö. </ts>
               <ts e="T318" id="Seg_3208" n="e" s="T317">"Kör, </ts>
               <ts e="T319" id="Seg_3210" n="e" s="T318">dogoː, </ts>
               <ts e="T320" id="Seg_3212" n="e" s="T319">munnulara </ts>
               <ts e="T321" id="Seg_3214" n="e" s="T320">kürej </ts>
               <ts e="T322" id="Seg_3216" n="e" s="T321">lepsetin </ts>
               <ts e="T323" id="Seg_3218" n="e" s="T322">kördük </ts>
               <ts e="T324" id="Seg_3220" n="e" s="T323">iti </ts>
               <ts e="T325" id="Seg_3222" n="e" s="T324">ire </ts>
               <ts e="T326" id="Seg_3224" n="e" s="T325">ikki </ts>
               <ts e="T327" id="Seg_3226" n="e" s="T326">ire </ts>
               <ts e="T328" id="Seg_3228" n="e" s="T327">üːtteːkter. </ts>
               <ts e="T329" id="Seg_3230" n="e" s="T328">Taba </ts>
               <ts e="T330" id="Seg_3232" n="e" s="T329">itinnik </ts>
               <ts e="T331" id="Seg_3234" n="e" s="T330">bu͡olbatak, </ts>
               <ts e="T332" id="Seg_3236" n="e" s="T331">bihi͡ettere– </ts>
               <ts e="T333" id="Seg_3238" n="e" s="T332">bosku͡ojdar". </ts>
               <ts e="T334" id="Seg_3240" n="e" s="T333">Maŋnaj </ts>
               <ts e="T335" id="Seg_3242" n="e" s="T334">kuttana </ts>
               <ts e="T336" id="Seg_3244" n="e" s="T335">hɨspɨttar, </ts>
               <ts e="T337" id="Seg_3246" n="e" s="T336">onton </ts>
               <ts e="T338" id="Seg_3248" n="e" s="T337">čugahaːbɨttar </ts>
               <ts e="T339" id="Seg_3250" n="e" s="T338">ötü͡ötük </ts>
               <ts e="T340" id="Seg_3252" n="e" s="T339">köröːrü. </ts>
               <ts e="T341" id="Seg_3254" n="e" s="T340">Tojoː </ts>
               <ts e="T342" id="Seg_3256" n="e" s="T341">bejetin </ts>
               <ts e="T343" id="Seg_3258" n="e" s="T342">allaːgɨn </ts>
               <ts e="T344" id="Seg_3260" n="e" s="T343">kɨ͡ajbatak. </ts>
               <ts e="T345" id="Seg_3262" n="e" s="T344">Biːrderiger </ts>
               <ts e="T346" id="Seg_3264" n="e" s="T345">hüːren </ts>
               <ts e="T347" id="Seg_3266" n="e" s="T346">kelen </ts>
               <ts e="T348" id="Seg_3268" n="e" s="T347">ürdütüger </ts>
               <ts e="T349" id="Seg_3270" n="e" s="T348">ugučaktana </ts>
               <ts e="T350" id="Seg_3272" n="e" s="T349">gɨna </ts>
               <ts e="T351" id="Seg_3274" n="e" s="T350">ɨstammɨt. </ts>
               <ts e="T352" id="Seg_3276" n="e" s="T351">Kɨːla, </ts>
               <ts e="T353" id="Seg_3278" n="e" s="T352">hihiger </ts>
               <ts e="T354" id="Seg_3280" n="e" s="T353">kihini </ts>
               <ts e="T355" id="Seg_3282" n="e" s="T354">bilen, </ts>
               <ts e="T356" id="Seg_3284" n="e" s="T355">turbut </ts>
               <ts e="T357" id="Seg_3286" n="e" s="T356">hiritten </ts>
               <ts e="T358" id="Seg_3288" n="e" s="T357">ɨstammɨt </ts>
               <ts e="T359" id="Seg_3290" n="e" s="T358">ürbüčče </ts>
               <ts e="T360" id="Seg_3292" n="e" s="T359">ele </ts>
               <ts e="T361" id="Seg_3294" n="e" s="T360">küːhünen </ts>
               <ts e="T362" id="Seg_3296" n="e" s="T361">köppüt </ts>
               <ts e="T363" id="Seg_3298" n="e" s="T362">bahɨn </ts>
               <ts e="T364" id="Seg_3300" n="e" s="T363">kotu. </ts>
               <ts e="T365" id="Seg_3302" n="e" s="T364">Uguːčaktan </ts>
               <ts e="T366" id="Seg_3304" n="e" s="T365">tüspet </ts>
               <ts e="T367" id="Seg_3306" n="e" s="T366">ogo </ts>
               <ts e="T368" id="Seg_3308" n="e" s="T367">tutta </ts>
               <ts e="T369" id="Seg_3310" n="e" s="T368">hataːbɨt </ts>
               <ts e="T370" id="Seg_3312" n="e" s="T369">tabatɨn </ts>
               <ts e="T371" id="Seg_3314" n="e" s="T370">tüːtütten. </ts>
               <ts e="T372" id="Seg_3316" n="e" s="T371">Kajtak </ts>
               <ts e="T373" id="Seg_3318" n="e" s="T372">da, </ts>
               <ts e="T374" id="Seg_3320" n="e" s="T373">kanna </ts>
               <ts e="T375" id="Seg_3322" n="e" s="T374">da </ts>
               <ts e="T376" id="Seg_3324" n="e" s="T375">iliːlere </ts>
               <ts e="T377" id="Seg_3326" n="e" s="T376">iŋnibet. </ts>
               <ts e="T378" id="Seg_3328" n="e" s="T377">Tabatɨn </ts>
               <ts e="T379" id="Seg_3330" n="e" s="T378">tüːlere </ts>
               <ts e="T380" id="Seg_3332" n="e" s="T379">katɨː </ts>
               <ts e="T381" id="Seg_3334" n="e" s="T380">kördükter. </ts>
               <ts e="T382" id="Seg_3336" n="e" s="T381">Uguːčaga </ts>
               <ts e="T383" id="Seg_3338" n="e" s="T382">ele </ts>
               <ts e="T384" id="Seg_3340" n="e" s="T383">tɨːnɨnan </ts>
               <ts e="T385" id="Seg_3342" n="e" s="T384">ü͡ögüleːbit </ts>
               <ts e="T386" id="Seg_3344" n="e" s="T385">bahɨn </ts>
               <ts e="T387" id="Seg_3346" n="e" s="T386">kotu </ts>
               <ts e="T388" id="Seg_3348" n="e" s="T387">hüːren </ts>
               <ts e="T389" id="Seg_3350" n="e" s="T388">ispit. </ts>
               <ts e="T390" id="Seg_3352" n="e" s="T389">Horok </ts>
               <ts e="T391" id="Seg_3354" n="e" s="T390">hibinnʼeːler </ts>
               <ts e="T392" id="Seg_3356" n="e" s="T391">onu </ts>
               <ts e="T393" id="Seg_3358" n="e" s="T392">körönnör </ts>
               <ts e="T394" id="Seg_3360" n="e" s="T393">biːr </ts>
               <ts e="T395" id="Seg_3362" n="e" s="T394">duruk </ts>
               <ts e="T396" id="Seg_3364" n="e" s="T395">köppütter. </ts>
               <ts e="T397" id="Seg_3366" n="e" s="T396">Kennileritten </ts>
               <ts e="T398" id="Seg_3368" n="e" s="T397">kɨrdʼagas </ts>
               <ts e="T399" id="Seg_3370" n="e" s="T398">emeːksin </ts>
               <ts e="T400" id="Seg_3372" n="e" s="T399">hüːre </ts>
               <ts e="T401" id="Seg_3374" n="e" s="T400">hataːbɨt. </ts>
               <ts e="T402" id="Seg_3376" n="e" s="T401">Tojoː </ts>
               <ts e="T403" id="Seg_3378" n="e" s="T402">tuːgu </ts>
               <ts e="T404" id="Seg_3380" n="e" s="T403">ire </ts>
               <ts e="T405" id="Seg_3382" n="e" s="T404">ü͡ögülüː-ü͡ögülüː. </ts>
               <ts e="T406" id="Seg_3384" n="e" s="T405">Hüːrerin </ts>
               <ts e="T407" id="Seg_3386" n="e" s="T406">aːjɨ </ts>
               <ts e="T408" id="Seg_3388" n="e" s="T407">Tojoː </ts>
               <ts e="T409" id="Seg_3390" n="e" s="T408">di͡ek </ts>
               <ts e="T410" id="Seg_3392" n="e" s="T409">tanʼagɨnan </ts>
               <ts e="T411" id="Seg_3394" n="e" s="T410">dalajar. </ts>
               <ts e="T412" id="Seg_3396" n="e" s="T411">Hɨgɨnnʼak </ts>
               <ts e="T413" id="Seg_3398" n="e" s="T412">ugučaga </ts>
               <ts e="T414" id="Seg_3400" n="e" s="T413">dʼüdeŋi, </ts>
               <ts e="T415" id="Seg_3402" n="e" s="T414">bileːleːk </ts>
               <ts e="T416" id="Seg_3404" n="e" s="T415">uːlaːk </ts>
               <ts e="T417" id="Seg_3406" n="e" s="T416">oŋkuːčakka </ts>
               <ts e="T418" id="Seg_3408" n="e" s="T417">"laŋ" </ts>
               <ts e="T419" id="Seg_3410" n="e" s="T418">gɨna </ts>
               <ts e="T420" id="Seg_3412" n="e" s="T419">tüspüt. </ts>
               <ts e="T421" id="Seg_3414" n="e" s="T420">Tojoː </ts>
               <ts e="T422" id="Seg_3416" n="e" s="T421">hibinnʼeːtin </ts>
               <ts e="T423" id="Seg_3418" n="e" s="T422">gɨtta </ts>
               <ts e="T424" id="Seg_3420" n="e" s="T423">tura </ts>
               <ts e="T425" id="Seg_3422" n="e" s="T424">hataːn </ts>
               <ts e="T426" id="Seg_3424" n="e" s="T425">hɨppɨt. </ts>
               <ts e="T427" id="Seg_3426" n="e" s="T426">Hɨraja </ts>
               <ts e="T428" id="Seg_3428" n="e" s="T427">daː </ts>
               <ts e="T429" id="Seg_3430" n="e" s="T428">tu͡oga </ts>
               <ts e="T430" id="Seg_3432" n="e" s="T429">da </ts>
               <ts e="T431" id="Seg_3434" n="e" s="T430">köstübet, </ts>
               <ts e="T432" id="Seg_3436" n="e" s="T431">bu͡or </ts>
               <ts e="T433" id="Seg_3438" n="e" s="T432">bu͡olbut. </ts>
               <ts e="T434" id="Seg_3440" n="e" s="T433">Tura </ts>
               <ts e="T435" id="Seg_3442" n="e" s="T434">hataːn </ts>
               <ts e="T436" id="Seg_3444" n="e" s="T435">elete </ts>
               <ts e="T437" id="Seg_3446" n="e" s="T436">bu͡olan </ts>
               <ts e="T438" id="Seg_3448" n="e" s="T437">emeːksin </ts>
               <ts e="T439" id="Seg_3450" n="e" s="T438">hippit </ts>
               <ts e="T440" id="Seg_3452" n="e" s="T439">ginini. </ts>
               <ts e="T441" id="Seg_3454" n="e" s="T440">Tojoː </ts>
               <ts e="T442" id="Seg_3456" n="e" s="T441">hɨrajɨn </ts>
               <ts e="T443" id="Seg_3458" n="e" s="T442">körön </ts>
               <ts e="T444" id="Seg_3460" n="e" s="T443">külümneːbit, </ts>
               <ts e="T445" id="Seg_3462" n="e" s="T444">iliːtinen </ts>
               <ts e="T446" id="Seg_3464" n="e" s="T445">agaj </ts>
               <ts e="T447" id="Seg_3466" n="e" s="T446">hapsɨjbɨt. </ts>
               <ts e="T448" id="Seg_3468" n="e" s="T447">"Hupturuj </ts>
               <ts e="T449" id="Seg_3470" n="e" s="T448">kantan </ts>
               <ts e="T450" id="Seg_3472" n="e" s="T449">kelbitiŋ", </ts>
               <ts e="T451" id="Seg_3474" n="e" s="T450">di͡ebit </ts>
               <ts e="T452" id="Seg_3476" n="e" s="T451">kördük. </ts>
               <ts e="T453" id="Seg_3478" n="e" s="T452">Tojoː </ts>
               <ts e="T454" id="Seg_3480" n="e" s="T453">tura </ts>
               <ts e="T455" id="Seg_3482" n="e" s="T454">ekkireːt, </ts>
               <ts e="T456" id="Seg_3484" n="e" s="T455">ele </ts>
               <ts e="T457" id="Seg_3486" n="e" s="T456">küːhünen </ts>
               <ts e="T458" id="Seg_3488" n="e" s="T457">tɨːna </ts>
               <ts e="T459" id="Seg_3490" n="e" s="T458">hu͡ok </ts>
               <ts e="T460" id="Seg_3492" n="e" s="T459">dogottorun </ts>
               <ts e="T461" id="Seg_3494" n="e" s="T460">di͡ek </ts>
               <ts e="T462" id="Seg_3496" n="e" s="T461">teppit. </ts>
               <ts e="T463" id="Seg_3498" n="e" s="T462">"Araː, </ts>
               <ts e="T464" id="Seg_3500" n="e" s="T463">araččɨ </ts>
               <ts e="T465" id="Seg_3502" n="e" s="T464">ölö </ts>
               <ts e="T466" id="Seg_3504" n="e" s="T465">hɨstɨm, </ts>
               <ts e="T467" id="Seg_3506" n="e" s="T466">araččɨ </ts>
               <ts e="T468" id="Seg_3508" n="e" s="T467">ölö </ts>
               <ts e="T469" id="Seg_3510" n="e" s="T468">hɨstɨm!" </ts>
               <ts e="T470" id="Seg_3512" n="e" s="T469">Učʼiːtʼellere </ts>
               <ts e="T471" id="Seg_3514" n="e" s="T470">dʼirʼektar </ts>
               <ts e="T472" id="Seg_3516" n="e" s="T471">bu͡olan </ts>
               <ts e="T473" id="Seg_3518" n="e" s="T472">tu͡ok </ts>
               <ts e="T474" id="Seg_3520" n="e" s="T473">da </ts>
               <ts e="T475" id="Seg_3522" n="e" s="T474">di͡ekterin </ts>
               <ts e="T476" id="Seg_3524" n="e" s="T475">berditten </ts>
               <ts e="T477" id="Seg_3526" n="e" s="T476">ele </ts>
               <ts e="T478" id="Seg_3528" n="e" s="T477">isterinen </ts>
               <ts e="T479" id="Seg_3530" n="e" s="T478">küle </ts>
               <ts e="T480" id="Seg_3532" n="e" s="T479">turbuttar. </ts>
               <ts e="T723" id="Seg_3534" n="e" s="T480">Pavʼel </ts>
               <ts e="T481" id="Seg_3536" n="e" s="T723">Kuzʼmʼičʼ </ts>
               <ts e="T482" id="Seg_3538" n="e" s="T481">ele </ts>
               <ts e="T483" id="Seg_3540" n="e" s="T482">hanaːtɨn </ts>
               <ts e="T484" id="Seg_3542" n="e" s="T483">haŋarbɨt: </ts>
               <ts e="T485" id="Seg_3544" n="e" s="T484">"Kajtak </ts>
               <ts e="T486" id="Seg_3546" n="e" s="T485">da </ts>
               <ts e="T487" id="Seg_3548" n="e" s="T486">di͡ekke </ts>
               <ts e="T488" id="Seg_3550" n="e" s="T487">bert </ts>
               <ts e="T489" id="Seg_3552" n="e" s="T488">olus </ts>
               <ts e="T490" id="Seg_3554" n="e" s="T489">menik </ts>
               <ts e="T491" id="Seg_3556" n="e" s="T490">dʼon </ts>
               <ts e="T492" id="Seg_3558" n="e" s="T491">kelbitter, </ts>
               <ts e="T494" id="Seg_3560" n="e" s="T492">bɨhɨlaːk". </ts>
               <ts e="T495" id="Seg_3562" n="e" s="T494">Dogottorugar </ts>
               <ts e="T496" id="Seg_3564" n="e" s="T495">di͡ebit: </ts>
               <ts e="T497" id="Seg_3566" n="e" s="T496">"Anɨ </ts>
               <ts e="T498" id="Seg_3568" n="e" s="T497">ginileri </ts>
               <ts e="T499" id="Seg_3570" n="e" s="T498">intʼernaːkka </ts>
               <ts e="T500" id="Seg_3572" n="e" s="T499">illiŋ, </ts>
               <ts e="T501" id="Seg_3574" n="e" s="T500">onton </ts>
               <ts e="T502" id="Seg_3576" n="e" s="T501">huːnnarɨŋ, </ts>
               <ts e="T503" id="Seg_3578" n="e" s="T502">astarɨn </ts>
               <ts e="T504" id="Seg_3580" n="e" s="T503">belemni͡ekteriger </ts>
               <ts e="T505" id="Seg_3582" n="e" s="T504">di͡eri. </ts>
               <ts e="T506" id="Seg_3584" n="e" s="T505">Barɨtɨn </ts>
               <ts e="T507" id="Seg_3586" n="e" s="T506">kɨčɨrɨ͡aktɨk </ts>
               <ts e="T508" id="Seg_3588" n="e" s="T507">oŋoruŋ, </ts>
               <ts e="T509" id="Seg_3590" n="e" s="T508">ogoloru </ts>
               <ts e="T510" id="Seg_3592" n="e" s="T509">ürpekke". </ts>
               <ts e="T511" id="Seg_3594" n="e" s="T510">Ogoloru </ts>
               <ts e="T512" id="Seg_3596" n="e" s="T511">honno </ts>
               <ts e="T513" id="Seg_3598" n="e" s="T512">üːren </ts>
               <ts e="T514" id="Seg_3600" n="e" s="T513">ilpitter, </ts>
               <ts e="T515" id="Seg_3602" n="e" s="T514">köllörbütter </ts>
               <ts e="T516" id="Seg_3604" n="e" s="T515">barɨlarɨgar </ts>
               <ts e="T517" id="Seg_3606" n="e" s="T516">oloror </ts>
               <ts e="T518" id="Seg_3608" n="e" s="T517">dʼi͡elerin, </ts>
               <ts e="T519" id="Seg_3610" n="e" s="T518">nʼuːččalɨː </ts>
               <ts e="T520" id="Seg_3612" n="e" s="T519">"koːmnata" </ts>
               <ts e="T521" id="Seg_3614" n="e" s="T520">aːttaːgɨ. </ts>
               <ts e="T522" id="Seg_3616" n="e" s="T521">Hamaːr, </ts>
               <ts e="T523" id="Seg_3618" n="e" s="T522">haŋa </ts>
               <ts e="T524" id="Seg_3620" n="e" s="T523">urbaːkɨlarɨ, </ts>
               <ts e="T525" id="Seg_3622" n="e" s="T524">ataktarɨ </ts>
               <ts e="T526" id="Seg_3624" n="e" s="T525">bi͡eretteːbitter. </ts>
               <ts e="T527" id="Seg_3626" n="e" s="T526">Barɨ </ts>
               <ts e="T528" id="Seg_3628" n="e" s="T527">kelbit </ts>
               <ts e="T529" id="Seg_3630" n="e" s="T528">dʼon </ts>
               <ts e="T530" id="Seg_3632" n="e" s="T529">astarɨn </ts>
               <ts e="T531" id="Seg_3634" n="e" s="T530">kɨrpalaːbɨttar, </ts>
               <ts e="T532" id="Seg_3636" n="e" s="T531">tu͡oktan </ts>
               <ts e="T533" id="Seg_3638" n="e" s="T532">kɨːs </ts>
               <ts e="T534" id="Seg_3640" n="e" s="T533">ogolor </ts>
               <ts e="T535" id="Seg_3642" n="e" s="T534">ɨtɨːllara </ts>
               <ts e="T536" id="Seg_3644" n="e" s="T535">bert </ts>
               <ts e="T537" id="Seg_3646" n="e" s="T536">ebit </ts>
               <ts e="T538" id="Seg_3648" n="e" s="T537">astarɨn </ts>
               <ts e="T539" id="Seg_3650" n="e" s="T538">ahɨnannar. </ts>
               <ts e="T540" id="Seg_3652" n="e" s="T539">Tojoːnɨ </ts>
               <ts e="T541" id="Seg_3654" n="e" s="T540">emi͡e </ts>
               <ts e="T542" id="Seg_3656" n="e" s="T541">kɨrpalaːbɨttar. </ts>
               <ts e="T543" id="Seg_3658" n="e" s="T542">Taŋnar </ts>
               <ts e="T544" id="Seg_3660" n="e" s="T543">taŋas </ts>
               <ts e="T545" id="Seg_3662" n="e" s="T544">bi͡erbitter, </ts>
               <ts e="T546" id="Seg_3664" n="e" s="T545">keter </ts>
               <ts e="T547" id="Seg_3666" n="e" s="T546">tunʼaktarɨn </ts>
               <ts e="T548" id="Seg_3668" n="e" s="T547">tuttarbɨttar. </ts>
               <ts e="T549" id="Seg_3670" n="e" s="T548">Kɨrabaːtɨgar </ts>
               <ts e="T550" id="Seg_3672" n="e" s="T549">keleːt </ts>
               <ts e="T551" id="Seg_3674" n="e" s="T550">olorbut </ts>
               <ts e="T552" id="Seg_3676" n="e" s="T551">körülüː </ts>
               <ts e="T553" id="Seg_3678" n="e" s="T552">tu͡ogu </ts>
               <ts e="T554" id="Seg_3680" n="e" s="T553">tuttarbɨttarɨn. </ts>
               <ts e="T555" id="Seg_3682" n="e" s="T554">Barɨta </ts>
               <ts e="T556" id="Seg_3684" n="e" s="T555">gini͡eke </ts>
               <ts e="T557" id="Seg_3686" n="e" s="T556">höp. </ts>
               <ts e="T558" id="Seg_3688" n="e" s="T557">Ol </ts>
               <ts e="T559" id="Seg_3690" n="e" s="T558">daː </ts>
               <ts e="T560" id="Seg_3692" n="e" s="T559">bu͡ollar </ts>
               <ts e="T561" id="Seg_3694" n="e" s="T560">togo </ts>
               <ts e="T562" id="Seg_3696" n="e" s="T561">ginner </ts>
               <ts e="T563" id="Seg_3698" n="e" s="T562">bi͡erbittere </ts>
               <ts e="T564" id="Seg_3700" n="e" s="T563">bu͡olu͡oj </ts>
               <ts e="T565" id="Seg_3702" n="e" s="T564">ikkiliː </ts>
               <ts e="T566" id="Seg_3704" n="e" s="T565">urbakɨnɨ, </ts>
               <ts e="T567" id="Seg_3706" n="e" s="T566">ikkiliː </ts>
               <ts e="T568" id="Seg_3708" n="e" s="T567">hɨ͡alɨjanɨ? </ts>
               <ts e="T569" id="Seg_3710" n="e" s="T568">Haŋa </ts>
               <ts e="T570" id="Seg_3712" n="e" s="T569">taŋastarɨn </ts>
               <ts e="T571" id="Seg_3714" n="e" s="T570">keter </ts>
               <ts e="T573" id="Seg_3716" n="e" s="T571">(han-) </ts>
               <ts e="T574" id="Seg_3718" n="e" s="T573">hanaːbɨt. </ts>
               <ts e="T575" id="Seg_3720" n="e" s="T574">Ogoloru </ts>
               <ts e="T576" id="Seg_3722" n="e" s="T575">huːnnara </ts>
               <ts e="T577" id="Seg_3724" n="e" s="T576">ilpitter. </ts>
               <ts e="T578" id="Seg_3726" n="e" s="T577">Kanna </ts>
               <ts e="T579" id="Seg_3728" n="e" s="T578">menik </ts>
               <ts e="T580" id="Seg_3730" n="e" s="T579">dʼon </ts>
               <ts e="T581" id="Seg_3732" n="e" s="T580">ele </ts>
               <ts e="T582" id="Seg_3734" n="e" s="T581">hanaːlarɨnan </ts>
               <ts e="T583" id="Seg_3736" n="e" s="T582">ü͡örüːleːktik </ts>
               <ts e="T584" id="Seg_3738" n="e" s="T583">külse-halsa </ts>
               <ts e="T585" id="Seg_3740" n="e" s="T584">"ataj, </ts>
               <ts e="T586" id="Seg_3742" n="e" s="T585">ataj" </ts>
               <ts e="T587" id="Seg_3744" n="e" s="T586">dehe </ts>
               <ts e="T588" id="Seg_3746" n="e" s="T587">olorbuttar. </ts>
               <ts e="T589" id="Seg_3748" n="e" s="T588">Tojoː </ts>
               <ts e="T590" id="Seg_3750" n="e" s="T589">huːnan </ts>
               <ts e="T591" id="Seg_3752" n="e" s="T590">büteːt </ts>
               <ts e="T592" id="Seg_3754" n="e" s="T591">taŋnɨbɨt. </ts>
               <ts e="T593" id="Seg_3756" n="e" s="T592">Keppit </ts>
               <ts e="T594" id="Seg_3758" n="e" s="T593">ürüŋ </ts>
               <ts e="T595" id="Seg_3760" n="e" s="T594">urbaːkɨtɨn, </ts>
               <ts e="T596" id="Seg_3762" n="e" s="T595">hɨ͡alɨjatɨn. </ts>
               <ts e="T597" id="Seg_3764" n="e" s="T596">Horoktorun </ts>
               <ts e="T598" id="Seg_3766" n="e" s="T597">hapaːstaːn </ts>
               <ts e="T599" id="Seg_3768" n="e" s="T598">huːlaːbɨt, </ts>
               <ts e="T600" id="Seg_3770" n="e" s="T599">kojut </ts>
               <ts e="T601" id="Seg_3772" n="e" s="T600">keteːri. </ts>
               <ts e="T602" id="Seg_3774" n="e" s="T601">Etiger </ts>
               <ts e="T603" id="Seg_3776" n="e" s="T602">keten </ts>
               <ts e="T604" id="Seg_3778" n="e" s="T603">ürüŋ </ts>
               <ts e="T605" id="Seg_3780" n="e" s="T604">taŋastarɨn </ts>
               <ts e="T606" id="Seg_3782" n="e" s="T605">tu͡ok </ts>
               <ts e="T607" id="Seg_3784" n="e" s="T606">da </ts>
               <ts e="T608" id="Seg_3786" n="e" s="T607">bu͡olbatak </ts>
               <ts e="T609" id="Seg_3788" n="e" s="T608">kördük </ts>
               <ts e="T610" id="Seg_3790" n="e" s="T609">hɨldʼɨbɨt. </ts>
               <ts e="T611" id="Seg_3792" n="e" s="T610">Dogottoro </ts>
               <ts e="T612" id="Seg_3794" n="e" s="T611">da </ts>
               <ts e="T613" id="Seg_3796" n="e" s="T612">tu͡ogu </ts>
               <ts e="T614" id="Seg_3798" n="e" s="T613">da </ts>
               <ts e="T615" id="Seg_3800" n="e" s="T614">di͡ebetter </ts>
               <ts e="T616" id="Seg_3802" n="e" s="T615">ebit </ts>
               <ts e="T617" id="Seg_3804" n="e" s="T616">gini͡eke. </ts>
               <ts e="T618" id="Seg_3806" n="e" s="T617">Intʼernaːtka </ts>
               <ts e="T619" id="Seg_3808" n="e" s="T618">üleliːr </ts>
               <ts e="T620" id="Seg_3810" n="e" s="T619">biːr </ts>
               <ts e="T621" id="Seg_3812" n="e" s="T620">dʼaktar </ts>
               <ts e="T622" id="Seg_3814" n="e" s="T621">tu͡olkulappɨt </ts>
               <ts e="T623" id="Seg_3816" n="e" s="T622">kajtak </ts>
               <ts e="T624" id="Seg_3818" n="e" s="T623">gini </ts>
               <ts e="T625" id="Seg_3820" n="e" s="T624">taŋnar </ts>
               <ts e="T626" id="Seg_3822" n="e" s="T625">bu͡olu͡ogun. </ts>
               <ts e="T627" id="Seg_3824" n="e" s="T626">Ahɨːr </ts>
               <ts e="T628" id="Seg_3826" n="e" s="T627">kemnere </ts>
               <ts e="T629" id="Seg_3828" n="e" s="T628">kelbit. </ts>
               <ts e="T630" id="Seg_3830" n="e" s="T629">Ogoloru </ts>
               <ts e="T631" id="Seg_3832" n="e" s="T630">olortoːbuttar </ts>
               <ts e="T633" id="Seg_3834" n="e" s="T631">(uhu-) </ts>
               <ts e="T634" id="Seg_3836" n="e" s="T633">uhun </ts>
               <ts e="T635" id="Seg_3838" n="e" s="T634">bagajɨ </ts>
               <ts e="T636" id="Seg_3840" n="e" s="T635">ostollor </ts>
               <ts e="T637" id="Seg_3842" n="e" s="T636">ikki </ts>
               <ts e="T638" id="Seg_3844" n="e" s="T637">di͡eki </ts>
               <ts e="T639" id="Seg_3846" n="e" s="T638">öttüleriger. </ts>
               <ts e="T640" id="Seg_3848" n="e" s="T639">Ulakan </ts>
               <ts e="T641" id="Seg_3850" n="e" s="T640">ihitterge </ts>
               <ts e="T642" id="Seg_3852" n="e" s="T641">uːrullubuttar </ts>
               <ts e="T643" id="Seg_3854" n="e" s="T642">minnʼiges </ts>
               <ts e="T644" id="Seg_3856" n="e" s="T643">hɨttaːk </ts>
               <ts e="T645" id="Seg_3858" n="e" s="T644">kili͡epter. </ts>
               <ts e="T646" id="Seg_3860" n="e" s="T645">Ogo </ts>
               <ts e="T647" id="Seg_3862" n="e" s="T646">aːjɨ </ts>
               <ts e="T648" id="Seg_3864" n="e" s="T647">kutattaːbɨttar </ts>
               <ts e="T649" id="Seg_3866" n="e" s="T648">balɨk </ts>
               <ts e="T650" id="Seg_3868" n="e" s="T649">minin. </ts>
               <ts e="T651" id="Seg_3870" n="e" s="T650">Lu͡oskalarɨ </ts>
               <ts e="T652" id="Seg_3872" n="e" s="T651">bi͡eretteːbitter. </ts>
               <ts e="T653" id="Seg_3874" n="e" s="T652">Körbüt, </ts>
               <ts e="T654" id="Seg_3876" n="e" s="T653">ogolor </ts>
               <ts e="T655" id="Seg_3878" n="e" s="T654">ahaːnnar </ts>
               <ts e="T656" id="Seg_3880" n="e" s="T655">ketektere </ts>
               <ts e="T657" id="Seg_3882" n="e" s="T656">agaj </ts>
               <ts e="T658" id="Seg_3884" n="e" s="T657">hüte </ts>
               <ts e="T659" id="Seg_3886" n="e" s="T658">hɨtar. </ts>
               <ts e="T660" id="Seg_3888" n="e" s="T659">Tojoː </ts>
               <ts e="T661" id="Seg_3890" n="e" s="T660">ahɨː </ts>
               <ts e="T662" id="Seg_3892" n="e" s="T661">oloron </ts>
               <ts e="T663" id="Seg_3894" n="e" s="T662">tugu </ts>
               <ts e="T664" id="Seg_3896" n="e" s="T663">ire </ts>
               <ts e="T665" id="Seg_3898" n="e" s="T664">hanaːta </ts>
               <ts e="T666" id="Seg_3900" n="e" s="T665">kelbit, </ts>
               <ts e="T667" id="Seg_3902" n="e" s="T666">maːmatɨn, </ts>
               <ts e="T668" id="Seg_3904" n="e" s="T667">agatɨn, </ts>
               <ts e="T669" id="Seg_3906" n="e" s="T668">Kɨčatɨn </ts>
               <ts e="T670" id="Seg_3908" n="e" s="T669">öjdöːn. </ts>
               <ts e="T671" id="Seg_3910" n="e" s="T670">Ahaːn </ts>
               <ts e="T672" id="Seg_3912" n="e" s="T671">büteːt </ts>
               <ts e="T673" id="Seg_3914" n="e" s="T672">ogolor </ts>
               <ts e="T674" id="Seg_3916" n="e" s="T673">Valačaːnka </ts>
               <ts e="T675" id="Seg_3918" n="e" s="T674">üstün </ts>
               <ts e="T676" id="Seg_3920" n="e" s="T675">oːnnʼuː </ts>
               <ts e="T677" id="Seg_3922" n="e" s="T676">hɨldʼɨbɨttar, </ts>
               <ts e="T678" id="Seg_3924" n="e" s="T677">körülüː </ts>
               <ts e="T679" id="Seg_3926" n="e" s="T678">nʼuːčča </ts>
               <ts e="T680" id="Seg_3928" n="e" s="T679">dʼi͡eleri </ts>
               <ts e="T681" id="Seg_3930" n="e" s="T680">barɨ </ts>
               <ts e="T682" id="Seg_3932" n="e" s="T681">tergi͡eni. </ts>
               <ts e="T683" id="Seg_3934" n="e" s="T682">Ginnerge </ts>
               <ts e="T684" id="Seg_3936" n="e" s="T683">barɨta </ts>
               <ts e="T685" id="Seg_3938" n="e" s="T684">dʼikti, </ts>
               <ts e="T686" id="Seg_3940" n="e" s="T685">haŋardɨː </ts>
               <ts e="T687" id="Seg_3942" n="e" s="T686">kelbit </ts>
               <ts e="T688" id="Seg_3944" n="e" s="T687">kihilerge. </ts>
               <ts e="T689" id="Seg_3946" n="e" s="T688">Nöŋü͡ö </ts>
               <ts e="T690" id="Seg_3948" n="e" s="T689">kün </ts>
               <ts e="T691" id="Seg_3950" n="e" s="T690">emi͡e </ts>
               <ts e="T692" id="Seg_3952" n="e" s="T691">ulakan </ts>
               <ts e="T693" id="Seg_3954" n="e" s="T692">ogoloːk </ts>
               <ts e="T694" id="Seg_3956" n="e" s="T693">kös </ts>
               <ts e="T695" id="Seg_3958" n="e" s="T694">kelbit </ts>
               <ts e="T696" id="Seg_3960" n="e" s="T695">atɨn </ts>
               <ts e="T697" id="Seg_3962" n="e" s="T696">kalxoːztan. </ts>
               <ts e="T698" id="Seg_3964" n="e" s="T697">Haŋardɨː </ts>
               <ts e="T699" id="Seg_3966" n="e" s="T698">keliː </ts>
               <ts e="T700" id="Seg_3968" n="e" s="T699">ogolor </ts>
               <ts e="T701" id="Seg_3970" n="e" s="T700">kiːrbikteːge </ts>
               <ts e="T702" id="Seg_3972" n="e" s="T701">bert, </ts>
               <ts e="T703" id="Seg_3974" n="e" s="T702">barɨttan </ts>
               <ts e="T704" id="Seg_3976" n="e" s="T703">(kutana) </ts>
               <ts e="T705" id="Seg_3978" n="e" s="T704">kuttanar </ts>
               <ts e="T706" id="Seg_3980" n="e" s="T705">kördükter. </ts>
               <ts e="T707" id="Seg_3982" n="e" s="T706">Ginner </ts>
               <ts e="T708" id="Seg_3984" n="e" s="T707">olus </ts>
               <ts e="T709" id="Seg_3986" n="e" s="T708">ör </ts>
               <ts e="T710" id="Seg_3988" n="e" s="T709">itigirdik </ts>
               <ts e="T711" id="Seg_3990" n="e" s="T710">bu͡olbataktar. </ts>
               <ts e="T712" id="Seg_3992" n="e" s="T711">Emi͡e </ts>
               <ts e="T713" id="Seg_3994" n="e" s="T712">Tojoː, </ts>
               <ts e="T714" id="Seg_3996" n="e" s="T713">Miteː, </ts>
               <ts e="T715" id="Seg_3998" n="e" s="T714">Ujbaːn </ts>
               <ts e="T716" id="Seg_4000" n="e" s="T715">kördük </ts>
               <ts e="T717" id="Seg_4002" n="e" s="T716">tollubat </ts>
               <ts e="T718" id="Seg_4004" n="e" s="T717">mi͡ereni </ts>
               <ts e="T719" id="Seg_4006" n="e" s="T718">ɨlbɨttar </ts>
               <ts e="T720" id="Seg_4008" n="e" s="T719">haŋa </ts>
               <ts e="T721" id="Seg_4010" n="e" s="T720">dogottorun </ts>
               <ts e="T722" id="Seg_4012" n="e" s="T721">körönnör. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_4013" s="T0">PoNA_19900810_TojooInVolochanka_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_4014" s="T4">PoNA_19900810_TojooInVolochanka_nar.002 (001.002)</ta>
            <ta e="T26" id="Seg_4015" s="T10">PoNA_19900810_TojooInVolochanka_nar.003 (001.003)</ta>
            <ta e="T42" id="Seg_4016" s="T26">PoNA_19900810_TojooInVolochanka_nar.004 (001.004)</ta>
            <ta e="T46" id="Seg_4017" s="T42">PoNA_19900810_TojooInVolochanka_nar.005 (001.005)</ta>
            <ta e="T53" id="Seg_4018" s="T46">PoNA_19900810_TojooInVolochanka_nar.006 (001.006)</ta>
            <ta e="T58" id="Seg_4019" s="T53">PoNA_19900810_TojooInVolochanka_nar.007 (001.007)</ta>
            <ta e="T64" id="Seg_4020" s="T58">PoNA_19900810_TojooInVolochanka_nar.008 (001.008)</ta>
            <ta e="T73" id="Seg_4021" s="T64">PoNA_19900810_TojooInVolochanka_nar.009 (001.009)</ta>
            <ta e="T84" id="Seg_4022" s="T73">PoNA_19900810_TojooInVolochanka_nar.010 (001.010)</ta>
            <ta e="T93" id="Seg_4023" s="T84">PoNA_19900810_TojooInVolochanka_nar.011 (001.011)</ta>
            <ta e="T97" id="Seg_4024" s="T93">PoNA_19900810_TojooInVolochanka_nar.012 (001.012)</ta>
            <ta e="T103" id="Seg_4025" s="T97">PoNA_19900810_TojooInVolochanka_nar.013 (001.013)</ta>
            <ta e="T108" id="Seg_4026" s="T103">PoNA_19900810_TojooInVolochanka_nar.014 (001.014)</ta>
            <ta e="T112" id="Seg_4027" s="T108">PoNA_19900810_TojooInVolochanka_nar.015 (001.015)</ta>
            <ta e="T122" id="Seg_4028" s="T112">PoNA_19900810_TojooInVolochanka_nar.016 (001.016)</ta>
            <ta e="T129" id="Seg_4029" s="T122">PoNA_19900810_TojooInVolochanka_nar.017 (001.017)</ta>
            <ta e="T138" id="Seg_4030" s="T129">PoNA_19900810_TojooInVolochanka_nar.018 (001.018)</ta>
            <ta e="T141" id="Seg_4031" s="T138">PoNA_19900810_TojooInVolochanka_nar.019 (001.019)</ta>
            <ta e="T146" id="Seg_4032" s="T141">PoNA_19900810_TojooInVolochanka_nar.020 (001.020)</ta>
            <ta e="T156" id="Seg_4033" s="T146">PoNA_19900810_TojooInVolochanka_nar.021 (001.021)</ta>
            <ta e="T161" id="Seg_4034" s="T156">PoNA_19900810_TojooInVolochanka_nar.022 (001.022)</ta>
            <ta e="T163" id="Seg_4035" s="T161">PoNA_19900810_TojooInVolochanka_nar.023 (001.023)</ta>
            <ta e="T172" id="Seg_4036" s="T163">PoNA_19900810_TojooInVolochanka_nar.024 (001.023)</ta>
            <ta e="T179" id="Seg_4037" s="T172">PoNA_19900810_TojooInVolochanka_nar.025 (001.024)</ta>
            <ta e="T186" id="Seg_4038" s="T179">PoNA_19900810_TojooInVolochanka_nar.026 (001.025)</ta>
            <ta e="T192" id="Seg_4039" s="T186">PoNA_19900810_TojooInVolochanka_nar.027 (001.026)</ta>
            <ta e="T194" id="Seg_4040" s="T192">PoNA_19900810_TojooInVolochanka_nar.028 (001.027)</ta>
            <ta e="T201" id="Seg_4041" s="T194">PoNA_19900810_TojooInVolochanka_nar.029 (001.028)</ta>
            <ta e="T207" id="Seg_4042" s="T201">PoNA_19900810_TojooInVolochanka_nar.030 (001.029)</ta>
            <ta e="T212" id="Seg_4043" s="T207">PoNA_19900810_TojooInVolochanka_nar.031 (001.030)</ta>
            <ta e="T220" id="Seg_4044" s="T212">PoNA_19900810_TojooInVolochanka_nar.032 (001.031)</ta>
            <ta e="T228" id="Seg_4045" s="T220">PoNA_19900810_TojooInVolochanka_nar.033 (001.032)</ta>
            <ta e="T235" id="Seg_4046" s="T228">PoNA_19900810_TojooInVolochanka_nar.034 (001.033)</ta>
            <ta e="T238" id="Seg_4047" s="T235">PoNA_19900810_TojooInVolochanka_nar.035 (001.034)</ta>
            <ta e="T244" id="Seg_4048" s="T238">PoNA_19900810_TojooInVolochanka_nar.036 (001.035)</ta>
            <ta e="T252" id="Seg_4049" s="T244">PoNA_19900810_TojooInVolochanka_nar.037 (001.036)</ta>
            <ta e="T263" id="Seg_4050" s="T252">PoNA_19900810_TojooInVolochanka_nar.038 (001.037)</ta>
            <ta e="T268" id="Seg_4051" s="T263">PoNA_19900810_TojooInVolochanka_nar.039 (001.039)</ta>
            <ta e="T272" id="Seg_4052" s="T268">PoNA_19900810_TojooInVolochanka_nar.040 (001.040)</ta>
            <ta e="T281" id="Seg_4053" s="T272">PoNA_19900810_TojooInVolochanka_nar.041 (001.041)</ta>
            <ta e="T290" id="Seg_4054" s="T281">PoNA_19900810_TojooInVolochanka_nar.042 (001.042)</ta>
            <ta e="T298" id="Seg_4055" s="T290">PoNA_19900810_TojooInVolochanka_nar.043 (001.043)</ta>
            <ta e="T305" id="Seg_4056" s="T298">PoNA_19900810_TojooInVolochanka_nar.044 (001.044)</ta>
            <ta e="T311" id="Seg_4057" s="T305">PoNA_19900810_TojooInVolochanka_nar.045 (001.045)</ta>
            <ta e="T317" id="Seg_4058" s="T311">PoNA_19900810_TojooInVolochanka_nar.046 (001.047)</ta>
            <ta e="T328" id="Seg_4059" s="T317">PoNA_19900810_TojooInVolochanka_nar.047 (001.048)</ta>
            <ta e="T333" id="Seg_4060" s="T328">PoNA_19900810_TojooInVolochanka_nar.048 (001.049)</ta>
            <ta e="T340" id="Seg_4061" s="T333">PoNA_19900810_TojooInVolochanka_nar.049 (001.050)</ta>
            <ta e="T344" id="Seg_4062" s="T340">PoNA_19900810_TojooInVolochanka_nar.050 (001.051)</ta>
            <ta e="T351" id="Seg_4063" s="T344">PoNA_19900810_TojooInVolochanka_nar.051 (001.052)</ta>
            <ta e="T364" id="Seg_4064" s="T351">PoNA_19900810_TojooInVolochanka_nar.052 (001.053)</ta>
            <ta e="T371" id="Seg_4065" s="T364">PoNA_19900810_TojooInVolochanka_nar.053 (001.054)</ta>
            <ta e="T377" id="Seg_4066" s="T371">PoNA_19900810_TojooInVolochanka_nar.054 (001.055)</ta>
            <ta e="T381" id="Seg_4067" s="T377">PoNA_19900810_TojooInVolochanka_nar.055 (001.056)</ta>
            <ta e="T389" id="Seg_4068" s="T381">PoNA_19900810_TojooInVolochanka_nar.056 (001.057)</ta>
            <ta e="T396" id="Seg_4069" s="T389">PoNA_19900810_TojooInVolochanka_nar.057 (001.058)</ta>
            <ta e="T401" id="Seg_4070" s="T396">PoNA_19900810_TojooInVolochanka_nar.058 (001.059)</ta>
            <ta e="T405" id="Seg_4071" s="T401">PoNA_19900810_TojooInVolochanka_nar.059 (001.060)</ta>
            <ta e="T411" id="Seg_4072" s="T405">PoNA_19900810_TojooInVolochanka_nar.060 (001.061)</ta>
            <ta e="T420" id="Seg_4073" s="T411">PoNA_19900810_TojooInVolochanka_nar.061 (001.062)</ta>
            <ta e="T426" id="Seg_4074" s="T420">PoNA_19900810_TojooInVolochanka_nar.062 (001.063)</ta>
            <ta e="T433" id="Seg_4075" s="T426">PoNA_19900810_TojooInVolochanka_nar.063 (001.064)</ta>
            <ta e="T440" id="Seg_4076" s="T433">PoNA_19900810_TojooInVolochanka_nar.064 (001.065)</ta>
            <ta e="T447" id="Seg_4077" s="T440">PoNA_19900810_TojooInVolochanka_nar.065 (001.066)</ta>
            <ta e="T452" id="Seg_4078" s="T447">PoNA_19900810_TojooInVolochanka_nar.066 (001.067)</ta>
            <ta e="T462" id="Seg_4079" s="T452">PoNA_19900810_TojooInVolochanka_nar.067 (001.069)</ta>
            <ta e="T469" id="Seg_4080" s="T462">PoNA_19900810_TojooInVolochanka_nar.068 (001.070)</ta>
            <ta e="T480" id="Seg_4081" s="T469">PoNA_19900810_TojooInVolochanka_nar.069 (001.071)</ta>
            <ta e="T484" id="Seg_4082" s="T480">PoNA_19900810_TojooInVolochanka_nar.070 (001.072)</ta>
            <ta e="T494" id="Seg_4083" s="T484">PoNA_19900810_TojooInVolochanka_nar.071 (001.072)</ta>
            <ta e="T496" id="Seg_4084" s="T494">PoNA_19900810_TojooInVolochanka_nar.072 (001.073)</ta>
            <ta e="T505" id="Seg_4085" s="T496">PoNA_19900810_TojooInVolochanka_nar.073 (001.073)</ta>
            <ta e="T510" id="Seg_4086" s="T505">PoNA_19900810_TojooInVolochanka_nar.074 (001.074)</ta>
            <ta e="T521" id="Seg_4087" s="T510">PoNA_19900810_TojooInVolochanka_nar.075 (001.075)</ta>
            <ta e="T526" id="Seg_4088" s="T521">PoNA_19900810_TojooInVolochanka_nar.076 (001.076)</ta>
            <ta e="T539" id="Seg_4089" s="T526">PoNA_19900810_TojooInVolochanka_nar.077 (001.077)</ta>
            <ta e="T542" id="Seg_4090" s="T539">PoNA_19900810_TojooInVolochanka_nar.078 (001.078)</ta>
            <ta e="T548" id="Seg_4091" s="T542">PoNA_19900810_TojooInVolochanka_nar.079 (001.079)</ta>
            <ta e="T554" id="Seg_4092" s="T548">PoNA_19900810_TojooInVolochanka_nar.080 (001.080)</ta>
            <ta e="T557" id="Seg_4093" s="T554">PoNA_19900810_TojooInVolochanka_nar.081 (001.081)</ta>
            <ta e="T568" id="Seg_4094" s="T557">PoNA_19900810_TojooInVolochanka_nar.082 (001.082)</ta>
            <ta e="T574" id="Seg_4095" s="T568">PoNA_19900810_TojooInVolochanka_nar.083 (001.083)</ta>
            <ta e="T577" id="Seg_4096" s="T574">PoNA_19900810_TojooInVolochanka_nar.084 (001.084)</ta>
            <ta e="T588" id="Seg_4097" s="T577">PoNA_19900810_TojooInVolochanka_nar.085 (001.085)</ta>
            <ta e="T592" id="Seg_4098" s="T588">PoNA_19900810_TojooInVolochanka_nar.086 (001.087)</ta>
            <ta e="T596" id="Seg_4099" s="T592">PoNA_19900810_TojooInVolochanka_nar.087 (001.088)</ta>
            <ta e="T601" id="Seg_4100" s="T596">PoNA_19900810_TojooInVolochanka_nar.088 (001.089)</ta>
            <ta e="T610" id="Seg_4101" s="T601">PoNA_19900810_TojooInVolochanka_nar.089 (001.090)</ta>
            <ta e="T617" id="Seg_4102" s="T610">PoNA_19900810_TojooInVolochanka_nar.090 (001.091)</ta>
            <ta e="T626" id="Seg_4103" s="T617">PoNA_19900810_TojooInVolochanka_nar.091 (001.092)</ta>
            <ta e="T629" id="Seg_4104" s="T626">PoNA_19900810_TojooInVolochanka_nar.092 (001.093)</ta>
            <ta e="T639" id="Seg_4105" s="T629">PoNA_19900810_TojooInVolochanka_nar.093 (001.094)</ta>
            <ta e="T645" id="Seg_4106" s="T639">PoNA_19900810_TojooInVolochanka_nar.094 (001.095)</ta>
            <ta e="T650" id="Seg_4107" s="T645">PoNA_19900810_TojooInVolochanka_nar.095 (001.096)</ta>
            <ta e="T652" id="Seg_4108" s="T650">PoNA_19900810_TojooInVolochanka_nar.096 (001.097)</ta>
            <ta e="T659" id="Seg_4109" s="T652">PoNA_19900810_TojooInVolochanka_nar.097 (001.098)</ta>
            <ta e="T670" id="Seg_4110" s="T659">PoNA_19900810_TojooInVolochanka_nar.098 (001.099)</ta>
            <ta e="T682" id="Seg_4111" s="T670">PoNA_19900810_TojooInVolochanka_nar.099 (001.100)</ta>
            <ta e="T688" id="Seg_4112" s="T682">PoNA_19900810_TojooInVolochanka_nar.100 (001.101)</ta>
            <ta e="T697" id="Seg_4113" s="T688">PoNA_19900810_TojooInVolochanka_nar.101 (001.102)</ta>
            <ta e="T706" id="Seg_4114" s="T697">PoNA_19900810_TojooInVolochanka_nar.102 (001.103)</ta>
            <ta e="T711" id="Seg_4115" s="T706">PoNA_19900810_TojooInVolochanka_nar.103 (001.104)</ta>
            <ta e="T722" id="Seg_4116" s="T711">PoNA_19900810_TojooInVolochanka_nar.104 (001.105)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_4117" s="T0">Попов Николай Анисимович. Иһиллээӈ өһү – "Дьикти табалар".</ta>
            <ta e="T10" id="Seg_4118" s="T4">Үһүс күнүгэр оголоок көс тийбит Волочанкага.</ta>
            <ta e="T26" id="Seg_4119" s="T10">Авам районун киин ологун ньууччалара, һакалара, -- барылара комуллубуттара кэлии оголору көрөөрү, кукла көрдүк таӈастаактары, киһи таптыактары.</ta>
            <ta e="T42" id="Seg_4120" s="T26">Олус аайдаана һуок, дьылыгыр Волочанка олок биир дурук тиллибит көрдүк, таӈара күнүн бэлиэтиир көрдүк буолан каалбыт. </ta>
            <ta e="T46" id="Seg_4121" s="T42">Оголор буоллактарына киһилэри көрбөттөр.</ta>
            <ta e="T53" id="Seg_4122" s="T46">Ньуучча дьиэлэр диэк агай кар.. карактара.</ta>
            <ta e="T58" id="Seg_4123" s="T53">Улица үстүн атаралатан (атарылатан) иһэннэр уучагынан.</ta>
            <ta e="T64" id="Seg_4124" s="T58">Эһэлэрэ, Ониисим огонньор турбут аскуола (ускуола) аттыгар.</ta>
            <ta e="T73" id="Seg_4125" s="T64">Гинини төгүрүччү аайданнаак армия угучактарынан (угучактарын) үрд.. үрдүлэриттэн ыстаӈалаабыттар. </ta>
            <ta e="T84" id="Seg_4126" s="T73">Аскуола (ускуола) аттыгар тураллар Пётр Кузьмич, үөрэтэр дьон һүбэһиттэрэ онно интернат үлэһиттэрэ.</ta>
            <ta e="T93" id="Seg_4127" s="T84">Барылара оголору кабан ыланнар дорооболоһоллор, туогу эрэ гинилэргэ һаӈараллар.</ta>
            <ta e="T97" id="Seg_4128" s="T93">Оголор ньуучча тылын туолкулаабаттар.</ta>
            <ta e="T103" id="Seg_4129" s="T97">Ол иһин кэлии кыра дьон кэпсэппэттэр.</ta>
            <ta e="T108" id="Seg_4130" s="T103">Одуулур агай буолбуттар учителлэрин һырайдарын.</ta>
            <ta e="T112" id="Seg_4131" s="T108">Гинилэр олус интириэһи көрбүттэр.</ta>
            <ta e="T122" id="Seg_4132" s="T112">Олус улакан ньуучча дьиэлэри, атын киһилэри, ньууччалары, атын тыллаактары, таӈастаактары.</ta>
            <ta e="T129" id="Seg_4133" s="T122">Атактарыгар буоллагына таба туньага көрдүк атагы иилиммиттэр.</ta>
            <ta e="T138" id="Seg_4134" s="T129">Эр киһилэр бастарыгар маамаларын комуоһун көрдүк бэргэһэни кэтэ һылдьаллар.</ta>
            <ta e="T141" id="Seg_4135" s="T138">Дьактаттара өрүүтэ һуоктар.</ta>
            <ta e="T146" id="Seg_4136" s="T141">Астара быһыллыбыттар, эр киһи көрдүк.</ta>
            <ta e="T156" id="Seg_4137" s="T146">Ырбаакылара аллара өттүгэр олус чипчигыр, ол иһин гиннэр араччы (арыччы) каамаллар.</ta>
            <ta e="T161" id="Seg_4138" s="T156">Тойоо олус одулуу турбут дьактаттары.</ta>
            <ta e="T163" id="Seg_4139" s="T161">Иһигэр һаныыр: </ta>
            <ta e="T172" id="Seg_4140" s="T163">"Бадага, итинник ырбаакыннан һүүрэ һатаабаттар, кыратык ира (эрэ) каамаллар эбит".</ta>
            <ta e="T179" id="Seg_4141" s="T172">Доготторугар тугу эрэ ити туһунан кэпсээри гыммыт.</ta>
            <ta e="T186" id="Seg_4142" s="T179">Аньагын ирэ (эрэ) арыйбыт, туок ирэ (эрэ) аайдаана буолбут.</ta>
            <ta e="T192" id="Seg_4143" s="T186">Көрбүт ол диэк, кантан аайдаан таксыбыт.</ta>
            <ta e="T194" id="Seg_4144" s="T192">О, һосподи!</ta>
            <ta e="T201" id="Seg_4145" s="T194">Туок ирэ (эрэ) киһи көрбөтөк һыргалаага таллатан испит.</ta>
            <ta e="T207" id="Seg_4146" s="T201">Һыргата иэрикэй, табата буоллагына улакана бэрт.</ta>
            <ta e="T212" id="Seg_4147" s="T207">Олус уһун дөйбүүр көрдүк кутуруктаак.</ta>
            <ta e="T220" id="Seg_4148" s="T212">Һыргалыыр киһитэ буоллагына һырайа түүтэ бэрт, көп бытыктаак.</ta>
            <ta e="T228" id="Seg_4149" s="T220">Табатын кэнниттэн һатыы каман иһэн туппут икки ньуоӈуну.</ta>
            <ta e="T235" id="Seg_4150" s="T228">Табата кайтак (кайдак) ирэ (эрэ) каамарын аайы баһын мөӈтөрөр.</ta>
            <ta e="T238" id="Seg_4151" s="T235">Ол аайы һыргаһыт:</ta>
            <ta e="T244" id="Seg_4152" s="T238">"Но-о, но-о!" – диир, олоӈкону иһиллиир көрдүк.</ta>
            <ta e="T252" id="Seg_4153" s="T244">Күүстээк багайитык (багайдык) аньагынан амсайбыт, минньигэс һыаны һиир көрдүк.</ta>
            <ta e="T263" id="Seg_4154" s="T252">"Кайа, ити туок табата буолар?" – көрө туран үөрбүччэ Тойоо һаӈа аллыбыт.</ta>
            <ta e="T268" id="Seg_4155" s="T263">Гини агай ону көрөн турбатага.</ta>
            <ta e="T272" id="Seg_4156" s="T268">Бары оголор одуугу буолбуттар.</ta>
            <ta e="T281" id="Seg_4157" s="T272">Бэйэ бэйэлэрин гытта кэпсэтэ иликтэринэ эмиэ көрбүттэр һаӈа дьиктини.</ta>
            <ta e="T290" id="Seg_4158" s="T281">Кырдьагас ньуучча эмээксинэ туок ирэ (эрэ) кыра табакааттарын үүрэн испит.</ta>
            <ta e="T298" id="Seg_4159" s="T290">Һыгынньак, култумак табакааттар ньамчыһак атактарынан һүрэлээн испиттэр бардьыгыныы-бардьыгыныы.</ta>
            <ta e="T305" id="Seg_4160" s="T298">Онтулара котон (көтөн) иһэннэр ол-бу һир диэк бутарыһаллар(бытарыһаллар).</ta>
            <ta e="T311" id="Seg_4161" s="T305">Паа! – диэн үөгүлээбит Тойоо, – һыгынньак кыыллар!"</ta>
            <ta e="T317" id="Seg_4162" s="T311">Оголор элэ истэринэн күлсэллэр чөмүйэлэринэн көллөрө-көллөрө.</ta>
            <ta e="T328" id="Seg_4163" s="T317">"Көр, догоо, муннулара күрэй лэпсэтин көрдүк ити ирэ (эрэ) икки ирэ (эрэ) үүттээктэр.</ta>
            <ta e="T333" id="Seg_4164" s="T328">Таба итинник буолбатак, биһиэттэрэ – боскуойдар".</ta>
            <ta e="T340" id="Seg_4165" s="T333">Маӈнай куттана һыспыттар, онтон чугаһаабыттар өтүөтүк (үтүөтүк) көрөөрү.</ta>
            <ta e="T344" id="Seg_4166" s="T340">Тойоо бэйтин аллаагын кыайбатак.</ta>
            <ta e="T351" id="Seg_4167" s="T344">Биирдэригэр һүүрэн кэлэн үрдүтүгэр угучактана гына ыстаммыт.</ta>
            <ta e="T364" id="Seg_4168" s="T351">Кыыла, һиһигэр киһини билэн, турбут һириттэн ыстаммыт үрбүччэ элэ күүһүнэн көппүт баһын коту.</ta>
            <ta e="T371" id="Seg_4169" s="T364">Угучактан түспэт ого тутта һатаабыт табатын түүтүүттэн. </ta>
            <ta e="T377" id="Seg_4170" s="T371">Кайтак (кайдак) да, канна да илиилэрэ иӈнибэт.</ta>
            <ta e="T381" id="Seg_4171" s="T377">Табатын түүлэрэ катыы көрдүктэр.</ta>
            <ta e="T389" id="Seg_4172" s="T381">Угучага элэ тыынынан үөгүлээбит баһын коту һүүрэн испит.</ta>
            <ta e="T396" id="Seg_4173" s="T389">Һорок һибинньээлэр ону көрөннөр биир дурук көппүттэр.</ta>
            <ta e="T401" id="Seg_4174" s="T396">Кэннилэриттэн кырдьагас эмээксин һүүрэ һатаабыт.</ta>
            <ta e="T405" id="Seg_4175" s="T401">Тойоо туугу ирэ (эрэ) үөгүлүү-үөгүлүү… </ta>
            <ta e="T411" id="Seg_4176" s="T405">Һүүрэрин аайы Тойоо диэк таньагынан далайар.</ta>
            <ta e="T420" id="Seg_4177" s="T411">Һыгынньак угучага дьүдэӈи, бэлээлээк (билээлээк) уулаак оӈкуучакка "лаӈ" гына түспүт.</ta>
            <ta e="T426" id="Seg_4178" s="T420">Тойоо һибинньээтин гытта тура һатаан һыппыт.</ta>
            <ta e="T433" id="Seg_4179" s="T426">Һырайа даа туога да көстүбэт, буор буолбут.</ta>
            <ta e="T440" id="Seg_4180" s="T433">Тура һатаан элэтэ буолан эмээксин һиппит гинини.</ta>
            <ta e="T447" id="Seg_4181" s="T440">Тойоо һырайын көрөн күлүмнээбит, илиитинэн агай һапсыйбыт.</ta>
            <ta e="T452" id="Seg_4182" s="T447">"Һуптуруй кантан кэлбитиӈ!" -- диэбит көрдүк.</ta>
            <ta e="T462" id="Seg_4183" s="T452">Тойоо тура эккирээт, элэ күүһүнэн тыына һуок доготторун диэк тэппит.</ta>
            <ta e="T469" id="Seg_4184" s="T462">"Ара-а, араччы өлө һыстым, араччы өлө һыстым!"</ta>
            <ta e="T480" id="Seg_4185" s="T469">Учителлэрэ дирэктор буолан туок да диэктэрин бэрдиттэн элэ истэринэн күлэ турбуттар.</ta>
            <ta e="T484" id="Seg_4186" s="T480">Павел Кузьмич элэ һанаатын һаӈарбыт: </ta>
            <ta e="T494" id="Seg_4187" s="T484">"Кайтак (кайдак) да диэккэ бэрт олус мэник дьон кэлбиттэр, быһылаак".</ta>
            <ta e="T496" id="Seg_4188" s="T494">Доготторугар диэбит: </ta>
            <ta e="T505" id="Seg_4189" s="T496">"Аны гинилэри интернакка иллиӈ, онтон һууннарыӈ, астарын бэлэмниэктэригэр диэри".</ta>
            <ta e="T510" id="Seg_4190" s="T505">"Барытын кычырыактык оӈоруӈ, оголору үртпэккэ".</ta>
            <ta e="T521" id="Seg_4191" s="T510">Оголору һонно үүрэн илпиттэр, көллөрбүттэр барыларыгар олорор дьиэлэрин, ньууччалыы "комната" ааттаагы.</ta>
            <ta e="T526" id="Seg_4192" s="T521">Һамаар, һаӈа урбаакылары, атактары биэрэттээбиттэр.</ta>
            <ta e="T539" id="Seg_4193" s="T526">Бары кэлбит дьон астарын кырпалаабыттар, туоктан кыыс оголор ытыыллара бэрт эбит астарын аһынаннар.</ta>
            <ta e="T542" id="Seg_4194" s="T539">Тойооны эмиэ кырпалаабыттар.</ta>
            <ta e="T548" id="Seg_4195" s="T542">Таӈнар таӈас биэрбиттэр, кэтэр туньактарын туттарбыттар.</ta>
            <ta e="T554" id="Seg_4196" s="T548">Кырабаатыгар кэлээт олорбут көрүлүү туогу туттарбыттарын.</ta>
            <ta e="T557" id="Seg_4197" s="T554">Барыта гиниэкэ һөп.</ta>
            <ta e="T568" id="Seg_4198" s="T557">Ол даа буоллар того гиннэр биэрбиттэрэ буолуой иккилии урбакыны, иккилии һыалыйаны?</ta>
            <ta e="T574" id="Seg_4199" s="T568">Һаӈа таӈастарын кэтэр һан.. һанаабыт.</ta>
            <ta e="T577" id="Seg_4200" s="T574">Оголору һууннара илпиттэр.</ta>
            <ta e="T588" id="Seg_4201" s="T577">Канна мэник дьон элэ һанааларынан үөрүлээктик күлсэ-һарса: ‎‎"Атай, атай!" – дэһэ олорбуттар.</ta>
            <ta e="T592" id="Seg_4202" s="T588">Тойоо һуунан бүтээт таӈныбыт.</ta>
            <ta e="T596" id="Seg_4203" s="T592">Кэппит үрүӈ урбаакытын, һыалыйатын.</ta>
            <ta e="T601" id="Seg_4204" s="T596">Һорокторун һапаастаан һуулаабыт, койут кэтээри.</ta>
            <ta e="T610" id="Seg_4205" s="T601">Этигэр кэтэн үрүӈ таӈастарын туок да буолбатак көрдүк һылдьыбыт.</ta>
            <ta e="T617" id="Seg_4206" s="T610">Доготторо да туогу да диэбэттэр эбит гиниэкэ.</ta>
            <ta e="T626" id="Seg_4207" s="T617">Интернаатка үлэлиир биир дьактар туолкулаппыт (кайтак) кайдак гини таӈнар буолуогун. </ta>
            <ta e="T629" id="Seg_4208" s="T626">Аһыыр кэмнэрэ кэлбит.</ta>
            <ta e="T639" id="Seg_4209" s="T629">Оголору олортообуттар уһу.. уһун багайы остоллор икки диэки өттүлэригэр.</ta>
            <ta e="T645" id="Seg_4210" s="T639">Улакан иһиттэргэ ууруллубуттар минньигэс һыттаак килиэптэр.</ta>
            <ta e="T650" id="Seg_4211" s="T645">Ого аайы кутаттаабыттар балык минин.</ta>
            <ta e="T652" id="Seg_4212" s="T650">Луоскулары биэрэттээбиттэр.</ta>
            <ta e="T659" id="Seg_4213" s="T652">Көрбүт, оголор аһаннар кэтэктэрэ агай һүтэ һытар.</ta>
            <ta e="T670" id="Seg_4214" s="T659">Тойоо аһыы олорон тугу ирэ (эрэ) һанаата кэлбит: мааматын, агатын, Кычатын өйдөөн.</ta>
            <ta e="T682" id="Seg_4215" s="T670">Аһаан бүтээт оголор Волочанка үстүн оонньуу һылдьыбыттар, көрүлүү ньуучча дьиэлэри бары тэргиэни.</ta>
            <ta e="T688" id="Seg_4216" s="T682">Гиннэргэ барыта дьикти, һаӈардыы кэлбит киһилэргэ.</ta>
            <ta e="T697" id="Seg_4217" s="T688">Нөӈүө күн эмиэ улакан оголоок көс кэлбит атын колхуозтан. </ta>
            <ta e="T706" id="Seg_4218" s="T697">Һаӈардыы кэлии оголор киирбиктээрэ бэрт: барыттан кутана, куттанар көрдүктэр.</ta>
            <ta e="T711" id="Seg_4219" s="T706">Гиннэр олус өр итигирдик буолбатактар.</ta>
            <ta e="T722" id="Seg_4220" s="T711">Эмиэ Тойоо, Митээ, Уйбаан көрдүк толлубат миэрэни ылбыттар һаӈа доготторун көрөннөр.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_4221" s="T0">Ihilleːŋ öhü – "Dʼikti tabalar". </ta>
            <ta e="T10" id="Seg_4222" s="T4">Ühüs künüger ogoloːk kös tiːjbit Valačaŋkaga. </ta>
            <ta e="T26" id="Seg_4223" s="T10">Avam rajonun kiːn ologun nʼuːččalara, hakalara, barɨlara komullubuttara keliː ogoloru köröːrü, kukla kördük taŋastaːktarɨ, kihi taptɨ͡aktarɨ. </ta>
            <ta e="T42" id="Seg_4224" s="T26">Olus aːjdaːna hu͡ok, dʼɨlɨgɨr Valačaːnka olok biːr duruk tillibit kördük, taŋara künün beli͡etiːr kördük bu͡olan kaːlbɨt. </ta>
            <ta e="T46" id="Seg_4225" s="T42">Ogolor bu͡ollaktarɨna kihileri körböttör. </ta>
            <ta e="T53" id="Seg_4226" s="T46">Nʼuːčča dʼi͡eler di͡ek agaj (kar-) karaktara. </ta>
            <ta e="T58" id="Seg_4227" s="T53">Ulʼica üstün ataralatan ihenner uːčagɨnan. </ta>
            <ta e="T64" id="Seg_4228" s="T58">Ehelere, Anʼiːsʼim ogonnʼor turbut asku͡ola attɨgar. </ta>
            <ta e="T73" id="Seg_4229" s="T64">Ginini tögürüččü aːjdannaːk armʼija ugučaktarɨnan (ürd-) ürdüleritten ɨstaŋalaːbɨttar. </ta>
            <ta e="T84" id="Seg_4230" s="T73">Asku͡ola attɨgar turallar Pʼotr Kuzʼmʼičʼ, ü͡öreter dʼon hübehittere onno intʼernaːt ülehittere. </ta>
            <ta e="T93" id="Seg_4231" s="T84">Barɨlara ogoloru kaban ɨlannar doroːbolohollor, tu͡ogu ere ginilerge haŋarallar. </ta>
            <ta e="T97" id="Seg_4232" s="T93">Ogolor nʼuːčča tɨlɨn tu͡olkulaːbattar. </ta>
            <ta e="T103" id="Seg_4233" s="T97">Ol ihin keliː kɨra dʼon kepseppetter. </ta>
            <ta e="T108" id="Seg_4234" s="T103">Oduːluːr agaj bu͡olbuttar učʼitʼellerin hɨrajdarɨn. </ta>
            <ta e="T112" id="Seg_4235" s="T108">Giniler olus intiri͡ehi körbütter. </ta>
            <ta e="T122" id="Seg_4236" s="T112">Olus ulakan nʼuːčča dʼi͡eleri, atɨn kihileri, nʼuːččalarɨ, atɨn tɨllaːktarɨ, taŋastaːktarɨ. </ta>
            <ta e="T129" id="Seg_4237" s="T122">Ataktarɨgar bu͡ollagɨna taba tunʼaga kördük atagɨ iːlimmitter. </ta>
            <ta e="T138" id="Seg_4238" s="T129">Er kihiler bastarɨgar maːmalarɨn komu͡ohun kördük bergeheni kete hɨldʼallar. </ta>
            <ta e="T141" id="Seg_4239" s="T138">Dʼaktattara örüːte hu͡oktar. </ta>
            <ta e="T146" id="Seg_4240" s="T141">Astara bɨhɨllɨbɨttar, er kihi kördük. </ta>
            <ta e="T156" id="Seg_4241" s="T146">ɨrbaːkɨlara allara öttüger olus čipčigɨr, ol ihin ginner araččɨ kaːmallar. </ta>
            <ta e="T161" id="Seg_4242" s="T156">Tojoː olus oduːluː turbut dʼaktattarɨ. </ta>
            <ta e="T163" id="Seg_4243" s="T161">Ihiger hanɨːr: </ta>
            <ta e="T172" id="Seg_4244" s="T163">"Badaga, itinnik ɨrbaːkɨnnan hüːre hataːbattar, kɨratɨk ira kaːmallar ebit". </ta>
            <ta e="T179" id="Seg_4245" s="T172">Dogottorugar tugu ere iti tuhunan kepseːri gɨmmɨt. </ta>
            <ta e="T186" id="Seg_4246" s="T179">Anʼagɨn ire arɨjbɨt, tu͡ok ire aːjdaːna bu͡olbut. </ta>
            <ta e="T192" id="Seg_4247" s="T186">Körbüt ol di͡ek, kantan aːjdaːn taksɨbɨt. </ta>
            <ta e="T194" id="Seg_4248" s="T192">Oː, hoːspodi! </ta>
            <ta e="T201" id="Seg_4249" s="T194">Tu͡ok ire kihi körbötök hɨrgalaːga (tallatan) ispit. </ta>
            <ta e="T207" id="Seg_4250" s="T201">Hɨrgata i͡erikej, tabata bu͡ollagɨna ulakana bert. </ta>
            <ta e="T212" id="Seg_4251" s="T207">Olus uhun döjbüːr kördük kuturuktaːk. </ta>
            <ta e="T220" id="Seg_4252" s="T212">Hɨrgalɨːr kihite bu͡ollagɨna hɨraja tüːte bert, köp bɨtɨktaːk. </ta>
            <ta e="T228" id="Seg_4253" s="T220">Tabatɨn kennitten hatɨː kaːman ihen tupput ikki nʼu͡oŋunu. </ta>
            <ta e="T235" id="Seg_4254" s="T228">Tabata kajtak ire kaːmarɨn aːjɨ bahɨn möŋtörör. </ta>
            <ta e="T238" id="Seg_4255" s="T235">Ol aːjɨ hɨrgahɨt: </ta>
            <ta e="T244" id="Seg_4256" s="T238">"Noː, noː", diːr, oloŋkonu ihilliːr kördük. </ta>
            <ta e="T252" id="Seg_4257" s="T244">Küːsteːk bagajɨtɨk anʼagɨnan amsajbɨt, minnʼiges hɨ͡anɨ hiːr kördük. </ta>
            <ta e="T263" id="Seg_4258" s="T252">"Kaja, iti tu͡ok tabata bu͡olar", körö turan ü͡örbüčče Tojoː haŋa allɨbɨt. </ta>
            <ta e="T268" id="Seg_4259" s="T263">Gini agaj onu körön turbataga. </ta>
            <ta e="T272" id="Seg_4260" s="T268">Barɨ ogolor oduːgu bu͡olbuttar. </ta>
            <ta e="T281" id="Seg_4261" s="T272">Beje bejelerin gɨtta kepsete ilikterine emi͡e körbütter haŋa dʼiktini. </ta>
            <ta e="T290" id="Seg_4262" s="T281">Kɨrdʼagas nʼuːčča emeːksine tu͡ok ire kɨra tabakaːttarɨn üːren ispit. </ta>
            <ta e="T298" id="Seg_4263" s="T290">Hɨgɨnnʼak, kultumak tabakaːttar nʼamčɨhak ataktarɨnan hüreleːn ispitter bardʼɨgɨnɨː-bardʼɨgɨnɨː. </ta>
            <ta e="T305" id="Seg_4264" s="T298">Ontulara kötön ihenner ol-bu hir di͡ek butarɨhallar. </ta>
            <ta e="T311" id="Seg_4265" s="T305">"Paː", di͡en ü͡ögüleːbit Tojoː, "hɨgɨnnʼak kɨːllar!" </ta>
            <ta e="T317" id="Seg_4266" s="T311">Ogolor ele isterinen külseller čömüjelerinen köllörö-köllörö. </ta>
            <ta e="T328" id="Seg_4267" s="T317">"Kör, dogoː, munnulara kürej lepsetin kördük iti ire ikki ire üːtteːkter. </ta>
            <ta e="T333" id="Seg_4268" s="T328">Taba itinnik bu͡olbatak, bihi͡ettere– bosku͡ojdar". </ta>
            <ta e="T340" id="Seg_4269" s="T333">Maŋnaj kuttana hɨspɨttar, onton čugahaːbɨttar ötü͡ötük köröːrü. </ta>
            <ta e="T344" id="Seg_4270" s="T340">Tojoː bejetin allaːgɨn kɨ͡ajbatak. </ta>
            <ta e="T351" id="Seg_4271" s="T344">Biːrderiger hüːren kelen ürdütüger ugučaktana gɨna ɨstammɨt. </ta>
            <ta e="T364" id="Seg_4272" s="T351">Kɨːla, hihiger kihini bilen, turbut hiritten ɨstammɨt ürbüčče ele küːhünen köppüt bahɨn kotu. </ta>
            <ta e="T371" id="Seg_4273" s="T364">Uguːčaktan tüspet ogo tutta hataːbɨt tabatɨn tüːtütten. </ta>
            <ta e="T377" id="Seg_4274" s="T371">Kajtak da, kanna da iliːlere iŋnibet. </ta>
            <ta e="T381" id="Seg_4275" s="T377">Tabatɨn tüːlere katɨː kördükter. </ta>
            <ta e="T389" id="Seg_4276" s="T381">Uguːčaga ele tɨːnɨnan ü͡ögüleːbit bahɨn kotu hüːren ispit. </ta>
            <ta e="T396" id="Seg_4277" s="T389">Horok hibinnʼeːler onu körönnör biːr duruk köppütter. </ta>
            <ta e="T401" id="Seg_4278" s="T396">Kennileritten kɨrdʼagas emeːksin hüːre hataːbɨt. </ta>
            <ta e="T405" id="Seg_4279" s="T401">Tojoː tuːgu ire ü͡ögülüː-ü͡ögülüː. </ta>
            <ta e="T411" id="Seg_4280" s="T405">Hüːrerin aːjɨ Tojoː di͡ek tanʼagɨnan dalajar. </ta>
            <ta e="T420" id="Seg_4281" s="T411">Hɨgɨnnʼak ugučaga dʼüdeŋi, bileːleːk uːlaːk oŋkuːčakka "laŋ" gɨna tüspüt. </ta>
            <ta e="T426" id="Seg_4282" s="T420">Tojoː hibinnʼeːtin gɨtta tura hataːn hɨppɨt. </ta>
            <ta e="T433" id="Seg_4283" s="T426">Hɨraja daː tu͡oga da köstübet, bu͡or bu͡olbut. </ta>
            <ta e="T440" id="Seg_4284" s="T433">Tura hataːn elete bu͡olan emeːksin hippit ginini. </ta>
            <ta e="T447" id="Seg_4285" s="T440">Tojoː hɨrajɨn körön külümneːbit, iliːtinen agaj hapsɨjbɨt. </ta>
            <ta e="T452" id="Seg_4286" s="T447">"Hupturuj kantan kelbitiŋ", di͡ebit kördük. </ta>
            <ta e="T462" id="Seg_4287" s="T452">Tojoː tura ekkireːt, ele küːhünen tɨːna hu͡ok dogottorun di͡ek teppit. </ta>
            <ta e="T469" id="Seg_4288" s="T462">"Araː, araččɨ ölö hɨstɨm, araččɨ ölö hɨstɨm!" </ta>
            <ta e="T480" id="Seg_4289" s="T469">Učʼiːtʼellere dʼirʼektar bu͡olan tu͡ok da di͡ekterin berditten ele isterinen küle turbuttar. </ta>
            <ta e="T484" id="Seg_4290" s="T480">Pavʼel Kuzʼmʼičʼ ele hanaːtɨn haŋarbɨt: </ta>
            <ta e="T494" id="Seg_4291" s="T484">"Kajtak da di͡ekke bert olus menik dʼon kelbitter, bɨhɨlaːk". </ta>
            <ta e="T496" id="Seg_4292" s="T494">Dogottorugar di͡ebit: </ta>
            <ta e="T505" id="Seg_4293" s="T496">"Anɨ ginileri intʼernaːkka illiŋ, onton huːnnarɨŋ, astarɨn belemni͡ekteriger di͡eri. </ta>
            <ta e="T510" id="Seg_4294" s="T505">Barɨtɨn kɨčɨrɨ͡aktɨk oŋoruŋ, ogoloru ürpekke". </ta>
            <ta e="T521" id="Seg_4295" s="T510">Ogoloru honno üːren ilpitter, köllörbütter barɨlarɨgar oloror dʼi͡elerin, nʼuːččalɨː "koːmnata" aːttaːgɨ. </ta>
            <ta e="T526" id="Seg_4296" s="T521">Hamaːr, haŋa urbaːkɨlarɨ, ataktarɨ bi͡eretteːbitter. </ta>
            <ta e="T539" id="Seg_4297" s="T526">Barɨ kelbit dʼon astarɨn kɨrpalaːbɨttar, tu͡oktan kɨːs ogolor ɨtɨːllara bert ebit astarɨn ahɨnannar. </ta>
            <ta e="T542" id="Seg_4298" s="T539">Tojoːnɨ emi͡e kɨrpalaːbɨttar. </ta>
            <ta e="T548" id="Seg_4299" s="T542">Taŋnar taŋas bi͡erbitter, keter tunʼaktarɨn tuttarbɨttar. </ta>
            <ta e="T554" id="Seg_4300" s="T548">Kɨrabaːtɨgar keleːt olorbut körülüː tu͡ogu tuttarbɨttarɨn. </ta>
            <ta e="T557" id="Seg_4301" s="T554">Barɨta gini͡eke höp. </ta>
            <ta e="T568" id="Seg_4302" s="T557">Ol daː bu͡ollar togo ginner bi͡erbittere bu͡olu͡oj ikkiliː urbakɨnɨ, ikkiliː hɨ͡alɨjanɨ? </ta>
            <ta e="T574" id="Seg_4303" s="T568">Haŋa taŋastarɨn keter (han-) hanaːbɨt. </ta>
            <ta e="T577" id="Seg_4304" s="T574">Ogoloru huːnnara ilpitter. </ta>
            <ta e="T588" id="Seg_4305" s="T577">Kanna menik dʼon ele hanaːlarɨnan ü͡örüːleːktik külse-halsa "ataj, ataj" dehe olorbuttar. </ta>
            <ta e="T592" id="Seg_4306" s="T588">Tojoː huːnan büteːt taŋnɨbɨt. </ta>
            <ta e="T596" id="Seg_4307" s="T592">Keppit ürüŋ urbaːkɨtɨn, hɨ͡alɨjatɨn. </ta>
            <ta e="T601" id="Seg_4308" s="T596">Horoktorun hapaːstaːn huːlaːbɨt, kojut keteːri. </ta>
            <ta e="T610" id="Seg_4309" s="T601">Etiger keten ürüŋ taŋastarɨn tu͡ok da bu͡olbatak kördük hɨldʼɨbɨt. </ta>
            <ta e="T617" id="Seg_4310" s="T610">Dogottoro da tu͡ogu da di͡ebetter ebit gini͡eke. </ta>
            <ta e="T626" id="Seg_4311" s="T617">Intʼernaːtka üleliːr biːr dʼaktar tu͡olkulappɨt kajtak gini taŋnar bu͡olu͡ogun. </ta>
            <ta e="T629" id="Seg_4312" s="T626">Ahɨːr kemnere kelbit. </ta>
            <ta e="T639" id="Seg_4313" s="T629">Ogoloru olortoːbuttar (uhu-) uhun bagajɨ ostollor ikki di͡eki öttüleriger. </ta>
            <ta e="T645" id="Seg_4314" s="T639">Ulakan ihitterge uːrullubuttar minnʼiges hɨttaːk kili͡epter. </ta>
            <ta e="T650" id="Seg_4315" s="T645">Ogo aːjɨ kutattaːbɨttar balɨk minin. </ta>
            <ta e="T652" id="Seg_4316" s="T650">Lu͡oskalarɨ bi͡eretteːbitter. </ta>
            <ta e="T659" id="Seg_4317" s="T652">Körbüt, ogolor ahaːnnar ketektere agaj hüte hɨtar. </ta>
            <ta e="T670" id="Seg_4318" s="T659">Tojoː ahɨː oloron tugu ire hanaːta kelbit, maːmatɨn, agatɨn, Kɨčatɨn öjdöːn. </ta>
            <ta e="T682" id="Seg_4319" s="T670">Ahaːn büteːt ogolor Valačaːnka üstün oːnnʼuː hɨldʼɨbɨttar, körülüː nʼuːčča dʼi͡eleri barɨ tergi͡eni. </ta>
            <ta e="T688" id="Seg_4320" s="T682">Ginnerge barɨta dʼikti, haŋardɨː kelbit kihilerge. </ta>
            <ta e="T697" id="Seg_4321" s="T688">Nöŋü͡ö kün emi͡e ulakan ogoloːk kös kelbit atɨn kalxoːztan. </ta>
            <ta e="T706" id="Seg_4322" s="T697">Haŋardɨː keliː ogolor kiːrbikteːge bert, barɨttan (kutana) kuttanar kördükter. </ta>
            <ta e="T711" id="Seg_4323" s="T706">Ginner olus ör itigirdik bu͡olbataktar. </ta>
            <ta e="T722" id="Seg_4324" s="T711">Emi͡e Tojoː, Miteː, Ujbaːn kördük tollubat mi͡ereni ɨlbɨttar haŋa dogottorun körönnör. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_4325" s="T0">ihilleː-ŋ</ta>
            <ta e="T2" id="Seg_4326" s="T1">öh-ü</ta>
            <ta e="T3" id="Seg_4327" s="T2">dʼikti</ta>
            <ta e="T4" id="Seg_4328" s="T3">taba-lar</ta>
            <ta e="T5" id="Seg_4329" s="T4">üh-üs</ta>
            <ta e="T6" id="Seg_4330" s="T5">kün-ü-ger</ta>
            <ta e="T7" id="Seg_4331" s="T6">ogo-loːk</ta>
            <ta e="T8" id="Seg_4332" s="T7">kös</ta>
            <ta e="T9" id="Seg_4333" s="T8">tiːj-bit</ta>
            <ta e="T10" id="Seg_4334" s="T9">Valačaŋka-ga</ta>
            <ta e="T11" id="Seg_4335" s="T10">Avam</ta>
            <ta e="T12" id="Seg_4336" s="T11">rajon-u-n</ta>
            <ta e="T13" id="Seg_4337" s="T12">kiːn</ta>
            <ta e="T14" id="Seg_4338" s="T13">olog-u-n</ta>
            <ta e="T15" id="Seg_4339" s="T14">nʼuːčča-lara</ta>
            <ta e="T16" id="Seg_4340" s="T15">haka-lara</ta>
            <ta e="T17" id="Seg_4341" s="T16">barɨ-lara</ta>
            <ta e="T18" id="Seg_4342" s="T17">komu-ll-u-but-tara</ta>
            <ta e="T19" id="Seg_4343" s="T18">kel-iː</ta>
            <ta e="T20" id="Seg_4344" s="T19">ogo-lor-u</ta>
            <ta e="T21" id="Seg_4345" s="T20">kör-öːrü</ta>
            <ta e="T22" id="Seg_4346" s="T21">kukla</ta>
            <ta e="T23" id="Seg_4347" s="T22">kördük</ta>
            <ta e="T24" id="Seg_4348" s="T23">taŋas-taːk-tar-ɨ</ta>
            <ta e="T25" id="Seg_4349" s="T24">kihi</ta>
            <ta e="T26" id="Seg_4350" s="T25">tapt-ɨ͡ak-tar-ɨ</ta>
            <ta e="T27" id="Seg_4351" s="T26">olus</ta>
            <ta e="T28" id="Seg_4352" s="T27">aːjdaːn-a</ta>
            <ta e="T29" id="Seg_4353" s="T28">hu͡ok</ta>
            <ta e="T30" id="Seg_4354" s="T29">dʼɨlɨgɨr</ta>
            <ta e="T31" id="Seg_4355" s="T30">Valačaːnka</ta>
            <ta e="T32" id="Seg_4356" s="T31">olok</ta>
            <ta e="T33" id="Seg_4357" s="T32">biːr</ta>
            <ta e="T34" id="Seg_4358" s="T33">duruk</ta>
            <ta e="T35" id="Seg_4359" s="T34">till-i-bit</ta>
            <ta e="T36" id="Seg_4360" s="T35">kördük</ta>
            <ta e="T37" id="Seg_4361" s="T36">taŋara</ta>
            <ta e="T38" id="Seg_4362" s="T37">kün-ü-n</ta>
            <ta e="T39" id="Seg_4363" s="T38">beli͡e-tiː-r</ta>
            <ta e="T40" id="Seg_4364" s="T39">kördük</ta>
            <ta e="T41" id="Seg_4365" s="T40">bu͡ol-an</ta>
            <ta e="T42" id="Seg_4366" s="T41">kaːl-bɨt</ta>
            <ta e="T43" id="Seg_4367" s="T42">ogo-lor</ta>
            <ta e="T44" id="Seg_4368" s="T43">bu͡ollaktarɨna</ta>
            <ta e="T45" id="Seg_4369" s="T44">kihi-ler-i</ta>
            <ta e="T46" id="Seg_4370" s="T45">kör-böt-tör</ta>
            <ta e="T47" id="Seg_4371" s="T46">nʼuːčča</ta>
            <ta e="T48" id="Seg_4372" s="T47">dʼi͡e-ler</ta>
            <ta e="T49" id="Seg_4373" s="T48">di͡ek</ta>
            <ta e="T50" id="Seg_4374" s="T49">agaj</ta>
            <ta e="T53" id="Seg_4375" s="T52">karak-tara</ta>
            <ta e="T54" id="Seg_4376" s="T53">ulʼica</ta>
            <ta e="T55" id="Seg_4377" s="T54">üstün</ta>
            <ta e="T56" id="Seg_4378" s="T55">ataral-a-t-an</ta>
            <ta e="T57" id="Seg_4379" s="T56">ih-en-ner</ta>
            <ta e="T58" id="Seg_4380" s="T57">uːčag-ɨ-nan</ta>
            <ta e="T59" id="Seg_4381" s="T58">ehe-lere</ta>
            <ta e="T60" id="Seg_4382" s="T59">Anʼiːsʼim</ta>
            <ta e="T61" id="Seg_4383" s="T60">ogonnʼor</ta>
            <ta e="T62" id="Seg_4384" s="T61">tur-but</ta>
            <ta e="T63" id="Seg_4385" s="T62">asku͡ola</ta>
            <ta e="T64" id="Seg_4386" s="T63">attɨ-gar</ta>
            <ta e="T65" id="Seg_4387" s="T64">gini-ni</ta>
            <ta e="T66" id="Seg_4388" s="T65">tögürüččü</ta>
            <ta e="T67" id="Seg_4389" s="T66">aːjdan-naːk</ta>
            <ta e="T68" id="Seg_4390" s="T67">armʼija</ta>
            <ta e="T69" id="Seg_4391" s="T68">ugučak-tarɨ-nan</ta>
            <ta e="T72" id="Seg_4392" s="T71">ürdü-leri-tten</ta>
            <ta e="T73" id="Seg_4393" s="T72">ɨstaŋalaː-bɨt-tar</ta>
            <ta e="T74" id="Seg_4394" s="T73">asku͡ola</ta>
            <ta e="T75" id="Seg_4395" s="T74">attɨ-gar</ta>
            <ta e="T76" id="Seg_4396" s="T75">tur-al-lar</ta>
            <ta e="T79" id="Seg_4397" s="T78">ü͡öret-er</ta>
            <ta e="T80" id="Seg_4398" s="T79">dʼon</ta>
            <ta e="T81" id="Seg_4399" s="T80">hübehit-tere</ta>
            <ta e="T82" id="Seg_4400" s="T81">onno</ta>
            <ta e="T83" id="Seg_4401" s="T82">intʼernaːt</ta>
            <ta e="T84" id="Seg_4402" s="T83">ülehit-ter-e</ta>
            <ta e="T85" id="Seg_4403" s="T84">barɨ-lara</ta>
            <ta e="T86" id="Seg_4404" s="T85">ogo-lor-u</ta>
            <ta e="T87" id="Seg_4405" s="T86">kab-an</ta>
            <ta e="T88" id="Seg_4406" s="T87">ɨl-an-nar</ta>
            <ta e="T89" id="Seg_4407" s="T88">doroːbo-l-o-h-ol-lor</ta>
            <ta e="T90" id="Seg_4408" s="T89">tu͡og-u</ta>
            <ta e="T91" id="Seg_4409" s="T90">ere</ta>
            <ta e="T92" id="Seg_4410" s="T91">giniler-ge</ta>
            <ta e="T93" id="Seg_4411" s="T92">haŋar-al-lar</ta>
            <ta e="T94" id="Seg_4412" s="T93">ogo-lor</ta>
            <ta e="T95" id="Seg_4413" s="T94">nʼuːčča</ta>
            <ta e="T96" id="Seg_4414" s="T95">tɨl-ɨ-n</ta>
            <ta e="T97" id="Seg_4415" s="T96">tu͡olkulaː-bat-tar</ta>
            <ta e="T98" id="Seg_4416" s="T97">ol</ta>
            <ta e="T99" id="Seg_4417" s="T98">ihin</ta>
            <ta e="T100" id="Seg_4418" s="T99">kel-iː</ta>
            <ta e="T101" id="Seg_4419" s="T100">kɨra</ta>
            <ta e="T102" id="Seg_4420" s="T101">dʼon</ta>
            <ta e="T103" id="Seg_4421" s="T102">kepsep-pet-ter</ta>
            <ta e="T104" id="Seg_4422" s="T103">oduːluː-r</ta>
            <ta e="T105" id="Seg_4423" s="T104">agaj</ta>
            <ta e="T106" id="Seg_4424" s="T105">bu͡ol-but-tar</ta>
            <ta e="T107" id="Seg_4425" s="T106">učʼitʼel-leri-n</ta>
            <ta e="T108" id="Seg_4426" s="T107">hɨraj-darɨ-n</ta>
            <ta e="T109" id="Seg_4427" s="T108">giniler</ta>
            <ta e="T110" id="Seg_4428" s="T109">olus</ta>
            <ta e="T111" id="Seg_4429" s="T110">intiri͡eh-i</ta>
            <ta e="T112" id="Seg_4430" s="T111">kör-büt-ter</ta>
            <ta e="T113" id="Seg_4431" s="T112">olus</ta>
            <ta e="T114" id="Seg_4432" s="T113">ulakan</ta>
            <ta e="T115" id="Seg_4433" s="T114">nʼuːčča</ta>
            <ta e="T116" id="Seg_4434" s="T115">dʼi͡e-ler-i</ta>
            <ta e="T117" id="Seg_4435" s="T116">atɨn</ta>
            <ta e="T118" id="Seg_4436" s="T117">kihi-ler-i</ta>
            <ta e="T119" id="Seg_4437" s="T118">nʼuːčča-lar-ɨ</ta>
            <ta e="T120" id="Seg_4438" s="T119">atɨn</ta>
            <ta e="T121" id="Seg_4439" s="T120">tɨl-laːk-tar-ɨ</ta>
            <ta e="T122" id="Seg_4440" s="T121">taŋas-taːk-tar-ɨ</ta>
            <ta e="T123" id="Seg_4441" s="T122">atak-tarɨ-gar</ta>
            <ta e="T124" id="Seg_4442" s="T123">bu͡ollagɨna</ta>
            <ta e="T125" id="Seg_4443" s="T124">taba</ta>
            <ta e="T126" id="Seg_4444" s="T125">tunʼag-a</ta>
            <ta e="T127" id="Seg_4445" s="T126">kördük</ta>
            <ta e="T128" id="Seg_4446" s="T127">atag-ɨ</ta>
            <ta e="T129" id="Seg_4447" s="T128">iːl-i-m-mit-ter</ta>
            <ta e="T130" id="Seg_4448" s="T129">er</ta>
            <ta e="T131" id="Seg_4449" s="T130">kihi-ler</ta>
            <ta e="T132" id="Seg_4450" s="T131">bas-tarɨ-gar</ta>
            <ta e="T133" id="Seg_4451" s="T132">maːma-larɨ-n</ta>
            <ta e="T134" id="Seg_4452" s="T133">komu͡oh-u-n</ta>
            <ta e="T135" id="Seg_4453" s="T134">kördük</ta>
            <ta e="T136" id="Seg_4454" s="T135">bergehe-ni</ta>
            <ta e="T137" id="Seg_4455" s="T136">ket-e</ta>
            <ta e="T138" id="Seg_4456" s="T137">hɨldʼ-al-lar</ta>
            <ta e="T139" id="Seg_4457" s="T138">dʼaktat-tar-a</ta>
            <ta e="T140" id="Seg_4458" s="T139">örüː-te</ta>
            <ta e="T141" id="Seg_4459" s="T140">hu͡ok-tar</ta>
            <ta e="T142" id="Seg_4460" s="T141">as-tara</ta>
            <ta e="T143" id="Seg_4461" s="T142">bɨh-ɨ-ll-ɨ-bɨt-tar</ta>
            <ta e="T144" id="Seg_4462" s="T143">er</ta>
            <ta e="T145" id="Seg_4463" s="T144">kihi</ta>
            <ta e="T146" id="Seg_4464" s="T145">kördük</ta>
            <ta e="T147" id="Seg_4465" s="T146">ɨrbaːkɨ-lara</ta>
            <ta e="T148" id="Seg_4466" s="T147">allara</ta>
            <ta e="T149" id="Seg_4467" s="T148">ött-ü-ger</ta>
            <ta e="T150" id="Seg_4468" s="T149">olus</ta>
            <ta e="T151" id="Seg_4469" s="T150">čipčigɨr</ta>
            <ta e="T152" id="Seg_4470" s="T151">ol</ta>
            <ta e="T153" id="Seg_4471" s="T152">ihin</ta>
            <ta e="T154" id="Seg_4472" s="T153">ginner</ta>
            <ta e="T155" id="Seg_4473" s="T154">araččɨ</ta>
            <ta e="T156" id="Seg_4474" s="T155">kaːm-al-lar</ta>
            <ta e="T157" id="Seg_4475" s="T156">Tojoː</ta>
            <ta e="T158" id="Seg_4476" s="T157">olus</ta>
            <ta e="T159" id="Seg_4477" s="T158">oduːl-uː</ta>
            <ta e="T160" id="Seg_4478" s="T159">tur-but</ta>
            <ta e="T161" id="Seg_4479" s="T160">dʼaktat-tar-ɨ</ta>
            <ta e="T162" id="Seg_4480" s="T161">ih-i-ger</ta>
            <ta e="T163" id="Seg_4481" s="T162">hanɨː-r</ta>
            <ta e="T164" id="Seg_4482" s="T163">badaga</ta>
            <ta e="T165" id="Seg_4483" s="T164">itinnik</ta>
            <ta e="T166" id="Seg_4484" s="T165">ɨrbaːkɨ-nnan</ta>
            <ta e="T167" id="Seg_4485" s="T166">hüːr-e</ta>
            <ta e="T168" id="Seg_4486" s="T167">hataː-bat-tar</ta>
            <ta e="T169" id="Seg_4487" s="T168">kɨratɨk</ta>
            <ta e="T170" id="Seg_4488" s="T169">ira</ta>
            <ta e="T171" id="Seg_4489" s="T170">kaːm-al-lar</ta>
            <ta e="T172" id="Seg_4490" s="T171">e-bit</ta>
            <ta e="T173" id="Seg_4491" s="T172">dogot-tor-u-gar</ta>
            <ta e="T174" id="Seg_4492" s="T173">tug-u</ta>
            <ta e="T175" id="Seg_4493" s="T174">ere</ta>
            <ta e="T176" id="Seg_4494" s="T175">iti</ta>
            <ta e="T177" id="Seg_4495" s="T176">tuh-u-nan</ta>
            <ta e="T178" id="Seg_4496" s="T177">keps-eːri</ta>
            <ta e="T179" id="Seg_4497" s="T178">gɨm-mɨt</ta>
            <ta e="T180" id="Seg_4498" s="T179">anʼag-ɨ-n</ta>
            <ta e="T181" id="Seg_4499" s="T180">ire</ta>
            <ta e="T182" id="Seg_4500" s="T181">arɨj-bɨt</ta>
            <ta e="T183" id="Seg_4501" s="T182">tu͡ok</ta>
            <ta e="T184" id="Seg_4502" s="T183">ire</ta>
            <ta e="T185" id="Seg_4503" s="T184">aːjdaːn-a</ta>
            <ta e="T186" id="Seg_4504" s="T185">bu͡ol-but</ta>
            <ta e="T187" id="Seg_4505" s="T186">kör-büt</ta>
            <ta e="T188" id="Seg_4506" s="T187">ol</ta>
            <ta e="T189" id="Seg_4507" s="T188">di͡ek</ta>
            <ta e="T190" id="Seg_4508" s="T189">kantan</ta>
            <ta e="T191" id="Seg_4509" s="T190">aːjdaːn</ta>
            <ta e="T192" id="Seg_4510" s="T191">taks-ɨ-bɨt</ta>
            <ta e="T195" id="Seg_4511" s="T194">tu͡ok</ta>
            <ta e="T196" id="Seg_4512" s="T195">ire</ta>
            <ta e="T197" id="Seg_4513" s="T196">kihi</ta>
            <ta e="T198" id="Seg_4514" s="T197">kör-bötök</ta>
            <ta e="T199" id="Seg_4515" s="T198">hɨrga-laːg-a</ta>
            <ta e="T200" id="Seg_4516" s="T199">tal-l-a-t-an</ta>
            <ta e="T201" id="Seg_4517" s="T200">is-pit</ta>
            <ta e="T202" id="Seg_4518" s="T201">hɨrga-ta</ta>
            <ta e="T203" id="Seg_4519" s="T202">i͡erikej</ta>
            <ta e="T204" id="Seg_4520" s="T203">taba-ta</ta>
            <ta e="T205" id="Seg_4521" s="T204">bu͡ollagɨna</ta>
            <ta e="T206" id="Seg_4522" s="T205">ulakan-a</ta>
            <ta e="T207" id="Seg_4523" s="T206">bert</ta>
            <ta e="T208" id="Seg_4524" s="T207">olus</ta>
            <ta e="T209" id="Seg_4525" s="T208">uhun</ta>
            <ta e="T210" id="Seg_4526" s="T209">döjbüː-r</ta>
            <ta e="T211" id="Seg_4527" s="T210">kördük</ta>
            <ta e="T212" id="Seg_4528" s="T211">kuturuk-taːk</ta>
            <ta e="T213" id="Seg_4529" s="T212">hɨrga-lɨː-r</ta>
            <ta e="T214" id="Seg_4530" s="T213">kihi-te</ta>
            <ta e="T215" id="Seg_4531" s="T214">bu͡ollagɨna</ta>
            <ta e="T216" id="Seg_4532" s="T215">hɨraj-a</ta>
            <ta e="T217" id="Seg_4533" s="T216">tüː-te</ta>
            <ta e="T218" id="Seg_4534" s="T217">bert</ta>
            <ta e="T219" id="Seg_4535" s="T218">köp</ta>
            <ta e="T220" id="Seg_4536" s="T219">bɨtɨk-taːk</ta>
            <ta e="T221" id="Seg_4537" s="T220">taba-tɨ-n</ta>
            <ta e="T222" id="Seg_4538" s="T221">kenn-i-tten</ta>
            <ta e="T223" id="Seg_4539" s="T222">hatɨː</ta>
            <ta e="T224" id="Seg_4540" s="T223">kaːm-an</ta>
            <ta e="T225" id="Seg_4541" s="T224">ih-en</ta>
            <ta e="T226" id="Seg_4542" s="T225">tup-put</ta>
            <ta e="T227" id="Seg_4543" s="T226">ikki</ta>
            <ta e="T228" id="Seg_4544" s="T227">nʼu͡oŋu-nu</ta>
            <ta e="T229" id="Seg_4545" s="T228">taba-ta</ta>
            <ta e="T230" id="Seg_4546" s="T229">kajtak</ta>
            <ta e="T231" id="Seg_4547" s="T230">ire</ta>
            <ta e="T232" id="Seg_4548" s="T231">kaːm-ar-ɨ-n</ta>
            <ta e="T233" id="Seg_4549" s="T232">aːjɨ</ta>
            <ta e="T234" id="Seg_4550" s="T233">bah-ɨ-n</ta>
            <ta e="T235" id="Seg_4551" s="T234">möŋ-tör-ör</ta>
            <ta e="T236" id="Seg_4552" s="T235">ol</ta>
            <ta e="T237" id="Seg_4553" s="T236">aːjɨ</ta>
            <ta e="T238" id="Seg_4554" s="T237">hɨrga-hɨt</ta>
            <ta e="T239" id="Seg_4555" s="T238">noː</ta>
            <ta e="T240" id="Seg_4556" s="T239">noː</ta>
            <ta e="T241" id="Seg_4557" s="T240">diː-r</ta>
            <ta e="T242" id="Seg_4558" s="T241">oloŋko-nu</ta>
            <ta e="T243" id="Seg_4559" s="T242">ihilliː-r</ta>
            <ta e="T244" id="Seg_4560" s="T243">kördük</ta>
            <ta e="T245" id="Seg_4561" s="T244">küːsteːk</ta>
            <ta e="T246" id="Seg_4562" s="T245">bagajɨ-tɨk</ta>
            <ta e="T247" id="Seg_4563" s="T246">anʼag-ɨ-nan</ta>
            <ta e="T248" id="Seg_4564" s="T247">amsaj-bɨt</ta>
            <ta e="T249" id="Seg_4565" s="T248">minnʼiges</ta>
            <ta e="T250" id="Seg_4566" s="T249">hɨ͡a-nɨ</ta>
            <ta e="T251" id="Seg_4567" s="T250">hiː-r</ta>
            <ta e="T252" id="Seg_4568" s="T251">kördük</ta>
            <ta e="T253" id="Seg_4569" s="T252">kaja</ta>
            <ta e="T254" id="Seg_4570" s="T253">iti</ta>
            <ta e="T255" id="Seg_4571" s="T254">tu͡ok</ta>
            <ta e="T256" id="Seg_4572" s="T255">taba-ta</ta>
            <ta e="T257" id="Seg_4573" s="T256">bu͡ol-ar</ta>
            <ta e="T258" id="Seg_4574" s="T257">kör-ö</ta>
            <ta e="T259" id="Seg_4575" s="T258">tur-an</ta>
            <ta e="T260" id="Seg_4576" s="T259">ü͡ör-büčče</ta>
            <ta e="T261" id="Seg_4577" s="T260">Tojoː</ta>
            <ta e="T262" id="Seg_4578" s="T261">haŋa</ta>
            <ta e="T263" id="Seg_4579" s="T262">all-ɨ-bɨt</ta>
            <ta e="T264" id="Seg_4580" s="T263">gini</ta>
            <ta e="T265" id="Seg_4581" s="T264">agaj</ta>
            <ta e="T266" id="Seg_4582" s="T265">o-nu</ta>
            <ta e="T267" id="Seg_4583" s="T266">kör-ön</ta>
            <ta e="T268" id="Seg_4584" s="T267">tur-batag-a</ta>
            <ta e="T269" id="Seg_4585" s="T268">barɨ</ta>
            <ta e="T270" id="Seg_4586" s="T269">ogo-lor</ta>
            <ta e="T271" id="Seg_4587" s="T270">oduː-gu</ta>
            <ta e="T272" id="Seg_4588" s="T271">bu͡ol-but-tar</ta>
            <ta e="T273" id="Seg_4589" s="T272">beje</ta>
            <ta e="T274" id="Seg_4590" s="T273">beje-leri-n</ta>
            <ta e="T275" id="Seg_4591" s="T274">gɨtta</ta>
            <ta e="T276" id="Seg_4592" s="T275">kepset-e</ta>
            <ta e="T277" id="Seg_4593" s="T276">ilik-terine</ta>
            <ta e="T278" id="Seg_4594" s="T277">emi͡e</ta>
            <ta e="T279" id="Seg_4595" s="T278">kör-büt-ter</ta>
            <ta e="T280" id="Seg_4596" s="T279">haŋa</ta>
            <ta e="T281" id="Seg_4597" s="T280">dʼikti-ni</ta>
            <ta e="T282" id="Seg_4598" s="T281">kɨrdʼagas</ta>
            <ta e="T283" id="Seg_4599" s="T282">nʼuːčča</ta>
            <ta e="T284" id="Seg_4600" s="T283">emeːksin-e</ta>
            <ta e="T285" id="Seg_4601" s="T284">tu͡ok</ta>
            <ta e="T286" id="Seg_4602" s="T285">ire</ta>
            <ta e="T287" id="Seg_4603" s="T286">kɨra</ta>
            <ta e="T288" id="Seg_4604" s="T287">taba-kaːt-tar-ɨ-n</ta>
            <ta e="T289" id="Seg_4605" s="T288">üːr-en</ta>
            <ta e="T290" id="Seg_4606" s="T289">is-pit</ta>
            <ta e="T291" id="Seg_4607" s="T290">hɨgɨnnʼak</ta>
            <ta e="T292" id="Seg_4608" s="T291">kultumak</ta>
            <ta e="T293" id="Seg_4609" s="T292">taba-kaːt-tar</ta>
            <ta e="T294" id="Seg_4610" s="T293">nʼamčɨhak</ta>
            <ta e="T295" id="Seg_4611" s="T294">atak-tarɨ-nan</ta>
            <ta e="T296" id="Seg_4612" s="T295">hür-eleː-n</ta>
            <ta e="T297" id="Seg_4613" s="T296">is-pit-ter</ta>
            <ta e="T298" id="Seg_4614" s="T297">bardʼɨgɨn-ɨː-bardʼɨgɨn-ɨː</ta>
            <ta e="T299" id="Seg_4615" s="T298">on-tu-lara</ta>
            <ta e="T300" id="Seg_4616" s="T299">köt-ön</ta>
            <ta e="T301" id="Seg_4617" s="T300">ih-en-ner</ta>
            <ta e="T302" id="Seg_4618" s="T301">ol-bu</ta>
            <ta e="T303" id="Seg_4619" s="T302">hir</ta>
            <ta e="T304" id="Seg_4620" s="T303">di͡ek</ta>
            <ta e="T305" id="Seg_4621" s="T304">butarɨh-al-lar</ta>
            <ta e="T306" id="Seg_4622" s="T305">paː</ta>
            <ta e="T307" id="Seg_4623" s="T306">di͡e-n</ta>
            <ta e="T308" id="Seg_4624" s="T307">ü͡ögüleː-bit</ta>
            <ta e="T309" id="Seg_4625" s="T308">Tojoː</ta>
            <ta e="T310" id="Seg_4626" s="T309">hɨgɨnnʼak</ta>
            <ta e="T311" id="Seg_4627" s="T310">kɨːl-lar</ta>
            <ta e="T312" id="Seg_4628" s="T311">ogo-lor</ta>
            <ta e="T313" id="Seg_4629" s="T312">ele</ta>
            <ta e="T314" id="Seg_4630" s="T313">is-teri-nen</ta>
            <ta e="T315" id="Seg_4631" s="T314">kül-s-el-ler</ta>
            <ta e="T316" id="Seg_4632" s="T315">čömüje-leri-nen</ta>
            <ta e="T317" id="Seg_4633" s="T316">köl-lör-ö-köl-lör-ö</ta>
            <ta e="T318" id="Seg_4634" s="T317">kör</ta>
            <ta e="T319" id="Seg_4635" s="T318">dogoː</ta>
            <ta e="T320" id="Seg_4636" s="T319">munnu-lara</ta>
            <ta e="T321" id="Seg_4637" s="T320">kürej</ta>
            <ta e="T322" id="Seg_4638" s="T321">lepse-ti-n</ta>
            <ta e="T323" id="Seg_4639" s="T322">kördük</ta>
            <ta e="T324" id="Seg_4640" s="T323">iti</ta>
            <ta e="T325" id="Seg_4641" s="T324">ire</ta>
            <ta e="T326" id="Seg_4642" s="T325">ikki</ta>
            <ta e="T327" id="Seg_4643" s="T326">ire</ta>
            <ta e="T328" id="Seg_4644" s="T327">üːt-teːk-ter</ta>
            <ta e="T329" id="Seg_4645" s="T328">taba</ta>
            <ta e="T330" id="Seg_4646" s="T329">itinnik</ta>
            <ta e="T331" id="Seg_4647" s="T330">bu͡ol-batak</ta>
            <ta e="T332" id="Seg_4648" s="T331">bihi͡ettere</ta>
            <ta e="T333" id="Seg_4649" s="T332">bosku͡oj-dar</ta>
            <ta e="T334" id="Seg_4650" s="T333">maŋnaj</ta>
            <ta e="T335" id="Seg_4651" s="T334">kuttan-a</ta>
            <ta e="T336" id="Seg_4652" s="T335">hɨs-pɨt-tar</ta>
            <ta e="T337" id="Seg_4653" s="T336">onton</ta>
            <ta e="T338" id="Seg_4654" s="T337">čugahaː-bɨt-tar</ta>
            <ta e="T339" id="Seg_4655" s="T338">ötü͡ö-tük</ta>
            <ta e="T340" id="Seg_4656" s="T339">kör-öːrü</ta>
            <ta e="T341" id="Seg_4657" s="T340">Tojoː</ta>
            <ta e="T342" id="Seg_4658" s="T341">beje-ti-n</ta>
            <ta e="T343" id="Seg_4659" s="T342">allaːg-ɨ-n</ta>
            <ta e="T344" id="Seg_4660" s="T343">kɨ͡aj-batak</ta>
            <ta e="T345" id="Seg_4661" s="T344">biːr-deri-ger</ta>
            <ta e="T346" id="Seg_4662" s="T345">hüːr-en</ta>
            <ta e="T347" id="Seg_4663" s="T346">kel-en</ta>
            <ta e="T348" id="Seg_4664" s="T347">ürdü-tü-ger</ta>
            <ta e="T349" id="Seg_4665" s="T348">ugučak-tan-a</ta>
            <ta e="T350" id="Seg_4666" s="T349">gɨn-a</ta>
            <ta e="T351" id="Seg_4667" s="T350">ɨstam-mɨt</ta>
            <ta e="T352" id="Seg_4668" s="T351">kɨːl-a</ta>
            <ta e="T353" id="Seg_4669" s="T352">hih-i-ger</ta>
            <ta e="T354" id="Seg_4670" s="T353">kihi-ni</ta>
            <ta e="T355" id="Seg_4671" s="T354">bil-en</ta>
            <ta e="T356" id="Seg_4672" s="T355">tur-but</ta>
            <ta e="T357" id="Seg_4673" s="T356">hir-i-tten</ta>
            <ta e="T358" id="Seg_4674" s="T357">ɨstam-mɨt</ta>
            <ta e="T359" id="Seg_4675" s="T358">ür-büčče</ta>
            <ta e="T360" id="Seg_4676" s="T359">ele</ta>
            <ta e="T361" id="Seg_4677" s="T360">küːh-ü-nen</ta>
            <ta e="T362" id="Seg_4678" s="T361">köp-püt</ta>
            <ta e="T363" id="Seg_4679" s="T362">bah-ɨ-n</ta>
            <ta e="T364" id="Seg_4680" s="T363">kotu</ta>
            <ta e="T365" id="Seg_4681" s="T364">uguːčak-tan</ta>
            <ta e="T366" id="Seg_4682" s="T365">tüs-pet</ta>
            <ta e="T367" id="Seg_4683" s="T366">ogo</ta>
            <ta e="T368" id="Seg_4684" s="T367">tutt-a</ta>
            <ta e="T369" id="Seg_4685" s="T368">hataː-bɨt</ta>
            <ta e="T370" id="Seg_4686" s="T369">taba-tɨ-n</ta>
            <ta e="T371" id="Seg_4687" s="T370">tüː-tü-tten</ta>
            <ta e="T372" id="Seg_4688" s="T371">kajtak</ta>
            <ta e="T373" id="Seg_4689" s="T372">da</ta>
            <ta e="T374" id="Seg_4690" s="T373">kanna</ta>
            <ta e="T375" id="Seg_4691" s="T374">da</ta>
            <ta e="T376" id="Seg_4692" s="T375">iliː-ler-e</ta>
            <ta e="T377" id="Seg_4693" s="T376">iŋn-i-bet</ta>
            <ta e="T378" id="Seg_4694" s="T377">taba-tɨ-n</ta>
            <ta e="T379" id="Seg_4695" s="T378">tüː-ler-e</ta>
            <ta e="T380" id="Seg_4696" s="T379">katɨː</ta>
            <ta e="T381" id="Seg_4697" s="T380">kördük-ter</ta>
            <ta e="T382" id="Seg_4698" s="T381">uguːčag-a</ta>
            <ta e="T383" id="Seg_4699" s="T382">ele</ta>
            <ta e="T384" id="Seg_4700" s="T383">tɨːn-ɨ-nan</ta>
            <ta e="T385" id="Seg_4701" s="T384">ü͡ögüleː-bit</ta>
            <ta e="T386" id="Seg_4702" s="T385">bah-ɨ-n</ta>
            <ta e="T387" id="Seg_4703" s="T386">kotu</ta>
            <ta e="T388" id="Seg_4704" s="T387">hüːr-en</ta>
            <ta e="T389" id="Seg_4705" s="T388">is-pit</ta>
            <ta e="T390" id="Seg_4706" s="T389">horok</ta>
            <ta e="T391" id="Seg_4707" s="T390">hibinnʼeː-ler</ta>
            <ta e="T392" id="Seg_4708" s="T391">o-nu</ta>
            <ta e="T393" id="Seg_4709" s="T392">kör-ön-nör</ta>
            <ta e="T394" id="Seg_4710" s="T393">biːr</ta>
            <ta e="T395" id="Seg_4711" s="T394">duruk</ta>
            <ta e="T396" id="Seg_4712" s="T395">köp-püt-ter</ta>
            <ta e="T397" id="Seg_4713" s="T396">kenni-leri-tten</ta>
            <ta e="T398" id="Seg_4714" s="T397">kɨrdʼagas</ta>
            <ta e="T399" id="Seg_4715" s="T398">emeːksin</ta>
            <ta e="T400" id="Seg_4716" s="T399">hüːr-e</ta>
            <ta e="T401" id="Seg_4717" s="T400">hataː-bɨt</ta>
            <ta e="T402" id="Seg_4718" s="T401">Tojoː</ta>
            <ta e="T403" id="Seg_4719" s="T402">tuːg-u</ta>
            <ta e="T404" id="Seg_4720" s="T403">ire</ta>
            <ta e="T405" id="Seg_4721" s="T404">ü͡ögül-üː-ü͡ögül-üː</ta>
            <ta e="T406" id="Seg_4722" s="T405">hüːr-er-i-n</ta>
            <ta e="T407" id="Seg_4723" s="T406">aːjɨ</ta>
            <ta e="T408" id="Seg_4724" s="T407">Tojoː</ta>
            <ta e="T409" id="Seg_4725" s="T408">di͡ek</ta>
            <ta e="T410" id="Seg_4726" s="T409">tanʼag-ɨ-nan</ta>
            <ta e="T411" id="Seg_4727" s="T410">dalaj-ar</ta>
            <ta e="T412" id="Seg_4728" s="T411">hɨgɨnnʼak</ta>
            <ta e="T413" id="Seg_4729" s="T412">ugučag-a</ta>
            <ta e="T414" id="Seg_4730" s="T413">dʼüdeŋi</ta>
            <ta e="T415" id="Seg_4731" s="T414">bileː-leːk</ta>
            <ta e="T416" id="Seg_4732" s="T415">uː-laːk</ta>
            <ta e="T417" id="Seg_4733" s="T416">oŋkuːčak-ka</ta>
            <ta e="T418" id="Seg_4734" s="T417">laŋ</ta>
            <ta e="T419" id="Seg_4735" s="T418">gɨn-a</ta>
            <ta e="T420" id="Seg_4736" s="T419">tüs-püt</ta>
            <ta e="T421" id="Seg_4737" s="T420">Tojoː</ta>
            <ta e="T422" id="Seg_4738" s="T421">hibinnʼeː-ti-n</ta>
            <ta e="T423" id="Seg_4739" s="T422">gɨtta</ta>
            <ta e="T424" id="Seg_4740" s="T423">tur-a</ta>
            <ta e="T425" id="Seg_4741" s="T424">hataː-n</ta>
            <ta e="T426" id="Seg_4742" s="T425">hɨp-pɨt</ta>
            <ta e="T427" id="Seg_4743" s="T426">hɨraj-a</ta>
            <ta e="T428" id="Seg_4744" s="T427">daː</ta>
            <ta e="T429" id="Seg_4745" s="T428">tu͡og-a</ta>
            <ta e="T430" id="Seg_4746" s="T429">da</ta>
            <ta e="T431" id="Seg_4747" s="T430">köst-ü-bet</ta>
            <ta e="T432" id="Seg_4748" s="T431">bu͡or</ta>
            <ta e="T433" id="Seg_4749" s="T432">bu͡ol-but</ta>
            <ta e="T434" id="Seg_4750" s="T433">tur-a</ta>
            <ta e="T435" id="Seg_4751" s="T434">hataː-n</ta>
            <ta e="T436" id="Seg_4752" s="T435">ele-te</ta>
            <ta e="T437" id="Seg_4753" s="T436">bu͡ol-an</ta>
            <ta e="T438" id="Seg_4754" s="T437">emeːksin</ta>
            <ta e="T439" id="Seg_4755" s="T438">hip-pit</ta>
            <ta e="T440" id="Seg_4756" s="T439">gini-ni</ta>
            <ta e="T441" id="Seg_4757" s="T440">Tojoː</ta>
            <ta e="T442" id="Seg_4758" s="T441">hɨraj-ɨ-n</ta>
            <ta e="T443" id="Seg_4759" s="T442">kör-ön</ta>
            <ta e="T444" id="Seg_4760" s="T443">külümneː-bit</ta>
            <ta e="T445" id="Seg_4761" s="T444">iliː-ti-nen</ta>
            <ta e="T446" id="Seg_4762" s="T445">agaj</ta>
            <ta e="T447" id="Seg_4763" s="T446">hapsɨj-bɨt</ta>
            <ta e="T448" id="Seg_4764" s="T447">hupturuj</ta>
            <ta e="T449" id="Seg_4765" s="T448">kantan</ta>
            <ta e="T450" id="Seg_4766" s="T449">kel-bit-i-ŋ</ta>
            <ta e="T451" id="Seg_4767" s="T450">di͡e-bit</ta>
            <ta e="T452" id="Seg_4768" s="T451">kördük</ta>
            <ta e="T453" id="Seg_4769" s="T452">Tojoː</ta>
            <ta e="T454" id="Seg_4770" s="T453">tur-a</ta>
            <ta e="T455" id="Seg_4771" s="T454">ekkir-eːt</ta>
            <ta e="T456" id="Seg_4772" s="T455">ele</ta>
            <ta e="T457" id="Seg_4773" s="T456">küːh-ü-nen</ta>
            <ta e="T458" id="Seg_4774" s="T457">tɨːn-a</ta>
            <ta e="T459" id="Seg_4775" s="T458">hu͡ok</ta>
            <ta e="T460" id="Seg_4776" s="T459">dogot-tor-u-n</ta>
            <ta e="T461" id="Seg_4777" s="T460">di͡ek</ta>
            <ta e="T462" id="Seg_4778" s="T461">tep-pit</ta>
            <ta e="T463" id="Seg_4779" s="T462">araː</ta>
            <ta e="T464" id="Seg_4780" s="T463">araččɨ</ta>
            <ta e="T465" id="Seg_4781" s="T464">öl-ö</ta>
            <ta e="T466" id="Seg_4782" s="T465">hɨs-tɨ-m</ta>
            <ta e="T467" id="Seg_4783" s="T466">araččɨ</ta>
            <ta e="T468" id="Seg_4784" s="T467">öl-ö</ta>
            <ta e="T469" id="Seg_4785" s="T468">hɨs-tɨ-m</ta>
            <ta e="T470" id="Seg_4786" s="T469">učʼiːtʼel-lere</ta>
            <ta e="T471" id="Seg_4787" s="T470">dʼirʼektar</ta>
            <ta e="T472" id="Seg_4788" s="T471">bu͡ol-an</ta>
            <ta e="T473" id="Seg_4789" s="T472">tu͡ok</ta>
            <ta e="T474" id="Seg_4790" s="T473">da</ta>
            <ta e="T475" id="Seg_4791" s="T474">d-i͡ek-teri-n</ta>
            <ta e="T476" id="Seg_4792" s="T475">berd-i-tten</ta>
            <ta e="T477" id="Seg_4793" s="T476">ele</ta>
            <ta e="T478" id="Seg_4794" s="T477">is-teri-nen</ta>
            <ta e="T479" id="Seg_4795" s="T478">kül-e</ta>
            <ta e="T480" id="Seg_4796" s="T479">tur-but-tar</ta>
            <ta e="T482" id="Seg_4797" s="T481">ele</ta>
            <ta e="T483" id="Seg_4798" s="T482">hanaː-tɨ-n</ta>
            <ta e="T484" id="Seg_4799" s="T483">haŋar-bɨt</ta>
            <ta e="T485" id="Seg_4800" s="T484">kajtak</ta>
            <ta e="T486" id="Seg_4801" s="T485">da</ta>
            <ta e="T487" id="Seg_4802" s="T486">d-i͡ek-ke</ta>
            <ta e="T488" id="Seg_4803" s="T487">bert</ta>
            <ta e="T489" id="Seg_4804" s="T488">olus</ta>
            <ta e="T490" id="Seg_4805" s="T489">menik</ta>
            <ta e="T491" id="Seg_4806" s="T490">dʼon</ta>
            <ta e="T492" id="Seg_4807" s="T491">kel-bit-ter</ta>
            <ta e="T494" id="Seg_4808" s="T492">bɨhɨlaːk</ta>
            <ta e="T495" id="Seg_4809" s="T494">dogot-tor-u-gar</ta>
            <ta e="T496" id="Seg_4810" s="T495">di͡e-bit</ta>
            <ta e="T497" id="Seg_4811" s="T496">anɨ</ta>
            <ta e="T498" id="Seg_4812" s="T497">giniler-i</ta>
            <ta e="T499" id="Seg_4813" s="T498">intʼernaːk-ka</ta>
            <ta e="T500" id="Seg_4814" s="T499">ill-i-ŋ</ta>
            <ta e="T501" id="Seg_4815" s="T500">onton</ta>
            <ta e="T502" id="Seg_4816" s="T501">huːn-nar-ɨ-ŋ</ta>
            <ta e="T503" id="Seg_4817" s="T502">as-tarɨ-n</ta>
            <ta e="T504" id="Seg_4818" s="T503">belemn-i͡ek-teri-ger</ta>
            <ta e="T505" id="Seg_4819" s="T504">di͡eri</ta>
            <ta e="T506" id="Seg_4820" s="T505">barɨ-tɨ-n</ta>
            <ta e="T507" id="Seg_4821" s="T506">kɨčɨrɨ͡ak-tɨk</ta>
            <ta e="T508" id="Seg_4822" s="T507">oŋor-u-ŋ</ta>
            <ta e="T509" id="Seg_4823" s="T508">ogo-lor-u</ta>
            <ta e="T510" id="Seg_4824" s="T509">ür-pekke</ta>
            <ta e="T511" id="Seg_4825" s="T510">ogo-lor-u</ta>
            <ta e="T512" id="Seg_4826" s="T511">honno</ta>
            <ta e="T513" id="Seg_4827" s="T512">üːr-en</ta>
            <ta e="T514" id="Seg_4828" s="T513">il-pit-ter</ta>
            <ta e="T515" id="Seg_4829" s="T514">köllör-büt-ter</ta>
            <ta e="T516" id="Seg_4830" s="T515">barɨ-larɨ-gar</ta>
            <ta e="T517" id="Seg_4831" s="T516">olor-or</ta>
            <ta e="T518" id="Seg_4832" s="T517">dʼi͡e-leri-n</ta>
            <ta e="T519" id="Seg_4833" s="T518">nʼuːčča-lɨː</ta>
            <ta e="T521" id="Seg_4834" s="T520">aːt-taːgɨ</ta>
            <ta e="T522" id="Seg_4835" s="T521">hamaː-r</ta>
            <ta e="T523" id="Seg_4836" s="T522">haŋa</ta>
            <ta e="T524" id="Seg_4837" s="T523">urbaːkɨ-lar-ɨ</ta>
            <ta e="T525" id="Seg_4838" s="T524">atak-tar-ɨ</ta>
            <ta e="T526" id="Seg_4839" s="T525">bi͡er-etteː-bit-ter</ta>
            <ta e="T527" id="Seg_4840" s="T526">barɨ</ta>
            <ta e="T528" id="Seg_4841" s="T527">kel-bit</ta>
            <ta e="T529" id="Seg_4842" s="T528">dʼon</ta>
            <ta e="T530" id="Seg_4843" s="T529">as-tar-ɨ-n</ta>
            <ta e="T531" id="Seg_4844" s="T530">kɨrpalaː-bɨt-tar</ta>
            <ta e="T532" id="Seg_4845" s="T531">tu͡ok-tan</ta>
            <ta e="T533" id="Seg_4846" s="T532">kɨːs</ta>
            <ta e="T534" id="Seg_4847" s="T533">ogo-lor</ta>
            <ta e="T535" id="Seg_4848" s="T534">ɨtɨː-l-lara</ta>
            <ta e="T536" id="Seg_4849" s="T535">bert</ta>
            <ta e="T537" id="Seg_4850" s="T536">e-bit</ta>
            <ta e="T538" id="Seg_4851" s="T537">as-tarɨ-n</ta>
            <ta e="T539" id="Seg_4852" s="T538">ahɨn-an-nar</ta>
            <ta e="T540" id="Seg_4853" s="T539">Tojoː-nɨ</ta>
            <ta e="T541" id="Seg_4854" s="T540">emi͡e</ta>
            <ta e="T542" id="Seg_4855" s="T541">kɨrpalaː-bɨt-tar</ta>
            <ta e="T543" id="Seg_4856" s="T542">taŋn-ar</ta>
            <ta e="T544" id="Seg_4857" s="T543">taŋas</ta>
            <ta e="T545" id="Seg_4858" s="T544">bi͡er-bit-ter</ta>
            <ta e="T546" id="Seg_4859" s="T545">ket-er</ta>
            <ta e="T547" id="Seg_4860" s="T546">tunʼak-tarɨ-n</ta>
            <ta e="T548" id="Seg_4861" s="T547">tuttar-bɨt-tar</ta>
            <ta e="T549" id="Seg_4862" s="T548">kɨrabaːt-ɨ-gar</ta>
            <ta e="T550" id="Seg_4863" s="T549">kel-eːt</ta>
            <ta e="T551" id="Seg_4864" s="T550">olor-but</ta>
            <ta e="T552" id="Seg_4865" s="T551">körül-üː</ta>
            <ta e="T553" id="Seg_4866" s="T552">tu͡og-u</ta>
            <ta e="T554" id="Seg_4867" s="T553">tuttar-bɨt-tarɨ-n</ta>
            <ta e="T555" id="Seg_4868" s="T554">barɨ-ta</ta>
            <ta e="T556" id="Seg_4869" s="T555">gini͡e-ke</ta>
            <ta e="T557" id="Seg_4870" s="T556">höp</ta>
            <ta e="T558" id="Seg_4871" s="T557">ol</ta>
            <ta e="T559" id="Seg_4872" s="T558">daː</ta>
            <ta e="T560" id="Seg_4873" s="T559">bu͡ol-lar</ta>
            <ta e="T561" id="Seg_4874" s="T560">togo</ta>
            <ta e="T562" id="Seg_4875" s="T561">ginner</ta>
            <ta e="T563" id="Seg_4876" s="T562">bi͡er-bit-tere</ta>
            <ta e="T564" id="Seg_4877" s="T563">bu͡olu͡o=j</ta>
            <ta e="T565" id="Seg_4878" s="T564">ikki-liː</ta>
            <ta e="T566" id="Seg_4879" s="T565">urbakɨ-nɨ</ta>
            <ta e="T567" id="Seg_4880" s="T566">ikki-liː</ta>
            <ta e="T568" id="Seg_4881" s="T567">hɨ͡alɨja-nɨ</ta>
            <ta e="T569" id="Seg_4882" s="T568">haŋa</ta>
            <ta e="T570" id="Seg_4883" s="T569">taŋas-tarɨ-n</ta>
            <ta e="T571" id="Seg_4884" s="T570">ket-er</ta>
            <ta e="T574" id="Seg_4885" s="T573">hanaː-bɨt</ta>
            <ta e="T575" id="Seg_4886" s="T574">ogo-lor-u</ta>
            <ta e="T576" id="Seg_4887" s="T575">huːn-nar-a</ta>
            <ta e="T577" id="Seg_4888" s="T576">il-pit-ter</ta>
            <ta e="T578" id="Seg_4889" s="T577">kanna</ta>
            <ta e="T579" id="Seg_4890" s="T578">menik</ta>
            <ta e="T580" id="Seg_4891" s="T579">dʼon</ta>
            <ta e="T581" id="Seg_4892" s="T580">ele</ta>
            <ta e="T582" id="Seg_4893" s="T581">hanaː-larɨ-nan</ta>
            <ta e="T583" id="Seg_4894" s="T582">ü͡örüːleːk-tik</ta>
            <ta e="T584" id="Seg_4895" s="T583">kül-s-e-hal-s-a</ta>
            <ta e="T585" id="Seg_4896" s="T584">ataj</ta>
            <ta e="T586" id="Seg_4897" s="T585">ataj</ta>
            <ta e="T587" id="Seg_4898" s="T586">d-e-h-e</ta>
            <ta e="T588" id="Seg_4899" s="T587">olor-but-tar</ta>
            <ta e="T589" id="Seg_4900" s="T588">Tojoː</ta>
            <ta e="T590" id="Seg_4901" s="T589">huːn-an</ta>
            <ta e="T591" id="Seg_4902" s="T590">büt-eːt</ta>
            <ta e="T592" id="Seg_4903" s="T591">taŋn-ɨ-bɨt</ta>
            <ta e="T593" id="Seg_4904" s="T592">kep-pit</ta>
            <ta e="T594" id="Seg_4905" s="T593">ürüŋ</ta>
            <ta e="T595" id="Seg_4906" s="T594">urbaːkɨ-tɨ-n</ta>
            <ta e="T596" id="Seg_4907" s="T595">hɨ͡alɨja-tɨ-n</ta>
            <ta e="T597" id="Seg_4908" s="T596">horok-tor-u-n</ta>
            <ta e="T598" id="Seg_4909" s="T597">hapaːs-taː-n</ta>
            <ta e="T599" id="Seg_4910" s="T598">huːlaː-bɨt</ta>
            <ta e="T600" id="Seg_4911" s="T599">kojut</ta>
            <ta e="T601" id="Seg_4912" s="T600">ket-eːri</ta>
            <ta e="T602" id="Seg_4913" s="T601">et-i-ger</ta>
            <ta e="T603" id="Seg_4914" s="T602">ket-en</ta>
            <ta e="T604" id="Seg_4915" s="T603">ürüŋ</ta>
            <ta e="T605" id="Seg_4916" s="T604">taŋas-tar-ɨ-n</ta>
            <ta e="T606" id="Seg_4917" s="T605">tu͡ok</ta>
            <ta e="T607" id="Seg_4918" s="T606">da</ta>
            <ta e="T608" id="Seg_4919" s="T607">bu͡ol-batak</ta>
            <ta e="T609" id="Seg_4920" s="T608">kördük</ta>
            <ta e="T610" id="Seg_4921" s="T609">hɨldʼ-ɨ-bɨt</ta>
            <ta e="T611" id="Seg_4922" s="T610">dogot-tor-o</ta>
            <ta e="T612" id="Seg_4923" s="T611">da</ta>
            <ta e="T613" id="Seg_4924" s="T612">tu͡og-u</ta>
            <ta e="T614" id="Seg_4925" s="T613">da</ta>
            <ta e="T615" id="Seg_4926" s="T614">di͡e-bet-ter</ta>
            <ta e="T616" id="Seg_4927" s="T615">e-bit</ta>
            <ta e="T617" id="Seg_4928" s="T616">gini͡e-ke</ta>
            <ta e="T618" id="Seg_4929" s="T617">intʼernaːt-ka</ta>
            <ta e="T619" id="Seg_4930" s="T618">üleliː-r</ta>
            <ta e="T620" id="Seg_4931" s="T619">biːr</ta>
            <ta e="T621" id="Seg_4932" s="T620">dʼaktar</ta>
            <ta e="T622" id="Seg_4933" s="T621">tu͡olkulap-pɨt</ta>
            <ta e="T623" id="Seg_4934" s="T622">kajtak</ta>
            <ta e="T624" id="Seg_4935" s="T623">gini</ta>
            <ta e="T625" id="Seg_4936" s="T624">taŋn-ar</ta>
            <ta e="T626" id="Seg_4937" s="T625">bu͡ol-u͡og-u-n</ta>
            <ta e="T627" id="Seg_4938" s="T626">ahɨː-r</ta>
            <ta e="T628" id="Seg_4939" s="T627">kem-nere</ta>
            <ta e="T629" id="Seg_4940" s="T628">kel-bit</ta>
            <ta e="T630" id="Seg_4941" s="T629">ogo-lor-u</ta>
            <ta e="T631" id="Seg_4942" s="T630">olor-toː-but-tar</ta>
            <ta e="T634" id="Seg_4943" s="T633">uhun</ta>
            <ta e="T635" id="Seg_4944" s="T634">bagajɨ</ta>
            <ta e="T636" id="Seg_4945" s="T635">ostol-lor</ta>
            <ta e="T637" id="Seg_4946" s="T636">ikki</ta>
            <ta e="T638" id="Seg_4947" s="T637">di͡eki</ta>
            <ta e="T639" id="Seg_4948" s="T638">öttü-leri-ger</ta>
            <ta e="T640" id="Seg_4949" s="T639">ulakan</ta>
            <ta e="T641" id="Seg_4950" s="T640">ihit-ter-ge</ta>
            <ta e="T642" id="Seg_4951" s="T641">uːr-u-ll-u-but-tar</ta>
            <ta e="T643" id="Seg_4952" s="T642">minnʼiges</ta>
            <ta e="T644" id="Seg_4953" s="T643">hɨt-taːk</ta>
            <ta e="T645" id="Seg_4954" s="T644">kili͡ep-ter</ta>
            <ta e="T646" id="Seg_4955" s="T645">ogo</ta>
            <ta e="T647" id="Seg_4956" s="T646">aːjɨ</ta>
            <ta e="T648" id="Seg_4957" s="T647">kut-attaː-bɨt-tar</ta>
            <ta e="T649" id="Seg_4958" s="T648">balɨk</ta>
            <ta e="T650" id="Seg_4959" s="T649">min-i-n</ta>
            <ta e="T651" id="Seg_4960" s="T650">lu͡oska-lar-ɨ</ta>
            <ta e="T652" id="Seg_4961" s="T651">bi͡er-etteː-bit-ter</ta>
            <ta e="T653" id="Seg_4962" s="T652">kör-büt</ta>
            <ta e="T654" id="Seg_4963" s="T653">ogo-lor</ta>
            <ta e="T655" id="Seg_4964" s="T654">ahaː-n-nar</ta>
            <ta e="T656" id="Seg_4965" s="T655">ketek-tere</ta>
            <ta e="T657" id="Seg_4966" s="T656">agaj</ta>
            <ta e="T658" id="Seg_4967" s="T657">hüt-e</ta>
            <ta e="T659" id="Seg_4968" s="T658">hɨt-ar</ta>
            <ta e="T660" id="Seg_4969" s="T659">Tojoː</ta>
            <ta e="T661" id="Seg_4970" s="T660">ah-ɨː</ta>
            <ta e="T662" id="Seg_4971" s="T661">olor-on</ta>
            <ta e="T663" id="Seg_4972" s="T662">tug-u</ta>
            <ta e="T664" id="Seg_4973" s="T663">ire</ta>
            <ta e="T665" id="Seg_4974" s="T664">hanaː-ta</ta>
            <ta e="T666" id="Seg_4975" s="T665">kel-bit</ta>
            <ta e="T667" id="Seg_4976" s="T666">maːma-tɨ-n</ta>
            <ta e="T668" id="Seg_4977" s="T667">aga-tɨ-n</ta>
            <ta e="T669" id="Seg_4978" s="T668">Kɨča-tɨ-n</ta>
            <ta e="T670" id="Seg_4979" s="T669">öjdöː-n</ta>
            <ta e="T671" id="Seg_4980" s="T670">ahaː-n</ta>
            <ta e="T672" id="Seg_4981" s="T671">büt-eːt</ta>
            <ta e="T673" id="Seg_4982" s="T672">ogo-lor</ta>
            <ta e="T674" id="Seg_4983" s="T673">Valačaːnka</ta>
            <ta e="T675" id="Seg_4984" s="T674">üstün</ta>
            <ta e="T676" id="Seg_4985" s="T675">oːnnʼ-uː</ta>
            <ta e="T677" id="Seg_4986" s="T676">hɨldʼ-ɨ-bɨt-tar</ta>
            <ta e="T678" id="Seg_4987" s="T677">körül-üː</ta>
            <ta e="T679" id="Seg_4988" s="T678">nʼuːčča</ta>
            <ta e="T680" id="Seg_4989" s="T679">dʼi͡e-ler-i</ta>
            <ta e="T681" id="Seg_4990" s="T680">barɨ</ta>
            <ta e="T682" id="Seg_4991" s="T681">tergi͡en-i</ta>
            <ta e="T683" id="Seg_4992" s="T682">ginner-ge</ta>
            <ta e="T684" id="Seg_4993" s="T683">barɨ-ta</ta>
            <ta e="T685" id="Seg_4994" s="T684">dʼikti</ta>
            <ta e="T686" id="Seg_4995" s="T685">haŋardɨː</ta>
            <ta e="T687" id="Seg_4996" s="T686">kel-bit</ta>
            <ta e="T688" id="Seg_4997" s="T687">kihi-ler-ge</ta>
            <ta e="T689" id="Seg_4998" s="T688">nöŋü͡ö</ta>
            <ta e="T690" id="Seg_4999" s="T689">kün</ta>
            <ta e="T691" id="Seg_5000" s="T690">emi͡e</ta>
            <ta e="T692" id="Seg_5001" s="T691">ulakan</ta>
            <ta e="T693" id="Seg_5002" s="T692">ogo-loːk</ta>
            <ta e="T694" id="Seg_5003" s="T693">kös</ta>
            <ta e="T695" id="Seg_5004" s="T694">kel-bit</ta>
            <ta e="T696" id="Seg_5005" s="T695">atɨn</ta>
            <ta e="T697" id="Seg_5006" s="T696">kalxoːz-tan</ta>
            <ta e="T698" id="Seg_5007" s="T697">haŋardɨː</ta>
            <ta e="T699" id="Seg_5008" s="T698">kel-iː</ta>
            <ta e="T700" id="Seg_5009" s="T699">ogo-lor</ta>
            <ta e="T701" id="Seg_5010" s="T700">kiːrbikteːg-e</ta>
            <ta e="T702" id="Seg_5011" s="T701">bert</ta>
            <ta e="T703" id="Seg_5012" s="T702">barɨ-ttan</ta>
            <ta e="T705" id="Seg_5013" s="T704">kuttan-ar</ta>
            <ta e="T706" id="Seg_5014" s="T705">kördük-ter</ta>
            <ta e="T707" id="Seg_5015" s="T706">ginner</ta>
            <ta e="T708" id="Seg_5016" s="T707">olus</ta>
            <ta e="T709" id="Seg_5017" s="T708">ör</ta>
            <ta e="T710" id="Seg_5018" s="T709">itigirdik</ta>
            <ta e="T711" id="Seg_5019" s="T710">bu͡ol-batak-tar</ta>
            <ta e="T712" id="Seg_5020" s="T711">emi͡e</ta>
            <ta e="T713" id="Seg_5021" s="T712">Tojoː</ta>
            <ta e="T714" id="Seg_5022" s="T713">Miteː</ta>
            <ta e="T715" id="Seg_5023" s="T714">Ujbaːn</ta>
            <ta e="T716" id="Seg_5024" s="T715">kördük</ta>
            <ta e="T717" id="Seg_5025" s="T716">toll-u-bat</ta>
            <ta e="T718" id="Seg_5026" s="T717">mi͡ere-ni</ta>
            <ta e="T719" id="Seg_5027" s="T718">ɨl-bɨt-tar</ta>
            <ta e="T720" id="Seg_5028" s="T719">haŋa</ta>
            <ta e="T721" id="Seg_5029" s="T720">dogot-tor-u-n</ta>
            <ta e="T722" id="Seg_5030" s="T721">kör-ön-nör</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_5031" s="T0">ihilleː-ŋ</ta>
            <ta e="T2" id="Seg_5032" s="T1">ös-nI</ta>
            <ta e="T3" id="Seg_5033" s="T2">dʼikti</ta>
            <ta e="T4" id="Seg_5034" s="T3">taba-LAr</ta>
            <ta e="T5" id="Seg_5035" s="T4">üs-Is</ta>
            <ta e="T6" id="Seg_5036" s="T5">kün-tI-GAr</ta>
            <ta e="T7" id="Seg_5037" s="T6">ogo-LAːK</ta>
            <ta e="T8" id="Seg_5038" s="T7">kös</ta>
            <ta e="T9" id="Seg_5039" s="T8">tij-BIT</ta>
            <ta e="T10" id="Seg_5040" s="T9">Voločanka-GA</ta>
            <ta e="T11" id="Seg_5041" s="T10">Avam</ta>
            <ta e="T12" id="Seg_5042" s="T11">rajon-tI-n</ta>
            <ta e="T13" id="Seg_5043" s="T12">kiːn</ta>
            <ta e="T14" id="Seg_5044" s="T13">olok-tI-n</ta>
            <ta e="T15" id="Seg_5045" s="T14">nuːčča-LArA</ta>
            <ta e="T16" id="Seg_5046" s="T15">haka-LArA</ta>
            <ta e="T17" id="Seg_5047" s="T16">barɨ-LArA</ta>
            <ta e="T18" id="Seg_5048" s="T17">komuj-LIN-I-BIT-LArA</ta>
            <ta e="T19" id="Seg_5049" s="T18">kel-Iː</ta>
            <ta e="T20" id="Seg_5050" s="T19">ogo-LAr-nI</ta>
            <ta e="T21" id="Seg_5051" s="T20">kör-AːrI</ta>
            <ta e="T22" id="Seg_5052" s="T21">kukla</ta>
            <ta e="T23" id="Seg_5053" s="T22">kördük</ta>
            <ta e="T24" id="Seg_5054" s="T23">taŋas-LAːK-LAr-nI</ta>
            <ta e="T25" id="Seg_5055" s="T24">kihi</ta>
            <ta e="T26" id="Seg_5056" s="T25">taptaː-IAK-LAr-nI</ta>
            <ta e="T27" id="Seg_5057" s="T26">olus</ta>
            <ta e="T28" id="Seg_5058" s="T27">ajdaːn-tA</ta>
            <ta e="T29" id="Seg_5059" s="T28">hu͡ok</ta>
            <ta e="T30" id="Seg_5060" s="T29">dʼɨlɨgɨr</ta>
            <ta e="T31" id="Seg_5061" s="T30">Voločanka</ta>
            <ta e="T32" id="Seg_5062" s="T31">olok</ta>
            <ta e="T33" id="Seg_5063" s="T32">biːr</ta>
            <ta e="T34" id="Seg_5064" s="T33">duruk</ta>
            <ta e="T35" id="Seg_5065" s="T34">tilin-I-BIT</ta>
            <ta e="T36" id="Seg_5066" s="T35">kördük</ta>
            <ta e="T37" id="Seg_5067" s="T36">taŋara</ta>
            <ta e="T38" id="Seg_5068" s="T37">kün-tI-n</ta>
            <ta e="T39" id="Seg_5069" s="T38">beli͡e-LAː-Ar</ta>
            <ta e="T40" id="Seg_5070" s="T39">kördük</ta>
            <ta e="T41" id="Seg_5071" s="T40">bu͡ol-An</ta>
            <ta e="T42" id="Seg_5072" s="T41">kaːl-BIT</ta>
            <ta e="T43" id="Seg_5073" s="T42">ogo-LAr</ta>
            <ta e="T44" id="Seg_5074" s="T43">bu͡ollagɨna</ta>
            <ta e="T45" id="Seg_5075" s="T44">kihi-LAr-nI</ta>
            <ta e="T46" id="Seg_5076" s="T45">kör-BAT-LAr</ta>
            <ta e="T47" id="Seg_5077" s="T46">nuːčča</ta>
            <ta e="T48" id="Seg_5078" s="T47">dʼi͡e-LAr</ta>
            <ta e="T49" id="Seg_5079" s="T48">dek</ta>
            <ta e="T50" id="Seg_5080" s="T49">agaj</ta>
            <ta e="T53" id="Seg_5081" s="T52">karak-LArA</ta>
            <ta e="T54" id="Seg_5082" s="T53">ulica</ta>
            <ta e="T55" id="Seg_5083" s="T54">üstün</ta>
            <ta e="T56" id="Seg_5084" s="T55">ataralaː-A-t-An</ta>
            <ta e="T57" id="Seg_5085" s="T56">is-An-LAr</ta>
            <ta e="T58" id="Seg_5086" s="T57">uːčak-I-nAn</ta>
            <ta e="T59" id="Seg_5087" s="T58">ehe-LArA</ta>
            <ta e="T60" id="Seg_5088" s="T59">Anʼiːsʼim</ta>
            <ta e="T61" id="Seg_5089" s="T60">ogonnʼor</ta>
            <ta e="T62" id="Seg_5090" s="T61">tur-BIT</ta>
            <ta e="T63" id="Seg_5091" s="T62">usku͡ola</ta>
            <ta e="T64" id="Seg_5092" s="T63">attɨ-GAr</ta>
            <ta e="T65" id="Seg_5093" s="T64">gini-nI</ta>
            <ta e="T66" id="Seg_5094" s="T65">tögürüččü</ta>
            <ta e="T67" id="Seg_5095" s="T66">ajdaːn-LAːK</ta>
            <ta e="T68" id="Seg_5096" s="T67">armʼija</ta>
            <ta e="T69" id="Seg_5097" s="T68">ugučak-LArI-nAn</ta>
            <ta e="T72" id="Seg_5098" s="T71">ürüt-LArI-ttAn</ta>
            <ta e="T73" id="Seg_5099" s="T72">ɨstaŋalaː-BIT-LAr</ta>
            <ta e="T74" id="Seg_5100" s="T73">usku͡ola</ta>
            <ta e="T75" id="Seg_5101" s="T74">attɨ-GAr</ta>
            <ta e="T76" id="Seg_5102" s="T75">tur-Ar-LAr</ta>
            <ta e="T79" id="Seg_5103" s="T78">ü͡öret-Ar</ta>
            <ta e="T80" id="Seg_5104" s="T79">dʼon</ta>
            <ta e="T81" id="Seg_5105" s="T80">hübehit-LArA</ta>
            <ta e="T82" id="Seg_5106" s="T81">onno</ta>
            <ta e="T83" id="Seg_5107" s="T82">internat</ta>
            <ta e="T84" id="Seg_5108" s="T83">ülehit-LAr-tA</ta>
            <ta e="T85" id="Seg_5109" s="T84">barɨ-LArA</ta>
            <ta e="T86" id="Seg_5110" s="T85">ogo-LAr-nI</ta>
            <ta e="T87" id="Seg_5111" s="T86">kap-An</ta>
            <ta e="T88" id="Seg_5112" s="T87">ɨl-An-LAr</ta>
            <ta e="T89" id="Seg_5113" s="T88">doroːbo-LAː-A-s-Ar-LAr</ta>
            <ta e="T90" id="Seg_5114" s="T89">tu͡ok-nI</ta>
            <ta e="T91" id="Seg_5115" s="T90">ere</ta>
            <ta e="T92" id="Seg_5116" s="T91">giniler-GA</ta>
            <ta e="T93" id="Seg_5117" s="T92">haŋar-Ar-LAr</ta>
            <ta e="T94" id="Seg_5118" s="T93">ogo-LAr</ta>
            <ta e="T95" id="Seg_5119" s="T94">nuːčča</ta>
            <ta e="T96" id="Seg_5120" s="T95">tɨl-tI-n</ta>
            <ta e="T97" id="Seg_5121" s="T96">tu͡olkulaː-BAT-LAr</ta>
            <ta e="T98" id="Seg_5122" s="T97">ol</ta>
            <ta e="T99" id="Seg_5123" s="T98">ihin</ta>
            <ta e="T100" id="Seg_5124" s="T99">kel-A</ta>
            <ta e="T101" id="Seg_5125" s="T100">kɨra</ta>
            <ta e="T102" id="Seg_5126" s="T101">dʼon</ta>
            <ta e="T103" id="Seg_5127" s="T102">kepset-BAT-LAr</ta>
            <ta e="T104" id="Seg_5128" s="T103">oduːlaː-Ar</ta>
            <ta e="T105" id="Seg_5129" s="T104">agaj</ta>
            <ta e="T106" id="Seg_5130" s="T105">bu͡ol-BIT-LAr</ta>
            <ta e="T107" id="Seg_5131" s="T106">učuːtal-LArI-n</ta>
            <ta e="T108" id="Seg_5132" s="T107">hɨraj-LArI-n</ta>
            <ta e="T109" id="Seg_5133" s="T108">giniler</ta>
            <ta e="T110" id="Seg_5134" s="T109">olus</ta>
            <ta e="T111" id="Seg_5135" s="T110">intiri͡es-nI</ta>
            <ta e="T112" id="Seg_5136" s="T111">kör-BIT-LAr</ta>
            <ta e="T113" id="Seg_5137" s="T112">olus</ta>
            <ta e="T114" id="Seg_5138" s="T113">ulakan</ta>
            <ta e="T115" id="Seg_5139" s="T114">nuːčča</ta>
            <ta e="T116" id="Seg_5140" s="T115">dʼi͡e-LAr-nI</ta>
            <ta e="T117" id="Seg_5141" s="T116">atɨn</ta>
            <ta e="T118" id="Seg_5142" s="T117">kihi-LAr-nI</ta>
            <ta e="T119" id="Seg_5143" s="T118">nuːčča-LAr-nI</ta>
            <ta e="T120" id="Seg_5144" s="T119">atɨn</ta>
            <ta e="T121" id="Seg_5145" s="T120">tɨl-LAːK-LAr-nI</ta>
            <ta e="T122" id="Seg_5146" s="T121">taŋas-LAːK-LAr-nI</ta>
            <ta e="T123" id="Seg_5147" s="T122">atak-LArI-GAr</ta>
            <ta e="T124" id="Seg_5148" s="T123">bu͡ollagɨna</ta>
            <ta e="T125" id="Seg_5149" s="T124">taba</ta>
            <ta e="T126" id="Seg_5150" s="T125">tunʼak-tA</ta>
            <ta e="T127" id="Seg_5151" s="T126">kördük</ta>
            <ta e="T128" id="Seg_5152" s="T127">atak-nI</ta>
            <ta e="T129" id="Seg_5153" s="T128">iːl-I-n-BIT-LAr</ta>
            <ta e="T130" id="Seg_5154" s="T129">er</ta>
            <ta e="T131" id="Seg_5155" s="T130">kihi-LAr</ta>
            <ta e="T132" id="Seg_5156" s="T131">bas-LArI-GAr</ta>
            <ta e="T133" id="Seg_5157" s="T132">maːma-LArI-n</ta>
            <ta e="T134" id="Seg_5158" s="T133">komu͡os-tI-n</ta>
            <ta e="T135" id="Seg_5159" s="T134">kördük</ta>
            <ta e="T136" id="Seg_5160" s="T135">bergehe-nI</ta>
            <ta e="T137" id="Seg_5161" s="T136">ket-A</ta>
            <ta e="T138" id="Seg_5162" s="T137">hɨrɨt-Ar-LAr</ta>
            <ta e="T139" id="Seg_5163" s="T138">dʼaktar-LAr-tA</ta>
            <ta e="T140" id="Seg_5164" s="T139">örüː-tA</ta>
            <ta e="T141" id="Seg_5165" s="T140">hu͡ok-LAr</ta>
            <ta e="T142" id="Seg_5166" s="T141">as-LArA</ta>
            <ta e="T143" id="Seg_5167" s="T142">bɨs-I-LIN-I-BIT-LAr</ta>
            <ta e="T144" id="Seg_5168" s="T143">er</ta>
            <ta e="T145" id="Seg_5169" s="T144">kihi</ta>
            <ta e="T146" id="Seg_5170" s="T145">kördük</ta>
            <ta e="T147" id="Seg_5171" s="T146">ɨrbaːkɨ-LArA</ta>
            <ta e="T148" id="Seg_5172" s="T147">allaraː</ta>
            <ta e="T149" id="Seg_5173" s="T148">örüt-tI-GAr</ta>
            <ta e="T150" id="Seg_5174" s="T149">olus</ta>
            <ta e="T151" id="Seg_5175" s="T150">čipčigɨr</ta>
            <ta e="T152" id="Seg_5176" s="T151">ol</ta>
            <ta e="T153" id="Seg_5177" s="T152">ihin</ta>
            <ta e="T154" id="Seg_5178" s="T153">giniler</ta>
            <ta e="T155" id="Seg_5179" s="T154">arɨːččɨ</ta>
            <ta e="T156" id="Seg_5180" s="T155">kaːm-Ar-LAr</ta>
            <ta e="T157" id="Seg_5181" s="T156">Tojoː</ta>
            <ta e="T158" id="Seg_5182" s="T157">olus</ta>
            <ta e="T159" id="Seg_5183" s="T158">oduːlaː-A</ta>
            <ta e="T160" id="Seg_5184" s="T159">tur-BIT</ta>
            <ta e="T161" id="Seg_5185" s="T160">dʼaktar-LAr-nI</ta>
            <ta e="T162" id="Seg_5186" s="T161">is-tI-GAr</ta>
            <ta e="T163" id="Seg_5187" s="T162">hanaː-Ar</ta>
            <ta e="T164" id="Seg_5188" s="T163">badaga</ta>
            <ta e="T165" id="Seg_5189" s="T164">itinnik</ta>
            <ta e="T166" id="Seg_5190" s="T165">ɨrbaːkɨ-nAn</ta>
            <ta e="T167" id="Seg_5191" s="T166">hüːr-A</ta>
            <ta e="T168" id="Seg_5192" s="T167">hataː-BAT-LAr</ta>
            <ta e="T169" id="Seg_5193" s="T168">kɨratɨk</ta>
            <ta e="T170" id="Seg_5194" s="T169">ere</ta>
            <ta e="T171" id="Seg_5195" s="T170">kaːm-Ar-LAr</ta>
            <ta e="T172" id="Seg_5196" s="T171">e-BIT</ta>
            <ta e="T173" id="Seg_5197" s="T172">dogor-LAr-tI-GAr</ta>
            <ta e="T174" id="Seg_5198" s="T173">tu͡ok-nI</ta>
            <ta e="T175" id="Seg_5199" s="T174">ere</ta>
            <ta e="T176" id="Seg_5200" s="T175">iti</ta>
            <ta e="T177" id="Seg_5201" s="T176">tus-tI-nAn</ta>
            <ta e="T178" id="Seg_5202" s="T177">kepseː-AːrI</ta>
            <ta e="T179" id="Seg_5203" s="T178">gɨn-BIT</ta>
            <ta e="T180" id="Seg_5204" s="T179">anʼak-tI-n</ta>
            <ta e="T181" id="Seg_5205" s="T180">ere</ta>
            <ta e="T182" id="Seg_5206" s="T181">arɨj-BIT</ta>
            <ta e="T183" id="Seg_5207" s="T182">tu͡ok</ta>
            <ta e="T184" id="Seg_5208" s="T183">ere</ta>
            <ta e="T185" id="Seg_5209" s="T184">ajdaːn-tA</ta>
            <ta e="T186" id="Seg_5210" s="T185">bu͡ol-BIT</ta>
            <ta e="T187" id="Seg_5211" s="T186">kör-BIT</ta>
            <ta e="T188" id="Seg_5212" s="T187">ol</ta>
            <ta e="T189" id="Seg_5213" s="T188">dek</ta>
            <ta e="T190" id="Seg_5214" s="T189">kantan</ta>
            <ta e="T191" id="Seg_5215" s="T190">ajdaːn</ta>
            <ta e="T192" id="Seg_5216" s="T191">tagɨs-I-BIT</ta>
            <ta e="T195" id="Seg_5217" s="T194">tu͡ok</ta>
            <ta e="T196" id="Seg_5218" s="T195">ere</ta>
            <ta e="T197" id="Seg_5219" s="T196">kihi</ta>
            <ta e="T198" id="Seg_5220" s="T197">kör-BAtAK</ta>
            <ta e="T199" id="Seg_5221" s="T198">hɨrga-LAːK-tA</ta>
            <ta e="T200" id="Seg_5222" s="T199">tal-n-A-t-An</ta>
            <ta e="T201" id="Seg_5223" s="T200">is-BIT</ta>
            <ta e="T202" id="Seg_5224" s="T201">hɨrga-tA</ta>
            <ta e="T203" id="Seg_5225" s="T202">i͡erikej</ta>
            <ta e="T204" id="Seg_5226" s="T203">taba-tA</ta>
            <ta e="T205" id="Seg_5227" s="T204">bu͡ollagɨna</ta>
            <ta e="T206" id="Seg_5228" s="T205">ulakan-tA</ta>
            <ta e="T207" id="Seg_5229" s="T206">bert</ta>
            <ta e="T208" id="Seg_5230" s="T207">olus</ta>
            <ta e="T209" id="Seg_5231" s="T208">uhun</ta>
            <ta e="T210" id="Seg_5232" s="T209">dajbaː-Ar</ta>
            <ta e="T211" id="Seg_5233" s="T210">kördük</ta>
            <ta e="T212" id="Seg_5234" s="T211">kuturuk-LAːK</ta>
            <ta e="T213" id="Seg_5235" s="T212">hɨrga-LAː-Ar</ta>
            <ta e="T214" id="Seg_5236" s="T213">kihi-tA</ta>
            <ta e="T215" id="Seg_5237" s="T214">bu͡ollagɨna</ta>
            <ta e="T216" id="Seg_5238" s="T215">hɨraj-tA</ta>
            <ta e="T217" id="Seg_5239" s="T216">tüː-tA</ta>
            <ta e="T218" id="Seg_5240" s="T217">bert</ta>
            <ta e="T219" id="Seg_5241" s="T218">köp</ta>
            <ta e="T220" id="Seg_5242" s="T219">bɨtɨk-LAːK</ta>
            <ta e="T221" id="Seg_5243" s="T220">taba-tI-n</ta>
            <ta e="T222" id="Seg_5244" s="T221">kelin-tI-ttAn</ta>
            <ta e="T223" id="Seg_5245" s="T222">hatɨː</ta>
            <ta e="T224" id="Seg_5246" s="T223">kaːm-An</ta>
            <ta e="T225" id="Seg_5247" s="T224">is-An</ta>
            <ta e="T226" id="Seg_5248" s="T225">tut-BIT</ta>
            <ta e="T227" id="Seg_5249" s="T226">ikki</ta>
            <ta e="T228" id="Seg_5250" s="T227">nʼu͡oguː-nI</ta>
            <ta e="T229" id="Seg_5251" s="T228">taba-tA</ta>
            <ta e="T230" id="Seg_5252" s="T229">kajdak</ta>
            <ta e="T231" id="Seg_5253" s="T230">ere</ta>
            <ta e="T232" id="Seg_5254" s="T231">kaːm-Ar-tI-n</ta>
            <ta e="T233" id="Seg_5255" s="T232">aːjɨ</ta>
            <ta e="T234" id="Seg_5256" s="T233">bas-tI-n</ta>
            <ta e="T235" id="Seg_5257" s="T234">möŋ-TAr-Ar</ta>
            <ta e="T236" id="Seg_5258" s="T235">ol</ta>
            <ta e="T237" id="Seg_5259" s="T236">aːjɨ</ta>
            <ta e="T238" id="Seg_5260" s="T237">hɨrga-ČIt</ta>
            <ta e="T239" id="Seg_5261" s="T238">noː</ta>
            <ta e="T240" id="Seg_5262" s="T239">noː</ta>
            <ta e="T241" id="Seg_5263" s="T240">di͡e-Ar</ta>
            <ta e="T242" id="Seg_5264" s="T241">oloŋko-nI</ta>
            <ta e="T243" id="Seg_5265" s="T242">ihilleː-Ar</ta>
            <ta e="T244" id="Seg_5266" s="T243">kördük</ta>
            <ta e="T245" id="Seg_5267" s="T244">küːsteːk</ta>
            <ta e="T246" id="Seg_5268" s="T245">bagajɨ-LIk</ta>
            <ta e="T247" id="Seg_5269" s="T246">anʼak-tI-nAn</ta>
            <ta e="T248" id="Seg_5270" s="T247">amsaj-BIT</ta>
            <ta e="T249" id="Seg_5271" s="T248">minnʼiges</ta>
            <ta e="T250" id="Seg_5272" s="T249">hɨ͡a-nI</ta>
            <ta e="T251" id="Seg_5273" s="T250">hi͡e-Ar</ta>
            <ta e="T252" id="Seg_5274" s="T251">kördük</ta>
            <ta e="T253" id="Seg_5275" s="T252">kaja</ta>
            <ta e="T254" id="Seg_5276" s="T253">iti</ta>
            <ta e="T255" id="Seg_5277" s="T254">tu͡ok</ta>
            <ta e="T256" id="Seg_5278" s="T255">taba-tA</ta>
            <ta e="T257" id="Seg_5279" s="T256">bu͡ol-Ar</ta>
            <ta e="T258" id="Seg_5280" s="T257">kör-A</ta>
            <ta e="T259" id="Seg_5281" s="T258">tur-An</ta>
            <ta e="T260" id="Seg_5282" s="T259">ü͡ör-BIččA</ta>
            <ta e="T261" id="Seg_5283" s="T260">Tojoː</ta>
            <ta e="T262" id="Seg_5284" s="T261">haŋa</ta>
            <ta e="T263" id="Seg_5285" s="T262">alɨn-I-BIT</ta>
            <ta e="T264" id="Seg_5286" s="T263">gini</ta>
            <ta e="T265" id="Seg_5287" s="T264">agaj</ta>
            <ta e="T266" id="Seg_5288" s="T265">ol-nI</ta>
            <ta e="T267" id="Seg_5289" s="T266">kör-An</ta>
            <ta e="T268" id="Seg_5290" s="T267">tur-BAtAK-tA</ta>
            <ta e="T269" id="Seg_5291" s="T268">barɨ</ta>
            <ta e="T270" id="Seg_5292" s="T269">ogo-LAr</ta>
            <ta e="T271" id="Seg_5293" s="T270">oduː-GI</ta>
            <ta e="T272" id="Seg_5294" s="T271">bu͡ol-BIT-LAr</ta>
            <ta e="T273" id="Seg_5295" s="T272">beje</ta>
            <ta e="T274" id="Seg_5296" s="T273">beje-LArI-n</ta>
            <ta e="T275" id="Seg_5297" s="T274">kɨtta</ta>
            <ta e="T276" id="Seg_5298" s="T275">kepset-A</ta>
            <ta e="T277" id="Seg_5299" s="T276">ilik-TArInA</ta>
            <ta e="T278" id="Seg_5300" s="T277">emi͡e</ta>
            <ta e="T279" id="Seg_5301" s="T278">kör-BIT-LAr</ta>
            <ta e="T280" id="Seg_5302" s="T279">haŋa</ta>
            <ta e="T281" id="Seg_5303" s="T280">dʼikti-nI</ta>
            <ta e="T282" id="Seg_5304" s="T281">kɨrdʼagas</ta>
            <ta e="T283" id="Seg_5305" s="T282">nuːčča</ta>
            <ta e="T284" id="Seg_5306" s="T283">emeːksin-tA</ta>
            <ta e="T285" id="Seg_5307" s="T284">tu͡ok</ta>
            <ta e="T286" id="Seg_5308" s="T285">ere</ta>
            <ta e="T287" id="Seg_5309" s="T286">kɨra</ta>
            <ta e="T288" id="Seg_5310" s="T287">taba-kAːN-LAr-tI-n</ta>
            <ta e="T289" id="Seg_5311" s="T288">üːr-An</ta>
            <ta e="T290" id="Seg_5312" s="T289">is-BIT</ta>
            <ta e="T291" id="Seg_5313" s="T290">hɨgɨnʼak</ta>
            <ta e="T292" id="Seg_5314" s="T291">kultumak</ta>
            <ta e="T293" id="Seg_5315" s="T292">taba-kAːN-LAr</ta>
            <ta e="T294" id="Seg_5316" s="T293">nʼamčɨgas</ta>
            <ta e="T295" id="Seg_5317" s="T294">atak-LArI-nAn</ta>
            <ta e="T296" id="Seg_5318" s="T295">hüːr-AlAː-An</ta>
            <ta e="T297" id="Seg_5319" s="T296">is-BIT-LAr</ta>
            <ta e="T298" id="Seg_5320" s="T297">bardʼɨgɨnaː-A-bardʼɨgɨnaː-A</ta>
            <ta e="T299" id="Seg_5321" s="T298">ol-tI-LArA</ta>
            <ta e="T300" id="Seg_5322" s="T299">köt-An</ta>
            <ta e="T301" id="Seg_5323" s="T300">is-An-LAr</ta>
            <ta e="T302" id="Seg_5324" s="T301">ol-bu</ta>
            <ta e="T303" id="Seg_5325" s="T302">hir</ta>
            <ta e="T304" id="Seg_5326" s="T303">dek</ta>
            <ta e="T305" id="Seg_5327" s="T304">butarɨs-Ar-LAr</ta>
            <ta e="T306" id="Seg_5328" s="T305">paː</ta>
            <ta e="T307" id="Seg_5329" s="T306">di͡e-An</ta>
            <ta e="T308" id="Seg_5330" s="T307">ü͡ögüleː-BIT</ta>
            <ta e="T309" id="Seg_5331" s="T308">Tojoː</ta>
            <ta e="T310" id="Seg_5332" s="T309">hɨgɨnʼak</ta>
            <ta e="T311" id="Seg_5333" s="T310">kɨːl-LAr</ta>
            <ta e="T312" id="Seg_5334" s="T311">ogo-LAr</ta>
            <ta e="T313" id="Seg_5335" s="T312">ele</ta>
            <ta e="T314" id="Seg_5336" s="T313">is-LArI-nAn</ta>
            <ta e="T315" id="Seg_5337" s="T314">kül-s-Ar-LAr</ta>
            <ta e="T316" id="Seg_5338" s="T315">čömüje-LArI-nAn</ta>
            <ta e="T317" id="Seg_5339" s="T316">kör-TAr-A-kör-TAr-A</ta>
            <ta e="T318" id="Seg_5340" s="T317">kör</ta>
            <ta e="T319" id="Seg_5341" s="T318">dogor</ta>
            <ta e="T320" id="Seg_5342" s="T319">munnu-LArA</ta>
            <ta e="T321" id="Seg_5343" s="T320">kürej</ta>
            <ta e="T322" id="Seg_5344" s="T321">lepse-tI-n</ta>
            <ta e="T323" id="Seg_5345" s="T322">kördük</ta>
            <ta e="T324" id="Seg_5346" s="T323">iti</ta>
            <ta e="T325" id="Seg_5347" s="T324">ere</ta>
            <ta e="T326" id="Seg_5348" s="T325">ikki</ta>
            <ta e="T327" id="Seg_5349" s="T326">ere</ta>
            <ta e="T328" id="Seg_5350" s="T327">üːt-LAːK-LAr</ta>
            <ta e="T329" id="Seg_5351" s="T328">taba</ta>
            <ta e="T330" id="Seg_5352" s="T329">itinnik</ta>
            <ta e="T331" id="Seg_5353" s="T330">bu͡ol-BAtAK</ta>
            <ta e="T332" id="Seg_5354" s="T331">bihi͡ettere</ta>
            <ta e="T333" id="Seg_5355" s="T332">bosku͡oj-LAr</ta>
            <ta e="T334" id="Seg_5356" s="T333">maŋnaj</ta>
            <ta e="T335" id="Seg_5357" s="T334">kuttan-A</ta>
            <ta e="T336" id="Seg_5358" s="T335">hɨs-BIT-LAr</ta>
            <ta e="T337" id="Seg_5359" s="T336">onton</ta>
            <ta e="T338" id="Seg_5360" s="T337">čugahaː-BIT-LAr</ta>
            <ta e="T339" id="Seg_5361" s="T338">ötü͡ö-LIk</ta>
            <ta e="T340" id="Seg_5362" s="T339">kör-AːrI</ta>
            <ta e="T341" id="Seg_5363" s="T340">Tojoː</ta>
            <ta e="T342" id="Seg_5364" s="T341">beje-tI-n</ta>
            <ta e="T343" id="Seg_5365" s="T342">allaːk-tI-n</ta>
            <ta e="T344" id="Seg_5366" s="T343">kɨ͡aj-BAtAK</ta>
            <ta e="T345" id="Seg_5367" s="T344">biːr-LArI-GAr</ta>
            <ta e="T346" id="Seg_5368" s="T345">hüːr-An</ta>
            <ta e="T347" id="Seg_5369" s="T346">kel-An</ta>
            <ta e="T348" id="Seg_5370" s="T347">ürüt-tI-GAr</ta>
            <ta e="T349" id="Seg_5371" s="T348">ugučak-LAN-A</ta>
            <ta e="T350" id="Seg_5372" s="T349">gɨn-A</ta>
            <ta e="T351" id="Seg_5373" s="T350">ɨstan-BIT</ta>
            <ta e="T352" id="Seg_5374" s="T351">kɨːl-tA</ta>
            <ta e="T353" id="Seg_5375" s="T352">his-tI-GAr</ta>
            <ta e="T354" id="Seg_5376" s="T353">kihi-nI</ta>
            <ta e="T355" id="Seg_5377" s="T354">bil-An</ta>
            <ta e="T356" id="Seg_5378" s="T355">tur-BIT</ta>
            <ta e="T357" id="Seg_5379" s="T356">hir-tI-ttAn</ta>
            <ta e="T358" id="Seg_5380" s="T357">ɨstan-BIT</ta>
            <ta e="T359" id="Seg_5381" s="T358">ür-BIččA</ta>
            <ta e="T360" id="Seg_5382" s="T359">ele</ta>
            <ta e="T361" id="Seg_5383" s="T360">küːs-tI-nAn</ta>
            <ta e="T362" id="Seg_5384" s="T361">köt-BIT</ta>
            <ta e="T363" id="Seg_5385" s="T362">bas-tI-n</ta>
            <ta e="T364" id="Seg_5386" s="T363">kotun</ta>
            <ta e="T365" id="Seg_5387" s="T364">ugučak-ttAn</ta>
            <ta e="T366" id="Seg_5388" s="T365">tüs-BAT</ta>
            <ta e="T367" id="Seg_5389" s="T366">ogo</ta>
            <ta e="T368" id="Seg_5390" s="T367">tutun-A</ta>
            <ta e="T369" id="Seg_5391" s="T368">hataː-BIT</ta>
            <ta e="T370" id="Seg_5392" s="T369">taba-tI-n</ta>
            <ta e="T371" id="Seg_5393" s="T370">tüː-tI-ttAn</ta>
            <ta e="T372" id="Seg_5394" s="T371">kajdak</ta>
            <ta e="T373" id="Seg_5395" s="T372">da</ta>
            <ta e="T374" id="Seg_5396" s="T373">kanna</ta>
            <ta e="T375" id="Seg_5397" s="T374">da</ta>
            <ta e="T376" id="Seg_5398" s="T375">iliː-LAr-tA</ta>
            <ta e="T377" id="Seg_5399" s="T376">iŋin-I-BAT</ta>
            <ta e="T378" id="Seg_5400" s="T377">taba-tI-n</ta>
            <ta e="T379" id="Seg_5401" s="T378">tüː-LAr-tA</ta>
            <ta e="T380" id="Seg_5402" s="T379">katɨː</ta>
            <ta e="T381" id="Seg_5403" s="T380">kördük-LAr</ta>
            <ta e="T382" id="Seg_5404" s="T381">ugučak-tA</ta>
            <ta e="T383" id="Seg_5405" s="T382">ele</ta>
            <ta e="T384" id="Seg_5406" s="T383">tɨːn-tI-nAn</ta>
            <ta e="T385" id="Seg_5407" s="T384">ü͡ögüleː-BIT</ta>
            <ta e="T386" id="Seg_5408" s="T385">bas-tI-n</ta>
            <ta e="T387" id="Seg_5409" s="T386">kotun</ta>
            <ta e="T388" id="Seg_5410" s="T387">hüːr-An</ta>
            <ta e="T389" id="Seg_5411" s="T388">is-BIT</ta>
            <ta e="T390" id="Seg_5412" s="T389">horok</ta>
            <ta e="T391" id="Seg_5413" s="T390">hibinnʼeː-LAr</ta>
            <ta e="T392" id="Seg_5414" s="T391">ol-nI</ta>
            <ta e="T393" id="Seg_5415" s="T392">kör-An-LAr</ta>
            <ta e="T394" id="Seg_5416" s="T393">biːr</ta>
            <ta e="T395" id="Seg_5417" s="T394">duruk</ta>
            <ta e="T396" id="Seg_5418" s="T395">köt-BIT-LAr</ta>
            <ta e="T397" id="Seg_5419" s="T396">kelin-LArI-ttAn</ta>
            <ta e="T398" id="Seg_5420" s="T397">kɨrdʼagas</ta>
            <ta e="T399" id="Seg_5421" s="T398">emeːksin</ta>
            <ta e="T400" id="Seg_5422" s="T399">hüːr-A</ta>
            <ta e="T401" id="Seg_5423" s="T400">hataː-BIT</ta>
            <ta e="T402" id="Seg_5424" s="T401">Tojoː</ta>
            <ta e="T403" id="Seg_5425" s="T402">tu͡ok-nI</ta>
            <ta e="T404" id="Seg_5426" s="T403">ere</ta>
            <ta e="T405" id="Seg_5427" s="T404">ü͡ögüleː-A-ü͡ögüleː-A</ta>
            <ta e="T406" id="Seg_5428" s="T405">hüːr-Ar-tI-n</ta>
            <ta e="T407" id="Seg_5429" s="T406">aːjɨ</ta>
            <ta e="T408" id="Seg_5430" s="T407">Tojoː</ta>
            <ta e="T409" id="Seg_5431" s="T408">dek</ta>
            <ta e="T410" id="Seg_5432" s="T409">tanʼak-tI-nAn</ta>
            <ta e="T411" id="Seg_5433" s="T410">dalaj-Ar</ta>
            <ta e="T412" id="Seg_5434" s="T411">hɨgɨnʼak</ta>
            <ta e="T413" id="Seg_5435" s="T412">ugučak-tA</ta>
            <ta e="T414" id="Seg_5436" s="T413">dʼüdeŋi</ta>
            <ta e="T415" id="Seg_5437" s="T414">bileː-LAːK</ta>
            <ta e="T416" id="Seg_5438" s="T415">uː-LAːK</ta>
            <ta e="T417" id="Seg_5439" s="T416">oŋkučak-GA</ta>
            <ta e="T418" id="Seg_5440" s="T417">laŋ</ta>
            <ta e="T419" id="Seg_5441" s="T418">gɨn-A</ta>
            <ta e="T420" id="Seg_5442" s="T419">tüs-BIT</ta>
            <ta e="T421" id="Seg_5443" s="T420">Tojoː</ta>
            <ta e="T422" id="Seg_5444" s="T421">hibinnʼeː-tI-n</ta>
            <ta e="T423" id="Seg_5445" s="T422">kɨtta</ta>
            <ta e="T424" id="Seg_5446" s="T423">tur-A</ta>
            <ta e="T425" id="Seg_5447" s="T424">hataː-An</ta>
            <ta e="T426" id="Seg_5448" s="T425">hɨt-BIT</ta>
            <ta e="T427" id="Seg_5449" s="T426">hɨraj-tA</ta>
            <ta e="T428" id="Seg_5450" s="T427">da</ta>
            <ta e="T429" id="Seg_5451" s="T428">tu͡ok-tA</ta>
            <ta e="T430" id="Seg_5452" s="T429">da</ta>
            <ta e="T431" id="Seg_5453" s="T430">köhün-I-BAT</ta>
            <ta e="T432" id="Seg_5454" s="T431">bu͡or</ta>
            <ta e="T433" id="Seg_5455" s="T432">bu͡ol-BIT</ta>
            <ta e="T434" id="Seg_5456" s="T433">tur-A</ta>
            <ta e="T435" id="Seg_5457" s="T434">hataː-An</ta>
            <ta e="T436" id="Seg_5458" s="T435">ele-tA</ta>
            <ta e="T437" id="Seg_5459" s="T436">bu͡ol-An</ta>
            <ta e="T438" id="Seg_5460" s="T437">emeːksin</ta>
            <ta e="T439" id="Seg_5461" s="T438">hit-BIT</ta>
            <ta e="T440" id="Seg_5462" s="T439">gini-nI</ta>
            <ta e="T441" id="Seg_5463" s="T440">Tojoː</ta>
            <ta e="T442" id="Seg_5464" s="T441">hɨraj-tI-n</ta>
            <ta e="T443" id="Seg_5465" s="T442">kör-An</ta>
            <ta e="T444" id="Seg_5466" s="T443">külümneː-BIT</ta>
            <ta e="T445" id="Seg_5467" s="T444">iliː-tI-nAn</ta>
            <ta e="T446" id="Seg_5468" s="T445">agaj</ta>
            <ta e="T447" id="Seg_5469" s="T446">hapsɨj-BIT</ta>
            <ta e="T448" id="Seg_5470" s="T447">hupturuj</ta>
            <ta e="T449" id="Seg_5471" s="T448">kantan</ta>
            <ta e="T450" id="Seg_5472" s="T449">kel-BIT-I-ŋ</ta>
            <ta e="T451" id="Seg_5473" s="T450">di͡e-BIT</ta>
            <ta e="T452" id="Seg_5474" s="T451">kördük</ta>
            <ta e="T453" id="Seg_5475" s="T452">Tojoː</ta>
            <ta e="T454" id="Seg_5476" s="T453">tur-A</ta>
            <ta e="T455" id="Seg_5477" s="T454">ekkireː-AːT</ta>
            <ta e="T456" id="Seg_5478" s="T455">ele</ta>
            <ta e="T457" id="Seg_5479" s="T456">küːs-tI-nAn</ta>
            <ta e="T458" id="Seg_5480" s="T457">tɨːn-tA</ta>
            <ta e="T459" id="Seg_5481" s="T458">hu͡ok</ta>
            <ta e="T460" id="Seg_5482" s="T459">dogor-LAr-tI-n</ta>
            <ta e="T461" id="Seg_5483" s="T460">dek</ta>
            <ta e="T462" id="Seg_5484" s="T461">tep-BIT</ta>
            <ta e="T463" id="Seg_5485" s="T462">araː</ta>
            <ta e="T464" id="Seg_5486" s="T463">arɨːččɨ</ta>
            <ta e="T465" id="Seg_5487" s="T464">öl-A</ta>
            <ta e="T466" id="Seg_5488" s="T465">hɨs-TI-m</ta>
            <ta e="T467" id="Seg_5489" s="T466">arɨːččɨ</ta>
            <ta e="T468" id="Seg_5490" s="T467">öl-A</ta>
            <ta e="T469" id="Seg_5491" s="T468">hɨs-TI-m</ta>
            <ta e="T470" id="Seg_5492" s="T469">učuːtal-LArA</ta>
            <ta e="T471" id="Seg_5493" s="T470">dʼirʼektar</ta>
            <ta e="T472" id="Seg_5494" s="T471">bu͡ol-An</ta>
            <ta e="T473" id="Seg_5495" s="T472">tu͡ok</ta>
            <ta e="T474" id="Seg_5496" s="T473">da</ta>
            <ta e="T475" id="Seg_5497" s="T474">di͡e-IAK-LArI-n</ta>
            <ta e="T476" id="Seg_5498" s="T475">bert-tI-ttAn</ta>
            <ta e="T477" id="Seg_5499" s="T476">ele</ta>
            <ta e="T478" id="Seg_5500" s="T477">is-LArI-nAn</ta>
            <ta e="T479" id="Seg_5501" s="T478">kül-A</ta>
            <ta e="T480" id="Seg_5502" s="T479">tur-BIT-LAr</ta>
            <ta e="T482" id="Seg_5503" s="T481">ele</ta>
            <ta e="T483" id="Seg_5504" s="T482">hanaː-tI-n</ta>
            <ta e="T484" id="Seg_5505" s="T483">haŋar-BIT</ta>
            <ta e="T485" id="Seg_5506" s="T484">kajdak</ta>
            <ta e="T486" id="Seg_5507" s="T485">da</ta>
            <ta e="T487" id="Seg_5508" s="T486">di͡e-IAK-GA</ta>
            <ta e="T488" id="Seg_5509" s="T487">bert</ta>
            <ta e="T489" id="Seg_5510" s="T488">olus</ta>
            <ta e="T490" id="Seg_5511" s="T489">menik</ta>
            <ta e="T491" id="Seg_5512" s="T490">dʼon</ta>
            <ta e="T492" id="Seg_5513" s="T491">kel-BIT-LAr</ta>
            <ta e="T494" id="Seg_5514" s="T492">bɨhɨːlaːk</ta>
            <ta e="T495" id="Seg_5515" s="T494">dogor-LAr-tI-GAr</ta>
            <ta e="T496" id="Seg_5516" s="T495">di͡e-BIT</ta>
            <ta e="T497" id="Seg_5517" s="T496">anɨ</ta>
            <ta e="T498" id="Seg_5518" s="T497">giniler-nI</ta>
            <ta e="T499" id="Seg_5519" s="T498">internat-GA</ta>
            <ta e="T500" id="Seg_5520" s="T499">ilin-I-ŋ</ta>
            <ta e="T501" id="Seg_5521" s="T500">onton</ta>
            <ta e="T502" id="Seg_5522" s="T501">huːn-TAr-I-ŋ</ta>
            <ta e="T503" id="Seg_5523" s="T502">as-LArI-n</ta>
            <ta e="T504" id="Seg_5524" s="T503">belemneː-IAK-LArI-GAr</ta>
            <ta e="T505" id="Seg_5525" s="T504">di͡eri</ta>
            <ta e="T506" id="Seg_5526" s="T505">barɨ-tI-n</ta>
            <ta e="T507" id="Seg_5527" s="T506">kɨčɨrɨ͡ak-LIk</ta>
            <ta e="T508" id="Seg_5528" s="T507">oŋor-I-ŋ</ta>
            <ta e="T509" id="Seg_5529" s="T508">ogo-LAr-nI</ta>
            <ta e="T510" id="Seg_5530" s="T509">ür.[t]-BAkkA</ta>
            <ta e="T511" id="Seg_5531" s="T510">ogo-LAr-nI</ta>
            <ta e="T512" id="Seg_5532" s="T511">honno</ta>
            <ta e="T513" id="Seg_5533" s="T512">üːr-An</ta>
            <ta e="T514" id="Seg_5534" s="T513">ilt-BIT-LAr</ta>
            <ta e="T515" id="Seg_5535" s="T514">köllör-BIT-LAr</ta>
            <ta e="T516" id="Seg_5536" s="T515">barɨ-LArI-GAr</ta>
            <ta e="T517" id="Seg_5537" s="T516">olor-Ar</ta>
            <ta e="T518" id="Seg_5538" s="T517">dʼi͡e-LArI-n</ta>
            <ta e="T519" id="Seg_5539" s="T518">nuːčča-LIː</ta>
            <ta e="T521" id="Seg_5540" s="T520">aːt-LAːgI</ta>
            <ta e="T522" id="Seg_5541" s="T521">hamaː-Ar</ta>
            <ta e="T523" id="Seg_5542" s="T522">haŋa</ta>
            <ta e="T524" id="Seg_5543" s="T523">ɨrbaːkɨ-LAr-nI</ta>
            <ta e="T525" id="Seg_5544" s="T524">atak-LAr-nI</ta>
            <ta e="T526" id="Seg_5545" s="T525">bi͡er-AːktAː-BIT-LAr</ta>
            <ta e="T527" id="Seg_5546" s="T526">barɨ</ta>
            <ta e="T528" id="Seg_5547" s="T527">kel-BIT</ta>
            <ta e="T529" id="Seg_5548" s="T528">dʼon</ta>
            <ta e="T530" id="Seg_5549" s="T529">as-LAr-tI-n</ta>
            <ta e="T531" id="Seg_5550" s="T530">kɨrpalaː-BIT-LAr</ta>
            <ta e="T532" id="Seg_5551" s="T531">tu͡ok-ttAn</ta>
            <ta e="T533" id="Seg_5552" s="T532">kɨːs</ta>
            <ta e="T534" id="Seg_5553" s="T533">ogo-LAr</ta>
            <ta e="T535" id="Seg_5554" s="T534">ɨtaː-Ar-LArA</ta>
            <ta e="T536" id="Seg_5555" s="T535">bert</ta>
            <ta e="T537" id="Seg_5556" s="T536">e-BIT</ta>
            <ta e="T538" id="Seg_5557" s="T537">as-LArI-n</ta>
            <ta e="T539" id="Seg_5558" s="T538">ahɨn-An-LAr</ta>
            <ta e="T540" id="Seg_5559" s="T539">Tojoː-nI</ta>
            <ta e="T541" id="Seg_5560" s="T540">emi͡e</ta>
            <ta e="T542" id="Seg_5561" s="T541">kɨrpalaː-BIT-LAr</ta>
            <ta e="T543" id="Seg_5562" s="T542">taŋɨn-Ar</ta>
            <ta e="T544" id="Seg_5563" s="T543">taŋas</ta>
            <ta e="T545" id="Seg_5564" s="T544">bi͡er-BIT-LAr</ta>
            <ta e="T546" id="Seg_5565" s="T545">ket-Ar</ta>
            <ta e="T547" id="Seg_5566" s="T546">tunʼak-LArI-n</ta>
            <ta e="T548" id="Seg_5567" s="T547">tuttar-BIT-LAr</ta>
            <ta e="T549" id="Seg_5568" s="T548">kɨrabaːt-tI-GAr</ta>
            <ta e="T550" id="Seg_5569" s="T549">kel-AːT</ta>
            <ta e="T551" id="Seg_5570" s="T550">olor-BIT</ta>
            <ta e="T552" id="Seg_5571" s="T551">körüleː-A</ta>
            <ta e="T553" id="Seg_5572" s="T552">tu͡ok-nI</ta>
            <ta e="T554" id="Seg_5573" s="T553">tuttar-BIT-LArI-n</ta>
            <ta e="T555" id="Seg_5574" s="T554">barɨ-tA</ta>
            <ta e="T556" id="Seg_5575" s="T555">gini-GA</ta>
            <ta e="T557" id="Seg_5576" s="T556">höp</ta>
            <ta e="T558" id="Seg_5577" s="T557">ol</ta>
            <ta e="T559" id="Seg_5578" s="T558">da</ta>
            <ta e="T560" id="Seg_5579" s="T559">bu͡ol-TAR</ta>
            <ta e="T561" id="Seg_5580" s="T560">togo</ta>
            <ta e="T562" id="Seg_5581" s="T561">giniler</ta>
            <ta e="T563" id="Seg_5582" s="T562">bi͡er-BIT-LArA</ta>
            <ta e="T564" id="Seg_5583" s="T563">bu͡olu͡o=Ij</ta>
            <ta e="T565" id="Seg_5584" s="T564">ikki-LIː</ta>
            <ta e="T566" id="Seg_5585" s="T565">ɨrbaːkɨ-nI</ta>
            <ta e="T567" id="Seg_5586" s="T566">ikki-LIː</ta>
            <ta e="T568" id="Seg_5587" s="T567">hɨ͡aldʼa-nI</ta>
            <ta e="T569" id="Seg_5588" s="T568">haŋa</ta>
            <ta e="T570" id="Seg_5589" s="T569">taŋas-LArI-n</ta>
            <ta e="T571" id="Seg_5590" s="T570">ket-Ar</ta>
            <ta e="T574" id="Seg_5591" s="T573">hanaː-BIT</ta>
            <ta e="T575" id="Seg_5592" s="T574">ogo-LAr-nI</ta>
            <ta e="T576" id="Seg_5593" s="T575">huːn-TAr-A</ta>
            <ta e="T577" id="Seg_5594" s="T576">ilt-BIT-LAr</ta>
            <ta e="T578" id="Seg_5595" s="T577">kanna</ta>
            <ta e="T579" id="Seg_5596" s="T578">menik</ta>
            <ta e="T580" id="Seg_5597" s="T579">dʼon</ta>
            <ta e="T581" id="Seg_5598" s="T580">ele</ta>
            <ta e="T582" id="Seg_5599" s="T581">hanaː-LArI-nAn</ta>
            <ta e="T583" id="Seg_5600" s="T582">ü͡örüːleːk-LIk</ta>
            <ta e="T584" id="Seg_5601" s="T583">kül-s-A-kül-s-A</ta>
            <ta e="T585" id="Seg_5602" s="T584">ataj</ta>
            <ta e="T586" id="Seg_5603" s="T585">ataj</ta>
            <ta e="T587" id="Seg_5604" s="T586">di͡e-A-s-A</ta>
            <ta e="T588" id="Seg_5605" s="T587">olor-BIT-LAr</ta>
            <ta e="T589" id="Seg_5606" s="T588">Tojoː</ta>
            <ta e="T590" id="Seg_5607" s="T589">huːn-An</ta>
            <ta e="T591" id="Seg_5608" s="T590">büt-AːT</ta>
            <ta e="T592" id="Seg_5609" s="T591">taŋɨn-I-BIT</ta>
            <ta e="T593" id="Seg_5610" s="T592">ket-BIT</ta>
            <ta e="T594" id="Seg_5611" s="T593">ürüŋ</ta>
            <ta e="T595" id="Seg_5612" s="T594">ɨrbaːkɨ-tI-n</ta>
            <ta e="T596" id="Seg_5613" s="T595">hɨ͡aldʼa-tI-n</ta>
            <ta e="T597" id="Seg_5614" s="T596">horok-LAr-tI-n</ta>
            <ta e="T598" id="Seg_5615" s="T597">hapaːs-LAː-An</ta>
            <ta e="T599" id="Seg_5616" s="T598">huːlaː-BIT</ta>
            <ta e="T600" id="Seg_5617" s="T599">kojut</ta>
            <ta e="T601" id="Seg_5618" s="T600">ket-AːrI</ta>
            <ta e="T602" id="Seg_5619" s="T601">et-tI-GAr</ta>
            <ta e="T603" id="Seg_5620" s="T602">ket-An</ta>
            <ta e="T604" id="Seg_5621" s="T603">ürüŋ</ta>
            <ta e="T605" id="Seg_5622" s="T604">taŋas-LAr-tI-n</ta>
            <ta e="T606" id="Seg_5623" s="T605">tu͡ok</ta>
            <ta e="T607" id="Seg_5624" s="T606">da</ta>
            <ta e="T608" id="Seg_5625" s="T607">bu͡ol-BAtAK</ta>
            <ta e="T609" id="Seg_5626" s="T608">kördük</ta>
            <ta e="T610" id="Seg_5627" s="T609">hɨrɨt-I-BIT</ta>
            <ta e="T611" id="Seg_5628" s="T610">dogor-LAr-tA</ta>
            <ta e="T612" id="Seg_5629" s="T611">da</ta>
            <ta e="T613" id="Seg_5630" s="T612">tu͡ok-nI</ta>
            <ta e="T614" id="Seg_5631" s="T613">da</ta>
            <ta e="T615" id="Seg_5632" s="T614">di͡e-BAT-LAr</ta>
            <ta e="T616" id="Seg_5633" s="T615">e-BIT</ta>
            <ta e="T617" id="Seg_5634" s="T616">gini-GA</ta>
            <ta e="T618" id="Seg_5635" s="T617">internat-GA</ta>
            <ta e="T619" id="Seg_5636" s="T618">üleleː-Ar</ta>
            <ta e="T620" id="Seg_5637" s="T619">biːr</ta>
            <ta e="T621" id="Seg_5638" s="T620">dʼaktar</ta>
            <ta e="T622" id="Seg_5639" s="T621">tu͡olkulat-BIT</ta>
            <ta e="T623" id="Seg_5640" s="T622">kajdak</ta>
            <ta e="T624" id="Seg_5641" s="T623">gini</ta>
            <ta e="T625" id="Seg_5642" s="T624">taŋɨn-Ar</ta>
            <ta e="T626" id="Seg_5643" s="T625">bu͡ol-IAK-tI-n</ta>
            <ta e="T627" id="Seg_5644" s="T626">ahaː-Ar</ta>
            <ta e="T628" id="Seg_5645" s="T627">kem-LArA</ta>
            <ta e="T629" id="Seg_5646" s="T628">kel-BIT</ta>
            <ta e="T630" id="Seg_5647" s="T629">ogo-LAr-nI</ta>
            <ta e="T631" id="Seg_5648" s="T630">olort-TAː-BIT-LAr</ta>
            <ta e="T634" id="Seg_5649" s="T633">uhun</ta>
            <ta e="T635" id="Seg_5650" s="T634">bagajɨ</ta>
            <ta e="T636" id="Seg_5651" s="T635">ostu͡ol-LAr</ta>
            <ta e="T637" id="Seg_5652" s="T636">ikki</ta>
            <ta e="T638" id="Seg_5653" s="T637">di͡ekki</ta>
            <ta e="T639" id="Seg_5654" s="T638">öttü-LArI-GAr</ta>
            <ta e="T640" id="Seg_5655" s="T639">ulakan</ta>
            <ta e="T641" id="Seg_5656" s="T640">ihit-LAr-GA</ta>
            <ta e="T642" id="Seg_5657" s="T641">uːr-I-LIN-I-BIT-LAr</ta>
            <ta e="T643" id="Seg_5658" s="T642">minnʼiges</ta>
            <ta e="T644" id="Seg_5659" s="T643">hɨt-LAːK</ta>
            <ta e="T645" id="Seg_5660" s="T644">keli͡ep-LAr</ta>
            <ta e="T646" id="Seg_5661" s="T645">ogo</ta>
            <ta e="T647" id="Seg_5662" s="T646">aːjɨ</ta>
            <ta e="T648" id="Seg_5663" s="T647">kut-AttAː-BIT-LAr</ta>
            <ta e="T649" id="Seg_5664" s="T648">balɨk</ta>
            <ta e="T650" id="Seg_5665" s="T649">min-tI-n</ta>
            <ta e="T651" id="Seg_5666" s="T650">lu͡osku-LAr-nI</ta>
            <ta e="T652" id="Seg_5667" s="T651">bi͡er-AːktAː-BIT-LAr</ta>
            <ta e="T653" id="Seg_5668" s="T652">kör-BIT</ta>
            <ta e="T654" id="Seg_5669" s="T653">ogo-LAr</ta>
            <ta e="T655" id="Seg_5670" s="T654">ahaː-An-LAr</ta>
            <ta e="T656" id="Seg_5671" s="T655">ketek-LArA</ta>
            <ta e="T657" id="Seg_5672" s="T656">agaj</ta>
            <ta e="T658" id="Seg_5673" s="T657">hüt-A</ta>
            <ta e="T659" id="Seg_5674" s="T658">hɨt-Ar</ta>
            <ta e="T660" id="Seg_5675" s="T659">Tojoː</ta>
            <ta e="T661" id="Seg_5676" s="T660">ahaː-A</ta>
            <ta e="T662" id="Seg_5677" s="T661">olor-An</ta>
            <ta e="T663" id="Seg_5678" s="T662">tu͡ok-nI</ta>
            <ta e="T664" id="Seg_5679" s="T663">ere</ta>
            <ta e="T665" id="Seg_5680" s="T664">hanaː-tA</ta>
            <ta e="T666" id="Seg_5681" s="T665">kel-BIT</ta>
            <ta e="T667" id="Seg_5682" s="T666">maːma-tI-n</ta>
            <ta e="T668" id="Seg_5683" s="T667">aga-tI-n</ta>
            <ta e="T669" id="Seg_5684" s="T668">Kɨča-tI-n</ta>
            <ta e="T670" id="Seg_5685" s="T669">öjdöː-An</ta>
            <ta e="T671" id="Seg_5686" s="T670">ahaː-An</ta>
            <ta e="T672" id="Seg_5687" s="T671">büt-AːT</ta>
            <ta e="T673" id="Seg_5688" s="T672">ogo-LAr</ta>
            <ta e="T674" id="Seg_5689" s="T673">Voločanka</ta>
            <ta e="T675" id="Seg_5690" s="T674">üstün</ta>
            <ta e="T676" id="Seg_5691" s="T675">oːnnʼoː-A</ta>
            <ta e="T677" id="Seg_5692" s="T676">hɨrɨt-I-BIT-LAr</ta>
            <ta e="T678" id="Seg_5693" s="T677">körüleː-A</ta>
            <ta e="T679" id="Seg_5694" s="T678">nuːčča</ta>
            <ta e="T680" id="Seg_5695" s="T679">dʼi͡e-LAr-nI</ta>
            <ta e="T681" id="Seg_5696" s="T680">barɨ</ta>
            <ta e="T682" id="Seg_5697" s="T681">ti͡ergen-nI</ta>
            <ta e="T683" id="Seg_5698" s="T682">giniler-GA</ta>
            <ta e="T684" id="Seg_5699" s="T683">barɨ-tA</ta>
            <ta e="T685" id="Seg_5700" s="T684">dʼikti</ta>
            <ta e="T686" id="Seg_5701" s="T685">haŋardɨː</ta>
            <ta e="T687" id="Seg_5702" s="T686">kel-BIT</ta>
            <ta e="T688" id="Seg_5703" s="T687">kihi-LAr-GA</ta>
            <ta e="T689" id="Seg_5704" s="T688">nöŋü͡ö</ta>
            <ta e="T690" id="Seg_5705" s="T689">kün</ta>
            <ta e="T691" id="Seg_5706" s="T690">emi͡e</ta>
            <ta e="T692" id="Seg_5707" s="T691">ulakan</ta>
            <ta e="T693" id="Seg_5708" s="T692">ogo-LAːK</ta>
            <ta e="T694" id="Seg_5709" s="T693">kös</ta>
            <ta e="T695" id="Seg_5710" s="T694">kel-BIT</ta>
            <ta e="T696" id="Seg_5711" s="T695">atɨn</ta>
            <ta e="T697" id="Seg_5712" s="T696">kolxoz-ttAn</ta>
            <ta e="T698" id="Seg_5713" s="T697">haŋardɨː</ta>
            <ta e="T699" id="Seg_5714" s="T698">kel-A</ta>
            <ta e="T700" id="Seg_5715" s="T699">ogo-LAr</ta>
            <ta e="T701" id="Seg_5716" s="T700">kiːrbikteːk-tA</ta>
            <ta e="T702" id="Seg_5717" s="T701">bert</ta>
            <ta e="T703" id="Seg_5718" s="T702">barɨ-ttAn</ta>
            <ta e="T705" id="Seg_5719" s="T704">kuttan-Ar</ta>
            <ta e="T706" id="Seg_5720" s="T705">kördük-LAr</ta>
            <ta e="T707" id="Seg_5721" s="T706">giniler</ta>
            <ta e="T708" id="Seg_5722" s="T707">olus</ta>
            <ta e="T709" id="Seg_5723" s="T708">ör</ta>
            <ta e="T710" id="Seg_5724" s="T709">itigirdik</ta>
            <ta e="T711" id="Seg_5725" s="T710">bu͡ol-BAtAK-LAr</ta>
            <ta e="T712" id="Seg_5726" s="T711">emi͡e</ta>
            <ta e="T713" id="Seg_5727" s="T712">Tojoː</ta>
            <ta e="T714" id="Seg_5728" s="T713">Miteː</ta>
            <ta e="T715" id="Seg_5729" s="T714">Ujbaːn</ta>
            <ta e="T716" id="Seg_5730" s="T715">kördük</ta>
            <ta e="T717" id="Seg_5731" s="T716">tolun-I-BAT</ta>
            <ta e="T718" id="Seg_5732" s="T717">mi͡ere-nI</ta>
            <ta e="T719" id="Seg_5733" s="T718">ɨl-BIT-LAr</ta>
            <ta e="T720" id="Seg_5734" s="T719">haŋa</ta>
            <ta e="T721" id="Seg_5735" s="T720">dogor-LAr-tI-n</ta>
            <ta e="T722" id="Seg_5736" s="T721">kör-An-LAr</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_5737" s="T0">listen-IMP.2PL</ta>
            <ta e="T2" id="Seg_5738" s="T1">story-ACC</ta>
            <ta e="T3" id="Seg_5739" s="T2">miracle.[NOM]</ta>
            <ta e="T4" id="Seg_5740" s="T3">reindeer-PL.[NOM]</ta>
            <ta e="T5" id="Seg_5741" s="T4">three-ORD</ta>
            <ta e="T6" id="Seg_5742" s="T5">day-3SG-DAT/LOC</ta>
            <ta e="T7" id="Seg_5743" s="T6">child-PROPR</ta>
            <ta e="T8" id="Seg_5744" s="T7">reindeer.caravan.[NOM]</ta>
            <ta e="T9" id="Seg_5745" s="T8">reach-PST2.[3SG]</ta>
            <ta e="T10" id="Seg_5746" s="T9">Volochanka-DAT/LOC</ta>
            <ta e="T11" id="Seg_5747" s="T10">Ust_Avam.[NOM]</ta>
            <ta e="T12" id="Seg_5748" s="T11">rayon-3SG-GEN</ta>
            <ta e="T13" id="Seg_5749" s="T12">centre.[NOM]</ta>
            <ta e="T14" id="Seg_5750" s="T13">settlement-3SG-GEN</ta>
            <ta e="T15" id="Seg_5751" s="T14">Russian-3PL.[NOM]</ta>
            <ta e="T16" id="Seg_5752" s="T15">Dolgan-3PL.[NOM]</ta>
            <ta e="T17" id="Seg_5753" s="T16">every-3PL.[NOM]</ta>
            <ta e="T18" id="Seg_5754" s="T17">gather-PASS/REFL-EP-PST2-3PL</ta>
            <ta e="T19" id="Seg_5755" s="T18">come-NMNZ.[NOM]</ta>
            <ta e="T20" id="Seg_5756" s="T19">child-PL-ACC</ta>
            <ta e="T21" id="Seg_5757" s="T20">see-CVB.PURP</ta>
            <ta e="T22" id="Seg_5758" s="T21">doll.[NOM]</ta>
            <ta e="T23" id="Seg_5759" s="T22">similar</ta>
            <ta e="T24" id="Seg_5760" s="T23">clothes-PROPR-PL-ACC</ta>
            <ta e="T25" id="Seg_5761" s="T24">human.being.[NOM]</ta>
            <ta e="T26" id="Seg_5762" s="T25">love-PTCP.FUT-PL-ACC</ta>
            <ta e="T27" id="Seg_5763" s="T26">very</ta>
            <ta e="T28" id="Seg_5764" s="T27">noise-POSS</ta>
            <ta e="T29" id="Seg_5765" s="T28">NEG</ta>
            <ta e="T30" id="Seg_5766" s="T29">quite</ta>
            <ta e="T31" id="Seg_5767" s="T30">Volochanka.[NOM]</ta>
            <ta e="T32" id="Seg_5768" s="T31">life.[NOM]</ta>
            <ta e="T33" id="Seg_5769" s="T32">one</ta>
            <ta e="T34" id="Seg_5770" s="T33">moment.[NOM]</ta>
            <ta e="T35" id="Seg_5771" s="T34">revive-EP-PTCP.PST.[NOM]</ta>
            <ta e="T36" id="Seg_5772" s="T35">similar</ta>
            <ta e="T37" id="Seg_5773" s="T36">god.[NOM]</ta>
            <ta e="T38" id="Seg_5774" s="T37">day-3SG-ACC</ta>
            <ta e="T39" id="Seg_5775" s="T38">mark-VBZ-PTCP.PRS.[NOM]</ta>
            <ta e="T40" id="Seg_5776" s="T39">similar</ta>
            <ta e="T41" id="Seg_5777" s="T40">be-CVB.SEQ</ta>
            <ta e="T42" id="Seg_5778" s="T41">stay-PST2.[3SG]</ta>
            <ta e="T43" id="Seg_5779" s="T42">child-PL.[NOM]</ta>
            <ta e="T44" id="Seg_5780" s="T43">though</ta>
            <ta e="T45" id="Seg_5781" s="T44">human.being-PL-ACC</ta>
            <ta e="T46" id="Seg_5782" s="T45">see-NEG-3PL</ta>
            <ta e="T47" id="Seg_5783" s="T46">Russian</ta>
            <ta e="T48" id="Seg_5784" s="T47">house-PL.[NOM]</ta>
            <ta e="T49" id="Seg_5785" s="T48">to</ta>
            <ta e="T50" id="Seg_5786" s="T49">only</ta>
            <ta e="T53" id="Seg_5787" s="T52">eye-3PL.[NOM]</ta>
            <ta e="T54" id="Seg_5788" s="T53">street.[NOM]</ta>
            <ta e="T55" id="Seg_5789" s="T54">along</ta>
            <ta e="T56" id="Seg_5790" s="T55">gallop-EP-CAUS-CVB.SEQ</ta>
            <ta e="T57" id="Seg_5791" s="T56">go-CVB.SEQ-3PL</ta>
            <ta e="T58" id="Seg_5792" s="T57">riding.reindeer-EP-INSTR</ta>
            <ta e="T59" id="Seg_5793" s="T58">grandfather-3PL.[NOM]</ta>
            <ta e="T60" id="Seg_5794" s="T59">Anisim</ta>
            <ta e="T61" id="Seg_5795" s="T60">old.man.[NOM]</ta>
            <ta e="T62" id="Seg_5796" s="T61">stand-PST2.[3SG]</ta>
            <ta e="T63" id="Seg_5797" s="T62">school.[NOM]</ta>
            <ta e="T64" id="Seg_5798" s="T63">place.beneath-DAT/LOC</ta>
            <ta e="T65" id="Seg_5799" s="T64">3SG-ACC</ta>
            <ta e="T66" id="Seg_5800" s="T65">around</ta>
            <ta e="T67" id="Seg_5801" s="T66">noise-PROPR</ta>
            <ta e="T68" id="Seg_5802" s="T67">army.[NOM]</ta>
            <ta e="T69" id="Seg_5803" s="T68">riding.reindeer-3PL-INSTR</ta>
            <ta e="T72" id="Seg_5804" s="T71">upper.part-3PL-ABL</ta>
            <ta e="T73" id="Seg_5805" s="T72">jump-PST2-3PL</ta>
            <ta e="T74" id="Seg_5806" s="T73">school.[NOM]</ta>
            <ta e="T75" id="Seg_5807" s="T74">place.beneath-DAT/LOC</ta>
            <ta e="T76" id="Seg_5808" s="T75">stand-PRS-3PL</ta>
            <ta e="T79" id="Seg_5809" s="T78">teach-PTCP.PRS</ta>
            <ta e="T80" id="Seg_5810" s="T79">people.[NOM]</ta>
            <ta e="T81" id="Seg_5811" s="T80">leader-3PL.[NOM]</ta>
            <ta e="T82" id="Seg_5812" s="T81">there</ta>
            <ta e="T83" id="Seg_5813" s="T82">boarding.school.[NOM]</ta>
            <ta e="T84" id="Seg_5814" s="T83">worker-PL-3SG.[NOM]</ta>
            <ta e="T85" id="Seg_5815" s="T84">every-3PL.[NOM]</ta>
            <ta e="T86" id="Seg_5816" s="T85">child-PL-ACC</ta>
            <ta e="T87" id="Seg_5817" s="T86">catch-CVB.SEQ</ta>
            <ta e="T88" id="Seg_5818" s="T87">take-CVB.SEQ-3PL</ta>
            <ta e="T89" id="Seg_5819" s="T88">hello-VBZ-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T90" id="Seg_5820" s="T89">what-ACC</ta>
            <ta e="T91" id="Seg_5821" s="T90">INDEF</ta>
            <ta e="T92" id="Seg_5822" s="T91">3PL-DAT/LOC</ta>
            <ta e="T93" id="Seg_5823" s="T92">speak-PRS-3PL</ta>
            <ta e="T94" id="Seg_5824" s="T93">child-PL.[NOM]</ta>
            <ta e="T95" id="Seg_5825" s="T94">Russian</ta>
            <ta e="T96" id="Seg_5826" s="T95">language-3SG-ACC</ta>
            <ta e="T97" id="Seg_5827" s="T96">understand-NEG-3PL</ta>
            <ta e="T98" id="Seg_5828" s="T97">that.[NOM]</ta>
            <ta e="T99" id="Seg_5829" s="T98">because.of</ta>
            <ta e="T100" id="Seg_5830" s="T99">come-CVB.SIM</ta>
            <ta e="T101" id="Seg_5831" s="T100">small</ta>
            <ta e="T102" id="Seg_5832" s="T101">people.[NOM]</ta>
            <ta e="T103" id="Seg_5833" s="T102">chat-NEG-3PL</ta>
            <ta e="T104" id="Seg_5834" s="T103">watch-PTCP.PRS</ta>
            <ta e="T105" id="Seg_5835" s="T104">only</ta>
            <ta e="T106" id="Seg_5836" s="T105">be-PST2-3PL</ta>
            <ta e="T107" id="Seg_5837" s="T106">teacher-3PL-GEN</ta>
            <ta e="T108" id="Seg_5838" s="T107">face-3PL-ACC</ta>
            <ta e="T109" id="Seg_5839" s="T108">3PL.[NOM]</ta>
            <ta e="T110" id="Seg_5840" s="T109">very</ta>
            <ta e="T111" id="Seg_5841" s="T110">interesting-ACC</ta>
            <ta e="T112" id="Seg_5842" s="T111">see-PST2-3PL</ta>
            <ta e="T113" id="Seg_5843" s="T112">very</ta>
            <ta e="T114" id="Seg_5844" s="T113">big</ta>
            <ta e="T115" id="Seg_5845" s="T114">Russian</ta>
            <ta e="T116" id="Seg_5846" s="T115">house-PL-ACC</ta>
            <ta e="T117" id="Seg_5847" s="T116">different</ta>
            <ta e="T118" id="Seg_5848" s="T117">human.being-PL-ACC</ta>
            <ta e="T119" id="Seg_5849" s="T118">Russian-PL-ACC</ta>
            <ta e="T120" id="Seg_5850" s="T119">different</ta>
            <ta e="T121" id="Seg_5851" s="T120">language-PROPR-PL-ACC</ta>
            <ta e="T122" id="Seg_5852" s="T121">clothes-PROPR-PL-ACC</ta>
            <ta e="T123" id="Seg_5853" s="T122">foot-3PL-DAT/LOC</ta>
            <ta e="T124" id="Seg_5854" s="T123">though</ta>
            <ta e="T125" id="Seg_5855" s="T124">reindeer.[NOM]</ta>
            <ta e="T126" id="Seg_5856" s="T125">hoof-3SG.[NOM]</ta>
            <ta e="T127" id="Seg_5857" s="T126">similar</ta>
            <ta e="T128" id="Seg_5858" s="T127">shoes-ACC</ta>
            <ta e="T129" id="Seg_5859" s="T128">put.on-EP-MED-PST2-3PL</ta>
            <ta e="T130" id="Seg_5860" s="T129">man.[NOM]</ta>
            <ta e="T131" id="Seg_5861" s="T130">human.being-PL.[NOM]</ta>
            <ta e="T132" id="Seg_5862" s="T131">head-3PL-DAT/LOC</ta>
            <ta e="T133" id="Seg_5863" s="T132">mum-3PL-GEN</ta>
            <ta e="T134" id="Seg_5864" s="T133">scoop-3SG-ACC</ta>
            <ta e="T135" id="Seg_5865" s="T134">similar</ta>
            <ta e="T136" id="Seg_5866" s="T135">cap-ACC</ta>
            <ta e="T137" id="Seg_5867" s="T136">dress-CVB.SIM</ta>
            <ta e="T138" id="Seg_5868" s="T137">go-PRS-3PL</ta>
            <ta e="T139" id="Seg_5869" s="T138">woman-PL-3SG.[NOM]</ta>
            <ta e="T140" id="Seg_5870" s="T139">plaited-POSS</ta>
            <ta e="T141" id="Seg_5871" s="T140">NEG-3PL</ta>
            <ta e="T142" id="Seg_5872" s="T141">hair-3PL.[NOM]</ta>
            <ta e="T143" id="Seg_5873" s="T142">cut-EP-PASS/REFL-EP-PTCP.PST-PL.[NOM]</ta>
            <ta e="T144" id="Seg_5874" s="T143">man.[NOM]</ta>
            <ta e="T145" id="Seg_5875" s="T144">human.being.[NOM]</ta>
            <ta e="T146" id="Seg_5876" s="T145">similar</ta>
            <ta e="T147" id="Seg_5877" s="T146">dress-3PL.[NOM]</ta>
            <ta e="T148" id="Seg_5878" s="T147">lower</ta>
            <ta e="T149" id="Seg_5879" s="T148">side-3SG-DAT/LOC</ta>
            <ta e="T150" id="Seg_5880" s="T149">very</ta>
            <ta e="T151" id="Seg_5881" s="T150">sewed.tightly.[NOM]</ta>
            <ta e="T152" id="Seg_5882" s="T151">that.[NOM]</ta>
            <ta e="T153" id="Seg_5883" s="T152">because.of</ta>
            <ta e="T154" id="Seg_5884" s="T153">3PL.[NOM]</ta>
            <ta e="T155" id="Seg_5885" s="T154">hardly</ta>
            <ta e="T156" id="Seg_5886" s="T155">walk-PRS-3PL</ta>
            <ta e="T157" id="Seg_5887" s="T156">Toyoo.[NOM]</ta>
            <ta e="T158" id="Seg_5888" s="T157">very</ta>
            <ta e="T159" id="Seg_5889" s="T158">watch-CVB.SIM</ta>
            <ta e="T160" id="Seg_5890" s="T159">stand-PST2.[3SG]</ta>
            <ta e="T161" id="Seg_5891" s="T160">woman-PL-ACC</ta>
            <ta e="T162" id="Seg_5892" s="T161">inside-3SG-DAT/LOC</ta>
            <ta e="T163" id="Seg_5893" s="T162">think-PRS.[3SG]</ta>
            <ta e="T164" id="Seg_5894" s="T163">probably</ta>
            <ta e="T165" id="Seg_5895" s="T164">such</ta>
            <ta e="T166" id="Seg_5896" s="T165">dress-INSTR</ta>
            <ta e="T167" id="Seg_5897" s="T166">run-CVB.SIM</ta>
            <ta e="T168" id="Seg_5898" s="T167">not.manage-NEG-3PL</ta>
            <ta e="T169" id="Seg_5899" s="T168">few</ta>
            <ta e="T170" id="Seg_5900" s="T169">just</ta>
            <ta e="T171" id="Seg_5901" s="T170">walk-PRS-3PL</ta>
            <ta e="T172" id="Seg_5902" s="T171">be-PST2.[3SG]</ta>
            <ta e="T173" id="Seg_5903" s="T172">friend-PL-3SG-DAT/LOC</ta>
            <ta e="T174" id="Seg_5904" s="T173">what-ACC</ta>
            <ta e="T175" id="Seg_5905" s="T174">INDEF</ta>
            <ta e="T176" id="Seg_5906" s="T175">that.[NOM]</ta>
            <ta e="T177" id="Seg_5907" s="T176">side-3SG-INSTR</ta>
            <ta e="T178" id="Seg_5908" s="T177">tell-CVB.PURP</ta>
            <ta e="T179" id="Seg_5909" s="T178">want-PST2.[3SG]</ta>
            <ta e="T180" id="Seg_5910" s="T179">mouth-3SG-ACC</ta>
            <ta e="T181" id="Seg_5911" s="T180">just</ta>
            <ta e="T182" id="Seg_5912" s="T181">open-PST2.[3SG]</ta>
            <ta e="T183" id="Seg_5913" s="T182">what.[NOM]</ta>
            <ta e="T184" id="Seg_5914" s="T183">INDEF</ta>
            <ta e="T185" id="Seg_5915" s="T184">noise-3SG.[NOM]</ta>
            <ta e="T186" id="Seg_5916" s="T185">become-PST2.[3SG]</ta>
            <ta e="T187" id="Seg_5917" s="T186">see-PST2.[3SG]</ta>
            <ta e="T188" id="Seg_5918" s="T187">that.[NOM]</ta>
            <ta e="T189" id="Seg_5919" s="T188">to</ta>
            <ta e="T190" id="Seg_5920" s="T189">where.from</ta>
            <ta e="T191" id="Seg_5921" s="T190">noise.[NOM]</ta>
            <ta e="T192" id="Seg_5922" s="T191">go.out-EP-PST2.[3SG]</ta>
            <ta e="T195" id="Seg_5923" s="T194">what.[NOM]</ta>
            <ta e="T196" id="Seg_5924" s="T195">INDEF</ta>
            <ta e="T197" id="Seg_5925" s="T196">human.being.[NOM]</ta>
            <ta e="T198" id="Seg_5926" s="T197">see-NEG.PTCP.PST</ta>
            <ta e="T199" id="Seg_5927" s="T198">sledge-PROPR-3SG.[NOM]</ta>
            <ta e="T200" id="Seg_5928" s="T199">choose-MED-EP-CAUS-CVB.SEQ</ta>
            <ta e="T201" id="Seg_5929" s="T200">go-PST2.[3SG]</ta>
            <ta e="T202" id="Seg_5930" s="T201">sledge-3SG.[NOM]</ta>
            <ta e="T203" id="Seg_5931" s="T202">wondrous.[NOM]</ta>
            <ta e="T204" id="Seg_5932" s="T203">reindeer-3SG.[NOM]</ta>
            <ta e="T205" id="Seg_5933" s="T204">though</ta>
            <ta e="T206" id="Seg_5934" s="T205">big-3SG.[NOM]</ta>
            <ta e="T207" id="Seg_5935" s="T206">very</ta>
            <ta e="T208" id="Seg_5936" s="T207">very</ta>
            <ta e="T209" id="Seg_5937" s="T208">long</ta>
            <ta e="T210" id="Seg_5938" s="T209">wave-PTCP.PRS</ta>
            <ta e="T211" id="Seg_5939" s="T210">similar</ta>
            <ta e="T212" id="Seg_5940" s="T211">tail-PROPR.[NOM]</ta>
            <ta e="T213" id="Seg_5941" s="T212">sledge-VBZ-PTCP.PRS</ta>
            <ta e="T214" id="Seg_5942" s="T213">human.being-3SG.[NOM]</ta>
            <ta e="T215" id="Seg_5943" s="T214">though</ta>
            <ta e="T216" id="Seg_5944" s="T215">face-3SG.[NOM]</ta>
            <ta e="T217" id="Seg_5945" s="T216">animal.hair-3SG.[NOM]</ta>
            <ta e="T218" id="Seg_5946" s="T217">very</ta>
            <ta e="T219" id="Seg_5947" s="T218">furry</ta>
            <ta e="T220" id="Seg_5948" s="T219">beard-PROPR.[NOM]</ta>
            <ta e="T221" id="Seg_5949" s="T220">reindeer-3SG-GEN</ta>
            <ta e="T222" id="Seg_5950" s="T221">back-3SG-ABL</ta>
            <ta e="T223" id="Seg_5951" s="T222">by.foot</ta>
            <ta e="T224" id="Seg_5952" s="T223">walk-CVB.SEQ</ta>
            <ta e="T225" id="Seg_5953" s="T224">go-CVB.SEQ</ta>
            <ta e="T226" id="Seg_5954" s="T225">hold-PST2.[3SG]</ta>
            <ta e="T227" id="Seg_5955" s="T226">two</ta>
            <ta e="T228" id="Seg_5956" s="T227">rein-ACC</ta>
            <ta e="T229" id="Seg_5957" s="T228">reindeer-3SG.[NOM]</ta>
            <ta e="T230" id="Seg_5958" s="T229">how</ta>
            <ta e="T231" id="Seg_5959" s="T230">INDEF</ta>
            <ta e="T232" id="Seg_5960" s="T231">stride-PTCP.PRS-3SG-ACC</ta>
            <ta e="T233" id="Seg_5961" s="T232">every</ta>
            <ta e="T234" id="Seg_5962" s="T233">head-3SG-ACC</ta>
            <ta e="T235" id="Seg_5963" s="T234">move-CAUS-PRS.[3SG]</ta>
            <ta e="T236" id="Seg_5964" s="T235">that.[NOM]</ta>
            <ta e="T237" id="Seg_5965" s="T236">every</ta>
            <ta e="T238" id="Seg_5966" s="T237">sledge-AG.[NOM]</ta>
            <ta e="T239" id="Seg_5967" s="T238">noo</ta>
            <ta e="T240" id="Seg_5968" s="T239">noo</ta>
            <ta e="T241" id="Seg_5969" s="T240">say-PRS.[3SG]</ta>
            <ta e="T242" id="Seg_5970" s="T241">tale-ACC</ta>
            <ta e="T243" id="Seg_5971" s="T242">listen-PTCP.PRS.[NOM]</ta>
            <ta e="T244" id="Seg_5972" s="T243">similar</ta>
            <ta e="T245" id="Seg_5973" s="T244">strong</ta>
            <ta e="T246" id="Seg_5974" s="T245">very-ADVZ</ta>
            <ta e="T247" id="Seg_5975" s="T246">mouth-3SG-INSTR</ta>
            <ta e="T248" id="Seg_5976" s="T247">test-PST2.[3SG]</ta>
            <ta e="T249" id="Seg_5977" s="T248">sweet</ta>
            <ta e="T250" id="Seg_5978" s="T249">fat-ACC</ta>
            <ta e="T251" id="Seg_5979" s="T250">eat-PTCP.PRS.[NOM]</ta>
            <ta e="T252" id="Seg_5980" s="T251">similar</ta>
            <ta e="T253" id="Seg_5981" s="T252">well</ta>
            <ta e="T254" id="Seg_5982" s="T253">that.[NOM]</ta>
            <ta e="T255" id="Seg_5983" s="T254">what.[NOM]</ta>
            <ta e="T256" id="Seg_5984" s="T255">reindeer-3SG.[NOM]</ta>
            <ta e="T257" id="Seg_5985" s="T256">be-PRS.[3SG]</ta>
            <ta e="T258" id="Seg_5986" s="T257">see-CVB.SIM</ta>
            <ta e="T259" id="Seg_5987" s="T258">stand-CVB.SEQ</ta>
            <ta e="T260" id="Seg_5988" s="T259">be.happy-CVB.COND</ta>
            <ta e="T261" id="Seg_5989" s="T260">Toyoo.[NOM]</ta>
            <ta e="T262" id="Seg_5990" s="T261">voice.[NOM]</ta>
            <ta e="T263" id="Seg_5991" s="T262">occur-EP-PST2.[3SG]</ta>
            <ta e="T264" id="Seg_5992" s="T263">3SG.[NOM]</ta>
            <ta e="T265" id="Seg_5993" s="T264">only</ta>
            <ta e="T266" id="Seg_5994" s="T265">that-ACC</ta>
            <ta e="T267" id="Seg_5995" s="T266">see-CVB.SEQ</ta>
            <ta e="T268" id="Seg_5996" s="T267">stand-PST2.NEG-3SG</ta>
            <ta e="T269" id="Seg_5997" s="T268">every</ta>
            <ta e="T270" id="Seg_5998" s="T269">child-PL.[NOM]</ta>
            <ta e="T271" id="Seg_5999" s="T270">miracle-ADJZ.[NOM]</ta>
            <ta e="T272" id="Seg_6000" s="T271">become-PST2-3PL</ta>
            <ta e="T273" id="Seg_6001" s="T272">self.[NOM]</ta>
            <ta e="T274" id="Seg_6002" s="T273">self-3PL-ACC</ta>
            <ta e="T275" id="Seg_6003" s="T274">with</ta>
            <ta e="T276" id="Seg_6004" s="T275">chat-CVB.SIM</ta>
            <ta e="T277" id="Seg_6005" s="T276">not.yet-3PL</ta>
            <ta e="T278" id="Seg_6006" s="T277">again</ta>
            <ta e="T279" id="Seg_6007" s="T278">see-PST2-3PL</ta>
            <ta e="T280" id="Seg_6008" s="T279">new</ta>
            <ta e="T281" id="Seg_6009" s="T280">miracle-ACC</ta>
            <ta e="T282" id="Seg_6010" s="T281">old</ta>
            <ta e="T283" id="Seg_6011" s="T282">Russian</ta>
            <ta e="T284" id="Seg_6012" s="T283">old.woman-3SG.[NOM]</ta>
            <ta e="T285" id="Seg_6013" s="T284">what.[NOM]</ta>
            <ta e="T286" id="Seg_6014" s="T285">INDEF</ta>
            <ta e="T287" id="Seg_6015" s="T286">small</ta>
            <ta e="T288" id="Seg_6016" s="T287">reindeer-DIM-PL-3SG-ACC</ta>
            <ta e="T289" id="Seg_6017" s="T288">hunt-CVB.SEQ</ta>
            <ta e="T290" id="Seg_6018" s="T289">go-PST2.[3SG]</ta>
            <ta e="T291" id="Seg_6019" s="T290">naked</ta>
            <ta e="T292" id="Seg_6020" s="T291">without.antlers</ta>
            <ta e="T293" id="Seg_6021" s="T292">reindeer-DIM-PL.[NOM]</ta>
            <ta e="T294" id="Seg_6022" s="T293">low</ta>
            <ta e="T295" id="Seg_6023" s="T294">leg-3PL-INSTR</ta>
            <ta e="T296" id="Seg_6024" s="T295">run-FREQ-CVB.SEQ</ta>
            <ta e="T297" id="Seg_6025" s="T296">go-PST2-3PL</ta>
            <ta e="T298" id="Seg_6026" s="T297">cry.loudly-CVB.SIM-cry.loudly-CVB.SIM</ta>
            <ta e="T299" id="Seg_6027" s="T298">that-3SG-3PL.[NOM]</ta>
            <ta e="T300" id="Seg_6028" s="T299">run-CVB.SEQ</ta>
            <ta e="T301" id="Seg_6029" s="T300">go-CVB.SEQ-3PL</ta>
            <ta e="T302" id="Seg_6030" s="T301">that-this</ta>
            <ta e="T303" id="Seg_6031" s="T302">earth.[NOM]</ta>
            <ta e="T304" id="Seg_6032" s="T303">to</ta>
            <ta e="T305" id="Seg_6033" s="T304">scatter-PRS-3PL</ta>
            <ta e="T306" id="Seg_6034" s="T305">jeez</ta>
            <ta e="T307" id="Seg_6035" s="T306">say-CVB.SEQ</ta>
            <ta e="T308" id="Seg_6036" s="T307">shout-PST2.[3SG]</ta>
            <ta e="T309" id="Seg_6037" s="T308">Toyoo.[NOM]</ta>
            <ta e="T310" id="Seg_6038" s="T309">naked</ta>
            <ta e="T311" id="Seg_6039" s="T310">wild.reindeer-PL.[NOM]</ta>
            <ta e="T312" id="Seg_6040" s="T311">child-PL.[NOM]</ta>
            <ta e="T313" id="Seg_6041" s="T312">last</ta>
            <ta e="T314" id="Seg_6042" s="T313">belly-3PL-INSTR</ta>
            <ta e="T315" id="Seg_6043" s="T314">laugh-RECP/COLL-PRS-3PL</ta>
            <ta e="T316" id="Seg_6044" s="T315">finger-3PL-INSTR</ta>
            <ta e="T317" id="Seg_6045" s="T316">see-CAUS-CVB.SIM-see-CAUS-CVB.SIM</ta>
            <ta e="T318" id="Seg_6046" s="T317">see.[IMP.2SG]</ta>
            <ta e="T319" id="Seg_6047" s="T318">friend</ta>
            <ta e="T320" id="Seg_6048" s="T319">nose-3PL.[NOM]</ta>
            <ta e="T321" id="Seg_6049" s="T320">driving.pole.[NOM]</ta>
            <ta e="T322" id="Seg_6050" s="T321">ring.at.the.end.of.driving.pole-3SG-ACC</ta>
            <ta e="T323" id="Seg_6051" s="T322">similar</ta>
            <ta e="T324" id="Seg_6052" s="T323">that.[NOM]</ta>
            <ta e="T325" id="Seg_6053" s="T324">just</ta>
            <ta e="T326" id="Seg_6054" s="T325">two</ta>
            <ta e="T327" id="Seg_6055" s="T326">just</ta>
            <ta e="T328" id="Seg_6056" s="T327">hole-PROPR-3PL</ta>
            <ta e="T329" id="Seg_6057" s="T328">reindeer.[NOM]</ta>
            <ta e="T330" id="Seg_6058" s="T329">such</ta>
            <ta e="T331" id="Seg_6059" s="T330">be-PST2.NEG.[3SG]</ta>
            <ta e="T332" id="Seg_6060" s="T331">our.[NOM]</ta>
            <ta e="T333" id="Seg_6061" s="T332">beautiful-PL.[NOM]</ta>
            <ta e="T334" id="Seg_6062" s="T333">at.first</ta>
            <ta e="T335" id="Seg_6063" s="T334">be.afraid-CVB.SIM</ta>
            <ta e="T336" id="Seg_6064" s="T335">beat-PST2-3PL</ta>
            <ta e="T337" id="Seg_6065" s="T336">then</ta>
            <ta e="T338" id="Seg_6066" s="T337">come.closer-PST2-3PL</ta>
            <ta e="T339" id="Seg_6067" s="T338">good-ADVZ</ta>
            <ta e="T340" id="Seg_6068" s="T339">see-CVB.PURP</ta>
            <ta e="T341" id="Seg_6069" s="T340">Toyoo.[NOM]</ta>
            <ta e="T342" id="Seg_6070" s="T341">self-3SG-GEN</ta>
            <ta e="T343" id="Seg_6071" s="T342">temper-3SG-ACC</ta>
            <ta e="T344" id="Seg_6072" s="T343">win-PST2.NEG.[3SG]</ta>
            <ta e="T345" id="Seg_6073" s="T344">one-3PL-DAT/LOC</ta>
            <ta e="T346" id="Seg_6074" s="T345">run-CVB.SEQ</ta>
            <ta e="T347" id="Seg_6075" s="T346">come-CVB.SEQ</ta>
            <ta e="T348" id="Seg_6076" s="T347">upper.part-3SG-DAT/LOC</ta>
            <ta e="T349" id="Seg_6077" s="T348">riding.reindeer-VBZ-CVB.SIM</ta>
            <ta e="T350" id="Seg_6078" s="T349">make-CVB.SIM</ta>
            <ta e="T351" id="Seg_6079" s="T350">jump-PST2.[3SG]</ta>
            <ta e="T352" id="Seg_6080" s="T351">wild.reindeer-3SG.[NOM]</ta>
            <ta e="T353" id="Seg_6081" s="T352">back-3SG-DAT/LOC</ta>
            <ta e="T354" id="Seg_6082" s="T353">human.being-ACC</ta>
            <ta e="T355" id="Seg_6083" s="T354">notice-CVB.SEQ</ta>
            <ta e="T356" id="Seg_6084" s="T355">stand-PTCP.PST</ta>
            <ta e="T357" id="Seg_6085" s="T356">earth-3SG-ABL</ta>
            <ta e="T358" id="Seg_6086" s="T357">jump-PST2.[3SG]</ta>
            <ta e="T359" id="Seg_6087" s="T358">be.afraid-CVB.COND</ta>
            <ta e="T360" id="Seg_6088" s="T359">last</ta>
            <ta e="T361" id="Seg_6089" s="T360">power-3SG-INSTR</ta>
            <ta e="T362" id="Seg_6090" s="T361">run-PST2.[3SG]</ta>
            <ta e="T363" id="Seg_6091" s="T362">head-3SG-ACC</ta>
            <ta e="T364" id="Seg_6092" s="T363">in.the.direction</ta>
            <ta e="T365" id="Seg_6093" s="T364">riding.reindeer-ABL</ta>
            <ta e="T366" id="Seg_6094" s="T365">fall-NEG.PTCP</ta>
            <ta e="T367" id="Seg_6095" s="T366">child.[NOM]</ta>
            <ta e="T368" id="Seg_6096" s="T367">catch-CVB.SIM</ta>
            <ta e="T369" id="Seg_6097" s="T368">try-PST2.[3SG]</ta>
            <ta e="T370" id="Seg_6098" s="T369">reindeer-3SG-GEN</ta>
            <ta e="T371" id="Seg_6099" s="T370">animal.hair-3SG-ABL</ta>
            <ta e="T372" id="Seg_6100" s="T371">how</ta>
            <ta e="T373" id="Seg_6101" s="T372">NEG</ta>
            <ta e="T374" id="Seg_6102" s="T373">where</ta>
            <ta e="T375" id="Seg_6103" s="T374">NEG</ta>
            <ta e="T376" id="Seg_6104" s="T375">hand-PL-3SG.[NOM]</ta>
            <ta e="T377" id="Seg_6105" s="T376">get.caught-EP-NEG.[3SG]</ta>
            <ta e="T378" id="Seg_6106" s="T377">reindeer-3SG-GEN</ta>
            <ta e="T379" id="Seg_6107" s="T378">animal.hair-PL-3SG.[NOM]</ta>
            <ta e="T380" id="Seg_6108" s="T379">small.nail.[NOM]</ta>
            <ta e="T381" id="Seg_6109" s="T380">like-PL</ta>
            <ta e="T382" id="Seg_6110" s="T381">riding.reindeer-3SG.[NOM]</ta>
            <ta e="T383" id="Seg_6111" s="T382">last</ta>
            <ta e="T384" id="Seg_6112" s="T383">power-3SG-INSTR</ta>
            <ta e="T385" id="Seg_6113" s="T384">shout-PST2.[3SG]</ta>
            <ta e="T386" id="Seg_6114" s="T385">head-3SG-ACC</ta>
            <ta e="T387" id="Seg_6115" s="T386">in.the.direction</ta>
            <ta e="T388" id="Seg_6116" s="T387">run-CVB.SEQ</ta>
            <ta e="T389" id="Seg_6117" s="T388">go-PST2.[3SG]</ta>
            <ta e="T390" id="Seg_6118" s="T389">some</ta>
            <ta e="T391" id="Seg_6119" s="T390">pig-PL.[NOM]</ta>
            <ta e="T392" id="Seg_6120" s="T391">that-ACC</ta>
            <ta e="T393" id="Seg_6121" s="T392">see-CVB.SEQ-3PL</ta>
            <ta e="T394" id="Seg_6122" s="T393">one</ta>
            <ta e="T395" id="Seg_6123" s="T394">moment.[NOM]</ta>
            <ta e="T396" id="Seg_6124" s="T395">run-PST2-3PL</ta>
            <ta e="T397" id="Seg_6125" s="T396">back-3PL-ABL</ta>
            <ta e="T398" id="Seg_6126" s="T397">old</ta>
            <ta e="T399" id="Seg_6127" s="T398">old.woman.[NOM]</ta>
            <ta e="T400" id="Seg_6128" s="T399">run-CVB.SIM</ta>
            <ta e="T401" id="Seg_6129" s="T400">try-PST2.[3SG]</ta>
            <ta e="T402" id="Seg_6130" s="T401">Toyoo.[NOM]</ta>
            <ta e="T403" id="Seg_6131" s="T402">what-ACC</ta>
            <ta e="T404" id="Seg_6132" s="T403">just</ta>
            <ta e="T405" id="Seg_6133" s="T404">shout-CVB.SIM-shout-CVB.SIM</ta>
            <ta e="T406" id="Seg_6134" s="T405">run-PTCP.PRS-3SG-ACC</ta>
            <ta e="T407" id="Seg_6135" s="T406">every</ta>
            <ta e="T408" id="Seg_6136" s="T407">Toyoo.[NOM]</ta>
            <ta e="T409" id="Seg_6137" s="T408">to</ta>
            <ta e="T410" id="Seg_6138" s="T409">stick-3SG-INSTR</ta>
            <ta e="T411" id="Seg_6139" s="T410">swing-PRS.[3SG]</ta>
            <ta e="T412" id="Seg_6140" s="T411">naked</ta>
            <ta e="T413" id="Seg_6141" s="T412">riding.reindeer-3SG.[NOM]</ta>
            <ta e="T414" id="Seg_6142" s="T413">dirty</ta>
            <ta e="T415" id="Seg_6143" s="T414">mud-PROPR</ta>
            <ta e="T416" id="Seg_6144" s="T415">water-PROPR</ta>
            <ta e="T417" id="Seg_6145" s="T416">pit-DAT/LOC</ta>
            <ta e="T418" id="Seg_6146" s="T417">splash</ta>
            <ta e="T419" id="Seg_6147" s="T418">make-CVB.SIM</ta>
            <ta e="T420" id="Seg_6148" s="T419">fall-PST2.[3SG]</ta>
            <ta e="T421" id="Seg_6149" s="T420">Toyoo.[NOM]</ta>
            <ta e="T422" id="Seg_6150" s="T421">pig-3SG-ACC</ta>
            <ta e="T423" id="Seg_6151" s="T422">with</ta>
            <ta e="T424" id="Seg_6152" s="T423">stand.up-CVB.SIM</ta>
            <ta e="T425" id="Seg_6153" s="T424">do.in.vain-CVB.SEQ</ta>
            <ta e="T426" id="Seg_6154" s="T425">lie-PST2.[3SG]</ta>
            <ta e="T427" id="Seg_6155" s="T426">face-3SG.[NOM]</ta>
            <ta e="T428" id="Seg_6156" s="T427">NEG</ta>
            <ta e="T429" id="Seg_6157" s="T428">what-3SG.[NOM]</ta>
            <ta e="T430" id="Seg_6158" s="T429">NEG</ta>
            <ta e="T431" id="Seg_6159" s="T430">to.be.on.view-EP-NEG.[3SG]</ta>
            <ta e="T432" id="Seg_6160" s="T431">clay.[NOM]</ta>
            <ta e="T433" id="Seg_6161" s="T432">become-PST2.[3SG]</ta>
            <ta e="T434" id="Seg_6162" s="T433">stand.up-CVB.SIM</ta>
            <ta e="T435" id="Seg_6163" s="T434">try-CVB.SEQ</ta>
            <ta e="T436" id="Seg_6164" s="T435">last-3SG.[NOM]</ta>
            <ta e="T437" id="Seg_6165" s="T436">be-CVB.SEQ</ta>
            <ta e="T438" id="Seg_6166" s="T437">old.woman.[NOM]</ta>
            <ta e="T439" id="Seg_6167" s="T438">reach-PST2.[3SG]</ta>
            <ta e="T440" id="Seg_6168" s="T439">3SG-ACC</ta>
            <ta e="T441" id="Seg_6169" s="T440">Toyoo.[NOM]</ta>
            <ta e="T442" id="Seg_6170" s="T441">face-3SG-ACC</ta>
            <ta e="T443" id="Seg_6171" s="T442">see-CVB.SEQ</ta>
            <ta e="T444" id="Seg_6172" s="T443">burst.into.laughter-PST2.[3SG]</ta>
            <ta e="T445" id="Seg_6173" s="T444">hand-3SG-INSTR</ta>
            <ta e="T446" id="Seg_6174" s="T445">only</ta>
            <ta e="T447" id="Seg_6175" s="T446">wave-PST2.[3SG]</ta>
            <ta e="T448" id="Seg_6176" s="T447">break.through.[IMP.2SG]</ta>
            <ta e="T449" id="Seg_6177" s="T448">where.from</ta>
            <ta e="T450" id="Seg_6178" s="T449">come-PST2-EP-2SG</ta>
            <ta e="T451" id="Seg_6179" s="T450">say-PTCP.PST.[NOM]</ta>
            <ta e="T452" id="Seg_6180" s="T451">similar</ta>
            <ta e="T453" id="Seg_6181" s="T452">Toyoo.[NOM]</ta>
            <ta e="T454" id="Seg_6182" s="T453">stand.up-CVB.SIM</ta>
            <ta e="T455" id="Seg_6183" s="T454">jump-CVB.ANT</ta>
            <ta e="T456" id="Seg_6184" s="T455">last</ta>
            <ta e="T457" id="Seg_6185" s="T456">power-3SG-INSTR</ta>
            <ta e="T458" id="Seg_6186" s="T457">breath-POSS</ta>
            <ta e="T459" id="Seg_6187" s="T458">NEG</ta>
            <ta e="T460" id="Seg_6188" s="T459">friend-PL-3SG-ACC</ta>
            <ta e="T461" id="Seg_6189" s="T460">to</ta>
            <ta e="T462" id="Seg_6190" s="T461">kick-PST2.[3SG]</ta>
            <ta e="T463" id="Seg_6191" s="T462">oh.dear</ta>
            <ta e="T464" id="Seg_6192" s="T463">hardly</ta>
            <ta e="T465" id="Seg_6193" s="T464">die-CVB.SIM</ta>
            <ta e="T466" id="Seg_6194" s="T465">beat-PST1-1SG</ta>
            <ta e="T467" id="Seg_6195" s="T466">hardly</ta>
            <ta e="T468" id="Seg_6196" s="T467">die-CVB.SIM</ta>
            <ta e="T469" id="Seg_6197" s="T468">beat-PST1-1SG</ta>
            <ta e="T470" id="Seg_6198" s="T469">teacher-3PL.[NOM]</ta>
            <ta e="T471" id="Seg_6199" s="T470">director.[NOM]</ta>
            <ta e="T472" id="Seg_6200" s="T471">be-CVB.SEQ</ta>
            <ta e="T473" id="Seg_6201" s="T472">what.[NOM]</ta>
            <ta e="T474" id="Seg_6202" s="T473">NEG</ta>
            <ta e="T475" id="Seg_6203" s="T474">say-PTCP.FUT-3PL-ACC</ta>
            <ta e="T476" id="Seg_6204" s="T475">power-3SG-ABL</ta>
            <ta e="T477" id="Seg_6205" s="T476">last</ta>
            <ta e="T478" id="Seg_6206" s="T477">soul-3PL-INSTR</ta>
            <ta e="T479" id="Seg_6207" s="T478">laugh-CVB.SIM</ta>
            <ta e="T480" id="Seg_6208" s="T479">stand-PST2-3PL</ta>
            <ta e="T482" id="Seg_6209" s="T481">last</ta>
            <ta e="T483" id="Seg_6210" s="T482">thought-3SG-ACC</ta>
            <ta e="T484" id="Seg_6211" s="T483">speak-PST2.[3SG]</ta>
            <ta e="T485" id="Seg_6212" s="T484">how</ta>
            <ta e="T486" id="Seg_6213" s="T485">NEG</ta>
            <ta e="T487" id="Seg_6214" s="T486">say-PTCP.FUT-DAT/LOC</ta>
            <ta e="T488" id="Seg_6215" s="T487">very</ta>
            <ta e="T489" id="Seg_6216" s="T488">very</ta>
            <ta e="T490" id="Seg_6217" s="T489">agile</ta>
            <ta e="T491" id="Seg_6218" s="T490">people.[NOM]</ta>
            <ta e="T492" id="Seg_6219" s="T491">come-PST2-3PL</ta>
            <ta e="T494" id="Seg_6220" s="T492">apparently</ta>
            <ta e="T495" id="Seg_6221" s="T494">colleague-PL-3SG-DAT/LOC</ta>
            <ta e="T496" id="Seg_6222" s="T495">say-PST2.[3SG]</ta>
            <ta e="T497" id="Seg_6223" s="T496">now</ta>
            <ta e="T498" id="Seg_6224" s="T497">3PL-ACC</ta>
            <ta e="T499" id="Seg_6225" s="T498">boarding.school-DAT/LOC</ta>
            <ta e="T500" id="Seg_6226" s="T499">take.away-EP-IMP.2PL</ta>
            <ta e="T501" id="Seg_6227" s="T500">then</ta>
            <ta e="T502" id="Seg_6228" s="T501">wash.oneself-CAUS-EP-IMP.2PL</ta>
            <ta e="T503" id="Seg_6229" s="T502">food-3PL-ACC</ta>
            <ta e="T504" id="Seg_6230" s="T503">prepare-PTCP.FUT-3PL-DAT/LOC</ta>
            <ta e="T505" id="Seg_6231" s="T504">until</ta>
            <ta e="T506" id="Seg_6232" s="T505">whole-3SG-ACC</ta>
            <ta e="T507" id="Seg_6233" s="T506">thorough-ADVZ</ta>
            <ta e="T508" id="Seg_6234" s="T507">make-EP-IMP.2PL</ta>
            <ta e="T509" id="Seg_6235" s="T508">child-PL-ACC</ta>
            <ta e="T510" id="Seg_6236" s="T509">frighten.[CAUS]-NEG.CVB.SIM</ta>
            <ta e="T511" id="Seg_6237" s="T510">child-PL-ACC</ta>
            <ta e="T512" id="Seg_6238" s="T511">immediately</ta>
            <ta e="T513" id="Seg_6239" s="T512">hunt-CVB.SEQ</ta>
            <ta e="T514" id="Seg_6240" s="T513">bring-PST2-3PL</ta>
            <ta e="T515" id="Seg_6241" s="T514">show-PST2-3PL</ta>
            <ta e="T516" id="Seg_6242" s="T515">every-3PL-DAT/LOC</ta>
            <ta e="T517" id="Seg_6243" s="T516">live-PTCP.PRS</ta>
            <ta e="T518" id="Seg_6244" s="T517">house-3PL-ACC</ta>
            <ta e="T519" id="Seg_6245" s="T518">Russian-SIM</ta>
            <ta e="T521" id="Seg_6246" s="T520">name-ADJZ.[NOM]</ta>
            <ta e="T522" id="Seg_6247" s="T521">patch-PTCP.PRS</ta>
            <ta e="T523" id="Seg_6248" s="T522">new</ta>
            <ta e="T524" id="Seg_6249" s="T523">shirt-PL-ACC</ta>
            <ta e="T525" id="Seg_6250" s="T524">shoes-PL-ACC</ta>
            <ta e="T526" id="Seg_6251" s="T525">give-FREQ-PST2-3PL</ta>
            <ta e="T527" id="Seg_6252" s="T526">every</ta>
            <ta e="T528" id="Seg_6253" s="T527">come-PTCP.PST</ta>
            <ta e="T529" id="Seg_6254" s="T528">people.[NOM]</ta>
            <ta e="T530" id="Seg_6255" s="T529">hair-PL-3SG-ACC</ta>
            <ta e="T531" id="Seg_6256" s="T530">cut-PST2-3PL</ta>
            <ta e="T532" id="Seg_6257" s="T531">what-ABL</ta>
            <ta e="T533" id="Seg_6258" s="T532">girl.[NOM]</ta>
            <ta e="T534" id="Seg_6259" s="T533">child-PL.[NOM]</ta>
            <ta e="T535" id="Seg_6260" s="T534">cry-PTCP.PRS-3PL.[NOM]</ta>
            <ta e="T536" id="Seg_6261" s="T535">very</ta>
            <ta e="T537" id="Seg_6262" s="T536">be-PST2.[3SG]</ta>
            <ta e="T538" id="Seg_6263" s="T537">hair-3PL-ACC</ta>
            <ta e="T539" id="Seg_6264" s="T538">feel.sorry-CVB.SEQ-3PL</ta>
            <ta e="T540" id="Seg_6265" s="T539">Toyoo-ACC</ta>
            <ta e="T541" id="Seg_6266" s="T540">also</ta>
            <ta e="T542" id="Seg_6267" s="T541">cut-PST2-3PL</ta>
            <ta e="T543" id="Seg_6268" s="T542">dress-PTCP.PRS</ta>
            <ta e="T544" id="Seg_6269" s="T543">clothes.[NOM]</ta>
            <ta e="T545" id="Seg_6270" s="T544">give-PST2-3PL</ta>
            <ta e="T546" id="Seg_6271" s="T545">dress-PTCP.PRS</ta>
            <ta e="T547" id="Seg_6272" s="T546">hoof-3PL-ACC</ta>
            <ta e="T548" id="Seg_6273" s="T547">hand-PST2-3PL</ta>
            <ta e="T549" id="Seg_6274" s="T548">bed-3SG-DAT/LOC</ta>
            <ta e="T550" id="Seg_6275" s="T549">come-CVB.ANT</ta>
            <ta e="T551" id="Seg_6276" s="T550">sit.down-PST2.[3SG]</ta>
            <ta e="T552" id="Seg_6277" s="T551">look.at-CVB.SIM</ta>
            <ta e="T553" id="Seg_6278" s="T552">what-ACC</ta>
            <ta e="T554" id="Seg_6279" s="T553">hand-PTCP.PST-3PL-ACC</ta>
            <ta e="T555" id="Seg_6280" s="T554">every-3SG.[NOM]</ta>
            <ta e="T556" id="Seg_6281" s="T555">3SG-DAT/LOC</ta>
            <ta e="T557" id="Seg_6282" s="T556">alright</ta>
            <ta e="T558" id="Seg_6283" s="T557">that</ta>
            <ta e="T559" id="Seg_6284" s="T558">and</ta>
            <ta e="T560" id="Seg_6285" s="T559">be-COND.[3SG]</ta>
            <ta e="T561" id="Seg_6286" s="T560">why</ta>
            <ta e="T562" id="Seg_6287" s="T561">3PL.[NOM]</ta>
            <ta e="T563" id="Seg_6288" s="T562">give-PST2-3PL</ta>
            <ta e="T564" id="Seg_6289" s="T563">probably=Q</ta>
            <ta e="T565" id="Seg_6290" s="T564">two-DISTR</ta>
            <ta e="T566" id="Seg_6291" s="T565">shirt-ACC</ta>
            <ta e="T567" id="Seg_6292" s="T566">two-DISTR</ta>
            <ta e="T568" id="Seg_6293" s="T567">trousers-ACC</ta>
            <ta e="T569" id="Seg_6294" s="T568">new</ta>
            <ta e="T570" id="Seg_6295" s="T569">clothes-3PL-ACC</ta>
            <ta e="T571" id="Seg_6296" s="T570">dress-PTCP.PRS.[NOM]</ta>
            <ta e="T574" id="Seg_6297" s="T573">think-PST2.[3SG]</ta>
            <ta e="T575" id="Seg_6298" s="T574">child-PL-ACC</ta>
            <ta e="T576" id="Seg_6299" s="T575">wash.oneself-CAUS-CVB.SIM</ta>
            <ta e="T577" id="Seg_6300" s="T576">bring-PST2-3PL</ta>
            <ta e="T578" id="Seg_6301" s="T577">where</ta>
            <ta e="T579" id="Seg_6302" s="T578">agile</ta>
            <ta e="T580" id="Seg_6303" s="T579">people.[NOM]</ta>
            <ta e="T581" id="Seg_6304" s="T580">last</ta>
            <ta e="T582" id="Seg_6305" s="T581">thought-3PL-INSTR</ta>
            <ta e="T583" id="Seg_6306" s="T582">happy-ADVZ</ta>
            <ta e="T584" id="Seg_6307" s="T583">laugh-RECP/COLL-CVB.SIM-laugh-RECP/COLL-CVB.SIM</ta>
            <ta e="T585" id="Seg_6308" s="T584">hot.[NOM]</ta>
            <ta e="T586" id="Seg_6309" s="T585">hot.[NOM]</ta>
            <ta e="T587" id="Seg_6310" s="T586">say-EP-RECP/COLL-CVB.SIM</ta>
            <ta e="T588" id="Seg_6311" s="T587">sit-PST2-3PL</ta>
            <ta e="T589" id="Seg_6312" s="T588">Toyoo.[NOM]</ta>
            <ta e="T590" id="Seg_6313" s="T589">wash.oneself-CVB.SEQ</ta>
            <ta e="T591" id="Seg_6314" s="T590">stop-CVB.ANT</ta>
            <ta e="T592" id="Seg_6315" s="T591">dress-EP-PST2.[3SG]</ta>
            <ta e="T593" id="Seg_6316" s="T592">dress-PST2.[3SG]</ta>
            <ta e="T594" id="Seg_6317" s="T593">white</ta>
            <ta e="T595" id="Seg_6318" s="T594">shirt-3SG-ACC</ta>
            <ta e="T596" id="Seg_6319" s="T595">trousers-3SG-ACC</ta>
            <ta e="T597" id="Seg_6320" s="T596">some-PL-3SG-ACC</ta>
            <ta e="T598" id="Seg_6321" s="T597">reserves-VBZ-CVB.SEQ</ta>
            <ta e="T599" id="Seg_6322" s="T598">wrap.up-PST2.[3SG]</ta>
            <ta e="T600" id="Seg_6323" s="T599">later</ta>
            <ta e="T601" id="Seg_6324" s="T600">dress-CVB.PURP</ta>
            <ta e="T602" id="Seg_6325" s="T601">body-3SG-DAT/LOC</ta>
            <ta e="T603" id="Seg_6326" s="T602">dress-CVB.SEQ</ta>
            <ta e="T604" id="Seg_6327" s="T603">white</ta>
            <ta e="T605" id="Seg_6328" s="T604">clothes-PL-3SG-ACC</ta>
            <ta e="T606" id="Seg_6329" s="T605">what.[NOM]</ta>
            <ta e="T607" id="Seg_6330" s="T606">NEG</ta>
            <ta e="T608" id="Seg_6331" s="T607">be-NEG.PTCP.PST.[NOM]</ta>
            <ta e="T609" id="Seg_6332" s="T608">similar</ta>
            <ta e="T610" id="Seg_6333" s="T609">go-EP-PST2.[3SG]</ta>
            <ta e="T611" id="Seg_6334" s="T610">friend-PL-3SG.[NOM]</ta>
            <ta e="T612" id="Seg_6335" s="T611">EMPH</ta>
            <ta e="T613" id="Seg_6336" s="T612">what-ACC</ta>
            <ta e="T614" id="Seg_6337" s="T613">NEG</ta>
            <ta e="T615" id="Seg_6338" s="T614">say-NEG-3PL</ta>
            <ta e="T616" id="Seg_6339" s="T615">be-PST2.[3SG]</ta>
            <ta e="T617" id="Seg_6340" s="T616">3SG-DAT/LOC</ta>
            <ta e="T618" id="Seg_6341" s="T617">boarding.school-DAT/LOC</ta>
            <ta e="T619" id="Seg_6342" s="T618">work-PTCP.PRS</ta>
            <ta e="T620" id="Seg_6343" s="T619">one</ta>
            <ta e="T621" id="Seg_6344" s="T620">woman.[NOM]</ta>
            <ta e="T622" id="Seg_6345" s="T621">explain-PST2.[3SG]</ta>
            <ta e="T623" id="Seg_6346" s="T622">how</ta>
            <ta e="T624" id="Seg_6347" s="T623">3SG.[NOM]</ta>
            <ta e="T625" id="Seg_6348" s="T624">dress-PTCP.PRS</ta>
            <ta e="T626" id="Seg_6349" s="T625">become-PTCP.FUT-3SG-ACC</ta>
            <ta e="T627" id="Seg_6350" s="T626">eat-PTCP.PRS</ta>
            <ta e="T628" id="Seg_6351" s="T627">time-3PL.[NOM]</ta>
            <ta e="T629" id="Seg_6352" s="T628">come-PST2.[3SG]</ta>
            <ta e="T630" id="Seg_6353" s="T629">child-PL-ACC</ta>
            <ta e="T631" id="Seg_6354" s="T630">seat-ITER-PST2-3PL</ta>
            <ta e="T634" id="Seg_6355" s="T633">long</ta>
            <ta e="T635" id="Seg_6356" s="T634">very</ta>
            <ta e="T636" id="Seg_6357" s="T635">table-PL.[NOM]</ta>
            <ta e="T637" id="Seg_6358" s="T636">two</ta>
            <ta e="T638" id="Seg_6359" s="T637">in.the.direction</ta>
            <ta e="T639" id="Seg_6360" s="T638">side-3PL-DAT/LOC</ta>
            <ta e="T640" id="Seg_6361" s="T639">big</ta>
            <ta e="T641" id="Seg_6362" s="T640">dishes-PL-DAT/LOC</ta>
            <ta e="T642" id="Seg_6363" s="T641">lay-EP-PASS/REFL-EP-PST2-3PL</ta>
            <ta e="T643" id="Seg_6364" s="T642">tasty</ta>
            <ta e="T644" id="Seg_6365" s="T643">smell-PROPR</ta>
            <ta e="T645" id="Seg_6366" s="T644">bread-PL.[NOM]</ta>
            <ta e="T646" id="Seg_6367" s="T645">child.[NOM]</ta>
            <ta e="T647" id="Seg_6368" s="T646">every</ta>
            <ta e="T648" id="Seg_6369" s="T647">pour-MULT-PST2-3PL</ta>
            <ta e="T649" id="Seg_6370" s="T648">fish.[NOM]</ta>
            <ta e="T650" id="Seg_6371" s="T649">soup-3SG-ACC</ta>
            <ta e="T651" id="Seg_6372" s="T650">spoon-PL-ACC</ta>
            <ta e="T652" id="Seg_6373" s="T651">give-FREQ-PST2-3PL</ta>
            <ta e="T653" id="Seg_6374" s="T652">see-PST2.[3SG]</ta>
            <ta e="T654" id="Seg_6375" s="T653">child-PL.[NOM]</ta>
            <ta e="T655" id="Seg_6376" s="T654">eat-CVB.SEQ-3PL</ta>
            <ta e="T656" id="Seg_6377" s="T655">nape-3PL.[NOM]</ta>
            <ta e="T657" id="Seg_6378" s="T656">only</ta>
            <ta e="T658" id="Seg_6379" s="T657">get.lost-CVB.SIM</ta>
            <ta e="T659" id="Seg_6380" s="T658">lie-PRS.[3SG]</ta>
            <ta e="T660" id="Seg_6381" s="T659">Toyoo.[NOM]</ta>
            <ta e="T661" id="Seg_6382" s="T660">eat-CVB.SIM</ta>
            <ta e="T662" id="Seg_6383" s="T661">sit-CVB.SEQ</ta>
            <ta e="T663" id="Seg_6384" s="T662">what-ACC</ta>
            <ta e="T664" id="Seg_6385" s="T663">INDEF</ta>
            <ta e="T665" id="Seg_6386" s="T664">thought-3SG.[NOM]</ta>
            <ta e="T666" id="Seg_6387" s="T665">come-PST2.[3SG]</ta>
            <ta e="T667" id="Seg_6388" s="T666">mum-3SG-ACC</ta>
            <ta e="T668" id="Seg_6389" s="T667">father-3SG-ACC</ta>
            <ta e="T669" id="Seg_6390" s="T668">Kycha-3SG-ACC</ta>
            <ta e="T670" id="Seg_6391" s="T669">remember-CVB.SEQ</ta>
            <ta e="T671" id="Seg_6392" s="T670">eat-CVB.SEQ</ta>
            <ta e="T672" id="Seg_6393" s="T671">stop-CVB.ANT</ta>
            <ta e="T673" id="Seg_6394" s="T672">child-PL.[NOM]</ta>
            <ta e="T674" id="Seg_6395" s="T673">Volochanka.[NOM]</ta>
            <ta e="T675" id="Seg_6396" s="T674">along</ta>
            <ta e="T676" id="Seg_6397" s="T675">play-CVB.SIM</ta>
            <ta e="T677" id="Seg_6398" s="T676">go-EP-PST2-3PL</ta>
            <ta e="T678" id="Seg_6399" s="T677">look.at-CVB.SIM</ta>
            <ta e="T679" id="Seg_6400" s="T678">Russian</ta>
            <ta e="T680" id="Seg_6401" s="T679">house-PL-ACC</ta>
            <ta e="T681" id="Seg_6402" s="T680">every</ta>
            <ta e="T682" id="Seg_6403" s="T681">yard-ACC</ta>
            <ta e="T683" id="Seg_6404" s="T682">3PL-DAT/LOC</ta>
            <ta e="T684" id="Seg_6405" s="T683">every-3SG.[NOM]</ta>
            <ta e="T685" id="Seg_6406" s="T684">miracle.[NOM]</ta>
            <ta e="T686" id="Seg_6407" s="T685">just</ta>
            <ta e="T687" id="Seg_6408" s="T686">come-PTCP.PST</ta>
            <ta e="T688" id="Seg_6409" s="T687">human.being-PL-DAT/LOC</ta>
            <ta e="T689" id="Seg_6410" s="T688">next</ta>
            <ta e="T690" id="Seg_6411" s="T689">day.[NOM]</ta>
            <ta e="T691" id="Seg_6412" s="T690">again</ta>
            <ta e="T692" id="Seg_6413" s="T691">big</ta>
            <ta e="T693" id="Seg_6414" s="T692">child-PROPR</ta>
            <ta e="T694" id="Seg_6415" s="T693">reindeer.caravan.[NOM]</ta>
            <ta e="T695" id="Seg_6416" s="T694">come-PST2.[3SG]</ta>
            <ta e="T696" id="Seg_6417" s="T695">different</ta>
            <ta e="T697" id="Seg_6418" s="T696">kolkhoz-ABL</ta>
            <ta e="T698" id="Seg_6419" s="T697">just</ta>
            <ta e="T699" id="Seg_6420" s="T698">come-CVB.SIM</ta>
            <ta e="T700" id="Seg_6421" s="T699">child-PL.[NOM]</ta>
            <ta e="T701" id="Seg_6422" s="T700">confused-3SG.[NOM]</ta>
            <ta e="T702" id="Seg_6423" s="T701">very</ta>
            <ta e="T703" id="Seg_6424" s="T702">every-ABL</ta>
            <ta e="T705" id="Seg_6425" s="T704">be.afraid-PTCP.PRS.[NOM]</ta>
            <ta e="T706" id="Seg_6426" s="T705">like-PL</ta>
            <ta e="T707" id="Seg_6427" s="T706">3PL.[NOM]</ta>
            <ta e="T708" id="Seg_6428" s="T707">very</ta>
            <ta e="T709" id="Seg_6429" s="T708">long</ta>
            <ta e="T710" id="Seg_6430" s="T709">such.[NOM]</ta>
            <ta e="T711" id="Seg_6431" s="T710">be-PST2.NEG-3PL</ta>
            <ta e="T712" id="Seg_6432" s="T711">also</ta>
            <ta e="T713" id="Seg_6433" s="T712">Toyoo.[NOM]</ta>
            <ta e="T714" id="Seg_6434" s="T713">Mitya.[NOM]</ta>
            <ta e="T715" id="Seg_6435" s="T714">Ivan.[NOM]</ta>
            <ta e="T716" id="Seg_6436" s="T715">similar</ta>
            <ta e="T717" id="Seg_6437" s="T716">be.embarassed-EP-NEG.PTCP</ta>
            <ta e="T718" id="Seg_6438" s="T717">custom-ACC</ta>
            <ta e="T719" id="Seg_6439" s="T718">take-PST2-3PL</ta>
            <ta e="T720" id="Seg_6440" s="T719">new</ta>
            <ta e="T721" id="Seg_6441" s="T720">friend-PL-3SG-ACC</ta>
            <ta e="T722" id="Seg_6442" s="T721">see-CVB.SEQ-3PL</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_6443" s="T0">zuhören-IMP.2PL</ta>
            <ta e="T2" id="Seg_6444" s="T1">Erzählung-ACC</ta>
            <ta e="T3" id="Seg_6445" s="T2">Wunder.[NOM]</ta>
            <ta e="T4" id="Seg_6446" s="T3">Rentier-PL.[NOM]</ta>
            <ta e="T5" id="Seg_6447" s="T4">drei-ORD</ta>
            <ta e="T6" id="Seg_6448" s="T5">Tag-3SG-DAT/LOC</ta>
            <ta e="T7" id="Seg_6449" s="T6">Kind-PROPR</ta>
            <ta e="T8" id="Seg_6450" s="T7">Rentierkarawane.[NOM]</ta>
            <ta e="T9" id="Seg_6451" s="T8">ankommen-PST2.[3SG]</ta>
            <ta e="T10" id="Seg_6452" s="T9">Volochanka-DAT/LOC</ta>
            <ta e="T11" id="Seg_6453" s="T10">Ust_Avam.[NOM]</ta>
            <ta e="T12" id="Seg_6454" s="T11">Rajon-3SG-GEN</ta>
            <ta e="T13" id="Seg_6455" s="T12">Zentrum.[NOM]</ta>
            <ta e="T14" id="Seg_6456" s="T13">Siedlung-3SG-GEN</ta>
            <ta e="T15" id="Seg_6457" s="T14">Russe-3PL.[NOM]</ta>
            <ta e="T16" id="Seg_6458" s="T15">Dolgane-3PL.[NOM]</ta>
            <ta e="T17" id="Seg_6459" s="T16">jeder-3PL.[NOM]</ta>
            <ta e="T18" id="Seg_6460" s="T17">sammeln-PASS/REFL-EP-PST2-3PL</ta>
            <ta e="T19" id="Seg_6461" s="T18">kommen-NMNZ.[NOM]</ta>
            <ta e="T20" id="Seg_6462" s="T19">Kind-PL-ACC</ta>
            <ta e="T21" id="Seg_6463" s="T20">sehen-CVB.PURP</ta>
            <ta e="T22" id="Seg_6464" s="T21">Puppe.[NOM]</ta>
            <ta e="T23" id="Seg_6465" s="T22">ähnlich</ta>
            <ta e="T24" id="Seg_6466" s="T23">Kleidung-PROPR-PL-ACC</ta>
            <ta e="T25" id="Seg_6467" s="T24">Mensch.[NOM]</ta>
            <ta e="T26" id="Seg_6468" s="T25">lieben-PTCP.FUT-PL-ACC</ta>
            <ta e="T27" id="Seg_6469" s="T26">sehr</ta>
            <ta e="T28" id="Seg_6470" s="T27">Lärm-POSS</ta>
            <ta e="T29" id="Seg_6471" s="T28">NEG</ta>
            <ta e="T30" id="Seg_6472" s="T29">ruhig</ta>
            <ta e="T31" id="Seg_6473" s="T30">Volochanka.[NOM]</ta>
            <ta e="T32" id="Seg_6474" s="T31">Leben.[NOM]</ta>
            <ta e="T33" id="Seg_6475" s="T32">eins</ta>
            <ta e="T34" id="Seg_6476" s="T33">Augenblick.[NOM]</ta>
            <ta e="T35" id="Seg_6477" s="T34">wieder.aufleben-EP-PTCP.PST.[NOM]</ta>
            <ta e="T36" id="Seg_6478" s="T35">ähnlich</ta>
            <ta e="T37" id="Seg_6479" s="T36">Gott.[NOM]</ta>
            <ta e="T38" id="Seg_6480" s="T37">Tag-3SG-ACC</ta>
            <ta e="T39" id="Seg_6481" s="T38">Kennzeichen-VBZ-PTCP.PRS.[NOM]</ta>
            <ta e="T40" id="Seg_6482" s="T39">ähnlich</ta>
            <ta e="T41" id="Seg_6483" s="T40">sein-CVB.SEQ</ta>
            <ta e="T42" id="Seg_6484" s="T41">bleiben-PST2.[3SG]</ta>
            <ta e="T43" id="Seg_6485" s="T42">Kind-PL.[NOM]</ta>
            <ta e="T44" id="Seg_6486" s="T43">aber</ta>
            <ta e="T45" id="Seg_6487" s="T44">Mensch-PL-ACC</ta>
            <ta e="T46" id="Seg_6488" s="T45">sehen-NEG-3PL</ta>
            <ta e="T47" id="Seg_6489" s="T46">russisch</ta>
            <ta e="T48" id="Seg_6490" s="T47">Haus-PL.[NOM]</ta>
            <ta e="T49" id="Seg_6491" s="T48">zu</ta>
            <ta e="T50" id="Seg_6492" s="T49">nur</ta>
            <ta e="T53" id="Seg_6493" s="T52">Auge-3PL.[NOM]</ta>
            <ta e="T54" id="Seg_6494" s="T53">Straße.[NOM]</ta>
            <ta e="T55" id="Seg_6495" s="T54">entlang</ta>
            <ta e="T56" id="Seg_6496" s="T55">galoppieren-EP-CAUS-CVB.SEQ</ta>
            <ta e="T57" id="Seg_6497" s="T56">gehen-CVB.SEQ-3PL</ta>
            <ta e="T58" id="Seg_6498" s="T57">Reitrentier-EP-INSTR</ta>
            <ta e="T59" id="Seg_6499" s="T58">Großvater-3PL.[NOM]</ta>
            <ta e="T60" id="Seg_6500" s="T59">Anisim</ta>
            <ta e="T61" id="Seg_6501" s="T60">alter.Mann.[NOM]</ta>
            <ta e="T62" id="Seg_6502" s="T61">stehen-PST2.[3SG]</ta>
            <ta e="T63" id="Seg_6503" s="T62">Schule.[NOM]</ta>
            <ta e="T64" id="Seg_6504" s="T63">Platz.neben-DAT/LOC</ta>
            <ta e="T65" id="Seg_6505" s="T64">3SG-ACC</ta>
            <ta e="T66" id="Seg_6506" s="T65">um.herum</ta>
            <ta e="T67" id="Seg_6507" s="T66">Lärm-PROPR</ta>
            <ta e="T68" id="Seg_6508" s="T67">Armee.[NOM]</ta>
            <ta e="T69" id="Seg_6509" s="T68">Reitrentier-3PL-INSTR</ta>
            <ta e="T72" id="Seg_6510" s="T71">oberer.Teil-3PL-ABL</ta>
            <ta e="T73" id="Seg_6511" s="T72">springen-PST2-3PL</ta>
            <ta e="T74" id="Seg_6512" s="T73">Schule.[NOM]</ta>
            <ta e="T75" id="Seg_6513" s="T74">Platz.neben-DAT/LOC</ta>
            <ta e="T76" id="Seg_6514" s="T75">stehen-PRS-3PL</ta>
            <ta e="T79" id="Seg_6515" s="T78">lehren-PTCP.PRS</ta>
            <ta e="T80" id="Seg_6516" s="T79">Leute.[NOM]</ta>
            <ta e="T81" id="Seg_6517" s="T80">Anführer-3PL.[NOM]</ta>
            <ta e="T82" id="Seg_6518" s="T81">dort</ta>
            <ta e="T83" id="Seg_6519" s="T82">Internat.[NOM]</ta>
            <ta e="T84" id="Seg_6520" s="T83">Arbeiter-PL-3SG.[NOM]</ta>
            <ta e="T85" id="Seg_6521" s="T84">jeder-3PL.[NOM]</ta>
            <ta e="T86" id="Seg_6522" s="T85">Kind-PL-ACC</ta>
            <ta e="T87" id="Seg_6523" s="T86">fangen-CVB.SEQ</ta>
            <ta e="T88" id="Seg_6524" s="T87">nehmen-CVB.SEQ-3PL</ta>
            <ta e="T89" id="Seg_6525" s="T88">hallo-VBZ-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T90" id="Seg_6526" s="T89">was-ACC</ta>
            <ta e="T91" id="Seg_6527" s="T90">INDEF</ta>
            <ta e="T92" id="Seg_6528" s="T91">3PL-DAT/LOC</ta>
            <ta e="T93" id="Seg_6529" s="T92">sprechen-PRS-3PL</ta>
            <ta e="T94" id="Seg_6530" s="T93">Kind-PL.[NOM]</ta>
            <ta e="T95" id="Seg_6531" s="T94">russisch</ta>
            <ta e="T96" id="Seg_6532" s="T95">Sprache-3SG-ACC</ta>
            <ta e="T97" id="Seg_6533" s="T96">verstehen-NEG-3PL</ta>
            <ta e="T98" id="Seg_6534" s="T97">jenes.[NOM]</ta>
            <ta e="T99" id="Seg_6535" s="T98">wegen</ta>
            <ta e="T100" id="Seg_6536" s="T99">kommen-CVB.SIM</ta>
            <ta e="T101" id="Seg_6537" s="T100">klein</ta>
            <ta e="T102" id="Seg_6538" s="T101">Leute.[NOM]</ta>
            <ta e="T103" id="Seg_6539" s="T102">sich.unterhalten-NEG-3PL</ta>
            <ta e="T104" id="Seg_6540" s="T103">betrachten-PTCP.PRS</ta>
            <ta e="T105" id="Seg_6541" s="T104">nur</ta>
            <ta e="T106" id="Seg_6542" s="T105">sein-PST2-3PL</ta>
            <ta e="T107" id="Seg_6543" s="T106">Lehrer-3PL-GEN</ta>
            <ta e="T108" id="Seg_6544" s="T107">Gesicht-3PL-ACC</ta>
            <ta e="T109" id="Seg_6545" s="T108">3PL.[NOM]</ta>
            <ta e="T110" id="Seg_6546" s="T109">sehr</ta>
            <ta e="T111" id="Seg_6547" s="T110">interessant-ACC</ta>
            <ta e="T112" id="Seg_6548" s="T111">sehen-PST2-3PL</ta>
            <ta e="T113" id="Seg_6549" s="T112">sehr</ta>
            <ta e="T114" id="Seg_6550" s="T113">groß</ta>
            <ta e="T115" id="Seg_6551" s="T114">russisch</ta>
            <ta e="T116" id="Seg_6552" s="T115">Haus-PL-ACC</ta>
            <ta e="T117" id="Seg_6553" s="T116">anders</ta>
            <ta e="T118" id="Seg_6554" s="T117">Mensch-PL-ACC</ta>
            <ta e="T119" id="Seg_6555" s="T118">Russe-PL-ACC</ta>
            <ta e="T120" id="Seg_6556" s="T119">anders</ta>
            <ta e="T121" id="Seg_6557" s="T120">Sprache-PROPR-PL-ACC</ta>
            <ta e="T122" id="Seg_6558" s="T121">Kleidung-PROPR-PL-ACC</ta>
            <ta e="T123" id="Seg_6559" s="T122">Fuß-3PL-DAT/LOC</ta>
            <ta e="T124" id="Seg_6560" s="T123">aber</ta>
            <ta e="T125" id="Seg_6561" s="T124">Rentier.[NOM]</ta>
            <ta e="T126" id="Seg_6562" s="T125">Huf-3SG.[NOM]</ta>
            <ta e="T127" id="Seg_6563" s="T126">ähnlich</ta>
            <ta e="T128" id="Seg_6564" s="T127">Schuhe-ACC</ta>
            <ta e="T129" id="Seg_6565" s="T128">umhängen-EP-MED-PST2-3PL</ta>
            <ta e="T130" id="Seg_6566" s="T129">Mann.[NOM]</ta>
            <ta e="T131" id="Seg_6567" s="T130">Mensch-PL.[NOM]</ta>
            <ta e="T132" id="Seg_6568" s="T131">Kopf-3PL-DAT/LOC</ta>
            <ta e="T133" id="Seg_6569" s="T132">Mama-3PL-GEN</ta>
            <ta e="T134" id="Seg_6570" s="T133">Schöpfkelle-3SG-ACC</ta>
            <ta e="T135" id="Seg_6571" s="T134">ähnlich</ta>
            <ta e="T136" id="Seg_6572" s="T135">Mütze-ACC</ta>
            <ta e="T137" id="Seg_6573" s="T136">anziehen-CVB.SIM</ta>
            <ta e="T138" id="Seg_6574" s="T137">gehen-PRS-3PL</ta>
            <ta e="T139" id="Seg_6575" s="T138">Frau-PL-3SG.[NOM]</ta>
            <ta e="T140" id="Seg_6576" s="T139">geflochten-POSS</ta>
            <ta e="T141" id="Seg_6577" s="T140">NEG-3PL</ta>
            <ta e="T142" id="Seg_6578" s="T141">Haar-3PL.[NOM]</ta>
            <ta e="T143" id="Seg_6579" s="T142">schneiden-EP-PASS/REFL-EP-PTCP.PST-PL.[NOM]</ta>
            <ta e="T144" id="Seg_6580" s="T143">Mann.[NOM]</ta>
            <ta e="T145" id="Seg_6581" s="T144">Mensch.[NOM]</ta>
            <ta e="T146" id="Seg_6582" s="T145">ähnlich</ta>
            <ta e="T147" id="Seg_6583" s="T146">Kleid-3PL.[NOM]</ta>
            <ta e="T148" id="Seg_6584" s="T147">unterer</ta>
            <ta e="T149" id="Seg_6585" s="T148">Seite-3SG-DAT/LOC</ta>
            <ta e="T150" id="Seg_6586" s="T149">sehr</ta>
            <ta e="T151" id="Seg_6587" s="T150">eng.genäht.[NOM]</ta>
            <ta e="T152" id="Seg_6588" s="T151">jenes.[NOM]</ta>
            <ta e="T153" id="Seg_6589" s="T152">wegen</ta>
            <ta e="T154" id="Seg_6590" s="T153">3PL.[NOM]</ta>
            <ta e="T155" id="Seg_6591" s="T154">kaum</ta>
            <ta e="T156" id="Seg_6592" s="T155">go-PRS-3PL</ta>
            <ta e="T157" id="Seg_6593" s="T156">Tojoo.[NOM]</ta>
            <ta e="T158" id="Seg_6594" s="T157">sehr</ta>
            <ta e="T159" id="Seg_6595" s="T158">betrachten-CVB.SIM</ta>
            <ta e="T160" id="Seg_6596" s="T159">stehen-PST2.[3SG]</ta>
            <ta e="T161" id="Seg_6597" s="T160">Frau-PL-ACC</ta>
            <ta e="T162" id="Seg_6598" s="T161">Inneres-3SG-DAT/LOC</ta>
            <ta e="T163" id="Seg_6599" s="T162">denken-PRS.[3SG]</ta>
            <ta e="T164" id="Seg_6600" s="T163">wohl</ta>
            <ta e="T165" id="Seg_6601" s="T164">solch</ta>
            <ta e="T166" id="Seg_6602" s="T165">Kleid-INSTR</ta>
            <ta e="T167" id="Seg_6603" s="T166">laufen-CVB.SIM</ta>
            <ta e="T168" id="Seg_6604" s="T167">nicht.schaffen-NEG-3PL</ta>
            <ta e="T169" id="Seg_6605" s="T168">wenig</ta>
            <ta e="T170" id="Seg_6606" s="T169">nur</ta>
            <ta e="T171" id="Seg_6607" s="T170">go-PRS-3PL</ta>
            <ta e="T172" id="Seg_6608" s="T171">sein-PST2.[3SG]</ta>
            <ta e="T173" id="Seg_6609" s="T172">Freund-PL-3SG-DAT/LOC</ta>
            <ta e="T174" id="Seg_6610" s="T173">was-ACC</ta>
            <ta e="T175" id="Seg_6611" s="T174">INDEF</ta>
            <ta e="T176" id="Seg_6612" s="T175">dieses.[NOM]</ta>
            <ta e="T177" id="Seg_6613" s="T176">Seite-3SG-INSTR</ta>
            <ta e="T178" id="Seg_6614" s="T177">erzählen-CVB.PURP</ta>
            <ta e="T179" id="Seg_6615" s="T178">wollen-PST2.[3SG]</ta>
            <ta e="T180" id="Seg_6616" s="T179">Mund-3SG-ACC</ta>
            <ta e="T181" id="Seg_6617" s="T180">nur</ta>
            <ta e="T182" id="Seg_6618" s="T181">öffnen-PST2.[3SG]</ta>
            <ta e="T183" id="Seg_6619" s="T182">was.[NOM]</ta>
            <ta e="T184" id="Seg_6620" s="T183">INDEF</ta>
            <ta e="T185" id="Seg_6621" s="T184">Lärm-3SG.[NOM]</ta>
            <ta e="T186" id="Seg_6622" s="T185">werden-PST2.[3SG]</ta>
            <ta e="T187" id="Seg_6623" s="T186">sehen-PST2.[3SG]</ta>
            <ta e="T188" id="Seg_6624" s="T187">jenes.[NOM]</ta>
            <ta e="T189" id="Seg_6625" s="T188">zu</ta>
            <ta e="T190" id="Seg_6626" s="T189">woher</ta>
            <ta e="T191" id="Seg_6627" s="T190">Lärm.[NOM]</ta>
            <ta e="T192" id="Seg_6628" s="T191">hinausgehen-EP-PST2.[3SG]</ta>
            <ta e="T195" id="Seg_6629" s="T194">was.[NOM]</ta>
            <ta e="T196" id="Seg_6630" s="T195">INDEF</ta>
            <ta e="T197" id="Seg_6631" s="T196">Mensch.[NOM]</ta>
            <ta e="T198" id="Seg_6632" s="T197">sehen-NEG.PTCP.PST</ta>
            <ta e="T199" id="Seg_6633" s="T198">Schlitten-PROPR-3SG.[NOM]</ta>
            <ta e="T200" id="Seg_6634" s="T199">wählen-MED-EP-CAUS-CVB.SEQ</ta>
            <ta e="T201" id="Seg_6635" s="T200">gehen-PST2.[3SG]</ta>
            <ta e="T202" id="Seg_6636" s="T201">Schlitten-3SG.[NOM]</ta>
            <ta e="T203" id="Seg_6637" s="T202">wundersam.[NOM]</ta>
            <ta e="T204" id="Seg_6638" s="T203">Rentier-3SG.[NOM]</ta>
            <ta e="T205" id="Seg_6639" s="T204">aber</ta>
            <ta e="T206" id="Seg_6640" s="T205">groß-3SG.[NOM]</ta>
            <ta e="T207" id="Seg_6641" s="T206">sehr</ta>
            <ta e="T208" id="Seg_6642" s="T207">sehr</ta>
            <ta e="T209" id="Seg_6643" s="T208">lang</ta>
            <ta e="T210" id="Seg_6644" s="T209">schwenken-PTCP.PRS</ta>
            <ta e="T211" id="Seg_6645" s="T210">ähnlich</ta>
            <ta e="T212" id="Seg_6646" s="T211">Schwanz-PROPR.[NOM]</ta>
            <ta e="T213" id="Seg_6647" s="T212">Schlitten-VBZ-PTCP.PRS</ta>
            <ta e="T214" id="Seg_6648" s="T213">Mensch-3SG.[NOM]</ta>
            <ta e="T215" id="Seg_6649" s="T214">aber</ta>
            <ta e="T216" id="Seg_6650" s="T215">Gesicht-3SG.[NOM]</ta>
            <ta e="T217" id="Seg_6651" s="T216">Tierhaar-3SG.[NOM]</ta>
            <ta e="T218" id="Seg_6652" s="T217">sehr</ta>
            <ta e="T219" id="Seg_6653" s="T218">pelzig</ta>
            <ta e="T220" id="Seg_6654" s="T219">Bart-PROPR.[NOM]</ta>
            <ta e="T221" id="Seg_6655" s="T220">Rentier-3SG-GEN</ta>
            <ta e="T222" id="Seg_6656" s="T221">Hinterteil-3SG-ABL</ta>
            <ta e="T223" id="Seg_6657" s="T222">zu.Fuß</ta>
            <ta e="T224" id="Seg_6658" s="T223">go-CVB.SEQ</ta>
            <ta e="T225" id="Seg_6659" s="T224">gehen-CVB.SEQ</ta>
            <ta e="T226" id="Seg_6660" s="T225">halten-PST2.[3SG]</ta>
            <ta e="T227" id="Seg_6661" s="T226">zwei</ta>
            <ta e="T228" id="Seg_6662" s="T227">Zügel-ACC</ta>
            <ta e="T229" id="Seg_6663" s="T228">Rentier-3SG.[NOM]</ta>
            <ta e="T230" id="Seg_6664" s="T229">wie</ta>
            <ta e="T231" id="Seg_6665" s="T230">INDEF</ta>
            <ta e="T232" id="Seg_6666" s="T231">schreiten-PTCP.PRS-3SG-ACC</ta>
            <ta e="T233" id="Seg_6667" s="T232">jeder</ta>
            <ta e="T234" id="Seg_6668" s="T233">Kopf-3SG-ACC</ta>
            <ta e="T235" id="Seg_6669" s="T234">sich.bewegen-CAUS-PRS.[3SG]</ta>
            <ta e="T236" id="Seg_6670" s="T235">jenes.[NOM]</ta>
            <ta e="T237" id="Seg_6671" s="T236">jeder</ta>
            <ta e="T238" id="Seg_6672" s="T237">Schlitten-AG.[NOM]</ta>
            <ta e="T239" id="Seg_6673" s="T238">noo</ta>
            <ta e="T240" id="Seg_6674" s="T239">noo</ta>
            <ta e="T241" id="Seg_6675" s="T240">sagen-PRS.[3SG]</ta>
            <ta e="T242" id="Seg_6676" s="T241">Märchen-ACC</ta>
            <ta e="T243" id="Seg_6677" s="T242">zuhören-PTCP.PRS.[NOM]</ta>
            <ta e="T244" id="Seg_6678" s="T243">ähnlich</ta>
            <ta e="T245" id="Seg_6679" s="T244">stark</ta>
            <ta e="T246" id="Seg_6680" s="T245">sehr-ADVZ</ta>
            <ta e="T247" id="Seg_6681" s="T246">Mund-3SG-INSTR</ta>
            <ta e="T248" id="Seg_6682" s="T247">probieren-PST2.[3SG]</ta>
            <ta e="T249" id="Seg_6683" s="T248">süß</ta>
            <ta e="T250" id="Seg_6684" s="T249">Fett-ACC</ta>
            <ta e="T251" id="Seg_6685" s="T250">essen-PTCP.PRS.[NOM]</ta>
            <ta e="T252" id="Seg_6686" s="T251">ähnlich</ta>
            <ta e="T253" id="Seg_6687" s="T252">na</ta>
            <ta e="T254" id="Seg_6688" s="T253">dieses.[NOM]</ta>
            <ta e="T255" id="Seg_6689" s="T254">was.[NOM]</ta>
            <ta e="T256" id="Seg_6690" s="T255">Rentier-3SG.[NOM]</ta>
            <ta e="T257" id="Seg_6691" s="T256">sein-PRS.[3SG]</ta>
            <ta e="T258" id="Seg_6692" s="T257">sehen-CVB.SIM</ta>
            <ta e="T259" id="Seg_6693" s="T258">stehen-CVB.SEQ</ta>
            <ta e="T260" id="Seg_6694" s="T259">sich.freuen-CVB.COND</ta>
            <ta e="T261" id="Seg_6695" s="T260">Tojoo.[NOM]</ta>
            <ta e="T262" id="Seg_6696" s="T261">Stimme.[NOM]</ta>
            <ta e="T263" id="Seg_6697" s="T262">auftreten-EP-PST2.[3SG]</ta>
            <ta e="T264" id="Seg_6698" s="T263">3SG.[NOM]</ta>
            <ta e="T265" id="Seg_6699" s="T264">nur</ta>
            <ta e="T266" id="Seg_6700" s="T265">jenes-ACC</ta>
            <ta e="T267" id="Seg_6701" s="T266">sehen-CVB.SEQ</ta>
            <ta e="T268" id="Seg_6702" s="T267">stehen-PST2.NEG-3SG</ta>
            <ta e="T269" id="Seg_6703" s="T268">jeder</ta>
            <ta e="T270" id="Seg_6704" s="T269">Kind-PL.[NOM]</ta>
            <ta e="T271" id="Seg_6705" s="T270">Wunder-ADJZ.[NOM]</ta>
            <ta e="T272" id="Seg_6706" s="T271">werden-PST2-3PL</ta>
            <ta e="T273" id="Seg_6707" s="T272">selbst.[NOM]</ta>
            <ta e="T274" id="Seg_6708" s="T273">selbst-3PL-ACC</ta>
            <ta e="T275" id="Seg_6709" s="T274">mit</ta>
            <ta e="T276" id="Seg_6710" s="T275">sich.unterhalten-CVB.SIM</ta>
            <ta e="T277" id="Seg_6711" s="T276">noch.nicht-3PL</ta>
            <ta e="T278" id="Seg_6712" s="T277">wieder</ta>
            <ta e="T279" id="Seg_6713" s="T278">sehen-PST2-3PL</ta>
            <ta e="T280" id="Seg_6714" s="T279">neu</ta>
            <ta e="T281" id="Seg_6715" s="T280">Wunder-ACC</ta>
            <ta e="T282" id="Seg_6716" s="T281">alt</ta>
            <ta e="T283" id="Seg_6717" s="T282">russisch</ta>
            <ta e="T284" id="Seg_6718" s="T283">Alte-3SG.[NOM]</ta>
            <ta e="T285" id="Seg_6719" s="T284">was.[NOM]</ta>
            <ta e="T286" id="Seg_6720" s="T285">INDEF</ta>
            <ta e="T287" id="Seg_6721" s="T286">klein</ta>
            <ta e="T288" id="Seg_6722" s="T287">Rentier-DIM-PL-3SG-ACC</ta>
            <ta e="T289" id="Seg_6723" s="T288">jagen-CVB.SEQ</ta>
            <ta e="T290" id="Seg_6724" s="T289">gehen-PST2.[3SG]</ta>
            <ta e="T291" id="Seg_6725" s="T290">nackt</ta>
            <ta e="T292" id="Seg_6726" s="T291">ohne.Geweih</ta>
            <ta e="T293" id="Seg_6727" s="T292">Rentier-DIM-PL.[NOM]</ta>
            <ta e="T294" id="Seg_6728" s="T293">niedrig</ta>
            <ta e="T295" id="Seg_6729" s="T294">Bein-3PL-INSTR</ta>
            <ta e="T296" id="Seg_6730" s="T295">laufen-FREQ-CVB.SEQ</ta>
            <ta e="T297" id="Seg_6731" s="T296">gehen-PST2-3PL</ta>
            <ta e="T298" id="Seg_6732" s="T297">laut.schreien-CVB.SIM-laut.schreien-CVB.SIM</ta>
            <ta e="T299" id="Seg_6733" s="T298">jenes-3SG-3PL.[NOM]</ta>
            <ta e="T300" id="Seg_6734" s="T299">rennen-CVB.SEQ</ta>
            <ta e="T301" id="Seg_6735" s="T300">gehen-CVB.SEQ-3PL</ta>
            <ta e="T302" id="Seg_6736" s="T301">jenes-dieses</ta>
            <ta e="T303" id="Seg_6737" s="T302">Erde.[NOM]</ta>
            <ta e="T304" id="Seg_6738" s="T303">zu</ta>
            <ta e="T305" id="Seg_6739" s="T304">sich.verteilen-PRS-3PL</ta>
            <ta e="T306" id="Seg_6740" s="T305">ui</ta>
            <ta e="T307" id="Seg_6741" s="T306">sagen-CVB.SEQ</ta>
            <ta e="T308" id="Seg_6742" s="T307">schreien-PST2.[3SG]</ta>
            <ta e="T309" id="Seg_6743" s="T308">Tojoo.[NOM]</ta>
            <ta e="T310" id="Seg_6744" s="T309">nackt</ta>
            <ta e="T311" id="Seg_6745" s="T310">wildes.Rentier-PL.[NOM]</ta>
            <ta e="T312" id="Seg_6746" s="T311">Kind-PL.[NOM]</ta>
            <ta e="T313" id="Seg_6747" s="T312">letzter</ta>
            <ta e="T314" id="Seg_6748" s="T313">Bauch-3PL-INSTR</ta>
            <ta e="T315" id="Seg_6749" s="T314">lachen-RECP/COLL-PRS-3PL</ta>
            <ta e="T316" id="Seg_6750" s="T315">Finger-3PL-INSTR</ta>
            <ta e="T317" id="Seg_6751" s="T316">sehen-CAUS-CVB.SIM-sehen-CAUS-CVB.SIM</ta>
            <ta e="T318" id="Seg_6752" s="T317">sehen.[IMP.2SG]</ta>
            <ta e="T319" id="Seg_6753" s="T318">Freund</ta>
            <ta e="T320" id="Seg_6754" s="T319">Nase-3PL.[NOM]</ta>
            <ta e="T321" id="Seg_6755" s="T320">Lenkstange.[NOM]</ta>
            <ta e="T322" id="Seg_6756" s="T321">Ring.am.Ende.der.Lenkstange-3SG-ACC</ta>
            <ta e="T323" id="Seg_6757" s="T322">ähnlich</ta>
            <ta e="T324" id="Seg_6758" s="T323">dieses.[NOM]</ta>
            <ta e="T325" id="Seg_6759" s="T324">nur</ta>
            <ta e="T326" id="Seg_6760" s="T325">zwei</ta>
            <ta e="T327" id="Seg_6761" s="T326">nur</ta>
            <ta e="T328" id="Seg_6762" s="T327">Loch-PROPR-3PL</ta>
            <ta e="T329" id="Seg_6763" s="T328">Rentier.[NOM]</ta>
            <ta e="T330" id="Seg_6764" s="T329">solch</ta>
            <ta e="T331" id="Seg_6765" s="T330">sein-PST2.NEG.[3SG]</ta>
            <ta e="T332" id="Seg_6766" s="T331">unsere.[NOM]</ta>
            <ta e="T333" id="Seg_6767" s="T332">schön-PL.[NOM]</ta>
            <ta e="T334" id="Seg_6768" s="T333">zuerst</ta>
            <ta e="T335" id="Seg_6769" s="T334">Angst.haben-CVB.SIM</ta>
            <ta e="T336" id="Seg_6770" s="T335">schlagen-PST2-3PL</ta>
            <ta e="T337" id="Seg_6771" s="T336">dann</ta>
            <ta e="T338" id="Seg_6772" s="T337">sich.nähern-PST2-3PL</ta>
            <ta e="T339" id="Seg_6773" s="T338">gut-ADVZ</ta>
            <ta e="T340" id="Seg_6774" s="T339">sehen-CVB.PURP</ta>
            <ta e="T341" id="Seg_6775" s="T340">Tojoo.[NOM]</ta>
            <ta e="T342" id="Seg_6776" s="T341">selbst-3SG-GEN</ta>
            <ta e="T343" id="Seg_6777" s="T342">Temperament-3SG-ACC</ta>
            <ta e="T344" id="Seg_6778" s="T343">siegen-PST2.NEG.[3SG]</ta>
            <ta e="T345" id="Seg_6779" s="T344">eins-3PL-DAT/LOC</ta>
            <ta e="T346" id="Seg_6780" s="T345">laufen-CVB.SEQ</ta>
            <ta e="T347" id="Seg_6781" s="T346">kommen-CVB.SEQ</ta>
            <ta e="T348" id="Seg_6782" s="T347">oberer.Teil-3SG-DAT/LOC</ta>
            <ta e="T349" id="Seg_6783" s="T348">Reitrentier-VBZ-CVB.SIM</ta>
            <ta e="T350" id="Seg_6784" s="T349">machen-CVB.SIM</ta>
            <ta e="T351" id="Seg_6785" s="T350">springen-PST2.[3SG]</ta>
            <ta e="T352" id="Seg_6786" s="T351">wildes.Rentier-3SG.[NOM]</ta>
            <ta e="T353" id="Seg_6787" s="T352">Rücken-3SG-DAT/LOC</ta>
            <ta e="T354" id="Seg_6788" s="T353">Mensch-ACC</ta>
            <ta e="T355" id="Seg_6789" s="T354">bemerken-CVB.SEQ</ta>
            <ta e="T356" id="Seg_6790" s="T355">stehen-PTCP.PST</ta>
            <ta e="T357" id="Seg_6791" s="T356">Erde-3SG-ABL</ta>
            <ta e="T358" id="Seg_6792" s="T357">springen-PST2.[3SG]</ta>
            <ta e="T359" id="Seg_6793" s="T358">Angst.haben-CVB.COND</ta>
            <ta e="T360" id="Seg_6794" s="T359">letzter</ta>
            <ta e="T361" id="Seg_6795" s="T360">Kraft-3SG-INSTR</ta>
            <ta e="T362" id="Seg_6796" s="T361">rennen-PST2.[3SG]</ta>
            <ta e="T363" id="Seg_6797" s="T362">Kopf-3SG-ACC</ta>
            <ta e="T364" id="Seg_6798" s="T363">in.die.Richtung</ta>
            <ta e="T365" id="Seg_6799" s="T364">Reitrentier-ABL</ta>
            <ta e="T366" id="Seg_6800" s="T365">fallen-NEG.PTCP</ta>
            <ta e="T367" id="Seg_6801" s="T366">Kind.[NOM]</ta>
            <ta e="T368" id="Seg_6802" s="T367">ergreifen-CVB.SIM</ta>
            <ta e="T369" id="Seg_6803" s="T368">versuchen-PST2.[3SG]</ta>
            <ta e="T370" id="Seg_6804" s="T369">Rentier-3SG-GEN</ta>
            <ta e="T371" id="Seg_6805" s="T370">Tierhaar-3SG-ABL</ta>
            <ta e="T372" id="Seg_6806" s="T371">wie</ta>
            <ta e="T373" id="Seg_6807" s="T372">NEG</ta>
            <ta e="T374" id="Seg_6808" s="T373">wo</ta>
            <ta e="T375" id="Seg_6809" s="T374">NEG</ta>
            <ta e="T376" id="Seg_6810" s="T375">Hand-PL-3SG.[NOM]</ta>
            <ta e="T377" id="Seg_6811" s="T376">hängen.bleiben-EP-NEG.[3SG]</ta>
            <ta e="T378" id="Seg_6812" s="T377">Rentier-3SG-GEN</ta>
            <ta e="T379" id="Seg_6813" s="T378">Tierhaar-PL-3SG.[NOM]</ta>
            <ta e="T380" id="Seg_6814" s="T379">kleiner.Nagel.[NOM]</ta>
            <ta e="T381" id="Seg_6815" s="T380">wie-PL</ta>
            <ta e="T382" id="Seg_6816" s="T381">Reitrentier-3SG.[NOM]</ta>
            <ta e="T383" id="Seg_6817" s="T382">letzter</ta>
            <ta e="T384" id="Seg_6818" s="T383">Kraft-3SG-INSTR</ta>
            <ta e="T385" id="Seg_6819" s="T384">schreien-PST2.[3SG]</ta>
            <ta e="T386" id="Seg_6820" s="T385">Kopf-3SG-ACC</ta>
            <ta e="T387" id="Seg_6821" s="T386">in.die.Richtung</ta>
            <ta e="T388" id="Seg_6822" s="T387">laufen-CVB.SEQ</ta>
            <ta e="T389" id="Seg_6823" s="T388">gehen-PST2.[3SG]</ta>
            <ta e="T390" id="Seg_6824" s="T389">mancher</ta>
            <ta e="T391" id="Seg_6825" s="T390">Schwein-PL.[NOM]</ta>
            <ta e="T392" id="Seg_6826" s="T391">jenes-ACC</ta>
            <ta e="T393" id="Seg_6827" s="T392">sehen-CVB.SEQ-3PL</ta>
            <ta e="T394" id="Seg_6828" s="T393">eins</ta>
            <ta e="T395" id="Seg_6829" s="T394">Augenblick.[NOM]</ta>
            <ta e="T396" id="Seg_6830" s="T395">rennen-PST2-3PL</ta>
            <ta e="T397" id="Seg_6831" s="T396">Hinterteil-3PL-ABL</ta>
            <ta e="T398" id="Seg_6832" s="T397">alt</ta>
            <ta e="T399" id="Seg_6833" s="T398">Alte.[NOM]</ta>
            <ta e="T400" id="Seg_6834" s="T399">laufen-CVB.SIM</ta>
            <ta e="T401" id="Seg_6835" s="T400">versuchen-PST2.[3SG]</ta>
            <ta e="T402" id="Seg_6836" s="T401">Tojoo.[NOM]</ta>
            <ta e="T403" id="Seg_6837" s="T402">was-ACC</ta>
            <ta e="T404" id="Seg_6838" s="T403">nur</ta>
            <ta e="T405" id="Seg_6839" s="T404">schreien-CVB.SIM-schreien-CVB.SIM</ta>
            <ta e="T406" id="Seg_6840" s="T405">laufen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T407" id="Seg_6841" s="T406">jeder</ta>
            <ta e="T408" id="Seg_6842" s="T407">Tojoo.[NOM]</ta>
            <ta e="T409" id="Seg_6843" s="T408">zu</ta>
            <ta e="T410" id="Seg_6844" s="T409">Stab-3SG-INSTR</ta>
            <ta e="T411" id="Seg_6845" s="T410">schwingen-PRS.[3SG]</ta>
            <ta e="T412" id="Seg_6846" s="T411">nackt</ta>
            <ta e="T413" id="Seg_6847" s="T412">Reitrentier-3SG.[NOM]</ta>
            <ta e="T414" id="Seg_6848" s="T413">dreckig</ta>
            <ta e="T415" id="Seg_6849" s="T414">Matsch-PROPR</ta>
            <ta e="T416" id="Seg_6850" s="T415">Wasser-PROPR</ta>
            <ta e="T417" id="Seg_6851" s="T416">Grube-DAT/LOC</ta>
            <ta e="T418" id="Seg_6852" s="T417">platsch</ta>
            <ta e="T419" id="Seg_6853" s="T418">machen-CVB.SIM</ta>
            <ta e="T420" id="Seg_6854" s="T419">fallen-PST2.[3SG]</ta>
            <ta e="T421" id="Seg_6855" s="T420">Tojoo.[NOM]</ta>
            <ta e="T422" id="Seg_6856" s="T421">Schwein-3SG-ACC</ta>
            <ta e="T423" id="Seg_6857" s="T422">mit</ta>
            <ta e="T424" id="Seg_6858" s="T423">aufstehen-CVB.SIM</ta>
            <ta e="T425" id="Seg_6859" s="T424">vergeblich.tun-CVB.SEQ</ta>
            <ta e="T426" id="Seg_6860" s="T425">liegen-PST2.[3SG]</ta>
            <ta e="T427" id="Seg_6861" s="T426">Gesicht-3SG.[NOM]</ta>
            <ta e="T428" id="Seg_6862" s="T427">NEG</ta>
            <ta e="T429" id="Seg_6863" s="T428">was-3SG.[NOM]</ta>
            <ta e="T430" id="Seg_6864" s="T429">NEG</ta>
            <ta e="T431" id="Seg_6865" s="T430">zu.sehen.sein-EP-NEG.[3SG]</ta>
            <ta e="T432" id="Seg_6866" s="T431">Lehm.[NOM]</ta>
            <ta e="T433" id="Seg_6867" s="T432">werden-PST2.[3SG]</ta>
            <ta e="T434" id="Seg_6868" s="T433">aufstehen-CVB.SIM</ta>
            <ta e="T435" id="Seg_6869" s="T434">versuchen-CVB.SEQ</ta>
            <ta e="T436" id="Seg_6870" s="T435">letzter-3SG.[NOM]</ta>
            <ta e="T437" id="Seg_6871" s="T436">sein-CVB.SEQ</ta>
            <ta e="T438" id="Seg_6872" s="T437">Alte.[NOM]</ta>
            <ta e="T439" id="Seg_6873" s="T438">erreichen-PST2.[3SG]</ta>
            <ta e="T440" id="Seg_6874" s="T439">3SG-ACC</ta>
            <ta e="T441" id="Seg_6875" s="T440">Tojoo.[NOM]</ta>
            <ta e="T442" id="Seg_6876" s="T441">Gesicht-3SG-ACC</ta>
            <ta e="T443" id="Seg_6877" s="T442">sehen-CVB.SEQ</ta>
            <ta e="T444" id="Seg_6878" s="T443">in.Lachen.ausbrechen-PST2.[3SG]</ta>
            <ta e="T445" id="Seg_6879" s="T444">Hand-3SG-INSTR</ta>
            <ta e="T446" id="Seg_6880" s="T445">nur</ta>
            <ta e="T447" id="Seg_6881" s="T446">winken-PST2.[3SG]</ta>
            <ta e="T448" id="Seg_6882" s="T447">durchbrechen.[IMP.2SG]</ta>
            <ta e="T449" id="Seg_6883" s="T448">woher</ta>
            <ta e="T450" id="Seg_6884" s="T449">kommen-PST2-EP-2SG</ta>
            <ta e="T451" id="Seg_6885" s="T450">sagen-PTCP.PST.[NOM]</ta>
            <ta e="T452" id="Seg_6886" s="T451">ähnlich</ta>
            <ta e="T453" id="Seg_6887" s="T452">Tojoo.[NOM]</ta>
            <ta e="T454" id="Seg_6888" s="T453">aufstehen-CVB.SIM</ta>
            <ta e="T455" id="Seg_6889" s="T454">springen-CVB.ANT</ta>
            <ta e="T456" id="Seg_6890" s="T455">letzter</ta>
            <ta e="T457" id="Seg_6891" s="T456">Kraft-3SG-INSTR</ta>
            <ta e="T458" id="Seg_6892" s="T457">Atem-POSS</ta>
            <ta e="T459" id="Seg_6893" s="T458">NEG</ta>
            <ta e="T460" id="Seg_6894" s="T459">Freund-PL-3SG-ACC</ta>
            <ta e="T461" id="Seg_6895" s="T460">zu</ta>
            <ta e="T462" id="Seg_6896" s="T461">treten-PST2.[3SG]</ta>
            <ta e="T463" id="Seg_6897" s="T462">oh.nein</ta>
            <ta e="T464" id="Seg_6898" s="T463">kaum</ta>
            <ta e="T465" id="Seg_6899" s="T464">sterben-CVB.SIM</ta>
            <ta e="T466" id="Seg_6900" s="T465">schlagen-PST1-1SG</ta>
            <ta e="T467" id="Seg_6901" s="T466">kaum</ta>
            <ta e="T468" id="Seg_6902" s="T467">sterben-CVB.SIM</ta>
            <ta e="T469" id="Seg_6903" s="T468">schlagen-PST1-1SG</ta>
            <ta e="T470" id="Seg_6904" s="T469">Lehrer-3PL.[NOM]</ta>
            <ta e="T471" id="Seg_6905" s="T470">Direktor.[NOM]</ta>
            <ta e="T472" id="Seg_6906" s="T471">sein-CVB.SEQ</ta>
            <ta e="T473" id="Seg_6907" s="T472">was.[NOM]</ta>
            <ta e="T474" id="Seg_6908" s="T473">NEG</ta>
            <ta e="T475" id="Seg_6909" s="T474">sagen-PTCP.FUT-3PL-ACC</ta>
            <ta e="T476" id="Seg_6910" s="T475">Kraft-3SG-ABL</ta>
            <ta e="T477" id="Seg_6911" s="T476">letzter</ta>
            <ta e="T478" id="Seg_6912" s="T477">Seele-3PL-INSTR</ta>
            <ta e="T479" id="Seg_6913" s="T478">lachen-CVB.SIM</ta>
            <ta e="T480" id="Seg_6914" s="T479">stehen-PST2-3PL</ta>
            <ta e="T482" id="Seg_6915" s="T481">letzter</ta>
            <ta e="T483" id="Seg_6916" s="T482">Gedanke-3SG-ACC</ta>
            <ta e="T484" id="Seg_6917" s="T483">sprechen-PST2.[3SG]</ta>
            <ta e="T485" id="Seg_6918" s="T484">wie</ta>
            <ta e="T486" id="Seg_6919" s="T485">NEG</ta>
            <ta e="T487" id="Seg_6920" s="T486">sagen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T488" id="Seg_6921" s="T487">sehr</ta>
            <ta e="T489" id="Seg_6922" s="T488">sehr</ta>
            <ta e="T490" id="Seg_6923" s="T489">flink</ta>
            <ta e="T491" id="Seg_6924" s="T490">Leute.[NOM]</ta>
            <ta e="T492" id="Seg_6925" s="T491">kommen-PST2-3PL</ta>
            <ta e="T494" id="Seg_6926" s="T492">offenbar</ta>
            <ta e="T495" id="Seg_6927" s="T494">Kollege-PL-3SG-DAT/LOC</ta>
            <ta e="T496" id="Seg_6928" s="T495">sagen-PST2.[3SG]</ta>
            <ta e="T497" id="Seg_6929" s="T496">jetzt</ta>
            <ta e="T498" id="Seg_6930" s="T497">3PL-ACC</ta>
            <ta e="T499" id="Seg_6931" s="T498">Internat-DAT/LOC</ta>
            <ta e="T500" id="Seg_6932" s="T499">mitnehmen-EP-IMP.2PL</ta>
            <ta e="T501" id="Seg_6933" s="T500">dann</ta>
            <ta e="T502" id="Seg_6934" s="T501">sich.waschen-CAUS-EP-IMP.2PL</ta>
            <ta e="T503" id="Seg_6935" s="T502">Nahrung-3PL-ACC</ta>
            <ta e="T504" id="Seg_6936" s="T503">vorbereiten-PTCP.FUT-3PL-DAT/LOC</ta>
            <ta e="T505" id="Seg_6937" s="T504">bis.zu</ta>
            <ta e="T506" id="Seg_6938" s="T505">ganz-3SG-ACC</ta>
            <ta e="T507" id="Seg_6939" s="T506">gründlich-ADVZ</ta>
            <ta e="T508" id="Seg_6940" s="T507">machen-EP-IMP.2PL</ta>
            <ta e="T509" id="Seg_6941" s="T508">Kind-PL-ACC</ta>
            <ta e="T510" id="Seg_6942" s="T509">ängstigen.[CAUS]-NEG.CVB.SIM</ta>
            <ta e="T511" id="Seg_6943" s="T510">Kind-PL-ACC</ta>
            <ta e="T512" id="Seg_6944" s="T511">sofort</ta>
            <ta e="T513" id="Seg_6945" s="T512">jagen-CVB.SEQ</ta>
            <ta e="T514" id="Seg_6946" s="T513">bringen-PST2-3PL</ta>
            <ta e="T515" id="Seg_6947" s="T514">zeigen-PST2-3PL</ta>
            <ta e="T516" id="Seg_6948" s="T515">jeder-3PL-DAT/LOC</ta>
            <ta e="T517" id="Seg_6949" s="T516">leben-PTCP.PRS</ta>
            <ta e="T518" id="Seg_6950" s="T517">Haus-3PL-ACC</ta>
            <ta e="T519" id="Seg_6951" s="T518">russisch-SIM</ta>
            <ta e="T521" id="Seg_6952" s="T520">Name-ADJZ.[NOM]</ta>
            <ta e="T522" id="Seg_6953" s="T521">flicken-PTCP.PRS</ta>
            <ta e="T523" id="Seg_6954" s="T522">neu</ta>
            <ta e="T524" id="Seg_6955" s="T523">Hemd-PL-ACC</ta>
            <ta e="T525" id="Seg_6956" s="T524">Schuhe-PL-ACC</ta>
            <ta e="T526" id="Seg_6957" s="T525">geben-FREQ-PST2-3PL</ta>
            <ta e="T527" id="Seg_6958" s="T526">jeder</ta>
            <ta e="T528" id="Seg_6959" s="T527">kommen-PTCP.PST</ta>
            <ta e="T529" id="Seg_6960" s="T528">Leute.[NOM]</ta>
            <ta e="T530" id="Seg_6961" s="T529">Haar-PL-3SG-ACC</ta>
            <ta e="T531" id="Seg_6962" s="T530">schneiden-PST2-3PL</ta>
            <ta e="T532" id="Seg_6963" s="T531">was-ABL</ta>
            <ta e="T533" id="Seg_6964" s="T532">Mädchen.[NOM]</ta>
            <ta e="T534" id="Seg_6965" s="T533">Kind-PL.[NOM]</ta>
            <ta e="T535" id="Seg_6966" s="T534">weinen-PTCP.PRS-3PL.[NOM]</ta>
            <ta e="T536" id="Seg_6967" s="T535">sehr</ta>
            <ta e="T537" id="Seg_6968" s="T536">sein-PST2.[3SG]</ta>
            <ta e="T538" id="Seg_6969" s="T537">Haar-3PL-ACC</ta>
            <ta e="T539" id="Seg_6970" s="T538">bemitleiden-CVB.SEQ-3PL</ta>
            <ta e="T540" id="Seg_6971" s="T539">Tojoo-ACC</ta>
            <ta e="T541" id="Seg_6972" s="T540">auch</ta>
            <ta e="T542" id="Seg_6973" s="T541">schneiden-PST2-3PL</ta>
            <ta e="T543" id="Seg_6974" s="T542">sich.anziehen-PTCP.PRS</ta>
            <ta e="T544" id="Seg_6975" s="T543">Kleidung.[NOM]</ta>
            <ta e="T545" id="Seg_6976" s="T544">geben-PST2-3PL</ta>
            <ta e="T546" id="Seg_6977" s="T545">anziehen-PTCP.PRS</ta>
            <ta e="T547" id="Seg_6978" s="T546">Huf-3PL-ACC</ta>
            <ta e="T548" id="Seg_6979" s="T547">überreichen-PST2-3PL</ta>
            <ta e="T549" id="Seg_6980" s="T548">Bett-3SG-DAT/LOC</ta>
            <ta e="T550" id="Seg_6981" s="T549">kommen-CVB.ANT</ta>
            <ta e="T551" id="Seg_6982" s="T550">sich.setzen-PST2.[3SG]</ta>
            <ta e="T552" id="Seg_6983" s="T551">schauen.auf-CVB.SIM</ta>
            <ta e="T553" id="Seg_6984" s="T552">was-ACC</ta>
            <ta e="T554" id="Seg_6985" s="T553">überreichen-PTCP.PST-3PL-ACC</ta>
            <ta e="T555" id="Seg_6986" s="T554">jeder-3SG.[NOM]</ta>
            <ta e="T556" id="Seg_6987" s="T555">3SG-DAT/LOC</ta>
            <ta e="T557" id="Seg_6988" s="T556">in.Ordnung</ta>
            <ta e="T558" id="Seg_6989" s="T557">jenes</ta>
            <ta e="T559" id="Seg_6990" s="T558">und</ta>
            <ta e="T560" id="Seg_6991" s="T559">sein-COND.[3SG]</ta>
            <ta e="T561" id="Seg_6992" s="T560">warum</ta>
            <ta e="T562" id="Seg_6993" s="T561">3PL.[NOM]</ta>
            <ta e="T563" id="Seg_6994" s="T562">geben-PST2-3PL</ta>
            <ta e="T564" id="Seg_6995" s="T563">wahrscheinlich=Q</ta>
            <ta e="T565" id="Seg_6996" s="T564">zwei-DISTR</ta>
            <ta e="T566" id="Seg_6997" s="T565">Hemd-ACC</ta>
            <ta e="T567" id="Seg_6998" s="T566">zwei-DISTR</ta>
            <ta e="T568" id="Seg_6999" s="T567">Hose-ACC</ta>
            <ta e="T569" id="Seg_7000" s="T568">neu</ta>
            <ta e="T570" id="Seg_7001" s="T569">Kleidung-3PL-ACC</ta>
            <ta e="T571" id="Seg_7002" s="T570">anziehen-PTCP.PRS.[NOM]</ta>
            <ta e="T574" id="Seg_7003" s="T573">denken-PST2.[3SG]</ta>
            <ta e="T575" id="Seg_7004" s="T574">Kind-PL-ACC</ta>
            <ta e="T576" id="Seg_7005" s="T575">sich.waschen-CAUS-CVB.SIM</ta>
            <ta e="T577" id="Seg_7006" s="T576">bringen-PST2-3PL</ta>
            <ta e="T578" id="Seg_7007" s="T577">wo</ta>
            <ta e="T579" id="Seg_7008" s="T578">flink</ta>
            <ta e="T580" id="Seg_7009" s="T579">Leute.[NOM]</ta>
            <ta e="T581" id="Seg_7010" s="T580">letzter</ta>
            <ta e="T582" id="Seg_7011" s="T581">Gedanke-3PL-INSTR</ta>
            <ta e="T583" id="Seg_7012" s="T582">fröhlich-ADVZ</ta>
            <ta e="T584" id="Seg_7013" s="T583">lachen-RECP/COLL-CVB.SIM-lachen-RECP/COLL-CVB.SIM</ta>
            <ta e="T585" id="Seg_7014" s="T584">heiß.[NOM]</ta>
            <ta e="T586" id="Seg_7015" s="T585">heiß.[NOM]</ta>
            <ta e="T587" id="Seg_7016" s="T586">sagen-EP-RECP/COLL-CVB.SIM</ta>
            <ta e="T588" id="Seg_7017" s="T587">sitzen-PST2-3PL</ta>
            <ta e="T589" id="Seg_7018" s="T588">Tojoo.[NOM]</ta>
            <ta e="T590" id="Seg_7019" s="T589">sich.waschen-CVB.SEQ</ta>
            <ta e="T591" id="Seg_7020" s="T590">aufhören-CVB.ANT</ta>
            <ta e="T592" id="Seg_7021" s="T591">sich.anziehen-EP-PST2.[3SG]</ta>
            <ta e="T593" id="Seg_7022" s="T592">anziehen-PST2.[3SG]</ta>
            <ta e="T594" id="Seg_7023" s="T593">weiß</ta>
            <ta e="T595" id="Seg_7024" s="T594">Hemd-3SG-ACC</ta>
            <ta e="T596" id="Seg_7025" s="T595">Hose-3SG-ACC</ta>
            <ta e="T597" id="Seg_7026" s="T596">mancher-PL-3SG-ACC</ta>
            <ta e="T598" id="Seg_7027" s="T597">Vorrat-VBZ-CVB.SEQ</ta>
            <ta e="T599" id="Seg_7028" s="T598">einwickeln-PST2.[3SG]</ta>
            <ta e="T600" id="Seg_7029" s="T599">später</ta>
            <ta e="T601" id="Seg_7030" s="T600">anziehen-CVB.PURP</ta>
            <ta e="T602" id="Seg_7031" s="T601">Körper-3SG-DAT/LOC</ta>
            <ta e="T603" id="Seg_7032" s="T602">anziehen-CVB.SEQ</ta>
            <ta e="T604" id="Seg_7033" s="T603">weiß</ta>
            <ta e="T605" id="Seg_7034" s="T604">Kleidung-PL-3SG-ACC</ta>
            <ta e="T606" id="Seg_7035" s="T605">was.[NOM]</ta>
            <ta e="T607" id="Seg_7036" s="T606">NEG</ta>
            <ta e="T608" id="Seg_7037" s="T607">sein-NEG.PTCP.PST.[NOM]</ta>
            <ta e="T609" id="Seg_7038" s="T608">ähnlich</ta>
            <ta e="T610" id="Seg_7039" s="T609">gehen-EP-PST2.[3SG]</ta>
            <ta e="T611" id="Seg_7040" s="T610">Freund-PL-3SG.[NOM]</ta>
            <ta e="T612" id="Seg_7041" s="T611">EMPH</ta>
            <ta e="T613" id="Seg_7042" s="T612">was-ACC</ta>
            <ta e="T614" id="Seg_7043" s="T613">NEG</ta>
            <ta e="T615" id="Seg_7044" s="T614">sagen-NEG-3PL</ta>
            <ta e="T616" id="Seg_7045" s="T615">sein-PST2.[3SG]</ta>
            <ta e="T617" id="Seg_7046" s="T616">3SG-DAT/LOC</ta>
            <ta e="T618" id="Seg_7047" s="T617">Internat-DAT/LOC</ta>
            <ta e="T619" id="Seg_7048" s="T618">arbeiten-PTCP.PRS</ta>
            <ta e="T620" id="Seg_7049" s="T619">eins</ta>
            <ta e="T621" id="Seg_7050" s="T620">Frau.[NOM]</ta>
            <ta e="T622" id="Seg_7051" s="T621">erklären-PST2.[3SG]</ta>
            <ta e="T623" id="Seg_7052" s="T622">wie</ta>
            <ta e="T624" id="Seg_7053" s="T623">3SG.[NOM]</ta>
            <ta e="T625" id="Seg_7054" s="T624">sich.anziehen-PTCP.PRS</ta>
            <ta e="T626" id="Seg_7055" s="T625">werden-PTCP.FUT-3SG-ACC</ta>
            <ta e="T627" id="Seg_7056" s="T626">essen-PTCP.PRS</ta>
            <ta e="T628" id="Seg_7057" s="T627">Zeit-3PL.[NOM]</ta>
            <ta e="T629" id="Seg_7058" s="T628">kommen-PST2.[3SG]</ta>
            <ta e="T630" id="Seg_7059" s="T629">Kind-PL-ACC</ta>
            <ta e="T631" id="Seg_7060" s="T630">setzen-ITER-PST2-3PL</ta>
            <ta e="T634" id="Seg_7061" s="T633">lang</ta>
            <ta e="T635" id="Seg_7062" s="T634">sehr</ta>
            <ta e="T636" id="Seg_7063" s="T635">Tisch-PL.[NOM]</ta>
            <ta e="T637" id="Seg_7064" s="T636">zwei</ta>
            <ta e="T638" id="Seg_7065" s="T637">in.Richtung</ta>
            <ta e="T639" id="Seg_7066" s="T638">Seite-3PL-DAT/LOC</ta>
            <ta e="T640" id="Seg_7067" s="T639">groß</ta>
            <ta e="T641" id="Seg_7068" s="T640">Geschirr-PL-DAT/LOC</ta>
            <ta e="T642" id="Seg_7069" s="T641">legen-EP-PASS/REFL-EP-PST2-3PL</ta>
            <ta e="T643" id="Seg_7070" s="T642">schmackhaft</ta>
            <ta e="T644" id="Seg_7071" s="T643">Geruch-PROPR</ta>
            <ta e="T645" id="Seg_7072" s="T644">Brot-PL.[NOM]</ta>
            <ta e="T646" id="Seg_7073" s="T645">Kind.[NOM]</ta>
            <ta e="T647" id="Seg_7074" s="T646">jeder</ta>
            <ta e="T648" id="Seg_7075" s="T647">gießen-MULT-PST2-3PL</ta>
            <ta e="T649" id="Seg_7076" s="T648">Fisch.[NOM]</ta>
            <ta e="T650" id="Seg_7077" s="T649">Suppe-3SG-ACC</ta>
            <ta e="T651" id="Seg_7078" s="T650">Löffel-PL-ACC</ta>
            <ta e="T652" id="Seg_7079" s="T651">geben-FREQ-PST2-3PL</ta>
            <ta e="T653" id="Seg_7080" s="T652">sehen-PST2.[3SG]</ta>
            <ta e="T654" id="Seg_7081" s="T653">Kind-PL.[NOM]</ta>
            <ta e="T655" id="Seg_7082" s="T654">essen-CVB.SEQ-3PL</ta>
            <ta e="T656" id="Seg_7083" s="T655">Nacken-3PL.[NOM]</ta>
            <ta e="T657" id="Seg_7084" s="T656">nur</ta>
            <ta e="T658" id="Seg_7085" s="T657">verlorengehen-CVB.SIM</ta>
            <ta e="T659" id="Seg_7086" s="T658">liegen-PRS.[3SG]</ta>
            <ta e="T660" id="Seg_7087" s="T659">Tojoo.[NOM]</ta>
            <ta e="T661" id="Seg_7088" s="T660">essen-CVB.SIM</ta>
            <ta e="T662" id="Seg_7089" s="T661">sitzen-CVB.SEQ</ta>
            <ta e="T663" id="Seg_7090" s="T662">was-ACC</ta>
            <ta e="T664" id="Seg_7091" s="T663">INDEF</ta>
            <ta e="T665" id="Seg_7092" s="T664">Gedanke-3SG.[NOM]</ta>
            <ta e="T666" id="Seg_7093" s="T665">kommen-PST2.[3SG]</ta>
            <ta e="T667" id="Seg_7094" s="T666">Mama-3SG-ACC</ta>
            <ta e="T668" id="Seg_7095" s="T667">Vater-3SG-ACC</ta>
            <ta e="T669" id="Seg_7096" s="T668">Kytscha-3SG-ACC</ta>
            <ta e="T670" id="Seg_7097" s="T669">erinnern-CVB.SEQ</ta>
            <ta e="T671" id="Seg_7098" s="T670">essen-CVB.SEQ</ta>
            <ta e="T672" id="Seg_7099" s="T671">aufhören-CVB.ANT</ta>
            <ta e="T673" id="Seg_7100" s="T672">Kind-PL.[NOM]</ta>
            <ta e="T674" id="Seg_7101" s="T673">Volochanka.[NOM]</ta>
            <ta e="T675" id="Seg_7102" s="T674">entlang</ta>
            <ta e="T676" id="Seg_7103" s="T675">spielen-CVB.SIM</ta>
            <ta e="T677" id="Seg_7104" s="T676">gehen-EP-PST2-3PL</ta>
            <ta e="T678" id="Seg_7105" s="T677">schauen.auf-CVB.SIM</ta>
            <ta e="T679" id="Seg_7106" s="T678">russisch</ta>
            <ta e="T680" id="Seg_7107" s="T679">Haus-PL-ACC</ta>
            <ta e="T681" id="Seg_7108" s="T680">jeder</ta>
            <ta e="T682" id="Seg_7109" s="T681">Hof-ACC</ta>
            <ta e="T683" id="Seg_7110" s="T682">3PL-DAT/LOC</ta>
            <ta e="T684" id="Seg_7111" s="T683">jeder-3SG.[NOM]</ta>
            <ta e="T685" id="Seg_7112" s="T684">Wunder.[NOM]</ta>
            <ta e="T686" id="Seg_7113" s="T685">soeben</ta>
            <ta e="T687" id="Seg_7114" s="T686">kommen-PTCP.PST</ta>
            <ta e="T688" id="Seg_7115" s="T687">Mensch-PL-DAT/LOC</ta>
            <ta e="T689" id="Seg_7116" s="T688">nächster</ta>
            <ta e="T690" id="Seg_7117" s="T689">Tag.[NOM]</ta>
            <ta e="T691" id="Seg_7118" s="T690">wieder</ta>
            <ta e="T692" id="Seg_7119" s="T691">groß</ta>
            <ta e="T693" id="Seg_7120" s="T692">Kind-PROPR</ta>
            <ta e="T694" id="Seg_7121" s="T693">Rentierkarawane.[NOM]</ta>
            <ta e="T695" id="Seg_7122" s="T694">kommen-PST2.[3SG]</ta>
            <ta e="T696" id="Seg_7123" s="T695">anders</ta>
            <ta e="T697" id="Seg_7124" s="T696">Kolchose-ABL</ta>
            <ta e="T698" id="Seg_7125" s="T697">soeben</ta>
            <ta e="T699" id="Seg_7126" s="T698">kommen-CVB.SIM</ta>
            <ta e="T700" id="Seg_7127" s="T699">Kind-PL.[NOM]</ta>
            <ta e="T701" id="Seg_7128" s="T700">verwirrt-3SG.[NOM]</ta>
            <ta e="T702" id="Seg_7129" s="T701">sehr</ta>
            <ta e="T703" id="Seg_7130" s="T702">jeder-ABL</ta>
            <ta e="T705" id="Seg_7131" s="T704">Angst.haben-PTCP.PRS.[NOM]</ta>
            <ta e="T706" id="Seg_7132" s="T705">wie-PL</ta>
            <ta e="T707" id="Seg_7133" s="T706">3PL.[NOM]</ta>
            <ta e="T708" id="Seg_7134" s="T707">sehr</ta>
            <ta e="T709" id="Seg_7135" s="T708">lange</ta>
            <ta e="T710" id="Seg_7136" s="T709">solch.[NOM]</ta>
            <ta e="T711" id="Seg_7137" s="T710">sein-PST2.NEG-3PL</ta>
            <ta e="T712" id="Seg_7138" s="T711">auch</ta>
            <ta e="T713" id="Seg_7139" s="T712">Tojoo.[NOM]</ta>
            <ta e="T714" id="Seg_7140" s="T713">Mitja.[NOM]</ta>
            <ta e="T715" id="Seg_7141" s="T714">Ivan.[NOM]</ta>
            <ta e="T716" id="Seg_7142" s="T715">ähnlich</ta>
            <ta e="T717" id="Seg_7143" s="T716">sich.schämen-EP-NEG.PTCP</ta>
            <ta e="T718" id="Seg_7144" s="T717">Brauch-ACC</ta>
            <ta e="T719" id="Seg_7145" s="T718">nehmen-PST2-3PL</ta>
            <ta e="T720" id="Seg_7146" s="T719">neu</ta>
            <ta e="T721" id="Seg_7147" s="T720">Freund-PL-3SG-ACC</ta>
            <ta e="T722" id="Seg_7148" s="T721">sehen-CVB.SEQ-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_7149" s="T0">слушать-IMP.2PL</ta>
            <ta e="T2" id="Seg_7150" s="T1">рассказ-ACC</ta>
            <ta e="T3" id="Seg_7151" s="T2">чудо.[NOM]</ta>
            <ta e="T4" id="Seg_7152" s="T3">олень-PL.[NOM]</ta>
            <ta e="T5" id="Seg_7153" s="T4">три-ORD</ta>
            <ta e="T6" id="Seg_7154" s="T5">день-3SG-DAT/LOC</ta>
            <ta e="T7" id="Seg_7155" s="T6">ребенок-PROPR</ta>
            <ta e="T8" id="Seg_7156" s="T7">аргиш.[NOM]</ta>
            <ta e="T9" id="Seg_7157" s="T8">доезжать-PST2.[3SG]</ta>
            <ta e="T10" id="Seg_7158" s="T9">Волочанка-DAT/LOC</ta>
            <ta e="T11" id="Seg_7159" s="T10">Усть_Авам.[NOM]</ta>
            <ta e="T12" id="Seg_7160" s="T11">район-3SG-GEN</ta>
            <ta e="T13" id="Seg_7161" s="T12">центр.[NOM]</ta>
            <ta e="T14" id="Seg_7162" s="T13">стойбище-3SG-GEN</ta>
            <ta e="T15" id="Seg_7163" s="T14">русский-3PL.[NOM]</ta>
            <ta e="T16" id="Seg_7164" s="T15">долганин-3PL.[NOM]</ta>
            <ta e="T17" id="Seg_7165" s="T16">каждый-3PL.[NOM]</ta>
            <ta e="T18" id="Seg_7166" s="T17">собирать-PASS/REFL-EP-PST2-3PL</ta>
            <ta e="T19" id="Seg_7167" s="T18">приходить-NMNZ.[NOM]</ta>
            <ta e="T20" id="Seg_7168" s="T19">ребенок-PL-ACC</ta>
            <ta e="T21" id="Seg_7169" s="T20">видеть-CVB.PURP</ta>
            <ta e="T22" id="Seg_7170" s="T21">кукла.[NOM]</ta>
            <ta e="T23" id="Seg_7171" s="T22">подобно</ta>
            <ta e="T24" id="Seg_7172" s="T23">одежда-PROPR-PL-ACC</ta>
            <ta e="T25" id="Seg_7173" s="T24">человек.[NOM]</ta>
            <ta e="T26" id="Seg_7174" s="T25">любить-PTCP.FUT-PL-ACC</ta>
            <ta e="T27" id="Seg_7175" s="T26">очень</ta>
            <ta e="T28" id="Seg_7176" s="T27">шум-POSS</ta>
            <ta e="T29" id="Seg_7177" s="T28">NEG</ta>
            <ta e="T30" id="Seg_7178" s="T29">тихий</ta>
            <ta e="T31" id="Seg_7179" s="T30">Волочанка.[NOM]</ta>
            <ta e="T32" id="Seg_7180" s="T31">жизнь.[NOM]</ta>
            <ta e="T33" id="Seg_7181" s="T32">один</ta>
            <ta e="T34" id="Seg_7182" s="T33">миг.[NOM]</ta>
            <ta e="T35" id="Seg_7183" s="T34">оживать-EP-PTCP.PST.[NOM]</ta>
            <ta e="T36" id="Seg_7184" s="T35">подобно</ta>
            <ta e="T37" id="Seg_7185" s="T36">Бог.[NOM]</ta>
            <ta e="T38" id="Seg_7186" s="T37">день-3SG-ACC</ta>
            <ta e="T39" id="Seg_7187" s="T38">метка-VBZ-PTCP.PRS.[NOM]</ta>
            <ta e="T40" id="Seg_7188" s="T39">подобно</ta>
            <ta e="T41" id="Seg_7189" s="T40">быть-CVB.SEQ</ta>
            <ta e="T42" id="Seg_7190" s="T41">оставаться-PST2.[3SG]</ta>
            <ta e="T43" id="Seg_7191" s="T42">ребенок-PL.[NOM]</ta>
            <ta e="T44" id="Seg_7192" s="T43">однако</ta>
            <ta e="T45" id="Seg_7193" s="T44">человек-PL-ACC</ta>
            <ta e="T46" id="Seg_7194" s="T45">видеть-NEG-3PL</ta>
            <ta e="T47" id="Seg_7195" s="T46">русский</ta>
            <ta e="T48" id="Seg_7196" s="T47">дом-PL.[NOM]</ta>
            <ta e="T49" id="Seg_7197" s="T48">к</ta>
            <ta e="T50" id="Seg_7198" s="T49">только</ta>
            <ta e="T53" id="Seg_7199" s="T52">глаз-3PL.[NOM]</ta>
            <ta e="T54" id="Seg_7200" s="T53">улица.[NOM]</ta>
            <ta e="T55" id="Seg_7201" s="T54">вдоль</ta>
            <ta e="T56" id="Seg_7202" s="T55">идти.галопом-EP-CAUS-CVB.SEQ</ta>
            <ta e="T57" id="Seg_7203" s="T56">идти-CVB.SEQ-3PL</ta>
            <ta e="T58" id="Seg_7204" s="T57">верховой.олень-EP-INSTR</ta>
            <ta e="T59" id="Seg_7205" s="T58">дедушка-3PL.[NOM]</ta>
            <ta e="T60" id="Seg_7206" s="T59">Анисим</ta>
            <ta e="T61" id="Seg_7207" s="T60">старик.[NOM]</ta>
            <ta e="T62" id="Seg_7208" s="T61">стоять-PST2.[3SG]</ta>
            <ta e="T63" id="Seg_7209" s="T62">школа.[NOM]</ta>
            <ta e="T64" id="Seg_7210" s="T63">место.около-DAT/LOC</ta>
            <ta e="T65" id="Seg_7211" s="T64">3SG-ACC</ta>
            <ta e="T66" id="Seg_7212" s="T65">вокруг</ta>
            <ta e="T67" id="Seg_7213" s="T66">шум-PROPR</ta>
            <ta e="T68" id="Seg_7214" s="T67">армия.[NOM]</ta>
            <ta e="T69" id="Seg_7215" s="T68">верховой.олень-3PL-INSTR</ta>
            <ta e="T72" id="Seg_7216" s="T71">верхняя.часть-3PL-ABL</ta>
            <ta e="T73" id="Seg_7217" s="T72">прыгать-PST2-3PL</ta>
            <ta e="T74" id="Seg_7218" s="T73">школа.[NOM]</ta>
            <ta e="T75" id="Seg_7219" s="T74">место.около-DAT/LOC</ta>
            <ta e="T76" id="Seg_7220" s="T75">стоять-PRS-3PL</ta>
            <ta e="T79" id="Seg_7221" s="T78">учить-PTCP.PRS</ta>
            <ta e="T80" id="Seg_7222" s="T79">люди.[NOM]</ta>
            <ta e="T81" id="Seg_7223" s="T80">руководитель-3PL.[NOM]</ta>
            <ta e="T82" id="Seg_7224" s="T81">там</ta>
            <ta e="T83" id="Seg_7225" s="T82">интернат.[NOM]</ta>
            <ta e="T84" id="Seg_7226" s="T83">работник-PL-3SG.[NOM]</ta>
            <ta e="T85" id="Seg_7227" s="T84">каждый-3PL.[NOM]</ta>
            <ta e="T86" id="Seg_7228" s="T85">ребенок-PL-ACC</ta>
            <ta e="T87" id="Seg_7229" s="T86">поймать-CVB.SEQ</ta>
            <ta e="T88" id="Seg_7230" s="T87">взять-CVB.SEQ-3PL</ta>
            <ta e="T89" id="Seg_7231" s="T88">здравствуй-VBZ-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T90" id="Seg_7232" s="T89">что-ACC</ta>
            <ta e="T91" id="Seg_7233" s="T90">INDEF</ta>
            <ta e="T92" id="Seg_7234" s="T91">3PL-DAT/LOC</ta>
            <ta e="T93" id="Seg_7235" s="T92">говорить-PRS-3PL</ta>
            <ta e="T94" id="Seg_7236" s="T93">ребенок-PL.[NOM]</ta>
            <ta e="T95" id="Seg_7237" s="T94">русский</ta>
            <ta e="T96" id="Seg_7238" s="T95">язык-3SG-ACC</ta>
            <ta e="T97" id="Seg_7239" s="T96">понимать-NEG-3PL</ta>
            <ta e="T98" id="Seg_7240" s="T97">тот.[NOM]</ta>
            <ta e="T99" id="Seg_7241" s="T98">из_за</ta>
            <ta e="T100" id="Seg_7242" s="T99">приходить-CVB.SIM</ta>
            <ta e="T101" id="Seg_7243" s="T100">маленький</ta>
            <ta e="T102" id="Seg_7244" s="T101">люди.[NOM]</ta>
            <ta e="T103" id="Seg_7245" s="T102">разговаривать-NEG-3PL</ta>
            <ta e="T104" id="Seg_7246" s="T103">смотреть-PTCP.PRS</ta>
            <ta e="T105" id="Seg_7247" s="T104">только</ta>
            <ta e="T106" id="Seg_7248" s="T105">быть-PST2-3PL</ta>
            <ta e="T107" id="Seg_7249" s="T106">учитель-3PL-GEN</ta>
            <ta e="T108" id="Seg_7250" s="T107">лицо-3PL-ACC</ta>
            <ta e="T109" id="Seg_7251" s="T108">3PL.[NOM]</ta>
            <ta e="T110" id="Seg_7252" s="T109">очень</ta>
            <ta e="T111" id="Seg_7253" s="T110">интересный-ACC</ta>
            <ta e="T112" id="Seg_7254" s="T111">видеть-PST2-3PL</ta>
            <ta e="T113" id="Seg_7255" s="T112">очень</ta>
            <ta e="T114" id="Seg_7256" s="T113">большой</ta>
            <ta e="T115" id="Seg_7257" s="T114">русский</ta>
            <ta e="T116" id="Seg_7258" s="T115">дом-PL-ACC</ta>
            <ta e="T117" id="Seg_7259" s="T116">другой</ta>
            <ta e="T118" id="Seg_7260" s="T117">человек-PL-ACC</ta>
            <ta e="T119" id="Seg_7261" s="T118">русский-PL-ACC</ta>
            <ta e="T120" id="Seg_7262" s="T119">другой</ta>
            <ta e="T121" id="Seg_7263" s="T120">язык-PROPR-PL-ACC</ta>
            <ta e="T122" id="Seg_7264" s="T121">одежда-PROPR-PL-ACC</ta>
            <ta e="T123" id="Seg_7265" s="T122">нога-3PL-DAT/LOC</ta>
            <ta e="T124" id="Seg_7266" s="T123">однако</ta>
            <ta e="T125" id="Seg_7267" s="T124">олень.[NOM]</ta>
            <ta e="T126" id="Seg_7268" s="T125">копыто-3SG.[NOM]</ta>
            <ta e="T127" id="Seg_7269" s="T126">подобно</ta>
            <ta e="T128" id="Seg_7270" s="T127">обувь-ACC</ta>
            <ta e="T129" id="Seg_7271" s="T128">накидывать-EP-MED-PST2-3PL</ta>
            <ta e="T130" id="Seg_7272" s="T129">мужчина.[NOM]</ta>
            <ta e="T131" id="Seg_7273" s="T130">человек-PL.[NOM]</ta>
            <ta e="T132" id="Seg_7274" s="T131">голова-3PL-DAT/LOC</ta>
            <ta e="T133" id="Seg_7275" s="T132">мама-3PL-GEN</ta>
            <ta e="T134" id="Seg_7276" s="T133">ковш-3SG-ACC</ta>
            <ta e="T135" id="Seg_7277" s="T134">подобно</ta>
            <ta e="T136" id="Seg_7278" s="T135">шапка-ACC</ta>
            <ta e="T137" id="Seg_7279" s="T136">одеть-CVB.SIM</ta>
            <ta e="T138" id="Seg_7280" s="T137">идти-PRS-3PL</ta>
            <ta e="T139" id="Seg_7281" s="T138">жена-PL-3SG.[NOM]</ta>
            <ta e="T140" id="Seg_7282" s="T139">плетеный-POSS</ta>
            <ta e="T141" id="Seg_7283" s="T140">NEG-3PL</ta>
            <ta e="T142" id="Seg_7284" s="T141">волос-3PL.[NOM]</ta>
            <ta e="T143" id="Seg_7285" s="T142">резать-EP-PASS/REFL-EP-PTCP.PST-PL.[NOM]</ta>
            <ta e="T144" id="Seg_7286" s="T143">мужчина.[NOM]</ta>
            <ta e="T145" id="Seg_7287" s="T144">человек.[NOM]</ta>
            <ta e="T146" id="Seg_7288" s="T145">подобно</ta>
            <ta e="T147" id="Seg_7289" s="T146">платье-3PL.[NOM]</ta>
            <ta e="T148" id="Seg_7290" s="T147">нижний</ta>
            <ta e="T149" id="Seg_7291" s="T148">сторона-3SG-DAT/LOC</ta>
            <ta e="T150" id="Seg_7292" s="T149">очень</ta>
            <ta e="T151" id="Seg_7293" s="T150">зауженный.[NOM]</ta>
            <ta e="T152" id="Seg_7294" s="T151">тот.[NOM]</ta>
            <ta e="T153" id="Seg_7295" s="T152">из_за</ta>
            <ta e="T154" id="Seg_7296" s="T153">3PL.[NOM]</ta>
            <ta e="T155" id="Seg_7297" s="T154">еле</ta>
            <ta e="T156" id="Seg_7298" s="T155">идти-PRS-3PL</ta>
            <ta e="T157" id="Seg_7299" s="T156">Тойоо.[NOM]</ta>
            <ta e="T158" id="Seg_7300" s="T157">очень</ta>
            <ta e="T159" id="Seg_7301" s="T158">смотреть-CVB.SIM</ta>
            <ta e="T160" id="Seg_7302" s="T159">стоять-PST2.[3SG]</ta>
            <ta e="T161" id="Seg_7303" s="T160">жена-PL-ACC</ta>
            <ta e="T162" id="Seg_7304" s="T161">нутро-3SG-DAT/LOC</ta>
            <ta e="T163" id="Seg_7305" s="T162">думать-PRS.[3SG]</ta>
            <ta e="T164" id="Seg_7306" s="T163">видно</ta>
            <ta e="T165" id="Seg_7307" s="T164">такой</ta>
            <ta e="T166" id="Seg_7308" s="T165">платье-INSTR</ta>
            <ta e="T167" id="Seg_7309" s="T166">бегать-CVB.SIM</ta>
            <ta e="T168" id="Seg_7310" s="T167">не.справиться-NEG-3PL</ta>
            <ta e="T169" id="Seg_7311" s="T168">мало</ta>
            <ta e="T170" id="Seg_7312" s="T169">только</ta>
            <ta e="T171" id="Seg_7313" s="T170">идти-PRS-3PL</ta>
            <ta e="T172" id="Seg_7314" s="T171">быть-PST2.[3SG]</ta>
            <ta e="T173" id="Seg_7315" s="T172">друг-PL-3SG-DAT/LOC</ta>
            <ta e="T174" id="Seg_7316" s="T173">что-ACC</ta>
            <ta e="T175" id="Seg_7317" s="T174">INDEF</ta>
            <ta e="T176" id="Seg_7318" s="T175">тот.[NOM]</ta>
            <ta e="T177" id="Seg_7319" s="T176">сторона-3SG-INSTR</ta>
            <ta e="T178" id="Seg_7320" s="T177">рассказывать-CVB.PURP</ta>
            <ta e="T179" id="Seg_7321" s="T178">хотеть-PST2.[3SG]</ta>
            <ta e="T180" id="Seg_7322" s="T179">рот-3SG-ACC</ta>
            <ta e="T181" id="Seg_7323" s="T180">только</ta>
            <ta e="T182" id="Seg_7324" s="T181">открывать-PST2.[3SG]</ta>
            <ta e="T183" id="Seg_7325" s="T182">что.[NOM]</ta>
            <ta e="T184" id="Seg_7326" s="T183">INDEF</ta>
            <ta e="T185" id="Seg_7327" s="T184">шум-3SG.[NOM]</ta>
            <ta e="T186" id="Seg_7328" s="T185">становиться-PST2.[3SG]</ta>
            <ta e="T187" id="Seg_7329" s="T186">видеть-PST2.[3SG]</ta>
            <ta e="T188" id="Seg_7330" s="T187">тот.[NOM]</ta>
            <ta e="T189" id="Seg_7331" s="T188">к</ta>
            <ta e="T190" id="Seg_7332" s="T189">откуда</ta>
            <ta e="T191" id="Seg_7333" s="T190">шум.[NOM]</ta>
            <ta e="T192" id="Seg_7334" s="T191">выйти-EP-PST2.[3SG]</ta>
            <ta e="T195" id="Seg_7335" s="T194">что.[NOM]</ta>
            <ta e="T196" id="Seg_7336" s="T195">INDEF</ta>
            <ta e="T197" id="Seg_7337" s="T196">человек.[NOM]</ta>
            <ta e="T198" id="Seg_7338" s="T197">видеть-NEG.PTCP.PST</ta>
            <ta e="T199" id="Seg_7339" s="T198">сани-PROPR-3SG.[NOM]</ta>
            <ta e="T200" id="Seg_7340" s="T199">выбирать-MED-EP-CAUS-CVB.SEQ</ta>
            <ta e="T201" id="Seg_7341" s="T200">идти-PST2.[3SG]</ta>
            <ta e="T202" id="Seg_7342" s="T201">сани-3SG.[NOM]</ta>
            <ta e="T203" id="Seg_7343" s="T202">диковенный.[NOM]</ta>
            <ta e="T204" id="Seg_7344" s="T203">олень-3SG.[NOM]</ta>
            <ta e="T205" id="Seg_7345" s="T204">однако</ta>
            <ta e="T206" id="Seg_7346" s="T205">большой-3SG.[NOM]</ta>
            <ta e="T207" id="Seg_7347" s="T206">очень</ta>
            <ta e="T208" id="Seg_7348" s="T207">очень</ta>
            <ta e="T209" id="Seg_7349" s="T208">длинный</ta>
            <ta e="T210" id="Seg_7350" s="T209">махнуть-PTCP.PRS</ta>
            <ta e="T211" id="Seg_7351" s="T210">подобно</ta>
            <ta e="T212" id="Seg_7352" s="T211">хвост-PROPR.[NOM]</ta>
            <ta e="T213" id="Seg_7353" s="T212">сани-VBZ-PTCP.PRS</ta>
            <ta e="T214" id="Seg_7354" s="T213">человек-3SG.[NOM]</ta>
            <ta e="T215" id="Seg_7355" s="T214">однако</ta>
            <ta e="T216" id="Seg_7356" s="T215">лицо-3SG.[NOM]</ta>
            <ta e="T217" id="Seg_7357" s="T216">шерсть-3SG.[NOM]</ta>
            <ta e="T218" id="Seg_7358" s="T217">очень</ta>
            <ta e="T219" id="Seg_7359" s="T218">мохнатый</ta>
            <ta e="T220" id="Seg_7360" s="T219">борода-PROPR.[NOM]</ta>
            <ta e="T221" id="Seg_7361" s="T220">олень-3SG-GEN</ta>
            <ta e="T222" id="Seg_7362" s="T221">задняя.часть-3SG-ABL</ta>
            <ta e="T223" id="Seg_7363" s="T222">пешком</ta>
            <ta e="T224" id="Seg_7364" s="T223">идти-CVB.SEQ</ta>
            <ta e="T225" id="Seg_7365" s="T224">идти-CVB.SEQ</ta>
            <ta e="T226" id="Seg_7366" s="T225">держать-PST2.[3SG]</ta>
            <ta e="T227" id="Seg_7367" s="T226">два</ta>
            <ta e="T228" id="Seg_7368" s="T227">повод-ACC</ta>
            <ta e="T229" id="Seg_7369" s="T228">олень-3SG.[NOM]</ta>
            <ta e="T230" id="Seg_7370" s="T229">как</ta>
            <ta e="T231" id="Seg_7371" s="T230">INDEF</ta>
            <ta e="T232" id="Seg_7372" s="T231">шагать-PTCP.PRS-3SG-ACC</ta>
            <ta e="T233" id="Seg_7373" s="T232">каждый</ta>
            <ta e="T234" id="Seg_7374" s="T233">голова-3SG-ACC</ta>
            <ta e="T235" id="Seg_7375" s="T234">двигаться-CAUS-PRS.[3SG]</ta>
            <ta e="T236" id="Seg_7376" s="T235">тот.[NOM]</ta>
            <ta e="T237" id="Seg_7377" s="T236">каждый</ta>
            <ta e="T238" id="Seg_7378" s="T237">сани-AG.[NOM]</ta>
            <ta e="T239" id="Seg_7379" s="T238">ноо</ta>
            <ta e="T240" id="Seg_7380" s="T239">ноо</ta>
            <ta e="T241" id="Seg_7381" s="T240">говорить-PRS.[3SG]</ta>
            <ta e="T242" id="Seg_7382" s="T241">сказка-ACC</ta>
            <ta e="T243" id="Seg_7383" s="T242">слушать-PTCP.PRS.[NOM]</ta>
            <ta e="T244" id="Seg_7384" s="T243">подобно</ta>
            <ta e="T245" id="Seg_7385" s="T244">сильный</ta>
            <ta e="T246" id="Seg_7386" s="T245">очень-ADVZ</ta>
            <ta e="T247" id="Seg_7387" s="T246">рот-3SG-INSTR</ta>
            <ta e="T248" id="Seg_7388" s="T247">пробовать-PST2.[3SG]</ta>
            <ta e="T249" id="Seg_7389" s="T248">сладкий</ta>
            <ta e="T250" id="Seg_7390" s="T249">сало-ACC</ta>
            <ta e="T251" id="Seg_7391" s="T250">есть-PTCP.PRS.[NOM]</ta>
            <ta e="T252" id="Seg_7392" s="T251">подобно</ta>
            <ta e="T253" id="Seg_7393" s="T252">эй</ta>
            <ta e="T254" id="Seg_7394" s="T253">тот.[NOM]</ta>
            <ta e="T255" id="Seg_7395" s="T254">что.[NOM]</ta>
            <ta e="T256" id="Seg_7396" s="T255">олень-3SG.[NOM]</ta>
            <ta e="T257" id="Seg_7397" s="T256">быть-PRS.[3SG]</ta>
            <ta e="T258" id="Seg_7398" s="T257">видеть-CVB.SIM</ta>
            <ta e="T259" id="Seg_7399" s="T258">стоять-CVB.SEQ</ta>
            <ta e="T260" id="Seg_7400" s="T259">радоваться-CVB.COND</ta>
            <ta e="T261" id="Seg_7401" s="T260">Тойоо.[NOM]</ta>
            <ta e="T262" id="Seg_7402" s="T261">голос.[NOM]</ta>
            <ta e="T263" id="Seg_7403" s="T262">вступать-EP-PST2.[3SG]</ta>
            <ta e="T264" id="Seg_7404" s="T263">3SG.[NOM]</ta>
            <ta e="T265" id="Seg_7405" s="T264">только</ta>
            <ta e="T266" id="Seg_7406" s="T265">тот-ACC</ta>
            <ta e="T267" id="Seg_7407" s="T266">видеть-CVB.SEQ</ta>
            <ta e="T268" id="Seg_7408" s="T267">стоять-PST2.NEG-3SG</ta>
            <ta e="T269" id="Seg_7409" s="T268">каждый</ta>
            <ta e="T270" id="Seg_7410" s="T269">ребенок-PL.[NOM]</ta>
            <ta e="T271" id="Seg_7411" s="T270">чудо-ADJZ.[NOM]</ta>
            <ta e="T272" id="Seg_7412" s="T271">становиться-PST2-3PL</ta>
            <ta e="T273" id="Seg_7413" s="T272">сам.[NOM]</ta>
            <ta e="T274" id="Seg_7414" s="T273">сам-3PL-ACC</ta>
            <ta e="T275" id="Seg_7415" s="T274">с</ta>
            <ta e="T276" id="Seg_7416" s="T275">разговаривать-CVB.SIM</ta>
            <ta e="T277" id="Seg_7417" s="T276">еще.не-3PL</ta>
            <ta e="T278" id="Seg_7418" s="T277">опять</ta>
            <ta e="T279" id="Seg_7419" s="T278">видеть-PST2-3PL</ta>
            <ta e="T280" id="Seg_7420" s="T279">новый</ta>
            <ta e="T281" id="Seg_7421" s="T280">чудо-ACC</ta>
            <ta e="T282" id="Seg_7422" s="T281">старый</ta>
            <ta e="T283" id="Seg_7423" s="T282">русский</ta>
            <ta e="T284" id="Seg_7424" s="T283">старуха-3SG.[NOM]</ta>
            <ta e="T285" id="Seg_7425" s="T284">что.[NOM]</ta>
            <ta e="T286" id="Seg_7426" s="T285">INDEF</ta>
            <ta e="T287" id="Seg_7427" s="T286">маленький</ta>
            <ta e="T288" id="Seg_7428" s="T287">олень-DIM-PL-3SG-ACC</ta>
            <ta e="T289" id="Seg_7429" s="T288">гнать-CVB.SEQ</ta>
            <ta e="T290" id="Seg_7430" s="T289">идти-PST2.[3SG]</ta>
            <ta e="T291" id="Seg_7431" s="T290">голый</ta>
            <ta e="T292" id="Seg_7432" s="T291">безрогий</ta>
            <ta e="T293" id="Seg_7433" s="T292">олень-DIM-PL.[NOM]</ta>
            <ta e="T294" id="Seg_7434" s="T293">низкий</ta>
            <ta e="T295" id="Seg_7435" s="T294">нога-3PL-INSTR</ta>
            <ta e="T296" id="Seg_7436" s="T295">бегать-FREQ-CVB.SEQ</ta>
            <ta e="T297" id="Seg_7437" s="T296">идти-PST2-3PL</ta>
            <ta e="T298" id="Seg_7438" s="T297">кричать.громко-CVB.SIM-кричать.громко-CVB.SIM</ta>
            <ta e="T299" id="Seg_7439" s="T298">тот-3SG-3PL.[NOM]</ta>
            <ta e="T300" id="Seg_7440" s="T299">бежать-CVB.SEQ</ta>
            <ta e="T301" id="Seg_7441" s="T300">идти-CVB.SEQ-3PL</ta>
            <ta e="T302" id="Seg_7442" s="T301">тот-этот</ta>
            <ta e="T303" id="Seg_7443" s="T302">земля.[NOM]</ta>
            <ta e="T304" id="Seg_7444" s="T303">к</ta>
            <ta e="T305" id="Seg_7445" s="T304">разбегаться-PRS-3PL</ta>
            <ta e="T306" id="Seg_7446" s="T305">фуу</ta>
            <ta e="T307" id="Seg_7447" s="T306">говорить-CVB.SEQ</ta>
            <ta e="T308" id="Seg_7448" s="T307">кричать-PST2.[3SG]</ta>
            <ta e="T309" id="Seg_7449" s="T308">Тойоо.[NOM]</ta>
            <ta e="T310" id="Seg_7450" s="T309">голый</ta>
            <ta e="T311" id="Seg_7451" s="T310">дикий.олень-PL.[NOM]</ta>
            <ta e="T312" id="Seg_7452" s="T311">ребенок-PL.[NOM]</ta>
            <ta e="T313" id="Seg_7453" s="T312">последний</ta>
            <ta e="T314" id="Seg_7454" s="T313">живот-3PL-INSTR</ta>
            <ta e="T315" id="Seg_7455" s="T314">смеяться-RECP/COLL-PRS-3PL</ta>
            <ta e="T316" id="Seg_7456" s="T315">палец-3PL-INSTR</ta>
            <ta e="T317" id="Seg_7457" s="T316">видеть-CAUS-CVB.SIM-видеть-CAUS-CVB.SIM</ta>
            <ta e="T318" id="Seg_7458" s="T317">видеть.[IMP.2SG]</ta>
            <ta e="T319" id="Seg_7459" s="T318">друг</ta>
            <ta e="T320" id="Seg_7460" s="T319">нос-3PL.[NOM]</ta>
            <ta e="T321" id="Seg_7461" s="T320">хорей.[NOM]</ta>
            <ta e="T322" id="Seg_7462" s="T321">кольцо.на.хорее-3SG-ACC</ta>
            <ta e="T323" id="Seg_7463" s="T322">подобно</ta>
            <ta e="T324" id="Seg_7464" s="T323">тот.[NOM]</ta>
            <ta e="T325" id="Seg_7465" s="T324">только</ta>
            <ta e="T326" id="Seg_7466" s="T325">два</ta>
            <ta e="T327" id="Seg_7467" s="T326">только</ta>
            <ta e="T328" id="Seg_7468" s="T327">дыра-PROPR-3PL</ta>
            <ta e="T329" id="Seg_7469" s="T328">олень.[NOM]</ta>
            <ta e="T330" id="Seg_7470" s="T329">такой</ta>
            <ta e="T331" id="Seg_7471" s="T330">быть-PST2.NEG.[3SG]</ta>
            <ta e="T332" id="Seg_7472" s="T331">наши.[NOM]</ta>
            <ta e="T333" id="Seg_7473" s="T332">красивый-PL.[NOM]</ta>
            <ta e="T334" id="Seg_7474" s="T333">сначала</ta>
            <ta e="T335" id="Seg_7475" s="T334">бояться-CVB.SIM</ta>
            <ta e="T336" id="Seg_7476" s="T335">бить-PST2-3PL</ta>
            <ta e="T337" id="Seg_7477" s="T336">потом</ta>
            <ta e="T338" id="Seg_7478" s="T337">приближаться-PST2-3PL</ta>
            <ta e="T339" id="Seg_7479" s="T338">добрый-ADVZ</ta>
            <ta e="T340" id="Seg_7480" s="T339">видеть-CVB.PURP</ta>
            <ta e="T341" id="Seg_7481" s="T340">Тойоо.[NOM]</ta>
            <ta e="T342" id="Seg_7482" s="T341">сам-3SG-GEN</ta>
            <ta e="T343" id="Seg_7483" s="T342">прыть-3SG-ACC</ta>
            <ta e="T344" id="Seg_7484" s="T343">побеждать-PST2.NEG.[3SG]</ta>
            <ta e="T345" id="Seg_7485" s="T344">один-3PL-DAT/LOC</ta>
            <ta e="T346" id="Seg_7486" s="T345">бегать-CVB.SEQ</ta>
            <ta e="T347" id="Seg_7487" s="T346">приходить-CVB.SEQ</ta>
            <ta e="T348" id="Seg_7488" s="T347">верхняя.часть-3SG-DAT/LOC</ta>
            <ta e="T349" id="Seg_7489" s="T348">верховой.олень-VBZ-CVB.SIM</ta>
            <ta e="T350" id="Seg_7490" s="T349">делать-CVB.SIM</ta>
            <ta e="T351" id="Seg_7491" s="T350">прыгать-PST2.[3SG]</ta>
            <ta e="T352" id="Seg_7492" s="T351">дикий.олень-3SG.[NOM]</ta>
            <ta e="T353" id="Seg_7493" s="T352">спина-3SG-DAT/LOC</ta>
            <ta e="T354" id="Seg_7494" s="T353">человек-ACC</ta>
            <ta e="T355" id="Seg_7495" s="T354">замечать-CVB.SEQ</ta>
            <ta e="T356" id="Seg_7496" s="T355">стоять-PTCP.PST</ta>
            <ta e="T357" id="Seg_7497" s="T356">земля-3SG-ABL</ta>
            <ta e="T358" id="Seg_7498" s="T357">прыгать-PST2.[3SG]</ta>
            <ta e="T359" id="Seg_7499" s="T358">бояться-CVB.COND</ta>
            <ta e="T360" id="Seg_7500" s="T359">последний</ta>
            <ta e="T361" id="Seg_7501" s="T360">сила-3SG-INSTR</ta>
            <ta e="T362" id="Seg_7502" s="T361">бежать-PST2.[3SG]</ta>
            <ta e="T363" id="Seg_7503" s="T362">голова-3SG-ACC</ta>
            <ta e="T364" id="Seg_7504" s="T363">в.сторону</ta>
            <ta e="T365" id="Seg_7505" s="T364">верховой.олень-ABL</ta>
            <ta e="T366" id="Seg_7506" s="T365">падать-NEG.PTCP</ta>
            <ta e="T367" id="Seg_7507" s="T366">ребенок.[NOM]</ta>
            <ta e="T368" id="Seg_7508" s="T367">поймать-CVB.SIM</ta>
            <ta e="T369" id="Seg_7509" s="T368">пытаться-PST2.[3SG]</ta>
            <ta e="T370" id="Seg_7510" s="T369">олень-3SG-GEN</ta>
            <ta e="T371" id="Seg_7511" s="T370">шерсть-3SG-ABL</ta>
            <ta e="T372" id="Seg_7512" s="T371">как</ta>
            <ta e="T373" id="Seg_7513" s="T372">NEG</ta>
            <ta e="T374" id="Seg_7514" s="T373">где</ta>
            <ta e="T375" id="Seg_7515" s="T374">NEG</ta>
            <ta e="T376" id="Seg_7516" s="T375">рука-PL-3SG.[NOM]</ta>
            <ta e="T377" id="Seg_7517" s="T376">зацепляться-EP-NEG.[3SG]</ta>
            <ta e="T378" id="Seg_7518" s="T377">олень-3SG-GEN</ta>
            <ta e="T379" id="Seg_7519" s="T378">шерсть-PL-3SG.[NOM]</ta>
            <ta e="T380" id="Seg_7520" s="T379">маленький.гвоздик.[NOM]</ta>
            <ta e="T381" id="Seg_7521" s="T380">как-PL</ta>
            <ta e="T382" id="Seg_7522" s="T381">верховой.олень-3SG.[NOM]</ta>
            <ta e="T383" id="Seg_7523" s="T382">последний</ta>
            <ta e="T384" id="Seg_7524" s="T383">сила-3SG-INSTR</ta>
            <ta e="T385" id="Seg_7525" s="T384">кричать-PST2.[3SG]</ta>
            <ta e="T386" id="Seg_7526" s="T385">голова-3SG-ACC</ta>
            <ta e="T387" id="Seg_7527" s="T386">в.сторону</ta>
            <ta e="T388" id="Seg_7528" s="T387">бегать-CVB.SEQ</ta>
            <ta e="T389" id="Seg_7529" s="T388">идти-PST2.[3SG]</ta>
            <ta e="T390" id="Seg_7530" s="T389">некоторый</ta>
            <ta e="T391" id="Seg_7531" s="T390">свинья-PL.[NOM]</ta>
            <ta e="T392" id="Seg_7532" s="T391">тот-ACC</ta>
            <ta e="T393" id="Seg_7533" s="T392">видеть-CVB.SEQ-3PL</ta>
            <ta e="T394" id="Seg_7534" s="T393">один</ta>
            <ta e="T395" id="Seg_7535" s="T394">миг.[NOM]</ta>
            <ta e="T396" id="Seg_7536" s="T395">бежать-PST2-3PL</ta>
            <ta e="T397" id="Seg_7537" s="T396">задняя.часть-3PL-ABL</ta>
            <ta e="T398" id="Seg_7538" s="T397">старый</ta>
            <ta e="T399" id="Seg_7539" s="T398">старуха.[NOM]</ta>
            <ta e="T400" id="Seg_7540" s="T399">бегать-CVB.SIM</ta>
            <ta e="T401" id="Seg_7541" s="T400">пытаться-PST2.[3SG]</ta>
            <ta e="T402" id="Seg_7542" s="T401">Тойоо.[NOM]</ta>
            <ta e="T403" id="Seg_7543" s="T402">что-ACC</ta>
            <ta e="T404" id="Seg_7544" s="T403">только</ta>
            <ta e="T405" id="Seg_7545" s="T404">кричать-CVB.SIM-кричать-CVB.SIM</ta>
            <ta e="T406" id="Seg_7546" s="T405">бегать-PTCP.PRS-3SG-ACC</ta>
            <ta e="T407" id="Seg_7547" s="T406">каждый</ta>
            <ta e="T408" id="Seg_7548" s="T407">Тойоо.[NOM]</ta>
            <ta e="T409" id="Seg_7549" s="T408">к</ta>
            <ta e="T410" id="Seg_7550" s="T409">палка-3SG-INSTR</ta>
            <ta e="T411" id="Seg_7551" s="T410">взмахивать-PRS.[3SG]</ta>
            <ta e="T412" id="Seg_7552" s="T411">голый</ta>
            <ta e="T413" id="Seg_7553" s="T412">верховой.олень-3SG.[NOM]</ta>
            <ta e="T414" id="Seg_7554" s="T413">грязный</ta>
            <ta e="T415" id="Seg_7555" s="T414">грязь-PROPR</ta>
            <ta e="T416" id="Seg_7556" s="T415">вода-PROPR</ta>
            <ta e="T417" id="Seg_7557" s="T416">яма-DAT/LOC</ta>
            <ta e="T418" id="Seg_7558" s="T417">всплеск</ta>
            <ta e="T419" id="Seg_7559" s="T418">делать-CVB.SIM</ta>
            <ta e="T420" id="Seg_7560" s="T419">падать-PST2.[3SG]</ta>
            <ta e="T421" id="Seg_7561" s="T420">Тойоо.[NOM]</ta>
            <ta e="T422" id="Seg_7562" s="T421">свинья-3SG-ACC</ta>
            <ta e="T423" id="Seg_7563" s="T422">с</ta>
            <ta e="T424" id="Seg_7564" s="T423">вставать-CVB.SIM</ta>
            <ta e="T425" id="Seg_7565" s="T424">делать.напрасно-CVB.SEQ</ta>
            <ta e="T426" id="Seg_7566" s="T425">лежать-PST2.[3SG]</ta>
            <ta e="T427" id="Seg_7567" s="T426">лицо-3SG.[NOM]</ta>
            <ta e="T428" id="Seg_7568" s="T427">NEG</ta>
            <ta e="T429" id="Seg_7569" s="T428">что-3SG.[NOM]</ta>
            <ta e="T430" id="Seg_7570" s="T429">NEG</ta>
            <ta e="T431" id="Seg_7571" s="T430">быть.видно-EP-NEG.[3SG]</ta>
            <ta e="T432" id="Seg_7572" s="T431">глина.[NOM]</ta>
            <ta e="T433" id="Seg_7573" s="T432">становиться-PST2.[3SG]</ta>
            <ta e="T434" id="Seg_7574" s="T433">вставать-CVB.SIM</ta>
            <ta e="T435" id="Seg_7575" s="T434">пытаться-CVB.SEQ</ta>
            <ta e="T436" id="Seg_7576" s="T435">последний-3SG.[NOM]</ta>
            <ta e="T437" id="Seg_7577" s="T436">быть-CVB.SEQ</ta>
            <ta e="T438" id="Seg_7578" s="T437">старуха.[NOM]</ta>
            <ta e="T439" id="Seg_7579" s="T438">достигать-PST2.[3SG]</ta>
            <ta e="T440" id="Seg_7580" s="T439">3SG-ACC</ta>
            <ta e="T441" id="Seg_7581" s="T440">Тойоо.[NOM]</ta>
            <ta e="T442" id="Seg_7582" s="T441">лицо-3SG-ACC</ta>
            <ta e="T443" id="Seg_7583" s="T442">видеть-CVB.SEQ</ta>
            <ta e="T444" id="Seg_7584" s="T443">рассмеяться-PST2.[3SG]</ta>
            <ta e="T445" id="Seg_7585" s="T444">рука-3SG-INSTR</ta>
            <ta e="T446" id="Seg_7586" s="T445">только</ta>
            <ta e="T447" id="Seg_7587" s="T446">махнуть-PST2.[3SG]</ta>
            <ta e="T448" id="Seg_7588" s="T447">проваливать.[IMP.2SG]</ta>
            <ta e="T449" id="Seg_7589" s="T448">откуда</ta>
            <ta e="T450" id="Seg_7590" s="T449">приходить-PST2-EP-2SG</ta>
            <ta e="T451" id="Seg_7591" s="T450">говорить-PTCP.PST.[NOM]</ta>
            <ta e="T452" id="Seg_7592" s="T451">подобно</ta>
            <ta e="T453" id="Seg_7593" s="T452">Тойоо.[NOM]</ta>
            <ta e="T454" id="Seg_7594" s="T453">вставать-CVB.SIM</ta>
            <ta e="T455" id="Seg_7595" s="T454">прыгать-CVB.ANT</ta>
            <ta e="T456" id="Seg_7596" s="T455">последний</ta>
            <ta e="T457" id="Seg_7597" s="T456">сила-3SG-INSTR</ta>
            <ta e="T458" id="Seg_7598" s="T457">дыхание-POSS</ta>
            <ta e="T459" id="Seg_7599" s="T458">NEG</ta>
            <ta e="T460" id="Seg_7600" s="T459">друг-PL-3SG-ACC</ta>
            <ta e="T461" id="Seg_7601" s="T460">к</ta>
            <ta e="T462" id="Seg_7602" s="T461">пнуть-PST2.[3SG]</ta>
            <ta e="T463" id="Seg_7603" s="T462">вот.беда</ta>
            <ta e="T464" id="Seg_7604" s="T463">еле</ta>
            <ta e="T465" id="Seg_7605" s="T464">умирать-CVB.SIM</ta>
            <ta e="T466" id="Seg_7606" s="T465">бить-PST1-1SG</ta>
            <ta e="T467" id="Seg_7607" s="T466">еле</ta>
            <ta e="T468" id="Seg_7608" s="T467">умирать-CVB.SIM</ta>
            <ta e="T469" id="Seg_7609" s="T468">бить-PST1-1SG</ta>
            <ta e="T470" id="Seg_7610" s="T469">учитель-3PL.[NOM]</ta>
            <ta e="T471" id="Seg_7611" s="T470">директор.[NOM]</ta>
            <ta e="T472" id="Seg_7612" s="T471">быть-CVB.SEQ</ta>
            <ta e="T473" id="Seg_7613" s="T472">что.[NOM]</ta>
            <ta e="T474" id="Seg_7614" s="T473">NEG</ta>
            <ta e="T475" id="Seg_7615" s="T474">говорить-PTCP.FUT-3PL-ACC</ta>
            <ta e="T476" id="Seg_7616" s="T475">сила-3SG-ABL</ta>
            <ta e="T477" id="Seg_7617" s="T476">последний</ta>
            <ta e="T478" id="Seg_7618" s="T477">душа-3PL-INSTR</ta>
            <ta e="T479" id="Seg_7619" s="T478">смеяться-CVB.SIM</ta>
            <ta e="T480" id="Seg_7620" s="T479">стоять-PST2-3PL</ta>
            <ta e="T482" id="Seg_7621" s="T481">последний</ta>
            <ta e="T483" id="Seg_7622" s="T482">мысль-3SG-ACC</ta>
            <ta e="T484" id="Seg_7623" s="T483">говорить-PST2.[3SG]</ta>
            <ta e="T485" id="Seg_7624" s="T484">как</ta>
            <ta e="T486" id="Seg_7625" s="T485">NEG</ta>
            <ta e="T487" id="Seg_7626" s="T486">говорить-PTCP.FUT-DAT/LOC</ta>
            <ta e="T488" id="Seg_7627" s="T487">очень</ta>
            <ta e="T489" id="Seg_7628" s="T488">очень</ta>
            <ta e="T490" id="Seg_7629" s="T489">шустрый</ta>
            <ta e="T491" id="Seg_7630" s="T490">люди.[NOM]</ta>
            <ta e="T492" id="Seg_7631" s="T491">приходить-PST2-3PL</ta>
            <ta e="T494" id="Seg_7632" s="T492">наверное</ta>
            <ta e="T495" id="Seg_7633" s="T494">коллега-PL-3SG-DAT/LOC</ta>
            <ta e="T496" id="Seg_7634" s="T495">говорить-PST2.[3SG]</ta>
            <ta e="T497" id="Seg_7635" s="T496">теперь</ta>
            <ta e="T498" id="Seg_7636" s="T497">3PL-ACC</ta>
            <ta e="T499" id="Seg_7637" s="T498">интернат-DAT/LOC</ta>
            <ta e="T500" id="Seg_7638" s="T499">уносить-EP-IMP.2PL</ta>
            <ta e="T501" id="Seg_7639" s="T500">потом</ta>
            <ta e="T502" id="Seg_7640" s="T501">мыться-CAUS-EP-IMP.2PL</ta>
            <ta e="T503" id="Seg_7641" s="T502">пища-3PL-ACC</ta>
            <ta e="T504" id="Seg_7642" s="T503">готовить-PTCP.FUT-3PL-DAT/LOC</ta>
            <ta e="T505" id="Seg_7643" s="T504">пока</ta>
            <ta e="T506" id="Seg_7644" s="T505">целый-3SG-ACC</ta>
            <ta e="T507" id="Seg_7645" s="T506">тщательный-ADVZ</ta>
            <ta e="T508" id="Seg_7646" s="T507">делать-EP-IMP.2PL</ta>
            <ta e="T509" id="Seg_7647" s="T508">ребенок-PL-ACC</ta>
            <ta e="T510" id="Seg_7648" s="T509">пугать.[CAUS]-NEG.CVB.SIM</ta>
            <ta e="T511" id="Seg_7649" s="T510">ребенок-PL-ACC</ta>
            <ta e="T512" id="Seg_7650" s="T511">сразу</ta>
            <ta e="T513" id="Seg_7651" s="T512">гнать-CVB.SEQ</ta>
            <ta e="T514" id="Seg_7652" s="T513">приносить-PST2-3PL</ta>
            <ta e="T515" id="Seg_7653" s="T514">показывать-PST2-3PL</ta>
            <ta e="T516" id="Seg_7654" s="T515">каждый-3PL-DAT/LOC</ta>
            <ta e="T517" id="Seg_7655" s="T516">жить-PTCP.PRS</ta>
            <ta e="T518" id="Seg_7656" s="T517">дом-3PL-ACC</ta>
            <ta e="T519" id="Seg_7657" s="T518">русский-SIM</ta>
            <ta e="T521" id="Seg_7658" s="T520">имя-ADJZ.[NOM]</ta>
            <ta e="T522" id="Seg_7659" s="T521">залатать-PTCP.PRS</ta>
            <ta e="T523" id="Seg_7660" s="T522">новый</ta>
            <ta e="T524" id="Seg_7661" s="T523">рубашка-PL-ACC</ta>
            <ta e="T525" id="Seg_7662" s="T524">обувь-PL-ACC</ta>
            <ta e="T526" id="Seg_7663" s="T525">давать-FREQ-PST2-3PL</ta>
            <ta e="T527" id="Seg_7664" s="T526">каждый</ta>
            <ta e="T528" id="Seg_7665" s="T527">приходить-PTCP.PST</ta>
            <ta e="T529" id="Seg_7666" s="T528">люди.[NOM]</ta>
            <ta e="T530" id="Seg_7667" s="T529">волос-PL-3SG-ACC</ta>
            <ta e="T531" id="Seg_7668" s="T530">резать-PST2-3PL</ta>
            <ta e="T532" id="Seg_7669" s="T531">что-ABL</ta>
            <ta e="T533" id="Seg_7670" s="T532">девушка.[NOM]</ta>
            <ta e="T534" id="Seg_7671" s="T533">ребенок-PL.[NOM]</ta>
            <ta e="T535" id="Seg_7672" s="T534">плакать-PTCP.PRS-3PL.[NOM]</ta>
            <ta e="T536" id="Seg_7673" s="T535">очень</ta>
            <ta e="T537" id="Seg_7674" s="T536">быть-PST2.[3SG]</ta>
            <ta e="T538" id="Seg_7675" s="T537">волос-3PL-ACC</ta>
            <ta e="T539" id="Seg_7676" s="T538">жалеть-CVB.SEQ-3PL</ta>
            <ta e="T540" id="Seg_7677" s="T539">Тойоо-ACC</ta>
            <ta e="T541" id="Seg_7678" s="T540">тоже</ta>
            <ta e="T542" id="Seg_7679" s="T541">резать-PST2-3PL</ta>
            <ta e="T543" id="Seg_7680" s="T542">одеваться-PTCP.PRS</ta>
            <ta e="T544" id="Seg_7681" s="T543">одежда.[NOM]</ta>
            <ta e="T545" id="Seg_7682" s="T544">давать-PST2-3PL</ta>
            <ta e="T546" id="Seg_7683" s="T545">одеть-PTCP.PRS</ta>
            <ta e="T547" id="Seg_7684" s="T546">копыто-3PL-ACC</ta>
            <ta e="T548" id="Seg_7685" s="T547">подать-PST2-3PL</ta>
            <ta e="T549" id="Seg_7686" s="T548">кровать-3SG-DAT/LOC</ta>
            <ta e="T550" id="Seg_7687" s="T549">приходить-CVB.ANT</ta>
            <ta e="T551" id="Seg_7688" s="T550">сесть-PST2.[3SG]</ta>
            <ta e="T552" id="Seg_7689" s="T551">заглянуть-CVB.SIM</ta>
            <ta e="T553" id="Seg_7690" s="T552">что-ACC</ta>
            <ta e="T554" id="Seg_7691" s="T553">подать-PTCP.PST-3PL-ACC</ta>
            <ta e="T555" id="Seg_7692" s="T554">каждый-3SG.[NOM]</ta>
            <ta e="T556" id="Seg_7693" s="T555">3SG-DAT/LOC</ta>
            <ta e="T557" id="Seg_7694" s="T556">ладно</ta>
            <ta e="T558" id="Seg_7695" s="T557">тот</ta>
            <ta e="T559" id="Seg_7696" s="T558">да</ta>
            <ta e="T560" id="Seg_7697" s="T559">быть-COND.[3SG]</ta>
            <ta e="T561" id="Seg_7698" s="T560">почему</ta>
            <ta e="T562" id="Seg_7699" s="T561">3PL.[NOM]</ta>
            <ta e="T563" id="Seg_7700" s="T562">давать-PST2-3PL</ta>
            <ta e="T564" id="Seg_7701" s="T563">наверное=Q</ta>
            <ta e="T565" id="Seg_7702" s="T564">два-DISTR</ta>
            <ta e="T566" id="Seg_7703" s="T565">рубашка-ACC</ta>
            <ta e="T567" id="Seg_7704" s="T566">два-DISTR</ta>
            <ta e="T568" id="Seg_7705" s="T567">штаны-ACC</ta>
            <ta e="T569" id="Seg_7706" s="T568">новый</ta>
            <ta e="T570" id="Seg_7707" s="T569">одежда-3PL-ACC</ta>
            <ta e="T571" id="Seg_7708" s="T570">одеть-PTCP.PRS.[NOM]</ta>
            <ta e="T574" id="Seg_7709" s="T573">думать-PST2.[3SG]</ta>
            <ta e="T575" id="Seg_7710" s="T574">ребенок-PL-ACC</ta>
            <ta e="T576" id="Seg_7711" s="T575">мыться-CAUS-CVB.SIM</ta>
            <ta e="T577" id="Seg_7712" s="T576">приносить-PST2-3PL</ta>
            <ta e="T578" id="Seg_7713" s="T577">где</ta>
            <ta e="T579" id="Seg_7714" s="T578">шустрый</ta>
            <ta e="T580" id="Seg_7715" s="T579">люди.[NOM]</ta>
            <ta e="T581" id="Seg_7716" s="T580">последний</ta>
            <ta e="T582" id="Seg_7717" s="T581">мысль-3PL-INSTR</ta>
            <ta e="T583" id="Seg_7718" s="T582">радостный-ADVZ</ta>
            <ta e="T584" id="Seg_7719" s="T583">смеяться-RECP/COLL-CVB.SIM-смеяться-RECP/COLL-CVB.SIM</ta>
            <ta e="T585" id="Seg_7720" s="T584">горячий.[NOM]</ta>
            <ta e="T586" id="Seg_7721" s="T585">горячий.[NOM]</ta>
            <ta e="T587" id="Seg_7722" s="T586">говорить-EP-RECP/COLL-CVB.SIM</ta>
            <ta e="T588" id="Seg_7723" s="T587">сидеть-PST2-3PL</ta>
            <ta e="T589" id="Seg_7724" s="T588">Тойоо.[NOM]</ta>
            <ta e="T590" id="Seg_7725" s="T589">мыться-CVB.SEQ</ta>
            <ta e="T591" id="Seg_7726" s="T590">кончать-CVB.ANT</ta>
            <ta e="T592" id="Seg_7727" s="T591">одеваться-EP-PST2.[3SG]</ta>
            <ta e="T593" id="Seg_7728" s="T592">одеть-PST2.[3SG]</ta>
            <ta e="T594" id="Seg_7729" s="T593">белый</ta>
            <ta e="T595" id="Seg_7730" s="T594">рубашка-3SG-ACC</ta>
            <ta e="T596" id="Seg_7731" s="T595">штаны-3SG-ACC</ta>
            <ta e="T597" id="Seg_7732" s="T596">некоторый-PL-3SG-ACC</ta>
            <ta e="T598" id="Seg_7733" s="T597">запас-VBZ-CVB.SEQ</ta>
            <ta e="T599" id="Seg_7734" s="T598">укутать-PST2.[3SG]</ta>
            <ta e="T600" id="Seg_7735" s="T599">позже</ta>
            <ta e="T601" id="Seg_7736" s="T600">одеть-CVB.PURP</ta>
            <ta e="T602" id="Seg_7737" s="T601">тело-3SG-DAT/LOC</ta>
            <ta e="T603" id="Seg_7738" s="T602">одеть-CVB.SEQ</ta>
            <ta e="T604" id="Seg_7739" s="T603">белый</ta>
            <ta e="T605" id="Seg_7740" s="T604">одежда-PL-3SG-ACC</ta>
            <ta e="T606" id="Seg_7741" s="T605">что.[NOM]</ta>
            <ta e="T607" id="Seg_7742" s="T606">NEG</ta>
            <ta e="T608" id="Seg_7743" s="T607">быть-NEG.PTCP.PST.[NOM]</ta>
            <ta e="T609" id="Seg_7744" s="T608">подобно</ta>
            <ta e="T610" id="Seg_7745" s="T609">идти-EP-PST2.[3SG]</ta>
            <ta e="T611" id="Seg_7746" s="T610">друг-PL-3SG.[NOM]</ta>
            <ta e="T612" id="Seg_7747" s="T611">EMPH</ta>
            <ta e="T613" id="Seg_7748" s="T612">что-ACC</ta>
            <ta e="T614" id="Seg_7749" s="T613">NEG</ta>
            <ta e="T615" id="Seg_7750" s="T614">говорить-NEG-3PL</ta>
            <ta e="T616" id="Seg_7751" s="T615">быть-PST2.[3SG]</ta>
            <ta e="T617" id="Seg_7752" s="T616">3SG-DAT/LOC</ta>
            <ta e="T618" id="Seg_7753" s="T617">интернат-DAT/LOC</ta>
            <ta e="T619" id="Seg_7754" s="T618">работать-PTCP.PRS</ta>
            <ta e="T620" id="Seg_7755" s="T619">один</ta>
            <ta e="T621" id="Seg_7756" s="T620">жена.[NOM]</ta>
            <ta e="T622" id="Seg_7757" s="T621">объяснять-PST2.[3SG]</ta>
            <ta e="T623" id="Seg_7758" s="T622">как</ta>
            <ta e="T624" id="Seg_7759" s="T623">3SG.[NOM]</ta>
            <ta e="T625" id="Seg_7760" s="T624">одеваться-PTCP.PRS</ta>
            <ta e="T626" id="Seg_7761" s="T625">становиться-PTCP.FUT-3SG-ACC</ta>
            <ta e="T627" id="Seg_7762" s="T626">есть-PTCP.PRS</ta>
            <ta e="T628" id="Seg_7763" s="T627">час-3PL.[NOM]</ta>
            <ta e="T629" id="Seg_7764" s="T628">приходить-PST2.[3SG]</ta>
            <ta e="T630" id="Seg_7765" s="T629">ребенок-PL-ACC</ta>
            <ta e="T631" id="Seg_7766" s="T630">посадить-ITER-PST2-3PL</ta>
            <ta e="T634" id="Seg_7767" s="T633">длинный</ta>
            <ta e="T635" id="Seg_7768" s="T634">очень</ta>
            <ta e="T636" id="Seg_7769" s="T635">стол-PL.[NOM]</ta>
            <ta e="T637" id="Seg_7770" s="T636">два</ta>
            <ta e="T638" id="Seg_7771" s="T637">в.сторону</ta>
            <ta e="T639" id="Seg_7772" s="T638">сторона-3PL-DAT/LOC</ta>
            <ta e="T640" id="Seg_7773" s="T639">большой</ta>
            <ta e="T641" id="Seg_7774" s="T640">посуда-PL-DAT/LOC</ta>
            <ta e="T642" id="Seg_7775" s="T641">класть-EP-PASS/REFL-EP-PST2-3PL</ta>
            <ta e="T643" id="Seg_7776" s="T642">вкусный</ta>
            <ta e="T644" id="Seg_7777" s="T643">запах-PROPR</ta>
            <ta e="T645" id="Seg_7778" s="T644">хлеб-PL.[NOM]</ta>
            <ta e="T646" id="Seg_7779" s="T645">ребенок.[NOM]</ta>
            <ta e="T647" id="Seg_7780" s="T646">каждый</ta>
            <ta e="T648" id="Seg_7781" s="T647">лить-MULT-PST2-3PL</ta>
            <ta e="T649" id="Seg_7782" s="T648">рыба.[NOM]</ta>
            <ta e="T650" id="Seg_7783" s="T649">суп-3SG-ACC</ta>
            <ta e="T651" id="Seg_7784" s="T650">ложка-PL-ACC</ta>
            <ta e="T652" id="Seg_7785" s="T651">давать-FREQ-PST2-3PL</ta>
            <ta e="T653" id="Seg_7786" s="T652">видеть-PST2.[3SG]</ta>
            <ta e="T654" id="Seg_7787" s="T653">ребенок-PL.[NOM]</ta>
            <ta e="T655" id="Seg_7788" s="T654">есть-CVB.SEQ-3PL</ta>
            <ta e="T656" id="Seg_7789" s="T655">затылок-3PL.[NOM]</ta>
            <ta e="T657" id="Seg_7790" s="T656">только</ta>
            <ta e="T658" id="Seg_7791" s="T657">пропадать-CVB.SIM</ta>
            <ta e="T659" id="Seg_7792" s="T658">лежать-PRS.[3SG]</ta>
            <ta e="T660" id="Seg_7793" s="T659">Тойоо.[NOM]</ta>
            <ta e="T661" id="Seg_7794" s="T660">есть-CVB.SIM</ta>
            <ta e="T662" id="Seg_7795" s="T661">сидеть-CVB.SEQ</ta>
            <ta e="T663" id="Seg_7796" s="T662">что-ACC</ta>
            <ta e="T664" id="Seg_7797" s="T663">INDEF</ta>
            <ta e="T665" id="Seg_7798" s="T664">мысль-3SG.[NOM]</ta>
            <ta e="T666" id="Seg_7799" s="T665">приходить-PST2.[3SG]</ta>
            <ta e="T667" id="Seg_7800" s="T666">мама-3SG-ACC</ta>
            <ta e="T668" id="Seg_7801" s="T667">отец-3SG-ACC</ta>
            <ta e="T669" id="Seg_7802" s="T668">Кыча-3SG-ACC</ta>
            <ta e="T670" id="Seg_7803" s="T669">помнить-CVB.SEQ</ta>
            <ta e="T671" id="Seg_7804" s="T670">есть-CVB.SEQ</ta>
            <ta e="T672" id="Seg_7805" s="T671">кончать-CVB.ANT</ta>
            <ta e="T673" id="Seg_7806" s="T672">ребенок-PL.[NOM]</ta>
            <ta e="T674" id="Seg_7807" s="T673">Волочанка.[NOM]</ta>
            <ta e="T675" id="Seg_7808" s="T674">вдоль</ta>
            <ta e="T676" id="Seg_7809" s="T675">играть-CVB.SIM</ta>
            <ta e="T677" id="Seg_7810" s="T676">идти-EP-PST2-3PL</ta>
            <ta e="T678" id="Seg_7811" s="T677">заглянуть-CVB.SIM</ta>
            <ta e="T679" id="Seg_7812" s="T678">русский</ta>
            <ta e="T680" id="Seg_7813" s="T679">дом-PL-ACC</ta>
            <ta e="T681" id="Seg_7814" s="T680">каждый</ta>
            <ta e="T682" id="Seg_7815" s="T681">двор-ACC</ta>
            <ta e="T683" id="Seg_7816" s="T682">3PL-DAT/LOC</ta>
            <ta e="T684" id="Seg_7817" s="T683">каждый-3SG.[NOM]</ta>
            <ta e="T685" id="Seg_7818" s="T684">чудо.[NOM]</ta>
            <ta e="T686" id="Seg_7819" s="T685">только.что</ta>
            <ta e="T687" id="Seg_7820" s="T686">приходить-PTCP.PST</ta>
            <ta e="T688" id="Seg_7821" s="T687">человек-PL-DAT/LOC</ta>
            <ta e="T689" id="Seg_7822" s="T688">следующий</ta>
            <ta e="T690" id="Seg_7823" s="T689">день.[NOM]</ta>
            <ta e="T691" id="Seg_7824" s="T690">опять</ta>
            <ta e="T692" id="Seg_7825" s="T691">большой</ta>
            <ta e="T693" id="Seg_7826" s="T692">ребенок-PROPR</ta>
            <ta e="T694" id="Seg_7827" s="T693">аргиш.[NOM]</ta>
            <ta e="T695" id="Seg_7828" s="T694">приходить-PST2.[3SG]</ta>
            <ta e="T696" id="Seg_7829" s="T695">другой</ta>
            <ta e="T697" id="Seg_7830" s="T696">колхоз-ABL</ta>
            <ta e="T698" id="Seg_7831" s="T697">только.что</ta>
            <ta e="T699" id="Seg_7832" s="T698">приходить-CVB.SIM</ta>
            <ta e="T700" id="Seg_7833" s="T699">ребенок-PL.[NOM]</ta>
            <ta e="T701" id="Seg_7834" s="T700">растерянный-3SG.[NOM]</ta>
            <ta e="T702" id="Seg_7835" s="T701">очень</ta>
            <ta e="T703" id="Seg_7836" s="T702">каждый-ABL</ta>
            <ta e="T705" id="Seg_7837" s="T704">бояться-PTCP.PRS.[NOM]</ta>
            <ta e="T706" id="Seg_7838" s="T705">как-PL</ta>
            <ta e="T707" id="Seg_7839" s="T706">3PL.[NOM]</ta>
            <ta e="T708" id="Seg_7840" s="T707">очень</ta>
            <ta e="T709" id="Seg_7841" s="T708">долго</ta>
            <ta e="T710" id="Seg_7842" s="T709">такой.[NOM]</ta>
            <ta e="T711" id="Seg_7843" s="T710">быть-PST2.NEG-3PL</ta>
            <ta e="T712" id="Seg_7844" s="T711">тоже</ta>
            <ta e="T713" id="Seg_7845" s="T712">Тойоо.[NOM]</ta>
            <ta e="T714" id="Seg_7846" s="T713">Митя.[NOM]</ta>
            <ta e="T715" id="Seg_7847" s="T714">Иван.[NOM]</ta>
            <ta e="T716" id="Seg_7848" s="T715">подобно</ta>
            <ta e="T717" id="Seg_7849" s="T716">стесняться-EP-NEG.PTCP</ta>
            <ta e="T718" id="Seg_7850" s="T717">обычай-ACC</ta>
            <ta e="T719" id="Seg_7851" s="T718">взять-PST2-3PL</ta>
            <ta e="T720" id="Seg_7852" s="T719">новый</ta>
            <ta e="T721" id="Seg_7853" s="T720">друг-PL-3SG-ACC</ta>
            <ta e="T722" id="Seg_7854" s="T721">видеть-CVB.SEQ-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_7855" s="T0">v-v:mood.pn</ta>
            <ta e="T2" id="Seg_7856" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_7857" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_7858" s="T3">n-n:(num)-n:case</ta>
            <ta e="T5" id="Seg_7859" s="T4">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T6" id="Seg_7860" s="T5">n-n:poss-n:case</ta>
            <ta e="T7" id="Seg_7861" s="T6">n-n&gt;adj</ta>
            <ta e="T8" id="Seg_7862" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_7863" s="T8">v-v:tense-v:pred.pn</ta>
            <ta e="T10" id="Seg_7864" s="T9">propr-n:case</ta>
            <ta e="T11" id="Seg_7865" s="T10">propr-n:case</ta>
            <ta e="T12" id="Seg_7866" s="T11">n-n:poss-n:case</ta>
            <ta e="T13" id="Seg_7867" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_7868" s="T13">n-n:poss-n:case</ta>
            <ta e="T15" id="Seg_7869" s="T14">n-n:(poss)-n:case</ta>
            <ta e="T16" id="Seg_7870" s="T15">n-n:(poss)-n:case</ta>
            <ta e="T17" id="Seg_7871" s="T16">adj-n:(poss)-n:case</ta>
            <ta e="T18" id="Seg_7872" s="T17">v-v&gt;v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T19" id="Seg_7873" s="T18">v-v&gt;n-n:case</ta>
            <ta e="T20" id="Seg_7874" s="T19">n-n:(num)-n:case</ta>
            <ta e="T21" id="Seg_7875" s="T20">v-v:cvb</ta>
            <ta e="T22" id="Seg_7876" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_7877" s="T22">post</ta>
            <ta e="T24" id="Seg_7878" s="T23">n-n&gt;adj-n:(num)-n:case</ta>
            <ta e="T25" id="Seg_7879" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_7880" s="T25">v-v:ptcp-v:(num)-v:(case)</ta>
            <ta e="T27" id="Seg_7881" s="T26">adv</ta>
            <ta e="T28" id="Seg_7882" s="T27">n-n:(poss)</ta>
            <ta e="T29" id="Seg_7883" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_7884" s="T29">adj</ta>
            <ta e="T31" id="Seg_7885" s="T30">propr-n:case</ta>
            <ta e="T32" id="Seg_7886" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_7887" s="T32">cardnum</ta>
            <ta e="T34" id="Seg_7888" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_7889" s="T34">v-v:(ins)-v:ptcp-v:(case)</ta>
            <ta e="T36" id="Seg_7890" s="T35">post</ta>
            <ta e="T37" id="Seg_7891" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_7892" s="T37">n-n:poss-n:case</ta>
            <ta e="T39" id="Seg_7893" s="T38">n-n&gt;v-v:ptcp-v:(case)</ta>
            <ta e="T40" id="Seg_7894" s="T39">post</ta>
            <ta e="T41" id="Seg_7895" s="T40">v-v:cvb</ta>
            <ta e="T42" id="Seg_7896" s="T41">v-v:tense-v:pred.pn</ta>
            <ta e="T43" id="Seg_7897" s="T42">n-n:(num)-n:case</ta>
            <ta e="T44" id="Seg_7898" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_7899" s="T44">n-n:(num)-n:case</ta>
            <ta e="T46" id="Seg_7900" s="T45">v-v:(neg)-v:pred.pn</ta>
            <ta e="T47" id="Seg_7901" s="T46">adj</ta>
            <ta e="T48" id="Seg_7902" s="T47">n-n:(num)-n:case</ta>
            <ta e="T49" id="Seg_7903" s="T48">post</ta>
            <ta e="T50" id="Seg_7904" s="T49">ptcl</ta>
            <ta e="T53" id="Seg_7905" s="T52">n-n:(poss)-n:case</ta>
            <ta e="T54" id="Seg_7906" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_7907" s="T54">post</ta>
            <ta e="T56" id="Seg_7908" s="T55">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T57" id="Seg_7909" s="T56">v-v:cvb-v:pred.pn</ta>
            <ta e="T58" id="Seg_7910" s="T57">n-n:(ins)-n:case</ta>
            <ta e="T59" id="Seg_7911" s="T58">n-n:(poss)-n:case</ta>
            <ta e="T60" id="Seg_7912" s="T59">propr</ta>
            <ta e="T61" id="Seg_7913" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_7914" s="T61">v-v:tense-v:pred.pn</ta>
            <ta e="T63" id="Seg_7915" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_7916" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_7917" s="T64">pers-pro:case</ta>
            <ta e="T66" id="Seg_7918" s="T65">post</ta>
            <ta e="T67" id="Seg_7919" s="T66">n-n&gt;adj</ta>
            <ta e="T68" id="Seg_7920" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_7921" s="T68">n-n:poss-n:case</ta>
            <ta e="T72" id="Seg_7922" s="T71">n-n:poss-n:case</ta>
            <ta e="T73" id="Seg_7923" s="T72">v-v:tense-v:pred.pn</ta>
            <ta e="T74" id="Seg_7924" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_7925" s="T74">n-n:case</ta>
            <ta e="T76" id="Seg_7926" s="T75">v-v:tense-v:pred.pn</ta>
            <ta e="T79" id="Seg_7927" s="T78">v-v:ptcp</ta>
            <ta e="T80" id="Seg_7928" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_7929" s="T80">n-n:(poss)-n:case</ta>
            <ta e="T82" id="Seg_7930" s="T81">adv</ta>
            <ta e="T83" id="Seg_7931" s="T82">n-n:case</ta>
            <ta e="T84" id="Seg_7932" s="T83">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T85" id="Seg_7933" s="T84">adj-n:(poss)-n:case</ta>
            <ta e="T86" id="Seg_7934" s="T85">n-n:(num)-n:case</ta>
            <ta e="T87" id="Seg_7935" s="T86">v-v:cvb</ta>
            <ta e="T88" id="Seg_7936" s="T87">v-v:cvb-v:pred.pn</ta>
            <ta e="T89" id="Seg_7937" s="T88">interj-n&gt;v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T90" id="Seg_7938" s="T89">que-pro:case</ta>
            <ta e="T91" id="Seg_7939" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_7940" s="T91">pers-pro:case</ta>
            <ta e="T93" id="Seg_7941" s="T92">v-v:tense-v:pred.pn</ta>
            <ta e="T94" id="Seg_7942" s="T93">n-n:(num)-n:case</ta>
            <ta e="T95" id="Seg_7943" s="T94">adj</ta>
            <ta e="T96" id="Seg_7944" s="T95">n-n:poss-n:case</ta>
            <ta e="T97" id="Seg_7945" s="T96">v-v:(neg)-v:pred.pn</ta>
            <ta e="T98" id="Seg_7946" s="T97">dempro-pro:case</ta>
            <ta e="T99" id="Seg_7947" s="T98">post</ta>
            <ta e="T100" id="Seg_7948" s="T99">v-v:cvb</ta>
            <ta e="T101" id="Seg_7949" s="T100">adj</ta>
            <ta e="T102" id="Seg_7950" s="T101">n-n:case</ta>
            <ta e="T103" id="Seg_7951" s="T102">v-v:(neg)-v:pred.pn</ta>
            <ta e="T104" id="Seg_7952" s="T103">v-v:ptcp</ta>
            <ta e="T105" id="Seg_7953" s="T104">ptcl</ta>
            <ta e="T106" id="Seg_7954" s="T105">v-v:tense-v:pred.pn</ta>
            <ta e="T107" id="Seg_7955" s="T106">n-n:poss-n:case</ta>
            <ta e="T108" id="Seg_7956" s="T107">n-n:poss-n:case</ta>
            <ta e="T109" id="Seg_7957" s="T108">pers-pro:case</ta>
            <ta e="T110" id="Seg_7958" s="T109">adv</ta>
            <ta e="T111" id="Seg_7959" s="T110">adj-n:case</ta>
            <ta e="T112" id="Seg_7960" s="T111">v-v:tense-v:pred.pn</ta>
            <ta e="T113" id="Seg_7961" s="T112">adv</ta>
            <ta e="T114" id="Seg_7962" s="T113">adj</ta>
            <ta e="T115" id="Seg_7963" s="T114">adj</ta>
            <ta e="T116" id="Seg_7964" s="T115">n-n:(num)-n:case</ta>
            <ta e="T117" id="Seg_7965" s="T116">adj</ta>
            <ta e="T118" id="Seg_7966" s="T117">n-n:(num)-n:case</ta>
            <ta e="T119" id="Seg_7967" s="T118">n-n:(num)-n:case</ta>
            <ta e="T120" id="Seg_7968" s="T119">adj</ta>
            <ta e="T121" id="Seg_7969" s="T120">n-n&gt;adj-n:(num)-n:case</ta>
            <ta e="T122" id="Seg_7970" s="T121">n-n&gt;adj-n:(num)-n:case</ta>
            <ta e="T123" id="Seg_7971" s="T122">n-n:poss-n:case</ta>
            <ta e="T124" id="Seg_7972" s="T123">ptcl</ta>
            <ta e="T125" id="Seg_7973" s="T124">n-n:case</ta>
            <ta e="T126" id="Seg_7974" s="T125">n-n:(poss)-n:case</ta>
            <ta e="T127" id="Seg_7975" s="T126">post</ta>
            <ta e="T128" id="Seg_7976" s="T127">n-n:case</ta>
            <ta e="T129" id="Seg_7977" s="T128">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T130" id="Seg_7978" s="T129">n-n:case</ta>
            <ta e="T131" id="Seg_7979" s="T130">n-n:(num)-n:case</ta>
            <ta e="T132" id="Seg_7980" s="T131">n-n:poss-n:case</ta>
            <ta e="T133" id="Seg_7981" s="T132">n-n:poss-n:case</ta>
            <ta e="T134" id="Seg_7982" s="T133">n-n:poss-n:case</ta>
            <ta e="T135" id="Seg_7983" s="T134">post</ta>
            <ta e="T136" id="Seg_7984" s="T135">n-n:case</ta>
            <ta e="T137" id="Seg_7985" s="T136">v-v:cvb</ta>
            <ta e="T138" id="Seg_7986" s="T137">v-v:tense-v:pred.pn</ta>
            <ta e="T139" id="Seg_7987" s="T138">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T140" id="Seg_7988" s="T139">adj-n:(poss)</ta>
            <ta e="T141" id="Seg_7989" s="T140">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T142" id="Seg_7990" s="T141">n-n:(poss)-n:case</ta>
            <ta e="T143" id="Seg_7991" s="T142">v-v:(ins)-v&gt;v-v:(ins)-v:ptcp-v:(num)-v:(case)</ta>
            <ta e="T144" id="Seg_7992" s="T143">n-n:case</ta>
            <ta e="T145" id="Seg_7993" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_7994" s="T145">post</ta>
            <ta e="T147" id="Seg_7995" s="T146">n-n:(poss)-n:case</ta>
            <ta e="T148" id="Seg_7996" s="T147">adj</ta>
            <ta e="T149" id="Seg_7997" s="T148">n-n:poss-n:case</ta>
            <ta e="T150" id="Seg_7998" s="T149">adv</ta>
            <ta e="T151" id="Seg_7999" s="T150">adj-n:case</ta>
            <ta e="T152" id="Seg_8000" s="T151">dempro-pro:case</ta>
            <ta e="T153" id="Seg_8001" s="T152">post</ta>
            <ta e="T154" id="Seg_8002" s="T153">pers-pro:case</ta>
            <ta e="T155" id="Seg_8003" s="T154">adv</ta>
            <ta e="T156" id="Seg_8004" s="T155">v-v:tense-v:pred.pn</ta>
            <ta e="T157" id="Seg_8005" s="T156">propr-n:case</ta>
            <ta e="T158" id="Seg_8006" s="T157">adv</ta>
            <ta e="T159" id="Seg_8007" s="T158">v-v:cvb</ta>
            <ta e="T160" id="Seg_8008" s="T159">v-v:tense-v:pred.pn</ta>
            <ta e="T161" id="Seg_8009" s="T160">n-n:(num)-n:case</ta>
            <ta e="T162" id="Seg_8010" s="T161">n-n:poss-n:case</ta>
            <ta e="T163" id="Seg_8011" s="T162">v-v:tense-v:pred.pn</ta>
            <ta e="T164" id="Seg_8012" s="T163">ptcl</ta>
            <ta e="T165" id="Seg_8013" s="T164">dempro</ta>
            <ta e="T166" id="Seg_8014" s="T165">n-n:case</ta>
            <ta e="T167" id="Seg_8015" s="T166">v-v:cvb</ta>
            <ta e="T168" id="Seg_8016" s="T167">v-v:(neg)-v:pred.pn</ta>
            <ta e="T169" id="Seg_8017" s="T168">adv</ta>
            <ta e="T170" id="Seg_8018" s="T169">ptcl</ta>
            <ta e="T171" id="Seg_8019" s="T170">v-v:tense-v:pred.pn</ta>
            <ta e="T172" id="Seg_8020" s="T171">v-v:tense-v:pred.pn</ta>
            <ta e="T173" id="Seg_8021" s="T172">n-n:(num)-n:poss-n:case</ta>
            <ta e="T174" id="Seg_8022" s="T173">que-pro:case</ta>
            <ta e="T175" id="Seg_8023" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_8024" s="T175">dempro-pro:case</ta>
            <ta e="T177" id="Seg_8025" s="T176">n-n:poss-n:case</ta>
            <ta e="T178" id="Seg_8026" s="T177">v-v:cvb</ta>
            <ta e="T179" id="Seg_8027" s="T178">v-v:tense-v:pred.pn</ta>
            <ta e="T180" id="Seg_8028" s="T179">n-n:poss-n:case</ta>
            <ta e="T181" id="Seg_8029" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_8030" s="T181">v-v:tense-v:pred.pn</ta>
            <ta e="T183" id="Seg_8031" s="T182">que-pro:case</ta>
            <ta e="T184" id="Seg_8032" s="T183">ptcl</ta>
            <ta e="T185" id="Seg_8033" s="T184">n-n:(poss)-n:case</ta>
            <ta e="T186" id="Seg_8034" s="T185">v-v:tense-v:pred.pn</ta>
            <ta e="T187" id="Seg_8035" s="T186">v-v:tense-v:pred.pn</ta>
            <ta e="T188" id="Seg_8036" s="T187">dempro-pro:case</ta>
            <ta e="T189" id="Seg_8037" s="T188">post</ta>
            <ta e="T190" id="Seg_8038" s="T189">que</ta>
            <ta e="T191" id="Seg_8039" s="T190">n-n:case</ta>
            <ta e="T192" id="Seg_8040" s="T191">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T195" id="Seg_8041" s="T194">que-pro:case</ta>
            <ta e="T196" id="Seg_8042" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_8043" s="T196">n-n:case</ta>
            <ta e="T198" id="Seg_8044" s="T197">v-v:ptcp</ta>
            <ta e="T199" id="Seg_8045" s="T198">n-n&gt;adj-n:(poss)-n:case</ta>
            <ta e="T200" id="Seg_8046" s="T199">v-v&gt;v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T201" id="Seg_8047" s="T200">v-v:tense-v:pred.pn</ta>
            <ta e="T202" id="Seg_8048" s="T201">n-n:(poss)-n:case</ta>
            <ta e="T203" id="Seg_8049" s="T202">adj-n:case</ta>
            <ta e="T204" id="Seg_8050" s="T203">n-n:(poss)-n:case</ta>
            <ta e="T205" id="Seg_8051" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_8052" s="T205">adj-n:(poss)-n:case</ta>
            <ta e="T207" id="Seg_8053" s="T206">adv</ta>
            <ta e="T208" id="Seg_8054" s="T207">adv</ta>
            <ta e="T209" id="Seg_8055" s="T208">adj</ta>
            <ta e="T210" id="Seg_8056" s="T209">v-v:ptcp</ta>
            <ta e="T211" id="Seg_8057" s="T210">post</ta>
            <ta e="T212" id="Seg_8058" s="T211">n-n&gt;adj-n:case</ta>
            <ta e="T213" id="Seg_8059" s="T212">n-n&gt;v-v:ptcp</ta>
            <ta e="T214" id="Seg_8060" s="T213">n-n:(poss)-n:case</ta>
            <ta e="T215" id="Seg_8061" s="T214">ptcl</ta>
            <ta e="T216" id="Seg_8062" s="T215">n-n:(poss)-n:case</ta>
            <ta e="T217" id="Seg_8063" s="T216">n-n:(poss)-n:case</ta>
            <ta e="T218" id="Seg_8064" s="T217">adv</ta>
            <ta e="T219" id="Seg_8065" s="T218">adj</ta>
            <ta e="T220" id="Seg_8066" s="T219">n-n&gt;adj-n:case</ta>
            <ta e="T221" id="Seg_8067" s="T220">n-n:poss-n:case</ta>
            <ta e="T222" id="Seg_8068" s="T221">n-n:poss-n:case</ta>
            <ta e="T223" id="Seg_8069" s="T222">adv</ta>
            <ta e="T224" id="Seg_8070" s="T223">v-v:cvb</ta>
            <ta e="T225" id="Seg_8071" s="T224">v-v:cvb</ta>
            <ta e="T226" id="Seg_8072" s="T225">v-v:tense-v:pred.pn</ta>
            <ta e="T227" id="Seg_8073" s="T226">cardnum</ta>
            <ta e="T228" id="Seg_8074" s="T227">n-n:case</ta>
            <ta e="T229" id="Seg_8075" s="T228">n-n:(poss)-n:case</ta>
            <ta e="T230" id="Seg_8076" s="T229">que</ta>
            <ta e="T231" id="Seg_8077" s="T230">ptcl</ta>
            <ta e="T232" id="Seg_8078" s="T231">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T233" id="Seg_8079" s="T232">adj</ta>
            <ta e="T234" id="Seg_8080" s="T233">n-n:poss-n:case</ta>
            <ta e="T235" id="Seg_8081" s="T234">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T236" id="Seg_8082" s="T235">dempro-pro:case</ta>
            <ta e="T237" id="Seg_8083" s="T236">adj</ta>
            <ta e="T238" id="Seg_8084" s="T237">n-n&gt;n-n:case</ta>
            <ta e="T239" id="Seg_8085" s="T238">interj</ta>
            <ta e="T240" id="Seg_8086" s="T239">interj</ta>
            <ta e="T241" id="Seg_8087" s="T240">v-v:tense-v:pred.pn</ta>
            <ta e="T242" id="Seg_8088" s="T241">n-n:case</ta>
            <ta e="T243" id="Seg_8089" s="T242">v-v:ptcp-v:(case)</ta>
            <ta e="T244" id="Seg_8090" s="T243">post</ta>
            <ta e="T245" id="Seg_8091" s="T244">adj</ta>
            <ta e="T246" id="Seg_8092" s="T245">ptcl-adj&gt;adv</ta>
            <ta e="T247" id="Seg_8093" s="T246">n-n:poss-n:case</ta>
            <ta e="T248" id="Seg_8094" s="T247">v-v:tense-v:pred.pn</ta>
            <ta e="T249" id="Seg_8095" s="T248">adj</ta>
            <ta e="T250" id="Seg_8096" s="T249">n-n:case</ta>
            <ta e="T251" id="Seg_8097" s="T250">v-v:ptcp-v:(case)</ta>
            <ta e="T252" id="Seg_8098" s="T251">post</ta>
            <ta e="T253" id="Seg_8099" s="T252">interj</ta>
            <ta e="T254" id="Seg_8100" s="T253">dempro-pro:case</ta>
            <ta e="T255" id="Seg_8101" s="T254">que-pro:case</ta>
            <ta e="T256" id="Seg_8102" s="T255">n-n:(poss)-n:case</ta>
            <ta e="T257" id="Seg_8103" s="T256">v-v:tense-v:pred.pn</ta>
            <ta e="T258" id="Seg_8104" s="T257">v-v:cvb</ta>
            <ta e="T259" id="Seg_8105" s="T258">v-v:cvb</ta>
            <ta e="T260" id="Seg_8106" s="T259">v-v:cvb</ta>
            <ta e="T261" id="Seg_8107" s="T260">propr-n:case</ta>
            <ta e="T262" id="Seg_8108" s="T261">n-n:case</ta>
            <ta e="T263" id="Seg_8109" s="T262">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T264" id="Seg_8110" s="T263">pers-pro:case</ta>
            <ta e="T265" id="Seg_8111" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_8112" s="T265">dempro-pro:case</ta>
            <ta e="T267" id="Seg_8113" s="T266">v-v:cvb</ta>
            <ta e="T268" id="Seg_8114" s="T267">v-v:neg-v:poss.pn</ta>
            <ta e="T269" id="Seg_8115" s="T268">adj</ta>
            <ta e="T270" id="Seg_8116" s="T269">n-n:(num)-n:case</ta>
            <ta e="T271" id="Seg_8117" s="T270">n-n&gt;adj-n:case</ta>
            <ta e="T272" id="Seg_8118" s="T271">v-v:tense-v:pred.pn</ta>
            <ta e="T273" id="Seg_8119" s="T272">emphpro-pro:case</ta>
            <ta e="T274" id="Seg_8120" s="T273">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T275" id="Seg_8121" s="T274">post</ta>
            <ta e="T276" id="Seg_8122" s="T275">v-v:cvb</ta>
            <ta e="T277" id="Seg_8123" s="T276">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T278" id="Seg_8124" s="T277">ptcl</ta>
            <ta e="T279" id="Seg_8125" s="T278">v-v:tense-v:pred.pn</ta>
            <ta e="T280" id="Seg_8126" s="T279">adj</ta>
            <ta e="T281" id="Seg_8127" s="T280">n-n:case</ta>
            <ta e="T282" id="Seg_8128" s="T281">adj</ta>
            <ta e="T283" id="Seg_8129" s="T282">adj</ta>
            <ta e="T284" id="Seg_8130" s="T283">n-n:(poss)-n:case</ta>
            <ta e="T285" id="Seg_8131" s="T284">que-pro:case</ta>
            <ta e="T286" id="Seg_8132" s="T285">ptcl</ta>
            <ta e="T287" id="Seg_8133" s="T286">adj</ta>
            <ta e="T288" id="Seg_8134" s="T287">n-n&gt;n-n:(num)-n:poss-n:case</ta>
            <ta e="T289" id="Seg_8135" s="T288">v-v:cvb</ta>
            <ta e="T290" id="Seg_8136" s="T289">v-v:tense-v:pred.pn</ta>
            <ta e="T291" id="Seg_8137" s="T290">adj</ta>
            <ta e="T292" id="Seg_8138" s="T291">adj</ta>
            <ta e="T293" id="Seg_8139" s="T292">n-n&gt;n-n:(num)-n:case</ta>
            <ta e="T294" id="Seg_8140" s="T293">adj</ta>
            <ta e="T295" id="Seg_8141" s="T294">n-n:poss-n:case</ta>
            <ta e="T296" id="Seg_8142" s="T295">v-v&gt;v-v:cvb</ta>
            <ta e="T297" id="Seg_8143" s="T296">v-v:tense-v:pred.pn</ta>
            <ta e="T298" id="Seg_8144" s="T297">v-v:cvb-v-v:cvb</ta>
            <ta e="T299" id="Seg_8145" s="T298">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T300" id="Seg_8146" s="T299">v-v:cvb</ta>
            <ta e="T301" id="Seg_8147" s="T300">v-v:cvb-v:pred.pn</ta>
            <ta e="T302" id="Seg_8148" s="T301">dempro-dempro</ta>
            <ta e="T303" id="Seg_8149" s="T302">n-n:case</ta>
            <ta e="T304" id="Seg_8150" s="T303">post</ta>
            <ta e="T305" id="Seg_8151" s="T304">v-v:tense-v:pred.pn</ta>
            <ta e="T306" id="Seg_8152" s="T305">interj</ta>
            <ta e="T307" id="Seg_8153" s="T306">v-v:cvb</ta>
            <ta e="T308" id="Seg_8154" s="T307">v-v:tense-v:pred.pn</ta>
            <ta e="T309" id="Seg_8155" s="T308">propr-n:case</ta>
            <ta e="T310" id="Seg_8156" s="T309">adj</ta>
            <ta e="T311" id="Seg_8157" s="T310">n-n:(num)-n:case</ta>
            <ta e="T312" id="Seg_8158" s="T311">n-n:(num)-n:case</ta>
            <ta e="T313" id="Seg_8159" s="T312">adj</ta>
            <ta e="T314" id="Seg_8160" s="T313">n-n:poss-n:case</ta>
            <ta e="T315" id="Seg_8161" s="T314">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T316" id="Seg_8162" s="T315">n-n:poss-n:case</ta>
            <ta e="T317" id="Seg_8163" s="T316">v-v&gt;v-v:cvb-v-v&gt;v-v:cvb</ta>
            <ta e="T318" id="Seg_8164" s="T317">v-v:mood.pn</ta>
            <ta e="T319" id="Seg_8165" s="T318">n</ta>
            <ta e="T320" id="Seg_8166" s="T319">n-n:(poss)-n:case</ta>
            <ta e="T321" id="Seg_8167" s="T320">n-n:case</ta>
            <ta e="T322" id="Seg_8168" s="T321">n-n:poss-n:case</ta>
            <ta e="T323" id="Seg_8169" s="T322">post</ta>
            <ta e="T324" id="Seg_8170" s="T323">dempro-pro:case</ta>
            <ta e="T325" id="Seg_8171" s="T324">ptcl</ta>
            <ta e="T326" id="Seg_8172" s="T325">cardnum</ta>
            <ta e="T327" id="Seg_8173" s="T326">ptcl</ta>
            <ta e="T328" id="Seg_8174" s="T327">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T329" id="Seg_8175" s="T328">n-n:case</ta>
            <ta e="T330" id="Seg_8176" s="T329">dempro</ta>
            <ta e="T331" id="Seg_8177" s="T330">v-v:neg-v:pred.pn</ta>
            <ta e="T332" id="Seg_8178" s="T331">posspr-pro:case</ta>
            <ta e="T333" id="Seg_8179" s="T332">adj-n:(num)-n:case</ta>
            <ta e="T334" id="Seg_8180" s="T333">adv</ta>
            <ta e="T335" id="Seg_8181" s="T334">v-v:cvb</ta>
            <ta e="T336" id="Seg_8182" s="T335">v-v:tense-v:pred.pn</ta>
            <ta e="T337" id="Seg_8183" s="T336">adv</ta>
            <ta e="T338" id="Seg_8184" s="T337">v-v:tense-v:pred.pn</ta>
            <ta e="T339" id="Seg_8185" s="T338">adj-adj&gt;adv</ta>
            <ta e="T340" id="Seg_8186" s="T339">v-v:cvb</ta>
            <ta e="T341" id="Seg_8187" s="T340">propr-n:case</ta>
            <ta e="T342" id="Seg_8188" s="T341">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T343" id="Seg_8189" s="T342">n-n:poss-n:case</ta>
            <ta e="T344" id="Seg_8190" s="T343">v-v:neg-v:pred.pn</ta>
            <ta e="T345" id="Seg_8191" s="T344">cardnum-n:poss-n:case</ta>
            <ta e="T346" id="Seg_8192" s="T345">v-v:cvb</ta>
            <ta e="T347" id="Seg_8193" s="T346">v-v:cvb</ta>
            <ta e="T348" id="Seg_8194" s="T347">n-n:poss-n:case</ta>
            <ta e="T349" id="Seg_8195" s="T348">n-n&gt;v-v:cvb</ta>
            <ta e="T350" id="Seg_8196" s="T349">v-v:cvb</ta>
            <ta e="T351" id="Seg_8197" s="T350">v-v:tense-v:pred.pn</ta>
            <ta e="T352" id="Seg_8198" s="T351">n-n:(poss)-n:case</ta>
            <ta e="T353" id="Seg_8199" s="T352">n-n:poss-n:case</ta>
            <ta e="T354" id="Seg_8200" s="T353">n-n:case</ta>
            <ta e="T355" id="Seg_8201" s="T354">v-v:cvb</ta>
            <ta e="T356" id="Seg_8202" s="T355">v-v:ptcp</ta>
            <ta e="T357" id="Seg_8203" s="T356">n-n:poss-n:case</ta>
            <ta e="T358" id="Seg_8204" s="T357">v-v:tense-v:pred.pn</ta>
            <ta e="T359" id="Seg_8205" s="T358">v-v:cvb</ta>
            <ta e="T360" id="Seg_8206" s="T359">adj</ta>
            <ta e="T361" id="Seg_8207" s="T360">n-n:poss-n:case</ta>
            <ta e="T362" id="Seg_8208" s="T361">v-v:tense-v:pred.pn</ta>
            <ta e="T363" id="Seg_8209" s="T362">n-n:poss-n:case</ta>
            <ta e="T364" id="Seg_8210" s="T363">post</ta>
            <ta e="T365" id="Seg_8211" s="T364">n-n:case</ta>
            <ta e="T366" id="Seg_8212" s="T365">v-v:ptcp</ta>
            <ta e="T367" id="Seg_8213" s="T366">n-n:case</ta>
            <ta e="T368" id="Seg_8214" s="T367">v-v:cvb</ta>
            <ta e="T369" id="Seg_8215" s="T368">v-v:tense-v:pred.pn</ta>
            <ta e="T370" id="Seg_8216" s="T369">n-n:poss-n:case</ta>
            <ta e="T371" id="Seg_8217" s="T370">n-n:poss-n:case</ta>
            <ta e="T372" id="Seg_8218" s="T371">que</ta>
            <ta e="T373" id="Seg_8219" s="T372">ptcl</ta>
            <ta e="T374" id="Seg_8220" s="T373">que</ta>
            <ta e="T375" id="Seg_8221" s="T374">ptcl</ta>
            <ta e="T376" id="Seg_8222" s="T375">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T377" id="Seg_8223" s="T376">v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T378" id="Seg_8224" s="T377">n-n:poss-n:case</ta>
            <ta e="T379" id="Seg_8225" s="T378">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T380" id="Seg_8226" s="T379">n-n:case</ta>
            <ta e="T381" id="Seg_8227" s="T380">post-n:(num)</ta>
            <ta e="T382" id="Seg_8228" s="T381">n-n:(poss)-n:case</ta>
            <ta e="T383" id="Seg_8229" s="T382">adj</ta>
            <ta e="T384" id="Seg_8230" s="T383">n-n:poss-n:case</ta>
            <ta e="T385" id="Seg_8231" s="T384">v-v:tense-v:pred.pn</ta>
            <ta e="T386" id="Seg_8232" s="T385">n-n:poss-n:case</ta>
            <ta e="T387" id="Seg_8233" s="T386">post</ta>
            <ta e="T388" id="Seg_8234" s="T387">v-v:cvb</ta>
            <ta e="T389" id="Seg_8235" s="T388">v-v:tense-v:pred.pn</ta>
            <ta e="T390" id="Seg_8236" s="T389">indfpro</ta>
            <ta e="T391" id="Seg_8237" s="T390">n-n:(num)-n:case</ta>
            <ta e="T392" id="Seg_8238" s="T391">dempro-pro:case</ta>
            <ta e="T393" id="Seg_8239" s="T392">v-v:cvb-v:pred.pn</ta>
            <ta e="T394" id="Seg_8240" s="T393">cardnum</ta>
            <ta e="T395" id="Seg_8241" s="T394">n-n:case</ta>
            <ta e="T396" id="Seg_8242" s="T395">v-v:tense-v:pred.pn</ta>
            <ta e="T397" id="Seg_8243" s="T396">n-n:poss-n:case</ta>
            <ta e="T398" id="Seg_8244" s="T397">adj</ta>
            <ta e="T399" id="Seg_8245" s="T398">n-n:case</ta>
            <ta e="T400" id="Seg_8246" s="T399">v-v:cvb</ta>
            <ta e="T401" id="Seg_8247" s="T400">v-v:tense-v:pred.pn</ta>
            <ta e="T402" id="Seg_8248" s="T401">propr-n:case</ta>
            <ta e="T403" id="Seg_8249" s="T402">que-pro:case</ta>
            <ta e="T404" id="Seg_8250" s="T403">ptcl</ta>
            <ta e="T405" id="Seg_8251" s="T404">v-v:cvb-v-v:cvb</ta>
            <ta e="T406" id="Seg_8252" s="T405">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T407" id="Seg_8253" s="T406">adj</ta>
            <ta e="T408" id="Seg_8254" s="T407">propr-n:case</ta>
            <ta e="T409" id="Seg_8255" s="T408">post</ta>
            <ta e="T410" id="Seg_8256" s="T409">n-n:poss-n:case</ta>
            <ta e="T411" id="Seg_8257" s="T410">v-v:tense-v:pred.pn</ta>
            <ta e="T412" id="Seg_8258" s="T411">adj</ta>
            <ta e="T413" id="Seg_8259" s="T412">n-n:(poss)-n:case</ta>
            <ta e="T414" id="Seg_8260" s="T413">adj</ta>
            <ta e="T415" id="Seg_8261" s="T414">n-n&gt;adj</ta>
            <ta e="T416" id="Seg_8262" s="T415">n-n&gt;adj</ta>
            <ta e="T417" id="Seg_8263" s="T416">n-n:case</ta>
            <ta e="T418" id="Seg_8264" s="T417">interj</ta>
            <ta e="T419" id="Seg_8265" s="T418">v-v:cvb</ta>
            <ta e="T420" id="Seg_8266" s="T419">v-v:tense-v:pred.pn</ta>
            <ta e="T421" id="Seg_8267" s="T420">propr-n:case</ta>
            <ta e="T422" id="Seg_8268" s="T421">n-n:poss-n:case</ta>
            <ta e="T423" id="Seg_8269" s="T422">post</ta>
            <ta e="T424" id="Seg_8270" s="T423">v-v:cvb</ta>
            <ta e="T425" id="Seg_8271" s="T424">v-v:cvb</ta>
            <ta e="T426" id="Seg_8272" s="T425">v-v:tense-v:pred.pn</ta>
            <ta e="T427" id="Seg_8273" s="T426">n-n:(poss)-n:case</ta>
            <ta e="T428" id="Seg_8274" s="T427">ptcl</ta>
            <ta e="T429" id="Seg_8275" s="T428">que-pro:(poss)-pro:case</ta>
            <ta e="T430" id="Seg_8276" s="T429">ptcl</ta>
            <ta e="T431" id="Seg_8277" s="T430">v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T432" id="Seg_8278" s="T431">n-n:case</ta>
            <ta e="T433" id="Seg_8279" s="T432">v-v:tense-v:pred.pn</ta>
            <ta e="T434" id="Seg_8280" s="T433">v-v:cvb</ta>
            <ta e="T435" id="Seg_8281" s="T434">v-v:cvb</ta>
            <ta e="T436" id="Seg_8282" s="T435">adj-n:(poss)-n:case</ta>
            <ta e="T437" id="Seg_8283" s="T436">v-v:cvb</ta>
            <ta e="T438" id="Seg_8284" s="T437">n-n:case</ta>
            <ta e="T439" id="Seg_8285" s="T438">v-v:tense-v:pred.pn</ta>
            <ta e="T440" id="Seg_8286" s="T439">pers-pro:case</ta>
            <ta e="T441" id="Seg_8287" s="T440">propr-n:case</ta>
            <ta e="T442" id="Seg_8288" s="T441">n-n:poss-n:case</ta>
            <ta e="T443" id="Seg_8289" s="T442">v-v:cvb</ta>
            <ta e="T444" id="Seg_8290" s="T443">v-v:tense-v:pred.pn</ta>
            <ta e="T445" id="Seg_8291" s="T444">n-n:poss-n:case</ta>
            <ta e="T446" id="Seg_8292" s="T445">ptcl</ta>
            <ta e="T447" id="Seg_8293" s="T446">v-v:tense-v:pred.pn</ta>
            <ta e="T448" id="Seg_8294" s="T447">v-v:mood.pn</ta>
            <ta e="T449" id="Seg_8295" s="T448">que</ta>
            <ta e="T450" id="Seg_8296" s="T449">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T451" id="Seg_8297" s="T450">v-v:ptcp-v:(case)</ta>
            <ta e="T452" id="Seg_8298" s="T451">post</ta>
            <ta e="T453" id="Seg_8299" s="T452">propr-n:case</ta>
            <ta e="T454" id="Seg_8300" s="T453">v-v:cvb</ta>
            <ta e="T455" id="Seg_8301" s="T454">v-v:cvb</ta>
            <ta e="T456" id="Seg_8302" s="T455">adj</ta>
            <ta e="T457" id="Seg_8303" s="T456">n-n:poss-n:case</ta>
            <ta e="T458" id="Seg_8304" s="T457">n-n:(poss)</ta>
            <ta e="T459" id="Seg_8305" s="T458">ptcl</ta>
            <ta e="T460" id="Seg_8306" s="T459">n-n:(num)-n:poss-n:case</ta>
            <ta e="T461" id="Seg_8307" s="T460">post</ta>
            <ta e="T462" id="Seg_8308" s="T461">v-v:tense-v:pred.pn</ta>
            <ta e="T463" id="Seg_8309" s="T462">interj</ta>
            <ta e="T464" id="Seg_8310" s="T463">adv</ta>
            <ta e="T465" id="Seg_8311" s="T464">v-v:cvb</ta>
            <ta e="T466" id="Seg_8312" s="T465">v-v:tense-v:poss.pn</ta>
            <ta e="T467" id="Seg_8313" s="T466">adv</ta>
            <ta e="T468" id="Seg_8314" s="T467">v-v:cvb</ta>
            <ta e="T469" id="Seg_8315" s="T468">v-v:tense-v:poss.pn</ta>
            <ta e="T470" id="Seg_8316" s="T469">n-n:(poss)-n:case</ta>
            <ta e="T471" id="Seg_8317" s="T470">n-n:case</ta>
            <ta e="T472" id="Seg_8318" s="T471">v-v:cvb</ta>
            <ta e="T473" id="Seg_8319" s="T472">que-pro:case</ta>
            <ta e="T474" id="Seg_8320" s="T473">ptcl</ta>
            <ta e="T475" id="Seg_8321" s="T474">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T476" id="Seg_8322" s="T475">n-n:poss-n:case</ta>
            <ta e="T477" id="Seg_8323" s="T476">adj</ta>
            <ta e="T478" id="Seg_8324" s="T477">n-n:poss-n:case</ta>
            <ta e="T479" id="Seg_8325" s="T478">v-v:cvb</ta>
            <ta e="T480" id="Seg_8326" s="T479">v-v:tense-v:pred.pn</ta>
            <ta e="T482" id="Seg_8327" s="T481">adj</ta>
            <ta e="T483" id="Seg_8328" s="T482">n-n:poss-n:case</ta>
            <ta e="T484" id="Seg_8329" s="T483">v-v:tense-v:pred.pn</ta>
            <ta e="T485" id="Seg_8330" s="T484">que</ta>
            <ta e="T486" id="Seg_8331" s="T485">ptcl</ta>
            <ta e="T487" id="Seg_8332" s="T486">v-v:ptcp-v:(case)</ta>
            <ta e="T488" id="Seg_8333" s="T487">adv</ta>
            <ta e="T489" id="Seg_8334" s="T488">adv</ta>
            <ta e="T490" id="Seg_8335" s="T489">adj</ta>
            <ta e="T491" id="Seg_8336" s="T490">n-n:case</ta>
            <ta e="T492" id="Seg_8337" s="T491">v-v:tense-v:pred.pn</ta>
            <ta e="T494" id="Seg_8338" s="T492">adv</ta>
            <ta e="T495" id="Seg_8339" s="T494">n-n:(num)-n:poss-n:case</ta>
            <ta e="T496" id="Seg_8340" s="T495">v-v:tense-v:pred.pn</ta>
            <ta e="T497" id="Seg_8341" s="T496">adv</ta>
            <ta e="T498" id="Seg_8342" s="T497">pers-pro:case</ta>
            <ta e="T499" id="Seg_8343" s="T498">n-n:case</ta>
            <ta e="T500" id="Seg_8344" s="T499">v-v:(ins)-v:mood.pn</ta>
            <ta e="T501" id="Seg_8345" s="T500">adv</ta>
            <ta e="T502" id="Seg_8346" s="T501">v-v&gt;v-v:(ins)-v:mood.pn</ta>
            <ta e="T503" id="Seg_8347" s="T502">n-n:poss-n:case</ta>
            <ta e="T504" id="Seg_8348" s="T503">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T505" id="Seg_8349" s="T504">post</ta>
            <ta e="T506" id="Seg_8350" s="T505">adj-n:poss-n:case</ta>
            <ta e="T507" id="Seg_8351" s="T506">adj-adj&gt;adv</ta>
            <ta e="T508" id="Seg_8352" s="T507">v-v:(ins)-v:mood.pn</ta>
            <ta e="T509" id="Seg_8353" s="T508">n-n:(num)-n:case</ta>
            <ta e="T510" id="Seg_8354" s="T509">v-v&gt;v-v:cvb</ta>
            <ta e="T511" id="Seg_8355" s="T510">n-n:(num)-n:case</ta>
            <ta e="T512" id="Seg_8356" s="T511">adv</ta>
            <ta e="T513" id="Seg_8357" s="T512">v-v:cvb</ta>
            <ta e="T514" id="Seg_8358" s="T513">v-v:tense-v:pred.pn</ta>
            <ta e="T515" id="Seg_8359" s="T514">v-v:tense-v:pred.pn</ta>
            <ta e="T516" id="Seg_8360" s="T515">adj-n:poss-n:case</ta>
            <ta e="T517" id="Seg_8361" s="T516">v-v:ptcp</ta>
            <ta e="T518" id="Seg_8362" s="T517">n-n:poss-n:case</ta>
            <ta e="T519" id="Seg_8363" s="T518">adj-adj&gt;adv</ta>
            <ta e="T521" id="Seg_8364" s="T520">n-n&gt;adj-n:case</ta>
            <ta e="T522" id="Seg_8365" s="T521">v-v:ptcp</ta>
            <ta e="T523" id="Seg_8366" s="T522">adj</ta>
            <ta e="T524" id="Seg_8367" s="T523">n-n:(num)-n:case</ta>
            <ta e="T525" id="Seg_8368" s="T524">n-n:(num)-n:case</ta>
            <ta e="T526" id="Seg_8369" s="T525">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T527" id="Seg_8370" s="T526">adj</ta>
            <ta e="T528" id="Seg_8371" s="T527">v-v:ptcp</ta>
            <ta e="T529" id="Seg_8372" s="T528">n-n:case</ta>
            <ta e="T530" id="Seg_8373" s="T529">n-n:(num)-n:poss-n:case</ta>
            <ta e="T531" id="Seg_8374" s="T530">v-v:tense-v:pred.pn</ta>
            <ta e="T532" id="Seg_8375" s="T531">que-pro:case</ta>
            <ta e="T533" id="Seg_8376" s="T532">n-n:case</ta>
            <ta e="T534" id="Seg_8377" s="T533">n-n:(num)-n:case</ta>
            <ta e="T535" id="Seg_8378" s="T534">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T536" id="Seg_8379" s="T535">adv</ta>
            <ta e="T537" id="Seg_8380" s="T536">v-v:tense-v:pred.pn</ta>
            <ta e="T538" id="Seg_8381" s="T537">n-n:poss-n:case</ta>
            <ta e="T539" id="Seg_8382" s="T538">v-v:cvb-v:pred.pn</ta>
            <ta e="T540" id="Seg_8383" s="T539">propr-n:case</ta>
            <ta e="T541" id="Seg_8384" s="T540">ptcl</ta>
            <ta e="T542" id="Seg_8385" s="T541">v-v:tense-v:pred.pn</ta>
            <ta e="T543" id="Seg_8386" s="T542">v-v:ptcp</ta>
            <ta e="T544" id="Seg_8387" s="T543">n-n:case</ta>
            <ta e="T545" id="Seg_8388" s="T544">v-v:tense-v:pred.pn</ta>
            <ta e="T546" id="Seg_8389" s="T545">v-v:ptcp</ta>
            <ta e="T547" id="Seg_8390" s="T546">n-n:poss-n:case</ta>
            <ta e="T548" id="Seg_8391" s="T547">v-v:tense-v:pred.pn</ta>
            <ta e="T549" id="Seg_8392" s="T548">n-n:poss-n:case</ta>
            <ta e="T550" id="Seg_8393" s="T549">v-v:cvb</ta>
            <ta e="T551" id="Seg_8394" s="T550">v-v:tense-v:pred.pn</ta>
            <ta e="T552" id="Seg_8395" s="T551">v-v:cvb</ta>
            <ta e="T553" id="Seg_8396" s="T552">que-pro:case</ta>
            <ta e="T554" id="Seg_8397" s="T553">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T555" id="Seg_8398" s="T554">adj-n:(poss)-n:case</ta>
            <ta e="T556" id="Seg_8399" s="T555">pers-pro:case</ta>
            <ta e="T557" id="Seg_8400" s="T556">interj</ta>
            <ta e="T558" id="Seg_8401" s="T557">dempro</ta>
            <ta e="T559" id="Seg_8402" s="T558">conj</ta>
            <ta e="T560" id="Seg_8403" s="T559">v-v:mood-v:pred.pn</ta>
            <ta e="T561" id="Seg_8404" s="T560">que</ta>
            <ta e="T562" id="Seg_8405" s="T561">pers-pro:case</ta>
            <ta e="T563" id="Seg_8406" s="T562">v-v:tense-v:poss.pn</ta>
            <ta e="T564" id="Seg_8407" s="T563">adv-ptcl</ta>
            <ta e="T565" id="Seg_8408" s="T564">cardnum-cardnum&gt;distrnum</ta>
            <ta e="T566" id="Seg_8409" s="T565">n-n:case</ta>
            <ta e="T567" id="Seg_8410" s="T566">cardnum-cardnum&gt;distrnum</ta>
            <ta e="T568" id="Seg_8411" s="T567">n-n:case</ta>
            <ta e="T569" id="Seg_8412" s="T568">adj</ta>
            <ta e="T570" id="Seg_8413" s="T569">n-n:poss-n:case</ta>
            <ta e="T571" id="Seg_8414" s="T570">v-v:ptcp-v:(case)</ta>
            <ta e="T574" id="Seg_8415" s="T573">v-v:tense-v:pred.pn</ta>
            <ta e="T575" id="Seg_8416" s="T574">n-n:(num)-n:case</ta>
            <ta e="T576" id="Seg_8417" s="T575">v-v&gt;v-v:cvb</ta>
            <ta e="T577" id="Seg_8418" s="T576">v-v:tense-v:pred.pn</ta>
            <ta e="T578" id="Seg_8419" s="T577">que</ta>
            <ta e="T579" id="Seg_8420" s="T578">adj</ta>
            <ta e="T580" id="Seg_8421" s="T579">n-n:case</ta>
            <ta e="T581" id="Seg_8422" s="T580">adj</ta>
            <ta e="T582" id="Seg_8423" s="T581">n-n:poss-n:case</ta>
            <ta e="T583" id="Seg_8424" s="T582">adj-adj&gt;adv</ta>
            <ta e="T584" id="Seg_8425" s="T583">v-v&gt;v-v:cvb-v-v&gt;v-v:cvb</ta>
            <ta e="T585" id="Seg_8426" s="T584">adj-n:case</ta>
            <ta e="T586" id="Seg_8427" s="T585">adj-n:case</ta>
            <ta e="T587" id="Seg_8428" s="T586">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T588" id="Seg_8429" s="T587">v-v:tense-v:pred.pn</ta>
            <ta e="T589" id="Seg_8430" s="T588">propr-n:case</ta>
            <ta e="T590" id="Seg_8431" s="T589">v-v:cvb</ta>
            <ta e="T591" id="Seg_8432" s="T590">v-v:cvb</ta>
            <ta e="T592" id="Seg_8433" s="T591">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T593" id="Seg_8434" s="T592">v-v:tense-v:pred.pn</ta>
            <ta e="T594" id="Seg_8435" s="T593">adj</ta>
            <ta e="T595" id="Seg_8436" s="T594">n-n:poss-n:case</ta>
            <ta e="T596" id="Seg_8437" s="T595">n-n:poss-n:case</ta>
            <ta e="T597" id="Seg_8438" s="T596">indfpro-pro:(num)-pro:(poss)-pro:case</ta>
            <ta e="T598" id="Seg_8439" s="T597">n-n&gt;v-v:cvb</ta>
            <ta e="T599" id="Seg_8440" s="T598">v-v:tense-v:pred.pn</ta>
            <ta e="T600" id="Seg_8441" s="T599">adv</ta>
            <ta e="T601" id="Seg_8442" s="T600">v-v:cvb</ta>
            <ta e="T602" id="Seg_8443" s="T601">n-n:poss-n:case</ta>
            <ta e="T603" id="Seg_8444" s="T602">v-v:cvb</ta>
            <ta e="T604" id="Seg_8445" s="T603">adj</ta>
            <ta e="T605" id="Seg_8446" s="T604">n-n:(num)-n:poss-n:case</ta>
            <ta e="T606" id="Seg_8447" s="T605">que-pro:case</ta>
            <ta e="T607" id="Seg_8448" s="T606">ptcl</ta>
            <ta e="T608" id="Seg_8449" s="T607">v-v:ptcp-v:(case)</ta>
            <ta e="T609" id="Seg_8450" s="T608">post</ta>
            <ta e="T610" id="Seg_8451" s="T609">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T611" id="Seg_8452" s="T610">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T612" id="Seg_8453" s="T611">ptcl</ta>
            <ta e="T613" id="Seg_8454" s="T612">que-pro:case</ta>
            <ta e="T614" id="Seg_8455" s="T613">ptcl</ta>
            <ta e="T615" id="Seg_8456" s="T614">v-v:(neg)-v:pred.pn</ta>
            <ta e="T616" id="Seg_8457" s="T615">v-v:tense-v:pred.pn</ta>
            <ta e="T617" id="Seg_8458" s="T616">pers-pro:case</ta>
            <ta e="T618" id="Seg_8459" s="T617">n-n:case</ta>
            <ta e="T619" id="Seg_8460" s="T618">v-v:ptcp</ta>
            <ta e="T620" id="Seg_8461" s="T619">cardnum</ta>
            <ta e="T621" id="Seg_8462" s="T620">n-n:case</ta>
            <ta e="T622" id="Seg_8463" s="T621">v-v:tense-v:pred.pn</ta>
            <ta e="T623" id="Seg_8464" s="T622">que</ta>
            <ta e="T624" id="Seg_8465" s="T623">pers-pro:case</ta>
            <ta e="T625" id="Seg_8466" s="T624">v-v:ptcp</ta>
            <ta e="T626" id="Seg_8467" s="T625">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T627" id="Seg_8468" s="T626">v-v:ptcp</ta>
            <ta e="T628" id="Seg_8469" s="T627">n-n:(poss)-n:case</ta>
            <ta e="T629" id="Seg_8470" s="T628">v-v:tense-v:pred.pn</ta>
            <ta e="T630" id="Seg_8471" s="T629">n-n:(num)-n:case</ta>
            <ta e="T631" id="Seg_8472" s="T630">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T634" id="Seg_8473" s="T633">adj</ta>
            <ta e="T635" id="Seg_8474" s="T634">ptcl</ta>
            <ta e="T636" id="Seg_8475" s="T635">n-n:(num)-n:case</ta>
            <ta e="T637" id="Seg_8476" s="T636">cardnum</ta>
            <ta e="T638" id="Seg_8477" s="T637">post</ta>
            <ta e="T639" id="Seg_8478" s="T638">n-n:poss-n:case</ta>
            <ta e="T640" id="Seg_8479" s="T639">adj</ta>
            <ta e="T641" id="Seg_8480" s="T640">n-n:(num)-n:case</ta>
            <ta e="T642" id="Seg_8481" s="T641">v-v:(ins)-v&gt;v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T643" id="Seg_8482" s="T642">adj</ta>
            <ta e="T644" id="Seg_8483" s="T643">n-n&gt;adj</ta>
            <ta e="T645" id="Seg_8484" s="T644">n-n:(num)-n:case</ta>
            <ta e="T646" id="Seg_8485" s="T645">n-n:case</ta>
            <ta e="T647" id="Seg_8486" s="T646">adj</ta>
            <ta e="T648" id="Seg_8487" s="T647">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T649" id="Seg_8488" s="T648">n-n:case</ta>
            <ta e="T650" id="Seg_8489" s="T649">n-n:poss-n:case</ta>
            <ta e="T651" id="Seg_8490" s="T650">n-n:(num)-n:case</ta>
            <ta e="T652" id="Seg_8491" s="T651">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T653" id="Seg_8492" s="T652">v-v:tense-v:pred.pn</ta>
            <ta e="T654" id="Seg_8493" s="T653">n-n:(num)-n:case</ta>
            <ta e="T655" id="Seg_8494" s="T654">v-v:cvb-v:pred.pn</ta>
            <ta e="T656" id="Seg_8495" s="T655">n-n:(poss)-n:case</ta>
            <ta e="T657" id="Seg_8496" s="T656">ptcl</ta>
            <ta e="T658" id="Seg_8497" s="T657">v-v:cvb</ta>
            <ta e="T659" id="Seg_8498" s="T658">v-v:tense-v:pred.pn</ta>
            <ta e="T660" id="Seg_8499" s="T659">propr-n:case</ta>
            <ta e="T661" id="Seg_8500" s="T660">v-v:cvb</ta>
            <ta e="T662" id="Seg_8501" s="T661">v-v:cvb</ta>
            <ta e="T663" id="Seg_8502" s="T662">que-pro:case</ta>
            <ta e="T664" id="Seg_8503" s="T663">ptcl</ta>
            <ta e="T665" id="Seg_8504" s="T664">n-n:(poss)-n:case</ta>
            <ta e="T666" id="Seg_8505" s="T665">v-v:tense-v:pred.pn</ta>
            <ta e="T667" id="Seg_8506" s="T666">n-n:poss-n:case</ta>
            <ta e="T668" id="Seg_8507" s="T667">n-n:poss-n:case</ta>
            <ta e="T669" id="Seg_8508" s="T668">propr-n:poss-n:case</ta>
            <ta e="T670" id="Seg_8509" s="T669">v-v:cvb</ta>
            <ta e="T671" id="Seg_8510" s="T670">v-v:cvb</ta>
            <ta e="T672" id="Seg_8511" s="T671">v-v:cvb</ta>
            <ta e="T673" id="Seg_8512" s="T672">n-n:(num)-n:case</ta>
            <ta e="T674" id="Seg_8513" s="T673">propr-n:case</ta>
            <ta e="T675" id="Seg_8514" s="T674">post</ta>
            <ta e="T676" id="Seg_8515" s="T675">v-v:cvb</ta>
            <ta e="T677" id="Seg_8516" s="T676">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T678" id="Seg_8517" s="T677">v-v:cvb</ta>
            <ta e="T679" id="Seg_8518" s="T678">adj</ta>
            <ta e="T680" id="Seg_8519" s="T679">n-n:(num)-n:case</ta>
            <ta e="T681" id="Seg_8520" s="T680">adj</ta>
            <ta e="T682" id="Seg_8521" s="T681">n-n:case</ta>
            <ta e="T683" id="Seg_8522" s="T682">pers-pro:case</ta>
            <ta e="T684" id="Seg_8523" s="T683">adj-n:(poss)-n:case</ta>
            <ta e="T685" id="Seg_8524" s="T684">n-n:case</ta>
            <ta e="T686" id="Seg_8525" s="T685">adv</ta>
            <ta e="T687" id="Seg_8526" s="T686">v-v:ptcp</ta>
            <ta e="T688" id="Seg_8527" s="T687">n-n:(num)-n:case</ta>
            <ta e="T689" id="Seg_8528" s="T688">adj</ta>
            <ta e="T690" id="Seg_8529" s="T689">n-n:case</ta>
            <ta e="T691" id="Seg_8530" s="T690">ptcl</ta>
            <ta e="T692" id="Seg_8531" s="T691">adj</ta>
            <ta e="T693" id="Seg_8532" s="T692">n-n&gt;adj</ta>
            <ta e="T694" id="Seg_8533" s="T693">n-n:case</ta>
            <ta e="T695" id="Seg_8534" s="T694">v-v:tense-v:pred.pn</ta>
            <ta e="T696" id="Seg_8535" s="T695">adj</ta>
            <ta e="T697" id="Seg_8536" s="T696">n-n:case</ta>
            <ta e="T698" id="Seg_8537" s="T697">adv</ta>
            <ta e="T699" id="Seg_8538" s="T698">v-v:cvb</ta>
            <ta e="T700" id="Seg_8539" s="T699">n-n:(num)-n:case</ta>
            <ta e="T701" id="Seg_8540" s="T700">adj-n:(poss)-n:case</ta>
            <ta e="T702" id="Seg_8541" s="T701">adv</ta>
            <ta e="T703" id="Seg_8542" s="T702">adj-n:case</ta>
            <ta e="T705" id="Seg_8543" s="T704">v-v:ptcp-v:(case)</ta>
            <ta e="T706" id="Seg_8544" s="T705">post-n:(num)</ta>
            <ta e="T707" id="Seg_8545" s="T706">pers-pro:case</ta>
            <ta e="T708" id="Seg_8546" s="T707">adv</ta>
            <ta e="T709" id="Seg_8547" s="T708">adv</ta>
            <ta e="T710" id="Seg_8548" s="T709">dempro-pro:case</ta>
            <ta e="T711" id="Seg_8549" s="T710">v-v:neg-v:pred.pn</ta>
            <ta e="T712" id="Seg_8550" s="T711">ptcl</ta>
            <ta e="T713" id="Seg_8551" s="T712">propr-n:case</ta>
            <ta e="T714" id="Seg_8552" s="T713">propr-n:case</ta>
            <ta e="T715" id="Seg_8553" s="T714">propr-n:case</ta>
            <ta e="T716" id="Seg_8554" s="T715">post</ta>
            <ta e="T717" id="Seg_8555" s="T716">v-v:(ins)-v:ptcp</ta>
            <ta e="T718" id="Seg_8556" s="T717">n-n:case</ta>
            <ta e="T719" id="Seg_8557" s="T718">v-v:tense-v:pred.pn</ta>
            <ta e="T720" id="Seg_8558" s="T719">adj</ta>
            <ta e="T721" id="Seg_8559" s="T720">n-n:(num)-n:poss-n:case</ta>
            <ta e="T722" id="Seg_8560" s="T721">v-v:cvb-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_8561" s="T0">v</ta>
            <ta e="T2" id="Seg_8562" s="T1">n</ta>
            <ta e="T3" id="Seg_8563" s="T2">n</ta>
            <ta e="T4" id="Seg_8564" s="T3">n</ta>
            <ta e="T5" id="Seg_8565" s="T4">ordnum</ta>
            <ta e="T6" id="Seg_8566" s="T5">n</ta>
            <ta e="T7" id="Seg_8567" s="T6">adj</ta>
            <ta e="T8" id="Seg_8568" s="T7">n</ta>
            <ta e="T9" id="Seg_8569" s="T8">v</ta>
            <ta e="T10" id="Seg_8570" s="T9">propr</ta>
            <ta e="T11" id="Seg_8571" s="T10">propr</ta>
            <ta e="T12" id="Seg_8572" s="T11">n</ta>
            <ta e="T13" id="Seg_8573" s="T12">n</ta>
            <ta e="T14" id="Seg_8574" s="T13">n</ta>
            <ta e="T15" id="Seg_8575" s="T14">n</ta>
            <ta e="T16" id="Seg_8576" s="T15">n</ta>
            <ta e="T17" id="Seg_8577" s="T16">adj</ta>
            <ta e="T18" id="Seg_8578" s="T17">v</ta>
            <ta e="T19" id="Seg_8579" s="T18">n</ta>
            <ta e="T20" id="Seg_8580" s="T19">n</ta>
            <ta e="T21" id="Seg_8581" s="T20">v</ta>
            <ta e="T22" id="Seg_8582" s="T21">n</ta>
            <ta e="T23" id="Seg_8583" s="T22">post</ta>
            <ta e="T24" id="Seg_8584" s="T23">adj</ta>
            <ta e="T25" id="Seg_8585" s="T24">n</ta>
            <ta e="T26" id="Seg_8586" s="T25">v</ta>
            <ta e="T27" id="Seg_8587" s="T26">adv</ta>
            <ta e="T28" id="Seg_8588" s="T27">n</ta>
            <ta e="T29" id="Seg_8589" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_8590" s="T29">adj</ta>
            <ta e="T31" id="Seg_8591" s="T30">propr</ta>
            <ta e="T32" id="Seg_8592" s="T31">n</ta>
            <ta e="T33" id="Seg_8593" s="T32">cardnum</ta>
            <ta e="T34" id="Seg_8594" s="T33">n</ta>
            <ta e="T35" id="Seg_8595" s="T34">v</ta>
            <ta e="T36" id="Seg_8596" s="T35">post</ta>
            <ta e="T37" id="Seg_8597" s="T36">n</ta>
            <ta e="T38" id="Seg_8598" s="T37">n</ta>
            <ta e="T39" id="Seg_8599" s="T38">v</ta>
            <ta e="T40" id="Seg_8600" s="T39">post</ta>
            <ta e="T41" id="Seg_8601" s="T40">cop</ta>
            <ta e="T42" id="Seg_8602" s="T41">aux</ta>
            <ta e="T43" id="Seg_8603" s="T42">n</ta>
            <ta e="T44" id="Seg_8604" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_8605" s="T44">n</ta>
            <ta e="T46" id="Seg_8606" s="T45">v</ta>
            <ta e="T47" id="Seg_8607" s="T46">adj</ta>
            <ta e="T48" id="Seg_8608" s="T47">n</ta>
            <ta e="T49" id="Seg_8609" s="T48">post</ta>
            <ta e="T50" id="Seg_8610" s="T49">ptcl</ta>
            <ta e="T53" id="Seg_8611" s="T52">n</ta>
            <ta e="T54" id="Seg_8612" s="T53">n</ta>
            <ta e="T55" id="Seg_8613" s="T54">post</ta>
            <ta e="T56" id="Seg_8614" s="T55">v</ta>
            <ta e="T57" id="Seg_8615" s="T56">aux</ta>
            <ta e="T58" id="Seg_8616" s="T57">n</ta>
            <ta e="T59" id="Seg_8617" s="T58">n</ta>
            <ta e="T60" id="Seg_8618" s="T59">propr</ta>
            <ta e="T61" id="Seg_8619" s="T60">n</ta>
            <ta e="T62" id="Seg_8620" s="T61">v</ta>
            <ta e="T63" id="Seg_8621" s="T62">n</ta>
            <ta e="T64" id="Seg_8622" s="T63">n</ta>
            <ta e="T65" id="Seg_8623" s="T64">pers</ta>
            <ta e="T66" id="Seg_8624" s="T65">post</ta>
            <ta e="T67" id="Seg_8625" s="T66">adj</ta>
            <ta e="T68" id="Seg_8626" s="T67">n</ta>
            <ta e="T69" id="Seg_8627" s="T68">n</ta>
            <ta e="T72" id="Seg_8628" s="T71">n</ta>
            <ta e="T73" id="Seg_8629" s="T72">v</ta>
            <ta e="T74" id="Seg_8630" s="T73">n</ta>
            <ta e="T75" id="Seg_8631" s="T74">n</ta>
            <ta e="T76" id="Seg_8632" s="T75">v</ta>
            <ta e="T79" id="Seg_8633" s="T78">v</ta>
            <ta e="T80" id="Seg_8634" s="T79">n</ta>
            <ta e="T81" id="Seg_8635" s="T80">n</ta>
            <ta e="T82" id="Seg_8636" s="T81">adv</ta>
            <ta e="T83" id="Seg_8637" s="T82">n</ta>
            <ta e="T84" id="Seg_8638" s="T83">n</ta>
            <ta e="T85" id="Seg_8639" s="T84">adj</ta>
            <ta e="T86" id="Seg_8640" s="T85">n</ta>
            <ta e="T87" id="Seg_8641" s="T86">v</ta>
            <ta e="T88" id="Seg_8642" s="T87">aux</ta>
            <ta e="T89" id="Seg_8643" s="T88">v</ta>
            <ta e="T90" id="Seg_8644" s="T89">que</ta>
            <ta e="T91" id="Seg_8645" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_8646" s="T91">pers</ta>
            <ta e="T93" id="Seg_8647" s="T92">v</ta>
            <ta e="T94" id="Seg_8648" s="T93">n</ta>
            <ta e="T95" id="Seg_8649" s="T94">adj</ta>
            <ta e="T96" id="Seg_8650" s="T95">n</ta>
            <ta e="T97" id="Seg_8651" s="T96">v</ta>
            <ta e="T98" id="Seg_8652" s="T97">dempro</ta>
            <ta e="T99" id="Seg_8653" s="T98">post</ta>
            <ta e="T100" id="Seg_8654" s="T99">v</ta>
            <ta e="T101" id="Seg_8655" s="T100">adj</ta>
            <ta e="T102" id="Seg_8656" s="T101">n</ta>
            <ta e="T103" id="Seg_8657" s="T102">v</ta>
            <ta e="T104" id="Seg_8658" s="T103">v</ta>
            <ta e="T105" id="Seg_8659" s="T104">ptcl</ta>
            <ta e="T106" id="Seg_8660" s="T105">aux</ta>
            <ta e="T107" id="Seg_8661" s="T106">n</ta>
            <ta e="T108" id="Seg_8662" s="T107">n</ta>
            <ta e="T109" id="Seg_8663" s="T108">pers</ta>
            <ta e="T110" id="Seg_8664" s="T109">adv</ta>
            <ta e="T111" id="Seg_8665" s="T110">n</ta>
            <ta e="T112" id="Seg_8666" s="T111">v</ta>
            <ta e="T113" id="Seg_8667" s="T112">adv</ta>
            <ta e="T114" id="Seg_8668" s="T113">adj</ta>
            <ta e="T115" id="Seg_8669" s="T114">adj</ta>
            <ta e="T116" id="Seg_8670" s="T115">n</ta>
            <ta e="T117" id="Seg_8671" s="T116">adj</ta>
            <ta e="T118" id="Seg_8672" s="T117">n</ta>
            <ta e="T119" id="Seg_8673" s="T118">n</ta>
            <ta e="T120" id="Seg_8674" s="T119">adj</ta>
            <ta e="T121" id="Seg_8675" s="T120">n</ta>
            <ta e="T122" id="Seg_8676" s="T121">n</ta>
            <ta e="T123" id="Seg_8677" s="T122">n</ta>
            <ta e="T124" id="Seg_8678" s="T123">ptcl</ta>
            <ta e="T125" id="Seg_8679" s="T124">n</ta>
            <ta e="T126" id="Seg_8680" s="T125">n</ta>
            <ta e="T127" id="Seg_8681" s="T126">post</ta>
            <ta e="T128" id="Seg_8682" s="T127">n</ta>
            <ta e="T129" id="Seg_8683" s="T128">v</ta>
            <ta e="T130" id="Seg_8684" s="T129">n</ta>
            <ta e="T131" id="Seg_8685" s="T130">n</ta>
            <ta e="T132" id="Seg_8686" s="T131">n</ta>
            <ta e="T133" id="Seg_8687" s="T132">n</ta>
            <ta e="T134" id="Seg_8688" s="T133">n</ta>
            <ta e="T135" id="Seg_8689" s="T134">post</ta>
            <ta e="T136" id="Seg_8690" s="T135">n</ta>
            <ta e="T137" id="Seg_8691" s="T136">v</ta>
            <ta e="T138" id="Seg_8692" s="T137">aux</ta>
            <ta e="T139" id="Seg_8693" s="T138">n</ta>
            <ta e="T140" id="Seg_8694" s="T139">n</ta>
            <ta e="T141" id="Seg_8695" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_8696" s="T141">n</ta>
            <ta e="T143" id="Seg_8697" s="T142">v</ta>
            <ta e="T144" id="Seg_8698" s="T143">n</ta>
            <ta e="T145" id="Seg_8699" s="T144">n</ta>
            <ta e="T146" id="Seg_8700" s="T145">post</ta>
            <ta e="T147" id="Seg_8701" s="T146">n</ta>
            <ta e="T148" id="Seg_8702" s="T147">adj</ta>
            <ta e="T149" id="Seg_8703" s="T148">n</ta>
            <ta e="T150" id="Seg_8704" s="T149">adv</ta>
            <ta e="T151" id="Seg_8705" s="T150">adj</ta>
            <ta e="T152" id="Seg_8706" s="T151">dempro</ta>
            <ta e="T153" id="Seg_8707" s="T152">post</ta>
            <ta e="T154" id="Seg_8708" s="T153">pers</ta>
            <ta e="T155" id="Seg_8709" s="T154">adv</ta>
            <ta e="T156" id="Seg_8710" s="T155">v</ta>
            <ta e="T157" id="Seg_8711" s="T156">propr</ta>
            <ta e="T158" id="Seg_8712" s="T157">adv</ta>
            <ta e="T159" id="Seg_8713" s="T158">v</ta>
            <ta e="T160" id="Seg_8714" s="T159">aux</ta>
            <ta e="T161" id="Seg_8715" s="T160">n</ta>
            <ta e="T162" id="Seg_8716" s="T161">n</ta>
            <ta e="T163" id="Seg_8717" s="T162">v</ta>
            <ta e="T164" id="Seg_8718" s="T163">ptcl</ta>
            <ta e="T165" id="Seg_8719" s="T164">dempro</ta>
            <ta e="T166" id="Seg_8720" s="T165">n</ta>
            <ta e="T167" id="Seg_8721" s="T166">v</ta>
            <ta e="T168" id="Seg_8722" s="T167">v</ta>
            <ta e="T169" id="Seg_8723" s="T168">adv</ta>
            <ta e="T170" id="Seg_8724" s="T169">ptcl</ta>
            <ta e="T171" id="Seg_8725" s="T170">v</ta>
            <ta e="T172" id="Seg_8726" s="T171">aux</ta>
            <ta e="T173" id="Seg_8727" s="T172">n</ta>
            <ta e="T174" id="Seg_8728" s="T173">que</ta>
            <ta e="T175" id="Seg_8729" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_8730" s="T175">dempro</ta>
            <ta e="T177" id="Seg_8731" s="T176">n</ta>
            <ta e="T178" id="Seg_8732" s="T177">v</ta>
            <ta e="T179" id="Seg_8733" s="T178">v</ta>
            <ta e="T180" id="Seg_8734" s="T179">n</ta>
            <ta e="T181" id="Seg_8735" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_8736" s="T181">v</ta>
            <ta e="T183" id="Seg_8737" s="T182">que</ta>
            <ta e="T184" id="Seg_8738" s="T183">ptcl</ta>
            <ta e="T185" id="Seg_8739" s="T184">n</ta>
            <ta e="T186" id="Seg_8740" s="T185">cop</ta>
            <ta e="T187" id="Seg_8741" s="T186">v</ta>
            <ta e="T188" id="Seg_8742" s="T187">dempro</ta>
            <ta e="T189" id="Seg_8743" s="T188">post</ta>
            <ta e="T190" id="Seg_8744" s="T189">que</ta>
            <ta e="T191" id="Seg_8745" s="T190">n</ta>
            <ta e="T192" id="Seg_8746" s="T191">v</ta>
            <ta e="T195" id="Seg_8747" s="T194">que</ta>
            <ta e="T196" id="Seg_8748" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_8749" s="T196">n</ta>
            <ta e="T198" id="Seg_8750" s="T197">v</ta>
            <ta e="T199" id="Seg_8751" s="T198">n</ta>
            <ta e="T200" id="Seg_8752" s="T199">v</ta>
            <ta e="T201" id="Seg_8753" s="T200">v</ta>
            <ta e="T202" id="Seg_8754" s="T201">n</ta>
            <ta e="T203" id="Seg_8755" s="T202">adj</ta>
            <ta e="T204" id="Seg_8756" s="T203">n</ta>
            <ta e="T205" id="Seg_8757" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_8758" s="T205">n</ta>
            <ta e="T207" id="Seg_8759" s="T206">adv</ta>
            <ta e="T208" id="Seg_8760" s="T207">adv</ta>
            <ta e="T209" id="Seg_8761" s="T208">adj</ta>
            <ta e="T210" id="Seg_8762" s="T209">v</ta>
            <ta e="T211" id="Seg_8763" s="T210">post</ta>
            <ta e="T212" id="Seg_8764" s="T211">adj</ta>
            <ta e="T213" id="Seg_8765" s="T212">v</ta>
            <ta e="T214" id="Seg_8766" s="T213">n</ta>
            <ta e="T215" id="Seg_8767" s="T214">ptcl</ta>
            <ta e="T216" id="Seg_8768" s="T215">n</ta>
            <ta e="T217" id="Seg_8769" s="T216">n</ta>
            <ta e="T218" id="Seg_8770" s="T217">adv</ta>
            <ta e="T219" id="Seg_8771" s="T218">adj</ta>
            <ta e="T220" id="Seg_8772" s="T219">adj</ta>
            <ta e="T221" id="Seg_8773" s="T220">n</ta>
            <ta e="T222" id="Seg_8774" s="T221">n</ta>
            <ta e="T223" id="Seg_8775" s="T222">adv</ta>
            <ta e="T224" id="Seg_8776" s="T223">v</ta>
            <ta e="T225" id="Seg_8777" s="T224">aux</ta>
            <ta e="T226" id="Seg_8778" s="T225">v</ta>
            <ta e="T227" id="Seg_8779" s="T226">cardnum</ta>
            <ta e="T228" id="Seg_8780" s="T227">n</ta>
            <ta e="T229" id="Seg_8781" s="T228">n</ta>
            <ta e="T230" id="Seg_8782" s="T229">que</ta>
            <ta e="T231" id="Seg_8783" s="T230">ptcl</ta>
            <ta e="T232" id="Seg_8784" s="T231">v</ta>
            <ta e="T233" id="Seg_8785" s="T232">adj</ta>
            <ta e="T234" id="Seg_8786" s="T233">n</ta>
            <ta e="T235" id="Seg_8787" s="T234">v</ta>
            <ta e="T236" id="Seg_8788" s="T235">dempro</ta>
            <ta e="T237" id="Seg_8789" s="T236">adj</ta>
            <ta e="T238" id="Seg_8790" s="T237">n</ta>
            <ta e="T239" id="Seg_8791" s="T238">interj</ta>
            <ta e="T240" id="Seg_8792" s="T239">interj</ta>
            <ta e="T241" id="Seg_8793" s="T240">v</ta>
            <ta e="T242" id="Seg_8794" s="T241">n</ta>
            <ta e="T243" id="Seg_8795" s="T242">v</ta>
            <ta e="T244" id="Seg_8796" s="T243">post</ta>
            <ta e="T245" id="Seg_8797" s="T244">adj</ta>
            <ta e="T246" id="Seg_8798" s="T245">adv</ta>
            <ta e="T247" id="Seg_8799" s="T246">n</ta>
            <ta e="T248" id="Seg_8800" s="T247">v</ta>
            <ta e="T249" id="Seg_8801" s="T248">adj</ta>
            <ta e="T250" id="Seg_8802" s="T249">n</ta>
            <ta e="T251" id="Seg_8803" s="T250">v</ta>
            <ta e="T252" id="Seg_8804" s="T251">post</ta>
            <ta e="T253" id="Seg_8805" s="T252">interj</ta>
            <ta e="T254" id="Seg_8806" s="T253">dempro</ta>
            <ta e="T255" id="Seg_8807" s="T254">que</ta>
            <ta e="T256" id="Seg_8808" s="T255">n</ta>
            <ta e="T257" id="Seg_8809" s="T256">cop</ta>
            <ta e="T258" id="Seg_8810" s="T257">v</ta>
            <ta e="T259" id="Seg_8811" s="T258">aux</ta>
            <ta e="T260" id="Seg_8812" s="T259">v</ta>
            <ta e="T261" id="Seg_8813" s="T260">propr</ta>
            <ta e="T262" id="Seg_8814" s="T261">n</ta>
            <ta e="T263" id="Seg_8815" s="T262">v</ta>
            <ta e="T264" id="Seg_8816" s="T263">pers</ta>
            <ta e="T265" id="Seg_8817" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_8818" s="T265">dempro</ta>
            <ta e="T267" id="Seg_8819" s="T266">v</ta>
            <ta e="T268" id="Seg_8820" s="T267">aux</ta>
            <ta e="T269" id="Seg_8821" s="T268">adj</ta>
            <ta e="T270" id="Seg_8822" s="T269">n</ta>
            <ta e="T271" id="Seg_8823" s="T270">adj</ta>
            <ta e="T272" id="Seg_8824" s="T271">cop</ta>
            <ta e="T273" id="Seg_8825" s="T272">emphpro</ta>
            <ta e="T274" id="Seg_8826" s="T273">emphpro</ta>
            <ta e="T275" id="Seg_8827" s="T274">post</ta>
            <ta e="T276" id="Seg_8828" s="T275">v</ta>
            <ta e="T277" id="Seg_8829" s="T276">ptcl</ta>
            <ta e="T278" id="Seg_8830" s="T277">ptcl</ta>
            <ta e="T279" id="Seg_8831" s="T278">v</ta>
            <ta e="T280" id="Seg_8832" s="T279">adj</ta>
            <ta e="T281" id="Seg_8833" s="T280">n</ta>
            <ta e="T282" id="Seg_8834" s="T281">adj</ta>
            <ta e="T283" id="Seg_8835" s="T282">adj</ta>
            <ta e="T284" id="Seg_8836" s="T283">n</ta>
            <ta e="T285" id="Seg_8837" s="T284">que</ta>
            <ta e="T286" id="Seg_8838" s="T285">ptcl</ta>
            <ta e="T287" id="Seg_8839" s="T286">adj</ta>
            <ta e="T288" id="Seg_8840" s="T287">n</ta>
            <ta e="T289" id="Seg_8841" s="T288">v</ta>
            <ta e="T290" id="Seg_8842" s="T289">aux</ta>
            <ta e="T291" id="Seg_8843" s="T290">adj</ta>
            <ta e="T292" id="Seg_8844" s="T291">adj</ta>
            <ta e="T293" id="Seg_8845" s="T292">n</ta>
            <ta e="T294" id="Seg_8846" s="T293">adj</ta>
            <ta e="T295" id="Seg_8847" s="T294">n</ta>
            <ta e="T296" id="Seg_8848" s="T295">v</ta>
            <ta e="T297" id="Seg_8849" s="T296">aux</ta>
            <ta e="T298" id="Seg_8850" s="T297">v</ta>
            <ta e="T299" id="Seg_8851" s="T298">dempro</ta>
            <ta e="T300" id="Seg_8852" s="T299">v</ta>
            <ta e="T301" id="Seg_8853" s="T300">aux</ta>
            <ta e="T302" id="Seg_8854" s="T301">dempro</ta>
            <ta e="T303" id="Seg_8855" s="T302">n</ta>
            <ta e="T304" id="Seg_8856" s="T303">post</ta>
            <ta e="T305" id="Seg_8857" s="T304">v</ta>
            <ta e="T306" id="Seg_8858" s="T305">interj</ta>
            <ta e="T307" id="Seg_8859" s="T306">v</ta>
            <ta e="T308" id="Seg_8860" s="T307">v</ta>
            <ta e="T309" id="Seg_8861" s="T308">propr</ta>
            <ta e="T310" id="Seg_8862" s="T309">adj</ta>
            <ta e="T311" id="Seg_8863" s="T310">n</ta>
            <ta e="T312" id="Seg_8864" s="T311">n</ta>
            <ta e="T313" id="Seg_8865" s="T312">adj</ta>
            <ta e="T314" id="Seg_8866" s="T313">n</ta>
            <ta e="T315" id="Seg_8867" s="T314">v</ta>
            <ta e="T316" id="Seg_8868" s="T315">n</ta>
            <ta e="T317" id="Seg_8869" s="T316">v</ta>
            <ta e="T318" id="Seg_8870" s="T317">v</ta>
            <ta e="T319" id="Seg_8871" s="T318">n</ta>
            <ta e="T320" id="Seg_8872" s="T319">n</ta>
            <ta e="T321" id="Seg_8873" s="T320">n</ta>
            <ta e="T322" id="Seg_8874" s="T321">n</ta>
            <ta e="T323" id="Seg_8875" s="T322">post</ta>
            <ta e="T324" id="Seg_8876" s="T323">dempro</ta>
            <ta e="T325" id="Seg_8877" s="T324">ptcl</ta>
            <ta e="T326" id="Seg_8878" s="T325">cardnum</ta>
            <ta e="T327" id="Seg_8879" s="T326">ptcl</ta>
            <ta e="T328" id="Seg_8880" s="T327">adj</ta>
            <ta e="T329" id="Seg_8881" s="T328">n</ta>
            <ta e="T330" id="Seg_8882" s="T329">dempro</ta>
            <ta e="T331" id="Seg_8883" s="T330">cop</ta>
            <ta e="T332" id="Seg_8884" s="T331">posspr</ta>
            <ta e="T333" id="Seg_8885" s="T332">adj</ta>
            <ta e="T334" id="Seg_8886" s="T333">adv</ta>
            <ta e="T335" id="Seg_8887" s="T334">v</ta>
            <ta e="T336" id="Seg_8888" s="T335">aux</ta>
            <ta e="T337" id="Seg_8889" s="T336">adv</ta>
            <ta e="T338" id="Seg_8890" s="T337">v</ta>
            <ta e="T339" id="Seg_8891" s="T338">adv</ta>
            <ta e="T340" id="Seg_8892" s="T339">v</ta>
            <ta e="T341" id="Seg_8893" s="T340">propr</ta>
            <ta e="T342" id="Seg_8894" s="T341">emphpro</ta>
            <ta e="T343" id="Seg_8895" s="T342">n</ta>
            <ta e="T344" id="Seg_8896" s="T343">v</ta>
            <ta e="T345" id="Seg_8897" s="T344">cardnum</ta>
            <ta e="T346" id="Seg_8898" s="T345">v</ta>
            <ta e="T347" id="Seg_8899" s="T346">v</ta>
            <ta e="T348" id="Seg_8900" s="T347">n</ta>
            <ta e="T349" id="Seg_8901" s="T348">v</ta>
            <ta e="T350" id="Seg_8902" s="T349">aux</ta>
            <ta e="T351" id="Seg_8903" s="T350">v</ta>
            <ta e="T352" id="Seg_8904" s="T351">n</ta>
            <ta e="T353" id="Seg_8905" s="T352">n</ta>
            <ta e="T354" id="Seg_8906" s="T353">n</ta>
            <ta e="T355" id="Seg_8907" s="T354">v</ta>
            <ta e="T356" id="Seg_8908" s="T355">v</ta>
            <ta e="T357" id="Seg_8909" s="T356">n</ta>
            <ta e="T358" id="Seg_8910" s="T357">v</ta>
            <ta e="T359" id="Seg_8911" s="T358">v</ta>
            <ta e="T360" id="Seg_8912" s="T359">adj</ta>
            <ta e="T361" id="Seg_8913" s="T360">n</ta>
            <ta e="T362" id="Seg_8914" s="T361">v</ta>
            <ta e="T363" id="Seg_8915" s="T362">n</ta>
            <ta e="T364" id="Seg_8916" s="T363">post</ta>
            <ta e="T365" id="Seg_8917" s="T364">n</ta>
            <ta e="T366" id="Seg_8918" s="T365">v</ta>
            <ta e="T367" id="Seg_8919" s="T366">n</ta>
            <ta e="T368" id="Seg_8920" s="T367">v</ta>
            <ta e="T369" id="Seg_8921" s="T368">v</ta>
            <ta e="T370" id="Seg_8922" s="T369">n</ta>
            <ta e="T371" id="Seg_8923" s="T370">n</ta>
            <ta e="T372" id="Seg_8924" s="T371">que</ta>
            <ta e="T373" id="Seg_8925" s="T372">ptcl</ta>
            <ta e="T374" id="Seg_8926" s="T373">que</ta>
            <ta e="T375" id="Seg_8927" s="T374">ptcl</ta>
            <ta e="T376" id="Seg_8928" s="T375">n</ta>
            <ta e="T377" id="Seg_8929" s="T376">v</ta>
            <ta e="T378" id="Seg_8930" s="T377">n</ta>
            <ta e="T379" id="Seg_8931" s="T378">n</ta>
            <ta e="T380" id="Seg_8932" s="T379">n</ta>
            <ta e="T381" id="Seg_8933" s="T380">post</ta>
            <ta e="T382" id="Seg_8934" s="T381">n</ta>
            <ta e="T383" id="Seg_8935" s="T382">adj</ta>
            <ta e="T384" id="Seg_8936" s="T383">n</ta>
            <ta e="T385" id="Seg_8937" s="T384">v</ta>
            <ta e="T386" id="Seg_8938" s="T385">n</ta>
            <ta e="T387" id="Seg_8939" s="T386">post</ta>
            <ta e="T388" id="Seg_8940" s="T387">v</ta>
            <ta e="T389" id="Seg_8941" s="T388">aux</ta>
            <ta e="T390" id="Seg_8942" s="T389">indfpro</ta>
            <ta e="T391" id="Seg_8943" s="T390">n</ta>
            <ta e="T392" id="Seg_8944" s="T391">dempro</ta>
            <ta e="T393" id="Seg_8945" s="T392">v</ta>
            <ta e="T394" id="Seg_8946" s="T393">cardnum</ta>
            <ta e="T395" id="Seg_8947" s="T394">n</ta>
            <ta e="T396" id="Seg_8948" s="T395">v</ta>
            <ta e="T397" id="Seg_8949" s="T396">n</ta>
            <ta e="T398" id="Seg_8950" s="T397">adj</ta>
            <ta e="T399" id="Seg_8951" s="T398">n</ta>
            <ta e="T400" id="Seg_8952" s="T399">v</ta>
            <ta e="T401" id="Seg_8953" s="T400">aux</ta>
            <ta e="T402" id="Seg_8954" s="T401">propr</ta>
            <ta e="T403" id="Seg_8955" s="T402">que</ta>
            <ta e="T404" id="Seg_8956" s="T403">ptcl</ta>
            <ta e="T405" id="Seg_8957" s="T404">v</ta>
            <ta e="T406" id="Seg_8958" s="T405">v</ta>
            <ta e="T407" id="Seg_8959" s="T406">adj</ta>
            <ta e="T408" id="Seg_8960" s="T407">propr</ta>
            <ta e="T409" id="Seg_8961" s="T408">post</ta>
            <ta e="T410" id="Seg_8962" s="T409">n</ta>
            <ta e="T411" id="Seg_8963" s="T410">v</ta>
            <ta e="T412" id="Seg_8964" s="T411">adj</ta>
            <ta e="T413" id="Seg_8965" s="T412">n</ta>
            <ta e="T414" id="Seg_8966" s="T413">adj</ta>
            <ta e="T415" id="Seg_8967" s="T414">adj</ta>
            <ta e="T416" id="Seg_8968" s="T415">adj</ta>
            <ta e="T417" id="Seg_8969" s="T416">n</ta>
            <ta e="T418" id="Seg_8970" s="T417">interj</ta>
            <ta e="T419" id="Seg_8971" s="T418">v</ta>
            <ta e="T420" id="Seg_8972" s="T419">v</ta>
            <ta e="T421" id="Seg_8973" s="T420">propr</ta>
            <ta e="T422" id="Seg_8974" s="T421">n</ta>
            <ta e="T423" id="Seg_8975" s="T422">post</ta>
            <ta e="T424" id="Seg_8976" s="T423">v</ta>
            <ta e="T425" id="Seg_8977" s="T424">v</ta>
            <ta e="T426" id="Seg_8978" s="T425">aux</ta>
            <ta e="T427" id="Seg_8979" s="T426">n</ta>
            <ta e="T428" id="Seg_8980" s="T427">post</ta>
            <ta e="T429" id="Seg_8981" s="T428">que</ta>
            <ta e="T430" id="Seg_8982" s="T429">ptcl</ta>
            <ta e="T431" id="Seg_8983" s="T430">v</ta>
            <ta e="T432" id="Seg_8984" s="T431">n</ta>
            <ta e="T433" id="Seg_8985" s="T432">cop</ta>
            <ta e="T434" id="Seg_8986" s="T433">v</ta>
            <ta e="T435" id="Seg_8987" s="T434">v</ta>
            <ta e="T436" id="Seg_8988" s="T435">adj</ta>
            <ta e="T437" id="Seg_8989" s="T436">cop</ta>
            <ta e="T438" id="Seg_8990" s="T437">n</ta>
            <ta e="T439" id="Seg_8991" s="T438">v</ta>
            <ta e="T440" id="Seg_8992" s="T439">pers</ta>
            <ta e="T441" id="Seg_8993" s="T440">propr</ta>
            <ta e="T442" id="Seg_8994" s="T441">n</ta>
            <ta e="T443" id="Seg_8995" s="T442">v</ta>
            <ta e="T444" id="Seg_8996" s="T443">v</ta>
            <ta e="T445" id="Seg_8997" s="T444">n</ta>
            <ta e="T446" id="Seg_8998" s="T445">ptcl</ta>
            <ta e="T447" id="Seg_8999" s="T446">v</ta>
            <ta e="T448" id="Seg_9000" s="T447">v</ta>
            <ta e="T449" id="Seg_9001" s="T448">que</ta>
            <ta e="T450" id="Seg_9002" s="T449">v</ta>
            <ta e="T451" id="Seg_9003" s="T450">v</ta>
            <ta e="T452" id="Seg_9004" s="T451">post</ta>
            <ta e="T453" id="Seg_9005" s="T452">propr</ta>
            <ta e="T454" id="Seg_9006" s="T453">v</ta>
            <ta e="T455" id="Seg_9007" s="T454">v</ta>
            <ta e="T456" id="Seg_9008" s="T455">adj</ta>
            <ta e="T457" id="Seg_9009" s="T456">n</ta>
            <ta e="T458" id="Seg_9010" s="T457">n</ta>
            <ta e="T459" id="Seg_9011" s="T458">ptcl</ta>
            <ta e="T460" id="Seg_9012" s="T459">n</ta>
            <ta e="T461" id="Seg_9013" s="T460">post</ta>
            <ta e="T462" id="Seg_9014" s="T461">v</ta>
            <ta e="T463" id="Seg_9015" s="T462">interj</ta>
            <ta e="T464" id="Seg_9016" s="T463">adv</ta>
            <ta e="T465" id="Seg_9017" s="T464">v</ta>
            <ta e="T466" id="Seg_9018" s="T465">aux</ta>
            <ta e="T467" id="Seg_9019" s="T466">adv</ta>
            <ta e="T468" id="Seg_9020" s="T467">v</ta>
            <ta e="T469" id="Seg_9021" s="T468">aux</ta>
            <ta e="T470" id="Seg_9022" s="T469">n</ta>
            <ta e="T471" id="Seg_9023" s="T470">n</ta>
            <ta e="T472" id="Seg_9024" s="T471">cop</ta>
            <ta e="T473" id="Seg_9025" s="T472">que</ta>
            <ta e="T474" id="Seg_9026" s="T473">ptcl</ta>
            <ta e="T475" id="Seg_9027" s="T474">v</ta>
            <ta e="T476" id="Seg_9028" s="T475">n</ta>
            <ta e="T477" id="Seg_9029" s="T476">adj</ta>
            <ta e="T478" id="Seg_9030" s="T477">n</ta>
            <ta e="T479" id="Seg_9031" s="T478">v</ta>
            <ta e="T480" id="Seg_9032" s="T479">aux</ta>
            <ta e="T482" id="Seg_9033" s="T481">adj</ta>
            <ta e="T483" id="Seg_9034" s="T482">n</ta>
            <ta e="T484" id="Seg_9035" s="T483">v</ta>
            <ta e="T485" id="Seg_9036" s="T484">que</ta>
            <ta e="T486" id="Seg_9037" s="T485">ptcl</ta>
            <ta e="T487" id="Seg_9038" s="T486">v</ta>
            <ta e="T488" id="Seg_9039" s="T487">adv</ta>
            <ta e="T489" id="Seg_9040" s="T488">adv</ta>
            <ta e="T490" id="Seg_9041" s="T489">adj</ta>
            <ta e="T491" id="Seg_9042" s="T490">n</ta>
            <ta e="T492" id="Seg_9043" s="T491">v</ta>
            <ta e="T494" id="Seg_9044" s="T492">adv</ta>
            <ta e="T495" id="Seg_9045" s="T494">n</ta>
            <ta e="T496" id="Seg_9046" s="T495">v</ta>
            <ta e="T497" id="Seg_9047" s="T496">adv</ta>
            <ta e="T498" id="Seg_9048" s="T497">pers</ta>
            <ta e="T499" id="Seg_9049" s="T498">n</ta>
            <ta e="T500" id="Seg_9050" s="T499">v</ta>
            <ta e="T501" id="Seg_9051" s="T500">adv</ta>
            <ta e="T502" id="Seg_9052" s="T501">v</ta>
            <ta e="T503" id="Seg_9053" s="T502">n</ta>
            <ta e="T504" id="Seg_9054" s="T503">v</ta>
            <ta e="T505" id="Seg_9055" s="T504">post</ta>
            <ta e="T506" id="Seg_9056" s="T505">adj</ta>
            <ta e="T507" id="Seg_9057" s="T506">adv</ta>
            <ta e="T508" id="Seg_9058" s="T507">v</ta>
            <ta e="T509" id="Seg_9059" s="T508">n</ta>
            <ta e="T510" id="Seg_9060" s="T509">v</ta>
            <ta e="T511" id="Seg_9061" s="T510">n</ta>
            <ta e="T512" id="Seg_9062" s="T511">adv</ta>
            <ta e="T513" id="Seg_9063" s="T512">v</ta>
            <ta e="T514" id="Seg_9064" s="T513">v</ta>
            <ta e="T515" id="Seg_9065" s="T514">v</ta>
            <ta e="T516" id="Seg_9066" s="T515">adj</ta>
            <ta e="T517" id="Seg_9067" s="T516">v</ta>
            <ta e="T518" id="Seg_9068" s="T517">n</ta>
            <ta e="T519" id="Seg_9069" s="T518">adv</ta>
            <ta e="T521" id="Seg_9070" s="T520">adj</ta>
            <ta e="T522" id="Seg_9071" s="T521">v</ta>
            <ta e="T523" id="Seg_9072" s="T522">adj</ta>
            <ta e="T524" id="Seg_9073" s="T523">n</ta>
            <ta e="T525" id="Seg_9074" s="T524">n</ta>
            <ta e="T526" id="Seg_9075" s="T525">v</ta>
            <ta e="T527" id="Seg_9076" s="T526">adj</ta>
            <ta e="T528" id="Seg_9077" s="T527">v</ta>
            <ta e="T529" id="Seg_9078" s="T528">n</ta>
            <ta e="T530" id="Seg_9079" s="T529">n</ta>
            <ta e="T531" id="Seg_9080" s="T530">v</ta>
            <ta e="T532" id="Seg_9081" s="T531">que</ta>
            <ta e="T533" id="Seg_9082" s="T532">n</ta>
            <ta e="T534" id="Seg_9083" s="T533">n</ta>
            <ta e="T535" id="Seg_9084" s="T534">v</ta>
            <ta e="T536" id="Seg_9085" s="T535">adv</ta>
            <ta e="T537" id="Seg_9086" s="T536">cop</ta>
            <ta e="T538" id="Seg_9087" s="T537">n</ta>
            <ta e="T539" id="Seg_9088" s="T538">v</ta>
            <ta e="T540" id="Seg_9089" s="T539">propr</ta>
            <ta e="T541" id="Seg_9090" s="T540">ptcl</ta>
            <ta e="T542" id="Seg_9091" s="T541">v</ta>
            <ta e="T543" id="Seg_9092" s="T542">v</ta>
            <ta e="T544" id="Seg_9093" s="T543">n</ta>
            <ta e="T545" id="Seg_9094" s="T544">v</ta>
            <ta e="T546" id="Seg_9095" s="T545">v</ta>
            <ta e="T547" id="Seg_9096" s="T546">n</ta>
            <ta e="T548" id="Seg_9097" s="T547">v</ta>
            <ta e="T549" id="Seg_9098" s="T548">n</ta>
            <ta e="T550" id="Seg_9099" s="T549">v</ta>
            <ta e="T551" id="Seg_9100" s="T550">v</ta>
            <ta e="T552" id="Seg_9101" s="T551">v</ta>
            <ta e="T553" id="Seg_9102" s="T552">que</ta>
            <ta e="T554" id="Seg_9103" s="T553">v</ta>
            <ta e="T555" id="Seg_9104" s="T554">adj</ta>
            <ta e="T556" id="Seg_9105" s="T555">pers</ta>
            <ta e="T557" id="Seg_9106" s="T556">interj</ta>
            <ta e="T558" id="Seg_9107" s="T557">dempro</ta>
            <ta e="T559" id="Seg_9108" s="T558">conj</ta>
            <ta e="T560" id="Seg_9109" s="T559">cop</ta>
            <ta e="T561" id="Seg_9110" s="T560">que</ta>
            <ta e="T562" id="Seg_9111" s="T561">pers</ta>
            <ta e="T563" id="Seg_9112" s="T562">v</ta>
            <ta e="T564" id="Seg_9113" s="T563">adv</ta>
            <ta e="T565" id="Seg_9114" s="T564">distrnum</ta>
            <ta e="T566" id="Seg_9115" s="T565">n</ta>
            <ta e="T567" id="Seg_9116" s="T566">distrnum</ta>
            <ta e="T568" id="Seg_9117" s="T567">n</ta>
            <ta e="T569" id="Seg_9118" s="T568">adj</ta>
            <ta e="T570" id="Seg_9119" s="T569">n</ta>
            <ta e="T571" id="Seg_9120" s="T570">v</ta>
            <ta e="T574" id="Seg_9121" s="T573">v</ta>
            <ta e="T575" id="Seg_9122" s="T574">n</ta>
            <ta e="T576" id="Seg_9123" s="T575">v</ta>
            <ta e="T577" id="Seg_9124" s="T576">v</ta>
            <ta e="T578" id="Seg_9125" s="T577">que</ta>
            <ta e="T579" id="Seg_9126" s="T578">adj</ta>
            <ta e="T580" id="Seg_9127" s="T579">n</ta>
            <ta e="T581" id="Seg_9128" s="T580">adj</ta>
            <ta e="T582" id="Seg_9129" s="T581">n</ta>
            <ta e="T583" id="Seg_9130" s="T582">adv</ta>
            <ta e="T584" id="Seg_9131" s="T583">v</ta>
            <ta e="T585" id="Seg_9132" s="T584">adj</ta>
            <ta e="T586" id="Seg_9133" s="T585">adj</ta>
            <ta e="T587" id="Seg_9134" s="T586">v</ta>
            <ta e="T588" id="Seg_9135" s="T587">aux</ta>
            <ta e="T589" id="Seg_9136" s="T588">propr</ta>
            <ta e="T590" id="Seg_9137" s="T589">v</ta>
            <ta e="T591" id="Seg_9138" s="T590">aux</ta>
            <ta e="T592" id="Seg_9139" s="T591">v</ta>
            <ta e="T593" id="Seg_9140" s="T592">v</ta>
            <ta e="T594" id="Seg_9141" s="T593">adj</ta>
            <ta e="T595" id="Seg_9142" s="T594">n</ta>
            <ta e="T596" id="Seg_9143" s="T595">n</ta>
            <ta e="T597" id="Seg_9144" s="T596">indfpro</ta>
            <ta e="T598" id="Seg_9145" s="T597">v</ta>
            <ta e="T599" id="Seg_9146" s="T598">v</ta>
            <ta e="T600" id="Seg_9147" s="T599">adv</ta>
            <ta e="T601" id="Seg_9148" s="T600">v</ta>
            <ta e="T602" id="Seg_9149" s="T601">n</ta>
            <ta e="T603" id="Seg_9150" s="T602">v</ta>
            <ta e="T604" id="Seg_9151" s="T603">adj</ta>
            <ta e="T605" id="Seg_9152" s="T604">n</ta>
            <ta e="T606" id="Seg_9153" s="T605">que</ta>
            <ta e="T607" id="Seg_9154" s="T606">ptcl</ta>
            <ta e="T608" id="Seg_9155" s="T607">cop</ta>
            <ta e="T609" id="Seg_9156" s="T608">post</ta>
            <ta e="T610" id="Seg_9157" s="T609">v</ta>
            <ta e="T611" id="Seg_9158" s="T610">n</ta>
            <ta e="T612" id="Seg_9159" s="T611">ptcl</ta>
            <ta e="T613" id="Seg_9160" s="T612">que</ta>
            <ta e="T614" id="Seg_9161" s="T613">ptcl</ta>
            <ta e="T615" id="Seg_9162" s="T614">v</ta>
            <ta e="T616" id="Seg_9163" s="T615">aux</ta>
            <ta e="T617" id="Seg_9164" s="T616">pers</ta>
            <ta e="T618" id="Seg_9165" s="T617">n</ta>
            <ta e="T619" id="Seg_9166" s="T618">v</ta>
            <ta e="T620" id="Seg_9167" s="T619">cardnum</ta>
            <ta e="T621" id="Seg_9168" s="T620">n</ta>
            <ta e="T622" id="Seg_9169" s="T621">v</ta>
            <ta e="T623" id="Seg_9170" s="T622">que</ta>
            <ta e="T624" id="Seg_9171" s="T623">pers</ta>
            <ta e="T625" id="Seg_9172" s="T624">v</ta>
            <ta e="T626" id="Seg_9173" s="T625">aux</ta>
            <ta e="T627" id="Seg_9174" s="T626">v</ta>
            <ta e="T628" id="Seg_9175" s="T627">n</ta>
            <ta e="T629" id="Seg_9176" s="T628">v</ta>
            <ta e="T630" id="Seg_9177" s="T629">n</ta>
            <ta e="T631" id="Seg_9178" s="T630">v</ta>
            <ta e="T634" id="Seg_9179" s="T633">adj</ta>
            <ta e="T635" id="Seg_9180" s="T634">ptcl</ta>
            <ta e="T636" id="Seg_9181" s="T635">n</ta>
            <ta e="T637" id="Seg_9182" s="T636">cardnum</ta>
            <ta e="T638" id="Seg_9183" s="T637">post</ta>
            <ta e="T639" id="Seg_9184" s="T638">n</ta>
            <ta e="T640" id="Seg_9185" s="T639">adj</ta>
            <ta e="T641" id="Seg_9186" s="T640">n</ta>
            <ta e="T642" id="Seg_9187" s="T641">v</ta>
            <ta e="T643" id="Seg_9188" s="T642">adj</ta>
            <ta e="T644" id="Seg_9189" s="T643">adj</ta>
            <ta e="T645" id="Seg_9190" s="T644">n</ta>
            <ta e="T646" id="Seg_9191" s="T645">n</ta>
            <ta e="T647" id="Seg_9192" s="T646">adj</ta>
            <ta e="T648" id="Seg_9193" s="T647">v</ta>
            <ta e="T649" id="Seg_9194" s="T648">n</ta>
            <ta e="T650" id="Seg_9195" s="T649">n</ta>
            <ta e="T651" id="Seg_9196" s="T650">n</ta>
            <ta e="T652" id="Seg_9197" s="T651">v</ta>
            <ta e="T653" id="Seg_9198" s="T652">v</ta>
            <ta e="T654" id="Seg_9199" s="T653">n</ta>
            <ta e="T655" id="Seg_9200" s="T654">v</ta>
            <ta e="T656" id="Seg_9201" s="T655">n</ta>
            <ta e="T657" id="Seg_9202" s="T656">ptcl</ta>
            <ta e="T658" id="Seg_9203" s="T657">v</ta>
            <ta e="T659" id="Seg_9204" s="T658">aux</ta>
            <ta e="T660" id="Seg_9205" s="T659">propr</ta>
            <ta e="T661" id="Seg_9206" s="T660">v</ta>
            <ta e="T662" id="Seg_9207" s="T661">v</ta>
            <ta e="T663" id="Seg_9208" s="T662">que</ta>
            <ta e="T664" id="Seg_9209" s="T663">ptcl</ta>
            <ta e="T665" id="Seg_9210" s="T664">n</ta>
            <ta e="T666" id="Seg_9211" s="T665">v</ta>
            <ta e="T667" id="Seg_9212" s="T666">n</ta>
            <ta e="T668" id="Seg_9213" s="T667">n</ta>
            <ta e="T669" id="Seg_9214" s="T668">propr</ta>
            <ta e="T670" id="Seg_9215" s="T669">v</ta>
            <ta e="T671" id="Seg_9216" s="T670">v</ta>
            <ta e="T672" id="Seg_9217" s="T671">aux</ta>
            <ta e="T673" id="Seg_9218" s="T672">n</ta>
            <ta e="T674" id="Seg_9219" s="T673">propr</ta>
            <ta e="T675" id="Seg_9220" s="T674">post</ta>
            <ta e="T676" id="Seg_9221" s="T675">v</ta>
            <ta e="T677" id="Seg_9222" s="T676">aux</ta>
            <ta e="T678" id="Seg_9223" s="T677">v</ta>
            <ta e="T679" id="Seg_9224" s="T678">adj</ta>
            <ta e="T680" id="Seg_9225" s="T679">n</ta>
            <ta e="T681" id="Seg_9226" s="T680">adj</ta>
            <ta e="T682" id="Seg_9227" s="T681">n</ta>
            <ta e="T683" id="Seg_9228" s="T682">pers</ta>
            <ta e="T684" id="Seg_9229" s="T683">adj</ta>
            <ta e="T685" id="Seg_9230" s="T684">n</ta>
            <ta e="T686" id="Seg_9231" s="T685">adv</ta>
            <ta e="T687" id="Seg_9232" s="T686">v</ta>
            <ta e="T688" id="Seg_9233" s="T687">n</ta>
            <ta e="T689" id="Seg_9234" s="T688">adj</ta>
            <ta e="T690" id="Seg_9235" s="T689">n</ta>
            <ta e="T691" id="Seg_9236" s="T690">ptcl</ta>
            <ta e="T692" id="Seg_9237" s="T691">adj</ta>
            <ta e="T693" id="Seg_9238" s="T692">adj</ta>
            <ta e="T694" id="Seg_9239" s="T693">n</ta>
            <ta e="T695" id="Seg_9240" s="T694">v</ta>
            <ta e="T696" id="Seg_9241" s="T695">adj</ta>
            <ta e="T697" id="Seg_9242" s="T696">n</ta>
            <ta e="T698" id="Seg_9243" s="T697">adv</ta>
            <ta e="T699" id="Seg_9244" s="T698">v</ta>
            <ta e="T700" id="Seg_9245" s="T699">n</ta>
            <ta e="T701" id="Seg_9246" s="T700">n</ta>
            <ta e="T702" id="Seg_9247" s="T701">adv</ta>
            <ta e="T703" id="Seg_9248" s="T702">adj</ta>
            <ta e="T705" id="Seg_9249" s="T704">v</ta>
            <ta e="T706" id="Seg_9250" s="T705">post</ta>
            <ta e="T707" id="Seg_9251" s="T706">pers</ta>
            <ta e="T708" id="Seg_9252" s="T707">adv</ta>
            <ta e="T709" id="Seg_9253" s="T708">adv</ta>
            <ta e="T710" id="Seg_9254" s="T709">dempro</ta>
            <ta e="T711" id="Seg_9255" s="T710">cop</ta>
            <ta e="T712" id="Seg_9256" s="T711">ptcl</ta>
            <ta e="T713" id="Seg_9257" s="T712">propr</ta>
            <ta e="T714" id="Seg_9258" s="T713">propr</ta>
            <ta e="T715" id="Seg_9259" s="T714">propr</ta>
            <ta e="T716" id="Seg_9260" s="T715">post</ta>
            <ta e="T717" id="Seg_9261" s="T716">v</ta>
            <ta e="T718" id="Seg_9262" s="T717">n</ta>
            <ta e="T719" id="Seg_9263" s="T718">v</ta>
            <ta e="T720" id="Seg_9264" s="T719">adj</ta>
            <ta e="T721" id="Seg_9265" s="T720">n</ta>
            <ta e="T722" id="Seg_9266" s="T721">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T10" id="Seg_9267" s="T9">RUS:cult</ta>
            <ta e="T11" id="Seg_9268" s="T10">EV:cult</ta>
            <ta e="T12" id="Seg_9269" s="T11">RUS:cult</ta>
            <ta e="T22" id="Seg_9270" s="T21">RUS:cult</ta>
            <ta e="T31" id="Seg_9271" s="T30">RUS:cult</ta>
            <ta e="T54" id="Seg_9272" s="T53">RUS:cult</ta>
            <ta e="T58" id="Seg_9273" s="T57">EV:cult</ta>
            <ta e="T60" id="Seg_9274" s="T59">RUS:cult</ta>
            <ta e="T63" id="Seg_9275" s="T62">RUS:cult</ta>
            <ta e="T68" id="Seg_9276" s="T67">RUS:cult</ta>
            <ta e="T69" id="Seg_9277" s="T68">EV:cult</ta>
            <ta e="T74" id="Seg_9278" s="T73">RUS:cult</ta>
            <ta e="T89" id="Seg_9279" s="T88">RUS:disc</ta>
            <ta e="T97" id="Seg_9280" s="T96">RUS:cult</ta>
            <ta e="T107" id="Seg_9281" s="T106">RUS:cult</ta>
            <ta e="T111" id="Seg_9282" s="T110">RUS:cult</ta>
            <ta e="T133" id="Seg_9283" s="T132">RUS:cult</ta>
            <ta e="T147" id="Seg_9284" s="T146">RUS:cult</ta>
            <ta e="T166" id="Seg_9285" s="T165">RUS:cult</ta>
            <ta e="T173" id="Seg_9286" s="T172">EV:core</ta>
            <ta e="T288" id="Seg_9287" s="T287">EV:gram (DIM)</ta>
            <ta e="T293" id="Seg_9288" s="T292">EV:gram (DIM)</ta>
            <ta e="T319" id="Seg_9289" s="T318">EV:core</ta>
            <ta e="T321" id="Seg_9290" s="T320">EV:cult</ta>
            <ta e="T333" id="Seg_9291" s="T332">RUS:core</ta>
            <ta e="T349" id="Seg_9292" s="T348">EV:cult</ta>
            <ta e="T365" id="Seg_9293" s="T364">EV:cult</ta>
            <ta e="T382" id="Seg_9294" s="T381">EV:cult</ta>
            <ta e="T391" id="Seg_9295" s="T390">RUS:cult</ta>
            <ta e="T413" id="Seg_9296" s="T412">EV:cult</ta>
            <ta e="T415" id="Seg_9297" s="T414">EV:core</ta>
            <ta e="T422" id="Seg_9298" s="T421">RUS:cult</ta>
            <ta e="T460" id="Seg_9299" s="T459">EV:core</ta>
            <ta e="T470" id="Seg_9300" s="T469">RUS:cult</ta>
            <ta e="T471" id="Seg_9301" s="T470">RUS:cult</ta>
            <ta e="T495" id="Seg_9302" s="T494">EV:core</ta>
            <ta e="T524" id="Seg_9303" s="T523">RUS:cult</ta>
            <ta e="T549" id="Seg_9304" s="T548">RUS:cult</ta>
            <ta e="T559" id="Seg_9305" s="T558">RUS:gram</ta>
            <ta e="T566" id="Seg_9306" s="T565">RUS:cult</ta>
            <ta e="T595" id="Seg_9307" s="T594">RUS:cult</ta>
            <ta e="T598" id="Seg_9308" s="T597">RUS:cult</ta>
            <ta e="T611" id="Seg_9309" s="T610">EV:core</ta>
            <ta e="T622" id="Seg_9310" s="T621">RUS:cult</ta>
            <ta e="T636" id="Seg_9311" s="T635">RUS:cult</ta>
            <ta e="T645" id="Seg_9312" s="T644">RUS:cult</ta>
            <ta e="T651" id="Seg_9313" s="T650">RUS:cult</ta>
            <ta e="T667" id="Seg_9314" s="T666">RUS:cult</ta>
            <ta e="T674" id="Seg_9315" s="T673">RUS:cult</ta>
            <ta e="T697" id="Seg_9316" s="T696">RUS:cult</ta>
            <ta e="T714" id="Seg_9317" s="T713">RUS:cult</ta>
            <ta e="T715" id="Seg_9318" s="T714">RUS:cult</ta>
            <ta e="T718" id="Seg_9319" s="T717">RUS:cult</ta>
            <ta e="T721" id="Seg_9320" s="T720">EV:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_9321" s="T0">Listen to the story "The wonderous reindeers".</ta>
            <ta e="T10" id="Seg_9322" s="T4">On the third day the reindeer caravan with the children reached Volochanka.</ta>
            <ta e="T26" id="Seg_9323" s="T10">The Russians and Dolgans of the central settlement of the rayon of Ust'-Avam - all had gathered to see the arrival of the children, who are dressed like dolls and who are very kind.</ta>
            <ta e="T42" id="Seg_9324" s="T26">The not very loud, quite life in Volochanka revived in one moment, as if the day of the revelation had come.</ta>
            <ta e="T46" id="Seg_9325" s="T42">But the children don't look at the people.</ta>
            <ta e="T53" id="Seg_9326" s="T46">They look only at the Russian houses.</ta>
            <ta e="T58" id="Seg_9327" s="T53">People ride along the street on riding reindeer.</ta>
            <ta e="T64" id="Seg_9328" s="T58">The grandfather, the old man Anisim is standing beneath the school.</ta>
            <ta e="T73" id="Seg_9329" s="T64">Around him they was a noise making army of riding reindeers, from the backs of which they jumped off.</ta>
            <ta e="T84" id="Seg_9330" s="T73">Beneath the school Pytor Kuzmich, the chiefs of the teachers and the workers of the boarding school are standing.</ta>
            <ta e="T93" id="Seg_9331" s="T84">After having caught the children, all greet them and say something to them.</ta>
            <ta e="T97" id="Seg_9332" s="T93">The children don't understand Russian.</ta>
            <ta e="T103" id="Seg_9333" s="T97">Therefore the small coming people don't talk.</ta>
            <ta e="T108" id="Seg_9334" s="T103">They just watch the teachers' faces.</ta>
            <ta e="T112" id="Seg_9335" s="T108">They saw something very interesting.</ta>
            <ta e="T122" id="Seg_9336" s="T112">Very big Russian houses, other people, Russians, with different languages and clothes.</ta>
            <ta e="T129" id="Seg_9337" s="T122">They had shoes on their feet, like reindeer hooves.</ta>
            <ta e="T138" id="Seg_9338" s="T129">The men wear caps on their heads, similar to the scoops of their mothers.</ta>
            <ta e="T141" id="Seg_9339" s="T138">The women don't have plaited hair.</ta>
            <ta e="T146" id="Seg_9340" s="T141">Their hair is cut like [the hair] of the men.</ta>
            <ta e="T156" id="Seg_9341" s="T146">Their clothes are sewed very tightly at the bottom, therefore they can hardly walk.</ta>
            <ta e="T161" id="Seg_9342" s="T156">Toyoo watched the women very (thoroughly).</ta>
            <ta e="T163" id="Seg_9343" s="T161">He thinks: </ta>
            <ta e="T172" id="Seg_9344" s="T163">"Probably they can't walk in such clothes, they probably just walk little."</ta>
            <ta e="T179" id="Seg_9345" s="T172">He wanted to tell his friends something about this.</ta>
            <ta e="T186" id="Seg_9346" s="T179">He just opened his mouth, some noise was heard there.</ta>
            <ta e="T192" id="Seg_9347" s="T186">He looked there, from where the noise came.</ta>
            <ta e="T194" id="Seg_9348" s="T192">Oh my God!</ta>
            <ta e="T201" id="Seg_9349" s="T194">A reindeer sledge, never seen by humans, is driving.</ta>
            <ta e="T207" id="Seg_9350" s="T201">The sledge is wonderous, but the reindeer are very big.</ta>
            <ta e="T212" id="Seg_9351" s="T207">They have a very long, waving tail.</ta>
            <ta e="T220" id="Seg_9352" s="T212">The sledge driver has a very hairy face, he has a furry beard.</ta>
            <ta e="T228" id="Seg_9353" s="T220">Going behing the reindeer he held two reins.</ta>
            <ta e="T235" id="Seg_9354" s="T228">The reindeer is moving its head with every step.</ta>
            <ta e="T238" id="Seg_9355" s="T235">And every time the sledge driver:</ta>
            <ta e="T244" id="Seg_9356" s="T238">"Noo, noo", he says, as if he would listen to a tale.</ta>
            <ta e="T252" id="Seg_9357" s="T244">He tried very strongly with his mouth, as if he was eating sweet fat.</ta>
            <ta e="T263" id="Seg_9358" s="T252">"Well, what's that for a reindeer?", the voice of Toyoo, who watched and was happy, was heard.</ta>
            <ta e="T268" id="Seg_9359" s="T263">Not only he was watching.</ta>
            <ta e="T272" id="Seg_9360" s="T268">All children were astonished.</ta>
            <ta e="T281" id="Seg_9361" s="T272">They didn't chat with each other yet, they again watched the new miracle.</ta>
            <ta e="T290" id="Seg_9362" s="T281">An old Russian woman was chasing some small reindeer.</ta>
            <ta e="T298" id="Seg_9363" s="T290">Naked small reindeer without antlers and with short legs were running and crying.</ta>
            <ta e="T305" id="Seg_9364" s="T298">Those, having run past, scattered here and there.</ta>
            <ta e="T311" id="Seg_9365" s="T305">"Jeez!", Toyoo shouted, "naked wild reindeer!"</ta>
            <ta e="T317" id="Seg_9366" s="T311">The children are laughing loudly and are pointing with their fingers.</ta>
            <ta e="T328" id="Seg_9367" s="T317">"Look, friend, their noses are like the rings on the driving pole, they just have two holes.</ta>
            <ta e="T333" id="Seg_9368" s="T328">That's no reindeer, ours are [more] beautiful."</ta>
            <ta e="T340" id="Seg_9369" s="T333">At first they were a bit afraid, then they came closer to have a good sight.</ta>
            <ta e="T344" id="Seg_9370" s="T340">Toyoo couldn't defeat his temper.</ta>
            <ta e="T351" id="Seg_9371" s="T344">He ran to one of them and mounted it, like a riding reindeer.</ta>
            <ta e="T364" id="Seg_9372" s="T351">The wild reindeer, feeling the human on its back, jumped off from the place, where it stood, and being frightened it ran forward at full power.</ta>
            <ta e="T371" id="Seg_9373" s="T364">The boy, not falling down from a riding reindeer, tried to grab the reindeer fur.</ta>
            <ta e="T377" id="Seg_9374" s="T371">In no way, nowhere his hands get caught.</ta>
            <ta e="T381" id="Seg_9375" s="T377">The hairs of the reindeer are like small nails.</ta>
            <ta e="T389" id="Seg_9376" s="T381">The riding reindeer shouted very loudly and ran forward.</ta>
            <ta e="T396" id="Seg_9377" s="T389">Some pigs, seeing this, ran off at once.</ta>
            <ta e="T401" id="Seg_9378" s="T396">Behind them an old woman was running.</ta>
            <ta e="T405" id="Seg_9379" s="T401">Toyoo shouted something…</ta>
            <ta e="T411" id="Seg_9380" s="T405">She runs towards Toyoo and swings her stick.</ta>
            <ta e="T420" id="Seg_9381" s="T411">The naked riding reindeer fell into a dirty, muddy and wet pit and made "splash".</ta>
            <ta e="T426" id="Seg_9382" s="T420">Toyoo tried to stand up with his pig in vain.</ta>
            <ta e="T433" id="Seg_9383" s="T426">Neither his face nor something else is visible, everything is full of clay.</ta>
            <ta e="T440" id="Seg_9384" s="T433">When he tried to stand up and was very tried, the old woman reached him.</ta>
            <ta e="T447" id="Seg_9385" s="T440">Having seen Toyoo's face, she burst into laughter and just waved with her hands.</ta>
            <ta e="T452" id="Seg_9386" s="T447">"Go away, where you came from!",as if she would say this.</ta>
            <ta e="T462" id="Seg_9387" s="T452">Toyoo jumped up and ran with the last of his strength and out of breath to his friends.</ta>
            <ta e="T469" id="Seg_9388" s="T462">"My God, I nearly died, I nearly died!"</ta>
            <ta e="T480" id="Seg_9389" s="T469">The teaches and the director burst into laughing with heart and soul.</ta>
            <ta e="T484" id="Seg_9390" s="T480">Pavel Kuzmich said what he was thinking: </ta>
            <ta e="T494" id="Seg_9391" s="T484">"What shall I say, apparently very agile people have arrived."</ta>
            <ta e="T496" id="Seg_9392" s="T494">He said to his colleagues: </ta>
            <ta e="T505" id="Seg_9393" s="T496">"Bring them now to the boarding school, wash them, until the food has prepared.</ta>
            <ta e="T510" id="Seg_9394" s="T505">Make everything thoroughly without frightening the children."</ta>
            <ta e="T521" id="Seg_9395" s="T510">They brought all children, they showed them all the living places, named "komnata" in Russian.</ta>
            <ta e="T526" id="Seg_9396" s="T521">They gave out patched, new shirts and shoes.</ta>
            <ta e="T539" id="Seg_9397" s="T526">They cut off the hair of everyone who had come, therefore the girls cried because they felt sorry for their hair.</ta>
            <ta e="T542" id="Seg_9398" s="T539">Also Toyoo got a haircut.</ta>
            <ta e="T548" id="Seg_9399" s="T542">They gave out clothes, they distributed shoes (lit. hooves).</ta>
            <ta e="T554" id="Seg_9400" s="T548">He came to his bed, sat down and looked at what they had distributed.</ta>
            <ta e="T557" id="Seg_9401" s="T554">Everything fits him.</ta>
            <ta e="T568" id="Seg_9402" s="T557">But why did they give shirts and trousers twice?</ta>
            <ta e="T574" id="Seg_9403" s="T568">He thought to put on the new clothes.</ta>
            <ta e="T577" id="Seg_9404" s="T574">They brought the children to wash themselves.</ta>
            <ta e="T588" id="Seg_9405" s="T577">Where the agile people are laughing out of joy: "Hot, hot!", they shouted.</ta>
            <ta e="T592" id="Seg_9406" s="T588">Having washed himself, Toyoo got dressed.</ta>
            <ta e="T596" id="Seg_9407" s="T592">He put on the white shirt and the white trousers.</ta>
            <ta e="T601" id="Seg_9408" s="T596">Some clothes he wrapped up like reserves, for dressing them later.</ta>
            <ta e="T610" id="Seg_9409" s="T601">Having put on the underwear, he went, as if nothing was.</ta>
            <ta e="T617" id="Seg_9410" s="T610">His friends didn't even say something to him.</ta>
            <ta e="T626" id="Seg_9411" s="T617">A woman, working in the boarding school, explained him how to dress.</ta>
            <ta e="T629" id="Seg_9412" s="T626">Mealtime came.</ta>
            <ta e="T639" id="Seg_9413" s="T629">The children were seated at both sides of a long table.</ta>
            <ta e="T645" id="Seg_9414" s="T639">On big plates tastily smelling breads are lying.</ta>
            <ta e="T650" id="Seg_9415" s="T645">They filled in fish soup for every child.</ta>
            <ta e="T652" id="Seg_9416" s="T650">They distributed spoons.</ta>
            <ta e="T659" id="Seg_9417" s="T652">He saw that the children were eating and that there necks got lost (?).</ta>
            <ta e="T670" id="Seg_9418" s="T659">While Toyoo was sitting and eating, some thoughts came up: He remembered his mother, his father, Kycha.</ta>
            <ta e="T682" id="Seg_9419" s="T670">Having finished eating the children went playing through Volochanka and watched all Russian houses and yards.</ta>
            <ta e="T688" id="Seg_9420" s="T682">For them all is wonderous, for the people, just having arrived.</ta>
            <ta e="T697" id="Seg_9421" s="T688">On the next day there came again a big caravan with children from another kolkhoz.</ta>
            <ta e="T706" id="Seg_9422" s="T697">The children, just having come, are very confused; as if they were afraid.</ta>
            <ta e="T711" id="Seg_9423" s="T706">They weren't so for a long time.</ta>
            <ta e="T722" id="Seg_9424" s="T711">Like Toyoo, Mitya and Ivan, who weren't ashamed anymore seeing new friends.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_9425" s="T0">Hört die Geschichte "Die wundersamen Rentiere".</ta>
            <ta e="T10" id="Seg_9426" s="T4">Am dritten Tag kam die Rentierkarawane mit den Kindern in Volochanka an.</ta>
            <ta e="T26" id="Seg_9427" s="T10">Die Russen und Dolganen der zentralen Siedlung des Rajons Ust'-Avam - alle hatten sich versammelt, um die Ankunft der Kinder zu sehen, die angezogen sind wie Puppen und sehr lieb sind.</ta>
            <ta e="T42" id="Seg_9428" s="T26">Das nicht sehr laute, ruhige Leben in Volochanka lebte in einem Augenblick auf, als ob der Tag der Offenbarung gekommen wäre.</ta>
            <ta e="T46" id="Seg_9429" s="T42">Die Kinder aber schauen die Leute nicht an.</ta>
            <ta e="T53" id="Seg_9430" s="T46">Sie haben nur Augen für die russischen Häuser.</ta>
            <ta e="T58" id="Seg_9431" s="T53">Auf Reitrentieren reiten Leute die Straße entlang.</ta>
            <ta e="T64" id="Seg_9432" s="T58">Der Großvater, der alte Mann Anisim stand neben der Schule.</ta>
            <ta e="T73" id="Seg_9433" s="T64">Um ihn herum war eine lärmende Armee an Reitrentieren, von deren Rücken sie heruntersprangen.</ta>
            <ta e="T84" id="Seg_9434" s="T73">Neben der Schule stehen Pjotr Kuzmič, die Chefs der Lehrer und Arbeiter des Internats.</ta>
            <ta e="T93" id="Seg_9435" s="T84">Alle, nachdem sie die Kinder eingefangen haben, begrüßen sie und sagen ihnen etwas.</ta>
            <ta e="T97" id="Seg_9436" s="T93">Die Kinder verstehen kein Russisch.</ta>
            <ta e="T103" id="Seg_9437" s="T97">Deshalb unterhalten sich die kleinen angekommenen Leute nicht.</ta>
            <ta e="T108" id="Seg_9438" s="T103">Sie betrachteten nur die Gesichter der Lehrer.</ta>
            <ta e="T112" id="Seg_9439" s="T108">Sie sahen etwas sehr Interessantes.</ta>
            <ta e="T122" id="Seg_9440" s="T112">Sehr große russische Häuser, andere Menschen, Russen, mit anderer Sprache und Kleidung.</ta>
            <ta e="T129" id="Seg_9441" s="T122">An den Füßen hatten sie Schuhe, ähnlich Rentierhufen.</ta>
            <ta e="T138" id="Seg_9442" s="T129">Die Männer tragen Mützen auf dem Kopf, ähnlich den Schöpfkellen ihrer Mütter.</ta>
            <ta e="T141" id="Seg_9443" s="T138">Die Frauen haben kein geflochtetenes Haar.</ta>
            <ta e="T146" id="Seg_9444" s="T141">Ihre Haare sind wie bei den Männern geschnitten.</ta>
            <ta e="T156" id="Seg_9445" s="T146">Ihre Kleider sind unten sehr eng genäht, deshalb können sie kaum gehen.</ta>
            <ta e="T161" id="Seg_9446" s="T156">Tojoo schaute sich die Frauen sehr (genau) an.</ta>
            <ta e="T163" id="Seg_9447" s="T161">Für sich denkt er: </ta>
            <ta e="T172" id="Seg_9448" s="T163">"Wahrscheinlich können sie in solchen Kleidern nicht laufen, sie laufen wohl nur wenig."</ta>
            <ta e="T179" id="Seg_9449" s="T172">Er wollte seinen Freunden etwas darüber erzählen.</ta>
            <ta e="T186" id="Seg_9450" s="T179">Er öffnete gerade seinen Mund, da war irgendein Lärm zu hören.</ta>
            <ta e="T192" id="Seg_9451" s="T186">Er schaute dorthin, wo der Lärm her kam.</ta>
            <ta e="T194" id="Seg_9452" s="T192">Oh mein Gott!</ta>
            <ta e="T201" id="Seg_9453" s="T194">Ein von Menschen noch nicht gesehenes, (ausgewähltes?) Rentiergespann fährt.</ta>
            <ta e="T207" id="Seg_9454" s="T201">Der Schlitten ist wundersam, die Rentiere aber sind sehr groß.</ta>
            <ta e="T212" id="Seg_9455" s="T207">Sie haben einen sehr langen, wippenden Schwanz.</ta>
            <ta e="T220" id="Seg_9456" s="T212">Der Schlittenlenker hat ein sehr behaartes Gesicht, er hat einen pelzigen Bart.</ta>
            <ta e="T228" id="Seg_9457" s="T220">Hinter dem Rentier hergehend hielt er zwei Zügel.</ta>
            <ta e="T235" id="Seg_9458" s="T228">Das Rentier bewegt mit jedem Schritt seinen Kopf.</ta>
            <ta e="T238" id="Seg_9459" s="T235">Und jedes Mal der Schlittenlenker:</ta>
            <ta e="T244" id="Seg_9460" s="T238">"Noo, noo", sagt er, als würde er einem Märchen zuhören.</ta>
            <ta e="T252" id="Seg_9461" s="T244">Er versuchte sehr kräftig mit seinem Mund, als äße er süßes Fett.</ta>
            <ta e="T263" id="Seg_9462" s="T252">"Nun, was ist das für ein Rentier?", erklang die Stimme von Tojoo, der schaute und sich freute.</ta>
            <ta e="T268" id="Seg_9463" s="T263">Nicht nur er schaute sich das an.</ta>
            <ta e="T272" id="Seg_9464" s="T268">Alle Kinder waren erstaunt.</ta>
            <ta e="T281" id="Seg_9465" s="T272">Sie unterhielten sich noch nicht unter sich, sie schauten wieder das neue Wunder an.</ta>
            <ta e="T290" id="Seg_9466" s="T281">Eine alte russische Frau jagte irgendwelche kleinen Rentierchen.</ta>
            <ta e="T298" id="Seg_9467" s="T290">Nackte kleine Rentiere ohne Hörner und mit kurzen Beinen liefen und schrieen.</ta>
            <ta e="T305" id="Seg_9468" s="T298">Diese, nachdem sie vorbeigelaufen sind, verteilen sich hierhin und dorthin.</ta>
            <ta e="T311" id="Seg_9469" s="T305">"Ui!", schrie Tojoo, "nackte wilde Rentiere!"</ta>
            <ta e="T317" id="Seg_9470" s="T311">Die Kinder lachen aus vollem Hals und zeigen mit den Fingern.</ta>
            <ta e="T328" id="Seg_9471" s="T317">"Guck, Freund, ihre Schnauzen sind wie die Ringe auf der Lenkstange, nur haben sie zwei Löcher.</ta>
            <ta e="T333" id="Seg_9472" s="T328">So ist kein Rentier, unsere sind schön[er]."</ta>
            <ta e="T340" id="Seg_9473" s="T333">Zuerst hatten sie ein Bisschen Angst, dann kamen sie näher, um gut gucken zu können.</ta>
            <ta e="T344" id="Seg_9474" s="T340">Tojoo konnte sein Temperament nicht besiegen.</ta>
            <ta e="T351" id="Seg_9475" s="T344">Zu einem lief er hin und setzte sich darauf, wie auf ein Reitrentier.</ta>
            <ta e="T364" id="Seg_9476" s="T351">Das wilde Rentier, als es den Menschen auf seinem Rücken spürte, sprang von dem Platz, wo es stand, und als es erschrak, rannte es mit voller Kraft nach vorne davon.</ta>
            <ta e="T371" id="Seg_9477" s="T364">Der Junge, der nicht vom Reitrentier fällt, versuchte am Rentierfell festzuhalten.</ta>
            <ta e="T377" id="Seg_9478" s="T371">Nirgendwie, nirgendwo finden seine Hände Halt.</ta>
            <ta e="T381" id="Seg_9479" s="T377">Die Haare des Rentiers sind wie kleine Nägel.</ta>
            <ta e="T389" id="Seg_9480" s="T381">Das Reitrentier schrie mit letzter Kraft und rannte nach vorne.</ta>
            <ta e="T396" id="Seg_9481" s="T389">Einige Schweine, die das sahen, liefen in einem Augenblick davon.</ta>
            <ta e="T401" id="Seg_9482" s="T396">Hinter ihnen lief eine alte Frau her.</ta>
            <ta e="T405" id="Seg_9483" s="T401">Tojoo schrie etwas… </ta>
            <ta e="T411" id="Seg_9484" s="T405">Sie läuft auf Tojoo zu und schwingt ihren Stab.</ta>
            <ta e="T420" id="Seg_9485" s="T411">Das nackte Reitrentier fiel in eine dreckige, matschig-nasse Grube und machte "platsch".</ta>
            <ta e="T426" id="Seg_9486" s="T420">Tojoo versuchte vergeblich mit seinem Schwein aufzustehen.</ta>
            <ta e="T433" id="Seg_9487" s="T426">Weder Gesicht noch sonst etwas ist zu sehen, alles ist voll Lehm.</ta>
            <ta e="T440" id="Seg_9488" s="T433">Als er versuchte aufzustehen und sehr müde war, erreichte die alte Frau ihn.</ta>
            <ta e="T447" id="Seg_9489" s="T440">Nachdem sie Tojoos Gesicht gesehen hatte, brach sie in Lachen aus und wedelte nur mit ihren Händen.</ta>
            <ta e="T452" id="Seg_9490" s="T447">"Verschwinde, woher du gekommen bist!", als ob sie das sagen würde.</ta>
            <ta e="T462" id="Seg_9491" s="T452">Tojoo sprang auf und lief mit letzter Kraft und außer Atem zu seinen Freunden.</ta>
            <ta e="T469" id="Seg_9492" s="T462">"Mein Gott, ich wäre fast gestorben, ich wäre fast gestorben!"</ta>
            <ta e="T480" id="Seg_9493" s="T469">Die Lehrer und der Direktor fingen von ganzem Herzen und mit ganzer Kraft an zu lachen.</ta>
            <ta e="T484" id="Seg_9494" s="T480">Pavel Kuzmič sagte, was er dachte: </ta>
            <ta e="T494" id="Seg_9495" s="T484">"Was soll ich sagen, es sind offenbar sehr flinke Leute angekommen."</ta>
            <ta e="T496" id="Seg_9496" s="T494">Seinen Kollegen sagte er: </ta>
            <ta e="T505" id="Seg_9497" s="T496">"Bringt sie jetzt ins Internat, wascht sie, bis das Essen zubereitet wurde.</ta>
            <ta e="T510" id="Seg_9498" s="T505">Macht das alles gründlich, ohne die Kinder einzuschüchtern."</ta>
            <ta e="T521" id="Seg_9499" s="T510">Sie brachten alle Kinder, zeigten allen die Wohnstätten, auf Russisch "komnata" genannt.</ta>
            <ta e="T526" id="Seg_9500" s="T521">Geflickte, neue Hemden und Schuhe gaben sie aus.</ta>
            <ta e="T539" id="Seg_9501" s="T526">Jedem, der gekommen war, wurden die Haare abgeschnitten, weshalb die Mädchen weinten, weil es ihnen um ihre Haare leid tat.</ta>
            <ta e="T542" id="Seg_9502" s="T539">Auch Tojoo bekam einen Haarschnitt.</ta>
            <ta e="T548" id="Seg_9503" s="T542">Es wurde Kleidung ausgegeben, es wurden Schuhe (wörtl. Hufe) verteilt.</ta>
            <ta e="T554" id="Seg_9504" s="T548">Er kam zu seinem Bett, setzte sich und schaute sich an, was sie verteilt hatten.</ta>
            <ta e="T557" id="Seg_9505" s="T554">Alles passt ihm.</ta>
            <ta e="T568" id="Seg_9506" s="T557">Aber warum haben sie je zwei mal Hemden und Hosen gegeben?</ta>
            <ta e="T574" id="Seg_9507" s="T568">Er dachte daran, die neue Kleidung anzuziehen.</ta>
            <ta e="T577" id="Seg_9508" s="T574">Sie brachten die Kinder zum Waschen.</ta>
            <ta e="T588" id="Seg_9509" s="T577">Wo das flinke Volk vor Freude lacht: "Heiß, heiß!", riefen sie.</ta>
            <ta e="T592" id="Seg_9510" s="T588">Nachdem Tojoo sich gewaschen hatte, zog er sich an.</ta>
            <ta e="T596" id="Seg_9511" s="T592">Er zog das weiße Hemd und die weißen Hosen an.</ta>
            <ta e="T601" id="Seg_9512" s="T596">Einige Sachen wickelte er auf Vorrat ein, um sie später anzuziehen.</ta>
            <ta e="T610" id="Seg_9513" s="T601">Nachdem er die Unterwäsche angezogen hatte, ging er, als ob nichts wäre.</ta>
            <ta e="T617" id="Seg_9514" s="T610">Seine Freunde sagten nicht einmal etwas zu ihm.</ta>
            <ta e="T626" id="Seg_9515" s="T617">Eine im Internat arbeitende Frau erklärte ihm, wie er sich anziehen sollte.</ta>
            <ta e="T629" id="Seg_9516" s="T626">Es wurde Zeit zu essen.</ta>
            <ta e="T639" id="Seg_9517" s="T629">Man setzte die Kinder an beide Seiten eines langen Tisches.</ta>
            <ta e="T645" id="Seg_9518" s="T639">Auf große Teller sind lecker riechende Brote gelegt.</ta>
            <ta e="T650" id="Seg_9519" s="T645">Jedem Kind wurde Fischsuppe aufgefüllt.</ta>
            <ta e="T652" id="Seg_9520" s="T650">Es wurden Löffel verteilt.</ta>
            <ta e="T659" id="Seg_9521" s="T652">Er sah, dass die Kinder aßen und ihre Nacken verloren gingen (?).</ta>
            <ta e="T670" id="Seg_9522" s="T659">Während Tojoo saß und aß, kamen ihm einige Gedanken: Er erinnerte sich an seine Mutter, an seinen Vater, an Kytscha.</ta>
            <ta e="T682" id="Seg_9523" s="T670">Nach dem Essen zogen die Kinder spielend durch Volochanka und schauten sich alle russischen Häuser und Höfe an.</ta>
            <ta e="T688" id="Seg_9524" s="T682">Für sie ist das alles wundersam, für die gerade angekommenen Leute.</ta>
            <ta e="T697" id="Seg_9525" s="T688">Am nächsten Tag kam wieder eine große Karawane mit Kindern von einer anderen Kolchose.</ta>
            <ta e="T706" id="Seg_9526" s="T697">Die gerade gekommenen Kinder sind sehr verwirrt. Als hätten sie vor allem Angst.</ta>
            <ta e="T711" id="Seg_9527" s="T706">Sie waren nicht sehr lange so.</ta>
            <ta e="T722" id="Seg_9528" s="T711">Wie auch Tojoo, Mitja und Ivan sich nicht mehr schämten, als sie neue Freunde sahen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_9529" s="T0">Попов Николай Анисимович. Прослушайте рассказ "Диво-олени".</ta>
            <ta e="T10" id="Seg_9530" s="T4">На третий день с детьми аргиш доехал до Волочанки.</ta>
            <ta e="T26" id="Seg_9531" s="T10">Усть-авамского районного центра русские, долганы, -- все собрались на приехавших детей посмотреть, как кукла одетые, очень милые. </ta>
            <ta e="T42" id="Seg_9532" s="T26">В не очень шумной, тихой Волочанке жизнь в один миг ожила, как-будто божьего дня встреча наступила.</ta>
            <ta e="T46" id="Seg_9533" s="T42">Дети к тому же на людей не смотрят.</ta>
            <ta e="T53" id="Seg_9534" s="T46">На русские дома только вглядываются.</ta>
            <ta e="T58" id="Seg_9535" s="T53">По улице проезжая верхом на оленях.</ta>
            <ta e="T64" id="Seg_9536" s="T58">Дедушка, Анисим старик стоял у школы рядом.</ta>
            <ta e="T73" id="Seg_9537" s="T64">Вокруг него шумная армия с верховых оленей с верху поспрыгивали.</ta>
            <ta e="T84" id="Seg_9538" s="T73">Около школы стоят Петр Кузьмич, школьников воспитатели и работники интерната.</ta>
            <ta e="T93" id="Seg_9539" s="T84">Все, схватив детей, здороваются, что-то им говорят.</ta>
            <ta e="T97" id="Seg_9540" s="T93">Дети русский язык не понимают.</ta>
            <ta e="T103" id="Seg_9541" s="T97">Поэтому приехавший маленький народ не разговаривают.</ta>
            <ta e="T108" id="Seg_9542" s="T103">Рассматривают только учителей лица.</ta>
            <ta e="T112" id="Seg_9543" s="T108">Они очень интересное увидели.</ta>
            <ta e="T122" id="Seg_9544" s="T112">Слишком большие русские дома, других людей, русских, с другим языком и одеждой.</ta>
            <ta e="T129" id="Seg_9545" s="T122">На ногах ведь оленьи копыта будто сапоги натянули.</ta>
            <ta e="T138" id="Seg_9546" s="T129">У мужчин на голове мамин ковшик словно шапки носят.</ta>
            <ta e="T141" id="Seg_9547" s="T138">Женщины без заплетённых кос.</ta>
            <ta e="T146" id="Seg_9548" s="T141">Волосы пострижены как у мужчин.</ta>
            <ta e="T156" id="Seg_9549" s="T146">У платья низ подола слишком заужен ( короток), поэтому они еле ходят.</ta>
            <ta e="T161" id="Seg_9550" s="T156">Тойоо очень удивёенно посмотрел на женщин.</ta>
            <ta e="T163" id="Seg_9551" s="T161">Про себя думает: </ta>
            <ta e="T172" id="Seg_9552" s="T163">"Наверное, в таких платьях бегать не умеют, маленько только ходят, оказывается".</ta>
            <ta e="T179" id="Seg_9553" s="T172">Друзьям что-то об этом рассказать хотел. </ta>
            <ta e="T186" id="Seg_9554" s="T179">Рот только открыл, какой-то шум раздался.</ta>
            <ta e="T192" id="Seg_9555" s="T186">Посмотрел туда, откуда шум исходил.</ta>
            <ta e="T194" id="Seg_9556" s="T192">О, господи! </ta>
            <ta e="T201" id="Seg_9557" s="T194">Какая-то, человек не видавший, оленья упряжка летит.</ta>
            <ta e="T207" id="Seg_9558" s="T201">Санки диковенные, олени же большие очень.</ta>
            <ta e="T212" id="Seg_9559" s="T207">Слишком большое махало словно хвост.</ta>
            <ta e="T220" id="Seg_9560" s="T212">У каюра лицо очень волосатое с мохнатой бородой.</ta>
            <ta e="T228" id="Seg_9561" s="T220">За оленем пешком шагая взял за возжи с двух сторон.</ta>
            <ta e="T235" id="Seg_9562" s="T228">Олень как-то с каждым шагом башкой трясет.</ta>
            <ta e="T238" id="Seg_9563" s="T235">И каждый раз каюр:</ta>
            <ta e="T244" id="Seg_9564" s="T238">"Но-о, но-о!" – проговаривает, сказку слушает будто. </ta>
            <ta e="T252" id="Seg_9565" s="T244">С силой ртом попробовал, вкусное сало ест будто.</ta>
            <ta e="T263" id="Seg_9566" s="T252">"Слушай это что за олень будет?" – глядя, порадовавшийся Тойо промолвил.</ta>
            <ta e="T268" id="Seg_9567" s="T263">Не он только на это стоял смотрел.</ta>
            <ta e="T272" id="Seg_9568" s="T268">Все дети удивленно смотрели. </ta>
            <ta e="T281" id="Seg_9569" s="T272">Между собой пока не разговарились, снова посмотрели новое диво.</ta>
            <ta e="T290" id="Seg_9570" s="T281">Пожилая русская старушка каких-то маленьких оленят подгоняла.</ta>
            <ta e="T298" id="Seg_9571" s="T290">Голые, безрогие оленята короткими ножками пробежали фыркая. </ta>
            <ta e="T305" id="Seg_9572" s="T298">Они, пролетая мимо, то туда, то сюда разбегаются.</ta>
            <ta e="T311" id="Seg_9573" s="T305">"Фу-у! – прокричал Тойо, – голые дикие олени!"</ta>
            <ta e="T317" id="Seg_9574" s="T311">Дети изо всех сил хохочут, пальцами тыча.</ta>
            <ta e="T328" id="Seg_9575" s="T317">"Смотри, друг, морда у них, как у хорея кончик, две только дырки имеют.</ta>
            <ta e="T333" id="Seg_9576" s="T328">Олень не такой, наши – красивее.</ta>
            <ta e="T340" id="Seg_9577" s="T333">Вначале чуть не испугались, потом подошли поближе, чтобы хорошо рассмотреть.</ta>
            <ta e="T344" id="Seg_9578" s="T340">Тойоо свою прыть не удержал.</ta>
            <ta e="T351" id="Seg_9579" s="T344">На одного, с разбега верхом забравшись, оседлал.</ta>
            <ta e="T364" id="Seg_9580" s="T351">Дикий олень, человека почувствовав, с места, где стоял, рванул, со всей силой полетел сломя голову.</ta>
            <ta e="T371" id="Seg_9581" s="T364">С верхового животного не падающий малыш крепко пытался схватиться за оленью шерсть.</ta>
            <ta e="T377" id="Seg_9582" s="T371">Никак, нигде пальцы не цепляются.</ta>
            <ta e="T381" id="Seg_9583" s="T377">У оленя шерсть словно гвозди.</ta>
            <ta e="T389" id="Seg_9584" s="T381">Олень, изо всей силы прокричав, сломя голову поскакал.</ta>
            <ta e="T396" id="Seg_9585" s="T389">Некоторые свиньи, это увидев, разом разбежались.</ta>
            <ta e="T401" id="Seg_9586" s="T396">За ними следом старая бабка бежать пыталась.</ta>
            <ta e="T405" id="Seg_9587" s="T401">Тойо что-то покрикивая… </ta>
            <ta e="T411" id="Seg_9588" s="T405">Пробегая в сторону Тойо палкой взмахивает.</ta>
            <ta e="T420" id="Seg_9589" s="T411">Голый олень в грязную, глинистую с водой яму плашмя упал.</ta>
            <ta e="T426" id="Seg_9590" s="T420">Тойо со своей свиньой пытаясь встать уже лёг.</ta>
            <ta e="T433" id="Seg_9591" s="T426">Ни лица, ничего не видать в глину превратилось.</ta>
            <ta e="T440" id="Seg_9592" s="T433">Пытаясь встать, уставши, старушка догнала его.</ta>
            <ta e="T447" id="Seg_9593" s="T440">Тойо лицо увидев, рассмеялась, руками только взмахивала.</ta>
            <ta e="T452" id="Seg_9594" s="T447">"Иди, откуда пришел" -- сказала будто.</ta>
            <ta e="T462" id="Seg_9595" s="T452">Тойо соскочив, со всех сил в сторону друзей ударил</ta>
            <ta e="T469" id="Seg_9596" s="T462">"Боже, чуть не помер, чуть не помер!"</ta>
            <ta e="T480" id="Seg_9597" s="T469">Учителя вместе с директором, не зная что сказать, изо всех сил хохотали стоя.</ta>
            <ta e="T484" id="Seg_9598" s="T480">Павел Кузьмич от всей души сказал: </ta>
            <ta e="T494" id="Seg_9599" s="T484">"Что сказать даже не знаю, очень шустрый народ прибыл, кажется".</ta>
            <ta e="T496" id="Seg_9600" s="T494">Коллегам сказал: </ta>
            <ta e="T505" id="Seg_9601" s="T496">"Сейчас их в интернат отведите, потом искупайте, пока еду им будут готовить".</ta>
            <ta e="T510" id="Seg_9602" s="T505">"Все доконально (хорошо) сделайте, детей не пугая."</ta>
            <ta e="T521" id="Seg_9603" s="T510">Детей тут же толпой отвели, показали жилое помещение, по-русски "комната" называемое. </ta>
            <ta e="T526" id="Seg_9604" s="T521">(...), новые рубашки, обувь выдали.</ta>
            <ta e="T539" id="Seg_9605" s="T526">Всем, кто приехал волосы постригли, отчего девочек плач сильный, оказывается, волосы жалея.</ta>
            <ta e="T542" id="Seg_9606" s="T539">Тойо тоже постригли.</ta>
            <ta e="T548" id="Seg_9607" s="T542">Одежду для одевания выдали, обувь как копыта дали в руки.</ta>
            <ta e="T554" id="Seg_9608" s="T548">К кровати подошёл, присел и посмотрел то, что выдали.</ta>
            <ta e="T557" id="Seg_9609" s="T554">Все ему в пору.</ta>
            <ta e="T568" id="Seg_9610" s="T557">Но для чего же они выдали по две рубашки и по двое штанов?</ta>
            <ta e="T574" id="Seg_9611" s="T568">Новую одежду надевая подумал.</ta>
            <ta e="T577" id="Seg_9612" s="T574">Детей купаться повели.</ta>
            <ta e="T588" id="Seg_9613" s="T577">Где пошустрее народ от души радуясь и смеясь: "Горячо, горячо!" – проговаривали сидя.</ta>
            <ta e="T592" id="Seg_9614" s="T588">Тойо помывшись оделся.</ta>
            <ta e="T596" id="Seg_9615" s="T592">Одел светлую рубашку, штаны.</ta>
            <ta e="T601" id="Seg_9616" s="T596">Некоторые вещи тщательно (прозапас) завернул, потом чтоб одеть.</ta>
            <ta e="T610" id="Seg_9617" s="T601">Надев нательное бельё светлое, как-будто ничего не случилось, так и ходил.</ta>
            <ta e="T617" id="Seg_9618" s="T610">Друзья даже ему ничего не говорят даже ему.</ta>
            <ta e="T626" id="Seg_9619" s="T617">В интернате работавшая одна женщина разъяснила, как ему надо одеваться. </ta>
            <ta e="T629" id="Seg_9620" s="T626">Кушать время подошло.</ta>
            <ta e="T639" id="Seg_9621" s="T629">Детей рассадили за очень дли.. длинные столы с двух сторон. </ta>
            <ta e="T645" id="Seg_9622" s="T639">В больших тарелках разложены вкусно пахнущие кусочки хлеба.</ta>
            <ta e="T650" id="Seg_9623" s="T645">Каждому ребёнку налили ухи.</ta>
            <ta e="T652" id="Seg_9624" s="T650">Ложки раздали.</ta>
            <ta e="T659" id="Seg_9625" s="T652">Увидел, у детей которые кушали, затылки только пропали.</ta>
            <ta e="T670" id="Seg_9626" s="T659">Тойо кушая сидя за столом о чём-то призадумался: маму, папу и Кычу вспоминая.</ta>
            <ta e="T682" id="Seg_9627" s="T670">После как поели дети по Волочанке играть пошли и посмотреть русские дома и окрестности. </ta>
            <ta e="T688" id="Seg_9628" s="T682">Для них всё в диковинку, как для вновь прибывших людей.</ta>
            <ta e="T697" id="Seg_9629" s="T688">На следующий день снова большой с детьми аргиш прибыл из другого колхоза.</ta>
            <ta e="T706" id="Seg_9630" s="T697">Вновь прибывшие дети пугливые очень: всего боятся будто.</ta>
            <ta e="T711" id="Seg_9631" s="T706">Они так долго такими не были. </ta>
            <ta e="T722" id="Seg_9632" s="T711">Так же как Тойо, Митя, Иван не стесняются, увидев новых друзей. </ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T659" id="Seg_9633" s="T652">[DCh]: Last part of the sentence is not clear, neither for native speakers.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T723" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
