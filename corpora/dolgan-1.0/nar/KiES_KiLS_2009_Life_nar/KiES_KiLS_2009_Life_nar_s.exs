<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID0BBFAEC0-2FC9-1480-BCFF-32CAA0D12CB9">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>KiES_KiLS_2009_Life_nar</transcription-name>
         <referenced-file url="KiES_KiLS_2009_Life_nar.wav" />
         <referenced-file url="KiES_KiLS_2009_Life_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\KiES_KiLS_2009_Life_nar\KiES_KiLS_2009_Life_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">799</ud-information>
            <ud-information attribute-name="# HIAT:w">550</ud-information>
            <ud-information attribute-name="# e">557</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">7</ud-information>
            <ud-information attribute-name="# HIAT:u">81</ud-information>
            <ud-information attribute-name="# sc">50</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KiES">
            <abbreviation>KiES</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="KiLS">
            <abbreviation>KiLS</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.201" type="appl" />
         <tli id="T2" time="0.7481111111111112" type="appl" />
         <tli id="T3" time="1.2952222222222223" type="appl" />
         <tli id="T4" time="1.8423333333333334" type="appl" />
         <tli id="T5" time="2.3894444444444445" type="appl" />
         <tli id="T6" time="2.9365555555555556" type="appl" />
         <tli id="T7" time="3.4836666666666667" type="appl" />
         <tli id="T8" time="4.030777777777778" type="appl" />
         <tli id="T9" time="4.5778888888888885" type="appl" />
         <tli id="T10" time="5.124999999999999" type="appl" />
         <tli id="T11" time="5.125" type="appl" />
         <tli id="T12" time="5.606307692307692" type="appl" />
         <tli id="T13" time="6.087615384615384" type="appl" />
         <tli id="T14" time="6.568923076923077" type="appl" />
         <tli id="T15" time="7.050230769230769" type="appl" />
         <tli id="T16" time="7.531538461538462" type="appl" />
         <tli id="T17" time="8.012846153846153" type="appl" />
         <tli id="T18" time="8.494153846153846" type="appl" />
         <tli id="T19" time="8.975461538461538" type="appl" />
         <tli id="T20" time="9.45676923076923" type="appl" />
         <tli id="T21" time="9.938076923076924" type="appl" />
         <tli id="T22" time="10.419384615384615" type="appl" />
         <tli id="T23" time="10.900692307692307" type="appl" />
         <tli id="T24" time="11.382" type="appl" />
         <tli id="T25" time="12.0625" type="appl" />
         <tli id="T26" time="12.742999999999999" type="appl" />
         <tli id="T27" time="13.423499999999999" type="appl" />
         <tli id="T28" time="14.104" type="appl" />
         <tli id="T29" time="14.7845" type="appl" />
         <tli id="T30" time="15.465" type="appl" />
         <tli id="T31" time="16.1455" type="appl" />
         <tli id="T32" time="16.826" type="appl" />
         <tli id="T33" time="17.5065" type="appl" />
         <tli id="T34" time="18.186999999999998" type="appl" />
         <tli id="T35" time="18.8675" type="appl" />
         <tli id="T36" time="20.192980547850205" />
         <tli id="T37" time="20.194999999999997" type="appl" />
         <tli id="T38" time="20.842" type="appl" />
         <tli id="T39" time="21.488999999999997" type="appl" />
         <tli id="T40" time="22.136" type="appl" />
         <tli id="T41" time="22.782999999999998" type="appl" />
         <tli id="T42" time="23.43" type="appl" />
         <tli id="T43" time="24.076999999999998" type="appl" />
         <tli id="T44" time="24.723999999999997" type="appl" />
         <tli id="T45" time="25.371" type="appl" />
         <tli id="T46" time="26.017999999999997" type="appl" />
         <tli id="T47" time="26.665" type="appl" />
         <tli id="T48" time="27.311999999999998" type="appl" />
         <tli id="T49" time="27.958999999999996" type="appl" />
         <tli id="T50" time="28.605999999999995" type="appl" />
         <tli id="T51" time="29.252999999999997" type="appl" />
         <tli id="T52" time="29.9" type="appl" />
         <tli id="T53" time="30.546999999999997" type="appl" />
         <tli id="T54" time="31.193999999999996" type="appl" />
         <tli id="T55" time="31.840999999999994" type="appl" />
         <tli id="T56" time="32.488" type="appl" />
         <tli id="T57" time="33.135" type="appl" />
         <tli id="T58" time="33.782" type="appl" />
         <tli id="T59" time="33.784666919715" />
         <tli id="T60" time="34.117666666666665" type="appl" />
         <tli id="T61" time="34.397333333333336" type="appl" />
         <tli id="T62" time="34.70366779121082" />
         <tli id="T63" time="34.717" type="appl" />
         <tli id="T64" time="35.213" type="appl" />
         <tli id="T65" time="35.708999999999996" type="appl" />
         <tli id="T66" time="36.205" type="appl" />
         <tli id="T67" time="36.835499999999996" type="appl" />
         <tli id="T68" time="37.466" type="appl" />
         <tli id="T69" time="38.0965" type="appl" />
         <tli id="T70" time="38.727" type="appl" />
         <tli id="T71" time="39.3575" type="appl" />
         <tli id="T72" time="39.988" type="appl" />
         <tli id="T73" time="40.34442857142857" type="appl" />
         <tli id="T74" time="40.70085714285714" type="appl" />
         <tli id="T75" time="41.05728571428571" type="appl" />
         <tli id="T76" time="41.413714285714285" type="appl" />
         <tli id="T77" time="41.77014285714286" type="appl" />
         <tli id="T78" time="42.126571428571424" type="appl" />
         <tli id="T79" time="42.483" type="appl" />
         <tli id="T80" time="42.974199999999996" type="appl" />
         <tli id="T81" time="43.465399999999995" type="appl" />
         <tli id="T82" time="43.9566" type="appl" />
         <tli id="T83" time="44.4478" type="appl" />
         <tli id="T84" time="45.025665193077124" />
         <tli id="T85" time="45.3962" type="appl" />
         <tli id="T86" time="45.8534" type="appl" />
         <tli id="T87" time="46.3106" type="appl" />
         <tli id="T88" time="46.7678" type="appl" />
         <tli id="T89" time="47.2058419476155" />
         <tli id="T90" time="47.552502557879016" />
         <tli id="T91" time="48.203" type="appl" />
         <tli id="T92" time="49.022666666666666" type="appl" />
         <tli id="T93" time="49.842333333333336" type="appl" />
         <tli id="T94" time="50.662000000000006" type="appl" />
         <tli id="T95" time="51.48166666666667" type="appl" />
         <tli id="T96" time="52.30133333333333" type="appl" />
         <tli id="T97" time="53.121" type="appl" />
         <tli id="T98" time="53.982" type="appl" />
         <tli id="T99" time="54.843" type="appl" />
         <tli id="T100" time="55.704" type="appl" />
         <tli id="T101" time="56.1422" type="appl" />
         <tli id="T102" time="56.580400000000004" type="appl" />
         <tli id="T103" time="57.0186" type="appl" />
         <tli id="T104" time="57.4568" type="appl" />
         <tli id="T105" time="57.908330748906124" />
         <tli id="T106" time="58.449444444444445" type="appl" />
         <tli id="T107" time="59.00388888888889" type="appl" />
         <tli id="T108" time="59.55833333333334" type="appl" />
         <tli id="T109" time="60.11277777777778" type="appl" />
         <tli id="T110" time="60.66722222222222" type="appl" />
         <tli id="T111" time="61.221666666666664" type="appl" />
         <tli id="T112" time="61.776111111111106" type="appl" />
         <tli id="T113" time="62.330555555555556" type="appl" />
         <tli id="T114" time="63.01833261943538" />
         <tli id="T115" time="63.415" type="appl" />
         <tli id="T116" time="63.945" type="appl" />
         <tli id="T117" time="64.475" type="appl" />
         <tli id="T118" time="65.005" type="appl" />
         <tli id="T119" time="65.535" type="appl" />
         <tli id="T120" time="66.065" type="appl" />
         <tli id="T121" time="66.56283333333333" type="appl" />
         <tli id="T122" time="67.06066666666666" type="appl" />
         <tli id="T123" time="67.55850000000001" type="appl" />
         <tli id="T124" time="68.05633333333334" type="appl" />
         <tli id="T125" time="68.55416666666667" type="appl" />
         <tli id="T126" time="70.61209969060066" />
         <tli id="T127" time="70.67067757151155" type="intp" />
         <tli id="T128" time="70.72925545242245" type="intp" />
         <tli id="T129" time="70.78783333333334" type="appl" />
         <tli id="T130" time="71.36644444444445" type="appl" />
         <tli id="T131" time="71.94505555555556" type="appl" />
         <tli id="T132" time="72.52366666666667" type="appl" />
         <tli id="T133" time="73.10227777777779" type="appl" />
         <tli id="T134" time="73.68088888888889" type="appl" />
         <tli id="T135" time="74.2595" type="appl" />
         <tli id="T136" time="74.83811111111112" type="appl" />
         <tli id="T137" time="75.41672222222222" type="appl" />
         <tli id="T138" time="75.99533333333333" type="appl" />
         <tli id="T139" time="76.57394444444445" type="appl" />
         <tli id="T140" time="77.15255555555555" type="appl" />
         <tli id="T141" time="77.73116666666667" type="appl" />
         <tli id="T142" time="78.30977777777778" type="appl" />
         <tli id="T143" time="78.88838888888888" type="appl" />
         <tli id="T144" time="79.9803214378859" />
         <tli id="T145" time="79.98725" type="appl" />
         <tli id="T146" time="80.5075" type="appl" />
         <tli id="T147" time="81.02775" type="appl" />
         <tli id="T148" time="81.548" type="appl" />
         <tli id="T149" time="81.81450000000001" type="appl" />
         <tli id="T150" time="82.8452193027846" />
         <tli id="T151" time="83.2006096513923" type="intp" />
         <tli id="T152" time="83.556" type="appl" />
         <tli id="T153" time="83.604" type="appl" />
         <tli id="T154" time="83.92933333333333" type="appl" />
         <tli id="T155" time="84.25466666666667" type="appl" />
         <tli id="T156" time="84.50519030193108" />
         <tli id="T157" time="84.9292" type="appl" />
         <tli id="T158" time="85.2784" type="appl" />
         <tli id="T159" time="85.6276" type="appl" />
         <tli id="T160" time="85.9768" type="appl" />
         <tli id="T161" time="86.326" type="appl" />
         <tli id="T162" time="86.6752" type="appl" />
         <tli id="T163" time="87.0244" type="appl" />
         <tli id="T164" time="87.3736" type="appl" />
         <tli id="T165" time="87.7228" type="appl" />
         <tli id="T166" time="88.072" type="appl" />
         <tli id="T167" time="88.468" type="appl" />
         <tli id="T168" time="88.822" type="appl" />
         <tli id="T169" time="88.864" type="appl" />
         <tli id="T170" time="89.26" type="appl" />
         <tli id="T171" time="89.75843185746292" />
         <tli id="T172" time="90.40100000000001" type="appl" />
         <tli id="T173" time="91.146" type="appl" />
         <tli id="T174" time="91.89099999999999" type="appl" />
         <tli id="T175" time="93.2026555386347" />
         <tli id="T176" time="93.23299999999999" type="appl" />
         <tli id="T177" time="93.83" type="appl" />
         <tli id="T178" time="94.42699999999999" type="appl" />
         <tli id="T179" time="95.024" type="appl" />
         <tli id="T180" time="95.621" type="appl" />
         <tli id="T181" time="96.218" type="appl" />
         <tli id="T182" time="96.815" type="appl" />
         <tli id="T183" time="97.2735" type="appl" />
         <tli id="T184" time="97.732" type="appl" />
         <tli id="T185" time="98.1905" type="appl" />
         <tli id="T186" time="98.649" type="appl" />
         <tli id="T187" time="99.1075" type="appl" />
         <tli id="T188" time="99.71266550212744" />
         <tli id="T189" time="100.0885" type="appl" />
         <tli id="T190" time="100.611" type="appl" />
         <tli id="T191" time="101.1335" type="appl" />
         <tli id="T192" time="101.656" type="appl" />
         <tli id="T193" time="102.1785" type="appl" />
         <tli id="T194" time="102.701" type="appl" />
         <tli id="T195" time="103.2235" type="appl" />
         <tli id="T196" time="103.93932994059567" />
         <tli id="T197" time="104.241625" type="appl" />
         <tli id="T198" time="104.73724999999999" type="appl" />
         <tli id="T199" time="105.23287499999999" type="appl" />
         <tli id="T200" time="105.7285" type="appl" />
         <tli id="T201" time="106.224125" type="appl" />
         <tli id="T202" time="106.71975" type="appl" />
         <tli id="T203" time="107.215375" type="appl" />
         <tli id="T204" time="107.711" type="appl" />
         <tli id="T205" time="108.206625" type="appl" />
         <tli id="T206" time="108.70224999999999" type="appl" />
         <tli id="T207" time="109.197875" type="appl" />
         <tli id="T208" time="109.6935" type="appl" />
         <tli id="T209" time="110.189125" type="appl" />
         <tli id="T210" time="110.68475000000001" type="appl" />
         <tli id="T211" time="111.180375" type="appl" />
         <tli id="T212" time="111.88933427763027" />
         <tli id="T213" time="112.0958" type="appl" />
         <tli id="T214" time="112.5156" type="appl" />
         <tli id="T215" time="112.9354" type="appl" />
         <tli id="T216" time="113.3552" type="appl" />
         <tli id="T217" time="113.775" type="appl" />
         <tli id="T218" time="114.1948" type="appl" />
         <tli id="T219" time="114.6146" type="appl" />
         <tli id="T220" time="115.03439999999999" type="appl" />
         <tli id="T221" time="115.4542" type="appl" />
         <tli id="T222" time="115.89400389658648" />
         <tli id="T223" time="116.71433333333333" type="appl" />
         <tli id="T224" time="117.55466666666666" type="appl" />
         <tli id="T225" time="118.40833496946536" />
         <tli id="T226" time="118.443" type="appl" />
         <tli id="T227" time="118.85433333333333" type="appl" />
         <tli id="T228" time="119.26566666666666" type="appl" />
         <tli id="T229" time="119.67699999999999" type="appl" />
         <tli id="T230" time="120.86455507841673" />
         <tli id="T231" time="121.15965408949742" />
         <tli id="T232" time="121.41787874479888" />
         <tli id="T233" time="122.14453271631281" />
         <tli id="T236" time="122.2045316680892" />
         <tli id="T237" time="122.28500000000001" type="appl" />
         <tli id="T238" time="122.771" type="appl" />
         <tli id="T239" time="123.257" type="appl" />
         <tli id="T240" time="123.74300000000001" type="appl" />
         <tli id="T241" time="124.229" type="appl" />
         <tli id="T242" time="124.715" type="appl" />
         <tli id="T243" time="125.20100000000001" type="appl" />
         <tli id="T244" time="125.687" type="appl" />
         <tli id="T245" time="126.22633640434958" />
         <tli id="T246" time="126.70100000000001" type="appl" />
         <tli id="T247" time="127.229" type="appl" />
         <tli id="T248" time="127.757" type="appl" />
         <tli id="T249" time="128.285" type="appl" />
         <tli id="T250" time="128.813" type="appl" />
         <tli id="T251" time="129.341" type="appl" />
         <tli id="T252" time="129.71700458660348" />
         <tli id="T253" time="129.869" type="appl" />
         <tli id="T254" time="130.5265" type="appl" />
         <tli id="T255" time="130.766" type="appl" />
         <tli id="T256" time="131.196" type="appl" />
         <tli id="T257" time="131.2175" type="appl" />
         <tli id="T258" time="131.77566653700706" />
         <tli id="T259" time="132.10675" type="appl" />
         <tli id="T260" time="132.5445" type="appl" />
         <tli id="T261" time="132.98225000000002" type="appl" />
         <tli id="T262" time="133.42000000000002" type="appl" />
         <tli id="T263" time="133.85775" type="appl" />
         <tli id="T264" time="134.2955" type="appl" />
         <tli id="T265" time="134.73325" type="appl" />
         <tli id="T266" time="135.171" type="appl" />
         <tli id="T267" time="135.60875" type="appl" />
         <tli id="T268" time="136.0465" type="appl" />
         <tli id="T269" time="136.48425" type="appl" />
         <tli id="T270" time="137.0219941508303" />
         <tli id="T271" time="137.66166666666666" type="appl" />
         <tli id="T272" time="138.40133333333333" type="appl" />
         <tli id="T273" time="139.00766258481497" />
         <tli id="T274" time="139.49505555555555" type="appl" />
         <tli id="T275" time="139.8491111111111" type="appl" />
         <tli id="T276" time="140.20316666666668" type="appl" />
         <tli id="T277" time="140.5572222222222" type="appl" />
         <tli id="T278" time="140.91127777777777" type="appl" />
         <tli id="T279" time="141.26533333333333" type="appl" />
         <tli id="T280" time="141.6193888888889" type="appl" />
         <tli id="T281" time="141.97344444444445" type="appl" />
         <tli id="T282" time="142.3275" type="appl" />
         <tli id="T283" time="142.68155555555555" type="appl" />
         <tli id="T284" time="143.0356111111111" type="appl" />
         <tli id="T285" time="143.38966666666667" type="appl" />
         <tli id="T286" time="143.74372222222223" type="appl" />
         <tli id="T287" time="144.0977777777778" type="appl" />
         <tli id="T288" time="144.45183333333335" type="appl" />
         <tli id="T289" time="144.8058888888889" type="appl" />
         <tli id="T290" time="145.15994444444445" type="appl" />
         <tli id="T291" time="146.91076669956257" />
         <tli id="T292" time="146.97848549201606" type="intp" />
         <tli id="T293" time="147.04620428446958" type="intp" />
         <tli id="T294" time="147.11392307692307" type="appl" />
         <tli id="T295" time="147.6472307692308" type="appl" />
         <tli id="T296" time="148.18053846153848" type="appl" />
         <tli id="T297" time="148.71384615384616" type="appl" />
         <tli id="T298" time="149.24715384615385" type="appl" />
         <tli id="T299" time="149.78046153846154" type="appl" />
         <tli id="T300" time="150.31376923076922" type="appl" />
         <tli id="T301" time="150.84707692307694" type="appl" />
         <tli id="T302" time="151.38038461538463" type="appl" />
         <tli id="T303" time="151.91369230769232" type="appl" />
         <tli id="T304" time="152.6603277090467" />
         <tli id="T305" time="153.1755" type="appl" />
         <tli id="T306" time="153.904" type="appl" />
         <tli id="T307" time="154.6325" type="appl" />
         <tli id="T308" time="155.361" type="appl" />
         <tli id="T309" time="155.81064999999998" type="appl" />
         <tli id="T310" time="156.2603" type="appl" />
         <tli id="T311" time="156.70995" type="appl" />
         <tli id="T312" time="157.15959999999998" type="appl" />
         <tli id="T313" time="157.60925" type="appl" />
         <tli id="T314" time="158.0589" type="appl" />
         <tli id="T315" time="158.50854999999999" type="appl" />
         <tli id="T316" time="158.9582" type="appl" />
         <tli id="T317" time="159.40785" type="appl" />
         <tli id="T318" time="159.85750000000002" type="appl" />
         <tli id="T319" time="160.30715" type="appl" />
         <tli id="T320" time="160.7568" type="appl" />
         <tli id="T321" time="161.20645000000002" type="appl" />
         <tli id="T322" time="161.6561" type="appl" />
         <tli id="T323" time="162.10575" type="appl" />
         <tli id="T324" time="162.55540000000002" type="appl" />
         <tli id="T325" time="163.00505" type="appl" />
         <tli id="T326" time="163.4547" type="appl" />
         <tli id="T327" time="163.90435000000002" type="appl" />
         <tli id="T328" time="164.354" type="appl" />
         <tli id="T329" time="164.395" type="appl" />
         <tli id="T330" time="164.84875" type="appl" />
         <tli id="T331" time="165.3025" type="appl" />
         <tli id="T332" time="165.75625000000002" type="appl" />
         <tli id="T333" time="166.50999459985317" />
         <tli id="T334" time="166.84025" type="appl" />
         <tli id="T335" time="167.47050000000002" type="appl" />
         <tli id="T336" time="168.10075" type="appl" />
         <tli id="T337" time="168.78432986562655" />
         <tli id="T338" time="169.28144444444445" type="appl" />
         <tli id="T339" time="169.8318888888889" type="appl" />
         <tli id="T340" time="170.38233333333332" type="appl" />
         <tli id="T341" time="170.93277777777777" type="appl" />
         <tli id="T342" time="171.48322222222222" type="appl" />
         <tli id="T343" time="172.03366666666668" type="appl" />
         <tli id="T344" time="172.5841111111111" type="appl" />
         <tli id="T345" time="173.13455555555555" type="appl" />
         <tli id="T346" time="173.66499980524492" />
         <tli id="T347" time="174.115" type="appl" />
         <tli id="T348" time="174.54500000000002" type="appl" />
         <tli id="T349" time="174.975" type="appl" />
         <tli id="T350" time="175.405" type="appl" />
         <tli id="T351" time="175.835" type="appl" />
         <tli id="T352" time="176.26500000000001" type="appl" />
         <tli id="T353" time="176.695" type="appl" />
         <tli id="T354" time="177.125" type="appl" />
         <tli id="T355" time="178.13688782406916" />
         <tli id="T356" time="178.50875000000002" type="appl" />
         <tli id="T357" time="179.4625" type="appl" />
         <tli id="T358" time="180.41625" type="appl" />
         <tli id="T359" time="181.61665775922302" />
         <tli id="T360" time="181.94766666666666" type="appl" />
         <tli id="T361" time="182.52533333333335" type="appl" />
         <tli id="T362" time="183.41632683857972" />
         <tli id="T363" time="183.66654545454546" type="appl" />
         <tli id="T364" time="184.2300909090909" type="appl" />
         <tli id="T365" time="184.79363636363638" type="appl" />
         <tli id="T366" time="185.35718181818183" type="appl" />
         <tli id="T367" time="185.92072727272728" type="appl" />
         <tli id="T368" time="186.48427272727272" type="appl" />
         <tli id="T369" time="187.04781818181817" type="appl" />
         <tli id="T370" time="187.61136363636362" type="appl" />
         <tli id="T371" time="188.1749090909091" type="appl" />
         <tli id="T372" time="188.73845454545454" type="appl" />
         <tli id="T373" time="189.5966876133575" />
         <tli id="T374" time="189.6867" type="appl" />
         <tli id="T375" time="190.07139999999998" type="appl" />
         <tli id="T376" time="190.4561" type="appl" />
         <tli id="T377" time="190.8408" type="appl" />
         <tli id="T378" time="191.2255" type="appl" />
         <tli id="T379" time="191.6102" type="appl" />
         <tli id="T380" time="191.9949" type="appl" />
         <tli id="T381" time="192.3796" type="appl" />
         <tli id="T382" time="192.7643" type="appl" />
         <tli id="T383" time="193.149" type="appl" />
         <tli id="T384" time="193.51008333333334" type="appl" />
         <tli id="T385" time="193.87116666666668" type="appl" />
         <tli id="T386" time="194.23225" type="appl" />
         <tli id="T387" time="194.59333333333333" type="appl" />
         <tli id="T388" time="194.95441666666667" type="appl" />
         <tli id="T389" time="195.3155" type="appl" />
         <tli id="T390" time="195.67658333333333" type="appl" />
         <tli id="T391" time="196.03766666666667" type="appl" />
         <tli id="T392" time="196.39875" type="appl" />
         <tli id="T393" time="196.75983333333335" type="appl" />
         <tli id="T394" time="197.12091666666666" type="appl" />
         <tli id="T395" time="197.482" type="appl" />
         <tli id="T396" time="198.277" type="appl" />
         <tli id="T397" time="199.072" type="appl" />
         <tli id="T398" time="199.867" type="appl" />
         <tli id="T399" time="200.662" type="appl" />
         <tli id="T400" time="201.457" type="appl" />
         <tli id="T401" time="202.252" type="appl" />
         <tli id="T402" time="204.80975516376827" />
         <tli id="T403" time="205.2060442485508" type="intp" />
         <tli id="T404" time="205.60233333333332" type="appl" />
         <tli id="T405" time="207.07333540910602" />
         <tli id="T406" time="207.47825" type="appl" />
         <tli id="T407" time="208.0765" type="appl" />
         <tli id="T408" time="208.67475" type="appl" />
         <tli id="T409" time="209.44632780543702" />
         <tli id="T410" time="210.07842857142856" type="appl" />
         <tli id="T411" time="210.88385714285715" type="appl" />
         <tli id="T412" time="211.68928571428572" type="appl" />
         <tli id="T413" time="212.49471428571428" type="appl" />
         <tli id="T414" time="213.30014285714284" type="appl" />
         <tli id="T415" time="214.10557142857144" type="appl" />
         <tli id="T416" time="214.911" type="appl" />
         <tli id="T417" time="214.943" type="appl" />
         <tli id="T418" time="215.31775000000002" type="appl" />
         <tli id="T419" time="215.6925" type="appl" />
         <tli id="T420" time="216.06725" type="appl" />
         <tli id="T421" time="217.58286534193962" />
         <tli id="T422" time="217.6019326709698" type="intp" />
         <tli id="T423" time="217.621" type="appl" />
         <tli id="T424" time="218.21050000000002" type="appl" />
         <tli id="T425" time="218.8" type="appl" />
         <tli id="T426" time="219.3895" type="appl" />
         <tli id="T427" time="219.979" type="appl" />
         <tli id="T428" time="220.40457142857144" type="appl" />
         <tli id="T429" time="220.83014285714287" type="appl" />
         <tli id="T430" time="221.2557142857143" type="appl" />
         <tli id="T431" time="221.6812857142857" type="appl" />
         <tli id="T432" time="222.10685714285714" type="appl" />
         <tli id="T433" time="222.53242857142857" type="appl" />
         <tli id="T434" time="224.70274095273658" />
         <tli id="T435" time="224.73782730182438" type="intp" />
         <tli id="T436" time="224.7729136509122" type="intp" />
         <tli id="T437" time="224.808" type="appl" />
         <tli id="T438" time="225.42466666666667" type="appl" />
         <tli id="T439" time="226.04133333333334" type="appl" />
         <tli id="T440" time="226.65800000000002" type="appl" />
         <tli id="T441" time="227.27466666666666" type="appl" />
         <tli id="T442" time="227.89133333333334" type="appl" />
         <tli id="T443" time="228.64134401758832" />
         <tli id="T444" time="228.965375" type="appl" />
         <tli id="T445" time="229.42275" type="appl" />
         <tli id="T446" time="229.88012500000002" type="appl" />
         <tli id="T447" time="230.3375" type="appl" />
         <tli id="T448" time="230.794875" type="appl" />
         <tli id="T449" time="231.25225" type="appl" />
         <tli id="T450" time="231.70962500000002" type="appl" />
         <tli id="T451" time="232.44700669666818" />
         <tli id="T452" time="232.62277777777777" type="appl" />
         <tli id="T453" time="233.07855555555557" type="appl" />
         <tli id="T454" time="233.53433333333334" type="appl" />
         <tli id="T455" time="233.9901111111111" type="appl" />
         <tli id="T456" time="234.4458888888889" type="appl" />
         <tli id="T457" time="234.90166666666667" type="appl" />
         <tli id="T458" time="235.35744444444444" type="appl" />
         <tli id="T459" time="235.81322222222224" type="appl" />
         <tli id="T460" time="236.269" type="appl" />
         <tli id="T461" time="237.04955555555557" type="appl" />
         <tli id="T462" time="237.8301111111111" type="appl" />
         <tli id="T463" time="238.61066666666667" type="appl" />
         <tli id="T464" time="239.39122222222224" type="appl" />
         <tli id="T465" time="240.17177777777778" type="appl" />
         <tli id="T466" time="240.95233333333334" type="appl" />
         <tli id="T467" time="241.7328888888889" type="appl" />
         <tli id="T468" time="242.51344444444445" type="appl" />
         <tli id="T469" time="243.34067054341384" />
         <tli id="T470" time="243.85750000000002" type="appl" />
         <tli id="T471" time="244.421" type="appl" />
         <tli id="T472" time="244.9845" type="appl" />
         <tli id="T473" time="245.548" type="appl" />
         <tli id="T474" time="245.58" type="appl" />
         <tli id="T475" time="246.0542" type="appl" />
         <tli id="T476" time="246.5284" type="appl" />
         <tli id="T477" time="247.0026" type="appl" />
         <tli id="T478" time="247.4768" type="appl" />
         <tli id="T479" time="247.951" type="appl" />
         <tli id="T480" time="250.54895606796117" />
         <tli id="T481" time="250.73298977824362" type="intp" />
         <tli id="T482" time="250.91702348852604" type="intp" />
         <tli id="T483" time="251.10105719880846" type="intp" />
         <tli id="T484" time="251.2850909090909" type="appl" />
         <tli id="T485" time="252.10636363636362" type="appl" />
         <tli id="T486" time="252.92763636363637" type="appl" />
         <tli id="T487" time="253.74890909090908" type="appl" />
         <tli id="T488" time="254.57018181818182" type="appl" />
         <tli id="T489" time="255.39145454545454" type="appl" />
         <tli id="T490" time="256.21272727272725" type="appl" />
         <tli id="T491" time="257.034" type="appl" />
         <tli id="T492" time="257.54415384615385" type="appl" />
         <tli id="T493" time="258.0543076923077" type="appl" />
         <tli id="T494" time="258.56446153846156" type="appl" />
         <tli id="T495" time="259.07461538461536" type="appl" />
         <tli id="T496" time="259.5847692307692" type="appl" />
         <tli id="T497" time="260.09492307692307" type="appl" />
         <tli id="T498" time="260.6050769230769" type="appl" />
         <tli id="T499" time="261.1152307692308" type="appl" />
         <tli id="T500" time="261.62538461538463" type="appl" />
         <tli id="T501" time="262.13553846153843" type="appl" />
         <tli id="T502" time="262.6456923076923" type="appl" />
         <tli id="T503" time="263.15584615384614" type="appl" />
         <tli id="T504" time="264.22871707297554" />
         <tli id="T505" time="264.2308888888889" type="appl" />
         <tli id="T506" time="264.7957777777778" type="appl" />
         <tli id="T507" time="265.36066666666665" type="appl" />
         <tli id="T508" time="265.92555555555555" type="appl" />
         <tli id="T509" time="266.49044444444445" type="appl" />
         <tli id="T510" time="267.05533333333335" type="appl" />
         <tli id="T511" time="267.6202222222222" type="appl" />
         <tli id="T512" time="268.1851111111111" type="appl" />
         <tli id="T513" time="270.9219334711405" />
         <tli id="T514" time="271.11471673557025" type="intp" />
         <tli id="T515" time="271.3075" type="appl" />
         <tli id="T516" time="272.58625" type="appl" />
         <tli id="T517" time="273.9316725555619" />
         <tli id="T518" time="274.27828571428574" type="appl" />
         <tli id="T519" time="274.6915714285714" type="appl" />
         <tli id="T520" time="275.10485714285716" type="appl" />
         <tli id="T521" time="275.51814285714283" type="appl" />
         <tli id="T522" time="275.93142857142857" type="appl" />
         <tli id="T523" time="276.34471428571425" type="appl" />
         <tli id="T524" time="276.7913361369071" />
         <tli id="T525" time="276.822" type="appl" />
         <tli id="T526" time="277.451" type="appl" />
         <tli id="T527" time="278.08" type="appl" />
         <tli id="T528" time="278.096" type="appl" />
         <tli id="T529" time="279.17533333333336" type="appl" />
         <tli id="T530" time="280.25466666666665" type="appl" />
         <tli id="T531" time="281.334" type="appl" />
         <tli id="T532" time="281.4733376724014" />
         <tli id="T533" time="281.60174688733593" />
         <tli id="T534" time="281.88840854582315" />
         <tli id="T535" time="284.73567651039514" />
         <tli id="T536" time="285.85766211685575" />
         <tli id="T537" time="286.49325" type="appl" />
         <tli id="T538" time="287.1555" type="appl" />
         <tli id="T539" time="287.81775000000005" type="appl" />
         <tli id="T540" time="288.48" type="appl" />
         <tli id="T541" time="289.14225" type="appl" />
         <tli id="T542" time="289.8045" type="appl" />
         <tli id="T543" time="290.46675000000005" type="appl" />
         <tli id="T544" time="291.129" type="appl" />
         <tli id="T545" time="291.79525" type="appl" />
         <tli id="T546" time="292.4615" type="appl" />
         <tli id="T547" time="293.12775" type="appl" />
         <tli id="T548" time="293.914005741902" />
         <tli id="T549" time="294.24333333333334" type="appl" />
         <tli id="T550" time="294.69266666666664" type="appl" />
         <tli id="T551" time="295.142" type="appl" />
         <tli id="T552" time="295.59133333333335" type="appl" />
         <tli id="T553" time="296.04066666666665" type="appl" />
         <tli id="T554" time="296.49" type="appl" />
         <tli id="T555" time="296.93933333333337" type="appl" />
         <tli id="T556" time="297.38866666666667" type="appl" />
         <tli id="T557" time="297.8779989881695" />
         <tli id="T558" time="298.799" type="appl" />
         <tli id="T559" time="299.36375" type="appl" />
         <tli id="T560" time="299.9285" type="appl" />
         <tli id="T561" time="300.49325" type="appl" />
         <tli id="T562" time="301.90139223834416" />
         <tli id="T563" time="301.97472429051527" />
         <tli id="T564" time="302.4791111111111" type="appl" />
         <tli id="T565" time="303.18966666666665" type="appl" />
         <tli id="T566" time="303.9002222222222" type="appl" />
         <tli id="T567" time="304.61077777777774" type="appl" />
         <tli id="T568" time="305.3213333333333" type="appl" />
         <tli id="T569" time="306.0318888888889" type="appl" />
         <tli id="T570" time="306.7424444444444" type="appl" />
         <tli id="T571" time="307.8879543155873" />
         <tli id="T572" time="308.719" type="appl" />
         <tli id="T573" time="310.21166371876" />
         <tli id="T574" time="311.359" type="appl" />
         <tli id="T0" time="312.427" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-KiES"
                      id="tx-KiES"
                      speaker="KiES"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KiES">
            <ts e="T58" id="Seg_0" n="sc" s="T1">
               <ts e="T11" id="Seg_2" n="HIAT:u" s="T1">
                  <nts id="Seg_3" n="HIAT:ip">–</nts>
                  <nts id="Seg_4" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_6" n="HIAT:w" s="T1">Dʼe</ts>
                  <nts id="Seg_7" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_9" n="HIAT:w" s="T2">Korgoːgo</ts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_12" n="HIAT:w" s="T3">baːr</ts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_15" n="HIAT:w" s="T4">etibit</ts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_18" n="HIAT:w" s="T5">hirdeːk</ts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_21" n="HIAT:w" s="T6">etibit</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_24" n="HIAT:w" s="T7">ginilerge</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_27" n="HIAT:w" s="T8">Korgoːgo</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_30" n="HIAT:w" s="T9">hirdemmitim</ts>
                  <nts id="Seg_31" n="HIAT:ip">.</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_34" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_36" n="HIAT:w" s="T11">Onton</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_39" n="HIAT:w" s="T12">bu͡ollagɨna</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_42" n="HIAT:w" s="T13">bu</ts>
                  <nts id="Seg_43" n="HIAT:ip">,</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_46" n="HIAT:w" s="T14">eː</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_49" n="HIAT:w" s="T15">bu</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_52" n="HIAT:w" s="T16">ogonu</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_55" n="HIAT:w" s="T17">töröːbütüm</ts>
                  <nts id="Seg_56" n="HIAT:ip">,</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_59" n="HIAT:w" s="T18">onton</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_62" n="HIAT:w" s="T19">ol</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_65" n="HIAT:w" s="T20">u͡ol</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_68" n="HIAT:w" s="T21">ogom</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_71" n="HIAT:w" s="T22">töröːmmün</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_74" n="HIAT:w" s="T23">ol</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_78" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_80" n="HIAT:w" s="T24">Onton</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_83" n="HIAT:w" s="T25">bu͡ollagɨna</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_86" n="HIAT:w" s="T26">ogonnʼorbut</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_89" n="HIAT:w" s="T27">ɨ͡aldʼar</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_92" n="HIAT:w" s="T28">ete</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_95" n="HIAT:w" s="T29">ölön</ts>
                  <nts id="Seg_96" n="HIAT:ip">,</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_99" n="HIAT:w" s="T30">ölön</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_102" n="HIAT:w" s="T31">ol</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_105" n="HIAT:w" s="T32">kimi͡eke</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_108" n="HIAT:w" s="T33">Hɨndaːskaga</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_111" n="HIAT:w" s="T34">hɨppɨppɨt</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_114" n="HIAT:w" s="T35">ol</ts>
                  <nts id="Seg_115" n="HIAT:ip">.</nts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_118" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_120" n="HIAT:w" s="T36">Onton</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_123" n="HIAT:w" s="T37">ülem</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_126" n="HIAT:w" s="T38">minigin</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_129" n="HIAT:w" s="T39">načʼalnʼikʼi</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_132" n="HIAT:w" s="T40">zaabrazalʼi</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_135" n="HIAT:w" s="T41">eti</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_138" n="HIAT:w" s="T42">bu</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_141" n="HIAT:w" s="T43">ke</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_144" n="HIAT:w" s="T44">kak</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_147" n="HIAT:w" s="T45">budu</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_150" n="HIAT:w" s="T46">eti</ts>
                  <nts id="Seg_151" n="HIAT:ip">,</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_154" n="HIAT:w" s="T47">onton</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_157" n="HIAT:w" s="T48">üle</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_160" n="HIAT:w" s="T49">bi͡erbittere</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_163" n="HIAT:w" s="T50">mi͡eke</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_166" n="HIAT:w" s="T51">saːdikka</ts>
                  <nts id="Seg_167" n="HIAT:ip">,</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_170" n="HIAT:w" s="T52">sadʼik</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_173" n="HIAT:w" s="T53">pervɨj</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_176" n="HIAT:w" s="T54">raz</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_179" n="HIAT:w" s="T55">bɨl</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_182" n="HIAT:w" s="T56">v</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_185" n="HIAT:w" s="T57">Sɨndasske</ts>
                  <nts id="Seg_186" n="HIAT:ip">.</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T90" id="Seg_188" n="sc" s="T63">
               <ts e="T66" id="Seg_190" n="HIAT:u" s="T63">
                  <nts id="Seg_191" n="HIAT:ip">–</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_194" n="HIAT:w" s="T63">Eː</ts>
                  <nts id="Seg_195" n="HIAT:ip">,</nts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_198" n="HIAT:w" s="T64">hakalɨː</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_201" n="HIAT:w" s="T65">du͡o</ts>
                  <nts id="Seg_202" n="HIAT:ip">.</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_205" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_207" n="HIAT:w" s="T66">Hɨndaːskaga</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_209" n="HIAT:ip">(</nts>
                  <ts e="T68" id="Seg_211" n="HIAT:w" s="T67">haka</ts>
                  <nts id="Seg_212" n="HIAT:ip">)</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_215" n="HIAT:w" s="T68">haŋardɨː</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_218" n="HIAT:w" s="T69">saːdik</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_221" n="HIAT:w" s="T70">turbut</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_224" n="HIAT:w" s="T71">ete</ts>
                  <nts id="Seg_225" n="HIAT:ip">.</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_228" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_230" n="HIAT:w" s="T72">Dʼe</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_233" n="HIAT:w" s="T73">tugu</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_236" n="HIAT:w" s="T74">da</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_239" n="HIAT:w" s="T75">bilpeppin</ts>
                  <nts id="Seg_240" n="HIAT:ip">,</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_243" n="HIAT:w" s="T76">saːdik</ts>
                  <nts id="Seg_244" n="HIAT:ip">,</nts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_247" n="HIAT:w" s="T77">tu͡ok</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_250" n="HIAT:w" s="T78">aːtaj</ts>
                  <nts id="Seg_251" n="HIAT:ip">?</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_254" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_256" n="HIAT:w" s="T79">Ničevo</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_259" n="HIAT:w" s="T80">nʼe</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_262" n="HIAT:w" s="T81">znajem</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_265" n="HIAT:w" s="T82">mɨ</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_268" n="HIAT:w" s="T83">tundravʼičkʼi</ts>
                  <nts id="Seg_269" n="HIAT:ip">.</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_272" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_274" n="HIAT:w" s="T84">Vsʼo</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_277" n="HIAT:w" s="T85">takoje</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_280" n="HIAT:w" s="T86">eta</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_283" n="HIAT:w" s="T87">karmʼit</ts>
                  <nts id="Seg_284" n="HIAT:ip">,</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_287" n="HIAT:w" s="T88">kak</ts>
                  <nts id="Seg_288" n="HIAT:ip">…</nts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T148" id="Seg_290" n="sc" s="T91">
               <ts e="T97" id="Seg_292" n="HIAT:u" s="T91">
                  <nts id="Seg_293" n="HIAT:ip">–</nts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_296" n="HIAT:w" s="T91">Oː</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_298" n="HIAT:ip">(</nts>
                  <nts id="Seg_299" n="HIAT:ip">(</nts>
                  <ats e="T93" id="Seg_300" n="HIAT:non-pho" s="T92">…</ats>
                  <nts id="Seg_301" n="HIAT:ip">)</nts>
                  <nts id="Seg_302" n="HIAT:ip">)</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_305" n="HIAT:w" s="T93">dʼe</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_308" n="HIAT:w" s="T94">kajdak</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_311" n="HIAT:w" s="T95">bu͡olu͡okputuj</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_313" n="HIAT:ip">(</nts>
                  <nts id="Seg_314" n="HIAT:ip">(</nts>
                  <ats e="T97" id="Seg_315" n="HIAT:non-pho" s="T96">…</ats>
                  <nts id="Seg_316" n="HIAT:ip">)</nts>
                  <nts id="Seg_317" n="HIAT:ip">)</nts>
                  <nts id="Seg_318" n="HIAT:ip">.</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_321" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_323" n="HIAT:w" s="T97">Hataːn</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_326" n="HIAT:w" s="T98">haŋarbappɨn</ts>
                  <nts id="Seg_327" n="HIAT:ip">,</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_330" n="HIAT:w" s="T99">ogolorgo</ts>
                  <nts id="Seg_331" n="HIAT:ip">.</nts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T105" id="Seg_334" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_336" n="HIAT:w" s="T100">Če</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_339" n="HIAT:w" s="T101">ogolorgo</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_342" n="HIAT:w" s="T102">hataːn</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_345" n="HIAT:w" s="T103">buharbappɨn</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_348" n="HIAT:w" s="T104">diːbin</ts>
                  <nts id="Seg_349" n="HIAT:ip">.</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_352" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_354" n="HIAT:w" s="T105">Hu͡ok</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_357" n="HIAT:w" s="T106">povardar</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_360" n="HIAT:w" s="T107">diːller</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_363" n="HIAT:w" s="T108">bu͡o</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_366" n="HIAT:w" s="T109">bu</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_369" n="HIAT:w" s="T110">saːdik</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_372" n="HIAT:w" s="T111">turda</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_375" n="HIAT:w" s="T112">haŋa</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_378" n="HIAT:w" s="T113">saːdikka</ts>
                  <nts id="Seg_379" n="HIAT:ip">.</nts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_382" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_384" n="HIAT:w" s="T114">Onuga</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_387" n="HIAT:w" s="T115">poːvar</ts>
                  <nts id="Seg_388" n="HIAT:ip">,</nts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_391" n="HIAT:w" s="T116">poːvar</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_394" n="HIAT:w" s="T117">naːda</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_397" n="HIAT:w" s="T118">diːller</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_400" n="HIAT:w" s="T119">tojottor</ts>
                  <nts id="Seg_401" n="HIAT:ip">.</nts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_404" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_406" n="HIAT:w" s="T120">Innʼe</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_409" n="HIAT:w" s="T121">tojottor</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_412" n="HIAT:w" s="T122">minigin</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_415" n="HIAT:w" s="T123">poːvar</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_418" n="HIAT:w" s="T124">uːrdular</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_421" n="HIAT:w" s="T125">bu͡olla</ts>
                  <nts id="Seg_422" n="HIAT:ip">.</nts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_425" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_427" n="HIAT:w" s="T126">Dʼe</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_430" n="HIAT:w" s="T127">ol</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_433" n="HIAT:w" s="T128">hɨppɨppɨt</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_436" n="HIAT:w" s="T129">onton</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_439" n="HIAT:w" s="T130">ol</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_442" n="HIAT:w" s="T131">poːvardaːmmɨn</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_445" n="HIAT:w" s="T132">bu͡ollagɨna</ts>
                  <nts id="Seg_446" n="HIAT:ip">,</nts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_449" n="HIAT:w" s="T133">eː</ts>
                  <nts id="Seg_450" n="HIAT:ip">,</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_453" n="HIAT:w" s="T134">kimi͡eke</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_456" n="HIAT:w" s="T135">bu</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_459" n="HIAT:w" s="T136">ke</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_462" n="HIAT:w" s="T137">iti</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_465" n="HIAT:w" s="T138">ikki</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_468" n="HIAT:w" s="T139">ogom</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_471" n="HIAT:w" s="T140">hɨldʼallar</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_474" n="HIAT:w" s="T141">saːdikka</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_477" n="HIAT:w" s="T142">araj</ts>
                  <nts id="Seg_478" n="HIAT:ip">,</nts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_481" n="HIAT:w" s="T143">heː</ts>
                  <nts id="Seg_482" n="HIAT:ip">.</nts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_485" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_487" n="HIAT:w" s="T144">Dʼe</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_490" n="HIAT:w" s="T145">ol</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_493" n="HIAT:w" s="T146">hɨldʼannar</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_495" n="HIAT:ip">(</nts>
                  <nts id="Seg_496" n="HIAT:ip">(</nts>
                  <ats e="T148" id="Seg_497" n="HIAT:non-pho" s="T147">…</ats>
                  <nts id="Seg_498" n="HIAT:ip">)</nts>
                  <nts id="Seg_499" n="HIAT:ip">)</nts>
                  <nts id="Seg_500" n="HIAT:ip">.</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T152" id="Seg_502" n="sc" s="T150">
               <ts e="T152" id="Seg_504" n="HIAT:u" s="T150">
                  <ts e="T151" id="Seg_506" n="HIAT:w" s="T150">Kimi</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_509" n="HIAT:w" s="T151">gɨtta</ts>
                  <nts id="Seg_510" n="HIAT:ip">?</nts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T166" id="Seg_512" n="sc" s="T156">
               <ts e="T166" id="Seg_514" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_516" n="HIAT:w" s="T156">Togo</ts>
                  <nts id="Seg_517" n="HIAT:ip">,</nts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_520" n="HIAT:w" s="T157">ol</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_523" n="HIAT:w" s="T158">onton</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_526" n="HIAT:w" s="T159">ol</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_529" n="HIAT:w" s="T160">kojut</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_532" n="HIAT:w" s="T161">ol</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_535" n="HIAT:w" s="T162">kimim</ts>
                  <nts id="Seg_536" n="HIAT:ip">,</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_539" n="HIAT:w" s="T163">ol</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_542" n="HIAT:w" s="T164">edʼijim</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_545" n="HIAT:w" s="T165">ölbüte</ts>
                  <nts id="Seg_546" n="HIAT:ip">.</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T225" id="Seg_548" n="sc" s="T168">
               <ts e="T171" id="Seg_550" n="HIAT:u" s="T168">
                  <nts id="Seg_551" n="HIAT:ip">–</nts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_554" n="HIAT:w" s="T168">Dʼe</ts>
                  <nts id="Seg_555" n="HIAT:ip">.</nts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_558" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_560" n="HIAT:w" s="T171">Ijem</ts>
                  <nts id="Seg_561" n="HIAT:ip">,</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_564" n="HIAT:w" s="T172">ijem</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_567" n="HIAT:w" s="T173">baltɨta</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_570" n="HIAT:w" s="T174">ölbüte</ts>
                  <nts id="Seg_571" n="HIAT:ip">.</nts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_574" n="HIAT:u" s="T175">
                  <ts e="T176" id="Seg_576" n="HIAT:w" s="T175">ɨ͡aldʼan</ts>
                  <nts id="Seg_577" n="HIAT:ip">,</nts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_580" n="HIAT:w" s="T176">oččogo</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_583" n="HIAT:w" s="T177">tü͡ört</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_586" n="HIAT:w" s="T178">ogoloːk</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_589" n="HIAT:w" s="T179">kaːlla</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_592" n="HIAT:w" s="T180">bu</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_595" n="HIAT:w" s="T181">kihi</ts>
                  <nts id="Seg_596" n="HIAT:ip">.</nts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_599" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_601" n="HIAT:w" s="T182">Tü͡ört</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_604" n="HIAT:w" s="T183">ogo</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_607" n="HIAT:w" s="T184">bu͡olla</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_610" n="HIAT:w" s="T185">mini͡eke</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_613" n="HIAT:w" s="T186">kelliler</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_616" n="HIAT:w" s="T187">bu͡olla</ts>
                  <nts id="Seg_617" n="HIAT:ip">.</nts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_620" n="HIAT:u" s="T188">
                  <nts id="Seg_621" n="HIAT:ip">"</nts>
                  <ts e="T189" id="Seg_623" n="HIAT:w" s="T188">Bu͡o</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_626" n="HIAT:w" s="T189">edʼiːjbitiger</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_629" n="HIAT:w" s="T190">bu͡olu͡okput</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_632" n="HIAT:w" s="T191">bihigi</ts>
                  <nts id="Seg_633" n="HIAT:ip">,</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_636" n="HIAT:w" s="T192">kajdi͡et</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_639" n="HIAT:w" s="T193">da</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_642" n="HIAT:w" s="T194">barbappɨt</ts>
                  <nts id="Seg_643" n="HIAT:ip">"</nts>
                  <nts id="Seg_644" n="HIAT:ip">,</nts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_647" n="HIAT:w" s="T195">diːller</ts>
                  <nts id="Seg_648" n="HIAT:ip">.</nts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_651" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_653" n="HIAT:w" s="T196">Inʼem</ts>
                  <nts id="Seg_654" n="HIAT:ip">,</nts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_657" n="HIAT:w" s="T197">eː</ts>
                  <nts id="Seg_658" n="HIAT:ip">,</nts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_660" n="HIAT:ip">(</nts>
                  <ts e="T199" id="Seg_662" n="HIAT:w" s="T198">inʼe-</ts>
                  <nts id="Seg_663" n="HIAT:ip">)</nts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_666" n="HIAT:w" s="T199">inʼete</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_669" n="HIAT:w" s="T200">geri͡es</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_672" n="HIAT:w" s="T201">eppit</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_675" n="HIAT:w" s="T202">bu͡olla</ts>
                  <nts id="Seg_676" n="HIAT:ip">,</nts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_678" n="HIAT:ip">"</nts>
                  <ts e="T204" id="Seg_680" n="HIAT:w" s="T203">bu</ts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_683" n="HIAT:w" s="T204">ke</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_686" n="HIAT:w" s="T205">edʼiːjgitiger</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_689" n="HIAT:w" s="T206">kaːlan</ts>
                  <nts id="Seg_690" n="HIAT:ip">"</nts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_693" n="HIAT:w" s="T207">di͡ebit</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_696" n="HIAT:w" s="T208">bu͡olla</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_698" n="HIAT:ip">"</nts>
                  <ts e="T210" id="Seg_700" n="HIAT:w" s="T209">min</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_703" n="HIAT:w" s="T210">kajdak</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_706" n="HIAT:w" s="T211">bu͡ollakpɨna</ts>
                  <nts id="Seg_707" n="HIAT:ip">"</nts>
                  <nts id="Seg_708" n="HIAT:ip">.</nts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T222" id="Seg_711" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_713" n="HIAT:w" s="T212">Dʼe</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_716" n="HIAT:w" s="T213">ol</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_719" n="HIAT:w" s="T214">ol</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_722" n="HIAT:w" s="T215">tü͡ört</ts>
                  <nts id="Seg_723" n="HIAT:ip">,</nts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_726" n="HIAT:w" s="T216">tü͡ört</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_729" n="HIAT:w" s="T217">ogo</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_732" n="HIAT:w" s="T218">bihi͡eke</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_735" n="HIAT:w" s="T219">kelliler</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_738" n="HIAT:w" s="T220">araj</ts>
                  <nts id="Seg_739" n="HIAT:ip">,</nts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_742" n="HIAT:w" s="T221">mhm</ts>
                  <nts id="Seg_743" n="HIAT:ip">.</nts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_746" n="HIAT:u" s="T222">
                  <ts e="T223" id="Seg_748" n="HIAT:w" s="T222">Nosku͡o</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_751" n="HIAT:w" s="T223">ü͡örene</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_754" n="HIAT:w" s="T224">barallar</ts>
                  <nts id="Seg_755" n="HIAT:ip">.</nts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T233" id="Seg_757" n="sc" s="T230">
               <ts e="T233" id="Seg_759" n="HIAT:u" s="T230">
                  <nts id="Seg_760" n="HIAT:ip">–</nts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_763" n="HIAT:w" s="T230">Dʼe</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_766" n="HIAT:w" s="T231">beːj</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_769" n="HIAT:w" s="T232">onton</ts>
                  <nts id="Seg_770" n="HIAT:ip">.</nts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T253" id="Seg_772" n="sc" s="T236">
               <ts e="T245" id="Seg_774" n="HIAT:u" s="T236">
                  <ts e="T237" id="Seg_776" n="HIAT:w" s="T236">Dʼe</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_779" n="HIAT:w" s="T237">onton</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_781" n="HIAT:ip">(</nts>
                  <ts e="T239" id="Seg_783" n="HIAT:w" s="T238">kanʼikulga</ts>
                  <nts id="Seg_784" n="HIAT:ip">)</nts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_787" n="HIAT:w" s="T239">keleller</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_790" n="HIAT:w" s="T240">bu</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_793" n="HIAT:w" s="T241">ogolor</ts>
                  <nts id="Seg_794" n="HIAT:ip">,</nts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_797" n="HIAT:w" s="T242">hübeliːller</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_800" n="HIAT:w" s="T243">tojottor</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_803" n="HIAT:w" s="T244">araj</ts>
                  <nts id="Seg_804" n="HIAT:ip">.</nts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T253" id="Seg_807" n="HIAT:u" s="T245">
                  <nts id="Seg_808" n="HIAT:ip">"</nts>
                  <ts e="T246" id="Seg_810" n="HIAT:w" s="T245">Bu</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_813" n="HIAT:w" s="T246">ogonu</ts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_816" n="HIAT:w" s="T247">bu͡olla</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_819" n="HIAT:w" s="T248">Pöpügejge</ts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_822" n="HIAT:w" s="T249">ɨːtɨ͡akka</ts>
                  <nts id="Seg_823" n="HIAT:ip">"</nts>
                  <nts id="Seg_824" n="HIAT:ip">,</nts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_827" n="HIAT:w" s="T250">diːller</ts>
                  <nts id="Seg_828" n="HIAT:ip">,</nts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_830" n="HIAT:ip">"</nts>
                  <ts e="T253" id="Seg_832" n="HIAT:w" s="T251">uruːtugar</ts>
                  <nts id="Seg_833" n="HIAT:ip">.</nts>
                  <nts id="Seg_834" n="HIAT:ip">"</nts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T328" id="Seg_836" n="sc" s="T255">
               <ts e="T258" id="Seg_838" n="HIAT:u" s="T255">
                  <nts id="Seg_839" n="HIAT:ip">–</nts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_842" n="HIAT:w" s="T255">Mhm</ts>
                  <nts id="Seg_843" n="HIAT:ip">,</nts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_845" n="HIAT:ip">(</nts>
                  <ts e="T258" id="Seg_847" n="HIAT:w" s="T257">Keːtʼeni</ts>
                  <nts id="Seg_848" n="HIAT:ip">)</nts>
                  <nts id="Seg_849" n="HIAT:ip">.</nts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T270" id="Seg_852" n="HIAT:u" s="T258">
                  <ts e="T259" id="Seg_854" n="HIAT:w" s="T258">Biːr</ts>
                  <nts id="Seg_855" n="HIAT:ip">,</nts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_858" n="HIAT:w" s="T259">biːr</ts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_861" n="HIAT:w" s="T260">ogonu</ts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_864" n="HIAT:w" s="T261">bu͡olla</ts>
                  <nts id="Seg_865" n="HIAT:ip">,</nts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_868" n="HIAT:w" s="T262">munna</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_871" n="HIAT:w" s="T263">tɨ͡aga</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_874" n="HIAT:w" s="T264">baːr</ts>
                  <nts id="Seg_875" n="HIAT:ip">,</nts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_878" n="HIAT:w" s="T265">tundra</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_881" n="HIAT:w" s="T266">ogonnʼordoːk</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_884" n="HIAT:w" s="T267">emeːksin</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_887" n="HIAT:w" s="T268">uruːbut</ts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_890" n="HIAT:w" s="T269">araj</ts>
                  <nts id="Seg_891" n="HIAT:ip">.</nts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T273" id="Seg_894" n="HIAT:u" s="T270">
                  <ts e="T271" id="Seg_896" n="HIAT:w" s="T270">Dʼe</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_899" n="HIAT:w" s="T271">olorgo</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_902" n="HIAT:w" s="T272">bi͡erdiler</ts>
                  <nts id="Seg_903" n="HIAT:ip">.</nts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T291" id="Seg_906" n="HIAT:u" s="T273">
                  <ts e="T274" id="Seg_908" n="HIAT:w" s="T273">Oččogo</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_911" n="HIAT:w" s="T274">min</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_914" n="HIAT:w" s="T275">ikki</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_917" n="HIAT:w" s="T276">u͡ol</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_920" n="HIAT:w" s="T277">ogonu</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_923" n="HIAT:w" s="T278">gɨtta</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_926" n="HIAT:w" s="T279">kaːllɨm</ts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_929" n="HIAT:w" s="T280">bu͡o</ts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_932" n="HIAT:w" s="T281">araj</ts>
                  <nts id="Seg_933" n="HIAT:ip">,</nts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_936" n="HIAT:w" s="T282">itiler</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_939" n="HIAT:w" s="T283">mini͡eke</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_941" n="HIAT:ip">(</nts>
                  <ts e="T285" id="Seg_943" n="HIAT:w" s="T284">bar-</ts>
                  <nts id="Seg_944" n="HIAT:ip">)</nts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_947" n="HIAT:w" s="T285">barbattar</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_949" n="HIAT:ip">(</nts>
                  <ts e="T287" id="Seg_951" n="HIAT:w" s="T286">töttörü</ts>
                  <nts id="Seg_952" n="HIAT:ip">)</nts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_955" n="HIAT:w" s="T287">iti</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_958" n="HIAT:w" s="T288">ikki</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_961" n="HIAT:w" s="T289">u͡ol</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_964" n="HIAT:w" s="T290">ogo</ts>
                  <nts id="Seg_965" n="HIAT:ip">.</nts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T304" id="Seg_968" n="HIAT:u" s="T291">
                  <ts e="T292" id="Seg_970" n="HIAT:w" s="T291">Dʼe</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_973" n="HIAT:w" s="T292">onton</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_976" n="HIAT:w" s="T293">töhö</ts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_979" n="HIAT:w" s="T294">da</ts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_982" n="HIAT:w" s="T295">bu͡olbakka</ts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_985" n="HIAT:w" s="T296">kanʼikulga</ts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_988" n="HIAT:w" s="T297">kelbittere</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_991" n="HIAT:w" s="T298">ol</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_994" n="HIAT:w" s="T299">ogoloruŋ</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_997" n="HIAT:w" s="T300">bu͡o</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1000" n="HIAT:w" s="T301">töttörü</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1003" n="HIAT:w" s="T302">kelliler</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1006" n="HIAT:w" s="T303">bihi͡eke</ts>
                  <nts id="Seg_1007" n="HIAT:ip">.</nts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T308" id="Seg_1010" n="HIAT:u" s="T304">
                  <ts e="T305" id="Seg_1012" n="HIAT:w" s="T304">Ol</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1015" n="HIAT:w" s="T305">höbüleːbetter</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1018" n="HIAT:w" s="T306">ebit</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1021" n="HIAT:w" s="T307">oloru</ts>
                  <nts id="Seg_1022" n="HIAT:ip">.</nts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T328" id="Seg_1025" n="HIAT:u" s="T308">
                  <ts e="T309" id="Seg_1027" n="HIAT:w" s="T308">Onton</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1030" n="HIAT:w" s="T309">tundraga</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1033" n="HIAT:w" s="T310">ogonnʼordoːk</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1036" n="HIAT:w" s="T311">emeːksin</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1039" n="HIAT:w" s="T312">emi͡e</ts>
                  <nts id="Seg_1040" n="HIAT:ip">,</nts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1043" n="HIAT:w" s="T313">ol</ts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1046" n="HIAT:w" s="T314">barbɨt</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1049" n="HIAT:w" s="T315">ikki</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1052" n="HIAT:w" s="T316">ogobut</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1055" n="HIAT:w" s="T317">emi͡e</ts>
                  <nts id="Seg_1056" n="HIAT:ip">,</nts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1059" n="HIAT:w" s="T318">emi͡e</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1062" n="HIAT:w" s="T319">töttörü</ts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1065" n="HIAT:w" s="T320">kelle</ts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1068" n="HIAT:w" s="T321">bu͡o</ts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1071" n="HIAT:w" s="T322">oččogo</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1074" n="HIAT:w" s="T323">agɨs</ts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1077" n="HIAT:w" s="T324">ogoloːk</ts>
                  <nts id="Seg_1078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1080" n="HIAT:w" s="T325">bu͡ollum</ts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1083" n="HIAT:w" s="T326">bu͡o</ts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1086" n="HIAT:w" s="T327">min</ts>
                  <nts id="Seg_1087" n="HIAT:ip">.</nts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T416" id="Seg_1089" n="sc" s="T329">
               <ts e="T333" id="Seg_1091" n="HIAT:u" s="T329">
                  <ts e="T330" id="Seg_1093" n="HIAT:w" s="T329">Bejem</ts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1096" n="HIAT:w" s="T330">tü͡ört</ts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1099" n="HIAT:w" s="T331">ogoloːppun</ts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1102" n="HIAT:w" s="T332">bu͡olla</ts>
                  <nts id="Seg_1103" n="HIAT:ip">.</nts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T337" id="Seg_1106" n="HIAT:u" s="T333">
                  <ts e="T334" id="Seg_1108" n="HIAT:w" s="T333">Iti</ts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1111" n="HIAT:w" s="T334">ikki</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1114" n="HIAT:w" s="T335">kɨːstaːppɨn</ts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1117" n="HIAT:w" s="T336">bu͡olla</ts>
                  <nts id="Seg_1118" n="HIAT:ip">.</nts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_1121" n="HIAT:u" s="T337">
                  <ts e="T338" id="Seg_1123" n="HIAT:w" s="T337">Inʼem</ts>
                  <nts id="Seg_1124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1126" n="HIAT:w" s="T338">kɨrdʼagas</ts>
                  <nts id="Seg_1127" n="HIAT:ip">,</nts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1130" n="HIAT:w" s="T339">ubajɨm</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1133" n="HIAT:w" s="T340">kɨrdʼagas</ts>
                  <nts id="Seg_1134" n="HIAT:ip">,</nts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1137" n="HIAT:w" s="T341">onton</ts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1140" n="HIAT:w" s="T342">ol</ts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1143" n="HIAT:w" s="T343">agɨs</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1146" n="HIAT:w" s="T344">ogoloːkpun</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1149" n="HIAT:w" s="T345">bejem</ts>
                  <nts id="Seg_1150" n="HIAT:ip">.</nts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T355" id="Seg_1153" n="HIAT:u" s="T346">
                  <ts e="T347" id="Seg_1155" n="HIAT:w" s="T346">Edʼiːjim</ts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1158" n="HIAT:w" s="T347">gi͡ene</ts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1161" n="HIAT:w" s="T348">tü͡ört</ts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1164" n="HIAT:w" s="T349">ogo</ts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1167" n="HIAT:w" s="T350">kaːlla</ts>
                  <nts id="Seg_1168" n="HIAT:ip">,</nts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1171" n="HIAT:w" s="T351">mini͡ene</ts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1174" n="HIAT:w" s="T352">tü͡ört</ts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1177" n="HIAT:w" s="T353">ogo</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1180" n="HIAT:w" s="T354">bu͡olla</ts>
                  <nts id="Seg_1181" n="HIAT:ip">.</nts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T359" id="Seg_1184" n="HIAT:u" s="T355">
                  <ts e="T356" id="Seg_1186" n="HIAT:w" s="T355">Naːdʼa</ts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1189" n="HIAT:w" s="T356">paslʼednʼaja</ts>
                  <nts id="Seg_1190" n="HIAT:ip">,</nts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1193" n="HIAT:w" s="T357">malʼenkaja</ts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1196" n="HIAT:w" s="T358">bɨla</ts>
                  <nts id="Seg_1197" n="HIAT:ip">.</nts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T362" id="Seg_1200" n="HIAT:u" s="T359">
                  <ts e="T360" id="Seg_1202" n="HIAT:w" s="T359">Küččügüj</ts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1205" n="HIAT:w" s="T360">ete</ts>
                  <nts id="Seg_1206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1208" n="HIAT:w" s="T361">ol</ts>
                  <nts id="Seg_1209" n="HIAT:ip">.</nts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T373" id="Seg_1212" n="HIAT:u" s="T362">
                  <ts e="T363" id="Seg_1214" n="HIAT:w" s="T362">Dʼe</ts>
                  <nts id="Seg_1215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1217" n="HIAT:w" s="T363">ol</ts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1220" n="HIAT:w" s="T364">kaːlammɨn</ts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1223" n="HIAT:w" s="T365">dʼe</ts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1226" n="HIAT:w" s="T366">iti</ts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1229" n="HIAT:w" s="T367">olordubut</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1232" n="HIAT:w" s="T368">bu͡olla</ts>
                  <nts id="Seg_1233" n="HIAT:ip">,</nts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1236" n="HIAT:w" s="T369">inʼem</ts>
                  <nts id="Seg_1237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1239" n="HIAT:w" s="T370">kɨrdʼan</ts>
                  <nts id="Seg_1240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1242" n="HIAT:w" s="T371">öllö</ts>
                  <nts id="Seg_1243" n="HIAT:ip">,</nts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1246" n="HIAT:w" s="T372">ɨ͡aldʼan</ts>
                  <nts id="Seg_1247" n="HIAT:ip">.</nts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T383" id="Seg_1250" n="HIAT:u" s="T373">
                  <ts e="T374" id="Seg_1252" n="HIAT:w" s="T373">Ol</ts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1255" n="HIAT:w" s="T374">ubajɨm</ts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1258" n="HIAT:w" s="T375">emi͡e</ts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1261" n="HIAT:w" s="T376">dʼaktara</ts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1264" n="HIAT:w" s="T377">da</ts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1267" n="HIAT:w" s="T378">hu͡ok</ts>
                  <nts id="Seg_1268" n="HIAT:ip">,</nts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1271" n="HIAT:w" s="T379">tu͡ok</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1274" n="HIAT:w" s="T380">da</ts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1277" n="HIAT:w" s="T381">hu͡ok</ts>
                  <nts id="Seg_1278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1280" n="HIAT:w" s="T382">ete</ts>
                  <nts id="Seg_1281" n="HIAT:ip">.</nts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1284" n="HIAT:u" s="T383">
                  <ts e="T384" id="Seg_1286" n="HIAT:w" s="T383">Laːpkɨga</ts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1289" n="HIAT:w" s="T384">üleleːčči</ts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1292" n="HIAT:w" s="T385">ete</ts>
                  <nts id="Seg_1293" n="HIAT:ip">,</nts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1296" n="HIAT:w" s="T386">ol</ts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1299" n="HIAT:w" s="T387">ihin</ts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1302" n="HIAT:w" s="T388">ahɨnan</ts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1305" n="HIAT:w" s="T389">da</ts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1308" n="HIAT:w" s="T390">olus</ts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1311" n="HIAT:w" s="T391">da</ts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1314" n="HIAT:w" s="T392">kimneːččite</ts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1317" n="HIAT:w" s="T393">hu͡ok</ts>
                  <nts id="Seg_1318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1320" n="HIAT:w" s="T394">etibit</ts>
                  <nts id="Seg_1321" n="HIAT:ip">.</nts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T402" id="Seg_1324" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_1326" n="HIAT:w" s="T395">Üleliːr</ts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1329" n="HIAT:w" s="T396">bu͡olan</ts>
                  <nts id="Seg_1330" n="HIAT:ip">,</nts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1332" n="HIAT:ip">(</nts>
                  <nts id="Seg_1333" n="HIAT:ip">(</nts>
                  <ats e="T398" id="Seg_1334" n="HIAT:non-pho" s="T397">…</ats>
                  <nts id="Seg_1335" n="HIAT:ip">)</nts>
                  <nts id="Seg_1336" n="HIAT:ip">)</nts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1339" n="HIAT:w" s="T398">egelen</ts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1342" n="HIAT:w" s="T399">iheːčči</ts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1345" n="HIAT:w" s="T400">as</ts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1348" n="HIAT:w" s="T401">laːpkɨttan</ts>
                  <nts id="Seg_1349" n="HIAT:ip">.</nts>
                  <nts id="Seg_1350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T405" id="Seg_1352" n="HIAT:u" s="T402">
                  <ts e="T403" id="Seg_1354" n="HIAT:w" s="T402">Harpɨlaːkkaːn</ts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1356" n="HIAT:ip">(</nts>
                  <ts e="T404" id="Seg_1358" n="HIAT:w" s="T403">iti</ts>
                  <nts id="Seg_1359" n="HIAT:ip">)</nts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1362" n="HIAT:w" s="T404">ɨlaːččɨ</ts>
                  <nts id="Seg_1363" n="HIAT:ip">.</nts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T409" id="Seg_1366" n="HIAT:u" s="T405">
                  <ts e="T406" id="Seg_1368" n="HIAT:w" s="T405">Onton</ts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1371" n="HIAT:w" s="T406">min</ts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1374" n="HIAT:w" s="T407">poːvardiːbin</ts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1377" n="HIAT:w" s="T408">bu͡olla</ts>
                  <nts id="Seg_1378" n="HIAT:ip">.</nts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T416" id="Seg_1381" n="HIAT:u" s="T409">
                  <ts e="T410" id="Seg_1383" n="HIAT:w" s="T409">Ol</ts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1386" n="HIAT:w" s="T410">iti</ts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1389" n="HIAT:w" s="T411">olorbupput</ts>
                  <nts id="Seg_1390" n="HIAT:ip">,</nts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1393" n="HIAT:w" s="T412">barɨkaːttara</ts>
                  <nts id="Seg_1394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1396" n="HIAT:w" s="T413">ulaːppɨttara</ts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1399" n="HIAT:w" s="T414">eni</ts>
                  <nts id="Seg_1400" n="HIAT:ip">,</nts>
                  <nts id="Seg_1401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1403" n="HIAT:w" s="T415">dʼaktardaːktar</ts>
                  <nts id="Seg_1404" n="HIAT:ip">.</nts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T473" id="Seg_1406" n="sc" s="T421">
               <ts e="T427" id="Seg_1408" n="HIAT:u" s="T421">
                  <nts id="Seg_1409" n="HIAT:ip">–</nts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1412" n="HIAT:w" s="T421">Dʼe</ts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1415" n="HIAT:w" s="T422">taksɨbɨta</ts>
                  <nts id="Seg_1416" n="HIAT:ip">,</nts>
                  <nts id="Seg_1417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1418" n="HIAT:ip">(</nts>
                  <ts e="T424" id="Seg_1420" n="HIAT:w" s="T423">tu͡ok</ts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1423" n="HIAT:w" s="T424">gɨnaːrɨ</ts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1426" n="HIAT:w" s="T425">da</ts>
                  <nts id="Seg_1427" n="HIAT:ip">)</nts>
                  <nts id="Seg_1428" n="HIAT:ip">,</nts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1431" n="HIAT:w" s="T426">taksɨbɨt</ts>
                  <nts id="Seg_1432" n="HIAT:ip">.</nts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T434" id="Seg_1435" n="HIAT:u" s="T427">
                  <ts e="T428" id="Seg_1437" n="HIAT:w" s="T427">Dʼe</ts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1440" n="HIAT:w" s="T428">ol</ts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1443" n="HIAT:w" s="T429">ihin</ts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1446" n="HIAT:w" s="T430">agɨs</ts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1449" n="HIAT:w" s="T431">ogo</ts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1452" n="HIAT:w" s="T432">bu͡olar</ts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1455" n="HIAT:w" s="T433">bu͡olla</ts>
                  <nts id="Seg_1456" n="HIAT:ip">.</nts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T443" id="Seg_1459" n="HIAT:u" s="T434">
                  <ts e="T435" id="Seg_1461" n="HIAT:w" s="T434">Maladoj</ts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1464" n="HIAT:w" s="T435">dʼaktarbɨn</ts>
                  <nts id="Seg_1465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1467" n="HIAT:w" s="T436">bu͡ol</ts>
                  <nts id="Seg_1468" n="HIAT:ip">,</nts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1471" n="HIAT:w" s="T437">onno</ts>
                  <nts id="Seg_1472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1474" n="HIAT:w" s="T438">ol</ts>
                  <nts id="Seg_1475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1477" n="HIAT:w" s="T439">zamuž</ts>
                  <nts id="Seg_1478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1480" n="HIAT:w" s="T440">vɨšlʼi</ts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1483" n="HIAT:w" s="T441">etʼi</ts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1486" n="HIAT:w" s="T442">ke</ts>
                  <nts id="Seg_1487" n="HIAT:ip">.</nts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T451" id="Seg_1490" n="HIAT:u" s="T443">
                  <ts e="T444" id="Seg_1492" n="HIAT:w" s="T443">Erge</ts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1495" n="HIAT:w" s="T444">taksan</ts>
                  <nts id="Seg_1496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1498" n="HIAT:w" s="T445">kaːllɨm</ts>
                  <nts id="Seg_1499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1501" n="HIAT:w" s="T446">bu͡o</ts>
                  <nts id="Seg_1502" n="HIAT:ip">,</nts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1505" n="HIAT:w" s="T447">ikki</ts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1508" n="HIAT:w" s="T448">ogonu</ts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1510" n="HIAT:ip">(</nts>
                  <ts e="T450" id="Seg_1512" n="HIAT:w" s="T449">töröːbüt</ts>
                  <nts id="Seg_1513" n="HIAT:ip">)</nts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1516" n="HIAT:w" s="T450">bu͡olla</ts>
                  <nts id="Seg_1517" n="HIAT:ip">.</nts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T460" id="Seg_1520" n="HIAT:u" s="T451">
                  <ts e="T452" id="Seg_1522" n="HIAT:w" s="T451">Ol</ts>
                  <nts id="Seg_1523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1525" n="HIAT:w" s="T452">ühüs</ts>
                  <nts id="Seg_1526" n="HIAT:ip">,</nts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1529" n="HIAT:w" s="T453">ühüs</ts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1532" n="HIAT:w" s="T454">ogom</ts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1535" n="HIAT:w" s="T455">u͡ol</ts>
                  <nts id="Seg_1536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1538" n="HIAT:w" s="T456">ogo</ts>
                  <nts id="Seg_1539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1541" n="HIAT:w" s="T457">ete</ts>
                  <nts id="Seg_1542" n="HIAT:ip">,</nts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1545" n="HIAT:w" s="T458">ontum</ts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1548" n="HIAT:w" s="T459">tüspüte</ts>
                  <nts id="Seg_1549" n="HIAT:ip">.</nts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T469" id="Seg_1552" n="HIAT:u" s="T460">
                  <ts e="T461" id="Seg_1554" n="HIAT:w" s="T460">Ölörünemmin</ts>
                  <nts id="Seg_1555" n="HIAT:ip">,</nts>
                  <nts id="Seg_1556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1557" n="HIAT:ip">(</nts>
                  <ts e="T462" id="Seg_1559" n="HIAT:w" s="T461">saː-</ts>
                  <nts id="Seg_1560" n="HIAT:ip">)</nts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1563" n="HIAT:w" s="T462">saːdikka</ts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1566" n="HIAT:w" s="T463">buːstarɨ</ts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1569" n="HIAT:w" s="T464">uːgollarɨ</ts>
                  <nts id="Seg_1570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1572" n="HIAT:w" s="T465">taskajdaːn</ts>
                  <nts id="Seg_1573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1575" n="HIAT:w" s="T466">ol</ts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1578" n="HIAT:w" s="T467">tüspüt</ts>
                  <nts id="Seg_1579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1581" n="HIAT:w" s="T468">ete</ts>
                  <nts id="Seg_1582" n="HIAT:ip">.</nts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T473" id="Seg_1585" n="HIAT:u" s="T469">
                  <ts e="T470" id="Seg_1587" n="HIAT:w" s="T469">Iti</ts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1590" n="HIAT:w" s="T470">ikki</ts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1593" n="HIAT:w" s="T471">u͡ol</ts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1596" n="HIAT:w" s="T472">ɨkkardɨta</ts>
                  <nts id="Seg_1597" n="HIAT:ip">.</nts>
                  <nts id="Seg_1598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T524" id="Seg_1599" n="sc" s="T480">
               <ts e="T491" id="Seg_1601" n="HIAT:u" s="T480">
                  <nts id="Seg_1602" n="HIAT:ip">–</nts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1605" n="HIAT:w" s="T480">Ölöːnö</ts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1608" n="HIAT:w" s="T481">alta</ts>
                  <nts id="Seg_1609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1611" n="HIAT:w" s="T482">dʼɨllaːgɨ</ts>
                  <nts id="Seg_1612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1614" n="HIAT:w" s="T483">ɨlbɨppɨt</ts>
                  <nts id="Seg_1615" n="HIAT:ip">,</nts>
                  <nts id="Seg_1616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1618" n="HIAT:w" s="T484">onton</ts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1621" n="HIAT:w" s="T485">hette</ts>
                  <nts id="Seg_1622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1624" n="HIAT:w" s="T486">dʼɨllanarɨgar</ts>
                  <nts id="Seg_1625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1627" n="HIAT:w" s="T487">munna</ts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1630" n="HIAT:w" s="T488">kellim</ts>
                  <nts id="Seg_1631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1633" n="HIAT:w" s="T489">bu͡olla</ts>
                  <nts id="Seg_1634" n="HIAT:ip">,</nts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1637" n="HIAT:w" s="T490">Dudʼinkaga</ts>
                  <nts id="Seg_1638" n="HIAT:ip">.</nts>
                  <nts id="Seg_1639" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T504" id="Seg_1641" n="HIAT:u" s="T491">
                  <ts e="T492" id="Seg_1643" n="HIAT:w" s="T491">Ogom</ts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1646" n="HIAT:w" s="T492">dʼi͡e</ts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1649" n="HIAT:w" s="T493">ɨlar</ts>
                  <nts id="Seg_1650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1652" n="HIAT:w" s="T494">ebit</ts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1655" n="HIAT:w" s="T495">araj</ts>
                  <nts id="Seg_1656" n="HIAT:ip">,</nts>
                  <nts id="Seg_1657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1659" n="HIAT:w" s="T496">dʼe</ts>
                  <nts id="Seg_1660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1662" n="HIAT:w" s="T497">onuga</ts>
                  <nts id="Seg_1663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1665" n="HIAT:w" s="T498">prapʼiskalɨ͡an</ts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1668" n="HIAT:w" s="T499">naːda</ts>
                  <nts id="Seg_1669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1671" n="HIAT:w" s="T500">ebit</ts>
                  <nts id="Seg_1672" n="HIAT:ip">,</nts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1675" n="HIAT:w" s="T501">dʼe</ts>
                  <nts id="Seg_1676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1678" n="HIAT:w" s="T502">ol</ts>
                  <nts id="Seg_1679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1681" n="HIAT:w" s="T503">kelbitim</ts>
                  <nts id="Seg_1682" n="HIAT:ip">.</nts>
                  <nts id="Seg_1683" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T513" id="Seg_1685" n="HIAT:u" s="T504">
                  <ts e="T505" id="Seg_1687" n="HIAT:w" s="T504">Ol</ts>
                  <nts id="Seg_1688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1690" n="HIAT:w" s="T505">kelemmin</ts>
                  <nts id="Seg_1691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1693" n="HIAT:w" s="T506">bu͡o</ts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1696" n="HIAT:w" s="T507">ogom</ts>
                  <nts id="Seg_1697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1699" n="HIAT:w" s="T508">onus</ts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1702" n="HIAT:w" s="T509">kɨlaːs</ts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1705" n="HIAT:w" s="T510">ete</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1707" n="HIAT:ip">(</nts>
                  <nts id="Seg_1708" n="HIAT:ip">(</nts>
                  <ats e="T512" id="Seg_1709" n="HIAT:non-pho" s="T511">…</ats>
                  <nts id="Seg_1710" n="HIAT:ip">)</nts>
                  <nts id="Seg_1711" n="HIAT:ip">)</nts>
                  <nts id="Seg_1712" n="HIAT:ip">,</nts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1715" n="HIAT:w" s="T512">vot</ts>
                  <nts id="Seg_1716" n="HIAT:ip">.</nts>
                  <nts id="Seg_1717" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T517" id="Seg_1719" n="HIAT:u" s="T513">
                  <ts e="T514" id="Seg_1721" n="HIAT:w" s="T513">Maːmata</ts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1724" n="HIAT:w" s="T514">bu͡ollagɨna</ts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1727" n="HIAT:w" s="T515">Džakuskajga</ts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1730" n="HIAT:w" s="T516">erdemmite</ts>
                  <nts id="Seg_1731" n="HIAT:ip">.</nts>
                  <nts id="Seg_1732" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T524" id="Seg_1734" n="HIAT:u" s="T517">
                  <ts e="T518" id="Seg_1736" n="HIAT:w" s="T517">Ontuŋ</ts>
                  <nts id="Seg_1737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1739" n="HIAT:w" s="T518">anɨ</ts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1742" n="HIAT:w" s="T519">ikki</ts>
                  <nts id="Seg_1743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1745" n="HIAT:w" s="T520">ogoloːk</ts>
                  <nts id="Seg_1746" n="HIAT:ip">,</nts>
                  <nts id="Seg_1747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1749" n="HIAT:w" s="T521">u͡olu</ts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1752" n="HIAT:w" s="T522">gɨtta</ts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1755" n="HIAT:w" s="T523">kɨːs</ts>
                  <nts id="Seg_1756" n="HIAT:ip">.</nts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T534" id="Seg_1758" n="sc" s="T528">
               <ts e="T534" id="Seg_1760" n="HIAT:u" s="T528">
                  <nts id="Seg_1761" n="HIAT:ip">–</nts>
                  <nts id="Seg_1762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1764" n="HIAT:w" s="T528">Tɨ͡aga</ts>
                  <nts id="Seg_1765" n="HIAT:ip">,</nts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1768" n="HIAT:w" s="T529">tuːndraga</ts>
                  <nts id="Seg_1769" n="HIAT:ip">,</nts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1772" n="HIAT:w" s="T530">hɨldʼallar</ts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1775" n="HIAT:w" s="T531">tɨ͡aga</ts>
                  <nts id="Seg_1776" n="HIAT:ip">,</nts>
                  <nts id="Seg_1777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1779" n="HIAT:w" s="T532">tabahɨt</ts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1782" n="HIAT:w" s="T533">ere</ts>
                  <nts id="Seg_1783" n="HIAT:ip">.</nts>
                  <nts id="Seg_1784" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T557" id="Seg_1785" n="sc" s="T535">
               <ts e="T536" id="Seg_1787" n="HIAT:u" s="T535">
                  <ts e="T536" id="Seg_1789" n="HIAT:w" s="T535">Elete</ts>
                  <nts id="Seg_1790" n="HIAT:ip">.</nts>
                  <nts id="Seg_1791" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T544" id="Seg_1793" n="HIAT:u" s="T536">
                  <ts e="T537" id="Seg_1795" n="HIAT:w" s="T536">Hɨndaːskaga</ts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1798" n="HIAT:w" s="T537">kaːlbɨttara</ts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1801" n="HIAT:w" s="T538">anɨ</ts>
                  <nts id="Seg_1802" n="HIAT:ip">,</nts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1805" n="HIAT:w" s="T539">barɨ͡aktara</ts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1808" n="HIAT:w" s="T540">maːj</ts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1811" n="HIAT:w" s="T541">ɨjga</ts>
                  <nts id="Seg_1812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1814" n="HIAT:w" s="T542">sku͡ola</ts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_1817" n="HIAT:w" s="T543">bütteren</ts>
                  <nts id="Seg_1818" n="HIAT:ip">.</nts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T548" id="Seg_1821" n="HIAT:u" s="T544">
                  <ts e="T545" id="Seg_1823" n="HIAT:w" s="T544">Ogoloro</ts>
                  <nts id="Seg_1824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1826" n="HIAT:w" s="T545">itinne</ts>
                  <nts id="Seg_1827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_1829" n="HIAT:w" s="T546">ü͡öreneller</ts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1832" n="HIAT:w" s="T547">Hɨndaːskaga</ts>
                  <nts id="Seg_1833" n="HIAT:ip">.</nts>
                  <nts id="Seg_1834" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T557" id="Seg_1836" n="HIAT:u" s="T548">
                  <ts e="T549" id="Seg_1838" n="HIAT:w" s="T548">Biːrgehe</ts>
                  <nts id="Seg_1839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_1841" n="HIAT:w" s="T549">tördüs</ts>
                  <nts id="Seg_1842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_1844" n="HIAT:w" s="T550">kɨlaːs</ts>
                  <nts id="Seg_1845" n="HIAT:ip">,</nts>
                  <nts id="Seg_1846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_1848" n="HIAT:w" s="T551">biːrgehe</ts>
                  <nts id="Seg_1849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_1851" n="HIAT:w" s="T552">ühüs</ts>
                  <nts id="Seg_1852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_1854" n="HIAT:w" s="T553">kɨlas</ts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_1857" n="HIAT:w" s="T554">du͡o</ts>
                  <nts id="Seg_1858" n="HIAT:ip">,</nts>
                  <nts id="Seg_1859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_1861" n="HIAT:w" s="T555">ikkis</ts>
                  <nts id="Seg_1862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_1864" n="HIAT:w" s="T556">du͡o</ts>
                  <nts id="Seg_1865" n="HIAT:ip">.</nts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T574" id="Seg_1867" n="sc" s="T558">
               <ts e="T562" id="Seg_1869" n="HIAT:u" s="T558">
                  <ts e="T559" id="Seg_1871" n="HIAT:w" s="T558">Ikkis</ts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_1874" n="HIAT:w" s="T559">kɨlas</ts>
                  <nts id="Seg_1875" n="HIAT:ip">,</nts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_1878" n="HIAT:w" s="T560">eː</ts>
                  <nts id="Seg_1879" n="HIAT:ip">,</nts>
                  <nts id="Seg_1880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_1882" n="HIAT:w" s="T561">kɨːha</ts>
                  <nts id="Seg_1883" n="HIAT:ip">.</nts>
                  <nts id="Seg_1884" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T571" id="Seg_1886" n="HIAT:u" s="T562">
                  <ts e="T563" id="Seg_1888" n="HIAT:w" s="T562">Anɨ</ts>
                  <nts id="Seg_1889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_1891" n="HIAT:w" s="T563">min</ts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_1894" n="HIAT:w" s="T564">di͡eber</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_1897" n="HIAT:w" s="T565">kaːlbɨttara</ts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_1900" n="HIAT:w" s="T566">anɨ</ts>
                  <nts id="Seg_1901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_1903" n="HIAT:w" s="T567">maːjga</ts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_1906" n="HIAT:w" s="T568">barɨ͡aktara</ts>
                  <nts id="Seg_1907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_1909" n="HIAT:w" s="T569">anɨ</ts>
                  <nts id="Seg_1910" n="HIAT:ip">,</nts>
                  <nts id="Seg_1911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_1913" n="HIAT:w" s="T570">abratna</ts>
                  <nts id="Seg_1914" n="HIAT:ip">.</nts>
                  <nts id="Seg_1915" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T573" id="Seg_1917" n="HIAT:u" s="T571">
                  <ts e="T572" id="Seg_1919" n="HIAT:w" s="T571">Köhön</ts>
                  <nts id="Seg_1920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_1922" n="HIAT:w" s="T572">barɨ͡aktara</ts>
                  <nts id="Seg_1923" n="HIAT:ip">.</nts>
                  <nts id="Seg_1924" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T574" id="Seg_1926" n="HIAT:u" s="T573">
                  <ts e="T574" id="Seg_1928" n="HIAT:w" s="T573">Elete</ts>
                  <nts id="Seg_1929" n="HIAT:ip">.</nts>
                  <nts id="Seg_1930" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KiES">
            <ts e="T58" id="Seg_1931" n="sc" s="T1">
               <ts e="T2" id="Seg_1933" n="e" s="T1">– Dʼe </ts>
               <ts e="T3" id="Seg_1935" n="e" s="T2">Korgoːgo </ts>
               <ts e="T4" id="Seg_1937" n="e" s="T3">baːr </ts>
               <ts e="T5" id="Seg_1939" n="e" s="T4">etibit </ts>
               <ts e="T6" id="Seg_1941" n="e" s="T5">hirdeːk </ts>
               <ts e="T7" id="Seg_1943" n="e" s="T6">etibit </ts>
               <ts e="T8" id="Seg_1945" n="e" s="T7">ginilerge </ts>
               <ts e="T9" id="Seg_1947" n="e" s="T8">Korgoːgo </ts>
               <ts e="T11" id="Seg_1949" n="e" s="T9">hirdemmitim. </ts>
               <ts e="T12" id="Seg_1951" n="e" s="T11">Onton </ts>
               <ts e="T13" id="Seg_1953" n="e" s="T12">bu͡ollagɨna </ts>
               <ts e="T14" id="Seg_1955" n="e" s="T13">bu, </ts>
               <ts e="T15" id="Seg_1957" n="e" s="T14">eː </ts>
               <ts e="T16" id="Seg_1959" n="e" s="T15">bu </ts>
               <ts e="T17" id="Seg_1961" n="e" s="T16">ogonu </ts>
               <ts e="T18" id="Seg_1963" n="e" s="T17">töröːbütüm, </ts>
               <ts e="T19" id="Seg_1965" n="e" s="T18">onton </ts>
               <ts e="T20" id="Seg_1967" n="e" s="T19">ol </ts>
               <ts e="T21" id="Seg_1969" n="e" s="T20">u͡ol </ts>
               <ts e="T22" id="Seg_1971" n="e" s="T21">ogom </ts>
               <ts e="T23" id="Seg_1973" n="e" s="T22">töröːmmün </ts>
               <ts e="T24" id="Seg_1975" n="e" s="T23">ol. </ts>
               <ts e="T25" id="Seg_1977" n="e" s="T24">Onton </ts>
               <ts e="T26" id="Seg_1979" n="e" s="T25">bu͡ollagɨna </ts>
               <ts e="T27" id="Seg_1981" n="e" s="T26">ogonnʼorbut </ts>
               <ts e="T28" id="Seg_1983" n="e" s="T27">ɨ͡aldʼar </ts>
               <ts e="T29" id="Seg_1985" n="e" s="T28">ete </ts>
               <ts e="T30" id="Seg_1987" n="e" s="T29">ölön, </ts>
               <ts e="T31" id="Seg_1989" n="e" s="T30">ölön </ts>
               <ts e="T32" id="Seg_1991" n="e" s="T31">ol </ts>
               <ts e="T33" id="Seg_1993" n="e" s="T32">kimi͡eke </ts>
               <ts e="T34" id="Seg_1995" n="e" s="T33">Hɨndaːskaga </ts>
               <ts e="T35" id="Seg_1997" n="e" s="T34">hɨppɨppɨt </ts>
               <ts e="T36" id="Seg_1999" n="e" s="T35">ol. </ts>
               <ts e="T37" id="Seg_2001" n="e" s="T36">Onton </ts>
               <ts e="T38" id="Seg_2003" n="e" s="T37">ülem </ts>
               <ts e="T39" id="Seg_2005" n="e" s="T38">minigin </ts>
               <ts e="T40" id="Seg_2007" n="e" s="T39">načʼalnʼikʼi </ts>
               <ts e="T41" id="Seg_2009" n="e" s="T40">zaabrazalʼi </ts>
               <ts e="T42" id="Seg_2011" n="e" s="T41">eti </ts>
               <ts e="T43" id="Seg_2013" n="e" s="T42">bu </ts>
               <ts e="T44" id="Seg_2015" n="e" s="T43">ke </ts>
               <ts e="T45" id="Seg_2017" n="e" s="T44">kak </ts>
               <ts e="T46" id="Seg_2019" n="e" s="T45">budu </ts>
               <ts e="T47" id="Seg_2021" n="e" s="T46">eti, </ts>
               <ts e="T48" id="Seg_2023" n="e" s="T47">onton </ts>
               <ts e="T49" id="Seg_2025" n="e" s="T48">üle </ts>
               <ts e="T50" id="Seg_2027" n="e" s="T49">bi͡erbittere </ts>
               <ts e="T51" id="Seg_2029" n="e" s="T50">mi͡eke </ts>
               <ts e="T52" id="Seg_2031" n="e" s="T51">saːdikka, </ts>
               <ts e="T53" id="Seg_2033" n="e" s="T52">sadʼik </ts>
               <ts e="T54" id="Seg_2035" n="e" s="T53">pervɨj </ts>
               <ts e="T55" id="Seg_2037" n="e" s="T54">raz </ts>
               <ts e="T56" id="Seg_2039" n="e" s="T55">bɨl </ts>
               <ts e="T57" id="Seg_2041" n="e" s="T56">v </ts>
               <ts e="T58" id="Seg_2043" n="e" s="T57">Sɨndasske. </ts>
            </ts>
            <ts e="T90" id="Seg_2044" n="sc" s="T63">
               <ts e="T64" id="Seg_2046" n="e" s="T63">– Eː, </ts>
               <ts e="T65" id="Seg_2048" n="e" s="T64">hakalɨː </ts>
               <ts e="T66" id="Seg_2050" n="e" s="T65">du͡o. </ts>
               <ts e="T67" id="Seg_2052" n="e" s="T66">Hɨndaːskaga </ts>
               <ts e="T68" id="Seg_2054" n="e" s="T67">(haka) </ts>
               <ts e="T69" id="Seg_2056" n="e" s="T68">haŋardɨː </ts>
               <ts e="T70" id="Seg_2058" n="e" s="T69">saːdik </ts>
               <ts e="T71" id="Seg_2060" n="e" s="T70">turbut </ts>
               <ts e="T72" id="Seg_2062" n="e" s="T71">ete. </ts>
               <ts e="T73" id="Seg_2064" n="e" s="T72">Dʼe </ts>
               <ts e="T74" id="Seg_2066" n="e" s="T73">tugu </ts>
               <ts e="T75" id="Seg_2068" n="e" s="T74">da </ts>
               <ts e="T76" id="Seg_2070" n="e" s="T75">bilpeppin, </ts>
               <ts e="T77" id="Seg_2072" n="e" s="T76">saːdik, </ts>
               <ts e="T78" id="Seg_2074" n="e" s="T77">tu͡ok </ts>
               <ts e="T79" id="Seg_2076" n="e" s="T78">aːtaj? </ts>
               <ts e="T80" id="Seg_2078" n="e" s="T79">Ničevo </ts>
               <ts e="T81" id="Seg_2080" n="e" s="T80">nʼe </ts>
               <ts e="T82" id="Seg_2082" n="e" s="T81">znajem </ts>
               <ts e="T83" id="Seg_2084" n="e" s="T82">mɨ </ts>
               <ts e="T84" id="Seg_2086" n="e" s="T83">tundravʼičkʼi. </ts>
               <ts e="T85" id="Seg_2088" n="e" s="T84">Vsʼo </ts>
               <ts e="T86" id="Seg_2090" n="e" s="T85">takoje </ts>
               <ts e="T87" id="Seg_2092" n="e" s="T86">eta </ts>
               <ts e="T88" id="Seg_2094" n="e" s="T87">karmʼit, </ts>
               <ts e="T90" id="Seg_2096" n="e" s="T88">kak… </ts>
            </ts>
            <ts e="T148" id="Seg_2097" n="sc" s="T91">
               <ts e="T92" id="Seg_2099" n="e" s="T91">– Oː </ts>
               <ts e="T93" id="Seg_2101" n="e" s="T92">((…)) </ts>
               <ts e="T94" id="Seg_2103" n="e" s="T93">dʼe </ts>
               <ts e="T95" id="Seg_2105" n="e" s="T94">kajdak </ts>
               <ts e="T96" id="Seg_2107" n="e" s="T95">bu͡olu͡okputuj </ts>
               <ts e="T97" id="Seg_2109" n="e" s="T96">((…)). </ts>
               <ts e="T98" id="Seg_2111" n="e" s="T97">Hataːn </ts>
               <ts e="T99" id="Seg_2113" n="e" s="T98">haŋarbappɨn, </ts>
               <ts e="T100" id="Seg_2115" n="e" s="T99">ogolorgo. </ts>
               <ts e="T101" id="Seg_2117" n="e" s="T100">Če </ts>
               <ts e="T102" id="Seg_2119" n="e" s="T101">ogolorgo </ts>
               <ts e="T103" id="Seg_2121" n="e" s="T102">hataːn </ts>
               <ts e="T104" id="Seg_2123" n="e" s="T103">buharbappɨn </ts>
               <ts e="T105" id="Seg_2125" n="e" s="T104">diːbin. </ts>
               <ts e="T106" id="Seg_2127" n="e" s="T105">Hu͡ok </ts>
               <ts e="T107" id="Seg_2129" n="e" s="T106">povardar </ts>
               <ts e="T108" id="Seg_2131" n="e" s="T107">diːller </ts>
               <ts e="T109" id="Seg_2133" n="e" s="T108">bu͡o </ts>
               <ts e="T110" id="Seg_2135" n="e" s="T109">bu </ts>
               <ts e="T111" id="Seg_2137" n="e" s="T110">saːdik </ts>
               <ts e="T112" id="Seg_2139" n="e" s="T111">turda </ts>
               <ts e="T113" id="Seg_2141" n="e" s="T112">haŋa </ts>
               <ts e="T114" id="Seg_2143" n="e" s="T113">saːdikka. </ts>
               <ts e="T115" id="Seg_2145" n="e" s="T114">Onuga </ts>
               <ts e="T116" id="Seg_2147" n="e" s="T115">poːvar, </ts>
               <ts e="T117" id="Seg_2149" n="e" s="T116">poːvar </ts>
               <ts e="T118" id="Seg_2151" n="e" s="T117">naːda </ts>
               <ts e="T119" id="Seg_2153" n="e" s="T118">diːller </ts>
               <ts e="T120" id="Seg_2155" n="e" s="T119">tojottor. </ts>
               <ts e="T121" id="Seg_2157" n="e" s="T120">Innʼe </ts>
               <ts e="T122" id="Seg_2159" n="e" s="T121">tojottor </ts>
               <ts e="T123" id="Seg_2161" n="e" s="T122">minigin </ts>
               <ts e="T124" id="Seg_2163" n="e" s="T123">poːvar </ts>
               <ts e="T125" id="Seg_2165" n="e" s="T124">uːrdular </ts>
               <ts e="T126" id="Seg_2167" n="e" s="T125">bu͡olla. </ts>
               <ts e="T127" id="Seg_2169" n="e" s="T126">Dʼe </ts>
               <ts e="T128" id="Seg_2171" n="e" s="T127">ol </ts>
               <ts e="T129" id="Seg_2173" n="e" s="T128">hɨppɨppɨt </ts>
               <ts e="T130" id="Seg_2175" n="e" s="T129">onton </ts>
               <ts e="T131" id="Seg_2177" n="e" s="T130">ol </ts>
               <ts e="T132" id="Seg_2179" n="e" s="T131">poːvardaːmmɨn </ts>
               <ts e="T133" id="Seg_2181" n="e" s="T132">bu͡ollagɨna, </ts>
               <ts e="T134" id="Seg_2183" n="e" s="T133">eː, </ts>
               <ts e="T135" id="Seg_2185" n="e" s="T134">kimi͡eke </ts>
               <ts e="T136" id="Seg_2187" n="e" s="T135">bu </ts>
               <ts e="T137" id="Seg_2189" n="e" s="T136">ke </ts>
               <ts e="T138" id="Seg_2191" n="e" s="T137">iti </ts>
               <ts e="T139" id="Seg_2193" n="e" s="T138">ikki </ts>
               <ts e="T140" id="Seg_2195" n="e" s="T139">ogom </ts>
               <ts e="T141" id="Seg_2197" n="e" s="T140">hɨldʼallar </ts>
               <ts e="T142" id="Seg_2199" n="e" s="T141">saːdikka </ts>
               <ts e="T143" id="Seg_2201" n="e" s="T142">araj, </ts>
               <ts e="T144" id="Seg_2203" n="e" s="T143">heː. </ts>
               <ts e="T145" id="Seg_2205" n="e" s="T144">Dʼe </ts>
               <ts e="T146" id="Seg_2207" n="e" s="T145">ol </ts>
               <ts e="T147" id="Seg_2209" n="e" s="T146">hɨldʼannar </ts>
               <ts e="T148" id="Seg_2211" n="e" s="T147">((…)). </ts>
            </ts>
            <ts e="T152" id="Seg_2212" n="sc" s="T150">
               <ts e="T151" id="Seg_2214" n="e" s="T150">Kimi </ts>
               <ts e="T152" id="Seg_2216" n="e" s="T151">gɨtta? </ts>
            </ts>
            <ts e="T166" id="Seg_2217" n="sc" s="T156">
               <ts e="T157" id="Seg_2219" n="e" s="T156">Togo, </ts>
               <ts e="T158" id="Seg_2221" n="e" s="T157">ol </ts>
               <ts e="T159" id="Seg_2223" n="e" s="T158">onton </ts>
               <ts e="T160" id="Seg_2225" n="e" s="T159">ol </ts>
               <ts e="T161" id="Seg_2227" n="e" s="T160">kojut </ts>
               <ts e="T162" id="Seg_2229" n="e" s="T161">ol </ts>
               <ts e="T163" id="Seg_2231" n="e" s="T162">kimim, </ts>
               <ts e="T164" id="Seg_2233" n="e" s="T163">ol </ts>
               <ts e="T165" id="Seg_2235" n="e" s="T164">edʼijim </ts>
               <ts e="T166" id="Seg_2237" n="e" s="T165">ölbüte. </ts>
            </ts>
            <ts e="T225" id="Seg_2238" n="sc" s="T168">
               <ts e="T171" id="Seg_2240" n="e" s="T168">– Dʼe. </ts>
               <ts e="T172" id="Seg_2242" n="e" s="T171">Ijem, </ts>
               <ts e="T173" id="Seg_2244" n="e" s="T172">ijem </ts>
               <ts e="T174" id="Seg_2246" n="e" s="T173">baltɨta </ts>
               <ts e="T175" id="Seg_2248" n="e" s="T174">ölbüte. </ts>
               <ts e="T176" id="Seg_2250" n="e" s="T175">ɨ͡aldʼan, </ts>
               <ts e="T177" id="Seg_2252" n="e" s="T176">oččogo </ts>
               <ts e="T178" id="Seg_2254" n="e" s="T177">tü͡ört </ts>
               <ts e="T179" id="Seg_2256" n="e" s="T178">ogoloːk </ts>
               <ts e="T180" id="Seg_2258" n="e" s="T179">kaːlla </ts>
               <ts e="T181" id="Seg_2260" n="e" s="T180">bu </ts>
               <ts e="T182" id="Seg_2262" n="e" s="T181">kihi. </ts>
               <ts e="T183" id="Seg_2264" n="e" s="T182">Tü͡ört </ts>
               <ts e="T184" id="Seg_2266" n="e" s="T183">ogo </ts>
               <ts e="T185" id="Seg_2268" n="e" s="T184">bu͡olla </ts>
               <ts e="T186" id="Seg_2270" n="e" s="T185">mini͡eke </ts>
               <ts e="T187" id="Seg_2272" n="e" s="T186">kelliler </ts>
               <ts e="T188" id="Seg_2274" n="e" s="T187">bu͡olla. </ts>
               <ts e="T189" id="Seg_2276" n="e" s="T188">"Bu͡o </ts>
               <ts e="T190" id="Seg_2278" n="e" s="T189">edʼiːjbitiger </ts>
               <ts e="T191" id="Seg_2280" n="e" s="T190">bu͡olu͡okput </ts>
               <ts e="T192" id="Seg_2282" n="e" s="T191">bihigi, </ts>
               <ts e="T193" id="Seg_2284" n="e" s="T192">kajdi͡et </ts>
               <ts e="T194" id="Seg_2286" n="e" s="T193">da </ts>
               <ts e="T195" id="Seg_2288" n="e" s="T194">barbappɨt", </ts>
               <ts e="T196" id="Seg_2290" n="e" s="T195">diːller. </ts>
               <ts e="T197" id="Seg_2292" n="e" s="T196">Inʼem, </ts>
               <ts e="T198" id="Seg_2294" n="e" s="T197">eː, </ts>
               <ts e="T199" id="Seg_2296" n="e" s="T198">(inʼe-) </ts>
               <ts e="T200" id="Seg_2298" n="e" s="T199">inʼete </ts>
               <ts e="T201" id="Seg_2300" n="e" s="T200">geri͡es </ts>
               <ts e="T202" id="Seg_2302" n="e" s="T201">eppit </ts>
               <ts e="T203" id="Seg_2304" n="e" s="T202">bu͡olla, </ts>
               <ts e="T204" id="Seg_2306" n="e" s="T203">"bu </ts>
               <ts e="T205" id="Seg_2308" n="e" s="T204">ke </ts>
               <ts e="T206" id="Seg_2310" n="e" s="T205">edʼiːjgitiger </ts>
               <ts e="T207" id="Seg_2312" n="e" s="T206">kaːlan" </ts>
               <ts e="T208" id="Seg_2314" n="e" s="T207">di͡ebit </ts>
               <ts e="T209" id="Seg_2316" n="e" s="T208">bu͡olla </ts>
               <ts e="T210" id="Seg_2318" n="e" s="T209">"min </ts>
               <ts e="T211" id="Seg_2320" n="e" s="T210">kajdak </ts>
               <ts e="T212" id="Seg_2322" n="e" s="T211">bu͡ollakpɨna". </ts>
               <ts e="T213" id="Seg_2324" n="e" s="T212">Dʼe </ts>
               <ts e="T214" id="Seg_2326" n="e" s="T213">ol </ts>
               <ts e="T215" id="Seg_2328" n="e" s="T214">ol </ts>
               <ts e="T216" id="Seg_2330" n="e" s="T215">tü͡ört, </ts>
               <ts e="T217" id="Seg_2332" n="e" s="T216">tü͡ört </ts>
               <ts e="T218" id="Seg_2334" n="e" s="T217">ogo </ts>
               <ts e="T219" id="Seg_2336" n="e" s="T218">bihi͡eke </ts>
               <ts e="T220" id="Seg_2338" n="e" s="T219">kelliler </ts>
               <ts e="T221" id="Seg_2340" n="e" s="T220">araj, </ts>
               <ts e="T222" id="Seg_2342" n="e" s="T221">mhm. </ts>
               <ts e="T223" id="Seg_2344" n="e" s="T222">Nosku͡o </ts>
               <ts e="T224" id="Seg_2346" n="e" s="T223">ü͡örene </ts>
               <ts e="T225" id="Seg_2348" n="e" s="T224">barallar. </ts>
            </ts>
            <ts e="T233" id="Seg_2349" n="sc" s="T230">
               <ts e="T231" id="Seg_2351" n="e" s="T230">– Dʼe </ts>
               <ts e="T232" id="Seg_2353" n="e" s="T231">beːj </ts>
               <ts e="T233" id="Seg_2355" n="e" s="T232">onton. </ts>
            </ts>
            <ts e="T253" id="Seg_2356" n="sc" s="T236">
               <ts e="T237" id="Seg_2358" n="e" s="T236">Dʼe </ts>
               <ts e="T238" id="Seg_2360" n="e" s="T237">onton </ts>
               <ts e="T239" id="Seg_2362" n="e" s="T238">(kanʼikulga) </ts>
               <ts e="T240" id="Seg_2364" n="e" s="T239">keleller </ts>
               <ts e="T241" id="Seg_2366" n="e" s="T240">bu </ts>
               <ts e="T242" id="Seg_2368" n="e" s="T241">ogolor, </ts>
               <ts e="T243" id="Seg_2370" n="e" s="T242">hübeliːller </ts>
               <ts e="T244" id="Seg_2372" n="e" s="T243">tojottor </ts>
               <ts e="T245" id="Seg_2374" n="e" s="T244">araj. </ts>
               <ts e="T246" id="Seg_2376" n="e" s="T245">"Bu </ts>
               <ts e="T247" id="Seg_2378" n="e" s="T246">ogonu </ts>
               <ts e="T248" id="Seg_2380" n="e" s="T247">bu͡olla </ts>
               <ts e="T249" id="Seg_2382" n="e" s="T248">Pöpügejge </ts>
               <ts e="T250" id="Seg_2384" n="e" s="T249">ɨːtɨ͡akka", </ts>
               <ts e="T251" id="Seg_2386" n="e" s="T250">diːller, </ts>
               <ts e="T253" id="Seg_2388" n="e" s="T251">"uruːtugar." </ts>
            </ts>
            <ts e="T328" id="Seg_2389" n="sc" s="T255">
               <ts e="T257" id="Seg_2391" n="e" s="T255">– Mhm, </ts>
               <ts e="T258" id="Seg_2393" n="e" s="T257">(Keːtʼeni). </ts>
               <ts e="T259" id="Seg_2395" n="e" s="T258">Biːr, </ts>
               <ts e="T260" id="Seg_2397" n="e" s="T259">biːr </ts>
               <ts e="T261" id="Seg_2399" n="e" s="T260">ogonu </ts>
               <ts e="T262" id="Seg_2401" n="e" s="T261">bu͡olla, </ts>
               <ts e="T263" id="Seg_2403" n="e" s="T262">munna </ts>
               <ts e="T264" id="Seg_2405" n="e" s="T263">tɨ͡aga </ts>
               <ts e="T265" id="Seg_2407" n="e" s="T264">baːr, </ts>
               <ts e="T266" id="Seg_2409" n="e" s="T265">tundra </ts>
               <ts e="T267" id="Seg_2411" n="e" s="T266">ogonnʼordoːk </ts>
               <ts e="T268" id="Seg_2413" n="e" s="T267">emeːksin </ts>
               <ts e="T269" id="Seg_2415" n="e" s="T268">uruːbut </ts>
               <ts e="T270" id="Seg_2417" n="e" s="T269">araj. </ts>
               <ts e="T271" id="Seg_2419" n="e" s="T270">Dʼe </ts>
               <ts e="T272" id="Seg_2421" n="e" s="T271">olorgo </ts>
               <ts e="T273" id="Seg_2423" n="e" s="T272">bi͡erdiler. </ts>
               <ts e="T274" id="Seg_2425" n="e" s="T273">Oččogo </ts>
               <ts e="T275" id="Seg_2427" n="e" s="T274">min </ts>
               <ts e="T276" id="Seg_2429" n="e" s="T275">ikki </ts>
               <ts e="T277" id="Seg_2431" n="e" s="T276">u͡ol </ts>
               <ts e="T278" id="Seg_2433" n="e" s="T277">ogonu </ts>
               <ts e="T279" id="Seg_2435" n="e" s="T278">gɨtta </ts>
               <ts e="T280" id="Seg_2437" n="e" s="T279">kaːllɨm </ts>
               <ts e="T281" id="Seg_2439" n="e" s="T280">bu͡o </ts>
               <ts e="T282" id="Seg_2441" n="e" s="T281">araj, </ts>
               <ts e="T283" id="Seg_2443" n="e" s="T282">itiler </ts>
               <ts e="T284" id="Seg_2445" n="e" s="T283">mini͡eke </ts>
               <ts e="T285" id="Seg_2447" n="e" s="T284">(bar-) </ts>
               <ts e="T286" id="Seg_2449" n="e" s="T285">barbattar </ts>
               <ts e="T287" id="Seg_2451" n="e" s="T286">(töttörü) </ts>
               <ts e="T288" id="Seg_2453" n="e" s="T287">iti </ts>
               <ts e="T289" id="Seg_2455" n="e" s="T288">ikki </ts>
               <ts e="T290" id="Seg_2457" n="e" s="T289">u͡ol </ts>
               <ts e="T291" id="Seg_2459" n="e" s="T290">ogo. </ts>
               <ts e="T292" id="Seg_2461" n="e" s="T291">Dʼe </ts>
               <ts e="T293" id="Seg_2463" n="e" s="T292">onton </ts>
               <ts e="T294" id="Seg_2465" n="e" s="T293">töhö </ts>
               <ts e="T295" id="Seg_2467" n="e" s="T294">da </ts>
               <ts e="T296" id="Seg_2469" n="e" s="T295">bu͡olbakka </ts>
               <ts e="T297" id="Seg_2471" n="e" s="T296">kanʼikulga </ts>
               <ts e="T298" id="Seg_2473" n="e" s="T297">kelbittere </ts>
               <ts e="T299" id="Seg_2475" n="e" s="T298">ol </ts>
               <ts e="T300" id="Seg_2477" n="e" s="T299">ogoloruŋ </ts>
               <ts e="T301" id="Seg_2479" n="e" s="T300">bu͡o </ts>
               <ts e="T302" id="Seg_2481" n="e" s="T301">töttörü </ts>
               <ts e="T303" id="Seg_2483" n="e" s="T302">kelliler </ts>
               <ts e="T304" id="Seg_2485" n="e" s="T303">bihi͡eke. </ts>
               <ts e="T305" id="Seg_2487" n="e" s="T304">Ol </ts>
               <ts e="T306" id="Seg_2489" n="e" s="T305">höbüleːbetter </ts>
               <ts e="T307" id="Seg_2491" n="e" s="T306">ebit </ts>
               <ts e="T308" id="Seg_2493" n="e" s="T307">oloru. </ts>
               <ts e="T309" id="Seg_2495" n="e" s="T308">Onton </ts>
               <ts e="T310" id="Seg_2497" n="e" s="T309">tundraga </ts>
               <ts e="T311" id="Seg_2499" n="e" s="T310">ogonnʼordoːk </ts>
               <ts e="T312" id="Seg_2501" n="e" s="T311">emeːksin </ts>
               <ts e="T313" id="Seg_2503" n="e" s="T312">emi͡e, </ts>
               <ts e="T314" id="Seg_2505" n="e" s="T313">ol </ts>
               <ts e="T315" id="Seg_2507" n="e" s="T314">barbɨt </ts>
               <ts e="T316" id="Seg_2509" n="e" s="T315">ikki </ts>
               <ts e="T317" id="Seg_2511" n="e" s="T316">ogobut </ts>
               <ts e="T318" id="Seg_2513" n="e" s="T317">emi͡e, </ts>
               <ts e="T319" id="Seg_2515" n="e" s="T318">emi͡e </ts>
               <ts e="T320" id="Seg_2517" n="e" s="T319">töttörü </ts>
               <ts e="T321" id="Seg_2519" n="e" s="T320">kelle </ts>
               <ts e="T322" id="Seg_2521" n="e" s="T321">bu͡o </ts>
               <ts e="T323" id="Seg_2523" n="e" s="T322">oččogo </ts>
               <ts e="T324" id="Seg_2525" n="e" s="T323">agɨs </ts>
               <ts e="T325" id="Seg_2527" n="e" s="T324">ogoloːk </ts>
               <ts e="T326" id="Seg_2529" n="e" s="T325">bu͡ollum </ts>
               <ts e="T327" id="Seg_2531" n="e" s="T326">bu͡o </ts>
               <ts e="T328" id="Seg_2533" n="e" s="T327">min. </ts>
            </ts>
            <ts e="T416" id="Seg_2534" n="sc" s="T329">
               <ts e="T330" id="Seg_2536" n="e" s="T329">Bejem </ts>
               <ts e="T331" id="Seg_2538" n="e" s="T330">tü͡ört </ts>
               <ts e="T332" id="Seg_2540" n="e" s="T331">ogoloːppun </ts>
               <ts e="T333" id="Seg_2542" n="e" s="T332">bu͡olla. </ts>
               <ts e="T334" id="Seg_2544" n="e" s="T333">Iti </ts>
               <ts e="T335" id="Seg_2546" n="e" s="T334">ikki </ts>
               <ts e="T336" id="Seg_2548" n="e" s="T335">kɨːstaːppɨn </ts>
               <ts e="T337" id="Seg_2550" n="e" s="T336">bu͡olla. </ts>
               <ts e="T338" id="Seg_2552" n="e" s="T337">Inʼem </ts>
               <ts e="T339" id="Seg_2554" n="e" s="T338">kɨrdʼagas, </ts>
               <ts e="T340" id="Seg_2556" n="e" s="T339">ubajɨm </ts>
               <ts e="T341" id="Seg_2558" n="e" s="T340">kɨrdʼagas, </ts>
               <ts e="T342" id="Seg_2560" n="e" s="T341">onton </ts>
               <ts e="T343" id="Seg_2562" n="e" s="T342">ol </ts>
               <ts e="T344" id="Seg_2564" n="e" s="T343">agɨs </ts>
               <ts e="T345" id="Seg_2566" n="e" s="T344">ogoloːkpun </ts>
               <ts e="T346" id="Seg_2568" n="e" s="T345">bejem. </ts>
               <ts e="T347" id="Seg_2570" n="e" s="T346">Edʼiːjim </ts>
               <ts e="T348" id="Seg_2572" n="e" s="T347">gi͡ene </ts>
               <ts e="T349" id="Seg_2574" n="e" s="T348">tü͡ört </ts>
               <ts e="T350" id="Seg_2576" n="e" s="T349">ogo </ts>
               <ts e="T351" id="Seg_2578" n="e" s="T350">kaːlla, </ts>
               <ts e="T352" id="Seg_2580" n="e" s="T351">mini͡ene </ts>
               <ts e="T353" id="Seg_2582" n="e" s="T352">tü͡ört </ts>
               <ts e="T354" id="Seg_2584" n="e" s="T353">ogo </ts>
               <ts e="T355" id="Seg_2586" n="e" s="T354">bu͡olla. </ts>
               <ts e="T356" id="Seg_2588" n="e" s="T355">Naːdʼa </ts>
               <ts e="T357" id="Seg_2590" n="e" s="T356">paslʼednʼaja, </ts>
               <ts e="T358" id="Seg_2592" n="e" s="T357">malʼenkaja </ts>
               <ts e="T359" id="Seg_2594" n="e" s="T358">bɨla. </ts>
               <ts e="T360" id="Seg_2596" n="e" s="T359">Küččügüj </ts>
               <ts e="T361" id="Seg_2598" n="e" s="T360">ete </ts>
               <ts e="T362" id="Seg_2600" n="e" s="T361">ol. </ts>
               <ts e="T363" id="Seg_2602" n="e" s="T362">Dʼe </ts>
               <ts e="T364" id="Seg_2604" n="e" s="T363">ol </ts>
               <ts e="T365" id="Seg_2606" n="e" s="T364">kaːlammɨn </ts>
               <ts e="T366" id="Seg_2608" n="e" s="T365">dʼe </ts>
               <ts e="T367" id="Seg_2610" n="e" s="T366">iti </ts>
               <ts e="T368" id="Seg_2612" n="e" s="T367">olordubut </ts>
               <ts e="T369" id="Seg_2614" n="e" s="T368">bu͡olla, </ts>
               <ts e="T370" id="Seg_2616" n="e" s="T369">inʼem </ts>
               <ts e="T371" id="Seg_2618" n="e" s="T370">kɨrdʼan </ts>
               <ts e="T372" id="Seg_2620" n="e" s="T371">öllö, </ts>
               <ts e="T373" id="Seg_2622" n="e" s="T372">ɨ͡aldʼan. </ts>
               <ts e="T374" id="Seg_2624" n="e" s="T373">Ol </ts>
               <ts e="T375" id="Seg_2626" n="e" s="T374">ubajɨm </ts>
               <ts e="T376" id="Seg_2628" n="e" s="T375">emi͡e </ts>
               <ts e="T377" id="Seg_2630" n="e" s="T376">dʼaktara </ts>
               <ts e="T378" id="Seg_2632" n="e" s="T377">da </ts>
               <ts e="T379" id="Seg_2634" n="e" s="T378">hu͡ok, </ts>
               <ts e="T380" id="Seg_2636" n="e" s="T379">tu͡ok </ts>
               <ts e="T381" id="Seg_2638" n="e" s="T380">da </ts>
               <ts e="T382" id="Seg_2640" n="e" s="T381">hu͡ok </ts>
               <ts e="T383" id="Seg_2642" n="e" s="T382">ete. </ts>
               <ts e="T384" id="Seg_2644" n="e" s="T383">Laːpkɨga </ts>
               <ts e="T385" id="Seg_2646" n="e" s="T384">üleleːčči </ts>
               <ts e="T386" id="Seg_2648" n="e" s="T385">ete, </ts>
               <ts e="T387" id="Seg_2650" n="e" s="T386">ol </ts>
               <ts e="T388" id="Seg_2652" n="e" s="T387">ihin </ts>
               <ts e="T389" id="Seg_2654" n="e" s="T388">ahɨnan </ts>
               <ts e="T390" id="Seg_2656" n="e" s="T389">da </ts>
               <ts e="T391" id="Seg_2658" n="e" s="T390">olus </ts>
               <ts e="T392" id="Seg_2660" n="e" s="T391">da </ts>
               <ts e="T393" id="Seg_2662" n="e" s="T392">kimneːččite </ts>
               <ts e="T394" id="Seg_2664" n="e" s="T393">hu͡ok </ts>
               <ts e="T395" id="Seg_2666" n="e" s="T394">etibit. </ts>
               <ts e="T396" id="Seg_2668" n="e" s="T395">Üleliːr </ts>
               <ts e="T397" id="Seg_2670" n="e" s="T396">bu͡olan, </ts>
               <ts e="T398" id="Seg_2672" n="e" s="T397">((…)) </ts>
               <ts e="T399" id="Seg_2674" n="e" s="T398">egelen </ts>
               <ts e="T400" id="Seg_2676" n="e" s="T399">iheːčči </ts>
               <ts e="T401" id="Seg_2678" n="e" s="T400">as </ts>
               <ts e="T402" id="Seg_2680" n="e" s="T401">laːpkɨttan. </ts>
               <ts e="T403" id="Seg_2682" n="e" s="T402">Harpɨlaːkkaːn </ts>
               <ts e="T404" id="Seg_2684" n="e" s="T403">(iti) </ts>
               <ts e="T405" id="Seg_2686" n="e" s="T404">ɨlaːččɨ. </ts>
               <ts e="T406" id="Seg_2688" n="e" s="T405">Onton </ts>
               <ts e="T407" id="Seg_2690" n="e" s="T406">min </ts>
               <ts e="T408" id="Seg_2692" n="e" s="T407">poːvardiːbin </ts>
               <ts e="T409" id="Seg_2694" n="e" s="T408">bu͡olla. </ts>
               <ts e="T410" id="Seg_2696" n="e" s="T409">Ol </ts>
               <ts e="T411" id="Seg_2698" n="e" s="T410">iti </ts>
               <ts e="T412" id="Seg_2700" n="e" s="T411">olorbupput, </ts>
               <ts e="T413" id="Seg_2702" n="e" s="T412">barɨkaːttara </ts>
               <ts e="T414" id="Seg_2704" n="e" s="T413">ulaːppɨttara </ts>
               <ts e="T415" id="Seg_2706" n="e" s="T414">eni, </ts>
               <ts e="T416" id="Seg_2708" n="e" s="T415">dʼaktardaːktar. </ts>
            </ts>
            <ts e="T473" id="Seg_2709" n="sc" s="T421">
               <ts e="T422" id="Seg_2711" n="e" s="T421">– Dʼe </ts>
               <ts e="T423" id="Seg_2713" n="e" s="T422">taksɨbɨta, </ts>
               <ts e="T424" id="Seg_2715" n="e" s="T423">(tu͡ok </ts>
               <ts e="T425" id="Seg_2717" n="e" s="T424">gɨnaːrɨ </ts>
               <ts e="T426" id="Seg_2719" n="e" s="T425">da), </ts>
               <ts e="T427" id="Seg_2721" n="e" s="T426">taksɨbɨt. </ts>
               <ts e="T428" id="Seg_2723" n="e" s="T427">Dʼe </ts>
               <ts e="T429" id="Seg_2725" n="e" s="T428">ol </ts>
               <ts e="T430" id="Seg_2727" n="e" s="T429">ihin </ts>
               <ts e="T431" id="Seg_2729" n="e" s="T430">agɨs </ts>
               <ts e="T432" id="Seg_2731" n="e" s="T431">ogo </ts>
               <ts e="T433" id="Seg_2733" n="e" s="T432">bu͡olar </ts>
               <ts e="T434" id="Seg_2735" n="e" s="T433">bu͡olla. </ts>
               <ts e="T435" id="Seg_2737" n="e" s="T434">Maladoj </ts>
               <ts e="T436" id="Seg_2739" n="e" s="T435">dʼaktarbɨn </ts>
               <ts e="T437" id="Seg_2741" n="e" s="T436">bu͡ol, </ts>
               <ts e="T438" id="Seg_2743" n="e" s="T437">onno </ts>
               <ts e="T439" id="Seg_2745" n="e" s="T438">ol </ts>
               <ts e="T440" id="Seg_2747" n="e" s="T439">zamuž </ts>
               <ts e="T441" id="Seg_2749" n="e" s="T440">vɨšlʼi </ts>
               <ts e="T442" id="Seg_2751" n="e" s="T441">etʼi </ts>
               <ts e="T443" id="Seg_2753" n="e" s="T442">ke. </ts>
               <ts e="T444" id="Seg_2755" n="e" s="T443">Erge </ts>
               <ts e="T445" id="Seg_2757" n="e" s="T444">taksan </ts>
               <ts e="T446" id="Seg_2759" n="e" s="T445">kaːllɨm </ts>
               <ts e="T447" id="Seg_2761" n="e" s="T446">bu͡o, </ts>
               <ts e="T448" id="Seg_2763" n="e" s="T447">ikki </ts>
               <ts e="T449" id="Seg_2765" n="e" s="T448">ogonu </ts>
               <ts e="T450" id="Seg_2767" n="e" s="T449">(töröːbüt) </ts>
               <ts e="T451" id="Seg_2769" n="e" s="T450">bu͡olla. </ts>
               <ts e="T452" id="Seg_2771" n="e" s="T451">Ol </ts>
               <ts e="T453" id="Seg_2773" n="e" s="T452">ühüs, </ts>
               <ts e="T454" id="Seg_2775" n="e" s="T453">ühüs </ts>
               <ts e="T455" id="Seg_2777" n="e" s="T454">ogom </ts>
               <ts e="T456" id="Seg_2779" n="e" s="T455">u͡ol </ts>
               <ts e="T457" id="Seg_2781" n="e" s="T456">ogo </ts>
               <ts e="T458" id="Seg_2783" n="e" s="T457">ete, </ts>
               <ts e="T459" id="Seg_2785" n="e" s="T458">ontum </ts>
               <ts e="T460" id="Seg_2787" n="e" s="T459">tüspüte. </ts>
               <ts e="T461" id="Seg_2789" n="e" s="T460">Ölörünemmin, </ts>
               <ts e="T462" id="Seg_2791" n="e" s="T461">(saː-) </ts>
               <ts e="T463" id="Seg_2793" n="e" s="T462">saːdikka </ts>
               <ts e="T464" id="Seg_2795" n="e" s="T463">buːstarɨ </ts>
               <ts e="T465" id="Seg_2797" n="e" s="T464">uːgollarɨ </ts>
               <ts e="T466" id="Seg_2799" n="e" s="T465">taskajdaːn </ts>
               <ts e="T467" id="Seg_2801" n="e" s="T466">ol </ts>
               <ts e="T468" id="Seg_2803" n="e" s="T467">tüspüt </ts>
               <ts e="T469" id="Seg_2805" n="e" s="T468">ete. </ts>
               <ts e="T470" id="Seg_2807" n="e" s="T469">Iti </ts>
               <ts e="T471" id="Seg_2809" n="e" s="T470">ikki </ts>
               <ts e="T472" id="Seg_2811" n="e" s="T471">u͡ol </ts>
               <ts e="T473" id="Seg_2813" n="e" s="T472">ɨkkardɨta. </ts>
            </ts>
            <ts e="T524" id="Seg_2814" n="sc" s="T480">
               <ts e="T481" id="Seg_2816" n="e" s="T480">– Ölöːnö </ts>
               <ts e="T482" id="Seg_2818" n="e" s="T481">alta </ts>
               <ts e="T483" id="Seg_2820" n="e" s="T482">dʼɨllaːgɨ </ts>
               <ts e="T484" id="Seg_2822" n="e" s="T483">ɨlbɨppɨt, </ts>
               <ts e="T485" id="Seg_2824" n="e" s="T484">onton </ts>
               <ts e="T486" id="Seg_2826" n="e" s="T485">hette </ts>
               <ts e="T487" id="Seg_2828" n="e" s="T486">dʼɨllanarɨgar </ts>
               <ts e="T488" id="Seg_2830" n="e" s="T487">munna </ts>
               <ts e="T489" id="Seg_2832" n="e" s="T488">kellim </ts>
               <ts e="T490" id="Seg_2834" n="e" s="T489">bu͡olla, </ts>
               <ts e="T491" id="Seg_2836" n="e" s="T490">Dudʼinkaga. </ts>
               <ts e="T492" id="Seg_2838" n="e" s="T491">Ogom </ts>
               <ts e="T493" id="Seg_2840" n="e" s="T492">dʼi͡e </ts>
               <ts e="T494" id="Seg_2842" n="e" s="T493">ɨlar </ts>
               <ts e="T495" id="Seg_2844" n="e" s="T494">ebit </ts>
               <ts e="T496" id="Seg_2846" n="e" s="T495">araj, </ts>
               <ts e="T497" id="Seg_2848" n="e" s="T496">dʼe </ts>
               <ts e="T498" id="Seg_2850" n="e" s="T497">onuga </ts>
               <ts e="T499" id="Seg_2852" n="e" s="T498">prapʼiskalɨ͡an </ts>
               <ts e="T500" id="Seg_2854" n="e" s="T499">naːda </ts>
               <ts e="T501" id="Seg_2856" n="e" s="T500">ebit, </ts>
               <ts e="T502" id="Seg_2858" n="e" s="T501">dʼe </ts>
               <ts e="T503" id="Seg_2860" n="e" s="T502">ol </ts>
               <ts e="T504" id="Seg_2862" n="e" s="T503">kelbitim. </ts>
               <ts e="T505" id="Seg_2864" n="e" s="T504">Ol </ts>
               <ts e="T506" id="Seg_2866" n="e" s="T505">kelemmin </ts>
               <ts e="T507" id="Seg_2868" n="e" s="T506">bu͡o </ts>
               <ts e="T508" id="Seg_2870" n="e" s="T507">ogom </ts>
               <ts e="T509" id="Seg_2872" n="e" s="T508">onus </ts>
               <ts e="T510" id="Seg_2874" n="e" s="T509">kɨlaːs </ts>
               <ts e="T511" id="Seg_2876" n="e" s="T510">ete </ts>
               <ts e="T512" id="Seg_2878" n="e" s="T511">((…)), </ts>
               <ts e="T513" id="Seg_2880" n="e" s="T512">vot. </ts>
               <ts e="T514" id="Seg_2882" n="e" s="T513">Maːmata </ts>
               <ts e="T515" id="Seg_2884" n="e" s="T514">bu͡ollagɨna </ts>
               <ts e="T516" id="Seg_2886" n="e" s="T515">Džakuskajga </ts>
               <ts e="T517" id="Seg_2888" n="e" s="T516">erdemmite. </ts>
               <ts e="T518" id="Seg_2890" n="e" s="T517">Ontuŋ </ts>
               <ts e="T519" id="Seg_2892" n="e" s="T518">anɨ </ts>
               <ts e="T520" id="Seg_2894" n="e" s="T519">ikki </ts>
               <ts e="T521" id="Seg_2896" n="e" s="T520">ogoloːk, </ts>
               <ts e="T522" id="Seg_2898" n="e" s="T521">u͡olu </ts>
               <ts e="T523" id="Seg_2900" n="e" s="T522">gɨtta </ts>
               <ts e="T524" id="Seg_2902" n="e" s="T523">kɨːs. </ts>
            </ts>
            <ts e="T534" id="Seg_2903" n="sc" s="T528">
               <ts e="T529" id="Seg_2905" n="e" s="T528">– Tɨ͡aga, </ts>
               <ts e="T530" id="Seg_2907" n="e" s="T529">tuːndraga, </ts>
               <ts e="T531" id="Seg_2909" n="e" s="T530">hɨldʼallar </ts>
               <ts e="T532" id="Seg_2911" n="e" s="T531">tɨ͡aga, </ts>
               <ts e="T533" id="Seg_2913" n="e" s="T532">tabahɨt </ts>
               <ts e="T534" id="Seg_2915" n="e" s="T533">ere. </ts>
            </ts>
            <ts e="T557" id="Seg_2916" n="sc" s="T535">
               <ts e="T536" id="Seg_2918" n="e" s="T535">Elete. </ts>
               <ts e="T537" id="Seg_2920" n="e" s="T536">Hɨndaːskaga </ts>
               <ts e="T538" id="Seg_2922" n="e" s="T537">kaːlbɨttara </ts>
               <ts e="T539" id="Seg_2924" n="e" s="T538">anɨ, </ts>
               <ts e="T540" id="Seg_2926" n="e" s="T539">barɨ͡aktara </ts>
               <ts e="T541" id="Seg_2928" n="e" s="T540">maːj </ts>
               <ts e="T542" id="Seg_2930" n="e" s="T541">ɨjga </ts>
               <ts e="T543" id="Seg_2932" n="e" s="T542">sku͡ola </ts>
               <ts e="T544" id="Seg_2934" n="e" s="T543">bütteren. </ts>
               <ts e="T545" id="Seg_2936" n="e" s="T544">Ogoloro </ts>
               <ts e="T546" id="Seg_2938" n="e" s="T545">itinne </ts>
               <ts e="T547" id="Seg_2940" n="e" s="T546">ü͡öreneller </ts>
               <ts e="T548" id="Seg_2942" n="e" s="T547">Hɨndaːskaga. </ts>
               <ts e="T549" id="Seg_2944" n="e" s="T548">Biːrgehe </ts>
               <ts e="T550" id="Seg_2946" n="e" s="T549">tördüs </ts>
               <ts e="T551" id="Seg_2948" n="e" s="T550">kɨlaːs, </ts>
               <ts e="T552" id="Seg_2950" n="e" s="T551">biːrgehe </ts>
               <ts e="T553" id="Seg_2952" n="e" s="T552">ühüs </ts>
               <ts e="T554" id="Seg_2954" n="e" s="T553">kɨlas </ts>
               <ts e="T555" id="Seg_2956" n="e" s="T554">du͡o, </ts>
               <ts e="T556" id="Seg_2958" n="e" s="T555">ikkis </ts>
               <ts e="T557" id="Seg_2960" n="e" s="T556">du͡o. </ts>
            </ts>
            <ts e="T574" id="Seg_2961" n="sc" s="T558">
               <ts e="T559" id="Seg_2963" n="e" s="T558">Ikkis </ts>
               <ts e="T560" id="Seg_2965" n="e" s="T559">kɨlas, </ts>
               <ts e="T561" id="Seg_2967" n="e" s="T560">eː, </ts>
               <ts e="T562" id="Seg_2969" n="e" s="T561">kɨːha. </ts>
               <ts e="T563" id="Seg_2971" n="e" s="T562">Anɨ </ts>
               <ts e="T564" id="Seg_2973" n="e" s="T563">min </ts>
               <ts e="T565" id="Seg_2975" n="e" s="T564">di͡eber </ts>
               <ts e="T566" id="Seg_2977" n="e" s="T565">kaːlbɨttara </ts>
               <ts e="T567" id="Seg_2979" n="e" s="T566">anɨ </ts>
               <ts e="T568" id="Seg_2981" n="e" s="T567">maːjga </ts>
               <ts e="T569" id="Seg_2983" n="e" s="T568">barɨ͡aktara </ts>
               <ts e="T570" id="Seg_2985" n="e" s="T569">anɨ, </ts>
               <ts e="T571" id="Seg_2987" n="e" s="T570">abratna. </ts>
               <ts e="T572" id="Seg_2989" n="e" s="T571">Köhön </ts>
               <ts e="T573" id="Seg_2991" n="e" s="T572">barɨ͡aktara. </ts>
               <ts e="T574" id="Seg_2993" n="e" s="T573">Elete. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KiES">
            <ta e="T11" id="Seg_2994" s="T1">KiES_KiLS_2009_Life_nar.KiES.001 (001.001)</ta>
            <ta e="T24" id="Seg_2995" s="T11">KiES_KiLS_2009_Life_nar.KiES.002 (001.002)</ta>
            <ta e="T36" id="Seg_2996" s="T24">KiES_KiLS_2009_Life_nar.KiES.003 (001.003)</ta>
            <ta e="T58" id="Seg_2997" s="T36">KiES_KiLS_2009_Life_nar.KiES.004 (001.004)</ta>
            <ta e="T66" id="Seg_2998" s="T63">KiES_KiLS_2009_Life_nar.KiES.005 (001.006)</ta>
            <ta e="T72" id="Seg_2999" s="T66">KiES_KiLS_2009_Life_nar.KiES.006 (001.007)</ta>
            <ta e="T79" id="Seg_3000" s="T72">KiES_KiLS_2009_Life_nar.KiES.007 (001.008)</ta>
            <ta e="T84" id="Seg_3001" s="T79">KiES_KiLS_2009_Life_nar.KiES.008 (001.009)</ta>
            <ta e="T90" id="Seg_3002" s="T84">KiES_KiLS_2009_Life_nar.KiES.009 (001.010)</ta>
            <ta e="T97" id="Seg_3003" s="T91">KiES_KiLS_2009_Life_nar.KiES.010 (001.012)</ta>
            <ta e="T100" id="Seg_3004" s="T97">KiES_KiLS_2009_Life_nar.KiES.011 (001.013)</ta>
            <ta e="T105" id="Seg_3005" s="T100">KiES_KiLS_2009_Life_nar.KiES.012 (001.014)</ta>
            <ta e="T114" id="Seg_3006" s="T105">KiES_KiLS_2009_Life_nar.KiES.013 (001.015)</ta>
            <ta e="T120" id="Seg_3007" s="T114">KiES_KiLS_2009_Life_nar.KiES.014 (001.016)</ta>
            <ta e="T126" id="Seg_3008" s="T120">KiES_KiLS_2009_Life_nar.KiES.015 (001.017)</ta>
            <ta e="T144" id="Seg_3009" s="T126">KiES_KiLS_2009_Life_nar.KiES.016 (001.018)</ta>
            <ta e="T148" id="Seg_3010" s="T144">KiES_KiLS_2009_Life_nar.KiES.017 (001.019)</ta>
            <ta e="T152" id="Seg_3011" s="T150">KiES_KiLS_2009_Life_nar.KiES.018 (001.021)</ta>
            <ta e="T166" id="Seg_3012" s="T156">KiES_KiLS_2009_Life_nar.KiES.019 (001.023)</ta>
            <ta e="T171" id="Seg_3013" s="T168">KiES_KiLS_2009_Life_nar.KiES.020 (001.025)</ta>
            <ta e="T175" id="Seg_3014" s="T171">KiES_KiLS_2009_Life_nar.KiES.021 (001.026)</ta>
            <ta e="T182" id="Seg_3015" s="T175">KiES_KiLS_2009_Life_nar.KiES.022 (001.027)</ta>
            <ta e="T188" id="Seg_3016" s="T182">KiES_KiLS_2009_Life_nar.KiES.023 (001.028)</ta>
            <ta e="T196" id="Seg_3017" s="T188">KiES_KiLS_2009_Life_nar.KiES.024 (001.029)</ta>
            <ta e="T212" id="Seg_3018" s="T196">KiES_KiLS_2009_Life_nar.KiES.025 (001.030)</ta>
            <ta e="T222" id="Seg_3019" s="T212">KiES_KiLS_2009_Life_nar.KiES.026 (001.031)</ta>
            <ta e="T225" id="Seg_3020" s="T222">KiES_KiLS_2009_Life_nar.KiES.027 (001.032)</ta>
            <ta e="T233" id="Seg_3021" s="T230">KiES_KiLS_2009_Life_nar.KiES.028 (001.034)</ta>
            <ta e="T245" id="Seg_3022" s="T236">KiES_KiLS_2009_Life_nar.KiES.029 (001.035)</ta>
            <ta e="T253" id="Seg_3023" s="T245">KiES_KiLS_2009_Life_nar.KiES.030 (001.036)</ta>
            <ta e="T258" id="Seg_3024" s="T255">KiES_KiLS_2009_Life_nar.KiES.031 (001.038)</ta>
            <ta e="T270" id="Seg_3025" s="T258">KiES_KiLS_2009_Life_nar.KiES.032 (001.039)</ta>
            <ta e="T273" id="Seg_3026" s="T270">KiES_KiLS_2009_Life_nar.KiES.033 (001.040)</ta>
            <ta e="T291" id="Seg_3027" s="T273">KiES_KiLS_2009_Life_nar.KiES.034 (001.041)</ta>
            <ta e="T304" id="Seg_3028" s="T291">KiES_KiLS_2009_Life_nar.KiES.035 (001.042)</ta>
            <ta e="T308" id="Seg_3029" s="T304">KiES_KiLS_2009_Life_nar.KiES.036 (001.043)</ta>
            <ta e="T328" id="Seg_3030" s="T308">KiES_KiLS_2009_Life_nar.KiES.037 (001.044)</ta>
            <ta e="T333" id="Seg_3031" s="T329">KiES_KiLS_2009_Life_nar.KiES.038 (001.045)</ta>
            <ta e="T337" id="Seg_3032" s="T333">KiES_KiLS_2009_Life_nar.KiES.039 (001.046)</ta>
            <ta e="T346" id="Seg_3033" s="T337">KiES_KiLS_2009_Life_nar.KiES.040 (001.047)</ta>
            <ta e="T355" id="Seg_3034" s="T346">KiES_KiLS_2009_Life_nar.KiES.041 (001.048)</ta>
            <ta e="T359" id="Seg_3035" s="T355">KiES_KiLS_2009_Life_nar.KiES.042 (001.049)</ta>
            <ta e="T362" id="Seg_3036" s="T359">KiES_KiLS_2009_Life_nar.KiES.043 (001.050)</ta>
            <ta e="T373" id="Seg_3037" s="T362">KiES_KiLS_2009_Life_nar.KiES.044 (001.051)</ta>
            <ta e="T383" id="Seg_3038" s="T373">KiES_KiLS_2009_Life_nar.KiES.045 (001.052)</ta>
            <ta e="T395" id="Seg_3039" s="T383">KiES_KiLS_2009_Life_nar.KiES.046 (001.053)</ta>
            <ta e="T402" id="Seg_3040" s="T395">KiES_KiLS_2009_Life_nar.KiES.047 (001.054)</ta>
            <ta e="T405" id="Seg_3041" s="T402">KiES_KiLS_2009_Life_nar.KiES.048 (001.055)</ta>
            <ta e="T409" id="Seg_3042" s="T405">KiES_KiLS_2009_Life_nar.KiES.049 (001.056)</ta>
            <ta e="T416" id="Seg_3043" s="T409">KiES_KiLS_2009_Life_nar.KiES.050 (001.057)</ta>
            <ta e="T427" id="Seg_3044" s="T421">KiES_KiLS_2009_Life_nar.KiES.051 (001.059)</ta>
            <ta e="T434" id="Seg_3045" s="T427">KiES_KiLS_2009_Life_nar.KiES.052 (001.060)</ta>
            <ta e="T443" id="Seg_3046" s="T434">KiES_KiLS_2009_Life_nar.KiES.053 (001.061)</ta>
            <ta e="T451" id="Seg_3047" s="T443">KiES_KiLS_2009_Life_nar.KiES.054 (001.062)</ta>
            <ta e="T460" id="Seg_3048" s="T451">KiES_KiLS_2009_Life_nar.KiES.055 (001.063)</ta>
            <ta e="T469" id="Seg_3049" s="T460">KiES_KiLS_2009_Life_nar.KiES.056 (001.064)</ta>
            <ta e="T473" id="Seg_3050" s="T469">KiES_KiLS_2009_Life_nar.KiES.057 (001.065)</ta>
            <ta e="T491" id="Seg_3051" s="T480">KiES_KiLS_2009_Life_nar.KiES.058 (001.067)</ta>
            <ta e="T504" id="Seg_3052" s="T491">KiES_KiLS_2009_Life_nar.KiES.059 (001.068)</ta>
            <ta e="T513" id="Seg_3053" s="T504">KiES_KiLS_2009_Life_nar.KiES.060 (001.069)</ta>
            <ta e="T517" id="Seg_3054" s="T513">KiES_KiLS_2009_Life_nar.KiES.061 (001.070)</ta>
            <ta e="T524" id="Seg_3055" s="T517">KiES_KiLS_2009_Life_nar.KiES.062 (001.071)</ta>
            <ta e="T534" id="Seg_3056" s="T528">KiES_KiLS_2009_Life_nar.KiES.063 (001.073)</ta>
            <ta e="T536" id="Seg_3057" s="T535">KiES_KiLS_2009_Life_nar.KiES.064 (001.074)</ta>
            <ta e="T544" id="Seg_3058" s="T536">KiES_KiLS_2009_Life_nar.KiES.065 (001.075)</ta>
            <ta e="T548" id="Seg_3059" s="T544">KiES_KiLS_2009_Life_nar.KiES.066 (001.076)</ta>
            <ta e="T557" id="Seg_3060" s="T548">KiES_KiLS_2009_Life_nar.KiES.067 (001.077)</ta>
            <ta e="T562" id="Seg_3061" s="T558">KiES_KiLS_2009_Life_nar.KiES.068 (001.078)</ta>
            <ta e="T571" id="Seg_3062" s="T562">KiES_KiLS_2009_Life_nar.KiES.069 (001.079)</ta>
            <ta e="T573" id="Seg_3063" s="T571">KiES_KiLS_2009_Life_nar.KiES.070 (001.080)</ta>
            <ta e="T574" id="Seg_3064" s="T573">KiES_KiLS_2009_Life_nar.KiES.071 (001.081)</ta>
         </annotation>
         <annotation name="st" tierref="st-KiES" />
         <annotation name="ts" tierref="ts-KiES">
            <ta e="T11" id="Seg_3065" s="T1">– Dʼe Korgoːgo baːr etibit hirdeːk etibit ginilerge Korgoːgo hirdemmitim. </ta>
            <ta e="T24" id="Seg_3066" s="T11">Onton bu͡ollagɨna bu, eː bu ogonu töröːbütüm, onton ol u͡ol ogom töröːmmün ol. </ta>
            <ta e="T36" id="Seg_3067" s="T24">((DMG)) Onton bu͡ollagɨna ogonnʼorbut ɨ͡aldʼar ete ölön, ölön ol kimi͡eke Hɨndaːskaga hɨppɨppɨt ol. </ta>
            <ta e="T58" id="Seg_3068" s="T36">Onton ülem minigin načʼalnʼikʼi zaabrazalʼi eti bu ke kak budu eti, onton üle bi͡erbittere mi͡eke saːdikka, sadʼik pervɨj raz bɨl v Sɨndasske. </ta>
            <ta e="T66" id="Seg_3069" s="T63">– Eː, hakalɨː du͡o. </ta>
            <ta e="T72" id="Seg_3070" s="T66">Hɨndaːskaga (haka) haŋardɨː saːdik turbut ete. </ta>
            <ta e="T79" id="Seg_3071" s="T72">Dʼe tugu da bilpeppin, saːdik, tu͡ok aːtaj? </ta>
            <ta e="T84" id="Seg_3072" s="T79">Ničevo nʼe znajem mɨ tundravʼičkʼi. </ta>
            <ta e="T90" id="Seg_3073" s="T84">Vsʼo takoje eta karmʼit, kak… </ta>
            <ta e="T97" id="Seg_3074" s="T91">– Oː ((…)) dʼe kajdak bu͡olu͡okputuj ((…)). </ta>
            <ta e="T100" id="Seg_3075" s="T97">Hataːn haŋarbappɨn, ogolorgo. </ta>
            <ta e="T105" id="Seg_3076" s="T100">Če ogolorgo hataːn buharbappɨn diːbin. </ta>
            <ta e="T114" id="Seg_3077" s="T105">Hu͡ok povardar diːller bu͡o bu saːdik turda haŋa saːdikka. </ta>
            <ta e="T120" id="Seg_3078" s="T114">Onuga poːvar, poːvar naːda diːller tojottor. </ta>
            <ta e="T126" id="Seg_3079" s="T120">Innʼe tojottor minigin poːvar uːrdular bu͡olla. </ta>
            <ta e="T144" id="Seg_3080" s="T126">Dʼe ol hɨppɨppɨt onton ol poːvardaːmmɨn bu͡ollagɨna, eː, kimi͡eke bu ke iti ikki ogom hɨldʼallar saːdikka araj, heː. </ta>
            <ta e="T148" id="Seg_3081" s="T144">Dʼe ol hɨldʼannar ((…)). </ta>
            <ta e="T152" id="Seg_3082" s="T150">Kimi gɨtta? </ta>
            <ta e="T166" id="Seg_3083" s="T156">Togo, ol onton ol kojut ol kimim, ol edʼijim ölbüte. </ta>
            <ta e="T171" id="Seg_3084" s="T168">– Dʼe. </ta>
            <ta e="T175" id="Seg_3085" s="T171">Ijem, ijem baltɨta ölbüte. </ta>
            <ta e="T182" id="Seg_3086" s="T175">ɨ͡aldʼan, oččogo tü͡ört ogoloːk kaːlla bu kihi. </ta>
            <ta e="T188" id="Seg_3087" s="T182">Tü͡ört ogo bu͡olla mini͡eke kelliler bu͡olla. </ta>
            <ta e="T196" id="Seg_3088" s="T188">"Bu͡o edʼiːjbitiger bu͡olu͡okput bihigi, kajdi͡et da barbappɨt", diːller. </ta>
            <ta e="T212" id="Seg_3089" s="T196">Inʼem, eː, (inʼe-) inʼete geri͡es eppit bu͡olla, "bu ke edʼiːjgitiger kaːlan" di͡ebit bu͡olla "min kajdak bu͡ollakpɨna". </ta>
            <ta e="T222" id="Seg_3090" s="T212">Dʼe ol ol tü͡ört, tü͡ört ogo bihi͡eke kelliler araj, mhm. </ta>
            <ta e="T225" id="Seg_3091" s="T222">Nosku͡o ü͡örene barallar. </ta>
            <ta e="T233" id="Seg_3092" s="T230">– Dʼe beːj onton. </ta>
            <ta e="T245" id="Seg_3093" s="T236">Dʼe onton (kanʼikulga) keleller bu ogolor, hübeliːller tojottor araj. </ta>
            <ta e="T253" id="Seg_3094" s="T245">"Bu ogonu bu͡olla Pöpügejge ɨːtɨ͡akka", diːller, "uruːtugar." </ta>
            <ta e="T258" id="Seg_3095" s="T255">– Mhm, (Keːtʼeni). </ta>
            <ta e="T270" id="Seg_3096" s="T258">Biːr, biːr ogonu bu͡olla, munna tɨ͡aga baːr, tundra ogonnʼordoːk emeːksin uruːbut araj. </ta>
            <ta e="T273" id="Seg_3097" s="T270">Dʼe olorgo bi͡erdiler. </ta>
            <ta e="T291" id="Seg_3098" s="T273">Oččogo min ikki u͡ol ogonu gɨtta kaːllɨm bu͡o araj, itiler mini͡eke (bar-) barbattar (töttörü) iti ikki u͡ol ogo. </ta>
            <ta e="T304" id="Seg_3099" s="T291">Dʼe onton töhö da bu͡olbakka kanʼikulga kelbittere ol ogoloruŋ bu͡o töttörü kelliler bihi͡eke. </ta>
            <ta e="T308" id="Seg_3100" s="T304">Ol höbüleːbetter ebit oloru. </ta>
            <ta e="T328" id="Seg_3101" s="T308">Onton tundraga ogonnʼordoːk emeːksin emi͡e, ol barbɨt ikki ogobut emi͡e, emi͡e töttörü kelle bu͡o oččogo agɨs ogoloːk bu͡ollum bu͡o min. </ta>
            <ta e="T333" id="Seg_3102" s="T329">Bejem tü͡ört ogoloːppun bu͡olla. </ta>
            <ta e="T337" id="Seg_3103" s="T333">Iti ikki kɨːstaːppɨn bu͡olla. </ta>
            <ta e="T346" id="Seg_3104" s="T337">Inʼem kɨrdʼagas, ubajɨm kɨrdʼagas, onton ol agɨs ogoloːkpun bejem. </ta>
            <ta e="T355" id="Seg_3105" s="T346">Edʼiːjim gi͡ene tü͡ört ogo kaːlla, mini͡ene tü͡ört ogo bu͡olla. </ta>
            <ta e="T359" id="Seg_3106" s="T355">Naːdʼa paslʼednʼaja, malʼenkaja bɨla. </ta>
            <ta e="T362" id="Seg_3107" s="T359">Küččügüj ete ol. </ta>
            <ta e="T373" id="Seg_3108" s="T362">Dʼe ol kaːlammɨn dʼe iti olordubut bu͡olla, inʼem kɨrdʼan öllö, ɨ͡aldʼan. </ta>
            <ta e="T383" id="Seg_3109" s="T373">Ol ubajɨm emi͡e dʼaktara da hu͡ok, tu͡ok da hu͡ok ete. </ta>
            <ta e="T395" id="Seg_3110" s="T383">Laːpkɨga üleleːčči ete, ol ihin ahɨnan da olus da kimneːččite hu͡ok etibit. </ta>
            <ta e="T402" id="Seg_3111" s="T395">Üleliːr bu͡olan, ((…)) egelen iheːčči as laːpkɨttan. </ta>
            <ta e="T405" id="Seg_3112" s="T402">Harpɨlaːkkaːn (iti) ɨlaːččɨ. </ta>
            <ta e="T409" id="Seg_3113" s="T405">Onton min poːvardiːbin bu͡olla. </ta>
            <ta e="T416" id="Seg_3114" s="T409">Ol iti olorbupput, barɨkaːttara ulaːppɨttara eni, dʼaktardaːktar. </ta>
            <ta e="T427" id="Seg_3115" s="T421">– Dʼe taksɨbɨta, (tu͡ok gɨnaːrɨ da), taksɨbɨt. </ta>
            <ta e="T434" id="Seg_3116" s="T427">Dʼe ol ihin agɨs ogo bu͡olar bu͡olla. </ta>
            <ta e="T443" id="Seg_3117" s="T434">Maladoj dʼaktarbɨn bu͡ol, onno ol zamuž vɨšlʼi etʼi ke. </ta>
            <ta e="T451" id="Seg_3118" s="T443">Erge taksan kaːllɨm bu͡o, ikki ogonu (töröːbüt) bu͡olla. </ta>
            <ta e="T460" id="Seg_3119" s="T451">Ol ühüs, ühüs ogom u͡ol ogo ete, ontum tüspüte. </ta>
            <ta e="T469" id="Seg_3120" s="T460">Ölörünemmin, (saː-) saːdikka buːstarɨ uːgollarɨ taskajdaːn ol tüspüt ete. </ta>
            <ta e="T473" id="Seg_3121" s="T469">Iti ikki u͡ol ɨkkardɨta. </ta>
            <ta e="T491" id="Seg_3122" s="T480">– Ölöːnö alta dʼɨllaːgɨ ɨlbɨppɨt, onton hette dʼɨllanarɨgar munna kellim bu͡olla, Dudʼinkaga. </ta>
            <ta e="T504" id="Seg_3123" s="T491">Ogom dʼi͡e ɨlar ebit araj, dʼe onuga prapʼiskalɨ͡an naːda ebit, dʼe ol kelbitim. </ta>
            <ta e="T513" id="Seg_3124" s="T504">Ol kelemmin bu͡o ogom onus kɨlaːs ete ((…)), vot. </ta>
            <ta e="T517" id="Seg_3125" s="T513">Maːmata bu͡ollagɨna Džakuskajga erdemmite. </ta>
            <ta e="T524" id="Seg_3126" s="T517">Ontuŋ anɨ ikki ogoloːk, u͡olu gɨtta kɨːs. </ta>
            <ta e="T534" id="Seg_3127" s="T528">– Tɨ͡aga, tuːndraga, hɨldʼallar tɨ͡aga, tabahɨt ere. </ta>
            <ta e="T536" id="Seg_3128" s="T535">Elete. </ta>
            <ta e="T544" id="Seg_3129" s="T536">Hɨndaːskaga kaːlbɨttara anɨ, barɨ͡aktara maːj ɨjga sku͡ola bütteren. </ta>
            <ta e="T548" id="Seg_3130" s="T544">Ogoloro itinne ü͡öreneller Hɨndaːskaga. </ta>
            <ta e="T557" id="Seg_3131" s="T548">Biːrgehe tördüs kɨlaːs, biːrgehe ühüs kɨlas du͡o, ikkis du͡o. </ta>
            <ta e="T562" id="Seg_3132" s="T558">Ikkis kɨlas, eː, kɨːha. </ta>
            <ta e="T571" id="Seg_3133" s="T562">Anɨ min di͡eber kaːlbɨttara anɨ maːjga barɨ͡aktara anɨ, abratna. </ta>
            <ta e="T573" id="Seg_3134" s="T571">Köhön barɨ͡aktara. </ta>
            <ta e="T574" id="Seg_3135" s="T573">Elete. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-KiES">
            <ta e="T2" id="Seg_3136" s="T1">dʼe</ta>
            <ta e="T3" id="Seg_3137" s="T2">Korgoː-go</ta>
            <ta e="T4" id="Seg_3138" s="T3">baːr</ta>
            <ta e="T5" id="Seg_3139" s="T4">e-ti-bit</ta>
            <ta e="T6" id="Seg_3140" s="T5">hir-deːk</ta>
            <ta e="T7" id="Seg_3141" s="T6">e-ti-bit</ta>
            <ta e="T8" id="Seg_3142" s="T7">giniler-ge</ta>
            <ta e="T9" id="Seg_3143" s="T8">Korgoː-go</ta>
            <ta e="T11" id="Seg_3144" s="T9">hir-dem-mit-i-m</ta>
            <ta e="T12" id="Seg_3145" s="T11">onton</ta>
            <ta e="T13" id="Seg_3146" s="T12">bu͡ollagɨna</ta>
            <ta e="T14" id="Seg_3147" s="T13">bu</ta>
            <ta e="T15" id="Seg_3148" s="T14">eː</ta>
            <ta e="T16" id="Seg_3149" s="T15">bu</ta>
            <ta e="T17" id="Seg_3150" s="T16">ogo-nu</ta>
            <ta e="T18" id="Seg_3151" s="T17">töröː-büt-ü-m</ta>
            <ta e="T19" id="Seg_3152" s="T18">onton</ta>
            <ta e="T20" id="Seg_3153" s="T19">ol</ta>
            <ta e="T21" id="Seg_3154" s="T20">u͡ol</ta>
            <ta e="T22" id="Seg_3155" s="T21">ogo-m</ta>
            <ta e="T23" id="Seg_3156" s="T22">töröː-m-mün</ta>
            <ta e="T24" id="Seg_3157" s="T23">ol</ta>
            <ta e="T25" id="Seg_3158" s="T24">onton</ta>
            <ta e="T26" id="Seg_3159" s="T25">bu͡ollagɨna</ta>
            <ta e="T27" id="Seg_3160" s="T26">ogonnʼor-but</ta>
            <ta e="T28" id="Seg_3161" s="T27">ɨ͡aldʼ-ar</ta>
            <ta e="T29" id="Seg_3162" s="T28">e-t-e</ta>
            <ta e="T30" id="Seg_3163" s="T29">öl-ön</ta>
            <ta e="T31" id="Seg_3164" s="T30">öl-ön</ta>
            <ta e="T32" id="Seg_3165" s="T31">ol</ta>
            <ta e="T33" id="Seg_3166" s="T32">kimi͡e-ke</ta>
            <ta e="T34" id="Seg_3167" s="T33">Hɨndaːska-ga</ta>
            <ta e="T35" id="Seg_3168" s="T34">hɨp-pɨp-pɨt</ta>
            <ta e="T36" id="Seg_3169" s="T35">ol</ta>
            <ta e="T37" id="Seg_3170" s="T36">onton</ta>
            <ta e="T38" id="Seg_3171" s="T37">üle-m</ta>
            <ta e="T39" id="Seg_3172" s="T38">minigi-n</ta>
            <ta e="T43" id="Seg_3173" s="T42">bu</ta>
            <ta e="T44" id="Seg_3174" s="T43">ke</ta>
            <ta e="T48" id="Seg_3175" s="T47">onton</ta>
            <ta e="T49" id="Seg_3176" s="T48">üle</ta>
            <ta e="T50" id="Seg_3177" s="T49">bi͡er-bit-tere</ta>
            <ta e="T51" id="Seg_3178" s="T50">mi͡e-ke</ta>
            <ta e="T52" id="Seg_3179" s="T51">saːdik-ka</ta>
            <ta e="T64" id="Seg_3180" s="T63">eː</ta>
            <ta e="T65" id="Seg_3181" s="T64">haka-lɨː</ta>
            <ta e="T66" id="Seg_3182" s="T65">du͡o</ta>
            <ta e="T67" id="Seg_3183" s="T66">Hɨndaːska-ga</ta>
            <ta e="T68" id="Seg_3184" s="T67">haka</ta>
            <ta e="T69" id="Seg_3185" s="T68">haŋardɨː</ta>
            <ta e="T70" id="Seg_3186" s="T69">saːdik</ta>
            <ta e="T71" id="Seg_3187" s="T70">tur-but</ta>
            <ta e="T72" id="Seg_3188" s="T71">e-t-e</ta>
            <ta e="T73" id="Seg_3189" s="T72">dʼe</ta>
            <ta e="T74" id="Seg_3190" s="T73">tug-u</ta>
            <ta e="T75" id="Seg_3191" s="T74">da</ta>
            <ta e="T76" id="Seg_3192" s="T75">bil-pep-pin</ta>
            <ta e="T77" id="Seg_3193" s="T76">saːdik</ta>
            <ta e="T78" id="Seg_3194" s="T77">tu͡ok</ta>
            <ta e="T79" id="Seg_3195" s="T78">aːt-a=j</ta>
            <ta e="T92" id="Seg_3196" s="T91">oː</ta>
            <ta e="T94" id="Seg_3197" s="T93">dʼe</ta>
            <ta e="T95" id="Seg_3198" s="T94">kajdak</ta>
            <ta e="T96" id="Seg_3199" s="T95">bu͡ol-u͡ok-put=uj</ta>
            <ta e="T98" id="Seg_3200" s="T97">hataː-n</ta>
            <ta e="T99" id="Seg_3201" s="T98">haŋar-bap-pɨn</ta>
            <ta e="T100" id="Seg_3202" s="T99">ogo-lor-go</ta>
            <ta e="T101" id="Seg_3203" s="T100">če</ta>
            <ta e="T102" id="Seg_3204" s="T101">ogo-lor-go</ta>
            <ta e="T103" id="Seg_3205" s="T102">hataː-n</ta>
            <ta e="T104" id="Seg_3206" s="T103">buh-a-r-bap-pɨn</ta>
            <ta e="T105" id="Seg_3207" s="T104">d-iː-bin</ta>
            <ta e="T106" id="Seg_3208" s="T105">hu͡ok</ta>
            <ta e="T107" id="Seg_3209" s="T106">povar-dar</ta>
            <ta e="T108" id="Seg_3210" s="T107">diː-l-ler</ta>
            <ta e="T109" id="Seg_3211" s="T108">bu͡o</ta>
            <ta e="T110" id="Seg_3212" s="T109">bu</ta>
            <ta e="T111" id="Seg_3213" s="T110">saːdik</ta>
            <ta e="T112" id="Seg_3214" s="T111">tur-d-a</ta>
            <ta e="T113" id="Seg_3215" s="T112">haŋa</ta>
            <ta e="T114" id="Seg_3216" s="T113">saːdik-ka</ta>
            <ta e="T115" id="Seg_3217" s="T114">onu-ga</ta>
            <ta e="T116" id="Seg_3218" s="T115">poːvar</ta>
            <ta e="T117" id="Seg_3219" s="T116">poːvar</ta>
            <ta e="T118" id="Seg_3220" s="T117">naːda</ta>
            <ta e="T119" id="Seg_3221" s="T118">diː-l-ler</ta>
            <ta e="T120" id="Seg_3222" s="T119">tojot-tor</ta>
            <ta e="T121" id="Seg_3223" s="T120">innʼe</ta>
            <ta e="T122" id="Seg_3224" s="T121">tojot-tor</ta>
            <ta e="T123" id="Seg_3225" s="T122">minigi-n</ta>
            <ta e="T124" id="Seg_3226" s="T123">poːvar</ta>
            <ta e="T125" id="Seg_3227" s="T124">uːr-du-lar</ta>
            <ta e="T126" id="Seg_3228" s="T125">bu͡olla</ta>
            <ta e="T127" id="Seg_3229" s="T126">dʼe</ta>
            <ta e="T128" id="Seg_3230" s="T127">ol</ta>
            <ta e="T129" id="Seg_3231" s="T128">hɨp-pɨp-pɨt</ta>
            <ta e="T130" id="Seg_3232" s="T129">onton</ta>
            <ta e="T131" id="Seg_3233" s="T130">ol</ta>
            <ta e="T132" id="Seg_3234" s="T131">poːvar-daː-m-mɨn</ta>
            <ta e="T133" id="Seg_3235" s="T132">bu͡ollagɨna</ta>
            <ta e="T134" id="Seg_3236" s="T133">eː</ta>
            <ta e="T135" id="Seg_3237" s="T134">kimi͡e-ke</ta>
            <ta e="T136" id="Seg_3238" s="T135">bu</ta>
            <ta e="T137" id="Seg_3239" s="T136">ke</ta>
            <ta e="T138" id="Seg_3240" s="T137">iti</ta>
            <ta e="T139" id="Seg_3241" s="T138">ikki</ta>
            <ta e="T140" id="Seg_3242" s="T139">ogo-m</ta>
            <ta e="T141" id="Seg_3243" s="T140">hɨldʼ-al-lar</ta>
            <ta e="T142" id="Seg_3244" s="T141">saːdik-ka</ta>
            <ta e="T143" id="Seg_3245" s="T142">araj</ta>
            <ta e="T144" id="Seg_3246" s="T143">heː</ta>
            <ta e="T145" id="Seg_3247" s="T144">dʼe</ta>
            <ta e="T146" id="Seg_3248" s="T145">ol</ta>
            <ta e="T147" id="Seg_3249" s="T146">hɨldʼ-an-nar</ta>
            <ta e="T151" id="Seg_3250" s="T150">kim-i</ta>
            <ta e="T152" id="Seg_3251" s="T151">gɨtta</ta>
            <ta e="T157" id="Seg_3252" s="T156">togo</ta>
            <ta e="T158" id="Seg_3253" s="T157">ol</ta>
            <ta e="T159" id="Seg_3254" s="T158">onton</ta>
            <ta e="T160" id="Seg_3255" s="T159">ol</ta>
            <ta e="T161" id="Seg_3256" s="T160">kojut</ta>
            <ta e="T162" id="Seg_3257" s="T161">ol</ta>
            <ta e="T163" id="Seg_3258" s="T162">kim-i-m</ta>
            <ta e="T164" id="Seg_3259" s="T163">ol</ta>
            <ta e="T165" id="Seg_3260" s="T164">edʼij-i-m</ta>
            <ta e="T166" id="Seg_3261" s="T165">öl-büt-e</ta>
            <ta e="T171" id="Seg_3262" s="T168">dʼe</ta>
            <ta e="T172" id="Seg_3263" s="T171">ije-m</ta>
            <ta e="T173" id="Seg_3264" s="T172">ije-m</ta>
            <ta e="T174" id="Seg_3265" s="T173">balt-ɨ-ta</ta>
            <ta e="T175" id="Seg_3266" s="T174">öl-büt-e</ta>
            <ta e="T176" id="Seg_3267" s="T175">ɨ͡aldʼ-an</ta>
            <ta e="T177" id="Seg_3268" s="T176">oččogo</ta>
            <ta e="T178" id="Seg_3269" s="T177">tü͡ört</ta>
            <ta e="T179" id="Seg_3270" s="T178">ogo-loːk</ta>
            <ta e="T180" id="Seg_3271" s="T179">kaːl-l-a</ta>
            <ta e="T181" id="Seg_3272" s="T180">bu</ta>
            <ta e="T182" id="Seg_3273" s="T181">kihi</ta>
            <ta e="T183" id="Seg_3274" s="T182">tü͡ört</ta>
            <ta e="T184" id="Seg_3275" s="T183">ogo</ta>
            <ta e="T185" id="Seg_3276" s="T184">bu͡olla</ta>
            <ta e="T186" id="Seg_3277" s="T185">mini͡e-ke</ta>
            <ta e="T187" id="Seg_3278" s="T186">kel-li-ler</ta>
            <ta e="T188" id="Seg_3279" s="T187">bu͡olla</ta>
            <ta e="T189" id="Seg_3280" s="T188">bu͡o</ta>
            <ta e="T190" id="Seg_3281" s="T189">edʼiːj-biti-ger</ta>
            <ta e="T191" id="Seg_3282" s="T190">bu͡ol-u͡ok-put</ta>
            <ta e="T192" id="Seg_3283" s="T191">bihigi</ta>
            <ta e="T193" id="Seg_3284" s="T192">kajdi͡et</ta>
            <ta e="T194" id="Seg_3285" s="T193">da</ta>
            <ta e="T195" id="Seg_3286" s="T194">bar-bap-pɨt</ta>
            <ta e="T196" id="Seg_3287" s="T195">diː-l-ler</ta>
            <ta e="T197" id="Seg_3288" s="T196">inʼe-m</ta>
            <ta e="T198" id="Seg_3289" s="T197">eː</ta>
            <ta e="T199" id="Seg_3290" s="T198">inʼe</ta>
            <ta e="T200" id="Seg_3291" s="T199">inʼe-te</ta>
            <ta e="T201" id="Seg_3292" s="T200">geri͡es</ta>
            <ta e="T202" id="Seg_3293" s="T201">ep-pit</ta>
            <ta e="T203" id="Seg_3294" s="T202">bu͡olla</ta>
            <ta e="T204" id="Seg_3295" s="T203">bu</ta>
            <ta e="T205" id="Seg_3296" s="T204">ke</ta>
            <ta e="T206" id="Seg_3297" s="T205">edʼiːj-giti-ger</ta>
            <ta e="T207" id="Seg_3298" s="T206">kaːl-an</ta>
            <ta e="T208" id="Seg_3299" s="T207">di͡e-bit</ta>
            <ta e="T209" id="Seg_3300" s="T208">bu͡olla</ta>
            <ta e="T210" id="Seg_3301" s="T209">min</ta>
            <ta e="T211" id="Seg_3302" s="T210">kajdak</ta>
            <ta e="T212" id="Seg_3303" s="T211">bu͡ol-lak-pɨna</ta>
            <ta e="T213" id="Seg_3304" s="T212">dʼe</ta>
            <ta e="T214" id="Seg_3305" s="T213">ol</ta>
            <ta e="T215" id="Seg_3306" s="T214">ol</ta>
            <ta e="T216" id="Seg_3307" s="T215">tü͡ört</ta>
            <ta e="T217" id="Seg_3308" s="T216">tü͡ört</ta>
            <ta e="T218" id="Seg_3309" s="T217">ogo</ta>
            <ta e="T219" id="Seg_3310" s="T218">bihi͡e-ke</ta>
            <ta e="T220" id="Seg_3311" s="T219">kel-li-ler</ta>
            <ta e="T221" id="Seg_3312" s="T220">araj</ta>
            <ta e="T222" id="Seg_3313" s="T221">mhm</ta>
            <ta e="T223" id="Seg_3314" s="T222">Nosku͡o</ta>
            <ta e="T224" id="Seg_3315" s="T223">ü͡ören-e</ta>
            <ta e="T225" id="Seg_3316" s="T224">bar-al-lar</ta>
            <ta e="T231" id="Seg_3317" s="T230">dʼe</ta>
            <ta e="T232" id="Seg_3318" s="T231">beːj</ta>
            <ta e="T233" id="Seg_3319" s="T232">onton</ta>
            <ta e="T237" id="Seg_3320" s="T236">dʼe</ta>
            <ta e="T238" id="Seg_3321" s="T237">onton</ta>
            <ta e="T239" id="Seg_3322" s="T238">kanʼikul-ga</ta>
            <ta e="T240" id="Seg_3323" s="T239">kel-el-ler</ta>
            <ta e="T241" id="Seg_3324" s="T240">bu</ta>
            <ta e="T242" id="Seg_3325" s="T241">ogo-lor</ta>
            <ta e="T243" id="Seg_3326" s="T242">hübeliː-l-ler</ta>
            <ta e="T244" id="Seg_3327" s="T243">tojot-tor</ta>
            <ta e="T245" id="Seg_3328" s="T244">araj</ta>
            <ta e="T246" id="Seg_3329" s="T245">bu</ta>
            <ta e="T247" id="Seg_3330" s="T246">ogo-nu</ta>
            <ta e="T248" id="Seg_3331" s="T247">bu͡olla</ta>
            <ta e="T249" id="Seg_3332" s="T248">Pöpügej-ge</ta>
            <ta e="T250" id="Seg_3333" s="T249">ɨːt-ɨ͡ak-ka</ta>
            <ta e="T251" id="Seg_3334" s="T250">diː-l-ler</ta>
            <ta e="T253" id="Seg_3335" s="T251">uruː-tu-gar</ta>
            <ta e="T257" id="Seg_3336" s="T255">mhm</ta>
            <ta e="T258" id="Seg_3337" s="T257">Keːtʼe-ni</ta>
            <ta e="T259" id="Seg_3338" s="T258">biːr</ta>
            <ta e="T260" id="Seg_3339" s="T259">biːr</ta>
            <ta e="T261" id="Seg_3340" s="T260">ogo-nu</ta>
            <ta e="T262" id="Seg_3341" s="T261">bu͡olla</ta>
            <ta e="T263" id="Seg_3342" s="T262">munna</ta>
            <ta e="T264" id="Seg_3343" s="T263">tɨ͡a-ga</ta>
            <ta e="T265" id="Seg_3344" s="T264">baːr</ta>
            <ta e="T266" id="Seg_3345" s="T265">tundra</ta>
            <ta e="T267" id="Seg_3346" s="T266">ogonnʼor-doːk</ta>
            <ta e="T268" id="Seg_3347" s="T267">emeːksin</ta>
            <ta e="T269" id="Seg_3348" s="T268">uruː-but</ta>
            <ta e="T270" id="Seg_3349" s="T269">araj</ta>
            <ta e="T271" id="Seg_3350" s="T270">dʼe</ta>
            <ta e="T272" id="Seg_3351" s="T271">o-lor-go</ta>
            <ta e="T273" id="Seg_3352" s="T272">bi͡er-di-ler</ta>
            <ta e="T274" id="Seg_3353" s="T273">oččogo</ta>
            <ta e="T275" id="Seg_3354" s="T274">min</ta>
            <ta e="T276" id="Seg_3355" s="T275">ikki</ta>
            <ta e="T277" id="Seg_3356" s="T276">u͡ol</ta>
            <ta e="T278" id="Seg_3357" s="T277">ogo-nu</ta>
            <ta e="T279" id="Seg_3358" s="T278">gɨtta</ta>
            <ta e="T280" id="Seg_3359" s="T279">kaːl-lɨ-m</ta>
            <ta e="T281" id="Seg_3360" s="T280">bu͡o</ta>
            <ta e="T282" id="Seg_3361" s="T281">araj</ta>
            <ta e="T283" id="Seg_3362" s="T282">iti-ler</ta>
            <ta e="T284" id="Seg_3363" s="T283">mini͡e-ke</ta>
            <ta e="T285" id="Seg_3364" s="T284">bar</ta>
            <ta e="T286" id="Seg_3365" s="T285">bar-bat-tar</ta>
            <ta e="T287" id="Seg_3366" s="T286">töttörü</ta>
            <ta e="T288" id="Seg_3367" s="T287">iti</ta>
            <ta e="T289" id="Seg_3368" s="T288">ikki</ta>
            <ta e="T290" id="Seg_3369" s="T289">u͡ol</ta>
            <ta e="T291" id="Seg_3370" s="T290">ogo</ta>
            <ta e="T292" id="Seg_3371" s="T291">dʼe</ta>
            <ta e="T293" id="Seg_3372" s="T292">onton</ta>
            <ta e="T294" id="Seg_3373" s="T293">töhö</ta>
            <ta e="T295" id="Seg_3374" s="T294">da</ta>
            <ta e="T296" id="Seg_3375" s="T295">bu͡ol-bakka</ta>
            <ta e="T297" id="Seg_3376" s="T296">kanʼikul-ga</ta>
            <ta e="T298" id="Seg_3377" s="T297">kel-bit-tere</ta>
            <ta e="T299" id="Seg_3378" s="T298">ol</ta>
            <ta e="T300" id="Seg_3379" s="T299">ogo-lor-u-ŋ</ta>
            <ta e="T301" id="Seg_3380" s="T300">bu͡o</ta>
            <ta e="T302" id="Seg_3381" s="T301">töttörü</ta>
            <ta e="T303" id="Seg_3382" s="T302">kel-li-ler</ta>
            <ta e="T304" id="Seg_3383" s="T303">bihi͡e-ke</ta>
            <ta e="T305" id="Seg_3384" s="T304">ol</ta>
            <ta e="T306" id="Seg_3385" s="T305">höbüleː-bet-ter</ta>
            <ta e="T307" id="Seg_3386" s="T306">e-bit</ta>
            <ta e="T308" id="Seg_3387" s="T307">o-lor-u</ta>
            <ta e="T309" id="Seg_3388" s="T308">onton</ta>
            <ta e="T310" id="Seg_3389" s="T309">tundra-ga</ta>
            <ta e="T311" id="Seg_3390" s="T310">ogonnʼor-doːk</ta>
            <ta e="T312" id="Seg_3391" s="T311">emeːksin</ta>
            <ta e="T313" id="Seg_3392" s="T312">emi͡e</ta>
            <ta e="T314" id="Seg_3393" s="T313">ol</ta>
            <ta e="T315" id="Seg_3394" s="T314">bar-bɨt</ta>
            <ta e="T316" id="Seg_3395" s="T315">ikki</ta>
            <ta e="T317" id="Seg_3396" s="T316">ogo-but</ta>
            <ta e="T318" id="Seg_3397" s="T317">emi͡e</ta>
            <ta e="T319" id="Seg_3398" s="T318">emi͡e</ta>
            <ta e="T320" id="Seg_3399" s="T319">töttörü</ta>
            <ta e="T321" id="Seg_3400" s="T320">kel-l-e</ta>
            <ta e="T322" id="Seg_3401" s="T321">bu͡o</ta>
            <ta e="T323" id="Seg_3402" s="T322">oččogo</ta>
            <ta e="T324" id="Seg_3403" s="T323">agɨs</ta>
            <ta e="T325" id="Seg_3404" s="T324">ogo-loːk</ta>
            <ta e="T326" id="Seg_3405" s="T325">bu͡ol-lu-m</ta>
            <ta e="T327" id="Seg_3406" s="T326">bu͡o</ta>
            <ta e="T328" id="Seg_3407" s="T327">min</ta>
            <ta e="T330" id="Seg_3408" s="T329">beje-m</ta>
            <ta e="T331" id="Seg_3409" s="T330">tü͡ört</ta>
            <ta e="T332" id="Seg_3410" s="T331">ogo-loːp-pun</ta>
            <ta e="T333" id="Seg_3411" s="T332">bu͡olla</ta>
            <ta e="T334" id="Seg_3412" s="T333">iti</ta>
            <ta e="T335" id="Seg_3413" s="T334">ikki</ta>
            <ta e="T336" id="Seg_3414" s="T335">kɨːs-taːp-pɨn</ta>
            <ta e="T337" id="Seg_3415" s="T336">bu͡olla</ta>
            <ta e="T338" id="Seg_3416" s="T337">inʼe-m</ta>
            <ta e="T339" id="Seg_3417" s="T338">kɨrdʼagas</ta>
            <ta e="T340" id="Seg_3418" s="T339">ubaj-ɨ-m</ta>
            <ta e="T341" id="Seg_3419" s="T340">kɨrdʼagas</ta>
            <ta e="T342" id="Seg_3420" s="T341">onton</ta>
            <ta e="T343" id="Seg_3421" s="T342">ol</ta>
            <ta e="T344" id="Seg_3422" s="T343">agɨs</ta>
            <ta e="T345" id="Seg_3423" s="T344">ogo-loːk-pun</ta>
            <ta e="T346" id="Seg_3424" s="T345">beje-m</ta>
            <ta e="T347" id="Seg_3425" s="T346">edʼiːj-i-m</ta>
            <ta e="T348" id="Seg_3426" s="T347">gi͡en-e</ta>
            <ta e="T349" id="Seg_3427" s="T348">tü͡ört</ta>
            <ta e="T350" id="Seg_3428" s="T349">ogo</ta>
            <ta e="T351" id="Seg_3429" s="T350">kaːl-l-a</ta>
            <ta e="T352" id="Seg_3430" s="T351">mini͡ene</ta>
            <ta e="T353" id="Seg_3431" s="T352">tü͡ört</ta>
            <ta e="T354" id="Seg_3432" s="T353">ogo</ta>
            <ta e="T355" id="Seg_3433" s="T354">bu͡olla</ta>
            <ta e="T360" id="Seg_3434" s="T359">küččügüj</ta>
            <ta e="T361" id="Seg_3435" s="T360">e-t-e</ta>
            <ta e="T362" id="Seg_3436" s="T361">ol</ta>
            <ta e="T363" id="Seg_3437" s="T362">dʼe</ta>
            <ta e="T364" id="Seg_3438" s="T363">ol</ta>
            <ta e="T365" id="Seg_3439" s="T364">kaːl-am-mɨn</ta>
            <ta e="T366" id="Seg_3440" s="T365">dʼe</ta>
            <ta e="T367" id="Seg_3441" s="T366">iti</ta>
            <ta e="T368" id="Seg_3442" s="T367">olor-du-but</ta>
            <ta e="T369" id="Seg_3443" s="T368">bu͡olla</ta>
            <ta e="T370" id="Seg_3444" s="T369">inʼe-m</ta>
            <ta e="T371" id="Seg_3445" s="T370">kɨrdʼ-an</ta>
            <ta e="T372" id="Seg_3446" s="T371">öl-l-ö</ta>
            <ta e="T373" id="Seg_3447" s="T372">ɨ͡aldʼ-an</ta>
            <ta e="T374" id="Seg_3448" s="T373">ol</ta>
            <ta e="T375" id="Seg_3449" s="T374">ubaj-ɨ-m</ta>
            <ta e="T376" id="Seg_3450" s="T375">emi͡e</ta>
            <ta e="T377" id="Seg_3451" s="T376">dʼaktar-a</ta>
            <ta e="T378" id="Seg_3452" s="T377">da</ta>
            <ta e="T379" id="Seg_3453" s="T378">hu͡ok</ta>
            <ta e="T380" id="Seg_3454" s="T379">tu͡ok</ta>
            <ta e="T381" id="Seg_3455" s="T380">da</ta>
            <ta e="T382" id="Seg_3456" s="T381">hu͡ok</ta>
            <ta e="T383" id="Seg_3457" s="T382">e-t-e</ta>
            <ta e="T384" id="Seg_3458" s="T383">laːpkɨ-ga</ta>
            <ta e="T385" id="Seg_3459" s="T384">üleleː-čči</ta>
            <ta e="T386" id="Seg_3460" s="T385">e-t-e</ta>
            <ta e="T387" id="Seg_3461" s="T386">ol</ta>
            <ta e="T388" id="Seg_3462" s="T387">ihin</ta>
            <ta e="T389" id="Seg_3463" s="T388">ah-ɨ-nan</ta>
            <ta e="T390" id="Seg_3464" s="T389">da</ta>
            <ta e="T391" id="Seg_3465" s="T390">olus</ta>
            <ta e="T392" id="Seg_3466" s="T391">da</ta>
            <ta e="T393" id="Seg_3467" s="T392">kim-neː-čči-te</ta>
            <ta e="T394" id="Seg_3468" s="T393">hu͡ok</ta>
            <ta e="T395" id="Seg_3469" s="T394">e-ti-bit</ta>
            <ta e="T396" id="Seg_3470" s="T395">üleliː-r</ta>
            <ta e="T397" id="Seg_3471" s="T396">bu͡ol-an</ta>
            <ta e="T399" id="Seg_3472" s="T398">egel-en</ta>
            <ta e="T400" id="Seg_3473" s="T399">ih-eːčči</ta>
            <ta e="T401" id="Seg_3474" s="T400">as</ta>
            <ta e="T402" id="Seg_3475" s="T401">laːpkɨ-ttan</ta>
            <ta e="T403" id="Seg_3476" s="T402">harpɨlaːk-kaːn</ta>
            <ta e="T404" id="Seg_3477" s="T403">iti</ta>
            <ta e="T405" id="Seg_3478" s="T404">ɨl-aːččɨ</ta>
            <ta e="T406" id="Seg_3479" s="T405">onton</ta>
            <ta e="T407" id="Seg_3480" s="T406">min</ta>
            <ta e="T408" id="Seg_3481" s="T407">poːvar-d-iː-bin</ta>
            <ta e="T409" id="Seg_3482" s="T408">bu͡olla</ta>
            <ta e="T410" id="Seg_3483" s="T409">ol</ta>
            <ta e="T411" id="Seg_3484" s="T410">iti</ta>
            <ta e="T412" id="Seg_3485" s="T411">olor-bup-put</ta>
            <ta e="T413" id="Seg_3486" s="T412">barɨ-kaːt-tara</ta>
            <ta e="T414" id="Seg_3487" s="T413">ulaːp-pɨt-tara</ta>
            <ta e="T415" id="Seg_3488" s="T414">eni</ta>
            <ta e="T416" id="Seg_3489" s="T415">dʼaktar-daːk-tar</ta>
            <ta e="T422" id="Seg_3490" s="T421">dʼe</ta>
            <ta e="T423" id="Seg_3491" s="T422">taks-ɨ-bɨt-a</ta>
            <ta e="T424" id="Seg_3492" s="T423">tu͡ok</ta>
            <ta e="T425" id="Seg_3493" s="T424">gɨn-aːrɨ</ta>
            <ta e="T426" id="Seg_3494" s="T425">da</ta>
            <ta e="T427" id="Seg_3495" s="T426">taks-ɨ-bɨt</ta>
            <ta e="T428" id="Seg_3496" s="T427">dʼe</ta>
            <ta e="T429" id="Seg_3497" s="T428">ol</ta>
            <ta e="T430" id="Seg_3498" s="T429">ihin</ta>
            <ta e="T431" id="Seg_3499" s="T430">agɨs</ta>
            <ta e="T432" id="Seg_3500" s="T431">ogo</ta>
            <ta e="T433" id="Seg_3501" s="T432">bu͡ol-ar</ta>
            <ta e="T434" id="Seg_3502" s="T433">bu͡olla</ta>
            <ta e="T435" id="Seg_3503" s="T434">maladoj</ta>
            <ta e="T436" id="Seg_3504" s="T435">dʼaktar-bɨn</ta>
            <ta e="T437" id="Seg_3505" s="T436">bu͡ol</ta>
            <ta e="T438" id="Seg_3506" s="T437">onno</ta>
            <ta e="T439" id="Seg_3507" s="T438">ol</ta>
            <ta e="T443" id="Seg_3508" s="T442">ke</ta>
            <ta e="T444" id="Seg_3509" s="T443">er-ge</ta>
            <ta e="T445" id="Seg_3510" s="T444">taks-an</ta>
            <ta e="T446" id="Seg_3511" s="T445">kaːl-lɨ-m</ta>
            <ta e="T447" id="Seg_3512" s="T446">bu͡o</ta>
            <ta e="T448" id="Seg_3513" s="T447">ikki</ta>
            <ta e="T449" id="Seg_3514" s="T448">ogo-nu</ta>
            <ta e="T450" id="Seg_3515" s="T449">töröː-büt</ta>
            <ta e="T451" id="Seg_3516" s="T450">bu͡olla</ta>
            <ta e="T452" id="Seg_3517" s="T451">ol</ta>
            <ta e="T453" id="Seg_3518" s="T452">üh-üs</ta>
            <ta e="T454" id="Seg_3519" s="T453">üh-üs</ta>
            <ta e="T455" id="Seg_3520" s="T454">ogo-m</ta>
            <ta e="T456" id="Seg_3521" s="T455">u͡ol</ta>
            <ta e="T457" id="Seg_3522" s="T456">ogo</ta>
            <ta e="T458" id="Seg_3523" s="T457">e-t-e</ta>
            <ta e="T459" id="Seg_3524" s="T458">on-tu-m</ta>
            <ta e="T460" id="Seg_3525" s="T459">tüs-püt-e</ta>
            <ta e="T461" id="Seg_3526" s="T460">ölör-ü-n-em-min</ta>
            <ta e="T463" id="Seg_3527" s="T462">saːdik-ka</ta>
            <ta e="T464" id="Seg_3528" s="T463">buːs-tar-ɨ</ta>
            <ta e="T465" id="Seg_3529" s="T464">uːgol-lar-ɨ</ta>
            <ta e="T466" id="Seg_3530" s="T465">taskaj-daː-n</ta>
            <ta e="T467" id="Seg_3531" s="T466">ol</ta>
            <ta e="T468" id="Seg_3532" s="T467">tüs-püt</ta>
            <ta e="T469" id="Seg_3533" s="T468">e-t-e</ta>
            <ta e="T470" id="Seg_3534" s="T469">iti</ta>
            <ta e="T471" id="Seg_3535" s="T470">ikki</ta>
            <ta e="T472" id="Seg_3536" s="T471">u͡ol</ta>
            <ta e="T473" id="Seg_3537" s="T472">ɨkkardɨ-ta</ta>
            <ta e="T481" id="Seg_3538" s="T480">Ölöːnö</ta>
            <ta e="T482" id="Seg_3539" s="T481">alta</ta>
            <ta e="T483" id="Seg_3540" s="T482">dʼɨl-laːg-ɨ</ta>
            <ta e="T484" id="Seg_3541" s="T483">ɨl-bɨp-pɨt</ta>
            <ta e="T485" id="Seg_3542" s="T484">onton</ta>
            <ta e="T486" id="Seg_3543" s="T485">hette</ta>
            <ta e="T487" id="Seg_3544" s="T486">dʼɨl-lan-ar-ɨ-gar</ta>
            <ta e="T488" id="Seg_3545" s="T487">munna</ta>
            <ta e="T489" id="Seg_3546" s="T488">kel-li-m</ta>
            <ta e="T490" id="Seg_3547" s="T489">bu͡olla</ta>
            <ta e="T491" id="Seg_3548" s="T490">Dudʼinka-ga</ta>
            <ta e="T492" id="Seg_3549" s="T491">ogo-m</ta>
            <ta e="T493" id="Seg_3550" s="T492">dʼi͡e</ta>
            <ta e="T494" id="Seg_3551" s="T493">ɨl-ar</ta>
            <ta e="T495" id="Seg_3552" s="T494">e-bit</ta>
            <ta e="T496" id="Seg_3553" s="T495">araj</ta>
            <ta e="T497" id="Seg_3554" s="T496">dʼe</ta>
            <ta e="T498" id="Seg_3555" s="T497">onu-ga</ta>
            <ta e="T499" id="Seg_3556" s="T498">prapʼiska-l-ɨ͡a-n</ta>
            <ta e="T500" id="Seg_3557" s="T499">naːda</ta>
            <ta e="T501" id="Seg_3558" s="T500">e-bit</ta>
            <ta e="T502" id="Seg_3559" s="T501">dʼe</ta>
            <ta e="T503" id="Seg_3560" s="T502">ol</ta>
            <ta e="T504" id="Seg_3561" s="T503">kel-bit-i-m</ta>
            <ta e="T505" id="Seg_3562" s="T504">ol</ta>
            <ta e="T506" id="Seg_3563" s="T505">kel-em-min</ta>
            <ta e="T507" id="Seg_3564" s="T506">bu͡o</ta>
            <ta e="T508" id="Seg_3565" s="T507">ogo-m</ta>
            <ta e="T509" id="Seg_3566" s="T508">on-us</ta>
            <ta e="T510" id="Seg_3567" s="T509">kɨlaːs</ta>
            <ta e="T511" id="Seg_3568" s="T510">e-t-e</ta>
            <ta e="T513" id="Seg_3569" s="T512">vot</ta>
            <ta e="T514" id="Seg_3570" s="T513">maːma-ta</ta>
            <ta e="T515" id="Seg_3571" s="T514">bu͡ollagɨna</ta>
            <ta e="T516" id="Seg_3572" s="T515">Džakuskaj-ga</ta>
            <ta e="T517" id="Seg_3573" s="T516">er-dem-mit-e</ta>
            <ta e="T518" id="Seg_3574" s="T517">on-tu-ŋ</ta>
            <ta e="T519" id="Seg_3575" s="T518">anɨ</ta>
            <ta e="T520" id="Seg_3576" s="T519">ikki</ta>
            <ta e="T521" id="Seg_3577" s="T520">ogo-loːk</ta>
            <ta e="T522" id="Seg_3578" s="T521">u͡ol-u</ta>
            <ta e="T523" id="Seg_3579" s="T522">gɨtta</ta>
            <ta e="T524" id="Seg_3580" s="T523">kɨːs</ta>
            <ta e="T529" id="Seg_3581" s="T528">tɨ͡a-ga</ta>
            <ta e="T530" id="Seg_3582" s="T529">tuːndra-ga</ta>
            <ta e="T531" id="Seg_3583" s="T530">hɨldʼ-al-lar</ta>
            <ta e="T532" id="Seg_3584" s="T531">tɨ͡a-ga</ta>
            <ta e="T533" id="Seg_3585" s="T532">taba-hɨt</ta>
            <ta e="T534" id="Seg_3586" s="T533">er-e</ta>
            <ta e="T536" id="Seg_3587" s="T535">ele-te</ta>
            <ta e="T537" id="Seg_3588" s="T536">Hɨndaːska-ga</ta>
            <ta e="T538" id="Seg_3589" s="T537">kaːl-bɨt-tara</ta>
            <ta e="T539" id="Seg_3590" s="T538">anɨ</ta>
            <ta e="T540" id="Seg_3591" s="T539">bar-ɨ͡ak-tara</ta>
            <ta e="T541" id="Seg_3592" s="T540">maːj</ta>
            <ta e="T542" id="Seg_3593" s="T541">ɨj-ga</ta>
            <ta e="T543" id="Seg_3594" s="T542">sku͡ola</ta>
            <ta e="T544" id="Seg_3595" s="T543">büt-ter-en</ta>
            <ta e="T545" id="Seg_3596" s="T544">ogo-loro</ta>
            <ta e="T546" id="Seg_3597" s="T545">itinne</ta>
            <ta e="T547" id="Seg_3598" s="T546">ü͡ören-el-ler</ta>
            <ta e="T548" id="Seg_3599" s="T547">Hɨndaːska-ga</ta>
            <ta e="T549" id="Seg_3600" s="T548">biːrgeh-e</ta>
            <ta e="T550" id="Seg_3601" s="T549">törd-üs</ta>
            <ta e="T551" id="Seg_3602" s="T550">kɨlaːs</ta>
            <ta e="T552" id="Seg_3603" s="T551">biːrgeh-e</ta>
            <ta e="T553" id="Seg_3604" s="T552">üh-üs</ta>
            <ta e="T554" id="Seg_3605" s="T553">kɨlas</ta>
            <ta e="T555" id="Seg_3606" s="T554">du͡o</ta>
            <ta e="T556" id="Seg_3607" s="T555">ikki-s</ta>
            <ta e="T557" id="Seg_3608" s="T556">du͡o</ta>
            <ta e="T559" id="Seg_3609" s="T558">ikki-s</ta>
            <ta e="T560" id="Seg_3610" s="T559">kɨlas</ta>
            <ta e="T561" id="Seg_3611" s="T560">eː</ta>
            <ta e="T562" id="Seg_3612" s="T561">kɨːh-a</ta>
            <ta e="T563" id="Seg_3613" s="T562">anɨ</ta>
            <ta e="T564" id="Seg_3614" s="T563">min</ta>
            <ta e="T565" id="Seg_3615" s="T564">dʼi͡e-be-r</ta>
            <ta e="T566" id="Seg_3616" s="T565">kaːl-bɨt-tara</ta>
            <ta e="T567" id="Seg_3617" s="T566">anɨ</ta>
            <ta e="T568" id="Seg_3618" s="T567">maːj-ga</ta>
            <ta e="T569" id="Seg_3619" s="T568">bar-ɨ͡ak-tara</ta>
            <ta e="T570" id="Seg_3620" s="T569">anɨ</ta>
            <ta e="T572" id="Seg_3621" s="T571">köh-ön</ta>
            <ta e="T573" id="Seg_3622" s="T572">bar-ɨ͡ak-tara</ta>
            <ta e="T574" id="Seg_3623" s="T573">ele-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp-KiES">
            <ta e="T2" id="Seg_3624" s="T1">dʼe</ta>
            <ta e="T3" id="Seg_3625" s="T2">Korgo-GA</ta>
            <ta e="T4" id="Seg_3626" s="T3">baːr</ta>
            <ta e="T5" id="Seg_3627" s="T4">e-TI-BIt</ta>
            <ta e="T6" id="Seg_3628" s="T5">hir-LAːK</ta>
            <ta e="T7" id="Seg_3629" s="T6">e-TI-BIt</ta>
            <ta e="T8" id="Seg_3630" s="T7">giniler-GA</ta>
            <ta e="T9" id="Seg_3631" s="T8">Korgo-GA</ta>
            <ta e="T11" id="Seg_3632" s="T9">hir-LAN-BIT-I-m</ta>
            <ta e="T12" id="Seg_3633" s="T11">onton</ta>
            <ta e="T13" id="Seg_3634" s="T12">bu͡ollagɨna</ta>
            <ta e="T14" id="Seg_3635" s="T13">bu</ta>
            <ta e="T15" id="Seg_3636" s="T14">eː</ta>
            <ta e="T16" id="Seg_3637" s="T15">bu</ta>
            <ta e="T17" id="Seg_3638" s="T16">ogo-nI</ta>
            <ta e="T18" id="Seg_3639" s="T17">töröː-BIT-I-m</ta>
            <ta e="T19" id="Seg_3640" s="T18">onton</ta>
            <ta e="T20" id="Seg_3641" s="T19">ol</ta>
            <ta e="T21" id="Seg_3642" s="T20">u͡ol</ta>
            <ta e="T22" id="Seg_3643" s="T21">ogo-m</ta>
            <ta e="T23" id="Seg_3644" s="T22">töröː-An-BIn</ta>
            <ta e="T24" id="Seg_3645" s="T23">ol</ta>
            <ta e="T25" id="Seg_3646" s="T24">onton</ta>
            <ta e="T26" id="Seg_3647" s="T25">bu͡ollagɨna</ta>
            <ta e="T27" id="Seg_3648" s="T26">ogonnʼor-BIt</ta>
            <ta e="T28" id="Seg_3649" s="T27">ɨ͡arɨj-Ar</ta>
            <ta e="T29" id="Seg_3650" s="T28">e-TI-tA</ta>
            <ta e="T30" id="Seg_3651" s="T29">öl-An</ta>
            <ta e="T31" id="Seg_3652" s="T30">öl-An</ta>
            <ta e="T32" id="Seg_3653" s="T31">ol</ta>
            <ta e="T33" id="Seg_3654" s="T32">kim-GA</ta>
            <ta e="T34" id="Seg_3655" s="T33">Hɨndaːska-GA</ta>
            <ta e="T35" id="Seg_3656" s="T34">hɨt-BIT-BIt</ta>
            <ta e="T36" id="Seg_3657" s="T35">ol</ta>
            <ta e="T37" id="Seg_3658" s="T36">onton</ta>
            <ta e="T38" id="Seg_3659" s="T37">üle-m</ta>
            <ta e="T39" id="Seg_3660" s="T38">min-n</ta>
            <ta e="T43" id="Seg_3661" s="T42">bu</ta>
            <ta e="T44" id="Seg_3662" s="T43">ka</ta>
            <ta e="T48" id="Seg_3663" s="T47">onton</ta>
            <ta e="T49" id="Seg_3664" s="T48">üle</ta>
            <ta e="T50" id="Seg_3665" s="T49">bi͡er-BIT-LArA</ta>
            <ta e="T51" id="Seg_3666" s="T50">min-GA</ta>
            <ta e="T52" id="Seg_3667" s="T51">saːdik-GA</ta>
            <ta e="T64" id="Seg_3668" s="T63">eː</ta>
            <ta e="T65" id="Seg_3669" s="T64">haka-LIː</ta>
            <ta e="T66" id="Seg_3670" s="T65">du͡o</ta>
            <ta e="T67" id="Seg_3671" s="T66">Hɨndaːska-GA</ta>
            <ta e="T68" id="Seg_3672" s="T67">haka</ta>
            <ta e="T69" id="Seg_3673" s="T68">haŋardɨː</ta>
            <ta e="T70" id="Seg_3674" s="T69">saːdik</ta>
            <ta e="T71" id="Seg_3675" s="T70">tur-BIT</ta>
            <ta e="T72" id="Seg_3676" s="T71">e-TI-tA</ta>
            <ta e="T73" id="Seg_3677" s="T72">dʼe</ta>
            <ta e="T74" id="Seg_3678" s="T73">tu͡ok-nI</ta>
            <ta e="T75" id="Seg_3679" s="T74">da</ta>
            <ta e="T76" id="Seg_3680" s="T75">bil-BAT-BIn</ta>
            <ta e="T77" id="Seg_3681" s="T76">saːdik</ta>
            <ta e="T78" id="Seg_3682" s="T77">tu͡ok</ta>
            <ta e="T79" id="Seg_3683" s="T78">aːt-tA=Ij</ta>
            <ta e="T92" id="Seg_3684" s="T91">oː</ta>
            <ta e="T94" id="Seg_3685" s="T93">dʼe</ta>
            <ta e="T95" id="Seg_3686" s="T94">kajdak</ta>
            <ta e="T96" id="Seg_3687" s="T95">bu͡ol-IAK-BIt=Ij</ta>
            <ta e="T98" id="Seg_3688" s="T97">hataː-An</ta>
            <ta e="T99" id="Seg_3689" s="T98">haŋar-BAT-BIn</ta>
            <ta e="T100" id="Seg_3690" s="T99">ogo-LAr-GA</ta>
            <ta e="T101" id="Seg_3691" s="T100">dʼe</ta>
            <ta e="T102" id="Seg_3692" s="T101">ogo-LAr-GA</ta>
            <ta e="T103" id="Seg_3693" s="T102">hataː-An</ta>
            <ta e="T104" id="Seg_3694" s="T103">bus-A-r-BAT-BIn</ta>
            <ta e="T105" id="Seg_3695" s="T104">di͡e-A-BIn</ta>
            <ta e="T106" id="Seg_3696" s="T105">hu͡ok</ta>
            <ta e="T107" id="Seg_3697" s="T106">poːvar-LAr</ta>
            <ta e="T108" id="Seg_3698" s="T107">di͡e-Ar-LAr</ta>
            <ta e="T109" id="Seg_3699" s="T108">bu͡o</ta>
            <ta e="T110" id="Seg_3700" s="T109">bu</ta>
            <ta e="T111" id="Seg_3701" s="T110">saːdik</ta>
            <ta e="T112" id="Seg_3702" s="T111">tur-TI-tA</ta>
            <ta e="T113" id="Seg_3703" s="T112">haŋa</ta>
            <ta e="T114" id="Seg_3704" s="T113">saːdik-GA</ta>
            <ta e="T115" id="Seg_3705" s="T114">ol-GA</ta>
            <ta e="T116" id="Seg_3706" s="T115">poːvar</ta>
            <ta e="T117" id="Seg_3707" s="T116">poːvar</ta>
            <ta e="T118" id="Seg_3708" s="T117">naːda</ta>
            <ta e="T119" id="Seg_3709" s="T118">di͡e-Ar-LAr</ta>
            <ta e="T120" id="Seg_3710" s="T119">tojon-LAr</ta>
            <ta e="T121" id="Seg_3711" s="T120">innʼe</ta>
            <ta e="T122" id="Seg_3712" s="T121">tojon-LAr</ta>
            <ta e="T123" id="Seg_3713" s="T122">min-n</ta>
            <ta e="T124" id="Seg_3714" s="T123">poːvar</ta>
            <ta e="T125" id="Seg_3715" s="T124">uːr-TI-LAr</ta>
            <ta e="T126" id="Seg_3716" s="T125">bu͡olla</ta>
            <ta e="T127" id="Seg_3717" s="T126">dʼe</ta>
            <ta e="T128" id="Seg_3718" s="T127">ol</ta>
            <ta e="T129" id="Seg_3719" s="T128">hɨt-BIT-BIt</ta>
            <ta e="T130" id="Seg_3720" s="T129">onton</ta>
            <ta e="T131" id="Seg_3721" s="T130">ol</ta>
            <ta e="T132" id="Seg_3722" s="T131">poːvar-LAː-An-BIn</ta>
            <ta e="T133" id="Seg_3723" s="T132">bu͡ollagɨna</ta>
            <ta e="T134" id="Seg_3724" s="T133">eː</ta>
            <ta e="T135" id="Seg_3725" s="T134">kim-GA</ta>
            <ta e="T136" id="Seg_3726" s="T135">bu</ta>
            <ta e="T137" id="Seg_3727" s="T136">ka</ta>
            <ta e="T138" id="Seg_3728" s="T137">iti</ta>
            <ta e="T139" id="Seg_3729" s="T138">ikki</ta>
            <ta e="T140" id="Seg_3730" s="T139">ogo-m</ta>
            <ta e="T141" id="Seg_3731" s="T140">hɨrɨt-Ar-LAr</ta>
            <ta e="T142" id="Seg_3732" s="T141">saːdik-GA</ta>
            <ta e="T143" id="Seg_3733" s="T142">agaj</ta>
            <ta e="T144" id="Seg_3734" s="T143">eː</ta>
            <ta e="T145" id="Seg_3735" s="T144">dʼe</ta>
            <ta e="T146" id="Seg_3736" s="T145">ol</ta>
            <ta e="T147" id="Seg_3737" s="T146">hɨrɨt-An-LAr</ta>
            <ta e="T151" id="Seg_3738" s="T150">kim-nI</ta>
            <ta e="T152" id="Seg_3739" s="T151">kɨtta</ta>
            <ta e="T157" id="Seg_3740" s="T156">togo</ta>
            <ta e="T158" id="Seg_3741" s="T157">ol</ta>
            <ta e="T159" id="Seg_3742" s="T158">onton</ta>
            <ta e="T160" id="Seg_3743" s="T159">ol</ta>
            <ta e="T161" id="Seg_3744" s="T160">kojut</ta>
            <ta e="T162" id="Seg_3745" s="T161">ol</ta>
            <ta e="T163" id="Seg_3746" s="T162">kim-I-m</ta>
            <ta e="T164" id="Seg_3747" s="T163">ol</ta>
            <ta e="T165" id="Seg_3748" s="T164">edʼij-I-m</ta>
            <ta e="T166" id="Seg_3749" s="T165">öl-BIT-tA</ta>
            <ta e="T171" id="Seg_3750" s="T168">dʼe</ta>
            <ta e="T172" id="Seg_3751" s="T171">inʼe-m</ta>
            <ta e="T173" id="Seg_3752" s="T172">inʼe-m</ta>
            <ta e="T174" id="Seg_3753" s="T173">balɨs-I-tA</ta>
            <ta e="T175" id="Seg_3754" s="T174">öl-BIT-tA</ta>
            <ta e="T176" id="Seg_3755" s="T175">ɨ͡arɨj-An</ta>
            <ta e="T177" id="Seg_3756" s="T176">oččogo</ta>
            <ta e="T178" id="Seg_3757" s="T177">tü͡ört</ta>
            <ta e="T179" id="Seg_3758" s="T178">ogo-LAːK</ta>
            <ta e="T180" id="Seg_3759" s="T179">kaːl-TI-tA</ta>
            <ta e="T181" id="Seg_3760" s="T180">bu</ta>
            <ta e="T182" id="Seg_3761" s="T181">kihi</ta>
            <ta e="T183" id="Seg_3762" s="T182">tü͡ört</ta>
            <ta e="T184" id="Seg_3763" s="T183">ogo</ta>
            <ta e="T185" id="Seg_3764" s="T184">bu͡olla</ta>
            <ta e="T186" id="Seg_3765" s="T185">min-GA</ta>
            <ta e="T187" id="Seg_3766" s="T186">kel-TI-LAr</ta>
            <ta e="T188" id="Seg_3767" s="T187">bu͡olla</ta>
            <ta e="T189" id="Seg_3768" s="T188">bu͡o</ta>
            <ta e="T190" id="Seg_3769" s="T189">edʼij-BItI-GAr</ta>
            <ta e="T191" id="Seg_3770" s="T190">bu͡ol-IAK-BIt</ta>
            <ta e="T192" id="Seg_3771" s="T191">bihigi</ta>
            <ta e="T193" id="Seg_3772" s="T192">kajdi͡ek</ta>
            <ta e="T194" id="Seg_3773" s="T193">da</ta>
            <ta e="T195" id="Seg_3774" s="T194">bar-BAT-BIt</ta>
            <ta e="T196" id="Seg_3775" s="T195">di͡e-Ar-LAr</ta>
            <ta e="T197" id="Seg_3776" s="T196">inʼe-m</ta>
            <ta e="T198" id="Seg_3777" s="T197">eː</ta>
            <ta e="T199" id="Seg_3778" s="T198">inʼe</ta>
            <ta e="T200" id="Seg_3779" s="T199">inʼe-tA</ta>
            <ta e="T201" id="Seg_3780" s="T200">geri͡es</ta>
            <ta e="T202" id="Seg_3781" s="T201">et-BIT</ta>
            <ta e="T203" id="Seg_3782" s="T202">bu͡olla</ta>
            <ta e="T204" id="Seg_3783" s="T203">bu</ta>
            <ta e="T205" id="Seg_3784" s="T204">ka</ta>
            <ta e="T206" id="Seg_3785" s="T205">edʼij-GItI-GAr</ta>
            <ta e="T207" id="Seg_3786" s="T206">kaːl-An</ta>
            <ta e="T208" id="Seg_3787" s="T207">di͡e-BIT</ta>
            <ta e="T209" id="Seg_3788" s="T208">bu͡olla</ta>
            <ta e="T210" id="Seg_3789" s="T209">min</ta>
            <ta e="T211" id="Seg_3790" s="T210">kajdak</ta>
            <ta e="T212" id="Seg_3791" s="T211">bu͡ol-TAK-BInA</ta>
            <ta e="T213" id="Seg_3792" s="T212">dʼe</ta>
            <ta e="T214" id="Seg_3793" s="T213">ol</ta>
            <ta e="T215" id="Seg_3794" s="T214">ol</ta>
            <ta e="T216" id="Seg_3795" s="T215">tü͡ört</ta>
            <ta e="T217" id="Seg_3796" s="T216">tü͡ört</ta>
            <ta e="T218" id="Seg_3797" s="T217">ogo</ta>
            <ta e="T219" id="Seg_3798" s="T218">bihigi-GA</ta>
            <ta e="T220" id="Seg_3799" s="T219">kel-TI-LAr</ta>
            <ta e="T221" id="Seg_3800" s="T220">agaj</ta>
            <ta e="T222" id="Seg_3801" s="T221">mm</ta>
            <ta e="T223" id="Seg_3802" s="T222">Nosku͡o</ta>
            <ta e="T224" id="Seg_3803" s="T223">ü͡ören-A</ta>
            <ta e="T225" id="Seg_3804" s="T224">bar-Ar-LAr</ta>
            <ta e="T231" id="Seg_3805" s="T230">dʼe</ta>
            <ta e="T232" id="Seg_3806" s="T231">beː</ta>
            <ta e="T233" id="Seg_3807" s="T232">onton</ta>
            <ta e="T237" id="Seg_3808" s="T236">dʼe</ta>
            <ta e="T238" id="Seg_3809" s="T237">onton</ta>
            <ta e="T239" id="Seg_3810" s="T238">kanikul-GA</ta>
            <ta e="T240" id="Seg_3811" s="T239">kel-Ar-LAr</ta>
            <ta e="T241" id="Seg_3812" s="T240">bu</ta>
            <ta e="T242" id="Seg_3813" s="T241">ogo-LAr</ta>
            <ta e="T243" id="Seg_3814" s="T242">hübeleː-Ar-LAr</ta>
            <ta e="T244" id="Seg_3815" s="T243">tojon-LAr</ta>
            <ta e="T245" id="Seg_3816" s="T244">agaj</ta>
            <ta e="T246" id="Seg_3817" s="T245">bu</ta>
            <ta e="T247" id="Seg_3818" s="T246">ogo-nI</ta>
            <ta e="T248" id="Seg_3819" s="T247">bu͡olla</ta>
            <ta e="T249" id="Seg_3820" s="T248">Popigaj-GA</ta>
            <ta e="T250" id="Seg_3821" s="T249">ɨːt-IAK-GA</ta>
            <ta e="T251" id="Seg_3822" s="T250">di͡e-Ar-LAr</ta>
            <ta e="T253" id="Seg_3823" s="T251">uruː-tI-GAr</ta>
            <ta e="T257" id="Seg_3824" s="T255">mm</ta>
            <ta e="T258" id="Seg_3825" s="T257">Kaːtʼa-nI</ta>
            <ta e="T259" id="Seg_3826" s="T258">biːr</ta>
            <ta e="T260" id="Seg_3827" s="T259">biːr</ta>
            <ta e="T261" id="Seg_3828" s="T260">ogo-nI</ta>
            <ta e="T262" id="Seg_3829" s="T261">bu͡olla</ta>
            <ta e="T263" id="Seg_3830" s="T262">manna</ta>
            <ta e="T264" id="Seg_3831" s="T263">tɨ͡a-GA</ta>
            <ta e="T265" id="Seg_3832" s="T264">baːr</ta>
            <ta e="T266" id="Seg_3833" s="T265">tundra</ta>
            <ta e="T267" id="Seg_3834" s="T266">ogonnʼor-LAːK</ta>
            <ta e="T268" id="Seg_3835" s="T267">emeːksin</ta>
            <ta e="T269" id="Seg_3836" s="T268">uruː-BIt</ta>
            <ta e="T270" id="Seg_3837" s="T269">agaj</ta>
            <ta e="T271" id="Seg_3838" s="T270">dʼe</ta>
            <ta e="T272" id="Seg_3839" s="T271">ol-LAr-GA</ta>
            <ta e="T273" id="Seg_3840" s="T272">bi͡er-TI-LAr</ta>
            <ta e="T274" id="Seg_3841" s="T273">oččogo</ta>
            <ta e="T275" id="Seg_3842" s="T274">min</ta>
            <ta e="T276" id="Seg_3843" s="T275">ikki</ta>
            <ta e="T277" id="Seg_3844" s="T276">u͡ol</ta>
            <ta e="T278" id="Seg_3845" s="T277">ogo-nI</ta>
            <ta e="T279" id="Seg_3846" s="T278">kɨtta</ta>
            <ta e="T280" id="Seg_3847" s="T279">kaːl-TI-m</ta>
            <ta e="T281" id="Seg_3848" s="T280">bu͡o</ta>
            <ta e="T282" id="Seg_3849" s="T281">agaj</ta>
            <ta e="T283" id="Seg_3850" s="T282">iti-LAr</ta>
            <ta e="T284" id="Seg_3851" s="T283">min-GA</ta>
            <ta e="T285" id="Seg_3852" s="T284">bar</ta>
            <ta e="T286" id="Seg_3853" s="T285">bar-BAT-LAr</ta>
            <ta e="T287" id="Seg_3854" s="T286">töttörü</ta>
            <ta e="T288" id="Seg_3855" s="T287">iti</ta>
            <ta e="T289" id="Seg_3856" s="T288">ikki</ta>
            <ta e="T290" id="Seg_3857" s="T289">u͡ol</ta>
            <ta e="T291" id="Seg_3858" s="T290">ogo</ta>
            <ta e="T292" id="Seg_3859" s="T291">dʼe</ta>
            <ta e="T293" id="Seg_3860" s="T292">onton</ta>
            <ta e="T294" id="Seg_3861" s="T293">töhö</ta>
            <ta e="T295" id="Seg_3862" s="T294">da</ta>
            <ta e="T296" id="Seg_3863" s="T295">bu͡ol-BAkkA</ta>
            <ta e="T297" id="Seg_3864" s="T296">kanikul-GA</ta>
            <ta e="T298" id="Seg_3865" s="T297">kel-BIT-LArA</ta>
            <ta e="T299" id="Seg_3866" s="T298">ol</ta>
            <ta e="T300" id="Seg_3867" s="T299">ogo-LAr-I-ŋ</ta>
            <ta e="T301" id="Seg_3868" s="T300">bu͡o</ta>
            <ta e="T302" id="Seg_3869" s="T301">töttörü</ta>
            <ta e="T303" id="Seg_3870" s="T302">kel-TI-LAr</ta>
            <ta e="T304" id="Seg_3871" s="T303">bihigi-GA</ta>
            <ta e="T305" id="Seg_3872" s="T304">ol</ta>
            <ta e="T306" id="Seg_3873" s="T305">höbüleː-BAT-LAr</ta>
            <ta e="T307" id="Seg_3874" s="T306">e-BIT</ta>
            <ta e="T308" id="Seg_3875" s="T307">ol-LAr-nI</ta>
            <ta e="T309" id="Seg_3876" s="T308">onton</ta>
            <ta e="T310" id="Seg_3877" s="T309">tundra-GA</ta>
            <ta e="T311" id="Seg_3878" s="T310">ogonnʼor-LAːK</ta>
            <ta e="T312" id="Seg_3879" s="T311">emeːksin</ta>
            <ta e="T313" id="Seg_3880" s="T312">emi͡e</ta>
            <ta e="T314" id="Seg_3881" s="T313">ol</ta>
            <ta e="T315" id="Seg_3882" s="T314">bar-BIT</ta>
            <ta e="T316" id="Seg_3883" s="T315">ikki</ta>
            <ta e="T317" id="Seg_3884" s="T316">ogo-BIt</ta>
            <ta e="T318" id="Seg_3885" s="T317">emi͡e</ta>
            <ta e="T319" id="Seg_3886" s="T318">emi͡e</ta>
            <ta e="T320" id="Seg_3887" s="T319">töttörü</ta>
            <ta e="T321" id="Seg_3888" s="T320">kel-TI-tA</ta>
            <ta e="T322" id="Seg_3889" s="T321">bu͡o</ta>
            <ta e="T323" id="Seg_3890" s="T322">oččogo</ta>
            <ta e="T324" id="Seg_3891" s="T323">agɨs</ta>
            <ta e="T325" id="Seg_3892" s="T324">ogo-LAːK</ta>
            <ta e="T326" id="Seg_3893" s="T325">bu͡ol-TI-m</ta>
            <ta e="T327" id="Seg_3894" s="T326">bu͡o</ta>
            <ta e="T328" id="Seg_3895" s="T327">min</ta>
            <ta e="T330" id="Seg_3896" s="T329">beje-m</ta>
            <ta e="T331" id="Seg_3897" s="T330">tü͡ört</ta>
            <ta e="T332" id="Seg_3898" s="T331">ogo-LAːK-BIn</ta>
            <ta e="T333" id="Seg_3899" s="T332">bu͡olla</ta>
            <ta e="T334" id="Seg_3900" s="T333">iti</ta>
            <ta e="T335" id="Seg_3901" s="T334">ikki</ta>
            <ta e="T336" id="Seg_3902" s="T335">kɨːs-LAːK-BIn</ta>
            <ta e="T337" id="Seg_3903" s="T336">bu͡olla</ta>
            <ta e="T338" id="Seg_3904" s="T337">inʼe-m</ta>
            <ta e="T339" id="Seg_3905" s="T338">kɨrdʼagas</ta>
            <ta e="T340" id="Seg_3906" s="T339">ubaj-I-m</ta>
            <ta e="T341" id="Seg_3907" s="T340">kɨrdʼagas</ta>
            <ta e="T342" id="Seg_3908" s="T341">onton</ta>
            <ta e="T343" id="Seg_3909" s="T342">ol</ta>
            <ta e="T344" id="Seg_3910" s="T343">agɨs</ta>
            <ta e="T345" id="Seg_3911" s="T344">ogo-LAːK-BIn</ta>
            <ta e="T346" id="Seg_3912" s="T345">beje-m</ta>
            <ta e="T347" id="Seg_3913" s="T346">edʼij-I-m</ta>
            <ta e="T348" id="Seg_3914" s="T347">gi͡en-tA</ta>
            <ta e="T349" id="Seg_3915" s="T348">tü͡ört</ta>
            <ta e="T350" id="Seg_3916" s="T349">ogo</ta>
            <ta e="T351" id="Seg_3917" s="T350">kaːl-TI-tA</ta>
            <ta e="T352" id="Seg_3918" s="T351">mini͡ene</ta>
            <ta e="T353" id="Seg_3919" s="T352">tü͡ört</ta>
            <ta e="T354" id="Seg_3920" s="T353">ogo</ta>
            <ta e="T355" id="Seg_3921" s="T354">bu͡olla</ta>
            <ta e="T360" id="Seg_3922" s="T359">küččügüj</ta>
            <ta e="T361" id="Seg_3923" s="T360">e-TI-tA</ta>
            <ta e="T362" id="Seg_3924" s="T361">ol</ta>
            <ta e="T363" id="Seg_3925" s="T362">dʼe</ta>
            <ta e="T364" id="Seg_3926" s="T363">ol</ta>
            <ta e="T365" id="Seg_3927" s="T364">kaːl-An-BIn</ta>
            <ta e="T366" id="Seg_3928" s="T365">dʼe</ta>
            <ta e="T367" id="Seg_3929" s="T366">iti</ta>
            <ta e="T368" id="Seg_3930" s="T367">olor-TI-BIt</ta>
            <ta e="T369" id="Seg_3931" s="T368">bu͡olla</ta>
            <ta e="T370" id="Seg_3932" s="T369">inʼe-m</ta>
            <ta e="T371" id="Seg_3933" s="T370">kɨrɨj-An</ta>
            <ta e="T372" id="Seg_3934" s="T371">öl-TI-tA</ta>
            <ta e="T373" id="Seg_3935" s="T372">ɨ͡arɨj-An</ta>
            <ta e="T374" id="Seg_3936" s="T373">ol</ta>
            <ta e="T375" id="Seg_3937" s="T374">ubaj-I-m</ta>
            <ta e="T376" id="Seg_3938" s="T375">emi͡e</ta>
            <ta e="T377" id="Seg_3939" s="T376">dʼaktar-tA</ta>
            <ta e="T378" id="Seg_3940" s="T377">da</ta>
            <ta e="T379" id="Seg_3941" s="T378">hu͡ok</ta>
            <ta e="T380" id="Seg_3942" s="T379">tu͡ok</ta>
            <ta e="T381" id="Seg_3943" s="T380">da</ta>
            <ta e="T382" id="Seg_3944" s="T381">hu͡ok</ta>
            <ta e="T383" id="Seg_3945" s="T382">e-TI-tA</ta>
            <ta e="T384" id="Seg_3946" s="T383">laːpkɨ-GA</ta>
            <ta e="T385" id="Seg_3947" s="T384">üleleː-AːččI</ta>
            <ta e="T386" id="Seg_3948" s="T385">e-TI-tA</ta>
            <ta e="T387" id="Seg_3949" s="T386">ol</ta>
            <ta e="T388" id="Seg_3950" s="T387">ihin</ta>
            <ta e="T389" id="Seg_3951" s="T388">as-I-nAn</ta>
            <ta e="T390" id="Seg_3952" s="T389">da</ta>
            <ta e="T391" id="Seg_3953" s="T390">olus</ta>
            <ta e="T392" id="Seg_3954" s="T391">da</ta>
            <ta e="T393" id="Seg_3955" s="T392">kim-LAː-AːččI-tA</ta>
            <ta e="T394" id="Seg_3956" s="T393">hu͡ok</ta>
            <ta e="T395" id="Seg_3957" s="T394">e-TI-BIt</ta>
            <ta e="T396" id="Seg_3958" s="T395">üleleː-Ar</ta>
            <ta e="T397" id="Seg_3959" s="T396">bu͡ol-An</ta>
            <ta e="T399" id="Seg_3960" s="T398">egel-An</ta>
            <ta e="T400" id="Seg_3961" s="T399">is-AːččI</ta>
            <ta e="T401" id="Seg_3962" s="T400">as</ta>
            <ta e="T402" id="Seg_3963" s="T401">laːpkɨ-ttAn</ta>
            <ta e="T403" id="Seg_3964" s="T402">harpalaːt-kAːN</ta>
            <ta e="T404" id="Seg_3965" s="T403">iti</ta>
            <ta e="T405" id="Seg_3966" s="T404">ɨl-AːččI</ta>
            <ta e="T406" id="Seg_3967" s="T405">onton</ta>
            <ta e="T407" id="Seg_3968" s="T406">min</ta>
            <ta e="T408" id="Seg_3969" s="T407">poːvar-LAː-A-BIn</ta>
            <ta e="T409" id="Seg_3970" s="T408">bu͡olla</ta>
            <ta e="T410" id="Seg_3971" s="T409">ol</ta>
            <ta e="T411" id="Seg_3972" s="T410">iti</ta>
            <ta e="T412" id="Seg_3973" s="T411">olor-BIT-BIt</ta>
            <ta e="T413" id="Seg_3974" s="T412">barɨ-kAːN-LArA</ta>
            <ta e="T414" id="Seg_3975" s="T413">ulaːt-BIT-LArA</ta>
            <ta e="T415" id="Seg_3976" s="T414">eni</ta>
            <ta e="T416" id="Seg_3977" s="T415">dʼaktar-LAːK-LAr</ta>
            <ta e="T422" id="Seg_3978" s="T421">dʼe</ta>
            <ta e="T423" id="Seg_3979" s="T422">tagɨs-I-BIT-tA</ta>
            <ta e="T424" id="Seg_3980" s="T423">tu͡ok</ta>
            <ta e="T425" id="Seg_3981" s="T424">gɨn-AːrI</ta>
            <ta e="T426" id="Seg_3982" s="T425">da</ta>
            <ta e="T427" id="Seg_3983" s="T426">tagɨs-I-BIT</ta>
            <ta e="T428" id="Seg_3984" s="T427">dʼe</ta>
            <ta e="T429" id="Seg_3985" s="T428">ol</ta>
            <ta e="T430" id="Seg_3986" s="T429">ihin</ta>
            <ta e="T431" id="Seg_3987" s="T430">agɨs</ta>
            <ta e="T432" id="Seg_3988" s="T431">ogo</ta>
            <ta e="T433" id="Seg_3989" s="T432">bu͡ol-Ar</ta>
            <ta e="T434" id="Seg_3990" s="T433">bu͡olla</ta>
            <ta e="T435" id="Seg_3991" s="T434">maladoj</ta>
            <ta e="T436" id="Seg_3992" s="T435">dʼaktar-BIn</ta>
            <ta e="T437" id="Seg_3993" s="T436">bu͡ol</ta>
            <ta e="T438" id="Seg_3994" s="T437">onno</ta>
            <ta e="T439" id="Seg_3995" s="T438">ol</ta>
            <ta e="T443" id="Seg_3996" s="T442">ka</ta>
            <ta e="T444" id="Seg_3997" s="T443">er-GA</ta>
            <ta e="T445" id="Seg_3998" s="T444">tagɨs-An</ta>
            <ta e="T446" id="Seg_3999" s="T445">kaːl-TI-m</ta>
            <ta e="T447" id="Seg_4000" s="T446">bu͡o</ta>
            <ta e="T448" id="Seg_4001" s="T447">ikki</ta>
            <ta e="T449" id="Seg_4002" s="T448">ogo-nI</ta>
            <ta e="T450" id="Seg_4003" s="T449">töröː-BIT</ta>
            <ta e="T451" id="Seg_4004" s="T450">bu͡olla</ta>
            <ta e="T452" id="Seg_4005" s="T451">ol</ta>
            <ta e="T453" id="Seg_4006" s="T452">üs-Is</ta>
            <ta e="T454" id="Seg_4007" s="T453">üs-Is</ta>
            <ta e="T455" id="Seg_4008" s="T454">ogo-m</ta>
            <ta e="T456" id="Seg_4009" s="T455">u͡ol</ta>
            <ta e="T457" id="Seg_4010" s="T456">ogo</ta>
            <ta e="T458" id="Seg_4011" s="T457">e-TI-tA</ta>
            <ta e="T459" id="Seg_4012" s="T458">ol-tI-m</ta>
            <ta e="T460" id="Seg_4013" s="T459">tüs-BIT-tA</ta>
            <ta e="T461" id="Seg_4014" s="T460">ölör-I-n-An-BIn</ta>
            <ta e="T463" id="Seg_4015" s="T462">saːdik-GA</ta>
            <ta e="T464" id="Seg_4016" s="T463">buːs-LAr-nI</ta>
            <ta e="T465" id="Seg_4017" s="T464">ugol-LAr-nI</ta>
            <ta e="T466" id="Seg_4018" s="T465">taskaj-LAː-An</ta>
            <ta e="T467" id="Seg_4019" s="T466">ol</ta>
            <ta e="T468" id="Seg_4020" s="T467">tüs-BIT</ta>
            <ta e="T469" id="Seg_4021" s="T468">e-TI-tA</ta>
            <ta e="T470" id="Seg_4022" s="T469">iti</ta>
            <ta e="T471" id="Seg_4023" s="T470">ikki</ta>
            <ta e="T472" id="Seg_4024" s="T471">u͡ol</ta>
            <ta e="T473" id="Seg_4025" s="T472">ɨkkarda-tA</ta>
            <ta e="T481" id="Seg_4026" s="T480">Ölöːnö</ta>
            <ta e="T482" id="Seg_4027" s="T481">alta</ta>
            <ta e="T483" id="Seg_4028" s="T482">dʼɨl-LAːK-nI</ta>
            <ta e="T484" id="Seg_4029" s="T483">ɨl-BIT-BIT</ta>
            <ta e="T485" id="Seg_4030" s="T484">onton</ta>
            <ta e="T486" id="Seg_4031" s="T485">hette</ta>
            <ta e="T487" id="Seg_4032" s="T486">dʼɨl-LAN-Ar-tI-GAr</ta>
            <ta e="T488" id="Seg_4033" s="T487">manna</ta>
            <ta e="T489" id="Seg_4034" s="T488">kel-TI-m</ta>
            <ta e="T490" id="Seg_4035" s="T489">bu͡olla</ta>
            <ta e="T491" id="Seg_4036" s="T490">Dudʼinka-GA</ta>
            <ta e="T492" id="Seg_4037" s="T491">ogo-m</ta>
            <ta e="T493" id="Seg_4038" s="T492">dʼi͡e</ta>
            <ta e="T494" id="Seg_4039" s="T493">ɨl-Ar</ta>
            <ta e="T495" id="Seg_4040" s="T494">e-BIT</ta>
            <ta e="T496" id="Seg_4041" s="T495">agaj</ta>
            <ta e="T497" id="Seg_4042" s="T496">dʼe</ta>
            <ta e="T498" id="Seg_4043" s="T497">ol-GA</ta>
            <ta e="T499" id="Seg_4044" s="T498">prapʼiska-LAː-IAK.[tI]-n</ta>
            <ta e="T500" id="Seg_4045" s="T499">naːda</ta>
            <ta e="T501" id="Seg_4046" s="T500">e-BIT</ta>
            <ta e="T502" id="Seg_4047" s="T501">dʼe</ta>
            <ta e="T503" id="Seg_4048" s="T502">ol</ta>
            <ta e="T504" id="Seg_4049" s="T503">kel-BIT-I-m</ta>
            <ta e="T505" id="Seg_4050" s="T504">ol</ta>
            <ta e="T506" id="Seg_4051" s="T505">kel-An-BIn</ta>
            <ta e="T507" id="Seg_4052" s="T506">bu͡o</ta>
            <ta e="T508" id="Seg_4053" s="T507">ogo-m</ta>
            <ta e="T509" id="Seg_4054" s="T508">u͡on-Is</ta>
            <ta e="T510" id="Seg_4055" s="T509">kɨlaːs</ta>
            <ta e="T511" id="Seg_4056" s="T510">e-TI-tA</ta>
            <ta e="T513" id="Seg_4057" s="T512">vot</ta>
            <ta e="T514" id="Seg_4058" s="T513">maːma-tA</ta>
            <ta e="T515" id="Seg_4059" s="T514">bu͡ollagɨna</ta>
            <ta e="T516" id="Seg_4060" s="T515">Džakuskaj-GA</ta>
            <ta e="T517" id="Seg_4061" s="T516">er-LAN-BIT-tA</ta>
            <ta e="T518" id="Seg_4062" s="T517">ol-tI-ŋ</ta>
            <ta e="T519" id="Seg_4063" s="T518">anɨ</ta>
            <ta e="T520" id="Seg_4064" s="T519">ikki</ta>
            <ta e="T521" id="Seg_4065" s="T520">ogo-LAːK</ta>
            <ta e="T522" id="Seg_4066" s="T521">u͡ol-nI</ta>
            <ta e="T523" id="Seg_4067" s="T522">kɨtta</ta>
            <ta e="T524" id="Seg_4068" s="T523">kɨːs</ta>
            <ta e="T529" id="Seg_4069" s="T528">tɨ͡a-GA</ta>
            <ta e="T530" id="Seg_4070" s="T529">tundra-GA</ta>
            <ta e="T531" id="Seg_4071" s="T530">hɨrɨt-Ar-LAr</ta>
            <ta e="T532" id="Seg_4072" s="T531">tɨ͡a-GA</ta>
            <ta e="T533" id="Seg_4073" s="T532">taba-ČIt</ta>
            <ta e="T534" id="Seg_4074" s="T533">er-tA</ta>
            <ta e="T536" id="Seg_4075" s="T535">ele-tA</ta>
            <ta e="T537" id="Seg_4076" s="T536">Hɨndaːska-GA</ta>
            <ta e="T538" id="Seg_4077" s="T537">kaːl-BIT-LArA</ta>
            <ta e="T539" id="Seg_4078" s="T538">anɨ</ta>
            <ta e="T540" id="Seg_4079" s="T539">bar-IAK-LArA</ta>
            <ta e="T541" id="Seg_4080" s="T540">maːj</ta>
            <ta e="T542" id="Seg_4081" s="T541">ɨj-GA</ta>
            <ta e="T543" id="Seg_4082" s="T542">usku͡ola</ta>
            <ta e="T544" id="Seg_4083" s="T543">büt-TAr-An</ta>
            <ta e="T545" id="Seg_4084" s="T544">ogo-LArA</ta>
            <ta e="T546" id="Seg_4085" s="T545">itinne</ta>
            <ta e="T547" id="Seg_4086" s="T546">ü͡ören-Ar-LAr</ta>
            <ta e="T548" id="Seg_4087" s="T547">Hɨndaːska-GA</ta>
            <ta e="T549" id="Seg_4088" s="T548">biːrges-tA</ta>
            <ta e="T550" id="Seg_4089" s="T549">tü͡ört-Is</ta>
            <ta e="T551" id="Seg_4090" s="T550">kɨlaːs</ta>
            <ta e="T552" id="Seg_4091" s="T551">biːrges-tA</ta>
            <ta e="T553" id="Seg_4092" s="T552">üs-Is</ta>
            <ta e="T554" id="Seg_4093" s="T553">kɨlaːs</ta>
            <ta e="T555" id="Seg_4094" s="T554">du͡o</ta>
            <ta e="T556" id="Seg_4095" s="T555">ikki-Is</ta>
            <ta e="T557" id="Seg_4096" s="T556">du͡o</ta>
            <ta e="T559" id="Seg_4097" s="T558">ikki-Is</ta>
            <ta e="T560" id="Seg_4098" s="T559">kɨlaːs</ta>
            <ta e="T561" id="Seg_4099" s="T560">eː</ta>
            <ta e="T562" id="Seg_4100" s="T561">kɨːs-tA</ta>
            <ta e="T563" id="Seg_4101" s="T562">anɨ</ta>
            <ta e="T564" id="Seg_4102" s="T563">min</ta>
            <ta e="T565" id="Seg_4103" s="T564">dʼi͡e-BA-r</ta>
            <ta e="T566" id="Seg_4104" s="T565">kaːl-BIT-LArA</ta>
            <ta e="T567" id="Seg_4105" s="T566">anɨ</ta>
            <ta e="T568" id="Seg_4106" s="T567">maːj-GA</ta>
            <ta e="T569" id="Seg_4107" s="T568">bar-IAK-LArA</ta>
            <ta e="T570" id="Seg_4108" s="T569">anɨ</ta>
            <ta e="T572" id="Seg_4109" s="T571">kös-An</ta>
            <ta e="T573" id="Seg_4110" s="T572">bar-IAK-LArA</ta>
            <ta e="T574" id="Seg_4111" s="T573">ele-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge-KiES">
            <ta e="T2" id="Seg_4112" s="T1">well</ta>
            <ta e="T3" id="Seg_4113" s="T2">Korgo-DAT/LOC</ta>
            <ta e="T4" id="Seg_4114" s="T3">there.is</ta>
            <ta e="T5" id="Seg_4115" s="T4">be-PST1-1PL</ta>
            <ta e="T6" id="Seg_4116" s="T5">earth-PROPR.[NOM]</ta>
            <ta e="T7" id="Seg_4117" s="T6">be-PST1-1PL</ta>
            <ta e="T8" id="Seg_4118" s="T7">3PL-DAT/LOC</ta>
            <ta e="T9" id="Seg_4119" s="T8">Korgo-DAT/LOC</ta>
            <ta e="T11" id="Seg_4120" s="T9">earth-VBZ-PST-EP-1SG</ta>
            <ta e="T12" id="Seg_4121" s="T11">then</ta>
            <ta e="T13" id="Seg_4122" s="T12">though</ta>
            <ta e="T14" id="Seg_4123" s="T13">this</ta>
            <ta e="T15" id="Seg_4124" s="T14">eh</ta>
            <ta e="T16" id="Seg_4125" s="T15">this</ta>
            <ta e="T17" id="Seg_4126" s="T16">child-ACC</ta>
            <ta e="T18" id="Seg_4127" s="T17">give.birth-PST2-EP-1SG</ta>
            <ta e="T19" id="Seg_4128" s="T18">then</ta>
            <ta e="T20" id="Seg_4129" s="T19">that</ta>
            <ta e="T21" id="Seg_4130" s="T20">boy.[NOM]</ta>
            <ta e="T22" id="Seg_4131" s="T21">child-1SG.[NOM]</ta>
            <ta e="T23" id="Seg_4132" s="T22">give.birth-CVB.SEQ-1SG</ta>
            <ta e="T24" id="Seg_4133" s="T23">that</ta>
            <ta e="T25" id="Seg_4134" s="T24">then</ta>
            <ta e="T26" id="Seg_4135" s="T25">though</ta>
            <ta e="T27" id="Seg_4136" s="T26">old.man-1PL.[NOM]</ta>
            <ta e="T28" id="Seg_4137" s="T27">be.sick-PTCP.PRS</ta>
            <ta e="T29" id="Seg_4138" s="T28">be-PST1-3SG</ta>
            <ta e="T30" id="Seg_4139" s="T29">die-CVB.SEQ</ta>
            <ta e="T31" id="Seg_4140" s="T30">die-CVB.SEQ</ta>
            <ta e="T32" id="Seg_4141" s="T31">that</ta>
            <ta e="T33" id="Seg_4142" s="T32">who-DAT/LOC</ta>
            <ta e="T34" id="Seg_4143" s="T33">Syndassko-DAT/LOC</ta>
            <ta e="T35" id="Seg_4144" s="T34">lie.down-PST2-1PL</ta>
            <ta e="T36" id="Seg_4145" s="T35">that</ta>
            <ta e="T37" id="Seg_4146" s="T36">then</ta>
            <ta e="T38" id="Seg_4147" s="T37">work-1SG.[NOM]</ta>
            <ta e="T39" id="Seg_4148" s="T38">1SG-ACC</ta>
            <ta e="T43" id="Seg_4149" s="T42">this</ta>
            <ta e="T44" id="Seg_4150" s="T43">well</ta>
            <ta e="T48" id="Seg_4151" s="T47">then</ta>
            <ta e="T49" id="Seg_4152" s="T48">work.[NOM]</ta>
            <ta e="T50" id="Seg_4153" s="T49">give-PST2-3PL</ta>
            <ta e="T51" id="Seg_4154" s="T50">1SG-DAT/LOC</ta>
            <ta e="T52" id="Seg_4155" s="T51">kindergarten-DAT/LOC</ta>
            <ta e="T64" id="Seg_4156" s="T63">AFFIRM</ta>
            <ta e="T65" id="Seg_4157" s="T64">Dolgan-SIM</ta>
            <ta e="T66" id="Seg_4158" s="T65">MOD</ta>
            <ta e="T67" id="Seg_4159" s="T66">Syndassko-DAT/LOC</ta>
            <ta e="T68" id="Seg_4160" s="T67">Dolgan</ta>
            <ta e="T69" id="Seg_4161" s="T68">for.the.first.time</ta>
            <ta e="T70" id="Seg_4162" s="T69">kindergarten.[NOM]</ta>
            <ta e="T71" id="Seg_4163" s="T70">stand-PTCP.PST</ta>
            <ta e="T72" id="Seg_4164" s="T71">be-PST1-3SG</ta>
            <ta e="T73" id="Seg_4165" s="T72">well</ta>
            <ta e="T74" id="Seg_4166" s="T73">what-ACC</ta>
            <ta e="T75" id="Seg_4167" s="T74">NEG</ta>
            <ta e="T76" id="Seg_4168" s="T75">know-NEG-1SG</ta>
            <ta e="T77" id="Seg_4169" s="T76">kindergarten.[NOM]</ta>
            <ta e="T78" id="Seg_4170" s="T77">what.[NOM]</ta>
            <ta e="T79" id="Seg_4171" s="T78">name-3SG.[NOM]=Q</ta>
            <ta e="T92" id="Seg_4172" s="T91">oh</ta>
            <ta e="T94" id="Seg_4173" s="T93">well</ta>
            <ta e="T95" id="Seg_4174" s="T94">how</ta>
            <ta e="T96" id="Seg_4175" s="T95">be-FUT-1PL=Q</ta>
            <ta e="T98" id="Seg_4176" s="T97">can-CVB.SEQ</ta>
            <ta e="T99" id="Seg_4177" s="T98">speak-NEG-1SG</ta>
            <ta e="T100" id="Seg_4178" s="T99">child-PL-DAT/LOC</ta>
            <ta e="T101" id="Seg_4179" s="T100">well</ta>
            <ta e="T102" id="Seg_4180" s="T101">child-PL-DAT/LOC</ta>
            <ta e="T103" id="Seg_4181" s="T102">can-CVB.SEQ</ta>
            <ta e="T104" id="Seg_4182" s="T103">boil-EP-CAUS-NEG-1SG</ta>
            <ta e="T105" id="Seg_4183" s="T104">say-PRS-1SG</ta>
            <ta e="T106" id="Seg_4184" s="T105">NEG.EX</ta>
            <ta e="T107" id="Seg_4185" s="T106">cook-PL.[NOM]</ta>
            <ta e="T108" id="Seg_4186" s="T107">say-PRS-3PL</ta>
            <ta e="T109" id="Seg_4187" s="T108">EMPH</ta>
            <ta e="T110" id="Seg_4188" s="T109">this</ta>
            <ta e="T111" id="Seg_4189" s="T110">kindergarten.[NOM]</ta>
            <ta e="T112" id="Seg_4190" s="T111">stand-PST1-3SG</ta>
            <ta e="T113" id="Seg_4191" s="T112">new</ta>
            <ta e="T114" id="Seg_4192" s="T113">kindergarten-DAT/LOC</ta>
            <ta e="T115" id="Seg_4193" s="T114">that-DAT/LOC</ta>
            <ta e="T116" id="Seg_4194" s="T115">cook.[NOM]</ta>
            <ta e="T117" id="Seg_4195" s="T116">cook.[NOM]</ta>
            <ta e="T118" id="Seg_4196" s="T117">need.to</ta>
            <ta e="T119" id="Seg_4197" s="T118">say-PRS-3PL</ta>
            <ta e="T120" id="Seg_4198" s="T119">head-PL.[NOM]</ta>
            <ta e="T121" id="Seg_4199" s="T120">so</ta>
            <ta e="T122" id="Seg_4200" s="T121">head-PL.[NOM]</ta>
            <ta e="T123" id="Seg_4201" s="T122">1SG-ACC</ta>
            <ta e="T124" id="Seg_4202" s="T123">cook.[NOM]</ta>
            <ta e="T125" id="Seg_4203" s="T124">lay-PST1-3PL</ta>
            <ta e="T126" id="Seg_4204" s="T125">MOD</ta>
            <ta e="T127" id="Seg_4205" s="T126">well</ta>
            <ta e="T128" id="Seg_4206" s="T127">that</ta>
            <ta e="T129" id="Seg_4207" s="T128">lie-PST2-1PL</ta>
            <ta e="T130" id="Seg_4208" s="T129">then</ta>
            <ta e="T131" id="Seg_4209" s="T130">that</ta>
            <ta e="T132" id="Seg_4210" s="T131">cook-VBZ-CVB.SEQ-1SG</ta>
            <ta e="T133" id="Seg_4211" s="T132">though</ta>
            <ta e="T134" id="Seg_4212" s="T133">eh</ta>
            <ta e="T135" id="Seg_4213" s="T134">who-DAT/LOC</ta>
            <ta e="T136" id="Seg_4214" s="T135">this</ta>
            <ta e="T137" id="Seg_4215" s="T136">well</ta>
            <ta e="T138" id="Seg_4216" s="T137">that.[NOM]</ta>
            <ta e="T139" id="Seg_4217" s="T138">two</ta>
            <ta e="T140" id="Seg_4218" s="T139">child-1SG.[NOM]</ta>
            <ta e="T141" id="Seg_4219" s="T140">go-PRS-3PL</ta>
            <ta e="T142" id="Seg_4220" s="T141">kindergarten-DAT/LOC</ta>
            <ta e="T143" id="Seg_4221" s="T142">only</ta>
            <ta e="T144" id="Seg_4222" s="T143">AFFIRM</ta>
            <ta e="T145" id="Seg_4223" s="T144">well</ta>
            <ta e="T146" id="Seg_4224" s="T145">that</ta>
            <ta e="T147" id="Seg_4225" s="T146">go-CVB.SEQ-3PL</ta>
            <ta e="T151" id="Seg_4226" s="T150">who-ACC</ta>
            <ta e="T152" id="Seg_4227" s="T151">with</ta>
            <ta e="T157" id="Seg_4228" s="T156">why</ta>
            <ta e="T158" id="Seg_4229" s="T157">that</ta>
            <ta e="T159" id="Seg_4230" s="T158">then</ta>
            <ta e="T160" id="Seg_4231" s="T159">that</ta>
            <ta e="T161" id="Seg_4232" s="T160">later</ta>
            <ta e="T162" id="Seg_4233" s="T161">that</ta>
            <ta e="T163" id="Seg_4234" s="T162">who-EP-1SG.[NOM]</ta>
            <ta e="T164" id="Seg_4235" s="T163">that</ta>
            <ta e="T165" id="Seg_4236" s="T164">older.sister-EP-1SG.[NOM]</ta>
            <ta e="T166" id="Seg_4237" s="T165">die-PST2-3SG</ta>
            <ta e="T171" id="Seg_4238" s="T168">well</ta>
            <ta e="T172" id="Seg_4239" s="T171">mother-1SG.[NOM]</ta>
            <ta e="T173" id="Seg_4240" s="T172">mother-1SG.[NOM]</ta>
            <ta e="T174" id="Seg_4241" s="T173">younger.sister-EP-3SG.[NOM]</ta>
            <ta e="T175" id="Seg_4242" s="T174">die-PST2-3SG</ta>
            <ta e="T176" id="Seg_4243" s="T175">be.sick-CVB.SEQ</ta>
            <ta e="T177" id="Seg_4244" s="T176">then</ta>
            <ta e="T178" id="Seg_4245" s="T177">four</ta>
            <ta e="T179" id="Seg_4246" s="T178">child-PROPR.[NOM]</ta>
            <ta e="T180" id="Seg_4247" s="T179">stay-PST1-3SG</ta>
            <ta e="T181" id="Seg_4248" s="T180">this</ta>
            <ta e="T182" id="Seg_4249" s="T181">human.being.[NOM]</ta>
            <ta e="T183" id="Seg_4250" s="T182">four</ta>
            <ta e="T184" id="Seg_4251" s="T183">child.[NOM]</ta>
            <ta e="T185" id="Seg_4252" s="T184">MOD</ta>
            <ta e="T186" id="Seg_4253" s="T185">1SG-DAT/LOC</ta>
            <ta e="T187" id="Seg_4254" s="T186">come-PST1-3PL</ta>
            <ta e="T188" id="Seg_4255" s="T187">MOD</ta>
            <ta e="T189" id="Seg_4256" s="T188">EMPH</ta>
            <ta e="T190" id="Seg_4257" s="T189">older.sister-1PL-DAT/LOC</ta>
            <ta e="T191" id="Seg_4258" s="T190">be-FUT-1PL</ta>
            <ta e="T192" id="Seg_4259" s="T191">1PL.[NOM]</ta>
            <ta e="T193" id="Seg_4260" s="T192">whereto</ta>
            <ta e="T194" id="Seg_4261" s="T193">NEG</ta>
            <ta e="T195" id="Seg_4262" s="T194">go-NEG-1PL</ta>
            <ta e="T196" id="Seg_4263" s="T195">say-PRS-3PL</ta>
            <ta e="T197" id="Seg_4264" s="T196">mother-1SG.[NOM]</ta>
            <ta e="T198" id="Seg_4265" s="T197">eh</ta>
            <ta e="T199" id="Seg_4266" s="T198">mother</ta>
            <ta e="T200" id="Seg_4267" s="T199">mother-3SG.[NOM]</ta>
            <ta e="T201" id="Seg_4268" s="T200">memory.[NOM]</ta>
            <ta e="T202" id="Seg_4269" s="T201">speak-PST2.[3SG]</ta>
            <ta e="T203" id="Seg_4270" s="T202">MOD</ta>
            <ta e="T204" id="Seg_4271" s="T203">this</ta>
            <ta e="T205" id="Seg_4272" s="T204">well</ta>
            <ta e="T206" id="Seg_4273" s="T205">older.sister-2PL-DAT/LOC</ta>
            <ta e="T207" id="Seg_4274" s="T206">stay-CVB.SEQ</ta>
            <ta e="T208" id="Seg_4275" s="T207">say-PST2.[3SG]</ta>
            <ta e="T209" id="Seg_4276" s="T208">MOD</ta>
            <ta e="T210" id="Seg_4277" s="T209">1SG.[NOM]</ta>
            <ta e="T211" id="Seg_4278" s="T210">how</ta>
            <ta e="T212" id="Seg_4279" s="T211">be-TEMP-1SG</ta>
            <ta e="T213" id="Seg_4280" s="T212">well</ta>
            <ta e="T214" id="Seg_4281" s="T213">that</ta>
            <ta e="T215" id="Seg_4282" s="T214">that</ta>
            <ta e="T216" id="Seg_4283" s="T215">four</ta>
            <ta e="T217" id="Seg_4284" s="T216">four</ta>
            <ta e="T218" id="Seg_4285" s="T217">child.[NOM]</ta>
            <ta e="T219" id="Seg_4286" s="T218">1PL-DAT/LOC</ta>
            <ta e="T220" id="Seg_4287" s="T219">come-PST1-3PL</ta>
            <ta e="T221" id="Seg_4288" s="T220">only</ta>
            <ta e="T222" id="Seg_4289" s="T221">mm</ta>
            <ta e="T223" id="Seg_4290" s="T222">Khatanga.[NOM]</ta>
            <ta e="T224" id="Seg_4291" s="T223">learn-CVB.SIM</ta>
            <ta e="T225" id="Seg_4292" s="T224">go-PRS-3PL</ta>
            <ta e="T231" id="Seg_4293" s="T230">well</ta>
            <ta e="T232" id="Seg_4294" s="T231">INTJ</ta>
            <ta e="T233" id="Seg_4295" s="T232">then</ta>
            <ta e="T237" id="Seg_4296" s="T236">well</ta>
            <ta e="T238" id="Seg_4297" s="T237">then</ta>
            <ta e="T239" id="Seg_4298" s="T238">holiday-DAT/LOC</ta>
            <ta e="T240" id="Seg_4299" s="T239">come-PRS-3PL</ta>
            <ta e="T241" id="Seg_4300" s="T240">this</ta>
            <ta e="T242" id="Seg_4301" s="T241">child-PL.[NOM]</ta>
            <ta e="T243" id="Seg_4302" s="T242">advise-PRS-3PL</ta>
            <ta e="T244" id="Seg_4303" s="T243">head-PL.[NOM]</ta>
            <ta e="T245" id="Seg_4304" s="T244">only</ta>
            <ta e="T246" id="Seg_4305" s="T245">this</ta>
            <ta e="T247" id="Seg_4306" s="T246">child-ACC</ta>
            <ta e="T248" id="Seg_4307" s="T247">MOD</ta>
            <ta e="T249" id="Seg_4308" s="T248">Popigaj-DAT/LOC</ta>
            <ta e="T250" id="Seg_4309" s="T249">send-PTCP.FUT-DAT/LOC</ta>
            <ta e="T251" id="Seg_4310" s="T250">say-PRS-3PL</ta>
            <ta e="T253" id="Seg_4311" s="T251">sibling-3SG-DAT/LOC</ta>
            <ta e="T257" id="Seg_4312" s="T255">mm</ta>
            <ta e="T258" id="Seg_4313" s="T257">Katya-ACC</ta>
            <ta e="T259" id="Seg_4314" s="T258">one</ta>
            <ta e="T260" id="Seg_4315" s="T259">one</ta>
            <ta e="T261" id="Seg_4316" s="T260">child-ACC</ta>
            <ta e="T262" id="Seg_4317" s="T261">MOD</ta>
            <ta e="T263" id="Seg_4318" s="T262">here</ta>
            <ta e="T264" id="Seg_4319" s="T263">tundra-DAT/LOC</ta>
            <ta e="T265" id="Seg_4320" s="T264">there.is</ta>
            <ta e="T266" id="Seg_4321" s="T265">tundra</ta>
            <ta e="T267" id="Seg_4322" s="T266">old.man-PROPR</ta>
            <ta e="T268" id="Seg_4323" s="T267">old.woman.[NOM]</ta>
            <ta e="T269" id="Seg_4324" s="T268">sibling-1PL.[NOM]</ta>
            <ta e="T270" id="Seg_4325" s="T269">only</ta>
            <ta e="T271" id="Seg_4326" s="T270">well</ta>
            <ta e="T272" id="Seg_4327" s="T271">that-PL-DAT/LOC</ta>
            <ta e="T273" id="Seg_4328" s="T272">give-PST1-3PL</ta>
            <ta e="T274" id="Seg_4329" s="T273">then</ta>
            <ta e="T275" id="Seg_4330" s="T274">1SG.[NOM]</ta>
            <ta e="T276" id="Seg_4331" s="T275">two</ta>
            <ta e="T277" id="Seg_4332" s="T276">boy.[NOM]</ta>
            <ta e="T278" id="Seg_4333" s="T277">child-ACC</ta>
            <ta e="T279" id="Seg_4334" s="T278">with</ta>
            <ta e="T280" id="Seg_4335" s="T279">stay-PST1-1SG</ta>
            <ta e="T281" id="Seg_4336" s="T280">EMPH</ta>
            <ta e="T282" id="Seg_4337" s="T281">only</ta>
            <ta e="T283" id="Seg_4338" s="T282">that-PL.[NOM]</ta>
            <ta e="T284" id="Seg_4339" s="T283">1SG-DAT/LOC</ta>
            <ta e="T285" id="Seg_4340" s="T284">go</ta>
            <ta e="T286" id="Seg_4341" s="T285">go-NEG-3PL</ta>
            <ta e="T287" id="Seg_4342" s="T286">back</ta>
            <ta e="T288" id="Seg_4343" s="T287">that.[NOM]</ta>
            <ta e="T289" id="Seg_4344" s="T288">two</ta>
            <ta e="T290" id="Seg_4345" s="T289">boy.[NOM]</ta>
            <ta e="T291" id="Seg_4346" s="T290">child.[NOM]</ta>
            <ta e="T292" id="Seg_4347" s="T291">well</ta>
            <ta e="T293" id="Seg_4348" s="T292">then</ta>
            <ta e="T294" id="Seg_4349" s="T293">how.much</ta>
            <ta e="T295" id="Seg_4350" s="T294">INDEF</ta>
            <ta e="T296" id="Seg_4351" s="T295">be-NEG.CVB.SIM</ta>
            <ta e="T297" id="Seg_4352" s="T296">holiday-DAT/LOC</ta>
            <ta e="T298" id="Seg_4353" s="T297">come-PST2-3PL</ta>
            <ta e="T299" id="Seg_4354" s="T298">that</ta>
            <ta e="T300" id="Seg_4355" s="T299">child-PL-EP-2SG.[NOM]</ta>
            <ta e="T301" id="Seg_4356" s="T300">EMPH</ta>
            <ta e="T302" id="Seg_4357" s="T301">back</ta>
            <ta e="T303" id="Seg_4358" s="T302">come-PST1-3PL</ta>
            <ta e="T304" id="Seg_4359" s="T303">1PL-DAT/LOC</ta>
            <ta e="T305" id="Seg_4360" s="T304">that</ta>
            <ta e="T306" id="Seg_4361" s="T305">be.content-NEG-3PL</ta>
            <ta e="T307" id="Seg_4362" s="T306">be-PST2.[3SG]</ta>
            <ta e="T308" id="Seg_4363" s="T307">that-PL-ACC</ta>
            <ta e="T309" id="Seg_4364" s="T308">then</ta>
            <ta e="T310" id="Seg_4365" s="T309">tundra-DAT/LOC</ta>
            <ta e="T311" id="Seg_4366" s="T310">old.man-PROPR</ta>
            <ta e="T312" id="Seg_4367" s="T311">old.woman.[NOM]</ta>
            <ta e="T313" id="Seg_4368" s="T312">again</ta>
            <ta e="T314" id="Seg_4369" s="T313">that</ta>
            <ta e="T315" id="Seg_4370" s="T314">go-PST2.[3SG]</ta>
            <ta e="T316" id="Seg_4371" s="T315">two</ta>
            <ta e="T317" id="Seg_4372" s="T316">child-1PL.[NOM]</ta>
            <ta e="T318" id="Seg_4373" s="T317">again</ta>
            <ta e="T319" id="Seg_4374" s="T318">again</ta>
            <ta e="T320" id="Seg_4375" s="T319">back</ta>
            <ta e="T321" id="Seg_4376" s="T320">come-PST1-3SG</ta>
            <ta e="T322" id="Seg_4377" s="T321">EMPH</ta>
            <ta e="T323" id="Seg_4378" s="T322">then</ta>
            <ta e="T324" id="Seg_4379" s="T323">eight</ta>
            <ta e="T325" id="Seg_4380" s="T324">child-PROPR.[NOM]</ta>
            <ta e="T326" id="Seg_4381" s="T325">become-PST1-1SG</ta>
            <ta e="T327" id="Seg_4382" s="T326">EMPH</ta>
            <ta e="T328" id="Seg_4383" s="T327">1SG.[NOM]</ta>
            <ta e="T330" id="Seg_4384" s="T329">self-1SG.[NOM]</ta>
            <ta e="T331" id="Seg_4385" s="T330">four</ta>
            <ta e="T332" id="Seg_4386" s="T331">child-PROPR-1SG</ta>
            <ta e="T333" id="Seg_4387" s="T332">MOD</ta>
            <ta e="T334" id="Seg_4388" s="T333">that.[NOM]</ta>
            <ta e="T335" id="Seg_4389" s="T334">two</ta>
            <ta e="T336" id="Seg_4390" s="T335">girl-PROPR-1SG</ta>
            <ta e="T337" id="Seg_4391" s="T336">MOD</ta>
            <ta e="T338" id="Seg_4392" s="T337">mother-1SG.[NOM]</ta>
            <ta e="T339" id="Seg_4393" s="T338">old.[NOM]</ta>
            <ta e="T340" id="Seg_4394" s="T339">elder.brother-EP-1SG.[NOM]</ta>
            <ta e="T341" id="Seg_4395" s="T340">old.[NOM]</ta>
            <ta e="T342" id="Seg_4396" s="T341">then</ta>
            <ta e="T343" id="Seg_4397" s="T342">that</ta>
            <ta e="T344" id="Seg_4398" s="T343">eight</ta>
            <ta e="T345" id="Seg_4399" s="T344">child-PROPR-1SG</ta>
            <ta e="T346" id="Seg_4400" s="T345">self-1SG.[NOM]</ta>
            <ta e="T347" id="Seg_4401" s="T346">older.sister-EP-1SG.[NOM]</ta>
            <ta e="T348" id="Seg_4402" s="T347">own-3SG</ta>
            <ta e="T349" id="Seg_4403" s="T348">four</ta>
            <ta e="T350" id="Seg_4404" s="T349">child.[NOM]</ta>
            <ta e="T351" id="Seg_4405" s="T350">stay-PST1-3SG</ta>
            <ta e="T352" id="Seg_4406" s="T351">my</ta>
            <ta e="T353" id="Seg_4407" s="T352">four</ta>
            <ta e="T354" id="Seg_4408" s="T353">child.[NOM]</ta>
            <ta e="T355" id="Seg_4409" s="T354">MOD</ta>
            <ta e="T360" id="Seg_4410" s="T359">small.[NOM]</ta>
            <ta e="T361" id="Seg_4411" s="T360">be-PST1-3SG</ta>
            <ta e="T362" id="Seg_4412" s="T361">that</ta>
            <ta e="T363" id="Seg_4413" s="T362">well</ta>
            <ta e="T364" id="Seg_4414" s="T363">that</ta>
            <ta e="T365" id="Seg_4415" s="T364">stay-CVB.SEQ-1SG</ta>
            <ta e="T366" id="Seg_4416" s="T365">well</ta>
            <ta e="T367" id="Seg_4417" s="T366">that.[NOM]</ta>
            <ta e="T368" id="Seg_4418" s="T367">live-PST1-1PL</ta>
            <ta e="T369" id="Seg_4419" s="T368">MOD</ta>
            <ta e="T370" id="Seg_4420" s="T369">mother-1SG.[NOM]</ta>
            <ta e="T371" id="Seg_4421" s="T370">age-CVB.SEQ</ta>
            <ta e="T372" id="Seg_4422" s="T371">die-PST1-3SG</ta>
            <ta e="T373" id="Seg_4423" s="T372">be.sick-CVB.SEQ</ta>
            <ta e="T374" id="Seg_4424" s="T373">that</ta>
            <ta e="T375" id="Seg_4425" s="T374">elder.brother-EP-1SG.[NOM]</ta>
            <ta e="T376" id="Seg_4426" s="T375">again</ta>
            <ta e="T377" id="Seg_4427" s="T376">woman-POSS</ta>
            <ta e="T378" id="Seg_4428" s="T377">NEG</ta>
            <ta e="T379" id="Seg_4429" s="T378">NEG</ta>
            <ta e="T380" id="Seg_4430" s="T379">what.[NOM]</ta>
            <ta e="T381" id="Seg_4431" s="T380">NEG</ta>
            <ta e="T382" id="Seg_4432" s="T381">NEG</ta>
            <ta e="T383" id="Seg_4433" s="T382">be-PST1-3SG</ta>
            <ta e="T384" id="Seg_4434" s="T383">shop-DAT/LOC</ta>
            <ta e="T385" id="Seg_4435" s="T384">work-PTCP.HAB</ta>
            <ta e="T386" id="Seg_4436" s="T385">be-PST1-3SG</ta>
            <ta e="T387" id="Seg_4437" s="T386">that.[NOM]</ta>
            <ta e="T388" id="Seg_4438" s="T387">because.of</ta>
            <ta e="T389" id="Seg_4439" s="T388">food-EP-INSTR</ta>
            <ta e="T390" id="Seg_4440" s="T389">NEG</ta>
            <ta e="T391" id="Seg_4441" s="T390">very</ta>
            <ta e="T392" id="Seg_4442" s="T391">NEG</ta>
            <ta e="T393" id="Seg_4443" s="T392">who-VBZ-PTCP.HAB-3SG</ta>
            <ta e="T394" id="Seg_4444" s="T393">NEG</ta>
            <ta e="T395" id="Seg_4445" s="T394">be-PST1-1PL</ta>
            <ta e="T396" id="Seg_4446" s="T395">work-PTCP.PRS</ta>
            <ta e="T397" id="Seg_4447" s="T396">be-CVB.SEQ</ta>
            <ta e="T399" id="Seg_4448" s="T398">get-CVB.SEQ</ta>
            <ta e="T400" id="Seg_4449" s="T399">go-HAB.[3SG]</ta>
            <ta e="T401" id="Seg_4450" s="T400">food.[NOM]</ta>
            <ta e="T402" id="Seg_4451" s="T401">shop-ABL</ta>
            <ta e="T403" id="Seg_4452" s="T402">salary-DIM.[NOM]</ta>
            <ta e="T404" id="Seg_4453" s="T403">that.[NOM]</ta>
            <ta e="T405" id="Seg_4454" s="T404">get-HAB.[3SG]</ta>
            <ta e="T406" id="Seg_4455" s="T405">then</ta>
            <ta e="T407" id="Seg_4456" s="T406">1SG.[NOM]</ta>
            <ta e="T408" id="Seg_4457" s="T407">cook-VBZ-PRS-1SG</ta>
            <ta e="T409" id="Seg_4458" s="T408">MOD</ta>
            <ta e="T410" id="Seg_4459" s="T409">that</ta>
            <ta e="T411" id="Seg_4460" s="T410">that.[NOM]</ta>
            <ta e="T412" id="Seg_4461" s="T411">live-PST2-1PL</ta>
            <ta e="T413" id="Seg_4462" s="T412">every-INTNS-3PL.[NOM]</ta>
            <ta e="T414" id="Seg_4463" s="T413">grow-PST2-3PL</ta>
            <ta e="T415" id="Seg_4464" s="T414">apparently</ta>
            <ta e="T416" id="Seg_4465" s="T415">woman-PROPR-3PL</ta>
            <ta e="T422" id="Seg_4466" s="T421">well</ta>
            <ta e="T423" id="Seg_4467" s="T422">go.out-EP-PST2-3SG</ta>
            <ta e="T424" id="Seg_4468" s="T423">what.[NOM]</ta>
            <ta e="T425" id="Seg_4469" s="T424">make-CVB.PURP</ta>
            <ta e="T426" id="Seg_4470" s="T425">and</ta>
            <ta e="T427" id="Seg_4471" s="T426">go.out-EP-PST2.[3SG]</ta>
            <ta e="T428" id="Seg_4472" s="T427">well</ta>
            <ta e="T429" id="Seg_4473" s="T428">that.[NOM]</ta>
            <ta e="T430" id="Seg_4474" s="T429">because.of</ta>
            <ta e="T431" id="Seg_4475" s="T430">eight</ta>
            <ta e="T432" id="Seg_4476" s="T431">child.[NOM]</ta>
            <ta e="T433" id="Seg_4477" s="T432">be-PRS.[3SG]</ta>
            <ta e="T434" id="Seg_4478" s="T433">MOD</ta>
            <ta e="T435" id="Seg_4479" s="T434">young.[NOM]</ta>
            <ta e="T436" id="Seg_4480" s="T435">woman-1SG</ta>
            <ta e="T437" id="Seg_4481" s="T436">EMPH</ta>
            <ta e="T438" id="Seg_4482" s="T437">there</ta>
            <ta e="T439" id="Seg_4483" s="T438">that</ta>
            <ta e="T443" id="Seg_4484" s="T442">well</ta>
            <ta e="T444" id="Seg_4485" s="T443">husband-DAT/LOC</ta>
            <ta e="T445" id="Seg_4486" s="T444">go.out-CVB.SEQ</ta>
            <ta e="T446" id="Seg_4487" s="T445">stay-PST1-1SG</ta>
            <ta e="T447" id="Seg_4488" s="T446">EMPH</ta>
            <ta e="T448" id="Seg_4489" s="T447">two</ta>
            <ta e="T449" id="Seg_4490" s="T448">child-ACC</ta>
            <ta e="T450" id="Seg_4491" s="T449">be.born-PTCP.PST</ta>
            <ta e="T451" id="Seg_4492" s="T450">MOD</ta>
            <ta e="T452" id="Seg_4493" s="T451">that</ta>
            <ta e="T453" id="Seg_4494" s="T452">three-ORD</ta>
            <ta e="T454" id="Seg_4495" s="T453">three-ORD</ta>
            <ta e="T455" id="Seg_4496" s="T454">child-1SG.[NOM]</ta>
            <ta e="T456" id="Seg_4497" s="T455">boy.[NOM]</ta>
            <ta e="T457" id="Seg_4498" s="T456">child.[NOM]</ta>
            <ta e="T458" id="Seg_4499" s="T457">be-PST1-3SG</ta>
            <ta e="T459" id="Seg_4500" s="T458">that-3SG-1SG.[NOM]</ta>
            <ta e="T460" id="Seg_4501" s="T459">fall-PST2-3SG</ta>
            <ta e="T461" id="Seg_4502" s="T460">kill-EP-MED-CVB.SEQ-1SG</ta>
            <ta e="T463" id="Seg_4503" s="T462">kindergarten-DAT/LOC</ta>
            <ta e="T464" id="Seg_4504" s="T463">ice-PL-ACC</ta>
            <ta e="T465" id="Seg_4505" s="T464">coal-PL-ACC</ta>
            <ta e="T466" id="Seg_4506" s="T465">pull-VBZ-CVB.SEQ</ta>
            <ta e="T467" id="Seg_4507" s="T466">that</ta>
            <ta e="T468" id="Seg_4508" s="T467">fall-PTCP.PST</ta>
            <ta e="T469" id="Seg_4509" s="T468">be-PST1-3SG</ta>
            <ta e="T470" id="Seg_4510" s="T469">that.[NOM]</ta>
            <ta e="T471" id="Seg_4511" s="T470">two</ta>
            <ta e="T472" id="Seg_4512" s="T471">boy.[NOM]</ta>
            <ta e="T473" id="Seg_4513" s="T472">space.in.between-3SG.[NOM]</ta>
            <ta e="T481" id="Seg_4514" s="T480">Elena.[NOM]</ta>
            <ta e="T482" id="Seg_4515" s="T481">six</ta>
            <ta e="T483" id="Seg_4516" s="T482">year-PROPR-ACC</ta>
            <ta e="T484" id="Seg_4517" s="T483">get-PST2-PST2</ta>
            <ta e="T485" id="Seg_4518" s="T484">then</ta>
            <ta e="T486" id="Seg_4519" s="T485">seven</ta>
            <ta e="T487" id="Seg_4520" s="T486">year-VBZ-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T488" id="Seg_4521" s="T487">hither</ta>
            <ta e="T489" id="Seg_4522" s="T488">come-PST1-1SG</ta>
            <ta e="T490" id="Seg_4523" s="T489">MOD</ta>
            <ta e="T491" id="Seg_4524" s="T490">Dudinka-DAT/LOC</ta>
            <ta e="T492" id="Seg_4525" s="T491">child-1SG.[NOM]</ta>
            <ta e="T493" id="Seg_4526" s="T492">house.[NOM]</ta>
            <ta e="T494" id="Seg_4527" s="T493">get-PTCP.PRS</ta>
            <ta e="T495" id="Seg_4528" s="T494">be-PST2.[3SG]</ta>
            <ta e="T496" id="Seg_4529" s="T495">only</ta>
            <ta e="T497" id="Seg_4530" s="T496">well</ta>
            <ta e="T498" id="Seg_4531" s="T497">that-DAT/LOC</ta>
            <ta e="T499" id="Seg_4532" s="T498">residence.permit-VBZ-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T500" id="Seg_4533" s="T499">need.to</ta>
            <ta e="T501" id="Seg_4534" s="T500">be-PST2.[3SG]</ta>
            <ta e="T502" id="Seg_4535" s="T501">well</ta>
            <ta e="T503" id="Seg_4536" s="T502">that</ta>
            <ta e="T504" id="Seg_4537" s="T503">come-PST2-EP-1SG</ta>
            <ta e="T505" id="Seg_4538" s="T504">that</ta>
            <ta e="T506" id="Seg_4539" s="T505">come-CVB.SEQ-1SG</ta>
            <ta e="T507" id="Seg_4540" s="T506">EMPH</ta>
            <ta e="T508" id="Seg_4541" s="T507">child-1SG.[NOM]</ta>
            <ta e="T509" id="Seg_4542" s="T508">ten-ORD</ta>
            <ta e="T510" id="Seg_4543" s="T509">class.[NOM]</ta>
            <ta e="T511" id="Seg_4544" s="T510">be-PST1-3SG</ta>
            <ta e="T513" id="Seg_4545" s="T512">well</ta>
            <ta e="T514" id="Seg_4546" s="T513">mum-3SG.[NOM]</ta>
            <ta e="T515" id="Seg_4547" s="T514">though</ta>
            <ta e="T516" id="Seg_4548" s="T515">Yakutsk-DAT/LOC</ta>
            <ta e="T517" id="Seg_4549" s="T516">husband-VBZ-PST2-3SG</ta>
            <ta e="T518" id="Seg_4550" s="T517">that-3SG-2SG.[NOM]</ta>
            <ta e="T519" id="Seg_4551" s="T518">now</ta>
            <ta e="T520" id="Seg_4552" s="T519">two</ta>
            <ta e="T521" id="Seg_4553" s="T520">child-PROPR.[NOM]</ta>
            <ta e="T522" id="Seg_4554" s="T521">boy-ACC</ta>
            <ta e="T523" id="Seg_4555" s="T522">with</ta>
            <ta e="T524" id="Seg_4556" s="T523">girl.[NOM]</ta>
            <ta e="T529" id="Seg_4557" s="T528">tundra-DAT/LOC</ta>
            <ta e="T530" id="Seg_4558" s="T529">tundra-DAT/LOC</ta>
            <ta e="T531" id="Seg_4559" s="T530">live-PRS-3PL</ta>
            <ta e="T532" id="Seg_4560" s="T531">tundra-DAT/LOC</ta>
            <ta e="T533" id="Seg_4561" s="T532">reindeer-AG.[NOM]</ta>
            <ta e="T534" id="Seg_4562" s="T533">husband-3SG.[NOM]</ta>
            <ta e="T536" id="Seg_4563" s="T535">last-3SG.[NOM]</ta>
            <ta e="T537" id="Seg_4564" s="T536">Syndassko-DAT/LOC</ta>
            <ta e="T538" id="Seg_4565" s="T537">stay-PST2-3PL</ta>
            <ta e="T539" id="Seg_4566" s="T538">now</ta>
            <ta e="T540" id="Seg_4567" s="T539">go-FUT-3PL</ta>
            <ta e="T541" id="Seg_4568" s="T540">May</ta>
            <ta e="T542" id="Seg_4569" s="T541">month-DAT/LOC</ta>
            <ta e="T543" id="Seg_4570" s="T542">school.[NOM]</ta>
            <ta e="T544" id="Seg_4571" s="T543">stop-PASS-CVB.SEQ</ta>
            <ta e="T545" id="Seg_4572" s="T544">child-3PL.[NOM]</ta>
            <ta e="T546" id="Seg_4573" s="T545">there</ta>
            <ta e="T547" id="Seg_4574" s="T546">learn-PRS-3PL</ta>
            <ta e="T548" id="Seg_4575" s="T547">Syndassko-DAT/LOC</ta>
            <ta e="T549" id="Seg_4576" s="T548">one.out.of.two-3SG.[NOM]</ta>
            <ta e="T550" id="Seg_4577" s="T549">four-ORD</ta>
            <ta e="T551" id="Seg_4578" s="T550">class.[NOM]</ta>
            <ta e="T552" id="Seg_4579" s="T551">one.out.of.two-3SG.[NOM]</ta>
            <ta e="T553" id="Seg_4580" s="T552">three-ORD</ta>
            <ta e="T554" id="Seg_4581" s="T553">class.[NOM]</ta>
            <ta e="T555" id="Seg_4582" s="T554">Q</ta>
            <ta e="T556" id="Seg_4583" s="T555">two-ORD</ta>
            <ta e="T557" id="Seg_4584" s="T556">Q</ta>
            <ta e="T559" id="Seg_4585" s="T558">two-ORD</ta>
            <ta e="T560" id="Seg_4586" s="T559">class.[NOM]</ta>
            <ta e="T561" id="Seg_4587" s="T560">eh</ta>
            <ta e="T562" id="Seg_4588" s="T561">daughter-3SG.[NOM]</ta>
            <ta e="T563" id="Seg_4589" s="T562">now</ta>
            <ta e="T564" id="Seg_4590" s="T563">1SG.[NOM]</ta>
            <ta e="T565" id="Seg_4591" s="T564">house-1SG-DAT/LOC</ta>
            <ta e="T566" id="Seg_4592" s="T565">stay-PST2-3PL</ta>
            <ta e="T567" id="Seg_4593" s="T566">now</ta>
            <ta e="T568" id="Seg_4594" s="T567">May-DAT/LOC</ta>
            <ta e="T569" id="Seg_4595" s="T568">go-FUT-3PL</ta>
            <ta e="T570" id="Seg_4596" s="T569">now</ta>
            <ta e="T572" id="Seg_4597" s="T571">nomadize-CVB.SEQ</ta>
            <ta e="T573" id="Seg_4598" s="T572">go-FUT-3PL</ta>
            <ta e="T574" id="Seg_4599" s="T573">last-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg-KiES">
            <ta e="T2" id="Seg_4600" s="T1">doch</ta>
            <ta e="T3" id="Seg_4601" s="T2">Korgo-DAT/LOC</ta>
            <ta e="T4" id="Seg_4602" s="T3">es.gibt</ta>
            <ta e="T5" id="Seg_4603" s="T4">sein-PST1-1PL</ta>
            <ta e="T6" id="Seg_4604" s="T5">Erde-PROPR.[NOM]</ta>
            <ta e="T7" id="Seg_4605" s="T6">sein-PST1-1PL</ta>
            <ta e="T8" id="Seg_4606" s="T7">3PL-DAT/LOC</ta>
            <ta e="T9" id="Seg_4607" s="T8">Korgo-DAT/LOC</ta>
            <ta e="T11" id="Seg_4608" s="T9">Erde-VBZ-PST-EP-1SG</ta>
            <ta e="T12" id="Seg_4609" s="T11">dann</ta>
            <ta e="T13" id="Seg_4610" s="T12">aber</ta>
            <ta e="T14" id="Seg_4611" s="T13">dieses</ta>
            <ta e="T15" id="Seg_4612" s="T14">äh</ta>
            <ta e="T16" id="Seg_4613" s="T15">dieses</ta>
            <ta e="T17" id="Seg_4614" s="T16">Kind-ACC</ta>
            <ta e="T18" id="Seg_4615" s="T17">gebären-PST2-EP-1SG</ta>
            <ta e="T19" id="Seg_4616" s="T18">dann</ta>
            <ta e="T20" id="Seg_4617" s="T19">jenes</ta>
            <ta e="T21" id="Seg_4618" s="T20">Junge.[NOM]</ta>
            <ta e="T22" id="Seg_4619" s="T21">Kind-1SG.[NOM]</ta>
            <ta e="T23" id="Seg_4620" s="T22">gebären-CVB.SEQ-1SG</ta>
            <ta e="T24" id="Seg_4621" s="T23">jenes</ta>
            <ta e="T25" id="Seg_4622" s="T24">dann</ta>
            <ta e="T26" id="Seg_4623" s="T25">aber</ta>
            <ta e="T27" id="Seg_4624" s="T26">alter.Mann-1PL.[NOM]</ta>
            <ta e="T28" id="Seg_4625" s="T27">krank.sein-PTCP.PRS</ta>
            <ta e="T29" id="Seg_4626" s="T28">sein-PST1-3SG</ta>
            <ta e="T30" id="Seg_4627" s="T29">sterben-CVB.SEQ</ta>
            <ta e="T31" id="Seg_4628" s="T30">sterben-CVB.SEQ</ta>
            <ta e="T32" id="Seg_4629" s="T31">jenes</ta>
            <ta e="T33" id="Seg_4630" s="T32">wer-DAT/LOC</ta>
            <ta e="T34" id="Seg_4631" s="T33">Syndassko-DAT/LOC</ta>
            <ta e="T35" id="Seg_4632" s="T34">sich.hinlegen-PST2-1PL</ta>
            <ta e="T36" id="Seg_4633" s="T35">jenes</ta>
            <ta e="T37" id="Seg_4634" s="T36">dann</ta>
            <ta e="T38" id="Seg_4635" s="T37">Arbeit-1SG.[NOM]</ta>
            <ta e="T39" id="Seg_4636" s="T38">1SG-ACC</ta>
            <ta e="T43" id="Seg_4637" s="T42">dieses</ta>
            <ta e="T44" id="Seg_4638" s="T43">nun</ta>
            <ta e="T48" id="Seg_4639" s="T47">dann</ta>
            <ta e="T49" id="Seg_4640" s="T48">Arbeit.[NOM]</ta>
            <ta e="T50" id="Seg_4641" s="T49">geben-PST2-3PL</ta>
            <ta e="T51" id="Seg_4642" s="T50">1SG-DAT/LOC</ta>
            <ta e="T52" id="Seg_4643" s="T51">Kindergarten-DAT/LOC</ta>
            <ta e="T64" id="Seg_4644" s="T63">AFFIRM</ta>
            <ta e="T65" id="Seg_4645" s="T64">dolganisch-SIM</ta>
            <ta e="T66" id="Seg_4646" s="T65">MOD</ta>
            <ta e="T67" id="Seg_4647" s="T66">Syndassko-DAT/LOC</ta>
            <ta e="T68" id="Seg_4648" s="T67">dolganisch</ta>
            <ta e="T69" id="Seg_4649" s="T68">erstmals</ta>
            <ta e="T70" id="Seg_4650" s="T69">Kindergarten.[NOM]</ta>
            <ta e="T71" id="Seg_4651" s="T70">stehen-PTCP.PST</ta>
            <ta e="T72" id="Seg_4652" s="T71">sein-PST1-3SG</ta>
            <ta e="T73" id="Seg_4653" s="T72">doch</ta>
            <ta e="T74" id="Seg_4654" s="T73">was-ACC</ta>
            <ta e="T75" id="Seg_4655" s="T74">NEG</ta>
            <ta e="T76" id="Seg_4656" s="T75">wissen-NEG-1SG</ta>
            <ta e="T77" id="Seg_4657" s="T76">Kindergarten.[NOM]</ta>
            <ta e="T78" id="Seg_4658" s="T77">was.[NOM]</ta>
            <ta e="T79" id="Seg_4659" s="T78">Name-3SG.[NOM]=Q</ta>
            <ta e="T92" id="Seg_4660" s="T91">oh</ta>
            <ta e="T94" id="Seg_4661" s="T93">doch</ta>
            <ta e="T95" id="Seg_4662" s="T94">wie</ta>
            <ta e="T96" id="Seg_4663" s="T95">sein-FUT-1PL=Q</ta>
            <ta e="T98" id="Seg_4664" s="T97">können-CVB.SEQ</ta>
            <ta e="T99" id="Seg_4665" s="T98">sprechen-NEG-1SG</ta>
            <ta e="T100" id="Seg_4666" s="T99">Kind-PL-DAT/LOC</ta>
            <ta e="T101" id="Seg_4667" s="T100">doch</ta>
            <ta e="T102" id="Seg_4668" s="T101">Kind-PL-DAT/LOC</ta>
            <ta e="T103" id="Seg_4669" s="T102">können-CVB.SEQ</ta>
            <ta e="T104" id="Seg_4670" s="T103">kochen-EP-CAUS-NEG-1SG</ta>
            <ta e="T105" id="Seg_4671" s="T104">sagen-PRS-1SG</ta>
            <ta e="T106" id="Seg_4672" s="T105">NEG.EX</ta>
            <ta e="T107" id="Seg_4673" s="T106">Koch-PL.[NOM]</ta>
            <ta e="T108" id="Seg_4674" s="T107">sagen-PRS-3PL</ta>
            <ta e="T109" id="Seg_4675" s="T108">EMPH</ta>
            <ta e="T110" id="Seg_4676" s="T109">dieses</ta>
            <ta e="T111" id="Seg_4677" s="T110">Kindergarten.[NOM]</ta>
            <ta e="T112" id="Seg_4678" s="T111">stehen-PST1-3SG</ta>
            <ta e="T113" id="Seg_4679" s="T112">neu</ta>
            <ta e="T114" id="Seg_4680" s="T113">Kindergarten-DAT/LOC</ta>
            <ta e="T115" id="Seg_4681" s="T114">jenes-DAT/LOC</ta>
            <ta e="T116" id="Seg_4682" s="T115">Koch.[NOM]</ta>
            <ta e="T117" id="Seg_4683" s="T116">Koch.[NOM]</ta>
            <ta e="T118" id="Seg_4684" s="T117">man.muss</ta>
            <ta e="T119" id="Seg_4685" s="T118">sagen-PRS-3PL</ta>
            <ta e="T120" id="Seg_4686" s="T119">Chef-PL.[NOM]</ta>
            <ta e="T121" id="Seg_4687" s="T120">so</ta>
            <ta e="T122" id="Seg_4688" s="T121">Chef-PL.[NOM]</ta>
            <ta e="T123" id="Seg_4689" s="T122">1SG-ACC</ta>
            <ta e="T124" id="Seg_4690" s="T123">Koch.[NOM]</ta>
            <ta e="T125" id="Seg_4691" s="T124">legen-PST1-3PL</ta>
            <ta e="T126" id="Seg_4692" s="T125">MOD</ta>
            <ta e="T127" id="Seg_4693" s="T126">doch</ta>
            <ta e="T128" id="Seg_4694" s="T127">jenes</ta>
            <ta e="T129" id="Seg_4695" s="T128">liegen-PST2-1PL</ta>
            <ta e="T130" id="Seg_4696" s="T129">dann</ta>
            <ta e="T131" id="Seg_4697" s="T130">jenes</ta>
            <ta e="T132" id="Seg_4698" s="T131">Koch-VBZ-CVB.SEQ-1SG</ta>
            <ta e="T133" id="Seg_4699" s="T132">aber</ta>
            <ta e="T134" id="Seg_4700" s="T133">äh</ta>
            <ta e="T135" id="Seg_4701" s="T134">wer-DAT/LOC</ta>
            <ta e="T136" id="Seg_4702" s="T135">dieses</ta>
            <ta e="T137" id="Seg_4703" s="T136">nun</ta>
            <ta e="T138" id="Seg_4704" s="T137">dieses.[NOM]</ta>
            <ta e="T139" id="Seg_4705" s="T138">zwei</ta>
            <ta e="T140" id="Seg_4706" s="T139">Kind-1SG.[NOM]</ta>
            <ta e="T141" id="Seg_4707" s="T140">gehen-PRS-3PL</ta>
            <ta e="T142" id="Seg_4708" s="T141">Kindergarten-DAT/LOC</ta>
            <ta e="T143" id="Seg_4709" s="T142">nur</ta>
            <ta e="T144" id="Seg_4710" s="T143">AFFIRM</ta>
            <ta e="T145" id="Seg_4711" s="T144">doch</ta>
            <ta e="T146" id="Seg_4712" s="T145">jenes</ta>
            <ta e="T147" id="Seg_4713" s="T146">gehen-CVB.SEQ-3PL</ta>
            <ta e="T151" id="Seg_4714" s="T150">wer-ACC</ta>
            <ta e="T152" id="Seg_4715" s="T151">mit</ta>
            <ta e="T157" id="Seg_4716" s="T156">warum</ta>
            <ta e="T158" id="Seg_4717" s="T157">jenes</ta>
            <ta e="T159" id="Seg_4718" s="T158">dann</ta>
            <ta e="T160" id="Seg_4719" s="T159">jenes</ta>
            <ta e="T161" id="Seg_4720" s="T160">später</ta>
            <ta e="T162" id="Seg_4721" s="T161">jenes</ta>
            <ta e="T163" id="Seg_4722" s="T162">wer-EP-1SG.[NOM]</ta>
            <ta e="T164" id="Seg_4723" s="T163">jenes</ta>
            <ta e="T165" id="Seg_4724" s="T164">ältere.Schwester-EP-1SG.[NOM]</ta>
            <ta e="T166" id="Seg_4725" s="T165">sterben-PST2-3SG</ta>
            <ta e="T171" id="Seg_4726" s="T168">doch</ta>
            <ta e="T172" id="Seg_4727" s="T171">Mutter-1SG.[NOM]</ta>
            <ta e="T173" id="Seg_4728" s="T172">Mutter-1SG.[NOM]</ta>
            <ta e="T174" id="Seg_4729" s="T173">jüngere.Schwester-EP-3SG.[NOM]</ta>
            <ta e="T175" id="Seg_4730" s="T174">sterben-PST2-3SG</ta>
            <ta e="T176" id="Seg_4731" s="T175">krank.sein-CVB.SEQ</ta>
            <ta e="T177" id="Seg_4732" s="T176">dann</ta>
            <ta e="T178" id="Seg_4733" s="T177">vier</ta>
            <ta e="T179" id="Seg_4734" s="T178">Kind-PROPR.[NOM]</ta>
            <ta e="T180" id="Seg_4735" s="T179">bleiben-PST1-3SG</ta>
            <ta e="T181" id="Seg_4736" s="T180">dieses</ta>
            <ta e="T182" id="Seg_4737" s="T181">Mensch.[NOM]</ta>
            <ta e="T183" id="Seg_4738" s="T182">vier</ta>
            <ta e="T184" id="Seg_4739" s="T183">Kind.[NOM]</ta>
            <ta e="T185" id="Seg_4740" s="T184">MOD</ta>
            <ta e="T186" id="Seg_4741" s="T185">1SG-DAT/LOC</ta>
            <ta e="T187" id="Seg_4742" s="T186">kommen-PST1-3PL</ta>
            <ta e="T188" id="Seg_4743" s="T187">MOD</ta>
            <ta e="T189" id="Seg_4744" s="T188">EMPH</ta>
            <ta e="T190" id="Seg_4745" s="T189">ältere.Schwester-1PL-DAT/LOC</ta>
            <ta e="T191" id="Seg_4746" s="T190">sein-FUT-1PL</ta>
            <ta e="T192" id="Seg_4747" s="T191">1PL.[NOM]</ta>
            <ta e="T193" id="Seg_4748" s="T192">wohin</ta>
            <ta e="T194" id="Seg_4749" s="T193">NEG</ta>
            <ta e="T195" id="Seg_4750" s="T194">gehen-NEG-1PL</ta>
            <ta e="T196" id="Seg_4751" s="T195">sagen-PRS-3PL</ta>
            <ta e="T197" id="Seg_4752" s="T196">Mutter-1SG.[NOM]</ta>
            <ta e="T198" id="Seg_4753" s="T197">äh</ta>
            <ta e="T199" id="Seg_4754" s="T198">Mutter</ta>
            <ta e="T200" id="Seg_4755" s="T199">Mutter-3SG.[NOM]</ta>
            <ta e="T201" id="Seg_4756" s="T200">Gedenken.[NOM]</ta>
            <ta e="T202" id="Seg_4757" s="T201">sprechen-PST2.[3SG]</ta>
            <ta e="T203" id="Seg_4758" s="T202">MOD</ta>
            <ta e="T204" id="Seg_4759" s="T203">dieses</ta>
            <ta e="T205" id="Seg_4760" s="T204">nun</ta>
            <ta e="T206" id="Seg_4761" s="T205">ältere.Schwester-2PL-DAT/LOC</ta>
            <ta e="T207" id="Seg_4762" s="T206">bleiben-CVB.SEQ</ta>
            <ta e="T208" id="Seg_4763" s="T207">sagen-PST2.[3SG]</ta>
            <ta e="T209" id="Seg_4764" s="T208">MOD</ta>
            <ta e="T210" id="Seg_4765" s="T209">1SG.[NOM]</ta>
            <ta e="T211" id="Seg_4766" s="T210">wie</ta>
            <ta e="T212" id="Seg_4767" s="T211">sein-TEMP-1SG</ta>
            <ta e="T213" id="Seg_4768" s="T212">doch</ta>
            <ta e="T214" id="Seg_4769" s="T213">jenes</ta>
            <ta e="T215" id="Seg_4770" s="T214">jenes</ta>
            <ta e="T216" id="Seg_4771" s="T215">vier</ta>
            <ta e="T217" id="Seg_4772" s="T216">vier</ta>
            <ta e="T218" id="Seg_4773" s="T217">Kind.[NOM]</ta>
            <ta e="T219" id="Seg_4774" s="T218">1PL-DAT/LOC</ta>
            <ta e="T220" id="Seg_4775" s="T219">kommen-PST1-3PL</ta>
            <ta e="T221" id="Seg_4776" s="T220">nur</ta>
            <ta e="T222" id="Seg_4777" s="T221">mm</ta>
            <ta e="T223" id="Seg_4778" s="T222">Chatanga.[NOM]</ta>
            <ta e="T224" id="Seg_4779" s="T223">lernen-CVB.SIM</ta>
            <ta e="T225" id="Seg_4780" s="T224">gehen-PRS-3PL</ta>
            <ta e="T231" id="Seg_4781" s="T230">doch</ta>
            <ta e="T232" id="Seg_4782" s="T231">INTJ</ta>
            <ta e="T233" id="Seg_4783" s="T232">dann</ta>
            <ta e="T237" id="Seg_4784" s="T236">doch</ta>
            <ta e="T238" id="Seg_4785" s="T237">dann</ta>
            <ta e="T239" id="Seg_4786" s="T238">Urlaub-DAT/LOC</ta>
            <ta e="T240" id="Seg_4787" s="T239">kommen-PRS-3PL</ta>
            <ta e="T241" id="Seg_4788" s="T240">dieses</ta>
            <ta e="T242" id="Seg_4789" s="T241">Kind-PL.[NOM]</ta>
            <ta e="T243" id="Seg_4790" s="T242">raten-PRS-3PL</ta>
            <ta e="T244" id="Seg_4791" s="T243">Chef-PL.[NOM]</ta>
            <ta e="T245" id="Seg_4792" s="T244">nur</ta>
            <ta e="T246" id="Seg_4793" s="T245">dieses</ta>
            <ta e="T247" id="Seg_4794" s="T246">Kind-ACC</ta>
            <ta e="T248" id="Seg_4795" s="T247">MOD</ta>
            <ta e="T249" id="Seg_4796" s="T248">Popigaj-DAT/LOC</ta>
            <ta e="T250" id="Seg_4797" s="T249">schicken-PTCP.FUT-DAT/LOC</ta>
            <ta e="T251" id="Seg_4798" s="T250">sagen-PRS-3PL</ta>
            <ta e="T253" id="Seg_4799" s="T251">Verwandter-3SG-DAT/LOC</ta>
            <ta e="T257" id="Seg_4800" s="T255">mm</ta>
            <ta e="T258" id="Seg_4801" s="T257">Katja-ACC</ta>
            <ta e="T259" id="Seg_4802" s="T258">eins</ta>
            <ta e="T260" id="Seg_4803" s="T259">eins</ta>
            <ta e="T261" id="Seg_4804" s="T260">Kind-ACC</ta>
            <ta e="T262" id="Seg_4805" s="T261">MOD</ta>
            <ta e="T263" id="Seg_4806" s="T262">hier</ta>
            <ta e="T264" id="Seg_4807" s="T263">Tundra-DAT/LOC</ta>
            <ta e="T265" id="Seg_4808" s="T264">es.gibt</ta>
            <ta e="T266" id="Seg_4809" s="T265">Tundra</ta>
            <ta e="T267" id="Seg_4810" s="T266">alter.Mann-PROPR</ta>
            <ta e="T268" id="Seg_4811" s="T267">Alte.[NOM]</ta>
            <ta e="T269" id="Seg_4812" s="T268">Verwandter-1PL.[NOM]</ta>
            <ta e="T270" id="Seg_4813" s="T269">nur</ta>
            <ta e="T271" id="Seg_4814" s="T270">doch</ta>
            <ta e="T272" id="Seg_4815" s="T271">jenes-PL-DAT/LOC</ta>
            <ta e="T273" id="Seg_4816" s="T272">geben-PST1-3PL</ta>
            <ta e="T274" id="Seg_4817" s="T273">dann</ta>
            <ta e="T275" id="Seg_4818" s="T274">1SG.[NOM]</ta>
            <ta e="T276" id="Seg_4819" s="T275">zwei</ta>
            <ta e="T277" id="Seg_4820" s="T276">Junge.[NOM]</ta>
            <ta e="T278" id="Seg_4821" s="T277">Kind-ACC</ta>
            <ta e="T279" id="Seg_4822" s="T278">mit</ta>
            <ta e="T280" id="Seg_4823" s="T279">bleiben-PST1-1SG</ta>
            <ta e="T281" id="Seg_4824" s="T280">EMPH</ta>
            <ta e="T282" id="Seg_4825" s="T281">nur</ta>
            <ta e="T283" id="Seg_4826" s="T282">dieses-PL.[NOM]</ta>
            <ta e="T284" id="Seg_4827" s="T283">1SG-DAT/LOC</ta>
            <ta e="T285" id="Seg_4828" s="T284">gehen</ta>
            <ta e="T286" id="Seg_4829" s="T285">gehen-NEG-3PL</ta>
            <ta e="T287" id="Seg_4830" s="T286">zurück</ta>
            <ta e="T288" id="Seg_4831" s="T287">dieses.[NOM]</ta>
            <ta e="T289" id="Seg_4832" s="T288">zwei</ta>
            <ta e="T290" id="Seg_4833" s="T289">Junge.[NOM]</ta>
            <ta e="T291" id="Seg_4834" s="T290">Kind.[NOM]</ta>
            <ta e="T292" id="Seg_4835" s="T291">doch</ta>
            <ta e="T293" id="Seg_4836" s="T292">dann</ta>
            <ta e="T294" id="Seg_4837" s="T293">wie.viel</ta>
            <ta e="T295" id="Seg_4838" s="T294">INDEF</ta>
            <ta e="T296" id="Seg_4839" s="T295">sein-NEG.CVB.SIM</ta>
            <ta e="T297" id="Seg_4840" s="T296">Urlaub-DAT/LOC</ta>
            <ta e="T298" id="Seg_4841" s="T297">kommen-PST2-3PL</ta>
            <ta e="T299" id="Seg_4842" s="T298">jenes</ta>
            <ta e="T300" id="Seg_4843" s="T299">Kind-PL-EP-2SG.[NOM]</ta>
            <ta e="T301" id="Seg_4844" s="T300">EMPH</ta>
            <ta e="T302" id="Seg_4845" s="T301">zurück</ta>
            <ta e="T303" id="Seg_4846" s="T302">kommen-PST1-3PL</ta>
            <ta e="T304" id="Seg_4847" s="T303">1PL-DAT/LOC</ta>
            <ta e="T305" id="Seg_4848" s="T304">jenes</ta>
            <ta e="T306" id="Seg_4849" s="T305">zufrieden.sein-NEG-3PL</ta>
            <ta e="T307" id="Seg_4850" s="T306">sein-PST2.[3SG]</ta>
            <ta e="T308" id="Seg_4851" s="T307">jenes-PL-ACC</ta>
            <ta e="T309" id="Seg_4852" s="T308">dann</ta>
            <ta e="T310" id="Seg_4853" s="T309">Tundra-DAT/LOC</ta>
            <ta e="T311" id="Seg_4854" s="T310">alter.Mann-PROPR</ta>
            <ta e="T312" id="Seg_4855" s="T311">Alte.[NOM]</ta>
            <ta e="T313" id="Seg_4856" s="T312">wieder</ta>
            <ta e="T314" id="Seg_4857" s="T313">jenes</ta>
            <ta e="T315" id="Seg_4858" s="T314">gehen-PST2.[3SG]</ta>
            <ta e="T316" id="Seg_4859" s="T315">zwei</ta>
            <ta e="T317" id="Seg_4860" s="T316">Kind-1PL.[NOM]</ta>
            <ta e="T318" id="Seg_4861" s="T317">wieder</ta>
            <ta e="T319" id="Seg_4862" s="T318">wieder</ta>
            <ta e="T320" id="Seg_4863" s="T319">zurück</ta>
            <ta e="T321" id="Seg_4864" s="T320">kommen-PST1-3SG</ta>
            <ta e="T322" id="Seg_4865" s="T321">EMPH</ta>
            <ta e="T323" id="Seg_4866" s="T322">dann</ta>
            <ta e="T324" id="Seg_4867" s="T323">acht</ta>
            <ta e="T325" id="Seg_4868" s="T324">Kind-PROPR.[NOM]</ta>
            <ta e="T326" id="Seg_4869" s="T325">werden-PST1-1SG</ta>
            <ta e="T327" id="Seg_4870" s="T326">EMPH</ta>
            <ta e="T328" id="Seg_4871" s="T327">1SG.[NOM]</ta>
            <ta e="T330" id="Seg_4872" s="T329">selbst-1SG.[NOM]</ta>
            <ta e="T331" id="Seg_4873" s="T330">vier</ta>
            <ta e="T332" id="Seg_4874" s="T331">Kind-PROPR-1SG</ta>
            <ta e="T333" id="Seg_4875" s="T332">MOD</ta>
            <ta e="T334" id="Seg_4876" s="T333">dieses.[NOM]</ta>
            <ta e="T335" id="Seg_4877" s="T334">zwei</ta>
            <ta e="T336" id="Seg_4878" s="T335">Mädchen-PROPR-1SG</ta>
            <ta e="T337" id="Seg_4879" s="T336">MOD</ta>
            <ta e="T338" id="Seg_4880" s="T337">Mutter-1SG.[NOM]</ta>
            <ta e="T339" id="Seg_4881" s="T338">alt.[NOM]</ta>
            <ta e="T340" id="Seg_4882" s="T339">älterer.Bruder-EP-1SG.[NOM]</ta>
            <ta e="T341" id="Seg_4883" s="T340">alt.[NOM]</ta>
            <ta e="T342" id="Seg_4884" s="T341">dann</ta>
            <ta e="T343" id="Seg_4885" s="T342">jenes</ta>
            <ta e="T344" id="Seg_4886" s="T343">acht</ta>
            <ta e="T345" id="Seg_4887" s="T344">Kind-PROPR-1SG</ta>
            <ta e="T346" id="Seg_4888" s="T345">selbst-1SG.[NOM]</ta>
            <ta e="T347" id="Seg_4889" s="T346">ältere.Schwester-EP-1SG.[NOM]</ta>
            <ta e="T348" id="Seg_4890" s="T347">eigen-3SG</ta>
            <ta e="T349" id="Seg_4891" s="T348">vier</ta>
            <ta e="T350" id="Seg_4892" s="T349">Kind.[NOM]</ta>
            <ta e="T351" id="Seg_4893" s="T350">bleiben-PST1-3SG</ta>
            <ta e="T352" id="Seg_4894" s="T351">mein</ta>
            <ta e="T353" id="Seg_4895" s="T352">vier</ta>
            <ta e="T354" id="Seg_4896" s="T353">Kind.[NOM]</ta>
            <ta e="T355" id="Seg_4897" s="T354">MOD</ta>
            <ta e="T360" id="Seg_4898" s="T359">klein.[NOM]</ta>
            <ta e="T361" id="Seg_4899" s="T360">sein-PST1-3SG</ta>
            <ta e="T362" id="Seg_4900" s="T361">jenes</ta>
            <ta e="T363" id="Seg_4901" s="T362">doch</ta>
            <ta e="T364" id="Seg_4902" s="T363">jenes</ta>
            <ta e="T365" id="Seg_4903" s="T364">bleiben-CVB.SEQ-1SG</ta>
            <ta e="T366" id="Seg_4904" s="T365">doch</ta>
            <ta e="T367" id="Seg_4905" s="T366">dieses.[NOM]</ta>
            <ta e="T368" id="Seg_4906" s="T367">leben-PST1-1PL</ta>
            <ta e="T369" id="Seg_4907" s="T368">MOD</ta>
            <ta e="T370" id="Seg_4908" s="T369">Mutter-1SG.[NOM]</ta>
            <ta e="T371" id="Seg_4909" s="T370">altern-CVB.SEQ</ta>
            <ta e="T372" id="Seg_4910" s="T371">sterben-PST1-3SG</ta>
            <ta e="T373" id="Seg_4911" s="T372">krank.sein-CVB.SEQ</ta>
            <ta e="T374" id="Seg_4912" s="T373">jenes</ta>
            <ta e="T375" id="Seg_4913" s="T374">älterer.Bruder-EP-1SG.[NOM]</ta>
            <ta e="T376" id="Seg_4914" s="T375">wieder</ta>
            <ta e="T377" id="Seg_4915" s="T376">Frau-POSS</ta>
            <ta e="T378" id="Seg_4916" s="T377">NEG</ta>
            <ta e="T379" id="Seg_4917" s="T378">NEG</ta>
            <ta e="T380" id="Seg_4918" s="T379">was.[NOM]</ta>
            <ta e="T381" id="Seg_4919" s="T380">NEG</ta>
            <ta e="T382" id="Seg_4920" s="T381">NEG</ta>
            <ta e="T383" id="Seg_4921" s="T382">sein-PST1-3SG</ta>
            <ta e="T384" id="Seg_4922" s="T383">Laden-DAT/LOC</ta>
            <ta e="T385" id="Seg_4923" s="T384">arbeiten-PTCP.HAB</ta>
            <ta e="T386" id="Seg_4924" s="T385">sein-PST1-3SG</ta>
            <ta e="T387" id="Seg_4925" s="T386">jenes.[NOM]</ta>
            <ta e="T388" id="Seg_4926" s="T387">wegen</ta>
            <ta e="T389" id="Seg_4927" s="T388">Nahrung-EP-INSTR</ta>
            <ta e="T390" id="Seg_4928" s="T389">NEG</ta>
            <ta e="T391" id="Seg_4929" s="T390">sehr</ta>
            <ta e="T392" id="Seg_4930" s="T391">NEG</ta>
            <ta e="T393" id="Seg_4931" s="T392">wer-VBZ-PTCP.HAB-3SG</ta>
            <ta e="T394" id="Seg_4932" s="T393">NEG</ta>
            <ta e="T395" id="Seg_4933" s="T394">sein-PST1-1PL</ta>
            <ta e="T396" id="Seg_4934" s="T395">arbeiten-PTCP.PRS</ta>
            <ta e="T397" id="Seg_4935" s="T396">sein-CVB.SEQ</ta>
            <ta e="T399" id="Seg_4936" s="T398">holen-CVB.SEQ</ta>
            <ta e="T400" id="Seg_4937" s="T399">gehen-HAB.[3SG]</ta>
            <ta e="T401" id="Seg_4938" s="T400">Nahrung.[NOM]</ta>
            <ta e="T402" id="Seg_4939" s="T401">Laden-ABL</ta>
            <ta e="T403" id="Seg_4940" s="T402">Lohn-DIM.[NOM]</ta>
            <ta e="T404" id="Seg_4941" s="T403">dieses.[NOM]</ta>
            <ta e="T405" id="Seg_4942" s="T404">bekommen-HAB.[3SG]</ta>
            <ta e="T406" id="Seg_4943" s="T405">dann</ta>
            <ta e="T407" id="Seg_4944" s="T406">1SG.[NOM]</ta>
            <ta e="T408" id="Seg_4945" s="T407">Koch-VBZ-PRS-1SG</ta>
            <ta e="T409" id="Seg_4946" s="T408">MOD</ta>
            <ta e="T410" id="Seg_4947" s="T409">jenes</ta>
            <ta e="T411" id="Seg_4948" s="T410">dieses.[NOM]</ta>
            <ta e="T412" id="Seg_4949" s="T411">leben-PST2-1PL</ta>
            <ta e="T413" id="Seg_4950" s="T412">jeder-INTNS-3PL.[NOM]</ta>
            <ta e="T414" id="Seg_4951" s="T413">wachsen-PST2-3PL</ta>
            <ta e="T415" id="Seg_4952" s="T414">offenbar</ta>
            <ta e="T416" id="Seg_4953" s="T415">Frau-PROPR-3PL</ta>
            <ta e="T422" id="Seg_4954" s="T421">doch</ta>
            <ta e="T423" id="Seg_4955" s="T422">hinausgehen-EP-PST2-3SG</ta>
            <ta e="T424" id="Seg_4956" s="T423">was.[NOM]</ta>
            <ta e="T425" id="Seg_4957" s="T424">machen-CVB.PURP</ta>
            <ta e="T426" id="Seg_4958" s="T425">und</ta>
            <ta e="T427" id="Seg_4959" s="T426">hinausgehen-EP-PST2.[3SG]</ta>
            <ta e="T428" id="Seg_4960" s="T427">doch</ta>
            <ta e="T429" id="Seg_4961" s="T428">jenes.[NOM]</ta>
            <ta e="T430" id="Seg_4962" s="T429">wegen</ta>
            <ta e="T431" id="Seg_4963" s="T430">acht</ta>
            <ta e="T432" id="Seg_4964" s="T431">Kind.[NOM]</ta>
            <ta e="T433" id="Seg_4965" s="T432">sein-PRS.[3SG]</ta>
            <ta e="T434" id="Seg_4966" s="T433">MOD</ta>
            <ta e="T435" id="Seg_4967" s="T434">jung.[NOM]</ta>
            <ta e="T436" id="Seg_4968" s="T435">Frau-1SG</ta>
            <ta e="T437" id="Seg_4969" s="T436">EMPH</ta>
            <ta e="T438" id="Seg_4970" s="T437">dort</ta>
            <ta e="T439" id="Seg_4971" s="T438">jenes</ta>
            <ta e="T443" id="Seg_4972" s="T442">nun</ta>
            <ta e="T444" id="Seg_4973" s="T443">Ehemann-DAT/LOC</ta>
            <ta e="T445" id="Seg_4974" s="T444">hinausgehen-CVB.SEQ</ta>
            <ta e="T446" id="Seg_4975" s="T445">bleiben-PST1-1SG</ta>
            <ta e="T447" id="Seg_4976" s="T446">EMPH</ta>
            <ta e="T448" id="Seg_4977" s="T447">zwei</ta>
            <ta e="T449" id="Seg_4978" s="T448">Kind-ACC</ta>
            <ta e="T450" id="Seg_4979" s="T449">geboren.werden-PTCP.PST</ta>
            <ta e="T451" id="Seg_4980" s="T450">MOD</ta>
            <ta e="T452" id="Seg_4981" s="T451">jenes</ta>
            <ta e="T453" id="Seg_4982" s="T452">drei-ORD</ta>
            <ta e="T454" id="Seg_4983" s="T453">drei-ORD</ta>
            <ta e="T455" id="Seg_4984" s="T454">Kind-1SG.[NOM]</ta>
            <ta e="T456" id="Seg_4985" s="T455">Junge.[NOM]</ta>
            <ta e="T457" id="Seg_4986" s="T456">Kind.[NOM]</ta>
            <ta e="T458" id="Seg_4987" s="T457">sein-PST1-3SG</ta>
            <ta e="T459" id="Seg_4988" s="T458">jenes-3SG-1SG.[NOM]</ta>
            <ta e="T460" id="Seg_4989" s="T459">fallen-PST2-3SG</ta>
            <ta e="T461" id="Seg_4990" s="T460">töten-EP-MED-CVB.SEQ-1SG</ta>
            <ta e="T463" id="Seg_4991" s="T462">Kindergarten-DAT/LOC</ta>
            <ta e="T464" id="Seg_4992" s="T463">Eis-PL-ACC</ta>
            <ta e="T465" id="Seg_4993" s="T464">Kohle-PL-ACC</ta>
            <ta e="T466" id="Seg_4994" s="T465">ziehen-VBZ-CVB.SEQ</ta>
            <ta e="T467" id="Seg_4995" s="T466">jenes</ta>
            <ta e="T468" id="Seg_4996" s="T467">fallen-PTCP.PST</ta>
            <ta e="T469" id="Seg_4997" s="T468">sein-PST1-3SG</ta>
            <ta e="T470" id="Seg_4998" s="T469">dieses.[NOM]</ta>
            <ta e="T471" id="Seg_4999" s="T470">zwei</ta>
            <ta e="T472" id="Seg_5000" s="T471">Junge.[NOM]</ta>
            <ta e="T473" id="Seg_5001" s="T472">Zwischenraum-3SG.[NOM]</ta>
            <ta e="T481" id="Seg_5002" s="T480">Elena.[NOM]</ta>
            <ta e="T482" id="Seg_5003" s="T481">sechs</ta>
            <ta e="T483" id="Seg_5004" s="T482">Jahr-PROPR-ACC</ta>
            <ta e="T484" id="Seg_5005" s="T483">bekommen-PST2-PST2</ta>
            <ta e="T485" id="Seg_5006" s="T484">dann</ta>
            <ta e="T486" id="Seg_5007" s="T485">sieben</ta>
            <ta e="T487" id="Seg_5008" s="T486">Jahr-VBZ-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T488" id="Seg_5009" s="T487">hierher</ta>
            <ta e="T489" id="Seg_5010" s="T488">kommen-PST1-1SG</ta>
            <ta e="T490" id="Seg_5011" s="T489">MOD</ta>
            <ta e="T491" id="Seg_5012" s="T490">Dudinka-DAT/LOC</ta>
            <ta e="T492" id="Seg_5013" s="T491">Kind-1SG.[NOM]</ta>
            <ta e="T493" id="Seg_5014" s="T492">Haus.[NOM]</ta>
            <ta e="T494" id="Seg_5015" s="T493">bekommen-PTCP.PRS</ta>
            <ta e="T495" id="Seg_5016" s="T494">sein-PST2.[3SG]</ta>
            <ta e="T496" id="Seg_5017" s="T495">nur</ta>
            <ta e="T497" id="Seg_5018" s="T496">doch</ta>
            <ta e="T498" id="Seg_5019" s="T497">jenes-DAT/LOC</ta>
            <ta e="T499" id="Seg_5020" s="T498">Wohnerlaubnis-VBZ-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T500" id="Seg_5021" s="T499">man.muss</ta>
            <ta e="T501" id="Seg_5022" s="T500">sein-PST2.[3SG]</ta>
            <ta e="T502" id="Seg_5023" s="T501">doch</ta>
            <ta e="T503" id="Seg_5024" s="T502">jenes</ta>
            <ta e="T504" id="Seg_5025" s="T503">kommen-PST2-EP-1SG</ta>
            <ta e="T505" id="Seg_5026" s="T504">jenes</ta>
            <ta e="T506" id="Seg_5027" s="T505">kommen-CVB.SEQ-1SG</ta>
            <ta e="T507" id="Seg_5028" s="T506">EMPH</ta>
            <ta e="T508" id="Seg_5029" s="T507">Kind-1SG.[NOM]</ta>
            <ta e="T509" id="Seg_5030" s="T508">zehn-ORD</ta>
            <ta e="T510" id="Seg_5031" s="T509">Klasse.[NOM]</ta>
            <ta e="T511" id="Seg_5032" s="T510">sein-PST1-3SG</ta>
            <ta e="T513" id="Seg_5033" s="T512">so</ta>
            <ta e="T514" id="Seg_5034" s="T513">Mama-3SG.[NOM]</ta>
            <ta e="T515" id="Seg_5035" s="T514">aber</ta>
            <ta e="T516" id="Seg_5036" s="T515">Jakutsk-DAT/LOC</ta>
            <ta e="T517" id="Seg_5037" s="T516">Ehemann-VBZ-PST2-3SG</ta>
            <ta e="T518" id="Seg_5038" s="T517">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T519" id="Seg_5039" s="T518">jetzt</ta>
            <ta e="T520" id="Seg_5040" s="T519">zwei</ta>
            <ta e="T521" id="Seg_5041" s="T520">Kind-PROPR.[NOM]</ta>
            <ta e="T522" id="Seg_5042" s="T521">Junge-ACC</ta>
            <ta e="T523" id="Seg_5043" s="T522">mit</ta>
            <ta e="T524" id="Seg_5044" s="T523">Mädchen.[NOM]</ta>
            <ta e="T529" id="Seg_5045" s="T528">Tundra-DAT/LOC</ta>
            <ta e="T530" id="Seg_5046" s="T529">Tundra-DAT/LOC</ta>
            <ta e="T531" id="Seg_5047" s="T530">leben-PRS-3PL</ta>
            <ta e="T532" id="Seg_5048" s="T531">Tundra-DAT/LOC</ta>
            <ta e="T533" id="Seg_5049" s="T532">Rentier-AG.[NOM]</ta>
            <ta e="T534" id="Seg_5050" s="T533">Ehemann-3SG.[NOM]</ta>
            <ta e="T536" id="Seg_5051" s="T535">letzter-3SG.[NOM]</ta>
            <ta e="T537" id="Seg_5052" s="T536">Syndassko-DAT/LOC</ta>
            <ta e="T538" id="Seg_5053" s="T537">bleiben-PST2-3PL</ta>
            <ta e="T539" id="Seg_5054" s="T538">jetzt</ta>
            <ta e="T540" id="Seg_5055" s="T539">gehen-FUT-3PL</ta>
            <ta e="T541" id="Seg_5056" s="T540">Mai</ta>
            <ta e="T542" id="Seg_5057" s="T541">Monat-DAT/LOC</ta>
            <ta e="T543" id="Seg_5058" s="T542">Schule.[NOM]</ta>
            <ta e="T544" id="Seg_5059" s="T543">aufhören-PASS-CVB.SEQ</ta>
            <ta e="T545" id="Seg_5060" s="T544">Kind-3PL.[NOM]</ta>
            <ta e="T546" id="Seg_5061" s="T545">dort</ta>
            <ta e="T547" id="Seg_5062" s="T546">lernen-PRS-3PL</ta>
            <ta e="T548" id="Seg_5063" s="T547">Syndassko-DAT/LOC</ta>
            <ta e="T549" id="Seg_5064" s="T548">einer.von.zwei-3SG.[NOM]</ta>
            <ta e="T550" id="Seg_5065" s="T549">vier-ORD</ta>
            <ta e="T551" id="Seg_5066" s="T550">Klasse.[NOM]</ta>
            <ta e="T552" id="Seg_5067" s="T551">einer.von.zwei-3SG.[NOM]</ta>
            <ta e="T553" id="Seg_5068" s="T552">drei-ORD</ta>
            <ta e="T554" id="Seg_5069" s="T553">Klasse.[NOM]</ta>
            <ta e="T555" id="Seg_5070" s="T554">Q</ta>
            <ta e="T556" id="Seg_5071" s="T555">zwei-ORD</ta>
            <ta e="T557" id="Seg_5072" s="T556">Q</ta>
            <ta e="T559" id="Seg_5073" s="T558">zwei-ORD</ta>
            <ta e="T560" id="Seg_5074" s="T559">Klasse.[NOM]</ta>
            <ta e="T561" id="Seg_5075" s="T560">äh</ta>
            <ta e="T562" id="Seg_5076" s="T561">Tochter-3SG.[NOM]</ta>
            <ta e="T563" id="Seg_5077" s="T562">jetzt</ta>
            <ta e="T564" id="Seg_5078" s="T563">1SG.[NOM]</ta>
            <ta e="T565" id="Seg_5079" s="T564">Haus-1SG-DAT/LOC</ta>
            <ta e="T566" id="Seg_5080" s="T565">bleiben-PST2-3PL</ta>
            <ta e="T567" id="Seg_5081" s="T566">jetzt</ta>
            <ta e="T568" id="Seg_5082" s="T567">Mai-DAT/LOC</ta>
            <ta e="T569" id="Seg_5083" s="T568">gehen-FUT-3PL</ta>
            <ta e="T570" id="Seg_5084" s="T569">jetzt</ta>
            <ta e="T572" id="Seg_5085" s="T571">nomadisieren-CVB.SEQ</ta>
            <ta e="T573" id="Seg_5086" s="T572">gehen-FUT-3PL</ta>
            <ta e="T574" id="Seg_5087" s="T573">letzter-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-KiES">
            <ta e="T2" id="Seg_5088" s="T1">вот</ta>
            <ta e="T3" id="Seg_5089" s="T2">Корго-DAT/LOC</ta>
            <ta e="T4" id="Seg_5090" s="T3">есть</ta>
            <ta e="T5" id="Seg_5091" s="T4">быть-PST1-1PL</ta>
            <ta e="T6" id="Seg_5092" s="T5">земля-PROPR.[NOM]</ta>
            <ta e="T7" id="Seg_5093" s="T6">быть-PST1-1PL</ta>
            <ta e="T8" id="Seg_5094" s="T7">3PL-DAT/LOC</ta>
            <ta e="T9" id="Seg_5095" s="T8">Корго-DAT/LOC</ta>
            <ta e="T11" id="Seg_5096" s="T9">земля-VBZ-PST-EP-1SG</ta>
            <ta e="T12" id="Seg_5097" s="T11">потом</ta>
            <ta e="T13" id="Seg_5098" s="T12">однако</ta>
            <ta e="T14" id="Seg_5099" s="T13">этот</ta>
            <ta e="T15" id="Seg_5100" s="T14">ээ</ta>
            <ta e="T16" id="Seg_5101" s="T15">этот</ta>
            <ta e="T17" id="Seg_5102" s="T16">ребенок-ACC</ta>
            <ta e="T18" id="Seg_5103" s="T17">родить-PST2-EP-1SG</ta>
            <ta e="T19" id="Seg_5104" s="T18">потом</ta>
            <ta e="T20" id="Seg_5105" s="T19">тот</ta>
            <ta e="T21" id="Seg_5106" s="T20">мальчик.[NOM]</ta>
            <ta e="T22" id="Seg_5107" s="T21">ребенок-1SG.[NOM]</ta>
            <ta e="T23" id="Seg_5108" s="T22">родить-CVB.SEQ-1SG</ta>
            <ta e="T24" id="Seg_5109" s="T23">тот</ta>
            <ta e="T25" id="Seg_5110" s="T24">потом</ta>
            <ta e="T26" id="Seg_5111" s="T25">однако</ta>
            <ta e="T27" id="Seg_5112" s="T26">старик-1PL.[NOM]</ta>
            <ta e="T28" id="Seg_5113" s="T27">быть.больным-PTCP.PRS</ta>
            <ta e="T29" id="Seg_5114" s="T28">быть-PST1-3SG</ta>
            <ta e="T30" id="Seg_5115" s="T29">умирать-CVB.SEQ</ta>
            <ta e="T31" id="Seg_5116" s="T30">умирать-CVB.SEQ</ta>
            <ta e="T32" id="Seg_5117" s="T31">тот</ta>
            <ta e="T33" id="Seg_5118" s="T32">кто-DAT/LOC</ta>
            <ta e="T34" id="Seg_5119" s="T33">Сындасско-DAT/LOC</ta>
            <ta e="T35" id="Seg_5120" s="T34">ложиться-PST2-1PL</ta>
            <ta e="T36" id="Seg_5121" s="T35">тот</ta>
            <ta e="T37" id="Seg_5122" s="T36">потом</ta>
            <ta e="T38" id="Seg_5123" s="T37">работа-1SG.[NOM]</ta>
            <ta e="T39" id="Seg_5124" s="T38">1SG-ACC</ta>
            <ta e="T43" id="Seg_5125" s="T42">этот</ta>
            <ta e="T44" id="Seg_5126" s="T43">вот</ta>
            <ta e="T48" id="Seg_5127" s="T47">потом</ta>
            <ta e="T49" id="Seg_5128" s="T48">работа.[NOM]</ta>
            <ta e="T50" id="Seg_5129" s="T49">давать-PST2-3PL</ta>
            <ta e="T51" id="Seg_5130" s="T50">1SG-DAT/LOC</ta>
            <ta e="T52" id="Seg_5131" s="T51">садик-DAT/LOC</ta>
            <ta e="T64" id="Seg_5132" s="T63">AFFIRM</ta>
            <ta e="T65" id="Seg_5133" s="T64">долганский-SIM</ta>
            <ta e="T66" id="Seg_5134" s="T65">MOD</ta>
            <ta e="T67" id="Seg_5135" s="T66">Сындасско-DAT/LOC</ta>
            <ta e="T68" id="Seg_5136" s="T67">долганский</ta>
            <ta e="T69" id="Seg_5137" s="T68">впервые</ta>
            <ta e="T70" id="Seg_5138" s="T69">садик.[NOM]</ta>
            <ta e="T71" id="Seg_5139" s="T70">стоять-PTCP.PST</ta>
            <ta e="T72" id="Seg_5140" s="T71">быть-PST1-3SG</ta>
            <ta e="T73" id="Seg_5141" s="T72">вот</ta>
            <ta e="T74" id="Seg_5142" s="T73">что-ACC</ta>
            <ta e="T75" id="Seg_5143" s="T74">NEG</ta>
            <ta e="T76" id="Seg_5144" s="T75">знать-NEG-1SG</ta>
            <ta e="T77" id="Seg_5145" s="T76">садик.[NOM]</ta>
            <ta e="T78" id="Seg_5146" s="T77">что.[NOM]</ta>
            <ta e="T79" id="Seg_5147" s="T78">имя-3SG.[NOM]=Q</ta>
            <ta e="T92" id="Seg_5148" s="T91">о</ta>
            <ta e="T94" id="Seg_5149" s="T93">вот</ta>
            <ta e="T95" id="Seg_5150" s="T94">как</ta>
            <ta e="T96" id="Seg_5151" s="T95">быть-FUT-1PL=Q</ta>
            <ta e="T98" id="Seg_5152" s="T97">мочь-CVB.SEQ</ta>
            <ta e="T99" id="Seg_5153" s="T98">говорить-NEG-1SG</ta>
            <ta e="T100" id="Seg_5154" s="T99">ребенок-PL-DAT/LOC</ta>
            <ta e="T101" id="Seg_5155" s="T100">вот</ta>
            <ta e="T102" id="Seg_5156" s="T101">ребенок-PL-DAT/LOC</ta>
            <ta e="T103" id="Seg_5157" s="T102">мочь-CVB.SEQ</ta>
            <ta e="T104" id="Seg_5158" s="T103">вариться-EP-CAUS-NEG-1SG</ta>
            <ta e="T105" id="Seg_5159" s="T104">говорить-PRS-1SG</ta>
            <ta e="T106" id="Seg_5160" s="T105">NEG.EX</ta>
            <ta e="T107" id="Seg_5161" s="T106">повар-PL.[NOM]</ta>
            <ta e="T108" id="Seg_5162" s="T107">говорить-PRS-3PL</ta>
            <ta e="T109" id="Seg_5163" s="T108">EMPH</ta>
            <ta e="T110" id="Seg_5164" s="T109">этот</ta>
            <ta e="T111" id="Seg_5165" s="T110">садик.[NOM]</ta>
            <ta e="T112" id="Seg_5166" s="T111">стоять-PST1-3SG</ta>
            <ta e="T113" id="Seg_5167" s="T112">новый</ta>
            <ta e="T114" id="Seg_5168" s="T113">садик-DAT/LOC</ta>
            <ta e="T115" id="Seg_5169" s="T114">тот-DAT/LOC</ta>
            <ta e="T116" id="Seg_5170" s="T115">повар.[NOM]</ta>
            <ta e="T117" id="Seg_5171" s="T116">повар.[NOM]</ta>
            <ta e="T118" id="Seg_5172" s="T117">надо</ta>
            <ta e="T119" id="Seg_5173" s="T118">говорить-PRS-3PL</ta>
            <ta e="T120" id="Seg_5174" s="T119">начальник-PL.[NOM]</ta>
            <ta e="T121" id="Seg_5175" s="T120">так</ta>
            <ta e="T122" id="Seg_5176" s="T121">начальник-PL.[NOM]</ta>
            <ta e="T123" id="Seg_5177" s="T122">1SG-ACC</ta>
            <ta e="T124" id="Seg_5178" s="T123">повар.[NOM]</ta>
            <ta e="T125" id="Seg_5179" s="T124">класть-PST1-3PL</ta>
            <ta e="T126" id="Seg_5180" s="T125">MOD</ta>
            <ta e="T127" id="Seg_5181" s="T126">вот</ta>
            <ta e="T128" id="Seg_5182" s="T127">тот</ta>
            <ta e="T129" id="Seg_5183" s="T128">лежать-PST2-1PL</ta>
            <ta e="T130" id="Seg_5184" s="T129">потом</ta>
            <ta e="T131" id="Seg_5185" s="T130">тот</ta>
            <ta e="T132" id="Seg_5186" s="T131">повар-VBZ-CVB.SEQ-1SG</ta>
            <ta e="T133" id="Seg_5187" s="T132">однако</ta>
            <ta e="T134" id="Seg_5188" s="T133">ээ</ta>
            <ta e="T135" id="Seg_5189" s="T134">кто-DAT/LOC</ta>
            <ta e="T136" id="Seg_5190" s="T135">этот</ta>
            <ta e="T137" id="Seg_5191" s="T136">вот</ta>
            <ta e="T138" id="Seg_5192" s="T137">тот.[NOM]</ta>
            <ta e="T139" id="Seg_5193" s="T138">два</ta>
            <ta e="T140" id="Seg_5194" s="T139">ребенок-1SG.[NOM]</ta>
            <ta e="T141" id="Seg_5195" s="T140">идти-PRS-3PL</ta>
            <ta e="T142" id="Seg_5196" s="T141">садик-DAT/LOC</ta>
            <ta e="T143" id="Seg_5197" s="T142">только</ta>
            <ta e="T144" id="Seg_5198" s="T143">AFFIRM</ta>
            <ta e="T145" id="Seg_5199" s="T144">вот</ta>
            <ta e="T146" id="Seg_5200" s="T145">тот</ta>
            <ta e="T147" id="Seg_5201" s="T146">идти-CVB.SEQ-3PL</ta>
            <ta e="T151" id="Seg_5202" s="T150">кто-ACC</ta>
            <ta e="T152" id="Seg_5203" s="T151">с</ta>
            <ta e="T157" id="Seg_5204" s="T156">почему</ta>
            <ta e="T158" id="Seg_5205" s="T157">тот</ta>
            <ta e="T159" id="Seg_5206" s="T158">потом</ta>
            <ta e="T160" id="Seg_5207" s="T159">тот</ta>
            <ta e="T161" id="Seg_5208" s="T160">позже</ta>
            <ta e="T162" id="Seg_5209" s="T161">тот</ta>
            <ta e="T163" id="Seg_5210" s="T162">кто-EP-1SG.[NOM]</ta>
            <ta e="T164" id="Seg_5211" s="T163">тот</ta>
            <ta e="T165" id="Seg_5212" s="T164">старшая.сестра-EP-1SG.[NOM]</ta>
            <ta e="T166" id="Seg_5213" s="T165">умирать-PST2-3SG</ta>
            <ta e="T171" id="Seg_5214" s="T168">вот</ta>
            <ta e="T172" id="Seg_5215" s="T171">мать-1SG.[NOM]</ta>
            <ta e="T173" id="Seg_5216" s="T172">мать-1SG.[NOM]</ta>
            <ta e="T174" id="Seg_5217" s="T173">младшая.сестра-EP-3SG.[NOM]</ta>
            <ta e="T175" id="Seg_5218" s="T174">умирать-PST2-3SG</ta>
            <ta e="T176" id="Seg_5219" s="T175">быть.больным-CVB.SEQ</ta>
            <ta e="T177" id="Seg_5220" s="T176">тогда</ta>
            <ta e="T178" id="Seg_5221" s="T177">четыре</ta>
            <ta e="T179" id="Seg_5222" s="T178">ребенок-PROPR.[NOM]</ta>
            <ta e="T180" id="Seg_5223" s="T179">оставаться-PST1-3SG</ta>
            <ta e="T181" id="Seg_5224" s="T180">этот</ta>
            <ta e="T182" id="Seg_5225" s="T181">человек.[NOM]</ta>
            <ta e="T183" id="Seg_5226" s="T182">четыре</ta>
            <ta e="T184" id="Seg_5227" s="T183">ребенок.[NOM]</ta>
            <ta e="T185" id="Seg_5228" s="T184">MOD</ta>
            <ta e="T186" id="Seg_5229" s="T185">1SG-DAT/LOC</ta>
            <ta e="T187" id="Seg_5230" s="T186">приходить-PST1-3PL</ta>
            <ta e="T188" id="Seg_5231" s="T187">MOD</ta>
            <ta e="T189" id="Seg_5232" s="T188">EMPH</ta>
            <ta e="T190" id="Seg_5233" s="T189">старшая.сестра-1PL-DAT/LOC</ta>
            <ta e="T191" id="Seg_5234" s="T190">быть-FUT-1PL</ta>
            <ta e="T192" id="Seg_5235" s="T191">1PL.[NOM]</ta>
            <ta e="T193" id="Seg_5236" s="T192">куда</ta>
            <ta e="T194" id="Seg_5237" s="T193">NEG</ta>
            <ta e="T195" id="Seg_5238" s="T194">идти-NEG-1PL</ta>
            <ta e="T196" id="Seg_5239" s="T195">говорить-PRS-3PL</ta>
            <ta e="T197" id="Seg_5240" s="T196">мать-1SG.[NOM]</ta>
            <ta e="T198" id="Seg_5241" s="T197">ээ</ta>
            <ta e="T199" id="Seg_5242" s="T198">мать</ta>
            <ta e="T200" id="Seg_5243" s="T199">мать-3SG.[NOM]</ta>
            <ta e="T201" id="Seg_5244" s="T200">память.[NOM]</ta>
            <ta e="T202" id="Seg_5245" s="T201">говорить-PST2.[3SG]</ta>
            <ta e="T203" id="Seg_5246" s="T202">MOD</ta>
            <ta e="T204" id="Seg_5247" s="T203">этот</ta>
            <ta e="T205" id="Seg_5248" s="T204">вот</ta>
            <ta e="T206" id="Seg_5249" s="T205">старшая.сестра-2PL-DAT/LOC</ta>
            <ta e="T207" id="Seg_5250" s="T206">оставаться-CVB.SEQ</ta>
            <ta e="T208" id="Seg_5251" s="T207">говорить-PST2.[3SG]</ta>
            <ta e="T209" id="Seg_5252" s="T208">MOD</ta>
            <ta e="T210" id="Seg_5253" s="T209">1SG.[NOM]</ta>
            <ta e="T211" id="Seg_5254" s="T210">как</ta>
            <ta e="T212" id="Seg_5255" s="T211">быть-TEMP-1SG</ta>
            <ta e="T213" id="Seg_5256" s="T212">вот</ta>
            <ta e="T214" id="Seg_5257" s="T213">тот</ta>
            <ta e="T215" id="Seg_5258" s="T214">тот</ta>
            <ta e="T216" id="Seg_5259" s="T215">четыре</ta>
            <ta e="T217" id="Seg_5260" s="T216">четыре</ta>
            <ta e="T218" id="Seg_5261" s="T217">ребенок.[NOM]</ta>
            <ta e="T219" id="Seg_5262" s="T218">1PL-DAT/LOC</ta>
            <ta e="T220" id="Seg_5263" s="T219">приходить-PST1-3PL</ta>
            <ta e="T221" id="Seg_5264" s="T220">только</ta>
            <ta e="T222" id="Seg_5265" s="T221">мм</ta>
            <ta e="T223" id="Seg_5266" s="T222">Хатанга.[NOM]</ta>
            <ta e="T224" id="Seg_5267" s="T223">учиться-CVB.SIM</ta>
            <ta e="T225" id="Seg_5268" s="T224">идти-PRS-3PL</ta>
            <ta e="T231" id="Seg_5269" s="T230">вот</ta>
            <ta e="T232" id="Seg_5270" s="T231">INTJ</ta>
            <ta e="T233" id="Seg_5271" s="T232">потом</ta>
            <ta e="T237" id="Seg_5272" s="T236">вот</ta>
            <ta e="T238" id="Seg_5273" s="T237">потом</ta>
            <ta e="T239" id="Seg_5274" s="T238">каникулы-DAT/LOC</ta>
            <ta e="T240" id="Seg_5275" s="T239">приходить-PRS-3PL</ta>
            <ta e="T241" id="Seg_5276" s="T240">этот</ta>
            <ta e="T242" id="Seg_5277" s="T241">ребенок-PL.[NOM]</ta>
            <ta e="T243" id="Seg_5278" s="T242">советовать-PRS-3PL</ta>
            <ta e="T244" id="Seg_5279" s="T243">начальник-PL.[NOM]</ta>
            <ta e="T245" id="Seg_5280" s="T244">только</ta>
            <ta e="T246" id="Seg_5281" s="T245">этот</ta>
            <ta e="T247" id="Seg_5282" s="T246">ребенок-ACC</ta>
            <ta e="T248" id="Seg_5283" s="T247">MOD</ta>
            <ta e="T249" id="Seg_5284" s="T248">Попигай-DAT/LOC</ta>
            <ta e="T250" id="Seg_5285" s="T249">послать-PTCP.FUT-DAT/LOC</ta>
            <ta e="T251" id="Seg_5286" s="T250">говорить-PRS-3PL</ta>
            <ta e="T253" id="Seg_5287" s="T251">родственник-3SG-DAT/LOC</ta>
            <ta e="T257" id="Seg_5288" s="T255">мм</ta>
            <ta e="T258" id="Seg_5289" s="T257">Катя-ACC</ta>
            <ta e="T259" id="Seg_5290" s="T258">один</ta>
            <ta e="T260" id="Seg_5291" s="T259">один</ta>
            <ta e="T261" id="Seg_5292" s="T260">ребенок-ACC</ta>
            <ta e="T262" id="Seg_5293" s="T261">MOD</ta>
            <ta e="T263" id="Seg_5294" s="T262">здесь</ta>
            <ta e="T264" id="Seg_5295" s="T263">тундра-DAT/LOC</ta>
            <ta e="T265" id="Seg_5296" s="T264">есть</ta>
            <ta e="T266" id="Seg_5297" s="T265">тундра</ta>
            <ta e="T267" id="Seg_5298" s="T266">старик-PROPR</ta>
            <ta e="T268" id="Seg_5299" s="T267">старуха.[NOM]</ta>
            <ta e="T269" id="Seg_5300" s="T268">родственник-1PL.[NOM]</ta>
            <ta e="T270" id="Seg_5301" s="T269">только</ta>
            <ta e="T271" id="Seg_5302" s="T270">вот</ta>
            <ta e="T272" id="Seg_5303" s="T271">тот-PL-DAT/LOC</ta>
            <ta e="T273" id="Seg_5304" s="T272">давать-PST1-3PL</ta>
            <ta e="T274" id="Seg_5305" s="T273">тогда</ta>
            <ta e="T275" id="Seg_5306" s="T274">1SG.[NOM]</ta>
            <ta e="T276" id="Seg_5307" s="T275">два</ta>
            <ta e="T277" id="Seg_5308" s="T276">мальчик.[NOM]</ta>
            <ta e="T278" id="Seg_5309" s="T277">ребенок-ACC</ta>
            <ta e="T279" id="Seg_5310" s="T278">с</ta>
            <ta e="T280" id="Seg_5311" s="T279">оставаться-PST1-1SG</ta>
            <ta e="T281" id="Seg_5312" s="T280">EMPH</ta>
            <ta e="T282" id="Seg_5313" s="T281">только</ta>
            <ta e="T283" id="Seg_5314" s="T282">тот-PL.[NOM]</ta>
            <ta e="T284" id="Seg_5315" s="T283">1SG-DAT/LOC</ta>
            <ta e="T285" id="Seg_5316" s="T284">идти</ta>
            <ta e="T286" id="Seg_5317" s="T285">идти-NEG-3PL</ta>
            <ta e="T287" id="Seg_5318" s="T286">назад</ta>
            <ta e="T288" id="Seg_5319" s="T287">тот.[NOM]</ta>
            <ta e="T289" id="Seg_5320" s="T288">два</ta>
            <ta e="T290" id="Seg_5321" s="T289">мальчик.[NOM]</ta>
            <ta e="T291" id="Seg_5322" s="T290">ребенок.[NOM]</ta>
            <ta e="T292" id="Seg_5323" s="T291">вот</ta>
            <ta e="T293" id="Seg_5324" s="T292">потом</ta>
            <ta e="T294" id="Seg_5325" s="T293">сколько</ta>
            <ta e="T295" id="Seg_5326" s="T294">INDEF</ta>
            <ta e="T296" id="Seg_5327" s="T295">быть-NEG.CVB.SIM</ta>
            <ta e="T297" id="Seg_5328" s="T296">каникулы-DAT/LOC</ta>
            <ta e="T298" id="Seg_5329" s="T297">приходить-PST2-3PL</ta>
            <ta e="T299" id="Seg_5330" s="T298">тот</ta>
            <ta e="T300" id="Seg_5331" s="T299">ребенок-PL-EP-2SG.[NOM]</ta>
            <ta e="T301" id="Seg_5332" s="T300">EMPH</ta>
            <ta e="T302" id="Seg_5333" s="T301">назад</ta>
            <ta e="T303" id="Seg_5334" s="T302">приходить-PST1-3PL</ta>
            <ta e="T304" id="Seg_5335" s="T303">1PL-DAT/LOC</ta>
            <ta e="T305" id="Seg_5336" s="T304">тот</ta>
            <ta e="T306" id="Seg_5337" s="T305">быть.довольиым-NEG-3PL</ta>
            <ta e="T307" id="Seg_5338" s="T306">быть-PST2.[3SG]</ta>
            <ta e="T308" id="Seg_5339" s="T307">тот-PL-ACC</ta>
            <ta e="T309" id="Seg_5340" s="T308">потом</ta>
            <ta e="T310" id="Seg_5341" s="T309">тундра-DAT/LOC</ta>
            <ta e="T311" id="Seg_5342" s="T310">старик-PROPR</ta>
            <ta e="T312" id="Seg_5343" s="T311">старуха.[NOM]</ta>
            <ta e="T313" id="Seg_5344" s="T312">опять</ta>
            <ta e="T314" id="Seg_5345" s="T313">тот</ta>
            <ta e="T315" id="Seg_5346" s="T314">идти-PST2.[3SG]</ta>
            <ta e="T316" id="Seg_5347" s="T315">два</ta>
            <ta e="T317" id="Seg_5348" s="T316">ребенок-1PL.[NOM]</ta>
            <ta e="T318" id="Seg_5349" s="T317">опять</ta>
            <ta e="T319" id="Seg_5350" s="T318">опять</ta>
            <ta e="T320" id="Seg_5351" s="T319">назад</ta>
            <ta e="T321" id="Seg_5352" s="T320">приходить-PST1-3SG</ta>
            <ta e="T322" id="Seg_5353" s="T321">EMPH</ta>
            <ta e="T323" id="Seg_5354" s="T322">тогда</ta>
            <ta e="T324" id="Seg_5355" s="T323">восемь</ta>
            <ta e="T325" id="Seg_5356" s="T324">ребенок-PROPR.[NOM]</ta>
            <ta e="T326" id="Seg_5357" s="T325">становиться-PST1-1SG</ta>
            <ta e="T327" id="Seg_5358" s="T326">EMPH</ta>
            <ta e="T328" id="Seg_5359" s="T327">1SG.[NOM]</ta>
            <ta e="T330" id="Seg_5360" s="T329">сам-1SG.[NOM]</ta>
            <ta e="T331" id="Seg_5361" s="T330">четыре</ta>
            <ta e="T332" id="Seg_5362" s="T331">ребенок-PROPR-1SG</ta>
            <ta e="T333" id="Seg_5363" s="T332">MOD</ta>
            <ta e="T334" id="Seg_5364" s="T333">тот.[NOM]</ta>
            <ta e="T335" id="Seg_5365" s="T334">два</ta>
            <ta e="T336" id="Seg_5366" s="T335">девушка-PROPR-1SG</ta>
            <ta e="T337" id="Seg_5367" s="T336">MOD</ta>
            <ta e="T338" id="Seg_5368" s="T337">мать-1SG.[NOM]</ta>
            <ta e="T339" id="Seg_5369" s="T338">старый.[NOM]</ta>
            <ta e="T340" id="Seg_5370" s="T339">старший.брат-EP-1SG.[NOM]</ta>
            <ta e="T341" id="Seg_5371" s="T340">старый.[NOM]</ta>
            <ta e="T342" id="Seg_5372" s="T341">потом</ta>
            <ta e="T343" id="Seg_5373" s="T342">тот</ta>
            <ta e="T344" id="Seg_5374" s="T343">восемь</ta>
            <ta e="T345" id="Seg_5375" s="T344">ребенок-PROPR-1SG</ta>
            <ta e="T346" id="Seg_5376" s="T345">сам-1SG.[NOM]</ta>
            <ta e="T347" id="Seg_5377" s="T346">старшая.сестра-EP-1SG.[NOM]</ta>
            <ta e="T348" id="Seg_5378" s="T347">собственный-3SG</ta>
            <ta e="T349" id="Seg_5379" s="T348">четыре</ta>
            <ta e="T350" id="Seg_5380" s="T349">ребенок.[NOM]</ta>
            <ta e="T351" id="Seg_5381" s="T350">оставаться-PST1-3SG</ta>
            <ta e="T352" id="Seg_5382" s="T351">мой</ta>
            <ta e="T353" id="Seg_5383" s="T352">четыре</ta>
            <ta e="T354" id="Seg_5384" s="T353">ребенок.[NOM]</ta>
            <ta e="T355" id="Seg_5385" s="T354">MOD</ta>
            <ta e="T360" id="Seg_5386" s="T359">маленький.[NOM]</ta>
            <ta e="T361" id="Seg_5387" s="T360">быть-PST1-3SG</ta>
            <ta e="T362" id="Seg_5388" s="T361">тот</ta>
            <ta e="T363" id="Seg_5389" s="T362">вот</ta>
            <ta e="T364" id="Seg_5390" s="T363">тот</ta>
            <ta e="T365" id="Seg_5391" s="T364">оставаться-CVB.SEQ-1SG</ta>
            <ta e="T366" id="Seg_5392" s="T365">вот</ta>
            <ta e="T367" id="Seg_5393" s="T366">тот.[NOM]</ta>
            <ta e="T368" id="Seg_5394" s="T367">жить-PST1-1PL</ta>
            <ta e="T369" id="Seg_5395" s="T368">MOD</ta>
            <ta e="T370" id="Seg_5396" s="T369">мать-1SG.[NOM]</ta>
            <ta e="T371" id="Seg_5397" s="T370">постареть-CVB.SEQ</ta>
            <ta e="T372" id="Seg_5398" s="T371">умирать-PST1-3SG</ta>
            <ta e="T373" id="Seg_5399" s="T372">быть.больным-CVB.SEQ</ta>
            <ta e="T374" id="Seg_5400" s="T373">тот</ta>
            <ta e="T375" id="Seg_5401" s="T374">старший.брат-EP-1SG.[NOM]</ta>
            <ta e="T376" id="Seg_5402" s="T375">опять</ta>
            <ta e="T377" id="Seg_5403" s="T376">жена-POSS</ta>
            <ta e="T378" id="Seg_5404" s="T377">NEG</ta>
            <ta e="T379" id="Seg_5405" s="T378">NEG</ta>
            <ta e="T380" id="Seg_5406" s="T379">что.[NOM]</ta>
            <ta e="T381" id="Seg_5407" s="T380">NEG</ta>
            <ta e="T382" id="Seg_5408" s="T381">NEG</ta>
            <ta e="T383" id="Seg_5409" s="T382">быть-PST1-3SG</ta>
            <ta e="T384" id="Seg_5410" s="T383">лапка-DAT/LOC</ta>
            <ta e="T385" id="Seg_5411" s="T384">работать-PTCP.HAB</ta>
            <ta e="T386" id="Seg_5412" s="T385">быть-PST1-3SG</ta>
            <ta e="T387" id="Seg_5413" s="T386">тот.[NOM]</ta>
            <ta e="T388" id="Seg_5414" s="T387">из_за</ta>
            <ta e="T389" id="Seg_5415" s="T388">пища-EP-INSTR</ta>
            <ta e="T390" id="Seg_5416" s="T389">NEG</ta>
            <ta e="T391" id="Seg_5417" s="T390">очень</ta>
            <ta e="T392" id="Seg_5418" s="T391">NEG</ta>
            <ta e="T393" id="Seg_5419" s="T392">кто-VBZ-PTCP.HAB-3SG</ta>
            <ta e="T394" id="Seg_5420" s="T393">NEG</ta>
            <ta e="T395" id="Seg_5421" s="T394">быть-PST1-1PL</ta>
            <ta e="T396" id="Seg_5422" s="T395">работать-PTCP.PRS</ta>
            <ta e="T397" id="Seg_5423" s="T396">быть-CVB.SEQ</ta>
            <ta e="T399" id="Seg_5424" s="T398">приносить-CVB.SEQ</ta>
            <ta e="T400" id="Seg_5425" s="T399">идти-HAB.[3SG]</ta>
            <ta e="T401" id="Seg_5426" s="T400">пища.[NOM]</ta>
            <ta e="T402" id="Seg_5427" s="T401">лапка-ABL</ta>
            <ta e="T403" id="Seg_5428" s="T402">зарплата-DIM.[NOM]</ta>
            <ta e="T404" id="Seg_5429" s="T403">тот.[NOM]</ta>
            <ta e="T405" id="Seg_5430" s="T404">получить-HAB.[3SG]</ta>
            <ta e="T406" id="Seg_5431" s="T405">потом</ta>
            <ta e="T407" id="Seg_5432" s="T406">1SG.[NOM]</ta>
            <ta e="T408" id="Seg_5433" s="T407">повар-VBZ-PRS-1SG</ta>
            <ta e="T409" id="Seg_5434" s="T408">MOD</ta>
            <ta e="T410" id="Seg_5435" s="T409">тот</ta>
            <ta e="T411" id="Seg_5436" s="T410">тот.[NOM]</ta>
            <ta e="T412" id="Seg_5437" s="T411">жить-PST2-1PL</ta>
            <ta e="T413" id="Seg_5438" s="T412">каждый-INTNS-3PL.[NOM]</ta>
            <ta e="T414" id="Seg_5439" s="T413">расти-PST2-3PL</ta>
            <ta e="T415" id="Seg_5440" s="T414">очевидно</ta>
            <ta e="T416" id="Seg_5441" s="T415">жена-PROPR-3PL</ta>
            <ta e="T422" id="Seg_5442" s="T421">вот</ta>
            <ta e="T423" id="Seg_5443" s="T422">выйти-EP-PST2-3SG</ta>
            <ta e="T424" id="Seg_5444" s="T423">что.[NOM]</ta>
            <ta e="T425" id="Seg_5445" s="T424">делать-CVB.PURP</ta>
            <ta e="T426" id="Seg_5446" s="T425">да</ta>
            <ta e="T427" id="Seg_5447" s="T426">выйти-EP-PST2.[3SG]</ta>
            <ta e="T428" id="Seg_5448" s="T427">вот</ta>
            <ta e="T429" id="Seg_5449" s="T428">тот.[NOM]</ta>
            <ta e="T430" id="Seg_5450" s="T429">из_за</ta>
            <ta e="T431" id="Seg_5451" s="T430">восемь</ta>
            <ta e="T432" id="Seg_5452" s="T431">ребенок.[NOM]</ta>
            <ta e="T433" id="Seg_5453" s="T432">быть-PRS.[3SG]</ta>
            <ta e="T434" id="Seg_5454" s="T433">MOD</ta>
            <ta e="T435" id="Seg_5455" s="T434">молодой.[NOM]</ta>
            <ta e="T436" id="Seg_5456" s="T435">жена-1SG</ta>
            <ta e="T437" id="Seg_5457" s="T436">EMPH</ta>
            <ta e="T438" id="Seg_5458" s="T437">там</ta>
            <ta e="T439" id="Seg_5459" s="T438">тот</ta>
            <ta e="T443" id="Seg_5460" s="T442">вот</ta>
            <ta e="T444" id="Seg_5461" s="T443">муж-DAT/LOC</ta>
            <ta e="T445" id="Seg_5462" s="T444">выйти-CVB.SEQ</ta>
            <ta e="T446" id="Seg_5463" s="T445">оставаться-PST1-1SG</ta>
            <ta e="T447" id="Seg_5464" s="T446">EMPH</ta>
            <ta e="T448" id="Seg_5465" s="T447">два</ta>
            <ta e="T449" id="Seg_5466" s="T448">ребенок-ACC</ta>
            <ta e="T450" id="Seg_5467" s="T449">родиться-PTCP.PST</ta>
            <ta e="T451" id="Seg_5468" s="T450">MOD</ta>
            <ta e="T452" id="Seg_5469" s="T451">тот</ta>
            <ta e="T453" id="Seg_5470" s="T452">три-ORD</ta>
            <ta e="T454" id="Seg_5471" s="T453">три-ORD</ta>
            <ta e="T455" id="Seg_5472" s="T454">ребенок-1SG.[NOM]</ta>
            <ta e="T456" id="Seg_5473" s="T455">мальчик.[NOM]</ta>
            <ta e="T457" id="Seg_5474" s="T456">ребенок.[NOM]</ta>
            <ta e="T458" id="Seg_5475" s="T457">быть-PST1-3SG</ta>
            <ta e="T459" id="Seg_5476" s="T458">тот-3SG-1SG.[NOM]</ta>
            <ta e="T460" id="Seg_5477" s="T459">падать-PST2-3SG</ta>
            <ta e="T461" id="Seg_5478" s="T460">убить-EP-MED-CVB.SEQ-1SG</ta>
            <ta e="T463" id="Seg_5479" s="T462">садик-DAT/LOC</ta>
            <ta e="T464" id="Seg_5480" s="T463">лед-PL-ACC</ta>
            <ta e="T465" id="Seg_5481" s="T464">уголь-PL-ACC</ta>
            <ta e="T466" id="Seg_5482" s="T465">таскать-VBZ-CVB.SEQ</ta>
            <ta e="T467" id="Seg_5483" s="T466">тот</ta>
            <ta e="T468" id="Seg_5484" s="T467">падать-PTCP.PST</ta>
            <ta e="T469" id="Seg_5485" s="T468">быть-PST1-3SG</ta>
            <ta e="T470" id="Seg_5486" s="T469">тот.[NOM]</ta>
            <ta e="T471" id="Seg_5487" s="T470">два</ta>
            <ta e="T472" id="Seg_5488" s="T471">мальчик.[NOM]</ta>
            <ta e="T473" id="Seg_5489" s="T472">промежуток-3SG.[NOM]</ta>
            <ta e="T481" id="Seg_5490" s="T480">Елена.[NOM]</ta>
            <ta e="T482" id="Seg_5491" s="T481">шесть</ta>
            <ta e="T483" id="Seg_5492" s="T482">год-PROPR-ACC</ta>
            <ta e="T484" id="Seg_5493" s="T483">получить-PST2-PST2</ta>
            <ta e="T485" id="Seg_5494" s="T484">потом</ta>
            <ta e="T486" id="Seg_5495" s="T485">семь</ta>
            <ta e="T487" id="Seg_5496" s="T486">год-VBZ-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T488" id="Seg_5497" s="T487">сюда</ta>
            <ta e="T489" id="Seg_5498" s="T488">приходить-PST1-1SG</ta>
            <ta e="T490" id="Seg_5499" s="T489">MOD</ta>
            <ta e="T491" id="Seg_5500" s="T490">Дудинка-DAT/LOC</ta>
            <ta e="T492" id="Seg_5501" s="T491">ребенок-1SG.[NOM]</ta>
            <ta e="T493" id="Seg_5502" s="T492">дом.[NOM]</ta>
            <ta e="T494" id="Seg_5503" s="T493">получить-PTCP.PRS</ta>
            <ta e="T495" id="Seg_5504" s="T494">быть-PST2.[3SG]</ta>
            <ta e="T496" id="Seg_5505" s="T495">только</ta>
            <ta e="T497" id="Seg_5506" s="T496">вот</ta>
            <ta e="T498" id="Seg_5507" s="T497">тот-DAT/LOC</ta>
            <ta e="T499" id="Seg_5508" s="T498">прописка-VBZ-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T500" id="Seg_5509" s="T499">надо</ta>
            <ta e="T501" id="Seg_5510" s="T500">быть-PST2.[3SG]</ta>
            <ta e="T502" id="Seg_5511" s="T501">вот</ta>
            <ta e="T503" id="Seg_5512" s="T502">тот</ta>
            <ta e="T504" id="Seg_5513" s="T503">приходить-PST2-EP-1SG</ta>
            <ta e="T505" id="Seg_5514" s="T504">тот</ta>
            <ta e="T506" id="Seg_5515" s="T505">приходить-CVB.SEQ-1SG</ta>
            <ta e="T507" id="Seg_5516" s="T506">EMPH</ta>
            <ta e="T508" id="Seg_5517" s="T507">ребенок-1SG.[NOM]</ta>
            <ta e="T509" id="Seg_5518" s="T508">десять-ORD</ta>
            <ta e="T510" id="Seg_5519" s="T509">класс.[NOM]</ta>
            <ta e="T511" id="Seg_5520" s="T510">быть-PST1-3SG</ta>
            <ta e="T513" id="Seg_5521" s="T512">вот</ta>
            <ta e="T514" id="Seg_5522" s="T513">мама-3SG.[NOM]</ta>
            <ta e="T515" id="Seg_5523" s="T514">однако</ta>
            <ta e="T516" id="Seg_5524" s="T515">Якутск-DAT/LOC</ta>
            <ta e="T517" id="Seg_5525" s="T516">муж-VBZ-PST2-3SG</ta>
            <ta e="T518" id="Seg_5526" s="T517">тот-3SG-2SG.[NOM]</ta>
            <ta e="T519" id="Seg_5527" s="T518">теперь</ta>
            <ta e="T520" id="Seg_5528" s="T519">два</ta>
            <ta e="T521" id="Seg_5529" s="T520">ребенок-PROPR.[NOM]</ta>
            <ta e="T522" id="Seg_5530" s="T521">мальчик-ACC</ta>
            <ta e="T523" id="Seg_5531" s="T522">с</ta>
            <ta e="T524" id="Seg_5532" s="T523">девушка.[NOM]</ta>
            <ta e="T529" id="Seg_5533" s="T528">тундра-DAT/LOC</ta>
            <ta e="T530" id="Seg_5534" s="T529">тундра-DAT/LOC</ta>
            <ta e="T531" id="Seg_5535" s="T530">жить-PRS-3PL</ta>
            <ta e="T532" id="Seg_5536" s="T531">тундра-DAT/LOC</ta>
            <ta e="T533" id="Seg_5537" s="T532">олень-AG.[NOM]</ta>
            <ta e="T534" id="Seg_5538" s="T533">муж-3SG.[NOM]</ta>
            <ta e="T536" id="Seg_5539" s="T535">последний-3SG.[NOM]</ta>
            <ta e="T537" id="Seg_5540" s="T536">Сындасско-DAT/LOC</ta>
            <ta e="T538" id="Seg_5541" s="T537">оставаться-PST2-3PL</ta>
            <ta e="T539" id="Seg_5542" s="T538">теперь</ta>
            <ta e="T540" id="Seg_5543" s="T539">идти-FUT-3PL</ta>
            <ta e="T541" id="Seg_5544" s="T540">май</ta>
            <ta e="T542" id="Seg_5545" s="T541">месяц-DAT/LOC</ta>
            <ta e="T543" id="Seg_5546" s="T542">школа.[NOM]</ta>
            <ta e="T544" id="Seg_5547" s="T543">кончать-PASS-CVB.SEQ</ta>
            <ta e="T545" id="Seg_5548" s="T544">ребенок-3PL.[NOM]</ta>
            <ta e="T546" id="Seg_5549" s="T545">там</ta>
            <ta e="T547" id="Seg_5550" s="T546">учиться-PRS-3PL</ta>
            <ta e="T548" id="Seg_5551" s="T547">Сындасско-DAT/LOC</ta>
            <ta e="T549" id="Seg_5552" s="T548">один.из.двух-3SG.[NOM]</ta>
            <ta e="T550" id="Seg_5553" s="T549">четыре-ORD</ta>
            <ta e="T551" id="Seg_5554" s="T550">класс.[NOM]</ta>
            <ta e="T552" id="Seg_5555" s="T551">один.из.двух-3SG.[NOM]</ta>
            <ta e="T553" id="Seg_5556" s="T552">три-ORD</ta>
            <ta e="T554" id="Seg_5557" s="T553">класс.[NOM]</ta>
            <ta e="T555" id="Seg_5558" s="T554">Q</ta>
            <ta e="T556" id="Seg_5559" s="T555">два-ORD</ta>
            <ta e="T557" id="Seg_5560" s="T556">Q</ta>
            <ta e="T559" id="Seg_5561" s="T558">два-ORD</ta>
            <ta e="T560" id="Seg_5562" s="T559">класс.[NOM]</ta>
            <ta e="T561" id="Seg_5563" s="T560">ээ</ta>
            <ta e="T562" id="Seg_5564" s="T561">дочь-3SG.[NOM]</ta>
            <ta e="T563" id="Seg_5565" s="T562">теперь</ta>
            <ta e="T564" id="Seg_5566" s="T563">1SG.[NOM]</ta>
            <ta e="T565" id="Seg_5567" s="T564">дом-1SG-DAT/LOC</ta>
            <ta e="T566" id="Seg_5568" s="T565">оставаться-PST2-3PL</ta>
            <ta e="T567" id="Seg_5569" s="T566">теперь</ta>
            <ta e="T568" id="Seg_5570" s="T567">май-DAT/LOC</ta>
            <ta e="T569" id="Seg_5571" s="T568">идти-FUT-3PL</ta>
            <ta e="T570" id="Seg_5572" s="T569">теперь</ta>
            <ta e="T572" id="Seg_5573" s="T571">кочевать-CVB.SEQ</ta>
            <ta e="T573" id="Seg_5574" s="T572">идти-FUT-3PL</ta>
            <ta e="T574" id="Seg_5575" s="T573">последний-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-KiES">
            <ta e="T2" id="Seg_5576" s="T1">ptcl</ta>
            <ta e="T3" id="Seg_5577" s="T2">propr-n:case</ta>
            <ta e="T4" id="Seg_5578" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_5579" s="T4">v-v:tense-v:poss.pn</ta>
            <ta e="T6" id="Seg_5580" s="T5">n-n&gt;adj.[n:case]</ta>
            <ta e="T7" id="Seg_5581" s="T6">v-v:tense-v:poss.pn</ta>
            <ta e="T8" id="Seg_5582" s="T7">pers-pro:case</ta>
            <ta e="T9" id="Seg_5583" s="T8">propr-n:case</ta>
            <ta e="T11" id="Seg_5584" s="T9">n-n&gt;v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T12" id="Seg_5585" s="T11">adv</ta>
            <ta e="T13" id="Seg_5586" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_5587" s="T13">dempro</ta>
            <ta e="T15" id="Seg_5588" s="T14">interj</ta>
            <ta e="T16" id="Seg_5589" s="T15">dempro</ta>
            <ta e="T17" id="Seg_5590" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_5591" s="T17">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T19" id="Seg_5592" s="T18">adv</ta>
            <ta e="T20" id="Seg_5593" s="T19">dempro</ta>
            <ta e="T21" id="Seg_5594" s="T20">n.[n:case]</ta>
            <ta e="T22" id="Seg_5595" s="T21">n-n:(poss).[n:case]</ta>
            <ta e="T23" id="Seg_5596" s="T22">v-v:cvb-v:pred.pn</ta>
            <ta e="T24" id="Seg_5597" s="T23">dempro</ta>
            <ta e="T25" id="Seg_5598" s="T24">adv</ta>
            <ta e="T26" id="Seg_5599" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_5600" s="T26">n-n:(poss).[n:case]</ta>
            <ta e="T28" id="Seg_5601" s="T27">v-v:ptcp</ta>
            <ta e="T29" id="Seg_5602" s="T28">v-v:tense-v:poss.pn</ta>
            <ta e="T30" id="Seg_5603" s="T29">v-v:cvb</ta>
            <ta e="T31" id="Seg_5604" s="T30">v-v:cvb</ta>
            <ta e="T32" id="Seg_5605" s="T31">dempro</ta>
            <ta e="T33" id="Seg_5606" s="T32">que-pro:case</ta>
            <ta e="T34" id="Seg_5607" s="T33">propr-n:case</ta>
            <ta e="T35" id="Seg_5608" s="T34">v-v:tense-v:pred.pn</ta>
            <ta e="T36" id="Seg_5609" s="T35">dempro</ta>
            <ta e="T37" id="Seg_5610" s="T36">adv</ta>
            <ta e="T38" id="Seg_5611" s="T37">n-n:(poss).[n:case]</ta>
            <ta e="T39" id="Seg_5612" s="T38">pers-pro:case</ta>
            <ta e="T43" id="Seg_5613" s="T42">dempro</ta>
            <ta e="T44" id="Seg_5614" s="T43">ptcl</ta>
            <ta e="T48" id="Seg_5615" s="T47">adv</ta>
            <ta e="T49" id="Seg_5616" s="T48">n.[n:case]</ta>
            <ta e="T50" id="Seg_5617" s="T49">v-v:tense-v:poss.pn</ta>
            <ta e="T51" id="Seg_5618" s="T50">pers-pro:case</ta>
            <ta e="T52" id="Seg_5619" s="T51">n-n:case</ta>
            <ta e="T64" id="Seg_5620" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_5621" s="T64">adj-adj&gt;adv</ta>
            <ta e="T66" id="Seg_5622" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_5623" s="T66">propr-n:case</ta>
            <ta e="T68" id="Seg_5624" s="T67">adj</ta>
            <ta e="T69" id="Seg_5625" s="T68">adv</ta>
            <ta e="T70" id="Seg_5626" s="T69">n.[n:case]</ta>
            <ta e="T71" id="Seg_5627" s="T70">v-v:ptcp</ta>
            <ta e="T72" id="Seg_5628" s="T71">v-v:tense-v:poss.pn</ta>
            <ta e="T73" id="Seg_5629" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_5630" s="T73">que-pro:case</ta>
            <ta e="T75" id="Seg_5631" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_5632" s="T75">v-v:(neg)-v:pred.pn</ta>
            <ta e="T77" id="Seg_5633" s="T76">n.[n:case]</ta>
            <ta e="T78" id="Seg_5634" s="T77">que.[pro:case]</ta>
            <ta e="T79" id="Seg_5635" s="T78">n-n:(poss).[n:case]=ptcl</ta>
            <ta e="T92" id="Seg_5636" s="T91">interj</ta>
            <ta e="T94" id="Seg_5637" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_5638" s="T94">que</ta>
            <ta e="T96" id="Seg_5639" s="T95">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T98" id="Seg_5640" s="T97">v-v:cvb</ta>
            <ta e="T99" id="Seg_5641" s="T98">v-v:(neg)-v:pred.pn</ta>
            <ta e="T100" id="Seg_5642" s="T99">n-n:(num)-n:case</ta>
            <ta e="T101" id="Seg_5643" s="T100">ptcl</ta>
            <ta e="T102" id="Seg_5644" s="T101">n-n:(num)-n:case</ta>
            <ta e="T103" id="Seg_5645" s="T102">v-v:cvb</ta>
            <ta e="T104" id="Seg_5646" s="T103">v-v:(ins)-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T105" id="Seg_5647" s="T104">v-v:tense-v:pred.pn</ta>
            <ta e="T106" id="Seg_5648" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_5649" s="T106">n-n:(num).[n:case]</ta>
            <ta e="T108" id="Seg_5650" s="T107">v-v:tense-v:pred.pn</ta>
            <ta e="T109" id="Seg_5651" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_5652" s="T109">dempro</ta>
            <ta e="T111" id="Seg_5653" s="T110">n.[n:case]</ta>
            <ta e="T112" id="Seg_5654" s="T111">v-v:tense-v:poss.pn</ta>
            <ta e="T113" id="Seg_5655" s="T112">adj</ta>
            <ta e="T114" id="Seg_5656" s="T113">n-n:case</ta>
            <ta e="T115" id="Seg_5657" s="T114">dempro-pro:case</ta>
            <ta e="T116" id="Seg_5658" s="T115">n.[n:case]</ta>
            <ta e="T117" id="Seg_5659" s="T116">n.[n:case]</ta>
            <ta e="T118" id="Seg_5660" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_5661" s="T118">v-v:tense-v:pred.pn</ta>
            <ta e="T120" id="Seg_5662" s="T119">n-n:(num).[n:case]</ta>
            <ta e="T121" id="Seg_5663" s="T120">adv</ta>
            <ta e="T122" id="Seg_5664" s="T121">n-n:(num).[n:case]</ta>
            <ta e="T123" id="Seg_5665" s="T122">pers-pro:case</ta>
            <ta e="T124" id="Seg_5666" s="T123">n.[n:case]</ta>
            <ta e="T125" id="Seg_5667" s="T124">v-v:tense-v:pred.pn</ta>
            <ta e="T126" id="Seg_5668" s="T125">ptcl</ta>
            <ta e="T127" id="Seg_5669" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_5670" s="T127">dempro</ta>
            <ta e="T129" id="Seg_5671" s="T128">v-v:tense-v:pred.pn</ta>
            <ta e="T130" id="Seg_5672" s="T129">adv</ta>
            <ta e="T131" id="Seg_5673" s="T130">dempro</ta>
            <ta e="T132" id="Seg_5674" s="T131">n-n&gt;v-v:cvb-v:pred.pn</ta>
            <ta e="T133" id="Seg_5675" s="T132">ptcl</ta>
            <ta e="T134" id="Seg_5676" s="T133">interj</ta>
            <ta e="T135" id="Seg_5677" s="T134">que-pro:case</ta>
            <ta e="T136" id="Seg_5678" s="T135">dempro</ta>
            <ta e="T137" id="Seg_5679" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_5680" s="T137">dempro.[pro:case]</ta>
            <ta e="T139" id="Seg_5681" s="T138">cardnum</ta>
            <ta e="T140" id="Seg_5682" s="T139">n-n:(poss).[n:case]</ta>
            <ta e="T141" id="Seg_5683" s="T140">v-v:tense-v:pred.pn</ta>
            <ta e="T142" id="Seg_5684" s="T141">n-n:case</ta>
            <ta e="T143" id="Seg_5685" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_5686" s="T143">ptcl</ta>
            <ta e="T145" id="Seg_5687" s="T144">ptcl</ta>
            <ta e="T146" id="Seg_5688" s="T145">dempro</ta>
            <ta e="T147" id="Seg_5689" s="T146">v-v:cvb-v:pred.pn</ta>
            <ta e="T151" id="Seg_5690" s="T150">que-pro:case</ta>
            <ta e="T152" id="Seg_5691" s="T151">post</ta>
            <ta e="T157" id="Seg_5692" s="T156">que</ta>
            <ta e="T158" id="Seg_5693" s="T157">dempro</ta>
            <ta e="T159" id="Seg_5694" s="T158">adv</ta>
            <ta e="T160" id="Seg_5695" s="T159">dempro</ta>
            <ta e="T161" id="Seg_5696" s="T160">adv</ta>
            <ta e="T162" id="Seg_5697" s="T161">dempro</ta>
            <ta e="T163" id="Seg_5698" s="T162">que-pro:(ins)-pro:(poss).[pro:case]</ta>
            <ta e="T164" id="Seg_5699" s="T163">dempro</ta>
            <ta e="T165" id="Seg_5700" s="T164">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T166" id="Seg_5701" s="T165">v-v:tense-v:poss.pn</ta>
            <ta e="T171" id="Seg_5702" s="T168">ptcl</ta>
            <ta e="T172" id="Seg_5703" s="T171">n-n:(poss).[n:case]</ta>
            <ta e="T173" id="Seg_5704" s="T172">n-n:(poss).[n:case]</ta>
            <ta e="T174" id="Seg_5705" s="T173">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T175" id="Seg_5706" s="T174">v-v:tense-v:poss.pn</ta>
            <ta e="T176" id="Seg_5707" s="T175">v-v:cvb</ta>
            <ta e="T177" id="Seg_5708" s="T176">adv</ta>
            <ta e="T178" id="Seg_5709" s="T177">cardnum</ta>
            <ta e="T179" id="Seg_5710" s="T178">n-n&gt;adj.[n:case]</ta>
            <ta e="T180" id="Seg_5711" s="T179">v-v:tense-v:poss.pn</ta>
            <ta e="T181" id="Seg_5712" s="T180">dempro</ta>
            <ta e="T182" id="Seg_5713" s="T181">n.[n:case]</ta>
            <ta e="T183" id="Seg_5714" s="T182">cardnum</ta>
            <ta e="T184" id="Seg_5715" s="T183">n.[n:case]</ta>
            <ta e="T185" id="Seg_5716" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_5717" s="T185">pers-pro:case</ta>
            <ta e="T187" id="Seg_5718" s="T186">v-v:tense-v:pred.pn</ta>
            <ta e="T188" id="Seg_5719" s="T187">ptcl</ta>
            <ta e="T189" id="Seg_5720" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_5721" s="T189">n-n:poss-n:case</ta>
            <ta e="T191" id="Seg_5722" s="T190">v-v:tense-v:poss.pn</ta>
            <ta e="T192" id="Seg_5723" s="T191">pers.[pro:case]</ta>
            <ta e="T193" id="Seg_5724" s="T192">que</ta>
            <ta e="T194" id="Seg_5725" s="T193">ptcl</ta>
            <ta e="T195" id="Seg_5726" s="T194">v-v:(neg)-v:pred.pn</ta>
            <ta e="T196" id="Seg_5727" s="T195">v-v:tense-v:pred.pn</ta>
            <ta e="T197" id="Seg_5728" s="T196">n-n:(poss).[n:case]</ta>
            <ta e="T198" id="Seg_5729" s="T197">interj</ta>
            <ta e="T199" id="Seg_5730" s="T198">n</ta>
            <ta e="T200" id="Seg_5731" s="T199">n-n:(poss).[n:case]</ta>
            <ta e="T201" id="Seg_5732" s="T200">n.[n:case]</ta>
            <ta e="T202" id="Seg_5733" s="T201">v-v:tense.[v:pred.pn]</ta>
            <ta e="T203" id="Seg_5734" s="T202">ptcl</ta>
            <ta e="T204" id="Seg_5735" s="T203">dempro</ta>
            <ta e="T205" id="Seg_5736" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_5737" s="T205">n-n:poss-n:case</ta>
            <ta e="T207" id="Seg_5738" s="T206">v-v:cvb</ta>
            <ta e="T208" id="Seg_5739" s="T207">v-v:tense.[v:pred.pn]</ta>
            <ta e="T209" id="Seg_5740" s="T208">ptcl</ta>
            <ta e="T210" id="Seg_5741" s="T209">pers.[pro:case]</ta>
            <ta e="T211" id="Seg_5742" s="T210">que</ta>
            <ta e="T212" id="Seg_5743" s="T211">v-v:mood-v:temp.pn</ta>
            <ta e="T213" id="Seg_5744" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_5745" s="T213">dempro</ta>
            <ta e="T215" id="Seg_5746" s="T214">dempro</ta>
            <ta e="T216" id="Seg_5747" s="T215">cardnum</ta>
            <ta e="T217" id="Seg_5748" s="T216">cardnum</ta>
            <ta e="T218" id="Seg_5749" s="T217">n.[n:case]</ta>
            <ta e="T219" id="Seg_5750" s="T218">pers-pro:case</ta>
            <ta e="T220" id="Seg_5751" s="T219">v-v:tense-v:pred.pn</ta>
            <ta e="T221" id="Seg_5752" s="T220">ptcl</ta>
            <ta e="T222" id="Seg_5753" s="T221">interj</ta>
            <ta e="T223" id="Seg_5754" s="T222">propr.[n:case]</ta>
            <ta e="T224" id="Seg_5755" s="T223">v-v:cvb</ta>
            <ta e="T225" id="Seg_5756" s="T224">v-v:tense-v:pred.pn</ta>
            <ta e="T231" id="Seg_5757" s="T230">ptcl</ta>
            <ta e="T232" id="Seg_5758" s="T231">interj</ta>
            <ta e="T233" id="Seg_5759" s="T232">adv</ta>
            <ta e="T237" id="Seg_5760" s="T236">ptcl</ta>
            <ta e="T238" id="Seg_5761" s="T237">adv</ta>
            <ta e="T239" id="Seg_5762" s="T238">n-n:case</ta>
            <ta e="T240" id="Seg_5763" s="T239">v-v:tense-v:pred.pn</ta>
            <ta e="T241" id="Seg_5764" s="T240">dempro</ta>
            <ta e="T242" id="Seg_5765" s="T241">n-n:(num).[n:case]</ta>
            <ta e="T243" id="Seg_5766" s="T242">v-v:tense-v:pred.pn</ta>
            <ta e="T244" id="Seg_5767" s="T243">n-n:(num).[n:case]</ta>
            <ta e="T245" id="Seg_5768" s="T244">ptcl</ta>
            <ta e="T246" id="Seg_5769" s="T245">dempro</ta>
            <ta e="T247" id="Seg_5770" s="T246">n-n:case</ta>
            <ta e="T248" id="Seg_5771" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_5772" s="T248">propr-n:case</ta>
            <ta e="T250" id="Seg_5773" s="T249">v-v:ptcp-v:(case)</ta>
            <ta e="T251" id="Seg_5774" s="T250">v-v:tense-v:pred.pn</ta>
            <ta e="T253" id="Seg_5775" s="T251">n-n:poss-n:case</ta>
            <ta e="T257" id="Seg_5776" s="T255">interj</ta>
            <ta e="T258" id="Seg_5777" s="T257">propr-n:case</ta>
            <ta e="T259" id="Seg_5778" s="T258">cardnum</ta>
            <ta e="T260" id="Seg_5779" s="T259">cardnum</ta>
            <ta e="T261" id="Seg_5780" s="T260">n-n:case</ta>
            <ta e="T262" id="Seg_5781" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_5782" s="T262">adv</ta>
            <ta e="T264" id="Seg_5783" s="T263">n-n:case</ta>
            <ta e="T265" id="Seg_5784" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_5785" s="T265">n</ta>
            <ta e="T267" id="Seg_5786" s="T266">n-n&gt;adj</ta>
            <ta e="T268" id="Seg_5787" s="T267">n.[n:case]</ta>
            <ta e="T269" id="Seg_5788" s="T268">n-n:(poss).[n:case]</ta>
            <ta e="T270" id="Seg_5789" s="T269">ptcl</ta>
            <ta e="T271" id="Seg_5790" s="T270">ptcl</ta>
            <ta e="T272" id="Seg_5791" s="T271">dempro-pro:(num)-pro:case</ta>
            <ta e="T273" id="Seg_5792" s="T272">v-v:tense-v:pred.pn</ta>
            <ta e="T274" id="Seg_5793" s="T273">adv</ta>
            <ta e="T275" id="Seg_5794" s="T274">pers.[pro:case]</ta>
            <ta e="T276" id="Seg_5795" s="T275">cardnum</ta>
            <ta e="T277" id="Seg_5796" s="T276">n.[n:case]</ta>
            <ta e="T278" id="Seg_5797" s="T277">n-n:case</ta>
            <ta e="T279" id="Seg_5798" s="T278">post</ta>
            <ta e="T280" id="Seg_5799" s="T279">v-v:tense-v:poss.pn</ta>
            <ta e="T281" id="Seg_5800" s="T280">ptcl</ta>
            <ta e="T282" id="Seg_5801" s="T281">ptcl</ta>
            <ta e="T283" id="Seg_5802" s="T282">dempro-pro:(num).[pro:case]</ta>
            <ta e="T284" id="Seg_5803" s="T283">pers-pro:case</ta>
            <ta e="T285" id="Seg_5804" s="T284">v</ta>
            <ta e="T286" id="Seg_5805" s="T285">v-v:(neg)-v:pred.pn</ta>
            <ta e="T287" id="Seg_5806" s="T286">adv</ta>
            <ta e="T288" id="Seg_5807" s="T287">dempro.[pro:case]</ta>
            <ta e="T289" id="Seg_5808" s="T288">cardnum</ta>
            <ta e="T290" id="Seg_5809" s="T289">n.[n:case]</ta>
            <ta e="T291" id="Seg_5810" s="T290">n.[n:case]</ta>
            <ta e="T292" id="Seg_5811" s="T291">ptcl</ta>
            <ta e="T293" id="Seg_5812" s="T292">adv</ta>
            <ta e="T294" id="Seg_5813" s="T293">que</ta>
            <ta e="T295" id="Seg_5814" s="T294">ptcl</ta>
            <ta e="T296" id="Seg_5815" s="T295">v-v:cvb</ta>
            <ta e="T297" id="Seg_5816" s="T296">n-n:case</ta>
            <ta e="T298" id="Seg_5817" s="T297">v-v:tense-v:poss.pn</ta>
            <ta e="T299" id="Seg_5818" s="T298">dempro</ta>
            <ta e="T300" id="Seg_5819" s="T299">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T301" id="Seg_5820" s="T300">ptcl</ta>
            <ta e="T302" id="Seg_5821" s="T301">adv</ta>
            <ta e="T303" id="Seg_5822" s="T302">v-v:tense-v:pred.pn</ta>
            <ta e="T304" id="Seg_5823" s="T303">pers-pro:case</ta>
            <ta e="T305" id="Seg_5824" s="T304">dempro</ta>
            <ta e="T306" id="Seg_5825" s="T305">v-v:(neg)-v:pred.pn</ta>
            <ta e="T307" id="Seg_5826" s="T306">v-v:tense.[v:pred.pn]</ta>
            <ta e="T308" id="Seg_5827" s="T307">dempro-pro:(num)-pro:case</ta>
            <ta e="T309" id="Seg_5828" s="T308">adv</ta>
            <ta e="T310" id="Seg_5829" s="T309">n-n:case</ta>
            <ta e="T311" id="Seg_5830" s="T310">n-n&gt;adj</ta>
            <ta e="T312" id="Seg_5831" s="T311">n.[n:case]</ta>
            <ta e="T313" id="Seg_5832" s="T312">ptcl</ta>
            <ta e="T314" id="Seg_5833" s="T313">dempro</ta>
            <ta e="T315" id="Seg_5834" s="T314">v-v:tense.[v:pred.pn]</ta>
            <ta e="T316" id="Seg_5835" s="T315">cardnum</ta>
            <ta e="T317" id="Seg_5836" s="T316">n-n:(poss).[n:case]</ta>
            <ta e="T318" id="Seg_5837" s="T317">ptcl</ta>
            <ta e="T319" id="Seg_5838" s="T318">ptcl</ta>
            <ta e="T320" id="Seg_5839" s="T319">adv</ta>
            <ta e="T321" id="Seg_5840" s="T320">v-v:tense-v:poss.pn</ta>
            <ta e="T322" id="Seg_5841" s="T321">ptcl</ta>
            <ta e="T323" id="Seg_5842" s="T322">adv</ta>
            <ta e="T324" id="Seg_5843" s="T323">cardnum</ta>
            <ta e="T325" id="Seg_5844" s="T324">n-n&gt;adj.[n:case]</ta>
            <ta e="T326" id="Seg_5845" s="T325">v-v:tense-v:poss.pn</ta>
            <ta e="T327" id="Seg_5846" s="T326">ptcl</ta>
            <ta e="T328" id="Seg_5847" s="T327">pers.[pro:case]</ta>
            <ta e="T330" id="Seg_5848" s="T329">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T331" id="Seg_5849" s="T330">cardnum</ta>
            <ta e="T332" id="Seg_5850" s="T331">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T333" id="Seg_5851" s="T332">ptcl</ta>
            <ta e="T334" id="Seg_5852" s="T333">dempro.[pro:case]</ta>
            <ta e="T335" id="Seg_5853" s="T334">cardnum</ta>
            <ta e="T336" id="Seg_5854" s="T335">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T337" id="Seg_5855" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_5856" s="T337">n-n:(poss).[n:case]</ta>
            <ta e="T339" id="Seg_5857" s="T338">adj.[n:case]</ta>
            <ta e="T340" id="Seg_5858" s="T339">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T341" id="Seg_5859" s="T340">adj.[n:case]</ta>
            <ta e="T342" id="Seg_5860" s="T341">adv</ta>
            <ta e="T343" id="Seg_5861" s="T342">dempro</ta>
            <ta e="T344" id="Seg_5862" s="T343">cardnum</ta>
            <ta e="T345" id="Seg_5863" s="T344">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T346" id="Seg_5864" s="T345">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T347" id="Seg_5865" s="T346">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T348" id="Seg_5866" s="T347">adj-n:(poss)</ta>
            <ta e="T349" id="Seg_5867" s="T348">cardnum</ta>
            <ta e="T350" id="Seg_5868" s="T349">n.[n:case]</ta>
            <ta e="T351" id="Seg_5869" s="T350">v-v:tense-v:poss.pn</ta>
            <ta e="T352" id="Seg_5870" s="T351">posspr</ta>
            <ta e="T353" id="Seg_5871" s="T352">cardnum</ta>
            <ta e="T354" id="Seg_5872" s="T353">n.[n:case]</ta>
            <ta e="T355" id="Seg_5873" s="T354">ptcl</ta>
            <ta e="T360" id="Seg_5874" s="T359">adj.[n:case]</ta>
            <ta e="T361" id="Seg_5875" s="T360">v-v:tense-v:poss.pn</ta>
            <ta e="T362" id="Seg_5876" s="T361">dempro</ta>
            <ta e="T363" id="Seg_5877" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_5878" s="T363">dempro</ta>
            <ta e="T365" id="Seg_5879" s="T364">v-v:cvb-v:pred.pn</ta>
            <ta e="T366" id="Seg_5880" s="T365">ptcl</ta>
            <ta e="T367" id="Seg_5881" s="T366">dempro.[pro:case]</ta>
            <ta e="T368" id="Seg_5882" s="T367">v-v:tense-v:poss.pn</ta>
            <ta e="T369" id="Seg_5883" s="T368">ptcl</ta>
            <ta e="T370" id="Seg_5884" s="T369">n-n:(poss).[n:case]</ta>
            <ta e="T371" id="Seg_5885" s="T370">v-v:cvb</ta>
            <ta e="T372" id="Seg_5886" s="T371">v-v:tense-v:(poss)</ta>
            <ta e="T373" id="Seg_5887" s="T372">v-v:cvb</ta>
            <ta e="T374" id="Seg_5888" s="T373">dempro</ta>
            <ta e="T375" id="Seg_5889" s="T374">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T376" id="Seg_5890" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_5891" s="T376">n-n:(poss)</ta>
            <ta e="T378" id="Seg_5892" s="T377">ptcl</ta>
            <ta e="T379" id="Seg_5893" s="T378">ptcl</ta>
            <ta e="T380" id="Seg_5894" s="T379">que.[pro:case]</ta>
            <ta e="T381" id="Seg_5895" s="T380">ptcl</ta>
            <ta e="T382" id="Seg_5896" s="T381">ptcl</ta>
            <ta e="T383" id="Seg_5897" s="T382">v-v:tense-v:poss.pn</ta>
            <ta e="T384" id="Seg_5898" s="T383">n-n:case</ta>
            <ta e="T385" id="Seg_5899" s="T384">v-v:ptcp</ta>
            <ta e="T386" id="Seg_5900" s="T385">v-v:tense-v:poss.pn</ta>
            <ta e="T387" id="Seg_5901" s="T386">dempro.[pro:case]</ta>
            <ta e="T388" id="Seg_5902" s="T387">post</ta>
            <ta e="T389" id="Seg_5903" s="T388">n-n:(ins)-n:case</ta>
            <ta e="T390" id="Seg_5904" s="T389">ptcl</ta>
            <ta e="T391" id="Seg_5905" s="T390">adv</ta>
            <ta e="T392" id="Seg_5906" s="T391">ptcl</ta>
            <ta e="T393" id="Seg_5907" s="T392">que-que&gt;v-v:ptcp-v:(poss)</ta>
            <ta e="T394" id="Seg_5908" s="T393">ptcl</ta>
            <ta e="T395" id="Seg_5909" s="T394">v-v:tense-v:poss.pn</ta>
            <ta e="T396" id="Seg_5910" s="T395">v-v:ptcp</ta>
            <ta e="T397" id="Seg_5911" s="T396">v-v:cvb</ta>
            <ta e="T399" id="Seg_5912" s="T398">v-v:cvb</ta>
            <ta e="T400" id="Seg_5913" s="T399">v-v:mood.[v:pred.pn]</ta>
            <ta e="T401" id="Seg_5914" s="T400">n.[n:case]</ta>
            <ta e="T402" id="Seg_5915" s="T401">n-n:case</ta>
            <ta e="T403" id="Seg_5916" s="T402">n-n&gt;n.[n:case]</ta>
            <ta e="T404" id="Seg_5917" s="T403">dempro.[pro:case]</ta>
            <ta e="T405" id="Seg_5918" s="T404">v-v:mood.[v:pred.pn]</ta>
            <ta e="T406" id="Seg_5919" s="T405">adv</ta>
            <ta e="T407" id="Seg_5920" s="T406">pers.[pro:case]</ta>
            <ta e="T408" id="Seg_5921" s="T407">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T409" id="Seg_5922" s="T408">ptcl</ta>
            <ta e="T410" id="Seg_5923" s="T409">dempro</ta>
            <ta e="T411" id="Seg_5924" s="T410">dempro.[pro:case]</ta>
            <ta e="T412" id="Seg_5925" s="T411">v-v:tense-v:pred.pn</ta>
            <ta e="T413" id="Seg_5926" s="T412">adj-adj&gt;adj-n:(poss).[n:case]</ta>
            <ta e="T414" id="Seg_5927" s="T413">v-v:tense-v:poss.pn</ta>
            <ta e="T415" id="Seg_5928" s="T414">ptcl</ta>
            <ta e="T416" id="Seg_5929" s="T415">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T422" id="Seg_5930" s="T421">ptcl</ta>
            <ta e="T423" id="Seg_5931" s="T422">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T424" id="Seg_5932" s="T423">que.[pro:case]</ta>
            <ta e="T425" id="Seg_5933" s="T424">v-v:cvb</ta>
            <ta e="T426" id="Seg_5934" s="T425">conj</ta>
            <ta e="T427" id="Seg_5935" s="T426">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T428" id="Seg_5936" s="T427">ptcl</ta>
            <ta e="T429" id="Seg_5937" s="T428">dempro.[pro:case]</ta>
            <ta e="T430" id="Seg_5938" s="T429">post</ta>
            <ta e="T431" id="Seg_5939" s="T430">cardnum</ta>
            <ta e="T432" id="Seg_5940" s="T431">n.[n:case]</ta>
            <ta e="T433" id="Seg_5941" s="T432">v-v:tense.[v:pred.pn]</ta>
            <ta e="T434" id="Seg_5942" s="T433">ptcl</ta>
            <ta e="T435" id="Seg_5943" s="T434">adj.[pro:case]</ta>
            <ta e="T436" id="Seg_5944" s="T435">n-n:(pred.pn)</ta>
            <ta e="T437" id="Seg_5945" s="T436">ptcl</ta>
            <ta e="T438" id="Seg_5946" s="T437">adv</ta>
            <ta e="T439" id="Seg_5947" s="T438">dempro</ta>
            <ta e="T443" id="Seg_5948" s="T442">ptcl</ta>
            <ta e="T444" id="Seg_5949" s="T443">n-n:case</ta>
            <ta e="T445" id="Seg_5950" s="T444">v-v:cvb</ta>
            <ta e="T446" id="Seg_5951" s="T445">v-v:tense-v:poss.pn</ta>
            <ta e="T447" id="Seg_5952" s="T446">ptcl</ta>
            <ta e="T448" id="Seg_5953" s="T447">cardnum</ta>
            <ta e="T449" id="Seg_5954" s="T448">n-n:case</ta>
            <ta e="T450" id="Seg_5955" s="T449">v-v:ptcp</ta>
            <ta e="T451" id="Seg_5956" s="T450">ptcl</ta>
            <ta e="T452" id="Seg_5957" s="T451">dempro</ta>
            <ta e="T453" id="Seg_5958" s="T452">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T454" id="Seg_5959" s="T453">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T455" id="Seg_5960" s="T454">n-n:(poss).[n:case]</ta>
            <ta e="T456" id="Seg_5961" s="T455">n.[n:case]</ta>
            <ta e="T457" id="Seg_5962" s="T456">n.[n:case]</ta>
            <ta e="T458" id="Seg_5963" s="T457">v-v:tense-v:poss.pn</ta>
            <ta e="T459" id="Seg_5964" s="T458">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T460" id="Seg_5965" s="T459">v-v:tense-v:poss.pn</ta>
            <ta e="T461" id="Seg_5966" s="T460">v-v:(ins)-v&gt;v-v:cvb-v:pred.pn</ta>
            <ta e="T463" id="Seg_5967" s="T462">n-n:case</ta>
            <ta e="T464" id="Seg_5968" s="T463">n-n:(num)-n:case</ta>
            <ta e="T465" id="Seg_5969" s="T464">n-n:(num)-n:case</ta>
            <ta e="T466" id="Seg_5970" s="T465">v-v&gt;v-v:cvb</ta>
            <ta e="T467" id="Seg_5971" s="T466">dempro</ta>
            <ta e="T468" id="Seg_5972" s="T467">v-v:ptcp</ta>
            <ta e="T469" id="Seg_5973" s="T468">v-v:tense-v:poss.pn</ta>
            <ta e="T470" id="Seg_5974" s="T469">dempro.[pro:case]</ta>
            <ta e="T471" id="Seg_5975" s="T470">cardnum</ta>
            <ta e="T472" id="Seg_5976" s="T471">n.[n:case]</ta>
            <ta e="T473" id="Seg_5977" s="T472">n-n:(poss).[n:case]</ta>
            <ta e="T481" id="Seg_5978" s="T480">propr.[n:case]</ta>
            <ta e="T482" id="Seg_5979" s="T481">cardnum</ta>
            <ta e="T483" id="Seg_5980" s="T482">n-n&gt;adj-n:case</ta>
            <ta e="T484" id="Seg_5981" s="T483">v-v:tense-v:tense</ta>
            <ta e="T485" id="Seg_5982" s="T484">adv</ta>
            <ta e="T486" id="Seg_5983" s="T485">cardnum</ta>
            <ta e="T487" id="Seg_5984" s="T486">n-n&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T488" id="Seg_5985" s="T487">adv</ta>
            <ta e="T489" id="Seg_5986" s="T488">v-v:tense-v:poss.pn</ta>
            <ta e="T490" id="Seg_5987" s="T489">ptcl</ta>
            <ta e="T491" id="Seg_5988" s="T490">propr-n:case</ta>
            <ta e="T492" id="Seg_5989" s="T491">n-n:(poss).[n:case]</ta>
            <ta e="T493" id="Seg_5990" s="T492">n.[n:case]</ta>
            <ta e="T494" id="Seg_5991" s="T493">v-v:ptcp</ta>
            <ta e="T495" id="Seg_5992" s="T494">v-v:tense.[v:pred.pn]</ta>
            <ta e="T496" id="Seg_5993" s="T495">ptcl</ta>
            <ta e="T497" id="Seg_5994" s="T496">ptcl</ta>
            <ta e="T498" id="Seg_5995" s="T497">dempro-pro:case</ta>
            <ta e="T499" id="Seg_5996" s="T498">n-n&gt;v-v:ptcp.[v:(poss)]-v:(case)</ta>
            <ta e="T500" id="Seg_5997" s="T499">ptcl</ta>
            <ta e="T501" id="Seg_5998" s="T500">v-v:tense.[v:pred.pn]</ta>
            <ta e="T502" id="Seg_5999" s="T501">ptcl</ta>
            <ta e="T503" id="Seg_6000" s="T502">dempro</ta>
            <ta e="T504" id="Seg_6001" s="T503">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T505" id="Seg_6002" s="T504">dempro</ta>
            <ta e="T506" id="Seg_6003" s="T505">v-v:cvb-v:pred.pn</ta>
            <ta e="T507" id="Seg_6004" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_6005" s="T507">n-n:(poss).[n:case]</ta>
            <ta e="T509" id="Seg_6006" s="T508">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T510" id="Seg_6007" s="T509">n.[n:case]</ta>
            <ta e="T511" id="Seg_6008" s="T510">v-v:tense-v:poss.pn</ta>
            <ta e="T513" id="Seg_6009" s="T512">ptcl</ta>
            <ta e="T514" id="Seg_6010" s="T513">n-n:(poss).[n:case]</ta>
            <ta e="T515" id="Seg_6011" s="T514">ptcl</ta>
            <ta e="T516" id="Seg_6012" s="T515">propr-n:case</ta>
            <ta e="T517" id="Seg_6013" s="T516">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T518" id="Seg_6014" s="T517">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T519" id="Seg_6015" s="T518">adv</ta>
            <ta e="T520" id="Seg_6016" s="T519">cardnum</ta>
            <ta e="T521" id="Seg_6017" s="T520">n-n&gt;adj.[n:case]</ta>
            <ta e="T522" id="Seg_6018" s="T521">n-n:case</ta>
            <ta e="T523" id="Seg_6019" s="T522">post</ta>
            <ta e="T524" id="Seg_6020" s="T523">n.[n:case]</ta>
            <ta e="T529" id="Seg_6021" s="T528">n-n:case</ta>
            <ta e="T530" id="Seg_6022" s="T529">n-n:case</ta>
            <ta e="T531" id="Seg_6023" s="T530">v-v:tense-v:pred.pn</ta>
            <ta e="T532" id="Seg_6024" s="T531">n-n:case</ta>
            <ta e="T533" id="Seg_6025" s="T532">n-n&gt;n.[n:case]</ta>
            <ta e="T534" id="Seg_6026" s="T533">n-n:(poss).[n:case]</ta>
            <ta e="T536" id="Seg_6027" s="T535">adj-n:(poss).[n:case]</ta>
            <ta e="T537" id="Seg_6028" s="T536">propr-n:case</ta>
            <ta e="T538" id="Seg_6029" s="T537">v-v:tense-v:poss.pn</ta>
            <ta e="T539" id="Seg_6030" s="T538">adv</ta>
            <ta e="T540" id="Seg_6031" s="T539">v-v:tense-v:poss.pn</ta>
            <ta e="T541" id="Seg_6032" s="T540">n</ta>
            <ta e="T542" id="Seg_6033" s="T541">n-n:case</ta>
            <ta e="T543" id="Seg_6034" s="T542">n.[n:case]</ta>
            <ta e="T544" id="Seg_6035" s="T543">v-v&gt;v-v:cvb</ta>
            <ta e="T545" id="Seg_6036" s="T544">n-n:(poss).[n:case]</ta>
            <ta e="T546" id="Seg_6037" s="T545">adv</ta>
            <ta e="T547" id="Seg_6038" s="T546">v-v:tense-v:pred.pn</ta>
            <ta e="T548" id="Seg_6039" s="T547">propr-n:case</ta>
            <ta e="T549" id="Seg_6040" s="T548">adj-n:(poss).[n:case]</ta>
            <ta e="T550" id="Seg_6041" s="T549">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T551" id="Seg_6042" s="T550">n.[n:case]</ta>
            <ta e="T552" id="Seg_6043" s="T551">adj-n:(poss).[n:case]</ta>
            <ta e="T553" id="Seg_6044" s="T552">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T554" id="Seg_6045" s="T553">n.[n:case]</ta>
            <ta e="T555" id="Seg_6046" s="T554">ptcl</ta>
            <ta e="T556" id="Seg_6047" s="T555">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T557" id="Seg_6048" s="T556">ptcl</ta>
            <ta e="T559" id="Seg_6049" s="T558">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T560" id="Seg_6050" s="T559">n.[n:case]</ta>
            <ta e="T561" id="Seg_6051" s="T560">interj</ta>
            <ta e="T562" id="Seg_6052" s="T561">n-n:(poss).[n:case]</ta>
            <ta e="T563" id="Seg_6053" s="T562">adv</ta>
            <ta e="T564" id="Seg_6054" s="T563">pers.[pro:case]</ta>
            <ta e="T565" id="Seg_6055" s="T564">n-n:poss-n:case</ta>
            <ta e="T566" id="Seg_6056" s="T565">v-v:tense-v:poss.pn</ta>
            <ta e="T567" id="Seg_6057" s="T566">adv</ta>
            <ta e="T568" id="Seg_6058" s="T567">n-n:case</ta>
            <ta e="T569" id="Seg_6059" s="T568">v-v:tense-v:poss.pn</ta>
            <ta e="T570" id="Seg_6060" s="T569">adv</ta>
            <ta e="T572" id="Seg_6061" s="T571">v-v:cvb</ta>
            <ta e="T573" id="Seg_6062" s="T572">v-v:tense-v:poss.pn</ta>
            <ta e="T574" id="Seg_6063" s="T573">adj-n:(poss).[n:case]</ta>
         </annotation>
         <annotation name="ps" tierref="ps-KiES">
            <ta e="T2" id="Seg_6064" s="T1">ptcl</ta>
            <ta e="T3" id="Seg_6065" s="T2">propr</ta>
            <ta e="T4" id="Seg_6066" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_6067" s="T4">cop</ta>
            <ta e="T6" id="Seg_6068" s="T5">adj</ta>
            <ta e="T7" id="Seg_6069" s="T6">cop</ta>
            <ta e="T8" id="Seg_6070" s="T7">pers</ta>
            <ta e="T9" id="Seg_6071" s="T8">propr</ta>
            <ta e="T11" id="Seg_6072" s="T9">v</ta>
            <ta e="T12" id="Seg_6073" s="T11">adv</ta>
            <ta e="T13" id="Seg_6074" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_6075" s="T13">dempro</ta>
            <ta e="T15" id="Seg_6076" s="T14">interj</ta>
            <ta e="T16" id="Seg_6077" s="T15">dempro</ta>
            <ta e="T17" id="Seg_6078" s="T16">n</ta>
            <ta e="T18" id="Seg_6079" s="T17">v</ta>
            <ta e="T19" id="Seg_6080" s="T18">adv</ta>
            <ta e="T20" id="Seg_6081" s="T19">dempro</ta>
            <ta e="T21" id="Seg_6082" s="T20">n</ta>
            <ta e="T22" id="Seg_6083" s="T21">n</ta>
            <ta e="T23" id="Seg_6084" s="T22">v</ta>
            <ta e="T24" id="Seg_6085" s="T23">dempro</ta>
            <ta e="T25" id="Seg_6086" s="T24">adv</ta>
            <ta e="T26" id="Seg_6087" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_6088" s="T26">n</ta>
            <ta e="T28" id="Seg_6089" s="T27">v</ta>
            <ta e="T29" id="Seg_6090" s="T28">aux</ta>
            <ta e="T30" id="Seg_6091" s="T29">v</ta>
            <ta e="T31" id="Seg_6092" s="T30">v</ta>
            <ta e="T32" id="Seg_6093" s="T31">dempro</ta>
            <ta e="T33" id="Seg_6094" s="T32">que</ta>
            <ta e="T34" id="Seg_6095" s="T33">propr</ta>
            <ta e="T35" id="Seg_6096" s="T34">v</ta>
            <ta e="T36" id="Seg_6097" s="T35">dempro</ta>
            <ta e="T37" id="Seg_6098" s="T36">adv</ta>
            <ta e="T38" id="Seg_6099" s="T37">n</ta>
            <ta e="T39" id="Seg_6100" s="T38">pers</ta>
            <ta e="T43" id="Seg_6101" s="T42">dempro</ta>
            <ta e="T44" id="Seg_6102" s="T43">ptcl</ta>
            <ta e="T48" id="Seg_6103" s="T47">adv</ta>
            <ta e="T49" id="Seg_6104" s="T48">n</ta>
            <ta e="T50" id="Seg_6105" s="T49">v</ta>
            <ta e="T51" id="Seg_6106" s="T50">pers</ta>
            <ta e="T52" id="Seg_6107" s="T51">n</ta>
            <ta e="T64" id="Seg_6108" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_6109" s="T64">adv</ta>
            <ta e="T66" id="Seg_6110" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_6111" s="T66">propr</ta>
            <ta e="T68" id="Seg_6112" s="T67">adj</ta>
            <ta e="T69" id="Seg_6113" s="T68">adv</ta>
            <ta e="T70" id="Seg_6114" s="T69">n</ta>
            <ta e="T71" id="Seg_6115" s="T70">v</ta>
            <ta e="T72" id="Seg_6116" s="T71">aux</ta>
            <ta e="T73" id="Seg_6117" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_6118" s="T73">que</ta>
            <ta e="T75" id="Seg_6119" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_6120" s="T75">v</ta>
            <ta e="T77" id="Seg_6121" s="T76">n</ta>
            <ta e="T78" id="Seg_6122" s="T77">que</ta>
            <ta e="T79" id="Seg_6123" s="T78">n</ta>
            <ta e="T92" id="Seg_6124" s="T91">interj</ta>
            <ta e="T94" id="Seg_6125" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_6126" s="T94">que</ta>
            <ta e="T96" id="Seg_6127" s="T95">cop</ta>
            <ta e="T98" id="Seg_6128" s="T97">v</ta>
            <ta e="T99" id="Seg_6129" s="T98">v</ta>
            <ta e="T100" id="Seg_6130" s="T99">n</ta>
            <ta e="T101" id="Seg_6131" s="T100">ptcl</ta>
            <ta e="T102" id="Seg_6132" s="T101">n</ta>
            <ta e="T103" id="Seg_6133" s="T102">v</ta>
            <ta e="T104" id="Seg_6134" s="T103">v</ta>
            <ta e="T105" id="Seg_6135" s="T104">v</ta>
            <ta e="T106" id="Seg_6136" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_6137" s="T106">n</ta>
            <ta e="T108" id="Seg_6138" s="T107">v</ta>
            <ta e="T109" id="Seg_6139" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_6140" s="T109">dempro</ta>
            <ta e="T111" id="Seg_6141" s="T110">n</ta>
            <ta e="T112" id="Seg_6142" s="T111">v</ta>
            <ta e="T113" id="Seg_6143" s="T112">adj</ta>
            <ta e="T114" id="Seg_6144" s="T113">n</ta>
            <ta e="T115" id="Seg_6145" s="T114">dempro</ta>
            <ta e="T116" id="Seg_6146" s="T115">n</ta>
            <ta e="T117" id="Seg_6147" s="T116">n</ta>
            <ta e="T118" id="Seg_6148" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_6149" s="T118">v</ta>
            <ta e="T120" id="Seg_6150" s="T119">n</ta>
            <ta e="T121" id="Seg_6151" s="T120">adv</ta>
            <ta e="T122" id="Seg_6152" s="T121">n</ta>
            <ta e="T123" id="Seg_6153" s="T122">pers</ta>
            <ta e="T124" id="Seg_6154" s="T123">n</ta>
            <ta e="T125" id="Seg_6155" s="T124">v</ta>
            <ta e="T126" id="Seg_6156" s="T125">ptcl</ta>
            <ta e="T127" id="Seg_6157" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_6158" s="T127">dempro</ta>
            <ta e="T129" id="Seg_6159" s="T128">v</ta>
            <ta e="T130" id="Seg_6160" s="T129">adv</ta>
            <ta e="T131" id="Seg_6161" s="T130">dempro</ta>
            <ta e="T132" id="Seg_6162" s="T131">v</ta>
            <ta e="T133" id="Seg_6163" s="T132">ptcl</ta>
            <ta e="T134" id="Seg_6164" s="T133">interj</ta>
            <ta e="T135" id="Seg_6165" s="T134">que</ta>
            <ta e="T136" id="Seg_6166" s="T135">dempro</ta>
            <ta e="T137" id="Seg_6167" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_6168" s="T137">dempro</ta>
            <ta e="T139" id="Seg_6169" s="T138">cardnum</ta>
            <ta e="T140" id="Seg_6170" s="T139">n</ta>
            <ta e="T141" id="Seg_6171" s="T140">v</ta>
            <ta e="T142" id="Seg_6172" s="T141">n</ta>
            <ta e="T143" id="Seg_6173" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_6174" s="T143">ptcl</ta>
            <ta e="T145" id="Seg_6175" s="T144">ptcl</ta>
            <ta e="T146" id="Seg_6176" s="T145">dempro</ta>
            <ta e="T147" id="Seg_6177" s="T146">v</ta>
            <ta e="T151" id="Seg_6178" s="T150">que</ta>
            <ta e="T152" id="Seg_6179" s="T151">post</ta>
            <ta e="T157" id="Seg_6180" s="T156">que</ta>
            <ta e="T158" id="Seg_6181" s="T157">dempro</ta>
            <ta e="T159" id="Seg_6182" s="T158">adv</ta>
            <ta e="T160" id="Seg_6183" s="T159">dempro</ta>
            <ta e="T161" id="Seg_6184" s="T160">adv</ta>
            <ta e="T162" id="Seg_6185" s="T161">dempro</ta>
            <ta e="T163" id="Seg_6186" s="T162">que</ta>
            <ta e="T164" id="Seg_6187" s="T163">dempro</ta>
            <ta e="T165" id="Seg_6188" s="T164">n</ta>
            <ta e="T166" id="Seg_6189" s="T165">v</ta>
            <ta e="T171" id="Seg_6190" s="T168">ptcl</ta>
            <ta e="T172" id="Seg_6191" s="T171">n</ta>
            <ta e="T173" id="Seg_6192" s="T172">n</ta>
            <ta e="T174" id="Seg_6193" s="T173">n</ta>
            <ta e="T175" id="Seg_6194" s="T174">v</ta>
            <ta e="T176" id="Seg_6195" s="T175">v</ta>
            <ta e="T177" id="Seg_6196" s="T176">adv</ta>
            <ta e="T178" id="Seg_6197" s="T177">cardnum</ta>
            <ta e="T179" id="Seg_6198" s="T178">adj</ta>
            <ta e="T180" id="Seg_6199" s="T179">v</ta>
            <ta e="T181" id="Seg_6200" s="T180">dempro</ta>
            <ta e="T182" id="Seg_6201" s="T181">n</ta>
            <ta e="T183" id="Seg_6202" s="T182">cardnum</ta>
            <ta e="T184" id="Seg_6203" s="T183">n</ta>
            <ta e="T185" id="Seg_6204" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_6205" s="T185">pers</ta>
            <ta e="T187" id="Seg_6206" s="T186">v</ta>
            <ta e="T188" id="Seg_6207" s="T187">ptcl</ta>
            <ta e="T189" id="Seg_6208" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_6209" s="T189">n</ta>
            <ta e="T191" id="Seg_6210" s="T190">cop</ta>
            <ta e="T192" id="Seg_6211" s="T191">pers</ta>
            <ta e="T193" id="Seg_6212" s="T192">que</ta>
            <ta e="T194" id="Seg_6213" s="T193">ptcl</ta>
            <ta e="T195" id="Seg_6214" s="T194">v</ta>
            <ta e="T196" id="Seg_6215" s="T195">v</ta>
            <ta e="T197" id="Seg_6216" s="T196">n</ta>
            <ta e="T198" id="Seg_6217" s="T197">interj</ta>
            <ta e="T199" id="Seg_6218" s="T198">n</ta>
            <ta e="T200" id="Seg_6219" s="T199">n</ta>
            <ta e="T201" id="Seg_6220" s="T200">n</ta>
            <ta e="T202" id="Seg_6221" s="T201">v</ta>
            <ta e="T203" id="Seg_6222" s="T202">ptcl</ta>
            <ta e="T204" id="Seg_6223" s="T203">dempro</ta>
            <ta e="T205" id="Seg_6224" s="T204">ptcl</ta>
            <ta e="T206" id="Seg_6225" s="T205">n</ta>
            <ta e="T207" id="Seg_6226" s="T206">v</ta>
            <ta e="T208" id="Seg_6227" s="T207">v</ta>
            <ta e="T209" id="Seg_6228" s="T208">ptcl</ta>
            <ta e="T210" id="Seg_6229" s="T209">pers</ta>
            <ta e="T211" id="Seg_6230" s="T210">que</ta>
            <ta e="T212" id="Seg_6231" s="T211">cop</ta>
            <ta e="T213" id="Seg_6232" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_6233" s="T213">dempro</ta>
            <ta e="T215" id="Seg_6234" s="T214">dempro</ta>
            <ta e="T216" id="Seg_6235" s="T215">cardnum</ta>
            <ta e="T217" id="Seg_6236" s="T216">cardnum</ta>
            <ta e="T218" id="Seg_6237" s="T217">n</ta>
            <ta e="T219" id="Seg_6238" s="T218">pers</ta>
            <ta e="T220" id="Seg_6239" s="T219">v</ta>
            <ta e="T221" id="Seg_6240" s="T220">ptcl</ta>
            <ta e="T222" id="Seg_6241" s="T221">interj</ta>
            <ta e="T223" id="Seg_6242" s="T222">propr</ta>
            <ta e="T224" id="Seg_6243" s="T223">v</ta>
            <ta e="T225" id="Seg_6244" s="T224">v</ta>
            <ta e="T231" id="Seg_6245" s="T230">ptcl</ta>
            <ta e="T232" id="Seg_6246" s="T231">interj</ta>
            <ta e="T233" id="Seg_6247" s="T232">adv</ta>
            <ta e="T237" id="Seg_6248" s="T236">ptcl</ta>
            <ta e="T238" id="Seg_6249" s="T237">adv</ta>
            <ta e="T239" id="Seg_6250" s="T238">n</ta>
            <ta e="T240" id="Seg_6251" s="T239">v</ta>
            <ta e="T241" id="Seg_6252" s="T240">dempro</ta>
            <ta e="T242" id="Seg_6253" s="T241">n</ta>
            <ta e="T243" id="Seg_6254" s="T242">v</ta>
            <ta e="T244" id="Seg_6255" s="T243">n</ta>
            <ta e="T245" id="Seg_6256" s="T244">ptcl</ta>
            <ta e="T246" id="Seg_6257" s="T245">dempro</ta>
            <ta e="T247" id="Seg_6258" s="T246">n</ta>
            <ta e="T248" id="Seg_6259" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_6260" s="T248">propr</ta>
            <ta e="T250" id="Seg_6261" s="T249">v</ta>
            <ta e="T251" id="Seg_6262" s="T250">v</ta>
            <ta e="T253" id="Seg_6263" s="T251">n</ta>
            <ta e="T257" id="Seg_6264" s="T255">interj</ta>
            <ta e="T258" id="Seg_6265" s="T257">propr</ta>
            <ta e="T259" id="Seg_6266" s="T258">cardnum</ta>
            <ta e="T260" id="Seg_6267" s="T259">cardnum</ta>
            <ta e="T261" id="Seg_6268" s="T260">n</ta>
            <ta e="T262" id="Seg_6269" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_6270" s="T262">adv</ta>
            <ta e="T264" id="Seg_6271" s="T263">n</ta>
            <ta e="T265" id="Seg_6272" s="T264">ptcl</ta>
            <ta e="T266" id="Seg_6273" s="T265">n</ta>
            <ta e="T267" id="Seg_6274" s="T266">adj</ta>
            <ta e="T268" id="Seg_6275" s="T267">n</ta>
            <ta e="T269" id="Seg_6276" s="T268">n</ta>
            <ta e="T270" id="Seg_6277" s="T269">ptcl</ta>
            <ta e="T271" id="Seg_6278" s="T270">ptcl</ta>
            <ta e="T272" id="Seg_6279" s="T271">dempro</ta>
            <ta e="T273" id="Seg_6280" s="T272">v</ta>
            <ta e="T274" id="Seg_6281" s="T273">adv</ta>
            <ta e="T275" id="Seg_6282" s="T274">pers</ta>
            <ta e="T276" id="Seg_6283" s="T275">cardnum</ta>
            <ta e="T277" id="Seg_6284" s="T276">n</ta>
            <ta e="T278" id="Seg_6285" s="T277">n</ta>
            <ta e="T279" id="Seg_6286" s="T278">post</ta>
            <ta e="T280" id="Seg_6287" s="T279">v</ta>
            <ta e="T281" id="Seg_6288" s="T280">ptcl</ta>
            <ta e="T282" id="Seg_6289" s="T281">ptcl</ta>
            <ta e="T283" id="Seg_6290" s="T282">dempro</ta>
            <ta e="T284" id="Seg_6291" s="T283">pers</ta>
            <ta e="T285" id="Seg_6292" s="T284">v</ta>
            <ta e="T286" id="Seg_6293" s="T285">v</ta>
            <ta e="T287" id="Seg_6294" s="T286">adv</ta>
            <ta e="T288" id="Seg_6295" s="T287">dempro</ta>
            <ta e="T289" id="Seg_6296" s="T288">cardnum</ta>
            <ta e="T290" id="Seg_6297" s="T289">n</ta>
            <ta e="T291" id="Seg_6298" s="T290">n</ta>
            <ta e="T292" id="Seg_6299" s="T291">ptcl</ta>
            <ta e="T293" id="Seg_6300" s="T292">adv</ta>
            <ta e="T294" id="Seg_6301" s="T293">que</ta>
            <ta e="T295" id="Seg_6302" s="T294">ptcl</ta>
            <ta e="T296" id="Seg_6303" s="T295">cop</ta>
            <ta e="T297" id="Seg_6304" s="T296">n</ta>
            <ta e="T298" id="Seg_6305" s="T297">v</ta>
            <ta e="T299" id="Seg_6306" s="T298">dempro</ta>
            <ta e="T300" id="Seg_6307" s="T299">n</ta>
            <ta e="T301" id="Seg_6308" s="T300">ptcl</ta>
            <ta e="T302" id="Seg_6309" s="T301">adv</ta>
            <ta e="T303" id="Seg_6310" s="T302">v</ta>
            <ta e="T304" id="Seg_6311" s="T303">pers</ta>
            <ta e="T305" id="Seg_6312" s="T304">dempro</ta>
            <ta e="T306" id="Seg_6313" s="T305">v</ta>
            <ta e="T307" id="Seg_6314" s="T306">aux</ta>
            <ta e="T308" id="Seg_6315" s="T307">dempro</ta>
            <ta e="T309" id="Seg_6316" s="T308">adv</ta>
            <ta e="T310" id="Seg_6317" s="T309">n</ta>
            <ta e="T311" id="Seg_6318" s="T310">adj</ta>
            <ta e="T312" id="Seg_6319" s="T311">n</ta>
            <ta e="T313" id="Seg_6320" s="T312">ptcl</ta>
            <ta e="T314" id="Seg_6321" s="T313">dempro</ta>
            <ta e="T315" id="Seg_6322" s="T314">v</ta>
            <ta e="T316" id="Seg_6323" s="T315">cardnum</ta>
            <ta e="T317" id="Seg_6324" s="T316">n</ta>
            <ta e="T318" id="Seg_6325" s="T317">ptcl</ta>
            <ta e="T319" id="Seg_6326" s="T318">ptcl</ta>
            <ta e="T320" id="Seg_6327" s="T319">adv</ta>
            <ta e="T321" id="Seg_6328" s="T320">v</ta>
            <ta e="T322" id="Seg_6329" s="T321">ptcl</ta>
            <ta e="T323" id="Seg_6330" s="T322">adv</ta>
            <ta e="T324" id="Seg_6331" s="T323">cardnum</ta>
            <ta e="T325" id="Seg_6332" s="T324">adj</ta>
            <ta e="T326" id="Seg_6333" s="T325">cop</ta>
            <ta e="T327" id="Seg_6334" s="T326">ptcl</ta>
            <ta e="T328" id="Seg_6335" s="T327">pers</ta>
            <ta e="T330" id="Seg_6336" s="T329">emphpro</ta>
            <ta e="T331" id="Seg_6337" s="T330">cardnum</ta>
            <ta e="T332" id="Seg_6338" s="T331">adj</ta>
            <ta e="T333" id="Seg_6339" s="T332">ptcl</ta>
            <ta e="T334" id="Seg_6340" s="T333">dempro</ta>
            <ta e="T335" id="Seg_6341" s="T334">cardnum</ta>
            <ta e="T336" id="Seg_6342" s="T335">adj</ta>
            <ta e="T337" id="Seg_6343" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_6344" s="T337">n</ta>
            <ta e="T339" id="Seg_6345" s="T338">adj</ta>
            <ta e="T340" id="Seg_6346" s="T339">n</ta>
            <ta e="T341" id="Seg_6347" s="T340">adj</ta>
            <ta e="T342" id="Seg_6348" s="T341">adv</ta>
            <ta e="T343" id="Seg_6349" s="T342">dempro</ta>
            <ta e="T344" id="Seg_6350" s="T343">cardnum</ta>
            <ta e="T345" id="Seg_6351" s="T344">adj</ta>
            <ta e="T346" id="Seg_6352" s="T345">emphpro</ta>
            <ta e="T347" id="Seg_6353" s="T346">n</ta>
            <ta e="T348" id="Seg_6354" s="T347">adj</ta>
            <ta e="T349" id="Seg_6355" s="T348">cardnum</ta>
            <ta e="T350" id="Seg_6356" s="T349">n</ta>
            <ta e="T351" id="Seg_6357" s="T350">v</ta>
            <ta e="T352" id="Seg_6358" s="T351">posspr</ta>
            <ta e="T353" id="Seg_6359" s="T352">cardnum</ta>
            <ta e="T354" id="Seg_6360" s="T353">n</ta>
            <ta e="T355" id="Seg_6361" s="T354">ptcl</ta>
            <ta e="T360" id="Seg_6362" s="T359">adj</ta>
            <ta e="T361" id="Seg_6363" s="T360">cop</ta>
            <ta e="T362" id="Seg_6364" s="T361">dempro</ta>
            <ta e="T363" id="Seg_6365" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_6366" s="T363">dempro</ta>
            <ta e="T365" id="Seg_6367" s="T364">v</ta>
            <ta e="T366" id="Seg_6368" s="T365">ptcl</ta>
            <ta e="T367" id="Seg_6369" s="T366">dempro</ta>
            <ta e="T368" id="Seg_6370" s="T367">v</ta>
            <ta e="T369" id="Seg_6371" s="T368">ptcl</ta>
            <ta e="T370" id="Seg_6372" s="T369">n</ta>
            <ta e="T371" id="Seg_6373" s="T370">v</ta>
            <ta e="T372" id="Seg_6374" s="T371">v</ta>
            <ta e="T373" id="Seg_6375" s="T372">v</ta>
            <ta e="T374" id="Seg_6376" s="T373">dempro</ta>
            <ta e="T375" id="Seg_6377" s="T374">n</ta>
            <ta e="T376" id="Seg_6378" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_6379" s="T376">n</ta>
            <ta e="T378" id="Seg_6380" s="T377">ptcl</ta>
            <ta e="T379" id="Seg_6381" s="T378">ptcl</ta>
            <ta e="T380" id="Seg_6382" s="T379">que</ta>
            <ta e="T381" id="Seg_6383" s="T380">ptcl</ta>
            <ta e="T382" id="Seg_6384" s="T381">ptcl</ta>
            <ta e="T383" id="Seg_6385" s="T382">cop</ta>
            <ta e="T384" id="Seg_6386" s="T383">n</ta>
            <ta e="T385" id="Seg_6387" s="T384">v</ta>
            <ta e="T386" id="Seg_6388" s="T385">aux</ta>
            <ta e="T387" id="Seg_6389" s="T386">dempro</ta>
            <ta e="T388" id="Seg_6390" s="T387">post</ta>
            <ta e="T389" id="Seg_6391" s="T388">n</ta>
            <ta e="T390" id="Seg_6392" s="T389">ptcl</ta>
            <ta e="T391" id="Seg_6393" s="T390">adv</ta>
            <ta e="T392" id="Seg_6394" s="T391">ptcl</ta>
            <ta e="T393" id="Seg_6395" s="T392">v</ta>
            <ta e="T394" id="Seg_6396" s="T393">ptcl</ta>
            <ta e="T395" id="Seg_6397" s="T394">aux</ta>
            <ta e="T396" id="Seg_6398" s="T395">v</ta>
            <ta e="T397" id="Seg_6399" s="T396">aux</ta>
            <ta e="T399" id="Seg_6400" s="T398">v</ta>
            <ta e="T400" id="Seg_6401" s="T399">aux</ta>
            <ta e="T401" id="Seg_6402" s="T400">n</ta>
            <ta e="T402" id="Seg_6403" s="T401">n</ta>
            <ta e="T403" id="Seg_6404" s="T402">n</ta>
            <ta e="T404" id="Seg_6405" s="T403">dempro</ta>
            <ta e="T405" id="Seg_6406" s="T404">v</ta>
            <ta e="T406" id="Seg_6407" s="T405">adv</ta>
            <ta e="T407" id="Seg_6408" s="T406">pers</ta>
            <ta e="T408" id="Seg_6409" s="T407">v</ta>
            <ta e="T409" id="Seg_6410" s="T408">ptcl</ta>
            <ta e="T410" id="Seg_6411" s="T409">dempro</ta>
            <ta e="T411" id="Seg_6412" s="T410">dempro</ta>
            <ta e="T412" id="Seg_6413" s="T411">v</ta>
            <ta e="T413" id="Seg_6414" s="T412">n</ta>
            <ta e="T414" id="Seg_6415" s="T413">v</ta>
            <ta e="T415" id="Seg_6416" s="T414">ptcl</ta>
            <ta e="T416" id="Seg_6417" s="T415">adj</ta>
            <ta e="T422" id="Seg_6418" s="T421">ptcl</ta>
            <ta e="T423" id="Seg_6419" s="T422">v</ta>
            <ta e="T424" id="Seg_6420" s="T423">que</ta>
            <ta e="T425" id="Seg_6421" s="T424">v</ta>
            <ta e="T426" id="Seg_6422" s="T425">conj</ta>
            <ta e="T427" id="Seg_6423" s="T426">v</ta>
            <ta e="T428" id="Seg_6424" s="T427">ptcl</ta>
            <ta e="T429" id="Seg_6425" s="T428">dempro</ta>
            <ta e="T430" id="Seg_6426" s="T429">post</ta>
            <ta e="T431" id="Seg_6427" s="T430">cardnum</ta>
            <ta e="T432" id="Seg_6428" s="T431">n</ta>
            <ta e="T433" id="Seg_6429" s="T432">cop</ta>
            <ta e="T434" id="Seg_6430" s="T433">ptcl</ta>
            <ta e="T435" id="Seg_6431" s="T434">adj</ta>
            <ta e="T436" id="Seg_6432" s="T435">n</ta>
            <ta e="T437" id="Seg_6433" s="T436">ptcl</ta>
            <ta e="T438" id="Seg_6434" s="T437">adv</ta>
            <ta e="T439" id="Seg_6435" s="T438">dempro</ta>
            <ta e="T443" id="Seg_6436" s="T442">ptcl</ta>
            <ta e="T444" id="Seg_6437" s="T443">n</ta>
            <ta e="T445" id="Seg_6438" s="T444">v</ta>
            <ta e="T446" id="Seg_6439" s="T445">aux</ta>
            <ta e="T447" id="Seg_6440" s="T446">ptcl</ta>
            <ta e="T448" id="Seg_6441" s="T447">cardnum</ta>
            <ta e="T449" id="Seg_6442" s="T448">n</ta>
            <ta e="T450" id="Seg_6443" s="T449">v</ta>
            <ta e="T451" id="Seg_6444" s="T450">ptcl</ta>
            <ta e="T452" id="Seg_6445" s="T451">dempro</ta>
            <ta e="T453" id="Seg_6446" s="T452">ordnum</ta>
            <ta e="T454" id="Seg_6447" s="T453">ordnum</ta>
            <ta e="T455" id="Seg_6448" s="T454">n</ta>
            <ta e="T456" id="Seg_6449" s="T455">n</ta>
            <ta e="T457" id="Seg_6450" s="T456">n</ta>
            <ta e="T458" id="Seg_6451" s="T457">cop</ta>
            <ta e="T459" id="Seg_6452" s="T458">dempro</ta>
            <ta e="T460" id="Seg_6453" s="T459">v</ta>
            <ta e="T461" id="Seg_6454" s="T460">v</ta>
            <ta e="T463" id="Seg_6455" s="T462">n</ta>
            <ta e="T464" id="Seg_6456" s="T463">n</ta>
            <ta e="T465" id="Seg_6457" s="T464">n</ta>
            <ta e="T466" id="Seg_6458" s="T465">v</ta>
            <ta e="T467" id="Seg_6459" s="T466">dempro</ta>
            <ta e="T468" id="Seg_6460" s="T467">v</ta>
            <ta e="T469" id="Seg_6461" s="T468">aux</ta>
            <ta e="T470" id="Seg_6462" s="T469">dempro</ta>
            <ta e="T471" id="Seg_6463" s="T470">cardnum</ta>
            <ta e="T472" id="Seg_6464" s="T471">n</ta>
            <ta e="T473" id="Seg_6465" s="T472">n</ta>
            <ta e="T481" id="Seg_6466" s="T480">propr</ta>
            <ta e="T482" id="Seg_6467" s="T481">cardnum</ta>
            <ta e="T483" id="Seg_6468" s="T482">adj</ta>
            <ta e="T484" id="Seg_6469" s="T483">v</ta>
            <ta e="T485" id="Seg_6470" s="T484">adv</ta>
            <ta e="T486" id="Seg_6471" s="T485">cardnum</ta>
            <ta e="T487" id="Seg_6472" s="T486">v</ta>
            <ta e="T488" id="Seg_6473" s="T487">adv</ta>
            <ta e="T489" id="Seg_6474" s="T488">v</ta>
            <ta e="T490" id="Seg_6475" s="T489">ptcl</ta>
            <ta e="T491" id="Seg_6476" s="T490">propr</ta>
            <ta e="T492" id="Seg_6477" s="T491">n</ta>
            <ta e="T493" id="Seg_6478" s="T492">n</ta>
            <ta e="T494" id="Seg_6479" s="T493">v</ta>
            <ta e="T495" id="Seg_6480" s="T494">aux</ta>
            <ta e="T496" id="Seg_6481" s="T495">ptcl</ta>
            <ta e="T497" id="Seg_6482" s="T496">ptcl</ta>
            <ta e="T498" id="Seg_6483" s="T497">dempro</ta>
            <ta e="T499" id="Seg_6484" s="T498">v</ta>
            <ta e="T500" id="Seg_6485" s="T499">ptcl</ta>
            <ta e="T501" id="Seg_6486" s="T500">aux</ta>
            <ta e="T502" id="Seg_6487" s="T501">ptcl</ta>
            <ta e="T503" id="Seg_6488" s="T502">dempro</ta>
            <ta e="T504" id="Seg_6489" s="T503">v</ta>
            <ta e="T505" id="Seg_6490" s="T504">dempro</ta>
            <ta e="T506" id="Seg_6491" s="T505">v</ta>
            <ta e="T507" id="Seg_6492" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_6493" s="T507">n</ta>
            <ta e="T509" id="Seg_6494" s="T508">ordnum</ta>
            <ta e="T510" id="Seg_6495" s="T509">n</ta>
            <ta e="T511" id="Seg_6496" s="T510">cop</ta>
            <ta e="T513" id="Seg_6497" s="T512">ptcl</ta>
            <ta e="T514" id="Seg_6498" s="T513">n</ta>
            <ta e="T515" id="Seg_6499" s="T514">ptcl</ta>
            <ta e="T516" id="Seg_6500" s="T515">propr</ta>
            <ta e="T517" id="Seg_6501" s="T516">v</ta>
            <ta e="T518" id="Seg_6502" s="T517">dempro</ta>
            <ta e="T519" id="Seg_6503" s="T518">adv</ta>
            <ta e="T520" id="Seg_6504" s="T519">cardnum</ta>
            <ta e="T521" id="Seg_6505" s="T520">adj</ta>
            <ta e="T522" id="Seg_6506" s="T521">n</ta>
            <ta e="T523" id="Seg_6507" s="T522">post</ta>
            <ta e="T524" id="Seg_6508" s="T523">n</ta>
            <ta e="T529" id="Seg_6509" s="T528">n</ta>
            <ta e="T530" id="Seg_6510" s="T529">n</ta>
            <ta e="T531" id="Seg_6511" s="T530">v</ta>
            <ta e="T532" id="Seg_6512" s="T531">n</ta>
            <ta e="T533" id="Seg_6513" s="T532">n</ta>
            <ta e="T534" id="Seg_6514" s="T533">n</ta>
            <ta e="T536" id="Seg_6515" s="T535">adj</ta>
            <ta e="T537" id="Seg_6516" s="T536">propr</ta>
            <ta e="T538" id="Seg_6517" s="T537">v</ta>
            <ta e="T539" id="Seg_6518" s="T538">adv</ta>
            <ta e="T540" id="Seg_6519" s="T539">v</ta>
            <ta e="T541" id="Seg_6520" s="T540">n</ta>
            <ta e="T542" id="Seg_6521" s="T541">n</ta>
            <ta e="T543" id="Seg_6522" s="T542">n</ta>
            <ta e="T544" id="Seg_6523" s="T543">v</ta>
            <ta e="T545" id="Seg_6524" s="T544">n</ta>
            <ta e="T546" id="Seg_6525" s="T545">adv</ta>
            <ta e="T547" id="Seg_6526" s="T546">v</ta>
            <ta e="T548" id="Seg_6527" s="T547">propr</ta>
            <ta e="T549" id="Seg_6528" s="T548">adj</ta>
            <ta e="T550" id="Seg_6529" s="T549">ordnum</ta>
            <ta e="T551" id="Seg_6530" s="T550">n</ta>
            <ta e="T552" id="Seg_6531" s="T551">adj</ta>
            <ta e="T553" id="Seg_6532" s="T552">ordnum</ta>
            <ta e="T554" id="Seg_6533" s="T553">n</ta>
            <ta e="T555" id="Seg_6534" s="T554">ptcl</ta>
            <ta e="T556" id="Seg_6535" s="T555">ordnum</ta>
            <ta e="T557" id="Seg_6536" s="T556">ptcl</ta>
            <ta e="T559" id="Seg_6537" s="T558">ordnum</ta>
            <ta e="T560" id="Seg_6538" s="T559">n</ta>
            <ta e="T561" id="Seg_6539" s="T560">interj</ta>
            <ta e="T562" id="Seg_6540" s="T561">n</ta>
            <ta e="T563" id="Seg_6541" s="T562">adv</ta>
            <ta e="T564" id="Seg_6542" s="T563">pers</ta>
            <ta e="T565" id="Seg_6543" s="T564">n</ta>
            <ta e="T566" id="Seg_6544" s="T565">v</ta>
            <ta e="T567" id="Seg_6545" s="T566">adv</ta>
            <ta e="T568" id="Seg_6546" s="T567">n</ta>
            <ta e="T569" id="Seg_6547" s="T568">v</ta>
            <ta e="T570" id="Seg_6548" s="T569">adv</ta>
            <ta e="T572" id="Seg_6549" s="T571">v</ta>
            <ta e="T573" id="Seg_6550" s="T572">v</ta>
            <ta e="T574" id="Seg_6551" s="T573">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-KiES" />
         <annotation name="SyF" tierref="SyF-KiES" />
         <annotation name="IST" tierref="IST-KiES" />
         <annotation name="Top" tierref="Top-KiES" />
         <annotation name="Foc" tierref="Foc-KiES" />
         <annotation name="BOR" tierref="BOR-KiES">
            <ta e="T34" id="Seg_6552" s="T33">RUS:cult</ta>
            <ta e="T52" id="Seg_6553" s="T51">RUS:cult</ta>
            <ta e="T67" id="Seg_6554" s="T66">RUS:cult</ta>
            <ta e="T70" id="Seg_6555" s="T69">RUS:cult</ta>
            <ta e="T77" id="Seg_6556" s="T76">RUS:cult</ta>
            <ta e="T107" id="Seg_6557" s="T106">RUS:cult</ta>
            <ta e="T111" id="Seg_6558" s="T110">RUS:cult</ta>
            <ta e="T114" id="Seg_6559" s="T113">RUS:cult</ta>
            <ta e="T116" id="Seg_6560" s="T115">RUS:cult</ta>
            <ta e="T117" id="Seg_6561" s="T116">RUS:cult</ta>
            <ta e="T118" id="Seg_6562" s="T117">RUS:mod</ta>
            <ta e="T124" id="Seg_6563" s="T123">RUS:cult</ta>
            <ta e="T132" id="Seg_6564" s="T131">RUS:cult</ta>
            <ta e="T142" id="Seg_6565" s="T141">RUS:cult</ta>
            <ta e="T239" id="Seg_6566" s="T238">RUS:cult</ta>
            <ta e="T249" id="Seg_6567" s="T248">NGAN:cult</ta>
            <ta e="T258" id="Seg_6568" s="T257">RUS:cult</ta>
            <ta e="T266" id="Seg_6569" s="T265">RUS:core</ta>
            <ta e="T297" id="Seg_6570" s="T296">RUS:cult</ta>
            <ta e="T310" id="Seg_6571" s="T309">RUS:core</ta>
            <ta e="T384" id="Seg_6572" s="T383">RUS:cult</ta>
            <ta e="T402" id="Seg_6573" s="T401">RUS:cult</ta>
            <ta e="T403" id="Seg_6574" s="T402">RUS:cultEV:gram (DIM)</ta>
            <ta e="T408" id="Seg_6575" s="T407">RUS:cult</ta>
            <ta e="T413" id="Seg_6576" s="T412">EV:gram (DIM)</ta>
            <ta e="T426" id="Seg_6577" s="T425">RUS:gram</ta>
            <ta e="T435" id="Seg_6578" s="T434">RUS:core</ta>
            <ta e="T463" id="Seg_6579" s="T462">RUS:cult</ta>
            <ta e="T465" id="Seg_6580" s="T464">RUS:cult</ta>
            <ta e="T466" id="Seg_6581" s="T465">RUS:core</ta>
            <ta e="T481" id="Seg_6582" s="T480">RUS:cult</ta>
            <ta e="T491" id="Seg_6583" s="T490">RUS:cult</ta>
            <ta e="T499" id="Seg_6584" s="T498">RUS:cult</ta>
            <ta e="T500" id="Seg_6585" s="T499">RUS:mod</ta>
            <ta e="T510" id="Seg_6586" s="T509">RUS:cult</ta>
            <ta e="T513" id="Seg_6587" s="T512">RUS:disc</ta>
            <ta e="T514" id="Seg_6588" s="T513">RUS:cult</ta>
            <ta e="T516" id="Seg_6589" s="T515">RUS:cult</ta>
            <ta e="T530" id="Seg_6590" s="T529">RUS:core</ta>
            <ta e="T537" id="Seg_6591" s="T536">RUS:cult</ta>
            <ta e="T541" id="Seg_6592" s="T540">RUS:cult</ta>
            <ta e="T543" id="Seg_6593" s="T542">RUS:cult</ta>
            <ta e="T548" id="Seg_6594" s="T547">RUS:cult</ta>
            <ta e="T551" id="Seg_6595" s="T550">RUS:cult</ta>
            <ta e="T554" id="Seg_6596" s="T553">RUS:cult</ta>
            <ta e="T560" id="Seg_6597" s="T559">RUS:cult</ta>
            <ta e="T568" id="Seg_6598" s="T567">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-KiES" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-KiES" />
         <annotation name="CS" tierref="CS-KiES" />
         <annotation name="fe" tierref="fe-KiES">
            <ta e="T11" id="Seg_6599" s="T1">– Well, we were in Korgo, we had our land there, I settled(?) in Korgo.</ta>
            <ta e="T24" id="Seg_6600" s="T11">But then, eh, I had a child, then after I had born this boy.</ta>
            <ta e="T36" id="Seg_6601" s="T24">But then my(?) husband became sick, after he had died, we settled down in whatchamacallit, in Syndassko.</ta>
            <ta e="T58" id="Seg_6602" s="T36">Then the chiefs found some work for (?), how will I be, then they gave me work in the kindergarten, there was a kindergarten for the first time in Syndassko.</ta>
            <ta e="T66" id="Seg_6603" s="T63">– Yes, in Dolgan.</ta>
            <ta e="T72" id="Seg_6604" s="T66">In Syndassko there was a kindergarten for the first time.</ta>
            <ta e="T79" id="Seg_6605" s="T72">I don't know anything, a kindergarten, what's that?</ta>
            <ta e="T84" id="Seg_6606" s="T79">We don't know anything, we people of the tundra.</ta>
            <ta e="T90" id="Seg_6607" s="T84">Nevertheless it feeds [me], how..</ta>
            <ta e="T97" id="Seg_6608" s="T91">– Oh, (…) how do we go on (…)?</ta>
            <ta e="T100" id="Seg_6609" s="T97">I can't speak, to the children.</ta>
            <ta e="T105" id="Seg_6610" s="T100">I can't cook for the children, I say.</ta>
            <ta e="T114" id="Seg_6611" s="T105">There are no cooks, they say, the kindergarten has just started, in the new kindergarten.</ta>
            <ta e="T120" id="Seg_6612" s="T114">One needs cooks there, the chiefs say.</ta>
            <ta e="T126" id="Seg_6613" s="T120">And so the chiefs made a cook out of me.</ta>
            <ta e="T144" id="Seg_6614" s="T126">Well, so we were living, I was a cook, eh, my two children are going to kindergarten, yes.</ta>
            <ta e="T148" id="Seg_6615" s="T144">Well, they are going (…).</ta>
            <ta e="T152" id="Seg_6616" s="T150">With whom?</ta>
            <ta e="T166" id="Seg_6617" s="T156">Why, then later my whatchamacallit, my elder sister died.</ta>
            <ta e="T171" id="Seg_6618" s="T168">– Well.</ta>
            <ta e="T175" id="Seg_6619" s="T171">My mother, my mother's younger sister died.</ta>
            <ta e="T182" id="Seg_6620" s="T175">She became sick, then she stayed with four children.</ta>
            <ta e="T188" id="Seg_6621" s="T182">The four children came to me.</ta>
            <ta e="T196" id="Seg_6622" s="T188">"We will be at our older sister, we don't go anywhere", they say.</ta>
            <ta e="T212" id="Seg_6623" s="T196">My mother said as a legacy: "Stay with your older sister", she said, "if something will happen to me".</ta>
            <ta e="T222" id="Seg_6624" s="T212">Well, so four children came to us.</ta>
            <ta e="T225" id="Seg_6625" s="T222">They went to Khatanga to learn.</ta>
            <ta e="T233" id="Seg_6626" s="T230">– Don't disturb.</ta>
            <ta e="T245" id="Seg_6627" s="T236">Then the children come (to holiday?), the chiefs advise:</ta>
            <ta e="T253" id="Seg_6628" s="T245">"This child should be sent to Popigay", they say, "to some siblings of his."</ta>
            <ta e="T258" id="Seg_6629" s="T255">– Mhm, (Katya?).</ta>
            <ta e="T270" id="Seg_6630" s="T258">One, one child though, here in the tundra there are (our) siblings, an old man and an old woman.</ta>
            <ta e="T273" id="Seg_6631" s="T270">Well, they gave it to them.</ta>
            <ta e="T291" id="Seg_6632" s="T273">Then I stayed with two boys, those don't go (back?) to me, those two boys.</ta>
            <ta e="T304" id="Seg_6633" s="T291">After a while those children came back again to us in the holidays.</ta>
            <ta e="T308" id="Seg_6634" s="T304">They didn't like those ones apparently.</ta>
            <ta e="T328" id="Seg_6635" s="T308">Then again in the tundra the old man and the old woman, the two children went again, they came again to us, then I had eight children.</ta>
            <ta e="T333" id="Seg_6636" s="T329">I have four children myself.</ta>
            <ta e="T337" id="Seg_6637" s="T333">I have two daughters.</ta>
            <ta e="T346" id="Seg_6638" s="T337">My mother is old, my elder brother is old, then I have eight children myself.</ta>
            <ta e="T355" id="Seg_6639" s="T346">The four children of my elder sister stayed, and my four children.</ta>
            <ta e="T359" id="Seg_6640" s="T355">Nadya was the last, she was small.</ta>
            <ta e="T362" id="Seg_6641" s="T359">She was small.</ta>
            <ta e="T373" id="Seg_6642" s="T362">I stayed and so we were living, my mother became old and died, she was sick.</ta>
            <ta e="T383" id="Seg_6643" s="T373">My older brother neither had a wife nor something else.</ta>
            <ta e="T395" id="Seg_6644" s="T383">He worked in the shop, therefore we didn't whatchamacallit [= lack] food.</ta>
            <ta e="T402" id="Seg_6645" s="T395">Working [there] (…) he brought food from the shop.</ta>
            <ta e="T405" id="Seg_6646" s="T402">He got some salary.</ta>
            <ta e="T409" id="Seg_6647" s="T405">And I work as a cook.</ta>
            <ta e="T416" id="Seg_6648" s="T409">So we were living, all grew up, they have wives.</ta>
            <ta e="T427" id="Seg_6649" s="T421">– Well, she got married (to do what?), she got married.</ta>
            <ta e="T434" id="Seg_6650" s="T427">Well, therefore it is eight children.</ta>
            <ta e="T443" id="Seg_6651" s="T434">I am young, at that time these married (?).</ta>
            <ta e="T451" id="Seg_6652" s="T443">I got married, I gave birth to two children.</ta>
            <ta e="T460" id="Seg_6653" s="T451">My third, my third child was a boy, he fell.</ta>
            <ta e="T469" id="Seg_6654" s="T460">(…), carrying ice an coal to the kindergarten, I had a miscarriage.</ta>
            <ta e="T473" id="Seg_6655" s="T469">Between two boys.</ta>
            <ta e="T491" id="Seg_6656" s="T480">– We got Elena when she was six, when she turned seven, I came here, to Dudinka.</ta>
            <ta e="T504" id="Seg_6657" s="T491">My child had gotten a flat, then she had to get a residence permit, and I came.</ta>
            <ta e="T513" id="Seg_6658" s="T504">When I came, my child was in the tenth grade.</ta>
            <ta e="T517" id="Seg_6659" s="T513">Her mother had gotten married in Yakutsk.</ta>
            <ta e="T524" id="Seg_6660" s="T517">Now she has two children, a son and a daughter.</ta>
            <ta e="T534" id="Seg_6661" s="T528">– They are living in the tundra, her husband is a reindeer herder.</ta>
            <ta e="T536" id="Seg_6662" s="T535">That's all.</ta>
            <ta e="T544" id="Seg_6663" s="T536">Now they stayed in Syndassko, they'll go in May when school has finished.</ta>
            <ta e="T548" id="Seg_6664" s="T544">Their children learn there in Syndassko.</ta>
            <ta e="T557" id="Seg_6665" s="T548">One is in the fourth grade, the other in the third or second grade.</ta>
            <ta e="T562" id="Seg_6666" s="T558">In the second grade, eh, the daughter.</ta>
            <ta e="T571" id="Seg_6667" s="T562">Now they stayed in my house, then in May they'll go, back.</ta>
            <ta e="T573" id="Seg_6668" s="T571">They'll nomadize.</ta>
            <ta e="T574" id="Seg_6669" s="T573">That's it.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KiES">
            <ta e="T11" id="Seg_6670" s="T1">– Nun, wir waren in Korgo, wir hatten unser Land dort, ich ließ mich in Korgo nieder(?).</ta>
            <ta e="T24" id="Seg_6671" s="T11">Dann aber, eh, ich bekam ein Kind, dann nachdem ich diesen Jungen geboren hatte.</ta>
            <ta e="T36" id="Seg_6672" s="T24">Dann aber wurde mein(?) Mann krank, nachdem er gestorben war, ließen wir uns in Dings, in Syndassko nieder.</ta>
            <ta e="T58" id="Seg_6673" s="T36">Dann fanden die Chefs eine Arbeit für mich (?), wie ich werde, dann gaben sie mir Arbeit im Kindergarten, einen Kindergarten gab es zum ersten Mal in Syndassko.</ta>
            <ta e="T66" id="Seg_6674" s="T63">– Ja, auf Dolganisch.</ta>
            <ta e="T72" id="Seg_6675" s="T66">In Syndassko stand zum ersten Mal ein Kindergarten.</ta>
            <ta e="T79" id="Seg_6676" s="T72">Ich weiß doch nichts, ein Kindergarten, was ist das?</ta>
            <ta e="T84" id="Seg_6677" s="T79">Nichts wissen wir Tundraleute.</ta>
            <ta e="T90" id="Seg_6678" s="T84">Dennoch ernährt es [mich], wie…</ta>
            <ta e="T97" id="Seg_6679" s="T91">– Oh, (…) wie machen wir weiter (…)?</ta>
            <ta e="T100" id="Seg_6680" s="T97">Ich kann nicht sprechen, mit den Kindern.</ta>
            <ta e="T105" id="Seg_6681" s="T100">Ich kann nicht für die Kinder kochen, sage ich.</ta>
            <ta e="T114" id="Seg_6682" s="T105">Es gibt keine Köche, sagen sie, der Kindergarten ist gerade entstanden, im neuen Kindergarten.</ta>
            <ta e="T120" id="Seg_6683" s="T114">Dort braucht man Köche, sagen die Chefs.</ta>
            <ta e="T126" id="Seg_6684" s="T120">Und so machten mich die Chefs zur Köchin.</ta>
            <ta e="T144" id="Seg_6685" s="T126">Nun, so lebten wir, ich war Köchin, äh, meine zwei Kinder gehen in den Kindergarten, ja.</ta>
            <ta e="T148" id="Seg_6686" s="T144">Nun, sie gehen (…).</ta>
            <ta e="T152" id="Seg_6687" s="T150">Mit wem?</ta>
            <ta e="T166" id="Seg_6688" s="T156">Warum, dann später starb meine Dings, meine ältere Schwester.</ta>
            <ta e="T171" id="Seg_6689" s="T168">– Gut.</ta>
            <ta e="T175" id="Seg_6690" s="T171">Meine Mutter, die jüngere Schwester meiner Mutter starb.</ta>
            <ta e="T182" id="Seg_6691" s="T175">Sie wurde krank, dann blieb sie mit vier Kindern.</ta>
            <ta e="T188" id="Seg_6692" s="T182">Die vier Kinder kamen zu mir.</ta>
            <ta e="T196" id="Seg_6693" s="T188">"Wir werden bei unserer älteren Schwester sein, wir gehen nirgendwo hin", sagen sie.</ta>
            <ta e="T212" id="Seg_6694" s="T196">Meine Mutter hatte als Vermächtnis gesagt: "Bleibt bei eurer älteren Schwester", sagte sie, "wenn etwas mit mir ist".</ta>
            <ta e="T222" id="Seg_6695" s="T212">Nun, so kamen vier Kinder zu uns.</ta>
            <ta e="T225" id="Seg_6696" s="T222">Sie gingen nach Chatanga zum Lernen.</ta>
            <ta e="T233" id="Seg_6697" s="T230">– Stör nicht.</ta>
            <ta e="T245" id="Seg_6698" s="T236">Dann kommen die Kinder (in die Ferien?), die Chefs raten: </ta>
            <ta e="T253" id="Seg_6699" s="T245">"Dieses Kind sollte man nach Popigaj schicken", sagen sie, "zu seinen Verwandten."</ta>
            <ta e="T258" id="Seg_6700" s="T255">– Mhm, (Katja?).</ta>
            <ta e="T270" id="Seg_6701" s="T258">Ein, ein Kind aber, hier in der Tundra gibt es unsere(?) Verwantder, ein alter Mann und eine alte Frau.</ta>
            <ta e="T273" id="Seg_6702" s="T270">Nun denen gaben sie es.</ta>
            <ta e="T291" id="Seg_6703" s="T273">Dann blieb ich mit zwei Jungen, diese gehen nicht zu mir (zurück?), diese zwei Jungen.</ta>
            <ta e="T304" id="Seg_6704" s="T291">Dann nach einiger Zeit kamen diese Kinder wieder in den Ferien zurück zu uns.</ta>
            <ta e="T308" id="Seg_6705" s="T304">Jene gefielen ihnen wohl nicht.</ta>
            <ta e="T328" id="Seg_6706" s="T308">Dann wieder in der Tundra der alte Mann und die alte Frau, die beiden Kinder gingen wieder, sie kamen wieder zu uns, dann hatte ich acht Kinder.</ta>
            <ta e="T333" id="Seg_6707" s="T329">Ich habe vier eigene Kinder.</ta>
            <ta e="T337" id="Seg_6708" s="T333">Ich habe zwei Töchter.</ta>
            <ta e="T346" id="Seg_6709" s="T337">Meine Mutter ist alt, mein älterer Bruder ist alt, dann habe ich acht eigene Kinder.</ta>
            <ta e="T355" id="Seg_6710" s="T346">Die vier Kinder meiner älteren Schwester blieben, und meine vier Kinder.</ta>
            <ta e="T359" id="Seg_6711" s="T355">Nadja ist die letzte, sie war klein.</ta>
            <ta e="T362" id="Seg_6712" s="T359">Sie war klein.</ta>
            <ta e="T373" id="Seg_6713" s="T362">Ich blieb und wir lebten so, meine Mutter wurde alt und starb, sie war krank.</ta>
            <ta e="T383" id="Seg_6714" s="T373">Mein älterer Bruder hatte weder Frau noch sonst etwas.</ta>
            <ta e="T395" id="Seg_6715" s="T383">Er arbeitete im Laden, deshalb dingste [=fehlte] es uns nicht an Nahrung.</ta>
            <ta e="T402" id="Seg_6716" s="T395">[Dort] arbeitend (…) bringt er Essen aus dem Laden.</ta>
            <ta e="T405" id="Seg_6717" s="T402">Er bekam etwas Lohn.</ta>
            <ta e="T409" id="Seg_6718" s="T405">Und ich arbeite als Köchin.</ta>
            <ta e="T416" id="Seg_6719" s="T409">So lebten wir, alle wurden groß, sie haben Frauen.</ta>
            <ta e="T427" id="Seg_6720" s="T421">– Nun, sie heiratete, (um was zu machen?), sie heiratete.</ta>
            <ta e="T434" id="Seg_6721" s="T427">Nun deshalb sind es acht Kinder.</ta>
            <ta e="T443" id="Seg_6722" s="T434">Ich bin jung, damals heirateten diese (?).</ta>
            <ta e="T451" id="Seg_6723" s="T443">Ich heiratete, ich gebar zwei Kinder.</ta>
            <ta e="T460" id="Seg_6724" s="T451">Mein drittes, mein drittes Kind war ein Junge, der fiel.</ta>
            <ta e="T469" id="Seg_6725" s="T460">(…), als ich Eis und Kohle in den Kindergarten schleppte, hatte ich eine Fehlgeburt.</ta>
            <ta e="T473" id="Seg_6726" s="T469">Zwischen zwei Jungen.</ta>
            <ta e="T491" id="Seg_6727" s="T480">– Wir bekamen Elena mit sechs Jahren, als sie sieben wurde, kam ich hierher, nach Dudinka.</ta>
            <ta e="T504" id="Seg_6728" s="T491">Mein Kind hatte eine Wohnung bekommen, dann musste sie eine Wohnerlaubnis einholen, und ich kam.</ta>
            <ta e="T513" id="Seg_6729" s="T504">Als ich kam, war mein Kind in der zehnten Klasse.</ta>
            <ta e="T517" id="Seg_6730" s="T513">Ihre Mutter hatte in Jakutsk geheiratet.</ta>
            <ta e="T524" id="Seg_6731" s="T517">Jetzt hat sie zwei Kinder, einen Sohn und eine Tochter.</ta>
            <ta e="T534" id="Seg_6732" s="T528">– Sie leben in der Tundra, ihr Mann ist Rentierhirte.</ta>
            <ta e="T536" id="Seg_6733" s="T535">Das ist alles.</ta>
            <ta e="T544" id="Seg_6734" s="T536">Jetzt sind sie in Syndassko geblieben, sie gehen im Mai, wenn die Schule aufhört.</ta>
            <ta e="T548" id="Seg_6735" s="T544">Ihre Kinder lernen dort in Syndassko.</ta>
            <ta e="T557" id="Seg_6736" s="T548">Eins ist in der vierten Klasse, das andere in der dritten oder zweiten Klasse.</ta>
            <ta e="T562" id="Seg_6737" s="T558">In der zweiten Klasse, äh, die Tochter.</ta>
            <ta e="T571" id="Seg_6738" s="T562">Jetzt sind sie in meinem Haus geblieben, dann im Mai gehen sie, zurück.</ta>
            <ta e="T573" id="Seg_6739" s="T571">Sie werden nomadisieren.</ta>
            <ta e="T574" id="Seg_6740" s="T573">Das ist alles.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KiES">
            <ta e="T11" id="Seg_6741" s="T1">– Ну, мы были в Корго, у нас там была земля, я поселилась(?) в Корго. </ta>
            <ta e="T24" id="Seg_6742" s="T11">Но потом, ээ, у меня родился ребенок, потом ппосле этого у меня родился этот мальчик.</ta>
            <ta e="T36" id="Seg_6743" s="T24">Но потом мой(?) муж заболел, после того как он умер, мы поселились в Сындасско.</ta>
            <ta e="T58" id="Seg_6744" s="T36">Потом мне начальники сообразили эти, ну как скажу, потом они дали мне работу в детском саду, детский сад первый раз был в Сындасске.</ta>
            <ta e="T66" id="Seg_6745" s="T63">– Да, по-долгански.</ta>
            <ta e="T72" id="Seg_6746" s="T66">В Сындасске первый раз появился детский сад.</ta>
            <ta e="T79" id="Seg_6747" s="T72">Я ничего не знаю, детский сад как будет?</ta>
            <ta e="T84" id="Seg_6748" s="T79">Ничего не знаем мы, тундровички.</ta>
            <ta e="T90" id="Seg_6749" s="T84">Всё такое, это кормить, как…</ta>
            <ta e="T97" id="Seg_6750" s="T91">– О, (…) как мы будем дальше (…)?</ta>
            <ta e="T100" id="Seg_6751" s="T97">Я не умею гворить с детьми.</ta>
            <ta e="T105" id="Seg_6752" s="T100">Я, говорю, не умею готовить детям.</ta>
            <ta e="T114" id="Seg_6753" s="T105">Говорят, поваров нет, детский сад только открылся, в новом садике.</ta>
            <ta e="T120" id="Seg_6754" s="T114">Там повар, повар нужен, говорят начальники.</ta>
            <ta e="T126" id="Seg_6755" s="T120">И так начальники сделали меня поваром.</ta>
            <ta e="T144" id="Seg_6756" s="T126">Так мы жили, я готовила, ээ, двое моих детей ходили в садик, да.</ta>
            <ta e="T148" id="Seg_6757" s="T144">Ну, они ходят (…).</ta>
            <ta e="T152" id="Seg_6758" s="T150">С кем?</ta>
            <ta e="T166" id="Seg_6759" s="T156">Почему, потом позже умерла это, умерла моя тетя.</ta>
            <ta e="T171" id="Seg_6760" s="T168">– Хорошо.</ta>
            <ta e="T175" id="Seg_6761" s="T171">Моя мама, умерла младшая сестра моей мамы.</ta>
            <ta e="T182" id="Seg_6762" s="T175">Она заболела, и у нее осталось четверо детей.</ta>
            <ta e="T188" id="Seg_6763" s="T182">Четверо детей пришли ко мне.</ta>
            <ta e="T196" id="Seg_6764" s="T188">"Мы будем с нашей тетей, мы никуда не пойдем", – сказали они. </ta>
            <ta e="T212" id="Seg_6765" s="T196"> Моя мама сказала в завещании: "Оставайтесь с моей старшей сестрой, – сказала она, – если что-то со мной случится".</ta>
            <ta e="T222" id="Seg_6766" s="T212">Ну, та четверо детей пришли к нам.</ta>
            <ta e="T225" id="Seg_6767" s="T222">Они поехали учиться в Хатангу.</ta>
            <ta e="T233" id="Seg_6768" s="T230">– Не мешай.</ta>
            <ta e="T245" id="Seg_6769" s="T236">Ну потом они приехали на каникулы, эти дети, начальники посоветовали:</ta>
            <ta e="T253" id="Seg_6770" s="T245">"Это ребенка нужно отправить в Попигай, – сказали они, – к родственникам".</ta>
            <ta e="T258" id="Seg_6771" s="T255">– Ммм, (Катю?).</ta>
            <ta e="T270" id="Seg_6772" s="T258">Одного, одного ребенка, здесь в тундре есть родственники, старик со старухой.</ta>
            <ta e="T273" id="Seg_6773" s="T270">Ну, они его им отдали.</ta>
            <ta e="T291" id="Seg_6774" s="T273">Тогда я осталась с двумя мальчиками, только они ко мне (обратно?) не пошли, эти два мальчика.</ta>
            <ta e="T304" id="Seg_6775" s="T291">Вскоре после того они приехали, эти дети, обратно к нам.</ta>
            <ta e="T308" id="Seg_6776" s="T304">Им не понравилось у них.</ta>
            <ta e="T328" id="Seg_6777" s="T308">Потом и в тундре эти старик со старухой, двое детей тоже пришли, они вернулись обратно к нам, и у меня стало восемь детей.</ta>
            <ta e="T333" id="Seg_6778" s="T329">Своих четверо детей.</ta>
            <ta e="T337" id="Seg_6779" s="T333">Две девочки у меня.</ta>
            <ta e="T346" id="Seg_6780" s="T337">Моя мама старая, мой старший брат старый, и еще у меня восемь своих детей.</ta>
            <ta e="T355" id="Seg_6781" s="T346">Моя сетсра оставила своих четверых детей, и у меня четверо детей.</ta>
            <ta e="T359" id="Seg_6782" s="T355">Надя последняя, она маленькая была.</ta>
            <ta e="T362" id="Seg_6783" s="T359">Она была маленькая.</ta>
            <ta e="T373" id="Seg_6784" s="T362">И вот я осталась и мы жили, мама состарилась и умерла, она болела.</ta>
            <ta e="T383" id="Seg_6785" s="T373">А у моего старшего брата не было ни жены, никого.</ta>
            <ta e="T395" id="Seg_6786" s="T383">Он работал в магазине, поэтому, мы не это [=не нуждались] в продуктах.</ta>
            <ta e="T402" id="Seg_6787" s="T395">Работая [там] (…), он приносил еду из магазина.</ta>
            <ta e="T405" id="Seg_6788" s="T402">Он получал зарплату.</ta>
            <ta e="T409" id="Seg_6789" s="T405">А я работала поваром.</ta>
            <ta e="T416" id="Seg_6790" s="T409">И так мы жили, все выросли, женились.</ta>
            <ta e="T427" id="Seg_6791" s="T421">– Да, она вышла замуж (чтобы что делать?), она вышла замуж.</ta>
            <ta e="T434" id="Seg_6792" s="T427">Вот поэтому стало восемь детей.</ta>
            <ta e="T443" id="Seg_6793" s="T434">Я молодая была тогда, вышли замуж (?).</ta>
            <ta e="T451" id="Seg_6794" s="T443">Выйдя замуж, родила двоих детей.</ta>
            <ta e="T460" id="Seg_6795" s="T451">Третий, мой третий ребенок, мальчик, у меня случился выкидыш.</ta>
            <ta e="T469" id="Seg_6796" s="T460">(…), когда я носила в садик лед и уголь, у меня случился выкидыш.</ta>
            <ta e="T473" id="Seg_6797" s="T469">Между двумя мальчиками.</ta>
            <ta e="T491" id="Seg_6798" s="T480">– Когда у нас появилась Алёна, ей было шесть лет, а когда ей стало семь, я приехала сюда, в Дудинку.</ta>
            <ta e="T504" id="Seg_6799" s="T491">Мой ребенок получил квартиру, а пото нужна была прописка, и я приехала.</ta>
            <ta e="T513" id="Seg_6800" s="T504">Когда я приехала, мой ребенок был в десятом классе, вот.</ta>
            <ta e="T517" id="Seg_6801" s="T513">Ее мама вышла замуж в Якутске.</ta>
            <ta e="T524" id="Seg_6802" s="T517">Сейчас у нее двое детей, мальчик и девочка.</ta>
            <ta e="T534" id="Seg_6803" s="T528">Они живут в тундре, у нее муж – оленевод.</ta>
            <ta e="T536" id="Seg_6804" s="T535">Это всё.</ta>
            <ta e="T544" id="Seg_6805" s="T536">А сейчас они в Сындасске, они поедут в мае, когда закончится школа.</ta>
            <ta e="T548" id="Seg_6806" s="T544">Их дети учатся учатся там, в Сындасске.</ta>
            <ta e="T557" id="Seg_6807" s="T548">Один в четвертом классе, а другой в третьем или во втором.</ta>
            <ta e="T562" id="Seg_6808" s="T558">Вот втором классе, ээ, дочка.</ta>
            <ta e="T571" id="Seg_6809" s="T562">Сейчас они живут в моем доме, а потом в мае они поедут обратно.</ta>
            <ta e="T573" id="Seg_6810" s="T571">Аргишить будут.</ta>
            <ta e="T574" id="Seg_6811" s="T573">Всё.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr-KiES">
            <ta e="T105" id="Seg_6812" s="T100">ja ne umeju varit detjam</ta>
            <ta e="T114" id="Seg_6813" s="T105">govorjat vari, sadik tolko chto otkrzlsja, novj sadik</ta>
            <ta e="T120" id="Seg_6814" s="T114">tam nuzhen povar govorjat nachalniki</ta>
            <ta e="T126" id="Seg_6815" s="T120">i tak nachalniki postavili menja povarom</ta>
            <ta e="T144" id="Seg_6816" s="T126">tak zhili potom rabotaja povarom dvoe detej hodili v sadik</ta>
            <ta e="T148" id="Seg_6817" s="T144">oni poseshajut</ta>
            <ta e="T152" id="Seg_6818" s="T150">s kem</ta>
            <ta e="T166" id="Seg_6819" s="T156">pochemu, potom umerla sestra</ta>
            <ta e="T175" id="Seg_6820" s="T171">mlladshaja sestra mamɨ umerla</ta>
            <ta e="T182" id="Seg_6821" s="T175">zabolela poetomu ostavila chetverɨh detej</ta>
            <ta e="T188" id="Seg_6822" s="T182">ko mne pereshli chetɨre detej</ta>
            <ta e="T196" id="Seg_6823" s="T188">mɨ budem u sestrɨ nikuda mɨ ne poidem</ta>
            <ta e="T212" id="Seg_6824" s="T196">ɨaveshanie mamɨ chtobɨ oni ostalis u sstrɨ esli chto nibud sluchitsja so mnoj</ta>
            <ta e="T222" id="Seg_6825" s="T212">chetvera detej pereshli k nam</ta>
            <ta e="T225" id="Seg_6826" s="T222">poehali uchitsja v khatangu</ta>
            <ta e="T233" id="Seg_6827" s="T230">ne meshaj</ta>
            <ta e="T245" id="Seg_6828" s="T236">potom priezd na kanikulɨ etih detej sovetujut nachalniki</ta>
            <ta e="T253" id="Seg_6829" s="T245">etogo rebenka otpravit k rodstvennikam v Popigaj</ta>
            <ta e="T270" id="Seg_6830" s="T258">odnogo rebonka zdes v tundru gde est babushka dedushka rodstveniiki </ta>
            <ta e="T273" id="Seg_6831" s="T270">tem dali</ta>
            <ta e="T291" id="Seg_6832" s="T273">togda ja ostalas s dvumja malchikami no oni ne hotjat idti k nam </ta>
            <ta e="T304" id="Seg_6833" s="T291">vskore posle togo oni priehali na kanikulɨ k nam obratno</ta>
            <ta e="T308" id="Seg_6834" s="T304">im ne ponravilos u nih</ta>
            <ta e="T328" id="Seg_6835" s="T308">tam v tundre baabushka s dedushkoj i tozhe te dvoe malchikov vernuls k nam, u menja stalo vosem detej</ta>
            <ta e="T333" id="Seg_6836" s="T329">u menja svoih chetvero detej</ta>
            <ta e="T337" id="Seg_6837" s="T333">u menja dvoe devochek</ta>
            <ta e="T346" id="Seg_6838" s="T337">sestra staraj brat starɨj i vosem detej svoih</ta>
            <ta e="T355" id="Seg_6839" s="T346">sestra ostavila chetverɨh i u menja svioh chetvero</ta>
            <ta e="T373" id="Seg_6840" s="T362">kogda ja ostalas mat umerla po starosti po boleɨn</ta>
            <ta e="T383" id="Seg_6841" s="T373">u brata nebɨlo zhenɨ</ta>
            <ta e="T395" id="Seg_6842" s="T383">on rabotal v magazine poetomu ne nuzhdalis v pitanie</ta>
            <ta e="T402" id="Seg_6843" s="T395">rabotal prinosil produktɨ iz magazina</ta>
            <ta e="T405" id="Seg_6844" s="T402">poluchal zarplatu</ta>
            <ta e="T409" id="Seg_6845" s="T405">potom ja rabotaju povarom</ta>
            <ta e="T416" id="Seg_6846" s="T409">tak mɨ zhili oni vɨrosli, pozhenilis</ta>
            <ta e="T427" id="Seg_6847" s="T421">da ona vɨshla zachem tebe nado</ta>
            <ta e="T434" id="Seg_6848" s="T427">poetomu stalo vosem detej</ta>
            <ta e="T443" id="Seg_6849" s="T434">ja togda molodaja vɨshla zamuzh</ta>
            <ta e="T451" id="Seg_6850" s="T443">vɨshla zamuzh i rodila dvoih</ta>
            <ta e="T460" id="Seg_6851" s="T451">tretij malchik bil bil vɨkidish</ta>
            <ta e="T469" id="Seg_6852" s="T460">podnimaja tjazhesti led ugol v sadike poluchilsja vɨkidish</ta>
            <ta e="T473" id="Seg_6853" s="T469">mezhdu dvumja malchikami</ta>
            <ta e="T491" id="Seg_6854" s="T480">alen e bɨlo shest let mɨ zabrali kogda isponilas sem let pereehaliv dudinku </ta>
            <ta e="T504" id="Seg_6855" s="T491">doch v eto vremja poluchila kvartiru, nuzhna bilo menja propisat je priehala</ta>
            <ta e="T513" id="Seg_6856" s="T504">kogda pereehala devochka poshla v desjatij klas</ta>
            <ta e="T517" id="Seg_6857" s="T513">mama ee vɨshla zamuzh v jakutske</ta>
            <ta e="T524" id="Seg_6858" s="T517">seihas u nee dvoe detej malchik i devochka</ta>
            <ta e="T534" id="Seg_6859" s="T528">zhivut oni v tundre muzh u nee olenevod</ta>
            <ta e="T536" id="Seg_6860" s="T535">konets</ta>
            <ta e="T544" id="Seg_6861" s="T536">seichas ostalis v sɨndassko oni vɨedut tundru kogda zakonchitsja uchebnɨj god</ta>
            <ta e="T548" id="Seg_6862" s="T544">deti tam uchatsa v sɨndasske</ta>
            <ta e="T557" id="Seg_6863" s="T548">odin v chetvertom klasse vtoroj v tretem ili vtorom klasse</ta>
            <ta e="T562" id="Seg_6864" s="T558">devochka vo vtorom klasse</ta>
            <ta e="T571" id="Seg_6865" s="T562">seichas zhivut v moem dome, a v maje oni vɨedut obratno</ta>
            <ta e="T573" id="Seg_6866" s="T571">argishom poedut</ta>
            <ta e="T574" id="Seg_6867" s="T573">vse</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KiES" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-KiLS"
                      id="tx-KiLS"
                      speaker="KiLS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KiLS">
            <ts e="T62" id="Seg_6868" n="sc" s="T59">
               <ts e="T62" id="Seg_6870" n="HIAT:u" s="T59">
                  <nts id="Seg_6871" n="HIAT:ip">–</nts>
                  <nts id="Seg_6872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_6874" n="HIAT:w" s="T59">Hakalɨː</ts>
                  <nts id="Seg_6875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_6877" n="HIAT:w" s="T60">kepseː</ts>
                  <nts id="Seg_6878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_6880" n="HIAT:w" s="T61">diː</ts>
                  <nts id="Seg_6881" n="HIAT:ip">.</nts>
                  <nts id="Seg_6882" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T91" id="Seg_6883" n="sc" s="T89">
               <ts e="T91" id="Seg_6885" n="HIAT:u" s="T89">
                  <nts id="Seg_6886" n="HIAT:ip">–</nts>
                  <nts id="Seg_6887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_6889" n="HIAT:w" s="T89">Hakalɨː</ts>
                  <nts id="Seg_6890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_6892" n="HIAT:w" s="T90">kepseː</ts>
                  <nts id="Seg_6893" n="HIAT:ip">!</nts>
                  <nts id="Seg_6894" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T150" id="Seg_6895" n="sc" s="T148">
               <ts e="T150" id="Seg_6897" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_6899" n="HIAT:w" s="T148">Eta</ts>
                  <nts id="Seg_6900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_6902" n="HIAT:w" s="T149">Vasʼi</ts>
                  <nts id="Seg_6903" n="HIAT:ip">.</nts>
                  <nts id="Seg_6904" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T156" id="Seg_6905" n="sc" s="T153">
               <ts e="T156" id="Seg_6907" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_6909" n="HIAT:w" s="T153">Ujbaːni</ts>
                  <nts id="Seg_6910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_6912" n="HIAT:w" s="T154">gɨtta</ts>
                  <nts id="Seg_6913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_6915" n="HIAT:w" s="T155">Vasʼiː</ts>
                  <nts id="Seg_6916" n="HIAT:ip">.</nts>
                  <nts id="Seg_6917" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T171" id="Seg_6918" n="sc" s="T166">
               <ts e="T171" id="Seg_6920" n="HIAT:u" s="T166">
                  <nts id="Seg_6921" n="HIAT:ip">–</nts>
                  <nts id="Seg_6922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_6924" n="HIAT:w" s="T166">Dʼe</ts>
                  <nts id="Seg_6925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_6927" n="HIAT:w" s="T167">onu</ts>
                  <nts id="Seg_6928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_6930" n="HIAT:w" s="T169">kepseː</ts>
                  <nts id="Seg_6931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_6933" n="HIAT:w" s="T170">anɨ</ts>
                  <nts id="Seg_6934" n="HIAT:ip">.</nts>
                  <nts id="Seg_6935" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T233" id="Seg_6936" n="sc" s="T226">
               <ts e="T233" id="Seg_6938" n="HIAT:u" s="T226">
                  <nts id="Seg_6939" n="HIAT:ip">–</nts>
                  <nts id="Seg_6940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_6942" n="HIAT:w" s="T226">Kimi͡eke</ts>
                  <nts id="Seg_6943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_6945" n="HIAT:w" s="T227">ba</ts>
                  <nts id="Seg_6946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_6948" n="HIAT:w" s="T228">ɨːppɨttara</ts>
                  <nts id="Seg_6949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_6951" n="HIAT:w" s="T229">Kaːtʼanɨ</ts>
                  <nts id="Seg_6952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_6954" n="HIAT:w" s="T230">gɨtta</ts>
                  <nts id="Seg_6955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_6957" n="HIAT:w" s="T231">Tanʼanɨ</ts>
                  <nts id="Seg_6958" n="HIAT:ip">?</nts>
                  <nts id="Seg_6959" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T255" id="Seg_6960" n="sc" s="T253">
               <ts e="T255" id="Seg_6962" n="HIAT:u" s="T253">
                  <nts id="Seg_6963" n="HIAT:ip">–</nts>
                  <nts id="Seg_6964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6965" n="HIAT:ip">(</nts>
                  <nts id="Seg_6966" n="HIAT:ip">(</nts>
                  <ats e="T254" id="Seg_6967" n="HIAT:non-pho" s="T253">…</ats>
                  <nts id="Seg_6968" n="HIAT:ip">)</nts>
                  <nts id="Seg_6969" n="HIAT:ip">)</nts>
                  <nts id="Seg_6970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_6972" n="HIAT:w" s="T254">Katʼa</ts>
                  <nts id="Seg_6973" n="HIAT:ip">.</nts>
                  <nts id="Seg_6974" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T421" id="Seg_6975" n="sc" s="T417">
               <ts e="T421" id="Seg_6977" n="HIAT:u" s="T417">
                  <nts id="Seg_6978" n="HIAT:ip">–</nts>
                  <nts id="Seg_6979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6980" n="HIAT:ip">(</nts>
                  <nts id="Seg_6981" n="HIAT:ip">(</nts>
                  <ats e="T418" id="Seg_6982" n="HIAT:non-pho" s="T417">…</ats>
                  <nts id="Seg_6983" n="HIAT:ip">)</nts>
                  <nts id="Seg_6984" n="HIAT:ip">)</nts>
                  <nts id="Seg_6985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_6987" n="HIAT:w" s="T418">erge</ts>
                  <nts id="Seg_6988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_6990" n="HIAT:w" s="T419">taksɨbɨtɨm</ts>
                  <nts id="Seg_6991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_6993" n="HIAT:w" s="T420">di͡en</ts>
                  <nts id="Seg_6994" n="HIAT:ip">.</nts>
                  <nts id="Seg_6995" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T479" id="Seg_6996" n="sc" s="T474">
               <ts e="T479" id="Seg_6998" n="HIAT:u" s="T474">
                  <nts id="Seg_6999" n="HIAT:ip">–</nts>
                  <nts id="Seg_7000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_7002" n="HIAT:w" s="T474">Ölöːnö</ts>
                  <nts id="Seg_7003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_7005" n="HIAT:w" s="T475">hette</ts>
                  <nts id="Seg_7006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_7008" n="HIAT:w" s="T476">dʼɨllammɨta</ts>
                  <nts id="Seg_7009" n="HIAT:ip">,</nts>
                  <nts id="Seg_7010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_7012" n="HIAT:w" s="T477">munna</ts>
                  <nts id="Seg_7013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_7015" n="HIAT:w" s="T478">kelbikkin</ts>
                  <nts id="Seg_7016" n="HIAT:ip">.</nts>
                  <nts id="Seg_7017" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T527" id="Seg_7018" n="sc" s="T525">
               <ts e="T527" id="Seg_7020" n="HIAT:u" s="T525">
                  <nts id="Seg_7021" n="HIAT:ip">–</nts>
                  <nts id="Seg_7022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_7024" n="HIAT:w" s="T525">Tü͡örd-u͡on</ts>
                  <nts id="Seg_7025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_7027" n="HIAT:w" s="T526">dʼɨllaːk</ts>
                  <nts id="Seg_7028" n="HIAT:ip">.</nts>
                  <nts id="Seg_7029" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KiLS">
            <ts e="T62" id="Seg_7030" n="sc" s="T59">
               <ts e="T60" id="Seg_7032" n="e" s="T59">– Hakalɨː </ts>
               <ts e="T61" id="Seg_7034" n="e" s="T60">kepseː </ts>
               <ts e="T62" id="Seg_7036" n="e" s="T61">diː. </ts>
            </ts>
            <ts e="T91" id="Seg_7037" n="sc" s="T89">
               <ts e="T90" id="Seg_7039" n="e" s="T89">– Hakalɨː </ts>
               <ts e="T91" id="Seg_7041" n="e" s="T90">kepseː! </ts>
            </ts>
            <ts e="T150" id="Seg_7042" n="sc" s="T148">
               <ts e="T149" id="Seg_7044" n="e" s="T148">Eta </ts>
               <ts e="T150" id="Seg_7046" n="e" s="T149">Vasʼi. </ts>
            </ts>
            <ts e="T156" id="Seg_7047" n="sc" s="T153">
               <ts e="T154" id="Seg_7049" n="e" s="T153">Ujbaːni </ts>
               <ts e="T155" id="Seg_7051" n="e" s="T154">gɨtta </ts>
               <ts e="T156" id="Seg_7053" n="e" s="T155">Vasʼiː. </ts>
            </ts>
            <ts e="T171" id="Seg_7054" n="sc" s="T166">
               <ts e="T167" id="Seg_7056" n="e" s="T166">– Dʼe </ts>
               <ts e="T169" id="Seg_7058" n="e" s="T167">onu </ts>
               <ts e="T170" id="Seg_7060" n="e" s="T169">kepseː </ts>
               <ts e="T171" id="Seg_7062" n="e" s="T170">anɨ. </ts>
            </ts>
            <ts e="T233" id="Seg_7063" n="sc" s="T226">
               <ts e="T227" id="Seg_7065" n="e" s="T226">– Kimi͡eke </ts>
               <ts e="T228" id="Seg_7067" n="e" s="T227">ba </ts>
               <ts e="T229" id="Seg_7069" n="e" s="T228">ɨːppɨttara </ts>
               <ts e="T230" id="Seg_7071" n="e" s="T229">Kaːtʼanɨ </ts>
               <ts e="T231" id="Seg_7073" n="e" s="T230">gɨtta </ts>
               <ts e="T233" id="Seg_7075" n="e" s="T231">Tanʼanɨ? </ts>
            </ts>
            <ts e="T255" id="Seg_7076" n="sc" s="T253">
               <ts e="T254" id="Seg_7078" n="e" s="T253">– ((…)) </ts>
               <ts e="T255" id="Seg_7080" n="e" s="T254">Katʼa. </ts>
            </ts>
            <ts e="T421" id="Seg_7081" n="sc" s="T417">
               <ts e="T418" id="Seg_7083" n="e" s="T417">– ((…)) </ts>
               <ts e="T419" id="Seg_7085" n="e" s="T418">erge </ts>
               <ts e="T420" id="Seg_7087" n="e" s="T419">taksɨbɨtɨm </ts>
               <ts e="T421" id="Seg_7089" n="e" s="T420">di͡en. </ts>
            </ts>
            <ts e="T479" id="Seg_7090" n="sc" s="T474">
               <ts e="T475" id="Seg_7092" n="e" s="T474">– Ölöːnö </ts>
               <ts e="T476" id="Seg_7094" n="e" s="T475">hette </ts>
               <ts e="T477" id="Seg_7096" n="e" s="T476">dʼɨllammɨta, </ts>
               <ts e="T478" id="Seg_7098" n="e" s="T477">munna </ts>
               <ts e="T479" id="Seg_7100" n="e" s="T478">kelbikkin. </ts>
            </ts>
            <ts e="T527" id="Seg_7101" n="sc" s="T525">
               <ts e="T526" id="Seg_7103" n="e" s="T525">– Tü͡örd-u͡on </ts>
               <ts e="T527" id="Seg_7105" n="e" s="T526">dʼɨllaːk. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KiLS">
            <ta e="T62" id="Seg_7106" s="T59">KiES_KiLS_2009_Life_nar.KiLS.001 (001.005)</ta>
            <ta e="T91" id="Seg_7107" s="T89">KiES_KiLS_2009_Life_nar.KiLS.002 (001.011)</ta>
            <ta e="T150" id="Seg_7108" s="T148">KiES_KiLS_2009_Life_nar.KiLS.003 (001.020)</ta>
            <ta e="T156" id="Seg_7109" s="T153">KiES_KiLS_2009_Life_nar.KiLS.004 (001.022)</ta>
            <ta e="T171" id="Seg_7110" s="T166">KiES_KiLS_2009_Life_nar.KiLS.005 (001.024)</ta>
            <ta e="T233" id="Seg_7111" s="T226">KiES_KiLS_2009_Life_nar.KiLS.006 (001.033)</ta>
            <ta e="T255" id="Seg_7112" s="T253">KiES_KiLS_2009_Life_nar.KiLS.007 (001.037)</ta>
            <ta e="T421" id="Seg_7113" s="T417">KiES_KiLS_2009_Life_nar.KiLS.008 (001.058)</ta>
            <ta e="T479" id="Seg_7114" s="T474">KiES_KiLS_2009_Life_nar.KiLS.009 (001.066)</ta>
            <ta e="T527" id="Seg_7115" s="T525">KiES_KiLS_2009_Life_nar.KiLS.010 (001.072)</ta>
         </annotation>
         <annotation name="st" tierref="st-KiLS" />
         <annotation name="ts" tierref="ts-KiLS">
            <ta e="T62" id="Seg_7116" s="T59">– Hakalɨː kepseː diː. </ta>
            <ta e="T91" id="Seg_7117" s="T89">– Hakalɨː kepseː! </ta>
            <ta e="T150" id="Seg_7118" s="T148">Eta Vasʼi. </ta>
            <ta e="T156" id="Seg_7119" s="T153">Ujbaːni gɨtta Vasʼiː. </ta>
            <ta e="T171" id="Seg_7120" s="T166">– Dʼe onu kepseː anɨ. </ta>
            <ta e="T233" id="Seg_7121" s="T226">– Kimi͡eke ba ɨːppɨttara Kaːtʼanɨ gɨtta Tanʼanɨ? </ta>
            <ta e="T255" id="Seg_7122" s="T253">– ((…)) Katʼa. </ta>
            <ta e="T421" id="Seg_7123" s="T417">– ((…)) erge taksɨbɨtɨm di͡en. </ta>
            <ta e="T479" id="Seg_7124" s="T474">– Ölöːnö hette dʼɨllammɨta, munna kelbikkin. </ta>
            <ta e="T527" id="Seg_7125" s="T525">– Tü͡örd-u͡on dʼɨllaːk. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-KiLS">
            <ta e="T60" id="Seg_7126" s="T59">haka-lɨː</ta>
            <ta e="T61" id="Seg_7127" s="T60">kepseː</ta>
            <ta e="T62" id="Seg_7128" s="T61">diː</ta>
            <ta e="T90" id="Seg_7129" s="T89">haka-lɨː</ta>
            <ta e="T91" id="Seg_7130" s="T90">kepseː</ta>
            <ta e="T154" id="Seg_7131" s="T153">Ujbaːn-i</ta>
            <ta e="T155" id="Seg_7132" s="T154">gɨtta</ta>
            <ta e="T156" id="Seg_7133" s="T155">Vasʼiː</ta>
            <ta e="T167" id="Seg_7134" s="T166">dʼe</ta>
            <ta e="T169" id="Seg_7135" s="T167">o-nu</ta>
            <ta e="T170" id="Seg_7136" s="T169">kepseː</ta>
            <ta e="T171" id="Seg_7137" s="T170">anɨ</ta>
            <ta e="T227" id="Seg_7138" s="T226">kimi͡e-ke</ta>
            <ta e="T228" id="Seg_7139" s="T227">ba</ta>
            <ta e="T229" id="Seg_7140" s="T228">ɨːp-pɨt-tara</ta>
            <ta e="T230" id="Seg_7141" s="T229">Kaːtʼa-nɨ</ta>
            <ta e="T231" id="Seg_7142" s="T230">gɨtta</ta>
            <ta e="T233" id="Seg_7143" s="T231">Tanʼa-nɨ</ta>
            <ta e="T255" id="Seg_7144" s="T254">Katʼa</ta>
            <ta e="T419" id="Seg_7145" s="T418">er-ge</ta>
            <ta e="T420" id="Seg_7146" s="T419">taks-ɨ-bɨt-ɨ-m</ta>
            <ta e="T421" id="Seg_7147" s="T420">di͡e-n</ta>
            <ta e="T475" id="Seg_7148" s="T474">Ölöːnö</ta>
            <ta e="T476" id="Seg_7149" s="T475">hette</ta>
            <ta e="T477" id="Seg_7150" s="T476">dʼɨl-lam-mɨt-a</ta>
            <ta e="T478" id="Seg_7151" s="T477">munna</ta>
            <ta e="T479" id="Seg_7152" s="T478">kel-bik-kin</ta>
            <ta e="T526" id="Seg_7153" s="T525">tü͡örd-u͡on</ta>
            <ta e="T527" id="Seg_7154" s="T526">dʼɨl-laːk</ta>
         </annotation>
         <annotation name="mp" tierref="mp-KiLS">
            <ta e="T60" id="Seg_7155" s="T59">haka-LIː</ta>
            <ta e="T61" id="Seg_7156" s="T60">kepseː</ta>
            <ta e="T62" id="Seg_7157" s="T61">diː</ta>
            <ta e="T90" id="Seg_7158" s="T89">haka-LIː</ta>
            <ta e="T91" id="Seg_7159" s="T90">kepseː</ta>
            <ta e="T154" id="Seg_7160" s="T153">Ujbaːn-nI</ta>
            <ta e="T155" id="Seg_7161" s="T154">kɨtta</ta>
            <ta e="T156" id="Seg_7162" s="T155">Vasʼiː</ta>
            <ta e="T167" id="Seg_7163" s="T166">dʼe</ta>
            <ta e="T169" id="Seg_7164" s="T167">ol-nI</ta>
            <ta e="T170" id="Seg_7165" s="T169">kepseː</ta>
            <ta e="T171" id="Seg_7166" s="T170">anɨ</ta>
            <ta e="T227" id="Seg_7167" s="T226">kim-GA</ta>
            <ta e="T228" id="Seg_7168" s="T227">bu</ta>
            <ta e="T229" id="Seg_7169" s="T228">ɨːt-BIT-LArA</ta>
            <ta e="T230" id="Seg_7170" s="T229">Kaːtʼa-nI</ta>
            <ta e="T231" id="Seg_7171" s="T230">kɨtta</ta>
            <ta e="T233" id="Seg_7172" s="T231">Tanʼa-nI</ta>
            <ta e="T255" id="Seg_7173" s="T254">Kaːtʼa</ta>
            <ta e="T419" id="Seg_7174" s="T418">er-GA</ta>
            <ta e="T420" id="Seg_7175" s="T419">tagɨs-I-BIT-I-m</ta>
            <ta e="T421" id="Seg_7176" s="T420">di͡e-An</ta>
            <ta e="T475" id="Seg_7177" s="T474">Ölöːnö</ta>
            <ta e="T476" id="Seg_7178" s="T475">hette</ta>
            <ta e="T477" id="Seg_7179" s="T476">dʼɨl-LAN-BIT-tA</ta>
            <ta e="T478" id="Seg_7180" s="T477">manna</ta>
            <ta e="T479" id="Seg_7181" s="T478">kel-BIT-GIn</ta>
            <ta e="T526" id="Seg_7182" s="T525">tü͡ört-u͡on</ta>
            <ta e="T527" id="Seg_7183" s="T526">dʼɨl-LAːK</ta>
         </annotation>
         <annotation name="ge" tierref="ge-KiLS">
            <ta e="T60" id="Seg_7184" s="T59">Dolgan-SIM</ta>
            <ta e="T61" id="Seg_7185" s="T60">tell.[IMP.2SG]</ta>
            <ta e="T62" id="Seg_7186" s="T61">EMPH</ta>
            <ta e="T90" id="Seg_7187" s="T89">Dolgan-SIM</ta>
            <ta e="T91" id="Seg_7188" s="T90">tell.[IMP.2SG]</ta>
            <ta e="T154" id="Seg_7189" s="T153">Ivan-ACC</ta>
            <ta e="T155" id="Seg_7190" s="T154">with</ta>
            <ta e="T156" id="Seg_7191" s="T155">Vasi</ta>
            <ta e="T167" id="Seg_7192" s="T166">well</ta>
            <ta e="T169" id="Seg_7193" s="T167">that -ACC</ta>
            <ta e="T170" id="Seg_7194" s="T169">tell.[IMP.2SG]</ta>
            <ta e="T171" id="Seg_7195" s="T170">now</ta>
            <ta e="T227" id="Seg_7196" s="T226">who-DAT/LOC</ta>
            <ta e="T228" id="Seg_7197" s="T227">this.[NOM]</ta>
            <ta e="T229" id="Seg_7198" s="T228">send-PST2-3PL</ta>
            <ta e="T230" id="Seg_7199" s="T229">Katya-ACC</ta>
            <ta e="T231" id="Seg_7200" s="T230">with</ta>
            <ta e="T233" id="Seg_7201" s="T231">Tanya-ACC</ta>
            <ta e="T255" id="Seg_7202" s="T254">Katya.[NOM]</ta>
            <ta e="T419" id="Seg_7203" s="T418">husband-DAT/LOC</ta>
            <ta e="T420" id="Seg_7204" s="T419">go.out-EP-PST2-EP-1SG</ta>
            <ta e="T421" id="Seg_7205" s="T420">say-CVB.SEQ</ta>
            <ta e="T475" id="Seg_7206" s="T474">Elena.[NOM]</ta>
            <ta e="T476" id="Seg_7207" s="T475">seven</ta>
            <ta e="T477" id="Seg_7208" s="T476">year-VBZ-PST2-3SG</ta>
            <ta e="T478" id="Seg_7209" s="T477">hither</ta>
            <ta e="T479" id="Seg_7210" s="T478">come-PST2-2SG</ta>
            <ta e="T526" id="Seg_7211" s="T525">four-ten</ta>
            <ta e="T527" id="Seg_7212" s="T526">year-PROPR.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg-KiLS">
            <ta e="T60" id="Seg_7213" s="T59">dolganisch-SIM</ta>
            <ta e="T61" id="Seg_7214" s="T60">erzählen.[IMP.2SG]</ta>
            <ta e="T62" id="Seg_7215" s="T61">EMPH</ta>
            <ta e="T90" id="Seg_7216" s="T89">dolganisch-SIM</ta>
            <ta e="T91" id="Seg_7217" s="T90">erzählen.[IMP.2SG]</ta>
            <ta e="T154" id="Seg_7218" s="T153">Ivan-ACC</ta>
            <ta e="T155" id="Seg_7219" s="T154">mit</ta>
            <ta e="T156" id="Seg_7220" s="T155">Vasi</ta>
            <ta e="T167" id="Seg_7221" s="T166">doch</ta>
            <ta e="T169" id="Seg_7222" s="T167">jenes-ACC</ta>
            <ta e="T170" id="Seg_7223" s="T169">erzählen.[IMP.2SG]</ta>
            <ta e="T171" id="Seg_7224" s="T170">jetzt</ta>
            <ta e="T227" id="Seg_7225" s="T226">wer-DAT/LOC</ta>
            <ta e="T228" id="Seg_7226" s="T227">dieses.[NOM]</ta>
            <ta e="T229" id="Seg_7227" s="T228">schicken-PST2-3PL</ta>
            <ta e="T230" id="Seg_7228" s="T229">Katja-ACC</ta>
            <ta e="T231" id="Seg_7229" s="T230">mit</ta>
            <ta e="T233" id="Seg_7230" s="T231">Tanja-ACC</ta>
            <ta e="T255" id="Seg_7231" s="T254">Katja.[NOM]</ta>
            <ta e="T419" id="Seg_7232" s="T418">Ehemann-DAT/LOC</ta>
            <ta e="T420" id="Seg_7233" s="T419">hinausgehen-EP-PST2-EP-1SG</ta>
            <ta e="T421" id="Seg_7234" s="T420">sagen-CVB.SEQ</ta>
            <ta e="T475" id="Seg_7235" s="T474">Elena.[NOM]</ta>
            <ta e="T476" id="Seg_7236" s="T475">sieben</ta>
            <ta e="T477" id="Seg_7237" s="T476">Jahr-VBZ-PST2-3SG</ta>
            <ta e="T478" id="Seg_7238" s="T477">hierher</ta>
            <ta e="T479" id="Seg_7239" s="T478">kommen-PST2-2SG</ta>
            <ta e="T526" id="Seg_7240" s="T525">vier-zehn</ta>
            <ta e="T527" id="Seg_7241" s="T526">Jahr-PROPR.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-KiLS">
            <ta e="T60" id="Seg_7242" s="T59">долганский-SIM</ta>
            <ta e="T61" id="Seg_7243" s="T60">рассказывать.[IMP.2SG]</ta>
            <ta e="T62" id="Seg_7244" s="T61">EMPH</ta>
            <ta e="T90" id="Seg_7245" s="T89">долганский-SIM</ta>
            <ta e="T91" id="Seg_7246" s="T90">рассказывать.[IMP.2SG]</ta>
            <ta e="T154" id="Seg_7247" s="T153">Иван-ACC</ta>
            <ta e="T155" id="Seg_7248" s="T154">с</ta>
            <ta e="T156" id="Seg_7249" s="T155">Васи</ta>
            <ta e="T167" id="Seg_7250" s="T166">вот</ta>
            <ta e="T169" id="Seg_7251" s="T167">тот -ACC</ta>
            <ta e="T170" id="Seg_7252" s="T169">рассказывать.[IMP.2SG]</ta>
            <ta e="T171" id="Seg_7253" s="T170">теперь</ta>
            <ta e="T227" id="Seg_7254" s="T226">кто-DAT/LOC</ta>
            <ta e="T228" id="Seg_7255" s="T227">этот.[NOM]</ta>
            <ta e="T229" id="Seg_7256" s="T228">послать-PST2-3PL</ta>
            <ta e="T230" id="Seg_7257" s="T229">Катя-ACC</ta>
            <ta e="T231" id="Seg_7258" s="T230">с</ta>
            <ta e="T233" id="Seg_7259" s="T231">Таня-ACC</ta>
            <ta e="T255" id="Seg_7260" s="T254">Катя.[NOM]</ta>
            <ta e="T419" id="Seg_7261" s="T418">муж-DAT/LOC</ta>
            <ta e="T420" id="Seg_7262" s="T419">выйти-EP-PST2-EP-1SG</ta>
            <ta e="T421" id="Seg_7263" s="T420">говорить-CVB.SEQ</ta>
            <ta e="T475" id="Seg_7264" s="T474">Елена.[NOM]</ta>
            <ta e="T476" id="Seg_7265" s="T475">семь</ta>
            <ta e="T477" id="Seg_7266" s="T476">год-VBZ-PST2-3SG</ta>
            <ta e="T478" id="Seg_7267" s="T477">сюда</ta>
            <ta e="T479" id="Seg_7268" s="T478">приходить-PST2-2SG</ta>
            <ta e="T526" id="Seg_7269" s="T525">четыре-десять</ta>
            <ta e="T527" id="Seg_7270" s="T526">год-PROPR.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-KiLS">
            <ta e="T60" id="Seg_7271" s="T59">adj-adj&gt;adv</ta>
            <ta e="T61" id="Seg_7272" s="T60">v.[v:mood.pn]</ta>
            <ta e="T62" id="Seg_7273" s="T61">ptcl</ta>
            <ta e="T90" id="Seg_7274" s="T89">adj-adj&gt;adv</ta>
            <ta e="T91" id="Seg_7275" s="T90">v.[v:mood.pn]</ta>
            <ta e="T154" id="Seg_7276" s="T153">propr-n:case</ta>
            <ta e="T155" id="Seg_7277" s="T154">post</ta>
            <ta e="T156" id="Seg_7278" s="T155">propr</ta>
            <ta e="T167" id="Seg_7279" s="T166">ptcl</ta>
            <ta e="T169" id="Seg_7280" s="T167">dempro-pro:case</ta>
            <ta e="T170" id="Seg_7281" s="T169">v.[v:mood.pn]</ta>
            <ta e="T171" id="Seg_7282" s="T170">adv</ta>
            <ta e="T227" id="Seg_7283" s="T226">que-pro:case</ta>
            <ta e="T228" id="Seg_7284" s="T227">dempro.[pro:case]</ta>
            <ta e="T229" id="Seg_7285" s="T228">v-v:tense-v:poss.pn</ta>
            <ta e="T230" id="Seg_7286" s="T229">propr-n:case</ta>
            <ta e="T231" id="Seg_7287" s="T230">post</ta>
            <ta e="T233" id="Seg_7288" s="T231">propr-n:case</ta>
            <ta e="T255" id="Seg_7289" s="T254">propr.[n:case]</ta>
            <ta e="T419" id="Seg_7290" s="T418">n-n:case</ta>
            <ta e="T420" id="Seg_7291" s="T419">v-v:(ins)-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T421" id="Seg_7292" s="T420">v-v:cvb</ta>
            <ta e="T475" id="Seg_7293" s="T474">propr.[n:case]</ta>
            <ta e="T476" id="Seg_7294" s="T475">cardnum</ta>
            <ta e="T477" id="Seg_7295" s="T476">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T478" id="Seg_7296" s="T477">adv</ta>
            <ta e="T479" id="Seg_7297" s="T478">v-v:tense-v:pred.pn</ta>
            <ta e="T526" id="Seg_7298" s="T525">cardnum-cardnum</ta>
            <ta e="T527" id="Seg_7299" s="T526">n-n&gt;adj.[n:case]</ta>
         </annotation>
         <annotation name="ps" tierref="ps-KiLS">
            <ta e="T60" id="Seg_7300" s="T59">adv</ta>
            <ta e="T61" id="Seg_7301" s="T60">v</ta>
            <ta e="T62" id="Seg_7302" s="T61">ptcl</ta>
            <ta e="T90" id="Seg_7303" s="T89">adv</ta>
            <ta e="T91" id="Seg_7304" s="T90">v</ta>
            <ta e="T154" id="Seg_7305" s="T153">propr</ta>
            <ta e="T155" id="Seg_7306" s="T154">post</ta>
            <ta e="T156" id="Seg_7307" s="T155">propr</ta>
            <ta e="T167" id="Seg_7308" s="T166">ptcl</ta>
            <ta e="T169" id="Seg_7309" s="T167">dempro</ta>
            <ta e="T170" id="Seg_7310" s="T169">v</ta>
            <ta e="T171" id="Seg_7311" s="T170">adv</ta>
            <ta e="T227" id="Seg_7312" s="T226">que</ta>
            <ta e="T228" id="Seg_7313" s="T227">dempro</ta>
            <ta e="T229" id="Seg_7314" s="T228">v</ta>
            <ta e="T230" id="Seg_7315" s="T229">propr</ta>
            <ta e="T231" id="Seg_7316" s="T230">post</ta>
            <ta e="T233" id="Seg_7317" s="T231">propr</ta>
            <ta e="T255" id="Seg_7318" s="T254">propr</ta>
            <ta e="T419" id="Seg_7319" s="T418">n</ta>
            <ta e="T420" id="Seg_7320" s="T419">v</ta>
            <ta e="T421" id="Seg_7321" s="T420">v</ta>
            <ta e="T475" id="Seg_7322" s="T474">propr</ta>
            <ta e="T476" id="Seg_7323" s="T475">cardnum</ta>
            <ta e="T477" id="Seg_7324" s="T476">v</ta>
            <ta e="T478" id="Seg_7325" s="T477">adv</ta>
            <ta e="T479" id="Seg_7326" s="T478">v</ta>
            <ta e="T526" id="Seg_7327" s="T525">cardnum</ta>
            <ta e="T527" id="Seg_7328" s="T526">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-KiLS" />
         <annotation name="SyF" tierref="SyF-KiLS" />
         <annotation name="IST" tierref="IST-KiLS" />
         <annotation name="Top" tierref="Top-KiLS" />
         <annotation name="Foc" tierref="Foc-KiLS" />
         <annotation name="BOR" tierref="BOR-KiLS">
            <ta e="T154" id="Seg_7329" s="T153">RUS:cult</ta>
            <ta e="T156" id="Seg_7330" s="T155">RUS:cult</ta>
            <ta e="T230" id="Seg_7331" s="T229">RUS:cult</ta>
            <ta e="T233" id="Seg_7332" s="T231">RUS:cult</ta>
            <ta e="T255" id="Seg_7333" s="T254">RUS:cult</ta>
            <ta e="T475" id="Seg_7334" s="T474">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-KiLS" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-KiLS" />
         <annotation name="CS" tierref="CS-KiLS" />
         <annotation name="fe" tierref="fe-KiLS">
            <ta e="T62" id="Seg_7335" s="T59">– Well, tell in Dolgan.</ta>
            <ta e="T91" id="Seg_7336" s="T89">– Tell in Dolgan!</ta>
            <ta e="T150" id="Seg_7337" s="T148">That's Vasi.</ta>
            <ta e="T156" id="Seg_7338" s="T153">Ivan and Vasi.</ta>
            <ta e="T171" id="Seg_7339" s="T166">– Tell about this now.</ta>
            <ta e="T233" id="Seg_7340" s="T226">– To whom Katya and Tanya were sent?</ta>
            <ta e="T255" id="Seg_7341" s="T253">– (…) Katya.</ta>
            <ta e="T421" id="Seg_7342" s="T417">– (…) I married [a man].</ta>
            <ta e="T479" id="Seg_7343" s="T474">– Elena turned seven years, you came here.</ta>
            <ta e="T527" id="Seg_7344" s="T525">– She is forty years old.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KiLS">
            <ta e="T62" id="Seg_7345" s="T59">– Erzähl doch auf Dolganisch.</ta>
            <ta e="T91" id="Seg_7346" s="T89">– Erzähl auf Dolganisch!</ta>
            <ta e="T150" id="Seg_7347" s="T148">Das ist Vasi.</ta>
            <ta e="T156" id="Seg_7348" s="T153">Ivan und Vasi.</ta>
            <ta e="T171" id="Seg_7349" s="T166">– Erzähl jetzt davon.</ta>
            <ta e="T233" id="Seg_7350" s="T226">– Zu wem wurden Katja und Tanja geschickt?</ta>
            <ta e="T255" id="Seg_7351" s="T253">– (…) Katja.</ta>
            <ta e="T421" id="Seg_7352" s="T417">– (…) ich heiratete [einen Mann].</ta>
            <ta e="T479" id="Seg_7353" s="T474">– Elena wurde sieben Jahre alt, du kamst hierher.</ta>
            <ta e="T527" id="Seg_7354" s="T525">– Sie ist vierzig Jahre alt.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KiLS">
            <ta e="T62" id="Seg_7355" s="T59">– Ну, расскажи по-долгански.</ta>
            <ta e="T91" id="Seg_7356" s="T89">– Расскажи по-долгански!</ta>
            <ta e="T150" id="Seg_7357" s="T148">Это Васи.</ta>
            <ta e="T156" id="Seg_7358" s="T153">Иван и Васи.</ta>
            <ta e="T171" id="Seg_7359" s="T166">– Расскажи теперь об этом.</ta>
            <ta e="T233" id="Seg_7360" s="T226">– К кому отправили Катю и Васю?</ta>
            <ta e="T255" id="Seg_7361" s="T253">– (…) Катя.</ta>
            <ta e="T421" id="Seg_7362" s="T417">– (…) я вышла замуж. </ta>
            <ta e="T479" id="Seg_7363" s="T474">– Елене исполнилось семь лет, ты приехала сюда.</ta>
            <ta e="T527" id="Seg_7364" s="T525">– Ей сорок лет.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr-KiLS">
            <ta e="T150" id="Seg_7365" s="T148">Eto Vasʼi.</ta>
            <ta e="T156" id="Seg_7366" s="T153">Ivan s Vasiliem</ta>
            <ta e="T171" id="Seg_7367" s="T166">govori ob etom seichas</ta>
            <ta e="T233" id="Seg_7368" s="T226">otpravlali k komu to katja i tanja</ta>
            <ta e="T527" id="Seg_7369" s="T525">ej sorok let</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KiLS" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-KiES"
                          name="ref"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-KiES"
                          name="st"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KiES"
                          name="ts"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KiES"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KiES"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-KiES"
                          name="mb"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-KiES"
                          name="mp"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-KiES"
                          name="ge"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-KiES"
                          name="gg"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-KiES"
                          name="gr"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-KiES"
                          name="mc"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-KiES"
                          name="ps"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-KiES"
                          name="SeR"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-KiES"
                          name="SyF"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-KiES"
                          name="IST"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-KiES"
                          name="Top"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-KiES"
                          name="Foc"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-KiES"
                          name="BOR"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-KiES"
                          name="BOR-Phon"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-KiES"
                          name="BOR-Morph"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-KiES"
                          name="CS"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KiES"
                          name="fe"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KiES"
                          name="fg"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KiES"
                          name="fr"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-KiES"
                          name="ltr"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KiES"
                          name="nt"
                          segmented-tier-id="tx-KiES"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-KiLS"
                          name="ref"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-KiLS"
                          name="st"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KiLS"
                          name="ts"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KiLS"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KiLS"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-KiLS"
                          name="mb"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-KiLS"
                          name="mp"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-KiLS"
                          name="ge"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-KiLS"
                          name="gg"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-KiLS"
                          name="gr"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-KiLS"
                          name="mc"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-KiLS"
                          name="ps"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-KiLS"
                          name="SeR"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-KiLS"
                          name="SyF"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-KiLS"
                          name="IST"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-KiLS"
                          name="Top"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-KiLS"
                          name="Foc"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-KiLS"
                          name="BOR"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-KiLS"
                          name="BOR-Phon"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-KiLS"
                          name="BOR-Morph"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-KiLS"
                          name="CS"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KiLS"
                          name="fe"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KiLS"
                          name="fg"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KiLS"
                          name="fr"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-KiLS"
                          name="ltr"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KiLS"
                          name="nt"
                          segmented-tier-id="tx-KiLS"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
