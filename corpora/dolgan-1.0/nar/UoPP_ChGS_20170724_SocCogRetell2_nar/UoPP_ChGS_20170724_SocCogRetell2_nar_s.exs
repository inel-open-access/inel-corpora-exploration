<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID49FABDF9-D05C-242B-B139-704802F20D8F">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>UoPP_ChGS_20170724_SocCogRetelling2_nar</transcription-name>
         <referenced-file url="UoPP_ChGS_20170724_SocCogRetell2_nar.wav" />
         <referenced-file url="UoPP_ChGS_20170724_SocCogRetell2_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\UoPP_ChGS_20170724_SocCogRetell2_nar\UoPP_ChGS_20170724_SocCogRetell2_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">545</ud-information>
            <ud-information attribute-name="# HIAT:w">312</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# e">311</ud-information>
            <ud-information attribute-name="# HIAT:u">71</ud-information>
            <ud-information attribute-name="# sc">132</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="DCh">
            <abbreviation>DCh</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="ChGS">
            <abbreviation>ChGS</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="UoPP">
            <abbreviation>UoPP</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.015" type="appl" />
         <tli id="T2" time="0.76925" type="appl" />
         <tli id="T3" time="1.5234999999999999" type="appl" />
         <tli id="T4" time="2.27775" type="appl" />
         <tli id="T5" time="3.032" type="appl" />
         <tli id="T6" time="3.566529411764706" type="appl" />
         <tli id="T7" time="4.101058823529412" type="appl" />
         <tli id="T8" time="4.635588235294118" type="appl" />
         <tli id="T9" time="5.170117647058824" type="appl" />
         <tli id="T10" time="5.70464705882353" type="appl" />
         <tli id="T11" time="6.2391764705882355" type="appl" />
         <tli id="T12" time="6.7737058823529415" type="appl" />
         <tli id="T13" time="7.308235294117647" type="appl" />
         <tli id="T14" time="7.842764705882353" type="appl" />
         <tli id="T15" time="8.37729411764706" type="appl" />
         <tli id="T16" time="8.911823529411766" type="appl" />
         <tli id="T17" time="9.446352941176471" type="appl" />
         <tli id="T18" time="9.980882352941176" type="appl" />
         <tli id="T19" time="10.515411764705883" type="appl" />
         <tli id="T20" time="11.04994117647059" type="appl" />
         <tli id="T21" time="11.584470588235295" type="appl" />
         <tli id="T22" time="12.119" type="appl" />
         <tli id="T23" time="12.653529411764707" type="appl" />
         <tli id="T24" time="13.188058823529413" type="appl" />
         <tli id="T25" time="13.722588235294118" type="appl" />
         <tli id="T26" time="14.257117647058823" type="appl" />
         <tli id="T27" time="14.79164705882353" type="appl" />
         <tli id="T28" time="15.326176470588237" type="appl" />
         <tli id="T29" time="15.860705882352942" type="appl" />
         <tli id="T30" time="16.395235294117647" type="appl" />
         <tli id="T31" time="16.929764705882356" type="appl" />
         <tli id="T32" time="17.46429411764706" type="appl" />
         <tli id="T33" time="17.998823529411766" type="appl" />
         <tli id="T34" time="18.53335294117647" type="appl" />
         <tli id="T35" time="19.06788235294118" type="appl" />
         <tli id="T36" time="19.602411764705884" type="appl" />
         <tli id="T37" time="20.13694117647059" type="appl" />
         <tli id="T38" time="20.671470588235294" type="appl" />
         <tli id="T39" time="21.206" type="appl" />
         <tli id="T40" time="22.0735" type="appl" />
         <tli id="T41" time="22.192813795967968" />
         <tli id="T42" time="22.941" type="appl" />
         <tli id="T43" time="23.2758" type="appl" />
         <tli id="T44" time="23.6286" type="appl" />
         <tli id="T45" time="23.9814" type="appl" />
         <tli id="T46" time="24.3342" type="appl" />
         <tli id="T47" time="24.687" type="appl" />
         <tli id="T48" time="24.691" type="appl" />
         <tli id="T49" time="25.113999999999997" type="appl" />
         <tli id="T50" time="25.515" type="appl" />
         <tli id="T51" time="25.537" type="appl" />
         <tli id="T52" time="26.104" type="appl" />
         <tli id="T53" time="26.125" type="appl" />
         <tli id="T54" time="26.615" type="appl" />
         <tli id="T55" time="27.001" type="appl" />
         <tli id="T56" time="27.1738" type="appl" />
         <tli id="T57" time="27.732599999999998" type="appl" />
         <tli id="T58" time="28.2914" type="appl" />
         <tli id="T59" time="28.850199999999997" type="appl" />
         <tli id="T60" time="29.398" type="appl" />
         <tli id="T61" time="29.409" type="appl" />
         <tli id="T62" time="31.26" type="appl" />
         <tli id="T63" time="33.122" type="appl" />
         <tli id="T64" time="34.97" type="appl" />
         <tli id="T65" time="35.05917925893528" />
         <tli id="T66" time="35.75125" type="appl" />
         <tli id="T67" time="36.5325" type="appl" />
         <tli id="T68" time="37.31375" type="appl" />
         <tli id="T69" time="38.095" type="appl" />
         <tli id="T70" time="40.64066317882606" />
         <tli id="T71" time="41.0075" type="appl" />
         <tli id="T72" time="41.581" type="appl" />
         <tli id="T73" time="42.1545" type="appl" />
         <tli id="T74" time="42.717" type="appl" />
         <tli id="T75" time="42.728" type="appl" />
         <tli id="T76" time="43.287" type="appl" />
         <tli id="T77" time="43.409" type="appl" />
         <tli id="T78" time="43.74966666666667" type="appl" />
         <tli id="T79" time="43.857" type="appl" />
         <tli id="T80" time="44.090333333333334" type="appl" />
         <tli id="T81" time="44.431" type="appl" />
         <tli id="T82" time="44.77166666666667" type="appl" />
         <tli id="T83" time="45.11233333333334" type="appl" />
         <tli id="T84" time="45.45" type="appl" />
         <tli id="T85" time="45.453" type="appl" />
         <tli id="T86" time="46.050333333333334" type="appl" />
         <tli id="T87" time="46.650666666666666" type="appl" />
         <tli id="T88" time="47.251" type="appl" />
         <tli id="T89" time="47.285" type="appl" />
         <tli id="T90" time="47.70363636363636" type="appl" />
         <tli id="T91" time="48.12227272727272" type="appl" />
         <tli id="T92" time="48.54090909090909" type="appl" />
         <tli id="T93" time="48.959545454545456" type="appl" />
         <tli id="T94" time="49.378181818181815" type="appl" />
         <tli id="T95" time="49.79681818181818" type="appl" />
         <tli id="T96" time="50.21545454545454" type="appl" />
         <tli id="T97" time="50.63409090909091" type="appl" />
         <tli id="T98" time="51.052727272727275" type="appl" />
         <tli id="T99" time="51.471363636363634" type="appl" />
         <tli id="T100" time="51.89" type="appl" />
         <tli id="T101" time="52.399375" type="appl" />
         <tli id="T102" time="52.90875" type="appl" />
         <tli id="T103" time="53.418125" type="appl" />
         <tli id="T104" time="53.9275" type="appl" />
         <tli id="T105" time="54.436875" type="appl" />
         <tli id="T106" time="54.946250000000006" type="appl" />
         <tli id="T107" time="55.455625000000005" type="appl" />
         <tli id="T108" time="55.62536446787525" />
         <tli id="T109" time="56.88999632097209" />
         <tli id="T110" time="57.115874999999996" type="appl" />
         <tli id="T111" time="57.40175" type="appl" />
         <tli id="T112" time="57.687625" type="appl" />
         <tli id="T113" time="57.9735" type="appl" />
         <tli id="T114" time="58.259375" type="appl" />
         <tli id="T115" time="58.545249999999996" type="appl" />
         <tli id="T116" time="58.831125" type="appl" />
         <tli id="T117" time="59.117" type="appl" />
         <tli id="T118" time="59.712666666666664" type="appl" />
         <tli id="T119" time="60.30833333333334" type="appl" />
         <tli id="T120" time="60.889" type="appl" />
         <tli id="T121" time="60.904" type="appl" />
         <tli id="T122" time="61.75188771164053" />
         <tli id="T123" time="62.367" type="appl" />
         <tli id="T124" time="62.817571428571426" type="appl" />
         <tli id="T125" time="63.268142857142855" type="appl" />
         <tli id="T126" time="63.718714285714285" type="appl" />
         <tli id="T127" time="64.1692857142857" type="appl" />
         <tli id="T128" time="64.61985714285714" type="appl" />
         <tli id="T129" time="65.07042857142858" type="appl" />
         <tli id="T130" time="65.521" type="appl" />
         <tli id="T131" time="66.411" type="appl" />
         <tli id="T132" time="67.4" type="appl" />
         <tli id="T133" time="68.12507184770101" />
         <tli id="T134" time="68.389" type="appl" />
         <tli id="T135" time="68.767" type="appl" />
         <tli id="T136" time="69.159" type="appl" />
         <tli id="T137" time="69.551" type="appl" />
         <tli id="T138" time="69.56166582097009" />
         <tli id="T139" time="70.23400000000001" type="appl" />
         <tli id="T140" time="70.913" type="appl" />
         <tli id="T141" time="71.592" type="appl" />
         <tli id="T142" time="73.94533272986777" />
         <tli id="T143" time="74.32075" type="appl" />
         <tli id="T144" time="74.7895" type="appl" />
         <tli id="T145" time="75.25825" type="appl" />
         <tli id="T146" time="75.411" type="appl" />
         <tli id="T147" time="75.727" type="appl" />
         <tli id="T148" time="75.735" type="appl" />
         <tli id="T149" time="75.876" type="appl" />
         <tli id="T150" time="76.34100000000001" type="appl" />
         <tli id="T151" time="76.643" type="appl" />
         <tli id="T152" time="76.806" type="appl" />
         <tli id="T153" time="77.271" type="appl" />
         <tli id="T154" time="77.551" type="appl" />
         <tli id="T155" time="77.559" type="appl" />
         <tli id="T156" time="77.928" type="appl" />
         <tli id="T157" time="78.297" type="appl" />
         <tli id="T158" time="78.666" type="appl" />
         <tli id="T159" time="79.035" type="appl" />
         <tli id="T160" time="79.404" type="appl" />
         <tli id="T161" time="79.418" type="appl" />
         <tli id="T162" time="80.24425000000001" type="appl" />
         <tli id="T163" time="81.07050000000001" type="appl" />
         <tli id="T164" time="81.89675" type="appl" />
         <tli id="T165" time="82.723" type="appl" />
         <tli id="T166" time="83.56366666666666" type="appl" />
         <tli id="T167" time="83.98400000000001" type="appl" />
         <tli id="T168" time="84.40433333333334" type="appl" />
         <tli id="T169" time="85.245" type="appl" />
         <tli id="T170" time="85.26" type="appl" />
         <tli id="T171" time="86.4625" type="appl" />
         <tli id="T172" time="87.30462285130565" />
         <tli id="T173" time="89.02458258676968" />
         <tli id="T174" time="89.518" type="appl" />
         <tli id="T175" time="90.137" type="appl" />
         <tli id="T176" time="90.756" type="appl" />
         <tli id="T177" time="91.375" type="appl" />
         <tli id="T178" time="91.994" type="appl" />
         <tli id="T179" time="92.613" type="appl" />
         <tli id="T180" time="93.232" type="appl" />
         <tli id="T181" time="93.851" type="appl" />
         <tli id="T182" time="94.47" type="appl" />
         <tli id="T183" time="95.089" type="appl" />
         <tli id="T184" time="95.708" type="appl" />
         <tli id="T185" time="98.68834073162635" />
         <tli id="T186" time="99.9566" type="appl" />
         <tli id="T187" time="101.0182" type="appl" />
         <tli id="T188" time="102.0798" type="appl" />
         <tli id="T189" time="103.1414" type="appl" />
         <tli id="T190" time="103.82300568453817" />
         <tli id="T191" time="104.09400194463849" />
         <tli id="T192" time="104.7558125" type="appl" />
         <tli id="T193" time="105.297625" type="appl" />
         <tli id="T194" time="105.8394375" type="appl" />
         <tli id="T195" time="106.38125" type="appl" />
         <tli id="T196" time="106.9230625" type="appl" />
         <tli id="T197" time="107.46487499999999" type="appl" />
         <tli id="T198" time="108.0066875" type="appl" />
         <tli id="T199" time="108.54849999999999" type="appl" />
         <tli id="T200" time="109.0903125" type="appl" />
         <tli id="T201" time="110.05075702728723" />
         <tli id="T202" time="110.1739375" type="appl" />
         <tli id="T203" time="110.71575" type="appl" />
         <tli id="T204" time="111.25756249999999" type="appl" />
         <tli id="T205" time="111.799375" type="appl" />
         <tli id="T206" time="112.34118749999999" type="appl" />
         <tli id="T207" time="113.02965864488458" />
         <tli id="T208" time="113.0309997593222" />
         <tli id="T209" time="113.35185714285714" type="appl" />
         <tli id="T210" time="113.81271428571429" type="appl" />
         <tli id="T211" time="114.27357142857143" type="appl" />
         <tli id="T212" time="114.73442857142858" type="appl" />
         <tli id="T213" time="115.19528571428572" type="appl" />
         <tli id="T214" time="115.65614285714287" type="appl" />
         <tli id="T215" time="115.71729103947493" />
         <tli id="T216" time="116.87059737372017" />
         <tli id="T217" time="117.065" type="appl" />
         <tli id="T218" time="118.01" type="appl" />
         <tli id="T219" time="118.955" type="appl" />
         <tli id="T220" time="119.9" type="appl" />
         <tli id="T221" time="120.845" type="appl" />
         <tli id="T222" time="121.79" type="appl" />
         <tli id="T223" time="122.63500669879758" />
         <tli id="T224" time="122.757" type="appl" />
         <tli id="T225" time="124.08" type="appl" />
         <tli id="T226" time="125.01040681946272" />
         <tli id="T227" time="126.05533333333334" type="appl" />
         <tli id="T228" time="128.02866666666668" type="appl" />
         <tli id="T229" time="129.13031037185328" />
         <tli id="T230" time="129.62363215644373" />
         <tli id="T231" time="131.20566666666667" type="appl" />
         <tli id="T232" time="132.40133333333333" type="appl" />
         <tli id="T233" time="132.89022235170486" />
         <tli id="T234" time="133.70232573606646" />
         <tli id="T235" time="135.822" type="appl" />
         <tli id="T236" time="138.1034336429175" />
         <tli id="T237" time="138.2837335397228" type="intp" />
         <tli id="T238" time="138.46403343652804" type="intp" />
         <tli id="T239" time="138.64433333333332" type="appl" />
         <tli id="T240" time="139.57977777777776" type="appl" />
         <tli id="T241" time="140.081" type="appl" />
         <tli id="T242" time="140.51522222222223" type="appl" />
         <tli id="T243" time="140.929" type="appl" />
         <tli id="T244" time="141.45066666666668" type="appl" />
         <tli id="T245" time="141.777" type="appl" />
         <tli id="T246" time="142.38611111111112" type="appl" />
         <tli id="T247" time="142.42499914103595" />
         <tli id="T248" time="142.99665242520663" />
         <tli id="T249" time="143.482" type="appl" />
         <tli id="T250" time="143.663303485464" />
         <tli id="T251" time="144.38995314114456" />
         <tli id="T252" time="144.55733203527754" />
         <tli id="T253" time="145.09733333333335" type="appl" />
         <tli id="T254" time="145.93066666666667" type="appl" />
         <tli id="T255" time="146.764" type="appl" />
         <tli id="T256" time="147.40321593350788" />
         <tli id="T257" time="147.926" type="appl" />
         <tli id="T258" time="148.9" type="appl" />
         <tli id="T259" time="149.073" type="appl" />
         <tli id="T260" time="150.22" type="appl" />
         <tli id="T261" time="150.663" type="intp" />
         <tli id="T262" time="151.106" type="appl" />
         <tli id="T263" time="151.36700000000002" type="appl" />
         <tli id="T264" time="152.514" type="appl" />
         <tli id="T265" time="152.533" type="appl" />
         <tli id="T266" time="152.83033333333333" type="appl" />
         <tli id="T267" time="152.999" type="appl" />
         <tli id="T268" time="153.14666666666668" type="appl" />
         <tli id="T269" time="153.35933333333332" type="appl" />
         <tli id="T270" time="153.463" type="appl" />
         <tli id="T271" time="153.71966666666665" type="appl" />
         <tli id="T272" time="154.07999999999998" type="appl" />
         <tli id="T273" time="154.44033333333334" type="appl" />
         <tli id="T274" time="154.80066666666667" type="appl" />
         <tli id="T275" time="155.15" type="appl" />
         <tli id="T276" time="155.3965" type="intp" />
         <tli id="T277" time="155.643" type="appl" />
         <tli id="T278" time="156.132" type="appl" />
         <tli id="T279" time="156.136" type="appl" />
         <tli id="T280" time="156.632" type="appl" />
         <tli id="T281" time="157.132" type="appl" />
         <tli id="T282" time="157.61101082154295" />
         <tli id="T283" time="158.78300000000002" type="appl" />
         <tli id="T284" time="159.29627084849943" />
         <tli id="T285" time="161.12289475360464" />
         <tli id="T286" time="161.5146" type="appl" />
         <tli id="T287" time="162.31220000000002" type="appl" />
         <tli id="T288" time="163.1098" type="appl" />
         <tli id="T289" time="163.90740000000002" type="appl" />
         <tli id="T290" time="163.96949478090363" />
         <tli id="T291" time="165.01613694550772" />
         <tli id="T292" time="165.35750000000002" type="appl" />
         <tli id="T293" time="165.988" type="appl" />
         <tli id="T294" time="166.183" type="appl" />
         <tli id="T295" time="166.61849999999998" type="appl" />
         <tli id="T296" time="167.249" type="appl" />
         <tli id="T297" time="167.257" type="appl" />
         <tli id="T298" time="167.7287142857143" type="appl" />
         <tli id="T299" time="167.96457142857145" type="intp" />
         <tli id="T300" time="168.20042857142857" type="appl" />
         <tli id="T301" time="168.67214285714286" type="appl" />
         <tli id="T302" time="169.14385714285714" type="appl" />
         <tli id="T303" time="169.61557142857143" type="appl" />
         <tli id="T304" time="170.0872857142857" type="appl" />
         <tli id="T305" time="170.43233827635734" />
         <tli id="T306" time="170.9129910866972" />
         <tli id="T307" time="171.259625" type="appl" />
         <tli id="T308" time="171.94625000000002" type="appl" />
         <tli id="T309" time="172.632875" type="appl" />
         <tli id="T310" time="173.3195" type="appl" />
         <tli id="T311" time="174.006125" type="appl" />
         <tli id="T312" time="174.69275" type="appl" />
         <tli id="T313" time="175.379375" type="appl" />
         <tli id="T314" time="176.065" type="appl" />
         <tli id="T315" time="176.63150000000002" type="intp" />
         <tli id="T316" time="177.02467092794893" />
         <tli id="T317" time="177.543" type="appl" />
         <tli id="T318" time="177.967625" type="appl" />
         <tli id="T319" time="178.73725000000002" type="appl" />
         <tli id="T320" time="179.021" type="appl" />
         <tli id="T321" time="179.506875" type="appl" />
         <tli id="T322" time="180.2765" type="appl" />
         <tli id="T323" time="181.046125" type="appl" />
         <tli id="T324" time="181.81574999999998" type="appl" />
         <tli id="T325" time="182.585375" type="appl" />
         <tli id="T326" time="183.355" type="appl" />
         <tli id="T327" time="187.684" type="appl" />
         <tli id="T328" time="188.32855555555557" type="appl" />
         <tli id="T329" time="188.9731111111111" type="appl" />
         <tli id="T330" time="189.61766666666668" type="appl" />
         <tli id="T331" time="190.26222222222222" type="appl" />
         <tli id="T332" time="190.9067777777778" type="appl" />
         <tli id="T333" time="191.55133333333333" type="appl" />
         <tli id="T334" time="192.1958888888889" type="appl" />
         <tli id="T335" time="192.84044444444444" type="appl" />
         <tli id="T336" time="193.00214845511223" />
         <tli id="T337" time="194.10212270453692" />
         <tli id="T338" time="194.2335" type="appl" />
         <tli id="T339" time="194.973" type="appl" />
         <tli id="T340" time="195.7125" type="appl" />
         <tli id="T341" time="196.452" type="appl" />
         <tli id="T342" time="197.19150000000002" type="appl" />
         <tli id="T343" time="197.931" type="appl" />
         <tli id="T344" time="198.6705" type="appl" />
         <tli id="T345" time="199.41" type="appl" />
         <tli id="T346" time="200.14950000000002" type="appl" />
         <tli id="T347" time="200.56197147843088" />
         <tli id="T348" time="201.16862394326506" />
         <tli id="T349" time="201.67375" type="appl" />
         <tli id="T350" time="202.4525" type="appl" />
         <tli id="T351" time="203.133" type="appl" />
         <tli id="T352" time="203.23125" type="appl" />
         <tli id="T353" time="203.6124" type="appl" />
         <tli id="T354" time="204.01" type="appl" />
         <tli id="T355" time="204.0918" type="appl" />
         <tli id="T356" time="204.5712" type="appl" />
         <tli id="T357" time="205.0506" type="appl" />
         <tli id="T358" time="205.53" type="appl" />
         <tli id="T359" time="206.181" type="appl" />
         <tli id="T360" time="207.262" type="appl" />
         <tli id="T361" time="207.8720086826093" />
         <tli id="T362" time="208.071" type="appl" />
         <tli id="T363" time="208.457" type="appl" />
         <tli id="T364" time="208.468" type="appl" />
         <tli id="T365" time="208.931" type="appl" />
         <tli id="T366" time="209.007" type="appl" />
         <tli id="T367" time="209.75" type="appl" />
         <tli id="T368" time="209.98149999999998" type="appl" />
         <tli id="T369" time="210.691" type="appl" />
         <tli id="T370" time="210.934" type="appl" />
         <tli id="T371" time="210.956" type="appl" />
         <tli id="T372" time="211.875" type="appl" />
         <tli id="T0" time="212.915" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-DCh"
                      id="tx-DCh"
                      speaker="DCh"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-DCh">
            <ts e="T41" id="Seg_0" n="sc" s="T1">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T1">
                  <nts id="Seg_3" n="HIAT:ip">–</nts>
                  <nts id="Seg_4" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_6" n="HIAT:w" s="T1">Nu</ts>
                  <nts id="Seg_7" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_9" n="HIAT:w" s="T2">laːdna</ts>
                  <nts id="Seg_10" n="HIAT:ip">,</nts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">paslʼednaja</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">zadačʼa</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_20" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">Vɨ</ts>
                  <nts id="Seg_23" n="HIAT:ip">,</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">pažalujsta</ts>
                  <nts id="Seg_27" n="HIAT:ip">,</nts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_30" n="HIAT:w" s="T7">prʼedstavlʼajtʼe</ts>
                  <nts id="Seg_31" n="HIAT:ip">,</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">što</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">Vɨ</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">odʼin</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">xaraktʼer</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">iz</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">rasskaza</ts>
                  <nts id="Seg_50" n="HIAT:ip">,</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">što</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">Vɨ</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">odʼin</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">čʼelavʼek</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">iz</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">rasskaza</ts>
                  <nts id="Seg_69" n="HIAT:ip">,</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_72" n="HIAT:w" s="T20">i</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_75" n="HIAT:w" s="T21">Vɨ</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_78" n="HIAT:w" s="T22">rasskažɨtʼe</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_81" n="HIAT:w" s="T23">pažalujsta</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_84" n="HIAT:w" s="T24">iz</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_87" n="HIAT:w" s="T25">pʼerspʼektʼiva</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_90" n="HIAT:w" s="T26">tavo</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_93" n="HIAT:w" s="T27">čʼelavʼeka</ts>
                  <nts id="Seg_94" n="HIAT:ip">,</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_97" n="HIAT:w" s="T28">to</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_100" n="HIAT:w" s="T29">jest</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_102" n="HIAT:ip">"</nts>
                  <nts id="Seg_103" n="HIAT:ip">(</nts>
                  <ts e="T31" id="Seg_105" n="HIAT:w" s="T30">mn-</ts>
                  <nts id="Seg_106" n="HIAT:ip">)</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_109" n="HIAT:w" s="T31">mnʼe</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_112" n="HIAT:w" s="T32">slučʼilasʼ</ts>
                  <nts id="Seg_113" n="HIAT:ip">,</nts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_116" n="HIAT:w" s="T33">ja</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_119" n="HIAT:w" s="T34">dʼelal</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_122" n="HIAT:w" s="T35">eta</ts>
                  <nts id="Seg_123" n="HIAT:ip">"</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_126" n="HIAT:w" s="T36">i</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_129" n="HIAT:w" s="T37">fsʼo</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_132" n="HIAT:w" s="T38">takoje</ts>
                  <nts id="Seg_133" n="HIAT:ip">.</nts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_136" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_138" n="HIAT:w" s="T39">Panʼatna</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_141" n="HIAT:w" s="T40">kak</ts>
                  <nts id="Seg_142" n="HIAT:ip">?</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T50" id="Seg_144" n="sc" s="T48">
               <ts e="T50" id="Seg_146" n="HIAT:u" s="T48">
                  <nts id="Seg_147" n="HIAT:ip">–</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_150" n="HIAT:w" s="T48">Da</ts>
                  <nts id="Seg_151" n="HIAT:ip">,</nts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_154" n="HIAT:w" s="T49">mm</ts>
                  <nts id="Seg_155" n="HIAT:ip">.</nts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T55" id="Seg_157" n="sc" s="T52">
               <ts e="T55" id="Seg_159" n="HIAT:u" s="T52">
                  <nts id="Seg_160" n="HIAT:ip">–</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_163" n="HIAT:w" s="T52">Lʼuboːva</ts>
                  <nts id="Seg_164" n="HIAT:ip">.</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T79" id="Seg_166" n="sc" s="T74">
               <ts e="T79" id="Seg_168" n="HIAT:u" s="T74">
                  <nts id="Seg_169" n="HIAT:ip">–</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_172" n="HIAT:w" s="T74">Adnavo</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_175" n="HIAT:w" s="T76">tolʼka</ts>
                  <nts id="Seg_176" n="HIAT:ip">.</nts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T100" id="Seg_178" n="sc" s="T89">
               <ts e="T100" id="Seg_180" n="HIAT:u" s="T89">
                  <nts id="Seg_181" n="HIAT:ip">–</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_184" n="HIAT:w" s="T89">Da</ts>
                  <nts id="Seg_185" n="HIAT:ip">,</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_188" n="HIAT:w" s="T90">da</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_191" n="HIAT:w" s="T91">Vɨ</ts>
                  <nts id="Seg_192" n="HIAT:ip">,</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_195" n="HIAT:w" s="T92">rasskažɨtʼe</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_198" n="HIAT:w" s="T93">Vɨ</ts>
                  <nts id="Seg_199" n="HIAT:ip">,</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_202" n="HIAT:w" s="T94">i</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_205" n="HIAT:w" s="T95">Vɨ</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_208" n="HIAT:w" s="T96">možetʼe</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_210" n="HIAT:ip">(</nts>
                  <ts e="T98" id="Seg_212" n="HIAT:w" s="T97">pam-</ts>
                  <nts id="Seg_213" n="HIAT:ip">)</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_216" n="HIAT:w" s="T98">pamočʼ</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_219" n="HIAT:w" s="T99">tagda</ts>
                  <nts id="Seg_220" n="HIAT:ip">.</nts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T153" id="Seg_222" n="sc" s="T146">
               <ts e="T153" id="Seg_224" n="HIAT:u" s="T146">
                  <nts id="Seg_225" n="HIAT:ip">–</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_228" n="HIAT:w" s="T146">Da</ts>
                  <nts id="Seg_229" n="HIAT:ip">,</nts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_232" n="HIAT:w" s="T149">pra</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_235" n="HIAT:w" s="T150">ženšʼinu</ts>
                  <nts id="Seg_236" n="HIAT:ip">,</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_239" n="HIAT:w" s="T152">kanʼešna</ts>
                  <nts id="Seg_240" n="HIAT:ip">.</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T362" id="Seg_242" n="sc" s="T360">
               <ts e="T362" id="Seg_244" n="HIAT:u" s="T360">
                  <nts id="Seg_245" n="HIAT:ip">–</nts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_248" n="HIAT:w" s="T360">Fsʼo</ts>
                  <nts id="Seg_249" n="HIAT:ip">.</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T365" id="Seg_251" n="sc" s="T364">
               <ts e="T365" id="Seg_253" n="HIAT:u" s="T364">
                  <nts id="Seg_254" n="HIAT:ip">–</nts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_257" n="HIAT:w" s="T364">Elete</ts>
                  <nts id="Seg_258" n="HIAT:ip">.</nts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T369" id="Seg_260" n="sc" s="T367">
               <ts e="T369" id="Seg_262" n="HIAT:u" s="T367">
                  <nts id="Seg_263" n="HIAT:ip">–</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_266" n="HIAT:w" s="T367">Elete</ts>
                  <nts id="Seg_267" n="HIAT:ip">.</nts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T372" id="Seg_269" n="sc" s="T370">
               <ts e="T372" id="Seg_271" n="HIAT:u" s="T370">
                  <nts id="Seg_272" n="HIAT:ip">–</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_275" n="HIAT:w" s="T370">Klassna</ts>
                  <nts id="Seg_276" n="HIAT:ip">.</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-DCh">
            <ts e="T41" id="Seg_278" n="sc" s="T1">
               <ts e="T2" id="Seg_280" n="e" s="T1">– Nu </ts>
               <ts e="T3" id="Seg_282" n="e" s="T2">laːdna, </ts>
               <ts e="T4" id="Seg_284" n="e" s="T3">paslʼednaja </ts>
               <ts e="T5" id="Seg_286" n="e" s="T4">zadačʼa. </ts>
               <ts e="T6" id="Seg_288" n="e" s="T5">Vɨ, </ts>
               <ts e="T7" id="Seg_290" n="e" s="T6">pažalujsta, </ts>
               <ts e="T8" id="Seg_292" n="e" s="T7">prʼedstavlʼajtʼe, </ts>
               <ts e="T9" id="Seg_294" n="e" s="T8">što </ts>
               <ts e="T10" id="Seg_296" n="e" s="T9">Vɨ </ts>
               <ts e="T11" id="Seg_298" n="e" s="T10">odʼin </ts>
               <ts e="T12" id="Seg_300" n="e" s="T11">xaraktʼer </ts>
               <ts e="T13" id="Seg_302" n="e" s="T12">iz </ts>
               <ts e="T14" id="Seg_304" n="e" s="T13">rasskaza, </ts>
               <ts e="T15" id="Seg_306" n="e" s="T14">što </ts>
               <ts e="T16" id="Seg_308" n="e" s="T15">Vɨ </ts>
               <ts e="T17" id="Seg_310" n="e" s="T16">odʼin </ts>
               <ts e="T18" id="Seg_312" n="e" s="T17">čʼelavʼek </ts>
               <ts e="T19" id="Seg_314" n="e" s="T18">iz </ts>
               <ts e="T20" id="Seg_316" n="e" s="T19">rasskaza, </ts>
               <ts e="T21" id="Seg_318" n="e" s="T20">i </ts>
               <ts e="T22" id="Seg_320" n="e" s="T21">Vɨ </ts>
               <ts e="T23" id="Seg_322" n="e" s="T22">rasskažɨtʼe </ts>
               <ts e="T24" id="Seg_324" n="e" s="T23">pažalujsta </ts>
               <ts e="T25" id="Seg_326" n="e" s="T24">iz </ts>
               <ts e="T26" id="Seg_328" n="e" s="T25">pʼerspʼektʼiva </ts>
               <ts e="T27" id="Seg_330" n="e" s="T26">tavo </ts>
               <ts e="T28" id="Seg_332" n="e" s="T27">čʼelavʼeka, </ts>
               <ts e="T29" id="Seg_334" n="e" s="T28">to </ts>
               <ts e="T30" id="Seg_336" n="e" s="T29">jest </ts>
               <ts e="T31" id="Seg_338" n="e" s="T30">"(mn-) </ts>
               <ts e="T32" id="Seg_340" n="e" s="T31">mnʼe </ts>
               <ts e="T33" id="Seg_342" n="e" s="T32">slučʼilasʼ, </ts>
               <ts e="T34" id="Seg_344" n="e" s="T33">ja </ts>
               <ts e="T35" id="Seg_346" n="e" s="T34">dʼelal </ts>
               <ts e="T36" id="Seg_348" n="e" s="T35">eta" </ts>
               <ts e="T37" id="Seg_350" n="e" s="T36">i </ts>
               <ts e="T38" id="Seg_352" n="e" s="T37">fsʼo </ts>
               <ts e="T39" id="Seg_354" n="e" s="T38">takoje. </ts>
               <ts e="T40" id="Seg_356" n="e" s="T39">Panʼatna </ts>
               <ts e="T41" id="Seg_358" n="e" s="T40">kak? </ts>
            </ts>
            <ts e="T50" id="Seg_359" n="sc" s="T48">
               <ts e="T49" id="Seg_361" n="e" s="T48">– Da, </ts>
               <ts e="T50" id="Seg_363" n="e" s="T49">mm. </ts>
            </ts>
            <ts e="T55" id="Seg_364" n="sc" s="T52">
               <ts e="T55" id="Seg_366" n="e" s="T52">– Lʼuboːva. </ts>
            </ts>
            <ts e="T79" id="Seg_367" n="sc" s="T74">
               <ts e="T76" id="Seg_369" n="e" s="T74">– Adnavo </ts>
               <ts e="T79" id="Seg_371" n="e" s="T76">tolʼka. </ts>
            </ts>
            <ts e="T100" id="Seg_372" n="sc" s="T89">
               <ts e="T90" id="Seg_374" n="e" s="T89">– Da, </ts>
               <ts e="T91" id="Seg_376" n="e" s="T90">da </ts>
               <ts e="T92" id="Seg_378" n="e" s="T91">Vɨ, </ts>
               <ts e="T93" id="Seg_380" n="e" s="T92">rasskažɨtʼe </ts>
               <ts e="T94" id="Seg_382" n="e" s="T93">Vɨ, </ts>
               <ts e="T95" id="Seg_384" n="e" s="T94">i </ts>
               <ts e="T96" id="Seg_386" n="e" s="T95">Vɨ </ts>
               <ts e="T97" id="Seg_388" n="e" s="T96">možetʼe </ts>
               <ts e="T98" id="Seg_390" n="e" s="T97">(pam-) </ts>
               <ts e="T99" id="Seg_392" n="e" s="T98">pamočʼ </ts>
               <ts e="T100" id="Seg_394" n="e" s="T99">tagda. </ts>
            </ts>
            <ts e="T153" id="Seg_395" n="sc" s="T146">
               <ts e="T149" id="Seg_397" n="e" s="T146">– Da, </ts>
               <ts e="T150" id="Seg_399" n="e" s="T149">pra </ts>
               <ts e="T152" id="Seg_401" n="e" s="T150">ženšʼinu, </ts>
               <ts e="T153" id="Seg_403" n="e" s="T152">kanʼešna. </ts>
            </ts>
            <ts e="T362" id="Seg_404" n="sc" s="T360">
               <ts e="T362" id="Seg_406" n="e" s="T360">– Fsʼo. </ts>
            </ts>
            <ts e="T365" id="Seg_407" n="sc" s="T364">
               <ts e="T365" id="Seg_409" n="e" s="T364">– Elete. </ts>
            </ts>
            <ts e="T369" id="Seg_410" n="sc" s="T367">
               <ts e="T369" id="Seg_412" n="e" s="T367">– Elete. </ts>
            </ts>
            <ts e="T372" id="Seg_413" n="sc" s="T370">
               <ts e="T372" id="Seg_415" n="e" s="T370">– Klassna. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-DCh">
            <ta e="T5" id="Seg_416" s="T1">UoPP_ChGS_20170724_SocCogRetell2_nar.DCh.001 (001.001)</ta>
            <ta e="T39" id="Seg_417" s="T5">UoPP_ChGS_20170724_SocCogRetell2_nar.DCh.002 (001.002)</ta>
            <ta e="T41" id="Seg_418" s="T39">UoPP_ChGS_20170724_SocCogRetell2_nar.DCh.003 (001.003)</ta>
            <ta e="T50" id="Seg_419" s="T48">UoPP_ChGS_20170724_SocCogRetell2_nar.DCh.004 (001.005)</ta>
            <ta e="T55" id="Seg_420" s="T52">UoPP_ChGS_20170724_SocCogRetell2_nar.DCh.005 (001.007)</ta>
            <ta e="T79" id="Seg_421" s="T74">UoPP_ChGS_20170724_SocCogRetell2_nar.DCh.006 (001.012)</ta>
            <ta e="T100" id="Seg_422" s="T89">UoPP_ChGS_20170724_SocCogRetell2_nar.DCh.007 (001.015)</ta>
            <ta e="T153" id="Seg_423" s="T146">UoPP_ChGS_20170724_SocCogRetell2_nar.DCh.008 (001.025)</ta>
            <ta e="T362" id="Seg_424" s="T360">UoPP_ChGS_20170724_SocCogRetell2_nar.DCh.009 (001.065)</ta>
            <ta e="T365" id="Seg_425" s="T364">UoPP_ChGS_20170724_SocCogRetell2_nar.DCh.010 (001.067)</ta>
            <ta e="T369" id="Seg_426" s="T367">UoPP_ChGS_20170724_SocCogRetell2_nar.DCh.011 (001.069)</ta>
            <ta e="T372" id="Seg_427" s="T370">UoPP_ChGS_20170724_SocCogRetell2_nar.DCh.012 (001.070)</ta>
         </annotation>
         <annotation name="st" tierref="st-DCh">
            <ta e="T5" id="Seg_428" s="T1">Ну ладно, последняя задача.</ta>
            <ta e="T39" id="Seg_429" s="T5">Вы, пожалуйста, представляйте что Вы один характер из рассказа, что Вы один человек из рассказа, и Вы расскажите пожалуйста из перспектива того человека, то ест "(мн-) мне случилось это, я делал это" и все такое.</ta>
            <ta e="T41" id="Seg_430" s="T39">Понятно как?</ta>
            <ta e="T50" id="Seg_431" s="T48">Да.</ta>
            <ta e="T55" id="Seg_432" s="T52">Любого.</ta>
            <ta e="T79" id="Seg_433" s="T74">Одного только.</ta>
            <ta e="T100" id="Seg_434" s="T89">Да, да Вы расскажите Вы, и Вы можете помочь тогда.</ta>
            <ta e="T153" id="Seg_435" s="T146">Да, про женщину, конечно.</ta>
            <ta e="T362" id="Seg_436" s="T360">Все.</ta>
            <ta e="T365" id="Seg_437" s="T364">Элэтэ.</ta>
            <ta e="T369" id="Seg_438" s="T367">Элэтэ.</ta>
            <ta e="T372" id="Seg_439" s="T370">Классно.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-DCh">
            <ta e="T5" id="Seg_440" s="T1">– Nu laːdna, paslʼednaja zadačʼa. </ta>
            <ta e="T39" id="Seg_441" s="T5">Vɨ, pažalujsta, prʼedstavlʼajtʼe, što Vɨ odʼin xaraktʼer iz rasskaza, što Vɨ odʼin čʼelavʼek iz rasskaza, i Vɨ rasskažɨtʼe pažalujsta iz pʼerspʼektʼiva tavo čʼelavʼeka, to jest "(mn-) mnʼe slučʼilasʼ, ja dʼelal eta" i fsʼo takoje. </ta>
            <ta e="T41" id="Seg_442" s="T39">Panʼatna kak? </ta>
            <ta e="T50" id="Seg_443" s="T48">– Da, mm. </ta>
            <ta e="T55" id="Seg_444" s="T52">– Lʼuboːva. </ta>
            <ta e="T79" id="Seg_445" s="T74">– Adnavo tolʼka. </ta>
            <ta e="T100" id="Seg_446" s="T89">– Da, da Vɨ, rasskažɨtʼe Vɨ, i Vɨ možetʼe (pam-) pamočʼ tagda. </ta>
            <ta e="T153" id="Seg_447" s="T146">– Da, pra ženšʼinu, kanʼešna. </ta>
            <ta e="T362" id="Seg_448" s="T360">– Fsʼo. </ta>
            <ta e="T365" id="Seg_449" s="T364">– Elete. </ta>
            <ta e="T369" id="Seg_450" s="T367">– Elete. </ta>
            <ta e="T372" id="Seg_451" s="T370">– Klassna. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-DCh">
            <ta e="T365" id="Seg_452" s="T364">ele-te</ta>
            <ta e="T369" id="Seg_453" s="T367">ele-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp-DCh">
            <ta e="T365" id="Seg_454" s="T364">ele-tA</ta>
            <ta e="T369" id="Seg_455" s="T367">ele-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge-DCh">
            <ta e="T365" id="Seg_456" s="T364">last-3SG.[NOM]</ta>
            <ta e="T369" id="Seg_457" s="T367">last-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg-DCh">
            <ta e="T365" id="Seg_458" s="T364">letzter-3SG.[NOM]</ta>
            <ta e="T369" id="Seg_459" s="T367">letzter-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-DCh">
            <ta e="T365" id="Seg_460" s="T364">последний-3SG.[NOM]</ta>
            <ta e="T369" id="Seg_461" s="T367">последний-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-DCh">
            <ta e="T365" id="Seg_462" s="T364">adj-n:(poss).[n:case]</ta>
            <ta e="T369" id="Seg_463" s="T367">adj-n:(poss).[n:case]</ta>
         </annotation>
         <annotation name="ps" tierref="ps-DCh">
            <ta e="T365" id="Seg_464" s="T364">adj</ta>
            <ta e="T369" id="Seg_465" s="T367">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-DCh" />
         <annotation name="SyF" tierref="SyF-DCh" />
         <annotation name="IST" tierref="IST-DCh" />
         <annotation name="Top" tierref="Top-DCh" />
         <annotation name="Foc" tierref="Foc-DCh" />
         <annotation name="BOR" tierref="BOR-DCh" />
         <annotation name="BOR-Phon" tierref="BOR-Phon-DCh" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-DCh" />
         <annotation name="CS" tierref="CS-DCh">
            <ta e="T5" id="Seg_466" s="T1">RUS:ext</ta>
            <ta e="T39" id="Seg_467" s="T5">RUS:ext</ta>
            <ta e="T41" id="Seg_468" s="T39">RUS:ext</ta>
            <ta e="T50" id="Seg_469" s="T48">RUS:ext</ta>
            <ta e="T55" id="Seg_470" s="T52">RUS:ext</ta>
            <ta e="T79" id="Seg_471" s="T74">RUS:ext</ta>
            <ta e="T100" id="Seg_472" s="T89">RUS:ext</ta>
            <ta e="T153" id="Seg_473" s="T146">RUS:ext</ta>
            <ta e="T362" id="Seg_474" s="T360">RUS:ext</ta>
            <ta e="T372" id="Seg_475" s="T370">RUS:ext</ta>
         </annotation>
         <annotation name="fe" tierref="fe-DCh">
            <ta e="T5" id="Seg_476" s="T1">– Ok, well, the last task.</ta>
            <ta e="T39" id="Seg_477" s="T5">Please imagine that You are one of the story's characters, one of the story's people, and You please tell from the perspective of that person, that is "to me happened, I was doing" and so on.</ta>
            <ta e="T41" id="Seg_478" s="T39">Clear how?</ta>
            <ta e="T50" id="Seg_479" s="T48">– Yes, mhm.</ta>
            <ta e="T55" id="Seg_480" s="T52">– Any.</ta>
            <ta e="T79" id="Seg_481" s="T74">– Only one.</ta>
            <ta e="T100" id="Seg_482" s="T89">– Yes, You, You tell and You can help then.</ta>
            <ta e="T153" id="Seg_483" s="T146">– Yes, about the woman, sure.</ta>
            <ta e="T362" id="Seg_484" s="T360">– That's all.</ta>
            <ta e="T365" id="Seg_485" s="T364">– That's it.</ta>
            <ta e="T369" id="Seg_486" s="T367">– That's it.</ta>
            <ta e="T372" id="Seg_487" s="T370">– Great.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-DCh">
            <ta e="T5" id="Seg_488" s="T1">– Nun gut, die letzte Aufgabe.</ta>
            <ta e="T39" id="Seg_489" s="T5">Sie stellen sich bitte vor, dass Sie einer der Charaktere aus der Geschichte, einer der Menschen aus der Geschichte sind, und Sie erzählen bitte aus der Perspektive dieses Menschen, also "mir ist passiert, ich habe gemacht" und so.</ta>
            <ta e="T41" id="Seg_490" s="T39">Klar wie?</ta>
            <ta e="T50" id="Seg_491" s="T48">– Ja, mhm.</ta>
            <ta e="T55" id="Seg_492" s="T52">– Einen beliebigen.</ta>
            <ta e="T79" id="Seg_493" s="T74">– Nur einen.</ta>
            <ta e="T100" id="Seg_494" s="T89">– Ja, Sie, erzählen Sie und Sie können dann helfen.</ta>
            <ta e="T153" id="Seg_495" s="T146">– Ja, über die Frau, klar.</ta>
            <ta e="T362" id="Seg_496" s="T360">– Alles.</ta>
            <ta e="T365" id="Seg_497" s="T364">– Schluss.</ta>
            <ta e="T369" id="Seg_498" s="T367">– Das war's.</ta>
            <ta e="T372" id="Seg_499" s="T370">– Klasse.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-DCh" />
         <annotation name="ltr" tierref="ltr-DCh" />
         <annotation name="nt" tierref="nt-DCh" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-ChGS"
                      id="tx-ChGS"
                      speaker="ChGS"
                      type="t">
         <timeline-fork end="T355" start="T353">
            <tli id="T353.tx-ChGS.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-ChGS">
            <ts e="T47" id="Seg_500" n="sc" s="T41">
               <ts e="T47" id="Seg_502" n="HIAT:u" s="T41">
                  <nts id="Seg_503" n="HIAT:ip">–</nts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_506" n="HIAT:w" s="T41">Vɨbʼirajem</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_509" n="HIAT:w" s="T43">adnavo</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_512" n="HIAT:w" s="T44">iz</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_515" n="HIAT:w" s="T45">gʼerojev</ts>
                  <nts id="Seg_516" n="HIAT:ip">,</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_519" n="HIAT:w" s="T46">da</ts>
                  <nts id="Seg_520" n="HIAT:ip">?</nts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T53" id="Seg_522" n="sc" s="T50">
               <ts e="T53" id="Seg_524" n="HIAT:u" s="T50">
                  <nts id="Seg_525" n="HIAT:ip">–</nts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_528" n="HIAT:w" s="T50">Lʼuboːva</ts>
                  <nts id="Seg_529" n="HIAT:ip">.</nts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T61" id="Seg_531" n="sc" s="T54">
               <ts e="T61" id="Seg_533" n="HIAT:u" s="T54">
                  <nts id="Seg_534" n="HIAT:ip">–</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_537" n="HIAT:w" s="T54">Ilʼi</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_540" n="HIAT:w" s="T56">ženšʼinu</ts>
                  <nts id="Seg_541" n="HIAT:ip">,</nts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_544" n="HIAT:w" s="T57">a</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_547" n="HIAT:w" s="T58">dʼedušku</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_550" n="HIAT:w" s="T59">možna</ts>
                  <nts id="Seg_551" n="HIAT:ip">.</nts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T69" id="Seg_553" n="sc" s="T65">
               <ts e="T69" id="Seg_555" n="HIAT:u" s="T65">
                  <nts id="Seg_556" n="HIAT:ip">–</nts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_559" n="HIAT:w" s="T65">Kimi</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_562" n="HIAT:w" s="T66">ɨlabɨt</ts>
                  <nts id="Seg_563" n="HIAT:ip">,</nts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_566" n="HIAT:w" s="T67">kimi</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_569" n="HIAT:w" s="T68">ɨlagɨn</ts>
                  <nts id="Seg_570" n="HIAT:ip">?</nts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T84" id="Seg_572" n="sc" s="T77">
               <ts e="T84" id="Seg_574" n="HIAT:u" s="T77">
                  <nts id="Seg_575" n="HIAT:ip">–</nts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_578" n="HIAT:w" s="T77">A</ts>
                  <nts id="Seg_579" n="HIAT:ip">,</nts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_582" n="HIAT:w" s="T78">adnavo</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_585" n="HIAT:w" s="T80">tolʼko</ts>
                  <nts id="Seg_586" n="HIAT:ip">,</nts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_589" n="HIAT:w" s="T81">vdvajom</ts>
                  <nts id="Seg_590" n="HIAT:ip">,</nts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_593" n="HIAT:w" s="T82">da</ts>
                  <nts id="Seg_594" n="HIAT:ip">,</nts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_597" n="HIAT:w" s="T83">možem</ts>
                  <nts id="Seg_598" n="HIAT:ip">.</nts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T108" id="Seg_600" n="sc" s="T100">
               <ts e="T108" id="Seg_602" n="HIAT:u" s="T100">
                  <nts id="Seg_603" n="HIAT:ip">–</nts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_606" n="HIAT:w" s="T100">Tuguj</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_609" n="HIAT:w" s="T101">kimi</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_612" n="HIAT:w" s="T102">ɨlɨ͡akpɨtɨj</ts>
                  <nts id="Seg_613" n="HIAT:ip">,</nts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_616" n="HIAT:w" s="T103">kimi</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_619" n="HIAT:w" s="T104">kepsiːbit</ts>
                  <nts id="Seg_620" n="HIAT:ip">,</nts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_623" n="HIAT:w" s="T105">kuhagan</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_626" n="HIAT:w" s="T106">kihini</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_629" n="HIAT:w" s="T107">ɨlɨ͡akpɨt</ts>
                  <nts id="Seg_630" n="HIAT:ip">?</nts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T117" id="Seg_632" n="sc" s="T109">
               <ts e="T117" id="Seg_634" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_636" n="HIAT:w" s="T109">Min</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_639" n="HIAT:w" s="T110">ke</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_642" n="HIAT:w" s="T111">kahan</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_645" n="HIAT:w" s="T112">da</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_648" n="HIAT:w" s="T113">itinnik</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_651" n="HIAT:w" s="T114">sʼituacɨjaga</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_654" n="HIAT:w" s="T115">tübehe</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_657" n="HIAT:w" s="T116">ilippin</ts>
                  <nts id="Seg_658" n="HIAT:ip">.</nts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T122" id="Seg_660" n="sc" s="T120">
               <ts e="T122" id="Seg_662" n="HIAT:u" s="T120">
                  <nts id="Seg_663" n="HIAT:ip">–</nts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_666" n="HIAT:w" s="T120">Dʼaktarɨ</ts>
                  <nts id="Seg_667" n="HIAT:ip">.</nts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T130" id="Seg_669" n="sc" s="T123">
               <ts e="T130" id="Seg_671" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_673" n="HIAT:w" s="T123">Min</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_676" n="HIAT:w" s="T124">že</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_679" n="HIAT:w" s="T125">dʼaktar</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_682" n="HIAT:w" s="T126">kimiger</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_685" n="HIAT:w" s="T127">emi͡e</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_688" n="HIAT:w" s="T128">hu͡ok</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_691" n="HIAT:w" s="T129">etim</ts>
                  <nts id="Seg_692" n="HIAT:ip">.</nts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T134" id="Seg_694" n="sc" s="T131">
               <ts e="T134" id="Seg_696" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_698" n="HIAT:w" s="T131">En</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_701" n="HIAT:w" s="T132">kepseː</ts>
                  <nts id="Seg_702" n="HIAT:ip">.</nts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T141" id="Seg_704" n="sc" s="T138">
               <ts e="T141" id="Seg_706" n="HIAT:u" s="T138">
                  <nts id="Seg_707" n="HIAT:ip">–</nts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_710" n="HIAT:w" s="T138">Min</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_713" n="HIAT:w" s="T139">kömölöhü͡öm</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_716" n="HIAT:w" s="T140">eni͡eke</ts>
                  <nts id="Seg_717" n="HIAT:ip">.</nts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T160" id="Seg_719" n="sc" s="T155">
               <ts e="T160" id="Seg_721" n="HIAT:u" s="T155">
                  <nts id="Seg_722" n="HIAT:ip">–</nts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_725" n="HIAT:w" s="T155">Kak</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_728" n="HIAT:w" s="T156">budta</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_731" n="HIAT:w" s="T157">en</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_734" n="HIAT:w" s="T158">iti</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_737" n="HIAT:w" s="T159">dʼaktargɨn</ts>
                  <nts id="Seg_738" n="HIAT:ip">.</nts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T169" id="Seg_740" n="sc" s="T165">
               <ts e="T169" id="Seg_742" n="HIAT:u" s="T165">
                  <nts id="Seg_743" n="HIAT:ip">–</nts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_746" n="HIAT:w" s="T165">Kak</ts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_749" n="HIAT:w" s="T167">budta</ts>
                  <nts id="Seg_750" n="HIAT:ip">…</nts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T172" id="Seg_752" n="sc" s="T170">
               <ts e="T172" id="Seg_754" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_756" n="HIAT:w" s="T170">Naːda</ts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_759" n="HIAT:w" s="T171">padumatʼ</ts>
                  <nts id="Seg_760" n="HIAT:ip">.</nts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T247" id="Seg_762" n="sc" s="T241">
               <ts e="T247" id="Seg_764" n="HIAT:u" s="T241">
                  <nts id="Seg_765" n="HIAT:ip">–</nts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_768" n="HIAT:w" s="T241">Kaːjɨːga</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_771" n="HIAT:w" s="T243">oloron</ts>
                  <nts id="Seg_772" n="HIAT:ip">,</nts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_775" n="HIAT:w" s="T245">kaːjɨːga</ts>
                  <nts id="Seg_776" n="HIAT:ip">.</nts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T251" id="Seg_778" n="sc" s="T249">
               <ts e="T251" id="Seg_780" n="HIAT:u" s="T249">
                  <nts id="Seg_781" n="HIAT:ip">–</nts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_784" n="HIAT:w" s="T249">Öjdömmüt</ts>
                  <nts id="Seg_785" n="HIAT:ip">.</nts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T261" id="Seg_787" n="sc" s="T258">
               <ts e="T261" id="Seg_789" n="HIAT:u" s="T258">
                  <nts id="Seg_790" n="HIAT:ip">–</nts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_793" n="HIAT:w" s="T258">Ej</ts>
                  <nts id="Seg_794" n="HIAT:ip">.</nts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T265" id="Seg_796" n="sc" s="T262">
               <ts e="T265" id="Seg_798" n="HIAT:u" s="T262">
                  <nts id="Seg_799" n="HIAT:ip">–</nts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_802" n="HIAT:w" s="T262">Ti͡ere</ts>
                  <nts id="Seg_803" n="HIAT:ip">.</nts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T276" id="Seg_805" n="sc" s="T267">
               <ts e="T276" id="Seg_807" n="HIAT:u" s="T267">
                  <nts id="Seg_808" n="HIAT:ip">–</nts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_811" n="HIAT:w" s="T267">Min</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_814" n="HIAT:w" s="T269">ti͡ere</ts>
                  <nts id="Seg_815" n="HIAT:ip">,</nts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_818" n="HIAT:w" s="T271">min</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_821" n="HIAT:w" s="T272">min</ts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_824" n="HIAT:w" s="T273">ti͡ere</ts>
                  <nts id="Seg_825" n="HIAT:ip">,</nts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_828" n="HIAT:w" s="T274">ja</ts>
                  <nts id="Seg_829" n="HIAT:ip">.</nts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T281" id="Seg_831" n="sc" s="T278">
               <ts e="T281" id="Seg_833" n="HIAT:u" s="T278">
                  <nts id="Seg_834" n="HIAT:ip">–</nts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_837" n="HIAT:w" s="T278">Nʼe</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_840" n="HIAT:w" s="T280">to</ts>
                  <nts id="Seg_841" n="HIAT:ip">.</nts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T296" id="Seg_843" n="sc" s="T291">
               <ts e="T296" id="Seg_845" n="HIAT:u" s="T291">
                  <nts id="Seg_846" n="HIAT:ip">–</nts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_849" n="HIAT:w" s="T291">En</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_852" n="HIAT:w" s="T292">en</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_855" n="HIAT:w" s="T293">kajdak</ts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_858" n="HIAT:w" s="T295">kimniːgin</ts>
                  <nts id="Seg_859" n="HIAT:ip">.</nts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T305" id="Seg_861" n="sc" s="T297">
               <ts e="T305" id="Seg_863" n="HIAT:u" s="T297">
                  <ts e="T298" id="Seg_865" n="HIAT:w" s="T297">Ü͡öregin</ts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_868" n="HIAT:w" s="T298">ü͡örbekkin</ts>
                  <nts id="Seg_869" n="HIAT:ip">,</nts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_872" n="HIAT:w" s="T300">en</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_875" n="HIAT:w" s="T301">bejeŋ</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_878" n="HIAT:w" s="T302">tuskunan</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_881" n="HIAT:w" s="T303">kepsi͡ekkin</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_884" n="HIAT:w" s="T304">naːda</ts>
                  <nts id="Seg_885" n="HIAT:ip">.</nts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T320" id="Seg_887" n="sc" s="T314">
               <ts e="T320" id="Seg_889" n="HIAT:u" s="T314">
                  <nts id="Seg_890" n="HIAT:ip">–</nts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_893" n="HIAT:w" s="T314">Herene-herene</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_896" n="HIAT:w" s="T317">tohujbutum</ts>
                  <nts id="Seg_897" n="HIAT:ip">.</nts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T358" id="Seg_899" n="sc" s="T351">
               <ts e="T358" id="Seg_901" n="HIAT:u" s="T351">
                  <nts id="Seg_902" n="HIAT:ip">–</nts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_905" n="HIAT:w" s="T351">Maladʼec</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353.tx-ChGS.1" id="Seg_908" n="HIAT:w" s="T353">vaabše</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_911" n="HIAT:w" s="T353.tx-ChGS.1">-</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_914" n="HIAT:w" s="T355">ta</ts>
                  <nts id="Seg_915" n="HIAT:ip">,</nts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_918" n="HIAT:w" s="T356">xarašo</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_921" n="HIAT:w" s="T357">že</ts>
                  <nts id="Seg_922" n="HIAT:ip">.</nts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-ChGS">
            <ts e="T47" id="Seg_924" n="sc" s="T41">
               <ts e="T43" id="Seg_926" n="e" s="T41">– Vɨbʼirajem </ts>
               <ts e="T44" id="Seg_928" n="e" s="T43">adnavo </ts>
               <ts e="T45" id="Seg_930" n="e" s="T44">iz </ts>
               <ts e="T46" id="Seg_932" n="e" s="T45">gʼerojev, </ts>
               <ts e="T47" id="Seg_934" n="e" s="T46">da? </ts>
            </ts>
            <ts e="T53" id="Seg_935" n="sc" s="T50">
               <ts e="T53" id="Seg_937" n="e" s="T50">– Lʼuboːva. </ts>
            </ts>
            <ts e="T61" id="Seg_938" n="sc" s="T54">
               <ts e="T56" id="Seg_940" n="e" s="T54">– Ilʼi </ts>
               <ts e="T57" id="Seg_942" n="e" s="T56">ženšʼinu, </ts>
               <ts e="T58" id="Seg_944" n="e" s="T57">a </ts>
               <ts e="T59" id="Seg_946" n="e" s="T58">dʼedušku </ts>
               <ts e="T61" id="Seg_948" n="e" s="T59">možna. </ts>
            </ts>
            <ts e="T69" id="Seg_949" n="sc" s="T65">
               <ts e="T66" id="Seg_951" n="e" s="T65">– Kimi </ts>
               <ts e="T67" id="Seg_953" n="e" s="T66">ɨlabɨt, </ts>
               <ts e="T68" id="Seg_955" n="e" s="T67">kimi </ts>
               <ts e="T69" id="Seg_957" n="e" s="T68">ɨlagɨn? </ts>
            </ts>
            <ts e="T84" id="Seg_958" n="sc" s="T77">
               <ts e="T78" id="Seg_960" n="e" s="T77">– A, </ts>
               <ts e="T80" id="Seg_962" n="e" s="T78">adnavo </ts>
               <ts e="T81" id="Seg_964" n="e" s="T80">tolʼko, </ts>
               <ts e="T82" id="Seg_966" n="e" s="T81">vdvajom, </ts>
               <ts e="T83" id="Seg_968" n="e" s="T82">da, </ts>
               <ts e="T84" id="Seg_970" n="e" s="T83">možem. </ts>
            </ts>
            <ts e="T108" id="Seg_971" n="sc" s="T100">
               <ts e="T101" id="Seg_973" n="e" s="T100">– Tuguj </ts>
               <ts e="T102" id="Seg_975" n="e" s="T101">kimi </ts>
               <ts e="T103" id="Seg_977" n="e" s="T102">ɨlɨ͡akpɨtɨj, </ts>
               <ts e="T104" id="Seg_979" n="e" s="T103">kimi </ts>
               <ts e="T105" id="Seg_981" n="e" s="T104">kepsiːbit, </ts>
               <ts e="T106" id="Seg_983" n="e" s="T105">kuhagan </ts>
               <ts e="T107" id="Seg_985" n="e" s="T106">kihini </ts>
               <ts e="T108" id="Seg_987" n="e" s="T107">ɨlɨ͡akpɨt? </ts>
            </ts>
            <ts e="T117" id="Seg_988" n="sc" s="T109">
               <ts e="T110" id="Seg_990" n="e" s="T109">Min </ts>
               <ts e="T111" id="Seg_992" n="e" s="T110">ke </ts>
               <ts e="T112" id="Seg_994" n="e" s="T111">kahan </ts>
               <ts e="T113" id="Seg_996" n="e" s="T112">da </ts>
               <ts e="T114" id="Seg_998" n="e" s="T113">itinnik </ts>
               <ts e="T115" id="Seg_1000" n="e" s="T114">sʼituacɨjaga </ts>
               <ts e="T116" id="Seg_1002" n="e" s="T115">tübehe </ts>
               <ts e="T117" id="Seg_1004" n="e" s="T116">ilippin. </ts>
            </ts>
            <ts e="T122" id="Seg_1005" n="sc" s="T120">
               <ts e="T122" id="Seg_1007" n="e" s="T120">– Dʼaktarɨ. </ts>
            </ts>
            <ts e="T130" id="Seg_1008" n="sc" s="T123">
               <ts e="T124" id="Seg_1010" n="e" s="T123">Min </ts>
               <ts e="T125" id="Seg_1012" n="e" s="T124">že </ts>
               <ts e="T126" id="Seg_1014" n="e" s="T125">dʼaktar </ts>
               <ts e="T127" id="Seg_1016" n="e" s="T126">kimiger </ts>
               <ts e="T128" id="Seg_1018" n="e" s="T127">emi͡e </ts>
               <ts e="T129" id="Seg_1020" n="e" s="T128">hu͡ok </ts>
               <ts e="T130" id="Seg_1022" n="e" s="T129">etim. </ts>
            </ts>
            <ts e="T134" id="Seg_1023" n="sc" s="T131">
               <ts e="T132" id="Seg_1025" n="e" s="T131">En </ts>
               <ts e="T134" id="Seg_1027" n="e" s="T132">kepseː. </ts>
            </ts>
            <ts e="T141" id="Seg_1028" n="sc" s="T138">
               <ts e="T139" id="Seg_1030" n="e" s="T138">– Min </ts>
               <ts e="T140" id="Seg_1032" n="e" s="T139">kömölöhü͡öm </ts>
               <ts e="T141" id="Seg_1034" n="e" s="T140">eni͡eke. </ts>
            </ts>
            <ts e="T160" id="Seg_1035" n="sc" s="T155">
               <ts e="T156" id="Seg_1037" n="e" s="T155">– Kak </ts>
               <ts e="T157" id="Seg_1039" n="e" s="T156">budta </ts>
               <ts e="T158" id="Seg_1041" n="e" s="T157">en </ts>
               <ts e="T159" id="Seg_1043" n="e" s="T158">iti </ts>
               <ts e="T160" id="Seg_1045" n="e" s="T159">dʼaktargɨn. </ts>
            </ts>
            <ts e="T169" id="Seg_1046" n="sc" s="T165">
               <ts e="T167" id="Seg_1048" n="e" s="T165">– Kak </ts>
               <ts e="T169" id="Seg_1050" n="e" s="T167">budta… </ts>
            </ts>
            <ts e="T172" id="Seg_1051" n="sc" s="T170">
               <ts e="T171" id="Seg_1053" n="e" s="T170">Naːda </ts>
               <ts e="T172" id="Seg_1055" n="e" s="T171">padumatʼ. </ts>
            </ts>
            <ts e="T247" id="Seg_1056" n="sc" s="T241">
               <ts e="T243" id="Seg_1058" n="e" s="T241">– Kaːjɨːga </ts>
               <ts e="T245" id="Seg_1060" n="e" s="T243">oloron, </ts>
               <ts e="T247" id="Seg_1062" n="e" s="T245">kaːjɨːga. </ts>
            </ts>
            <ts e="T251" id="Seg_1063" n="sc" s="T249">
               <ts e="T251" id="Seg_1065" n="e" s="T249">– Öjdömmüt. </ts>
            </ts>
            <ts e="T261" id="Seg_1066" n="sc" s="T258">
               <ts e="T261" id="Seg_1068" n="e" s="T258">– Ej. </ts>
            </ts>
            <ts e="T265" id="Seg_1069" n="sc" s="T262">
               <ts e="T265" id="Seg_1071" n="e" s="T262">– Ti͡ere. </ts>
            </ts>
            <ts e="T276" id="Seg_1072" n="sc" s="T267">
               <ts e="T269" id="Seg_1074" n="e" s="T267">– Min </ts>
               <ts e="T271" id="Seg_1076" n="e" s="T269">ti͡ere, </ts>
               <ts e="T272" id="Seg_1078" n="e" s="T271">min </ts>
               <ts e="T273" id="Seg_1080" n="e" s="T272">min </ts>
               <ts e="T274" id="Seg_1082" n="e" s="T273">ti͡ere, </ts>
               <ts e="T276" id="Seg_1084" n="e" s="T274">ja. </ts>
            </ts>
            <ts e="T281" id="Seg_1085" n="sc" s="T278">
               <ts e="T280" id="Seg_1087" n="e" s="T278">– Nʼe </ts>
               <ts e="T281" id="Seg_1089" n="e" s="T280">to. </ts>
            </ts>
            <ts e="T296" id="Seg_1090" n="sc" s="T291">
               <ts e="T292" id="Seg_1092" n="e" s="T291">– En </ts>
               <ts e="T293" id="Seg_1094" n="e" s="T292">en </ts>
               <ts e="T295" id="Seg_1096" n="e" s="T293">kajdak </ts>
               <ts e="T296" id="Seg_1098" n="e" s="T295">kimniːgin. </ts>
            </ts>
            <ts e="T305" id="Seg_1099" n="sc" s="T297">
               <ts e="T298" id="Seg_1101" n="e" s="T297">Ü͡öregin </ts>
               <ts e="T300" id="Seg_1103" n="e" s="T298">ü͡örbekkin, </ts>
               <ts e="T301" id="Seg_1105" n="e" s="T300">en </ts>
               <ts e="T302" id="Seg_1107" n="e" s="T301">bejeŋ </ts>
               <ts e="T303" id="Seg_1109" n="e" s="T302">tuskunan </ts>
               <ts e="T304" id="Seg_1111" n="e" s="T303">kepsi͡ekkin </ts>
               <ts e="T305" id="Seg_1113" n="e" s="T304">naːda. </ts>
            </ts>
            <ts e="T320" id="Seg_1114" n="sc" s="T314">
               <ts e="T317" id="Seg_1116" n="e" s="T314">– Herene-herene </ts>
               <ts e="T320" id="Seg_1118" n="e" s="T317">tohujbutum. </ts>
            </ts>
            <ts e="T358" id="Seg_1119" n="sc" s="T351">
               <ts e="T353" id="Seg_1121" n="e" s="T351">– Maladʼec </ts>
               <ts e="T355" id="Seg_1123" n="e" s="T353">vaabše - </ts>
               <ts e="T356" id="Seg_1125" n="e" s="T355">ta, </ts>
               <ts e="T357" id="Seg_1127" n="e" s="T356">xarašo </ts>
               <ts e="T358" id="Seg_1129" n="e" s="T357">že. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-ChGS">
            <ta e="T47" id="Seg_1130" s="T41">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.001 (001.004)</ta>
            <ta e="T53" id="Seg_1131" s="T50">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.002 (001.006)</ta>
            <ta e="T61" id="Seg_1132" s="T54">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.003 (001.008)</ta>
            <ta e="T69" id="Seg_1133" s="T65">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.004 (001.010)</ta>
            <ta e="T84" id="Seg_1134" s="T77">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.005 (001.013)</ta>
            <ta e="T108" id="Seg_1135" s="T100">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.006 (001.016)</ta>
            <ta e="T117" id="Seg_1136" s="T109">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.007 (001.017)</ta>
            <ta e="T122" id="Seg_1137" s="T120">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.008 (001.019)</ta>
            <ta e="T130" id="Seg_1138" s="T123">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.009 (001.020)</ta>
            <ta e="T134" id="Seg_1139" s="T131">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.010 (001.021)</ta>
            <ta e="T141" id="Seg_1140" s="T138">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.011 (001.023)</ta>
            <ta e="T160" id="Seg_1141" s="T155">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.012 (001.027)</ta>
            <ta e="T169" id="Seg_1142" s="T165">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.013 (001.030)</ta>
            <ta e="T172" id="Seg_1143" s="T170">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.014 (001.031)</ta>
            <ta e="T247" id="Seg_1144" s="T241">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.015 (001.042)</ta>
            <ta e="T251" id="Seg_1145" s="T249">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.016 (001.043)</ta>
            <ta e="T261" id="Seg_1146" s="T258">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.017 (001.046)</ta>
            <ta e="T265" id="Seg_1147" s="T262">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.018 (001.047)</ta>
            <ta e="T276" id="Seg_1148" s="T267">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.019 (001.049)</ta>
            <ta e="T281" id="Seg_1149" s="T278">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.020 (001.051)</ta>
            <ta e="T296" id="Seg_1150" s="T291">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.021 (001.054)</ta>
            <ta e="T305" id="Seg_1151" s="T297">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.022 (001.056)</ta>
            <ta e="T320" id="Seg_1152" s="T314">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.023 (001.058)</ta>
            <ta e="T358" id="Seg_1153" s="T351">UoPP_ChGS_20170724_SocCogRetell2_nar.ChGS.024 (001.063)</ta>
         </annotation>
         <annotation name="st" tierref="st-ChGS">
            <ta e="T47" id="Seg_1154" s="T41">Выбираем одного из героев, да?</ta>
            <ta e="T53" id="Seg_1155" s="T50">Любого.</ta>
            <ta e="T61" id="Seg_1156" s="T54">Или женщину, или дедушку можно.</ta>
            <ta e="T69" id="Seg_1157" s="T65">Кими ылабыт, кими ылагын?</ta>
            <ta e="T84" id="Seg_1158" s="T77">А одного только, да, вдвоем можно.</ta>
            <ta e="T108" id="Seg_1159" s="T100">Тугуй кими ылыакпытый, кими кэпсиибит, куһаган киһини ылыакпыт?</ta>
            <ta e="T117" id="Seg_1160" s="T109">Мин кэ каһан даагыны итинник ситуацияга түбэһэ иликпин.</ta>
            <ta e="T122" id="Seg_1161" s="T120">Дьактары.</ta>
            <ta e="T130" id="Seg_1162" s="T123">Мин кэ дьактар кимигэр эмиэ һуок этим.</ta>
            <ta e="T134" id="Seg_1163" s="T131">Эн кэпсээ.</ta>
            <ta e="T141" id="Seg_1164" s="T138">Мин көмөлөһүөм эниэкэ.</ta>
            <ta e="T160" id="Seg_1165" s="T155">Как будто эн ити дьактаргын.</ta>
            <ta e="T169" id="Seg_1166" s="T165">Как будто…</ta>
            <ta e="T172" id="Seg_1167" s="T170">Надо подумать.</ta>
            <ta e="T247" id="Seg_1168" s="T241">Кайыыга олорон, кайыыга.</ta>
            <ta e="T251" id="Seg_1169" s="T249">Өйдөммүт.</ta>
            <ta e="T261" id="Seg_1170" s="T258">Эй.</ta>
            <ta e="T265" id="Seg_1171" s="T262">(Тиэр-)…</ta>
            <ta e="T276" id="Seg_1172" s="T267">Мин тиэрэ, мин мин тиэрэ, я это.</ta>
            <ta e="T281" id="Seg_1173" s="T278">Не то.</ta>
            <ta e="T296" id="Seg_1174" s="T291">Эн эн кайдак, кимниигин.</ta>
            <ta e="T305" id="Seg_1175" s="T297">Үөрэгин үөрбэккин эн бэйэӈ тускунан кэпсиэккин наада.</ta>
            <ta e="T320" id="Seg_1176" s="T314">Һэрэнэ-һэрэнэ тоһуйбутум.</ta>
            <ta e="T358" id="Seg_1177" s="T351">Молодец вообще-то, үчүгэй дии.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-ChGS">
            <ta e="T47" id="Seg_1178" s="T41">– Vɨbʼirajem adnavo iz gʼerojev, da? </ta>
            <ta e="T53" id="Seg_1179" s="T50">– Lʼuboːva. </ta>
            <ta e="T61" id="Seg_1180" s="T54">– Ilʼi ženšʼinu, a dʼedušku možna. </ta>
            <ta e="T69" id="Seg_1181" s="T65">– Kimi ɨlabɨt, kimi ɨlagɨn? </ta>
            <ta e="T84" id="Seg_1182" s="T77">– A, adnavo tolʼko, vdvajom, da, možem. </ta>
            <ta e="T108" id="Seg_1183" s="T100">– Tuguj kimi ɨlɨ͡akpɨtɨj, kimi kepsiːbit, kuhagan kihini ɨlɨ͡akpɨt? </ta>
            <ta e="T117" id="Seg_1184" s="T109">Min ke kahan da itinnik sʼituacɨjaga tübehe ilippin. </ta>
            <ta e="T122" id="Seg_1185" s="T120">– Dʼaktarɨ. </ta>
            <ta e="T130" id="Seg_1186" s="T123">Min že dʼaktar kimiger emi͡e hu͡ok etim. </ta>
            <ta e="T134" id="Seg_1187" s="T131">En kepseː. </ta>
            <ta e="T141" id="Seg_1188" s="T138">– Min kömölöhü͡öm eni͡eke. </ta>
            <ta e="T160" id="Seg_1189" s="T155">– Kak budta en iti dʼaktargɨn. </ta>
            <ta e="T169" id="Seg_1190" s="T165">– Kak budta… </ta>
            <ta e="T172" id="Seg_1191" s="T170">Naːda padumatʼ. </ta>
            <ta e="T247" id="Seg_1192" s="T241">– Kaːjɨːga oloron, kaːjɨːga. </ta>
            <ta e="T251" id="Seg_1193" s="T249">– Öjdömmüt. </ta>
            <ta e="T261" id="Seg_1194" s="T258">– Ej. </ta>
            <ta e="T265" id="Seg_1195" s="T262">– Ti͡ere. </ta>
            <ta e="T276" id="Seg_1196" s="T267">– Min ti͡ere, min min ti͡ere, ja. </ta>
            <ta e="T281" id="Seg_1197" s="T278">– Nʼe to. </ta>
            <ta e="T296" id="Seg_1198" s="T291">– En en kajdak kimniːgin. </ta>
            <ta e="T305" id="Seg_1199" s="T297">Ü͡öregin ü͡örbekkin, en bejeŋ tuskunan kepsi͡ekkin naːda. </ta>
            <ta e="T320" id="Seg_1200" s="T314">– Herene-herene tohujbutum. </ta>
            <ta e="T358" id="Seg_1201" s="T351">– Maladʼec vaabše - ta, xarašo že. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-ChGS">
            <ta e="T66" id="Seg_1202" s="T65">kim-i</ta>
            <ta e="T67" id="Seg_1203" s="T66">ɨl-a-bɨt</ta>
            <ta e="T68" id="Seg_1204" s="T67">kim-i</ta>
            <ta e="T69" id="Seg_1205" s="T68">ɨl-a-gɨn</ta>
            <ta e="T101" id="Seg_1206" s="T100">tug=uj</ta>
            <ta e="T102" id="Seg_1207" s="T101">kim-i</ta>
            <ta e="T103" id="Seg_1208" s="T102">ɨl-ɨ͡ak-pɨt=ɨj</ta>
            <ta e="T104" id="Seg_1209" s="T103">kim-i</ta>
            <ta e="T105" id="Seg_1210" s="T104">keps-iː-bit</ta>
            <ta e="T106" id="Seg_1211" s="T105">kuhagan</ta>
            <ta e="T107" id="Seg_1212" s="T106">kihi-ni</ta>
            <ta e="T108" id="Seg_1213" s="T107">ɨl-ɨ͡ak-pɨt</ta>
            <ta e="T110" id="Seg_1214" s="T109">min</ta>
            <ta e="T111" id="Seg_1215" s="T110">ke</ta>
            <ta e="T112" id="Seg_1216" s="T111">kahan</ta>
            <ta e="T113" id="Seg_1217" s="T112">da</ta>
            <ta e="T114" id="Seg_1218" s="T113">itinnik</ta>
            <ta e="T115" id="Seg_1219" s="T114">sʼituacɨja-ga</ta>
            <ta e="T116" id="Seg_1220" s="T115">tübeh-e</ta>
            <ta e="T117" id="Seg_1221" s="T116">ilip-pin</ta>
            <ta e="T122" id="Seg_1222" s="T120">dʼaktar-ɨ</ta>
            <ta e="T124" id="Seg_1223" s="T123">min</ta>
            <ta e="T125" id="Seg_1224" s="T124">že</ta>
            <ta e="T126" id="Seg_1225" s="T125">dʼaktar</ta>
            <ta e="T127" id="Seg_1226" s="T126">kim-i-ger</ta>
            <ta e="T128" id="Seg_1227" s="T127">emi͡e</ta>
            <ta e="T129" id="Seg_1228" s="T128">hu͡ok</ta>
            <ta e="T130" id="Seg_1229" s="T129">e-ti-m</ta>
            <ta e="T132" id="Seg_1230" s="T131">en</ta>
            <ta e="T134" id="Seg_1231" s="T132">kepseː</ta>
            <ta e="T139" id="Seg_1232" s="T138">min</ta>
            <ta e="T140" id="Seg_1233" s="T139">kömölöh-ü͡ö-m</ta>
            <ta e="T141" id="Seg_1234" s="T140">eni͡e-ke</ta>
            <ta e="T158" id="Seg_1235" s="T157">en</ta>
            <ta e="T159" id="Seg_1236" s="T158">iti</ta>
            <ta e="T160" id="Seg_1237" s="T159">dʼaktar-gɨn</ta>
            <ta e="T243" id="Seg_1238" s="T241">kaːjɨː-ga</ta>
            <ta e="T245" id="Seg_1239" s="T243">olor-on</ta>
            <ta e="T247" id="Seg_1240" s="T245">kaːjɨː-ga</ta>
            <ta e="T251" id="Seg_1241" s="T249">öj-döm-müt</ta>
            <ta e="T261" id="Seg_1242" s="T258">ej</ta>
            <ta e="T265" id="Seg_1243" s="T262">ti͡ere</ta>
            <ta e="T269" id="Seg_1244" s="T267">min</ta>
            <ta e="T271" id="Seg_1245" s="T269">ti͡ere</ta>
            <ta e="T272" id="Seg_1246" s="T271">min</ta>
            <ta e="T273" id="Seg_1247" s="T272">min</ta>
            <ta e="T274" id="Seg_1248" s="T273">ti͡ere</ta>
            <ta e="T292" id="Seg_1249" s="T291">en</ta>
            <ta e="T293" id="Seg_1250" s="T292">en</ta>
            <ta e="T295" id="Seg_1251" s="T293">kajdak</ta>
            <ta e="T296" id="Seg_1252" s="T295">kim-n-iː-gin</ta>
            <ta e="T298" id="Seg_1253" s="T297">ü͡ör-e-gin</ta>
            <ta e="T300" id="Seg_1254" s="T298">ü͡ör-bek-kin</ta>
            <ta e="T301" id="Seg_1255" s="T300">en</ta>
            <ta e="T302" id="Seg_1256" s="T301">beje-ŋ</ta>
            <ta e="T303" id="Seg_1257" s="T302">tus-ku-nan</ta>
            <ta e="T304" id="Seg_1258" s="T303">keps-i͡ek-ki-n</ta>
            <ta e="T305" id="Seg_1259" s="T304">naːda</ta>
            <ta e="T317" id="Seg_1260" s="T314">heren-e-heren-e</ta>
            <ta e="T320" id="Seg_1261" s="T317">tohuj-but-u-m</ta>
         </annotation>
         <annotation name="mp" tierref="mp-ChGS">
            <ta e="T66" id="Seg_1262" s="T65">kim-nI</ta>
            <ta e="T67" id="Seg_1263" s="T66">ɨl-A-BIt</ta>
            <ta e="T68" id="Seg_1264" s="T67">kim-nI</ta>
            <ta e="T69" id="Seg_1265" s="T68">ɨl-A-GIn</ta>
            <ta e="T101" id="Seg_1266" s="T100">tu͡ok=Ij</ta>
            <ta e="T102" id="Seg_1267" s="T101">kim-nI</ta>
            <ta e="T103" id="Seg_1268" s="T102">ɨl-IAK-BIt=Ij</ta>
            <ta e="T104" id="Seg_1269" s="T103">kim-nI</ta>
            <ta e="T105" id="Seg_1270" s="T104">kepseː-A-BIt</ta>
            <ta e="T106" id="Seg_1271" s="T105">kuhagan</ta>
            <ta e="T107" id="Seg_1272" s="T106">kihi-nI</ta>
            <ta e="T108" id="Seg_1273" s="T107">ɨl-IAK-BIt</ta>
            <ta e="T110" id="Seg_1274" s="T109">min</ta>
            <ta e="T111" id="Seg_1275" s="T110">ka</ta>
            <ta e="T112" id="Seg_1276" s="T111">kahan</ta>
            <ta e="T113" id="Seg_1277" s="T112">da</ta>
            <ta e="T114" id="Seg_1278" s="T113">itinnik</ta>
            <ta e="T115" id="Seg_1279" s="T114">sʼituacɨja-GA</ta>
            <ta e="T116" id="Seg_1280" s="T115">tübes-A</ta>
            <ta e="T117" id="Seg_1281" s="T116">ilik-BIn</ta>
            <ta e="T122" id="Seg_1282" s="T120">dʼaktar-nI</ta>
            <ta e="T124" id="Seg_1283" s="T123">min</ta>
            <ta e="T125" id="Seg_1284" s="T124">že</ta>
            <ta e="T126" id="Seg_1285" s="T125">dʼaktar</ta>
            <ta e="T127" id="Seg_1286" s="T126">kim-tI-GAr</ta>
            <ta e="T128" id="Seg_1287" s="T127">emi͡e</ta>
            <ta e="T129" id="Seg_1288" s="T128">hu͡ok</ta>
            <ta e="T130" id="Seg_1289" s="T129">e-TI-m</ta>
            <ta e="T132" id="Seg_1290" s="T131">en</ta>
            <ta e="T134" id="Seg_1291" s="T132">kepseː</ta>
            <ta e="T139" id="Seg_1292" s="T138">min</ta>
            <ta e="T140" id="Seg_1293" s="T139">kömölös-IAK-m</ta>
            <ta e="T141" id="Seg_1294" s="T140">en-GA</ta>
            <ta e="T158" id="Seg_1295" s="T157">en</ta>
            <ta e="T159" id="Seg_1296" s="T158">iti</ta>
            <ta e="T160" id="Seg_1297" s="T159">dʼaktar-GIn</ta>
            <ta e="T243" id="Seg_1298" s="T241">kaːjɨː-GA</ta>
            <ta e="T245" id="Seg_1299" s="T243">olor-An</ta>
            <ta e="T247" id="Seg_1300" s="T245">kaːjɨː-GA</ta>
            <ta e="T251" id="Seg_1301" s="T249">öj-LAN-BIT</ta>
            <ta e="T261" id="Seg_1302" s="T258">ej</ta>
            <ta e="T265" id="Seg_1303" s="T262">ti͡ere</ta>
            <ta e="T269" id="Seg_1304" s="T267">min</ta>
            <ta e="T271" id="Seg_1305" s="T269">ti͡ere</ta>
            <ta e="T272" id="Seg_1306" s="T271">min</ta>
            <ta e="T273" id="Seg_1307" s="T272">min</ta>
            <ta e="T274" id="Seg_1308" s="T273">ti͡ere</ta>
            <ta e="T292" id="Seg_1309" s="T291">en</ta>
            <ta e="T293" id="Seg_1310" s="T292">en</ta>
            <ta e="T295" id="Seg_1311" s="T293">kajdak</ta>
            <ta e="T296" id="Seg_1312" s="T295">kim-LAː-A-GIn</ta>
            <ta e="T298" id="Seg_1313" s="T297">ü͡ör-A-GIn</ta>
            <ta e="T300" id="Seg_1314" s="T298">ü͡ör-BAT-GIn</ta>
            <ta e="T301" id="Seg_1315" s="T300">en</ta>
            <ta e="T302" id="Seg_1316" s="T301">beje-ŋ</ta>
            <ta e="T303" id="Seg_1317" s="T302">tus-GI-nAn</ta>
            <ta e="T304" id="Seg_1318" s="T303">kepseː-IAK-GI-n</ta>
            <ta e="T305" id="Seg_1319" s="T304">naːda</ta>
            <ta e="T317" id="Seg_1320" s="T314">heren-A-heren-A</ta>
            <ta e="T320" id="Seg_1321" s="T317">tohuj-BIT-I-m</ta>
         </annotation>
         <annotation name="ge" tierref="ge-ChGS">
            <ta e="T66" id="Seg_1322" s="T65">who-ACC</ta>
            <ta e="T67" id="Seg_1323" s="T66">take-PRS-1PL</ta>
            <ta e="T68" id="Seg_1324" s="T67">who-ACC</ta>
            <ta e="T69" id="Seg_1325" s="T68">take-PRS-2SG</ta>
            <ta e="T101" id="Seg_1326" s="T100">what.[NOM]=Q</ta>
            <ta e="T102" id="Seg_1327" s="T101">who-ACC</ta>
            <ta e="T103" id="Seg_1328" s="T102">take-FUT-1PL=Q</ta>
            <ta e="T104" id="Seg_1329" s="T103">who-ACC</ta>
            <ta e="T105" id="Seg_1330" s="T104">tell-PRS-1PL</ta>
            <ta e="T106" id="Seg_1331" s="T105">bad.[NOM]</ta>
            <ta e="T107" id="Seg_1332" s="T106">human.being-ACC</ta>
            <ta e="T108" id="Seg_1333" s="T107">take-FUT-1PL</ta>
            <ta e="T110" id="Seg_1334" s="T109">1SG.[NOM]</ta>
            <ta e="T111" id="Seg_1335" s="T110">well</ta>
            <ta e="T112" id="Seg_1336" s="T111">when</ta>
            <ta e="T113" id="Seg_1337" s="T112">NEG</ta>
            <ta e="T114" id="Seg_1338" s="T113">such</ta>
            <ta e="T115" id="Seg_1339" s="T114">situation-DAT/LOC</ta>
            <ta e="T116" id="Seg_1340" s="T115">get.into-CVB.SIM</ta>
            <ta e="T117" id="Seg_1341" s="T116">not.yet-1SG</ta>
            <ta e="T122" id="Seg_1342" s="T120">woman-ACC</ta>
            <ta e="T124" id="Seg_1343" s="T123">1SG.[NOM]</ta>
            <ta e="T125" id="Seg_1344" s="T124">EMPH</ta>
            <ta e="T126" id="Seg_1345" s="T125">woman.[NOM]</ta>
            <ta e="T127" id="Seg_1346" s="T126">who-3SG-DAT/LOC</ta>
            <ta e="T128" id="Seg_1347" s="T127">again</ta>
            <ta e="T129" id="Seg_1348" s="T128">NEG.EX</ta>
            <ta e="T130" id="Seg_1349" s="T129">be-PST1-1SG</ta>
            <ta e="T132" id="Seg_1350" s="T131">2SG.[NOM]</ta>
            <ta e="T134" id="Seg_1351" s="T132">tell.[IMP.2SG]</ta>
            <ta e="T139" id="Seg_1352" s="T138">1SG.[NOM]</ta>
            <ta e="T140" id="Seg_1353" s="T139">help-FUT-1SG</ta>
            <ta e="T141" id="Seg_1354" s="T140">2SG-DAT/LOC</ta>
            <ta e="T158" id="Seg_1355" s="T157">2SG.[NOM]</ta>
            <ta e="T159" id="Seg_1356" s="T158">that.[NOM]</ta>
            <ta e="T160" id="Seg_1357" s="T159">woman-2SG</ta>
            <ta e="T243" id="Seg_1358" s="T241">prison-DAT/LOC</ta>
            <ta e="T245" id="Seg_1359" s="T243">sit-CVB.SEQ</ta>
            <ta e="T247" id="Seg_1360" s="T245">prison-DAT/LOC</ta>
            <ta e="T251" id="Seg_1361" s="T249">mind-VBZ-PST2.[3SG]</ta>
            <ta e="T261" id="Seg_1362" s="T258">oh</ta>
            <ta e="T265" id="Seg_1363" s="T262">wrong</ta>
            <ta e="T269" id="Seg_1364" s="T267">1SG.[NOM]</ta>
            <ta e="T271" id="Seg_1365" s="T269">wrong.[NOM]</ta>
            <ta e="T272" id="Seg_1366" s="T271">1SG.[NOM]</ta>
            <ta e="T273" id="Seg_1367" s="T272">1SG.[NOM]</ta>
            <ta e="T274" id="Seg_1368" s="T273">wrong.[NOM]</ta>
            <ta e="T292" id="Seg_1369" s="T291">2SG.[NOM]</ta>
            <ta e="T293" id="Seg_1370" s="T292">2SG.[NOM]</ta>
            <ta e="T295" id="Seg_1371" s="T293">how</ta>
            <ta e="T296" id="Seg_1372" s="T295">who-VBZ-PRS-2SG</ta>
            <ta e="T298" id="Seg_1373" s="T297">be.happy-PRS-2SG</ta>
            <ta e="T300" id="Seg_1374" s="T298">be.happy-NEG-2SG</ta>
            <ta e="T301" id="Seg_1375" s="T300">2SG.[NOM]</ta>
            <ta e="T302" id="Seg_1376" s="T301">self-2SG.[NOM]</ta>
            <ta e="T303" id="Seg_1377" s="T302">side-2SG-INSTR</ta>
            <ta e="T304" id="Seg_1378" s="T303">tell-PTCP.FUT-2SG-ACC</ta>
            <ta e="T305" id="Seg_1379" s="T304">need.to</ta>
            <ta e="T317" id="Seg_1380" s="T314">be.careful-CVB.SIM-be.careful-CVB.SIM</ta>
            <ta e="T320" id="Seg_1381" s="T317">receive-PST2-EP-1SG</ta>
         </annotation>
         <annotation name="gg" tierref="gg-ChGS">
            <ta e="T66" id="Seg_1382" s="T65">wer-ACC</ta>
            <ta e="T67" id="Seg_1383" s="T66">nehmen-PRS-1PL</ta>
            <ta e="T68" id="Seg_1384" s="T67">wer-ACC</ta>
            <ta e="T69" id="Seg_1385" s="T68">nehmen-PRS-2SG</ta>
            <ta e="T101" id="Seg_1386" s="T100">was.[NOM]=Q</ta>
            <ta e="T102" id="Seg_1387" s="T101">wer-ACC</ta>
            <ta e="T103" id="Seg_1388" s="T102">nehmen-FUT-1PL=Q</ta>
            <ta e="T104" id="Seg_1389" s="T103">wer-ACC</ta>
            <ta e="T105" id="Seg_1390" s="T104">erzählen-PRS-1PL</ta>
            <ta e="T106" id="Seg_1391" s="T105">schlecht.[NOM]</ta>
            <ta e="T107" id="Seg_1392" s="T106">Mensch-ACC</ta>
            <ta e="T108" id="Seg_1393" s="T107">nehmen-FUT-1PL</ta>
            <ta e="T110" id="Seg_1394" s="T109">1SG.[NOM]</ta>
            <ta e="T111" id="Seg_1395" s="T110">nun</ta>
            <ta e="T112" id="Seg_1396" s="T111">wann</ta>
            <ta e="T113" id="Seg_1397" s="T112">NEG</ta>
            <ta e="T114" id="Seg_1398" s="T113">solch</ta>
            <ta e="T115" id="Seg_1399" s="T114">Situation-DAT/LOC</ta>
            <ta e="T116" id="Seg_1400" s="T115">geraten-CVB.SIM</ta>
            <ta e="T117" id="Seg_1401" s="T116">noch.nicht-1SG</ta>
            <ta e="T122" id="Seg_1402" s="T120">Frau-ACC</ta>
            <ta e="T124" id="Seg_1403" s="T123">1SG.[NOM]</ta>
            <ta e="T125" id="Seg_1404" s="T124">EMPH</ta>
            <ta e="T126" id="Seg_1405" s="T125">Frau.[NOM]</ta>
            <ta e="T127" id="Seg_1406" s="T126">wer-3SG-DAT/LOC</ta>
            <ta e="T128" id="Seg_1407" s="T127">wieder</ta>
            <ta e="T129" id="Seg_1408" s="T128">NEG.EX</ta>
            <ta e="T130" id="Seg_1409" s="T129">sein-PST1-1SG</ta>
            <ta e="T132" id="Seg_1410" s="T131">2SG.[NOM]</ta>
            <ta e="T134" id="Seg_1411" s="T132">erzählen.[IMP.2SG]</ta>
            <ta e="T139" id="Seg_1412" s="T138">1SG.[NOM]</ta>
            <ta e="T140" id="Seg_1413" s="T139">helfen-FUT-1SG</ta>
            <ta e="T141" id="Seg_1414" s="T140">2SG-DAT/LOC</ta>
            <ta e="T158" id="Seg_1415" s="T157">2SG.[NOM]</ta>
            <ta e="T159" id="Seg_1416" s="T158">dieses.[NOM]</ta>
            <ta e="T160" id="Seg_1417" s="T159">Frau-2SG</ta>
            <ta e="T243" id="Seg_1418" s="T241">Gefängnis-DAT/LOC</ta>
            <ta e="T245" id="Seg_1419" s="T243">sitzen-CVB.SEQ</ta>
            <ta e="T247" id="Seg_1420" s="T245">Gefängnis-DAT/LOC</ta>
            <ta e="T251" id="Seg_1421" s="T249">Verstand-VBZ-PST2.[3SG]</ta>
            <ta e="T261" id="Seg_1422" s="T258">ach</ta>
            <ta e="T265" id="Seg_1423" s="T262">falsch</ta>
            <ta e="T269" id="Seg_1424" s="T267">1SG.[NOM]</ta>
            <ta e="T271" id="Seg_1425" s="T269">falsch.[NOM]</ta>
            <ta e="T272" id="Seg_1426" s="T271">1SG.[NOM]</ta>
            <ta e="T273" id="Seg_1427" s="T272">1SG.[NOM]</ta>
            <ta e="T274" id="Seg_1428" s="T273">falsch.[NOM]</ta>
            <ta e="T292" id="Seg_1429" s="T291">2SG.[NOM]</ta>
            <ta e="T293" id="Seg_1430" s="T292">2SG.[NOM]</ta>
            <ta e="T295" id="Seg_1431" s="T293">wie</ta>
            <ta e="T296" id="Seg_1432" s="T295">wer-VBZ-PRS-2SG</ta>
            <ta e="T298" id="Seg_1433" s="T297">sich.freuen-PRS-2SG</ta>
            <ta e="T300" id="Seg_1434" s="T298">sich.freuen-NEG-2SG</ta>
            <ta e="T301" id="Seg_1435" s="T300">2SG.[NOM]</ta>
            <ta e="T302" id="Seg_1436" s="T301">selbst-2SG.[NOM]</ta>
            <ta e="T303" id="Seg_1437" s="T302">Seite-2SG-INSTR</ta>
            <ta e="T304" id="Seg_1438" s="T303">erzählen-PTCP.FUT-2SG-ACC</ta>
            <ta e="T305" id="Seg_1439" s="T304">man.muss</ta>
            <ta e="T317" id="Seg_1440" s="T314">vorsichtig.sein-CVB.SIM-vorsichtig.sein-CVB.SIM</ta>
            <ta e="T320" id="Seg_1441" s="T317">empfangen-PST2-EP-1SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr-ChGS">
            <ta e="T66" id="Seg_1442" s="T65">кто-ACC</ta>
            <ta e="T67" id="Seg_1443" s="T66">взять-PRS-1PL</ta>
            <ta e="T68" id="Seg_1444" s="T67">кто-ACC</ta>
            <ta e="T69" id="Seg_1445" s="T68">взять-PRS-2SG</ta>
            <ta e="T101" id="Seg_1446" s="T100">что.[NOM]=Q</ta>
            <ta e="T102" id="Seg_1447" s="T101">кто-ACC</ta>
            <ta e="T103" id="Seg_1448" s="T102">взять-FUT-1PL=Q</ta>
            <ta e="T104" id="Seg_1449" s="T103">кто-ACC</ta>
            <ta e="T105" id="Seg_1450" s="T104">рассказывать-PRS-1PL</ta>
            <ta e="T106" id="Seg_1451" s="T105">плохой.[NOM]</ta>
            <ta e="T107" id="Seg_1452" s="T106">человек-ACC</ta>
            <ta e="T108" id="Seg_1453" s="T107">взять-FUT-1PL</ta>
            <ta e="T110" id="Seg_1454" s="T109">1SG.[NOM]</ta>
            <ta e="T111" id="Seg_1455" s="T110">вот</ta>
            <ta e="T112" id="Seg_1456" s="T111">когда</ta>
            <ta e="T113" id="Seg_1457" s="T112">NEG</ta>
            <ta e="T114" id="Seg_1458" s="T113">такой</ta>
            <ta e="T115" id="Seg_1459" s="T114">ситуация-DAT/LOC</ta>
            <ta e="T116" id="Seg_1460" s="T115">попадать-CVB.SIM</ta>
            <ta e="T117" id="Seg_1461" s="T116">еще.не-1SG</ta>
            <ta e="T122" id="Seg_1462" s="T120">жена-ACC</ta>
            <ta e="T124" id="Seg_1463" s="T123">1SG.[NOM]</ta>
            <ta e="T125" id="Seg_1464" s="T124">EMPH</ta>
            <ta e="T126" id="Seg_1465" s="T125">жена.[NOM]</ta>
            <ta e="T127" id="Seg_1466" s="T126">кто-3SG-DAT/LOC</ta>
            <ta e="T128" id="Seg_1467" s="T127">опять</ta>
            <ta e="T129" id="Seg_1468" s="T128">NEG.EX</ta>
            <ta e="T130" id="Seg_1469" s="T129">быть-PST1-1SG</ta>
            <ta e="T132" id="Seg_1470" s="T131">2SG.[NOM]</ta>
            <ta e="T134" id="Seg_1471" s="T132">рассказывать.[IMP.2SG]</ta>
            <ta e="T139" id="Seg_1472" s="T138">1SG.[NOM]</ta>
            <ta e="T140" id="Seg_1473" s="T139">помогать-FUT-1SG</ta>
            <ta e="T141" id="Seg_1474" s="T140">2SG-DAT/LOC</ta>
            <ta e="T158" id="Seg_1475" s="T157">2SG.[NOM]</ta>
            <ta e="T159" id="Seg_1476" s="T158">тот.[NOM]</ta>
            <ta e="T160" id="Seg_1477" s="T159">жена-2SG</ta>
            <ta e="T243" id="Seg_1478" s="T241">тюрьма-DAT/LOC</ta>
            <ta e="T245" id="Seg_1479" s="T243">сидеть-CVB.SEQ</ta>
            <ta e="T247" id="Seg_1480" s="T245">тюрьма-DAT/LOC</ta>
            <ta e="T251" id="Seg_1481" s="T249">ум-VBZ-PST2.[3SG]</ta>
            <ta e="T261" id="Seg_1482" s="T258">эй</ta>
            <ta e="T265" id="Seg_1483" s="T262">неправильный</ta>
            <ta e="T269" id="Seg_1484" s="T267">1SG.[NOM]</ta>
            <ta e="T271" id="Seg_1485" s="T269">неправильный.[NOM]</ta>
            <ta e="T272" id="Seg_1486" s="T271">1SG.[NOM]</ta>
            <ta e="T273" id="Seg_1487" s="T272">1SG.[NOM]</ta>
            <ta e="T274" id="Seg_1488" s="T273">неправильный.[NOM]</ta>
            <ta e="T292" id="Seg_1489" s="T291">2SG.[NOM]</ta>
            <ta e="T293" id="Seg_1490" s="T292">2SG.[NOM]</ta>
            <ta e="T295" id="Seg_1491" s="T293">как</ta>
            <ta e="T296" id="Seg_1492" s="T295">кто-VBZ-PRS-2SG</ta>
            <ta e="T298" id="Seg_1493" s="T297">радоваться-PRS-2SG</ta>
            <ta e="T300" id="Seg_1494" s="T298">радоваться-NEG-2SG</ta>
            <ta e="T301" id="Seg_1495" s="T300">2SG.[NOM]</ta>
            <ta e="T302" id="Seg_1496" s="T301">сам-2SG.[NOM]</ta>
            <ta e="T303" id="Seg_1497" s="T302">сторона-2SG-INSTR</ta>
            <ta e="T304" id="Seg_1498" s="T303">рассказывать-PTCP.FUT-2SG-ACC</ta>
            <ta e="T305" id="Seg_1499" s="T304">надо</ta>
            <ta e="T317" id="Seg_1500" s="T314">беречься-CVB.SIM-беречься-CVB.SIM</ta>
            <ta e="T320" id="Seg_1501" s="T317">принимать-PST2-EP-1SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc-ChGS">
            <ta e="T66" id="Seg_1502" s="T65">que-pro:case</ta>
            <ta e="T67" id="Seg_1503" s="T66">v-v:tense-v:pred.pn</ta>
            <ta e="T68" id="Seg_1504" s="T67">que-pro:case</ta>
            <ta e="T69" id="Seg_1505" s="T68">v-v:tense-v:pred.pn</ta>
            <ta e="T101" id="Seg_1506" s="T100">que.[pro:case]=ptcl</ta>
            <ta e="T102" id="Seg_1507" s="T101">que-pro:case</ta>
            <ta e="T103" id="Seg_1508" s="T102">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T104" id="Seg_1509" s="T103">que-pro:case</ta>
            <ta e="T105" id="Seg_1510" s="T104">v-v:tense-v:pred.pn</ta>
            <ta e="T106" id="Seg_1511" s="T105">adj.[n:case]</ta>
            <ta e="T107" id="Seg_1512" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_1513" s="T107">v-v:tense-v:poss.pn</ta>
            <ta e="T110" id="Seg_1514" s="T109">pers.[pro:case]</ta>
            <ta e="T111" id="Seg_1515" s="T110">ptcl</ta>
            <ta e="T112" id="Seg_1516" s="T111">que</ta>
            <ta e="T113" id="Seg_1517" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_1518" s="T113">dempro</ta>
            <ta e="T115" id="Seg_1519" s="T114">n-n:case</ta>
            <ta e="T116" id="Seg_1520" s="T115">v-v:cvb</ta>
            <ta e="T117" id="Seg_1521" s="T116">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T122" id="Seg_1522" s="T120">n-n:case</ta>
            <ta e="T124" id="Seg_1523" s="T123">pers.[pro:case]</ta>
            <ta e="T125" id="Seg_1524" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_1525" s="T125">n.[n:case]</ta>
            <ta e="T127" id="Seg_1526" s="T126">que-pro:(poss)-pro:case</ta>
            <ta e="T128" id="Seg_1527" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_1528" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_1529" s="T129">v-v:tense-v:poss.pn</ta>
            <ta e="T132" id="Seg_1530" s="T131">pers.[pro:case]</ta>
            <ta e="T134" id="Seg_1531" s="T132">v.[v:mood.pn]</ta>
            <ta e="T139" id="Seg_1532" s="T138">pers.[pro:case]</ta>
            <ta e="T140" id="Seg_1533" s="T139">v-v:tense-v:poss.pn</ta>
            <ta e="T141" id="Seg_1534" s="T140">pers-pro:case</ta>
            <ta e="T158" id="Seg_1535" s="T157">pers.[pro:case]</ta>
            <ta e="T159" id="Seg_1536" s="T158">dempro.[pro:case]</ta>
            <ta e="T160" id="Seg_1537" s="T159">n-n:(pred.pn)</ta>
            <ta e="T243" id="Seg_1538" s="T241">n-n:case</ta>
            <ta e="T245" id="Seg_1539" s="T243">v-v:cvb</ta>
            <ta e="T247" id="Seg_1540" s="T245">n-n:case</ta>
            <ta e="T251" id="Seg_1541" s="T249">n-n&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T261" id="Seg_1542" s="T258">interj</ta>
            <ta e="T265" id="Seg_1543" s="T262">adj</ta>
            <ta e="T269" id="Seg_1544" s="T267">pers.[pro:case]</ta>
            <ta e="T271" id="Seg_1545" s="T269">adj.[n:case]</ta>
            <ta e="T272" id="Seg_1546" s="T271">pers.[pro:case]</ta>
            <ta e="T273" id="Seg_1547" s="T272">pers.[pro:case]</ta>
            <ta e="T274" id="Seg_1548" s="T273">adj.[n:case]</ta>
            <ta e="T292" id="Seg_1549" s="T291">pers.[pro:case]</ta>
            <ta e="T293" id="Seg_1550" s="T292">pers.[pro:case]</ta>
            <ta e="T295" id="Seg_1551" s="T293">que</ta>
            <ta e="T296" id="Seg_1552" s="T295">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T298" id="Seg_1553" s="T297">v-v:tense-v:pred.pn</ta>
            <ta e="T300" id="Seg_1554" s="T298">v-v:(neg)-v:pred.pn</ta>
            <ta e="T301" id="Seg_1555" s="T300">pers.[pro:case]</ta>
            <ta e="T302" id="Seg_1556" s="T301">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T303" id="Seg_1557" s="T302">n-n:poss-n:case</ta>
            <ta e="T304" id="Seg_1558" s="T303">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T305" id="Seg_1559" s="T304">ptcl</ta>
            <ta e="T317" id="Seg_1560" s="T314">v-v:cvb-v-v:cvb</ta>
            <ta e="T320" id="Seg_1561" s="T317">v-v:tense-v:(ins)-v:poss.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps-ChGS">
            <ta e="T66" id="Seg_1562" s="T65">que</ta>
            <ta e="T67" id="Seg_1563" s="T66">v</ta>
            <ta e="T68" id="Seg_1564" s="T67">que</ta>
            <ta e="T69" id="Seg_1565" s="T68">v</ta>
            <ta e="T101" id="Seg_1566" s="T100">que</ta>
            <ta e="T102" id="Seg_1567" s="T101">que</ta>
            <ta e="T103" id="Seg_1568" s="T102">v</ta>
            <ta e="T104" id="Seg_1569" s="T103">que</ta>
            <ta e="T105" id="Seg_1570" s="T104">v</ta>
            <ta e="T106" id="Seg_1571" s="T105">adj</ta>
            <ta e="T107" id="Seg_1572" s="T106">n</ta>
            <ta e="T108" id="Seg_1573" s="T107">v</ta>
            <ta e="T110" id="Seg_1574" s="T109">pers</ta>
            <ta e="T111" id="Seg_1575" s="T110">ptcl</ta>
            <ta e="T112" id="Seg_1576" s="T111">que</ta>
            <ta e="T113" id="Seg_1577" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_1578" s="T113">dempro</ta>
            <ta e="T115" id="Seg_1579" s="T114">n</ta>
            <ta e="T116" id="Seg_1580" s="T115">v</ta>
            <ta e="T117" id="Seg_1581" s="T116">ptcl</ta>
            <ta e="T122" id="Seg_1582" s="T120">n</ta>
            <ta e="T124" id="Seg_1583" s="T123">pers</ta>
            <ta e="T125" id="Seg_1584" s="T124">ptcl</ta>
            <ta e="T126" id="Seg_1585" s="T125">n</ta>
            <ta e="T127" id="Seg_1586" s="T126">que</ta>
            <ta e="T128" id="Seg_1587" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_1588" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_1589" s="T129">cop</ta>
            <ta e="T132" id="Seg_1590" s="T131">pers</ta>
            <ta e="T134" id="Seg_1591" s="T132">v</ta>
            <ta e="T139" id="Seg_1592" s="T138">pers</ta>
            <ta e="T140" id="Seg_1593" s="T139">v</ta>
            <ta e="T141" id="Seg_1594" s="T140">pers</ta>
            <ta e="T158" id="Seg_1595" s="T157">pers</ta>
            <ta e="T159" id="Seg_1596" s="T158">dempro</ta>
            <ta e="T160" id="Seg_1597" s="T159">n</ta>
            <ta e="T243" id="Seg_1598" s="T241">n</ta>
            <ta e="T245" id="Seg_1599" s="T243">v</ta>
            <ta e="T247" id="Seg_1600" s="T245">n</ta>
            <ta e="T251" id="Seg_1601" s="T249">v</ta>
            <ta e="T261" id="Seg_1602" s="T258">interj</ta>
            <ta e="T265" id="Seg_1603" s="T262">adj</ta>
            <ta e="T269" id="Seg_1604" s="T267">pers</ta>
            <ta e="T271" id="Seg_1605" s="T269">adj</ta>
            <ta e="T272" id="Seg_1606" s="T271">pers</ta>
            <ta e="T273" id="Seg_1607" s="T272">pers</ta>
            <ta e="T274" id="Seg_1608" s="T273">adj</ta>
            <ta e="T292" id="Seg_1609" s="T291">pers</ta>
            <ta e="T293" id="Seg_1610" s="T292">pers</ta>
            <ta e="T295" id="Seg_1611" s="T293">que</ta>
            <ta e="T296" id="Seg_1612" s="T295">v</ta>
            <ta e="T298" id="Seg_1613" s="T297">v</ta>
            <ta e="T300" id="Seg_1614" s="T298">v</ta>
            <ta e="T301" id="Seg_1615" s="T300">pers</ta>
            <ta e="T302" id="Seg_1616" s="T301">emphpro</ta>
            <ta e="T303" id="Seg_1617" s="T302">n</ta>
            <ta e="T304" id="Seg_1618" s="T303">v</ta>
            <ta e="T305" id="Seg_1619" s="T304">ptcl</ta>
            <ta e="T317" id="Seg_1620" s="T314">v</ta>
            <ta e="T320" id="Seg_1621" s="T317">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-ChGS" />
         <annotation name="SyF" tierref="SyF-ChGS" />
         <annotation name="IST" tierref="IST-ChGS" />
         <annotation name="Top" tierref="Top-ChGS" />
         <annotation name="Foc" tierref="Foc-ChGS" />
         <annotation name="BOR" tierref="BOR-ChGS">
            <ta e="T115" id="Seg_1622" s="T114">RUS:cult</ta>
            <ta e="T125" id="Seg_1623" s="T124">RUS:mod</ta>
            <ta e="T305" id="Seg_1624" s="T304">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-ChGS" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-ChGS">
            <ta e="T115" id="Seg_1625" s="T114">dir:infl</ta>
            <ta e="T125" id="Seg_1626" s="T124">dir:bare</ta>
            <ta e="T305" id="Seg_1627" s="T304">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS-ChGS">
            <ta e="T47" id="Seg_1628" s="T41">RUS:ext</ta>
            <ta e="T53" id="Seg_1629" s="T50">RUS:ext</ta>
            <ta e="T61" id="Seg_1630" s="T54">RUS:ext</ta>
            <ta e="T69" id="Seg_1631" s="T65">RUS:ext</ta>
            <ta e="T84" id="Seg_1632" s="T77">RUS:ext</ta>
            <ta e="T157" id="Seg_1633" s="T155">RUS:int.alt</ta>
            <ta e="T169" id="Seg_1634" s="T165">RUS:ext</ta>
            <ta e="T172" id="Seg_1635" s="T170">RUS:ext</ta>
            <ta e="T276" id="Seg_1636" s="T274">RUS:int.ins</ta>
            <ta e="T281" id="Seg_1637" s="T278">RUS:ext</ta>
            <ta e="T358" id="Seg_1638" s="T351">RUS:ext</ta>
         </annotation>
         <annotation name="fe" tierref="fe-ChGS">
            <ta e="T47" id="Seg_1639" s="T41">– We do choose one of the protagonists, right?</ta>
            <ta e="T53" id="Seg_1640" s="T50">– Any?</ta>
            <ta e="T61" id="Seg_1641" s="T54">– Either the woman, or the grandpa is possible.</ta>
            <ta e="T69" id="Seg_1642" s="T65">– Whom do we choose, whom do you choose?</ta>
            <ta e="T84" id="Seg_1643" s="T77">– Ah, only one, we can together, right.</ta>
            <ta e="T108" id="Seg_1644" s="T100">– Whom do we choose, about whom do we tell, do we choose a bad person?</ta>
            <ta e="T117" id="Seg_1645" s="T109">I was never in such a situation.</ta>
            <ta e="T122" id="Seg_1646" s="T120">– The woman.</ta>
            <ta e="T130" id="Seg_1647" s="T123">I was neither in that of the woman.</ta>
            <ta e="T134" id="Seg_1648" s="T131">You tell.</ta>
            <ta e="T141" id="Seg_1649" s="T138">– I'll help you.</ta>
            <ta e="T160" id="Seg_1650" s="T155">– As if you were that woman.</ta>
            <ta e="T169" id="Seg_1651" s="T165">– As if…</ta>
            <ta e="T172" id="Seg_1652" s="T170">Have to think.</ta>
            <ta e="T247" id="Seg_1653" s="T241">Sitting in prison, in prison.</ta>
            <ta e="T251" id="Seg_1654" s="T249">– He recollected himself.</ta>
            <ta e="T261" id="Seg_1655" s="T258">– Oh.</ta>
            <ta e="T265" id="Seg_1656" s="T262">– Wrong.</ta>
            <ta e="T276" id="Seg_1657" s="T267">– I am wrong, I am wrong, I.</ta>
            <ta e="T281" id="Seg_1658" s="T278">– Not that.</ta>
            <ta e="T296" id="Seg_1659" s="T291">– You, how you are doing?</ta>
            <ta e="T305" id="Seg_1660" s="T297">You're happy or not, you have to tell about yourself.</ta>
            <ta e="T320" id="Seg_1661" s="T314">– I met him very cautiously. </ta>
            <ta e="T358" id="Seg_1662" s="T351">– Well done, at all, well.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-ChGS">
            <ta e="T47" id="Seg_1663" s="T41">– Wir wählen einen der Protagonisten aus, ja?</ta>
            <ta e="T53" id="Seg_1664" s="T50">– Einen beliebigen?</ta>
            <ta e="T61" id="Seg_1665" s="T54">– Entweder die Frau, oder man kann den Opa.</ta>
            <ta e="T69" id="Seg_1666" s="T65">– Wen nehmen wir, wen nimmst du?</ta>
            <ta e="T84" id="Seg_1667" s="T77">– Ah, nur einen, zu zweit können wir, ja.</ta>
            <ta e="T108" id="Seg_1668" s="T100">– Wen nehmen wir, über wen erzählen wir, nehmen wir einen schlechten Menschen?</ta>
            <ta e="T117" id="Seg_1669" s="T109">Ich war noch nie in so einer Situation.</ta>
            <ta e="T122" id="Seg_1670" s="T120">– Die Frau.</ta>
            <ta e="T130" id="Seg_1671" s="T123">Ich war auch noch nie in der von der Frau.</ta>
            <ta e="T134" id="Seg_1672" s="T131">Erzähl du.</ta>
            <ta e="T141" id="Seg_1673" s="T138">– Ich helfe dir.</ta>
            <ta e="T160" id="Seg_1674" s="T155">– Als ob du die Frau bist.</ta>
            <ta e="T169" id="Seg_1675" s="T165">– Als ob…</ta>
            <ta e="T172" id="Seg_1676" s="T170">Muss man nachdenken.</ta>
            <ta e="T247" id="Seg_1677" s="T241">Im Gefängnis sitzend, im Gefängnis.</ta>
            <ta e="T251" id="Seg_1678" s="T249">– Er besann sich.</ta>
            <ta e="T261" id="Seg_1679" s="T258">– Ach.</ta>
            <ta e="T265" id="Seg_1680" s="T262">– Falsch.</ta>
            <ta e="T276" id="Seg_1681" s="T267">– Ich bin falsch, ich bin falsch, ich.</ta>
            <ta e="T281" id="Seg_1682" s="T278">– Nicht das.</ta>
            <ta e="T296" id="Seg_1683" s="T291">– Du, wie machst du?</ta>
            <ta e="T305" id="Seg_1684" s="T297">Du freust dich oder nicht, du musst über dich selbst erzählen.</ta>
            <ta e="T320" id="Seg_1685" s="T314">– Ich habe ihn vorsichtig empfangen.</ta>
            <ta e="T358" id="Seg_1686" s="T351">– Super insgesamt, gut.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-ChGS" />
         <annotation name="ltr" tierref="ltr-ChGS">
            <ta e="T69" id="Seg_1687" s="T65">Кого возмем, кого возмешь?</ta>
            <ta e="T108" id="Seg_1688" s="T100">Что, кого возмем, про кого расскажем, плохого человека возмем?</ta>
            <ta e="T117" id="Seg_1689" s="T109">Я никодга в такую ситуацию не попадала.</ta>
            <ta e="T122" id="Seg_1690" s="T120">Женщину.</ta>
            <ta e="T130" id="Seg_1691" s="T123">Я же в женской тоже не была.</ta>
            <ta e="T134" id="Seg_1692" s="T131">Расскажи ты.</ta>
            <ta e="T141" id="Seg_1693" s="T138">Я тебе помогу.</ta>
            <ta e="T160" id="Seg_1694" s="T155">Как будто эта женщина ты.</ta>
            <ta e="T247" id="Seg_1695" s="T241">В тюрьме сидя, в тюрьме.</ta>
            <ta e="T251" id="Seg_1696" s="T249">Поумнил.</ta>
            <ta e="T276" id="Seg_1697" s="T267">Я не правильно, я я неправильно.</ta>
            <ta e="T296" id="Seg_1698" s="T291">Ты ты как, делаешь.</ta>
            <ta e="T305" id="Seg_1699" s="T297">Рада не рада ты должна про себя рассказать.</ta>
            <ta e="T320" id="Seg_1700" s="T314">Опасаясь-опасаясь встретила.</ta>
            <ta e="T358" id="Seg_1701" s="T351">Молодец вообще-то, хорошо же.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-ChGS" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-UoPP"
                      id="tx-UoPP"
                      speaker="UoPP"
                      type="t">
         <timeline-fork end="T352" start="T350">
            <tli id="T350.tx-UoPP.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-UoPP">
            <ts e="T65" id="Seg_1702" n="sc" s="T60">
               <ts e="T65" id="Seg_1704" n="HIAT:u" s="T60">
                  <nts id="Seg_1705" n="HIAT:ip">–</nts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_1708" n="HIAT:w" s="T60">Lʼexčʼe</ts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_1711" n="HIAT:w" s="T62">rʼebʼonka</ts>
                  <nts id="Seg_1712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_1714" n="HIAT:w" s="T63">vɨbratʼ</ts>
                  <nts id="Seg_1715" n="HIAT:ip">.</nts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T75" id="Seg_1717" n="sc" s="T70">
               <ts e="T75" id="Seg_1719" n="HIAT:u" s="T70">
                  <nts id="Seg_1720" n="HIAT:ip">–</nts>
                  <nts id="Seg_1721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_1723" n="HIAT:w" s="T70">Adnavo</ts>
                  <nts id="Seg_1724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_1726" n="HIAT:w" s="T71">tolʼka</ts>
                  <nts id="Seg_1727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_1729" n="HIAT:w" s="T72">naːda</ts>
                  <nts id="Seg_1730" n="HIAT:ip">,</nts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_1733" n="HIAT:w" s="T73">da</ts>
                  <nts id="Seg_1734" n="HIAT:ip">?</nts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T88" id="Seg_1736" n="sc" s="T84">
               <ts e="T88" id="Seg_1738" n="HIAT:u" s="T84">
                  <nts id="Seg_1739" n="HIAT:ip">–</nts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_1742" n="HIAT:w" s="T84">Ja</ts>
                  <nts id="Seg_1743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_1745" n="HIAT:w" s="T86">pamagatʼ</ts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_1748" n="HIAT:w" s="T87">budu</ts>
                  <nts id="Seg_1749" n="HIAT:ip">.</nts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T121" id="Seg_1751" n="sc" s="T117">
               <ts e="T121" id="Seg_1753" n="HIAT:u" s="T117">
                  <nts id="Seg_1754" n="HIAT:ip">–</nts>
                  <nts id="Seg_1755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_1757" n="HIAT:w" s="T117">Dʼaktarɨ</ts>
                  <nts id="Seg_1758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_1760" n="HIAT:w" s="T118">ɨl</ts>
                  <nts id="Seg_1761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_1763" n="HIAT:w" s="T119">onto</ts>
                  <nts id="Seg_1764" n="HIAT:ip">.</nts>
                  <nts id="Seg_1765" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T137" id="Seg_1766" n="sc" s="T133">
               <ts e="T137" id="Seg_1768" n="HIAT:u" s="T133">
                  <nts id="Seg_1769" n="HIAT:ip">–</nts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_1772" n="HIAT:w" s="T133">Min</ts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1774" n="HIAT:ip">(</nts>
                  <ts e="T136" id="Seg_1776" n="HIAT:w" s="T135">kepseː</ts>
                  <nts id="Seg_1777" n="HIAT:ip">)</nts>
                  <nts id="Seg_1778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_1780" n="HIAT:w" s="T136">du͡o</ts>
                  <nts id="Seg_1781" n="HIAT:ip">.</nts>
                  <nts id="Seg_1782" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T147" id="Seg_1783" n="sc" s="T142">
               <ts e="T147" id="Seg_1785" n="HIAT:u" s="T142">
                  <nts id="Seg_1786" n="HIAT:ip">–</nts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_1789" n="HIAT:w" s="T142">Pra</ts>
                  <nts id="Seg_1790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_1792" n="HIAT:w" s="T143">ženšʼinu</ts>
                  <nts id="Seg_1793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_1795" n="HIAT:w" s="T144">možna</ts>
                  <nts id="Seg_1796" n="HIAT:ip">,</nts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_1799" n="HIAT:w" s="T145">da</ts>
                  <nts id="Seg_1800" n="HIAT:ip">?</nts>
                  <nts id="Seg_1801" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T154" id="Seg_1802" n="sc" s="T148">
               <ts e="T154" id="Seg_1804" n="HIAT:u" s="T148">
                  <nts id="Seg_1805" n="HIAT:ip">–</nts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_1808" n="HIAT:w" s="T148">Da</ts>
                  <nts id="Seg_1809" n="HIAT:ip">,</nts>
                  <nts id="Seg_1810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_1812" n="HIAT:w" s="T151">davajtʼe</ts>
                  <nts id="Seg_1813" n="HIAT:ip">.</nts>
                  <nts id="Seg_1814" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T169" id="Seg_1815" n="sc" s="T161">
               <ts e="T165" id="Seg_1817" n="HIAT:u" s="T161">
                  <nts id="Seg_1818" n="HIAT:ip">–</nts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_1821" n="HIAT:w" s="T161">Eta</ts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_1824" n="HIAT:w" s="T162">ja</ts>
                  <nts id="Seg_1825" n="HIAT:ip">,</nts>
                  <nts id="Seg_1826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_1828" n="HIAT:w" s="T163">da</ts>
                  <nts id="Seg_1829" n="HIAT:ip">,</nts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_1832" n="HIAT:w" s="T164">aː</ts>
                  <nts id="Seg_1833" n="HIAT:ip">.</nts>
                  <nts id="Seg_1834" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_1836" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_1838" n="HIAT:w" s="T165">Oj</ts>
                  <nts id="Seg_1839" n="HIAT:ip">,</nts>
                  <nts id="Seg_1840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1841" n="HIAT:ip">(</nts>
                  <ts e="T168" id="Seg_1843" n="HIAT:w" s="T166">pada-</ts>
                  <nts id="Seg_1844" n="HIAT:ip">)</nts>
                  <nts id="Seg_1845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1846" n="HIAT:ip">(</nts>
                  <nts id="Seg_1847" n="HIAT:ip">(</nts>
                  <ats e="T169" id="Seg_1848" n="HIAT:non-pho" s="T168">LAUGH</ats>
                  <nts id="Seg_1849" n="HIAT:ip">)</nts>
                  <nts id="Seg_1850" n="HIAT:ip">)</nts>
                  <nts id="Seg_1851" n="HIAT:ip">.</nts>
                  <nts id="Seg_1852" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T184" id="Seg_1853" n="sc" s="T173">
               <ts e="T184" id="Seg_1855" n="HIAT:u" s="T173">
                  <nts id="Seg_1856" n="HIAT:ip">–</nts>
                  <nts id="Seg_1857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_1859" n="HIAT:w" s="T173">Erbin</ts>
                  <nts id="Seg_1860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_1862" n="HIAT:w" s="T174">kɨtta</ts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_1865" n="HIAT:w" s="T175">üčügej</ts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_1868" n="HIAT:w" s="T176">bagajtɨk</ts>
                  <nts id="Seg_1869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_1871" n="HIAT:w" s="T177">oloror</ts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_1874" n="HIAT:w" s="T178">etibit</ts>
                  <nts id="Seg_1875" n="HIAT:ip">,</nts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_1878" n="HIAT:w" s="T179">ogobut</ts>
                  <nts id="Seg_1879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_1881" n="HIAT:w" s="T180">ogo</ts>
                  <nts id="Seg_1882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_1884" n="HIAT:w" s="T181">u͡ol</ts>
                  <nts id="Seg_1885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_1887" n="HIAT:w" s="T182">ogoloːk</ts>
                  <nts id="Seg_1888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_1890" n="HIAT:w" s="T183">etibit</ts>
                  <nts id="Seg_1891" n="HIAT:ip">.</nts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T190" id="Seg_1893" n="sc" s="T185">
               <ts e="T190" id="Seg_1895" n="HIAT:u" s="T185">
                  <ts e="T186" id="Seg_1897" n="HIAT:w" s="T185">Onton</ts>
                  <nts id="Seg_1898" n="HIAT:ip">,</nts>
                  <nts id="Seg_1899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_1901" n="HIAT:w" s="T186">erim</ts>
                  <nts id="Seg_1902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_1904" n="HIAT:w" s="T187">aragiː</ts>
                  <nts id="Seg_1905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_1907" n="HIAT:w" s="T188">iher</ts>
                  <nts id="Seg_1908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_1910" n="HIAT:w" s="T189">bu͡olbuta</ts>
                  <nts id="Seg_1911" n="HIAT:ip">.</nts>
                  <nts id="Seg_1912" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T207" id="Seg_1913" n="sc" s="T191">
               <ts e="T201" id="Seg_1915" n="HIAT:u" s="T191">
                  <ts e="T192" id="Seg_1917" n="HIAT:w" s="T191">Dogottorun</ts>
                  <nts id="Seg_1918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_1920" n="HIAT:w" s="T192">kɨtta</ts>
                  <nts id="Seg_1921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_1923" n="HIAT:w" s="T193">aragiːmsak</ts>
                  <nts id="Seg_1924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_1926" n="HIAT:w" s="T194">bu͡olbut</ts>
                  <nts id="Seg_1927" n="HIAT:ip">,</nts>
                  <nts id="Seg_1928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_1930" n="HIAT:w" s="T195">onton</ts>
                  <nts id="Seg_1931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_1933" n="HIAT:w" s="T196">ontulara</ts>
                  <nts id="Seg_1934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_1936" n="HIAT:w" s="T197">bu͡olla</ts>
                  <nts id="Seg_1937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_1939" n="HIAT:w" s="T198">min</ts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_1942" n="HIAT:w" s="T199">tuspunan</ts>
                  <nts id="Seg_1943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_1945" n="HIAT:w" s="T200">kepseːbitter</ts>
                  <nts id="Seg_1946" n="HIAT:ip">:</nts>
                  <nts id="Seg_1947" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_1949" n="HIAT:u" s="T201">
                  <nts id="Seg_1950" n="HIAT:ip">"</nts>
                  <ts e="T202" id="Seg_1952" n="HIAT:w" s="T201">Ol</ts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_1955" n="HIAT:w" s="T202">kördük</ts>
                  <nts id="Seg_1956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_1958" n="HIAT:w" s="T203">hɨldʼar</ts>
                  <nts id="Seg_1959" n="HIAT:ip">,</nts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_1962" n="HIAT:w" s="T204">bu</ts>
                  <nts id="Seg_1963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_1965" n="HIAT:w" s="T205">kördük</ts>
                  <nts id="Seg_1966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_1968" n="HIAT:w" s="T206">hɨldʼar</ts>
                  <nts id="Seg_1969" n="HIAT:ip">.</nts>
                  <nts id="Seg_1970" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T215" id="Seg_1971" n="sc" s="T208">
               <ts e="T215" id="Seg_1973" n="HIAT:u" s="T208">
                  <ts e="T209" id="Seg_1975" n="HIAT:w" s="T208">Er</ts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_1978" n="HIAT:w" s="T209">kihiler</ts>
                  <nts id="Seg_1979" n="HIAT:ip">,</nts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_1982" n="HIAT:w" s="T210">atɨn</ts>
                  <nts id="Seg_1983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_1985" n="HIAT:w" s="T211">er</ts>
                  <nts id="Seg_1986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_1988" n="HIAT:w" s="T212">kihileri</ts>
                  <nts id="Seg_1989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_1991" n="HIAT:w" s="T213">gɨtta</ts>
                  <nts id="Seg_1992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_1994" n="HIAT:w" s="T214">hɨldʼar</ts>
                  <nts id="Seg_1995" n="HIAT:ip">.</nts>
                  <nts id="Seg_1996" n="HIAT:ip">"</nts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T223" id="Seg_1998" n="sc" s="T216">
               <ts e="T223" id="Seg_2000" n="HIAT:u" s="T216">
                  <ts e="T217" id="Seg_2002" n="HIAT:w" s="T216">Ontuŋ</ts>
                  <nts id="Seg_2003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_2005" n="HIAT:w" s="T217">bu͡o</ts>
                  <nts id="Seg_2006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_2008" n="HIAT:w" s="T218">künüːleːn</ts>
                  <nts id="Seg_2009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_2011" n="HIAT:w" s="T219">bu͡o</ts>
                  <nts id="Seg_2012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_2014" n="HIAT:w" s="T220">minigin</ts>
                  <nts id="Seg_2015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_2017" n="HIAT:w" s="T221">oksubuta</ts>
                  <nts id="Seg_2018" n="HIAT:ip">,</nts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_2021" n="HIAT:w" s="T222">kɨrbaːbɨta</ts>
                  <nts id="Seg_2022" n="HIAT:ip">.</nts>
                  <nts id="Seg_2023" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T225" id="Seg_2024" n="sc" s="T224">
               <ts e="T225" id="Seg_2026" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_2028" n="HIAT:w" s="T224">Kɨrbaːbɨta</ts>
                  <nts id="Seg_2029" n="HIAT:ip">.</nts>
                  <nts id="Seg_2030" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T229" id="Seg_2031" n="sc" s="T226">
               <ts e="T229" id="Seg_2033" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_2035" n="HIAT:w" s="T226">Mʼilʼisʼijeler</ts>
                  <nts id="Seg_2036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_2038" n="HIAT:w" s="T227">mʼilʼisʼijaga</ts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_2041" n="HIAT:w" s="T228">ɨlbɨttara</ts>
                  <nts id="Seg_2042" n="HIAT:ip">.</nts>
                  <nts id="Seg_2043" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T233" id="Seg_2044" n="sc" s="T230">
               <ts e="T233" id="Seg_2046" n="HIAT:u" s="T230">
                  <ts e="T231" id="Seg_2048" n="HIAT:w" s="T230">Onton</ts>
                  <nts id="Seg_2049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_2051" n="HIAT:w" s="T231">kaːjɨːga</ts>
                  <nts id="Seg_2052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_2054" n="HIAT:w" s="T232">olorputtar</ts>
                  <nts id="Seg_2055" n="HIAT:ip">.</nts>
                  <nts id="Seg_2056" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T235" id="Seg_2057" n="sc" s="T234">
               <ts e="T235" id="Seg_2059" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_2061" n="HIAT:w" s="T234">Olorputtara</ts>
                  <nts id="Seg_2062" n="HIAT:ip">.</nts>
                  <nts id="Seg_2063" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T250" id="Seg_2064" n="sc" s="T236">
               <ts e="T250" id="Seg_2066" n="HIAT:u" s="T236">
                  <ts e="T237" id="Seg_2068" n="HIAT:w" s="T236">Mm</ts>
                  <nts id="Seg_2069" n="HIAT:ip">,</nts>
                  <nts id="Seg_2070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_2072" n="HIAT:w" s="T237">kaːjɨː</ts>
                  <nts id="Seg_2073" n="HIAT:ip">,</nts>
                  <nts id="Seg_2074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_2076" n="HIAT:w" s="T238">ör</ts>
                  <nts id="Seg_2077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_2079" n="HIAT:w" s="T239">bagajɨ</ts>
                  <nts id="Seg_2080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_2082" n="HIAT:w" s="T240">ördük</ts>
                  <nts id="Seg_2083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_2085" n="HIAT:w" s="T242">oloron</ts>
                  <nts id="Seg_2086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_2088" n="HIAT:w" s="T244">baran</ts>
                  <nts id="Seg_2089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_2091" n="HIAT:w" s="T246">onton</ts>
                  <nts id="Seg_2092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_2094" n="HIAT:w" s="T248">taksan</ts>
                  <nts id="Seg_2095" n="HIAT:ip">…</nts>
                  <nts id="Seg_2096" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T255" id="Seg_2097" n="sc" s="T252">
               <ts e="T255" id="Seg_2099" n="HIAT:u" s="T252">
                  <nts id="Seg_2100" n="HIAT:ip">–</nts>
                  <nts id="Seg_2101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_2103" n="HIAT:w" s="T252">Taksan</ts>
                  <nts id="Seg_2104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_2106" n="HIAT:w" s="T253">bu͡ollar</ts>
                  <nts id="Seg_2107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_2109" n="HIAT:w" s="T254">du͡o</ts>
                  <nts id="Seg_2110" n="HIAT:ip">…</nts>
                  <nts id="Seg_2111" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T270" id="Seg_2112" n="sc" s="T256">
               <ts e="T264" id="Seg_2114" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_2116" n="HIAT:w" s="T256">Taksan</ts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_2119" n="HIAT:w" s="T257">dʼi͡etiger</ts>
                  <nts id="Seg_2120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_2122" n="HIAT:w" s="T259">kelbite</ts>
                  <nts id="Seg_2123" n="HIAT:ip">,</nts>
                  <nts id="Seg_2124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_2126" n="HIAT:w" s="T260">ü͡örer</ts>
                  <nts id="Seg_2127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_2129" n="HIAT:w" s="T263">bagajɨ</ts>
                  <nts id="Seg_2130" n="HIAT:ip">.</nts>
                  <nts id="Seg_2131" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T270" id="Seg_2133" n="HIAT:u" s="T264">
                  <nts id="Seg_2134" n="HIAT:ip">–</nts>
                  <nts id="Seg_2135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_2137" n="HIAT:w" s="T264">Ti͡ere</ts>
                  <nts id="Seg_2138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_2140" n="HIAT:w" s="T266">kepsiːbin</ts>
                  <nts id="Seg_2141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_2143" n="HIAT:w" s="T268">du͡o</ts>
                  <nts id="Seg_2144" n="HIAT:ip">?</nts>
                  <nts id="Seg_2145" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T279" id="Seg_2146" n="sc" s="T275">
               <ts e="T279" id="Seg_2148" n="HIAT:u" s="T275">
                  <nts id="Seg_2149" n="HIAT:ip">–</nts>
                  <nts id="Seg_2150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_2152" n="HIAT:w" s="T275">Da</ts>
                  <nts id="Seg_2153" n="HIAT:ip">,</nts>
                  <nts id="Seg_2154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_2156" n="HIAT:w" s="T277">etat</ts>
                  <nts id="Seg_2157" n="HIAT:ip">.</nts>
                  <nts id="Seg_2158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T284" id="Seg_2159" n="sc" s="T282">
               <ts e="T284" id="Seg_2161" n="HIAT:u" s="T282">
                  <nts id="Seg_2162" n="HIAT:ip">–</nts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_2165" n="HIAT:w" s="T282">Dʼi͡etiger</ts>
                  <nts id="Seg_2166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_2168" n="HIAT:w" s="T283">kelbite</ts>
                  <nts id="Seg_2169" n="HIAT:ip">.</nts>
                  <nts id="Seg_2170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T290" id="Seg_2171" n="sc" s="T285">
               <ts e="T290" id="Seg_2173" n="HIAT:u" s="T285">
                  <ts e="T286" id="Seg_2175" n="HIAT:w" s="T285">Üčügej</ts>
                  <nts id="Seg_2176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_2178" n="HIAT:w" s="T286">bagajɨ</ts>
                  <nts id="Seg_2179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_2181" n="HIAT:w" s="T287">hanaːlaːk</ts>
                  <nts id="Seg_2182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_2184" n="HIAT:w" s="T288">kelle</ts>
                  <nts id="Seg_2185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_2187" n="HIAT:w" s="T289">bɨhɨlaːk</ts>
                  <nts id="Seg_2188" n="HIAT:ip">.</nts>
                  <nts id="Seg_2189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T299" id="Seg_2190" n="sc" s="T294">
               <ts e="T299" id="Seg_2192" n="HIAT:u" s="T294">
                  <nts id="Seg_2193" n="HIAT:ip">–</nts>
                  <nts id="Seg_2194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_2196" n="HIAT:w" s="T294">Beseleː</ts>
                  <nts id="Seg_2197" n="HIAT:ip">.</nts>
                  <nts id="Seg_2198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T315" id="Seg_2199" n="sc" s="T306">
               <ts e="T315" id="Seg_2201" n="HIAT:u" s="T306">
                  <ts e="T307" id="Seg_2203" n="HIAT:w" s="T306">Min</ts>
                  <nts id="Seg_2204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_2206" n="HIAT:w" s="T307">inniki</ts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_2209" n="HIAT:w" s="T308">bu͡ollar</ts>
                  <nts id="Seg_2210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_2212" n="HIAT:w" s="T309">kimniːr</ts>
                  <nts id="Seg_2213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_2215" n="HIAT:w" s="T310">etim</ts>
                  <nts id="Seg_2216" n="HIAT:ip">,</nts>
                  <nts id="Seg_2217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_2219" n="HIAT:w" s="T311">kečeher</ts>
                  <nts id="Seg_2220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_2222" n="HIAT:w" s="T312">etim</ts>
                  <nts id="Seg_2223" n="HIAT:ip">,</nts>
                  <nts id="Seg_2224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_2226" n="HIAT:w" s="T313">onton</ts>
                  <nts id="Seg_2227" n="HIAT:ip">.</nts>
                  <nts id="Seg_2228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T326" id="Seg_2229" n="sc" s="T316">
               <ts e="T326" id="Seg_2231" n="HIAT:u" s="T316">
                  <ts e="T318" id="Seg_2233" n="HIAT:w" s="T316">Onton</ts>
                  <nts id="Seg_2234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_2236" n="HIAT:w" s="T318">eː</ts>
                  <nts id="Seg_2237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_2239" n="HIAT:w" s="T319">tohujbutum</ts>
                  <nts id="Seg_2240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_2242" n="HIAT:w" s="T321">ehebin</ts>
                  <nts id="Seg_2243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_2245" n="HIAT:w" s="T322">kɨtta</ts>
                  <nts id="Seg_2246" n="HIAT:ip">,</nts>
                  <nts id="Seg_2247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2248" n="HIAT:ip">(</nts>
                  <ts e="T324" id="Seg_2250" n="HIAT:w" s="T323">ogolor</ts>
                  <nts id="Seg_2251" n="HIAT:ip">)</nts>
                  <nts id="Seg_2252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_2254" n="HIAT:w" s="T324">ogobutun</ts>
                  <nts id="Seg_2255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_2257" n="HIAT:w" s="T325">kɨtta</ts>
                  <nts id="Seg_2258" n="HIAT:ip">.</nts>
                  <nts id="Seg_2259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T336" id="Seg_2260" n="sc" s="T327">
               <ts e="T336" id="Seg_2262" n="HIAT:u" s="T327">
                  <ts e="T328" id="Seg_2264" n="HIAT:w" s="T327">Onton</ts>
                  <nts id="Seg_2265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_2267" n="HIAT:w" s="T328">üčügej</ts>
                  <nts id="Seg_2268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_2270" n="HIAT:w" s="T329">bagajɨtɨk</ts>
                  <nts id="Seg_2271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_2273" n="HIAT:w" s="T330">olorbupput</ts>
                  <nts id="Seg_2274" n="HIAT:ip">,</nts>
                  <nts id="Seg_2275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_2277" n="HIAT:w" s="T331">erim</ts>
                  <nts id="Seg_2278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_2280" n="HIAT:w" s="T332">aragiːtɨn</ts>
                  <nts id="Seg_2281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_2283" n="HIAT:w" s="T333">bɨrakpɨta</ts>
                  <nts id="Seg_2284" n="HIAT:ip">,</nts>
                  <nts id="Seg_2285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_2287" n="HIAT:w" s="T334">ogotun</ts>
                  <nts id="Seg_2288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_2290" n="HIAT:w" s="T335">kɨtta</ts>
                  <nts id="Seg_2291" n="HIAT:ip">.</nts>
                  <nts id="Seg_2292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T347" id="Seg_2293" n="sc" s="T337">
               <ts e="T347" id="Seg_2295" n="HIAT:u" s="T337">
                  <ts e="T338" id="Seg_2297" n="HIAT:w" s="T337">Bi͡ek</ts>
                  <nts id="Seg_2298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_2300" n="HIAT:w" s="T338">hɨldʼar</ts>
                  <nts id="Seg_2301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_2303" n="HIAT:w" s="T339">ogotun</ts>
                  <nts id="Seg_2304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_2306" n="HIAT:w" s="T340">hɨrɨtɨnnaraːččɨ</ts>
                  <nts id="Seg_2307" n="HIAT:ip">,</nts>
                  <nts id="Seg_2308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_2310" n="HIAT:w" s="T341">dʼi͡ebitiger</ts>
                  <nts id="Seg_2311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_2313" n="HIAT:w" s="T342">kömölöhöːččü</ts>
                  <nts id="Seg_2314" n="HIAT:ip">,</nts>
                  <nts id="Seg_2315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_2317" n="HIAT:w" s="T343">üčügej</ts>
                  <nts id="Seg_2318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_2320" n="HIAT:w" s="T344">bagajɨdɨk</ts>
                  <nts id="Seg_2321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_2323" n="HIAT:w" s="T345">olorobut</ts>
                  <nts id="Seg_2324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_2326" n="HIAT:w" s="T346">anɨ</ts>
                  <nts id="Seg_2327" n="HIAT:ip">.</nts>
                  <nts id="Seg_2328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T354" id="Seg_2329" n="sc" s="T348">
               <ts e="T354" id="Seg_2331" n="HIAT:u" s="T348">
                  <ts e="T349" id="Seg_2333" n="HIAT:w" s="T348">Tak</ts>
                  <nts id="Seg_2334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_2336" n="HIAT:w" s="T349">navʼerna</ts>
                  <nts id="Seg_2337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350.tx-UoPP.1" id="Seg_2339" n="HIAT:w" s="T350">gdʼe</ts>
                  <nts id="Seg_2340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_2342" n="HIAT:w" s="T350.tx-UoPP.1">-</ts>
                  <nts id="Seg_2343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_2345" n="HIAT:w" s="T352">ta</ts>
                  <nts id="Seg_2346" n="HIAT:ip">…</nts>
                  <nts id="Seg_2347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T360" id="Seg_2348" n="sc" s="T359">
               <ts e="T360" id="Seg_2350" n="HIAT:u" s="T359">
                  <nts id="Seg_2351" n="HIAT:ip">–</nts>
                  <nts id="Seg_2352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_2354" n="HIAT:w" s="T359">Fsʼo</ts>
                  <nts id="Seg_2355" n="HIAT:ip">.</nts>
                  <nts id="Seg_2356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T363" id="Seg_2357" n="sc" s="T361">
               <ts e="T363" id="Seg_2359" n="HIAT:u" s="T361">
                  <nts id="Seg_2360" n="HIAT:ip">–</nts>
                  <nts id="Seg_2361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_2363" n="HIAT:w" s="T361">Aha</ts>
                  <nts id="Seg_2364" n="HIAT:ip">.</nts>
                  <nts id="Seg_2365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T371" id="Seg_2366" n="sc" s="T366">
               <ts e="T371" id="Seg_2368" n="HIAT:u" s="T366">
                  <nts id="Seg_2369" n="HIAT:ip">–</nts>
                  <nts id="Seg_2370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_2372" n="HIAT:w" s="T366">Elete</ts>
                  <nts id="Seg_2373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_2375" n="HIAT:w" s="T368">du͡o</ts>
                  <nts id="Seg_2376" n="HIAT:ip">.</nts>
                  <nts id="Seg_2377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-UoPP">
            <ts e="T65" id="Seg_2378" n="sc" s="T60">
               <ts e="T62" id="Seg_2380" n="e" s="T60">– Lʼexčʼe </ts>
               <ts e="T63" id="Seg_2382" n="e" s="T62">rʼebʼonka </ts>
               <ts e="T65" id="Seg_2384" n="e" s="T63">vɨbratʼ. </ts>
            </ts>
            <ts e="T75" id="Seg_2385" n="sc" s="T70">
               <ts e="T71" id="Seg_2387" n="e" s="T70">– Adnavo </ts>
               <ts e="T72" id="Seg_2389" n="e" s="T71">tolʼka </ts>
               <ts e="T73" id="Seg_2391" n="e" s="T72">naːda, </ts>
               <ts e="T75" id="Seg_2393" n="e" s="T73">da? </ts>
            </ts>
            <ts e="T88" id="Seg_2394" n="sc" s="T84">
               <ts e="T86" id="Seg_2396" n="e" s="T84">– Ja </ts>
               <ts e="T87" id="Seg_2398" n="e" s="T86">pamagatʼ </ts>
               <ts e="T88" id="Seg_2400" n="e" s="T87">budu. </ts>
            </ts>
            <ts e="T121" id="Seg_2401" n="sc" s="T117">
               <ts e="T118" id="Seg_2403" n="e" s="T117">– Dʼaktarɨ </ts>
               <ts e="T119" id="Seg_2405" n="e" s="T118">ɨl </ts>
               <ts e="T121" id="Seg_2407" n="e" s="T119">onto. </ts>
            </ts>
            <ts e="T137" id="Seg_2408" n="sc" s="T133">
               <ts e="T135" id="Seg_2410" n="e" s="T133">– Min </ts>
               <ts e="T136" id="Seg_2412" n="e" s="T135">(kepseː) </ts>
               <ts e="T137" id="Seg_2414" n="e" s="T136">du͡o. </ts>
            </ts>
            <ts e="T147" id="Seg_2415" n="sc" s="T142">
               <ts e="T143" id="Seg_2417" n="e" s="T142">– Pra </ts>
               <ts e="T144" id="Seg_2419" n="e" s="T143">ženšʼinu </ts>
               <ts e="T145" id="Seg_2421" n="e" s="T144">možna, </ts>
               <ts e="T147" id="Seg_2423" n="e" s="T145">da? </ts>
            </ts>
            <ts e="T154" id="Seg_2424" n="sc" s="T148">
               <ts e="T151" id="Seg_2426" n="e" s="T148">– Da, </ts>
               <ts e="T154" id="Seg_2428" n="e" s="T151">davajtʼe. </ts>
            </ts>
            <ts e="T169" id="Seg_2429" n="sc" s="T161">
               <ts e="T162" id="Seg_2431" n="e" s="T161">– Eta </ts>
               <ts e="T163" id="Seg_2433" n="e" s="T162">ja, </ts>
               <ts e="T164" id="Seg_2435" n="e" s="T163">da, </ts>
               <ts e="T165" id="Seg_2437" n="e" s="T164">aː. </ts>
               <ts e="T166" id="Seg_2439" n="e" s="T165">Oj, </ts>
               <ts e="T168" id="Seg_2441" n="e" s="T166">(pada-) </ts>
               <ts e="T169" id="Seg_2443" n="e" s="T168">((LAUGH)). </ts>
            </ts>
            <ts e="T184" id="Seg_2444" n="sc" s="T173">
               <ts e="T174" id="Seg_2446" n="e" s="T173">– Erbin </ts>
               <ts e="T175" id="Seg_2448" n="e" s="T174">kɨtta </ts>
               <ts e="T176" id="Seg_2450" n="e" s="T175">üčügej </ts>
               <ts e="T177" id="Seg_2452" n="e" s="T176">bagajtɨk </ts>
               <ts e="T178" id="Seg_2454" n="e" s="T177">oloror </ts>
               <ts e="T179" id="Seg_2456" n="e" s="T178">etibit, </ts>
               <ts e="T180" id="Seg_2458" n="e" s="T179">ogobut </ts>
               <ts e="T181" id="Seg_2460" n="e" s="T180">ogo </ts>
               <ts e="T182" id="Seg_2462" n="e" s="T181">u͡ol </ts>
               <ts e="T183" id="Seg_2464" n="e" s="T182">ogoloːk </ts>
               <ts e="T184" id="Seg_2466" n="e" s="T183">etibit. </ts>
            </ts>
            <ts e="T190" id="Seg_2467" n="sc" s="T185">
               <ts e="T186" id="Seg_2469" n="e" s="T185">Onton, </ts>
               <ts e="T187" id="Seg_2471" n="e" s="T186">erim </ts>
               <ts e="T188" id="Seg_2473" n="e" s="T187">aragiː </ts>
               <ts e="T189" id="Seg_2475" n="e" s="T188">iher </ts>
               <ts e="T190" id="Seg_2477" n="e" s="T189">bu͡olbuta. </ts>
            </ts>
            <ts e="T207" id="Seg_2478" n="sc" s="T191">
               <ts e="T192" id="Seg_2480" n="e" s="T191">Dogottorun </ts>
               <ts e="T193" id="Seg_2482" n="e" s="T192">kɨtta </ts>
               <ts e="T194" id="Seg_2484" n="e" s="T193">aragiːmsak </ts>
               <ts e="T195" id="Seg_2486" n="e" s="T194">bu͡olbut, </ts>
               <ts e="T196" id="Seg_2488" n="e" s="T195">onton </ts>
               <ts e="T197" id="Seg_2490" n="e" s="T196">ontulara </ts>
               <ts e="T198" id="Seg_2492" n="e" s="T197">bu͡olla </ts>
               <ts e="T199" id="Seg_2494" n="e" s="T198">min </ts>
               <ts e="T200" id="Seg_2496" n="e" s="T199">tuspunan </ts>
               <ts e="T201" id="Seg_2498" n="e" s="T200">kepseːbitter: </ts>
               <ts e="T202" id="Seg_2500" n="e" s="T201">"Ol </ts>
               <ts e="T203" id="Seg_2502" n="e" s="T202">kördük </ts>
               <ts e="T204" id="Seg_2504" n="e" s="T203">hɨldʼar, </ts>
               <ts e="T205" id="Seg_2506" n="e" s="T204">bu </ts>
               <ts e="T206" id="Seg_2508" n="e" s="T205">kördük </ts>
               <ts e="T207" id="Seg_2510" n="e" s="T206">hɨldʼar. </ts>
            </ts>
            <ts e="T215" id="Seg_2511" n="sc" s="T208">
               <ts e="T209" id="Seg_2513" n="e" s="T208">Er </ts>
               <ts e="T210" id="Seg_2515" n="e" s="T209">kihiler, </ts>
               <ts e="T211" id="Seg_2517" n="e" s="T210">atɨn </ts>
               <ts e="T212" id="Seg_2519" n="e" s="T211">er </ts>
               <ts e="T213" id="Seg_2521" n="e" s="T212">kihileri </ts>
               <ts e="T214" id="Seg_2523" n="e" s="T213">gɨtta </ts>
               <ts e="T215" id="Seg_2525" n="e" s="T214">hɨldʼar." </ts>
            </ts>
            <ts e="T223" id="Seg_2526" n="sc" s="T216">
               <ts e="T217" id="Seg_2528" n="e" s="T216">Ontuŋ </ts>
               <ts e="T218" id="Seg_2530" n="e" s="T217">bu͡o </ts>
               <ts e="T219" id="Seg_2532" n="e" s="T218">künüːleːn </ts>
               <ts e="T220" id="Seg_2534" n="e" s="T219">bu͡o </ts>
               <ts e="T221" id="Seg_2536" n="e" s="T220">minigin </ts>
               <ts e="T222" id="Seg_2538" n="e" s="T221">oksubuta, </ts>
               <ts e="T223" id="Seg_2540" n="e" s="T222">kɨrbaːbɨta. </ts>
            </ts>
            <ts e="T225" id="Seg_2541" n="sc" s="T224">
               <ts e="T225" id="Seg_2543" n="e" s="T224">Kɨrbaːbɨta. </ts>
            </ts>
            <ts e="T229" id="Seg_2544" n="sc" s="T226">
               <ts e="T227" id="Seg_2546" n="e" s="T226">Mʼilʼisʼijeler </ts>
               <ts e="T228" id="Seg_2548" n="e" s="T227">mʼilʼisʼijaga </ts>
               <ts e="T229" id="Seg_2550" n="e" s="T228">ɨlbɨttara. </ts>
            </ts>
            <ts e="T233" id="Seg_2551" n="sc" s="T230">
               <ts e="T231" id="Seg_2553" n="e" s="T230">Onton </ts>
               <ts e="T232" id="Seg_2555" n="e" s="T231">kaːjɨːga </ts>
               <ts e="T233" id="Seg_2557" n="e" s="T232">olorputtar. </ts>
            </ts>
            <ts e="T235" id="Seg_2558" n="sc" s="T234">
               <ts e="T235" id="Seg_2560" n="e" s="T234">Olorputtara. </ts>
            </ts>
            <ts e="T250" id="Seg_2561" n="sc" s="T236">
               <ts e="T237" id="Seg_2563" n="e" s="T236">Mm, </ts>
               <ts e="T238" id="Seg_2565" n="e" s="T237">kaːjɨː, </ts>
               <ts e="T239" id="Seg_2567" n="e" s="T238">ör </ts>
               <ts e="T240" id="Seg_2569" n="e" s="T239">bagajɨ </ts>
               <ts e="T242" id="Seg_2571" n="e" s="T240">ördük </ts>
               <ts e="T244" id="Seg_2573" n="e" s="T242">oloron </ts>
               <ts e="T246" id="Seg_2575" n="e" s="T244">baran </ts>
               <ts e="T248" id="Seg_2577" n="e" s="T246">onton </ts>
               <ts e="T250" id="Seg_2579" n="e" s="T248">taksan… </ts>
            </ts>
            <ts e="T255" id="Seg_2580" n="sc" s="T252">
               <ts e="T253" id="Seg_2582" n="e" s="T252">– Taksan </ts>
               <ts e="T254" id="Seg_2584" n="e" s="T253">bu͡ollar </ts>
               <ts e="T255" id="Seg_2586" n="e" s="T254">du͡o… </ts>
            </ts>
            <ts e="T270" id="Seg_2587" n="sc" s="T256">
               <ts e="T257" id="Seg_2589" n="e" s="T256">Taksan </ts>
               <ts e="T259" id="Seg_2591" n="e" s="T257">dʼi͡etiger </ts>
               <ts e="T260" id="Seg_2593" n="e" s="T259">kelbite, </ts>
               <ts e="T263" id="Seg_2595" n="e" s="T260">ü͡örer </ts>
               <ts e="T264" id="Seg_2597" n="e" s="T263">bagajɨ. </ts>
               <ts e="T266" id="Seg_2599" n="e" s="T264">– Ti͡ere </ts>
               <ts e="T268" id="Seg_2601" n="e" s="T266">kepsiːbin </ts>
               <ts e="T270" id="Seg_2603" n="e" s="T268">du͡o? </ts>
            </ts>
            <ts e="T279" id="Seg_2604" n="sc" s="T275">
               <ts e="T277" id="Seg_2606" n="e" s="T275">– Da, </ts>
               <ts e="T279" id="Seg_2608" n="e" s="T277">etat. </ts>
            </ts>
            <ts e="T284" id="Seg_2609" n="sc" s="T282">
               <ts e="T283" id="Seg_2611" n="e" s="T282">– Dʼi͡etiger </ts>
               <ts e="T284" id="Seg_2613" n="e" s="T283">kelbite. </ts>
            </ts>
            <ts e="T290" id="Seg_2614" n="sc" s="T285">
               <ts e="T286" id="Seg_2616" n="e" s="T285">Üčügej </ts>
               <ts e="T287" id="Seg_2618" n="e" s="T286">bagajɨ </ts>
               <ts e="T288" id="Seg_2620" n="e" s="T287">hanaːlaːk </ts>
               <ts e="T289" id="Seg_2622" n="e" s="T288">kelle </ts>
               <ts e="T290" id="Seg_2624" n="e" s="T289">bɨhɨlaːk. </ts>
            </ts>
            <ts e="T299" id="Seg_2625" n="sc" s="T294">
               <ts e="T299" id="Seg_2627" n="e" s="T294">– Beseleː. </ts>
            </ts>
            <ts e="T315" id="Seg_2628" n="sc" s="T306">
               <ts e="T307" id="Seg_2630" n="e" s="T306">Min </ts>
               <ts e="T308" id="Seg_2632" n="e" s="T307">inniki </ts>
               <ts e="T309" id="Seg_2634" n="e" s="T308">bu͡ollar </ts>
               <ts e="T310" id="Seg_2636" n="e" s="T309">kimniːr </ts>
               <ts e="T311" id="Seg_2638" n="e" s="T310">etim, </ts>
               <ts e="T312" id="Seg_2640" n="e" s="T311">kečeher </ts>
               <ts e="T313" id="Seg_2642" n="e" s="T312">etim, </ts>
               <ts e="T315" id="Seg_2644" n="e" s="T313">onton. </ts>
            </ts>
            <ts e="T326" id="Seg_2645" n="sc" s="T316">
               <ts e="T318" id="Seg_2647" n="e" s="T316">Onton </ts>
               <ts e="T319" id="Seg_2649" n="e" s="T318">eː </ts>
               <ts e="T321" id="Seg_2651" n="e" s="T319">tohujbutum </ts>
               <ts e="T322" id="Seg_2653" n="e" s="T321">ehebin </ts>
               <ts e="T323" id="Seg_2655" n="e" s="T322">kɨtta, </ts>
               <ts e="T324" id="Seg_2657" n="e" s="T323">(ogolor) </ts>
               <ts e="T325" id="Seg_2659" n="e" s="T324">ogobutun </ts>
               <ts e="T326" id="Seg_2661" n="e" s="T325">kɨtta. </ts>
            </ts>
            <ts e="T336" id="Seg_2662" n="sc" s="T327">
               <ts e="T328" id="Seg_2664" n="e" s="T327">Onton </ts>
               <ts e="T329" id="Seg_2666" n="e" s="T328">üčügej </ts>
               <ts e="T330" id="Seg_2668" n="e" s="T329">bagajɨtɨk </ts>
               <ts e="T331" id="Seg_2670" n="e" s="T330">olorbupput, </ts>
               <ts e="T332" id="Seg_2672" n="e" s="T331">erim </ts>
               <ts e="T333" id="Seg_2674" n="e" s="T332">aragiːtɨn </ts>
               <ts e="T334" id="Seg_2676" n="e" s="T333">bɨrakpɨta, </ts>
               <ts e="T335" id="Seg_2678" n="e" s="T334">ogotun </ts>
               <ts e="T336" id="Seg_2680" n="e" s="T335">kɨtta. </ts>
            </ts>
            <ts e="T347" id="Seg_2681" n="sc" s="T337">
               <ts e="T338" id="Seg_2683" n="e" s="T337">Bi͡ek </ts>
               <ts e="T339" id="Seg_2685" n="e" s="T338">hɨldʼar </ts>
               <ts e="T340" id="Seg_2687" n="e" s="T339">ogotun </ts>
               <ts e="T341" id="Seg_2689" n="e" s="T340">hɨrɨtɨnnaraːččɨ, </ts>
               <ts e="T342" id="Seg_2691" n="e" s="T341">dʼi͡ebitiger </ts>
               <ts e="T343" id="Seg_2693" n="e" s="T342">kömölöhöːččü, </ts>
               <ts e="T344" id="Seg_2695" n="e" s="T343">üčügej </ts>
               <ts e="T345" id="Seg_2697" n="e" s="T344">bagajɨdɨk </ts>
               <ts e="T346" id="Seg_2699" n="e" s="T345">olorobut </ts>
               <ts e="T347" id="Seg_2701" n="e" s="T346">anɨ. </ts>
            </ts>
            <ts e="T354" id="Seg_2702" n="sc" s="T348">
               <ts e="T349" id="Seg_2704" n="e" s="T348">Tak </ts>
               <ts e="T350" id="Seg_2706" n="e" s="T349">navʼerna </ts>
               <ts e="T352" id="Seg_2708" n="e" s="T350">gdʼe - </ts>
               <ts e="T354" id="Seg_2710" n="e" s="T352">ta… </ts>
            </ts>
            <ts e="T360" id="Seg_2711" n="sc" s="T359">
               <ts e="T360" id="Seg_2713" n="e" s="T359">– Fsʼo. </ts>
            </ts>
            <ts e="T363" id="Seg_2714" n="sc" s="T361">
               <ts e="T363" id="Seg_2716" n="e" s="T361">– Aha. </ts>
            </ts>
            <ts e="T371" id="Seg_2717" n="sc" s="T366">
               <ts e="T368" id="Seg_2719" n="e" s="T366">– Elete </ts>
               <ts e="T371" id="Seg_2721" n="e" s="T368">du͡o. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-UoPP">
            <ta e="T65" id="Seg_2722" s="T60">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.001 (001.009)</ta>
            <ta e="T75" id="Seg_2723" s="T70">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.002 (001.011)</ta>
            <ta e="T88" id="Seg_2724" s="T84">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.003 (001.014)</ta>
            <ta e="T121" id="Seg_2725" s="T117">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.004 (001.018)</ta>
            <ta e="T137" id="Seg_2726" s="T133">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.005 (001.022)</ta>
            <ta e="T147" id="Seg_2727" s="T142">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.006 (001.024)</ta>
            <ta e="T154" id="Seg_2728" s="T148">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.007 (001.026)</ta>
            <ta e="T165" id="Seg_2729" s="T161">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.008 (001.028)</ta>
            <ta e="T169" id="Seg_2730" s="T165">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.009 (001.029)</ta>
            <ta e="T184" id="Seg_2731" s="T173">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.010 (001.032)</ta>
            <ta e="T190" id="Seg_2732" s="T185">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.011 (001.033)</ta>
            <ta e="T201" id="Seg_2733" s="T191">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.012 (001.034)</ta>
            <ta e="T207" id="Seg_2734" s="T201">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.013 (001.034)</ta>
            <ta e="T215" id="Seg_2735" s="T208">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.014 (001.035)</ta>
            <ta e="T223" id="Seg_2736" s="T216">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.015 (001.036)</ta>
            <ta e="T225" id="Seg_2737" s="T224">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.016 (001.037)</ta>
            <ta e="T229" id="Seg_2738" s="T226">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.017 (001.038)</ta>
            <ta e="T233" id="Seg_2739" s="T230">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.018 (001.039)</ta>
            <ta e="T235" id="Seg_2740" s="T234">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.019 (001.040)</ta>
            <ta e="T250" id="Seg_2741" s="T236">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.020 (001.041)</ta>
            <ta e="T255" id="Seg_2742" s="T252">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.021 (001.044)</ta>
            <ta e="T264" id="Seg_2743" s="T256">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.022 (001.045)</ta>
            <ta e="T270" id="Seg_2744" s="T264">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.023 (001.048)</ta>
            <ta e="T279" id="Seg_2745" s="T275">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.024 (001.050)</ta>
            <ta e="T284" id="Seg_2746" s="T282">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.025 (001.052)</ta>
            <ta e="T290" id="Seg_2747" s="T285">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.026 (001.053)</ta>
            <ta e="T299" id="Seg_2748" s="T294">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.027 (001.055)</ta>
            <ta e="T315" id="Seg_2749" s="T306">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.028 (001.057)</ta>
            <ta e="T326" id="Seg_2750" s="T316">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.029 (001.059)</ta>
            <ta e="T336" id="Seg_2751" s="T327">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.030 (001.060)</ta>
            <ta e="T347" id="Seg_2752" s="T337">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.031 (001.061)</ta>
            <ta e="T354" id="Seg_2753" s="T348">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.032 (001.062)</ta>
            <ta e="T360" id="Seg_2754" s="T359">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.033 (001.064)</ta>
            <ta e="T363" id="Seg_2755" s="T361">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.034 (001.066)</ta>
            <ta e="T371" id="Seg_2756" s="T366">UoPP_ChGS_20170724_SocCogRetell2_nar.UoPP.035 (001.068)</ta>
         </annotation>
         <annotation name="st" tierref="st-UoPP">
            <ta e="T65" id="Seg_2757" s="T60">Легче ребенка выбрать.</ta>
            <ta e="T75" id="Seg_2758" s="T70">Одного только надо, да?</ta>
            <ta e="T88" id="Seg_2759" s="T84">Я помогать буду.</ta>
            <ta e="T121" id="Seg_2760" s="T117">Дьактары ыл онто.</ta>
            <ta e="T137" id="Seg_2761" s="T133">Мин (кэпсээ-) да.</ta>
            <ta e="T147" id="Seg_2762" s="T142">Про женщину можно да?</ta>
            <ta e="T154" id="Seg_2763" s="T148">Да, давайте.</ta>
            <ta e="T165" id="Seg_2764" s="T161">Это я… аа…</ta>
            <ta e="T169" id="Seg_2765" s="T165">Ой, (подо-).</ta>
            <ta e="T184" id="Seg_2766" s="T173">Эрбин кытта үчүгэй багайытык олорор этибит, огобут ого уол оголоок этибит.</ta>
            <ta e="T190" id="Seg_2767" s="T185">Онтон, эрим араги иһэр буолбута.</ta>
            <ta e="T201" id="Seg_2768" s="T191">Доготторун кытта арагимсак буолбут, онтон онтулара буолла мин туспунан кэпсээбиттэр </ta>
            <ta e="T207" id="Seg_2769" s="T201">"ол көрдүк һылдьар, бу көрдүк һылдьар".</ta>
            <ta e="T215" id="Seg_2770" s="T208">"Эр киһилер, атын эр киһилэри кытта һылдьар."</ta>
            <ta e="T223" id="Seg_2771" s="T216">Онтуӈ буо күнүлээн буо минигин оксубута кырбаабыта.</ta>
            <ta e="T225" id="Seg_2772" s="T224">Кырбаабыта.</ta>
            <ta e="T229" id="Seg_2773" s="T226">Милициялар милицияга илбыттара.</ta>
            <ta e="T233" id="Seg_2774" s="T230">Онтон кайыыга олорпуттар.</ta>
            <ta e="T235" id="Seg_2775" s="T234">Олорпуттар(а).</ta>
            <ta e="T250" id="Seg_2776" s="T236">Мм, кайыы өрдүк өр багайытык олорон баран онтон таксан…</ta>
            <ta e="T255" id="Seg_2777" s="T252">Таксан буоллар дуо…</ta>
            <ta e="T264" id="Seg_2778" s="T256">Таксан дьиэтигэр кэлбитэ үөрэр багайы.</ta>
            <ta e="T270" id="Seg_2779" s="T264">Тиэрээ кэпсиибин дуо?</ta>
            <ta e="T279" id="Seg_2780" s="T275">Ну этот.</ta>
            <ta e="T284" id="Seg_2781" s="T282">Дьиэтигэр кэлбите.</ta>
            <ta e="T290" id="Seg_2782" s="T285">Үчүгэй багайы һаналаак кэллэ быһылаак.</ta>
            <ta e="T299" id="Seg_2783" s="T294">Бэсэлээ.</ta>
            <ta e="T315" id="Seg_2784" s="T306">Мин инники буоллар кимниир этим, кэчэһэр этим онтон…</ta>
            <ta e="T326" id="Seg_2785" s="T316">Онтон ээ тоһуйбуппут эһэбитин кытта, огобутун кытта.</ta>
            <ta e="T336" id="Seg_2786" s="T327">Онтон үчүгэй багайытык олорбуппут, эрим арагитын быракпыта, оготун кытта.</ta>
            <ta e="T347" id="Seg_2787" s="T337">биэк һылдьар оготун һырытыннарааччы, диэбитигэр көмөлөһөөччү, үчүгэй багайыдык олоробут аны.</ta>
            <ta e="T354" id="Seg_2788" s="T348">Так наверно где-то…</ta>
            <ta e="T360" id="Seg_2789" s="T359">Все.</ta>
            <ta e="T363" id="Seg_2790" s="T361">Аһа.</ta>
            <ta e="T371" id="Seg_2791" s="T366">Элэтэ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-UoPP">
            <ta e="T65" id="Seg_2792" s="T60">– Lʼexčʼe rʼebʼonka vɨbratʼ. </ta>
            <ta e="T75" id="Seg_2793" s="T70">– Adnavo tolʼka naːda, da? </ta>
            <ta e="T88" id="Seg_2794" s="T84">– Ja pamagatʼ budu. </ta>
            <ta e="T121" id="Seg_2795" s="T117">– Dʼaktarɨ ɨl onto. </ta>
            <ta e="T137" id="Seg_2796" s="T133">– Min (kepseː) du͡o. </ta>
            <ta e="T147" id="Seg_2797" s="T142">– Pra ženšʼinu možna, da? </ta>
            <ta e="T154" id="Seg_2798" s="T148">– Da, davajtʼe. </ta>
            <ta e="T165" id="Seg_2799" s="T161">– Eta ja, da, aː. </ta>
            <ta e="T169" id="Seg_2800" s="T165">Oj, (pada-) ((LAUGH)). </ta>
            <ta e="T184" id="Seg_2801" s="T173">– Erbin kɨtta üčügej bagajtɨk oloror etibit, ogobut ogo u͡ol ogoloːk etibit. </ta>
            <ta e="T190" id="Seg_2802" s="T185">Onton, erim aragiː iher bu͡olbuta. </ta>
            <ta e="T201" id="Seg_2803" s="T191">Dogottorun kɨtta aragiːmsak bu͡olbut, onton ontulara bu͡olla min tuspunan kepseːbitter: </ta>
            <ta e="T207" id="Seg_2804" s="T201">"Ol kördük hɨldʼar, bu kördük hɨldʼar. </ta>
            <ta e="T215" id="Seg_2805" s="T208">Er kihiler, atɨn er kihileri gɨtta hɨldʼar." </ta>
            <ta e="T223" id="Seg_2806" s="T216">Ontuŋ bu͡o künüːleːn bu͡o minigin oksubuta, kɨrbaːbɨta. </ta>
            <ta e="T225" id="Seg_2807" s="T224">Kɨrbaːbɨta. </ta>
            <ta e="T229" id="Seg_2808" s="T226">Mʼilʼisʼijeler mʼilʼisʼijaga ɨlbɨttara. </ta>
            <ta e="T233" id="Seg_2809" s="T230">Onton kaːjɨːga olorputtar. </ta>
            <ta e="T235" id="Seg_2810" s="T234">Olorputtara. </ta>
            <ta e="T250" id="Seg_2811" s="T236">Mm, kaːjɨː, ör bagajɨ ördük oloron baran onton taksan… </ta>
            <ta e="T255" id="Seg_2812" s="T252">– Taksan bu͡ollar du͡o… </ta>
            <ta e="T264" id="Seg_2813" s="T256">Taksan dʼi͡etiger kelbite, ü͡örer bagajɨ. </ta>
            <ta e="T270" id="Seg_2814" s="T264">– Ti͡ere kepsiːbin du͡o? </ta>
            <ta e="T279" id="Seg_2815" s="T275">– Da, etat. </ta>
            <ta e="T284" id="Seg_2816" s="T282">– Dʼi͡etiger kelbite. </ta>
            <ta e="T290" id="Seg_2817" s="T285">Üčügej bagajɨ hanaːlaːk kelle bɨhɨlaːk. </ta>
            <ta e="T299" id="Seg_2818" s="T294">– Beseleː. </ta>
            <ta e="T315" id="Seg_2819" s="T306">Min inniki bu͡ollar kimniːr etim, kečeher etim, onton. </ta>
            <ta e="T326" id="Seg_2820" s="T316">Onton eː tohujbutum ehebin kɨtta, (ogolor) ogobutun kɨtta. </ta>
            <ta e="T336" id="Seg_2821" s="T327">Onton üčügej bagajɨtɨk olorbupput, erim aragiːtɨn bɨrakpɨta, ogotun kɨtta. </ta>
            <ta e="T347" id="Seg_2822" s="T337">Bi͡ek hɨldʼar ogotun hɨrɨtɨnnaraːččɨ, dʼi͡ebitiger kömölöhöːččü, üčügej bagajɨdɨk olorobut anɨ. </ta>
            <ta e="T354" id="Seg_2823" s="T348">Tak navʼerna gdʼe - ta… </ta>
            <ta e="T360" id="Seg_2824" s="T359">– Fsʼo. </ta>
            <ta e="T363" id="Seg_2825" s="T361">– Aha. </ta>
            <ta e="T371" id="Seg_2826" s="T366">– Elete du͡o. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-UoPP">
            <ta e="T118" id="Seg_2827" s="T117">dʼaktar-ɨ</ta>
            <ta e="T119" id="Seg_2828" s="T118">ɨl</ta>
            <ta e="T121" id="Seg_2829" s="T119">onto</ta>
            <ta e="T135" id="Seg_2830" s="T133">min</ta>
            <ta e="T136" id="Seg_2831" s="T135">kepseː</ta>
            <ta e="T137" id="Seg_2832" s="T136">du͡o</ta>
            <ta e="T166" id="Seg_2833" s="T165">oj</ta>
            <ta e="T174" id="Seg_2834" s="T173">er-bi-n</ta>
            <ta e="T175" id="Seg_2835" s="T174">kɨtta</ta>
            <ta e="T176" id="Seg_2836" s="T175">üčügej</ta>
            <ta e="T177" id="Seg_2837" s="T176">bagaj-tɨk</ta>
            <ta e="T178" id="Seg_2838" s="T177">olor-or</ta>
            <ta e="T179" id="Seg_2839" s="T178">e-ti-bit</ta>
            <ta e="T180" id="Seg_2840" s="T179">ogo-but</ta>
            <ta e="T181" id="Seg_2841" s="T180">ogo</ta>
            <ta e="T182" id="Seg_2842" s="T181">u͡ol</ta>
            <ta e="T183" id="Seg_2843" s="T182">ogo-loːk</ta>
            <ta e="T184" id="Seg_2844" s="T183">e-ti-bit</ta>
            <ta e="T186" id="Seg_2845" s="T185">onton</ta>
            <ta e="T187" id="Seg_2846" s="T186">er-i-m</ta>
            <ta e="T188" id="Seg_2847" s="T187">aragiː</ta>
            <ta e="T189" id="Seg_2848" s="T188">ih-er</ta>
            <ta e="T190" id="Seg_2849" s="T189">bu͡ol-but-a</ta>
            <ta e="T192" id="Seg_2850" s="T191">dogot-tor-u-n</ta>
            <ta e="T193" id="Seg_2851" s="T192">kɨtta</ta>
            <ta e="T194" id="Seg_2852" s="T193">aragiː-msak</ta>
            <ta e="T195" id="Seg_2853" s="T194">bu͡ol-but</ta>
            <ta e="T196" id="Seg_2854" s="T195">onton</ta>
            <ta e="T197" id="Seg_2855" s="T196">on-tu-lara</ta>
            <ta e="T198" id="Seg_2856" s="T197">bu͡olla</ta>
            <ta e="T199" id="Seg_2857" s="T198">min</ta>
            <ta e="T200" id="Seg_2858" s="T199">tus-pu-nan</ta>
            <ta e="T201" id="Seg_2859" s="T200">kepseː-bit-ter</ta>
            <ta e="T202" id="Seg_2860" s="T201">ol</ta>
            <ta e="T203" id="Seg_2861" s="T202">kördük</ta>
            <ta e="T204" id="Seg_2862" s="T203">hɨldʼ-ar</ta>
            <ta e="T205" id="Seg_2863" s="T204">bu</ta>
            <ta e="T206" id="Seg_2864" s="T205">kördük</ta>
            <ta e="T207" id="Seg_2865" s="T206">hɨldʼ-ar</ta>
            <ta e="T209" id="Seg_2866" s="T208">er</ta>
            <ta e="T210" id="Seg_2867" s="T209">kihi-ler</ta>
            <ta e="T211" id="Seg_2868" s="T210">atɨn</ta>
            <ta e="T212" id="Seg_2869" s="T211">er</ta>
            <ta e="T213" id="Seg_2870" s="T212">kihi-ler-i</ta>
            <ta e="T214" id="Seg_2871" s="T213">gɨtta</ta>
            <ta e="T215" id="Seg_2872" s="T214">hɨldʼ-ar</ta>
            <ta e="T217" id="Seg_2873" s="T216">on-tu-ŋ</ta>
            <ta e="T218" id="Seg_2874" s="T217">bu͡o</ta>
            <ta e="T219" id="Seg_2875" s="T218">künüːleː-n</ta>
            <ta e="T220" id="Seg_2876" s="T219">bu͡o</ta>
            <ta e="T221" id="Seg_2877" s="T220">minigi-n</ta>
            <ta e="T222" id="Seg_2878" s="T221">oks-u-but-a</ta>
            <ta e="T223" id="Seg_2879" s="T222">kɨrbaː-bɨt-a</ta>
            <ta e="T225" id="Seg_2880" s="T224">kɨrbaː-bɨt-a</ta>
            <ta e="T227" id="Seg_2881" s="T226">mʼilʼisʼije-ler</ta>
            <ta e="T228" id="Seg_2882" s="T227">mʼilʼisʼija-ga</ta>
            <ta e="T229" id="Seg_2883" s="T228">ɨl-bɨt-tara</ta>
            <ta e="T231" id="Seg_2884" s="T230">onton</ta>
            <ta e="T232" id="Seg_2885" s="T231">kaːjɨː-ga</ta>
            <ta e="T233" id="Seg_2886" s="T232">olor-put-tar</ta>
            <ta e="T235" id="Seg_2887" s="T234">olor-put-tara</ta>
            <ta e="T237" id="Seg_2888" s="T236">mm</ta>
            <ta e="T238" id="Seg_2889" s="T237">kaːjɨː</ta>
            <ta e="T239" id="Seg_2890" s="T238">ör</ta>
            <ta e="T240" id="Seg_2891" s="T239">bagajɨ</ta>
            <ta e="T242" id="Seg_2892" s="T240">ör-dük</ta>
            <ta e="T244" id="Seg_2893" s="T242">olor-on</ta>
            <ta e="T246" id="Seg_2894" s="T244">bar-an</ta>
            <ta e="T248" id="Seg_2895" s="T246">onton</ta>
            <ta e="T250" id="Seg_2896" s="T248">taks-an</ta>
            <ta e="T253" id="Seg_2897" s="T252">taks-an</ta>
            <ta e="T254" id="Seg_2898" s="T253">bu͡ol-lar</ta>
            <ta e="T255" id="Seg_2899" s="T254">du͡o</ta>
            <ta e="T257" id="Seg_2900" s="T256">taks-an</ta>
            <ta e="T259" id="Seg_2901" s="T257">dʼi͡e-ti-ger</ta>
            <ta e="T260" id="Seg_2902" s="T259">kel-bit-e</ta>
            <ta e="T263" id="Seg_2903" s="T260">ü͡ör-er</ta>
            <ta e="T264" id="Seg_2904" s="T263">bagajɨ</ta>
            <ta e="T266" id="Seg_2905" s="T264">ti͡ere</ta>
            <ta e="T268" id="Seg_2906" s="T266">keps-iː-bin</ta>
            <ta e="T270" id="Seg_2907" s="T268">du͡o</ta>
            <ta e="T283" id="Seg_2908" s="T282">dʼi͡e-ti-ger</ta>
            <ta e="T284" id="Seg_2909" s="T283">kel-bit-e</ta>
            <ta e="T286" id="Seg_2910" s="T285">üčügej</ta>
            <ta e="T287" id="Seg_2911" s="T286">bagajɨ</ta>
            <ta e="T288" id="Seg_2912" s="T287">hanaː-laːk</ta>
            <ta e="T289" id="Seg_2913" s="T288">kel-l-e</ta>
            <ta e="T290" id="Seg_2914" s="T289">bɨhɨlaːk</ta>
            <ta e="T299" id="Seg_2915" s="T294">beseleː</ta>
            <ta e="T307" id="Seg_2916" s="T306">min</ta>
            <ta e="T308" id="Seg_2917" s="T307">inniki</ta>
            <ta e="T309" id="Seg_2918" s="T308">bu͡ol-lar</ta>
            <ta e="T310" id="Seg_2919" s="T309">kim-niː-r</ta>
            <ta e="T311" id="Seg_2920" s="T310">e-ti-m</ta>
            <ta e="T312" id="Seg_2921" s="T311">kečeh-er</ta>
            <ta e="T313" id="Seg_2922" s="T312">e-ti-m</ta>
            <ta e="T315" id="Seg_2923" s="T313">onton</ta>
            <ta e="T318" id="Seg_2924" s="T316">onton</ta>
            <ta e="T319" id="Seg_2925" s="T318">eː</ta>
            <ta e="T321" id="Seg_2926" s="T319">tohuj-but-u-m</ta>
            <ta e="T322" id="Seg_2927" s="T321">ehe-bi-n</ta>
            <ta e="T323" id="Seg_2928" s="T322">kɨtta</ta>
            <ta e="T324" id="Seg_2929" s="T323">ogo-lor</ta>
            <ta e="T325" id="Seg_2930" s="T324">ogo-butu-n</ta>
            <ta e="T326" id="Seg_2931" s="T325">kɨtta</ta>
            <ta e="T328" id="Seg_2932" s="T327">onton</ta>
            <ta e="T329" id="Seg_2933" s="T328">üčügej</ta>
            <ta e="T330" id="Seg_2934" s="T329">bagajɨ-tɨk</ta>
            <ta e="T331" id="Seg_2935" s="T330">olor-bup-put</ta>
            <ta e="T332" id="Seg_2936" s="T331">er-i-m</ta>
            <ta e="T333" id="Seg_2937" s="T332">aragiː-tɨ-n</ta>
            <ta e="T334" id="Seg_2938" s="T333">bɨrak-pɨt-a</ta>
            <ta e="T335" id="Seg_2939" s="T334">ogo-tu-n</ta>
            <ta e="T336" id="Seg_2940" s="T335">kɨtta</ta>
            <ta e="T338" id="Seg_2941" s="T337">bi͡ek</ta>
            <ta e="T339" id="Seg_2942" s="T338">hɨldʼ-ar</ta>
            <ta e="T340" id="Seg_2943" s="T339">ogo-tu-n</ta>
            <ta e="T341" id="Seg_2944" s="T340">hɨrɨt-ɨnnar-aːččɨ</ta>
            <ta e="T342" id="Seg_2945" s="T341">dʼi͡e-biti-ger</ta>
            <ta e="T343" id="Seg_2946" s="T342">kömölöh-öːččü</ta>
            <ta e="T344" id="Seg_2947" s="T343">üčügej</ta>
            <ta e="T345" id="Seg_2948" s="T344">bagajɨ-dɨk</ta>
            <ta e="T346" id="Seg_2949" s="T345">olor-o-but</ta>
            <ta e="T347" id="Seg_2950" s="T346">anɨ</ta>
            <ta e="T368" id="Seg_2951" s="T366">ele-te</ta>
            <ta e="T371" id="Seg_2952" s="T368">du͡o</ta>
         </annotation>
         <annotation name="mp" tierref="mp-UoPP">
            <ta e="T118" id="Seg_2953" s="T117">dʼaktar-nI</ta>
            <ta e="T119" id="Seg_2954" s="T118">ɨl</ta>
            <ta e="T121" id="Seg_2955" s="T119">onton</ta>
            <ta e="T135" id="Seg_2956" s="T133">min</ta>
            <ta e="T136" id="Seg_2957" s="T135">kepseː</ta>
            <ta e="T137" id="Seg_2958" s="T136">du͡o</ta>
            <ta e="T166" id="Seg_2959" s="T165">oj</ta>
            <ta e="T174" id="Seg_2960" s="T173">er-BI-n</ta>
            <ta e="T175" id="Seg_2961" s="T174">kɨtta</ta>
            <ta e="T176" id="Seg_2962" s="T175">üčügej</ta>
            <ta e="T177" id="Seg_2963" s="T176">bagajɨ-LIk</ta>
            <ta e="T178" id="Seg_2964" s="T177">olor-Ar</ta>
            <ta e="T179" id="Seg_2965" s="T178">e-TI-BIt</ta>
            <ta e="T180" id="Seg_2966" s="T179">ogo-BIt</ta>
            <ta e="T181" id="Seg_2967" s="T180">ogo</ta>
            <ta e="T182" id="Seg_2968" s="T181">u͡ol</ta>
            <ta e="T183" id="Seg_2969" s="T182">ogo-LAːK</ta>
            <ta e="T184" id="Seg_2970" s="T183">e-TI-BIt</ta>
            <ta e="T186" id="Seg_2971" s="T185">onton</ta>
            <ta e="T187" id="Seg_2972" s="T186">er-I-m</ta>
            <ta e="T188" id="Seg_2973" s="T187">aragiː</ta>
            <ta e="T189" id="Seg_2974" s="T188">is-Ar</ta>
            <ta e="T190" id="Seg_2975" s="T189">bu͡ol-BIT-tA</ta>
            <ta e="T192" id="Seg_2976" s="T191">dogor-LAr-tI-n</ta>
            <ta e="T193" id="Seg_2977" s="T192">kɨtta</ta>
            <ta e="T194" id="Seg_2978" s="T193">aragiː-msAk</ta>
            <ta e="T195" id="Seg_2979" s="T194">bu͡ol-BIT</ta>
            <ta e="T196" id="Seg_2980" s="T195">onton</ta>
            <ta e="T197" id="Seg_2981" s="T196">ol-tI-LArA</ta>
            <ta e="T198" id="Seg_2982" s="T197">bu͡olla</ta>
            <ta e="T199" id="Seg_2983" s="T198">min</ta>
            <ta e="T200" id="Seg_2984" s="T199">tus-BI-nAn</ta>
            <ta e="T201" id="Seg_2985" s="T200">kepseː-BIT-LAr</ta>
            <ta e="T202" id="Seg_2986" s="T201">ol</ta>
            <ta e="T203" id="Seg_2987" s="T202">kördük</ta>
            <ta e="T204" id="Seg_2988" s="T203">hɨrɨt-Ar</ta>
            <ta e="T205" id="Seg_2989" s="T204">bu</ta>
            <ta e="T206" id="Seg_2990" s="T205">kördük</ta>
            <ta e="T207" id="Seg_2991" s="T206">hɨrɨt-Ar</ta>
            <ta e="T209" id="Seg_2992" s="T208">er</ta>
            <ta e="T210" id="Seg_2993" s="T209">kihi-LAr</ta>
            <ta e="T211" id="Seg_2994" s="T210">atɨn</ta>
            <ta e="T212" id="Seg_2995" s="T211">er</ta>
            <ta e="T213" id="Seg_2996" s="T212">kihi-LAr-nI</ta>
            <ta e="T214" id="Seg_2997" s="T213">kɨtta</ta>
            <ta e="T215" id="Seg_2998" s="T214">hɨrɨt-Ar</ta>
            <ta e="T217" id="Seg_2999" s="T216">ol-tI-ŋ</ta>
            <ta e="T218" id="Seg_3000" s="T217">bu͡o</ta>
            <ta e="T219" id="Seg_3001" s="T218">künüːleː-An</ta>
            <ta e="T220" id="Seg_3002" s="T219">bu͡o</ta>
            <ta e="T221" id="Seg_3003" s="T220">min-n</ta>
            <ta e="T222" id="Seg_3004" s="T221">ogus-I-BIT-tA</ta>
            <ta e="T223" id="Seg_3005" s="T222">kɨrbaː-BIT-tA</ta>
            <ta e="T225" id="Seg_3006" s="T224">kɨrbaː-BIT-tA</ta>
            <ta e="T227" id="Seg_3007" s="T226">mʼilʼisʼija-LAr</ta>
            <ta e="T228" id="Seg_3008" s="T227">mʼilʼisʼija-GA</ta>
            <ta e="T229" id="Seg_3009" s="T228">ɨl-BIT-LArA</ta>
            <ta e="T231" id="Seg_3010" s="T230">onton</ta>
            <ta e="T232" id="Seg_3011" s="T231">kaːjɨː-GA</ta>
            <ta e="T233" id="Seg_3012" s="T232">olort-BIT-LAr</ta>
            <ta e="T235" id="Seg_3013" s="T234">olort-BIT-LArA</ta>
            <ta e="T237" id="Seg_3014" s="T236">mm</ta>
            <ta e="T238" id="Seg_3015" s="T237">kaːjɨː</ta>
            <ta e="T239" id="Seg_3016" s="T238">ör</ta>
            <ta e="T240" id="Seg_3017" s="T239">bagajɨ</ta>
            <ta e="T242" id="Seg_3018" s="T240">ör-LIk</ta>
            <ta e="T244" id="Seg_3019" s="T242">olor-An</ta>
            <ta e="T246" id="Seg_3020" s="T244">bar-An</ta>
            <ta e="T248" id="Seg_3021" s="T246">onton</ta>
            <ta e="T250" id="Seg_3022" s="T248">tagɨs-An</ta>
            <ta e="T253" id="Seg_3023" s="T252">tagɨs-An</ta>
            <ta e="T254" id="Seg_3024" s="T253">bu͡ol-TAR</ta>
            <ta e="T255" id="Seg_3025" s="T254">du͡o</ta>
            <ta e="T257" id="Seg_3026" s="T256">tagɨs-An</ta>
            <ta e="T259" id="Seg_3027" s="T257">dʼi͡e-tI-GAr</ta>
            <ta e="T260" id="Seg_3028" s="T259">kel-BIT-tA</ta>
            <ta e="T263" id="Seg_3029" s="T260">ü͡ör-Ar</ta>
            <ta e="T264" id="Seg_3030" s="T263">bagajɨ</ta>
            <ta e="T266" id="Seg_3031" s="T264">ti͡ere</ta>
            <ta e="T268" id="Seg_3032" s="T266">kepseː-A-BIn</ta>
            <ta e="T270" id="Seg_3033" s="T268">du͡o</ta>
            <ta e="T283" id="Seg_3034" s="T282">dʼi͡e-tI-GAr</ta>
            <ta e="T284" id="Seg_3035" s="T283">kel-BIT-tA</ta>
            <ta e="T286" id="Seg_3036" s="T285">üčügej</ta>
            <ta e="T287" id="Seg_3037" s="T286">bagajɨ</ta>
            <ta e="T288" id="Seg_3038" s="T287">hanaː-LAːK</ta>
            <ta e="T289" id="Seg_3039" s="T288">kel-TI-tA</ta>
            <ta e="T290" id="Seg_3040" s="T289">bɨhɨːlaːk</ta>
            <ta e="T299" id="Seg_3041" s="T294">beseleː</ta>
            <ta e="T307" id="Seg_3042" s="T306">min</ta>
            <ta e="T308" id="Seg_3043" s="T307">inniki</ta>
            <ta e="T309" id="Seg_3044" s="T308">bu͡ol-TAR</ta>
            <ta e="T310" id="Seg_3045" s="T309">kim-LAː-Ar</ta>
            <ta e="T311" id="Seg_3046" s="T310">e-TI-m</ta>
            <ta e="T312" id="Seg_3047" s="T311">kečes-Ar</ta>
            <ta e="T313" id="Seg_3048" s="T312">e-TI-m</ta>
            <ta e="T315" id="Seg_3049" s="T313">onton</ta>
            <ta e="T318" id="Seg_3050" s="T316">onton</ta>
            <ta e="T319" id="Seg_3051" s="T318">eː</ta>
            <ta e="T321" id="Seg_3052" s="T319">tohuj-BIT-I-m</ta>
            <ta e="T322" id="Seg_3053" s="T321">ehe-BI-n</ta>
            <ta e="T323" id="Seg_3054" s="T322">kɨtta</ta>
            <ta e="T324" id="Seg_3055" s="T323">ogo-LAr</ta>
            <ta e="T325" id="Seg_3056" s="T324">ogo-BItI-n</ta>
            <ta e="T326" id="Seg_3057" s="T325">kɨtta</ta>
            <ta e="T328" id="Seg_3058" s="T327">onton</ta>
            <ta e="T329" id="Seg_3059" s="T328">üčügej</ta>
            <ta e="T330" id="Seg_3060" s="T329">bagajɨ-LIk</ta>
            <ta e="T331" id="Seg_3061" s="T330">olor-BIT-BIt</ta>
            <ta e="T332" id="Seg_3062" s="T331">er-I-m</ta>
            <ta e="T333" id="Seg_3063" s="T332">aragiː-tI-n</ta>
            <ta e="T334" id="Seg_3064" s="T333">bɨrak-BIT-tA</ta>
            <ta e="T335" id="Seg_3065" s="T334">ogo-tI-n</ta>
            <ta e="T336" id="Seg_3066" s="T335">kɨtta</ta>
            <ta e="T338" id="Seg_3067" s="T337">bi͡ek</ta>
            <ta e="T339" id="Seg_3068" s="T338">hɨrɨt-Ar</ta>
            <ta e="T340" id="Seg_3069" s="T339">ogo-tI-n</ta>
            <ta e="T341" id="Seg_3070" s="T340">hɨrɨt-InnAr-AːččI</ta>
            <ta e="T342" id="Seg_3071" s="T341">dʼi͡e-BItI-GAr</ta>
            <ta e="T343" id="Seg_3072" s="T342">kömölös-AːččI</ta>
            <ta e="T344" id="Seg_3073" s="T343">üčügej</ta>
            <ta e="T345" id="Seg_3074" s="T344">bagajɨ-LIk</ta>
            <ta e="T346" id="Seg_3075" s="T345">olor-A-BIt</ta>
            <ta e="T347" id="Seg_3076" s="T346">anɨ</ta>
            <ta e="T368" id="Seg_3077" s="T366">ele-tA</ta>
            <ta e="T371" id="Seg_3078" s="T368">du͡o</ta>
         </annotation>
         <annotation name="ge" tierref="ge-UoPP">
            <ta e="T118" id="Seg_3079" s="T117">woman-ACC</ta>
            <ta e="T119" id="Seg_3080" s="T118">take.[IMP.2SG]</ta>
            <ta e="T121" id="Seg_3081" s="T119">then</ta>
            <ta e="T135" id="Seg_3082" s="T133">1SG.[NOM]</ta>
            <ta e="T136" id="Seg_3083" s="T135">tell</ta>
            <ta e="T137" id="Seg_3084" s="T136">Q</ta>
            <ta e="T166" id="Seg_3085" s="T165">EXCL</ta>
            <ta e="T174" id="Seg_3086" s="T173">husband-1SG-ACC</ta>
            <ta e="T175" id="Seg_3087" s="T174">with</ta>
            <ta e="T176" id="Seg_3088" s="T175">good.[NOM]</ta>
            <ta e="T177" id="Seg_3089" s="T176">very-ADVZ</ta>
            <ta e="T178" id="Seg_3090" s="T177">live-PTCP.PRS</ta>
            <ta e="T179" id="Seg_3091" s="T178">be-PST1-1PL</ta>
            <ta e="T180" id="Seg_3092" s="T179">child-1PL.[NOM]</ta>
            <ta e="T181" id="Seg_3093" s="T180">child.[NOM]</ta>
            <ta e="T182" id="Seg_3094" s="T181">boy.[NOM]</ta>
            <ta e="T183" id="Seg_3095" s="T182">child-PROPR.[NOM]</ta>
            <ta e="T184" id="Seg_3096" s="T183">be-PST1-1PL</ta>
            <ta e="T186" id="Seg_3097" s="T185">then</ta>
            <ta e="T187" id="Seg_3098" s="T186">man-EP-1SG.[NOM]</ta>
            <ta e="T188" id="Seg_3099" s="T187">spirit.[NOM]</ta>
            <ta e="T189" id="Seg_3100" s="T188">drink-PTCP.PRS</ta>
            <ta e="T190" id="Seg_3101" s="T189">become-PST2-3SG</ta>
            <ta e="T192" id="Seg_3102" s="T191">friend-PL-3SG-ACC</ta>
            <ta e="T193" id="Seg_3103" s="T192">with</ta>
            <ta e="T194" id="Seg_3104" s="T193">spirit-PHIL.[NOM]</ta>
            <ta e="T195" id="Seg_3105" s="T194">become-PST2.[3SG]</ta>
            <ta e="T196" id="Seg_3106" s="T195">then</ta>
            <ta e="T197" id="Seg_3107" s="T196">that-3SG-3PL.[NOM]</ta>
            <ta e="T198" id="Seg_3108" s="T197">MOD</ta>
            <ta e="T199" id="Seg_3109" s="T198">1SG.[NOM]</ta>
            <ta e="T200" id="Seg_3110" s="T199">side-1SG-INSTR</ta>
            <ta e="T201" id="Seg_3111" s="T200">tell-PST2-3PL</ta>
            <ta e="T202" id="Seg_3112" s="T201">that.[NOM]</ta>
            <ta e="T203" id="Seg_3113" s="T202">similar</ta>
            <ta e="T204" id="Seg_3114" s="T203">go-PRS.[3SG]</ta>
            <ta e="T205" id="Seg_3115" s="T204">this.[NOM]</ta>
            <ta e="T206" id="Seg_3116" s="T205">similar</ta>
            <ta e="T207" id="Seg_3117" s="T206">go-PRS.[3SG]</ta>
            <ta e="T209" id="Seg_3118" s="T208">man.[NOM]</ta>
            <ta e="T210" id="Seg_3119" s="T209">human.being-PL.[NOM]</ta>
            <ta e="T211" id="Seg_3120" s="T210">different</ta>
            <ta e="T212" id="Seg_3121" s="T211">man.[NOM]</ta>
            <ta e="T213" id="Seg_3122" s="T212">human.being-PL-ACC</ta>
            <ta e="T214" id="Seg_3123" s="T213">with</ta>
            <ta e="T215" id="Seg_3124" s="T214">go-PRS.[3SG]</ta>
            <ta e="T217" id="Seg_3125" s="T216">that-3SG-2SG.[NOM]</ta>
            <ta e="T218" id="Seg_3126" s="T217">EMPH</ta>
            <ta e="T219" id="Seg_3127" s="T218">be.jealous-CVB.SEQ</ta>
            <ta e="T220" id="Seg_3128" s="T219">EMPH</ta>
            <ta e="T221" id="Seg_3129" s="T220">1SG-ACC</ta>
            <ta e="T222" id="Seg_3130" s="T221">beat-EP-PST2-3SG</ta>
            <ta e="T223" id="Seg_3131" s="T222">thrash-PST2-3SG</ta>
            <ta e="T225" id="Seg_3132" s="T224">thrash-PST2-3SG</ta>
            <ta e="T227" id="Seg_3133" s="T226">police-PL.[NOM]</ta>
            <ta e="T228" id="Seg_3134" s="T227">police-DAT/LOC</ta>
            <ta e="T229" id="Seg_3135" s="T228">take-PST2-3PL</ta>
            <ta e="T231" id="Seg_3136" s="T230">then</ta>
            <ta e="T232" id="Seg_3137" s="T231">prison-DAT/LOC</ta>
            <ta e="T233" id="Seg_3138" s="T232">seat-PST2-3PL</ta>
            <ta e="T235" id="Seg_3139" s="T234">seat-PST2-3PL</ta>
            <ta e="T237" id="Seg_3140" s="T236">mm</ta>
            <ta e="T238" id="Seg_3141" s="T237">prison</ta>
            <ta e="T239" id="Seg_3142" s="T238">long</ta>
            <ta e="T240" id="Seg_3143" s="T239">very</ta>
            <ta e="T242" id="Seg_3144" s="T240">long-ADVZ</ta>
            <ta e="T244" id="Seg_3145" s="T242">sit-CVB.SEQ</ta>
            <ta e="T246" id="Seg_3146" s="T244">go-CVB.SEQ</ta>
            <ta e="T248" id="Seg_3147" s="T246">then</ta>
            <ta e="T250" id="Seg_3148" s="T248">go.out-CVB.SEQ</ta>
            <ta e="T253" id="Seg_3149" s="T252">go.out-CVB.SEQ</ta>
            <ta e="T254" id="Seg_3150" s="T253">be-COND.[3SG]</ta>
            <ta e="T255" id="Seg_3151" s="T254">Q</ta>
            <ta e="T257" id="Seg_3152" s="T256">go.out-CVB.SEQ</ta>
            <ta e="T259" id="Seg_3153" s="T257">house-3SG-DAT/LOC</ta>
            <ta e="T260" id="Seg_3154" s="T259">come-PST2-3SG</ta>
            <ta e="T263" id="Seg_3155" s="T260">be.happy-PRS.[3SG]</ta>
            <ta e="T264" id="Seg_3156" s="T263">very</ta>
            <ta e="T266" id="Seg_3157" s="T264">wrong</ta>
            <ta e="T268" id="Seg_3158" s="T266">tell-PRS-1SG</ta>
            <ta e="T270" id="Seg_3159" s="T268">Q</ta>
            <ta e="T283" id="Seg_3160" s="T282">house-3SG-DAT/LOC</ta>
            <ta e="T284" id="Seg_3161" s="T283">come-PST2-3SG</ta>
            <ta e="T286" id="Seg_3162" s="T285">good.[NOM]</ta>
            <ta e="T287" id="Seg_3163" s="T286">very</ta>
            <ta e="T288" id="Seg_3164" s="T287">thought-PROPR.[NOM]</ta>
            <ta e="T289" id="Seg_3165" s="T288">come-PST1-3SG</ta>
            <ta e="T290" id="Seg_3166" s="T289">apparently</ta>
            <ta e="T299" id="Seg_3167" s="T294">happily</ta>
            <ta e="T307" id="Seg_3168" s="T306">1SG.[NOM]</ta>
            <ta e="T308" id="Seg_3169" s="T307">front</ta>
            <ta e="T309" id="Seg_3170" s="T308">be-COND.[3SG]</ta>
            <ta e="T310" id="Seg_3171" s="T309">who-VBZ-PTCP.PRS</ta>
            <ta e="T311" id="Seg_3172" s="T310">be-PST1-1SG</ta>
            <ta e="T312" id="Seg_3173" s="T311">fear-PTCP.PRS</ta>
            <ta e="T313" id="Seg_3174" s="T312">be-PST1-1SG</ta>
            <ta e="T315" id="Seg_3175" s="T313">then</ta>
            <ta e="T318" id="Seg_3176" s="T316">then</ta>
            <ta e="T319" id="Seg_3177" s="T318">eh</ta>
            <ta e="T321" id="Seg_3178" s="T319">receive-PST2-EP-1SG</ta>
            <ta e="T322" id="Seg_3179" s="T321">grandfather-1SG-ACC</ta>
            <ta e="T323" id="Seg_3180" s="T322">with</ta>
            <ta e="T324" id="Seg_3181" s="T323">child-PL.[NOM]</ta>
            <ta e="T325" id="Seg_3182" s="T324">child-1PL-ACC</ta>
            <ta e="T326" id="Seg_3183" s="T325">with</ta>
            <ta e="T328" id="Seg_3184" s="T327">then</ta>
            <ta e="T329" id="Seg_3185" s="T328">good.[NOM]</ta>
            <ta e="T330" id="Seg_3186" s="T329">very-ADVZ</ta>
            <ta e="T331" id="Seg_3187" s="T330">live-PST2-1PL</ta>
            <ta e="T332" id="Seg_3188" s="T331">man-EP-1SG.[NOM]</ta>
            <ta e="T333" id="Seg_3189" s="T332">spirit-3SG-ACC</ta>
            <ta e="T334" id="Seg_3190" s="T333">throw-PST2-3SG</ta>
            <ta e="T335" id="Seg_3191" s="T334">child-3SG-ACC</ta>
            <ta e="T336" id="Seg_3192" s="T335">with</ta>
            <ta e="T338" id="Seg_3193" s="T337">always</ta>
            <ta e="T339" id="Seg_3194" s="T338">go-PRS.[3SG]</ta>
            <ta e="T340" id="Seg_3195" s="T339">child-3SG-ACC</ta>
            <ta e="T341" id="Seg_3196" s="T340">go-CAUS-HAB.[3SG]</ta>
            <ta e="T342" id="Seg_3197" s="T341">house-1PL-DAT/LOC</ta>
            <ta e="T343" id="Seg_3198" s="T342">help-HAB.[3SG]</ta>
            <ta e="T344" id="Seg_3199" s="T343">good.[NOM]</ta>
            <ta e="T345" id="Seg_3200" s="T344">very-ADVZ</ta>
            <ta e="T346" id="Seg_3201" s="T345">live-PRS-1PL</ta>
            <ta e="T347" id="Seg_3202" s="T346">now</ta>
            <ta e="T368" id="Seg_3203" s="T366">last-3SG.[NOM]</ta>
            <ta e="T371" id="Seg_3204" s="T368">MOD</ta>
         </annotation>
         <annotation name="gg" tierref="gg-UoPP">
            <ta e="T118" id="Seg_3205" s="T117">Frau-ACC</ta>
            <ta e="T119" id="Seg_3206" s="T118">nehmen.[IMP.2SG]</ta>
            <ta e="T121" id="Seg_3207" s="T119">dann</ta>
            <ta e="T135" id="Seg_3208" s="T133">1SG.[NOM]</ta>
            <ta e="T136" id="Seg_3209" s="T135">erzählen</ta>
            <ta e="T137" id="Seg_3210" s="T136">Q</ta>
            <ta e="T166" id="Seg_3211" s="T165">EXCL</ta>
            <ta e="T174" id="Seg_3212" s="T173">Ehemann-1SG-ACC</ta>
            <ta e="T175" id="Seg_3213" s="T174">mit</ta>
            <ta e="T176" id="Seg_3214" s="T175">gut.[NOM]</ta>
            <ta e="T177" id="Seg_3215" s="T176">sehr-ADVZ</ta>
            <ta e="T178" id="Seg_3216" s="T177">leben-PTCP.PRS</ta>
            <ta e="T179" id="Seg_3217" s="T178">sein-PST1-1PL</ta>
            <ta e="T180" id="Seg_3218" s="T179">Kind-1PL.[NOM]</ta>
            <ta e="T181" id="Seg_3219" s="T180">Kind.[NOM]</ta>
            <ta e="T182" id="Seg_3220" s="T181">Junge.[NOM]</ta>
            <ta e="T183" id="Seg_3221" s="T182">Kind-PROPR.[NOM]</ta>
            <ta e="T184" id="Seg_3222" s="T183">sein-PST1-1PL</ta>
            <ta e="T186" id="Seg_3223" s="T185">dann</ta>
            <ta e="T187" id="Seg_3224" s="T186">Mann-EP-1SG.[NOM]</ta>
            <ta e="T188" id="Seg_3225" s="T187">Schnaps.[NOM]</ta>
            <ta e="T189" id="Seg_3226" s="T188">trinken-PTCP.PRS</ta>
            <ta e="T190" id="Seg_3227" s="T189">werden-PST2-3SG</ta>
            <ta e="T192" id="Seg_3228" s="T191">Freund-PL-3SG-ACC</ta>
            <ta e="T193" id="Seg_3229" s="T192">mit</ta>
            <ta e="T194" id="Seg_3230" s="T193">Schnaps-PHIL.[NOM]</ta>
            <ta e="T195" id="Seg_3231" s="T194">werden-PST2.[3SG]</ta>
            <ta e="T196" id="Seg_3232" s="T195">dann</ta>
            <ta e="T197" id="Seg_3233" s="T196">jenes-3SG-3PL.[NOM]</ta>
            <ta e="T198" id="Seg_3234" s="T197">MOD</ta>
            <ta e="T199" id="Seg_3235" s="T198">1SG.[NOM]</ta>
            <ta e="T200" id="Seg_3236" s="T199">Seite-1SG-INSTR</ta>
            <ta e="T201" id="Seg_3237" s="T200">erzählen-PST2-3PL</ta>
            <ta e="T202" id="Seg_3238" s="T201">jenes.[NOM]</ta>
            <ta e="T203" id="Seg_3239" s="T202">ähnlich</ta>
            <ta e="T204" id="Seg_3240" s="T203">gehen-PRS.[3SG]</ta>
            <ta e="T205" id="Seg_3241" s="T204">dieses.[NOM]</ta>
            <ta e="T206" id="Seg_3242" s="T205">ähnlich</ta>
            <ta e="T207" id="Seg_3243" s="T206">gehen-PRS.[3SG]</ta>
            <ta e="T209" id="Seg_3244" s="T208">Mann.[NOM]</ta>
            <ta e="T210" id="Seg_3245" s="T209">Mensch-PL.[NOM]</ta>
            <ta e="T211" id="Seg_3246" s="T210">anders</ta>
            <ta e="T212" id="Seg_3247" s="T211">Mann.[NOM]</ta>
            <ta e="T213" id="Seg_3248" s="T212">Mensch-PL-ACC</ta>
            <ta e="T214" id="Seg_3249" s="T213">mit</ta>
            <ta e="T215" id="Seg_3250" s="T214">gehen-PRS.[3SG]</ta>
            <ta e="T217" id="Seg_3251" s="T216">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T218" id="Seg_3252" s="T217">EMPH</ta>
            <ta e="T219" id="Seg_3253" s="T218">eifersüchtig.sein-CVB.SEQ</ta>
            <ta e="T220" id="Seg_3254" s="T219">EMPH</ta>
            <ta e="T221" id="Seg_3255" s="T220">1SG-ACC</ta>
            <ta e="T222" id="Seg_3256" s="T221">schlagen-EP-PST2-3SG</ta>
            <ta e="T223" id="Seg_3257" s="T222">prügeln-PST2-3SG</ta>
            <ta e="T225" id="Seg_3258" s="T224">prügeln-PST2-3SG</ta>
            <ta e="T227" id="Seg_3259" s="T226">Polizei-PL.[NOM]</ta>
            <ta e="T228" id="Seg_3260" s="T227">Polizei-DAT/LOC</ta>
            <ta e="T229" id="Seg_3261" s="T228">nehmen-PST2-3PL</ta>
            <ta e="T231" id="Seg_3262" s="T230">dann</ta>
            <ta e="T232" id="Seg_3263" s="T231">Gefängnis-DAT/LOC</ta>
            <ta e="T233" id="Seg_3264" s="T232">setzen-PST2-3PL</ta>
            <ta e="T235" id="Seg_3265" s="T234">setzen-PST2-3PL</ta>
            <ta e="T237" id="Seg_3266" s="T236">mm</ta>
            <ta e="T238" id="Seg_3267" s="T237">Gefängnis</ta>
            <ta e="T239" id="Seg_3268" s="T238">lange</ta>
            <ta e="T240" id="Seg_3269" s="T239">sehr</ta>
            <ta e="T242" id="Seg_3270" s="T240">lange-ADVZ</ta>
            <ta e="T244" id="Seg_3271" s="T242">sitzen-CVB.SEQ</ta>
            <ta e="T246" id="Seg_3272" s="T244">gehen-CVB.SEQ</ta>
            <ta e="T248" id="Seg_3273" s="T246">dann</ta>
            <ta e="T250" id="Seg_3274" s="T248">hinausgehen-CVB.SEQ</ta>
            <ta e="T253" id="Seg_3275" s="T252">hinausgehen-CVB.SEQ</ta>
            <ta e="T254" id="Seg_3276" s="T253">sein-COND.[3SG]</ta>
            <ta e="T255" id="Seg_3277" s="T254">Q</ta>
            <ta e="T257" id="Seg_3278" s="T256">hinausgehen-CVB.SEQ</ta>
            <ta e="T259" id="Seg_3279" s="T257">Haus-3SG-DAT/LOC</ta>
            <ta e="T260" id="Seg_3280" s="T259">kommen-PST2-3SG</ta>
            <ta e="T263" id="Seg_3281" s="T260">sich.freuen-PRS.[3SG]</ta>
            <ta e="T264" id="Seg_3282" s="T263">sehr</ta>
            <ta e="T266" id="Seg_3283" s="T264">falsch</ta>
            <ta e="T268" id="Seg_3284" s="T266">erzählen-PRS-1SG</ta>
            <ta e="T270" id="Seg_3285" s="T268">Q</ta>
            <ta e="T283" id="Seg_3286" s="T282">Haus-3SG-DAT/LOC</ta>
            <ta e="T284" id="Seg_3287" s="T283">kommen-PST2-3SG</ta>
            <ta e="T286" id="Seg_3288" s="T285">gut.[NOM]</ta>
            <ta e="T287" id="Seg_3289" s="T286">sehr</ta>
            <ta e="T288" id="Seg_3290" s="T287">Gedanke-PROPR.[NOM]</ta>
            <ta e="T289" id="Seg_3291" s="T288">kommen-PST1-3SG</ta>
            <ta e="T290" id="Seg_3292" s="T289">offenbar</ta>
            <ta e="T299" id="Seg_3293" s="T294">fröhlich</ta>
            <ta e="T307" id="Seg_3294" s="T306">1SG.[NOM]</ta>
            <ta e="T308" id="Seg_3295" s="T307">vorderer</ta>
            <ta e="T309" id="Seg_3296" s="T308">sein-COND.[3SG]</ta>
            <ta e="T310" id="Seg_3297" s="T309">wer-VBZ-PTCP.PRS</ta>
            <ta e="T311" id="Seg_3298" s="T310">sein-PST1-1SG</ta>
            <ta e="T312" id="Seg_3299" s="T311">fürchten-PTCP.PRS</ta>
            <ta e="T313" id="Seg_3300" s="T312">sein-PST1-1SG</ta>
            <ta e="T315" id="Seg_3301" s="T313">dann</ta>
            <ta e="T318" id="Seg_3302" s="T316">dann</ta>
            <ta e="T319" id="Seg_3303" s="T318">äh</ta>
            <ta e="T321" id="Seg_3304" s="T319">empfangen-PST2-EP-1SG</ta>
            <ta e="T322" id="Seg_3305" s="T321">Großvater-1SG-ACC</ta>
            <ta e="T323" id="Seg_3306" s="T322">mit</ta>
            <ta e="T324" id="Seg_3307" s="T323">Kind-PL.[NOM]</ta>
            <ta e="T325" id="Seg_3308" s="T324">Kind-1PL-ACC</ta>
            <ta e="T326" id="Seg_3309" s="T325">mit</ta>
            <ta e="T328" id="Seg_3310" s="T327">dann</ta>
            <ta e="T329" id="Seg_3311" s="T328">gut.[NOM]</ta>
            <ta e="T330" id="Seg_3312" s="T329">sehr-ADVZ</ta>
            <ta e="T331" id="Seg_3313" s="T330">leben-PST2-1PL</ta>
            <ta e="T332" id="Seg_3314" s="T331">Mann-EP-1SG.[NOM]</ta>
            <ta e="T333" id="Seg_3315" s="T332">Schnaps-3SG-ACC</ta>
            <ta e="T334" id="Seg_3316" s="T333">werfen-PST2-3SG</ta>
            <ta e="T335" id="Seg_3317" s="T334">Kind-3SG-ACC</ta>
            <ta e="T336" id="Seg_3318" s="T335">mit</ta>
            <ta e="T338" id="Seg_3319" s="T337">immer</ta>
            <ta e="T339" id="Seg_3320" s="T338">gehen-PRS.[3SG]</ta>
            <ta e="T340" id="Seg_3321" s="T339">Kind-3SG-ACC</ta>
            <ta e="T341" id="Seg_3322" s="T340">gehen-CAUS-HAB.[3SG]</ta>
            <ta e="T342" id="Seg_3323" s="T341">Haus-1PL-DAT/LOC</ta>
            <ta e="T343" id="Seg_3324" s="T342">helfen-HAB.[3SG]</ta>
            <ta e="T344" id="Seg_3325" s="T343">gut.[NOM]</ta>
            <ta e="T345" id="Seg_3326" s="T344">sehr-ADVZ</ta>
            <ta e="T346" id="Seg_3327" s="T345">leben-PRS-1PL</ta>
            <ta e="T347" id="Seg_3328" s="T346">jetzt</ta>
            <ta e="T368" id="Seg_3329" s="T366">letzter-3SG.[NOM]</ta>
            <ta e="T371" id="Seg_3330" s="T368">MOD</ta>
         </annotation>
         <annotation name="gr" tierref="gr-UoPP">
            <ta e="T118" id="Seg_3331" s="T117">жена-ACC</ta>
            <ta e="T119" id="Seg_3332" s="T118">взять.[IMP.2SG]</ta>
            <ta e="T121" id="Seg_3333" s="T119">потом</ta>
            <ta e="T135" id="Seg_3334" s="T133">1SG.[NOM]</ta>
            <ta e="T136" id="Seg_3335" s="T135">рассказывать</ta>
            <ta e="T137" id="Seg_3336" s="T136">Q</ta>
            <ta e="T166" id="Seg_3337" s="T165">EXCL</ta>
            <ta e="T174" id="Seg_3338" s="T173">муж-1SG-ACC</ta>
            <ta e="T175" id="Seg_3339" s="T174">с</ta>
            <ta e="T176" id="Seg_3340" s="T175">хороший.[NOM]</ta>
            <ta e="T177" id="Seg_3341" s="T176">очень-ADVZ</ta>
            <ta e="T178" id="Seg_3342" s="T177">жить-PTCP.PRS</ta>
            <ta e="T179" id="Seg_3343" s="T178">быть-PST1-1PL</ta>
            <ta e="T180" id="Seg_3344" s="T179">ребенок-1PL.[NOM]</ta>
            <ta e="T181" id="Seg_3345" s="T180">ребенок.[NOM]</ta>
            <ta e="T182" id="Seg_3346" s="T181">мальчик.[NOM]</ta>
            <ta e="T183" id="Seg_3347" s="T182">ребенок-PROPR.[NOM]</ta>
            <ta e="T184" id="Seg_3348" s="T183">быть-PST1-1PL</ta>
            <ta e="T186" id="Seg_3349" s="T185">потом</ta>
            <ta e="T187" id="Seg_3350" s="T186">мужчина-EP-1SG.[NOM]</ta>
            <ta e="T188" id="Seg_3351" s="T187">спиртное.[NOM]</ta>
            <ta e="T189" id="Seg_3352" s="T188">пить-PTCP.PRS</ta>
            <ta e="T190" id="Seg_3353" s="T189">становиться-PST2-3SG</ta>
            <ta e="T192" id="Seg_3354" s="T191">друг-PL-3SG-ACC</ta>
            <ta e="T193" id="Seg_3355" s="T192">с</ta>
            <ta e="T194" id="Seg_3356" s="T193">спиртное-PHIL.[NOM]</ta>
            <ta e="T195" id="Seg_3357" s="T194">становиться-PST2.[3SG]</ta>
            <ta e="T196" id="Seg_3358" s="T195">потом</ta>
            <ta e="T197" id="Seg_3359" s="T196">тот-3SG-3PL.[NOM]</ta>
            <ta e="T198" id="Seg_3360" s="T197">MOD</ta>
            <ta e="T199" id="Seg_3361" s="T198">1SG.[NOM]</ta>
            <ta e="T200" id="Seg_3362" s="T199">сторона-1SG-INSTR</ta>
            <ta e="T201" id="Seg_3363" s="T200">рассказывать-PST2-3PL</ta>
            <ta e="T202" id="Seg_3364" s="T201">тот.[NOM]</ta>
            <ta e="T203" id="Seg_3365" s="T202">подобно</ta>
            <ta e="T204" id="Seg_3366" s="T203">идти-PRS.[3SG]</ta>
            <ta e="T205" id="Seg_3367" s="T204">этот.[NOM]</ta>
            <ta e="T206" id="Seg_3368" s="T205">подобно</ta>
            <ta e="T207" id="Seg_3369" s="T206">идти-PRS.[3SG]</ta>
            <ta e="T209" id="Seg_3370" s="T208">мужчина.[NOM]</ta>
            <ta e="T210" id="Seg_3371" s="T209">человек-PL.[NOM]</ta>
            <ta e="T211" id="Seg_3372" s="T210">другой</ta>
            <ta e="T212" id="Seg_3373" s="T211">мужчина.[NOM]</ta>
            <ta e="T213" id="Seg_3374" s="T212">человек-PL-ACC</ta>
            <ta e="T214" id="Seg_3375" s="T213">с</ta>
            <ta e="T215" id="Seg_3376" s="T214">идти-PRS.[3SG]</ta>
            <ta e="T217" id="Seg_3377" s="T216">тот-3SG-2SG.[NOM]</ta>
            <ta e="T218" id="Seg_3378" s="T217">EMPH</ta>
            <ta e="T219" id="Seg_3379" s="T218">ревновать-CVB.SEQ</ta>
            <ta e="T220" id="Seg_3380" s="T219">EMPH</ta>
            <ta e="T221" id="Seg_3381" s="T220">1SG-ACC</ta>
            <ta e="T222" id="Seg_3382" s="T221">бить-EP-PST2-3SG</ta>
            <ta e="T223" id="Seg_3383" s="T222">бить-PST2-3SG</ta>
            <ta e="T225" id="Seg_3384" s="T224">бить-PST2-3SG</ta>
            <ta e="T227" id="Seg_3385" s="T226">милиция-PL.[NOM]</ta>
            <ta e="T228" id="Seg_3386" s="T227">милиция-DAT/LOC</ta>
            <ta e="T229" id="Seg_3387" s="T228">взять-PST2-3PL</ta>
            <ta e="T231" id="Seg_3388" s="T230">потом</ta>
            <ta e="T232" id="Seg_3389" s="T231">тюрьма-DAT/LOC</ta>
            <ta e="T233" id="Seg_3390" s="T232">посадить-PST2-3PL</ta>
            <ta e="T235" id="Seg_3391" s="T234">посадить-PST2-3PL</ta>
            <ta e="T237" id="Seg_3392" s="T236">мм</ta>
            <ta e="T238" id="Seg_3393" s="T237">тюрьма</ta>
            <ta e="T239" id="Seg_3394" s="T238">долго</ta>
            <ta e="T240" id="Seg_3395" s="T239">очень</ta>
            <ta e="T242" id="Seg_3396" s="T240">долго-ADVZ</ta>
            <ta e="T244" id="Seg_3397" s="T242">сидеть-CVB.SEQ</ta>
            <ta e="T246" id="Seg_3398" s="T244">идти-CVB.SEQ</ta>
            <ta e="T248" id="Seg_3399" s="T246">потом</ta>
            <ta e="T250" id="Seg_3400" s="T248">выйти-CVB.SEQ</ta>
            <ta e="T253" id="Seg_3401" s="T252">выйти-CVB.SEQ</ta>
            <ta e="T254" id="Seg_3402" s="T253">быть-COND.[3SG]</ta>
            <ta e="T255" id="Seg_3403" s="T254">Q</ta>
            <ta e="T257" id="Seg_3404" s="T256">выйти-CVB.SEQ</ta>
            <ta e="T259" id="Seg_3405" s="T257">дом-3SG-DAT/LOC</ta>
            <ta e="T260" id="Seg_3406" s="T259">приходить-PST2-3SG</ta>
            <ta e="T263" id="Seg_3407" s="T260">радоваться-PRS.[3SG]</ta>
            <ta e="T264" id="Seg_3408" s="T263">очень</ta>
            <ta e="T266" id="Seg_3409" s="T264">неправильный</ta>
            <ta e="T268" id="Seg_3410" s="T266">рассказывать-PRS-1SG</ta>
            <ta e="T270" id="Seg_3411" s="T268">Q</ta>
            <ta e="T283" id="Seg_3412" s="T282">дом-3SG-DAT/LOC</ta>
            <ta e="T284" id="Seg_3413" s="T283">приходить-PST2-3SG</ta>
            <ta e="T286" id="Seg_3414" s="T285">хороший.[NOM]</ta>
            <ta e="T287" id="Seg_3415" s="T286">очень</ta>
            <ta e="T288" id="Seg_3416" s="T287">мысль-PROPR.[NOM]</ta>
            <ta e="T289" id="Seg_3417" s="T288">приходить-PST1-3SG</ta>
            <ta e="T290" id="Seg_3418" s="T289">наверное</ta>
            <ta e="T299" id="Seg_3419" s="T294">весело</ta>
            <ta e="T307" id="Seg_3420" s="T306">1SG.[NOM]</ta>
            <ta e="T308" id="Seg_3421" s="T307">передний</ta>
            <ta e="T309" id="Seg_3422" s="T308">быть-COND.[3SG]</ta>
            <ta e="T310" id="Seg_3423" s="T309">кто-VBZ-PTCP.PRS</ta>
            <ta e="T311" id="Seg_3424" s="T310">быть-PST1-1SG</ta>
            <ta e="T312" id="Seg_3425" s="T311">опасаться-PTCP.PRS</ta>
            <ta e="T313" id="Seg_3426" s="T312">быть-PST1-1SG</ta>
            <ta e="T315" id="Seg_3427" s="T313">потом</ta>
            <ta e="T318" id="Seg_3428" s="T316">потом</ta>
            <ta e="T319" id="Seg_3429" s="T318">ээ</ta>
            <ta e="T321" id="Seg_3430" s="T319">принимать-PST2-EP-1SG</ta>
            <ta e="T322" id="Seg_3431" s="T321">дедушка-1SG-ACC</ta>
            <ta e="T323" id="Seg_3432" s="T322">с</ta>
            <ta e="T324" id="Seg_3433" s="T323">ребенок-PL.[NOM]</ta>
            <ta e="T325" id="Seg_3434" s="T324">ребенок-1PL-ACC</ta>
            <ta e="T326" id="Seg_3435" s="T325">с</ta>
            <ta e="T328" id="Seg_3436" s="T327">потом</ta>
            <ta e="T329" id="Seg_3437" s="T328">хороший.[NOM]</ta>
            <ta e="T330" id="Seg_3438" s="T329">очень-ADVZ</ta>
            <ta e="T331" id="Seg_3439" s="T330">жить-PST2-1PL</ta>
            <ta e="T332" id="Seg_3440" s="T331">мужчина-EP-1SG.[NOM]</ta>
            <ta e="T333" id="Seg_3441" s="T332">спиртное-3SG-ACC</ta>
            <ta e="T334" id="Seg_3442" s="T333">бросать-PST2-3SG</ta>
            <ta e="T335" id="Seg_3443" s="T334">ребенок-3SG-ACC</ta>
            <ta e="T336" id="Seg_3444" s="T335">с</ta>
            <ta e="T338" id="Seg_3445" s="T337">всегда</ta>
            <ta e="T339" id="Seg_3446" s="T338">идти-PRS.[3SG]</ta>
            <ta e="T340" id="Seg_3447" s="T339">ребенок-3SG-ACC</ta>
            <ta e="T341" id="Seg_3448" s="T340">идти-CAUS-HAB.[3SG]</ta>
            <ta e="T342" id="Seg_3449" s="T341">дом-1PL-DAT/LOC</ta>
            <ta e="T343" id="Seg_3450" s="T342">помогать-HAB.[3SG]</ta>
            <ta e="T344" id="Seg_3451" s="T343">хороший.[NOM]</ta>
            <ta e="T345" id="Seg_3452" s="T344">очень-ADVZ</ta>
            <ta e="T346" id="Seg_3453" s="T345">жить-PRS-1PL</ta>
            <ta e="T347" id="Seg_3454" s="T346">теперь</ta>
            <ta e="T368" id="Seg_3455" s="T366">последний-3SG.[NOM]</ta>
            <ta e="T371" id="Seg_3456" s="T368">MOD</ta>
         </annotation>
         <annotation name="mc" tierref="mc-UoPP">
            <ta e="T118" id="Seg_3457" s="T117">n-n:case</ta>
            <ta e="T119" id="Seg_3458" s="T118">v.[v:mood.pn]</ta>
            <ta e="T121" id="Seg_3459" s="T119">adv</ta>
            <ta e="T135" id="Seg_3460" s="T133">pers.[pro:case]</ta>
            <ta e="T136" id="Seg_3461" s="T135">v</ta>
            <ta e="T137" id="Seg_3462" s="T136">ptcl</ta>
            <ta e="T166" id="Seg_3463" s="T165">interj</ta>
            <ta e="T174" id="Seg_3464" s="T173">n-n:poss-n:case</ta>
            <ta e="T175" id="Seg_3465" s="T174">post</ta>
            <ta e="T176" id="Seg_3466" s="T175">adj.[n:case]</ta>
            <ta e="T177" id="Seg_3467" s="T176">ptcl-ptcl&gt;adv</ta>
            <ta e="T178" id="Seg_3468" s="T177">v-v:ptcp</ta>
            <ta e="T179" id="Seg_3469" s="T178">v-v:tense-v:pred.pn</ta>
            <ta e="T180" id="Seg_3470" s="T179">n-n:(poss).[n:case]</ta>
            <ta e="T181" id="Seg_3471" s="T180">n.[n:case]</ta>
            <ta e="T182" id="Seg_3472" s="T181">n.[n:case]</ta>
            <ta e="T183" id="Seg_3473" s="T182">n-n&gt;adj.[n:case]</ta>
            <ta e="T184" id="Seg_3474" s="T183">v-v:tense-v:pred.pn</ta>
            <ta e="T186" id="Seg_3475" s="T185">adv</ta>
            <ta e="T187" id="Seg_3476" s="T186">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T188" id="Seg_3477" s="T187">n.[n:case]</ta>
            <ta e="T189" id="Seg_3478" s="T188">v-v:ptcp</ta>
            <ta e="T190" id="Seg_3479" s="T189">v-v:tense-v:poss.pn</ta>
            <ta e="T192" id="Seg_3480" s="T191">n-n:(num)-n:poss-n:case</ta>
            <ta e="T193" id="Seg_3481" s="T192">post</ta>
            <ta e="T194" id="Seg_3482" s="T193">n-n&gt;n.[n:case]</ta>
            <ta e="T195" id="Seg_3483" s="T194">v-v:tense.[v:pred.pn]</ta>
            <ta e="T196" id="Seg_3484" s="T195">adv</ta>
            <ta e="T197" id="Seg_3485" s="T196">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T198" id="Seg_3486" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_3487" s="T198">pers.[pro:case]</ta>
            <ta e="T200" id="Seg_3488" s="T199">n-n:poss-n:case</ta>
            <ta e="T201" id="Seg_3489" s="T200">v-v:tense-v:pred.pn</ta>
            <ta e="T202" id="Seg_3490" s="T201">dempro.[pro:case]</ta>
            <ta e="T203" id="Seg_3491" s="T202">post</ta>
            <ta e="T204" id="Seg_3492" s="T203">v-v:tense.[v:pred.pn]</ta>
            <ta e="T205" id="Seg_3493" s="T204">dempro.[pro:case]</ta>
            <ta e="T206" id="Seg_3494" s="T205">post</ta>
            <ta e="T207" id="Seg_3495" s="T206">v-v:tense.[v:pred.pn]</ta>
            <ta e="T209" id="Seg_3496" s="T208">n.[n:case]</ta>
            <ta e="T210" id="Seg_3497" s="T209">n-n:(num).[n:case]</ta>
            <ta e="T211" id="Seg_3498" s="T210">adj</ta>
            <ta e="T212" id="Seg_3499" s="T211">n.[n:case]</ta>
            <ta e="T213" id="Seg_3500" s="T212">n-n:(num)-n:case</ta>
            <ta e="T214" id="Seg_3501" s="T213">post</ta>
            <ta e="T215" id="Seg_3502" s="T214">v-v:tense.[v:pred.pn]</ta>
            <ta e="T217" id="Seg_3503" s="T216">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T218" id="Seg_3504" s="T217">ptcl</ta>
            <ta e="T219" id="Seg_3505" s="T218">v-v:cvb</ta>
            <ta e="T220" id="Seg_3506" s="T219">ptcl</ta>
            <ta e="T221" id="Seg_3507" s="T220">pers-pro:case</ta>
            <ta e="T222" id="Seg_3508" s="T221">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T223" id="Seg_3509" s="T222">v-v:tense-v:poss.pn</ta>
            <ta e="T225" id="Seg_3510" s="T224">v-v:tense-v:poss.pn</ta>
            <ta e="T227" id="Seg_3511" s="T226">n-n:(num).[n:case]</ta>
            <ta e="T228" id="Seg_3512" s="T227">n-n:case</ta>
            <ta e="T229" id="Seg_3513" s="T228">v-v:tense-v:poss.pn</ta>
            <ta e="T231" id="Seg_3514" s="T230">adv</ta>
            <ta e="T232" id="Seg_3515" s="T231">n-n:case</ta>
            <ta e="T233" id="Seg_3516" s="T232">v-v:tense-v:pred.pn</ta>
            <ta e="T235" id="Seg_3517" s="T234">v-v:tense-v:poss.pn</ta>
            <ta e="T237" id="Seg_3518" s="T236">interj</ta>
            <ta e="T238" id="Seg_3519" s="T237">n</ta>
            <ta e="T239" id="Seg_3520" s="T238">adv</ta>
            <ta e="T240" id="Seg_3521" s="T239">ptcl</ta>
            <ta e="T242" id="Seg_3522" s="T240">adv-adv&gt;adv</ta>
            <ta e="T244" id="Seg_3523" s="T242">v-v:cvb</ta>
            <ta e="T246" id="Seg_3524" s="T244">v-v:cvb</ta>
            <ta e="T248" id="Seg_3525" s="T246">adv</ta>
            <ta e="T250" id="Seg_3526" s="T248">v-v:cvb</ta>
            <ta e="T253" id="Seg_3527" s="T252">v-v:cvb</ta>
            <ta e="T254" id="Seg_3528" s="T253">v-v:mood.[v:pred.pn]</ta>
            <ta e="T255" id="Seg_3529" s="T254">ptcl</ta>
            <ta e="T257" id="Seg_3530" s="T256">v-v:cvb</ta>
            <ta e="T259" id="Seg_3531" s="T257">n-n:poss-n:case</ta>
            <ta e="T260" id="Seg_3532" s="T259">v-v:tense-v:poss.pn</ta>
            <ta e="T263" id="Seg_3533" s="T260">v-v:tense.[v:pred.pn]</ta>
            <ta e="T264" id="Seg_3534" s="T263">ptcl</ta>
            <ta e="T266" id="Seg_3535" s="T264">adj</ta>
            <ta e="T268" id="Seg_3536" s="T266">v-v:tense-v:pred.pn</ta>
            <ta e="T270" id="Seg_3537" s="T268">ptcl</ta>
            <ta e="T283" id="Seg_3538" s="T282">n-n:poss-n:case</ta>
            <ta e="T284" id="Seg_3539" s="T283">v-v:tense-v:poss.pn</ta>
            <ta e="T286" id="Seg_3540" s="T285">adj.[n:case]</ta>
            <ta e="T287" id="Seg_3541" s="T286">ptcl</ta>
            <ta e="T288" id="Seg_3542" s="T287">n-n&gt;adj.[n:case]</ta>
            <ta e="T289" id="Seg_3543" s="T288">v-v:tense-v:poss.pn</ta>
            <ta e="T290" id="Seg_3544" s="T289">adv</ta>
            <ta e="T299" id="Seg_3545" s="T294">adv</ta>
            <ta e="T307" id="Seg_3546" s="T306">pers.[pro:case]</ta>
            <ta e="T308" id="Seg_3547" s="T307">adj</ta>
            <ta e="T309" id="Seg_3548" s="T308">v-v:mood.[v:pred.pn]</ta>
            <ta e="T310" id="Seg_3549" s="T309">que-que&gt;v-v:ptcp</ta>
            <ta e="T311" id="Seg_3550" s="T310">v-v:tense-v:poss.pn</ta>
            <ta e="T312" id="Seg_3551" s="T311">v-v:ptcp</ta>
            <ta e="T313" id="Seg_3552" s="T312">v-v:tense-v:poss.pn</ta>
            <ta e="T315" id="Seg_3553" s="T313">adv</ta>
            <ta e="T318" id="Seg_3554" s="T316">adv</ta>
            <ta e="T319" id="Seg_3555" s="T318">interj</ta>
            <ta e="T321" id="Seg_3556" s="T319">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T322" id="Seg_3557" s="T321">n-n:poss-n:case</ta>
            <ta e="T323" id="Seg_3558" s="T322">post</ta>
            <ta e="T324" id="Seg_3559" s="T323">n-n:(num).[n:case]</ta>
            <ta e="T325" id="Seg_3560" s="T324">n-n:poss-n:case</ta>
            <ta e="T326" id="Seg_3561" s="T325">post</ta>
            <ta e="T328" id="Seg_3562" s="T327">adv</ta>
            <ta e="T329" id="Seg_3563" s="T328">adj.[n:case]</ta>
            <ta e="T330" id="Seg_3564" s="T329">ptcl-ptcl&gt;adv</ta>
            <ta e="T331" id="Seg_3565" s="T330">v-v:tense-v:pred.pn</ta>
            <ta e="T332" id="Seg_3566" s="T331">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T333" id="Seg_3567" s="T332">n-n:poss-n:case</ta>
            <ta e="T334" id="Seg_3568" s="T333">v-v:tense-v:poss.pn</ta>
            <ta e="T335" id="Seg_3569" s="T334">n-n:poss-n:case</ta>
            <ta e="T336" id="Seg_3570" s="T335">post</ta>
            <ta e="T338" id="Seg_3571" s="T337">adv</ta>
            <ta e="T339" id="Seg_3572" s="T338">v-v:tense.[v:pred.pn]</ta>
            <ta e="T340" id="Seg_3573" s="T339">n-n:poss-n:case</ta>
            <ta e="T341" id="Seg_3574" s="T340">v-v&gt;v-v:mood.[v:pred.pn]</ta>
            <ta e="T342" id="Seg_3575" s="T341">n-n:poss-n:case</ta>
            <ta e="T343" id="Seg_3576" s="T342">v-v:mood.[v:pred.pn]</ta>
            <ta e="T344" id="Seg_3577" s="T343">adj.[n:case]</ta>
            <ta e="T345" id="Seg_3578" s="T344">ptcl-ptcl&gt;adv</ta>
            <ta e="T346" id="Seg_3579" s="T345">v-v:tense-v:pred.pn</ta>
            <ta e="T347" id="Seg_3580" s="T346">adv</ta>
            <ta e="T368" id="Seg_3581" s="T366">adj-n:(poss).[n:case]</ta>
            <ta e="T371" id="Seg_3582" s="T368">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps-UoPP">
            <ta e="T118" id="Seg_3583" s="T117">n</ta>
            <ta e="T119" id="Seg_3584" s="T118">v</ta>
            <ta e="T121" id="Seg_3585" s="T119">adv</ta>
            <ta e="T135" id="Seg_3586" s="T133">pers</ta>
            <ta e="T136" id="Seg_3587" s="T135">v</ta>
            <ta e="T137" id="Seg_3588" s="T136">ptcl</ta>
            <ta e="T166" id="Seg_3589" s="T165">interj</ta>
            <ta e="T174" id="Seg_3590" s="T173">n</ta>
            <ta e="T175" id="Seg_3591" s="T174">post</ta>
            <ta e="T176" id="Seg_3592" s="T175">adj</ta>
            <ta e="T177" id="Seg_3593" s="T176">adv</ta>
            <ta e="T178" id="Seg_3594" s="T177">v</ta>
            <ta e="T179" id="Seg_3595" s="T178">aux</ta>
            <ta e="T180" id="Seg_3596" s="T179">n</ta>
            <ta e="T181" id="Seg_3597" s="T180">n</ta>
            <ta e="T182" id="Seg_3598" s="T181">n</ta>
            <ta e="T183" id="Seg_3599" s="T182">adj</ta>
            <ta e="T184" id="Seg_3600" s="T183">cop</ta>
            <ta e="T186" id="Seg_3601" s="T185">adv</ta>
            <ta e="T187" id="Seg_3602" s="T186">n</ta>
            <ta e="T188" id="Seg_3603" s="T187">n</ta>
            <ta e="T189" id="Seg_3604" s="T188">v</ta>
            <ta e="T190" id="Seg_3605" s="T189">aux</ta>
            <ta e="T192" id="Seg_3606" s="T191">n</ta>
            <ta e="T193" id="Seg_3607" s="T192">post</ta>
            <ta e="T194" id="Seg_3608" s="T193">n</ta>
            <ta e="T195" id="Seg_3609" s="T194">cop</ta>
            <ta e="T196" id="Seg_3610" s="T195">adv</ta>
            <ta e="T197" id="Seg_3611" s="T196">dempro</ta>
            <ta e="T198" id="Seg_3612" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_3613" s="T198">pers</ta>
            <ta e="T200" id="Seg_3614" s="T199">n</ta>
            <ta e="T201" id="Seg_3615" s="T200">v</ta>
            <ta e="T202" id="Seg_3616" s="T201">dempro</ta>
            <ta e="T203" id="Seg_3617" s="T202">post</ta>
            <ta e="T204" id="Seg_3618" s="T203">v</ta>
            <ta e="T205" id="Seg_3619" s="T204">dempro</ta>
            <ta e="T206" id="Seg_3620" s="T205">post</ta>
            <ta e="T207" id="Seg_3621" s="T206">v</ta>
            <ta e="T209" id="Seg_3622" s="T208">n</ta>
            <ta e="T210" id="Seg_3623" s="T209">n</ta>
            <ta e="T211" id="Seg_3624" s="T210">adj</ta>
            <ta e="T212" id="Seg_3625" s="T211">n</ta>
            <ta e="T213" id="Seg_3626" s="T212">n</ta>
            <ta e="T214" id="Seg_3627" s="T213">post</ta>
            <ta e="T215" id="Seg_3628" s="T214">v</ta>
            <ta e="T217" id="Seg_3629" s="T216">dempro</ta>
            <ta e="T218" id="Seg_3630" s="T217">ptcl</ta>
            <ta e="T219" id="Seg_3631" s="T218">v</ta>
            <ta e="T220" id="Seg_3632" s="T219">ptcl</ta>
            <ta e="T221" id="Seg_3633" s="T220">pers</ta>
            <ta e="T222" id="Seg_3634" s="T221">v</ta>
            <ta e="T223" id="Seg_3635" s="T222">v</ta>
            <ta e="T225" id="Seg_3636" s="T224">v</ta>
            <ta e="T227" id="Seg_3637" s="T226">n</ta>
            <ta e="T228" id="Seg_3638" s="T227">n</ta>
            <ta e="T229" id="Seg_3639" s="T228">v</ta>
            <ta e="T231" id="Seg_3640" s="T230">adv</ta>
            <ta e="T232" id="Seg_3641" s="T231">n</ta>
            <ta e="T233" id="Seg_3642" s="T232">v</ta>
            <ta e="T235" id="Seg_3643" s="T234">v</ta>
            <ta e="T237" id="Seg_3644" s="T236">interj</ta>
            <ta e="T238" id="Seg_3645" s="T237">n</ta>
            <ta e="T239" id="Seg_3646" s="T238">adv</ta>
            <ta e="T240" id="Seg_3647" s="T239">ptcl</ta>
            <ta e="T242" id="Seg_3648" s="T240">adv</ta>
            <ta e="T244" id="Seg_3649" s="T242">v</ta>
            <ta e="T246" id="Seg_3650" s="T244">v</ta>
            <ta e="T248" id="Seg_3651" s="T246">adv</ta>
            <ta e="T250" id="Seg_3652" s="T248">v</ta>
            <ta e="T253" id="Seg_3653" s="T252">v</ta>
            <ta e="T254" id="Seg_3654" s="T253">aux</ta>
            <ta e="T255" id="Seg_3655" s="T254">ptcl</ta>
            <ta e="T257" id="Seg_3656" s="T256">v</ta>
            <ta e="T259" id="Seg_3657" s="T257">n</ta>
            <ta e="T260" id="Seg_3658" s="T259">v</ta>
            <ta e="T263" id="Seg_3659" s="T260">v</ta>
            <ta e="T264" id="Seg_3660" s="T263">ptcl</ta>
            <ta e="T266" id="Seg_3661" s="T264">adv</ta>
            <ta e="T268" id="Seg_3662" s="T266">v</ta>
            <ta e="T270" id="Seg_3663" s="T268">ptcl</ta>
            <ta e="T283" id="Seg_3664" s="T282">n</ta>
            <ta e="T284" id="Seg_3665" s="T283">v</ta>
            <ta e="T286" id="Seg_3666" s="T285">adj</ta>
            <ta e="T287" id="Seg_3667" s="T286">ptcl</ta>
            <ta e="T288" id="Seg_3668" s="T287">adj</ta>
            <ta e="T289" id="Seg_3669" s="T288">v</ta>
            <ta e="T290" id="Seg_3670" s="T289">adv</ta>
            <ta e="T299" id="Seg_3671" s="T294">adv</ta>
            <ta e="T307" id="Seg_3672" s="T306">pers</ta>
            <ta e="T308" id="Seg_3673" s="T307">adj</ta>
            <ta e="T309" id="Seg_3674" s="T308">cop</ta>
            <ta e="T310" id="Seg_3675" s="T309">v</ta>
            <ta e="T311" id="Seg_3676" s="T310">aux</ta>
            <ta e="T312" id="Seg_3677" s="T311">v</ta>
            <ta e="T313" id="Seg_3678" s="T312">aux</ta>
            <ta e="T315" id="Seg_3679" s="T313">adv</ta>
            <ta e="T318" id="Seg_3680" s="T316">adv</ta>
            <ta e="T319" id="Seg_3681" s="T318">interj</ta>
            <ta e="T321" id="Seg_3682" s="T319">v</ta>
            <ta e="T322" id="Seg_3683" s="T321">n</ta>
            <ta e="T323" id="Seg_3684" s="T322">post</ta>
            <ta e="T324" id="Seg_3685" s="T323">n</ta>
            <ta e="T325" id="Seg_3686" s="T324">n</ta>
            <ta e="T326" id="Seg_3687" s="T325">post</ta>
            <ta e="T328" id="Seg_3688" s="T327">adv</ta>
            <ta e="T329" id="Seg_3689" s="T328">adj</ta>
            <ta e="T330" id="Seg_3690" s="T329">adv</ta>
            <ta e="T331" id="Seg_3691" s="T330">v</ta>
            <ta e="T332" id="Seg_3692" s="T331">n</ta>
            <ta e="T333" id="Seg_3693" s="T332">n</ta>
            <ta e="T334" id="Seg_3694" s="T333">v</ta>
            <ta e="T335" id="Seg_3695" s="T334">n</ta>
            <ta e="T336" id="Seg_3696" s="T335">post</ta>
            <ta e="T338" id="Seg_3697" s="T337">adv</ta>
            <ta e="T339" id="Seg_3698" s="T338">v</ta>
            <ta e="T340" id="Seg_3699" s="T339">n</ta>
            <ta e="T341" id="Seg_3700" s="T340">v</ta>
            <ta e="T342" id="Seg_3701" s="T341">n</ta>
            <ta e="T343" id="Seg_3702" s="T342">v</ta>
            <ta e="T344" id="Seg_3703" s="T343">adj</ta>
            <ta e="T345" id="Seg_3704" s="T344">adv</ta>
            <ta e="T346" id="Seg_3705" s="T345">v</ta>
            <ta e="T347" id="Seg_3706" s="T346">adv</ta>
            <ta e="T368" id="Seg_3707" s="T366">adj</ta>
            <ta e="T371" id="Seg_3708" s="T368">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-UoPP" />
         <annotation name="SyF" tierref="SyF-UoPP" />
         <annotation name="IST" tierref="IST-UoPP" />
         <annotation name="Top" tierref="Top-UoPP" />
         <annotation name="Foc" tierref="Foc-UoPP" />
         <annotation name="BOR" tierref="BOR-UoPP">
            <ta e="T192" id="Seg_3709" s="T191">EV:core</ta>
            <ta e="T227" id="Seg_3710" s="T226">RUS:cult</ta>
            <ta e="T228" id="Seg_3711" s="T227">RUS:cult</ta>
            <ta e="T299" id="Seg_3712" s="T294">RUS:core</ta>
            <ta e="T338" id="Seg_3713" s="T337">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-UoPP">
            <ta e="T227" id="Seg_3714" s="T226">Vsub</ta>
            <ta e="T299" id="Seg_3715" s="T294">fortition Vsub</ta>
            <ta e="T338" id="Seg_3716" s="T337">fortition Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph-UoPP">
            <ta e="T192" id="Seg_3717" s="T191">dir:infl</ta>
            <ta e="T227" id="Seg_3718" s="T226">dir:infl</ta>
            <ta e="T228" id="Seg_3719" s="T227">dir:infl</ta>
            <ta e="T299" id="Seg_3720" s="T294">dir:bare</ta>
            <ta e="T338" id="Seg_3721" s="T337">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS-UoPP">
            <ta e="T65" id="Seg_3722" s="T60">RUS:ext</ta>
            <ta e="T75" id="Seg_3723" s="T70">RUS:ext</ta>
            <ta e="T88" id="Seg_3724" s="T84">RUS:ext</ta>
            <ta e="T147" id="Seg_3725" s="T142">RUS:ext</ta>
            <ta e="T154" id="Seg_3726" s="T148">RUS:ext</ta>
            <ta e="T165" id="Seg_3727" s="T161">RUS:ext</ta>
            <ta e="T169" id="Seg_3728" s="T165">RUS:ext</ta>
            <ta e="T279" id="Seg_3729" s="T275">RUS:ext</ta>
            <ta e="T354" id="Seg_3730" s="T348">RUS:ext</ta>
            <ta e="T360" id="Seg_3731" s="T359">RUS:ext</ta>
            <ta e="T363" id="Seg_3732" s="T361">RUS:ext</ta>
         </annotation>
         <annotation name="fe" tierref="fe-UoPP">
            <ta e="T65" id="Seg_3733" s="T60">– The easiest is to choose the child.</ta>
            <ta e="T75" id="Seg_3734" s="T70">– We need only one, right?</ta>
            <ta e="T88" id="Seg_3735" s="T84">– I'll help.</ta>
            <ta e="T121" id="Seg_3736" s="T117">– Then choose the woman.</ta>
            <ta e="T137" id="Seg_3737" s="T133">– I (tell) then.</ta>
            <ta e="T147" id="Seg_3738" s="T142">– About the woman is right, yes?</ta>
            <ta e="T154" id="Seg_3739" s="T148">– Well, come on.</ta>
            <ta e="T165" id="Seg_3740" s="T161">– That's me, right, ah.</ta>
            <ta e="T169" id="Seg_3741" s="T165">Oj, wait.</ta>
            <ta e="T184" id="Seg_3742" s="T173">– I was living very well with my husband, our child, we had a son.</ta>
            <ta e="T190" id="Seg_3743" s="T185">Then my husband started to drink alcohol.</ta>
            <ta e="T201" id="Seg_3744" s="T191">Together with his friends he became a drinker, then they told about me: </ta>
            <ta e="T207" id="Seg_3745" s="T201">"Like this and that she's going.</ta>
            <ta e="T215" id="Seg_3746" s="T208">Men, she is going with other men."</ta>
            <ta e="T223" id="Seg_3747" s="T216">He beat me out of jealousy, thrashed.</ta>
            <ta e="T225" id="Seg_3748" s="T224">He thrashed.</ta>
            <ta e="T229" id="Seg_3749" s="T226">Policemen took him to the police station.</ta>
            <ta e="T233" id="Seg_3750" s="T230">Then they put him in prison.</ta>
            <ta e="T235" id="Seg_3751" s="T234">They put him.</ta>
            <ta e="T250" id="Seg_3752" s="T236">Mhm, prison, he was sitting for a long time, then he came out…</ta>
            <ta e="T255" id="Seg_3753" s="T252">– Then he apparently came out…</ta>
            <ta e="T264" id="Seg_3754" s="T256">He came out and came home, he is very happy.</ta>
            <ta e="T270" id="Seg_3755" s="T264">– Am I telling wrongly?</ta>
            <ta e="T279" id="Seg_3756" s="T275">– Yes, this.</ta>
            <ta e="T284" id="Seg_3757" s="T282">– He came home.</ta>
            <ta e="T290" id="Seg_3758" s="T285">He came apparently with very good thoughts.</ta>
            <ta e="T299" id="Seg_3759" s="T294">– Happily.</ta>
            <ta e="T315" id="Seg_3760" s="T306">In the beginning I whatchamacallit, I was afraid, then.</ta>
            <ta e="T326" id="Seg_3761" s="T316">Then, eh, I met [him] together with my grandpa, (children), with our child. </ta>
            <ta e="T336" id="Seg_3762" s="T327">Then we were living very well, my husband left the alcohol, with the child.</ta>
            <ta e="T347" id="Seg_3763" s="T337">He always goes, takes the child for a walk, helps at home, we are living very well.</ta>
            <ta e="T354" id="Seg_3764" s="T348">Like that probably somehow…</ta>
            <ta e="T360" id="Seg_3765" s="T359">– That's all.</ta>
            <ta e="T363" id="Seg_3766" s="T361">– Aha.</ta>
            <ta e="T371" id="Seg_3767" s="T366">– That's it then.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-UoPP">
            <ta e="T65" id="Seg_3768" s="T60">– Am einfachsten, wenn man das Kind nimmt.</ta>
            <ta e="T75" id="Seg_3769" s="T70">– Einen brauchen wir nur, ja?</ta>
            <ta e="T88" id="Seg_3770" s="T84">– Ich helfe.</ta>
            <ta e="T121" id="Seg_3771" s="T117">– Dann nimm die Frau.</ta>
            <ta e="T137" id="Seg_3772" s="T133">– Ich (erzähl) also.</ta>
            <ta e="T147" id="Seg_3773" s="T142">– Über die Frau geht, ja?</ta>
            <ta e="T154" id="Seg_3774" s="T148">– Nun, los.</ta>
            <ta e="T165" id="Seg_3775" s="T161">– Das bin ich, ja, ah.</ta>
            <ta e="T169" id="Seg_3776" s="T165">Oj, warte.</ta>
            <ta e="T184" id="Seg_3777" s="T173">– Ich lebte sehr gut mit meinem Mann, unser Kind, wir hatten einen Sohn.</ta>
            <ta e="T190" id="Seg_3778" s="T185">Dann fing mein Mann an, Schnaps zu trinken.</ta>
            <ta e="T201" id="Seg_3779" s="T191">Mit seinen Freunden wurde er Alkoholiker, dann erzählten jene über mich: </ta>
            <ta e="T207" id="Seg_3780" s="T201">"So und so geht sie.</ta>
            <ta e="T215" id="Seg_3781" s="T208">Männer, sie geht mit anderen Männern."</ta>
            <ta e="T223" id="Seg_3782" s="T216">Er schlug mich aus Eifersucht, er prügelte.</ta>
            <ta e="T225" id="Seg_3783" s="T224">Er prügelte.</ta>
            <ta e="T229" id="Seg_3784" s="T226">Polizisten nahmen ihn mit auf die Wache.</ta>
            <ta e="T233" id="Seg_3785" s="T230">Dann setzten sie ihn ins Gefängnis.</ta>
            <ta e="T235" id="Seg_3786" s="T234">Steckten sie ihn.</ta>
            <ta e="T250" id="Seg_3787" s="T236">Mhm, Gefängnis, er saß sehr lange, dann kam er heraus…</ta>
            <ta e="T255" id="Seg_3788" s="T252">– Er kam dann wohl raus…</ta>
            <ta e="T264" id="Seg_3789" s="T256">Er kam heraus und kam nach Hause, er freut sich sehr.</ta>
            <ta e="T270" id="Seg_3790" s="T264">– Erzähle ich falsch?</ta>
            <ta e="T279" id="Seg_3791" s="T275">– Ja das.</ta>
            <ta e="T284" id="Seg_3792" s="T282">– Er kam nach Hause.</ta>
            <ta e="T290" id="Seg_3793" s="T285">Er kam offenbar mit sehr guten Gedanken.</ta>
            <ta e="T299" id="Seg_3794" s="T294">– Fröhlich.</ta>
            <ta e="T315" id="Seg_3795" s="T306">Ich hatte zuerst gedingst, Angst gehabt, dann.</ta>
            <ta e="T326" id="Seg_3796" s="T316">Dann, äh, empfing ich [ihn] mit meinem Opa, (Kinder), mit unserem Kind.</ta>
            <ta e="T336" id="Seg_3797" s="T327">Dann lebten wir sehr gut, mein Mann hat den Schnaps sein gelassen, mit dem Kind.</ta>
            <ta e="T347" id="Seg_3798" s="T337">Er geht immer, führt das Kind spazieren, hilft zuhause, wir leben jetzt sehr gut.</ta>
            <ta e="T354" id="Seg_3799" s="T348">So wahrscheinlich irgendwie…</ta>
            <ta e="T360" id="Seg_3800" s="T359">– Alles.</ta>
            <ta e="T363" id="Seg_3801" s="T361">– Aha.</ta>
            <ta e="T371" id="Seg_3802" s="T366">– Das war's.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-UoPP" />
         <annotation name="ltr" tierref="ltr-UoPP">
            <ta e="T121" id="Seg_3803" s="T117">Женщину возьми тогда.</ta>
            <ta e="T137" id="Seg_3804" s="T133">Я (рассказ-) да.</ta>
            <ta e="T184" id="Seg_3805" s="T173">С мужем очень хорошо мы жили, ребенок сын у нас был.</ta>
            <ta e="T190" id="Seg_3806" s="T185">Потом, муж стал выпивать.</ta>
            <ta e="T201" id="Seg_3807" s="T191">С друзьями он стал пьяницей, потом те про меня рассказали </ta>
            <ta e="T207" id="Seg_3808" s="T201">"так она гуляет, сяк она гуляет".</ta>
            <ta e="T215" id="Seg_3809" s="T208">"Мужчины, с другими мужчинами гуляет."</ta>
            <ta e="T223" id="Seg_3810" s="T216">Ну этот от ревности ударил, избил.</ta>
            <ta e="T225" id="Seg_3811" s="T224">Избил.</ta>
            <ta e="T229" id="Seg_3812" s="T226">Милиционеры в милицию забрали.</ta>
            <ta e="T233" id="Seg_3813" s="T230">Потом посадили в тюрьму.</ta>
            <ta e="T235" id="Seg_3814" s="T234">Посадили.</ta>
            <ta e="T250" id="Seg_3815" s="T236">Мм, в тюрьме долго долго сидел, потом вышел…</ta>
            <ta e="T255" id="Seg_3816" s="T252">Выдя…</ta>
            <ta e="T264" id="Seg_3817" s="T256">Выдя домой пришел очень радостный.</ta>
            <ta e="T270" id="Seg_3818" s="T264">Неправильно рассказываю да?</ta>
            <ta e="T284" id="Seg_3819" s="T282">Домой пришел.</ta>
            <ta e="T290" id="Seg_3820" s="T285">С очень хорошими мыслями пришел, кажется.</ta>
            <ta e="T299" id="Seg_3821" s="T294">Весело.</ta>
            <ta e="T315" id="Seg_3822" s="T306">Я сначала беспокоилась, потом…</ta>
            <ta e="T326" id="Seg_3823" s="T316">Потом встретили с дедушкой и с ребенком.</ta>
            <ta e="T336" id="Seg_3824" s="T327">Потом очень хорошо жили, муж пить бросил, с ребенком.</ta>
            <ta e="T347" id="Seg_3825" s="T337">все время гуляет, ребенка прогуливает, дома помогает, очень хорошо с ним живем.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-UoPP">
            <ta e="T326" id="Seg_3826" s="T316">[DCh]: Probably the speaker doesn't have the grandpa of the women in mind, but her father. </ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-DCh"
                          name="ref"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-DCh"
                          name="st"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-DCh"
                          name="ts"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-DCh"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-DCh"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-DCh"
                          name="mb"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-DCh"
                          name="mp"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-DCh"
                          name="ge"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-DCh"
                          name="gg"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-DCh"
                          name="gr"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-DCh"
                          name="mc"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-DCh"
                          name="ps"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-DCh"
                          name="SeR"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-DCh"
                          name="SyF"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-DCh"
                          name="IST"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-DCh"
                          name="Top"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-DCh"
                          name="Foc"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-DCh"
                          name="BOR"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-DCh"
                          name="BOR-Phon"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-DCh"
                          name="BOR-Morph"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-DCh"
                          name="CS"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-DCh"
                          name="fe"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-DCh"
                          name="fg"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-DCh"
                          name="fr"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-DCh"
                          name="ltr"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-DCh"
                          name="nt"
                          segmented-tier-id="tx-DCh"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-ChGS"
                          name="ref"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-ChGS"
                          name="st"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-ChGS"
                          name="ts"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-ChGS"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-ChGS"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-ChGS"
                          name="mb"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-ChGS"
                          name="mp"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-ChGS"
                          name="ge"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-ChGS"
                          name="gg"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-ChGS"
                          name="gr"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-ChGS"
                          name="mc"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-ChGS"
                          name="ps"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-ChGS"
                          name="SeR"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-ChGS"
                          name="SyF"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-ChGS"
                          name="IST"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-ChGS"
                          name="Top"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-ChGS"
                          name="Foc"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-ChGS"
                          name="BOR"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-ChGS"
                          name="BOR-Phon"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-ChGS"
                          name="BOR-Morph"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-ChGS"
                          name="CS"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-ChGS"
                          name="fe"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-ChGS"
                          name="fg"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-ChGS"
                          name="fr"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-ChGS"
                          name="ltr"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-ChGS"
                          name="nt"
                          segmented-tier-id="tx-ChGS"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-UoPP"
                          name="ref"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-UoPP"
                          name="st"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-UoPP"
                          name="ts"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-UoPP"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-UoPP"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-UoPP"
                          name="mb"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-UoPP"
                          name="mp"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-UoPP"
                          name="ge"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-UoPP"
                          name="gg"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-UoPP"
                          name="gr"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-UoPP"
                          name="mc"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-UoPP"
                          name="ps"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-UoPP"
                          name="SeR"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-UoPP"
                          name="SyF"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-UoPP"
                          name="IST"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-UoPP"
                          name="Top"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-UoPP"
                          name="Foc"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-UoPP"
                          name="BOR"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-UoPP"
                          name="BOR-Phon"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-UoPP"
                          name="BOR-Morph"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-UoPP"
                          name="CS"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-UoPP"
                          name="fe"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-UoPP"
                          name="fg"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-UoPP"
                          name="fr"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-UoPP"
                          name="ltr"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-UoPP"
                          name="nt"
                          segmented-tier-id="tx-UoPP"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
