<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID881F8C8C-E99E-3C7C-B923-59BE73E18CC9">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>KiPP_2009_Syndassko_nar</transcription-name>
         <referenced-file url="KiPP_2009_Syndassko_nar.wav" />
         <referenced-file url="KiPP_2009_Syndassko_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\KiPP_2009_Syndassko_nar\KiPP_2009_Syndassko_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">916</ud-information>
            <ud-information attribute-name="# HIAT:w">695</ud-information>
            <ud-information attribute-name="# e">696</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">92</ud-information>
            <ud-information attribute-name="# sc">176</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KiPP">
            <abbreviation>KiPP</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.74" type="appl" />
         <tli id="T2" time="1.3033333333333332" type="appl" />
         <tli id="T3" time="1.8666666666666665" type="appl" />
         <tli id="T4" time="2.4299999999999997" type="appl" />
         <tli id="T5" time="2.993333333333333" type="appl" />
         <tli id="T6" time="3.5566666666666666" type="appl" />
         <tli id="T7" time="4.119999999999999" type="appl" />
         <tli id="T8" time="4.683333333333333" type="appl" />
         <tli id="T9" time="5.246666666666666" type="appl" />
         <tli id="T10" time="5.81" type="appl" />
         <tli id="T11" time="5.92" type="appl" />
         <tli id="T12" time="6.542" type="appl" />
         <tli id="T13" time="7.164" type="appl" />
         <tli id="T14" time="7.786" type="appl" />
         <tli id="T15" time="8.408" type="appl" />
         <tli id="T16" time="9.03" type="appl" />
         <tli id="T17" time="9.21" type="appl" />
         <tli id="T18" time="10.22" type="appl" />
         <tli id="T19" time="11.23" type="appl" />
         <tli id="T20" time="11.82" type="appl" />
         <tli id="T21" time="12.421818181818182" type="appl" />
         <tli id="T22" time="13.023636363636363" type="appl" />
         <tli id="T23" time="13.625454545454545" type="appl" />
         <tli id="T24" time="14.227272727272728" type="appl" />
         <tli id="T25" time="14.82909090909091" type="appl" />
         <tli id="T26" time="15.430909090909092" type="appl" />
         <tli id="T27" time="16.032727272727275" type="appl" />
         <tli id="T28" time="16.634545454545457" type="appl" />
         <tli id="T29" time="17.236363636363638" type="appl" />
         <tli id="T30" time="17.83818181818182" type="appl" />
         <tli id="T31" time="18.44" type="appl" />
         <tli id="T32" time="18.87" type="appl" />
         <tli id="T33" time="19.214285714285715" type="appl" />
         <tli id="T34" time="19.55857142857143" type="appl" />
         <tli id="T35" time="19.902857142857144" type="appl" />
         <tli id="T36" time="20.247142857142858" type="appl" />
         <tli id="T37" time="20.591428571428573" type="appl" />
         <tli id="T38" time="20.935714285714287" type="appl" />
         <tli id="T39" time="21.28" type="appl" />
         <tli id="T40" time="21.624285714285715" type="appl" />
         <tli id="T41" time="21.96857142857143" type="appl" />
         <tli id="T42" time="22.312857142857144" type="appl" />
         <tli id="T43" time="22.65714285714286" type="appl" />
         <tli id="T44" time="23.001428571428573" type="appl" />
         <tli id="T45" time="23.345714285714287" type="appl" />
         <tli id="T46" time="23.69" type="appl" />
         <tli id="T47" time="23.75" type="appl" />
         <tli id="T48" time="24.445" type="appl" />
         <tli id="T49" time="25.14" type="appl" />
         <tli id="T50" time="25.26" type="appl" />
         <tli id="T51" time="25.907500000000002" type="appl" />
         <tli id="T52" time="26.555" type="appl" />
         <tli id="T53" time="27.2025" type="appl" />
         <tli id="T54" time="27.85" type="appl" />
         <tli id="T55" time="27.95" type="appl" />
         <tli id="T56" time="28.619999999999997" type="appl" />
         <tli id="T57" time="29.29" type="appl" />
         <tli id="T58" time="29.96" type="appl" />
         <tli id="T59" time="30.63" type="appl" />
         <tli id="T60" time="30.87" type="appl" />
         <tli id="T61" time="31.42" type="appl" />
         <tli id="T62" time="31.970000000000002" type="appl" />
         <tli id="T63" time="32.52" type="appl" />
         <tli id="T64" time="33.07" type="appl" />
         <tli id="T65" time="33.620000000000005" type="appl" />
         <tli id="T66" time="34.17" type="appl" />
         <tli id="T67" time="34.38" type="appl" />
         <tli id="T68" time="35.29666666666667" type="appl" />
         <tli id="T69" time="36.21333333333334" type="appl" />
         <tli id="T70" time="37.13" type="appl" />
         <tli id="T71" time="37.26" type="appl" />
         <tli id="T72" time="37.629285714285714" type="appl" />
         <tli id="T73" time="37.998571428571424" type="appl" />
         <tli id="T74" time="38.36785714285714" type="appl" />
         <tli id="T75" time="38.73714285714286" type="appl" />
         <tli id="T76" time="39.10642857142857" type="appl" />
         <tli id="T77" time="39.47571428571428" type="appl" />
         <tli id="T78" time="39.845" type="appl" />
         <tli id="T79" time="40.214285714285715" type="appl" />
         <tli id="T80" time="40.583571428571425" type="appl" />
         <tli id="T81" time="40.95285714285714" type="appl" />
         <tli id="T82" time="41.32214285714286" type="appl" />
         <tli id="T83" time="41.691428571428574" type="appl" />
         <tli id="T84" time="42.06071428571428" type="appl" />
         <tli id="T85" time="42.43" type="appl" />
         <tli id="T86" time="42.62" type="appl" />
         <tli id="T87" time="43.26" type="appl" />
         <tli id="T88" time="43.9" type="appl" />
         <tli id="T89" time="44.54" type="appl" />
         <tli id="T90" time="44.79" type="appl" />
         <tli id="T91" time="45.20125" type="appl" />
         <tli id="T92" time="45.6125" type="appl" />
         <tli id="T93" time="46.02375" type="appl" />
         <tli id="T94" time="46.435" type="appl" />
         <tli id="T95" time="46.84625" type="appl" />
         <tli id="T96" time="47.2575" type="appl" />
         <tli id="T97" time="47.668749999999996" type="appl" />
         <tli id="T98" time="48.08" type="appl" />
         <tli id="T99" time="48.1" type="appl" />
         <tli id="T100" time="48.444" type="appl" />
         <tli id="T101" time="48.788000000000004" type="appl" />
         <tli id="T102" time="49.132" type="appl" />
         <tli id="T103" time="49.476" type="appl" />
         <tli id="T104" time="49.82" type="appl" />
         <tli id="T105" time="49.86" type="appl" />
         <tli id="T106" time="50.45" type="appl" />
         <tli id="T107" time="51.04" type="appl" />
         <tli id="T108" time="51.23" type="appl" />
         <tli id="T109" time="51.73571428571428" type="appl" />
         <tli id="T110" time="52.24142857142857" type="appl" />
         <tli id="T111" time="52.747142857142855" type="appl" />
         <tli id="T112" time="53.252857142857145" type="appl" />
         <tli id="T113" time="53.75857142857143" type="appl" />
         <tli id="T114" time="54.26428571428572" type="appl" />
         <tli id="T115" time="54.77" type="appl" />
         <tli id="T116" time="55.41" type="appl" />
         <tli id="T117" time="55.93857142857143" type="appl" />
         <tli id="T118" time="56.46714285714285" type="appl" />
         <tli id="T119" time="56.995714285714286" type="appl" />
         <tli id="T120" time="57.52428571428571" type="appl" />
         <tli id="T121" time="58.05285714285714" type="appl" />
         <tli id="T122" time="58.581428571428575" type="appl" />
         <tli id="T123" time="59.11" type="appl" />
         <tli id="T124" time="59.24" type="appl" />
         <tli id="T125" time="59.72666666666667" type="appl" />
         <tli id="T126" time="60.21333333333333" type="appl" />
         <tli id="T127" time="60.7" type="appl" />
         <tli id="T128" time="61.18666666666667" type="appl" />
         <tli id="T129" time="61.67333333333333" type="appl" />
         <tli id="T130" time="62.16" type="appl" />
         <tli id="T131" time="62.42" type="appl" />
         <tli id="T132" time="62.864285714285714" type="appl" />
         <tli id="T133" time="63.30857142857143" type="appl" />
         <tli id="T134" time="63.752857142857145" type="appl" />
         <tli id="T135" time="64.19714285714286" type="appl" />
         <tli id="T136" time="64.64142857142858" type="appl" />
         <tli id="T137" time="65.08571428571429" type="appl" />
         <tli id="T138" time="65.53" type="appl" />
         <tli id="T139" time="65.75" type="appl" />
         <tli id="T140" time="66.3" type="appl" />
         <tli id="T141" time="66.85" type="appl" />
         <tli id="T142" time="67.4" type="appl" />
         <tli id="T143" time="67.95" type="appl" />
         <tli id="T144" time="68.5" type="appl" />
         <tli id="T145" time="68.62" type="appl" />
         <tli id="T146" time="69.0625" type="appl" />
         <tli id="T147" time="69.505" type="appl" />
         <tli id="T148" time="69.9475" type="appl" />
         <tli id="T149" time="70.39" type="appl" />
         <tli id="T150" time="70.8325" type="appl" />
         <tli id="T151" time="71.275" type="appl" />
         <tli id="T152" time="71.7175" type="appl" />
         <tli id="T153" time="72.16" type="appl" />
         <tli id="T154" time="72.289" type="appl" />
         <tli id="T155" time="73.056" type="appl" />
         <tli id="T156" time="73.82300000000001" type="appl" />
         <tli id="T157" time="74.59" type="appl" />
         <tli id="T158" time="74.72" type="appl" />
         <tli id="T159" time="75.13285714285715" type="appl" />
         <tli id="T160" time="75.54571428571428" type="appl" />
         <tli id="T161" time="75.95857142857143" type="appl" />
         <tli id="T162" time="76.37142857142857" type="appl" />
         <tli id="T163" time="76.78428571428572" type="appl" />
         <tli id="T164" time="77.19714285714285" type="appl" />
         <tli id="T165" time="77.61" type="appl" />
         <tli id="T166" time="77.86" type="appl" />
         <tli id="T167" time="78.87666666666667" type="appl" />
         <tli id="T168" time="79.89333333333333" type="appl" />
         <tli id="T169" time="80.91" type="appl" />
         <tli id="T170" time="81.36" type="appl" />
         <tli id="T171" time="81.76714285714286" type="appl" />
         <tli id="T172" time="82.17428571428572" type="appl" />
         <tli id="T173" time="82.58142857142857" type="appl" />
         <tli id="T174" time="82.98857142857143" type="appl" />
         <tli id="T175" time="83.39571428571429" type="appl" />
         <tli id="T176" time="83.80285714285715" type="appl" />
         <tli id="T177" time="84.21000000000001" type="appl" />
         <tli id="T178" time="84.61714285714285" type="appl" />
         <tli id="T179" time="85.02428571428571" type="appl" />
         <tli id="T180" time="85.43142857142857" type="appl" />
         <tli id="T181" time="85.83857142857143" type="appl" />
         <tli id="T182" time="86.24571428571429" type="appl" />
         <tli id="T183" time="86.65285714285714" type="appl" />
         <tli id="T184" time="87.06" type="appl" />
         <tli id="T185" time="87.51" type="appl" />
         <tli id="T186" time="88.33428571428571" type="appl" />
         <tli id="T187" time="89.15857142857143" type="appl" />
         <tli id="T188" time="89.98285714285714" type="appl" />
         <tli id="T189" time="90.80714285714286" type="appl" />
         <tli id="T190" time="91.63142857142857" type="appl" />
         <tli id="T191" time="92.4557142857143" type="appl" />
         <tli id="T192" time="93.28" type="appl" />
         <tli id="T193" time="93.55" type="appl" />
         <tli id="T194" time="94.76333333333334" type="appl" />
         <tli id="T195" time="95.97666666666666" type="appl" />
         <tli id="T196" time="97.19" type="appl" />
         <tli id="T197" time="97.37" type="appl" />
         <tli id="T198" time="98.00545454545455" type="appl" />
         <tli id="T199" time="98.64090909090909" type="appl" />
         <tli id="T200" time="99.27636363636364" type="appl" />
         <tli id="T201" time="99.91181818181819" type="appl" />
         <tli id="T202" time="100.54727272727273" type="appl" />
         <tli id="T203" time="101.18272727272728" type="appl" />
         <tli id="T204" time="101.81818181818181" type="appl" />
         <tli id="T205" time="102.45363636363636" type="appl" />
         <tli id="T206" time="103.08909090909091" type="appl" />
         <tli id="T207" time="103.72454545454545" type="appl" />
         <tli id="T208" time="104.36" type="appl" />
         <tli id="T209" time="104.57" type="appl" />
         <tli id="T210" time="105.12571428571428" type="appl" />
         <tli id="T211" time="105.68142857142857" type="appl" />
         <tli id="T212" time="106.23714285714286" type="appl" />
         <tli id="T213" time="106.79285714285713" type="appl" />
         <tli id="T214" time="107.34857142857142" type="appl" />
         <tli id="T215" time="107.9042857142857" type="appl" />
         <tli id="T216" time="108.46" type="appl" />
         <tli id="T217" time="109.01571428571428" type="appl" />
         <tli id="T218" time="109.57142857142857" type="appl" />
         <tli id="T219" time="110.12714285714286" type="appl" />
         <tli id="T220" time="110.68285714285713" type="appl" />
         <tli id="T221" time="111.23857142857142" type="appl" />
         <tli id="T222" time="111.7942857142857" type="appl" />
         <tli id="T223" time="112.35" type="appl" />
         <tli id="T224" time="112.58" type="appl" />
         <tli id="T225" time="113.19" type="appl" />
         <tli id="T226" time="113.8" type="appl" />
         <tli id="T227" time="114.41" type="appl" />
         <tli id="T228" time="115.87" type="appl" />
         <tli id="T229" time="116.516" type="appl" />
         <tli id="T230" time="117.162" type="appl" />
         <tli id="T231" time="117.808" type="appl" />
         <tli id="T232" time="118.45400000000001" type="appl" />
         <tli id="T233" time="119.1" type="appl" />
         <tli id="T234" time="119.746" type="appl" />
         <tli id="T235" time="120.392" type="appl" />
         <tli id="T236" time="121.038" type="appl" />
         <tli id="T237" time="121.684" type="appl" />
         <tli id="T238" time="122.33" type="appl" />
         <tli id="T239" time="122.71" type="appl" />
         <tli id="T240" time="123.09727272727272" type="appl" />
         <tli id="T241" time="123.48454545454545" type="appl" />
         <tli id="T242" time="123.87181818181817" type="appl" />
         <tli id="T243" time="124.2590909090909" type="appl" />
         <tli id="T244" time="124.64636363636363" type="appl" />
         <tli id="T245" time="125.03363636363636" type="appl" />
         <tli id="T246" time="125.42090909090909" type="appl" />
         <tli id="T247" time="125.80818181818182" type="appl" />
         <tli id="T248" time="126.19545454545454" type="appl" />
         <tli id="T249" time="126.58272727272727" type="appl" />
         <tli id="T250" time="126.97" type="appl" />
         <tli id="T251" time="127.04" type="appl" />
         <tli id="T252" time="127.62" type="appl" />
         <tli id="T253" time="128.2" type="appl" />
         <tli id="T254" time="128.78" type="appl" />
         <tli id="T255" time="129.36" type="appl" />
         <tli id="T256" time="129.94" type="appl" />
         <tli id="T257" time="130.039" type="appl" />
         <tli id="T258" time="130.53292307692305" type="appl" />
         <tli id="T259" time="131.02684615384615" type="appl" />
         <tli id="T260" time="131.52076923076922" type="appl" />
         <tli id="T261" time="132.01469230769231" type="appl" />
         <tli id="T262" time="132.50861538461538" type="appl" />
         <tli id="T263" time="133.00253846153845" type="appl" />
         <tli id="T264" time="133.49646153846155" type="appl" />
         <tli id="T265" time="133.9903846153846" type="appl" />
         <tli id="T266" time="134.48430769230768" type="appl" />
         <tli id="T267" time="134.97823076923078" type="appl" />
         <tli id="T268" time="135.47215384615384" type="appl" />
         <tli id="T269" time="135.96607692307694" type="appl" />
         <tli id="T270" time="136.46" type="appl" />
         <tli id="T271" time="136.61" type="appl" />
         <tli id="T272" time="137.35000000000002" type="appl" />
         <tli id="T273" time="138.09" type="appl" />
         <tli id="T274" time="138.83" type="appl" />
         <tli id="T275" time="139.57" type="appl" />
         <tli id="T276" time="140.31" type="appl" />
         <tli id="T277" time="141.04999999999998" type="appl" />
         <tli id="T278" time="141.79" type="appl" />
         <tli id="T279" time="142.02" type="appl" />
         <tli id="T280" time="143.17450000000002" type="appl" />
         <tli id="T281" time="144.329" type="appl" />
         <tli id="T282" time="144.49" type="appl" />
         <tli id="T283" time="145.15" type="appl" />
         <tli id="T284" time="145.81" type="appl" />
         <tli id="T285" time="146.47" type="appl" />
         <tli id="T286" time="147.13" type="appl" />
         <tli id="T287" time="147.79" type="appl" />
         <tli id="T288" time="148.01" type="appl" />
         <tli id="T289" time="148.952" type="appl" />
         <tli id="T290" time="149.894" type="appl" />
         <tli id="T291" time="150.83599999999998" type="appl" />
         <tli id="T292" time="151.778" type="appl" />
         <tli id="T293" time="152.72" type="appl" />
         <tli id="T294" time="152.86" type="appl" />
         <tli id="T295" time="153.33166666666668" type="appl" />
         <tli id="T296" time="153.80333333333334" type="appl" />
         <tli id="T297" time="154.275" type="appl" />
         <tli id="T298" time="154.74666666666667" type="appl" />
         <tli id="T299" time="155.21833333333333" type="appl" />
         <tli id="T300" time="155.69" type="appl" />
         <tli id="T301" time="156.1616666666667" type="appl" />
         <tli id="T302" time="156.63333333333335" type="appl" />
         <tli id="T303" time="157.10500000000002" type="appl" />
         <tli id="T304" time="157.57666666666668" type="appl" />
         <tli id="T305" time="158.04833333333335" type="appl" />
         <tli id="T306" time="158.52" type="appl" />
         <tli id="T307" time="159.63" type="appl" />
         <tli id="T308" time="160.09333333333333" type="appl" />
         <tli id="T309" time="160.55666666666667" type="appl" />
         <tli id="T310" time="161.01999999999998" type="appl" />
         <tli id="T311" time="161.48333333333332" type="appl" />
         <tli id="T312" time="161.94666666666666" type="appl" />
         <tli id="T313" time="162.41" type="appl" />
         <tli id="T314" time="162.579" type="appl" />
         <tli id="T315" time="163.0991111111111" type="appl" />
         <tli id="T316" time="163.61922222222222" type="appl" />
         <tli id="T317" time="164.13933333333333" type="appl" />
         <tli id="T318" time="164.65944444444443" type="appl" />
         <tli id="T319" time="165.17955555555557" type="appl" />
         <tli id="T320" time="165.69966666666667" type="appl" />
         <tli id="T321" time="166.21977777777778" type="appl" />
         <tli id="T322" time="166.73988888888888" type="appl" />
         <tli id="T323" time="167.26" type="appl" />
         <tli id="T324" time="167.51" type="appl" />
         <tli id="T325" time="168.375" type="appl" />
         <tli id="T326" time="169.24" type="appl" />
         <tli id="T327" time="170.105" type="appl" />
         <tli id="T328" time="170.97" type="appl" />
         <tli id="T329" time="171.835" type="appl" />
         <tli id="T330" time="172.7" type="appl" />
         <tli id="T331" time="173.565" type="appl" />
         <tli id="T332" time="174.43" type="appl" />
         <tli id="T333" time="174.61" type="appl" />
         <tli id="T334" time="175.02937500000002" type="appl" />
         <tli id="T335" time="175.44875000000002" type="appl" />
         <tli id="T336" time="175.86812500000002" type="appl" />
         <tli id="T337" time="176.28750000000002" type="appl" />
         <tli id="T338" time="176.706875" type="appl" />
         <tli id="T339" time="177.12625" type="appl" />
         <tli id="T340" time="177.545625" type="appl" />
         <tli id="T341" time="177.965" type="appl" />
         <tli id="T342" time="178.384375" type="appl" />
         <tli id="T343" time="178.80375" type="appl" />
         <tli id="T344" time="179.223125" type="appl" />
         <tli id="T345" time="179.64249999999998" type="appl" />
         <tli id="T346" time="180.061875" type="appl" />
         <tli id="T347" time="180.48125" type="appl" />
         <tli id="T348" time="180.900625" type="appl" />
         <tli id="T349" time="181.32" type="appl" />
         <tli id="T350" time="181.47" type="appl" />
         <tli id="T351" time="181.9725" type="appl" />
         <tli id="T352" time="182.475" type="appl" />
         <tli id="T353" time="182.9775" type="appl" />
         <tli id="T354" time="183.48" type="appl" />
         <tli id="T355" time="183.91" type="appl" />
         <tli id="T356" time="184.47545454545454" type="appl" />
         <tli id="T357" time="185.04090909090908" type="appl" />
         <tli id="T358" time="185.60636363636362" type="appl" />
         <tli id="T359" time="186.17181818181817" type="appl" />
         <tli id="T360" time="186.7372727272727" type="appl" />
         <tli id="T361" time="187.30272727272728" type="appl" />
         <tli id="T362" time="187.86818181818182" type="appl" />
         <tli id="T363" time="188.43363636363637" type="appl" />
         <tli id="T364" time="188.9990909090909" type="appl" />
         <tli id="T365" time="189.56454545454545" type="appl" />
         <tli id="T366" time="190.13" type="appl" />
         <tli id="T367" time="190.38" type="appl" />
         <tli id="T368" time="190.922" type="appl" />
         <tli id="T369" time="191.464" type="appl" />
         <tli id="T370" time="192.006" type="appl" />
         <tli id="T371" time="192.548" type="appl" />
         <tli id="T372" time="193.09" type="appl" />
         <tli id="T373" time="193.19" type="appl" />
         <tli id="T374" time="193.74325" type="appl" />
         <tli id="T375" time="194.2965" type="appl" />
         <tli id="T376" time="194.84975" type="appl" />
         <tli id="T377" time="195.403" type="appl" />
         <tli id="T378" time="195.95625" type="appl" />
         <tli id="T379" time="196.5095" type="appl" />
         <tli id="T380" time="197.06275" type="appl" />
         <tli id="T381" time="197.616" type="appl" />
         <tli id="T382" time="198.16925" type="appl" />
         <tli id="T383" time="198.7225" type="appl" />
         <tli id="T384" time="199.27575000000002" type="appl" />
         <tli id="T385" time="199.829" type="appl" />
         <tli id="T386" time="200.249125" type="appl" />
         <tli id="T387" time="200.66925" type="appl" />
         <tli id="T388" time="201.08937500000002" type="appl" />
         <tli id="T389" time="201.5095" type="appl" />
         <tli id="T390" time="201.929625" type="appl" />
         <tli id="T391" time="202.34975" type="appl" />
         <tli id="T392" time="202.769875" type="appl" />
         <tli id="T393" time="203.19" type="appl" />
         <tli id="T394" time="203.67" type="appl" />
         <tli id="T395" time="204.12666666666667" type="appl" />
         <tli id="T396" time="204.58333333333331" type="appl" />
         <tli id="T397" time="205.04" type="appl" />
         <tli id="T398" time="205.49666666666667" type="appl" />
         <tli id="T399" time="205.95333333333332" type="appl" />
         <tli id="T400" time="206.41" type="appl" />
         <tli id="T401" time="206.49" type="appl" />
         <tli id="T402" time="207.10625000000002" type="appl" />
         <tli id="T403" time="207.7225" type="appl" />
         <tli id="T404" time="208.33875" type="appl" />
         <tli id="T405" time="208.95499999999998" type="appl" />
         <tli id="T406" time="209.57125" type="appl" />
         <tli id="T407" time="210.1875" type="appl" />
         <tli id="T408" time="210.80374999999998" type="appl" />
         <tli id="T409" time="211.42" type="appl" />
         <tli id="T410" time="211.77" type="appl" />
         <tli id="T411" time="212.23285714285714" type="appl" />
         <tli id="T412" time="212.6957142857143" type="appl" />
         <tli id="T413" time="213.15857142857143" type="appl" />
         <tli id="T414" time="213.62142857142857" type="appl" />
         <tli id="T415" time="214.08428571428573" type="appl" />
         <tli id="T416" time="214.54714285714286" type="appl" />
         <tli id="T417" time="215.01" type="appl" />
         <tli id="T418" time="215.47285714285715" type="appl" />
         <tli id="T419" time="215.93571428571428" type="appl" />
         <tli id="T420" time="216.39857142857144" type="appl" />
         <tli id="T421" time="216.86142857142858" type="appl" />
         <tli id="T422" time="217.3242857142857" type="appl" />
         <tli id="T423" time="217.78714285714287" type="appl" />
         <tli id="T424" time="218.25" type="appl" />
         <tli id="T425" time="219.02" type="appl" />
         <tli id="T426" time="219.835" type="appl" />
         <tli id="T427" time="220.65" type="appl" />
         <tli id="T428" time="221.22" type="appl" />
         <tli id="T429" time="221.76875" type="appl" />
         <tli id="T430" time="222.3175" type="appl" />
         <tli id="T431" time="222.86625" type="appl" />
         <tli id="T432" time="223.41500000000002" type="appl" />
         <tli id="T433" time="223.96375" type="appl" />
         <tli id="T434" time="224.51250000000002" type="appl" />
         <tli id="T435" time="225.06125" type="appl" />
         <tli id="T436" time="225.61" type="appl" />
         <tli id="T437" time="226.02" type="appl" />
         <tli id="T438" time="226.48816666666667" type="appl" />
         <tli id="T439" time="226.95633333333333" type="appl" />
         <tli id="T440" time="227.42450000000002" type="appl" />
         <tli id="T441" time="227.89266666666668" type="appl" />
         <tli id="T442" time="228.36083333333335" type="appl" />
         <tli id="T443" time="228.829" type="appl" />
         <tli id="T444" time="228.91" type="appl" />
         <tli id="T445" time="229.32666666666665" type="appl" />
         <tli id="T446" time="229.74333333333334" type="appl" />
         <tli id="T447" time="230.16" type="appl" />
         <tli id="T448" time="230.57666666666665" type="appl" />
         <tli id="T449" time="230.99333333333334" type="appl" />
         <tli id="T450" time="231.41" type="appl" />
         <tli id="T451" time="231.82666666666665" type="appl" />
         <tli id="T452" time="232.24333333333334" type="appl" />
         <tli id="T453" time="232.66" type="appl" />
         <tli id="T454" time="232.93" type="appl" />
         <tli id="T455" time="233.4476923076923" type="appl" />
         <tli id="T456" time="233.9653846153846" type="appl" />
         <tli id="T457" time="234.48307692307694" type="appl" />
         <tli id="T458" time="235.00076923076924" type="appl" />
         <tli id="T459" time="235.51846153846154" type="appl" />
         <tli id="T460" time="236.03615384615384" type="appl" />
         <tli id="T461" time="236.55384615384617" type="appl" />
         <tli id="T462" time="237.07153846153847" type="appl" />
         <tli id="T463" time="237.58923076923077" type="appl" />
         <tli id="T464" time="238.10692307692307" type="appl" />
         <tli id="T465" time="238.6246153846154" type="appl" />
         <tli id="T466" time="239.1423076923077" type="appl" />
         <tli id="T467" time="239.66" type="appl" />
         <tli id="T468" time="240.06" type="appl" />
         <tli id="T469" time="240.5423076923077" type="appl" />
         <tli id="T470" time="241.0246153846154" type="appl" />
         <tli id="T471" time="241.50692307692307" type="appl" />
         <tli id="T472" time="241.98923076923077" type="appl" />
         <tli id="T473" time="242.47153846153847" type="appl" />
         <tli id="T474" time="242.95384615384617" type="appl" />
         <tli id="T475" time="243.43615384615384" type="appl" />
         <tli id="T476" time="243.91846153846154" type="appl" />
         <tli id="T477" time="244.40076923076924" type="appl" />
         <tli id="T478" time="244.88307692307694" type="appl" />
         <tli id="T479" time="245.3653846153846" type="appl" />
         <tli id="T480" time="245.8476923076923" type="appl" />
         <tli id="T481" time="246.33" type="appl" />
         <tli id="T482" time="246.47" type="appl" />
         <tli id="T483" time="246.974" type="appl" />
         <tli id="T484" time="247.478" type="appl" />
         <tli id="T485" time="247.982" type="appl" />
         <tli id="T486" time="248.486" type="appl" />
         <tli id="T487" time="248.99" type="appl" />
         <tli id="T488" time="249.494" type="appl" />
         <tli id="T489" time="249.998" type="appl" />
         <tli id="T490" time="250.50199999999998" type="appl" />
         <tli id="T491" time="251.006" type="appl" />
         <tli id="T492" time="251.51" type="appl" />
         <tli id="T493" time="251.84" type="appl" />
         <tli id="T494" time="252.38727272727274" type="appl" />
         <tli id="T495" time="252.93454545454546" type="appl" />
         <tli id="T496" time="253.4818181818182" type="appl" />
         <tli id="T497" time="254.0290909090909" type="appl" />
         <tli id="T498" time="254.57636363636365" type="appl" />
         <tli id="T499" time="255.12363636363636" type="appl" />
         <tli id="T500" time="255.6709090909091" type="appl" />
         <tli id="T501" time="256.21818181818185" type="appl" />
         <tli id="T502" time="256.76545454545453" type="appl" />
         <tli id="T503" time="257.3127272727273" type="appl" />
         <tli id="T504" time="257.86" type="appl" />
         <tli id="T505" time="258.42" type="appl" />
         <tli id="T506" time="258.8988888888889" type="appl" />
         <tli id="T507" time="259.3777777777778" type="appl" />
         <tli id="T508" time="259.8566666666667" type="appl" />
         <tli id="T509" time="260.3355555555556" type="appl" />
         <tli id="T510" time="260.81444444444446" type="appl" />
         <tli id="T511" time="261.29333333333335" type="appl" />
         <tli id="T512" time="261.77222222222224" type="appl" />
         <tli id="T513" time="262.25111111111113" type="appl" />
         <tli id="T514" time="262.73" type="appl" />
         <tli id="T515" time="263.22" type="appl" />
         <tli id="T516" time="263.785" type="appl" />
         <tli id="T517" time="264.35" type="appl" />
         <tli id="T518" time="264.915" type="appl" />
         <tli id="T519" time="265.48" type="appl" />
         <tli id="T520" time="266.045" type="appl" />
         <tli id="T521" time="266.61" type="appl" />
         <tli id="T522" time="267.175" type="appl" />
         <tli id="T523" time="267.74" type="appl" />
         <tli id="T524" time="268.17" type="appl" />
         <tli id="T525" time="268.735" type="appl" />
         <tli id="T526" time="269.3" type="appl" />
         <tli id="T527" time="269.865" type="appl" />
         <tli id="T528" time="270.43" type="appl" />
         <tli id="T529" time="270.995" type="appl" />
         <tli id="T530" time="271.56" type="appl" />
         <tli id="T531" time="272.49" type="appl" />
         <tli id="T532" time="272.9716666666667" type="appl" />
         <tli id="T533" time="273.4533333333333" type="appl" />
         <tli id="T534" time="273.935" type="appl" />
         <tli id="T535" time="274.4166666666667" type="appl" />
         <tli id="T536" time="274.8983333333333" type="appl" />
         <tli id="T537" time="276.024874809309" />
         <tli id="T538" time="278.33152650199617" />
         <tli id="T539" />
         <tli id="T540" />
         <tli id="T541" />
         <tli id="T542" />
         <tli id="T543" />
         <tli id="T544" time="278.53" type="appl" />
         <tli id="T545" time="279.60749999999996" type="appl" />
         <tli id="T546" time="280.68499999999995" type="appl" />
         <tli id="T547" time="281.7625" type="appl" />
         <tli id="T548" time="282.84" type="appl" />
         <tli id="T549" time="283.43" type="appl" />
         <tli id="T550" time="283.98357142857145" type="appl" />
         <tli id="T551" time="284.53714285714284" type="appl" />
         <tli id="T552" time="285.0907142857143" type="appl" />
         <tli id="T553" time="285.64428571428573" type="appl" />
         <tli id="T554" time="286.1978571428572" type="appl" />
         <tli id="T555" time="286.75142857142856" type="appl" />
         <tli id="T556" time="287.305" type="appl" />
         <tli id="T557" time="287.85857142857145" type="appl" />
         <tli id="T558" time="288.41214285714284" type="appl" />
         <tli id="T559" time="288.9657142857143" type="appl" />
         <tli id="T560" time="289.51928571428573" type="appl" />
         <tli id="T561" time="290.0728571428572" type="appl" />
         <tli id="T562" time="290.62642857142856" type="appl" />
         <tli id="T563" time="291.18" type="appl" />
         <tli id="T564" time="291.47" type="appl" />
         <tli id="T565" time="291.96000000000004" type="appl" />
         <tli id="T566" time="292.45000000000005" type="appl" />
         <tli id="T567" time="292.94000000000005" type="appl" />
         <tli id="T568" time="293.43" type="appl" />
         <tli id="T569" time="293.92" type="appl" />
         <tli id="T570" time="294.41" type="appl" />
         <tli id="T571" time="294.90000000000003" type="appl" />
         <tli id="T572" time="295.39000000000004" type="appl" />
         <tli id="T573" time="295.88" type="appl" />
         <tli id="T574" time="296.37" type="appl" />
         <tli id="T575" time="296.86" type="appl" />
         <tli id="T576" time="297.35" type="appl" />
         <tli id="T577" time="297.97" type="appl" />
         <tli id="T578" time="298.4433333333334" type="appl" />
         <tli id="T579" time="298.9166666666667" type="appl" />
         <tli id="T580" time="299.39" type="appl" />
         <tli id="T581" time="299.86333333333334" type="appl" />
         <tli id="T582" time="300.3366666666667" type="appl" />
         <tli id="T583" time="300.81" type="appl" />
         <tli id="T584" time="301.2833333333333" type="appl" />
         <tli id="T585" time="301.75666666666666" type="appl" />
         <tli id="T586" time="302.23" type="appl" />
         <tli id="T587" time="302.7033333333333" type="appl" />
         <tli id="T588" time="303.1766666666666" type="appl" />
         <tli id="T589" time="303.65" type="appl" />
         <tli id="T590" time="303.72" type="appl" />
         <tli id="T591" time="304.1453333333334" type="appl" />
         <tli id="T592" time="304.5706666666667" type="appl" />
         <tli id="T593" time="304.99600000000004" type="appl" />
         <tli id="T594" time="305.42133333333334" type="appl" />
         <tli id="T595" time="305.8466666666667" type="appl" />
         <tli id="T596" time="306.27200000000005" type="appl" />
         <tli id="T597" time="306.69733333333335" type="appl" />
         <tli id="T598" time="307.1226666666667" type="appl" />
         <tli id="T599" time="307.548" type="appl" />
         <tli id="T600" time="307.97333333333336" type="appl" />
         <tli id="T601" time="308.3986666666667" type="appl" />
         <tli id="T602" time="308.824" type="appl" />
         <tli id="T603" time="309.24933333333337" type="appl" />
         <tli id="T604" time="309.67466666666667" type="appl" />
         <tli id="T605" time="310.1" type="appl" />
         <tli id="T606" time="310.18" type="appl" />
         <tli id="T607" time="310.6566666666667" type="appl" />
         <tli id="T608" time="311.1333333333333" type="appl" />
         <tli id="T609" time="311.61" type="appl" />
         <tli id="T610" time="311.63" type="appl" />
         <tli id="T611" time="312.1075" type="appl" />
         <tli id="T612" time="312.585" type="appl" />
         <tli id="T613" time="313.0625" type="appl" />
         <tli id="T614" time="313.53999999999996" type="appl" />
         <tli id="T615" time="314.0175" type="appl" />
         <tli id="T616" time="314.495" type="appl" />
         <tli id="T617" time="314.97249999999997" type="appl" />
         <tli id="T618" time="315.45" type="appl" />
         <tli id="T619" time="315.59" type="appl" />
         <tli id="T620" time="316.15749999999997" type="appl" />
         <tli id="T621" time="316.72499999999997" type="appl" />
         <tli id="T622" time="317.29249999999996" type="appl" />
         <tli id="T623" time="317.86" type="appl" />
         <tli id="T624" time="318.4275" type="appl" />
         <tli id="T625" time="318.995" type="appl" />
         <tli id="T626" time="319.5625" type="appl" />
         <tli id="T627" time="320.13" type="appl" />
         <tli id="T628" time="320.18" type="appl" />
         <tli id="T629" time="320.65" type="appl" />
         <tli id="T630" time="321.12" type="appl" />
         <tli id="T631" time="321.59000000000003" type="appl" />
         <tli id="T632" time="322.06" type="appl" />
         <tli id="T633" time="322.53" type="appl" />
         <tli id="T634" time="323.0" type="appl" />
         <tli id="T635" time="323.05" type="appl" />
         <tli id="T636" time="323.66" type="appl" />
         <tli id="T637" time="324.27" type="appl" />
         <tli id="T638" time="324.88" type="appl" />
         <tli id="T639" time="325.49" type="appl" />
         <tli id="T640" time="325.53" type="appl" />
         <tli id="T641" time="326.3433333333333" type="appl" />
         <tli id="T642" time="327.1566666666667" type="appl" />
         <tli id="T643" time="327.97" type="appl" />
         <tli id="T644" time="328.05" type="appl" />
         <tli id="T645" time="328.495" type="appl" />
         <tli id="T646" time="328.94" type="appl" />
         <tli id="T647" time="329.385" type="appl" />
         <tli id="T648" time="329.83000000000004" type="appl" />
         <tli id="T649" time="330.27500000000003" type="appl" />
         <tli id="T650" time="330.72" type="appl" />
         <tli id="T651" time="330.81" type="appl" />
         <tli id="T652" time="331.41636363636366" type="appl" />
         <tli id="T653" time="332.02272727272725" type="appl" />
         <tli id="T654" time="332.6290909090909" type="appl" />
         <tli id="T655" time="333.23545454545456" type="appl" />
         <tli id="T656" time="333.8418181818182" type="appl" />
         <tli id="T657" time="334.4481818181818" type="appl" />
         <tli id="T658" time="335.05454545454546" type="appl" />
         <tli id="T659" time="335.6609090909091" type="appl" />
         <tli id="T660" time="336.26727272727277" type="appl" />
         <tli id="T661" time="336.87363636363636" type="appl" />
         <tli id="T662" time="337.48" type="appl" />
         <tli id="T663" time="337.58" type="appl" />
         <tli id="T664" time="338.0222222222222" type="appl" />
         <tli id="T665" time="338.46444444444444" type="appl" />
         <tli id="T666" time="338.90666666666664" type="appl" />
         <tli id="T667" time="339.3488888888889" type="appl" />
         <tli id="T668" time="339.7911111111111" type="appl" />
         <tli id="T669" time="340.23333333333335" type="appl" />
         <tli id="T670" time="340.67555555555555" type="appl" />
         <tli id="T671" time="341.1177777777778" type="appl" />
         <tli id="T672" time="341.56" type="appl" />
         <tli id="T673" time="341.86" type="appl" />
         <tli id="T674" time="342.29266666666666" type="appl" />
         <tli id="T675" time="342.72533333333337" type="appl" />
         <tli id="T676" time="343.158" type="appl" />
         <tli id="T677" time="343.59066666666666" type="appl" />
         <tli id="T678" time="344.02333333333337" type="appl" />
         <tli id="T679" time="344.456" type="appl" />
         <tli id="T680" time="344.88866666666667" type="appl" />
         <tli id="T681" time="345.32133333333337" type="appl" />
         <tli id="T682" time="345.754" type="appl" />
         <tli id="T683" time="346.18666666666667" type="appl" />
         <tli id="T684" time="346.6193333333334" type="appl" />
         <tli id="T685" time="347.052" type="appl" />
         <tli id="T686" time="347.48466666666667" type="appl" />
         <tli id="T687" time="347.9173333333334" type="appl" />
         <tli id="T688" time="348.35" type="appl" />
         <tli id="T689" time="348.42" type="appl" />
         <tli id="T690" time="349.3666666666667" type="appl" />
         <tli id="T691" time="350.31333333333333" type="appl" />
         <tli id="T692" time="351.26" type="appl" />
         <tli id="T693" time="351.47" type="appl" />
         <tli id="T694" time="351.9225" type="appl" />
         <tli id="T695" time="352.375" type="appl" />
         <tli id="T696" time="352.8275" type="appl" />
         <tli id="T697" time="353.28" type="appl" />
         <tli id="T698" time="353.7325" type="appl" />
         <tli id="T699" time="354.185" type="appl" />
         <tli id="T700" time="354.6375" type="appl" />
         <tli id="T701" time="355.09" type="appl" />
         <tli id="T702" time="355.29" type="appl" />
         <tli id="T703" time="355.79333333333335" type="appl" />
         <tli id="T704" time="356.2966666666667" type="appl" />
         <tli id="T705" time="356.8" type="appl" />
         <tli id="T706" time="357.30333333333334" type="appl" />
         <tli id="T707" time="357.8066666666667" type="appl" />
         <tli id="T708" time="358.31" type="appl" />
         <tli id="T709" time="358.4" type="appl" />
         <tli id="T710" time="359.485" type="appl" />
         <tli id="T711" time="360.57" type="appl" />
         <tli id="T712" time="360.61" type="appl" />
         <tli id="T713" time="361.0675" type="appl" />
         <tli id="T714" time="361.525" type="appl" />
         <tli id="T715" time="361.9825" type="appl" />
         <tli id="T716" time="362.44" type="appl" />
         <tli id="T717" time="362.8975" type="appl" />
         <tli id="T718" time="363.355" type="appl" />
         <tli id="T719" time="363.8125" type="appl" />
         <tli id="T720" time="364.27" type="appl" />
         <tli id="T721" time="364.58" type="appl" />
         <tli id="T722" time="365.1525" type="appl" />
         <tli id="T723" time="365.725" type="appl" />
         <tli id="T724" time="366.2975" type="appl" />
         <tli id="T725" time="366.87" type="appl" />
         <tli id="T726" time="367.3" type="appl" />
         <tli id="T727" time="367.7266666666667" type="appl" />
         <tli id="T728" time="368.15333333333336" type="appl" />
         <tli id="T729" time="368.58000000000004" type="appl" />
         <tli id="T730" />
         <tli id="T731" time="369.43333333333334" type="appl" />
         <tli id="T732" time="369.86" type="appl" />
         <tli id="T733" time="370.2866666666667" type="appl" />
         <tli id="T734" time="370.71333333333337" type="appl" />
         <tli id="T735" time="371.14" type="appl" />
         <tli id="T736" time="371.56666666666666" type="appl" />
         <tli id="T737" time="371.99333333333334" type="appl" />
         <tli id="T738" time="372.42" type="appl" />
         <tli id="T739" time="372.58" type="appl" />
         <tli id="T740" time="373.1311111111111" type="appl" />
         <tli id="T741" time="373.6822222222222" type="appl" />
         <tli id="T742" time="374.23333333333335" type="appl" />
         <tli id="T743" time="374.78444444444443" type="appl" />
         <tli id="T744" time="375.3355555555556" type="appl" />
         <tli id="T745" time="375.88666666666666" type="appl" />
         <tli id="T746" time="376.4377777777778" type="appl" />
         <tli id="T747" time="376.9888888888889" type="appl" />
         <tli id="T748" time="377.54" type="appl" />
         <tli id="T749" time="377.68" type="appl" />
         <tli id="T750" time="378.19" type="appl" />
         <tli id="T751" time="378.7" type="appl" />
         <tli id="T752" time="379.21" type="appl" />
         <tli id="T753" time="379.72" type="appl" />
         <tli id="T754" time="380.23" type="appl" />
         <tli id="T755" time="380.74" type="appl" />
         <tli id="T756" time="381.25" type="appl" />
         <tli id="T757" time="381.76" type="appl" />
         <tli id="T758" time="383.07" type="appl" />
         <tli id="T759" time="383.6893333333333" type="appl" />
         <tli id="T760" time="384.3086666666667" type="appl" />
         <tli id="T761" time="384.928" type="appl" />
         <tli id="T762" time="385.5473333333333" type="appl" />
         <tli id="T763" time="386.1666666666667" type="appl" />
         <tli id="T764" time="386.786" type="appl" />
         <tli id="T765" time="387.4053333333333" type="appl" />
         <tli id="T766" time="388.0246666666667" type="appl" />
         <tli id="T767" time="388.644" type="appl" />
         <tli id="T768" time="389.2633333333333" type="appl" />
         <tli id="T769" time="389.8826666666667" type="appl" />
         <tli id="T770" time="390.502" type="appl" />
         <tli id="T771" time="391.1213333333333" type="appl" />
         <tli id="T772" time="391.7406666666667" type="appl" />
         <tli id="T773" time="392.36" type="appl" />
         <tli id="T774" time="392.95" type="appl" />
         <tli id="T775" time="393.42" type="appl" />
         <tli id="T776" time="393.89" type="appl" />
         <tli id="T777" time="394.36" type="appl" />
         <tli id="T778" time="394.83" type="appl" />
         <tli id="T779" time="395.3" type="appl" />
         <tli id="T780" time="395.8" type="appl" />
         <tli id="T781" time="396.3" type="appl" />
         <tli id="T782" time="396.8" type="appl" />
         <tli id="T783" time="397.3" type="appl" />
         <tli id="T784" time="397.8" type="appl" />
         <tli id="T0" time="410.784" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KiPP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T10" id="Seg_0" n="sc" s="T1">
               <ts e="T10" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Onton</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">biːrde</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">bultana</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">taksɨbɨttara</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">ogonnʼottor</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_18" n="HIAT:ip">(</nts>
                  <ts e="T7" id="Seg_20" n="HIAT:w" s="T6">emeːk-</ts>
                  <nts id="Seg_21" n="HIAT:ip">)</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_24" n="HIAT:w" s="T7">ogonnʼottor</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_27" n="HIAT:w" s="T8">keleller</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_30" n="HIAT:w" s="T9">u͡olattar</ts>
                  <nts id="Seg_31" n="HIAT:ip">.</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T16" id="Seg_33" n="sc" s="T11">
               <ts e="T16" id="Seg_35" n="HIAT:u" s="T11">
                  <nts id="Seg_36" n="HIAT:ip">"</nts>
                  <ts e="T12" id="Seg_38" n="HIAT:w" s="T11">Kaja</ts>
                  <nts id="Seg_39" n="HIAT:ip">,</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_42" n="HIAT:w" s="T12">Hɨndaːsskabɨtɨgar</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_45" n="HIAT:w" s="T13">dʼi͡e</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_48" n="HIAT:w" s="T14">turbut</ts>
                  <nts id="Seg_49" n="HIAT:ip">"</nts>
                  <nts id="Seg_50" n="HIAT:ip">,</nts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">di͡ebitter</ts>
                  <nts id="Seg_54" n="HIAT:ip">.</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T19" id="Seg_56" n="sc" s="T17">
               <ts e="T19" id="Seg_58" n="HIAT:u" s="T17">
                  <nts id="Seg_59" n="HIAT:ip">"</nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">Kajdi͡ek</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_64" n="HIAT:w" s="T18">bultanaːččɨbɨt</ts>
                  <nts id="Seg_65" n="HIAT:ip">?</nts>
                  <nts id="Seg_66" n="HIAT:ip">"</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T31" id="Seg_68" n="sc" s="T20">
               <ts e="T31" id="Seg_70" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_72" n="HIAT:w" s="T20">Oː</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_75" n="HIAT:w" s="T21">dʼe</ts>
                  <nts id="Seg_76" n="HIAT:ip">,</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_79" n="HIAT:w" s="T22">bu</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_82" n="HIAT:w" s="T23">dʼi͡e</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_85" n="HIAT:w" s="T24">turbutugar</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_88" n="HIAT:w" s="T25">kuttanallar</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_91" n="HIAT:w" s="T26">keli͡ekterin</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_94" n="HIAT:w" s="T27">ühü</ts>
                  <nts id="Seg_95" n="HIAT:ip">,</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_98" n="HIAT:w" s="T28">nuːččalar</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_101" n="HIAT:w" s="T29">baːllar</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_104" n="HIAT:w" s="T30">di͡en</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T46" id="Seg_107" n="sc" s="T32">
               <ts e="T46" id="Seg_109" n="HIAT:u" s="T32">
                  <nts id="Seg_110" n="HIAT:ip">"</nts>
                  <ts e="T33" id="Seg_112" n="HIAT:w" s="T32">Oː</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_115" n="HIAT:w" s="T33">dʼe</ts>
                  <nts id="Seg_116" n="HIAT:ip">,</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_119" n="HIAT:w" s="T34">bu</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_122" n="HIAT:w" s="T35">hirbitin</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_125" n="HIAT:w" s="T36">bulta</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_128" n="HIAT:w" s="T37">hu͡ok</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_131" n="HIAT:w" s="T38">oŋordular</ts>
                  <nts id="Seg_132" n="HIAT:ip">,</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_135" n="HIAT:w" s="T39">kajdak</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_138" n="HIAT:w" s="T40">bu͡olaːččɨbɨtɨj</ts>
                  <nts id="Seg_139" n="HIAT:ip">"</nts>
                  <nts id="Seg_140" n="HIAT:ip">,</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_143" n="HIAT:w" s="T41">diːller</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_146" n="HIAT:w" s="T42">ühü</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_149" n="HIAT:w" s="T43">ol</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_152" n="HIAT:w" s="T44">ogonnʼottor</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_155" n="HIAT:w" s="T45">ke</ts>
                  <nts id="Seg_156" n="HIAT:ip">.</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T49" id="Seg_158" n="sc" s="T47">
               <ts e="T49" id="Seg_160" n="HIAT:u" s="T47">
                  <nts id="Seg_161" n="HIAT:ip">"</nts>
                  <ts e="T48" id="Seg_163" n="HIAT:w" s="T47">Dehebit</ts>
                  <nts id="Seg_164" n="HIAT:ip">"</nts>
                  <nts id="Seg_165" n="HIAT:ip">,</nts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_168" n="HIAT:w" s="T48">diːller</ts>
                  <nts id="Seg_169" n="HIAT:ip">.</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T54" id="Seg_171" n="sc" s="T50">
               <ts e="T54" id="Seg_173" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_175" n="HIAT:w" s="T50">Kepsetellerin</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_178" n="HIAT:w" s="T51">istebin</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_181" n="HIAT:w" s="T52">ehelerim</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_184" n="HIAT:w" s="T53">ke</ts>
                  <nts id="Seg_185" n="HIAT:ip">.</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T59" id="Seg_187" n="sc" s="T55">
               <ts e="T59" id="Seg_189" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_191" n="HIAT:w" s="T55">Onton</ts>
                  <nts id="Seg_192" n="HIAT:ip">,</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_195" n="HIAT:w" s="T56">dʼe</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_198" n="HIAT:w" s="T57">onton</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_201" n="HIAT:w" s="T58">keleller</ts>
                  <nts id="Seg_202" n="HIAT:ip">.</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T66" id="Seg_204" n="sc" s="T60">
               <ts e="T66" id="Seg_206" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_208" n="HIAT:w" s="T60">Kelenner</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_211" n="HIAT:w" s="T61">körbüttere</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_214" n="HIAT:w" s="T62">ikki</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_217" n="HIAT:w" s="T63">kihi</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_220" n="HIAT:w" s="T64">hɨldʼallar</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_223" n="HIAT:w" s="T65">ühü</ts>
                  <nts id="Seg_224" n="HIAT:ip">.</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T70" id="Seg_226" n="sc" s="T67">
               <ts e="T70" id="Seg_228" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_230" n="HIAT:w" s="T67">Dʼaktarɨn</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_233" n="HIAT:w" s="T68">aːta</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_236" n="HIAT:w" s="T69">Daːrija</ts>
                  <nts id="Seg_237" n="HIAT:ip">.</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T85" id="Seg_239" n="sc" s="T71">
               <ts e="T85" id="Seg_241" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_243" n="HIAT:w" s="T71">Erin</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_246" n="HIAT:w" s="T72">aːta</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_249" n="HIAT:w" s="T73">kim</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_252" n="HIAT:w" s="T74">di͡ebitterij</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_255" n="HIAT:w" s="T75">ke</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_258" n="HIAT:w" s="T76">da</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_261" n="HIAT:w" s="T77">oluja</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_264" n="HIAT:w" s="T78">bagajɨ</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_267" n="HIAT:w" s="T79">aːttaːk</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_270" n="HIAT:w" s="T80">kihi</ts>
                  <nts id="Seg_271" n="HIAT:ip">,</nts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_274" n="HIAT:w" s="T81">kim</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_277" n="HIAT:w" s="T82">ere</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_280" n="HIAT:w" s="T83">di͡eččiler</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_283" n="HIAT:w" s="T84">et</ts>
                  <nts id="Seg_284" n="HIAT:ip">.</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T89" id="Seg_286" n="sc" s="T86">
               <ts e="T89" id="Seg_288" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_290" n="HIAT:w" s="T86">Törüt</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_293" n="HIAT:w" s="T87">öjdöːböppün</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_296" n="HIAT:w" s="T88">aːtɨn</ts>
                  <nts id="Seg_297" n="HIAT:ip">.</nts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T98" id="Seg_299" n="sc" s="T90">
               <ts e="T98" id="Seg_301" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_303" n="HIAT:w" s="T90">Onuga</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_306" n="HIAT:w" s="T91">bu͡o</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_309" n="HIAT:w" s="T92">čugahɨːllar</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_312" n="HIAT:w" s="T93">ühü</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_315" n="HIAT:w" s="T94">kuttana</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_318" n="HIAT:w" s="T95">kuttana</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_321" n="HIAT:w" s="T96">čugahɨːllar</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_324" n="HIAT:w" s="T97">ühü</ts>
                  <nts id="Seg_325" n="HIAT:ip">.</nts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T104" id="Seg_327" n="sc" s="T99">
               <ts e="T104" id="Seg_329" n="HIAT:u" s="T99">
                  <nts id="Seg_330" n="HIAT:ip">"</nts>
                  <ts e="T100" id="Seg_332" n="HIAT:w" s="T99">Bu</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_335" n="HIAT:w" s="T100">togo</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_338" n="HIAT:w" s="T101">kelbikkitij</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_341" n="HIAT:w" s="T102">bihigi</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_344" n="HIAT:w" s="T103">hirbitiger</ts>
                  <nts id="Seg_345" n="HIAT:ip">?</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T107" id="Seg_347" n="sc" s="T105">
               <ts e="T107" id="Seg_349" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_351" n="HIAT:w" s="T105">Togo</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_354" n="HIAT:w" s="T106">bultugut</ts>
                  <nts id="Seg_355" n="HIAT:ip">?</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T115" id="Seg_357" n="sc" s="T108">
               <ts e="T115" id="Seg_359" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_361" n="HIAT:w" s="T108">Bultanar</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_364" n="HIAT:w" s="T109">hirbitin</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_367" n="HIAT:w" s="T110">togo</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_370" n="HIAT:w" s="T111">aldʼatagɨt</ts>
                  <nts id="Seg_371" n="HIAT:ip">"</nts>
                  <nts id="Seg_372" n="HIAT:ip">,</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_375" n="HIAT:w" s="T112">diːller</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_378" n="HIAT:w" s="T113">ühü</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_381" n="HIAT:w" s="T114">ogonnʼottor</ts>
                  <nts id="Seg_382" n="HIAT:ip">.</nts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T123" id="Seg_384" n="sc" s="T116">
               <ts e="T123" id="Seg_386" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_388" n="HIAT:w" s="T116">Kaja</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_391" n="HIAT:w" s="T117">bunna</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_394" n="HIAT:w" s="T118">bu͡ollagɨna</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_397" n="HIAT:w" s="T119">naselenije</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_400" n="HIAT:w" s="T120">oŋoru͡oktara</ts>
                  <nts id="Seg_401" n="HIAT:ip">,</nts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_404" n="HIAT:w" s="T121">kihi</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_407" n="HIAT:w" s="T122">turu͡oga</ts>
                  <nts id="Seg_408" n="HIAT:ip">.</nts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T130" id="Seg_410" n="sc" s="T124">
               <ts e="T130" id="Seg_412" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_414" n="HIAT:w" s="T124">Kihi</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_417" n="HIAT:w" s="T125">ü͡öskü͡öge</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_420" n="HIAT:w" s="T126">munna</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_423" n="HIAT:w" s="T127">elbek</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_426" n="HIAT:w" s="T128">kihi</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_429" n="HIAT:w" s="T129">oloru͡oga</ts>
                  <nts id="Seg_430" n="HIAT:ip">.</nts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T138" id="Seg_432" n="sc" s="T131">
               <ts e="T138" id="Seg_434" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_436" n="HIAT:w" s="T131">Oččogo</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_439" n="HIAT:w" s="T132">peričalbɨt</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_442" n="HIAT:w" s="T133">bu͡o</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_445" n="HIAT:w" s="T134">iti</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_448" n="HIAT:w" s="T135">baːr</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_451" n="HIAT:w" s="T136">hurbujan</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_454" n="HIAT:w" s="T137">kiːrbit</ts>
                  <nts id="Seg_455" n="HIAT:ip">.</nts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T144" id="Seg_457" n="sc" s="T139">
               <ts e="T144" id="Seg_459" n="HIAT:u" s="T139">
                  <nts id="Seg_460" n="HIAT:ip">"</nts>
                  <ts e="T140" id="Seg_462" n="HIAT:w" s="T139">Togo</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_465" n="HIAT:w" s="T140">ɨraːgaj</ts>
                  <nts id="Seg_466" n="HIAT:ip">"</nts>
                  <nts id="Seg_467" n="HIAT:ip">,</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_470" n="HIAT:w" s="T141">diːller</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_473" n="HIAT:w" s="T142">ühü</ts>
                  <nts id="Seg_474" n="HIAT:ip">,</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_476" n="HIAT:ip">"</nts>
                  <ts e="T144" id="Seg_478" n="HIAT:w" s="T143">peričalbɨt</ts>
                  <nts id="Seg_479" n="HIAT:ip">?</nts>
                  <nts id="Seg_480" n="HIAT:ip">"</nts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T153" id="Seg_482" n="sc" s="T145">
               <ts e="T153" id="Seg_484" n="HIAT:u" s="T145">
                  <ts e="T146" id="Seg_486" n="HIAT:w" s="T145">Ü͡ösküt</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_489" n="HIAT:w" s="T146">küččügüj</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_492" n="HIAT:w" s="T147">munna</ts>
                  <nts id="Seg_493" n="HIAT:ip">,</nts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_496" n="HIAT:w" s="T148">ol</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_499" n="HIAT:w" s="T149">ihin</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_502" n="HIAT:w" s="T150">itinne</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_505" n="HIAT:w" s="T151">bu͡olar</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_508" n="HIAT:w" s="T152">ü͡ösküt</ts>
                  <nts id="Seg_509" n="HIAT:ip">.</nts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T157" id="Seg_511" n="sc" s="T154">
               <ts e="T157" id="Seg_513" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_515" n="HIAT:w" s="T154">Itinne</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_518" n="HIAT:w" s="T155">parahu͡ottar</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_521" n="HIAT:w" s="T156">keli͡ektere</ts>
                  <nts id="Seg_522" n="HIAT:ip">.</nts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T165" id="Seg_524" n="sc" s="T158">
               <ts e="T165" id="Seg_526" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_528" n="HIAT:w" s="T158">Oduːrguːllar</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_531" n="HIAT:w" s="T159">ühü</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_533" n="HIAT:ip">"</nts>
                  <ts e="T161" id="Seg_535" n="HIAT:w" s="T160">tu͡ok</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_538" n="HIAT:w" s="T161">aːtaj</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_541" n="HIAT:w" s="T162">parahu͡ot</ts>
                  <nts id="Seg_542" n="HIAT:ip">"</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_545" n="HIAT:w" s="T163">diːr</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_547" n="HIAT:ip">(</nts>
                  <nts id="Seg_548" n="HIAT:ip">(</nts>
                  <ats e="T165" id="Seg_549" n="HIAT:non-pho" s="T164">LAUGH</ats>
                  <nts id="Seg_550" n="HIAT:ip">)</nts>
                  <nts id="Seg_551" n="HIAT:ip">)</nts>
                  <nts id="Seg_552" n="HIAT:ip">.</nts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T169" id="Seg_554" n="sc" s="T166">
               <ts e="T169" id="Seg_556" n="HIAT:u" s="T166">
                  <ts e="T167" id="Seg_558" n="HIAT:w" s="T166">Tartar</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_561" n="HIAT:w" s="T167">keler</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_564" n="HIAT:w" s="T168">parahu͡ot</ts>
                  <nts id="Seg_565" n="HIAT:ip">?</nts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T184" id="Seg_567" n="sc" s="T170">
               <ts e="T184" id="Seg_569" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_571" n="HIAT:w" s="T170">Dʼe</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_574" n="HIAT:w" s="T171">ol</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_577" n="HIAT:w" s="T172">kojut</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_580" n="HIAT:w" s="T173">kojut</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_583" n="HIAT:w" s="T174">kojut</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_586" n="HIAT:w" s="T175">kojut</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_589" n="HIAT:w" s="T176">ol</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_592" n="HIAT:w" s="T177">ü͡öskeːn</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_595" n="HIAT:w" s="T178">ü͡öskeːn</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_598" n="HIAT:w" s="T179">bu</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_601" n="HIAT:w" s="T180">Hɨndaːsskaŋ</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_604" n="HIAT:w" s="T181">bu͡olan</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_607" n="HIAT:w" s="T182">kaːlbɨt</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_610" n="HIAT:w" s="T183">bu͡o</ts>
                  <nts id="Seg_611" n="HIAT:ip">.</nts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T192" id="Seg_613" n="sc" s="T185">
               <ts e="T192" id="Seg_615" n="HIAT:u" s="T185">
                  <ts e="T186" id="Seg_617" n="HIAT:w" s="T185">Usku͡olata</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_620" n="HIAT:w" s="T186">hu͡oktar</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_623" n="HIAT:w" s="T187">uraha</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_626" n="HIAT:w" s="T188">dʼi͡ege</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_629" n="HIAT:w" s="T189">ü͡öreteller</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_632" n="HIAT:w" s="T190">bejelere</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_635" n="HIAT:w" s="T191">tutannar</ts>
                  <nts id="Seg_636" n="HIAT:ip">.</nts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T196" id="Seg_638" n="sc" s="T193">
               <ts e="T196" id="Seg_640" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_642" n="HIAT:w" s="T193">Baloktorgo</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_644" n="HIAT:ip">(</nts>
                  <ts e="T195" id="Seg_646" n="HIAT:w" s="T194">ü͡ö-</ts>
                  <nts id="Seg_647" n="HIAT:ip">)</nts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_650" n="HIAT:w" s="T195">atɨːlɨːllar</ts>
                  <nts id="Seg_651" n="HIAT:ip">.</nts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T208" id="Seg_653" n="sc" s="T197">
               <ts e="T208" id="Seg_655" n="HIAT:u" s="T197">
                  <ts e="T198" id="Seg_657" n="HIAT:w" s="T197">Onton</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_660" n="HIAT:w" s="T198">tupput</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_663" n="HIAT:w" s="T199">dʼi͡elere</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_666" n="HIAT:w" s="T200">baːr</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_669" n="HIAT:w" s="T201">ete</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_672" n="HIAT:w" s="T202">oččotoŋu</ts>
                  <nts id="Seg_673" n="HIAT:ip">,</nts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_676" n="HIAT:w" s="T203">anɨ</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_679" n="HIAT:w" s="T204">huːlbuta</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_682" n="HIAT:w" s="T205">bu͡o</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_685" n="HIAT:w" s="T206">ontuŋ</ts>
                  <nts id="Seg_686" n="HIAT:ip">,</nts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_689" n="HIAT:w" s="T207">laːpkɨlara</ts>
                  <nts id="Seg_690" n="HIAT:ip">.</nts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T223" id="Seg_692" n="sc" s="T209">
               <ts e="T223" id="Seg_694" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_696" n="HIAT:w" s="T209">Ol</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_699" n="HIAT:w" s="T210">ihin</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_702" n="HIAT:w" s="T211">Hɨndaːssko</ts>
                  <nts id="Seg_703" n="HIAT:ip">,</nts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_706" n="HIAT:w" s="T212">Sɨndassko</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_709" n="HIAT:w" s="T213">Daːrija</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_712" n="HIAT:w" s="T214">tugu</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_715" n="HIAT:w" s="T215">kimi</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_718" n="HIAT:w" s="T216">Daːrija</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_721" n="HIAT:w" s="T217">diːller</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_724" n="HIAT:w" s="T218">et</ts>
                  <nts id="Seg_725" n="HIAT:ip">,</nts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_728" n="HIAT:w" s="T219">iti</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_731" n="HIAT:w" s="T220">ti͡ere</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_734" n="HIAT:w" s="T221">aːttɨːllar</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_737" n="HIAT:w" s="T222">ühü</ts>
                  <nts id="Seg_738" n="HIAT:ip">.</nts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T227" id="Seg_740" n="sc" s="T224">
               <ts e="T227" id="Seg_742" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_744" n="HIAT:w" s="T224">Heː</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_747" n="HIAT:w" s="T225">bu</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_750" n="HIAT:w" s="T226">Hɨndaːsskanɨ</ts>
                  <nts id="Seg_751" n="HIAT:ip">.</nts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T238" id="Seg_753" n="sc" s="T228">
               <ts e="T238" id="Seg_755" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_757" n="HIAT:w" s="T228">Iti</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_760" n="HIAT:w" s="T229">bultanar</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_763" n="HIAT:w" s="T230">hirderin</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_766" n="HIAT:w" s="T231">bɨldʼaːbɨttar</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_769" n="HIAT:w" s="T232">onton</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_772" n="HIAT:w" s="T233">kojut</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_775" n="HIAT:w" s="T234">kojut</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_778" n="HIAT:w" s="T235">et</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_781" n="HIAT:w" s="T236">iti</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_784" n="HIAT:w" s="T237">bu͡olbuttara</ts>
                  <nts id="Seg_785" n="HIAT:ip">.</nts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T250" id="Seg_787" n="sc" s="T239">
               <ts e="T250" id="Seg_789" n="HIAT:u" s="T239">
                  <ts e="T240" id="Seg_791" n="HIAT:w" s="T239">Onton</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_794" n="HIAT:w" s="T240">kojut</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_797" n="HIAT:w" s="T241">iti</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_800" n="HIAT:w" s="T242">bu</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_803" n="HIAT:w" s="T243">kele</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_806" n="HIAT:w" s="T244">iligine</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_809" n="HIAT:w" s="T245">Lɨgɨjdar</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_812" n="HIAT:w" s="T246">di͡en</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_815" n="HIAT:w" s="T247">baːr</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_818" n="HIAT:w" s="T248">etilere</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_821" n="HIAT:w" s="T249">ühü</ts>
                  <nts id="Seg_822" n="HIAT:ip">.</nts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T256" id="Seg_824" n="sc" s="T251">
               <ts e="T256" id="Seg_826" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_828" n="HIAT:w" s="T251">Eh</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_831" n="HIAT:w" s="T252">kimner</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_834" n="HIAT:w" s="T253">tigileːkter</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_837" n="HIAT:w" s="T254">diːller</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_840" n="HIAT:w" s="T255">ol</ts>
                  <nts id="Seg_841" n="HIAT:ip">.</nts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T270" id="Seg_843" n="sc" s="T257">
               <ts e="T270" id="Seg_845" n="HIAT:u" s="T257">
                  <ts e="T258" id="Seg_847" n="HIAT:w" s="T257">Itinne</ts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_850" n="HIAT:w" s="T258">iti</ts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_853" n="HIAT:w" s="T259">peričal</ts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_856" n="HIAT:w" s="T260">ɨnaraː</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_859" n="HIAT:w" s="T261">öttüger</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_862" n="HIAT:w" s="T262">bu͡olla</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_865" n="HIAT:w" s="T263">dʼi͡e</ts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_868" n="HIAT:w" s="T264">onnulara</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_871" n="HIAT:w" s="T265">baːllar</ts>
                  <nts id="Seg_872" n="HIAT:ip">,</nts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_875" n="HIAT:w" s="T266">ikki</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_878" n="HIAT:w" s="T267">üs</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_881" n="HIAT:w" s="T268">dʼi͡e</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_884" n="HIAT:w" s="T269">onnuta</ts>
                  <nts id="Seg_885" n="HIAT:ip">.</nts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T278" id="Seg_887" n="sc" s="T271">
               <ts e="T278" id="Seg_889" n="HIAT:u" s="T271">
                  <ts e="T272" id="Seg_891" n="HIAT:w" s="T271">Onno</ts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_894" n="HIAT:w" s="T272">olorollor</ts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_897" n="HIAT:w" s="T273">ol</ts>
                  <nts id="Seg_898" n="HIAT:ip">,</nts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_901" n="HIAT:w" s="T274">bulčuttar</ts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_904" n="HIAT:w" s="T275">olordoktorunan</ts>
                  <nts id="Seg_905" n="HIAT:ip">,</nts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_908" n="HIAT:w" s="T276">Lɨgɨjdar</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_911" n="HIAT:w" s="T277">kelbittere</ts>
                  <nts id="Seg_912" n="HIAT:ip">.</nts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T281" id="Seg_914" n="sc" s="T279">
               <ts e="T281" id="Seg_916" n="HIAT:u" s="T279">
                  <ts e="T280" id="Seg_918" n="HIAT:w" s="T279">Lɨgɨjder</ts>
                  <nts id="Seg_919" n="HIAT:ip">,</nts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_922" n="HIAT:w" s="T280">tigileːkter</ts>
                  <nts id="Seg_923" n="HIAT:ip">.</nts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T287" id="Seg_925" n="sc" s="T282">
               <ts e="T287" id="Seg_927" n="HIAT:u" s="T282">
                  <ts e="T283" id="Seg_929" n="HIAT:w" s="T282">Ol</ts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_932" n="HIAT:w" s="T283">kelbittere</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_935" n="HIAT:w" s="T284">üs</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_938" n="HIAT:w" s="T285">dʼaktardaːktar</ts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_941" n="HIAT:w" s="T286">olor</ts>
                  <nts id="Seg_942" n="HIAT:ip">.</nts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T293" id="Seg_944" n="sc" s="T288">
               <ts e="T293" id="Seg_946" n="HIAT:u" s="T288">
                  <ts e="T289" id="Seg_948" n="HIAT:w" s="T288">Biːrdere</ts>
                  <nts id="Seg_949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_951" n="HIAT:w" s="T289">ojuːn</ts>
                  <nts id="Seg_952" n="HIAT:ip">,</nts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_954" n="HIAT:ip">(</nts>
                  <ts e="T291" id="Seg_956" n="HIAT:w" s="T290">biːrde-</ts>
                  <nts id="Seg_957" n="HIAT:ip">)</nts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_960" n="HIAT:w" s="T291">ikkilere</ts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_963" n="HIAT:w" s="T292">povar</ts>
                  <nts id="Seg_964" n="HIAT:ip">.</nts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T306" id="Seg_966" n="sc" s="T294">
               <ts e="T306" id="Seg_968" n="HIAT:u" s="T294">
                  <ts e="T295" id="Seg_970" n="HIAT:w" s="T294">Dʼe</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_973" n="HIAT:w" s="T295">bu</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_976" n="HIAT:w" s="T296">kihini</ts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_979" n="HIAT:w" s="T297">ölördö</ts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_982" n="HIAT:w" s="T298">ölörbütünen</ts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_985" n="HIAT:w" s="T299">kelbitter</ts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_988" n="HIAT:w" s="T300">oloru</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_991" n="HIAT:w" s="T301">povar</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_994" n="HIAT:w" s="T302">gɨnaːrɨ</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_997" n="HIAT:w" s="T303">egelbittere</ts>
                  <nts id="Seg_998" n="HIAT:ip">,</nts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1001" n="HIAT:w" s="T304">kuhagan</ts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1004" n="HIAT:w" s="T305">kihiler</ts>
                  <nts id="Seg_1005" n="HIAT:ip">.</nts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T313" id="Seg_1007" n="sc" s="T307">
               <ts e="T313" id="Seg_1009" n="HIAT:u" s="T307">
                  <ts e="T308" id="Seg_1011" n="HIAT:w" s="T307">Kak</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1014" n="HIAT:w" s="T308">budta</ts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1017" n="HIAT:w" s="T309">tu͡oktara</ts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1020" n="HIAT:w" s="T310">hülügetter</ts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1023" n="HIAT:w" s="T311">eni</ts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1026" n="HIAT:w" s="T312">to</ts>
                  <nts id="Seg_1027" n="HIAT:ip">.</nts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T323" id="Seg_1029" n="sc" s="T314">
               <ts e="T323" id="Seg_1031" n="HIAT:u" s="T314">
                  <ts e="T315" id="Seg_1033" n="HIAT:w" s="T314">Ol</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1036" n="HIAT:w" s="T315">kelen</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1039" n="HIAT:w" s="T316">kelenner</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1042" n="HIAT:w" s="T317">munna</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1045" n="HIAT:w" s="T318">kelbittere</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1048" n="HIAT:w" s="T319">bulčuttar</ts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1051" n="HIAT:w" s="T320">biːrdere</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1054" n="HIAT:w" s="T321">bu͡o</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1057" n="HIAT:w" s="T322">bɨhɨj</ts>
                  <nts id="Seg_1058" n="HIAT:ip">.</nts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T332" id="Seg_1060" n="sc" s="T324">
               <ts e="T332" id="Seg_1062" n="HIAT:u" s="T324">
                  <ts e="T325" id="Seg_1064" n="HIAT:w" s="T324">Biːrdere</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1067" n="HIAT:w" s="T325">körügös</ts>
                  <nts id="Seg_1068" n="HIAT:ip">,</nts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1071" n="HIAT:w" s="T326">biːrdere</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1074" n="HIAT:w" s="T327">ɨtɨːhɨt</ts>
                  <nts id="Seg_1075" n="HIAT:ip">,</nts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1078" n="HIAT:w" s="T328">bulčuttar</ts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1081" n="HIAT:w" s="T329">bihi͡ettere</ts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1084" n="HIAT:w" s="T330">ühü</ts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1087" n="HIAT:w" s="T331">kepsiːller</ts>
                  <nts id="Seg_1088" n="HIAT:ip">.</nts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T349" id="Seg_1090" n="sc" s="T333">
               <ts e="T349" id="Seg_1092" n="HIAT:u" s="T333">
                  <ts e="T334" id="Seg_1094" n="HIAT:w" s="T333">Olor</ts>
                  <nts id="Seg_1095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1097" n="HIAT:w" s="T334">bu͡ollagɨna</ts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1100" n="HIAT:w" s="T335">kihini</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1103" n="HIAT:w" s="T336">baratan</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1106" n="HIAT:w" s="T337">kelen</ts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1109" n="HIAT:w" s="T338">kelenner</ts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1112" n="HIAT:w" s="T339">bu</ts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1115" n="HIAT:w" s="T340">ogonnʼordor</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1118" n="HIAT:w" s="T341">Malɨj</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1121" n="HIAT:w" s="T342">di͡en</ts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1124" n="HIAT:w" s="T343">hirge</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1127" n="HIAT:w" s="T344">Pu͡orotaj</ts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1130" n="HIAT:w" s="T345">di͡en</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1133" n="HIAT:w" s="T346">kihi</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1136" n="HIAT:w" s="T347">baːr</ts>
                  <nts id="Seg_1137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1139" n="HIAT:w" s="T348">ete</ts>
                  <nts id="Seg_1140" n="HIAT:ip">.</nts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T354" id="Seg_1142" n="sc" s="T350">
               <ts e="T354" id="Seg_1144" n="HIAT:u" s="T350">
                  <ts e="T351" id="Seg_1146" n="HIAT:w" s="T350">Onuga</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1149" n="HIAT:w" s="T351">barbɨttar</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1152" n="HIAT:w" s="T352">bu</ts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1155" n="HIAT:w" s="T353">kihiler</ts>
                  <nts id="Seg_1156" n="HIAT:ip">.</nts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T366" id="Seg_1158" n="sc" s="T355">
               <ts e="T366" id="Seg_1160" n="HIAT:u" s="T355">
                  <nts id="Seg_1161" n="HIAT:ip">"</nts>
                  <ts e="T356" id="Seg_1163" n="HIAT:w" s="T355">Kaja</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1166" n="HIAT:w" s="T356">dogo</ts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1169" n="HIAT:w" s="T357">onnuk</ts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1172" n="HIAT:w" s="T358">onnuk</ts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1175" n="HIAT:w" s="T359">heriː</ts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1177" n="HIAT:ip">(</nts>
                  <ts e="T361" id="Seg_1179" n="HIAT:w" s="T360">tumsunaːk</ts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1182" n="HIAT:w" s="T361">ɨttar</ts>
                  <nts id="Seg_1183" n="HIAT:ip">)</nts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1186" n="HIAT:w" s="T362">kejeller</ts>
                  <nts id="Seg_1187" n="HIAT:ip">,</nts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1190" n="HIAT:w" s="T363">ölörü͡öksütter</ts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1192" n="HIAT:ip">(</nts>
                  <ts e="T365" id="Seg_1194" n="HIAT:w" s="T364">keller</ts>
                  <nts id="Seg_1195" n="HIAT:ip">)</nts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1198" n="HIAT:w" s="T365">tigileːkter</ts>
                  <nts id="Seg_1199" n="HIAT:ip">.</nts>
                  <nts id="Seg_1200" n="HIAT:ip">"</nts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T372" id="Seg_1202" n="sc" s="T367">
               <ts e="T372" id="Seg_1204" n="HIAT:u" s="T367">
                  <ts e="T368" id="Seg_1206" n="HIAT:w" s="T367">De</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1209" n="HIAT:w" s="T368">ol</ts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1212" n="HIAT:w" s="T369">kürüːller</ts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1215" n="HIAT:w" s="T370">Malɨjga</ts>
                  <nts id="Seg_1216" n="HIAT:ip">,</nts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1219" n="HIAT:w" s="T371">küreːbittere</ts>
                  <nts id="Seg_1220" n="HIAT:ip">.</nts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T393" id="Seg_1222" n="sc" s="T373">
               <ts e="T385" id="Seg_1224" n="HIAT:u" s="T373">
                  <ts e="T374" id="Seg_1226" n="HIAT:w" s="T373">Ol</ts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1229" n="HIAT:w" s="T374">küreːn</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1232" n="HIAT:w" s="T375">tiːjbitter</ts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1235" n="HIAT:w" s="T376">Pu͡orotajga</ts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1238" n="HIAT:w" s="T377">tiːjenner</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1241" n="HIAT:w" s="T378">diː</ts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1244" n="HIAT:w" s="T379">kepsiːller</ts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1247" n="HIAT:w" s="T380">kaja</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1250" n="HIAT:w" s="T381">onnuktar</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1253" n="HIAT:w" s="T382">kelliler</ts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1256" n="HIAT:w" s="T383">kihini</ts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1259" n="HIAT:w" s="T384">baraːppɨttar</ts>
                  <nts id="Seg_1260" n="HIAT:ip">.</nts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T393" id="Seg_1263" n="HIAT:u" s="T385">
                  <ts e="T386" id="Seg_1265" n="HIAT:w" s="T385">Ki͡eŋ</ts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1268" n="HIAT:w" s="T386">kü͡ölten</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1271" n="HIAT:w" s="T387">ustannar</ts>
                  <nts id="Seg_1272" n="HIAT:ip">,</nts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1275" n="HIAT:w" s="T388">haŋa</ts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1278" n="HIAT:w" s="T389">ürek</ts>
                  <nts id="Seg_1279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1281" n="HIAT:w" s="T390">üstün</ts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1284" n="HIAT:w" s="T391">kelbitter</ts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1287" n="HIAT:w" s="T392">di͡en</ts>
                  <nts id="Seg_1288" n="HIAT:ip">.</nts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T400" id="Seg_1290" n="sc" s="T394">
               <ts e="T400" id="Seg_1292" n="HIAT:u" s="T394">
                  <ts e="T395" id="Seg_1294" n="HIAT:w" s="T394">Ol</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1297" n="HIAT:w" s="T395">ogonnʼorgo</ts>
                  <nts id="Seg_1298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1300" n="HIAT:w" s="T396">tiːjenner</ts>
                  <nts id="Seg_1301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1303" n="HIAT:w" s="T397">bu</ts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1306" n="HIAT:w" s="T398">kihiler</ts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1309" n="HIAT:w" s="T399">dʼe</ts>
                  <nts id="Seg_1310" n="HIAT:ip">.</nts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T409" id="Seg_1312" n="sc" s="T401">
               <ts e="T409" id="Seg_1314" n="HIAT:u" s="T401">
                  <nts id="Seg_1315" n="HIAT:ip">"</nts>
                  <ts e="T402" id="Seg_1317" n="HIAT:w" s="T401">Dʼe</ts>
                  <nts id="Seg_1318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1320" n="HIAT:w" s="T402">körüges</ts>
                  <nts id="Seg_1321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1323" n="HIAT:w" s="T403">körülüː</ts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1326" n="HIAT:w" s="T404">olor</ts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1329" n="HIAT:w" s="T405">üːhe</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1332" n="HIAT:w" s="T406">hu͡opkaga</ts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1335" n="HIAT:w" s="T407">taksaŋŋɨn</ts>
                  <nts id="Seg_1336" n="HIAT:ip">"</nts>
                  <nts id="Seg_1337" n="HIAT:ip">,</nts>
                  <nts id="Seg_1338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1340" n="HIAT:w" s="T408">di͡en</ts>
                  <nts id="Seg_1341" n="HIAT:ip">.</nts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T424" id="Seg_1343" n="sc" s="T410">
               <ts e="T424" id="Seg_1345" n="HIAT:u" s="T410">
                  <ts e="T411" id="Seg_1347" n="HIAT:w" s="T410">Biːrdere</ts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1350" n="HIAT:w" s="T411">ɨtɨːhɨttara</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1352" n="HIAT:ip">(</nts>
                  <ts e="T413" id="Seg_1354" n="HIAT:w" s="T412">kenne</ts>
                  <nts id="Seg_1355" n="HIAT:ip">)</nts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1358" n="HIAT:w" s="T413">kiːllerteːbit</ts>
                  <nts id="Seg_1359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1361" n="HIAT:w" s="T414">dogottorun</ts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1364" n="HIAT:w" s="T415">innʼe</ts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1367" n="HIAT:w" s="T416">gɨnan</ts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1370" n="HIAT:w" s="T417">baran</ts>
                  <nts id="Seg_1371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1373" n="HIAT:w" s="T418">dʼe</ts>
                  <nts id="Seg_1374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1376" n="HIAT:w" s="T419">kaːn</ts>
                  <nts id="Seg_1377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1379" n="HIAT:w" s="T420">butugas</ts>
                  <nts id="Seg_1380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1382" n="HIAT:w" s="T421">busputtar</ts>
                  <nts id="Seg_1383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1385" n="HIAT:w" s="T422">bu</ts>
                  <nts id="Seg_1386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1388" n="HIAT:w" s="T423">ogonnʼor</ts>
                  <nts id="Seg_1389" n="HIAT:ip">.</nts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T427" id="Seg_1391" n="sc" s="T425">
               <ts e="T427" id="Seg_1393" n="HIAT:u" s="T425">
                  <ts e="T426" id="Seg_1395" n="HIAT:w" s="T425">Uraha</ts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1398" n="HIAT:w" s="T426">dʼi͡eleːkter</ts>
                  <nts id="Seg_1399" n="HIAT:ip">.</nts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T436" id="Seg_1401" n="sc" s="T428">
               <ts e="T436" id="Seg_1403" n="HIAT:u" s="T428">
                  <ts e="T429" id="Seg_1405" n="HIAT:w" s="T428">Dʼe</ts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1408" n="HIAT:w" s="T429">butugastaːn</ts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1411" n="HIAT:w" s="T430">baran</ts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1414" n="HIAT:w" s="T431">bu͡o</ts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1417" n="HIAT:w" s="T432">butugahɨgar</ts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1420" n="HIAT:w" s="T433">bu͡olla</ts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1423" n="HIAT:w" s="T434">kimi</ts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1426" n="HIAT:w" s="T435">kerpit</ts>
                  <nts id="Seg_1427" n="HIAT:ip">.</nts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T443" id="Seg_1429" n="sc" s="T437">
               <ts e="T443" id="Seg_1431" n="HIAT:u" s="T437">
                  <ts e="T438" id="Seg_1433" n="HIAT:w" s="T437">Hɨn</ts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1436" n="HIAT:w" s="T438">di͡eččiler</ts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1439" n="HIAT:w" s="T439">oččogo</ts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1442" n="HIAT:w" s="T440">anɨ</ts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1445" n="HIAT:w" s="T441">hɨn</ts>
                  <nts id="Seg_1446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1448" n="HIAT:w" s="T442">li͡eske</ts>
                  <nts id="Seg_1449" n="HIAT:ip">.</nts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T453" id="Seg_1451" n="sc" s="T444">
               <ts e="T453" id="Seg_1453" n="HIAT:u" s="T444">
                  <ts e="T445" id="Seg_1455" n="HIAT:w" s="T444">Li͡eskanɨ</ts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1458" n="HIAT:w" s="T445">tabaːkka</ts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1461" n="HIAT:w" s="T446">di͡eri</ts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1464" n="HIAT:w" s="T447">kördük</ts>
                  <nts id="Seg_1465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1467" n="HIAT:w" s="T448">kerte-kerte</ts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1470" n="HIAT:w" s="T449">bu͡o</ts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1473" n="HIAT:w" s="T450">butugaska</ts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1476" n="HIAT:w" s="T451">kupput</ts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1479" n="HIAT:w" s="T452">bu͡o</ts>
                  <nts id="Seg_1480" n="HIAT:ip">.</nts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T467" id="Seg_1482" n="sc" s="T454">
               <ts e="T467" id="Seg_1484" n="HIAT:u" s="T454">
                  <nts id="Seg_1485" n="HIAT:ip">"</nts>
                  <ts e="T455" id="Seg_1487" n="HIAT:w" s="T454">Aččɨktaːn</ts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1490" n="HIAT:w" s="T455">iher</ts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1493" n="HIAT:w" s="T456">kihiler</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1496" n="HIAT:w" s="T457">butugas</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1499" n="HIAT:w" s="T458">ihi͡ektere</ts>
                  <nts id="Seg_1500" n="HIAT:ip">"</nts>
                  <nts id="Seg_1501" n="HIAT:ip">,</nts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1504" n="HIAT:w" s="T459">di͡en</ts>
                  <nts id="Seg_1505" n="HIAT:ip">,</nts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1507" n="HIAT:ip">"</nts>
                  <ts e="T461" id="Seg_1509" n="HIAT:w" s="T460">ol</ts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1512" n="HIAT:w" s="T461">ihi͡ektere</ts>
                  <nts id="Seg_1513" n="HIAT:ip">"</nts>
                  <nts id="Seg_1514" n="HIAT:ip">,</nts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1517" n="HIAT:w" s="T462">di͡en</ts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1520" n="HIAT:w" s="T463">ol</ts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1522" n="HIAT:ip">(</nts>
                  <ts e="T465" id="Seg_1524" n="HIAT:w" s="T464">butugas</ts>
                  <nts id="Seg_1525" n="HIAT:ip">)</nts>
                  <nts id="Seg_1526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1528" n="HIAT:w" s="T465">kerderbit</ts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1531" n="HIAT:w" s="T466">bu͡o</ts>
                  <nts id="Seg_1532" n="HIAT:ip">.</nts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T481" id="Seg_1534" n="sc" s="T468">
               <ts e="T481" id="Seg_1536" n="HIAT:u" s="T468">
                  <nts id="Seg_1537" n="HIAT:ip">"</nts>
                  <ts e="T469" id="Seg_1539" n="HIAT:w" s="T468">Innʼen</ts>
                  <nts id="Seg_1540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1542" n="HIAT:w" s="T469">kantas</ts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1545" n="HIAT:w" s="T470">gɨnnagɨna</ts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1548" n="HIAT:w" s="T471">ol</ts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1551" n="HIAT:w" s="T472">tojonnorun</ts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1554" n="HIAT:w" s="T473">kabɨrgatɨn</ts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1557" n="HIAT:w" s="T474">kaja</ts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1560" n="HIAT:w" s="T475">oksoːruŋ</ts>
                  <nts id="Seg_1561" n="HIAT:ip">"</nts>
                  <nts id="Seg_1562" n="HIAT:ip">,</nts>
                  <nts id="Seg_1563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1565" n="HIAT:w" s="T476">di͡en</ts>
                  <nts id="Seg_1566" n="HIAT:ip">,</nts>
                  <nts id="Seg_1567" n="HIAT:ip">"</nts>
                  <nts id="Seg_1568" n="HIAT:ip">(</nts>
                  <nts id="Seg_1569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1570" n="HIAT:ip">"</nts>
                  <nts id="Seg_1571" n="HIAT:ip">(</nts>
                  <ts e="T478" id="Seg_1573" n="HIAT:w" s="T477">bɨtɨgɨ-</ts>
                  <nts id="Seg_1574" n="HIAT:ip">)</nts>
                  <nts id="Seg_1575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1577" n="HIAT:w" s="T478">eː</ts>
                  <nts id="Seg_1578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1580" n="HIAT:w" s="T479">kiminen</ts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1583" n="HIAT:w" s="T480">batɨjanan</ts>
                  <nts id="Seg_1584" n="HIAT:ip">.</nts>
                  <nts id="Seg_1585" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T492" id="Seg_1586" n="sc" s="T482">
               <ts e="T492" id="Seg_1588" n="HIAT:u" s="T482">
                  <ts e="T483" id="Seg_1590" n="HIAT:w" s="T482">Oččogo</ts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1593" n="HIAT:w" s="T483">ere</ts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1596" n="HIAT:w" s="T484">kɨ͡ajɨ͡akpɨt</ts>
                  <nts id="Seg_1597" n="HIAT:ip">"</nts>
                  <nts id="Seg_1598" n="HIAT:ip">,</nts>
                  <nts id="Seg_1599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1601" n="HIAT:w" s="T485">di͡ebit</ts>
                  <nts id="Seg_1602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1604" n="HIAT:w" s="T486">bu͡o</ts>
                  <nts id="Seg_1605" n="HIAT:ip">,</nts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1608" n="HIAT:w" s="T487">onton</ts>
                  <nts id="Seg_1609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1611" n="HIAT:w" s="T488">haŋarda</ts>
                  <nts id="Seg_1612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1614" n="HIAT:w" s="T489">kɨ͡ajɨ͡akpɨt</ts>
                  <nts id="Seg_1615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1617" n="HIAT:w" s="T490">kajdak</ts>
                  <nts id="Seg_1618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1620" n="HIAT:w" s="T491">eme</ts>
                  <nts id="Seg_1621" n="HIAT:ip">"</nts>
                  <nts id="Seg_1622" n="HIAT:ip">.</nts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T504" id="Seg_1624" n="sc" s="T493">
               <ts e="T504" id="Seg_1626" n="HIAT:u" s="T493">
                  <ts e="T494" id="Seg_1628" n="HIAT:w" s="T493">De</ts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1631" n="HIAT:w" s="T494">ol</ts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1634" n="HIAT:w" s="T495">kelbitter</ts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1637" n="HIAT:w" s="T496">ol</ts>
                  <nts id="Seg_1638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1640" n="HIAT:w" s="T497">butugastaːbɨttar</ts>
                  <nts id="Seg_1641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1643" n="HIAT:w" s="T498">dʼe</ts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1646" n="HIAT:w" s="T499">iheller</ts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1648" n="HIAT:ip">"</nts>
                  <ts e="T501" id="Seg_1650" n="HIAT:w" s="T500">butugastaːŋ</ts>
                  <nts id="Seg_1651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1653" n="HIAT:w" s="T501">di͡ebit</ts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1656" n="HIAT:w" s="T502">bu͡o</ts>
                  <nts id="Seg_1657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1659" n="HIAT:w" s="T503">butugastaːbɨttar</ts>
                  <nts id="Seg_1660" n="HIAT:ip">.</nts>
                  <nts id="Seg_1661" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T514" id="Seg_1662" n="sc" s="T505">
               <ts e="T514" id="Seg_1664" n="HIAT:u" s="T505">
                  <ts e="T506" id="Seg_1666" n="HIAT:w" s="T505">Dʼe</ts>
                  <nts id="Seg_1667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1669" n="HIAT:w" s="T506">ol</ts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1672" n="HIAT:w" s="T507">kɨːnnʼara</ts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1675" n="HIAT:w" s="T508">hɨttaktarɨnan</ts>
                  <nts id="Seg_1676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1678" n="HIAT:w" s="T509">de</ts>
                  <nts id="Seg_1679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1681" n="HIAT:w" s="T510">kelbitter</ts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1684" n="HIAT:w" s="T511">bu͡o</ts>
                  <nts id="Seg_1685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1687" n="HIAT:w" s="T512">ol</ts>
                  <nts id="Seg_1688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1690" n="HIAT:w" s="T513">ečikejeleriŋ</ts>
                  <nts id="Seg_1691" n="HIAT:ip">.</nts>
                  <nts id="Seg_1692" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T523" id="Seg_1693" n="sc" s="T515">
               <ts e="T523" id="Seg_1695" n="HIAT:u" s="T515">
                  <ts e="T516" id="Seg_1697" n="HIAT:w" s="T515">Kelenner</ts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1700" n="HIAT:w" s="T516">dʼaktattarɨ</ts>
                  <nts id="Seg_1701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1703" n="HIAT:w" s="T517">kiːlletteːn</ts>
                  <nts id="Seg_1704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1706" n="HIAT:w" s="T518">de</ts>
                  <nts id="Seg_1707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1709" n="HIAT:w" s="T519">bu͡o</ts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1712" n="HIAT:w" s="T520">ogonnʼor</ts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1715" n="HIAT:w" s="T521">tohujar</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1718" n="HIAT:w" s="T522">kiːllettiːr</ts>
                  <nts id="Seg_1719" n="HIAT:ip">.</nts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T530" id="Seg_1721" n="sc" s="T524">
               <ts e="T530" id="Seg_1723" n="HIAT:u" s="T524">
                  <nts id="Seg_1724" n="HIAT:ip">"</nts>
                  <ts e="T525" id="Seg_1726" n="HIAT:w" s="T524">Kaja</ts>
                  <nts id="Seg_1727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1729" n="HIAT:w" s="T525">onnuktar</ts>
                  <nts id="Seg_1730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1732" n="HIAT:w" s="T526">mannɨktar</ts>
                  <nts id="Seg_1733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1735" n="HIAT:w" s="T527">östö</ts>
                  <nts id="Seg_1736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1738" n="HIAT:w" s="T528">kepseːŋ</ts>
                  <nts id="Seg_1739" n="HIAT:ip">"</nts>
                  <nts id="Seg_1740" n="HIAT:ip">,</nts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1743" n="HIAT:w" s="T529">di͡en</ts>
                  <nts id="Seg_1744" n="HIAT:ip">.</nts>
                  <nts id="Seg_1745" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T543" id="Seg_1746" n="sc" s="T531">
               <ts e="T537" id="Seg_1748" n="HIAT:u" s="T531">
                  <ts e="T532" id="Seg_1750" n="HIAT:w" s="T531">De</ts>
                  <nts id="Seg_1751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1753" n="HIAT:w" s="T532">ol</ts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1756" n="HIAT:w" s="T533">ogonnʼorgo</ts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1759" n="HIAT:w" s="T534">tiːjen</ts>
                  <nts id="Seg_1760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1762" n="HIAT:w" s="T535">kepsiːller</ts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1765" n="HIAT:w" s="T536">bu͡o</ts>
                  <nts id="Seg_1766" n="HIAT:ip">:</nts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T543" id="Seg_1769" n="HIAT:u" s="T537">
                  <nts id="Seg_1770" n="HIAT:ip">"</nts>
                  <ts e="T538" id="Seg_1772" n="HIAT:w" s="T537">Emeːksini</ts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1775" n="HIAT:w" s="T538">egellibit</ts>
                  <nts id="Seg_1776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1778" n="HIAT:w" s="T539">bihigi</ts>
                  <nts id="Seg_1779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1781" n="HIAT:w" s="T540">iti</ts>
                  <nts id="Seg_1782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1784" n="HIAT:w" s="T541">hirdʼippit</ts>
                  <nts id="Seg_1785" n="HIAT:ip">,</nts>
                  <nts id="Seg_1786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1788" n="HIAT:w" s="T542">diːbit</ts>
                  <nts id="Seg_1789" n="HIAT:ip">.</nts>
                  <nts id="Seg_1790" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T548" id="Seg_1791" n="sc" s="T544">
               <ts e="T548" id="Seg_1793" n="HIAT:u" s="T544">
                  <ts e="T545" id="Seg_1795" n="HIAT:w" s="T544">Ojuːn</ts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1798" n="HIAT:w" s="T545">emeːksin</ts>
                  <nts id="Seg_1799" n="HIAT:ip">,</nts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_1802" n="HIAT:w" s="T546">iti</ts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1805" n="HIAT:w" s="T547">povardarbɨt</ts>
                  <nts id="Seg_1806" n="HIAT:ip">.</nts>
                  <nts id="Seg_1807" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T563" id="Seg_1808" n="sc" s="T549">
               <ts e="T563" id="Seg_1810" n="HIAT:u" s="T549">
                  <ts e="T550" id="Seg_1812" n="HIAT:w" s="T549">Oččogo</ts>
                  <nts id="Seg_1813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_1815" n="HIAT:w" s="T550">ol</ts>
                  <nts id="Seg_1816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_1818" n="HIAT:w" s="T551">ojuːn</ts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_1821" n="HIAT:w" s="T552">emeːksin</ts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_1824" n="HIAT:w" s="T553">hirdeːn</ts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_1827" n="HIAT:w" s="T554">egelle</ts>
                  <nts id="Seg_1828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1829" n="HIAT:ip">(</nts>
                  <ts e="T556" id="Seg_1831" n="HIAT:w" s="T555">bihi͡eke</ts>
                  <nts id="Seg_1832" n="HIAT:ip">)</nts>
                  <nts id="Seg_1833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_1835" n="HIAT:w" s="T556">ehi͡eke</ts>
                  <nts id="Seg_1836" n="HIAT:ip">"</nts>
                  <nts id="Seg_1837" n="HIAT:ip">,</nts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_1840" n="HIAT:w" s="T557">di͡en</ts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_1843" n="HIAT:w" s="T558">ol</ts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_1846" n="HIAT:w" s="T559">kepsiːller</ts>
                  <nts id="Seg_1847" n="HIAT:ip">,</nts>
                  <nts id="Seg_1848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_1850" n="HIAT:w" s="T560">bu͡o</ts>
                  <nts id="Seg_1851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_1853" n="HIAT:w" s="T561">kihini</ts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_1856" n="HIAT:w" s="T562">barappɨttar</ts>
                  <nts id="Seg_1857" n="HIAT:ip">.</nts>
                  <nts id="Seg_1858" n="HIAT:ip">"</nts>
                  <nts id="Seg_1859" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T576" id="Seg_1860" n="sc" s="T564">
               <ts e="T576" id="Seg_1862" n="HIAT:u" s="T564">
                  <ts e="T565" id="Seg_1864" n="HIAT:w" s="T564">Kaːnnarɨn</ts>
                  <nts id="Seg_1865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_1867" n="HIAT:w" s="T565">bu͡olla</ts>
                  <nts id="Seg_1868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_1870" n="HIAT:w" s="T566">bahaktarɨn</ts>
                  <nts id="Seg_1871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_1873" n="HIAT:w" s="T567">munna</ts>
                  <nts id="Seg_1874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_1876" n="HIAT:w" s="T568">hotollor</ts>
                  <nts id="Seg_1877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_1879" n="HIAT:w" s="T569">ebit</ts>
                  <nts id="Seg_1880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_1882" n="HIAT:w" s="T570">kihileri</ts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_1885" n="HIAT:w" s="T571">kerde</ts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_1888" n="HIAT:w" s="T572">kerde</ts>
                  <nts id="Seg_1889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_1891" n="HIAT:w" s="T573">hiː</ts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_1894" n="HIAT:w" s="T574">hiː</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_1897" n="HIAT:w" s="T575">hiːller</ts>
                  <nts id="Seg_1898" n="HIAT:ip">.</nts>
                  <nts id="Seg_1899" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T589" id="Seg_1900" n="sc" s="T577">
               <ts e="T589" id="Seg_1902" n="HIAT:u" s="T577">
                  <nts id="Seg_1903" n="HIAT:ip">"</nts>
                  <ts e="T578" id="Seg_1905" n="HIAT:w" s="T577">Oː</ts>
                  <nts id="Seg_1906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_1908" n="HIAT:w" s="T578">tu͡ok</ts>
                  <nts id="Seg_1909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_1911" n="HIAT:w" s="T579">aːttaːk</ts>
                  <nts id="Seg_1912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_1914" n="HIAT:w" s="T580">kaːnɨgar</ts>
                  <nts id="Seg_1915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_1917" n="HIAT:w" s="T581">bihillibit</ts>
                  <nts id="Seg_1918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_1920" n="HIAT:w" s="T582">dʼonnorgutuj</ts>
                  <nts id="Seg_1921" n="HIAT:ip">,</nts>
                  <nts id="Seg_1922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_1924" n="HIAT:w" s="T583">dʼe</ts>
                  <nts id="Seg_1925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_1927" n="HIAT:w" s="T584">ahaːŋ</ts>
                  <nts id="Seg_1928" n="HIAT:ip">"</nts>
                  <nts id="Seg_1929" n="HIAT:ip">,</nts>
                  <nts id="Seg_1930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_1932" n="HIAT:w" s="T585">di͡en</ts>
                  <nts id="Seg_1933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_1935" n="HIAT:w" s="T586">ogonnʼor</ts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_1938" n="HIAT:w" s="T587">dʼe</ts>
                  <nts id="Seg_1939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_1941" n="HIAT:w" s="T588">ahatar</ts>
                  <nts id="Seg_1942" n="HIAT:ip">.</nts>
                  <nts id="Seg_1943" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T605" id="Seg_1944" n="sc" s="T590">
               <ts e="T605" id="Seg_1946" n="HIAT:u" s="T590">
                  <ts e="T591" id="Seg_1948" n="HIAT:w" s="T590">De</ts>
                  <nts id="Seg_1949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_1951" n="HIAT:w" s="T591">ol</ts>
                  <nts id="Seg_1952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1953" n="HIAT:ip">(</nts>
                  <ts e="T593" id="Seg_1955" n="HIAT:w" s="T592">ahaː-</ts>
                  <nts id="Seg_1956" n="HIAT:ip">)</nts>
                  <nts id="Seg_1957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_1959" n="HIAT:w" s="T593">ahappɨtɨn</ts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_1962" n="HIAT:w" s="T594">kenne</ts>
                  <nts id="Seg_1963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_1965" n="HIAT:w" s="T595">de</ts>
                  <nts id="Seg_1966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_1968" n="HIAT:w" s="T596">ol</ts>
                  <nts id="Seg_1969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_1971" n="HIAT:w" s="T597">oː</ts>
                  <nts id="Seg_1972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_1974" n="HIAT:w" s="T598">kallaːmmɨt</ts>
                  <nts id="Seg_1975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_1977" n="HIAT:w" s="T599">kajdak</ts>
                  <nts id="Seg_1978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_1980" n="HIAT:w" s="T600">kajdak</ts>
                  <nts id="Seg_1981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_1983" n="HIAT:w" s="T601">bu͡olar</ts>
                  <nts id="Seg_1984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_1986" n="HIAT:w" s="T602">de</ts>
                  <nts id="Seg_1987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_1989" n="HIAT:w" s="T603">bɨlɨppɨt</ts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_1992" n="HIAT:w" s="T604">kojdor</ts>
                  <nts id="Seg_1993" n="HIAT:ip">.</nts>
                  <nts id="Seg_1994" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T609" id="Seg_1995" n="sc" s="T606">
               <ts e="T609" id="Seg_1997" n="HIAT:u" s="T606">
                  <nts id="Seg_1998" n="HIAT:ip">"</nts>
                  <ts e="T607" id="Seg_2000" n="HIAT:w" s="T606">Tu͡ok</ts>
                  <nts id="Seg_2001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2003" n="HIAT:w" s="T607">bu͡olar</ts>
                  <nts id="Seg_2004" n="HIAT:ip">"</nts>
                  <nts id="Seg_2005" n="HIAT:ip">;</nts>
                  <nts id="Seg_2006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2008" n="HIAT:w" s="T608">di͡ebite</ts>
                  <nts id="Seg_2009" n="HIAT:ip">.</nts>
                  <nts id="Seg_2010" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T618" id="Seg_2011" n="sc" s="T610">
               <ts e="T618" id="Seg_2013" n="HIAT:u" s="T610">
                  <ts e="T611" id="Seg_2015" n="HIAT:w" s="T610">Dʼe</ts>
                  <nts id="Seg_2016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2018" n="HIAT:w" s="T611">ol</ts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2021" n="HIAT:w" s="T612">ikki</ts>
                  <nts id="Seg_2022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2024" n="HIAT:w" s="T613">kamnastaːgɨ</ts>
                  <nts id="Seg_2025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2027" n="HIAT:w" s="T614">ikki</ts>
                  <nts id="Seg_2028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2030" n="HIAT:w" s="T615">öttüge</ts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2033" n="HIAT:w" s="T616">olorput</ts>
                  <nts id="Seg_2034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2036" n="HIAT:w" s="T617">bu͡o</ts>
                  <nts id="Seg_2037" n="HIAT:ip">.</nts>
                  <nts id="Seg_2038" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T627" id="Seg_2039" n="sc" s="T619">
               <ts e="T627" id="Seg_2041" n="HIAT:u" s="T619">
                  <ts e="T620" id="Seg_2043" n="HIAT:w" s="T619">Dʼe</ts>
                  <nts id="Seg_2044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2046" n="HIAT:w" s="T620">ol</ts>
                  <nts id="Seg_2047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2049" n="HIAT:w" s="T621">kantas</ts>
                  <nts id="Seg_2050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2052" n="HIAT:w" s="T622">gɨmmɨtɨgar</ts>
                  <nts id="Seg_2053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2055" n="HIAT:w" s="T623">kabɨrgatɨn</ts>
                  <nts id="Seg_2056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2058" n="HIAT:w" s="T624">kaja</ts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2061" n="HIAT:w" s="T625">oksubuttara</ts>
                  <nts id="Seg_2062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2064" n="HIAT:w" s="T626">batɨjanan</ts>
                  <nts id="Seg_2065" n="HIAT:ip">.</nts>
                  <nts id="Seg_2066" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T634" id="Seg_2067" n="sc" s="T628">
               <ts e="T634" id="Seg_2069" n="HIAT:u" s="T628">
                  <ts e="T629" id="Seg_2071" n="HIAT:w" s="T628">Anɨ</ts>
                  <nts id="Seg_2072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2074" n="HIAT:w" s="T629">iti</ts>
                  <nts id="Seg_2075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2077" n="HIAT:w" s="T630">ölörsöllörüger</ts>
                  <nts id="Seg_2078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2080" n="HIAT:w" s="T631">diːr</ts>
                  <nts id="Seg_2081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2083" n="HIAT:w" s="T632">kinoːlarga</ts>
                  <nts id="Seg_2084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2086" n="HIAT:w" s="T633">di͡eri</ts>
                  <nts id="Seg_2087" n="HIAT:ip">.</nts>
                  <nts id="Seg_2088" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T639" id="Seg_2089" n="sc" s="T635">
               <ts e="T639" id="Seg_2091" n="HIAT:u" s="T635">
                  <ts e="T636" id="Seg_2093" n="HIAT:w" s="T635">Ol</ts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2096" n="HIAT:w" s="T636">kɨ͡ajbɨttar</ts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2099" n="HIAT:w" s="T637">ühü</ts>
                  <nts id="Seg_2100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2102" n="HIAT:w" s="T638">ontularɨn</ts>
                  <nts id="Seg_2103" n="HIAT:ip">.</nts>
                  <nts id="Seg_2104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T643" id="Seg_2105" n="sc" s="T640">
               <ts e="T643" id="Seg_2107" n="HIAT:u" s="T640">
                  <ts e="T641" id="Seg_2109" n="HIAT:w" s="T640">Dʼe</ts>
                  <nts id="Seg_2110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2112" n="HIAT:w" s="T641">aŋardarɨn</ts>
                  <nts id="Seg_2113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2115" n="HIAT:w" s="T642">ölörtöːbüttere</ts>
                  <nts id="Seg_2116" n="HIAT:ip">.</nts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T650" id="Seg_2118" n="sc" s="T644">
               <ts e="T650" id="Seg_2120" n="HIAT:u" s="T644">
                  <ts e="T645" id="Seg_2122" n="HIAT:w" s="T644">Hette</ts>
                  <nts id="Seg_2123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2125" n="HIAT:w" s="T645">kihite</ts>
                  <nts id="Seg_2126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2128" n="HIAT:w" s="T646">ebit</ts>
                  <nts id="Seg_2129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2131" n="HIAT:w" s="T647">du͡o</ts>
                  <nts id="Seg_2132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2134" n="HIAT:w" s="T648">kas</ts>
                  <nts id="Seg_2135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2137" n="HIAT:w" s="T649">du͡o</ts>
                  <nts id="Seg_2138" n="HIAT:ip">.</nts>
                  <nts id="Seg_2139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T662" id="Seg_2140" n="sc" s="T651">
               <ts e="T662" id="Seg_2142" n="HIAT:u" s="T651">
                  <ts e="T652" id="Seg_2144" n="HIAT:w" s="T651">Dʼe</ts>
                  <nts id="Seg_2145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2147" n="HIAT:w" s="T652">onton</ts>
                  <nts id="Seg_2148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2150" n="HIAT:w" s="T653">dʼaktattar</ts>
                  <nts id="Seg_2151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2153" n="HIAT:w" s="T654">kaːlbɨttarɨgar</ts>
                  <nts id="Seg_2154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2156" n="HIAT:w" s="T655">bu</ts>
                  <nts id="Seg_2157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2159" n="HIAT:w" s="T656">dʼaktattar</ts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2162" n="HIAT:w" s="T657">dʼe</ts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2165" n="HIAT:w" s="T658">etiheller</ts>
                  <nts id="Seg_2166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2168" n="HIAT:w" s="T659">ühü</ts>
                  <nts id="Seg_2169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2171" n="HIAT:w" s="T660">bahaktarɨn</ts>
                  <nts id="Seg_2172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2174" n="HIAT:w" s="T661">bɨldʼɨhannar</ts>
                  <nts id="Seg_2175" n="HIAT:ip">.</nts>
                  <nts id="Seg_2176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T672" id="Seg_2177" n="sc" s="T663">
               <ts e="T672" id="Seg_2179" n="HIAT:u" s="T663">
                  <nts id="Seg_2180" n="HIAT:ip">"</nts>
                  <ts e="T664" id="Seg_2182" n="HIAT:w" s="T663">Min</ts>
                  <nts id="Seg_2183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2185" n="HIAT:w" s="T664">erim</ts>
                  <nts id="Seg_2186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2188" n="HIAT:w" s="T665">bahaga</ts>
                  <nts id="Seg_2189" n="HIAT:ip">,</nts>
                  <nts id="Seg_2190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2192" n="HIAT:w" s="T666">min</ts>
                  <nts id="Seg_2193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2195" n="HIAT:w" s="T667">erim</ts>
                  <nts id="Seg_2196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2198" n="HIAT:w" s="T668">bahaga</ts>
                  <nts id="Seg_2199" n="HIAT:ip">"</nts>
                  <nts id="Seg_2200" n="HIAT:ip">,</nts>
                  <nts id="Seg_2201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2203" n="HIAT:w" s="T669">deheːktiːller</ts>
                  <nts id="Seg_2204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2206" n="HIAT:w" s="T670">ühü</ts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2209" n="HIAT:w" s="T671">kotokular</ts>
                  <nts id="Seg_2210" n="HIAT:ip">.</nts>
                  <nts id="Seg_2211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T688" id="Seg_2212" n="sc" s="T673">
               <ts e="T688" id="Seg_2214" n="HIAT:u" s="T673">
                  <nts id="Seg_2215" n="HIAT:ip">"</nts>
                  <ts e="T674" id="Seg_2217" n="HIAT:w" s="T673">Tu͡ok</ts>
                  <nts id="Seg_2218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2220" n="HIAT:w" s="T674">suːka</ts>
                  <nts id="Seg_2221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2223" n="HIAT:w" s="T675">kɨrgɨttaraj</ts>
                  <nts id="Seg_2224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2226" n="HIAT:w" s="T676">itileri</ts>
                  <nts id="Seg_2227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2229" n="HIAT:w" s="T677">ölöttöːn</ts>
                  <nts id="Seg_2230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2232" n="HIAT:w" s="T678">keːhiŋ</ts>
                  <nts id="Seg_2233" n="HIAT:ip">"</nts>
                  <nts id="Seg_2234" n="HIAT:ip">,</nts>
                  <nts id="Seg_2235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2237" n="HIAT:w" s="T679">di͡ebit</ts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2240" n="HIAT:w" s="T680">bu͡o</ts>
                  <nts id="Seg_2241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2243" n="HIAT:w" s="T681">tu͡ok</ts>
                  <nts id="Seg_2244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2246" n="HIAT:w" s="T682">sustogoj</ts>
                  <nts id="Seg_2247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2249" n="HIAT:w" s="T683">ogonnʼoro</ts>
                  <nts id="Seg_2250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2252" n="HIAT:w" s="T684">bu͡olla</ts>
                  <nts id="Seg_2253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2255" n="HIAT:w" s="T685">da</ts>
                  <nts id="Seg_2256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2258" n="HIAT:w" s="T686">bihi͡ene</ts>
                  <nts id="Seg_2259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2261" n="HIAT:w" s="T687">Pu͡orotaj</ts>
                  <nts id="Seg_2262" n="HIAT:ip">.</nts>
                  <nts id="Seg_2263" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T692" id="Seg_2264" n="sc" s="T689">
               <ts e="T692" id="Seg_2266" n="HIAT:u" s="T689">
                  <ts e="T690" id="Seg_2268" n="HIAT:w" s="T689">Ölöttöːn</ts>
                  <nts id="Seg_2269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2271" n="HIAT:w" s="T690">keːspitter</ts>
                  <nts id="Seg_2272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2274" n="HIAT:w" s="T691">muŋnaːktar</ts>
                  <nts id="Seg_2275" n="HIAT:ip">.</nts>
                  <nts id="Seg_2276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T701" id="Seg_2277" n="sc" s="T693">
               <ts e="T701" id="Seg_2279" n="HIAT:u" s="T693">
                  <ts e="T694" id="Seg_2281" n="HIAT:w" s="T693">Onton</ts>
                  <nts id="Seg_2282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2284" n="HIAT:w" s="T694">ol</ts>
                  <nts id="Seg_2285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2287" n="HIAT:w" s="T695">emeːksini</ts>
                  <nts id="Seg_2288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2290" n="HIAT:w" s="T696">dʼe</ts>
                  <nts id="Seg_2291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2293" n="HIAT:w" s="T697">ölörö</ts>
                  <nts id="Seg_2294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2296" n="HIAT:w" s="T698">hatɨːllar</ts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2299" n="HIAT:w" s="T699">ojuːn</ts>
                  <nts id="Seg_2300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2302" n="HIAT:w" s="T700">emeːksini</ts>
                  <nts id="Seg_2303" n="HIAT:ip">.</nts>
                  <nts id="Seg_2304" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T708" id="Seg_2305" n="sc" s="T702">
               <ts e="T708" id="Seg_2307" n="HIAT:u" s="T702">
                  <ts e="T703" id="Seg_2309" n="HIAT:w" s="T702">Uːga</ts>
                  <nts id="Seg_2310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2312" n="HIAT:w" s="T703">da</ts>
                  <nts id="Seg_2313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2315" n="HIAT:w" s="T704">timirdeller</ts>
                  <nts id="Seg_2316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2318" n="HIAT:w" s="T705">dʼe</ts>
                  <nts id="Seg_2319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2321" n="HIAT:w" s="T706">hakordaːn</ts>
                  <nts id="Seg_2322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2324" n="HIAT:w" s="T707">baran</ts>
                  <nts id="Seg_2325" n="HIAT:ip">.</nts>
                  <nts id="Seg_2326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T711" id="Seg_2327" n="sc" s="T709">
               <ts e="T711" id="Seg_2329" n="HIAT:u" s="T709">
                  <ts e="T710" id="Seg_2331" n="HIAT:w" s="T709">Törüt</ts>
                  <nts id="Seg_2332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2334" n="HIAT:w" s="T710">timirbet</ts>
                  <nts id="Seg_2335" n="HIAT:ip">.</nts>
                  <nts id="Seg_2336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T720" id="Seg_2337" n="sc" s="T712">
               <ts e="T720" id="Seg_2339" n="HIAT:u" s="T712">
                  <ts e="T713" id="Seg_2341" n="HIAT:w" s="T712">Ajagalɨː</ts>
                  <nts id="Seg_2342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2344" n="HIAT:w" s="T713">hataːn</ts>
                  <nts id="Seg_2345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2347" n="HIAT:w" s="T714">u͡otun</ts>
                  <nts id="Seg_2348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2350" n="HIAT:w" s="T715">otton</ts>
                  <nts id="Seg_2351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2353" n="HIAT:w" s="T716">bu͡o</ts>
                  <nts id="Seg_2354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2356" n="HIAT:w" s="T717">u͡ot</ts>
                  <nts id="Seg_2357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2359" n="HIAT:w" s="T718">ihiger</ts>
                  <nts id="Seg_2360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2362" n="HIAT:w" s="T719">bɨragallar</ts>
                  <nts id="Seg_2363" n="HIAT:ip">.</nts>
                  <nts id="Seg_2364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T725" id="Seg_2365" n="sc" s="T721">
               <ts e="T725" id="Seg_2367" n="HIAT:u" s="T721">
                  <ts e="T722" id="Seg_2369" n="HIAT:w" s="T721">U͡ottara</ts>
                  <nts id="Seg_2370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2372" n="HIAT:w" s="T722">karaːrčɨ</ts>
                  <nts id="Seg_2373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2375" n="HIAT:w" s="T723">utujar</ts>
                  <nts id="Seg_2376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2378" n="HIAT:w" s="T724">ühü</ts>
                  <nts id="Seg_2379" n="HIAT:ip">.</nts>
                  <nts id="Seg_2380" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T738" id="Seg_2381" n="sc" s="T726">
               <ts e="T730" id="Seg_2383" n="HIAT:u" s="T726">
                  <ts e="T727" id="Seg_2385" n="HIAT:w" s="T726">Onno</ts>
                  <nts id="Seg_2386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2388" n="HIAT:w" s="T727">da</ts>
                  <nts id="Seg_2389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2391" n="HIAT:w" s="T728">di͡ebit</ts>
                  <nts id="Seg_2392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2394" n="HIAT:w" s="T729">bu͡o</ts>
                  <nts id="Seg_2395" n="HIAT:ip">:</nts>
                  <nts id="Seg_2396" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T738" id="Seg_2398" n="HIAT:u" s="T730">
                  <nts id="Seg_2399" n="HIAT:ip">"</nts>
                  <ts e="T731" id="Seg_2401" n="HIAT:w" s="T730">Tukuːlar</ts>
                  <nts id="Seg_2402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2404" n="HIAT:w" s="T731">min</ts>
                  <nts id="Seg_2405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2407" n="HIAT:w" s="T732">kün</ts>
                  <nts id="Seg_2408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2410" n="HIAT:w" s="T733">emeːksin</ts>
                  <nts id="Seg_2411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2413" n="HIAT:w" s="T734">etim</ts>
                  <nts id="Seg_2414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2416" n="HIAT:w" s="T735">kaja</ts>
                  <nts id="Seg_2417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2419" n="HIAT:w" s="T736">abaːhɨ</ts>
                  <nts id="Seg_2420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2422" n="HIAT:w" s="T737">bu͡oltappɨn</ts>
                  <nts id="Seg_2423" n="HIAT:ip">.</nts>
                  <nts id="Seg_2424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T748" id="Seg_2425" n="sc" s="T739">
               <ts e="T748" id="Seg_2427" n="HIAT:u" s="T739">
                  <ts e="T740" id="Seg_2429" n="HIAT:w" s="T739">Dʼommun</ts>
                  <nts id="Seg_2430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2432" n="HIAT:w" s="T740">baraːbɨttara</ts>
                  <nts id="Seg_2433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2435" n="HIAT:w" s="T741">bu</ts>
                  <nts id="Seg_2436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2438" n="HIAT:w" s="T742">bu͡olla</ts>
                  <nts id="Seg_2439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2441" n="HIAT:w" s="T743">kihini</ts>
                  <nts id="Seg_2442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2444" n="HIAT:w" s="T744">sü͡öhünü</ts>
                  <nts id="Seg_2445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2447" n="HIAT:w" s="T745">köröːrübün</ts>
                  <nts id="Seg_2448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2450" n="HIAT:w" s="T746">hirdeːn</ts>
                  <nts id="Seg_2451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2453" n="HIAT:w" s="T747">kelbitim</ts>
                  <nts id="Seg_2454" n="HIAT:ip">.</nts>
                  <nts id="Seg_2455" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T757" id="Seg_2456" n="sc" s="T749">
               <ts e="T757" id="Seg_2458" n="HIAT:u" s="T749">
                  <ts e="T750" id="Seg_2460" n="HIAT:w" s="T749">Minigin</ts>
                  <nts id="Seg_2461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2463" n="HIAT:w" s="T750">erejdeːmeŋ</ts>
                  <nts id="Seg_2464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_2466" n="HIAT:w" s="T751">min</ts>
                  <nts id="Seg_2467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2469" n="HIAT:w" s="T752">itinnik</ts>
                  <nts id="Seg_2470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_2472" n="HIAT:w" s="T753">ke</ts>
                  <nts id="Seg_2473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_2475" n="HIAT:w" s="T754">ölü͡öm</ts>
                  <nts id="Seg_2476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2478" n="HIAT:w" s="T755">hu͡oga</ts>
                  <nts id="Seg_2479" n="HIAT:ip">"</nts>
                  <nts id="Seg_2480" n="HIAT:ip">,</nts>
                  <nts id="Seg_2481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2483" n="HIAT:w" s="T756">diːr</ts>
                  <nts id="Seg_2484" n="HIAT:ip">.</nts>
                  <nts id="Seg_2485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T773" id="Seg_2486" n="sc" s="T758">
               <ts e="T773" id="Seg_2488" n="HIAT:u" s="T758">
                  <nts id="Seg_2489" n="HIAT:ip">"</nts>
                  <ts e="T759" id="Seg_2491" n="HIAT:w" s="T758">Kɨːh</ts>
                  <nts id="Seg_2492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_2494" n="HIAT:w" s="T759">ogoto</ts>
                  <nts id="Seg_2495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_2497" n="HIAT:w" s="T760">haŋardɨː</ts>
                  <nts id="Seg_2498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_2500" n="HIAT:w" s="T761">bɨrtak</ts>
                  <nts id="Seg_2501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_2503" n="HIAT:w" s="T762">bu͡olbut</ts>
                  <nts id="Seg_2504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_2506" n="HIAT:w" s="T763">kɨːh</ts>
                  <nts id="Seg_2507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2509" n="HIAT:w" s="T764">ogoto</ts>
                  <nts id="Seg_2510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_2512" n="HIAT:w" s="T765">moːjbunan</ts>
                  <nts id="Seg_2513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_2515" n="HIAT:w" s="T766">atɨllatɨn</ts>
                  <nts id="Seg_2516" n="HIAT:ip">,</nts>
                  <nts id="Seg_2517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_2519" n="HIAT:w" s="T767">oččogo</ts>
                  <nts id="Seg_2520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_2522" n="HIAT:w" s="T768">ölü͡öm</ts>
                  <nts id="Seg_2523" n="HIAT:ip">"</nts>
                  <nts id="Seg_2524" n="HIAT:ip">,</nts>
                  <nts id="Seg_2525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_2527" n="HIAT:w" s="T769">diːr</ts>
                  <nts id="Seg_2528" n="HIAT:ip">,</nts>
                  <nts id="Seg_2529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2530" n="HIAT:ip">"</nts>
                  <ts e="T771" id="Seg_2532" n="HIAT:w" s="T770">ol</ts>
                  <nts id="Seg_2533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_2535" n="HIAT:w" s="T771">moːjbunan</ts>
                  <nts id="Seg_2536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_2538" n="HIAT:w" s="T772">atɨllappɨttar</ts>
                  <nts id="Seg_2539" n="HIAT:ip">.</nts>
                  <nts id="Seg_2540" n="HIAT:ip">"</nts>
                  <nts id="Seg_2541" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T784" id="Seg_2542" n="sc" s="T774">
               <ts e="T779" id="Seg_2544" n="HIAT:u" s="T774">
                  <ts e="T775" id="Seg_2546" n="HIAT:w" s="T774">De</ts>
                  <nts id="Seg_2547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2549" n="HIAT:w" s="T775">ol</ts>
                  <nts id="Seg_2550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_2552" n="HIAT:w" s="T776">emeːksinnere</ts>
                  <nts id="Seg_2553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_2555" n="HIAT:w" s="T777">ölön</ts>
                  <nts id="Seg_2556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_2558" n="HIAT:w" s="T778">kaːlbɨt</ts>
                  <nts id="Seg_2559" n="HIAT:ip">.</nts>
                  <nts id="Seg_2560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T784" id="Seg_2562" n="HIAT:u" s="T779">
                  <ts e="T780" id="Seg_2564" n="HIAT:w" s="T779">Dʼe</ts>
                  <nts id="Seg_2565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_2567" n="HIAT:w" s="T780">elete</ts>
                  <nts id="Seg_2568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_2570" n="HIAT:w" s="T781">ol</ts>
                  <nts id="Seg_2571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_2573" n="HIAT:w" s="T782">kepseːbittere</ts>
                  <nts id="Seg_2574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_2576" n="HIAT:w" s="T783">öjdöːböppün</ts>
                  <nts id="Seg_2577" n="HIAT:ip">.</nts>
                  <nts id="Seg_2578" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T10" id="Seg_2579" n="sc" s="T1">
               <ts e="T2" id="Seg_2581" n="e" s="T1">Onton </ts>
               <ts e="T3" id="Seg_2583" n="e" s="T2">biːrde </ts>
               <ts e="T4" id="Seg_2585" n="e" s="T3">bultana </ts>
               <ts e="T5" id="Seg_2587" n="e" s="T4">taksɨbɨttara </ts>
               <ts e="T6" id="Seg_2589" n="e" s="T5">ogonnʼottor </ts>
               <ts e="T7" id="Seg_2591" n="e" s="T6">(emeːk-) </ts>
               <ts e="T8" id="Seg_2593" n="e" s="T7">ogonnʼottor </ts>
               <ts e="T9" id="Seg_2595" n="e" s="T8">keleller </ts>
               <ts e="T10" id="Seg_2597" n="e" s="T9">u͡olattar. </ts>
            </ts>
            <ts e="T16" id="Seg_2598" n="sc" s="T11">
               <ts e="T12" id="Seg_2600" n="e" s="T11">"Kaja, </ts>
               <ts e="T13" id="Seg_2602" n="e" s="T12">Hɨndaːsskabɨtɨgar </ts>
               <ts e="T14" id="Seg_2604" n="e" s="T13">dʼi͡e </ts>
               <ts e="T15" id="Seg_2606" n="e" s="T14">turbut", </ts>
               <ts e="T16" id="Seg_2608" n="e" s="T15">di͡ebitter. </ts>
            </ts>
            <ts e="T19" id="Seg_2609" n="sc" s="T17">
               <ts e="T18" id="Seg_2611" n="e" s="T17">"Kajdi͡ek </ts>
               <ts e="T19" id="Seg_2613" n="e" s="T18">bultanaːččɨbɨt?" </ts>
            </ts>
            <ts e="T31" id="Seg_2614" n="sc" s="T20">
               <ts e="T21" id="Seg_2616" n="e" s="T20">Oː </ts>
               <ts e="T22" id="Seg_2618" n="e" s="T21">dʼe, </ts>
               <ts e="T23" id="Seg_2620" n="e" s="T22">bu </ts>
               <ts e="T24" id="Seg_2622" n="e" s="T23">dʼi͡e </ts>
               <ts e="T25" id="Seg_2624" n="e" s="T24">turbutugar </ts>
               <ts e="T26" id="Seg_2626" n="e" s="T25">kuttanallar </ts>
               <ts e="T27" id="Seg_2628" n="e" s="T26">keli͡ekterin </ts>
               <ts e="T28" id="Seg_2630" n="e" s="T27">ühü, </ts>
               <ts e="T29" id="Seg_2632" n="e" s="T28">nuːččalar </ts>
               <ts e="T30" id="Seg_2634" n="e" s="T29">baːllar </ts>
               <ts e="T31" id="Seg_2636" n="e" s="T30">di͡en. </ts>
            </ts>
            <ts e="T46" id="Seg_2637" n="sc" s="T32">
               <ts e="T33" id="Seg_2639" n="e" s="T32">"Oː </ts>
               <ts e="T34" id="Seg_2641" n="e" s="T33">dʼe, </ts>
               <ts e="T35" id="Seg_2643" n="e" s="T34">bu </ts>
               <ts e="T36" id="Seg_2645" n="e" s="T35">hirbitin </ts>
               <ts e="T37" id="Seg_2647" n="e" s="T36">bulta </ts>
               <ts e="T38" id="Seg_2649" n="e" s="T37">hu͡ok </ts>
               <ts e="T39" id="Seg_2651" n="e" s="T38">oŋordular, </ts>
               <ts e="T40" id="Seg_2653" n="e" s="T39">kajdak </ts>
               <ts e="T41" id="Seg_2655" n="e" s="T40">bu͡olaːččɨbɨtɨj", </ts>
               <ts e="T42" id="Seg_2657" n="e" s="T41">diːller </ts>
               <ts e="T43" id="Seg_2659" n="e" s="T42">ühü </ts>
               <ts e="T44" id="Seg_2661" n="e" s="T43">ol </ts>
               <ts e="T45" id="Seg_2663" n="e" s="T44">ogonnʼottor </ts>
               <ts e="T46" id="Seg_2665" n="e" s="T45">ke. </ts>
            </ts>
            <ts e="T49" id="Seg_2666" n="sc" s="T47">
               <ts e="T48" id="Seg_2668" n="e" s="T47">"Dehebit", </ts>
               <ts e="T49" id="Seg_2670" n="e" s="T48">diːller. </ts>
            </ts>
            <ts e="T54" id="Seg_2671" n="sc" s="T50">
               <ts e="T51" id="Seg_2673" n="e" s="T50">Kepsetellerin </ts>
               <ts e="T52" id="Seg_2675" n="e" s="T51">istebin </ts>
               <ts e="T53" id="Seg_2677" n="e" s="T52">ehelerim </ts>
               <ts e="T54" id="Seg_2679" n="e" s="T53">ke. </ts>
            </ts>
            <ts e="T59" id="Seg_2680" n="sc" s="T55">
               <ts e="T56" id="Seg_2682" n="e" s="T55">Onton, </ts>
               <ts e="T57" id="Seg_2684" n="e" s="T56">dʼe </ts>
               <ts e="T58" id="Seg_2686" n="e" s="T57">onton </ts>
               <ts e="T59" id="Seg_2688" n="e" s="T58">keleller. </ts>
            </ts>
            <ts e="T66" id="Seg_2689" n="sc" s="T60">
               <ts e="T61" id="Seg_2691" n="e" s="T60">Kelenner </ts>
               <ts e="T62" id="Seg_2693" n="e" s="T61">körbüttere </ts>
               <ts e="T63" id="Seg_2695" n="e" s="T62">ikki </ts>
               <ts e="T64" id="Seg_2697" n="e" s="T63">kihi </ts>
               <ts e="T65" id="Seg_2699" n="e" s="T64">hɨldʼallar </ts>
               <ts e="T66" id="Seg_2701" n="e" s="T65">ühü. </ts>
            </ts>
            <ts e="T70" id="Seg_2702" n="sc" s="T67">
               <ts e="T68" id="Seg_2704" n="e" s="T67">Dʼaktarɨn </ts>
               <ts e="T69" id="Seg_2706" n="e" s="T68">aːta </ts>
               <ts e="T70" id="Seg_2708" n="e" s="T69">Daːrija. </ts>
            </ts>
            <ts e="T85" id="Seg_2709" n="sc" s="T71">
               <ts e="T72" id="Seg_2711" n="e" s="T71">Erin </ts>
               <ts e="T73" id="Seg_2713" n="e" s="T72">aːta </ts>
               <ts e="T74" id="Seg_2715" n="e" s="T73">kim </ts>
               <ts e="T75" id="Seg_2717" n="e" s="T74">di͡ebitterij </ts>
               <ts e="T76" id="Seg_2719" n="e" s="T75">ke </ts>
               <ts e="T77" id="Seg_2721" n="e" s="T76">da </ts>
               <ts e="T78" id="Seg_2723" n="e" s="T77">oluja </ts>
               <ts e="T79" id="Seg_2725" n="e" s="T78">bagajɨ </ts>
               <ts e="T80" id="Seg_2727" n="e" s="T79">aːttaːk </ts>
               <ts e="T81" id="Seg_2729" n="e" s="T80">kihi, </ts>
               <ts e="T82" id="Seg_2731" n="e" s="T81">kim </ts>
               <ts e="T83" id="Seg_2733" n="e" s="T82">ere </ts>
               <ts e="T84" id="Seg_2735" n="e" s="T83">di͡eččiler </ts>
               <ts e="T85" id="Seg_2737" n="e" s="T84">et. </ts>
            </ts>
            <ts e="T89" id="Seg_2738" n="sc" s="T86">
               <ts e="T87" id="Seg_2740" n="e" s="T86">Törüt </ts>
               <ts e="T88" id="Seg_2742" n="e" s="T87">öjdöːböppün </ts>
               <ts e="T89" id="Seg_2744" n="e" s="T88">aːtɨn. </ts>
            </ts>
            <ts e="T98" id="Seg_2745" n="sc" s="T90">
               <ts e="T91" id="Seg_2747" n="e" s="T90">Onuga </ts>
               <ts e="T92" id="Seg_2749" n="e" s="T91">bu͡o </ts>
               <ts e="T93" id="Seg_2751" n="e" s="T92">čugahɨːllar </ts>
               <ts e="T94" id="Seg_2753" n="e" s="T93">ühü </ts>
               <ts e="T95" id="Seg_2755" n="e" s="T94">kuttana </ts>
               <ts e="T96" id="Seg_2757" n="e" s="T95">kuttana </ts>
               <ts e="T97" id="Seg_2759" n="e" s="T96">čugahɨːllar </ts>
               <ts e="T98" id="Seg_2761" n="e" s="T97">ühü. </ts>
            </ts>
            <ts e="T104" id="Seg_2762" n="sc" s="T99">
               <ts e="T100" id="Seg_2764" n="e" s="T99">"Bu </ts>
               <ts e="T101" id="Seg_2766" n="e" s="T100">togo </ts>
               <ts e="T102" id="Seg_2768" n="e" s="T101">kelbikkitij </ts>
               <ts e="T103" id="Seg_2770" n="e" s="T102">bihigi </ts>
               <ts e="T104" id="Seg_2772" n="e" s="T103">hirbitiger? </ts>
            </ts>
            <ts e="T107" id="Seg_2773" n="sc" s="T105">
               <ts e="T106" id="Seg_2775" n="e" s="T105">Togo </ts>
               <ts e="T107" id="Seg_2777" n="e" s="T106">bultugut? </ts>
            </ts>
            <ts e="T115" id="Seg_2778" n="sc" s="T108">
               <ts e="T109" id="Seg_2780" n="e" s="T108">Bultanar </ts>
               <ts e="T110" id="Seg_2782" n="e" s="T109">hirbitin </ts>
               <ts e="T111" id="Seg_2784" n="e" s="T110">togo </ts>
               <ts e="T112" id="Seg_2786" n="e" s="T111">aldʼatagɨt", </ts>
               <ts e="T113" id="Seg_2788" n="e" s="T112">diːller </ts>
               <ts e="T114" id="Seg_2790" n="e" s="T113">ühü </ts>
               <ts e="T115" id="Seg_2792" n="e" s="T114">ogonnʼottor. </ts>
            </ts>
            <ts e="T123" id="Seg_2793" n="sc" s="T116">
               <ts e="T117" id="Seg_2795" n="e" s="T116">Kaja </ts>
               <ts e="T118" id="Seg_2797" n="e" s="T117">bunna </ts>
               <ts e="T119" id="Seg_2799" n="e" s="T118">bu͡ollagɨna </ts>
               <ts e="T120" id="Seg_2801" n="e" s="T119">naselenije </ts>
               <ts e="T121" id="Seg_2803" n="e" s="T120">oŋoru͡oktara, </ts>
               <ts e="T122" id="Seg_2805" n="e" s="T121">kihi </ts>
               <ts e="T123" id="Seg_2807" n="e" s="T122">turu͡oga. </ts>
            </ts>
            <ts e="T130" id="Seg_2808" n="sc" s="T124">
               <ts e="T125" id="Seg_2810" n="e" s="T124">Kihi </ts>
               <ts e="T126" id="Seg_2812" n="e" s="T125">ü͡öskü͡öge </ts>
               <ts e="T127" id="Seg_2814" n="e" s="T126">munna </ts>
               <ts e="T128" id="Seg_2816" n="e" s="T127">elbek </ts>
               <ts e="T129" id="Seg_2818" n="e" s="T128">kihi </ts>
               <ts e="T130" id="Seg_2820" n="e" s="T129">oloru͡oga. </ts>
            </ts>
            <ts e="T138" id="Seg_2821" n="sc" s="T131">
               <ts e="T132" id="Seg_2823" n="e" s="T131">Oččogo </ts>
               <ts e="T133" id="Seg_2825" n="e" s="T132">peričalbɨt </ts>
               <ts e="T134" id="Seg_2827" n="e" s="T133">bu͡o </ts>
               <ts e="T135" id="Seg_2829" n="e" s="T134">iti </ts>
               <ts e="T136" id="Seg_2831" n="e" s="T135">baːr </ts>
               <ts e="T137" id="Seg_2833" n="e" s="T136">hurbujan </ts>
               <ts e="T138" id="Seg_2835" n="e" s="T137">kiːrbit. </ts>
            </ts>
            <ts e="T144" id="Seg_2836" n="sc" s="T139">
               <ts e="T140" id="Seg_2838" n="e" s="T139">"Togo </ts>
               <ts e="T141" id="Seg_2840" n="e" s="T140">ɨraːgaj", </ts>
               <ts e="T142" id="Seg_2842" n="e" s="T141">diːller </ts>
               <ts e="T143" id="Seg_2844" n="e" s="T142">ühü, </ts>
               <ts e="T144" id="Seg_2846" n="e" s="T143">"peričalbɨt?" </ts>
            </ts>
            <ts e="T153" id="Seg_2847" n="sc" s="T145">
               <ts e="T146" id="Seg_2849" n="e" s="T145">Ü͡ösküt </ts>
               <ts e="T147" id="Seg_2851" n="e" s="T146">küččügüj </ts>
               <ts e="T148" id="Seg_2853" n="e" s="T147">munna, </ts>
               <ts e="T149" id="Seg_2855" n="e" s="T148">ol </ts>
               <ts e="T150" id="Seg_2857" n="e" s="T149">ihin </ts>
               <ts e="T151" id="Seg_2859" n="e" s="T150">itinne </ts>
               <ts e="T152" id="Seg_2861" n="e" s="T151">bu͡olar </ts>
               <ts e="T153" id="Seg_2863" n="e" s="T152">ü͡ösküt. </ts>
            </ts>
            <ts e="T157" id="Seg_2864" n="sc" s="T154">
               <ts e="T155" id="Seg_2866" n="e" s="T154">Itinne </ts>
               <ts e="T156" id="Seg_2868" n="e" s="T155">parahu͡ottar </ts>
               <ts e="T157" id="Seg_2870" n="e" s="T156">keli͡ektere. </ts>
            </ts>
            <ts e="T165" id="Seg_2871" n="sc" s="T158">
               <ts e="T159" id="Seg_2873" n="e" s="T158">Oduːrguːllar </ts>
               <ts e="T160" id="Seg_2875" n="e" s="T159">ühü </ts>
               <ts e="T161" id="Seg_2877" n="e" s="T160">"tu͡ok </ts>
               <ts e="T162" id="Seg_2879" n="e" s="T161">aːtaj </ts>
               <ts e="T163" id="Seg_2881" n="e" s="T162">parahu͡ot" </ts>
               <ts e="T164" id="Seg_2883" n="e" s="T163">diːr </ts>
               <ts e="T165" id="Seg_2885" n="e" s="T164">((LAUGH)). </ts>
            </ts>
            <ts e="T169" id="Seg_2886" n="sc" s="T166">
               <ts e="T167" id="Seg_2888" n="e" s="T166">Tartar </ts>
               <ts e="T168" id="Seg_2890" n="e" s="T167">keler </ts>
               <ts e="T169" id="Seg_2892" n="e" s="T168">parahu͡ot? </ts>
            </ts>
            <ts e="T184" id="Seg_2893" n="sc" s="T170">
               <ts e="T171" id="Seg_2895" n="e" s="T170">Dʼe </ts>
               <ts e="T172" id="Seg_2897" n="e" s="T171">ol </ts>
               <ts e="T173" id="Seg_2899" n="e" s="T172">kojut </ts>
               <ts e="T174" id="Seg_2901" n="e" s="T173">kojut </ts>
               <ts e="T175" id="Seg_2903" n="e" s="T174">kojut </ts>
               <ts e="T176" id="Seg_2905" n="e" s="T175">kojut </ts>
               <ts e="T177" id="Seg_2907" n="e" s="T176">ol </ts>
               <ts e="T178" id="Seg_2909" n="e" s="T177">ü͡öskeːn </ts>
               <ts e="T179" id="Seg_2911" n="e" s="T178">ü͡öskeːn </ts>
               <ts e="T180" id="Seg_2913" n="e" s="T179">bu </ts>
               <ts e="T181" id="Seg_2915" n="e" s="T180">Hɨndaːsskaŋ </ts>
               <ts e="T182" id="Seg_2917" n="e" s="T181">bu͡olan </ts>
               <ts e="T183" id="Seg_2919" n="e" s="T182">kaːlbɨt </ts>
               <ts e="T184" id="Seg_2921" n="e" s="T183">bu͡o. </ts>
            </ts>
            <ts e="T192" id="Seg_2922" n="sc" s="T185">
               <ts e="T186" id="Seg_2924" n="e" s="T185">Usku͡olata </ts>
               <ts e="T187" id="Seg_2926" n="e" s="T186">hu͡oktar </ts>
               <ts e="T188" id="Seg_2928" n="e" s="T187">uraha </ts>
               <ts e="T189" id="Seg_2930" n="e" s="T188">dʼi͡ege </ts>
               <ts e="T190" id="Seg_2932" n="e" s="T189">ü͡öreteller </ts>
               <ts e="T191" id="Seg_2934" n="e" s="T190">bejelere </ts>
               <ts e="T192" id="Seg_2936" n="e" s="T191">tutannar. </ts>
            </ts>
            <ts e="T196" id="Seg_2937" n="sc" s="T193">
               <ts e="T194" id="Seg_2939" n="e" s="T193">Baloktorgo </ts>
               <ts e="T195" id="Seg_2941" n="e" s="T194">(ü͡ö-) </ts>
               <ts e="T196" id="Seg_2943" n="e" s="T195">atɨːlɨːllar. </ts>
            </ts>
            <ts e="T208" id="Seg_2944" n="sc" s="T197">
               <ts e="T198" id="Seg_2946" n="e" s="T197">Onton </ts>
               <ts e="T199" id="Seg_2948" n="e" s="T198">tupput </ts>
               <ts e="T200" id="Seg_2950" n="e" s="T199">dʼi͡elere </ts>
               <ts e="T201" id="Seg_2952" n="e" s="T200">baːr </ts>
               <ts e="T202" id="Seg_2954" n="e" s="T201">ete </ts>
               <ts e="T203" id="Seg_2956" n="e" s="T202">oččotoŋu, </ts>
               <ts e="T204" id="Seg_2958" n="e" s="T203">anɨ </ts>
               <ts e="T205" id="Seg_2960" n="e" s="T204">huːlbuta </ts>
               <ts e="T206" id="Seg_2962" n="e" s="T205">bu͡o </ts>
               <ts e="T207" id="Seg_2964" n="e" s="T206">ontuŋ, </ts>
               <ts e="T208" id="Seg_2966" n="e" s="T207">laːpkɨlara. </ts>
            </ts>
            <ts e="T223" id="Seg_2967" n="sc" s="T209">
               <ts e="T210" id="Seg_2969" n="e" s="T209">Ol </ts>
               <ts e="T211" id="Seg_2971" n="e" s="T210">ihin </ts>
               <ts e="T212" id="Seg_2973" n="e" s="T211">Hɨndaːssko, </ts>
               <ts e="T213" id="Seg_2975" n="e" s="T212">Sɨndassko </ts>
               <ts e="T214" id="Seg_2977" n="e" s="T213">Daːrija </ts>
               <ts e="T215" id="Seg_2979" n="e" s="T214">tugu </ts>
               <ts e="T216" id="Seg_2981" n="e" s="T215">kimi </ts>
               <ts e="T217" id="Seg_2983" n="e" s="T216">Daːrija </ts>
               <ts e="T218" id="Seg_2985" n="e" s="T217">diːller </ts>
               <ts e="T219" id="Seg_2987" n="e" s="T218">et, </ts>
               <ts e="T220" id="Seg_2989" n="e" s="T219">iti </ts>
               <ts e="T221" id="Seg_2991" n="e" s="T220">ti͡ere </ts>
               <ts e="T222" id="Seg_2993" n="e" s="T221">aːttɨːllar </ts>
               <ts e="T223" id="Seg_2995" n="e" s="T222">ühü. </ts>
            </ts>
            <ts e="T227" id="Seg_2996" n="sc" s="T224">
               <ts e="T225" id="Seg_2998" n="e" s="T224">Heː </ts>
               <ts e="T226" id="Seg_3000" n="e" s="T225">bu </ts>
               <ts e="T227" id="Seg_3002" n="e" s="T226">Hɨndaːsskanɨ. </ts>
            </ts>
            <ts e="T238" id="Seg_3003" n="sc" s="T228">
               <ts e="T229" id="Seg_3005" n="e" s="T228">Iti </ts>
               <ts e="T230" id="Seg_3007" n="e" s="T229">bultanar </ts>
               <ts e="T231" id="Seg_3009" n="e" s="T230">hirderin </ts>
               <ts e="T232" id="Seg_3011" n="e" s="T231">bɨldʼaːbɨttar </ts>
               <ts e="T233" id="Seg_3013" n="e" s="T232">onton </ts>
               <ts e="T234" id="Seg_3015" n="e" s="T233">kojut </ts>
               <ts e="T235" id="Seg_3017" n="e" s="T234">kojut </ts>
               <ts e="T236" id="Seg_3019" n="e" s="T235">et </ts>
               <ts e="T237" id="Seg_3021" n="e" s="T236">iti </ts>
               <ts e="T238" id="Seg_3023" n="e" s="T237">bu͡olbuttara. </ts>
            </ts>
            <ts e="T250" id="Seg_3024" n="sc" s="T239">
               <ts e="T240" id="Seg_3026" n="e" s="T239">Onton </ts>
               <ts e="T241" id="Seg_3028" n="e" s="T240">kojut </ts>
               <ts e="T242" id="Seg_3030" n="e" s="T241">iti </ts>
               <ts e="T243" id="Seg_3032" n="e" s="T242">bu </ts>
               <ts e="T244" id="Seg_3034" n="e" s="T243">kele </ts>
               <ts e="T245" id="Seg_3036" n="e" s="T244">iligine </ts>
               <ts e="T246" id="Seg_3038" n="e" s="T245">Lɨgɨjdar </ts>
               <ts e="T247" id="Seg_3040" n="e" s="T246">di͡en </ts>
               <ts e="T248" id="Seg_3042" n="e" s="T247">baːr </ts>
               <ts e="T249" id="Seg_3044" n="e" s="T248">etilere </ts>
               <ts e="T250" id="Seg_3046" n="e" s="T249">ühü. </ts>
            </ts>
            <ts e="T256" id="Seg_3047" n="sc" s="T251">
               <ts e="T252" id="Seg_3049" n="e" s="T251">Eh </ts>
               <ts e="T253" id="Seg_3051" n="e" s="T252">kimner </ts>
               <ts e="T254" id="Seg_3053" n="e" s="T253">tigileːkter </ts>
               <ts e="T255" id="Seg_3055" n="e" s="T254">diːller </ts>
               <ts e="T256" id="Seg_3057" n="e" s="T255">ol. </ts>
            </ts>
            <ts e="T270" id="Seg_3058" n="sc" s="T257">
               <ts e="T258" id="Seg_3060" n="e" s="T257">Itinne </ts>
               <ts e="T259" id="Seg_3062" n="e" s="T258">iti </ts>
               <ts e="T260" id="Seg_3064" n="e" s="T259">peričal </ts>
               <ts e="T261" id="Seg_3066" n="e" s="T260">ɨnaraː </ts>
               <ts e="T262" id="Seg_3068" n="e" s="T261">öttüger </ts>
               <ts e="T263" id="Seg_3070" n="e" s="T262">bu͡olla </ts>
               <ts e="T264" id="Seg_3072" n="e" s="T263">dʼi͡e </ts>
               <ts e="T265" id="Seg_3074" n="e" s="T264">onnulara </ts>
               <ts e="T266" id="Seg_3076" n="e" s="T265">baːllar, </ts>
               <ts e="T267" id="Seg_3078" n="e" s="T266">ikki </ts>
               <ts e="T268" id="Seg_3080" n="e" s="T267">üs </ts>
               <ts e="T269" id="Seg_3082" n="e" s="T268">dʼi͡e </ts>
               <ts e="T270" id="Seg_3084" n="e" s="T269">onnuta. </ts>
            </ts>
            <ts e="T278" id="Seg_3085" n="sc" s="T271">
               <ts e="T272" id="Seg_3087" n="e" s="T271">Onno </ts>
               <ts e="T273" id="Seg_3089" n="e" s="T272">olorollor </ts>
               <ts e="T274" id="Seg_3091" n="e" s="T273">ol, </ts>
               <ts e="T275" id="Seg_3093" n="e" s="T274">bulčuttar </ts>
               <ts e="T276" id="Seg_3095" n="e" s="T275">olordoktorunan, </ts>
               <ts e="T277" id="Seg_3097" n="e" s="T276">Lɨgɨjdar </ts>
               <ts e="T278" id="Seg_3099" n="e" s="T277">kelbittere. </ts>
            </ts>
            <ts e="T281" id="Seg_3100" n="sc" s="T279">
               <ts e="T280" id="Seg_3102" n="e" s="T279">Lɨgɨjder, </ts>
               <ts e="T281" id="Seg_3104" n="e" s="T280">tigileːkter. </ts>
            </ts>
            <ts e="T287" id="Seg_3105" n="sc" s="T282">
               <ts e="T283" id="Seg_3107" n="e" s="T282">Ol </ts>
               <ts e="T284" id="Seg_3109" n="e" s="T283">kelbittere </ts>
               <ts e="T285" id="Seg_3111" n="e" s="T284">üs </ts>
               <ts e="T286" id="Seg_3113" n="e" s="T285">dʼaktardaːktar </ts>
               <ts e="T287" id="Seg_3115" n="e" s="T286">olor. </ts>
            </ts>
            <ts e="T293" id="Seg_3116" n="sc" s="T288">
               <ts e="T289" id="Seg_3118" n="e" s="T288">Biːrdere </ts>
               <ts e="T290" id="Seg_3120" n="e" s="T289">ojuːn, </ts>
               <ts e="T291" id="Seg_3122" n="e" s="T290">(biːrde-) </ts>
               <ts e="T292" id="Seg_3124" n="e" s="T291">ikkilere </ts>
               <ts e="T293" id="Seg_3126" n="e" s="T292">povar. </ts>
            </ts>
            <ts e="T306" id="Seg_3127" n="sc" s="T294">
               <ts e="T295" id="Seg_3129" n="e" s="T294">Dʼe </ts>
               <ts e="T296" id="Seg_3131" n="e" s="T295">bu </ts>
               <ts e="T297" id="Seg_3133" n="e" s="T296">kihini </ts>
               <ts e="T298" id="Seg_3135" n="e" s="T297">ölördö </ts>
               <ts e="T299" id="Seg_3137" n="e" s="T298">ölörbütünen </ts>
               <ts e="T300" id="Seg_3139" n="e" s="T299">kelbitter </ts>
               <ts e="T301" id="Seg_3141" n="e" s="T300">oloru </ts>
               <ts e="T302" id="Seg_3143" n="e" s="T301">povar </ts>
               <ts e="T303" id="Seg_3145" n="e" s="T302">gɨnaːrɨ </ts>
               <ts e="T304" id="Seg_3147" n="e" s="T303">egelbittere, </ts>
               <ts e="T305" id="Seg_3149" n="e" s="T304">kuhagan </ts>
               <ts e="T306" id="Seg_3151" n="e" s="T305">kihiler. </ts>
            </ts>
            <ts e="T313" id="Seg_3152" n="sc" s="T307">
               <ts e="T308" id="Seg_3154" n="e" s="T307">Kak </ts>
               <ts e="T309" id="Seg_3156" n="e" s="T308">budta </ts>
               <ts e="T310" id="Seg_3158" n="e" s="T309">tu͡oktara </ts>
               <ts e="T311" id="Seg_3160" n="e" s="T310">hülügetter </ts>
               <ts e="T312" id="Seg_3162" n="e" s="T311">eni </ts>
               <ts e="T313" id="Seg_3164" n="e" s="T312">to. </ts>
            </ts>
            <ts e="T323" id="Seg_3165" n="sc" s="T314">
               <ts e="T315" id="Seg_3167" n="e" s="T314">Ol </ts>
               <ts e="T316" id="Seg_3169" n="e" s="T315">kelen </ts>
               <ts e="T317" id="Seg_3171" n="e" s="T316">kelenner </ts>
               <ts e="T318" id="Seg_3173" n="e" s="T317">munna </ts>
               <ts e="T319" id="Seg_3175" n="e" s="T318">kelbittere </ts>
               <ts e="T320" id="Seg_3177" n="e" s="T319">bulčuttar </ts>
               <ts e="T321" id="Seg_3179" n="e" s="T320">biːrdere </ts>
               <ts e="T322" id="Seg_3181" n="e" s="T321">bu͡o </ts>
               <ts e="T323" id="Seg_3183" n="e" s="T322">bɨhɨj. </ts>
            </ts>
            <ts e="T332" id="Seg_3184" n="sc" s="T324">
               <ts e="T325" id="Seg_3186" n="e" s="T324">Biːrdere </ts>
               <ts e="T326" id="Seg_3188" n="e" s="T325">körügös, </ts>
               <ts e="T327" id="Seg_3190" n="e" s="T326">biːrdere </ts>
               <ts e="T328" id="Seg_3192" n="e" s="T327">ɨtɨːhɨt, </ts>
               <ts e="T329" id="Seg_3194" n="e" s="T328">bulčuttar </ts>
               <ts e="T330" id="Seg_3196" n="e" s="T329">bihi͡ettere </ts>
               <ts e="T331" id="Seg_3198" n="e" s="T330">ühü </ts>
               <ts e="T332" id="Seg_3200" n="e" s="T331">kepsiːller. </ts>
            </ts>
            <ts e="T349" id="Seg_3201" n="sc" s="T333">
               <ts e="T334" id="Seg_3203" n="e" s="T333">Olor </ts>
               <ts e="T335" id="Seg_3205" n="e" s="T334">bu͡ollagɨna </ts>
               <ts e="T336" id="Seg_3207" n="e" s="T335">kihini </ts>
               <ts e="T337" id="Seg_3209" n="e" s="T336">baratan </ts>
               <ts e="T338" id="Seg_3211" n="e" s="T337">kelen </ts>
               <ts e="T339" id="Seg_3213" n="e" s="T338">kelenner </ts>
               <ts e="T340" id="Seg_3215" n="e" s="T339">bu </ts>
               <ts e="T341" id="Seg_3217" n="e" s="T340">ogonnʼordor </ts>
               <ts e="T342" id="Seg_3219" n="e" s="T341">Malɨj </ts>
               <ts e="T343" id="Seg_3221" n="e" s="T342">di͡en </ts>
               <ts e="T344" id="Seg_3223" n="e" s="T343">hirge </ts>
               <ts e="T345" id="Seg_3225" n="e" s="T344">Pu͡orotaj </ts>
               <ts e="T346" id="Seg_3227" n="e" s="T345">di͡en </ts>
               <ts e="T347" id="Seg_3229" n="e" s="T346">kihi </ts>
               <ts e="T348" id="Seg_3231" n="e" s="T347">baːr </ts>
               <ts e="T349" id="Seg_3233" n="e" s="T348">ete. </ts>
            </ts>
            <ts e="T354" id="Seg_3234" n="sc" s="T350">
               <ts e="T351" id="Seg_3236" n="e" s="T350">Onuga </ts>
               <ts e="T352" id="Seg_3238" n="e" s="T351">barbɨttar </ts>
               <ts e="T353" id="Seg_3240" n="e" s="T352">bu </ts>
               <ts e="T354" id="Seg_3242" n="e" s="T353">kihiler. </ts>
            </ts>
            <ts e="T366" id="Seg_3243" n="sc" s="T355">
               <ts e="T356" id="Seg_3245" n="e" s="T355">"Kaja </ts>
               <ts e="T357" id="Seg_3247" n="e" s="T356">dogo </ts>
               <ts e="T358" id="Seg_3249" n="e" s="T357">onnuk </ts>
               <ts e="T359" id="Seg_3251" n="e" s="T358">onnuk </ts>
               <ts e="T360" id="Seg_3253" n="e" s="T359">heriː </ts>
               <ts e="T361" id="Seg_3255" n="e" s="T360">(tumsunaːk </ts>
               <ts e="T362" id="Seg_3257" n="e" s="T361">ɨttar) </ts>
               <ts e="T363" id="Seg_3259" n="e" s="T362">kejeller, </ts>
               <ts e="T364" id="Seg_3261" n="e" s="T363">ölörü͡öksütter </ts>
               <ts e="T365" id="Seg_3263" n="e" s="T364">(keller) </ts>
               <ts e="T366" id="Seg_3265" n="e" s="T365">tigileːkter." </ts>
            </ts>
            <ts e="T372" id="Seg_3266" n="sc" s="T367">
               <ts e="T368" id="Seg_3268" n="e" s="T367">De </ts>
               <ts e="T369" id="Seg_3270" n="e" s="T368">ol </ts>
               <ts e="T370" id="Seg_3272" n="e" s="T369">kürüːller </ts>
               <ts e="T371" id="Seg_3274" n="e" s="T370">Malɨjga, </ts>
               <ts e="T372" id="Seg_3276" n="e" s="T371">küreːbittere. </ts>
            </ts>
            <ts e="T393" id="Seg_3277" n="sc" s="T373">
               <ts e="T374" id="Seg_3279" n="e" s="T373">Ol </ts>
               <ts e="T375" id="Seg_3281" n="e" s="T374">küreːn </ts>
               <ts e="T376" id="Seg_3283" n="e" s="T375">tiːjbitter </ts>
               <ts e="T377" id="Seg_3285" n="e" s="T376">Pu͡orotajga </ts>
               <ts e="T378" id="Seg_3287" n="e" s="T377">tiːjenner </ts>
               <ts e="T379" id="Seg_3289" n="e" s="T378">diː </ts>
               <ts e="T380" id="Seg_3291" n="e" s="T379">kepsiːller </ts>
               <ts e="T381" id="Seg_3293" n="e" s="T380">kaja </ts>
               <ts e="T382" id="Seg_3295" n="e" s="T381">onnuktar </ts>
               <ts e="T383" id="Seg_3297" n="e" s="T382">kelliler </ts>
               <ts e="T384" id="Seg_3299" n="e" s="T383">kihini </ts>
               <ts e="T385" id="Seg_3301" n="e" s="T384">baraːppɨttar. </ts>
               <ts e="T386" id="Seg_3303" n="e" s="T385">Ki͡eŋ </ts>
               <ts e="T387" id="Seg_3305" n="e" s="T386">kü͡ölten </ts>
               <ts e="T388" id="Seg_3307" n="e" s="T387">ustannar, </ts>
               <ts e="T389" id="Seg_3309" n="e" s="T388">haŋa </ts>
               <ts e="T390" id="Seg_3311" n="e" s="T389">ürek </ts>
               <ts e="T391" id="Seg_3313" n="e" s="T390">üstün </ts>
               <ts e="T392" id="Seg_3315" n="e" s="T391">kelbitter </ts>
               <ts e="T393" id="Seg_3317" n="e" s="T392">di͡en. </ts>
            </ts>
            <ts e="T400" id="Seg_3318" n="sc" s="T394">
               <ts e="T395" id="Seg_3320" n="e" s="T394">Ol </ts>
               <ts e="T396" id="Seg_3322" n="e" s="T395">ogonnʼorgo </ts>
               <ts e="T397" id="Seg_3324" n="e" s="T396">tiːjenner </ts>
               <ts e="T398" id="Seg_3326" n="e" s="T397">bu </ts>
               <ts e="T399" id="Seg_3328" n="e" s="T398">kihiler </ts>
               <ts e="T400" id="Seg_3330" n="e" s="T399">dʼe. </ts>
            </ts>
            <ts e="T409" id="Seg_3331" n="sc" s="T401">
               <ts e="T402" id="Seg_3333" n="e" s="T401">"Dʼe </ts>
               <ts e="T403" id="Seg_3335" n="e" s="T402">körüges </ts>
               <ts e="T404" id="Seg_3337" n="e" s="T403">körülüː </ts>
               <ts e="T405" id="Seg_3339" n="e" s="T404">olor </ts>
               <ts e="T406" id="Seg_3341" n="e" s="T405">üːhe </ts>
               <ts e="T407" id="Seg_3343" n="e" s="T406">hu͡opkaga </ts>
               <ts e="T408" id="Seg_3345" n="e" s="T407">taksaŋŋɨn", </ts>
               <ts e="T409" id="Seg_3347" n="e" s="T408">di͡en. </ts>
            </ts>
            <ts e="T424" id="Seg_3348" n="sc" s="T410">
               <ts e="T411" id="Seg_3350" n="e" s="T410">Biːrdere </ts>
               <ts e="T412" id="Seg_3352" n="e" s="T411">ɨtɨːhɨttara </ts>
               <ts e="T413" id="Seg_3354" n="e" s="T412">(kenne) </ts>
               <ts e="T414" id="Seg_3356" n="e" s="T413">kiːllerteːbit </ts>
               <ts e="T415" id="Seg_3358" n="e" s="T414">dogottorun </ts>
               <ts e="T416" id="Seg_3360" n="e" s="T415">innʼe </ts>
               <ts e="T417" id="Seg_3362" n="e" s="T416">gɨnan </ts>
               <ts e="T418" id="Seg_3364" n="e" s="T417">baran </ts>
               <ts e="T419" id="Seg_3366" n="e" s="T418">dʼe </ts>
               <ts e="T420" id="Seg_3368" n="e" s="T419">kaːn </ts>
               <ts e="T421" id="Seg_3370" n="e" s="T420">butugas </ts>
               <ts e="T422" id="Seg_3372" n="e" s="T421">busputtar </ts>
               <ts e="T423" id="Seg_3374" n="e" s="T422">bu </ts>
               <ts e="T424" id="Seg_3376" n="e" s="T423">ogonnʼor. </ts>
            </ts>
            <ts e="T427" id="Seg_3377" n="sc" s="T425">
               <ts e="T426" id="Seg_3379" n="e" s="T425">Uraha </ts>
               <ts e="T427" id="Seg_3381" n="e" s="T426">dʼi͡eleːkter. </ts>
            </ts>
            <ts e="T436" id="Seg_3382" n="sc" s="T428">
               <ts e="T429" id="Seg_3384" n="e" s="T428">Dʼe </ts>
               <ts e="T430" id="Seg_3386" n="e" s="T429">butugastaːn </ts>
               <ts e="T431" id="Seg_3388" n="e" s="T430">baran </ts>
               <ts e="T432" id="Seg_3390" n="e" s="T431">bu͡o </ts>
               <ts e="T433" id="Seg_3392" n="e" s="T432">butugahɨgar </ts>
               <ts e="T434" id="Seg_3394" n="e" s="T433">bu͡olla </ts>
               <ts e="T435" id="Seg_3396" n="e" s="T434">kimi </ts>
               <ts e="T436" id="Seg_3398" n="e" s="T435">kerpit. </ts>
            </ts>
            <ts e="T443" id="Seg_3399" n="sc" s="T437">
               <ts e="T438" id="Seg_3401" n="e" s="T437">Hɨn </ts>
               <ts e="T439" id="Seg_3403" n="e" s="T438">di͡eččiler </ts>
               <ts e="T440" id="Seg_3405" n="e" s="T439">oččogo </ts>
               <ts e="T441" id="Seg_3407" n="e" s="T440">anɨ </ts>
               <ts e="T442" id="Seg_3409" n="e" s="T441">hɨn </ts>
               <ts e="T443" id="Seg_3411" n="e" s="T442">li͡eske. </ts>
            </ts>
            <ts e="T453" id="Seg_3412" n="sc" s="T444">
               <ts e="T445" id="Seg_3414" n="e" s="T444">Li͡eskanɨ </ts>
               <ts e="T446" id="Seg_3416" n="e" s="T445">tabaːkka </ts>
               <ts e="T447" id="Seg_3418" n="e" s="T446">di͡eri </ts>
               <ts e="T448" id="Seg_3420" n="e" s="T447">kördük </ts>
               <ts e="T449" id="Seg_3422" n="e" s="T448">kerte-kerte </ts>
               <ts e="T450" id="Seg_3424" n="e" s="T449">bu͡o </ts>
               <ts e="T451" id="Seg_3426" n="e" s="T450">butugaska </ts>
               <ts e="T452" id="Seg_3428" n="e" s="T451">kupput </ts>
               <ts e="T453" id="Seg_3430" n="e" s="T452">bu͡o. </ts>
            </ts>
            <ts e="T467" id="Seg_3431" n="sc" s="T454">
               <ts e="T455" id="Seg_3433" n="e" s="T454">"Aččɨktaːn </ts>
               <ts e="T456" id="Seg_3435" n="e" s="T455">iher </ts>
               <ts e="T457" id="Seg_3437" n="e" s="T456">kihiler </ts>
               <ts e="T458" id="Seg_3439" n="e" s="T457">butugas </ts>
               <ts e="T459" id="Seg_3441" n="e" s="T458">ihi͡ektere", </ts>
               <ts e="T460" id="Seg_3443" n="e" s="T459">di͡en, </ts>
               <ts e="T461" id="Seg_3445" n="e" s="T460">"ol </ts>
               <ts e="T462" id="Seg_3447" n="e" s="T461">ihi͡ektere", </ts>
               <ts e="T463" id="Seg_3449" n="e" s="T462">di͡en </ts>
               <ts e="T464" id="Seg_3451" n="e" s="T463">ol </ts>
               <ts e="T465" id="Seg_3453" n="e" s="T464">(butugas) </ts>
               <ts e="T466" id="Seg_3455" n="e" s="T465">kerderbit </ts>
               <ts e="T467" id="Seg_3457" n="e" s="T466">bu͡o. </ts>
            </ts>
            <ts e="T481" id="Seg_3458" n="sc" s="T468">
               <ts e="T469" id="Seg_3460" n="e" s="T468">"Innʼen </ts>
               <ts e="T470" id="Seg_3462" n="e" s="T469">kantas </ts>
               <ts e="T471" id="Seg_3464" n="e" s="T470">gɨnnagɨna </ts>
               <ts e="T472" id="Seg_3466" n="e" s="T471">ol </ts>
               <ts e="T473" id="Seg_3468" n="e" s="T472">tojonnorun </ts>
               <ts e="T474" id="Seg_3470" n="e" s="T473">kabɨrgatɨn </ts>
               <ts e="T475" id="Seg_3472" n="e" s="T474">kaja </ts>
               <ts e="T476" id="Seg_3474" n="e" s="T475">oksoːruŋ", </ts>
               <ts e="T477" id="Seg_3476" n="e" s="T476">di͡en,"( </ts>
               <ts e="T478" id="Seg_3478" n="e" s="T477">"(bɨtɨgɨ-) </ts>
               <ts e="T479" id="Seg_3480" n="e" s="T478">eː </ts>
               <ts e="T480" id="Seg_3482" n="e" s="T479">kiminen </ts>
               <ts e="T481" id="Seg_3484" n="e" s="T480">batɨjanan. </ts>
            </ts>
            <ts e="T492" id="Seg_3485" n="sc" s="T482">
               <ts e="T483" id="Seg_3487" n="e" s="T482">Oččogo </ts>
               <ts e="T484" id="Seg_3489" n="e" s="T483">ere </ts>
               <ts e="T485" id="Seg_3491" n="e" s="T484">kɨ͡ajɨ͡akpɨt", </ts>
               <ts e="T486" id="Seg_3493" n="e" s="T485">di͡ebit </ts>
               <ts e="T487" id="Seg_3495" n="e" s="T486">bu͡o, </ts>
               <ts e="T488" id="Seg_3497" n="e" s="T487">onton </ts>
               <ts e="T489" id="Seg_3499" n="e" s="T488">haŋarda </ts>
               <ts e="T490" id="Seg_3501" n="e" s="T489">kɨ͡ajɨ͡akpɨt </ts>
               <ts e="T491" id="Seg_3503" n="e" s="T490">kajdak </ts>
               <ts e="T492" id="Seg_3505" n="e" s="T491">eme". </ts>
            </ts>
            <ts e="T504" id="Seg_3506" n="sc" s="T493">
               <ts e="T494" id="Seg_3508" n="e" s="T493">De </ts>
               <ts e="T495" id="Seg_3510" n="e" s="T494">ol </ts>
               <ts e="T496" id="Seg_3512" n="e" s="T495">kelbitter </ts>
               <ts e="T497" id="Seg_3514" n="e" s="T496">ol </ts>
               <ts e="T498" id="Seg_3516" n="e" s="T497">butugastaːbɨttar </ts>
               <ts e="T499" id="Seg_3518" n="e" s="T498">dʼe </ts>
               <ts e="T500" id="Seg_3520" n="e" s="T499">iheller </ts>
               <ts e="T501" id="Seg_3522" n="e" s="T500">"butugastaːŋ </ts>
               <ts e="T502" id="Seg_3524" n="e" s="T501">di͡ebit </ts>
               <ts e="T503" id="Seg_3526" n="e" s="T502">bu͡o </ts>
               <ts e="T504" id="Seg_3528" n="e" s="T503">butugastaːbɨttar. </ts>
            </ts>
            <ts e="T514" id="Seg_3529" n="sc" s="T505">
               <ts e="T506" id="Seg_3531" n="e" s="T505">Dʼe </ts>
               <ts e="T507" id="Seg_3533" n="e" s="T506">ol </ts>
               <ts e="T508" id="Seg_3535" n="e" s="T507">kɨːnnʼara </ts>
               <ts e="T509" id="Seg_3537" n="e" s="T508">hɨttaktarɨnan </ts>
               <ts e="T510" id="Seg_3539" n="e" s="T509">de </ts>
               <ts e="T511" id="Seg_3541" n="e" s="T510">kelbitter </ts>
               <ts e="T512" id="Seg_3543" n="e" s="T511">bu͡o </ts>
               <ts e="T513" id="Seg_3545" n="e" s="T512">ol </ts>
               <ts e="T514" id="Seg_3547" n="e" s="T513">ečikejeleriŋ. </ts>
            </ts>
            <ts e="T523" id="Seg_3548" n="sc" s="T515">
               <ts e="T516" id="Seg_3550" n="e" s="T515">Kelenner </ts>
               <ts e="T517" id="Seg_3552" n="e" s="T516">dʼaktattarɨ </ts>
               <ts e="T518" id="Seg_3554" n="e" s="T517">kiːlletteːn </ts>
               <ts e="T519" id="Seg_3556" n="e" s="T518">de </ts>
               <ts e="T520" id="Seg_3558" n="e" s="T519">bu͡o </ts>
               <ts e="T521" id="Seg_3560" n="e" s="T520">ogonnʼor </ts>
               <ts e="T522" id="Seg_3562" n="e" s="T521">tohujar </ts>
               <ts e="T523" id="Seg_3564" n="e" s="T522">kiːllettiːr. </ts>
            </ts>
            <ts e="T530" id="Seg_3565" n="sc" s="T524">
               <ts e="T525" id="Seg_3567" n="e" s="T524">"Kaja </ts>
               <ts e="T526" id="Seg_3569" n="e" s="T525">onnuktar </ts>
               <ts e="T527" id="Seg_3571" n="e" s="T526">mannɨktar </ts>
               <ts e="T528" id="Seg_3573" n="e" s="T527">östö </ts>
               <ts e="T529" id="Seg_3575" n="e" s="T528">kepseːŋ", </ts>
               <ts e="T530" id="Seg_3577" n="e" s="T529">di͡en. </ts>
            </ts>
            <ts e="T543" id="Seg_3578" n="sc" s="T531">
               <ts e="T532" id="Seg_3580" n="e" s="T531">De </ts>
               <ts e="T533" id="Seg_3582" n="e" s="T532">ol </ts>
               <ts e="T534" id="Seg_3584" n="e" s="T533">ogonnʼorgo </ts>
               <ts e="T535" id="Seg_3586" n="e" s="T534">tiːjen </ts>
               <ts e="T536" id="Seg_3588" n="e" s="T535">kepsiːller </ts>
               <ts e="T537" id="Seg_3590" n="e" s="T536">bu͡o: </ts>
               <ts e="T538" id="Seg_3592" n="e" s="T537">"Emeːksini </ts>
               <ts e="T539" id="Seg_3594" n="e" s="T538">egellibit </ts>
               <ts e="T540" id="Seg_3596" n="e" s="T539">bihigi </ts>
               <ts e="T541" id="Seg_3598" n="e" s="T540">iti </ts>
               <ts e="T542" id="Seg_3600" n="e" s="T541">hirdʼippit, </ts>
               <ts e="T543" id="Seg_3602" n="e" s="T542">diːbit. </ts>
            </ts>
            <ts e="T548" id="Seg_3603" n="sc" s="T544">
               <ts e="T545" id="Seg_3605" n="e" s="T544">Ojuːn </ts>
               <ts e="T546" id="Seg_3607" n="e" s="T545">emeːksin, </ts>
               <ts e="T547" id="Seg_3609" n="e" s="T546">iti </ts>
               <ts e="T548" id="Seg_3611" n="e" s="T547">povardarbɨt. </ts>
            </ts>
            <ts e="T563" id="Seg_3612" n="sc" s="T549">
               <ts e="T550" id="Seg_3614" n="e" s="T549">Oččogo </ts>
               <ts e="T551" id="Seg_3616" n="e" s="T550">ol </ts>
               <ts e="T552" id="Seg_3618" n="e" s="T551">ojuːn </ts>
               <ts e="T553" id="Seg_3620" n="e" s="T552">emeːksin </ts>
               <ts e="T554" id="Seg_3622" n="e" s="T553">hirdeːn </ts>
               <ts e="T555" id="Seg_3624" n="e" s="T554">egelle </ts>
               <ts e="T556" id="Seg_3626" n="e" s="T555">(bihi͡eke) </ts>
               <ts e="T557" id="Seg_3628" n="e" s="T556">ehi͡eke", </ts>
               <ts e="T558" id="Seg_3630" n="e" s="T557">di͡en </ts>
               <ts e="T559" id="Seg_3632" n="e" s="T558">ol </ts>
               <ts e="T560" id="Seg_3634" n="e" s="T559">kepsiːller, </ts>
               <ts e="T561" id="Seg_3636" n="e" s="T560">bu͡o </ts>
               <ts e="T562" id="Seg_3638" n="e" s="T561">kihini </ts>
               <ts e="T563" id="Seg_3640" n="e" s="T562">barappɨttar." </ts>
            </ts>
            <ts e="T576" id="Seg_3641" n="sc" s="T564">
               <ts e="T565" id="Seg_3643" n="e" s="T564">Kaːnnarɨn </ts>
               <ts e="T566" id="Seg_3645" n="e" s="T565">bu͡olla </ts>
               <ts e="T567" id="Seg_3647" n="e" s="T566">bahaktarɨn </ts>
               <ts e="T568" id="Seg_3649" n="e" s="T567">munna </ts>
               <ts e="T569" id="Seg_3651" n="e" s="T568">hotollor </ts>
               <ts e="T570" id="Seg_3653" n="e" s="T569">ebit </ts>
               <ts e="T571" id="Seg_3655" n="e" s="T570">kihileri </ts>
               <ts e="T572" id="Seg_3657" n="e" s="T571">kerde </ts>
               <ts e="T573" id="Seg_3659" n="e" s="T572">kerde </ts>
               <ts e="T574" id="Seg_3661" n="e" s="T573">hiː </ts>
               <ts e="T575" id="Seg_3663" n="e" s="T574">hiː </ts>
               <ts e="T576" id="Seg_3665" n="e" s="T575">hiːller. </ts>
            </ts>
            <ts e="T589" id="Seg_3666" n="sc" s="T577">
               <ts e="T578" id="Seg_3668" n="e" s="T577">"Oː </ts>
               <ts e="T579" id="Seg_3670" n="e" s="T578">tu͡ok </ts>
               <ts e="T580" id="Seg_3672" n="e" s="T579">aːttaːk </ts>
               <ts e="T581" id="Seg_3674" n="e" s="T580">kaːnɨgar </ts>
               <ts e="T582" id="Seg_3676" n="e" s="T581">bihillibit </ts>
               <ts e="T583" id="Seg_3678" n="e" s="T582">dʼonnorgutuj, </ts>
               <ts e="T584" id="Seg_3680" n="e" s="T583">dʼe </ts>
               <ts e="T585" id="Seg_3682" n="e" s="T584">ahaːŋ", </ts>
               <ts e="T586" id="Seg_3684" n="e" s="T585">di͡en </ts>
               <ts e="T587" id="Seg_3686" n="e" s="T586">ogonnʼor </ts>
               <ts e="T588" id="Seg_3688" n="e" s="T587">dʼe </ts>
               <ts e="T589" id="Seg_3690" n="e" s="T588">ahatar. </ts>
            </ts>
            <ts e="T605" id="Seg_3691" n="sc" s="T590">
               <ts e="T591" id="Seg_3693" n="e" s="T590">De </ts>
               <ts e="T592" id="Seg_3695" n="e" s="T591">ol </ts>
               <ts e="T593" id="Seg_3697" n="e" s="T592">(ahaː-) </ts>
               <ts e="T594" id="Seg_3699" n="e" s="T593">ahappɨtɨn </ts>
               <ts e="T595" id="Seg_3701" n="e" s="T594">kenne </ts>
               <ts e="T596" id="Seg_3703" n="e" s="T595">de </ts>
               <ts e="T597" id="Seg_3705" n="e" s="T596">ol </ts>
               <ts e="T598" id="Seg_3707" n="e" s="T597">oː </ts>
               <ts e="T599" id="Seg_3709" n="e" s="T598">kallaːmmɨt </ts>
               <ts e="T600" id="Seg_3711" n="e" s="T599">kajdak </ts>
               <ts e="T601" id="Seg_3713" n="e" s="T600">kajdak </ts>
               <ts e="T602" id="Seg_3715" n="e" s="T601">bu͡olar </ts>
               <ts e="T603" id="Seg_3717" n="e" s="T602">de </ts>
               <ts e="T604" id="Seg_3719" n="e" s="T603">bɨlɨppɨt </ts>
               <ts e="T605" id="Seg_3721" n="e" s="T604">kojdor. </ts>
            </ts>
            <ts e="T609" id="Seg_3722" n="sc" s="T606">
               <ts e="T607" id="Seg_3724" n="e" s="T606">"Tu͡ok </ts>
               <ts e="T608" id="Seg_3726" n="e" s="T607">bu͡olar"; </ts>
               <ts e="T609" id="Seg_3728" n="e" s="T608">di͡ebite. </ts>
            </ts>
            <ts e="T618" id="Seg_3729" n="sc" s="T610">
               <ts e="T611" id="Seg_3731" n="e" s="T610">Dʼe </ts>
               <ts e="T612" id="Seg_3733" n="e" s="T611">ol </ts>
               <ts e="T613" id="Seg_3735" n="e" s="T612">ikki </ts>
               <ts e="T614" id="Seg_3737" n="e" s="T613">kamnastaːgɨ </ts>
               <ts e="T615" id="Seg_3739" n="e" s="T614">ikki </ts>
               <ts e="T616" id="Seg_3741" n="e" s="T615">öttüge </ts>
               <ts e="T617" id="Seg_3743" n="e" s="T616">olorput </ts>
               <ts e="T618" id="Seg_3745" n="e" s="T617">bu͡o. </ts>
            </ts>
            <ts e="T627" id="Seg_3746" n="sc" s="T619">
               <ts e="T620" id="Seg_3748" n="e" s="T619">Dʼe </ts>
               <ts e="T621" id="Seg_3750" n="e" s="T620">ol </ts>
               <ts e="T622" id="Seg_3752" n="e" s="T621">kantas </ts>
               <ts e="T623" id="Seg_3754" n="e" s="T622">gɨmmɨtɨgar </ts>
               <ts e="T624" id="Seg_3756" n="e" s="T623">kabɨrgatɨn </ts>
               <ts e="T625" id="Seg_3758" n="e" s="T624">kaja </ts>
               <ts e="T626" id="Seg_3760" n="e" s="T625">oksubuttara </ts>
               <ts e="T627" id="Seg_3762" n="e" s="T626">batɨjanan. </ts>
            </ts>
            <ts e="T634" id="Seg_3763" n="sc" s="T628">
               <ts e="T629" id="Seg_3765" n="e" s="T628">Anɨ </ts>
               <ts e="T630" id="Seg_3767" n="e" s="T629">iti </ts>
               <ts e="T631" id="Seg_3769" n="e" s="T630">ölörsöllörüger </ts>
               <ts e="T632" id="Seg_3771" n="e" s="T631">diːr </ts>
               <ts e="T633" id="Seg_3773" n="e" s="T632">kinoːlarga </ts>
               <ts e="T634" id="Seg_3775" n="e" s="T633">di͡eri. </ts>
            </ts>
            <ts e="T639" id="Seg_3776" n="sc" s="T635">
               <ts e="T636" id="Seg_3778" n="e" s="T635">Ol </ts>
               <ts e="T637" id="Seg_3780" n="e" s="T636">kɨ͡ajbɨttar </ts>
               <ts e="T638" id="Seg_3782" n="e" s="T637">ühü </ts>
               <ts e="T639" id="Seg_3784" n="e" s="T638">ontularɨn. </ts>
            </ts>
            <ts e="T643" id="Seg_3785" n="sc" s="T640">
               <ts e="T641" id="Seg_3787" n="e" s="T640">Dʼe </ts>
               <ts e="T642" id="Seg_3789" n="e" s="T641">aŋardarɨn </ts>
               <ts e="T643" id="Seg_3791" n="e" s="T642">ölörtöːbüttere. </ts>
            </ts>
            <ts e="T650" id="Seg_3792" n="sc" s="T644">
               <ts e="T645" id="Seg_3794" n="e" s="T644">Hette </ts>
               <ts e="T646" id="Seg_3796" n="e" s="T645">kihite </ts>
               <ts e="T647" id="Seg_3798" n="e" s="T646">ebit </ts>
               <ts e="T648" id="Seg_3800" n="e" s="T647">du͡o </ts>
               <ts e="T649" id="Seg_3802" n="e" s="T648">kas </ts>
               <ts e="T650" id="Seg_3804" n="e" s="T649">du͡o. </ts>
            </ts>
            <ts e="T662" id="Seg_3805" n="sc" s="T651">
               <ts e="T652" id="Seg_3807" n="e" s="T651">Dʼe </ts>
               <ts e="T653" id="Seg_3809" n="e" s="T652">onton </ts>
               <ts e="T654" id="Seg_3811" n="e" s="T653">dʼaktattar </ts>
               <ts e="T655" id="Seg_3813" n="e" s="T654">kaːlbɨttarɨgar </ts>
               <ts e="T656" id="Seg_3815" n="e" s="T655">bu </ts>
               <ts e="T657" id="Seg_3817" n="e" s="T656">dʼaktattar </ts>
               <ts e="T658" id="Seg_3819" n="e" s="T657">dʼe </ts>
               <ts e="T659" id="Seg_3821" n="e" s="T658">etiheller </ts>
               <ts e="T660" id="Seg_3823" n="e" s="T659">ühü </ts>
               <ts e="T661" id="Seg_3825" n="e" s="T660">bahaktarɨn </ts>
               <ts e="T662" id="Seg_3827" n="e" s="T661">bɨldʼɨhannar. </ts>
            </ts>
            <ts e="T672" id="Seg_3828" n="sc" s="T663">
               <ts e="T664" id="Seg_3830" n="e" s="T663">"Min </ts>
               <ts e="T665" id="Seg_3832" n="e" s="T664">erim </ts>
               <ts e="T666" id="Seg_3834" n="e" s="T665">bahaga, </ts>
               <ts e="T667" id="Seg_3836" n="e" s="T666">min </ts>
               <ts e="T668" id="Seg_3838" n="e" s="T667">erim </ts>
               <ts e="T669" id="Seg_3840" n="e" s="T668">bahaga", </ts>
               <ts e="T670" id="Seg_3842" n="e" s="T669">deheːktiːller </ts>
               <ts e="T671" id="Seg_3844" n="e" s="T670">ühü </ts>
               <ts e="T672" id="Seg_3846" n="e" s="T671">kotokular. </ts>
            </ts>
            <ts e="T688" id="Seg_3847" n="sc" s="T673">
               <ts e="T674" id="Seg_3849" n="e" s="T673">"Tu͡ok </ts>
               <ts e="T675" id="Seg_3851" n="e" s="T674">suːka </ts>
               <ts e="T676" id="Seg_3853" n="e" s="T675">kɨrgɨttaraj </ts>
               <ts e="T677" id="Seg_3855" n="e" s="T676">itileri </ts>
               <ts e="T678" id="Seg_3857" n="e" s="T677">ölöttöːn </ts>
               <ts e="T679" id="Seg_3859" n="e" s="T678">keːhiŋ", </ts>
               <ts e="T680" id="Seg_3861" n="e" s="T679">di͡ebit </ts>
               <ts e="T681" id="Seg_3863" n="e" s="T680">bu͡o </ts>
               <ts e="T682" id="Seg_3865" n="e" s="T681">tu͡ok </ts>
               <ts e="T683" id="Seg_3867" n="e" s="T682">sustogoj </ts>
               <ts e="T684" id="Seg_3869" n="e" s="T683">ogonnʼoro </ts>
               <ts e="T685" id="Seg_3871" n="e" s="T684">bu͡olla </ts>
               <ts e="T686" id="Seg_3873" n="e" s="T685">da </ts>
               <ts e="T687" id="Seg_3875" n="e" s="T686">bihi͡ene </ts>
               <ts e="T688" id="Seg_3877" n="e" s="T687">Pu͡orotaj. </ts>
            </ts>
            <ts e="T692" id="Seg_3878" n="sc" s="T689">
               <ts e="T690" id="Seg_3880" n="e" s="T689">Ölöttöːn </ts>
               <ts e="T691" id="Seg_3882" n="e" s="T690">keːspitter </ts>
               <ts e="T692" id="Seg_3884" n="e" s="T691">muŋnaːktar. </ts>
            </ts>
            <ts e="T701" id="Seg_3885" n="sc" s="T693">
               <ts e="T694" id="Seg_3887" n="e" s="T693">Onton </ts>
               <ts e="T695" id="Seg_3889" n="e" s="T694">ol </ts>
               <ts e="T696" id="Seg_3891" n="e" s="T695">emeːksini </ts>
               <ts e="T697" id="Seg_3893" n="e" s="T696">dʼe </ts>
               <ts e="T698" id="Seg_3895" n="e" s="T697">ölörö </ts>
               <ts e="T699" id="Seg_3897" n="e" s="T698">hatɨːllar </ts>
               <ts e="T700" id="Seg_3899" n="e" s="T699">ojuːn </ts>
               <ts e="T701" id="Seg_3901" n="e" s="T700">emeːksini. </ts>
            </ts>
            <ts e="T708" id="Seg_3902" n="sc" s="T702">
               <ts e="T703" id="Seg_3904" n="e" s="T702">Uːga </ts>
               <ts e="T704" id="Seg_3906" n="e" s="T703">da </ts>
               <ts e="T705" id="Seg_3908" n="e" s="T704">timirdeller </ts>
               <ts e="T706" id="Seg_3910" n="e" s="T705">dʼe </ts>
               <ts e="T707" id="Seg_3912" n="e" s="T706">hakordaːn </ts>
               <ts e="T708" id="Seg_3914" n="e" s="T707">baran. </ts>
            </ts>
            <ts e="T711" id="Seg_3915" n="sc" s="T709">
               <ts e="T710" id="Seg_3917" n="e" s="T709">Törüt </ts>
               <ts e="T711" id="Seg_3919" n="e" s="T710">timirbet. </ts>
            </ts>
            <ts e="T720" id="Seg_3920" n="sc" s="T712">
               <ts e="T713" id="Seg_3922" n="e" s="T712">Ajagalɨː </ts>
               <ts e="T714" id="Seg_3924" n="e" s="T713">hataːn </ts>
               <ts e="T715" id="Seg_3926" n="e" s="T714">u͡otun </ts>
               <ts e="T716" id="Seg_3928" n="e" s="T715">otton </ts>
               <ts e="T717" id="Seg_3930" n="e" s="T716">bu͡o </ts>
               <ts e="T718" id="Seg_3932" n="e" s="T717">u͡ot </ts>
               <ts e="T719" id="Seg_3934" n="e" s="T718">ihiger </ts>
               <ts e="T720" id="Seg_3936" n="e" s="T719">bɨragallar. </ts>
            </ts>
            <ts e="T725" id="Seg_3937" n="sc" s="T721">
               <ts e="T722" id="Seg_3939" n="e" s="T721">U͡ottara </ts>
               <ts e="T723" id="Seg_3941" n="e" s="T722">karaːrčɨ </ts>
               <ts e="T724" id="Seg_3943" n="e" s="T723">utujar </ts>
               <ts e="T725" id="Seg_3945" n="e" s="T724">ühü. </ts>
            </ts>
            <ts e="T738" id="Seg_3946" n="sc" s="T726">
               <ts e="T727" id="Seg_3948" n="e" s="T726">Onno </ts>
               <ts e="T728" id="Seg_3950" n="e" s="T727">da </ts>
               <ts e="T729" id="Seg_3952" n="e" s="T728">di͡ebit </ts>
               <ts e="T730" id="Seg_3954" n="e" s="T729">bu͡o: </ts>
               <ts e="T731" id="Seg_3956" n="e" s="T730">"Tukuːlar </ts>
               <ts e="T732" id="Seg_3958" n="e" s="T731">min </ts>
               <ts e="T733" id="Seg_3960" n="e" s="T732">kün </ts>
               <ts e="T734" id="Seg_3962" n="e" s="T733">emeːksin </ts>
               <ts e="T735" id="Seg_3964" n="e" s="T734">etim </ts>
               <ts e="T736" id="Seg_3966" n="e" s="T735">kaja </ts>
               <ts e="T737" id="Seg_3968" n="e" s="T736">abaːhɨ </ts>
               <ts e="T738" id="Seg_3970" n="e" s="T737">bu͡oltappɨn. </ts>
            </ts>
            <ts e="T748" id="Seg_3971" n="sc" s="T739">
               <ts e="T740" id="Seg_3973" n="e" s="T739">Dʼommun </ts>
               <ts e="T741" id="Seg_3975" n="e" s="T740">baraːbɨttara </ts>
               <ts e="T742" id="Seg_3977" n="e" s="T741">bu </ts>
               <ts e="T743" id="Seg_3979" n="e" s="T742">bu͡olla </ts>
               <ts e="T744" id="Seg_3981" n="e" s="T743">kihini </ts>
               <ts e="T745" id="Seg_3983" n="e" s="T744">sü͡öhünü </ts>
               <ts e="T746" id="Seg_3985" n="e" s="T745">köröːrübün </ts>
               <ts e="T747" id="Seg_3987" n="e" s="T746">hirdeːn </ts>
               <ts e="T748" id="Seg_3989" n="e" s="T747">kelbitim. </ts>
            </ts>
            <ts e="T757" id="Seg_3990" n="sc" s="T749">
               <ts e="T750" id="Seg_3992" n="e" s="T749">Minigin </ts>
               <ts e="T751" id="Seg_3994" n="e" s="T750">erejdeːmeŋ </ts>
               <ts e="T752" id="Seg_3996" n="e" s="T751">min </ts>
               <ts e="T753" id="Seg_3998" n="e" s="T752">itinnik </ts>
               <ts e="T754" id="Seg_4000" n="e" s="T753">ke </ts>
               <ts e="T755" id="Seg_4002" n="e" s="T754">ölü͡öm </ts>
               <ts e="T756" id="Seg_4004" n="e" s="T755">hu͡oga", </ts>
               <ts e="T757" id="Seg_4006" n="e" s="T756">diːr. </ts>
            </ts>
            <ts e="T773" id="Seg_4007" n="sc" s="T758">
               <ts e="T759" id="Seg_4009" n="e" s="T758">"Kɨːh </ts>
               <ts e="T760" id="Seg_4011" n="e" s="T759">ogoto </ts>
               <ts e="T761" id="Seg_4013" n="e" s="T760">haŋardɨː </ts>
               <ts e="T762" id="Seg_4015" n="e" s="T761">bɨrtak </ts>
               <ts e="T763" id="Seg_4017" n="e" s="T762">bu͡olbut </ts>
               <ts e="T764" id="Seg_4019" n="e" s="T763">kɨːh </ts>
               <ts e="T765" id="Seg_4021" n="e" s="T764">ogoto </ts>
               <ts e="T766" id="Seg_4023" n="e" s="T765">moːjbunan </ts>
               <ts e="T767" id="Seg_4025" n="e" s="T766">atɨllatɨn, </ts>
               <ts e="T768" id="Seg_4027" n="e" s="T767">oččogo </ts>
               <ts e="T769" id="Seg_4029" n="e" s="T768">ölü͡öm", </ts>
               <ts e="T770" id="Seg_4031" n="e" s="T769">diːr, </ts>
               <ts e="T771" id="Seg_4033" n="e" s="T770">"ol </ts>
               <ts e="T772" id="Seg_4035" n="e" s="T771">moːjbunan </ts>
               <ts e="T773" id="Seg_4037" n="e" s="T772">atɨllappɨttar." </ts>
            </ts>
            <ts e="T784" id="Seg_4038" n="sc" s="T774">
               <ts e="T775" id="Seg_4040" n="e" s="T774">De </ts>
               <ts e="T776" id="Seg_4042" n="e" s="T775">ol </ts>
               <ts e="T777" id="Seg_4044" n="e" s="T776">emeːksinnere </ts>
               <ts e="T778" id="Seg_4046" n="e" s="T777">ölön </ts>
               <ts e="T779" id="Seg_4048" n="e" s="T778">kaːlbɨt. </ts>
               <ts e="T780" id="Seg_4050" n="e" s="T779">Dʼe </ts>
               <ts e="T781" id="Seg_4052" n="e" s="T780">elete </ts>
               <ts e="T782" id="Seg_4054" n="e" s="T781">ol </ts>
               <ts e="T783" id="Seg_4056" n="e" s="T782">kepseːbittere </ts>
               <ts e="T784" id="Seg_4058" n="e" s="T783">öjdöːböppün. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T10" id="Seg_4059" s="T1">KiPP_2009_Syndassko_nar.001 (001)</ta>
            <ta e="T16" id="Seg_4060" s="T11">KiPP_2009_Syndassko_nar.002 (002)</ta>
            <ta e="T19" id="Seg_4061" s="T17">KiPP_2009_Syndassko_nar.003 (003)</ta>
            <ta e="T31" id="Seg_4062" s="T20">KiPP_2009_Syndassko_nar.004 (004)</ta>
            <ta e="T46" id="Seg_4063" s="T32">KiPP_2009_Syndassko_nar.005 (005)</ta>
            <ta e="T49" id="Seg_4064" s="T47">KiPP_2009_Syndassko_nar.006 (006)</ta>
            <ta e="T54" id="Seg_4065" s="T50">KiPP_2009_Syndassko_nar.007 (007)</ta>
            <ta e="T59" id="Seg_4066" s="T55">KiPP_2009_Syndassko_nar.008 (008)</ta>
            <ta e="T66" id="Seg_4067" s="T60">KiPP_2009_Syndassko_nar.009 (009)</ta>
            <ta e="T70" id="Seg_4068" s="T67">KiPP_2009_Syndassko_nar.010 (010)</ta>
            <ta e="T85" id="Seg_4069" s="T71">KiPP_2009_Syndassko_nar.011 (011)</ta>
            <ta e="T89" id="Seg_4070" s="T86">KiPP_2009_Syndassko_nar.012 (012)</ta>
            <ta e="T98" id="Seg_4071" s="T90">KiPP_2009_Syndassko_nar.013 (013)</ta>
            <ta e="T104" id="Seg_4072" s="T99">KiPP_2009_Syndassko_nar.014 (014)</ta>
            <ta e="T107" id="Seg_4073" s="T105">KiPP_2009_Syndassko_nar.015 (015)</ta>
            <ta e="T115" id="Seg_4074" s="T108">KiPP_2009_Syndassko_nar.016 (016)</ta>
            <ta e="T123" id="Seg_4075" s="T116">KiPP_2009_Syndassko_nar.017 (017)</ta>
            <ta e="T130" id="Seg_4076" s="T124">KiPP_2009_Syndassko_nar.018 (018)</ta>
            <ta e="T138" id="Seg_4077" s="T131">KiPP_2009_Syndassko_nar.019 (019)</ta>
            <ta e="T144" id="Seg_4078" s="T139">KiPP_2009_Syndassko_nar.020 (020)</ta>
            <ta e="T153" id="Seg_4079" s="T145">KiPP_2009_Syndassko_nar.021 (021)</ta>
            <ta e="T157" id="Seg_4080" s="T154">KiPP_2009_Syndassko_nar.022 (022)</ta>
            <ta e="T165" id="Seg_4081" s="T158">KiPP_2009_Syndassko_nar.023 (023)</ta>
            <ta e="T169" id="Seg_4082" s="T166">KiPP_2009_Syndassko_nar.024 (024)</ta>
            <ta e="T184" id="Seg_4083" s="T170">KiPP_2009_Syndassko_nar.025 (025)</ta>
            <ta e="T192" id="Seg_4084" s="T185">KiPP_2009_Syndassko_nar.026 (026)</ta>
            <ta e="T196" id="Seg_4085" s="T193">KiPP_2009_Syndassko_nar.027 (027)</ta>
            <ta e="T208" id="Seg_4086" s="T197">KiPP_2009_Syndassko_nar.028 (028)</ta>
            <ta e="T223" id="Seg_4087" s="T209">KiPP_2009_Syndassko_nar.029 (029)</ta>
            <ta e="T227" id="Seg_4088" s="T224">KiPP_2009_Syndassko_nar.030 (030)</ta>
            <ta e="T238" id="Seg_4089" s="T228">KiPP_2009_Syndassko_nar.031 (031)</ta>
            <ta e="T250" id="Seg_4090" s="T239">KiPP_2009_Syndassko_nar.032 (032)</ta>
            <ta e="T256" id="Seg_4091" s="T251">KiPP_2009_Syndassko_nar.033 (033)</ta>
            <ta e="T270" id="Seg_4092" s="T257">KiPP_2009_Syndassko_nar.034 (034)</ta>
            <ta e="T278" id="Seg_4093" s="T271">KiPP_2009_Syndassko_nar.035 (035)</ta>
            <ta e="T281" id="Seg_4094" s="T279">KiPP_2009_Syndassko_nar.036 (036)</ta>
            <ta e="T287" id="Seg_4095" s="T282">KiPP_2009_Syndassko_nar.037 (037)</ta>
            <ta e="T293" id="Seg_4096" s="T288">KiPP_2009_Syndassko_nar.038 (038)</ta>
            <ta e="T306" id="Seg_4097" s="T294">KiPP_2009_Syndassko_nar.039 (039)</ta>
            <ta e="T313" id="Seg_4098" s="T307">KiPP_2009_Syndassko_nar.040 (040)</ta>
            <ta e="T323" id="Seg_4099" s="T314">KiPP_2009_Syndassko_nar.041 (041)</ta>
            <ta e="T332" id="Seg_4100" s="T324">KiPP_2009_Syndassko_nar.042 (042)</ta>
            <ta e="T349" id="Seg_4101" s="T333">KiPP_2009_Syndassko_nar.043 (043)</ta>
            <ta e="T354" id="Seg_4102" s="T350">KiPP_2009_Syndassko_nar.044 (044)</ta>
            <ta e="T366" id="Seg_4103" s="T355">KiPP_2009_Syndassko_nar.045 (045)</ta>
            <ta e="T372" id="Seg_4104" s="T367">KiPP_2009_Syndassko_nar.046 (046)</ta>
            <ta e="T385" id="Seg_4105" s="T373">KiPP_2009_Syndassko_nar.047 (047)</ta>
            <ta e="T393" id="Seg_4106" s="T385">KiPP_2009_Syndassko_nar.048 (048)</ta>
            <ta e="T400" id="Seg_4107" s="T394">KiPP_2009_Syndassko_nar.049 (049)</ta>
            <ta e="T409" id="Seg_4108" s="T401">KiPP_2009_Syndassko_nar.050 (050)</ta>
            <ta e="T424" id="Seg_4109" s="T410">KiPP_2009_Syndassko_nar.051 (051)</ta>
            <ta e="T427" id="Seg_4110" s="T425">KiPP_2009_Syndassko_nar.052 (052)</ta>
            <ta e="T436" id="Seg_4111" s="T428">KiPP_2009_Syndassko_nar.053 (053)</ta>
            <ta e="T443" id="Seg_4112" s="T437">KiPP_2009_Syndassko_nar.054 (054)</ta>
            <ta e="T453" id="Seg_4113" s="T444">KiPP_2009_Syndassko_nar.055 (055)</ta>
            <ta e="T467" id="Seg_4114" s="T454">KiPP_2009_Syndassko_nar.056 (056)</ta>
            <ta e="T481" id="Seg_4115" s="T468">KiPP_2009_Syndassko_nar.057 (057)</ta>
            <ta e="T492" id="Seg_4116" s="T482">KiPP_2009_Syndassko_nar.058 (058)</ta>
            <ta e="T504" id="Seg_4117" s="T493">KiPP_2009_Syndassko_nar.059 (059)</ta>
            <ta e="T514" id="Seg_4118" s="T505">KiPP_2009_Syndassko_nar.060 (060)</ta>
            <ta e="T523" id="Seg_4119" s="T515">KiPP_2009_Syndassko_nar.061 (061)</ta>
            <ta e="T530" id="Seg_4120" s="T524">KiPP_2009_Syndassko_nar.062 (062)</ta>
            <ta e="T537" id="Seg_4121" s="T531">KiPP_2009_Syndassko_nar.063 (063)</ta>
            <ta e="T543" id="Seg_4122" s="T537">KiPP_2009_Syndassko_nar.064 (063)</ta>
            <ta e="T548" id="Seg_4123" s="T544">KiPP_2009_Syndassko_nar.065 (064)</ta>
            <ta e="T563" id="Seg_4124" s="T549">KiPP_2009_Syndassko_nar.066 (065)</ta>
            <ta e="T576" id="Seg_4125" s="T564">KiPP_2009_Syndassko_nar.067 (066)</ta>
            <ta e="T589" id="Seg_4126" s="T577">KiPP_2009_Syndassko_nar.068 (067)</ta>
            <ta e="T605" id="Seg_4127" s="T590">KiPP_2009_Syndassko_nar.069 (068)</ta>
            <ta e="T609" id="Seg_4128" s="T606">KiPP_2009_Syndassko_nar.070 (069)</ta>
            <ta e="T618" id="Seg_4129" s="T610">KiPP_2009_Syndassko_nar.071 (070)</ta>
            <ta e="T627" id="Seg_4130" s="T619">KiPP_2009_Syndassko_nar.072 (071)</ta>
            <ta e="T634" id="Seg_4131" s="T628">KiPP_2009_Syndassko_nar.073 (072)</ta>
            <ta e="T639" id="Seg_4132" s="T635">KiPP_2009_Syndassko_nar.074 (073)</ta>
            <ta e="T643" id="Seg_4133" s="T640">KiPP_2009_Syndassko_nar.075 (074)</ta>
            <ta e="T650" id="Seg_4134" s="T644">KiPP_2009_Syndassko_nar.076 (075)</ta>
            <ta e="T662" id="Seg_4135" s="T651">KiPP_2009_Syndassko_nar.077 (076)</ta>
            <ta e="T672" id="Seg_4136" s="T663">KiPP_2009_Syndassko_nar.078 (077)</ta>
            <ta e="T688" id="Seg_4137" s="T673">KiPP_2009_Syndassko_nar.079 (078)</ta>
            <ta e="T692" id="Seg_4138" s="T689">KiPP_2009_Syndassko_nar.080 (079)</ta>
            <ta e="T701" id="Seg_4139" s="T693">KiPP_2009_Syndassko_nar.081 (080)</ta>
            <ta e="T708" id="Seg_4140" s="T702">KiPP_2009_Syndassko_nar.082 (081)</ta>
            <ta e="T711" id="Seg_4141" s="T709">KiPP_2009_Syndassko_nar.083 (082)</ta>
            <ta e="T720" id="Seg_4142" s="T712">KiPP_2009_Syndassko_nar.084 (083)</ta>
            <ta e="T725" id="Seg_4143" s="T721">KiPP_2009_Syndassko_nar.085 (084)</ta>
            <ta e="T730" id="Seg_4144" s="T726">KiPP_2009_Syndassko_nar.086 (085)</ta>
            <ta e="T738" id="Seg_4145" s="T730">KiPP_2009_Syndassko_nar.087 (085)</ta>
            <ta e="T748" id="Seg_4146" s="T739">KiPP_2009_Syndassko_nar.088 (086)</ta>
            <ta e="T757" id="Seg_4147" s="T749">KiPP_2009_Syndassko_nar.089 (087)</ta>
            <ta e="T773" id="Seg_4148" s="T758">KiPP_2009_Syndassko_nar.090 (088)</ta>
            <ta e="T779" id="Seg_4149" s="T774">KiPP_2009_Syndassko_nar.091 (089)</ta>
            <ta e="T784" id="Seg_4150" s="T779">KiPP_2009_Syndassko_nar.092 (090)</ta>
         </annotation>
         <annotation name="st" tierref="st" />
         <annotation name="ts" tierref="ts">
            <ta e="T10" id="Seg_4151" s="T1">Onton biːrde bultana taksɨbɨttara ogonnʼottor (emeːk-) ogonnʼottor keleller u͡olattar. </ta>
            <ta e="T16" id="Seg_4152" s="T11">"Kaja, Hɨndaːsskabɨtɨgar dʼi͡e turbut", di͡ebitter. </ta>
            <ta e="T19" id="Seg_4153" s="T17">"Kajdi͡ek bultanaːččɨbɨt?" </ta>
            <ta e="T31" id="Seg_4154" s="T20">Oː dʼe, bu dʼi͡e turbutugar kuttanallar keli͡ekterin ühü, nuːččalar baːllar di͡en. </ta>
            <ta e="T46" id="Seg_4155" s="T32">"Oː dʼe, bu hirbitin bulta hu͡ok oŋordular, kajdak bu͡olaːččɨbɨtɨj", diːller ühü ol ogonnʼottor ke. </ta>
            <ta e="T49" id="Seg_4156" s="T47">"Dehebit", diːller. </ta>
            <ta e="T54" id="Seg_4157" s="T50">Kepsetellerin istebin ehelerim ke. </ta>
            <ta e="T59" id="Seg_4158" s="T55">Onton, dʼe onton keleller. </ta>
            <ta e="T66" id="Seg_4159" s="T60">Kelenner körbüttere ikki kihi hɨldʼallar ühü. </ta>
            <ta e="T70" id="Seg_4160" s="T67">Dʼaktarɨn aːta Daːrija. </ta>
            <ta e="T85" id="Seg_4161" s="T71">Erin aːta kim di͡ebitterij ke da oluja bagajɨ aːttaːk kihi, kim ere di͡eččiler et. </ta>
            <ta e="T89" id="Seg_4162" s="T86">Törüt öjdöːböppün aːtɨn. </ta>
            <ta e="T98" id="Seg_4163" s="T90">Onuga bu͡o čugahɨːllar ühü kuttana kuttana čugahɨːllar ühü. </ta>
            <ta e="T104" id="Seg_4164" s="T99">"Bu togo kelbikkitij bihigi hirbitiger? </ta>
            <ta e="T107" id="Seg_4165" s="T105">Togo bultugut? </ta>
            <ta e="T115" id="Seg_4166" s="T108">Bultanar hirbitin togo aldʼatagɨt", diːller ühü ogonnʼottor. </ta>
            <ta e="T123" id="Seg_4167" s="T116">Kaja bunna bu͡ollagɨna naselenije oŋoru͡oktara, kihi turu͡oga. </ta>
            <ta e="T130" id="Seg_4168" s="T124">Kihi ü͡öskü͡öge munna elbek kihi oloru͡oga. </ta>
            <ta e="T138" id="Seg_4169" s="T131">Oččogo peričalbɨt bu͡o iti baːr hurbujan kiːrbit. </ta>
            <ta e="T144" id="Seg_4170" s="T139">"Togo ɨraːgaj", diːller ühü, "peričalbɨt?" </ta>
            <ta e="T153" id="Seg_4171" s="T145">Ü͡ösküt küččügüj munna, ol ihin itinne bu͡olar ü͡ösküt. </ta>
            <ta e="T157" id="Seg_4172" s="T154">Itinne parahu͡ottar keli͡ektere. </ta>
            <ta e="T165" id="Seg_4173" s="T158">Oduːrguːllar ühü "tu͡ok aːtaj parahu͡ot" diːr ((LAUGH)). </ta>
            <ta e="T169" id="Seg_4174" s="T166">Tartar keler parahu͡ot? </ta>
            <ta e="T184" id="Seg_4175" s="T170">Dʼe ol kojut kojut kojut kojut ol ü͡öskeːn ü͡öskeːn bu Hɨndaːsskaŋ bu͡olan kaːlbɨt bu͡o. </ta>
            <ta e="T192" id="Seg_4176" s="T185">Usku͡olata hu͡oktar uraha dʼi͡ege ü͡öreteller bejelere tutannar. </ta>
            <ta e="T196" id="Seg_4177" s="T193">Baloktorgo (ü͡ö-) atɨːlɨːllar. </ta>
            <ta e="T208" id="Seg_4178" s="T197">Onton tupput dʼi͡elere baːr ete oččotoŋu, anɨ huːlbuta bu͡o ontuŋ, laːpkɨlara. </ta>
            <ta e="T223" id="Seg_4179" s="T209">Ol ihin Hɨndaːssko, Sɨndassko Daːrija tugu kimi Daːrija diːller et, iti ti͡ere aːttɨːllar ühü. </ta>
            <ta e="T227" id="Seg_4180" s="T224">Heː bu Hɨndaːsskanɨ. </ta>
            <ta e="T238" id="Seg_4181" s="T228">Iti bultanar hirderin bɨldʼaːbɨttar onton kojut kojut et iti bu͡olbuttara. </ta>
            <ta e="T250" id="Seg_4182" s="T239">Onton kojut iti bu kele iligine Lɨgɨjdar di͡en baːr etilere ühü. </ta>
            <ta e="T256" id="Seg_4183" s="T251">Eh kimner tigileːkter diːller ol. </ta>
            <ta e="T270" id="Seg_4184" s="T257">Itinne iti peričal ɨnaraː öttüger bu͡olla dʼi͡e onnulara baːllar, ikki üs dʼi͡e onnuta. </ta>
            <ta e="T278" id="Seg_4185" s="T271">Onno olorollor ol, bulčuttar olordoktorunan, Lɨgɨjdar kelbittere. </ta>
            <ta e="T281" id="Seg_4186" s="T279">Lɨgɨjder, tigileːkter. </ta>
            <ta e="T287" id="Seg_4187" s="T282">Ol kelbittere üs dʼaktardaːktar olor. </ta>
            <ta e="T293" id="Seg_4188" s="T288">Biːrdere ojuːn, (biːrde-) ikkilere povar. </ta>
            <ta e="T306" id="Seg_4189" s="T294">Dʼe bu kihini ölördö ölörbütünen kelbitter oloru povar gɨnaːrɨ egelbittere, kuhagan kihiler. </ta>
            <ta e="T313" id="Seg_4190" s="T307">Kak budta tu͡oktara hülügetter eni to. </ta>
            <ta e="T323" id="Seg_4191" s="T314">Ol kelen kelenner munna kelbittere bulčuttar biːrdere bu͡o bɨhɨj. </ta>
            <ta e="T332" id="Seg_4192" s="T324">Biːrdere körügös, biːrdere ɨtɨːhɨt, bulčuttar bihi͡ettere ühü kepsiːller. </ta>
            <ta e="T349" id="Seg_4193" s="T333">Olor bu͡ollagɨna kihini baratan kelen kelenner bu ogonnʼordor Malɨj di͡en hirge Pu͡orotaj di͡en kihi baːr ete. </ta>
            <ta e="T354" id="Seg_4194" s="T350">Onuga barbɨttar bu kihiler. </ta>
            <ta e="T366" id="Seg_4195" s="T355">"Kaja dogo onnuk onnuk heriː (tumsunaːk ɨttar) kejeller, ölörü͡öksütter (keller) tigileːkter." </ta>
            <ta e="T372" id="Seg_4196" s="T367">De ol kürüːller Malɨjga, küreːbittere. </ta>
            <ta e="T385" id="Seg_4197" s="T373">Ol küreːn tiːjbitter Pu͡orotajga tiːjenner diː kepsiːller kaja onnuktar kelliler kihini baraːppɨttar. </ta>
            <ta e="T393" id="Seg_4198" s="T385">Ki͡eŋ kü͡ölten ustannar, haŋa ürek üstün kelbitter di͡en. </ta>
            <ta e="T400" id="Seg_4199" s="T394">Ol ogonnʼorgo tiːjenner bu kihiler dʼe. </ta>
            <ta e="T409" id="Seg_4200" s="T401">"Dʼe körüges körülüː olor üːhe hu͡opkaga taksaŋŋɨn", di͡en. </ta>
            <ta e="T424" id="Seg_4201" s="T410">Biːrdere ɨtɨːhɨttara (kenne) kiːllerteːbit dogottorun innʼe gɨnan baran dʼe kaːn butugas busputtar bu ogonnʼor. </ta>
            <ta e="T427" id="Seg_4202" s="T425">Uraha dʼi͡eleːkter. </ta>
            <ta e="T436" id="Seg_4203" s="T428">Dʼe butugastaːn baran bu͡o butugahɨgar bu͡olla kimi kerpit. </ta>
            <ta e="T443" id="Seg_4204" s="T437">Hɨn di͡eččiler oččogo anɨ hɨn li͡eske. </ta>
            <ta e="T453" id="Seg_4205" s="T444">Li͡eskanɨ tabaːkka di͡eri kördük kerte-kerte bu͡o butugaska kupput bu͡o. </ta>
            <ta e="T467" id="Seg_4206" s="T454">"Aččɨktaːn iher kihiler butugas ihi͡ektere", di͡en, "ol ihi͡ektere", di͡en ol (butugas) kerderbit bu͡o. </ta>
            <ta e="T481" id="Seg_4207" s="T468">"Innʼen kantas gɨnnagɨna ol tojonnorun kabɨrgatɨn kaja oksoːruŋ", di͡en,"( "(bɨtɨgɨ-) eː kiminen batɨjanan. </ta>
            <ta e="T492" id="Seg_4208" s="T482">Oččogo ere kɨ͡ajɨ͡akpɨt", di͡ebit bu͡o, onton haŋarda kɨ͡ajɨ͡akpɨt kajdak eme". </ta>
            <ta e="T504" id="Seg_4209" s="T493">De ol kelbitter ol butugastaːbɨttar dʼe iheller "butugastaːŋ di͡ebit bu͡o butugastaːbɨttar. </ta>
            <ta e="T514" id="Seg_4210" s="T505">Dʼe ol kɨːnnʼara hɨttaktarɨnan de kelbitter bu͡o ol ečikejeleriŋ. </ta>
            <ta e="T523" id="Seg_4211" s="T515">Kelenner dʼaktattarɨ kiːlletteːn de bu͡o ogonnʼor tohujar kiːllettiːr. </ta>
            <ta e="T530" id="Seg_4212" s="T524">"Kaja onnuktar mannɨktar östö kepseːŋ", di͡en. </ta>
            <ta e="T537" id="Seg_4213" s="T531">De ol ogonnʼorgo tiːjen kepsiːller bu͡o: </ta>
            <ta e="T543" id="Seg_4214" s="T537">"Emeːksini egellibit bihigi iti hirdʼippit, diːbit. </ta>
            <ta e="T548" id="Seg_4215" s="T544">Ojuːn emeːksin, iti povardarbɨt. </ta>
            <ta e="T563" id="Seg_4216" s="T549">Oččogo ol ojuːn emeːksin hirdeːn egelle (bihi͡eke) ehi͡eke", di͡en ol kepsiːller, bu͡o kihini barappɨttar." </ta>
            <ta e="T576" id="Seg_4217" s="T564">Kaːnnarɨn bu͡olla bahaktarɨn munna hotollor ebit kihileri kerde kerde hiː hiː hiːller. </ta>
            <ta e="T589" id="Seg_4218" s="T577">"Oː tu͡ok aːttaːk kaːnɨgar bihillibit dʼonnorgutuj, dʼe ahaːŋ", di͡en ogonnʼor dʼe ahatar. </ta>
            <ta e="T605" id="Seg_4219" s="T590">De ol (ahaː-) ahappɨtɨn kenne de ol oː kallaːmmɨt kajdak kajdak bu͡olar de bɨlɨppɨt kojdor. </ta>
            <ta e="T609" id="Seg_4220" s="T606">"Tu͡ok bu͡olar"; di͡ebite. </ta>
            <ta e="T618" id="Seg_4221" s="T610">Dʼe ol ikki kamnastaːgɨ ikki öttüge olorput bu͡o. </ta>
            <ta e="T627" id="Seg_4222" s="T619">Dʼe ol kantas gɨmmɨtɨgar kabɨrgatɨn kaja oksubuttara batɨjanan. </ta>
            <ta e="T634" id="Seg_4223" s="T628">Anɨ iti ölörsöllörüger diːr kinoːlarga di͡eri. </ta>
            <ta e="T639" id="Seg_4224" s="T635">Ol kɨ͡ajbɨttar ühü ontularɨn. </ta>
            <ta e="T643" id="Seg_4225" s="T640">Dʼe aŋardarɨn ölörtöːbüttere. </ta>
            <ta e="T650" id="Seg_4226" s="T644">Hette kihite ebit du͡o kas du͡o. </ta>
            <ta e="T662" id="Seg_4227" s="T651">Dʼe onton dʼaktattar kaːlbɨttarɨgar bu dʼaktattar dʼe etiheller ühü bahaktarɨn bɨldʼɨhannar. </ta>
            <ta e="T672" id="Seg_4228" s="T663">"Min erim bahaga, min erim bahaga", deheːktiːller ühü kotokular. </ta>
            <ta e="T688" id="Seg_4229" s="T673">"Tu͡ok suːka kɨrgɨttaraj itileri ölöttöːn keːhiŋ", di͡ebit bu͡o tu͡ok sustogoj ogonnʼoro bu͡olla da bihi͡ene Pu͡orotaj. </ta>
            <ta e="T692" id="Seg_4230" s="T689">Ölöttöːn keːspitter muŋnaːktar. </ta>
            <ta e="T701" id="Seg_4231" s="T693">Onton ol emeːksini dʼe ölörö hatɨːllar ojuːn emeːksini. </ta>
            <ta e="T708" id="Seg_4232" s="T702">Uːga da timirdeller dʼe hakordaːn baran. </ta>
            <ta e="T711" id="Seg_4233" s="T709">Törüt timirbet. </ta>
            <ta e="T720" id="Seg_4234" s="T712">Ajagalɨː hataːn u͡otun otton bu͡o u͡ot ihiger bɨragallar. </ta>
            <ta e="T725" id="Seg_4235" s="T721">U͡ottara karaːrčɨ utujar ühü. </ta>
            <ta e="T730" id="Seg_4236" s="T726">Onno da di͡ebit bu͡o: </ta>
            <ta e="T738" id="Seg_4237" s="T730">"Tukuːlar min kün emeːksin etim kaja abaːhɨ bu͡oltappɨn. </ta>
            <ta e="T748" id="Seg_4238" s="T739">Dʼommun baraːbɨttara bu bu͡olla kihini sü͡öhünü köröːrübün hirdeːn kelbitim. </ta>
            <ta e="T757" id="Seg_4239" s="T749">Minigin erejdeːmeŋ min itinnik ke ölü͡öm hu͡oga", diːr. </ta>
            <ta e="T773" id="Seg_4240" s="T758">"Kɨːh ogoto haŋardɨː bɨrtak bu͡olbut kɨːh ogoto moːjbunan atɨllatɨn, oččogo ölü͡öm", diːr, "ol moːjbunan atɨllappɨttar." </ta>
            <ta e="T779" id="Seg_4241" s="T774">De ol emeːksinnere ölön kaːlbɨt. </ta>
            <ta e="T784" id="Seg_4242" s="T779">Dʼe elete ol kepseːbittere öjdöːböppün. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_4243" s="T1">onton</ta>
            <ta e="T3" id="Seg_4244" s="T2">biːrde</ta>
            <ta e="T4" id="Seg_4245" s="T3">bultan-a</ta>
            <ta e="T5" id="Seg_4246" s="T4">taks-ɨ-bɨt-tara</ta>
            <ta e="T6" id="Seg_4247" s="T5">ogonnʼot-tor</ta>
            <ta e="T8" id="Seg_4248" s="T7">ogonnʼot-tor</ta>
            <ta e="T9" id="Seg_4249" s="T8">kel-el-ler</ta>
            <ta e="T10" id="Seg_4250" s="T9">u͡olat-tar</ta>
            <ta e="T12" id="Seg_4251" s="T11">kaja</ta>
            <ta e="T13" id="Seg_4252" s="T12">Hɨndaːsska-bɨtɨ-gar</ta>
            <ta e="T14" id="Seg_4253" s="T13">dʼi͡e</ta>
            <ta e="T15" id="Seg_4254" s="T14">tur-but</ta>
            <ta e="T16" id="Seg_4255" s="T15">di͡e-bit-ter</ta>
            <ta e="T18" id="Seg_4256" s="T17">kajdi͡ek</ta>
            <ta e="T19" id="Seg_4257" s="T18">bultan-aːččɨ-bɨt</ta>
            <ta e="T21" id="Seg_4258" s="T20">oː</ta>
            <ta e="T22" id="Seg_4259" s="T21">dʼe</ta>
            <ta e="T23" id="Seg_4260" s="T22">bu</ta>
            <ta e="T24" id="Seg_4261" s="T23">dʼi͡e</ta>
            <ta e="T25" id="Seg_4262" s="T24">tur-but-u-gar</ta>
            <ta e="T26" id="Seg_4263" s="T25">kuttan-al-lar</ta>
            <ta e="T27" id="Seg_4264" s="T26">kel-i͡ek-teri-n</ta>
            <ta e="T28" id="Seg_4265" s="T27">ühü</ta>
            <ta e="T29" id="Seg_4266" s="T28">nuːčča-lar</ta>
            <ta e="T30" id="Seg_4267" s="T29">baːl-lar</ta>
            <ta e="T31" id="Seg_4268" s="T30">di͡e-n</ta>
            <ta e="T33" id="Seg_4269" s="T32">oː</ta>
            <ta e="T34" id="Seg_4270" s="T33">dʼe</ta>
            <ta e="T35" id="Seg_4271" s="T34">bu</ta>
            <ta e="T36" id="Seg_4272" s="T35">hir-biti-n</ta>
            <ta e="T37" id="Seg_4273" s="T36">bult-a</ta>
            <ta e="T38" id="Seg_4274" s="T37">hu͡ok</ta>
            <ta e="T39" id="Seg_4275" s="T38">oŋor-du-lar</ta>
            <ta e="T40" id="Seg_4276" s="T39">kajdak</ta>
            <ta e="T41" id="Seg_4277" s="T40">bu͡ol-aːččɨ-bɨt=ɨj</ta>
            <ta e="T42" id="Seg_4278" s="T41">diː-l-ler</ta>
            <ta e="T43" id="Seg_4279" s="T42">ühü</ta>
            <ta e="T44" id="Seg_4280" s="T43">ol</ta>
            <ta e="T45" id="Seg_4281" s="T44">ogonnʼot-tor</ta>
            <ta e="T46" id="Seg_4282" s="T45">ke</ta>
            <ta e="T48" id="Seg_4283" s="T47">d-e-h-e-bit</ta>
            <ta e="T49" id="Seg_4284" s="T48">diː-l-ler</ta>
            <ta e="T51" id="Seg_4285" s="T50">kepset-el-leri-n</ta>
            <ta e="T52" id="Seg_4286" s="T51">ist-e-bin</ta>
            <ta e="T53" id="Seg_4287" s="T52">ehe-ler-i-m</ta>
            <ta e="T54" id="Seg_4288" s="T53">ke</ta>
            <ta e="T56" id="Seg_4289" s="T55">onton</ta>
            <ta e="T57" id="Seg_4290" s="T56">dʼe</ta>
            <ta e="T58" id="Seg_4291" s="T57">onton</ta>
            <ta e="T59" id="Seg_4292" s="T58">kel-el-ler</ta>
            <ta e="T61" id="Seg_4293" s="T60">kel-en-ner</ta>
            <ta e="T62" id="Seg_4294" s="T61">kör-büt-tere</ta>
            <ta e="T63" id="Seg_4295" s="T62">ikki</ta>
            <ta e="T64" id="Seg_4296" s="T63">kihi</ta>
            <ta e="T65" id="Seg_4297" s="T64">hɨldʼ-al-lar</ta>
            <ta e="T66" id="Seg_4298" s="T65">ühü</ta>
            <ta e="T68" id="Seg_4299" s="T67">dʼaktar-ɨ-n</ta>
            <ta e="T69" id="Seg_4300" s="T68">aːt-a</ta>
            <ta e="T70" id="Seg_4301" s="T69">Daːrija</ta>
            <ta e="T72" id="Seg_4302" s="T71">er-i-n</ta>
            <ta e="T73" id="Seg_4303" s="T72">aːt-a</ta>
            <ta e="T74" id="Seg_4304" s="T73">kim</ta>
            <ta e="T75" id="Seg_4305" s="T74">di͡e-bit-ter=ij</ta>
            <ta e="T76" id="Seg_4306" s="T75">ke</ta>
            <ta e="T77" id="Seg_4307" s="T76">da</ta>
            <ta e="T78" id="Seg_4308" s="T77">oluja</ta>
            <ta e="T79" id="Seg_4309" s="T78">bagajɨ</ta>
            <ta e="T80" id="Seg_4310" s="T79">aːt-taːk</ta>
            <ta e="T81" id="Seg_4311" s="T80">kihi</ta>
            <ta e="T82" id="Seg_4312" s="T81">kim</ta>
            <ta e="T83" id="Seg_4313" s="T82">ere</ta>
            <ta e="T84" id="Seg_4314" s="T83">di͡e-čči-ler</ta>
            <ta e="T85" id="Seg_4315" s="T84">et</ta>
            <ta e="T87" id="Seg_4316" s="T86">törüt</ta>
            <ta e="T88" id="Seg_4317" s="T87">öjdöː-böp-pün</ta>
            <ta e="T89" id="Seg_4318" s="T88">aːt-ɨ-n</ta>
            <ta e="T91" id="Seg_4319" s="T90">onu-ga</ta>
            <ta e="T92" id="Seg_4320" s="T91">bu͡o</ta>
            <ta e="T93" id="Seg_4321" s="T92">čugahɨː-l-lar</ta>
            <ta e="T94" id="Seg_4322" s="T93">ühü</ta>
            <ta e="T95" id="Seg_4323" s="T94">kuttan-a</ta>
            <ta e="T96" id="Seg_4324" s="T95">kuttan-a</ta>
            <ta e="T97" id="Seg_4325" s="T96">čugahɨː-l-lar</ta>
            <ta e="T98" id="Seg_4326" s="T97">ühü</ta>
            <ta e="T100" id="Seg_4327" s="T99">bu</ta>
            <ta e="T101" id="Seg_4328" s="T100">togo</ta>
            <ta e="T102" id="Seg_4329" s="T101">kel-bik-kit=ij</ta>
            <ta e="T103" id="Seg_4330" s="T102">bihigi</ta>
            <ta e="T104" id="Seg_4331" s="T103">hir-biti-ger</ta>
            <ta e="T106" id="Seg_4332" s="T105">togo</ta>
            <ta e="T107" id="Seg_4333" s="T106">bul-tu-gut</ta>
            <ta e="T109" id="Seg_4334" s="T108">bultan-ar</ta>
            <ta e="T110" id="Seg_4335" s="T109">hir-biti-n</ta>
            <ta e="T111" id="Seg_4336" s="T110">togo</ta>
            <ta e="T112" id="Seg_4337" s="T111">aldʼat-a-gɨt</ta>
            <ta e="T113" id="Seg_4338" s="T112">diː-l-ler</ta>
            <ta e="T114" id="Seg_4339" s="T113">ühü</ta>
            <ta e="T115" id="Seg_4340" s="T114">ogonnʼot-tor</ta>
            <ta e="T117" id="Seg_4341" s="T116">kaja</ta>
            <ta e="T118" id="Seg_4342" s="T117">bunna</ta>
            <ta e="T119" id="Seg_4343" s="T118">bu͡ollagɨna</ta>
            <ta e="T120" id="Seg_4344" s="T119">naselenije</ta>
            <ta e="T121" id="Seg_4345" s="T120">oŋor-u͡ok-tara</ta>
            <ta e="T122" id="Seg_4346" s="T121">kihi</ta>
            <ta e="T123" id="Seg_4347" s="T122">tur-u͡og-a</ta>
            <ta e="T125" id="Seg_4348" s="T124">kihi</ta>
            <ta e="T126" id="Seg_4349" s="T125">ü͡ösk-ü͡ög-e</ta>
            <ta e="T127" id="Seg_4350" s="T126">munna</ta>
            <ta e="T128" id="Seg_4351" s="T127">elbek</ta>
            <ta e="T129" id="Seg_4352" s="T128">kihi</ta>
            <ta e="T130" id="Seg_4353" s="T129">olor-u͡og-a</ta>
            <ta e="T132" id="Seg_4354" s="T131">oččogo</ta>
            <ta e="T133" id="Seg_4355" s="T132">peričal-bɨt</ta>
            <ta e="T134" id="Seg_4356" s="T133">bu͡o</ta>
            <ta e="T135" id="Seg_4357" s="T134">iti</ta>
            <ta e="T136" id="Seg_4358" s="T135">baːr</ta>
            <ta e="T137" id="Seg_4359" s="T136">hurbuj-an</ta>
            <ta e="T138" id="Seg_4360" s="T137">kiːr-bit</ta>
            <ta e="T140" id="Seg_4361" s="T139">togo</ta>
            <ta e="T141" id="Seg_4362" s="T140">ɨraːg-a=j</ta>
            <ta e="T142" id="Seg_4363" s="T141">diː-l-ler</ta>
            <ta e="T143" id="Seg_4364" s="T142">ühü</ta>
            <ta e="T144" id="Seg_4365" s="T143">peričal-bɨt</ta>
            <ta e="T146" id="Seg_4366" s="T145">ü͡ös-küt</ta>
            <ta e="T147" id="Seg_4367" s="T146">küččügüj</ta>
            <ta e="T148" id="Seg_4368" s="T147">munna</ta>
            <ta e="T149" id="Seg_4369" s="T148">ol</ta>
            <ta e="T150" id="Seg_4370" s="T149">ihin</ta>
            <ta e="T151" id="Seg_4371" s="T150">itinne</ta>
            <ta e="T152" id="Seg_4372" s="T151">bu͡ol-ar</ta>
            <ta e="T153" id="Seg_4373" s="T152">ü͡ös-küt</ta>
            <ta e="T155" id="Seg_4374" s="T154">itinne</ta>
            <ta e="T156" id="Seg_4375" s="T155">parahu͡ot-tar</ta>
            <ta e="T157" id="Seg_4376" s="T156">kel-i͡ek-tere</ta>
            <ta e="T159" id="Seg_4377" s="T158">oduːrguː-l-lar</ta>
            <ta e="T160" id="Seg_4378" s="T159">ühü</ta>
            <ta e="T161" id="Seg_4379" s="T160">tu͡ok</ta>
            <ta e="T162" id="Seg_4380" s="T161">aːt-a=j</ta>
            <ta e="T163" id="Seg_4381" s="T162">parahu͡ot</ta>
            <ta e="T164" id="Seg_4382" s="T163">diː-r</ta>
            <ta e="T167" id="Seg_4383" s="T166">tart-ar</ta>
            <ta e="T168" id="Seg_4384" s="T167">kel-er</ta>
            <ta e="T169" id="Seg_4385" s="T168">parahu͡ot</ta>
            <ta e="T171" id="Seg_4386" s="T170">dʼe</ta>
            <ta e="T172" id="Seg_4387" s="T171">ol</ta>
            <ta e="T173" id="Seg_4388" s="T172">kojut</ta>
            <ta e="T174" id="Seg_4389" s="T173">kojut</ta>
            <ta e="T175" id="Seg_4390" s="T174">kojut</ta>
            <ta e="T176" id="Seg_4391" s="T175">kojut</ta>
            <ta e="T177" id="Seg_4392" s="T176">ol</ta>
            <ta e="T178" id="Seg_4393" s="T177">ü͡öskeː-n</ta>
            <ta e="T179" id="Seg_4394" s="T178">ü͡öskeː-n</ta>
            <ta e="T180" id="Seg_4395" s="T179">bu</ta>
            <ta e="T181" id="Seg_4396" s="T180">Hɨndaːsska-ŋ</ta>
            <ta e="T182" id="Seg_4397" s="T181">bu͡ol-an</ta>
            <ta e="T183" id="Seg_4398" s="T182">kaːl-bɨt</ta>
            <ta e="T184" id="Seg_4399" s="T183">bu͡o</ta>
            <ta e="T186" id="Seg_4400" s="T185">usku͡ola-ta</ta>
            <ta e="T187" id="Seg_4401" s="T186">hu͡ok-tar</ta>
            <ta e="T188" id="Seg_4402" s="T187">uraha</ta>
            <ta e="T189" id="Seg_4403" s="T188">dʼi͡e-ge</ta>
            <ta e="T190" id="Seg_4404" s="T189">ü͡öret-el-ler</ta>
            <ta e="T191" id="Seg_4405" s="T190">beje-lere</ta>
            <ta e="T192" id="Seg_4406" s="T191">tut-an-nar</ta>
            <ta e="T194" id="Seg_4407" s="T193">balok-tor-go</ta>
            <ta e="T196" id="Seg_4408" s="T195">atɨːlɨː-l-lar</ta>
            <ta e="T198" id="Seg_4409" s="T197">onton</ta>
            <ta e="T199" id="Seg_4410" s="T198">tup-put</ta>
            <ta e="T200" id="Seg_4411" s="T199">dʼi͡e-lere</ta>
            <ta e="T201" id="Seg_4412" s="T200">baːr</ta>
            <ta e="T202" id="Seg_4413" s="T201">e-t-e</ta>
            <ta e="T203" id="Seg_4414" s="T202">oččotoŋu</ta>
            <ta e="T204" id="Seg_4415" s="T203">anɨ</ta>
            <ta e="T205" id="Seg_4416" s="T204">huːl-but-a</ta>
            <ta e="T206" id="Seg_4417" s="T205">bu͡o</ta>
            <ta e="T207" id="Seg_4418" s="T206">on-tu-ŋ</ta>
            <ta e="T208" id="Seg_4419" s="T207">laːpkɨ-lara</ta>
            <ta e="T210" id="Seg_4420" s="T209">ol</ta>
            <ta e="T211" id="Seg_4421" s="T210">ihin</ta>
            <ta e="T212" id="Seg_4422" s="T211">Hɨndaːssko</ta>
            <ta e="T213" id="Seg_4423" s="T212">Sɨndassko</ta>
            <ta e="T214" id="Seg_4424" s="T213">Daːrija</ta>
            <ta e="T215" id="Seg_4425" s="T214">tug-u</ta>
            <ta e="T216" id="Seg_4426" s="T215">kim-i</ta>
            <ta e="T217" id="Seg_4427" s="T216">Daːrija</ta>
            <ta e="T218" id="Seg_4428" s="T217">diː-l-ler</ta>
            <ta e="T219" id="Seg_4429" s="T218">et</ta>
            <ta e="T220" id="Seg_4430" s="T219">iti</ta>
            <ta e="T221" id="Seg_4431" s="T220">ti͡ere</ta>
            <ta e="T222" id="Seg_4432" s="T221">aːttɨː-l-lar</ta>
            <ta e="T223" id="Seg_4433" s="T222">ühü</ta>
            <ta e="T225" id="Seg_4434" s="T224">heː</ta>
            <ta e="T226" id="Seg_4435" s="T225">bu</ta>
            <ta e="T227" id="Seg_4436" s="T226">Hɨndaːsska-nɨ</ta>
            <ta e="T229" id="Seg_4437" s="T228">iti</ta>
            <ta e="T230" id="Seg_4438" s="T229">bultan-ar</ta>
            <ta e="T231" id="Seg_4439" s="T230">hir-deri-n</ta>
            <ta e="T232" id="Seg_4440" s="T231">bɨldʼaː-bɨt-tar</ta>
            <ta e="T233" id="Seg_4441" s="T232">onton</ta>
            <ta e="T234" id="Seg_4442" s="T233">kojut</ta>
            <ta e="T235" id="Seg_4443" s="T234">kojut</ta>
            <ta e="T236" id="Seg_4444" s="T235">et</ta>
            <ta e="T237" id="Seg_4445" s="T236">iti</ta>
            <ta e="T238" id="Seg_4446" s="T237">bu͡ol-but-tara</ta>
            <ta e="T240" id="Seg_4447" s="T239">onton</ta>
            <ta e="T241" id="Seg_4448" s="T240">kojut</ta>
            <ta e="T242" id="Seg_4449" s="T241">iti</ta>
            <ta e="T243" id="Seg_4450" s="T242">bu</ta>
            <ta e="T244" id="Seg_4451" s="T243">kel-e</ta>
            <ta e="T245" id="Seg_4452" s="T244">ilig-ine</ta>
            <ta e="T246" id="Seg_4453" s="T245">Lɨgɨj-dar</ta>
            <ta e="T247" id="Seg_4454" s="T246">di͡e-n</ta>
            <ta e="T248" id="Seg_4455" s="T247">baːr</ta>
            <ta e="T249" id="Seg_4456" s="T248">e-ti-lere</ta>
            <ta e="T250" id="Seg_4457" s="T249">ühü</ta>
            <ta e="T252" id="Seg_4458" s="T251">eh</ta>
            <ta e="T253" id="Seg_4459" s="T252">kim-ner</ta>
            <ta e="T254" id="Seg_4460" s="T253">tigileːk-ter</ta>
            <ta e="T255" id="Seg_4461" s="T254">diː-l-ler</ta>
            <ta e="T256" id="Seg_4462" s="T255">ol</ta>
            <ta e="T258" id="Seg_4463" s="T257">itinne</ta>
            <ta e="T259" id="Seg_4464" s="T258">iti</ta>
            <ta e="T260" id="Seg_4465" s="T259">peričal</ta>
            <ta e="T261" id="Seg_4466" s="T260">ɨnaraː</ta>
            <ta e="T262" id="Seg_4467" s="T261">ött-ü-ger</ta>
            <ta e="T263" id="Seg_4468" s="T262">bu͡olla</ta>
            <ta e="T264" id="Seg_4469" s="T263">dʼi͡e</ta>
            <ta e="T265" id="Seg_4470" s="T264">onnu-lar-a</ta>
            <ta e="T266" id="Seg_4471" s="T265">baːl-lar</ta>
            <ta e="T267" id="Seg_4472" s="T266">ikki</ta>
            <ta e="T268" id="Seg_4473" s="T267">üs</ta>
            <ta e="T269" id="Seg_4474" s="T268">dʼi͡e</ta>
            <ta e="T270" id="Seg_4475" s="T269">onnu-ta</ta>
            <ta e="T272" id="Seg_4476" s="T271">onno</ta>
            <ta e="T273" id="Seg_4477" s="T272">olor-ol-lor</ta>
            <ta e="T274" id="Seg_4478" s="T273">ol</ta>
            <ta e="T275" id="Seg_4479" s="T274">bulčut-tar</ta>
            <ta e="T276" id="Seg_4480" s="T275">olor-dok-torunan</ta>
            <ta e="T277" id="Seg_4481" s="T276">Lɨgɨj-dar</ta>
            <ta e="T278" id="Seg_4482" s="T277">kel-bit-tere</ta>
            <ta e="T280" id="Seg_4483" s="T279">Lɨgɨj-der</ta>
            <ta e="T281" id="Seg_4484" s="T280">tigileːk-ter</ta>
            <ta e="T283" id="Seg_4485" s="T282">ol</ta>
            <ta e="T284" id="Seg_4486" s="T283">kel-bit-tere</ta>
            <ta e="T285" id="Seg_4487" s="T284">üs</ta>
            <ta e="T286" id="Seg_4488" s="T285">dʼaktar-daːk-tar</ta>
            <ta e="T287" id="Seg_4489" s="T286">o-lor</ta>
            <ta e="T289" id="Seg_4490" s="T288">biːr-dere</ta>
            <ta e="T290" id="Seg_4491" s="T289">ojuːn</ta>
            <ta e="T292" id="Seg_4492" s="T291">ikki-lere</ta>
            <ta e="T293" id="Seg_4493" s="T292">povar</ta>
            <ta e="T295" id="Seg_4494" s="T294">dʼe</ta>
            <ta e="T296" id="Seg_4495" s="T295">bu</ta>
            <ta e="T297" id="Seg_4496" s="T296">kihi-ni</ta>
            <ta e="T298" id="Seg_4497" s="T297">ölör-d-ö</ta>
            <ta e="T299" id="Seg_4498" s="T298">ölör-büt-ü-nen</ta>
            <ta e="T300" id="Seg_4499" s="T299">kel-bit-ter</ta>
            <ta e="T301" id="Seg_4500" s="T300">o-lor-u</ta>
            <ta e="T302" id="Seg_4501" s="T301">povar</ta>
            <ta e="T303" id="Seg_4502" s="T302">gɨn-aːrɨ</ta>
            <ta e="T304" id="Seg_4503" s="T303">egel-bit-tere</ta>
            <ta e="T305" id="Seg_4504" s="T304">kuhagan</ta>
            <ta e="T306" id="Seg_4505" s="T305">kihi-ler</ta>
            <ta e="T308" id="Seg_4506" s="T307">kak</ta>
            <ta e="T309" id="Seg_4507" s="T308">budta</ta>
            <ta e="T310" id="Seg_4508" s="T309">tu͡ok-tara</ta>
            <ta e="T311" id="Seg_4509" s="T310">hülüget-ter</ta>
            <ta e="T312" id="Seg_4510" s="T311">eni</ta>
            <ta e="T313" id="Seg_4511" s="T312">to</ta>
            <ta e="T315" id="Seg_4512" s="T314">ol</ta>
            <ta e="T316" id="Seg_4513" s="T315">kel-en</ta>
            <ta e="T317" id="Seg_4514" s="T316">kel-en-ner</ta>
            <ta e="T318" id="Seg_4515" s="T317">munna</ta>
            <ta e="T319" id="Seg_4516" s="T318">kel-bit-tere</ta>
            <ta e="T320" id="Seg_4517" s="T319">bulčut-tar</ta>
            <ta e="T321" id="Seg_4518" s="T320">biːr-dere</ta>
            <ta e="T322" id="Seg_4519" s="T321">bu͡o</ta>
            <ta e="T323" id="Seg_4520" s="T322">bɨhɨj</ta>
            <ta e="T325" id="Seg_4521" s="T324">biːr-dere</ta>
            <ta e="T326" id="Seg_4522" s="T325">körügös</ta>
            <ta e="T327" id="Seg_4523" s="T326">biːr-dere</ta>
            <ta e="T328" id="Seg_4524" s="T327">ɨt-ɨː-hɨt</ta>
            <ta e="T329" id="Seg_4525" s="T328">bulčut-tar</ta>
            <ta e="T330" id="Seg_4526" s="T329">bihi͡ettere</ta>
            <ta e="T331" id="Seg_4527" s="T330">ühü</ta>
            <ta e="T332" id="Seg_4528" s="T331">kepsiː-l-ler</ta>
            <ta e="T334" id="Seg_4529" s="T333">o-lor</ta>
            <ta e="T335" id="Seg_4530" s="T334">bu͡ollagɨna</ta>
            <ta e="T336" id="Seg_4531" s="T335">kihi-ni</ta>
            <ta e="T337" id="Seg_4532" s="T336">bar-a-t-an</ta>
            <ta e="T338" id="Seg_4533" s="T337">kel-en</ta>
            <ta e="T339" id="Seg_4534" s="T338">kel-en-ner</ta>
            <ta e="T340" id="Seg_4535" s="T339">bu</ta>
            <ta e="T341" id="Seg_4536" s="T340">ogonnʼor-dor</ta>
            <ta e="T342" id="Seg_4537" s="T341">Malɨj</ta>
            <ta e="T343" id="Seg_4538" s="T342">di͡e-n</ta>
            <ta e="T344" id="Seg_4539" s="T343">hir-ge</ta>
            <ta e="T345" id="Seg_4540" s="T344">Pu͡orotaj</ta>
            <ta e="T346" id="Seg_4541" s="T345">di͡e-n</ta>
            <ta e="T347" id="Seg_4542" s="T346">kihi</ta>
            <ta e="T348" id="Seg_4543" s="T347">baːr</ta>
            <ta e="T349" id="Seg_4544" s="T348">e-t-e</ta>
            <ta e="T351" id="Seg_4545" s="T350">onu-ga</ta>
            <ta e="T352" id="Seg_4546" s="T351">bar-bɨt-tar</ta>
            <ta e="T353" id="Seg_4547" s="T352">bu</ta>
            <ta e="T354" id="Seg_4548" s="T353">kihi-ler</ta>
            <ta e="T356" id="Seg_4549" s="T355">kaja</ta>
            <ta e="T357" id="Seg_4550" s="T356">dogo</ta>
            <ta e="T358" id="Seg_4551" s="T357">onnuk</ta>
            <ta e="T359" id="Seg_4552" s="T358">onnuk</ta>
            <ta e="T360" id="Seg_4553" s="T359">heriː</ta>
            <ta e="T361" id="Seg_4554" s="T360">tumsu-naːk</ta>
            <ta e="T362" id="Seg_4555" s="T361">ɨt-tar</ta>
            <ta e="T363" id="Seg_4556" s="T362">kej-el-ler</ta>
            <ta e="T364" id="Seg_4557" s="T363">ölör-ü͡ök-süt-ter</ta>
            <ta e="T365" id="Seg_4558" s="T364">kel-ler</ta>
            <ta e="T366" id="Seg_4559" s="T365">tigileːk-ter</ta>
            <ta e="T368" id="Seg_4560" s="T367">de</ta>
            <ta e="T369" id="Seg_4561" s="T368">ol</ta>
            <ta e="T370" id="Seg_4562" s="T369">kürüː-l-ler</ta>
            <ta e="T371" id="Seg_4563" s="T370">Malɨj-ga</ta>
            <ta e="T372" id="Seg_4564" s="T371">küreː-bit-tere</ta>
            <ta e="T374" id="Seg_4565" s="T373">ol</ta>
            <ta e="T375" id="Seg_4566" s="T374">küreː-n</ta>
            <ta e="T376" id="Seg_4567" s="T375">tiːj-bit-ter</ta>
            <ta e="T377" id="Seg_4568" s="T376">Pu͡orotaj-ga</ta>
            <ta e="T378" id="Seg_4569" s="T377">tiːj-en-ner</ta>
            <ta e="T379" id="Seg_4570" s="T378">diː</ta>
            <ta e="T380" id="Seg_4571" s="T379">kepsiː-l-ler</ta>
            <ta e="T381" id="Seg_4572" s="T380">kaja</ta>
            <ta e="T382" id="Seg_4573" s="T381">onnuk-tar</ta>
            <ta e="T383" id="Seg_4574" s="T382">kel-li-ler</ta>
            <ta e="T384" id="Seg_4575" s="T383">kihi-ni</ta>
            <ta e="T385" id="Seg_4576" s="T384">baraː-p-pɨt-tar</ta>
            <ta e="T386" id="Seg_4577" s="T385">ki͡eŋ</ta>
            <ta e="T387" id="Seg_4578" s="T386">kü͡öl-ten</ta>
            <ta e="T388" id="Seg_4579" s="T387">ust-an-nar</ta>
            <ta e="T389" id="Seg_4580" s="T388">haŋa</ta>
            <ta e="T390" id="Seg_4581" s="T389">ürek</ta>
            <ta e="T391" id="Seg_4582" s="T390">üstün</ta>
            <ta e="T392" id="Seg_4583" s="T391">kel-bit-ter</ta>
            <ta e="T393" id="Seg_4584" s="T392">di͡e-n</ta>
            <ta e="T395" id="Seg_4585" s="T394">ol</ta>
            <ta e="T396" id="Seg_4586" s="T395">ogonnʼor-go</ta>
            <ta e="T397" id="Seg_4587" s="T396">tiːj-en-ner</ta>
            <ta e="T398" id="Seg_4588" s="T397">bu</ta>
            <ta e="T399" id="Seg_4589" s="T398">kihi-ler</ta>
            <ta e="T400" id="Seg_4590" s="T399">dʼe</ta>
            <ta e="T402" id="Seg_4591" s="T401">dʼe</ta>
            <ta e="T403" id="Seg_4592" s="T402">körüges</ta>
            <ta e="T404" id="Seg_4593" s="T403">kör-ül-üː</ta>
            <ta e="T405" id="Seg_4594" s="T404">olor</ta>
            <ta e="T406" id="Seg_4595" s="T405">üːhe</ta>
            <ta e="T407" id="Seg_4596" s="T406">hu͡opka-ga</ta>
            <ta e="T408" id="Seg_4597" s="T407">taks-aŋ-ŋɨn</ta>
            <ta e="T409" id="Seg_4598" s="T408">di͡e-n</ta>
            <ta e="T411" id="Seg_4599" s="T410">biːr-dere</ta>
            <ta e="T412" id="Seg_4600" s="T411">ɨt-ɨː-hɨt-tara</ta>
            <ta e="T413" id="Seg_4601" s="T412">kenne</ta>
            <ta e="T414" id="Seg_4602" s="T413">kiːl-ler-teː-bit</ta>
            <ta e="T415" id="Seg_4603" s="T414">dogot-tor-u-n</ta>
            <ta e="T416" id="Seg_4604" s="T415">innʼe</ta>
            <ta e="T417" id="Seg_4605" s="T416">gɨn-an</ta>
            <ta e="T418" id="Seg_4606" s="T417">baran</ta>
            <ta e="T419" id="Seg_4607" s="T418">dʼe</ta>
            <ta e="T420" id="Seg_4608" s="T419">kaːn</ta>
            <ta e="T421" id="Seg_4609" s="T420">butugas</ta>
            <ta e="T422" id="Seg_4610" s="T421">bus-put-tar</ta>
            <ta e="T423" id="Seg_4611" s="T422">bu</ta>
            <ta e="T424" id="Seg_4612" s="T423">ogonnʼor</ta>
            <ta e="T426" id="Seg_4613" s="T425">uraha</ta>
            <ta e="T427" id="Seg_4614" s="T426">dʼi͡e-leːk-ter</ta>
            <ta e="T429" id="Seg_4615" s="T428">dʼe</ta>
            <ta e="T430" id="Seg_4616" s="T429">butugas-taː-n</ta>
            <ta e="T431" id="Seg_4617" s="T430">baran</ta>
            <ta e="T432" id="Seg_4618" s="T431">bu͡o</ta>
            <ta e="T433" id="Seg_4619" s="T432">butugah-ɨ-gar</ta>
            <ta e="T434" id="Seg_4620" s="T433">bu͡olla</ta>
            <ta e="T435" id="Seg_4621" s="T434">kim-i</ta>
            <ta e="T436" id="Seg_4622" s="T435">ker-pit</ta>
            <ta e="T438" id="Seg_4623" s="T437">hɨn</ta>
            <ta e="T439" id="Seg_4624" s="T438">di͡e-čči-ler</ta>
            <ta e="T440" id="Seg_4625" s="T439">oččogo</ta>
            <ta e="T441" id="Seg_4626" s="T440">anɨ</ta>
            <ta e="T442" id="Seg_4627" s="T441">hɨn</ta>
            <ta e="T443" id="Seg_4628" s="T442">li͡eske</ta>
            <ta e="T445" id="Seg_4629" s="T444">li͡eska-nɨ</ta>
            <ta e="T446" id="Seg_4630" s="T445">tabaːk-ka</ta>
            <ta e="T447" id="Seg_4631" s="T446">di͡eri</ta>
            <ta e="T448" id="Seg_4632" s="T447">kördük</ta>
            <ta e="T449" id="Seg_4633" s="T448">kert-e-kert-e</ta>
            <ta e="T450" id="Seg_4634" s="T449">bu͡o</ta>
            <ta e="T451" id="Seg_4635" s="T450">butugas-ka</ta>
            <ta e="T452" id="Seg_4636" s="T451">kup-put</ta>
            <ta e="T453" id="Seg_4637" s="T452">bu͡o</ta>
            <ta e="T455" id="Seg_4638" s="T454">aččɨktaː-n</ta>
            <ta e="T456" id="Seg_4639" s="T455">ih-er</ta>
            <ta e="T457" id="Seg_4640" s="T456">kihi-ler</ta>
            <ta e="T458" id="Seg_4641" s="T457">butugas</ta>
            <ta e="T459" id="Seg_4642" s="T458">ih-i͡ek-tere</ta>
            <ta e="T460" id="Seg_4643" s="T459">di͡e-n</ta>
            <ta e="T461" id="Seg_4644" s="T460">ol</ta>
            <ta e="T462" id="Seg_4645" s="T461">ih-i͡ek-tere</ta>
            <ta e="T463" id="Seg_4646" s="T462">di͡e-n</ta>
            <ta e="T464" id="Seg_4647" s="T463">ol</ta>
            <ta e="T465" id="Seg_4648" s="T464">butugas</ta>
            <ta e="T466" id="Seg_4649" s="T465">kerd-er-bit</ta>
            <ta e="T467" id="Seg_4650" s="T466">bu͡o</ta>
            <ta e="T469" id="Seg_4651" s="T468">innʼen</ta>
            <ta e="T470" id="Seg_4652" s="T469">kanta-s</ta>
            <ta e="T471" id="Seg_4653" s="T470">gɨn-nag-ɨna</ta>
            <ta e="T472" id="Seg_4654" s="T471">ol</ta>
            <ta e="T473" id="Seg_4655" s="T472">tojon-noru-n</ta>
            <ta e="T474" id="Seg_4656" s="T473">kabɨrga-tɨ-n</ta>
            <ta e="T475" id="Seg_4657" s="T474">kaja</ta>
            <ta e="T476" id="Seg_4658" s="T475">oks-oːr-u-ŋ</ta>
            <ta e="T477" id="Seg_4659" s="T476">di͡e-n</ta>
            <ta e="T478" id="Seg_4660" s="T477">bɨtɨg-ɨ</ta>
            <ta e="T479" id="Seg_4661" s="T478">eː</ta>
            <ta e="T480" id="Seg_4662" s="T479">kim-i-nen</ta>
            <ta e="T481" id="Seg_4663" s="T480">batɨja-nan</ta>
            <ta e="T483" id="Seg_4664" s="T482">oččogo</ta>
            <ta e="T484" id="Seg_4665" s="T483">ere</ta>
            <ta e="T485" id="Seg_4666" s="T484">kɨ͡aj-ɨ͡ak-pɨt</ta>
            <ta e="T486" id="Seg_4667" s="T485">di͡e-bit</ta>
            <ta e="T487" id="Seg_4668" s="T486">bu͡o</ta>
            <ta e="T488" id="Seg_4669" s="T487">onton</ta>
            <ta e="T489" id="Seg_4670" s="T488">haŋar-d-a</ta>
            <ta e="T490" id="Seg_4671" s="T489">kɨ͡aj-ɨ͡ak-pɨt</ta>
            <ta e="T491" id="Seg_4672" s="T490">kajdak</ta>
            <ta e="T492" id="Seg_4673" s="T491">eme</ta>
            <ta e="T494" id="Seg_4674" s="T493">de</ta>
            <ta e="T495" id="Seg_4675" s="T494">ol</ta>
            <ta e="T496" id="Seg_4676" s="T495">kel-bit-ter</ta>
            <ta e="T497" id="Seg_4677" s="T496">ol</ta>
            <ta e="T498" id="Seg_4678" s="T497">butugas-taː-bɨt-tar</ta>
            <ta e="T499" id="Seg_4679" s="T498">dʼe</ta>
            <ta e="T500" id="Seg_4680" s="T499">ih-el-ler</ta>
            <ta e="T501" id="Seg_4681" s="T500">butugas-taː-ŋ</ta>
            <ta e="T502" id="Seg_4682" s="T501">di͡e-bit</ta>
            <ta e="T503" id="Seg_4683" s="T502">bu͡o</ta>
            <ta e="T504" id="Seg_4684" s="T503">butugas-taː-bɨt-tar</ta>
            <ta e="T506" id="Seg_4685" s="T505">dʼe</ta>
            <ta e="T507" id="Seg_4686" s="T506">ol</ta>
            <ta e="T508" id="Seg_4687" s="T507">kɨːnnʼ-a-r-a</ta>
            <ta e="T509" id="Seg_4688" s="T508">hɨt-tak-tarɨnan</ta>
            <ta e="T510" id="Seg_4689" s="T509">de</ta>
            <ta e="T511" id="Seg_4690" s="T510">kel-bit-ter</ta>
            <ta e="T512" id="Seg_4691" s="T511">bu͡o</ta>
            <ta e="T513" id="Seg_4692" s="T512">ol</ta>
            <ta e="T514" id="Seg_4693" s="T513">ečikeje-ler-i-ŋ</ta>
            <ta e="T516" id="Seg_4694" s="T515">kel-en-ner</ta>
            <ta e="T517" id="Seg_4695" s="T516">dʼaktat-tar-ɨ</ta>
            <ta e="T518" id="Seg_4696" s="T517">kiːl-let-teː-n</ta>
            <ta e="T519" id="Seg_4697" s="T518">de</ta>
            <ta e="T520" id="Seg_4698" s="T519">bu͡o</ta>
            <ta e="T521" id="Seg_4699" s="T520">ogonnʼor</ta>
            <ta e="T522" id="Seg_4700" s="T521">tohuj-ar</ta>
            <ta e="T523" id="Seg_4701" s="T522">kiːl-let-tiː-r</ta>
            <ta e="T525" id="Seg_4702" s="T524">kaja</ta>
            <ta e="T526" id="Seg_4703" s="T525">onnuk-tar</ta>
            <ta e="T527" id="Seg_4704" s="T526">mannɨk-tar</ta>
            <ta e="T528" id="Seg_4705" s="T527">ös-tö</ta>
            <ta e="T529" id="Seg_4706" s="T528">kepseː-ŋ</ta>
            <ta e="T530" id="Seg_4707" s="T529">di͡e-n</ta>
            <ta e="T532" id="Seg_4708" s="T531">de</ta>
            <ta e="T533" id="Seg_4709" s="T532">ol</ta>
            <ta e="T534" id="Seg_4710" s="T533">ogonnʼor-go</ta>
            <ta e="T535" id="Seg_4711" s="T534">tiːj-en</ta>
            <ta e="T536" id="Seg_4712" s="T535">kepsiː-l-ler</ta>
            <ta e="T537" id="Seg_4713" s="T536">bu͡o</ta>
            <ta e="T538" id="Seg_4714" s="T537">emeːksin-i</ta>
            <ta e="T539" id="Seg_4715" s="T538">egel-li-bit</ta>
            <ta e="T540" id="Seg_4716" s="T539">bihigi</ta>
            <ta e="T541" id="Seg_4717" s="T540">iti</ta>
            <ta e="T542" id="Seg_4718" s="T541">hir-dʼip-pit</ta>
            <ta e="T543" id="Seg_4719" s="T542">d-iː-bit</ta>
            <ta e="T545" id="Seg_4720" s="T544">ojuːn</ta>
            <ta e="T546" id="Seg_4721" s="T545">emeːksin</ta>
            <ta e="T547" id="Seg_4722" s="T546">iti</ta>
            <ta e="T548" id="Seg_4723" s="T547">povar-dar-bɨt</ta>
            <ta e="T550" id="Seg_4724" s="T549">oččogo</ta>
            <ta e="T551" id="Seg_4725" s="T550">ol</ta>
            <ta e="T552" id="Seg_4726" s="T551">ojuːn</ta>
            <ta e="T553" id="Seg_4727" s="T552">emeːksin</ta>
            <ta e="T554" id="Seg_4728" s="T553">hir-deː-n</ta>
            <ta e="T555" id="Seg_4729" s="T554">egel-l-e</ta>
            <ta e="T556" id="Seg_4730" s="T555">bihi͡e-ke</ta>
            <ta e="T557" id="Seg_4731" s="T556">ehi͡e-ke</ta>
            <ta e="T558" id="Seg_4732" s="T557">di͡e-n</ta>
            <ta e="T559" id="Seg_4733" s="T558">ol</ta>
            <ta e="T560" id="Seg_4734" s="T559">kepsiː-l-ler</ta>
            <ta e="T561" id="Seg_4735" s="T560">bu͡o</ta>
            <ta e="T562" id="Seg_4736" s="T561">kihi-ni</ta>
            <ta e="T563" id="Seg_4737" s="T562">bar-a-p-pɨt-tar</ta>
            <ta e="T565" id="Seg_4738" s="T564">kaːn-narɨ-n</ta>
            <ta e="T566" id="Seg_4739" s="T565">bu͡olla</ta>
            <ta e="T567" id="Seg_4740" s="T566">bahak-tarɨ-n</ta>
            <ta e="T568" id="Seg_4741" s="T567">munna</ta>
            <ta e="T569" id="Seg_4742" s="T568">hot-ol-lor</ta>
            <ta e="T570" id="Seg_4743" s="T569">e-bit</ta>
            <ta e="T571" id="Seg_4744" s="T570">kihi-ler-i</ta>
            <ta e="T572" id="Seg_4745" s="T571">kerd-e</ta>
            <ta e="T573" id="Seg_4746" s="T572">kerd-e</ta>
            <ta e="T574" id="Seg_4747" s="T573">h-iː</ta>
            <ta e="T575" id="Seg_4748" s="T574">h-iː</ta>
            <ta e="T576" id="Seg_4749" s="T575">hiː-l-ler</ta>
            <ta e="T578" id="Seg_4750" s="T577">oː</ta>
            <ta e="T579" id="Seg_4751" s="T578">tu͡ok</ta>
            <ta e="T580" id="Seg_4752" s="T579">aːt-taːk</ta>
            <ta e="T581" id="Seg_4753" s="T580">kaːn-ɨ-gar</ta>
            <ta e="T582" id="Seg_4754" s="T581">bih-i-ll-i-bit</ta>
            <ta e="T583" id="Seg_4755" s="T582">dʼon-nor-gut=uj</ta>
            <ta e="T584" id="Seg_4756" s="T583">dʼe</ta>
            <ta e="T585" id="Seg_4757" s="T584">ahaː-ŋ</ta>
            <ta e="T586" id="Seg_4758" s="T585">di͡e-n</ta>
            <ta e="T587" id="Seg_4759" s="T586">ogonnʼor</ta>
            <ta e="T588" id="Seg_4760" s="T587">dʼe</ta>
            <ta e="T589" id="Seg_4761" s="T588">ah-a-t-ar</ta>
            <ta e="T591" id="Seg_4762" s="T590">de</ta>
            <ta e="T592" id="Seg_4763" s="T591">ol</ta>
            <ta e="T593" id="Seg_4764" s="T592">ahaː</ta>
            <ta e="T594" id="Seg_4765" s="T593">ah-a-p-pɨt-ɨ-n</ta>
            <ta e="T595" id="Seg_4766" s="T594">kenne</ta>
            <ta e="T596" id="Seg_4767" s="T595">de</ta>
            <ta e="T597" id="Seg_4768" s="T596">ol</ta>
            <ta e="T598" id="Seg_4769" s="T597">oː</ta>
            <ta e="T599" id="Seg_4770" s="T598">kallaːm-mɨt</ta>
            <ta e="T600" id="Seg_4771" s="T599">kajdak</ta>
            <ta e="T601" id="Seg_4772" s="T600">kajdak</ta>
            <ta e="T602" id="Seg_4773" s="T601">bu͡ol-ar</ta>
            <ta e="T603" id="Seg_4774" s="T602">de</ta>
            <ta e="T604" id="Seg_4775" s="T603">bɨlɨp-pɨt</ta>
            <ta e="T605" id="Seg_4776" s="T604">kojd-or</ta>
            <ta e="T607" id="Seg_4777" s="T606">tu͡ok</ta>
            <ta e="T608" id="Seg_4778" s="T607">bu͡ol-ar</ta>
            <ta e="T609" id="Seg_4779" s="T608">di͡e-bit-e</ta>
            <ta e="T611" id="Seg_4780" s="T610">dʼe</ta>
            <ta e="T612" id="Seg_4781" s="T611">ol</ta>
            <ta e="T613" id="Seg_4782" s="T612">ikki</ta>
            <ta e="T614" id="Seg_4783" s="T613">kamnas-taːg-ɨ</ta>
            <ta e="T615" id="Seg_4784" s="T614">ikki</ta>
            <ta e="T616" id="Seg_4785" s="T615">ött-ü-ge</ta>
            <ta e="T617" id="Seg_4786" s="T616">olor-put</ta>
            <ta e="T618" id="Seg_4787" s="T617">bu͡o</ta>
            <ta e="T620" id="Seg_4788" s="T619">dʼe</ta>
            <ta e="T621" id="Seg_4789" s="T620">ol</ta>
            <ta e="T622" id="Seg_4790" s="T621">kanta-s</ta>
            <ta e="T623" id="Seg_4791" s="T622">gɨm-mɨt-ɨ-gar</ta>
            <ta e="T624" id="Seg_4792" s="T623">kabɨrga-tɨ-n</ta>
            <ta e="T625" id="Seg_4793" s="T624">kaj-a</ta>
            <ta e="T626" id="Seg_4794" s="T625">oks-u-but-tara</ta>
            <ta e="T627" id="Seg_4795" s="T626">batɨja-nan</ta>
            <ta e="T629" id="Seg_4796" s="T628">anɨ</ta>
            <ta e="T630" id="Seg_4797" s="T629">iti</ta>
            <ta e="T631" id="Seg_4798" s="T630">ölörs-öl-lörü-ger</ta>
            <ta e="T632" id="Seg_4799" s="T631">diː-r</ta>
            <ta e="T633" id="Seg_4800" s="T632">kinoː-lar-ga</ta>
            <ta e="T634" id="Seg_4801" s="T633">di͡eri</ta>
            <ta e="T636" id="Seg_4802" s="T635">ol</ta>
            <ta e="T637" id="Seg_4803" s="T636">kɨ͡aj-bɨt-tar</ta>
            <ta e="T638" id="Seg_4804" s="T637">ühü</ta>
            <ta e="T639" id="Seg_4805" s="T638">on-tu-larɨ-n</ta>
            <ta e="T641" id="Seg_4806" s="T640">dʼe</ta>
            <ta e="T642" id="Seg_4807" s="T641">aŋar-darɨ-n</ta>
            <ta e="T643" id="Seg_4808" s="T642">ölör-töː-büt-tere</ta>
            <ta e="T645" id="Seg_4809" s="T644">hette</ta>
            <ta e="T646" id="Seg_4810" s="T645">kihi-te</ta>
            <ta e="T647" id="Seg_4811" s="T646">e-bit</ta>
            <ta e="T648" id="Seg_4812" s="T647">du͡o</ta>
            <ta e="T649" id="Seg_4813" s="T648">kas</ta>
            <ta e="T650" id="Seg_4814" s="T649">du͡o</ta>
            <ta e="T652" id="Seg_4815" s="T651">dʼe</ta>
            <ta e="T653" id="Seg_4816" s="T652">onton</ta>
            <ta e="T654" id="Seg_4817" s="T653">dʼaktat-tar</ta>
            <ta e="T655" id="Seg_4818" s="T654">kaːl-bɨt-tarɨ-gar</ta>
            <ta e="T656" id="Seg_4819" s="T655">bu</ta>
            <ta e="T657" id="Seg_4820" s="T656">dʼaktat-tar</ta>
            <ta e="T658" id="Seg_4821" s="T657">dʼe</ta>
            <ta e="T659" id="Seg_4822" s="T658">etih-el-ler</ta>
            <ta e="T660" id="Seg_4823" s="T659">ühü</ta>
            <ta e="T661" id="Seg_4824" s="T660">bahak-tarɨ-n</ta>
            <ta e="T662" id="Seg_4825" s="T661">bɨldʼ-ɨ-h-an-nar</ta>
            <ta e="T664" id="Seg_4826" s="T663">min</ta>
            <ta e="T665" id="Seg_4827" s="T664">er-i-m</ta>
            <ta e="T666" id="Seg_4828" s="T665">bahag-a</ta>
            <ta e="T667" id="Seg_4829" s="T666">min</ta>
            <ta e="T668" id="Seg_4830" s="T667">er-i-m</ta>
            <ta e="T669" id="Seg_4831" s="T668">bahag-a</ta>
            <ta e="T670" id="Seg_4832" s="T669">d-e-h-eːktiː-l-ler</ta>
            <ta e="T671" id="Seg_4833" s="T670">ühü</ta>
            <ta e="T672" id="Seg_4834" s="T671">kotoku-lar</ta>
            <ta e="T674" id="Seg_4835" s="T673">tu͡ok</ta>
            <ta e="T675" id="Seg_4836" s="T674">suːka</ta>
            <ta e="T676" id="Seg_4837" s="T675">kɨrgɨt-tara=j</ta>
            <ta e="T677" id="Seg_4838" s="T676">iti-ler-i</ta>
            <ta e="T678" id="Seg_4839" s="T677">öl-öttöː-n</ta>
            <ta e="T679" id="Seg_4840" s="T678">keːh-i-ŋ</ta>
            <ta e="T680" id="Seg_4841" s="T679">di͡e-bit</ta>
            <ta e="T681" id="Seg_4842" s="T680">bu͡o</ta>
            <ta e="T682" id="Seg_4843" s="T681">tu͡ok</ta>
            <ta e="T683" id="Seg_4844" s="T682">sustogoj</ta>
            <ta e="T684" id="Seg_4845" s="T683">ogonnʼor-o</ta>
            <ta e="T685" id="Seg_4846" s="T684">bu͡olla</ta>
            <ta e="T686" id="Seg_4847" s="T685">da</ta>
            <ta e="T687" id="Seg_4848" s="T686">bihi͡ene</ta>
            <ta e="T688" id="Seg_4849" s="T687">Pu͡orotaj</ta>
            <ta e="T690" id="Seg_4850" s="T689">öl-öttöː-n</ta>
            <ta e="T691" id="Seg_4851" s="T690">keːs-pit-ter</ta>
            <ta e="T692" id="Seg_4852" s="T691">muŋ-naːk-tar</ta>
            <ta e="T694" id="Seg_4853" s="T693">onton</ta>
            <ta e="T695" id="Seg_4854" s="T694">ol</ta>
            <ta e="T696" id="Seg_4855" s="T695">emeːksin-i</ta>
            <ta e="T697" id="Seg_4856" s="T696">dʼe</ta>
            <ta e="T698" id="Seg_4857" s="T697">ölör-ö</ta>
            <ta e="T699" id="Seg_4858" s="T698">hatɨː-l-lar</ta>
            <ta e="T700" id="Seg_4859" s="T699">ojuːn</ta>
            <ta e="T701" id="Seg_4860" s="T700">emeːksin-i</ta>
            <ta e="T703" id="Seg_4861" s="T702">uː-ga</ta>
            <ta e="T704" id="Seg_4862" s="T703">da</ta>
            <ta e="T705" id="Seg_4863" s="T704">timir-d-el-ler</ta>
            <ta e="T706" id="Seg_4864" s="T705">dʼe</ta>
            <ta e="T707" id="Seg_4865" s="T706">hakor-daː-n</ta>
            <ta e="T708" id="Seg_4866" s="T707">baran</ta>
            <ta e="T710" id="Seg_4867" s="T709">törüt</ta>
            <ta e="T711" id="Seg_4868" s="T710">timir-bet</ta>
            <ta e="T713" id="Seg_4869" s="T712">ajagal-ɨː</ta>
            <ta e="T714" id="Seg_4870" s="T713">hataː-n</ta>
            <ta e="T715" id="Seg_4871" s="T714">u͡ot-u-n</ta>
            <ta e="T716" id="Seg_4872" s="T715">ott-on</ta>
            <ta e="T717" id="Seg_4873" s="T716">bu͡o</ta>
            <ta e="T718" id="Seg_4874" s="T717">u͡ot</ta>
            <ta e="T719" id="Seg_4875" s="T718">ih-i-ger</ta>
            <ta e="T720" id="Seg_4876" s="T719">bɨrag-al-lar</ta>
            <ta e="T722" id="Seg_4877" s="T721">u͡ot-tara</ta>
            <ta e="T723" id="Seg_4878" s="T722">karaːr-čɨ</ta>
            <ta e="T724" id="Seg_4879" s="T723">utuj-ar</ta>
            <ta e="T725" id="Seg_4880" s="T724">ühü</ta>
            <ta e="T727" id="Seg_4881" s="T726">onno</ta>
            <ta e="T728" id="Seg_4882" s="T727">da</ta>
            <ta e="T729" id="Seg_4883" s="T728">di͡e-bit</ta>
            <ta e="T730" id="Seg_4884" s="T729">bu͡o</ta>
            <ta e="T731" id="Seg_4885" s="T730">tukuː-lar</ta>
            <ta e="T732" id="Seg_4886" s="T731">min</ta>
            <ta e="T733" id="Seg_4887" s="T732">kün</ta>
            <ta e="T734" id="Seg_4888" s="T733">emeːksin</ta>
            <ta e="T735" id="Seg_4889" s="T734">e-ti-m</ta>
            <ta e="T736" id="Seg_4890" s="T735">kaja</ta>
            <ta e="T737" id="Seg_4891" s="T736">abaːhɨ</ta>
            <ta e="T738" id="Seg_4892" s="T737">bu͡ol-tap-pɨn</ta>
            <ta e="T740" id="Seg_4893" s="T739">dʼom-mu-n</ta>
            <ta e="T741" id="Seg_4894" s="T740">baraː-bɨt-tara</ta>
            <ta e="T742" id="Seg_4895" s="T741">bu</ta>
            <ta e="T743" id="Seg_4896" s="T742">bu͡olla</ta>
            <ta e="T744" id="Seg_4897" s="T743">kihi-ni</ta>
            <ta e="T745" id="Seg_4898" s="T744">sü͡öhü-nü</ta>
            <ta e="T746" id="Seg_4899" s="T745">kör-öːrü-bün</ta>
            <ta e="T747" id="Seg_4900" s="T746">hir-deː-n</ta>
            <ta e="T748" id="Seg_4901" s="T747">kel-bit-i-m</ta>
            <ta e="T750" id="Seg_4902" s="T749">minigi-n</ta>
            <ta e="T751" id="Seg_4903" s="T750">erej-deː-me-ŋ</ta>
            <ta e="T752" id="Seg_4904" s="T751">min</ta>
            <ta e="T753" id="Seg_4905" s="T752">itinnik</ta>
            <ta e="T754" id="Seg_4906" s="T753">ke</ta>
            <ta e="T755" id="Seg_4907" s="T754">öl-ü͡ö-m</ta>
            <ta e="T756" id="Seg_4908" s="T755">hu͡og-a</ta>
            <ta e="T757" id="Seg_4909" s="T756">diː-r</ta>
            <ta e="T759" id="Seg_4910" s="T758">kɨːh</ta>
            <ta e="T760" id="Seg_4911" s="T759">ogo-to</ta>
            <ta e="T761" id="Seg_4912" s="T760">haŋardɨː</ta>
            <ta e="T762" id="Seg_4913" s="T761">bɨrtak</ta>
            <ta e="T763" id="Seg_4914" s="T762">bu͡ol-but</ta>
            <ta e="T764" id="Seg_4915" s="T763">kɨːh</ta>
            <ta e="T765" id="Seg_4916" s="T764">ogo-to</ta>
            <ta e="T766" id="Seg_4917" s="T765">moːj-bu-nan</ta>
            <ta e="T767" id="Seg_4918" s="T766">atɨll-a-tɨn</ta>
            <ta e="T768" id="Seg_4919" s="T767">oččogo</ta>
            <ta e="T769" id="Seg_4920" s="T768">öl-ü͡ö-m</ta>
            <ta e="T770" id="Seg_4921" s="T769">diː-r</ta>
            <ta e="T771" id="Seg_4922" s="T770">ol</ta>
            <ta e="T772" id="Seg_4923" s="T771">moːj-bu-nan</ta>
            <ta e="T773" id="Seg_4924" s="T772">atɨll-a-p-pɨt-tar</ta>
            <ta e="T775" id="Seg_4925" s="T774">de</ta>
            <ta e="T776" id="Seg_4926" s="T775">ol</ta>
            <ta e="T777" id="Seg_4927" s="T776">emeːksin-nere</ta>
            <ta e="T778" id="Seg_4928" s="T777">öl-ön</ta>
            <ta e="T779" id="Seg_4929" s="T778">kaːl-bɨt</ta>
            <ta e="T780" id="Seg_4930" s="T779">dʼe</ta>
            <ta e="T781" id="Seg_4931" s="T780">ele-te</ta>
            <ta e="T782" id="Seg_4932" s="T781">ol</ta>
            <ta e="T783" id="Seg_4933" s="T782">kepseː-bit-tere</ta>
            <ta e="T784" id="Seg_4934" s="T783">öjdöː-böp-pün</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_4935" s="T1">onton</ta>
            <ta e="T3" id="Seg_4936" s="T2">biːrde</ta>
            <ta e="T4" id="Seg_4937" s="T3">bultan-A</ta>
            <ta e="T5" id="Seg_4938" s="T4">tagɨs-I-BIT-LArA</ta>
            <ta e="T6" id="Seg_4939" s="T5">ogonnʼor-LAr</ta>
            <ta e="T8" id="Seg_4940" s="T7">ogonnʼor-LAr</ta>
            <ta e="T9" id="Seg_4941" s="T8">kel-Ar-LAr</ta>
            <ta e="T10" id="Seg_4942" s="T9">u͡ol-LAr</ta>
            <ta e="T12" id="Seg_4943" s="T11">kaja</ta>
            <ta e="T13" id="Seg_4944" s="T12">Hɨndaːska-BItI-GAr</ta>
            <ta e="T14" id="Seg_4945" s="T13">dʼi͡e</ta>
            <ta e="T15" id="Seg_4946" s="T14">tur-BIT</ta>
            <ta e="T16" id="Seg_4947" s="T15">di͡e-BIT-LAr</ta>
            <ta e="T18" id="Seg_4948" s="T17">kajdi͡ek</ta>
            <ta e="T19" id="Seg_4949" s="T18">bultan-AːččI-BIt</ta>
            <ta e="T21" id="Seg_4950" s="T20">oː</ta>
            <ta e="T22" id="Seg_4951" s="T21">dʼe</ta>
            <ta e="T23" id="Seg_4952" s="T22">bu</ta>
            <ta e="T24" id="Seg_4953" s="T23">dʼi͡e</ta>
            <ta e="T25" id="Seg_4954" s="T24">tur-BIT-tI-GAr</ta>
            <ta e="T26" id="Seg_4955" s="T25">kuttan-Ar-LAr</ta>
            <ta e="T27" id="Seg_4956" s="T26">kel-IAK-LArI-n</ta>
            <ta e="T28" id="Seg_4957" s="T27">ühü</ta>
            <ta e="T29" id="Seg_4958" s="T28">nuːčča-LAr</ta>
            <ta e="T30" id="Seg_4959" s="T29">baːr-LAr</ta>
            <ta e="T31" id="Seg_4960" s="T30">di͡e-An</ta>
            <ta e="T33" id="Seg_4961" s="T32">oː</ta>
            <ta e="T34" id="Seg_4962" s="T33">dʼe</ta>
            <ta e="T35" id="Seg_4963" s="T34">bu</ta>
            <ta e="T36" id="Seg_4964" s="T35">hir-BItI-n</ta>
            <ta e="T37" id="Seg_4965" s="T36">bult-tA</ta>
            <ta e="T38" id="Seg_4966" s="T37">hu͡ok</ta>
            <ta e="T39" id="Seg_4967" s="T38">oŋor-TI-LAr</ta>
            <ta e="T40" id="Seg_4968" s="T39">kajdak</ta>
            <ta e="T41" id="Seg_4969" s="T40">bu͡ol-AːččI-BIt=Ij</ta>
            <ta e="T42" id="Seg_4970" s="T41">di͡e-Ar-LAr</ta>
            <ta e="T43" id="Seg_4971" s="T42">ühü</ta>
            <ta e="T44" id="Seg_4972" s="T43">ol</ta>
            <ta e="T45" id="Seg_4973" s="T44">ogonnʼor-LAr</ta>
            <ta e="T46" id="Seg_4974" s="T45">ka</ta>
            <ta e="T48" id="Seg_4975" s="T47">di͡e-A-s-A-BIt</ta>
            <ta e="T49" id="Seg_4976" s="T48">di͡e-Ar-LAr</ta>
            <ta e="T51" id="Seg_4977" s="T50">kepset-Ar-LArI-n</ta>
            <ta e="T52" id="Seg_4978" s="T51">ihit-A-BIn</ta>
            <ta e="T53" id="Seg_4979" s="T52">ehe-LAr-I-m</ta>
            <ta e="T54" id="Seg_4980" s="T53">ka</ta>
            <ta e="T56" id="Seg_4981" s="T55">onton</ta>
            <ta e="T57" id="Seg_4982" s="T56">dʼe</ta>
            <ta e="T58" id="Seg_4983" s="T57">onton</ta>
            <ta e="T59" id="Seg_4984" s="T58">kel-Ar-LAr</ta>
            <ta e="T61" id="Seg_4985" s="T60">kel-An-LAr</ta>
            <ta e="T62" id="Seg_4986" s="T61">kör-BIT-LArA</ta>
            <ta e="T63" id="Seg_4987" s="T62">ikki</ta>
            <ta e="T64" id="Seg_4988" s="T63">kihi</ta>
            <ta e="T65" id="Seg_4989" s="T64">hɨrɨt-Ar-LAr</ta>
            <ta e="T66" id="Seg_4990" s="T65">ühü</ta>
            <ta e="T68" id="Seg_4991" s="T67">dʼaktar-tI-n</ta>
            <ta e="T69" id="Seg_4992" s="T68">aːt-tA</ta>
            <ta e="T70" id="Seg_4993" s="T69">Darʼja</ta>
            <ta e="T72" id="Seg_4994" s="T71">er-tI-n</ta>
            <ta e="T73" id="Seg_4995" s="T72">aːt-tA</ta>
            <ta e="T74" id="Seg_4996" s="T73">kim</ta>
            <ta e="T75" id="Seg_4997" s="T74">di͡e-BIT-LAr=Ij</ta>
            <ta e="T76" id="Seg_4998" s="T75">ka</ta>
            <ta e="T77" id="Seg_4999" s="T76">da</ta>
            <ta e="T78" id="Seg_5000" s="T77">oluja</ta>
            <ta e="T79" id="Seg_5001" s="T78">bagajɨ</ta>
            <ta e="T80" id="Seg_5002" s="T79">aːt-LAːK</ta>
            <ta e="T81" id="Seg_5003" s="T80">kihi</ta>
            <ta e="T82" id="Seg_5004" s="T81">kim</ta>
            <ta e="T83" id="Seg_5005" s="T82">ere</ta>
            <ta e="T84" id="Seg_5006" s="T83">di͡e-AːččI-LAr</ta>
            <ta e="T85" id="Seg_5007" s="T84">eːt</ta>
            <ta e="T87" id="Seg_5008" s="T86">törüt</ta>
            <ta e="T88" id="Seg_5009" s="T87">öjdöː-BAT-BIn</ta>
            <ta e="T89" id="Seg_5010" s="T88">aːt-tI-n</ta>
            <ta e="T91" id="Seg_5011" s="T90">ol-GA</ta>
            <ta e="T92" id="Seg_5012" s="T91">bu͡o</ta>
            <ta e="T93" id="Seg_5013" s="T92">čugahaː-Ar-LAr</ta>
            <ta e="T94" id="Seg_5014" s="T93">ühü</ta>
            <ta e="T95" id="Seg_5015" s="T94">kuttan-A</ta>
            <ta e="T96" id="Seg_5016" s="T95">kuttan-A</ta>
            <ta e="T97" id="Seg_5017" s="T96">čugahaː-Ar-LAr</ta>
            <ta e="T98" id="Seg_5018" s="T97">ühü</ta>
            <ta e="T100" id="Seg_5019" s="T99">bu</ta>
            <ta e="T101" id="Seg_5020" s="T100">togo</ta>
            <ta e="T102" id="Seg_5021" s="T101">kel-BIT-GIt=Ij</ta>
            <ta e="T103" id="Seg_5022" s="T102">bihigi</ta>
            <ta e="T104" id="Seg_5023" s="T103">hir-BItI-GAr</ta>
            <ta e="T106" id="Seg_5024" s="T105">togo</ta>
            <ta e="T107" id="Seg_5025" s="T106">bul.[t]-TI-GIt</ta>
            <ta e="T109" id="Seg_5026" s="T108">bultan-Ar</ta>
            <ta e="T110" id="Seg_5027" s="T109">hir-BItI-n</ta>
            <ta e="T111" id="Seg_5028" s="T110">togo</ta>
            <ta e="T112" id="Seg_5029" s="T111">aldʼat-A-GIt</ta>
            <ta e="T113" id="Seg_5030" s="T112">di͡e-Ar-LAr</ta>
            <ta e="T114" id="Seg_5031" s="T113">ühü</ta>
            <ta e="T115" id="Seg_5032" s="T114">ogonnʼor-LAr</ta>
            <ta e="T117" id="Seg_5033" s="T116">kaja</ta>
            <ta e="T118" id="Seg_5034" s="T117">manna</ta>
            <ta e="T119" id="Seg_5035" s="T118">bu͡ollagɨna</ta>
            <ta e="T120" id="Seg_5036" s="T119">naselenije</ta>
            <ta e="T121" id="Seg_5037" s="T120">oŋor-IAK-LArA</ta>
            <ta e="T122" id="Seg_5038" s="T121">kihi</ta>
            <ta e="T123" id="Seg_5039" s="T122">tur-IAK-tA</ta>
            <ta e="T125" id="Seg_5040" s="T124">kihi</ta>
            <ta e="T126" id="Seg_5041" s="T125">ü͡öskeː-IAK-tA</ta>
            <ta e="T127" id="Seg_5042" s="T126">manna</ta>
            <ta e="T128" id="Seg_5043" s="T127">elbek</ta>
            <ta e="T129" id="Seg_5044" s="T128">kihi</ta>
            <ta e="T130" id="Seg_5045" s="T129">olor-IAK-tA</ta>
            <ta e="T132" id="Seg_5046" s="T131">oččogo</ta>
            <ta e="T133" id="Seg_5047" s="T132">peričal-BIt</ta>
            <ta e="T134" id="Seg_5048" s="T133">bu͡o</ta>
            <ta e="T135" id="Seg_5049" s="T134">iti</ta>
            <ta e="T136" id="Seg_5050" s="T135">baːr</ta>
            <ta e="T137" id="Seg_5051" s="T136">hurbuj-An</ta>
            <ta e="T138" id="Seg_5052" s="T137">kiːr-BIT</ta>
            <ta e="T140" id="Seg_5053" s="T139">togo</ta>
            <ta e="T141" id="Seg_5054" s="T140">ɨraːk-tA=Ij</ta>
            <ta e="T142" id="Seg_5055" s="T141">di͡e-Ar-LAr</ta>
            <ta e="T143" id="Seg_5056" s="T142">ühü</ta>
            <ta e="T144" id="Seg_5057" s="T143">peričal-BIt</ta>
            <ta e="T146" id="Seg_5058" s="T145">ü͡ös-GIt</ta>
            <ta e="T147" id="Seg_5059" s="T146">küččügüj</ta>
            <ta e="T148" id="Seg_5060" s="T147">manna</ta>
            <ta e="T149" id="Seg_5061" s="T148">ol</ta>
            <ta e="T150" id="Seg_5062" s="T149">ihin</ta>
            <ta e="T151" id="Seg_5063" s="T150">itinne</ta>
            <ta e="T152" id="Seg_5064" s="T151">bu͡ol-Ar</ta>
            <ta e="T153" id="Seg_5065" s="T152">ü͡ös-GIt</ta>
            <ta e="T155" id="Seg_5066" s="T154">itinne</ta>
            <ta e="T156" id="Seg_5067" s="T155">poroku͡ot-LAr</ta>
            <ta e="T157" id="Seg_5068" s="T156">kel-IAK-LArA</ta>
            <ta e="T159" id="Seg_5069" s="T158">oduːrgaː-Ar-LAr</ta>
            <ta e="T160" id="Seg_5070" s="T159">ühü</ta>
            <ta e="T161" id="Seg_5071" s="T160">tu͡ok</ta>
            <ta e="T162" id="Seg_5072" s="T161">aːt-tA=Ij</ta>
            <ta e="T163" id="Seg_5073" s="T162">poroku͡ot</ta>
            <ta e="T164" id="Seg_5074" s="T163">di͡e-Ar</ta>
            <ta e="T167" id="Seg_5075" s="T166">tart-Ar</ta>
            <ta e="T168" id="Seg_5076" s="T167">kel-Ar</ta>
            <ta e="T169" id="Seg_5077" s="T168">poroku͡ot</ta>
            <ta e="T171" id="Seg_5078" s="T170">dʼe</ta>
            <ta e="T172" id="Seg_5079" s="T171">ol</ta>
            <ta e="T173" id="Seg_5080" s="T172">kojut</ta>
            <ta e="T174" id="Seg_5081" s="T173">kojut</ta>
            <ta e="T175" id="Seg_5082" s="T174">kojut</ta>
            <ta e="T176" id="Seg_5083" s="T175">kojut</ta>
            <ta e="T177" id="Seg_5084" s="T176">ol</ta>
            <ta e="T178" id="Seg_5085" s="T177">ü͡öskeː-An</ta>
            <ta e="T179" id="Seg_5086" s="T178">ü͡öskeː-An</ta>
            <ta e="T180" id="Seg_5087" s="T179">bu</ta>
            <ta e="T181" id="Seg_5088" s="T180">Hɨndaːska-ŋ</ta>
            <ta e="T182" id="Seg_5089" s="T181">bu͡ol-An</ta>
            <ta e="T183" id="Seg_5090" s="T182">kaːl-BIT</ta>
            <ta e="T184" id="Seg_5091" s="T183">bu͡o</ta>
            <ta e="T186" id="Seg_5092" s="T185">usku͡ola-tA</ta>
            <ta e="T187" id="Seg_5093" s="T186">hu͡ok-LAr</ta>
            <ta e="T188" id="Seg_5094" s="T187">uraha</ta>
            <ta e="T189" id="Seg_5095" s="T188">dʼi͡e-GA</ta>
            <ta e="T190" id="Seg_5096" s="T189">ü͡öret-Ar-LAr</ta>
            <ta e="T191" id="Seg_5097" s="T190">beje-LArA</ta>
            <ta e="T192" id="Seg_5098" s="T191">tut-An-LAr</ta>
            <ta e="T194" id="Seg_5099" s="T193">balok-LAr-GA</ta>
            <ta e="T196" id="Seg_5100" s="T195">atɨːlaː-Ar-LAr</ta>
            <ta e="T198" id="Seg_5101" s="T197">onton</ta>
            <ta e="T199" id="Seg_5102" s="T198">tut-BIT</ta>
            <ta e="T200" id="Seg_5103" s="T199">dʼi͡e-LArA</ta>
            <ta e="T201" id="Seg_5104" s="T200">baːr</ta>
            <ta e="T202" id="Seg_5105" s="T201">e-TI-tA</ta>
            <ta e="T203" id="Seg_5106" s="T202">oččotoːgu</ta>
            <ta e="T204" id="Seg_5107" s="T203">anɨ</ta>
            <ta e="T205" id="Seg_5108" s="T204">huːl-BIT-tA</ta>
            <ta e="T206" id="Seg_5109" s="T205">bu͡o</ta>
            <ta e="T207" id="Seg_5110" s="T206">ol-tI-ŋ</ta>
            <ta e="T208" id="Seg_5111" s="T207">laːpkɨ-LArA</ta>
            <ta e="T210" id="Seg_5112" s="T209">ol</ta>
            <ta e="T211" id="Seg_5113" s="T210">ihin</ta>
            <ta e="T212" id="Seg_5114" s="T211">Hɨndaːska</ta>
            <ta e="T213" id="Seg_5115" s="T212">Hɨndaːska</ta>
            <ta e="T214" id="Seg_5116" s="T213">Darʼja</ta>
            <ta e="T215" id="Seg_5117" s="T214">tu͡ok-nI</ta>
            <ta e="T216" id="Seg_5118" s="T215">kim-nI</ta>
            <ta e="T217" id="Seg_5119" s="T216">Darʼja</ta>
            <ta e="T218" id="Seg_5120" s="T217">di͡e-Ar-LAr</ta>
            <ta e="T219" id="Seg_5121" s="T218">eːt</ta>
            <ta e="T220" id="Seg_5122" s="T219">iti</ta>
            <ta e="T221" id="Seg_5123" s="T220">ti͡ere</ta>
            <ta e="T222" id="Seg_5124" s="T221">aːttaː-Ar-LAr</ta>
            <ta e="T223" id="Seg_5125" s="T222">ühü</ta>
            <ta e="T225" id="Seg_5126" s="T224">eː</ta>
            <ta e="T226" id="Seg_5127" s="T225">bu</ta>
            <ta e="T227" id="Seg_5128" s="T226">Hɨndaːska-nI</ta>
            <ta e="T229" id="Seg_5129" s="T228">iti</ta>
            <ta e="T230" id="Seg_5130" s="T229">bultan-Ar</ta>
            <ta e="T231" id="Seg_5131" s="T230">hir-LArI-n</ta>
            <ta e="T232" id="Seg_5132" s="T231">bɨldʼaː-BIT-LAr</ta>
            <ta e="T233" id="Seg_5133" s="T232">onton</ta>
            <ta e="T234" id="Seg_5134" s="T233">kojut</ta>
            <ta e="T235" id="Seg_5135" s="T234">kojut</ta>
            <ta e="T236" id="Seg_5136" s="T235">eːt</ta>
            <ta e="T237" id="Seg_5137" s="T236">iti</ta>
            <ta e="T238" id="Seg_5138" s="T237">bu͡ol-BIT-LArA</ta>
            <ta e="T240" id="Seg_5139" s="T239">onton</ta>
            <ta e="T241" id="Seg_5140" s="T240">kojut</ta>
            <ta e="T242" id="Seg_5141" s="T241">iti</ta>
            <ta e="T243" id="Seg_5142" s="T242">bu</ta>
            <ta e="T244" id="Seg_5143" s="T243">kel-A</ta>
            <ta e="T245" id="Seg_5144" s="T244">ilik-InA</ta>
            <ta e="T246" id="Seg_5145" s="T245">Lɨgɨj-LAr</ta>
            <ta e="T247" id="Seg_5146" s="T246">di͡e-An</ta>
            <ta e="T248" id="Seg_5147" s="T247">baːr</ta>
            <ta e="T249" id="Seg_5148" s="T248">e-TI-LArA</ta>
            <ta e="T250" id="Seg_5149" s="T249">ühü</ta>
            <ta e="T252" id="Seg_5150" s="T251">eː</ta>
            <ta e="T253" id="Seg_5151" s="T252">kim-LAr</ta>
            <ta e="T254" id="Seg_5152" s="T253">tigileːk-LAr</ta>
            <ta e="T255" id="Seg_5153" s="T254">di͡e-Ar-LAr</ta>
            <ta e="T256" id="Seg_5154" s="T255">ol</ta>
            <ta e="T258" id="Seg_5155" s="T257">itinne</ta>
            <ta e="T259" id="Seg_5156" s="T258">iti</ta>
            <ta e="T260" id="Seg_5157" s="T259">peričal</ta>
            <ta e="T261" id="Seg_5158" s="T260">ɨnaraː</ta>
            <ta e="T262" id="Seg_5159" s="T261">örüt-tI-GAr</ta>
            <ta e="T263" id="Seg_5160" s="T262">bu͡olla</ta>
            <ta e="T264" id="Seg_5161" s="T263">dʼi͡e</ta>
            <ta e="T265" id="Seg_5162" s="T264">onnu-LAr-tA</ta>
            <ta e="T266" id="Seg_5163" s="T265">baːr-LAr</ta>
            <ta e="T267" id="Seg_5164" s="T266">ikki</ta>
            <ta e="T268" id="Seg_5165" s="T267">üs</ta>
            <ta e="T269" id="Seg_5166" s="T268">dʼi͡e</ta>
            <ta e="T270" id="Seg_5167" s="T269">onnu-tA</ta>
            <ta e="T272" id="Seg_5168" s="T271">onno</ta>
            <ta e="T273" id="Seg_5169" s="T272">olor-Ar-LAr</ta>
            <ta e="T274" id="Seg_5170" s="T273">ol</ta>
            <ta e="T275" id="Seg_5171" s="T274">bulčut-LAr</ta>
            <ta e="T276" id="Seg_5172" s="T275">olor-TAK-TArInA</ta>
            <ta e="T277" id="Seg_5173" s="T276">Lɨgɨj-LAr</ta>
            <ta e="T278" id="Seg_5174" s="T277">kel-BIT-LArA</ta>
            <ta e="T280" id="Seg_5175" s="T279">Lɨgɨj-LAr</ta>
            <ta e="T281" id="Seg_5176" s="T280">tigileːk-LAr</ta>
            <ta e="T283" id="Seg_5177" s="T282">ol</ta>
            <ta e="T284" id="Seg_5178" s="T283">kel-BIT-LArA</ta>
            <ta e="T285" id="Seg_5179" s="T284">üs</ta>
            <ta e="T286" id="Seg_5180" s="T285">dʼaktar-LAːK-LAr</ta>
            <ta e="T287" id="Seg_5181" s="T286">ol-LAr</ta>
            <ta e="T289" id="Seg_5182" s="T288">biːr-LArA</ta>
            <ta e="T290" id="Seg_5183" s="T289">ojun</ta>
            <ta e="T292" id="Seg_5184" s="T291">ikki-LArA</ta>
            <ta e="T293" id="Seg_5185" s="T292">poːvar</ta>
            <ta e="T295" id="Seg_5186" s="T294">dʼe</ta>
            <ta e="T296" id="Seg_5187" s="T295">bu</ta>
            <ta e="T297" id="Seg_5188" s="T296">kihi-nI</ta>
            <ta e="T298" id="Seg_5189" s="T297">ölör-TI-tA</ta>
            <ta e="T299" id="Seg_5190" s="T298">ölör-BIT-I-nAn</ta>
            <ta e="T300" id="Seg_5191" s="T299">kel-BIT-LAr</ta>
            <ta e="T301" id="Seg_5192" s="T300">ol-LAr-nI</ta>
            <ta e="T302" id="Seg_5193" s="T301">poːvar</ta>
            <ta e="T303" id="Seg_5194" s="T302">gɨn-AːrI</ta>
            <ta e="T304" id="Seg_5195" s="T303">egel-BIT-LArA</ta>
            <ta e="T305" id="Seg_5196" s="T304">kuhagan</ta>
            <ta e="T306" id="Seg_5197" s="T305">kihi-LAr</ta>
            <ta e="T308" id="Seg_5198" s="T307">kaːk</ta>
            <ta e="T309" id="Seg_5199" s="T308">budto</ta>
            <ta e="T310" id="Seg_5200" s="T309">tu͡ok-LArA</ta>
            <ta e="T311" id="Seg_5201" s="T310">hülügen-LAr</ta>
            <ta e="T312" id="Seg_5202" s="T311">eni</ta>
            <ta e="T313" id="Seg_5203" s="T312">to</ta>
            <ta e="T315" id="Seg_5204" s="T314">ol</ta>
            <ta e="T316" id="Seg_5205" s="T315">kel-An</ta>
            <ta e="T317" id="Seg_5206" s="T316">kel-An-LAr</ta>
            <ta e="T318" id="Seg_5207" s="T317">manna</ta>
            <ta e="T319" id="Seg_5208" s="T318">kel-BIT-LArA</ta>
            <ta e="T320" id="Seg_5209" s="T319">bulčut-LAr</ta>
            <ta e="T321" id="Seg_5210" s="T320">biːr-LArA</ta>
            <ta e="T322" id="Seg_5211" s="T321">bu͡o</ta>
            <ta e="T323" id="Seg_5212" s="T322">bɨhɨj</ta>
            <ta e="T325" id="Seg_5213" s="T324">biːr-LArA</ta>
            <ta e="T326" id="Seg_5214" s="T325">körügös</ta>
            <ta e="T327" id="Seg_5215" s="T326">biːr-LArA</ta>
            <ta e="T328" id="Seg_5216" s="T327">ɨt-Iː-ČIt</ta>
            <ta e="T329" id="Seg_5217" s="T328">bulčut-LAr</ta>
            <ta e="T330" id="Seg_5218" s="T329">bihi͡ettere</ta>
            <ta e="T331" id="Seg_5219" s="T330">ühü</ta>
            <ta e="T332" id="Seg_5220" s="T331">kepseː-Ar-LAr</ta>
            <ta e="T334" id="Seg_5221" s="T333">ol-LAr</ta>
            <ta e="T335" id="Seg_5222" s="T334">bu͡ollagɨna</ta>
            <ta e="T336" id="Seg_5223" s="T335">kihi-nI</ta>
            <ta e="T337" id="Seg_5224" s="T336">baraː-A-t-An</ta>
            <ta e="T338" id="Seg_5225" s="T337">kel-An</ta>
            <ta e="T339" id="Seg_5226" s="T338">kel-An-LAr</ta>
            <ta e="T340" id="Seg_5227" s="T339">bu</ta>
            <ta e="T341" id="Seg_5228" s="T340">ogonnʼor-LAr</ta>
            <ta e="T342" id="Seg_5229" s="T341">Malɨj</ta>
            <ta e="T343" id="Seg_5230" s="T342">di͡e-An</ta>
            <ta e="T344" id="Seg_5231" s="T343">hir-GA</ta>
            <ta e="T345" id="Seg_5232" s="T344">Porotov</ta>
            <ta e="T346" id="Seg_5233" s="T345">di͡e-An</ta>
            <ta e="T347" id="Seg_5234" s="T346">kihi</ta>
            <ta e="T348" id="Seg_5235" s="T347">baːr</ta>
            <ta e="T349" id="Seg_5236" s="T348">e-TI-tA</ta>
            <ta e="T351" id="Seg_5237" s="T350">ol-GA</ta>
            <ta e="T352" id="Seg_5238" s="T351">bar-BIT-LAr</ta>
            <ta e="T353" id="Seg_5239" s="T352">bu</ta>
            <ta e="T354" id="Seg_5240" s="T353">kihi-LAr</ta>
            <ta e="T356" id="Seg_5241" s="T355">kaja</ta>
            <ta e="T357" id="Seg_5242" s="T356">dogor</ta>
            <ta e="T358" id="Seg_5243" s="T357">onnuk</ta>
            <ta e="T359" id="Seg_5244" s="T358">onnuk</ta>
            <ta e="T360" id="Seg_5245" s="T359">heriː</ta>
            <ta e="T361" id="Seg_5246" s="T360">tumsu-LAːK</ta>
            <ta e="T362" id="Seg_5247" s="T361">ɨt-LAr</ta>
            <ta e="T363" id="Seg_5248" s="T362">kej-Ar-LAr</ta>
            <ta e="T364" id="Seg_5249" s="T363">ölör-IAK-ČIt-LAr</ta>
            <ta e="T365" id="Seg_5250" s="T364">kel-LAr</ta>
            <ta e="T366" id="Seg_5251" s="T365">tigileːk-LAr</ta>
            <ta e="T368" id="Seg_5252" s="T367">dʼe</ta>
            <ta e="T369" id="Seg_5253" s="T368">ol</ta>
            <ta e="T370" id="Seg_5254" s="T369">küreː-Ar-LAr</ta>
            <ta e="T371" id="Seg_5255" s="T370">Malɨj-GA</ta>
            <ta e="T372" id="Seg_5256" s="T371">küreː-BIT-LArA</ta>
            <ta e="T374" id="Seg_5257" s="T373">ol</ta>
            <ta e="T375" id="Seg_5258" s="T374">küreː-An</ta>
            <ta e="T376" id="Seg_5259" s="T375">tij-BIT-LAr</ta>
            <ta e="T377" id="Seg_5260" s="T376">Porotov-GA</ta>
            <ta e="T378" id="Seg_5261" s="T377">tij-An-LAr</ta>
            <ta e="T379" id="Seg_5262" s="T378">diː</ta>
            <ta e="T380" id="Seg_5263" s="T379">kepseː-Ar-LAr</ta>
            <ta e="T381" id="Seg_5264" s="T380">kaja</ta>
            <ta e="T382" id="Seg_5265" s="T381">onnuk-LAr</ta>
            <ta e="T383" id="Seg_5266" s="T382">kel-TI-LAr</ta>
            <ta e="T384" id="Seg_5267" s="T383">kihi-nI</ta>
            <ta e="T385" id="Seg_5268" s="T384">baraː-t-BIT-LAr</ta>
            <ta e="T386" id="Seg_5269" s="T385">ki͡eŋ</ta>
            <ta e="T387" id="Seg_5270" s="T386">kü͡öl-ttAn</ta>
            <ta e="T388" id="Seg_5271" s="T387">uhun-An-LAr</ta>
            <ta e="T389" id="Seg_5272" s="T388">haŋa</ta>
            <ta e="T390" id="Seg_5273" s="T389">ürek</ta>
            <ta e="T391" id="Seg_5274" s="T390">üstün</ta>
            <ta e="T392" id="Seg_5275" s="T391">kel-BIT-LAr</ta>
            <ta e="T393" id="Seg_5276" s="T392">di͡e-An</ta>
            <ta e="T395" id="Seg_5277" s="T394">ol</ta>
            <ta e="T396" id="Seg_5278" s="T395">ogonnʼor-GA</ta>
            <ta e="T397" id="Seg_5279" s="T396">tij-An-LAr</ta>
            <ta e="T398" id="Seg_5280" s="T397">bu</ta>
            <ta e="T399" id="Seg_5281" s="T398">kihi-LAr</ta>
            <ta e="T400" id="Seg_5282" s="T399">dʼe</ta>
            <ta e="T402" id="Seg_5283" s="T401">dʼe</ta>
            <ta e="T403" id="Seg_5284" s="T402">körügös</ta>
            <ta e="T404" id="Seg_5285" s="T403">kör-AlAː-A</ta>
            <ta e="T405" id="Seg_5286" s="T404">olor</ta>
            <ta e="T406" id="Seg_5287" s="T405">üːhe</ta>
            <ta e="T407" id="Seg_5288" s="T406">hu͡opka-GA</ta>
            <ta e="T408" id="Seg_5289" s="T407">tagɨs-An-GIn</ta>
            <ta e="T409" id="Seg_5290" s="T408">di͡e-An</ta>
            <ta e="T411" id="Seg_5291" s="T410">biːr-LArA</ta>
            <ta e="T412" id="Seg_5292" s="T411">ɨt-Iː-ČIt-LArA</ta>
            <ta e="T413" id="Seg_5293" s="T412">genne</ta>
            <ta e="T414" id="Seg_5294" s="T413">kiːr-TAr-TAː-BIT</ta>
            <ta e="T415" id="Seg_5295" s="T414">dogor-LAr-tI-n</ta>
            <ta e="T416" id="Seg_5296" s="T415">innʼe</ta>
            <ta e="T417" id="Seg_5297" s="T416">gɨn-An</ta>
            <ta e="T418" id="Seg_5298" s="T417">baran</ta>
            <ta e="T419" id="Seg_5299" s="T418">dʼe</ta>
            <ta e="T420" id="Seg_5300" s="T419">kaːn</ta>
            <ta e="T421" id="Seg_5301" s="T420">butugas</ta>
            <ta e="T422" id="Seg_5302" s="T421">bus-BIT-LAr</ta>
            <ta e="T423" id="Seg_5303" s="T422">bu</ta>
            <ta e="T424" id="Seg_5304" s="T423">ogonnʼor</ta>
            <ta e="T426" id="Seg_5305" s="T425">uraha</ta>
            <ta e="T427" id="Seg_5306" s="T426">dʼi͡e-LAːK-LAr</ta>
            <ta e="T429" id="Seg_5307" s="T428">dʼe</ta>
            <ta e="T430" id="Seg_5308" s="T429">butugas-LAː-An</ta>
            <ta e="T431" id="Seg_5309" s="T430">baran</ta>
            <ta e="T432" id="Seg_5310" s="T431">bu͡o</ta>
            <ta e="T433" id="Seg_5311" s="T432">butugas-tI-GAr</ta>
            <ta e="T434" id="Seg_5312" s="T433">bu͡olla</ta>
            <ta e="T435" id="Seg_5313" s="T434">kim-nI</ta>
            <ta e="T436" id="Seg_5314" s="T435">kert-BIT</ta>
            <ta e="T438" id="Seg_5315" s="T437">hɨn</ta>
            <ta e="T439" id="Seg_5316" s="T438">di͡e-AːččI-LAr</ta>
            <ta e="T440" id="Seg_5317" s="T439">oččogo</ta>
            <ta e="T441" id="Seg_5318" s="T440">anɨ</ta>
            <ta e="T442" id="Seg_5319" s="T441">hɨn</ta>
            <ta e="T443" id="Seg_5320" s="T442">li͡eska</ta>
            <ta e="T445" id="Seg_5321" s="T444">li͡eska-nI</ta>
            <ta e="T446" id="Seg_5322" s="T445">tabaːk-GA</ta>
            <ta e="T447" id="Seg_5323" s="T446">di͡eri</ta>
            <ta e="T448" id="Seg_5324" s="T447">kördük</ta>
            <ta e="T449" id="Seg_5325" s="T448">kert-A-kert-A</ta>
            <ta e="T450" id="Seg_5326" s="T449">bu͡o</ta>
            <ta e="T451" id="Seg_5327" s="T450">butugas-GA</ta>
            <ta e="T452" id="Seg_5328" s="T451">kut-BIT</ta>
            <ta e="T453" id="Seg_5329" s="T452">bu͡o</ta>
            <ta e="T455" id="Seg_5330" s="T454">aččɨktaː-An</ta>
            <ta e="T456" id="Seg_5331" s="T455">is-Ar</ta>
            <ta e="T457" id="Seg_5332" s="T456">kihi-LAr</ta>
            <ta e="T458" id="Seg_5333" s="T457">butugas</ta>
            <ta e="T459" id="Seg_5334" s="T458">is-IAK-LArA</ta>
            <ta e="T460" id="Seg_5335" s="T459">di͡e-An</ta>
            <ta e="T461" id="Seg_5336" s="T460">ol</ta>
            <ta e="T462" id="Seg_5337" s="T461">is-IAK-LArA</ta>
            <ta e="T463" id="Seg_5338" s="T462">di͡e-An</ta>
            <ta e="T464" id="Seg_5339" s="T463">ol</ta>
            <ta e="T465" id="Seg_5340" s="T464">butugas</ta>
            <ta e="T466" id="Seg_5341" s="T465">kert-r-BIT</ta>
            <ta e="T467" id="Seg_5342" s="T466">bu͡o</ta>
            <ta e="T469" id="Seg_5343" s="T468">innʼe</ta>
            <ta e="T470" id="Seg_5344" s="T469">kantaj-s</ta>
            <ta e="T471" id="Seg_5345" s="T470">gɨn-TAK-InA</ta>
            <ta e="T472" id="Seg_5346" s="T471">ol</ta>
            <ta e="T473" id="Seg_5347" s="T472">tojon-LArI-n</ta>
            <ta e="T474" id="Seg_5348" s="T473">kabɨrga-tI-n</ta>
            <ta e="T475" id="Seg_5349" s="T474">kaja</ta>
            <ta e="T476" id="Seg_5350" s="T475">ogus-Aːr-I-ŋ</ta>
            <ta e="T477" id="Seg_5351" s="T476">di͡e-An</ta>
            <ta e="T478" id="Seg_5352" s="T477">bɨtɨk-I</ta>
            <ta e="T479" id="Seg_5353" s="T478">eː</ta>
            <ta e="T480" id="Seg_5354" s="T479">kim-I-nAn</ta>
            <ta e="T481" id="Seg_5355" s="T480">batɨja-nAn</ta>
            <ta e="T483" id="Seg_5356" s="T482">oččogo</ta>
            <ta e="T484" id="Seg_5357" s="T483">ere</ta>
            <ta e="T485" id="Seg_5358" s="T484">kɨ͡aj-IAK-BIt</ta>
            <ta e="T486" id="Seg_5359" s="T485">di͡e-BIT</ta>
            <ta e="T487" id="Seg_5360" s="T486">bu͡o</ta>
            <ta e="T488" id="Seg_5361" s="T487">onton</ta>
            <ta e="T489" id="Seg_5362" s="T488">haŋar-TI-tA</ta>
            <ta e="T490" id="Seg_5363" s="T489">kɨ͡aj-IAK-BIt</ta>
            <ta e="T491" id="Seg_5364" s="T490">kajdak</ta>
            <ta e="T492" id="Seg_5365" s="T491">eme</ta>
            <ta e="T494" id="Seg_5366" s="T493">dʼe</ta>
            <ta e="T495" id="Seg_5367" s="T494">ol</ta>
            <ta e="T496" id="Seg_5368" s="T495">kel-BIT-LAr</ta>
            <ta e="T497" id="Seg_5369" s="T496">ol</ta>
            <ta e="T498" id="Seg_5370" s="T497">butugas-LAː-BIT-LAr</ta>
            <ta e="T499" id="Seg_5371" s="T498">dʼe</ta>
            <ta e="T500" id="Seg_5372" s="T499">is-Ar-LAr</ta>
            <ta e="T501" id="Seg_5373" s="T500">butugas-LAː-ŋ</ta>
            <ta e="T502" id="Seg_5374" s="T501">di͡e-BIT</ta>
            <ta e="T503" id="Seg_5375" s="T502">bu͡o</ta>
            <ta e="T504" id="Seg_5376" s="T503">butugas-LAː-BIT-LAr</ta>
            <ta e="T506" id="Seg_5377" s="T505">dʼe</ta>
            <ta e="T507" id="Seg_5378" s="T506">ol</ta>
            <ta e="T508" id="Seg_5379" s="T507">kɨːj-A-r-A</ta>
            <ta e="T509" id="Seg_5380" s="T508">hɨt-TAK-TArInA</ta>
            <ta e="T510" id="Seg_5381" s="T509">dʼe</ta>
            <ta e="T511" id="Seg_5382" s="T510">kel-BIT-LAr</ta>
            <ta e="T512" id="Seg_5383" s="T511">bu͡o</ta>
            <ta e="T513" id="Seg_5384" s="T512">ol</ta>
            <ta e="T514" id="Seg_5385" s="T513">ečikij-LAr-I-ŋ</ta>
            <ta e="T516" id="Seg_5386" s="T515">kel-An-LAr</ta>
            <ta e="T517" id="Seg_5387" s="T516">dʼaktar-LAr-nI</ta>
            <ta e="T518" id="Seg_5388" s="T517">kiːr-TAr-TAː-An</ta>
            <ta e="T519" id="Seg_5389" s="T518">dʼe</ta>
            <ta e="T520" id="Seg_5390" s="T519">bu͡o</ta>
            <ta e="T521" id="Seg_5391" s="T520">ogonnʼor</ta>
            <ta e="T522" id="Seg_5392" s="T521">tohuj-Ar</ta>
            <ta e="T523" id="Seg_5393" s="T522">kiːr-TAr-TAː-Ar</ta>
            <ta e="T525" id="Seg_5394" s="T524">kaja</ta>
            <ta e="T526" id="Seg_5395" s="T525">onnuk-LAr</ta>
            <ta e="T527" id="Seg_5396" s="T526">mannɨk-LAr</ta>
            <ta e="T528" id="Seg_5397" s="T527">ös-TA</ta>
            <ta e="T529" id="Seg_5398" s="T528">kepseː-ŋ</ta>
            <ta e="T530" id="Seg_5399" s="T529">di͡e-An</ta>
            <ta e="T532" id="Seg_5400" s="T531">dʼe</ta>
            <ta e="T533" id="Seg_5401" s="T532">ol</ta>
            <ta e="T534" id="Seg_5402" s="T533">ogonnʼor-GA</ta>
            <ta e="T535" id="Seg_5403" s="T534">tij-An</ta>
            <ta e="T536" id="Seg_5404" s="T535">kepseː-Ar-LAr</ta>
            <ta e="T537" id="Seg_5405" s="T536">bu͡o</ta>
            <ta e="T538" id="Seg_5406" s="T537">emeːksin-nI</ta>
            <ta e="T539" id="Seg_5407" s="T538">egel-TI-BIt</ta>
            <ta e="T540" id="Seg_5408" s="T539">bihigi</ta>
            <ta e="T541" id="Seg_5409" s="T540">iti</ta>
            <ta e="T542" id="Seg_5410" s="T541">hir-ČIt-BIt</ta>
            <ta e="T543" id="Seg_5411" s="T542">di͡e-A-BIt</ta>
            <ta e="T545" id="Seg_5412" s="T544">ojun</ta>
            <ta e="T546" id="Seg_5413" s="T545">emeːksin</ta>
            <ta e="T547" id="Seg_5414" s="T546">iti</ta>
            <ta e="T548" id="Seg_5415" s="T547">poːvar-LAr-BIt</ta>
            <ta e="T550" id="Seg_5416" s="T549">oččogo</ta>
            <ta e="T551" id="Seg_5417" s="T550">ol</ta>
            <ta e="T552" id="Seg_5418" s="T551">ojun</ta>
            <ta e="T553" id="Seg_5419" s="T552">emeːksin</ta>
            <ta e="T554" id="Seg_5420" s="T553">hir-LAː-An</ta>
            <ta e="T555" id="Seg_5421" s="T554">egel-TI-tA</ta>
            <ta e="T556" id="Seg_5422" s="T555">bihigi-GA</ta>
            <ta e="T557" id="Seg_5423" s="T556">ehigi-GA</ta>
            <ta e="T558" id="Seg_5424" s="T557">di͡e-An</ta>
            <ta e="T559" id="Seg_5425" s="T558">ol</ta>
            <ta e="T560" id="Seg_5426" s="T559">kepseː-Ar-LAr</ta>
            <ta e="T561" id="Seg_5427" s="T560">bu͡o</ta>
            <ta e="T562" id="Seg_5428" s="T561">kihi-nI</ta>
            <ta e="T563" id="Seg_5429" s="T562">baraː-A-t-BIT-LAr</ta>
            <ta e="T565" id="Seg_5430" s="T564">kaːn-LArI-n</ta>
            <ta e="T566" id="Seg_5431" s="T565">bu͡olla</ta>
            <ta e="T567" id="Seg_5432" s="T566">bahak-LArI-n</ta>
            <ta e="T568" id="Seg_5433" s="T567">manna</ta>
            <ta e="T569" id="Seg_5434" s="T568">hot-Ar-LAr</ta>
            <ta e="T570" id="Seg_5435" s="T569">e-BIT</ta>
            <ta e="T571" id="Seg_5436" s="T570">kihi-LAr-nI</ta>
            <ta e="T572" id="Seg_5437" s="T571">kert-A</ta>
            <ta e="T573" id="Seg_5438" s="T572">kert-A</ta>
            <ta e="T574" id="Seg_5439" s="T573">hi͡e-A</ta>
            <ta e="T575" id="Seg_5440" s="T574">hi͡e-A</ta>
            <ta e="T576" id="Seg_5441" s="T575">hi͡e-Ar-LAr</ta>
            <ta e="T578" id="Seg_5442" s="T577">oː</ta>
            <ta e="T579" id="Seg_5443" s="T578">tu͡ok</ta>
            <ta e="T580" id="Seg_5444" s="T579">aːt-LAːK</ta>
            <ta e="T581" id="Seg_5445" s="T580">kaːn-tI-GAr</ta>
            <ta e="T582" id="Seg_5446" s="T581">bis-I-LIN-I-BIT</ta>
            <ta e="T583" id="Seg_5447" s="T582">dʼon-LAr-GIt=Ij</ta>
            <ta e="T584" id="Seg_5448" s="T583">dʼe</ta>
            <ta e="T585" id="Seg_5449" s="T584">ahaː-ŋ</ta>
            <ta e="T586" id="Seg_5450" s="T585">di͡e-An</ta>
            <ta e="T587" id="Seg_5451" s="T586">ogonnʼor</ta>
            <ta e="T588" id="Seg_5452" s="T587">dʼe</ta>
            <ta e="T589" id="Seg_5453" s="T588">ahaː-A-t-Ar</ta>
            <ta e="T591" id="Seg_5454" s="T590">dʼe</ta>
            <ta e="T592" id="Seg_5455" s="T591">ol</ta>
            <ta e="T593" id="Seg_5456" s="T592">ahaː</ta>
            <ta e="T594" id="Seg_5457" s="T593">ahaː-A-t-BIT-tI-n</ta>
            <ta e="T595" id="Seg_5458" s="T594">genne</ta>
            <ta e="T596" id="Seg_5459" s="T595">dʼe</ta>
            <ta e="T597" id="Seg_5460" s="T596">ol</ta>
            <ta e="T598" id="Seg_5461" s="T597">oː</ta>
            <ta e="T599" id="Seg_5462" s="T598">kallaːn-BIt</ta>
            <ta e="T600" id="Seg_5463" s="T599">kajdak</ta>
            <ta e="T601" id="Seg_5464" s="T600">kajdak</ta>
            <ta e="T602" id="Seg_5465" s="T601">bu͡ol-Ar</ta>
            <ta e="T603" id="Seg_5466" s="T602">dʼe</ta>
            <ta e="T604" id="Seg_5467" s="T603">bɨlɨt-BIt</ta>
            <ta e="T605" id="Seg_5468" s="T604">kojut-Ar</ta>
            <ta e="T607" id="Seg_5469" s="T606">tu͡ok</ta>
            <ta e="T608" id="Seg_5470" s="T607">bu͡ol-Ar</ta>
            <ta e="T609" id="Seg_5471" s="T608">di͡e-BIT-tA</ta>
            <ta e="T611" id="Seg_5472" s="T610">dʼe</ta>
            <ta e="T612" id="Seg_5473" s="T611">ol</ta>
            <ta e="T613" id="Seg_5474" s="T612">ikki</ta>
            <ta e="T614" id="Seg_5475" s="T613">kamnas-LAːK-nI</ta>
            <ta e="T615" id="Seg_5476" s="T614">ikki</ta>
            <ta e="T616" id="Seg_5477" s="T615">örüt-tI-GA</ta>
            <ta e="T617" id="Seg_5478" s="T616">olort-BIT</ta>
            <ta e="T618" id="Seg_5479" s="T617">bu͡o</ta>
            <ta e="T620" id="Seg_5480" s="T619">dʼe</ta>
            <ta e="T621" id="Seg_5481" s="T620">ol</ta>
            <ta e="T622" id="Seg_5482" s="T621">kantaj-s</ta>
            <ta e="T623" id="Seg_5483" s="T622">gɨn-BIT-tI-GAr</ta>
            <ta e="T624" id="Seg_5484" s="T623">kabɨrga-tI-n</ta>
            <ta e="T625" id="Seg_5485" s="T624">kaj-A</ta>
            <ta e="T626" id="Seg_5486" s="T625">ogus-I-BIT-LArA</ta>
            <ta e="T627" id="Seg_5487" s="T626">batɨja-nAn</ta>
            <ta e="T629" id="Seg_5488" s="T628">anɨ</ta>
            <ta e="T630" id="Seg_5489" s="T629">iti</ta>
            <ta e="T631" id="Seg_5490" s="T630">ölörös-Ar-LArI-GAr</ta>
            <ta e="T632" id="Seg_5491" s="T631">di͡e-Ar</ta>
            <ta e="T633" id="Seg_5492" s="T632">kinoː-LAr-GA</ta>
            <ta e="T634" id="Seg_5493" s="T633">di͡eri</ta>
            <ta e="T636" id="Seg_5494" s="T635">ol</ta>
            <ta e="T637" id="Seg_5495" s="T636">kɨ͡aj-BIT-LAr</ta>
            <ta e="T638" id="Seg_5496" s="T637">ühü</ta>
            <ta e="T639" id="Seg_5497" s="T638">ol-tI-LArI-n</ta>
            <ta e="T641" id="Seg_5498" s="T640">dʼe</ta>
            <ta e="T642" id="Seg_5499" s="T641">aŋar-LArI-n</ta>
            <ta e="T643" id="Seg_5500" s="T642">ölör-TAː-BIT-LArA</ta>
            <ta e="T645" id="Seg_5501" s="T644">hette</ta>
            <ta e="T646" id="Seg_5502" s="T645">kihi-tA</ta>
            <ta e="T647" id="Seg_5503" s="T646">e-BIT</ta>
            <ta e="T648" id="Seg_5504" s="T647">du͡o</ta>
            <ta e="T649" id="Seg_5505" s="T648">kas</ta>
            <ta e="T650" id="Seg_5506" s="T649">du͡o</ta>
            <ta e="T652" id="Seg_5507" s="T651">dʼe</ta>
            <ta e="T653" id="Seg_5508" s="T652">onton</ta>
            <ta e="T654" id="Seg_5509" s="T653">dʼaktar-LAr</ta>
            <ta e="T655" id="Seg_5510" s="T654">kaːl-BIT-LArI-GAr</ta>
            <ta e="T656" id="Seg_5511" s="T655">bu</ta>
            <ta e="T657" id="Seg_5512" s="T656">dʼaktar-LAr</ta>
            <ta e="T658" id="Seg_5513" s="T657">dʼe</ta>
            <ta e="T659" id="Seg_5514" s="T658">etis-Ar-LAr</ta>
            <ta e="T660" id="Seg_5515" s="T659">ühü</ta>
            <ta e="T661" id="Seg_5516" s="T660">bahak-LArI-n</ta>
            <ta e="T662" id="Seg_5517" s="T661">bɨldʼaː-I-s-An-LAr</ta>
            <ta e="T664" id="Seg_5518" s="T663">min</ta>
            <ta e="T665" id="Seg_5519" s="T664">er-I-m</ta>
            <ta e="T666" id="Seg_5520" s="T665">bahak-tA</ta>
            <ta e="T667" id="Seg_5521" s="T666">min</ta>
            <ta e="T668" id="Seg_5522" s="T667">er-I-m</ta>
            <ta e="T669" id="Seg_5523" s="T668">bahak-tA</ta>
            <ta e="T670" id="Seg_5524" s="T669">di͡e-A-s-AːktAː-Ar-LAr</ta>
            <ta e="T671" id="Seg_5525" s="T670">ühü</ta>
            <ta e="T672" id="Seg_5526" s="T671">kotoku-LAr</ta>
            <ta e="T674" id="Seg_5527" s="T673">tu͡ok</ta>
            <ta e="T675" id="Seg_5528" s="T674">huːka</ta>
            <ta e="T676" id="Seg_5529" s="T675">kɨːs-LArA=Ij</ta>
            <ta e="T677" id="Seg_5530" s="T676">iti-LAr-nI</ta>
            <ta e="T678" id="Seg_5531" s="T677">öl-AttAː-An</ta>
            <ta e="T679" id="Seg_5532" s="T678">keːs-I-ŋ</ta>
            <ta e="T680" id="Seg_5533" s="T679">di͡e-BIT</ta>
            <ta e="T681" id="Seg_5534" s="T680">bu͡o</ta>
            <ta e="T682" id="Seg_5535" s="T681">tu͡ok</ta>
            <ta e="T683" id="Seg_5536" s="T682">sustogoj</ta>
            <ta e="T684" id="Seg_5537" s="T683">ogonnʼor-tA</ta>
            <ta e="T685" id="Seg_5538" s="T684">bu͡olla</ta>
            <ta e="T686" id="Seg_5539" s="T685">da</ta>
            <ta e="T687" id="Seg_5540" s="T686">bihi͡ene</ta>
            <ta e="T688" id="Seg_5541" s="T687">Porotov</ta>
            <ta e="T690" id="Seg_5542" s="T689">öl-AttAː-An</ta>
            <ta e="T691" id="Seg_5543" s="T690">keːs-BIT-LAr</ta>
            <ta e="T692" id="Seg_5544" s="T691">muŋ-LAːK-LAr</ta>
            <ta e="T694" id="Seg_5545" s="T693">onton</ta>
            <ta e="T695" id="Seg_5546" s="T694">ol</ta>
            <ta e="T696" id="Seg_5547" s="T695">emeːksin-nI</ta>
            <ta e="T697" id="Seg_5548" s="T696">dʼe</ta>
            <ta e="T698" id="Seg_5549" s="T697">ölör-A</ta>
            <ta e="T699" id="Seg_5550" s="T698">hataː-Ar-LAr</ta>
            <ta e="T700" id="Seg_5551" s="T699">ojun</ta>
            <ta e="T701" id="Seg_5552" s="T700">emeːksin-nI</ta>
            <ta e="T703" id="Seg_5553" s="T702">uː-GA</ta>
            <ta e="T704" id="Seg_5554" s="T703">da</ta>
            <ta e="T705" id="Seg_5555" s="T704">timir-t-Ar-LAr</ta>
            <ta e="T706" id="Seg_5556" s="T705">dʼe</ta>
            <ta e="T707" id="Seg_5557" s="T706">hakor-LAː-An</ta>
            <ta e="T708" id="Seg_5558" s="T707">baran</ta>
            <ta e="T710" id="Seg_5559" s="T709">törüt</ta>
            <ta e="T711" id="Seg_5560" s="T710">timir-BAT</ta>
            <ta e="T713" id="Seg_5561" s="T712">ajagalaː-A</ta>
            <ta e="T714" id="Seg_5562" s="T713">hataː-An</ta>
            <ta e="T715" id="Seg_5563" s="T714">u͡ot-tI-n</ta>
            <ta e="T716" id="Seg_5564" s="T715">otut-An</ta>
            <ta e="T717" id="Seg_5565" s="T716">bu͡o</ta>
            <ta e="T718" id="Seg_5566" s="T717">u͡ot</ta>
            <ta e="T719" id="Seg_5567" s="T718">is-tI-GAr</ta>
            <ta e="T720" id="Seg_5568" s="T719">bɨrak-Ar-LAr</ta>
            <ta e="T722" id="Seg_5569" s="T721">u͡ot-LArA</ta>
            <ta e="T723" id="Seg_5570" s="T722">karaːr-čI</ta>
            <ta e="T724" id="Seg_5571" s="T723">utuj-Ar</ta>
            <ta e="T725" id="Seg_5572" s="T724">ühü</ta>
            <ta e="T727" id="Seg_5573" s="T726">onno</ta>
            <ta e="T728" id="Seg_5574" s="T727">da</ta>
            <ta e="T729" id="Seg_5575" s="T728">di͡e-BIT</ta>
            <ta e="T730" id="Seg_5576" s="T729">bu͡o</ta>
            <ta e="T731" id="Seg_5577" s="T730">tukuː-LAr</ta>
            <ta e="T732" id="Seg_5578" s="T731">min</ta>
            <ta e="T733" id="Seg_5579" s="T732">kün</ta>
            <ta e="T734" id="Seg_5580" s="T733">emeːksin</ta>
            <ta e="T735" id="Seg_5581" s="T734">e-TI-m</ta>
            <ta e="T736" id="Seg_5582" s="T735">kaja</ta>
            <ta e="T737" id="Seg_5583" s="T736">abaːhɨ</ta>
            <ta e="T738" id="Seg_5584" s="T737">bu͡ol-BAtAK-BIn</ta>
            <ta e="T740" id="Seg_5585" s="T739">dʼon-BI-n</ta>
            <ta e="T741" id="Seg_5586" s="T740">baraː-BIT-LArA</ta>
            <ta e="T742" id="Seg_5587" s="T741">bu</ta>
            <ta e="T743" id="Seg_5588" s="T742">bu͡olla</ta>
            <ta e="T744" id="Seg_5589" s="T743">kihi-nI</ta>
            <ta e="T745" id="Seg_5590" s="T744">hü͡öhü-nI</ta>
            <ta e="T746" id="Seg_5591" s="T745">kör-AːrI-BIn</ta>
            <ta e="T747" id="Seg_5592" s="T746">hir-LAː-An</ta>
            <ta e="T748" id="Seg_5593" s="T747">kel-BIT-I-m</ta>
            <ta e="T750" id="Seg_5594" s="T749">min-n</ta>
            <ta e="T751" id="Seg_5595" s="T750">erej-LAː-m-ŋ</ta>
            <ta e="T752" id="Seg_5596" s="T751">min</ta>
            <ta e="T753" id="Seg_5597" s="T752">itinnik</ta>
            <ta e="T754" id="Seg_5598" s="T753">ka</ta>
            <ta e="T755" id="Seg_5599" s="T754">öl-IAK-m</ta>
            <ta e="T756" id="Seg_5600" s="T755">hu͡ok-tA</ta>
            <ta e="T757" id="Seg_5601" s="T756">di͡e-Ar</ta>
            <ta e="T759" id="Seg_5602" s="T758">kɨːs</ta>
            <ta e="T760" id="Seg_5603" s="T759">ogo-tA</ta>
            <ta e="T761" id="Seg_5604" s="T760">haŋardɨː</ta>
            <ta e="T762" id="Seg_5605" s="T761">bɨrtak</ta>
            <ta e="T763" id="Seg_5606" s="T762">bu͡ol-BIT</ta>
            <ta e="T764" id="Seg_5607" s="T763">kɨːs</ta>
            <ta e="T765" id="Seg_5608" s="T764">ogo-tA</ta>
            <ta e="T766" id="Seg_5609" s="T765">moːj-BI-nAn</ta>
            <ta e="T767" id="Seg_5610" s="T766">atɨllaː-A-TIn</ta>
            <ta e="T768" id="Seg_5611" s="T767">oččogo</ta>
            <ta e="T769" id="Seg_5612" s="T768">öl-IAK-m</ta>
            <ta e="T770" id="Seg_5613" s="T769">di͡e-Ar</ta>
            <ta e="T771" id="Seg_5614" s="T770">ol</ta>
            <ta e="T772" id="Seg_5615" s="T771">moːj-BI-nAn</ta>
            <ta e="T773" id="Seg_5616" s="T772">atɨllaː-A-t-BIT-LAr</ta>
            <ta e="T775" id="Seg_5617" s="T774">dʼe</ta>
            <ta e="T776" id="Seg_5618" s="T775">ol</ta>
            <ta e="T777" id="Seg_5619" s="T776">emeːksin-LArA</ta>
            <ta e="T778" id="Seg_5620" s="T777">öl-An</ta>
            <ta e="T779" id="Seg_5621" s="T778">kaːl-BIT</ta>
            <ta e="T780" id="Seg_5622" s="T779">dʼe</ta>
            <ta e="T781" id="Seg_5623" s="T780">ele-tA</ta>
            <ta e="T782" id="Seg_5624" s="T781">ol</ta>
            <ta e="T783" id="Seg_5625" s="T782">kepseː-BIT-LArA</ta>
            <ta e="T784" id="Seg_5626" s="T783">öjdöː-BAT-BIn</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_5627" s="T1">then</ta>
            <ta e="T3" id="Seg_5628" s="T2">once</ta>
            <ta e="T4" id="Seg_5629" s="T3">hunt-CVB.SIM</ta>
            <ta e="T5" id="Seg_5630" s="T4">go.out-EP-PST2-3PL</ta>
            <ta e="T6" id="Seg_5631" s="T5">old.man-PL.[NOM]</ta>
            <ta e="T8" id="Seg_5632" s="T7">old.man-PL.[NOM]</ta>
            <ta e="T9" id="Seg_5633" s="T8">come-PRS-3PL</ta>
            <ta e="T10" id="Seg_5634" s="T9">boy-PL.[NOM]</ta>
            <ta e="T12" id="Seg_5635" s="T11">well</ta>
            <ta e="T13" id="Seg_5636" s="T12">Syndassko-1PL-DAT/LOC</ta>
            <ta e="T14" id="Seg_5637" s="T13">house.[NOM]</ta>
            <ta e="T15" id="Seg_5638" s="T14">stand.up-PST2.[3SG]</ta>
            <ta e="T16" id="Seg_5639" s="T15">say-PST2-3PL</ta>
            <ta e="T18" id="Seg_5640" s="T17">whereto</ta>
            <ta e="T19" id="Seg_5641" s="T18">hunt-HAB-1PL</ta>
            <ta e="T21" id="Seg_5642" s="T20">oh</ta>
            <ta e="T22" id="Seg_5643" s="T21">well</ta>
            <ta e="T23" id="Seg_5644" s="T22">this</ta>
            <ta e="T24" id="Seg_5645" s="T23">house.[NOM]</ta>
            <ta e="T25" id="Seg_5646" s="T24">stand.up-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T26" id="Seg_5647" s="T25">be.afraid-PRS-3PL</ta>
            <ta e="T27" id="Seg_5648" s="T26">come-PTCP.FUT-3PL-ACC</ta>
            <ta e="T28" id="Seg_5649" s="T27">it.is.said</ta>
            <ta e="T29" id="Seg_5650" s="T28">Russian-PL.[NOM]</ta>
            <ta e="T30" id="Seg_5651" s="T29">there.is-3PL</ta>
            <ta e="T31" id="Seg_5652" s="T30">say-CVB.SEQ</ta>
            <ta e="T33" id="Seg_5653" s="T32">oh</ta>
            <ta e="T34" id="Seg_5654" s="T33">well</ta>
            <ta e="T35" id="Seg_5655" s="T34">this</ta>
            <ta e="T36" id="Seg_5656" s="T35">earth-1PL-ACC</ta>
            <ta e="T37" id="Seg_5657" s="T36">haul-POSS</ta>
            <ta e="T38" id="Seg_5658" s="T37">NEG</ta>
            <ta e="T39" id="Seg_5659" s="T38">make-PST1-3PL</ta>
            <ta e="T40" id="Seg_5660" s="T39">how</ta>
            <ta e="T41" id="Seg_5661" s="T40">be-HAB-1PL=Q</ta>
            <ta e="T42" id="Seg_5662" s="T41">say-PRS-3PL</ta>
            <ta e="T43" id="Seg_5663" s="T42">it.is.said</ta>
            <ta e="T44" id="Seg_5664" s="T43">that</ta>
            <ta e="T45" id="Seg_5665" s="T44">old.man-PL.[NOM]</ta>
            <ta e="T46" id="Seg_5666" s="T45">well</ta>
            <ta e="T48" id="Seg_5667" s="T47">say-EP-RECP/COLL-PRS-1PL</ta>
            <ta e="T49" id="Seg_5668" s="T48">say-PRS-3PL</ta>
            <ta e="T51" id="Seg_5669" s="T50">chat-PTCP.PRS-3PL-ACC</ta>
            <ta e="T52" id="Seg_5670" s="T51">hear-PRS-1SG</ta>
            <ta e="T53" id="Seg_5671" s="T52">grandfather-PL-EP-1SG.[NOM]</ta>
            <ta e="T54" id="Seg_5672" s="T53">well</ta>
            <ta e="T56" id="Seg_5673" s="T55">then</ta>
            <ta e="T57" id="Seg_5674" s="T56">well</ta>
            <ta e="T58" id="Seg_5675" s="T57">then</ta>
            <ta e="T59" id="Seg_5676" s="T58">come-PRS-3PL</ta>
            <ta e="T61" id="Seg_5677" s="T60">come-CVB.SEQ-3PL</ta>
            <ta e="T62" id="Seg_5678" s="T61">see-PST2-3PL</ta>
            <ta e="T63" id="Seg_5679" s="T62">two</ta>
            <ta e="T64" id="Seg_5680" s="T63">human.being.[NOM]</ta>
            <ta e="T65" id="Seg_5681" s="T64">go-PRS-3PL</ta>
            <ta e="T66" id="Seg_5682" s="T65">it.is.said</ta>
            <ta e="T68" id="Seg_5683" s="T67">woman-3SG-GEN</ta>
            <ta e="T69" id="Seg_5684" s="T68">name-3SG.[NOM]</ta>
            <ta e="T70" id="Seg_5685" s="T69">Darya.[NOM]</ta>
            <ta e="T72" id="Seg_5686" s="T71">man-3SG-ACC</ta>
            <ta e="T73" id="Seg_5687" s="T72">name-3SG.[NOM]</ta>
            <ta e="T74" id="Seg_5688" s="T73">who.[NOM]</ta>
            <ta e="T75" id="Seg_5689" s="T74">say-PST2-3PL=Q</ta>
            <ta e="T76" id="Seg_5690" s="T75">well</ta>
            <ta e="T77" id="Seg_5691" s="T76">EMPH</ta>
            <ta e="T78" id="Seg_5692" s="T77">%%</ta>
            <ta e="T79" id="Seg_5693" s="T78">very</ta>
            <ta e="T80" id="Seg_5694" s="T79">name-PROPR</ta>
            <ta e="T81" id="Seg_5695" s="T80">human.being.[NOM]</ta>
            <ta e="T82" id="Seg_5696" s="T81">who.[NOM]</ta>
            <ta e="T83" id="Seg_5697" s="T82">INDEF</ta>
            <ta e="T84" id="Seg_5698" s="T83">say-HAB-3PL</ta>
            <ta e="T85" id="Seg_5699" s="T84">EVID</ta>
            <ta e="T87" id="Seg_5700" s="T86">at.all</ta>
            <ta e="T88" id="Seg_5701" s="T87">remember-NEG-1SG</ta>
            <ta e="T89" id="Seg_5702" s="T88">name-3SG-ACC</ta>
            <ta e="T91" id="Seg_5703" s="T90">that-DAT/LOC</ta>
            <ta e="T92" id="Seg_5704" s="T91">EMPH</ta>
            <ta e="T93" id="Seg_5705" s="T92">come.closer-PRS-3PL</ta>
            <ta e="T94" id="Seg_5706" s="T93">it.is.said</ta>
            <ta e="T95" id="Seg_5707" s="T94">be.afraid-CVB.SIM</ta>
            <ta e="T96" id="Seg_5708" s="T95">be.afraid-CVB.SIM</ta>
            <ta e="T97" id="Seg_5709" s="T96">come.closer-PRS-3PL</ta>
            <ta e="T98" id="Seg_5710" s="T97">it.is.said</ta>
            <ta e="T100" id="Seg_5711" s="T99">this</ta>
            <ta e="T101" id="Seg_5712" s="T100">why</ta>
            <ta e="T102" id="Seg_5713" s="T101">come-PST2-2PL=Q</ta>
            <ta e="T103" id="Seg_5714" s="T102">1PL.[NOM]</ta>
            <ta e="T104" id="Seg_5715" s="T103">earth-1PL-DAT/LOC</ta>
            <ta e="T106" id="Seg_5716" s="T105">why</ta>
            <ta e="T107" id="Seg_5717" s="T106">find.[MED]-PST1-2PL</ta>
            <ta e="T109" id="Seg_5718" s="T108">hunt-PTCP.PRS</ta>
            <ta e="T110" id="Seg_5719" s="T109">earth-1PL-ACC</ta>
            <ta e="T111" id="Seg_5720" s="T110">why</ta>
            <ta e="T112" id="Seg_5721" s="T111">break-PRS-2PL</ta>
            <ta e="T113" id="Seg_5722" s="T112">say-PRS-3PL</ta>
            <ta e="T114" id="Seg_5723" s="T113">it.is.said</ta>
            <ta e="T115" id="Seg_5724" s="T114">old.man-PL.[NOM]</ta>
            <ta e="T117" id="Seg_5725" s="T116">well</ta>
            <ta e="T118" id="Seg_5726" s="T117">here</ta>
            <ta e="T119" id="Seg_5727" s="T118">though</ta>
            <ta e="T120" id="Seg_5728" s="T119">population.[NOM]</ta>
            <ta e="T121" id="Seg_5729" s="T120">make-FUT-3PL</ta>
            <ta e="T122" id="Seg_5730" s="T121">human.being.[NOM]</ta>
            <ta e="T123" id="Seg_5731" s="T122">stand.up-FUT-3SG</ta>
            <ta e="T125" id="Seg_5732" s="T124">human.being.[NOM]</ta>
            <ta e="T126" id="Seg_5733" s="T125">be.born-FUT-3SG</ta>
            <ta e="T127" id="Seg_5734" s="T126">here</ta>
            <ta e="T128" id="Seg_5735" s="T127">many</ta>
            <ta e="T129" id="Seg_5736" s="T128">human.being.[NOM]</ta>
            <ta e="T130" id="Seg_5737" s="T129">live-FUT-3SG</ta>
            <ta e="T132" id="Seg_5738" s="T131">then</ta>
            <ta e="T133" id="Seg_5739" s="T132">landing-1PL.[NOM]</ta>
            <ta e="T134" id="Seg_5740" s="T133">EMPH</ta>
            <ta e="T135" id="Seg_5741" s="T134">that.[NOM]</ta>
            <ta e="T136" id="Seg_5742" s="T135">there.is</ta>
            <ta e="T137" id="Seg_5743" s="T136">%%-CVB.SEQ</ta>
            <ta e="T138" id="Seg_5744" s="T137">go.in-PST2.[3SG]</ta>
            <ta e="T140" id="Seg_5745" s="T139">why</ta>
            <ta e="T141" id="Seg_5746" s="T140">distant-3SG.[NOM]=Q</ta>
            <ta e="T142" id="Seg_5747" s="T141">say-PRS-3PL</ta>
            <ta e="T143" id="Seg_5748" s="T142">it.is.said</ta>
            <ta e="T144" id="Seg_5749" s="T143">landing-1PL.[NOM]</ta>
            <ta e="T146" id="Seg_5750" s="T145">fairway-2PL.[NOM]</ta>
            <ta e="T147" id="Seg_5751" s="T146">small.[NOM]</ta>
            <ta e="T148" id="Seg_5752" s="T147">here</ta>
            <ta e="T149" id="Seg_5753" s="T148">that.[NOM]</ta>
            <ta e="T150" id="Seg_5754" s="T149">because.of</ta>
            <ta e="T151" id="Seg_5755" s="T150">there</ta>
            <ta e="T152" id="Seg_5756" s="T151">be-PRS.[3SG]</ta>
            <ta e="T153" id="Seg_5757" s="T152">fairway-2PL.[NOM]</ta>
            <ta e="T155" id="Seg_5758" s="T154">thither</ta>
            <ta e="T156" id="Seg_5759" s="T155">steamer-PL.[NOM]</ta>
            <ta e="T157" id="Seg_5760" s="T156">come-FUT-3PL</ta>
            <ta e="T159" id="Seg_5761" s="T158">be.surprised-PRS-3PL</ta>
            <ta e="T160" id="Seg_5762" s="T159">it.is.said</ta>
            <ta e="T161" id="Seg_5763" s="T160">what.[NOM]</ta>
            <ta e="T162" id="Seg_5764" s="T161">name-3SG.[NOM]=Q</ta>
            <ta e="T163" id="Seg_5765" s="T162">steamer.[NOM]</ta>
            <ta e="T164" id="Seg_5766" s="T163">think-PRS.[3SG]</ta>
            <ta e="T167" id="Seg_5767" s="T166">pull-PTCP.PRS</ta>
            <ta e="T168" id="Seg_5768" s="T167">come-PRS.[3SG]</ta>
            <ta e="T169" id="Seg_5769" s="T168">steamer.[NOM]</ta>
            <ta e="T171" id="Seg_5770" s="T170">well</ta>
            <ta e="T172" id="Seg_5771" s="T171">that</ta>
            <ta e="T173" id="Seg_5772" s="T172">later</ta>
            <ta e="T174" id="Seg_5773" s="T173">later</ta>
            <ta e="T175" id="Seg_5774" s="T174">later</ta>
            <ta e="T176" id="Seg_5775" s="T175">later</ta>
            <ta e="T177" id="Seg_5776" s="T176">that</ta>
            <ta e="T178" id="Seg_5777" s="T177">arise-CVB.SEQ</ta>
            <ta e="T179" id="Seg_5778" s="T178">arise-CVB.SEQ</ta>
            <ta e="T180" id="Seg_5779" s="T179">this</ta>
            <ta e="T181" id="Seg_5780" s="T180">Syndassko-2SG.[NOM]</ta>
            <ta e="T182" id="Seg_5781" s="T181">become-CVB.SEQ</ta>
            <ta e="T183" id="Seg_5782" s="T182">stay-PST2.[3SG]</ta>
            <ta e="T184" id="Seg_5783" s="T183">EMPH</ta>
            <ta e="T186" id="Seg_5784" s="T185">school-POSS</ta>
            <ta e="T187" id="Seg_5785" s="T186">NEG-3PL</ta>
            <ta e="T188" id="Seg_5786" s="T187">pole.[NOM]</ta>
            <ta e="T189" id="Seg_5787" s="T188">tent-DAT/LOC</ta>
            <ta e="T190" id="Seg_5788" s="T189">teach-PRS-3PL</ta>
            <ta e="T191" id="Seg_5789" s="T190">self-3PL.[NOM]</ta>
            <ta e="T192" id="Seg_5790" s="T191">build-CVB.SEQ-3PL</ta>
            <ta e="T194" id="Seg_5791" s="T193">balok-PL-DAT/LOC</ta>
            <ta e="T196" id="Seg_5792" s="T195">sell-PRS-3PL</ta>
            <ta e="T198" id="Seg_5793" s="T197">then</ta>
            <ta e="T199" id="Seg_5794" s="T198">build-PTCP.PST</ta>
            <ta e="T200" id="Seg_5795" s="T199">house-3PL.[NOM]</ta>
            <ta e="T201" id="Seg_5796" s="T200">there.is</ta>
            <ta e="T202" id="Seg_5797" s="T201">be-PST1-3SG</ta>
            <ta e="T203" id="Seg_5798" s="T202">former</ta>
            <ta e="T204" id="Seg_5799" s="T203">now</ta>
            <ta e="T205" id="Seg_5800" s="T204">crash-PST2-3SG</ta>
            <ta e="T206" id="Seg_5801" s="T205">EMPH</ta>
            <ta e="T207" id="Seg_5802" s="T206">that-3SG-2SG.[NOM]</ta>
            <ta e="T208" id="Seg_5803" s="T207">shop-3PL.[NOM]</ta>
            <ta e="T210" id="Seg_5804" s="T209">that.[NOM]</ta>
            <ta e="T211" id="Seg_5805" s="T210">because.of</ta>
            <ta e="T212" id="Seg_5806" s="T211">Syndassko.[NOM]</ta>
            <ta e="T213" id="Seg_5807" s="T212">Syndassko.[NOM]</ta>
            <ta e="T214" id="Seg_5808" s="T213">Darya.[NOM]</ta>
            <ta e="T215" id="Seg_5809" s="T214">what-ACC</ta>
            <ta e="T216" id="Seg_5810" s="T215">who-ACC</ta>
            <ta e="T217" id="Seg_5811" s="T216">Darya.[NOM]</ta>
            <ta e="T218" id="Seg_5812" s="T217">say-PRS-3PL</ta>
            <ta e="T219" id="Seg_5813" s="T218">EVID</ta>
            <ta e="T220" id="Seg_5814" s="T219">that.[NOM]</ta>
            <ta e="T221" id="Seg_5815" s="T220">differently</ta>
            <ta e="T222" id="Seg_5816" s="T221">name-PRS-3PL</ta>
            <ta e="T223" id="Seg_5817" s="T222">it.is.said</ta>
            <ta e="T225" id="Seg_5818" s="T224">AFFIRM</ta>
            <ta e="T226" id="Seg_5819" s="T225">this</ta>
            <ta e="T227" id="Seg_5820" s="T226">Syndassko-ACC</ta>
            <ta e="T229" id="Seg_5821" s="T228">that.[NOM]</ta>
            <ta e="T230" id="Seg_5822" s="T229">hunt-PTCP.PRS</ta>
            <ta e="T231" id="Seg_5823" s="T230">place-3PL-ACC</ta>
            <ta e="T232" id="Seg_5824" s="T231">take.away-PST2-3PL</ta>
            <ta e="T233" id="Seg_5825" s="T232">then</ta>
            <ta e="T234" id="Seg_5826" s="T233">later</ta>
            <ta e="T235" id="Seg_5827" s="T234">later</ta>
            <ta e="T236" id="Seg_5828" s="T235">EVID</ta>
            <ta e="T237" id="Seg_5829" s="T236">that.[NOM]</ta>
            <ta e="T238" id="Seg_5830" s="T237">be-PST2-3PL</ta>
            <ta e="T240" id="Seg_5831" s="T239">then</ta>
            <ta e="T241" id="Seg_5832" s="T240">later</ta>
            <ta e="T242" id="Seg_5833" s="T241">that.[NOM]</ta>
            <ta e="T243" id="Seg_5834" s="T242">this</ta>
            <ta e="T244" id="Seg_5835" s="T243">come-CVB.SIM</ta>
            <ta e="T245" id="Seg_5836" s="T244">not.yet-3SG</ta>
            <ta e="T246" id="Seg_5837" s="T245">Lygiy-PL.[NOM]</ta>
            <ta e="T247" id="Seg_5838" s="T246">say-CVB.SEQ</ta>
            <ta e="T248" id="Seg_5839" s="T247">there.is</ta>
            <ta e="T249" id="Seg_5840" s="T248">be-PST1-3PL</ta>
            <ta e="T250" id="Seg_5841" s="T249">it.is.said</ta>
            <ta e="T252" id="Seg_5842" s="T251">eh</ta>
            <ta e="T253" id="Seg_5843" s="T252">who-PL.[NOM]</ta>
            <ta e="T254" id="Seg_5844" s="T253">benäht-PL.[NOM]</ta>
            <ta e="T255" id="Seg_5845" s="T254">say-PRS-3PL</ta>
            <ta e="T256" id="Seg_5846" s="T255">that</ta>
            <ta e="T258" id="Seg_5847" s="T257">there</ta>
            <ta e="T259" id="Seg_5848" s="T258">that.[NOM]</ta>
            <ta e="T260" id="Seg_5849" s="T259">landing.[NOM]</ta>
            <ta e="T261" id="Seg_5850" s="T260">remote</ta>
            <ta e="T262" id="Seg_5851" s="T261">side-3SG-DAT/LOC</ta>
            <ta e="T263" id="Seg_5852" s="T262">MOD</ta>
            <ta e="T264" id="Seg_5853" s="T263">house.[NOM]</ta>
            <ta e="T265" id="Seg_5854" s="T264">trace-PL-3SG.[NOM]</ta>
            <ta e="T266" id="Seg_5855" s="T265">there.is-3PL</ta>
            <ta e="T267" id="Seg_5856" s="T266">two</ta>
            <ta e="T268" id="Seg_5857" s="T267">three</ta>
            <ta e="T269" id="Seg_5858" s="T268">house.[NOM]</ta>
            <ta e="T270" id="Seg_5859" s="T269">trace-3SG.[NOM]</ta>
            <ta e="T272" id="Seg_5860" s="T271">there</ta>
            <ta e="T273" id="Seg_5861" s="T272">live-PRS-3PL</ta>
            <ta e="T274" id="Seg_5862" s="T273">that</ta>
            <ta e="T275" id="Seg_5863" s="T274">hunter-PL.[NOM]</ta>
            <ta e="T276" id="Seg_5864" s="T275">live-TEMP-3PL</ta>
            <ta e="T277" id="Seg_5865" s="T276">Lygiy-PL.[NOM]</ta>
            <ta e="T278" id="Seg_5866" s="T277">come-PST2-3PL</ta>
            <ta e="T280" id="Seg_5867" s="T279">Lygiy-PL.[NOM]</ta>
            <ta e="T281" id="Seg_5868" s="T280">benäht-PL.[NOM]</ta>
            <ta e="T283" id="Seg_5869" s="T282">that</ta>
            <ta e="T284" id="Seg_5870" s="T283">come-PST2-3PL</ta>
            <ta e="T285" id="Seg_5871" s="T284">three</ta>
            <ta e="T286" id="Seg_5872" s="T285">woman-PROPR-3PL</ta>
            <ta e="T287" id="Seg_5873" s="T286">that-PL.[NOM]</ta>
            <ta e="T289" id="Seg_5874" s="T288">one-3PL.[NOM]</ta>
            <ta e="T290" id="Seg_5875" s="T289">shaman.[NOM]</ta>
            <ta e="T292" id="Seg_5876" s="T291">two-3PL.[NOM]</ta>
            <ta e="T293" id="Seg_5877" s="T292">cook.[NOM]</ta>
            <ta e="T295" id="Seg_5878" s="T294">well</ta>
            <ta e="T296" id="Seg_5879" s="T295">this</ta>
            <ta e="T297" id="Seg_5880" s="T296">human.being-ACC</ta>
            <ta e="T298" id="Seg_5881" s="T297">kill-PST1-3SG</ta>
            <ta e="T299" id="Seg_5882" s="T298">kill-PTCP.PST-EP-INSTR</ta>
            <ta e="T300" id="Seg_5883" s="T299">come-PST2-3PL</ta>
            <ta e="T301" id="Seg_5884" s="T300">that-PL-ACC</ta>
            <ta e="T302" id="Seg_5885" s="T301">cook.[NOM]</ta>
            <ta e="T303" id="Seg_5886" s="T302">make-CVB.PURP</ta>
            <ta e="T304" id="Seg_5887" s="T303">bring-PST2-3PL</ta>
            <ta e="T305" id="Seg_5888" s="T304">bad.[NOM]</ta>
            <ta e="T306" id="Seg_5889" s="T305">human.being-PL.[NOM]</ta>
            <ta e="T308" id="Seg_5890" s="T307">how</ta>
            <ta e="T309" id="Seg_5891" s="T308">as.if</ta>
            <ta e="T310" id="Seg_5892" s="T309">what-3PL.[NOM]</ta>
            <ta e="T311" id="Seg_5893" s="T310">ruffian-PL.[NOM]</ta>
            <ta e="T312" id="Seg_5894" s="T311">apparently</ta>
            <ta e="T313" id="Seg_5895" s="T312">then</ta>
            <ta e="T315" id="Seg_5896" s="T314">that</ta>
            <ta e="T316" id="Seg_5897" s="T315">come-CVB.SEQ</ta>
            <ta e="T317" id="Seg_5898" s="T316">come-CVB.SEQ-3PL</ta>
            <ta e="T318" id="Seg_5899" s="T317">hither</ta>
            <ta e="T319" id="Seg_5900" s="T318">come-PST2-3PL</ta>
            <ta e="T320" id="Seg_5901" s="T319">hunter-PL.[NOM]</ta>
            <ta e="T321" id="Seg_5902" s="T320">one-3PL.[NOM]</ta>
            <ta e="T322" id="Seg_5903" s="T321">EMPH</ta>
            <ta e="T323" id="Seg_5904" s="T322">agile.[NOM]</ta>
            <ta e="T325" id="Seg_5905" s="T324">one-3PL.[NOM]</ta>
            <ta e="T326" id="Seg_5906" s="T325">well.seeing.[NOM]</ta>
            <ta e="T327" id="Seg_5907" s="T326">one-3PL.[NOM]</ta>
            <ta e="T328" id="Seg_5908" s="T327">shoot-NMNZ-AG.[NOM]</ta>
            <ta e="T329" id="Seg_5909" s="T328">hunter-PL.[NOM]</ta>
            <ta e="T330" id="Seg_5910" s="T329">our.[NOM]</ta>
            <ta e="T331" id="Seg_5911" s="T330">it.is.said</ta>
            <ta e="T332" id="Seg_5912" s="T331">tell-PRS-3PL</ta>
            <ta e="T334" id="Seg_5913" s="T333">that-PL.[NOM]</ta>
            <ta e="T335" id="Seg_5914" s="T334">though</ta>
            <ta e="T336" id="Seg_5915" s="T335">human.being-ACC</ta>
            <ta e="T337" id="Seg_5916" s="T336">defeat-EP-CAUS-CVB.SEQ</ta>
            <ta e="T338" id="Seg_5917" s="T337">come-CVB.SEQ</ta>
            <ta e="T339" id="Seg_5918" s="T338">come-CVB.SEQ-3PL</ta>
            <ta e="T340" id="Seg_5919" s="T339">this</ta>
            <ta e="T341" id="Seg_5920" s="T340">old.man-PL.[NOM]</ta>
            <ta e="T342" id="Seg_5921" s="T341">Maliy</ta>
            <ta e="T343" id="Seg_5922" s="T342">say-CVB.SEQ</ta>
            <ta e="T344" id="Seg_5923" s="T343">place-DAT/LOC</ta>
            <ta e="T345" id="Seg_5924" s="T344">Porotov</ta>
            <ta e="T346" id="Seg_5925" s="T345">say-CVB.SEQ</ta>
            <ta e="T347" id="Seg_5926" s="T346">human.being.[NOM]</ta>
            <ta e="T348" id="Seg_5927" s="T347">there.is</ta>
            <ta e="T349" id="Seg_5928" s="T348">be-PST1-3SG</ta>
            <ta e="T351" id="Seg_5929" s="T350">that-DAT/LOC</ta>
            <ta e="T352" id="Seg_5930" s="T351">go-PST2-3PL</ta>
            <ta e="T353" id="Seg_5931" s="T352">this</ta>
            <ta e="T354" id="Seg_5932" s="T353">human.being-PL.[NOM]</ta>
            <ta e="T356" id="Seg_5933" s="T355">well</ta>
            <ta e="T357" id="Seg_5934" s="T356">friend.[NOM]</ta>
            <ta e="T358" id="Seg_5935" s="T357">such</ta>
            <ta e="T359" id="Seg_5936" s="T358">such</ta>
            <ta e="T360" id="Seg_5937" s="T359">army.[NOM]</ta>
            <ta e="T361" id="Seg_5938" s="T360">snout-PROPR</ta>
            <ta e="T362" id="Seg_5939" s="T361">dog-PL.[NOM]</ta>
            <ta e="T363" id="Seg_5940" s="T362">chop-PRS-3PL</ta>
            <ta e="T364" id="Seg_5941" s="T363">kill-PTCP.FUT-AG-PL.[NOM]</ta>
            <ta e="T365" id="Seg_5942" s="T364">come-3PL</ta>
            <ta e="T366" id="Seg_5943" s="T365">benäht-PL.[NOM]</ta>
            <ta e="T368" id="Seg_5944" s="T367">well</ta>
            <ta e="T369" id="Seg_5945" s="T368">that</ta>
            <ta e="T370" id="Seg_5946" s="T369">escape-PRS-3PL</ta>
            <ta e="T371" id="Seg_5947" s="T370">Maliy-DAT/LOC</ta>
            <ta e="T372" id="Seg_5948" s="T371">escape-PST2-3PL</ta>
            <ta e="T374" id="Seg_5949" s="T373">that</ta>
            <ta e="T375" id="Seg_5950" s="T374">escape-CVB.SEQ</ta>
            <ta e="T376" id="Seg_5951" s="T375">reach-PST2-3PL</ta>
            <ta e="T377" id="Seg_5952" s="T376">Porotov-DAT/LOC</ta>
            <ta e="T378" id="Seg_5953" s="T377">reach-CVB.SEQ-3PL</ta>
            <ta e="T379" id="Seg_5954" s="T378">EMPH</ta>
            <ta e="T380" id="Seg_5955" s="T379">tell-PRS-3PL</ta>
            <ta e="T381" id="Seg_5956" s="T380">well</ta>
            <ta e="T382" id="Seg_5957" s="T381">such-PL.[NOM]</ta>
            <ta e="T383" id="Seg_5958" s="T382">come-PST1-3PL</ta>
            <ta e="T384" id="Seg_5959" s="T383">human.being-ACC</ta>
            <ta e="T385" id="Seg_5960" s="T384">defeat-CAUS-PST2-3PL</ta>
            <ta e="T386" id="Seg_5961" s="T385">broad</ta>
            <ta e="T387" id="Seg_5962" s="T386">lake-ABL</ta>
            <ta e="T388" id="Seg_5963" s="T387">flow-CVB.SEQ-3PL</ta>
            <ta e="T389" id="Seg_5964" s="T388">new</ta>
            <ta e="T390" id="Seg_5965" s="T389">river.[NOM]</ta>
            <ta e="T391" id="Seg_5966" s="T390">along</ta>
            <ta e="T392" id="Seg_5967" s="T391">come-PST2-3PL</ta>
            <ta e="T393" id="Seg_5968" s="T392">say-CVB.SEQ</ta>
            <ta e="T395" id="Seg_5969" s="T394">that</ta>
            <ta e="T396" id="Seg_5970" s="T395">old.man-DAT/LOC</ta>
            <ta e="T397" id="Seg_5971" s="T396">reach-CVB.SEQ-3PL</ta>
            <ta e="T398" id="Seg_5972" s="T397">this</ta>
            <ta e="T399" id="Seg_5973" s="T398">human.being-PL.[NOM]</ta>
            <ta e="T400" id="Seg_5974" s="T399">well</ta>
            <ta e="T402" id="Seg_5975" s="T401">well</ta>
            <ta e="T403" id="Seg_5976" s="T402">well.seeing.[NOM]</ta>
            <ta e="T404" id="Seg_5977" s="T403">see-FREQ-CVB.SIM</ta>
            <ta e="T405" id="Seg_5978" s="T404">sit.[IMP.2SG]</ta>
            <ta e="T406" id="Seg_5979" s="T405">up</ta>
            <ta e="T407" id="Seg_5980" s="T406">hill-DAT/LOC</ta>
            <ta e="T408" id="Seg_5981" s="T407">go.out-CVB.SEQ-2SG</ta>
            <ta e="T409" id="Seg_5982" s="T408">say-CVB.SEQ</ta>
            <ta e="T411" id="Seg_5983" s="T410">one-3PL.[NOM]</ta>
            <ta e="T412" id="Seg_5984" s="T411">shoot-NMNZ-AG-3PL.[NOM]</ta>
            <ta e="T413" id="Seg_5985" s="T412">after</ta>
            <ta e="T414" id="Seg_5986" s="T413">go.in-CAUS-ITER-PST2.[3SG]</ta>
            <ta e="T415" id="Seg_5987" s="T414">friend-PL-3SG-ACC</ta>
            <ta e="T416" id="Seg_5988" s="T415">so</ta>
            <ta e="T417" id="Seg_5989" s="T416">make-CVB.SEQ</ta>
            <ta e="T418" id="Seg_5990" s="T417">after</ta>
            <ta e="T419" id="Seg_5991" s="T418">well</ta>
            <ta e="T420" id="Seg_5992" s="T419">blood.[NOM]</ta>
            <ta e="T421" id="Seg_5993" s="T420">chowder.[NOM]</ta>
            <ta e="T422" id="Seg_5994" s="T421">boil-PST2-3PL</ta>
            <ta e="T423" id="Seg_5995" s="T422">this</ta>
            <ta e="T424" id="Seg_5996" s="T423">old.man.[NOM]</ta>
            <ta e="T426" id="Seg_5997" s="T425">pole.[NOM]</ta>
            <ta e="T427" id="Seg_5998" s="T426">tent-PROPR-3PL</ta>
            <ta e="T429" id="Seg_5999" s="T428">well</ta>
            <ta e="T430" id="Seg_6000" s="T429">chowder-VBZ-CVB.SEQ</ta>
            <ta e="T431" id="Seg_6001" s="T430">after</ta>
            <ta e="T432" id="Seg_6002" s="T431">EMPH</ta>
            <ta e="T433" id="Seg_6003" s="T432">chowder-3SG-DAT/LOC</ta>
            <ta e="T434" id="Seg_6004" s="T433">MOD</ta>
            <ta e="T435" id="Seg_6005" s="T434">who-ACC</ta>
            <ta e="T436" id="Seg_6006" s="T435">cut.up-PST2.[3SG]</ta>
            <ta e="T438" id="Seg_6007" s="T437">%%</ta>
            <ta e="T439" id="Seg_6008" s="T438">say-HAB-3PL</ta>
            <ta e="T440" id="Seg_6009" s="T439">then</ta>
            <ta e="T441" id="Seg_6010" s="T440">now</ta>
            <ta e="T442" id="Seg_6011" s="T441">%%</ta>
            <ta e="T443" id="Seg_6012" s="T442">fishing.line</ta>
            <ta e="T445" id="Seg_6013" s="T444">fishing.line-ACC</ta>
            <ta e="T446" id="Seg_6014" s="T445">tobacco-DAT/LOC</ta>
            <ta e="T447" id="Seg_6015" s="T446">like</ta>
            <ta e="T448" id="Seg_6016" s="T447">similar</ta>
            <ta e="T449" id="Seg_6017" s="T448">cut.up-CVB.SIM-cut.up-CVB.SIM</ta>
            <ta e="T450" id="Seg_6018" s="T449">EMPH</ta>
            <ta e="T451" id="Seg_6019" s="T450">chowder-DAT/LOC</ta>
            <ta e="T452" id="Seg_6020" s="T451">pour-PST2.[3SG]</ta>
            <ta e="T453" id="Seg_6021" s="T452">EMPH</ta>
            <ta e="T455" id="Seg_6022" s="T454">hunger-CVB.SEQ</ta>
            <ta e="T456" id="Seg_6023" s="T455">go-PTCP.PRS</ta>
            <ta e="T457" id="Seg_6024" s="T456">human.being-PL.[NOM]</ta>
            <ta e="T458" id="Seg_6025" s="T457">chowder.[NOM]</ta>
            <ta e="T459" id="Seg_6026" s="T458">drink-FUT-3PL</ta>
            <ta e="T460" id="Seg_6027" s="T459">say-CVB.SEQ</ta>
            <ta e="T461" id="Seg_6028" s="T460">that</ta>
            <ta e="T462" id="Seg_6029" s="T461">drink-FUT-3PL</ta>
            <ta e="T463" id="Seg_6030" s="T462">say-CVB.SEQ</ta>
            <ta e="T464" id="Seg_6031" s="T463">that</ta>
            <ta e="T465" id="Seg_6032" s="T464">chowder</ta>
            <ta e="T466" id="Seg_6033" s="T465">cut.up-CAUS-PST2.[3SG]</ta>
            <ta e="T467" id="Seg_6034" s="T466">EMPH</ta>
            <ta e="T469" id="Seg_6035" s="T468">so</ta>
            <ta e="T470" id="Seg_6036" s="T469">swallow-NMNZ.[NOM]</ta>
            <ta e="T471" id="Seg_6037" s="T470">want-TEMP-3SG</ta>
            <ta e="T472" id="Seg_6038" s="T471">that</ta>
            <ta e="T473" id="Seg_6039" s="T472">head-3PL-ACC</ta>
            <ta e="T474" id="Seg_6040" s="T473">throat-3SG-ACC</ta>
            <ta e="T475" id="Seg_6041" s="T474">well</ta>
            <ta e="T476" id="Seg_6042" s="T475">beat-FUT-EP-IMP.2PL</ta>
            <ta e="T477" id="Seg_6043" s="T476">say-CVB.SEQ</ta>
            <ta e="T478" id="Seg_6044" s="T477">beard-EP</ta>
            <ta e="T479" id="Seg_6045" s="T478">eh</ta>
            <ta e="T480" id="Seg_6046" s="T479">who-EP-INSTR</ta>
            <ta e="T481" id="Seg_6047" s="T480">glaive-INSTR</ta>
            <ta e="T483" id="Seg_6048" s="T482">then</ta>
            <ta e="T484" id="Seg_6049" s="T483">just</ta>
            <ta e="T485" id="Seg_6050" s="T484">win-FUT-1PL</ta>
            <ta e="T486" id="Seg_6051" s="T485">say-PST2.[3SG]</ta>
            <ta e="T487" id="Seg_6052" s="T486">EMPH</ta>
            <ta e="T488" id="Seg_6053" s="T487">then</ta>
            <ta e="T489" id="Seg_6054" s="T488">speak-PST1-3SG</ta>
            <ta e="T490" id="Seg_6055" s="T489">can-FUT-1PL</ta>
            <ta e="T491" id="Seg_6056" s="T490">how</ta>
            <ta e="T492" id="Seg_6057" s="T491">INDEF</ta>
            <ta e="T494" id="Seg_6058" s="T493">well</ta>
            <ta e="T495" id="Seg_6059" s="T494">that</ta>
            <ta e="T496" id="Seg_6060" s="T495">come-PST2-3PL</ta>
            <ta e="T497" id="Seg_6061" s="T496">that</ta>
            <ta e="T498" id="Seg_6062" s="T497">chowder-VBZ-PST2-3PL</ta>
            <ta e="T499" id="Seg_6063" s="T498">well</ta>
            <ta e="T500" id="Seg_6064" s="T499">drink-PRS-3PL</ta>
            <ta e="T501" id="Seg_6065" s="T500">chowder-VBZ-IMP.2PL</ta>
            <ta e="T502" id="Seg_6066" s="T501">say-PST2.[3SG]</ta>
            <ta e="T503" id="Seg_6067" s="T502">EMPH</ta>
            <ta e="T504" id="Seg_6068" s="T503">chowder-VBZ-PST2-3PL</ta>
            <ta e="T506" id="Seg_6069" s="T505">well</ta>
            <ta e="T507" id="Seg_6070" s="T506">that</ta>
            <ta e="T508" id="Seg_6071" s="T507">boil-EP-CAUS-CVB.SIM</ta>
            <ta e="T509" id="Seg_6072" s="T508">lie-TEMP-3PL</ta>
            <ta e="T510" id="Seg_6073" s="T509">well</ta>
            <ta e="T511" id="Seg_6074" s="T510">come-PST2-3PL</ta>
            <ta e="T512" id="Seg_6075" s="T511">EMPH</ta>
            <ta e="T513" id="Seg_6076" s="T512">that</ta>
            <ta e="T514" id="Seg_6077" s="T513">brute-PL-EP-2SG.[NOM]</ta>
            <ta e="T516" id="Seg_6078" s="T515">come-CVB.SEQ-3PL</ta>
            <ta e="T517" id="Seg_6079" s="T516">woman-PL-ACC</ta>
            <ta e="T518" id="Seg_6080" s="T517">go.in-CAUS-ITER-CVB.SEQ</ta>
            <ta e="T519" id="Seg_6081" s="T518">well</ta>
            <ta e="T520" id="Seg_6082" s="T519">EMPH</ta>
            <ta e="T521" id="Seg_6083" s="T520">old.man.[NOM]</ta>
            <ta e="T522" id="Seg_6084" s="T521">receive-PRS.[3SG]</ta>
            <ta e="T523" id="Seg_6085" s="T522">go.in-CAUS-ITER-PRS.[3SG]</ta>
            <ta e="T525" id="Seg_6086" s="T524">well</ta>
            <ta e="T526" id="Seg_6087" s="T525">such-PL.[NOM]</ta>
            <ta e="T527" id="Seg_6088" s="T526">such-PL.[NOM]</ta>
            <ta e="T528" id="Seg_6089" s="T527">news-PART</ta>
            <ta e="T529" id="Seg_6090" s="T528">tell-IMP.2PL</ta>
            <ta e="T530" id="Seg_6091" s="T529">say-CVB.SEQ</ta>
            <ta e="T532" id="Seg_6092" s="T531">well</ta>
            <ta e="T533" id="Seg_6093" s="T532">that</ta>
            <ta e="T534" id="Seg_6094" s="T533">old.man-DAT/LOC</ta>
            <ta e="T535" id="Seg_6095" s="T534">reach-CVB.SEQ</ta>
            <ta e="T536" id="Seg_6096" s="T535">tell-PRS-3PL</ta>
            <ta e="T537" id="Seg_6097" s="T536">EMPH</ta>
            <ta e="T538" id="Seg_6098" s="T537">old.woman-ACC</ta>
            <ta e="T539" id="Seg_6099" s="T538">bring-PST1-1PL</ta>
            <ta e="T540" id="Seg_6100" s="T539">1PL.[NOM]</ta>
            <ta e="T541" id="Seg_6101" s="T540">that.[NOM]</ta>
            <ta e="T542" id="Seg_6102" s="T541">earth-AG-1PL.[NOM]</ta>
            <ta e="T543" id="Seg_6103" s="T542">say-PRS-1PL</ta>
            <ta e="T545" id="Seg_6104" s="T544">shaman.[NOM]</ta>
            <ta e="T546" id="Seg_6105" s="T545">old.woman.[NOM]</ta>
            <ta e="T547" id="Seg_6106" s="T546">that.[NOM]</ta>
            <ta e="T548" id="Seg_6107" s="T547">cook-PL-1PL.[NOM]</ta>
            <ta e="T550" id="Seg_6108" s="T549">then</ta>
            <ta e="T551" id="Seg_6109" s="T550">that</ta>
            <ta e="T552" id="Seg_6110" s="T551">shaman.[NOM]</ta>
            <ta e="T553" id="Seg_6111" s="T552">old.woman.[NOM]</ta>
            <ta e="T554" id="Seg_6112" s="T553">earth-VBZ-CVB.SEQ</ta>
            <ta e="T555" id="Seg_6113" s="T554">bring-PST1-3SG</ta>
            <ta e="T556" id="Seg_6114" s="T555">1PL-DAT/LOC</ta>
            <ta e="T557" id="Seg_6115" s="T556">2PL-DAT/LOC</ta>
            <ta e="T558" id="Seg_6116" s="T557">say-CVB.SEQ</ta>
            <ta e="T559" id="Seg_6117" s="T558">that</ta>
            <ta e="T560" id="Seg_6118" s="T559">tell-PRS-3PL</ta>
            <ta e="T561" id="Seg_6119" s="T560">EMPH</ta>
            <ta e="T562" id="Seg_6120" s="T561">human.being-ACC</ta>
            <ta e="T563" id="Seg_6121" s="T562">defeat-EP-CAUS-PST2-3PL</ta>
            <ta e="T565" id="Seg_6122" s="T564">blood-3PL-ACC</ta>
            <ta e="T566" id="Seg_6123" s="T565">MOD</ta>
            <ta e="T567" id="Seg_6124" s="T566">knife-3PL-ACC</ta>
            <ta e="T568" id="Seg_6125" s="T567">here</ta>
            <ta e="T569" id="Seg_6126" s="T568">wipe-PRS-3PL</ta>
            <ta e="T570" id="Seg_6127" s="T569">be-PST2.[3SG]</ta>
            <ta e="T571" id="Seg_6128" s="T570">human.being-PL-ACC</ta>
            <ta e="T572" id="Seg_6129" s="T571">cut.up-CVB.SIM</ta>
            <ta e="T573" id="Seg_6130" s="T572">cut.up-CVB.SIM</ta>
            <ta e="T574" id="Seg_6131" s="T573">eat-CVB.SIM</ta>
            <ta e="T575" id="Seg_6132" s="T574">eat-CVB.SIM</ta>
            <ta e="T576" id="Seg_6133" s="T575">eat-PRS-3PL</ta>
            <ta e="T578" id="Seg_6134" s="T577">oh</ta>
            <ta e="T579" id="Seg_6135" s="T578">what.[NOM]</ta>
            <ta e="T580" id="Seg_6136" s="T579">name-PROPR</ta>
            <ta e="T581" id="Seg_6137" s="T580">blood-3SG-DAT/LOC</ta>
            <ta e="T582" id="Seg_6138" s="T581">make.dirty-EP-PASS/REFL-EP-PTCP.PST</ta>
            <ta e="T583" id="Seg_6139" s="T582">people-PL-2PL=Q</ta>
            <ta e="T584" id="Seg_6140" s="T583">well</ta>
            <ta e="T585" id="Seg_6141" s="T584">eat-IMP.2PL</ta>
            <ta e="T586" id="Seg_6142" s="T585">say-CVB.SEQ</ta>
            <ta e="T587" id="Seg_6143" s="T586">old.man.[NOM]</ta>
            <ta e="T588" id="Seg_6144" s="T587">well</ta>
            <ta e="T589" id="Seg_6145" s="T588">eat-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T591" id="Seg_6146" s="T590">well</ta>
            <ta e="T592" id="Seg_6147" s="T591">that</ta>
            <ta e="T593" id="Seg_6148" s="T592">eat</ta>
            <ta e="T594" id="Seg_6149" s="T593">eat-EP-CAUS-PTCP.PST-3SG-ACC</ta>
            <ta e="T595" id="Seg_6150" s="T594">after</ta>
            <ta e="T596" id="Seg_6151" s="T595">well</ta>
            <ta e="T597" id="Seg_6152" s="T596">that</ta>
            <ta e="T598" id="Seg_6153" s="T597">oh</ta>
            <ta e="T599" id="Seg_6154" s="T598">sky-1PL.[NOM]</ta>
            <ta e="T600" id="Seg_6155" s="T599">how</ta>
            <ta e="T601" id="Seg_6156" s="T600">how</ta>
            <ta e="T602" id="Seg_6157" s="T601">be-PRS.[3SG]</ta>
            <ta e="T603" id="Seg_6158" s="T602">well</ta>
            <ta e="T604" id="Seg_6159" s="T603">cloud-1PL.[NOM]</ta>
            <ta e="T605" id="Seg_6160" s="T604">add-PRS.[3SG]</ta>
            <ta e="T607" id="Seg_6161" s="T606">what</ta>
            <ta e="T608" id="Seg_6162" s="T607">be-PRS.[3SG]</ta>
            <ta e="T609" id="Seg_6163" s="T608">say-PST2-3SG</ta>
            <ta e="T611" id="Seg_6164" s="T610">well</ta>
            <ta e="T612" id="Seg_6165" s="T611">that</ta>
            <ta e="T613" id="Seg_6166" s="T612">two</ta>
            <ta e="T614" id="Seg_6167" s="T613">alert-PROPR-ACC</ta>
            <ta e="T615" id="Seg_6168" s="T614">two</ta>
            <ta e="T616" id="Seg_6169" s="T615">side-3SG-DAT/LOC</ta>
            <ta e="T617" id="Seg_6170" s="T616">seat-PST2.[3SG]</ta>
            <ta e="T618" id="Seg_6171" s="T617">EMPH</ta>
            <ta e="T620" id="Seg_6172" s="T619">well</ta>
            <ta e="T621" id="Seg_6173" s="T620">that</ta>
            <ta e="T622" id="Seg_6174" s="T621">swallow-NMNZ.[NOM]</ta>
            <ta e="T623" id="Seg_6175" s="T622">make-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T624" id="Seg_6176" s="T623">throat-3SG-ACC</ta>
            <ta e="T625" id="Seg_6177" s="T624">split-CVB.SIM</ta>
            <ta e="T626" id="Seg_6178" s="T625">beat-EP-PST2-3PL</ta>
            <ta e="T627" id="Seg_6179" s="T626">glaive-INSTR</ta>
            <ta e="T629" id="Seg_6180" s="T628">now</ta>
            <ta e="T630" id="Seg_6181" s="T629">that.[NOM]</ta>
            <ta e="T631" id="Seg_6182" s="T630">fight-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T632" id="Seg_6183" s="T631">say-PRS.[3SG]</ta>
            <ta e="T633" id="Seg_6184" s="T632">cinema-PL-DAT/LOC</ta>
            <ta e="T634" id="Seg_6185" s="T633">like</ta>
            <ta e="T636" id="Seg_6186" s="T635">that</ta>
            <ta e="T637" id="Seg_6187" s="T636">win-PST2-3PL</ta>
            <ta e="T638" id="Seg_6188" s="T637">it.is.said</ta>
            <ta e="T639" id="Seg_6189" s="T638">that-3SG-3PL-ACC</ta>
            <ta e="T641" id="Seg_6190" s="T640">well</ta>
            <ta e="T642" id="Seg_6191" s="T641">other.of.two-3PL-ACC</ta>
            <ta e="T643" id="Seg_6192" s="T642">kill-ITER-PST2-3PL</ta>
            <ta e="T645" id="Seg_6193" s="T644">seven</ta>
            <ta e="T646" id="Seg_6194" s="T645">human.being-3SG.[NOM]</ta>
            <ta e="T647" id="Seg_6195" s="T646">be-PST2.[3SG]</ta>
            <ta e="T648" id="Seg_6196" s="T647">Q</ta>
            <ta e="T649" id="Seg_6197" s="T648">how.much</ta>
            <ta e="T650" id="Seg_6198" s="T649">Q</ta>
            <ta e="T652" id="Seg_6199" s="T651">well</ta>
            <ta e="T653" id="Seg_6200" s="T652">then</ta>
            <ta e="T654" id="Seg_6201" s="T653">woman-PL.[NOM]</ta>
            <ta e="T655" id="Seg_6202" s="T654">stay-PTCP.PST-3PL-DAT/LOC</ta>
            <ta e="T656" id="Seg_6203" s="T655">this</ta>
            <ta e="T657" id="Seg_6204" s="T656">woman-PL.[NOM]</ta>
            <ta e="T658" id="Seg_6205" s="T657">well</ta>
            <ta e="T659" id="Seg_6206" s="T658">quarrel-PRS-3PL</ta>
            <ta e="T660" id="Seg_6207" s="T659">it.is.said</ta>
            <ta e="T661" id="Seg_6208" s="T660">knife-3PL-ACC</ta>
            <ta e="T662" id="Seg_6209" s="T661">take.away-EP-RECP/COLL-CVB.SEQ-3PL</ta>
            <ta e="T664" id="Seg_6210" s="T663">1SG.[NOM]</ta>
            <ta e="T665" id="Seg_6211" s="T664">man-EP-1SG.[NOM]</ta>
            <ta e="T666" id="Seg_6212" s="T665">knife-3SG.[NOM]</ta>
            <ta e="T667" id="Seg_6213" s="T666">1SG.[NOM]</ta>
            <ta e="T668" id="Seg_6214" s="T667">man-EP-1SG.[NOM]</ta>
            <ta e="T669" id="Seg_6215" s="T668">knife-3SG.[NOM]</ta>
            <ta e="T670" id="Seg_6216" s="T669">say-EP-RECP/COLL-EMOT-PRS-3PL</ta>
            <ta e="T671" id="Seg_6217" s="T670">it.is.said</ta>
            <ta e="T672" id="Seg_6218" s="T671">poor.one-PL.[NOM]</ta>
            <ta e="T674" id="Seg_6219" s="T673">what</ta>
            <ta e="T675" id="Seg_6220" s="T674">bitch</ta>
            <ta e="T676" id="Seg_6221" s="T675">girl-3PL.[NOM]=Q</ta>
            <ta e="T677" id="Seg_6222" s="T676">that-PL-ACC</ta>
            <ta e="T678" id="Seg_6223" s="T677">die-MULT-CVB.SEQ</ta>
            <ta e="T679" id="Seg_6224" s="T678">throw-EP-IMP.2PL</ta>
            <ta e="T680" id="Seg_6225" s="T679">say-PST2.[3SG]</ta>
            <ta e="T681" id="Seg_6226" s="T680">EMPH</ta>
            <ta e="T682" id="Seg_6227" s="T681">what.[NOM]</ta>
            <ta e="T683" id="Seg_6228" s="T682">cruel.[NOM]</ta>
            <ta e="T684" id="Seg_6229" s="T683">old.man-3SG.[NOM]</ta>
            <ta e="T685" id="Seg_6230" s="T684">MOD</ta>
            <ta e="T686" id="Seg_6231" s="T685">EMPH</ta>
            <ta e="T687" id="Seg_6232" s="T686">our</ta>
            <ta e="T688" id="Seg_6233" s="T687">Porotov.[NOM]</ta>
            <ta e="T690" id="Seg_6234" s="T689">die-MULT-CVB.SEQ</ta>
            <ta e="T691" id="Seg_6235" s="T690">throw-PST2-3PL</ta>
            <ta e="T692" id="Seg_6236" s="T691">misery-PROPR-PL.[NOM]</ta>
            <ta e="T694" id="Seg_6237" s="T693">then</ta>
            <ta e="T695" id="Seg_6238" s="T694">that</ta>
            <ta e="T696" id="Seg_6239" s="T695">old.woman-ACC</ta>
            <ta e="T697" id="Seg_6240" s="T696">well</ta>
            <ta e="T698" id="Seg_6241" s="T697">kill-CVB.SIM</ta>
            <ta e="T699" id="Seg_6242" s="T698">try-PRS-3PL</ta>
            <ta e="T700" id="Seg_6243" s="T699">shaman.[NOM]</ta>
            <ta e="T701" id="Seg_6244" s="T700">old.woman-ACC</ta>
            <ta e="T703" id="Seg_6245" s="T702">water-DAT/LOC</ta>
            <ta e="T704" id="Seg_6246" s="T703">EMPH</ta>
            <ta e="T705" id="Seg_6247" s="T704">sink-CAUS-PRS-3PL</ta>
            <ta e="T706" id="Seg_6248" s="T705">well</ta>
            <ta e="T707" id="Seg_6249" s="T706">anchor-VBZ-CVB.SEQ</ta>
            <ta e="T708" id="Seg_6250" s="T707">after</ta>
            <ta e="T710" id="Seg_6251" s="T709">at.all</ta>
            <ta e="T711" id="Seg_6252" s="T710">sink-NEG.[3SG]</ta>
            <ta e="T713" id="Seg_6253" s="T712">try.ones.best-CVB.SIM</ta>
            <ta e="T714" id="Seg_6254" s="T713">try-CVB.SEQ</ta>
            <ta e="T715" id="Seg_6255" s="T714">fire-3SG-ACC</ta>
            <ta e="T716" id="Seg_6256" s="T715">light-CVB.SEQ</ta>
            <ta e="T717" id="Seg_6257" s="T716">EMPH</ta>
            <ta e="T718" id="Seg_6258" s="T717">fire.[NOM]</ta>
            <ta e="T719" id="Seg_6259" s="T718">inside-3SG-DAT/LOC</ta>
            <ta e="T720" id="Seg_6260" s="T719">throw-PRS-3PL</ta>
            <ta e="T722" id="Seg_6261" s="T721">fire-3PL.[NOM]</ta>
            <ta e="T723" id="Seg_6262" s="T722">be.black-INCH</ta>
            <ta e="T724" id="Seg_6263" s="T723">sleep-PRS.[3SG]</ta>
            <ta e="T725" id="Seg_6264" s="T724">it.is.said</ta>
            <ta e="T727" id="Seg_6265" s="T726">there</ta>
            <ta e="T728" id="Seg_6266" s="T727">EMPH</ta>
            <ta e="T729" id="Seg_6267" s="T728">say-PST2.[3SG]</ta>
            <ta e="T730" id="Seg_6268" s="T729">EMPH</ta>
            <ta e="T731" id="Seg_6269" s="T730">dear-PL.[NOM]</ta>
            <ta e="T732" id="Seg_6270" s="T731">1SG.[NOM]</ta>
            <ta e="T733" id="Seg_6271" s="T732">sun.[NOM]</ta>
            <ta e="T734" id="Seg_6272" s="T733">old.woman.[NOM]</ta>
            <ta e="T735" id="Seg_6273" s="T734">be-PST1-1SG</ta>
            <ta e="T736" id="Seg_6274" s="T735">well</ta>
            <ta e="T737" id="Seg_6275" s="T736">evil.spirit.[NOM]</ta>
            <ta e="T738" id="Seg_6276" s="T737">be-PST2.NEG-1SG</ta>
            <ta e="T740" id="Seg_6277" s="T739">people-1SG-ACC</ta>
            <ta e="T741" id="Seg_6278" s="T740">defeat-PST2-3PL</ta>
            <ta e="T742" id="Seg_6279" s="T741">this</ta>
            <ta e="T743" id="Seg_6280" s="T742">MOD</ta>
            <ta e="T744" id="Seg_6281" s="T743">human.being-ACC</ta>
            <ta e="T745" id="Seg_6282" s="T744">animal-ACC</ta>
            <ta e="T746" id="Seg_6283" s="T745">see-CVB.PURP-1SG</ta>
            <ta e="T747" id="Seg_6284" s="T746">earth-VBZ-CVB.SEQ</ta>
            <ta e="T748" id="Seg_6285" s="T747">come-PST2-EP-1SG</ta>
            <ta e="T750" id="Seg_6286" s="T749">1SG-ACC</ta>
            <ta e="T751" id="Seg_6287" s="T750">pain-VBZ-NEG-IMP.2PL</ta>
            <ta e="T752" id="Seg_6288" s="T751">1SG.[NOM]</ta>
            <ta e="T753" id="Seg_6289" s="T752">such</ta>
            <ta e="T754" id="Seg_6290" s="T753">well</ta>
            <ta e="T755" id="Seg_6291" s="T754">die-FUT-1SG</ta>
            <ta e="T756" id="Seg_6292" s="T755">NEG-3SG</ta>
            <ta e="T757" id="Seg_6293" s="T756">say-PRS.[3SG]</ta>
            <ta e="T759" id="Seg_6294" s="T758">girl.[NOM]</ta>
            <ta e="T760" id="Seg_6295" s="T759">child-3SG.[NOM]</ta>
            <ta e="T761" id="Seg_6296" s="T760">for.the.first.time</ta>
            <ta e="T762" id="Seg_6297" s="T761">dirty.[NOM]</ta>
            <ta e="T763" id="Seg_6298" s="T762">become-PST2.[3SG]</ta>
            <ta e="T764" id="Seg_6299" s="T763">girl.[NOM]</ta>
            <ta e="T765" id="Seg_6300" s="T764">child-3SG.[NOM]</ta>
            <ta e="T766" id="Seg_6301" s="T765">neck-1SG-INSTR</ta>
            <ta e="T767" id="Seg_6302" s="T766">step.over-EP-IMP.3SG</ta>
            <ta e="T768" id="Seg_6303" s="T767">then</ta>
            <ta e="T769" id="Seg_6304" s="T768">die-FUT-1SG</ta>
            <ta e="T770" id="Seg_6305" s="T769">say-PRS.[3SG]</ta>
            <ta e="T771" id="Seg_6306" s="T770">that</ta>
            <ta e="T772" id="Seg_6307" s="T771">neck-1SG-INSTR</ta>
            <ta e="T773" id="Seg_6308" s="T772">step.over-EP-CAUS-PST2-3PL</ta>
            <ta e="T775" id="Seg_6309" s="T774">well</ta>
            <ta e="T776" id="Seg_6310" s="T775">that</ta>
            <ta e="T777" id="Seg_6311" s="T776">old.woman-3PL.[NOM]</ta>
            <ta e="T778" id="Seg_6312" s="T777">die-CVB.SEQ</ta>
            <ta e="T779" id="Seg_6313" s="T778">stay-PST2.[3SG]</ta>
            <ta e="T780" id="Seg_6314" s="T779">well</ta>
            <ta e="T781" id="Seg_6315" s="T780">last-3SG.[NOM]</ta>
            <ta e="T782" id="Seg_6316" s="T781">that</ta>
            <ta e="T783" id="Seg_6317" s="T782">tell-PST2-3PL</ta>
            <ta e="T784" id="Seg_6318" s="T783">remember-NEG-1SG</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T2" id="Seg_6319" s="T1">dann</ta>
            <ta e="T3" id="Seg_6320" s="T2">einmal</ta>
            <ta e="T4" id="Seg_6321" s="T3">jagen-CVB.SIM</ta>
            <ta e="T5" id="Seg_6322" s="T4">hinausgehen-EP-PST2-3PL</ta>
            <ta e="T6" id="Seg_6323" s="T5">alter.Mann-PL.[NOM]</ta>
            <ta e="T8" id="Seg_6324" s="T7">alter.Mann-PL.[NOM]</ta>
            <ta e="T9" id="Seg_6325" s="T8">kommen-PRS-3PL</ta>
            <ta e="T10" id="Seg_6326" s="T9">Junge-PL.[NOM]</ta>
            <ta e="T12" id="Seg_6327" s="T11">na</ta>
            <ta e="T13" id="Seg_6328" s="T12">Syndassko-1PL-DAT/LOC</ta>
            <ta e="T14" id="Seg_6329" s="T13">Haus.[NOM]</ta>
            <ta e="T15" id="Seg_6330" s="T14">aufstehen-PST2.[3SG]</ta>
            <ta e="T16" id="Seg_6331" s="T15">sagen-PST2-3PL</ta>
            <ta e="T18" id="Seg_6332" s="T17">wohin</ta>
            <ta e="T19" id="Seg_6333" s="T18">jagen-HAB-1PL</ta>
            <ta e="T21" id="Seg_6334" s="T20">oh</ta>
            <ta e="T22" id="Seg_6335" s="T21">doch</ta>
            <ta e="T23" id="Seg_6336" s="T22">dieses</ta>
            <ta e="T24" id="Seg_6337" s="T23">Haus.[NOM]</ta>
            <ta e="T25" id="Seg_6338" s="T24">aufstehen-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T26" id="Seg_6339" s="T25">Angst.haben-PRS-3PL</ta>
            <ta e="T27" id="Seg_6340" s="T26">kommen-PTCP.FUT-3PL-ACC</ta>
            <ta e="T28" id="Seg_6341" s="T27">man.sagt</ta>
            <ta e="T29" id="Seg_6342" s="T28">Russe-PL.[NOM]</ta>
            <ta e="T30" id="Seg_6343" s="T29">es.gibt-3PL</ta>
            <ta e="T31" id="Seg_6344" s="T30">sagen-CVB.SEQ</ta>
            <ta e="T33" id="Seg_6345" s="T32">oh</ta>
            <ta e="T34" id="Seg_6346" s="T33">doch</ta>
            <ta e="T35" id="Seg_6347" s="T34">dieses</ta>
            <ta e="T36" id="Seg_6348" s="T35">Erde-1PL-ACC</ta>
            <ta e="T37" id="Seg_6349" s="T36">Fang-POSS</ta>
            <ta e="T38" id="Seg_6350" s="T37">NEG</ta>
            <ta e="T39" id="Seg_6351" s="T38">machen-PST1-3PL</ta>
            <ta e="T40" id="Seg_6352" s="T39">wie</ta>
            <ta e="T41" id="Seg_6353" s="T40">sein-HAB-1PL=Q</ta>
            <ta e="T42" id="Seg_6354" s="T41">sagen-PRS-3PL</ta>
            <ta e="T43" id="Seg_6355" s="T42">man.sagt</ta>
            <ta e="T44" id="Seg_6356" s="T43">jenes</ta>
            <ta e="T45" id="Seg_6357" s="T44">alter.Mann-PL.[NOM]</ta>
            <ta e="T46" id="Seg_6358" s="T45">nun</ta>
            <ta e="T48" id="Seg_6359" s="T47">sagen-EP-RECP/COLL-PRS-1PL</ta>
            <ta e="T49" id="Seg_6360" s="T48">sagen-PRS-3PL</ta>
            <ta e="T51" id="Seg_6361" s="T50">sich.unterhalten-PTCP.PRS-3PL-ACC</ta>
            <ta e="T52" id="Seg_6362" s="T51">hören-PRS-1SG</ta>
            <ta e="T53" id="Seg_6363" s="T52">Großvater-PL-EP-1SG.[NOM]</ta>
            <ta e="T54" id="Seg_6364" s="T53">nun</ta>
            <ta e="T56" id="Seg_6365" s="T55">dann</ta>
            <ta e="T57" id="Seg_6366" s="T56">doch</ta>
            <ta e="T58" id="Seg_6367" s="T57">dann</ta>
            <ta e="T59" id="Seg_6368" s="T58">kommen-PRS-3PL</ta>
            <ta e="T61" id="Seg_6369" s="T60">kommen-CVB.SEQ-3PL</ta>
            <ta e="T62" id="Seg_6370" s="T61">sehen-PST2-3PL</ta>
            <ta e="T63" id="Seg_6371" s="T62">zwei</ta>
            <ta e="T64" id="Seg_6372" s="T63">Mensch.[NOM]</ta>
            <ta e="T65" id="Seg_6373" s="T64">gehen-PRS-3PL</ta>
            <ta e="T66" id="Seg_6374" s="T65">man.sagt</ta>
            <ta e="T68" id="Seg_6375" s="T67">Frau-3SG-GEN</ta>
            <ta e="T69" id="Seg_6376" s="T68">Name-3SG.[NOM]</ta>
            <ta e="T70" id="Seg_6377" s="T69">Darja.[NOM]</ta>
            <ta e="T72" id="Seg_6378" s="T71">Mann-3SG-ACC</ta>
            <ta e="T73" id="Seg_6379" s="T72">Name-3SG.[NOM]</ta>
            <ta e="T74" id="Seg_6380" s="T73">wer.[NOM]</ta>
            <ta e="T75" id="Seg_6381" s="T74">sagen-PST2-3PL=Q</ta>
            <ta e="T76" id="Seg_6382" s="T75">nun</ta>
            <ta e="T77" id="Seg_6383" s="T76">EMPH</ta>
            <ta e="T78" id="Seg_6384" s="T77">%%</ta>
            <ta e="T79" id="Seg_6385" s="T78">sehr</ta>
            <ta e="T80" id="Seg_6386" s="T79">Name-PROPR</ta>
            <ta e="T81" id="Seg_6387" s="T80">Mensch.[NOM]</ta>
            <ta e="T82" id="Seg_6388" s="T81">wer.[NOM]</ta>
            <ta e="T83" id="Seg_6389" s="T82">INDEF</ta>
            <ta e="T84" id="Seg_6390" s="T83">sagen-HAB-3PL</ta>
            <ta e="T85" id="Seg_6391" s="T84">EVID</ta>
            <ta e="T87" id="Seg_6392" s="T86">ganz</ta>
            <ta e="T88" id="Seg_6393" s="T87">erinnern-NEG-1SG</ta>
            <ta e="T89" id="Seg_6394" s="T88">Name-3SG-ACC</ta>
            <ta e="T91" id="Seg_6395" s="T90">jenes-DAT/LOC</ta>
            <ta e="T92" id="Seg_6396" s="T91">EMPH</ta>
            <ta e="T93" id="Seg_6397" s="T92">sich.nähern-PRS-3PL</ta>
            <ta e="T94" id="Seg_6398" s="T93">man.sagt</ta>
            <ta e="T95" id="Seg_6399" s="T94">Angst.haben-CVB.SIM</ta>
            <ta e="T96" id="Seg_6400" s="T95">Angst.haben-CVB.SIM</ta>
            <ta e="T97" id="Seg_6401" s="T96">sich.nähern-PRS-3PL</ta>
            <ta e="T98" id="Seg_6402" s="T97">man.sagt</ta>
            <ta e="T100" id="Seg_6403" s="T99">dieses</ta>
            <ta e="T101" id="Seg_6404" s="T100">warum</ta>
            <ta e="T102" id="Seg_6405" s="T101">kommen-PST2-2PL=Q</ta>
            <ta e="T103" id="Seg_6406" s="T102">1PL.[NOM]</ta>
            <ta e="T104" id="Seg_6407" s="T103">Erde-1PL-DAT/LOC</ta>
            <ta e="T106" id="Seg_6408" s="T105">warum</ta>
            <ta e="T107" id="Seg_6409" s="T106">finden.[MED]-PST1-2PL</ta>
            <ta e="T109" id="Seg_6410" s="T108">jagen-PTCP.PRS</ta>
            <ta e="T110" id="Seg_6411" s="T109">Erde-1PL-ACC</ta>
            <ta e="T111" id="Seg_6412" s="T110">warum</ta>
            <ta e="T112" id="Seg_6413" s="T111">zerbrechen-PRS-2PL</ta>
            <ta e="T113" id="Seg_6414" s="T112">sagen-PRS-3PL</ta>
            <ta e="T114" id="Seg_6415" s="T113">man.sagt</ta>
            <ta e="T115" id="Seg_6416" s="T114">alter.Mann-PL.[NOM]</ta>
            <ta e="T117" id="Seg_6417" s="T116">na</ta>
            <ta e="T118" id="Seg_6418" s="T117">hier</ta>
            <ta e="T119" id="Seg_6419" s="T118">aber</ta>
            <ta e="T120" id="Seg_6420" s="T119">Bevölkerung.[NOM]</ta>
            <ta e="T121" id="Seg_6421" s="T120">machen-FUT-3PL</ta>
            <ta e="T122" id="Seg_6422" s="T121">Mensch.[NOM]</ta>
            <ta e="T123" id="Seg_6423" s="T122">aufstehen-FUT-3SG</ta>
            <ta e="T125" id="Seg_6424" s="T124">Mensch.[NOM]</ta>
            <ta e="T126" id="Seg_6425" s="T125">geboren.werden-FUT-3SG</ta>
            <ta e="T127" id="Seg_6426" s="T126">hier</ta>
            <ta e="T128" id="Seg_6427" s="T127">viel</ta>
            <ta e="T129" id="Seg_6428" s="T128">Mensch.[NOM]</ta>
            <ta e="T130" id="Seg_6429" s="T129">leben-FUT-3SG</ta>
            <ta e="T132" id="Seg_6430" s="T131">dann</ta>
            <ta e="T133" id="Seg_6431" s="T132">Anlegestelle-1PL.[NOM]</ta>
            <ta e="T134" id="Seg_6432" s="T133">EMPH</ta>
            <ta e="T135" id="Seg_6433" s="T134">dieses.[NOM]</ta>
            <ta e="T136" id="Seg_6434" s="T135">es.gibt</ta>
            <ta e="T137" id="Seg_6435" s="T136">%%-CVB.SEQ</ta>
            <ta e="T138" id="Seg_6436" s="T137">hineingehen-PST2.[3SG]</ta>
            <ta e="T140" id="Seg_6437" s="T139">warum</ta>
            <ta e="T141" id="Seg_6438" s="T140">fern-3SG.[NOM]=Q</ta>
            <ta e="T142" id="Seg_6439" s="T141">sagen-PRS-3PL</ta>
            <ta e="T143" id="Seg_6440" s="T142">man.sagt</ta>
            <ta e="T144" id="Seg_6441" s="T143">Anlegestelle-1PL.[NOM]</ta>
            <ta e="T146" id="Seg_6442" s="T145">Fahrrinne-2PL.[NOM]</ta>
            <ta e="T147" id="Seg_6443" s="T146">klein.[NOM]</ta>
            <ta e="T148" id="Seg_6444" s="T147">hier</ta>
            <ta e="T149" id="Seg_6445" s="T148">jenes.[NOM]</ta>
            <ta e="T150" id="Seg_6446" s="T149">wegen</ta>
            <ta e="T151" id="Seg_6447" s="T150">dort</ta>
            <ta e="T152" id="Seg_6448" s="T151">sein-PRS.[3SG]</ta>
            <ta e="T153" id="Seg_6449" s="T152">Fahrrinne-2PL.[NOM]</ta>
            <ta e="T155" id="Seg_6450" s="T154">dorthin</ta>
            <ta e="T156" id="Seg_6451" s="T155">Dampfer-PL.[NOM]</ta>
            <ta e="T157" id="Seg_6452" s="T156">kommen-FUT-3PL</ta>
            <ta e="T159" id="Seg_6453" s="T158">sich.wundern-PRS-3PL</ta>
            <ta e="T160" id="Seg_6454" s="T159">man.sagt</ta>
            <ta e="T161" id="Seg_6455" s="T160">was.[NOM]</ta>
            <ta e="T162" id="Seg_6456" s="T161">Name-3SG.[NOM]=Q</ta>
            <ta e="T163" id="Seg_6457" s="T162">Dampfer.[NOM]</ta>
            <ta e="T164" id="Seg_6458" s="T163">denken-PRS.[3SG]</ta>
            <ta e="T167" id="Seg_6459" s="T166">ziehen-PTCP.PRS</ta>
            <ta e="T168" id="Seg_6460" s="T167">kommen-PRS.[3SG]</ta>
            <ta e="T169" id="Seg_6461" s="T168">Dampfer.[NOM]</ta>
            <ta e="T171" id="Seg_6462" s="T170">doch</ta>
            <ta e="T172" id="Seg_6463" s="T171">jenes</ta>
            <ta e="T173" id="Seg_6464" s="T172">später</ta>
            <ta e="T174" id="Seg_6465" s="T173">später</ta>
            <ta e="T175" id="Seg_6466" s="T174">später</ta>
            <ta e="T176" id="Seg_6467" s="T175">später</ta>
            <ta e="T177" id="Seg_6468" s="T176">jenes</ta>
            <ta e="T178" id="Seg_6469" s="T177">entstehen-CVB.SEQ</ta>
            <ta e="T179" id="Seg_6470" s="T178">entstehen-CVB.SEQ</ta>
            <ta e="T180" id="Seg_6471" s="T179">dieses</ta>
            <ta e="T181" id="Seg_6472" s="T180">Syndassko-2SG.[NOM]</ta>
            <ta e="T182" id="Seg_6473" s="T181">werden-CVB.SEQ</ta>
            <ta e="T183" id="Seg_6474" s="T182">bleiben-PST2.[3SG]</ta>
            <ta e="T184" id="Seg_6475" s="T183">EMPH</ta>
            <ta e="T186" id="Seg_6476" s="T185">Schule-POSS</ta>
            <ta e="T187" id="Seg_6477" s="T186">NEG-3PL</ta>
            <ta e="T188" id="Seg_6478" s="T187">Stange.[NOM]</ta>
            <ta e="T189" id="Seg_6479" s="T188">Zelt-DAT/LOC</ta>
            <ta e="T190" id="Seg_6480" s="T189">lehren-PRS-3PL</ta>
            <ta e="T191" id="Seg_6481" s="T190">selbst-3PL.[NOM]</ta>
            <ta e="T192" id="Seg_6482" s="T191">bauen-CVB.SEQ-3PL</ta>
            <ta e="T194" id="Seg_6483" s="T193">Balok-PL-DAT/LOC</ta>
            <ta e="T196" id="Seg_6484" s="T195">verkaufen-PRS-3PL</ta>
            <ta e="T198" id="Seg_6485" s="T197">dann</ta>
            <ta e="T199" id="Seg_6486" s="T198">bauen-PTCP.PST</ta>
            <ta e="T200" id="Seg_6487" s="T199">Haus-3PL.[NOM]</ta>
            <ta e="T201" id="Seg_6488" s="T200">es.gibt</ta>
            <ta e="T202" id="Seg_6489" s="T201">sein-PST1-3SG</ta>
            <ta e="T203" id="Seg_6490" s="T202">damalig</ta>
            <ta e="T204" id="Seg_6491" s="T203">jetzt</ta>
            <ta e="T205" id="Seg_6492" s="T204">krachen-PST2-3SG</ta>
            <ta e="T206" id="Seg_6493" s="T205">EMPH</ta>
            <ta e="T207" id="Seg_6494" s="T206">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T208" id="Seg_6495" s="T207">Laden-3PL.[NOM]</ta>
            <ta e="T210" id="Seg_6496" s="T209">jenes.[NOM]</ta>
            <ta e="T211" id="Seg_6497" s="T210">wegen</ta>
            <ta e="T212" id="Seg_6498" s="T211">Syndassko.[NOM]</ta>
            <ta e="T213" id="Seg_6499" s="T212">Syndassko.[NOM]</ta>
            <ta e="T214" id="Seg_6500" s="T213">Darja.[NOM]</ta>
            <ta e="T215" id="Seg_6501" s="T214">was-ACC</ta>
            <ta e="T216" id="Seg_6502" s="T215">wer-ACC</ta>
            <ta e="T217" id="Seg_6503" s="T216">Darja.[NOM]</ta>
            <ta e="T218" id="Seg_6504" s="T217">sagen-PRS-3PL</ta>
            <ta e="T219" id="Seg_6505" s="T218">EVID</ta>
            <ta e="T220" id="Seg_6506" s="T219">dieses.[NOM]</ta>
            <ta e="T221" id="Seg_6507" s="T220">anders</ta>
            <ta e="T222" id="Seg_6508" s="T221">nennen-PRS-3PL</ta>
            <ta e="T223" id="Seg_6509" s="T222">man.sagt</ta>
            <ta e="T225" id="Seg_6510" s="T224">AFFIRM</ta>
            <ta e="T226" id="Seg_6511" s="T225">dieses</ta>
            <ta e="T227" id="Seg_6512" s="T226">Syndassko-ACC</ta>
            <ta e="T229" id="Seg_6513" s="T228">dieses.[NOM]</ta>
            <ta e="T230" id="Seg_6514" s="T229">jagen-PTCP.PRS</ta>
            <ta e="T231" id="Seg_6515" s="T230">Ort-3PL-ACC</ta>
            <ta e="T232" id="Seg_6516" s="T231">wegnehmen-PST2-3PL</ta>
            <ta e="T233" id="Seg_6517" s="T232">dann</ta>
            <ta e="T234" id="Seg_6518" s="T233">später</ta>
            <ta e="T235" id="Seg_6519" s="T234">später</ta>
            <ta e="T236" id="Seg_6520" s="T235">EVID</ta>
            <ta e="T237" id="Seg_6521" s="T236">dieses.[NOM]</ta>
            <ta e="T238" id="Seg_6522" s="T237">sein-PST2-3PL</ta>
            <ta e="T240" id="Seg_6523" s="T239">dann</ta>
            <ta e="T241" id="Seg_6524" s="T240">später</ta>
            <ta e="T242" id="Seg_6525" s="T241">dieses.[NOM]</ta>
            <ta e="T243" id="Seg_6526" s="T242">dieses</ta>
            <ta e="T244" id="Seg_6527" s="T243">kommen-CVB.SIM</ta>
            <ta e="T245" id="Seg_6528" s="T244">noch.nicht-3SG</ta>
            <ta e="T246" id="Seg_6529" s="T245">Lygyj-PL.[NOM]</ta>
            <ta e="T247" id="Seg_6530" s="T246">sagen-CVB.SEQ</ta>
            <ta e="T248" id="Seg_6531" s="T247">es.gibt</ta>
            <ta e="T249" id="Seg_6532" s="T248">sein-PST1-3PL</ta>
            <ta e="T250" id="Seg_6533" s="T249">man.sagt</ta>
            <ta e="T252" id="Seg_6534" s="T251">äh</ta>
            <ta e="T253" id="Seg_6535" s="T252">wer-PL.[NOM]</ta>
            <ta e="T254" id="Seg_6536" s="T253">sewn-PL.[NOM]</ta>
            <ta e="T255" id="Seg_6537" s="T254">sagen-PRS-3PL</ta>
            <ta e="T256" id="Seg_6538" s="T255">jenes</ta>
            <ta e="T258" id="Seg_6539" s="T257">dort</ta>
            <ta e="T259" id="Seg_6540" s="T258">dieses.[NOM]</ta>
            <ta e="T260" id="Seg_6541" s="T259">Anlegestelle.[NOM]</ta>
            <ta e="T261" id="Seg_6542" s="T260">entfernt</ta>
            <ta e="T262" id="Seg_6543" s="T261">Seite-3SG-DAT/LOC</ta>
            <ta e="T263" id="Seg_6544" s="T262">MOD</ta>
            <ta e="T264" id="Seg_6545" s="T263">Haus.[NOM]</ta>
            <ta e="T265" id="Seg_6546" s="T264">Spur-PL-3SG.[NOM]</ta>
            <ta e="T266" id="Seg_6547" s="T265">es.gibt-3PL</ta>
            <ta e="T267" id="Seg_6548" s="T266">zwei</ta>
            <ta e="T268" id="Seg_6549" s="T267">drei</ta>
            <ta e="T269" id="Seg_6550" s="T268">Haus.[NOM]</ta>
            <ta e="T270" id="Seg_6551" s="T269">Spur-3SG.[NOM]</ta>
            <ta e="T272" id="Seg_6552" s="T271">dort</ta>
            <ta e="T273" id="Seg_6553" s="T272">leben-PRS-3PL</ta>
            <ta e="T274" id="Seg_6554" s="T273">jenes</ta>
            <ta e="T275" id="Seg_6555" s="T274">Jäger-PL.[NOM]</ta>
            <ta e="T276" id="Seg_6556" s="T275">leben-TEMP-3PL</ta>
            <ta e="T277" id="Seg_6557" s="T276">Lygyj-PL.[NOM]</ta>
            <ta e="T278" id="Seg_6558" s="T277">kommen-PST2-3PL</ta>
            <ta e="T280" id="Seg_6559" s="T279">Lygyj-PL.[NOM]</ta>
            <ta e="T281" id="Seg_6560" s="T280">sewn-PL.[NOM]</ta>
            <ta e="T283" id="Seg_6561" s="T282">jenes</ta>
            <ta e="T284" id="Seg_6562" s="T283">kommen-PST2-3PL</ta>
            <ta e="T285" id="Seg_6563" s="T284">drei</ta>
            <ta e="T286" id="Seg_6564" s="T285">Frau-PROPR-3PL</ta>
            <ta e="T287" id="Seg_6565" s="T286">jenes-PL.[NOM]</ta>
            <ta e="T289" id="Seg_6566" s="T288">eins-3PL.[NOM]</ta>
            <ta e="T290" id="Seg_6567" s="T289">Schamane.[NOM]</ta>
            <ta e="T292" id="Seg_6568" s="T291">zwei-3PL.[NOM]</ta>
            <ta e="T293" id="Seg_6569" s="T292">Koch.[NOM]</ta>
            <ta e="T295" id="Seg_6570" s="T294">doch</ta>
            <ta e="T296" id="Seg_6571" s="T295">dieses</ta>
            <ta e="T297" id="Seg_6572" s="T296">Mensch-ACC</ta>
            <ta e="T298" id="Seg_6573" s="T297">töten-PST1-3SG</ta>
            <ta e="T299" id="Seg_6574" s="T298">töten-PTCP.PST-EP-INSTR</ta>
            <ta e="T300" id="Seg_6575" s="T299">kommen-PST2-3PL</ta>
            <ta e="T301" id="Seg_6576" s="T300">jenes-PL-ACC</ta>
            <ta e="T302" id="Seg_6577" s="T301">Koch.[NOM]</ta>
            <ta e="T303" id="Seg_6578" s="T302">machen-CVB.PURP</ta>
            <ta e="T304" id="Seg_6579" s="T303">bringen-PST2-3PL</ta>
            <ta e="T305" id="Seg_6580" s="T304">schlecht.[NOM]</ta>
            <ta e="T306" id="Seg_6581" s="T305">Mensch-PL.[NOM]</ta>
            <ta e="T308" id="Seg_6582" s="T307">wie</ta>
            <ta e="T309" id="Seg_6583" s="T308">als.ob</ta>
            <ta e="T310" id="Seg_6584" s="T309">was-3PL.[NOM]</ta>
            <ta e="T311" id="Seg_6585" s="T310">Raufbold-PL.[NOM]</ta>
            <ta e="T312" id="Seg_6586" s="T311">offenbar</ta>
            <ta e="T313" id="Seg_6587" s="T312">dann</ta>
            <ta e="T315" id="Seg_6588" s="T314">jenes</ta>
            <ta e="T316" id="Seg_6589" s="T315">kommen-CVB.SEQ</ta>
            <ta e="T317" id="Seg_6590" s="T316">kommen-CVB.SEQ-3PL</ta>
            <ta e="T318" id="Seg_6591" s="T317">hierher</ta>
            <ta e="T319" id="Seg_6592" s="T318">kommen-PST2-3PL</ta>
            <ta e="T320" id="Seg_6593" s="T319">Jäger-PL.[NOM]</ta>
            <ta e="T321" id="Seg_6594" s="T320">eins-3PL.[NOM]</ta>
            <ta e="T322" id="Seg_6595" s="T321">EMPH</ta>
            <ta e="T323" id="Seg_6596" s="T322">flink.[NOM]</ta>
            <ta e="T325" id="Seg_6597" s="T324">eins-3PL.[NOM]</ta>
            <ta e="T326" id="Seg_6598" s="T325">gut.sehend.[NOM]</ta>
            <ta e="T327" id="Seg_6599" s="T326">eins-3PL.[NOM]</ta>
            <ta e="T328" id="Seg_6600" s="T327">schießen-NMNZ-AG.[NOM]</ta>
            <ta e="T329" id="Seg_6601" s="T328">Jäger-PL.[NOM]</ta>
            <ta e="T330" id="Seg_6602" s="T329">unsere.[NOM]</ta>
            <ta e="T331" id="Seg_6603" s="T330">man.sagt</ta>
            <ta e="T332" id="Seg_6604" s="T331">erzählen-PRS-3PL</ta>
            <ta e="T334" id="Seg_6605" s="T333">jenes-PL.[NOM]</ta>
            <ta e="T335" id="Seg_6606" s="T334">aber</ta>
            <ta e="T336" id="Seg_6607" s="T335">Mensch-ACC</ta>
            <ta e="T337" id="Seg_6608" s="T336">vernichten-EP-CAUS-CVB.SEQ</ta>
            <ta e="T338" id="Seg_6609" s="T337">kommen-CVB.SEQ</ta>
            <ta e="T339" id="Seg_6610" s="T338">kommen-CVB.SEQ-3PL</ta>
            <ta e="T340" id="Seg_6611" s="T339">dieses</ta>
            <ta e="T341" id="Seg_6612" s="T340">alter.Mann-PL.[NOM]</ta>
            <ta e="T342" id="Seg_6613" s="T341">Malyj</ta>
            <ta e="T343" id="Seg_6614" s="T342">sagen-CVB.SEQ</ta>
            <ta e="T344" id="Seg_6615" s="T343">Ort-DAT/LOC</ta>
            <ta e="T345" id="Seg_6616" s="T344">Porotov</ta>
            <ta e="T346" id="Seg_6617" s="T345">sagen-CVB.SEQ</ta>
            <ta e="T347" id="Seg_6618" s="T346">Mensch.[NOM]</ta>
            <ta e="T348" id="Seg_6619" s="T347">es.gibt</ta>
            <ta e="T349" id="Seg_6620" s="T348">sein-PST1-3SG</ta>
            <ta e="T351" id="Seg_6621" s="T350">jenes-DAT/LOC</ta>
            <ta e="T352" id="Seg_6622" s="T351">gehen-PST2-3PL</ta>
            <ta e="T353" id="Seg_6623" s="T352">dieses</ta>
            <ta e="T354" id="Seg_6624" s="T353">Mensch-PL.[NOM]</ta>
            <ta e="T356" id="Seg_6625" s="T355">na</ta>
            <ta e="T357" id="Seg_6626" s="T356">Freund.[NOM]</ta>
            <ta e="T358" id="Seg_6627" s="T357">solch</ta>
            <ta e="T359" id="Seg_6628" s="T358">solch</ta>
            <ta e="T360" id="Seg_6629" s="T359">Heer.[NOM]</ta>
            <ta e="T361" id="Seg_6630" s="T360">Schnauze-PROPR</ta>
            <ta e="T362" id="Seg_6631" s="T361">Hund-PL.[NOM]</ta>
            <ta e="T363" id="Seg_6632" s="T362">hacken-PRS-3PL</ta>
            <ta e="T364" id="Seg_6633" s="T363">töten-PTCP.FUT-AG-PL.[NOM]</ta>
            <ta e="T365" id="Seg_6634" s="T364">kommen-3PL</ta>
            <ta e="T366" id="Seg_6635" s="T365">sewn-PL.[NOM]</ta>
            <ta e="T368" id="Seg_6636" s="T367">doch</ta>
            <ta e="T369" id="Seg_6637" s="T368">jenes</ta>
            <ta e="T370" id="Seg_6638" s="T369">entfliehen-PRS-3PL</ta>
            <ta e="T371" id="Seg_6639" s="T370">Malyj-DAT/LOC</ta>
            <ta e="T372" id="Seg_6640" s="T371">entfliehen-PST2-3PL</ta>
            <ta e="T374" id="Seg_6641" s="T373">jenes</ta>
            <ta e="T375" id="Seg_6642" s="T374">entfliehen-CVB.SEQ</ta>
            <ta e="T376" id="Seg_6643" s="T375">ankommen-PST2-3PL</ta>
            <ta e="T377" id="Seg_6644" s="T376">Porotov-DAT/LOC</ta>
            <ta e="T378" id="Seg_6645" s="T377">ankommen-CVB.SEQ-3PL</ta>
            <ta e="T379" id="Seg_6646" s="T378">EMPH</ta>
            <ta e="T380" id="Seg_6647" s="T379">erzählen-PRS-3PL</ta>
            <ta e="T381" id="Seg_6648" s="T380">na</ta>
            <ta e="T382" id="Seg_6649" s="T381">solch-PL.[NOM]</ta>
            <ta e="T383" id="Seg_6650" s="T382">kommen-PST1-3PL</ta>
            <ta e="T384" id="Seg_6651" s="T383">Mensch-ACC</ta>
            <ta e="T385" id="Seg_6652" s="T384">vernichten-CAUS-PST2-3PL</ta>
            <ta e="T386" id="Seg_6653" s="T385">breit</ta>
            <ta e="T387" id="Seg_6654" s="T386">See-ABL</ta>
            <ta e="T388" id="Seg_6655" s="T387">fließen-CVB.SEQ-3PL</ta>
            <ta e="T389" id="Seg_6656" s="T388">neu</ta>
            <ta e="T390" id="Seg_6657" s="T389">Fluss.[NOM]</ta>
            <ta e="T391" id="Seg_6658" s="T390">entlang</ta>
            <ta e="T392" id="Seg_6659" s="T391">kommen-PST2-3PL</ta>
            <ta e="T393" id="Seg_6660" s="T392">sagen-CVB.SEQ</ta>
            <ta e="T395" id="Seg_6661" s="T394">jenes</ta>
            <ta e="T396" id="Seg_6662" s="T395">alter.Mann-DAT/LOC</ta>
            <ta e="T397" id="Seg_6663" s="T396">ankommen-CVB.SEQ-3PL</ta>
            <ta e="T398" id="Seg_6664" s="T397">dieses</ta>
            <ta e="T399" id="Seg_6665" s="T398">Mensch-PL.[NOM]</ta>
            <ta e="T400" id="Seg_6666" s="T399">doch</ta>
            <ta e="T402" id="Seg_6667" s="T401">doch</ta>
            <ta e="T403" id="Seg_6668" s="T402">gut.sehend.[NOM]</ta>
            <ta e="T404" id="Seg_6669" s="T403">sehen-FREQ-CVB.SIM</ta>
            <ta e="T405" id="Seg_6670" s="T404">sitzen.[IMP.2SG]</ta>
            <ta e="T406" id="Seg_6671" s="T405">nach.oben</ta>
            <ta e="T407" id="Seg_6672" s="T406">Hügel-DAT/LOC</ta>
            <ta e="T408" id="Seg_6673" s="T407">hinausgehen-CVB.SEQ-2SG</ta>
            <ta e="T409" id="Seg_6674" s="T408">sagen-CVB.SEQ</ta>
            <ta e="T411" id="Seg_6675" s="T410">eins-3PL.[NOM]</ta>
            <ta e="T412" id="Seg_6676" s="T411">schießen-NMNZ-AG-3PL.[NOM]</ta>
            <ta e="T413" id="Seg_6677" s="T412">nachdem</ta>
            <ta e="T414" id="Seg_6678" s="T413">hineingehen-CAUS-ITER-PST2.[3SG]</ta>
            <ta e="T415" id="Seg_6679" s="T414">Freund-PL-3SG-ACC</ta>
            <ta e="T416" id="Seg_6680" s="T415">so</ta>
            <ta e="T417" id="Seg_6681" s="T416">machen-CVB.SEQ</ta>
            <ta e="T418" id="Seg_6682" s="T417">nachdem</ta>
            <ta e="T419" id="Seg_6683" s="T418">doch</ta>
            <ta e="T420" id="Seg_6684" s="T419">Blut.[NOM]</ta>
            <ta e="T421" id="Seg_6685" s="T420">dicke.Suppe.[NOM]</ta>
            <ta e="T422" id="Seg_6686" s="T421">kochen-PST2-3PL</ta>
            <ta e="T423" id="Seg_6687" s="T422">dieses</ta>
            <ta e="T424" id="Seg_6688" s="T423">alter.Mann.[NOM]</ta>
            <ta e="T426" id="Seg_6689" s="T425">Stange.[NOM]</ta>
            <ta e="T427" id="Seg_6690" s="T426">Zelt-PROPR-3PL</ta>
            <ta e="T429" id="Seg_6691" s="T428">doch</ta>
            <ta e="T430" id="Seg_6692" s="T429">dicke.Suppe-VBZ-CVB.SEQ</ta>
            <ta e="T431" id="Seg_6693" s="T430">nachdem</ta>
            <ta e="T432" id="Seg_6694" s="T431">EMPH</ta>
            <ta e="T433" id="Seg_6695" s="T432">dicke.Suppe-3SG-DAT/LOC</ta>
            <ta e="T434" id="Seg_6696" s="T433">MOD</ta>
            <ta e="T435" id="Seg_6697" s="T434">wer-ACC</ta>
            <ta e="T436" id="Seg_6698" s="T435">zerschneiden-PST2.[3SG]</ta>
            <ta e="T438" id="Seg_6699" s="T437">%%</ta>
            <ta e="T439" id="Seg_6700" s="T438">sagen-HAB-3PL</ta>
            <ta e="T440" id="Seg_6701" s="T439">dann</ta>
            <ta e="T441" id="Seg_6702" s="T440">jetzt</ta>
            <ta e="T442" id="Seg_6703" s="T441">%%</ta>
            <ta e="T443" id="Seg_6704" s="T442">Angelschnur</ta>
            <ta e="T445" id="Seg_6705" s="T444">Angelschnur-ACC</ta>
            <ta e="T446" id="Seg_6706" s="T445">Tabak-DAT/LOC</ta>
            <ta e="T447" id="Seg_6707" s="T446">wie</ta>
            <ta e="T448" id="Seg_6708" s="T447">ähnlich</ta>
            <ta e="T449" id="Seg_6709" s="T448">zerschneiden-CVB.SIM-zerschneiden-CVB.SIM</ta>
            <ta e="T450" id="Seg_6710" s="T449">EMPH</ta>
            <ta e="T451" id="Seg_6711" s="T450">dicke.Suppe-DAT/LOC</ta>
            <ta e="T452" id="Seg_6712" s="T451">gießen-PST2.[3SG]</ta>
            <ta e="T453" id="Seg_6713" s="T452">EMPH</ta>
            <ta e="T455" id="Seg_6714" s="T454">Hunger.haben-CVB.SEQ</ta>
            <ta e="T456" id="Seg_6715" s="T455">gehen-PTCP.PRS</ta>
            <ta e="T457" id="Seg_6716" s="T456">Mensch-PL.[NOM]</ta>
            <ta e="T458" id="Seg_6717" s="T457">dicke.Suppe.[NOM]</ta>
            <ta e="T459" id="Seg_6718" s="T458">trinken-FUT-3PL</ta>
            <ta e="T460" id="Seg_6719" s="T459">sagen-CVB.SEQ</ta>
            <ta e="T461" id="Seg_6720" s="T460">jenes</ta>
            <ta e="T462" id="Seg_6721" s="T461">trinken-FUT-3PL</ta>
            <ta e="T463" id="Seg_6722" s="T462">sagen-CVB.SEQ</ta>
            <ta e="T464" id="Seg_6723" s="T463">jenes</ta>
            <ta e="T465" id="Seg_6724" s="T464">dicke.Suppe</ta>
            <ta e="T466" id="Seg_6725" s="T465">zerschneiden-CAUS-PST2.[3SG]</ta>
            <ta e="T467" id="Seg_6726" s="T466">EMPH</ta>
            <ta e="T469" id="Seg_6727" s="T468">so</ta>
            <ta e="T470" id="Seg_6728" s="T469">schlucken-NMNZ.[NOM]</ta>
            <ta e="T471" id="Seg_6729" s="T470">wollen-TEMP-3SG</ta>
            <ta e="T472" id="Seg_6730" s="T471">jenes</ta>
            <ta e="T473" id="Seg_6731" s="T472">Chef-3PL-ACC</ta>
            <ta e="T474" id="Seg_6732" s="T473">Hals-3SG-ACC</ta>
            <ta e="T475" id="Seg_6733" s="T474">na</ta>
            <ta e="T476" id="Seg_6734" s="T475">schlagen-FUT-EP-IMP.2PL</ta>
            <ta e="T477" id="Seg_6735" s="T476">sagen-CVB.SEQ</ta>
            <ta e="T478" id="Seg_6736" s="T477">Bart-EP</ta>
            <ta e="T479" id="Seg_6737" s="T478">äh</ta>
            <ta e="T480" id="Seg_6738" s="T479">wer-EP-INSTR</ta>
            <ta e="T481" id="Seg_6739" s="T480">Glefe-INSTR</ta>
            <ta e="T483" id="Seg_6740" s="T482">dann</ta>
            <ta e="T484" id="Seg_6741" s="T483">nur</ta>
            <ta e="T485" id="Seg_6742" s="T484">siegen-FUT-1PL</ta>
            <ta e="T486" id="Seg_6743" s="T485">sagen-PST2.[3SG]</ta>
            <ta e="T487" id="Seg_6744" s="T486">EMPH</ta>
            <ta e="T488" id="Seg_6745" s="T487">dann</ta>
            <ta e="T489" id="Seg_6746" s="T488">sprechen-PST1-3SG</ta>
            <ta e="T490" id="Seg_6747" s="T489">können-FUT-1PL</ta>
            <ta e="T491" id="Seg_6748" s="T490">wie</ta>
            <ta e="T492" id="Seg_6749" s="T491">INDEF</ta>
            <ta e="T494" id="Seg_6750" s="T493">doch</ta>
            <ta e="T495" id="Seg_6751" s="T494">jenes</ta>
            <ta e="T496" id="Seg_6752" s="T495">kommen-PST2-3PL</ta>
            <ta e="T497" id="Seg_6753" s="T496">jenes</ta>
            <ta e="T498" id="Seg_6754" s="T497">dicke.Suppe-VBZ-PST2-3PL</ta>
            <ta e="T499" id="Seg_6755" s="T498">doch</ta>
            <ta e="T500" id="Seg_6756" s="T499">trinken-PRS-3PL</ta>
            <ta e="T501" id="Seg_6757" s="T500">dicke.Suppe-VBZ-IMP.2PL</ta>
            <ta e="T502" id="Seg_6758" s="T501">sagen-PST2.[3SG]</ta>
            <ta e="T503" id="Seg_6759" s="T502">EMPH</ta>
            <ta e="T504" id="Seg_6760" s="T503">dicke.Suppe-VBZ-PST2-3PL</ta>
            <ta e="T506" id="Seg_6761" s="T505">doch</ta>
            <ta e="T507" id="Seg_6762" s="T506">jenes</ta>
            <ta e="T508" id="Seg_6763" s="T507">kochen-EP-CAUS-CVB.SIM</ta>
            <ta e="T509" id="Seg_6764" s="T508">liegen-TEMP-3PL</ta>
            <ta e="T510" id="Seg_6765" s="T509">doch</ta>
            <ta e="T511" id="Seg_6766" s="T510">kommen-PST2-3PL</ta>
            <ta e="T512" id="Seg_6767" s="T511">EMPH</ta>
            <ta e="T513" id="Seg_6768" s="T512">jenes</ta>
            <ta e="T514" id="Seg_6769" s="T513">Unmensch-PL-EP-2SG.[NOM]</ta>
            <ta e="T516" id="Seg_6770" s="T515">kommen-CVB.SEQ-3PL</ta>
            <ta e="T517" id="Seg_6771" s="T516">Frau-PL-ACC</ta>
            <ta e="T518" id="Seg_6772" s="T517">hineingehen-CAUS-ITER-CVB.SEQ</ta>
            <ta e="T519" id="Seg_6773" s="T518">doch</ta>
            <ta e="T520" id="Seg_6774" s="T519">EMPH</ta>
            <ta e="T521" id="Seg_6775" s="T520">alter.Mann.[NOM]</ta>
            <ta e="T522" id="Seg_6776" s="T521">empfangen-PRS.[3SG]</ta>
            <ta e="T523" id="Seg_6777" s="T522">hineingehen-CAUS-ITER-PRS.[3SG]</ta>
            <ta e="T525" id="Seg_6778" s="T524">na</ta>
            <ta e="T526" id="Seg_6779" s="T525">solch-PL.[NOM]</ta>
            <ta e="T527" id="Seg_6780" s="T526">solch-PL.[NOM]</ta>
            <ta e="T528" id="Seg_6781" s="T527">Neuigkeit-PART</ta>
            <ta e="T529" id="Seg_6782" s="T528">erzählen-IMP.2PL</ta>
            <ta e="T530" id="Seg_6783" s="T529">sagen-CVB.SEQ</ta>
            <ta e="T532" id="Seg_6784" s="T531">doch</ta>
            <ta e="T533" id="Seg_6785" s="T532">jenes</ta>
            <ta e="T534" id="Seg_6786" s="T533">alter.Mann-DAT/LOC</ta>
            <ta e="T535" id="Seg_6787" s="T534">ankommen-CVB.SEQ</ta>
            <ta e="T536" id="Seg_6788" s="T535">erzählen-PRS-3PL</ta>
            <ta e="T537" id="Seg_6789" s="T536">EMPH</ta>
            <ta e="T538" id="Seg_6790" s="T537">Alte-ACC</ta>
            <ta e="T539" id="Seg_6791" s="T538">bringen-PST1-1PL</ta>
            <ta e="T540" id="Seg_6792" s="T539">1PL.[NOM]</ta>
            <ta e="T541" id="Seg_6793" s="T540">dieses.[NOM]</ta>
            <ta e="T542" id="Seg_6794" s="T541">Erde-AG-1PL.[NOM]</ta>
            <ta e="T543" id="Seg_6795" s="T542">sagen-PRS-1PL</ta>
            <ta e="T545" id="Seg_6796" s="T544">Schamane.[NOM]</ta>
            <ta e="T546" id="Seg_6797" s="T545">Alte.[NOM]</ta>
            <ta e="T547" id="Seg_6798" s="T546">dieses.[NOM]</ta>
            <ta e="T548" id="Seg_6799" s="T547">Koch-PL-1PL.[NOM]</ta>
            <ta e="T550" id="Seg_6800" s="T549">dann</ta>
            <ta e="T551" id="Seg_6801" s="T550">jenes</ta>
            <ta e="T552" id="Seg_6802" s="T551">Schamane.[NOM]</ta>
            <ta e="T553" id="Seg_6803" s="T552">Alte.[NOM]</ta>
            <ta e="T554" id="Seg_6804" s="T553">Erde-VBZ-CVB.SEQ</ta>
            <ta e="T555" id="Seg_6805" s="T554">bringen-PST1-3SG</ta>
            <ta e="T556" id="Seg_6806" s="T555">1PL-DAT/LOC</ta>
            <ta e="T557" id="Seg_6807" s="T556">2PL-DAT/LOC</ta>
            <ta e="T558" id="Seg_6808" s="T557">sagen-CVB.SEQ</ta>
            <ta e="T559" id="Seg_6809" s="T558">jenes</ta>
            <ta e="T560" id="Seg_6810" s="T559">erzählen-PRS-3PL</ta>
            <ta e="T561" id="Seg_6811" s="T560">EMPH</ta>
            <ta e="T562" id="Seg_6812" s="T561">Mensch-ACC</ta>
            <ta e="T563" id="Seg_6813" s="T562">vernichten-EP-CAUS-PST2-3PL</ta>
            <ta e="T565" id="Seg_6814" s="T564">Blut-3PL-ACC</ta>
            <ta e="T566" id="Seg_6815" s="T565">MOD</ta>
            <ta e="T567" id="Seg_6816" s="T566">Messer-3PL-ACC</ta>
            <ta e="T568" id="Seg_6817" s="T567">hier</ta>
            <ta e="T569" id="Seg_6818" s="T568">wischen-PRS-3PL</ta>
            <ta e="T570" id="Seg_6819" s="T569">sein-PST2.[3SG]</ta>
            <ta e="T571" id="Seg_6820" s="T570">Mensch-PL-ACC</ta>
            <ta e="T572" id="Seg_6821" s="T571">zerschneiden-CVB.SIM</ta>
            <ta e="T573" id="Seg_6822" s="T572">zerschneiden-CVB.SIM</ta>
            <ta e="T574" id="Seg_6823" s="T573">essen-CVB.SIM</ta>
            <ta e="T575" id="Seg_6824" s="T574">essen-CVB.SIM</ta>
            <ta e="T576" id="Seg_6825" s="T575">essen-PRS-3PL</ta>
            <ta e="T578" id="Seg_6826" s="T577">oh</ta>
            <ta e="T579" id="Seg_6827" s="T578">was.[NOM]</ta>
            <ta e="T580" id="Seg_6828" s="T579">Name-PROPR</ta>
            <ta e="T581" id="Seg_6829" s="T580">Blut-3SG-DAT/LOC</ta>
            <ta e="T582" id="Seg_6830" s="T581">vollschmieren-EP-PASS/REFL-EP-PTCP.PST</ta>
            <ta e="T583" id="Seg_6831" s="T582">Volk-PL-2PL=Q</ta>
            <ta e="T584" id="Seg_6832" s="T583">doch</ta>
            <ta e="T585" id="Seg_6833" s="T584">essen-IMP.2PL</ta>
            <ta e="T586" id="Seg_6834" s="T585">sagen-CVB.SEQ</ta>
            <ta e="T587" id="Seg_6835" s="T586">alter.Mann.[NOM]</ta>
            <ta e="T588" id="Seg_6836" s="T587">doch</ta>
            <ta e="T589" id="Seg_6837" s="T588">essen-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T591" id="Seg_6838" s="T590">doch</ta>
            <ta e="T592" id="Seg_6839" s="T591">jenes</ta>
            <ta e="T593" id="Seg_6840" s="T592">essen</ta>
            <ta e="T594" id="Seg_6841" s="T593">essen-EP-CAUS-PTCP.PST-3SG-ACC</ta>
            <ta e="T595" id="Seg_6842" s="T594">nachdem</ta>
            <ta e="T596" id="Seg_6843" s="T595">doch</ta>
            <ta e="T597" id="Seg_6844" s="T596">jenes</ta>
            <ta e="T598" id="Seg_6845" s="T597">oh</ta>
            <ta e="T599" id="Seg_6846" s="T598">Himmel-1PL.[NOM]</ta>
            <ta e="T600" id="Seg_6847" s="T599">wie</ta>
            <ta e="T601" id="Seg_6848" s="T600">wie</ta>
            <ta e="T602" id="Seg_6849" s="T601">sein-PRS.[3SG]</ta>
            <ta e="T603" id="Seg_6850" s="T602">doch</ta>
            <ta e="T604" id="Seg_6851" s="T603">Wolke-1PL.[NOM]</ta>
            <ta e="T605" id="Seg_6852" s="T604">hinzukommen-PRS.[3SG]</ta>
            <ta e="T607" id="Seg_6853" s="T606">was</ta>
            <ta e="T608" id="Seg_6854" s="T607">sein-PRS.[3SG]</ta>
            <ta e="T609" id="Seg_6855" s="T608">sagen-PST2-3SG</ta>
            <ta e="T611" id="Seg_6856" s="T610">doch</ta>
            <ta e="T612" id="Seg_6857" s="T611">jenes</ta>
            <ta e="T613" id="Seg_6858" s="T612">zwei</ta>
            <ta e="T614" id="Seg_6859" s="T613">munter-PROPR-ACC</ta>
            <ta e="T615" id="Seg_6860" s="T614">zwei</ta>
            <ta e="T616" id="Seg_6861" s="T615">Seite-3SG-DAT/LOC</ta>
            <ta e="T617" id="Seg_6862" s="T616">setzen-PST2.[3SG]</ta>
            <ta e="T618" id="Seg_6863" s="T617">EMPH</ta>
            <ta e="T620" id="Seg_6864" s="T619">doch</ta>
            <ta e="T621" id="Seg_6865" s="T620">jenes</ta>
            <ta e="T622" id="Seg_6866" s="T621">schlucken-NMNZ.[NOM]</ta>
            <ta e="T623" id="Seg_6867" s="T622">machen-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T624" id="Seg_6868" s="T623">Hals-3SG-ACC</ta>
            <ta e="T625" id="Seg_6869" s="T624">spalten-CVB.SIM</ta>
            <ta e="T626" id="Seg_6870" s="T625">schlagen-EP-PST2-3PL</ta>
            <ta e="T627" id="Seg_6871" s="T626">Glefe-INSTR</ta>
            <ta e="T629" id="Seg_6872" s="T628">jetzt</ta>
            <ta e="T630" id="Seg_6873" s="T629">dieses.[NOM]</ta>
            <ta e="T631" id="Seg_6874" s="T630">kämpfen-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T632" id="Seg_6875" s="T631">sagen-PRS.[3SG]</ta>
            <ta e="T633" id="Seg_6876" s="T632">Kino-PL-DAT/LOC</ta>
            <ta e="T634" id="Seg_6877" s="T633">wie</ta>
            <ta e="T636" id="Seg_6878" s="T635">jenes</ta>
            <ta e="T637" id="Seg_6879" s="T636">siegen-PST2-3PL</ta>
            <ta e="T638" id="Seg_6880" s="T637">man.sagt</ta>
            <ta e="T639" id="Seg_6881" s="T638">jenes-3SG-3PL-ACC</ta>
            <ta e="T641" id="Seg_6882" s="T640">doch</ta>
            <ta e="T642" id="Seg_6883" s="T641">anderer.von.zwei-3PL-ACC</ta>
            <ta e="T643" id="Seg_6884" s="T642">töten-ITER-PST2-3PL</ta>
            <ta e="T645" id="Seg_6885" s="T644">sieben</ta>
            <ta e="T646" id="Seg_6886" s="T645">Mensch-3SG.[NOM]</ta>
            <ta e="T647" id="Seg_6887" s="T646">sein-PST2.[3SG]</ta>
            <ta e="T648" id="Seg_6888" s="T647">Q</ta>
            <ta e="T649" id="Seg_6889" s="T648">wie.viel</ta>
            <ta e="T650" id="Seg_6890" s="T649">Q</ta>
            <ta e="T652" id="Seg_6891" s="T651">doch</ta>
            <ta e="T653" id="Seg_6892" s="T652">dann</ta>
            <ta e="T654" id="Seg_6893" s="T653">Frau-PL.[NOM]</ta>
            <ta e="T655" id="Seg_6894" s="T654">bleiben-PTCP.PST-3PL-DAT/LOC</ta>
            <ta e="T656" id="Seg_6895" s="T655">dieses</ta>
            <ta e="T657" id="Seg_6896" s="T656">Frau-PL.[NOM]</ta>
            <ta e="T658" id="Seg_6897" s="T657">doch</ta>
            <ta e="T659" id="Seg_6898" s="T658">sich.streiten-PRS-3PL</ta>
            <ta e="T660" id="Seg_6899" s="T659">man.sagt</ta>
            <ta e="T661" id="Seg_6900" s="T660">Messer-3PL-ACC</ta>
            <ta e="T662" id="Seg_6901" s="T661">wegnehmen-EP-RECP/COLL-CVB.SEQ-3PL</ta>
            <ta e="T664" id="Seg_6902" s="T663">1SG.[NOM]</ta>
            <ta e="T665" id="Seg_6903" s="T664">Mann-EP-1SG.[NOM]</ta>
            <ta e="T666" id="Seg_6904" s="T665">Messer-3SG.[NOM]</ta>
            <ta e="T667" id="Seg_6905" s="T666">1SG.[NOM]</ta>
            <ta e="T668" id="Seg_6906" s="T667">Mann-EP-1SG.[NOM]</ta>
            <ta e="T669" id="Seg_6907" s="T668">Messer-3SG.[NOM]</ta>
            <ta e="T670" id="Seg_6908" s="T669">sagen-EP-RECP/COLL-EMOT-PRS-3PL</ta>
            <ta e="T671" id="Seg_6909" s="T670">man.sagt</ta>
            <ta e="T672" id="Seg_6910" s="T671">Armer-PL.[NOM]</ta>
            <ta e="T674" id="Seg_6911" s="T673">was</ta>
            <ta e="T675" id="Seg_6912" s="T674">Schlampe</ta>
            <ta e="T676" id="Seg_6913" s="T675">Mädchen-3PL.[NOM]=Q</ta>
            <ta e="T677" id="Seg_6914" s="T676">dieses-PL-ACC</ta>
            <ta e="T678" id="Seg_6915" s="T677">sterben-MULT-CVB.SEQ</ta>
            <ta e="T679" id="Seg_6916" s="T678">werfen-EP-IMP.2PL</ta>
            <ta e="T680" id="Seg_6917" s="T679">sagen-PST2.[3SG]</ta>
            <ta e="T681" id="Seg_6918" s="T680">EMPH</ta>
            <ta e="T682" id="Seg_6919" s="T681">was.[NOM]</ta>
            <ta e="T683" id="Seg_6920" s="T682">grausam.[NOM]</ta>
            <ta e="T684" id="Seg_6921" s="T683">alter.Mann-3SG.[NOM]</ta>
            <ta e="T685" id="Seg_6922" s="T684">MOD</ta>
            <ta e="T686" id="Seg_6923" s="T685">EMPH</ta>
            <ta e="T687" id="Seg_6924" s="T686">unser</ta>
            <ta e="T688" id="Seg_6925" s="T687">Porotov.[NOM]</ta>
            <ta e="T690" id="Seg_6926" s="T689">sterben-MULT-CVB.SEQ</ta>
            <ta e="T691" id="Seg_6927" s="T690">werfen-PST2-3PL</ta>
            <ta e="T692" id="Seg_6928" s="T691">Unglück-PROPR-PL.[NOM]</ta>
            <ta e="T694" id="Seg_6929" s="T693">dann</ta>
            <ta e="T695" id="Seg_6930" s="T694">jenes</ta>
            <ta e="T696" id="Seg_6931" s="T695">Alte-ACC</ta>
            <ta e="T697" id="Seg_6932" s="T696">doch</ta>
            <ta e="T698" id="Seg_6933" s="T697">töten-CVB.SIM</ta>
            <ta e="T699" id="Seg_6934" s="T698">versuchen-PRS-3PL</ta>
            <ta e="T700" id="Seg_6935" s="T699">Schamane.[NOM]</ta>
            <ta e="T701" id="Seg_6936" s="T700">Alte-ACC</ta>
            <ta e="T703" id="Seg_6937" s="T702">Wasser-DAT/LOC</ta>
            <ta e="T704" id="Seg_6938" s="T703">EMPH</ta>
            <ta e="T705" id="Seg_6939" s="T704">sinken-CAUS-PRS-3PL</ta>
            <ta e="T706" id="Seg_6940" s="T705">doch</ta>
            <ta e="T707" id="Seg_6941" s="T706">Anker-VBZ-CVB.SEQ</ta>
            <ta e="T708" id="Seg_6942" s="T707">nachdem</ta>
            <ta e="T710" id="Seg_6943" s="T709">ganz</ta>
            <ta e="T711" id="Seg_6944" s="T710">sinken-NEG.[3SG]</ta>
            <ta e="T713" id="Seg_6945" s="T712">alles.probieren-CVB.SIM</ta>
            <ta e="T714" id="Seg_6946" s="T713">versuchen-CVB.SEQ</ta>
            <ta e="T715" id="Seg_6947" s="T714">Feuer-3SG-ACC</ta>
            <ta e="T716" id="Seg_6948" s="T715">anzünden-CVB.SEQ</ta>
            <ta e="T717" id="Seg_6949" s="T716">EMPH</ta>
            <ta e="T718" id="Seg_6950" s="T717">Feuer.[NOM]</ta>
            <ta e="T719" id="Seg_6951" s="T718">Inneres-3SG-DAT/LOC</ta>
            <ta e="T720" id="Seg_6952" s="T719">werfen-PRS-3PL</ta>
            <ta e="T722" id="Seg_6953" s="T721">Feuer-3PL.[NOM]</ta>
            <ta e="T723" id="Seg_6954" s="T722">schwarz.sein-INCH</ta>
            <ta e="T724" id="Seg_6955" s="T723">schlafen-PRS.[3SG]</ta>
            <ta e="T725" id="Seg_6956" s="T724">man.sagt</ta>
            <ta e="T727" id="Seg_6957" s="T726">dort</ta>
            <ta e="T728" id="Seg_6958" s="T727">EMPH</ta>
            <ta e="T729" id="Seg_6959" s="T728">sagen-PST2.[3SG]</ta>
            <ta e="T730" id="Seg_6960" s="T729">EMPH</ta>
            <ta e="T731" id="Seg_6961" s="T730">lieb-PL.[NOM]</ta>
            <ta e="T732" id="Seg_6962" s="T731">1SG.[NOM]</ta>
            <ta e="T733" id="Seg_6963" s="T732">Sonne.[NOM]</ta>
            <ta e="T734" id="Seg_6964" s="T733">Alte.[NOM]</ta>
            <ta e="T735" id="Seg_6965" s="T734">sein-PST1-1SG</ta>
            <ta e="T736" id="Seg_6966" s="T735">na</ta>
            <ta e="T737" id="Seg_6967" s="T736">böser.Geist.[NOM]</ta>
            <ta e="T738" id="Seg_6968" s="T737">sein-PST2.NEG-1SG</ta>
            <ta e="T740" id="Seg_6969" s="T739">Leute-1SG-ACC</ta>
            <ta e="T741" id="Seg_6970" s="T740">vernichten-PST2-3PL</ta>
            <ta e="T742" id="Seg_6971" s="T741">dieses</ta>
            <ta e="T743" id="Seg_6972" s="T742">MOD</ta>
            <ta e="T744" id="Seg_6973" s="T743">Mensch-ACC</ta>
            <ta e="T745" id="Seg_6974" s="T744">Tier-ACC</ta>
            <ta e="T746" id="Seg_6975" s="T745">sehen-CVB.PURP-1SG</ta>
            <ta e="T747" id="Seg_6976" s="T746">Erde-VBZ-CVB.SEQ</ta>
            <ta e="T748" id="Seg_6977" s="T747">kommen-PST2-EP-1SG</ta>
            <ta e="T750" id="Seg_6978" s="T749">1SG-ACC</ta>
            <ta e="T751" id="Seg_6979" s="T750">Qual-VBZ-NEG-IMP.2PL</ta>
            <ta e="T752" id="Seg_6980" s="T751">1SG.[NOM]</ta>
            <ta e="T753" id="Seg_6981" s="T752">solch</ta>
            <ta e="T754" id="Seg_6982" s="T753">nun</ta>
            <ta e="T755" id="Seg_6983" s="T754">sterben-FUT-1SG</ta>
            <ta e="T756" id="Seg_6984" s="T755">NEG-3SG</ta>
            <ta e="T757" id="Seg_6985" s="T756">sagen-PRS.[3SG]</ta>
            <ta e="T759" id="Seg_6986" s="T758">Mädchen.[NOM]</ta>
            <ta e="T760" id="Seg_6987" s="T759">Kind-3SG.[NOM]</ta>
            <ta e="T761" id="Seg_6988" s="T760">erstmals</ta>
            <ta e="T762" id="Seg_6989" s="T761">schmutzig.[NOM]</ta>
            <ta e="T763" id="Seg_6990" s="T762">werden-PST2.[3SG]</ta>
            <ta e="T764" id="Seg_6991" s="T763">Mädchen.[NOM]</ta>
            <ta e="T765" id="Seg_6992" s="T764">Kind-3SG.[NOM]</ta>
            <ta e="T766" id="Seg_6993" s="T765">Hals-1SG-INSTR</ta>
            <ta e="T767" id="Seg_6994" s="T766">übertreten-EP-IMP.3SG</ta>
            <ta e="T768" id="Seg_6995" s="T767">dann</ta>
            <ta e="T769" id="Seg_6996" s="T768">sterben-FUT-1SG</ta>
            <ta e="T770" id="Seg_6997" s="T769">sagen-PRS.[3SG]</ta>
            <ta e="T771" id="Seg_6998" s="T770">jenes</ta>
            <ta e="T772" id="Seg_6999" s="T771">Hals-1SG-INSTR</ta>
            <ta e="T773" id="Seg_7000" s="T772">übertreten-EP-CAUS-PST2-3PL</ta>
            <ta e="T775" id="Seg_7001" s="T774">doch</ta>
            <ta e="T776" id="Seg_7002" s="T775">jenes</ta>
            <ta e="T777" id="Seg_7003" s="T776">Alte-3PL.[NOM]</ta>
            <ta e="T778" id="Seg_7004" s="T777">sterben-CVB.SEQ</ta>
            <ta e="T779" id="Seg_7005" s="T778">bleiben-PST2.[3SG]</ta>
            <ta e="T780" id="Seg_7006" s="T779">doch</ta>
            <ta e="T781" id="Seg_7007" s="T780">letzter-3SG.[NOM]</ta>
            <ta e="T782" id="Seg_7008" s="T781">jenes</ta>
            <ta e="T783" id="Seg_7009" s="T782">erzählen-PST2-3PL</ta>
            <ta e="T784" id="Seg_7010" s="T783">erinnern-NEG-1SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_7011" s="T1">потом</ta>
            <ta e="T3" id="Seg_7012" s="T2">однажды</ta>
            <ta e="T4" id="Seg_7013" s="T3">охотиться-CVB.SIM</ta>
            <ta e="T5" id="Seg_7014" s="T4">выйти-EP-PST2-3PL</ta>
            <ta e="T6" id="Seg_7015" s="T5">старик-PL.[NOM]</ta>
            <ta e="T8" id="Seg_7016" s="T7">старик-PL.[NOM]</ta>
            <ta e="T9" id="Seg_7017" s="T8">приходить-PRS-3PL</ta>
            <ta e="T10" id="Seg_7018" s="T9">мальчик-PL.[NOM]</ta>
            <ta e="T12" id="Seg_7019" s="T11">эй</ta>
            <ta e="T13" id="Seg_7020" s="T12">Сындасско-1PL-DAT/LOC</ta>
            <ta e="T14" id="Seg_7021" s="T13">дом.[NOM]</ta>
            <ta e="T15" id="Seg_7022" s="T14">вставать-PST2.[3SG]</ta>
            <ta e="T16" id="Seg_7023" s="T15">говорить-PST2-3PL</ta>
            <ta e="T18" id="Seg_7024" s="T17">куда</ta>
            <ta e="T19" id="Seg_7025" s="T18">охотиться-HAB-1PL</ta>
            <ta e="T21" id="Seg_7026" s="T20">о</ta>
            <ta e="T22" id="Seg_7027" s="T21">вот</ta>
            <ta e="T23" id="Seg_7028" s="T22">этот</ta>
            <ta e="T24" id="Seg_7029" s="T23">дом.[NOM]</ta>
            <ta e="T25" id="Seg_7030" s="T24">вставать-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T26" id="Seg_7031" s="T25">бояться-PRS-3PL</ta>
            <ta e="T27" id="Seg_7032" s="T26">приходить-PTCP.FUT-3PL-ACC</ta>
            <ta e="T28" id="Seg_7033" s="T27">говорят</ta>
            <ta e="T29" id="Seg_7034" s="T28">русский-PL.[NOM]</ta>
            <ta e="T30" id="Seg_7035" s="T29">есть-3PL</ta>
            <ta e="T31" id="Seg_7036" s="T30">говорить-CVB.SEQ</ta>
            <ta e="T33" id="Seg_7037" s="T32">о</ta>
            <ta e="T34" id="Seg_7038" s="T33">вот</ta>
            <ta e="T35" id="Seg_7039" s="T34">этот</ta>
            <ta e="T36" id="Seg_7040" s="T35">земля-1PL-ACC</ta>
            <ta e="T37" id="Seg_7041" s="T36">добыча-POSS</ta>
            <ta e="T38" id="Seg_7042" s="T37">NEG</ta>
            <ta e="T39" id="Seg_7043" s="T38">делать-PST1-3PL</ta>
            <ta e="T40" id="Seg_7044" s="T39">как</ta>
            <ta e="T41" id="Seg_7045" s="T40">быть-HAB-1PL=Q</ta>
            <ta e="T42" id="Seg_7046" s="T41">говорить-PRS-3PL</ta>
            <ta e="T43" id="Seg_7047" s="T42">говорят</ta>
            <ta e="T44" id="Seg_7048" s="T43">тот</ta>
            <ta e="T45" id="Seg_7049" s="T44">старик-PL.[NOM]</ta>
            <ta e="T46" id="Seg_7050" s="T45">вот</ta>
            <ta e="T48" id="Seg_7051" s="T47">говорить-EP-RECP/COLL-PRS-1PL</ta>
            <ta e="T49" id="Seg_7052" s="T48">говорить-PRS-3PL</ta>
            <ta e="T51" id="Seg_7053" s="T50">разговаривать-PTCP.PRS-3PL-ACC</ta>
            <ta e="T52" id="Seg_7054" s="T51">слышать-PRS-1SG</ta>
            <ta e="T53" id="Seg_7055" s="T52">дедушка-PL-EP-1SG.[NOM]</ta>
            <ta e="T54" id="Seg_7056" s="T53">вот</ta>
            <ta e="T56" id="Seg_7057" s="T55">потом</ta>
            <ta e="T57" id="Seg_7058" s="T56">вот</ta>
            <ta e="T58" id="Seg_7059" s="T57">потом</ta>
            <ta e="T59" id="Seg_7060" s="T58">приходить-PRS-3PL</ta>
            <ta e="T61" id="Seg_7061" s="T60">приходить-CVB.SEQ-3PL</ta>
            <ta e="T62" id="Seg_7062" s="T61">видеть-PST2-3PL</ta>
            <ta e="T63" id="Seg_7063" s="T62">два</ta>
            <ta e="T64" id="Seg_7064" s="T63">человек.[NOM]</ta>
            <ta e="T65" id="Seg_7065" s="T64">идти-PRS-3PL</ta>
            <ta e="T66" id="Seg_7066" s="T65">говорят</ta>
            <ta e="T68" id="Seg_7067" s="T67">жена-3SG-GEN</ta>
            <ta e="T69" id="Seg_7068" s="T68">имя-3SG.[NOM]</ta>
            <ta e="T70" id="Seg_7069" s="T69">Дарья.[NOM]</ta>
            <ta e="T72" id="Seg_7070" s="T71">мужчина-3SG-ACC</ta>
            <ta e="T73" id="Seg_7071" s="T72">имя-3SG.[NOM]</ta>
            <ta e="T74" id="Seg_7072" s="T73">кто.[NOM]</ta>
            <ta e="T75" id="Seg_7073" s="T74">говорить-PST2-3PL=Q</ta>
            <ta e="T76" id="Seg_7074" s="T75">вот</ta>
            <ta e="T77" id="Seg_7075" s="T76">EMPH</ta>
            <ta e="T78" id="Seg_7076" s="T77">%%</ta>
            <ta e="T79" id="Seg_7077" s="T78">очень</ta>
            <ta e="T80" id="Seg_7078" s="T79">имя-PROPR</ta>
            <ta e="T81" id="Seg_7079" s="T80">человек.[NOM]</ta>
            <ta e="T82" id="Seg_7080" s="T81">кто.[NOM]</ta>
            <ta e="T83" id="Seg_7081" s="T82">INDEF</ta>
            <ta e="T84" id="Seg_7082" s="T83">говорить-HAB-3PL</ta>
            <ta e="T85" id="Seg_7083" s="T84">EVID</ta>
            <ta e="T87" id="Seg_7084" s="T86">совсем</ta>
            <ta e="T88" id="Seg_7085" s="T87">помнить-NEG-1SG</ta>
            <ta e="T89" id="Seg_7086" s="T88">имя-3SG-ACC</ta>
            <ta e="T91" id="Seg_7087" s="T90">тот-DAT/LOC</ta>
            <ta e="T92" id="Seg_7088" s="T91">EMPH</ta>
            <ta e="T93" id="Seg_7089" s="T92">приближаться-PRS-3PL</ta>
            <ta e="T94" id="Seg_7090" s="T93">говорят</ta>
            <ta e="T95" id="Seg_7091" s="T94">бояться-CVB.SIM</ta>
            <ta e="T96" id="Seg_7092" s="T95">бояться-CVB.SIM</ta>
            <ta e="T97" id="Seg_7093" s="T96">приближаться-PRS-3PL</ta>
            <ta e="T98" id="Seg_7094" s="T97">говорят</ta>
            <ta e="T100" id="Seg_7095" s="T99">этот</ta>
            <ta e="T101" id="Seg_7096" s="T100">почему</ta>
            <ta e="T102" id="Seg_7097" s="T101">приходить-PST2-2PL=Q</ta>
            <ta e="T103" id="Seg_7098" s="T102">1PL.[NOM]</ta>
            <ta e="T104" id="Seg_7099" s="T103">земля-1PL-DAT/LOC</ta>
            <ta e="T106" id="Seg_7100" s="T105">почему</ta>
            <ta e="T107" id="Seg_7101" s="T106">найти.[MED]-PST1-2PL</ta>
            <ta e="T109" id="Seg_7102" s="T108">охотиться-PTCP.PRS</ta>
            <ta e="T110" id="Seg_7103" s="T109">земля-1PL-ACC</ta>
            <ta e="T111" id="Seg_7104" s="T110">почему</ta>
            <ta e="T112" id="Seg_7105" s="T111">разбивать-PRS-2PL</ta>
            <ta e="T113" id="Seg_7106" s="T112">говорить-PRS-3PL</ta>
            <ta e="T114" id="Seg_7107" s="T113">говорят</ta>
            <ta e="T115" id="Seg_7108" s="T114">старик-PL.[NOM]</ta>
            <ta e="T117" id="Seg_7109" s="T116">эй</ta>
            <ta e="T118" id="Seg_7110" s="T117">здесь</ta>
            <ta e="T119" id="Seg_7111" s="T118">однако</ta>
            <ta e="T120" id="Seg_7112" s="T119">население.[NOM]</ta>
            <ta e="T121" id="Seg_7113" s="T120">делать-FUT-3PL</ta>
            <ta e="T122" id="Seg_7114" s="T121">человек.[NOM]</ta>
            <ta e="T123" id="Seg_7115" s="T122">вставать-FUT-3SG</ta>
            <ta e="T125" id="Seg_7116" s="T124">человек.[NOM]</ta>
            <ta e="T126" id="Seg_7117" s="T125">рождаться-FUT-3SG</ta>
            <ta e="T127" id="Seg_7118" s="T126">здесь</ta>
            <ta e="T128" id="Seg_7119" s="T127">много</ta>
            <ta e="T129" id="Seg_7120" s="T128">человек.[NOM]</ta>
            <ta e="T130" id="Seg_7121" s="T129">жить-FUT-3SG</ta>
            <ta e="T132" id="Seg_7122" s="T131">тогда</ta>
            <ta e="T133" id="Seg_7123" s="T132">причал-1PL.[NOM]</ta>
            <ta e="T134" id="Seg_7124" s="T133">EMPH</ta>
            <ta e="T135" id="Seg_7125" s="T134">тот.[NOM]</ta>
            <ta e="T136" id="Seg_7126" s="T135">есть</ta>
            <ta e="T137" id="Seg_7127" s="T136">%%-CVB.SEQ</ta>
            <ta e="T138" id="Seg_7128" s="T137">входить-PST2.[3SG]</ta>
            <ta e="T140" id="Seg_7129" s="T139">почему</ta>
            <ta e="T141" id="Seg_7130" s="T140">далекий-3SG.[NOM]=Q</ta>
            <ta e="T142" id="Seg_7131" s="T141">говорить-PRS-3PL</ta>
            <ta e="T143" id="Seg_7132" s="T142">говорят</ta>
            <ta e="T144" id="Seg_7133" s="T143">причал-1PL.[NOM]</ta>
            <ta e="T146" id="Seg_7134" s="T145">фарватер-2PL.[NOM]</ta>
            <ta e="T147" id="Seg_7135" s="T146">маленький.[NOM]</ta>
            <ta e="T148" id="Seg_7136" s="T147">здесь</ta>
            <ta e="T149" id="Seg_7137" s="T148">тот.[NOM]</ta>
            <ta e="T150" id="Seg_7138" s="T149">из_за</ta>
            <ta e="T151" id="Seg_7139" s="T150">там</ta>
            <ta e="T152" id="Seg_7140" s="T151">быть-PRS.[3SG]</ta>
            <ta e="T153" id="Seg_7141" s="T152">фарватер-2PL.[NOM]</ta>
            <ta e="T155" id="Seg_7142" s="T154">туда</ta>
            <ta e="T156" id="Seg_7143" s="T155">пароход-PL.[NOM]</ta>
            <ta e="T157" id="Seg_7144" s="T156">приходить-FUT-3PL</ta>
            <ta e="T159" id="Seg_7145" s="T158">удивляться-PRS-3PL</ta>
            <ta e="T160" id="Seg_7146" s="T159">говорят</ta>
            <ta e="T161" id="Seg_7147" s="T160">что.[NOM]</ta>
            <ta e="T162" id="Seg_7148" s="T161">имя-3SG.[NOM]=Q</ta>
            <ta e="T163" id="Seg_7149" s="T162">пароход.[NOM]</ta>
            <ta e="T164" id="Seg_7150" s="T163">думать-PRS.[3SG]</ta>
            <ta e="T167" id="Seg_7151" s="T166">тянуть-PTCP.PRS</ta>
            <ta e="T168" id="Seg_7152" s="T167">приходить-PRS.[3SG]</ta>
            <ta e="T169" id="Seg_7153" s="T168">пароход.[NOM]</ta>
            <ta e="T171" id="Seg_7154" s="T170">вот</ta>
            <ta e="T172" id="Seg_7155" s="T171">тот</ta>
            <ta e="T173" id="Seg_7156" s="T172">позже</ta>
            <ta e="T174" id="Seg_7157" s="T173">позже</ta>
            <ta e="T175" id="Seg_7158" s="T174">позже</ta>
            <ta e="T176" id="Seg_7159" s="T175">позже</ta>
            <ta e="T177" id="Seg_7160" s="T176">тот</ta>
            <ta e="T178" id="Seg_7161" s="T177">создаваться-CVB.SEQ</ta>
            <ta e="T179" id="Seg_7162" s="T178">создаваться-CVB.SEQ</ta>
            <ta e="T180" id="Seg_7163" s="T179">этот</ta>
            <ta e="T181" id="Seg_7164" s="T180">Сындасско-2SG.[NOM]</ta>
            <ta e="T182" id="Seg_7165" s="T181">становиться-CVB.SEQ</ta>
            <ta e="T183" id="Seg_7166" s="T182">оставаться-PST2.[3SG]</ta>
            <ta e="T184" id="Seg_7167" s="T183">EMPH</ta>
            <ta e="T186" id="Seg_7168" s="T185">школа-POSS</ta>
            <ta e="T187" id="Seg_7169" s="T186">NEG-3PL</ta>
            <ta e="T188" id="Seg_7170" s="T187">шест.[NOM]</ta>
            <ta e="T189" id="Seg_7171" s="T188">чум-DAT/LOC</ta>
            <ta e="T190" id="Seg_7172" s="T189">учить-PRS-3PL</ta>
            <ta e="T191" id="Seg_7173" s="T190">сам-3PL.[NOM]</ta>
            <ta e="T192" id="Seg_7174" s="T191">строить-CVB.SEQ-3PL</ta>
            <ta e="T194" id="Seg_7175" s="T193">балок-PL-DAT/LOC</ta>
            <ta e="T196" id="Seg_7176" s="T195">продавать-PRS-3PL</ta>
            <ta e="T198" id="Seg_7177" s="T197">потом</ta>
            <ta e="T199" id="Seg_7178" s="T198">строить-PTCP.PST</ta>
            <ta e="T200" id="Seg_7179" s="T199">дом-3PL.[NOM]</ta>
            <ta e="T201" id="Seg_7180" s="T200">есть</ta>
            <ta e="T202" id="Seg_7181" s="T201">быть-PST1-3SG</ta>
            <ta e="T203" id="Seg_7182" s="T202">тогдашний</ta>
            <ta e="T204" id="Seg_7183" s="T203">теперь</ta>
            <ta e="T205" id="Seg_7184" s="T204">рухнуть-PST2-3SG</ta>
            <ta e="T206" id="Seg_7185" s="T205">EMPH</ta>
            <ta e="T207" id="Seg_7186" s="T206">тот-3SG-2SG.[NOM]</ta>
            <ta e="T208" id="Seg_7187" s="T207">лапка-3PL.[NOM]</ta>
            <ta e="T210" id="Seg_7188" s="T209">тот.[NOM]</ta>
            <ta e="T211" id="Seg_7189" s="T210">из_за</ta>
            <ta e="T212" id="Seg_7190" s="T211">Сындасско.[NOM]</ta>
            <ta e="T213" id="Seg_7191" s="T212">Сындасско.[NOM]</ta>
            <ta e="T214" id="Seg_7192" s="T213">Дарья.[NOM]</ta>
            <ta e="T215" id="Seg_7193" s="T214">что-ACC</ta>
            <ta e="T216" id="Seg_7194" s="T215">кто-ACC</ta>
            <ta e="T217" id="Seg_7195" s="T216">Дарья.[NOM]</ta>
            <ta e="T218" id="Seg_7196" s="T217">говорить-PRS-3PL</ta>
            <ta e="T219" id="Seg_7197" s="T218">EVID</ta>
            <ta e="T220" id="Seg_7198" s="T219">тот.[NOM]</ta>
            <ta e="T221" id="Seg_7199" s="T220">по.другому</ta>
            <ta e="T222" id="Seg_7200" s="T221">назвать-PRS-3PL</ta>
            <ta e="T223" id="Seg_7201" s="T222">говорят</ta>
            <ta e="T225" id="Seg_7202" s="T224">AFFIRM</ta>
            <ta e="T226" id="Seg_7203" s="T225">этот</ta>
            <ta e="T227" id="Seg_7204" s="T226">Сындасско-ACC</ta>
            <ta e="T229" id="Seg_7205" s="T228">тот.[NOM]</ta>
            <ta e="T230" id="Seg_7206" s="T229">охотиться-PTCP.PRS</ta>
            <ta e="T231" id="Seg_7207" s="T230">место-3PL-ACC</ta>
            <ta e="T232" id="Seg_7208" s="T231">отнимать-PST2-3PL</ta>
            <ta e="T233" id="Seg_7209" s="T232">потом</ta>
            <ta e="T234" id="Seg_7210" s="T233">позже</ta>
            <ta e="T235" id="Seg_7211" s="T234">позже</ta>
            <ta e="T236" id="Seg_7212" s="T235">EVID</ta>
            <ta e="T237" id="Seg_7213" s="T236">тот.[NOM]</ta>
            <ta e="T238" id="Seg_7214" s="T237">быть-PST2-3PL</ta>
            <ta e="T240" id="Seg_7215" s="T239">потом</ta>
            <ta e="T241" id="Seg_7216" s="T240">позже</ta>
            <ta e="T242" id="Seg_7217" s="T241">тот.[NOM]</ta>
            <ta e="T243" id="Seg_7218" s="T242">этот</ta>
            <ta e="T244" id="Seg_7219" s="T243">приходить-CVB.SIM</ta>
            <ta e="T245" id="Seg_7220" s="T244">еще.не-3SG</ta>
            <ta e="T246" id="Seg_7221" s="T245">Лыгый-PL.[NOM]</ta>
            <ta e="T247" id="Seg_7222" s="T246">говорить-CVB.SEQ</ta>
            <ta e="T248" id="Seg_7223" s="T247">есть</ta>
            <ta e="T249" id="Seg_7224" s="T248">быть-PST1-3PL</ta>
            <ta e="T250" id="Seg_7225" s="T249">говорят</ta>
            <ta e="T252" id="Seg_7226" s="T251">ээ</ta>
            <ta e="T253" id="Seg_7227" s="T252">кто-PL.[NOM]</ta>
            <ta e="T254" id="Seg_7228" s="T253">зашитый-PL.[NOM]</ta>
            <ta e="T255" id="Seg_7229" s="T254">говорить-PRS-3PL</ta>
            <ta e="T256" id="Seg_7230" s="T255">тот</ta>
            <ta e="T258" id="Seg_7231" s="T257">там</ta>
            <ta e="T259" id="Seg_7232" s="T258">тот.[NOM]</ta>
            <ta e="T260" id="Seg_7233" s="T259">причал.[NOM]</ta>
            <ta e="T261" id="Seg_7234" s="T260">далекий</ta>
            <ta e="T262" id="Seg_7235" s="T261">сторона-3SG-DAT/LOC</ta>
            <ta e="T263" id="Seg_7236" s="T262">MOD</ta>
            <ta e="T264" id="Seg_7237" s="T263">дом.[NOM]</ta>
            <ta e="T265" id="Seg_7238" s="T264">след-PL-3SG.[NOM]</ta>
            <ta e="T266" id="Seg_7239" s="T265">есть-3PL</ta>
            <ta e="T267" id="Seg_7240" s="T266">два</ta>
            <ta e="T268" id="Seg_7241" s="T267">три</ta>
            <ta e="T269" id="Seg_7242" s="T268">дом.[NOM]</ta>
            <ta e="T270" id="Seg_7243" s="T269">след-3SG.[NOM]</ta>
            <ta e="T272" id="Seg_7244" s="T271">там</ta>
            <ta e="T273" id="Seg_7245" s="T272">жить-PRS-3PL</ta>
            <ta e="T274" id="Seg_7246" s="T273">тот</ta>
            <ta e="T275" id="Seg_7247" s="T274">охотник-PL.[NOM]</ta>
            <ta e="T276" id="Seg_7248" s="T275">жить-TEMP-3PL</ta>
            <ta e="T277" id="Seg_7249" s="T276">Лыгый-PL.[NOM]</ta>
            <ta e="T278" id="Seg_7250" s="T277">приходить-PST2-3PL</ta>
            <ta e="T280" id="Seg_7251" s="T279">Лыгый-PL.[NOM]</ta>
            <ta e="T281" id="Seg_7252" s="T280">зашитый-PL.[NOM]</ta>
            <ta e="T283" id="Seg_7253" s="T282">тот</ta>
            <ta e="T284" id="Seg_7254" s="T283">приходить-PST2-3PL</ta>
            <ta e="T285" id="Seg_7255" s="T284">три</ta>
            <ta e="T286" id="Seg_7256" s="T285">жена-PROPR-3PL</ta>
            <ta e="T287" id="Seg_7257" s="T286">тот-PL.[NOM]</ta>
            <ta e="T289" id="Seg_7258" s="T288">один-3PL.[NOM]</ta>
            <ta e="T290" id="Seg_7259" s="T289">шаман.[NOM]</ta>
            <ta e="T292" id="Seg_7260" s="T291">два-3PL.[NOM]</ta>
            <ta e="T293" id="Seg_7261" s="T292">повар.[NOM]</ta>
            <ta e="T295" id="Seg_7262" s="T294">вот</ta>
            <ta e="T296" id="Seg_7263" s="T295">этот</ta>
            <ta e="T297" id="Seg_7264" s="T296">человек-ACC</ta>
            <ta e="T298" id="Seg_7265" s="T297">убить-PST1-3SG</ta>
            <ta e="T299" id="Seg_7266" s="T298">убить-PTCP.PST-EP-INSTR</ta>
            <ta e="T300" id="Seg_7267" s="T299">приходить-PST2-3PL</ta>
            <ta e="T301" id="Seg_7268" s="T300">тот-PL-ACC</ta>
            <ta e="T302" id="Seg_7269" s="T301">повар.[NOM]</ta>
            <ta e="T303" id="Seg_7270" s="T302">делать-CVB.PURP</ta>
            <ta e="T304" id="Seg_7271" s="T303">принести-PST2-3PL</ta>
            <ta e="T305" id="Seg_7272" s="T304">плохой.[NOM]</ta>
            <ta e="T306" id="Seg_7273" s="T305">человек-PL.[NOM]</ta>
            <ta e="T308" id="Seg_7274" s="T307">как</ta>
            <ta e="T309" id="Seg_7275" s="T308">будто</ta>
            <ta e="T310" id="Seg_7276" s="T309">что-3PL.[NOM]</ta>
            <ta e="T311" id="Seg_7277" s="T310">хулиган-PL.[NOM]</ta>
            <ta e="T312" id="Seg_7278" s="T311">очевидно</ta>
            <ta e="T313" id="Seg_7279" s="T312">то</ta>
            <ta e="T315" id="Seg_7280" s="T314">тот</ta>
            <ta e="T316" id="Seg_7281" s="T315">приходить-CVB.SEQ</ta>
            <ta e="T317" id="Seg_7282" s="T316">приходить-CVB.SEQ-3PL</ta>
            <ta e="T318" id="Seg_7283" s="T317">сюда</ta>
            <ta e="T319" id="Seg_7284" s="T318">приходить-PST2-3PL</ta>
            <ta e="T320" id="Seg_7285" s="T319">охотник-PL.[NOM]</ta>
            <ta e="T321" id="Seg_7286" s="T320">один-3PL.[NOM]</ta>
            <ta e="T322" id="Seg_7287" s="T321">EMPH</ta>
            <ta e="T323" id="Seg_7288" s="T322">резвый.[NOM]</ta>
            <ta e="T325" id="Seg_7289" s="T324">один-3PL.[NOM]</ta>
            <ta e="T326" id="Seg_7290" s="T325">хорошо.видящий.[NOM]</ta>
            <ta e="T327" id="Seg_7291" s="T326">один-3PL.[NOM]</ta>
            <ta e="T328" id="Seg_7292" s="T327">стрелять-NMNZ-AG.[NOM]</ta>
            <ta e="T329" id="Seg_7293" s="T328">охотник-PL.[NOM]</ta>
            <ta e="T330" id="Seg_7294" s="T329">наши.[NOM]</ta>
            <ta e="T331" id="Seg_7295" s="T330">говорят</ta>
            <ta e="T332" id="Seg_7296" s="T331">рассказывать-PRS-3PL</ta>
            <ta e="T334" id="Seg_7297" s="T333">тот-PL.[NOM]</ta>
            <ta e="T335" id="Seg_7298" s="T334">однако</ta>
            <ta e="T336" id="Seg_7299" s="T335">человек-ACC</ta>
            <ta e="T337" id="Seg_7300" s="T336">истребить-EP-CAUS-CVB.SEQ</ta>
            <ta e="T338" id="Seg_7301" s="T337">приходить-CVB.SEQ</ta>
            <ta e="T339" id="Seg_7302" s="T338">приходить-CVB.SEQ-3PL</ta>
            <ta e="T340" id="Seg_7303" s="T339">этот</ta>
            <ta e="T341" id="Seg_7304" s="T340">старик-PL.[NOM]</ta>
            <ta e="T342" id="Seg_7305" s="T341">Малый</ta>
            <ta e="T343" id="Seg_7306" s="T342">говорить-CVB.SEQ</ta>
            <ta e="T344" id="Seg_7307" s="T343">место-DAT/LOC</ta>
            <ta e="T345" id="Seg_7308" s="T344">Поротов</ta>
            <ta e="T346" id="Seg_7309" s="T345">говорить-CVB.SEQ</ta>
            <ta e="T347" id="Seg_7310" s="T346">человек.[NOM]</ta>
            <ta e="T348" id="Seg_7311" s="T347">есть</ta>
            <ta e="T349" id="Seg_7312" s="T348">быть-PST1-3SG</ta>
            <ta e="T351" id="Seg_7313" s="T350">тот-DAT/LOC</ta>
            <ta e="T352" id="Seg_7314" s="T351">идти-PST2-3PL</ta>
            <ta e="T353" id="Seg_7315" s="T352">этот</ta>
            <ta e="T354" id="Seg_7316" s="T353">человек-PL.[NOM]</ta>
            <ta e="T356" id="Seg_7317" s="T355">эй</ta>
            <ta e="T357" id="Seg_7318" s="T356">друг.[NOM]</ta>
            <ta e="T358" id="Seg_7319" s="T357">такой</ta>
            <ta e="T359" id="Seg_7320" s="T358">такой</ta>
            <ta e="T360" id="Seg_7321" s="T359">войско.[NOM]</ta>
            <ta e="T361" id="Seg_7322" s="T360">морда-PROPR</ta>
            <ta e="T362" id="Seg_7323" s="T361">собака-PL.[NOM]</ta>
            <ta e="T363" id="Seg_7324" s="T362">колоть-PRS-3PL</ta>
            <ta e="T364" id="Seg_7325" s="T363">убить-PTCP.FUT-AG-PL.[NOM]</ta>
            <ta e="T365" id="Seg_7326" s="T364">приходить-3PL</ta>
            <ta e="T366" id="Seg_7327" s="T365">зашитый-PL.[NOM]</ta>
            <ta e="T368" id="Seg_7328" s="T367">вот</ta>
            <ta e="T369" id="Seg_7329" s="T368">тот</ta>
            <ta e="T370" id="Seg_7330" s="T369">спасаться-PRS-3PL</ta>
            <ta e="T371" id="Seg_7331" s="T370">Малый-DAT/LOC</ta>
            <ta e="T372" id="Seg_7332" s="T371">спасаться-PST2-3PL</ta>
            <ta e="T374" id="Seg_7333" s="T373">тот</ta>
            <ta e="T375" id="Seg_7334" s="T374">спасаться-CVB.SEQ</ta>
            <ta e="T376" id="Seg_7335" s="T375">доезжать-PST2-3PL</ta>
            <ta e="T377" id="Seg_7336" s="T376">Поротов-DAT/LOC</ta>
            <ta e="T378" id="Seg_7337" s="T377">доезжать-CVB.SEQ-3PL</ta>
            <ta e="T379" id="Seg_7338" s="T378">EMPH</ta>
            <ta e="T380" id="Seg_7339" s="T379">рассказывать-PRS-3PL</ta>
            <ta e="T381" id="Seg_7340" s="T380">эй</ta>
            <ta e="T382" id="Seg_7341" s="T381">такой-PL.[NOM]</ta>
            <ta e="T383" id="Seg_7342" s="T382">приходить-PST1-3PL</ta>
            <ta e="T384" id="Seg_7343" s="T383">человек-ACC</ta>
            <ta e="T385" id="Seg_7344" s="T384">истребить-CAUS-PST2-3PL</ta>
            <ta e="T386" id="Seg_7345" s="T385">широкий</ta>
            <ta e="T387" id="Seg_7346" s="T386">озеро-ABL</ta>
            <ta e="T388" id="Seg_7347" s="T387">течь-CVB.SEQ-3PL</ta>
            <ta e="T389" id="Seg_7348" s="T388">новый</ta>
            <ta e="T390" id="Seg_7349" s="T389">река.[NOM]</ta>
            <ta e="T391" id="Seg_7350" s="T390">вдоль</ta>
            <ta e="T392" id="Seg_7351" s="T391">приходить-PST2-3PL</ta>
            <ta e="T393" id="Seg_7352" s="T392">говорить-CVB.SEQ</ta>
            <ta e="T395" id="Seg_7353" s="T394">тот</ta>
            <ta e="T396" id="Seg_7354" s="T395">старик-DAT/LOC</ta>
            <ta e="T397" id="Seg_7355" s="T396">доезжать-CVB.SEQ-3PL</ta>
            <ta e="T398" id="Seg_7356" s="T397">этот</ta>
            <ta e="T399" id="Seg_7357" s="T398">человек-PL.[NOM]</ta>
            <ta e="T400" id="Seg_7358" s="T399">вот</ta>
            <ta e="T402" id="Seg_7359" s="T401">вот</ta>
            <ta e="T403" id="Seg_7360" s="T402">хорошо.видящий.[NOM]</ta>
            <ta e="T404" id="Seg_7361" s="T403">видеть-FREQ-CVB.SIM</ta>
            <ta e="T405" id="Seg_7362" s="T404">сидеть.[IMP.2SG]</ta>
            <ta e="T406" id="Seg_7363" s="T405">вверх</ta>
            <ta e="T407" id="Seg_7364" s="T406">сопка-DAT/LOC</ta>
            <ta e="T408" id="Seg_7365" s="T407">выйти-CVB.SEQ-2SG</ta>
            <ta e="T409" id="Seg_7366" s="T408">говорить-CVB.SEQ</ta>
            <ta e="T411" id="Seg_7367" s="T410">один-3PL.[NOM]</ta>
            <ta e="T412" id="Seg_7368" s="T411">стрелять-NMNZ-AG-3PL.[NOM]</ta>
            <ta e="T413" id="Seg_7369" s="T412">после.того</ta>
            <ta e="T414" id="Seg_7370" s="T413">входить-CAUS-ITER-PST2.[3SG]</ta>
            <ta e="T415" id="Seg_7371" s="T414">друг-PL-3SG-ACC</ta>
            <ta e="T416" id="Seg_7372" s="T415">так</ta>
            <ta e="T417" id="Seg_7373" s="T416">делать-CVB.SEQ</ta>
            <ta e="T418" id="Seg_7374" s="T417">после</ta>
            <ta e="T419" id="Seg_7375" s="T418">вот</ta>
            <ta e="T420" id="Seg_7376" s="T419">кровь.[NOM]</ta>
            <ta e="T421" id="Seg_7377" s="T420">похлёбка.[NOM]</ta>
            <ta e="T422" id="Seg_7378" s="T421">вариться-PST2-3PL</ta>
            <ta e="T423" id="Seg_7379" s="T422">этот</ta>
            <ta e="T424" id="Seg_7380" s="T423">старик.[NOM]</ta>
            <ta e="T426" id="Seg_7381" s="T425">шест.[NOM]</ta>
            <ta e="T427" id="Seg_7382" s="T426">чум-PROPR-3PL</ta>
            <ta e="T429" id="Seg_7383" s="T428">вот</ta>
            <ta e="T430" id="Seg_7384" s="T429">похлёбка-VBZ-CVB.SEQ</ta>
            <ta e="T431" id="Seg_7385" s="T430">после</ta>
            <ta e="T432" id="Seg_7386" s="T431">EMPH</ta>
            <ta e="T433" id="Seg_7387" s="T432">похлёбка-3SG-DAT/LOC</ta>
            <ta e="T434" id="Seg_7388" s="T433">MOD</ta>
            <ta e="T435" id="Seg_7389" s="T434">кто-ACC</ta>
            <ta e="T436" id="Seg_7390" s="T435">разрезать-PST2.[3SG]</ta>
            <ta e="T438" id="Seg_7391" s="T437">%%</ta>
            <ta e="T439" id="Seg_7392" s="T438">говорить-HAB-3PL</ta>
            <ta e="T440" id="Seg_7393" s="T439">тогда</ta>
            <ta e="T441" id="Seg_7394" s="T440">теперь</ta>
            <ta e="T442" id="Seg_7395" s="T441">%%</ta>
            <ta e="T443" id="Seg_7396" s="T442">леска</ta>
            <ta e="T445" id="Seg_7397" s="T444">леска-ACC</ta>
            <ta e="T446" id="Seg_7398" s="T445">табак-DAT/LOC</ta>
            <ta e="T447" id="Seg_7399" s="T446">как</ta>
            <ta e="T448" id="Seg_7400" s="T447">подобно</ta>
            <ta e="T449" id="Seg_7401" s="T448">разрезать-CVB.SIM-разрезать-CVB.SIM</ta>
            <ta e="T450" id="Seg_7402" s="T449">EMPH</ta>
            <ta e="T451" id="Seg_7403" s="T450">похлёбка-DAT/LOC</ta>
            <ta e="T452" id="Seg_7404" s="T451">лить-PST2.[3SG]</ta>
            <ta e="T453" id="Seg_7405" s="T452">EMPH</ta>
            <ta e="T455" id="Seg_7406" s="T454">проголодаться-CVB.SEQ</ta>
            <ta e="T456" id="Seg_7407" s="T455">идти-PTCP.PRS</ta>
            <ta e="T457" id="Seg_7408" s="T456">человек-PL.[NOM]</ta>
            <ta e="T458" id="Seg_7409" s="T457">похлёбка.[NOM]</ta>
            <ta e="T459" id="Seg_7410" s="T458">пить-FUT-3PL</ta>
            <ta e="T460" id="Seg_7411" s="T459">говорить-CVB.SEQ</ta>
            <ta e="T461" id="Seg_7412" s="T460">тот</ta>
            <ta e="T462" id="Seg_7413" s="T461">пить-FUT-3PL</ta>
            <ta e="T463" id="Seg_7414" s="T462">говорить-CVB.SEQ</ta>
            <ta e="T464" id="Seg_7415" s="T463">тот</ta>
            <ta e="T465" id="Seg_7416" s="T464">похлёбка</ta>
            <ta e="T466" id="Seg_7417" s="T465">разрезать-CAUS-PST2.[3SG]</ta>
            <ta e="T467" id="Seg_7418" s="T466">EMPH</ta>
            <ta e="T469" id="Seg_7419" s="T468">так</ta>
            <ta e="T470" id="Seg_7420" s="T469">глотать-NMNZ.[NOM]</ta>
            <ta e="T471" id="Seg_7421" s="T470">хотеть-TEMP-3SG</ta>
            <ta e="T472" id="Seg_7422" s="T471">тот</ta>
            <ta e="T473" id="Seg_7423" s="T472">начальник-3PL-ACC</ta>
            <ta e="T474" id="Seg_7424" s="T473">горло-3SG-ACC</ta>
            <ta e="T475" id="Seg_7425" s="T474">эй</ta>
            <ta e="T476" id="Seg_7426" s="T475">бить-FUT-EP-IMP.2PL</ta>
            <ta e="T477" id="Seg_7427" s="T476">говорить-CVB.SEQ</ta>
            <ta e="T478" id="Seg_7428" s="T477">борода-EP</ta>
            <ta e="T479" id="Seg_7429" s="T478">ээ</ta>
            <ta e="T480" id="Seg_7430" s="T479">кто-EP-INSTR</ta>
            <ta e="T481" id="Seg_7431" s="T480">пальма-INSTR</ta>
            <ta e="T483" id="Seg_7432" s="T482">тогда</ta>
            <ta e="T484" id="Seg_7433" s="T483">только</ta>
            <ta e="T485" id="Seg_7434" s="T484">побеждать-FUT-1PL</ta>
            <ta e="T486" id="Seg_7435" s="T485">говорить-PST2.[3SG]</ta>
            <ta e="T487" id="Seg_7436" s="T486">EMPH</ta>
            <ta e="T488" id="Seg_7437" s="T487">потом</ta>
            <ta e="T489" id="Seg_7438" s="T488">говорить-PST1-3SG</ta>
            <ta e="T490" id="Seg_7439" s="T489">мочь-FUT-1PL</ta>
            <ta e="T491" id="Seg_7440" s="T490">как</ta>
            <ta e="T492" id="Seg_7441" s="T491">INDEF</ta>
            <ta e="T494" id="Seg_7442" s="T493">вот</ta>
            <ta e="T495" id="Seg_7443" s="T494">тот</ta>
            <ta e="T496" id="Seg_7444" s="T495">приходить-PST2-3PL</ta>
            <ta e="T497" id="Seg_7445" s="T496">тот</ta>
            <ta e="T498" id="Seg_7446" s="T497">похлёбка-VBZ-PST2-3PL</ta>
            <ta e="T499" id="Seg_7447" s="T498">вот</ta>
            <ta e="T500" id="Seg_7448" s="T499">пить-PRS-3PL</ta>
            <ta e="T501" id="Seg_7449" s="T500">похлёбка-VBZ-IMP.2PL</ta>
            <ta e="T502" id="Seg_7450" s="T501">говорить-PST2.[3SG]</ta>
            <ta e="T503" id="Seg_7451" s="T502">EMPH</ta>
            <ta e="T504" id="Seg_7452" s="T503">похлёбка-VBZ-PST2-3PL</ta>
            <ta e="T506" id="Seg_7453" s="T505">вот</ta>
            <ta e="T507" id="Seg_7454" s="T506">тот</ta>
            <ta e="T508" id="Seg_7455" s="T507">кипеть-EP-CAUS-CVB.SIM</ta>
            <ta e="T509" id="Seg_7456" s="T508">лежать-TEMP-3PL</ta>
            <ta e="T510" id="Seg_7457" s="T509">вот</ta>
            <ta e="T511" id="Seg_7458" s="T510">приходить-PST2-3PL</ta>
            <ta e="T512" id="Seg_7459" s="T511">EMPH</ta>
            <ta e="T513" id="Seg_7460" s="T512">тот</ta>
            <ta e="T514" id="Seg_7461" s="T513">изверг-PL-EP-2SG.[NOM]</ta>
            <ta e="T516" id="Seg_7462" s="T515">приходить-CVB.SEQ-3PL</ta>
            <ta e="T517" id="Seg_7463" s="T516">жена-PL-ACC</ta>
            <ta e="T518" id="Seg_7464" s="T517">входить-CAUS-ITER-CVB.SEQ</ta>
            <ta e="T519" id="Seg_7465" s="T518">вот</ta>
            <ta e="T520" id="Seg_7466" s="T519">EMPH</ta>
            <ta e="T521" id="Seg_7467" s="T520">старик.[NOM]</ta>
            <ta e="T522" id="Seg_7468" s="T521">принимать-PRS.[3SG]</ta>
            <ta e="T523" id="Seg_7469" s="T522">входить-CAUS-ITER-PRS.[3SG]</ta>
            <ta e="T525" id="Seg_7470" s="T524">эй</ta>
            <ta e="T526" id="Seg_7471" s="T525">такой-PL.[NOM]</ta>
            <ta e="T527" id="Seg_7472" s="T526">такой-PL.[NOM]</ta>
            <ta e="T528" id="Seg_7473" s="T527">новость-PART</ta>
            <ta e="T529" id="Seg_7474" s="T528">рассказывать-IMP.2PL</ta>
            <ta e="T530" id="Seg_7475" s="T529">говорить-CVB.SEQ</ta>
            <ta e="T532" id="Seg_7476" s="T531">вот</ta>
            <ta e="T533" id="Seg_7477" s="T532">тот</ta>
            <ta e="T534" id="Seg_7478" s="T533">старик-DAT/LOC</ta>
            <ta e="T535" id="Seg_7479" s="T534">доезжать-CVB.SEQ</ta>
            <ta e="T536" id="Seg_7480" s="T535">рассказывать-PRS-3PL</ta>
            <ta e="T537" id="Seg_7481" s="T536">EMPH</ta>
            <ta e="T538" id="Seg_7482" s="T537">старуха-ACC</ta>
            <ta e="T539" id="Seg_7483" s="T538">принести-PST1-1PL</ta>
            <ta e="T540" id="Seg_7484" s="T539">1PL.[NOM]</ta>
            <ta e="T541" id="Seg_7485" s="T540">тот.[NOM]</ta>
            <ta e="T542" id="Seg_7486" s="T541">земля-AG-1PL.[NOM]</ta>
            <ta e="T543" id="Seg_7487" s="T542">говорить-PRS-1PL</ta>
            <ta e="T545" id="Seg_7488" s="T544">шаман.[NOM]</ta>
            <ta e="T546" id="Seg_7489" s="T545">старуха.[NOM]</ta>
            <ta e="T547" id="Seg_7490" s="T546">тот.[NOM]</ta>
            <ta e="T548" id="Seg_7491" s="T547">повар-PL-1PL.[NOM]</ta>
            <ta e="T550" id="Seg_7492" s="T549">тогда</ta>
            <ta e="T551" id="Seg_7493" s="T550">тот</ta>
            <ta e="T552" id="Seg_7494" s="T551">шаман.[NOM]</ta>
            <ta e="T553" id="Seg_7495" s="T552">старуха.[NOM]</ta>
            <ta e="T554" id="Seg_7496" s="T553">земля-VBZ-CVB.SEQ</ta>
            <ta e="T555" id="Seg_7497" s="T554">принести-PST1-3SG</ta>
            <ta e="T556" id="Seg_7498" s="T555">1PL-DAT/LOC</ta>
            <ta e="T557" id="Seg_7499" s="T556">2PL-DAT/LOC</ta>
            <ta e="T558" id="Seg_7500" s="T557">говорить-CVB.SEQ</ta>
            <ta e="T559" id="Seg_7501" s="T558">тот</ta>
            <ta e="T560" id="Seg_7502" s="T559">рассказывать-PRS-3PL</ta>
            <ta e="T561" id="Seg_7503" s="T560">EMPH</ta>
            <ta e="T562" id="Seg_7504" s="T561">человек-ACC</ta>
            <ta e="T563" id="Seg_7505" s="T562">истребить-EP-CAUS-PST2-3PL</ta>
            <ta e="T565" id="Seg_7506" s="T564">кровь-3PL-ACC</ta>
            <ta e="T566" id="Seg_7507" s="T565">MOD</ta>
            <ta e="T567" id="Seg_7508" s="T566">нож-3PL-ACC</ta>
            <ta e="T568" id="Seg_7509" s="T567">здесь</ta>
            <ta e="T569" id="Seg_7510" s="T568">вытирать-PRS-3PL</ta>
            <ta e="T570" id="Seg_7511" s="T569">быть-PST2.[3SG]</ta>
            <ta e="T571" id="Seg_7512" s="T570">человек-PL-ACC</ta>
            <ta e="T572" id="Seg_7513" s="T571">разрезать-CVB.SIM</ta>
            <ta e="T573" id="Seg_7514" s="T572">разрезать-CVB.SIM</ta>
            <ta e="T574" id="Seg_7515" s="T573">есть-CVB.SIM</ta>
            <ta e="T575" id="Seg_7516" s="T574">есть-CVB.SIM</ta>
            <ta e="T576" id="Seg_7517" s="T575">есть-PRS-3PL</ta>
            <ta e="T578" id="Seg_7518" s="T577">о</ta>
            <ta e="T579" id="Seg_7519" s="T578">что.[NOM]</ta>
            <ta e="T580" id="Seg_7520" s="T579">имя-PROPR</ta>
            <ta e="T581" id="Seg_7521" s="T580">кровь-3SG-DAT/LOC</ta>
            <ta e="T582" id="Seg_7522" s="T581">замазать-EP-PASS/REFL-EP-PTCP.PST</ta>
            <ta e="T583" id="Seg_7523" s="T582">народ-PL-2PL=Q</ta>
            <ta e="T584" id="Seg_7524" s="T583">вот</ta>
            <ta e="T585" id="Seg_7525" s="T584">есть-IMP.2PL</ta>
            <ta e="T586" id="Seg_7526" s="T585">говорить-CVB.SEQ</ta>
            <ta e="T587" id="Seg_7527" s="T586">старик.[NOM]</ta>
            <ta e="T588" id="Seg_7528" s="T587">вот</ta>
            <ta e="T589" id="Seg_7529" s="T588">есть-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T591" id="Seg_7530" s="T590">вот</ta>
            <ta e="T592" id="Seg_7531" s="T591">тот</ta>
            <ta e="T593" id="Seg_7532" s="T592">есть</ta>
            <ta e="T594" id="Seg_7533" s="T593">есть-EP-CAUS-PTCP.PST-3SG-ACC</ta>
            <ta e="T595" id="Seg_7534" s="T594">после.того</ta>
            <ta e="T596" id="Seg_7535" s="T595">вот</ta>
            <ta e="T597" id="Seg_7536" s="T596">тот</ta>
            <ta e="T598" id="Seg_7537" s="T597">о</ta>
            <ta e="T599" id="Seg_7538" s="T598">небо-1PL.[NOM]</ta>
            <ta e="T600" id="Seg_7539" s="T599">как</ta>
            <ta e="T601" id="Seg_7540" s="T600">как</ta>
            <ta e="T602" id="Seg_7541" s="T601">быть-PRS.[3SG]</ta>
            <ta e="T603" id="Seg_7542" s="T602">вот</ta>
            <ta e="T604" id="Seg_7543" s="T603">туча-1PL.[NOM]</ta>
            <ta e="T605" id="Seg_7544" s="T604">прибавляься-PRS.[3SG]</ta>
            <ta e="T607" id="Seg_7545" s="T606">что</ta>
            <ta e="T608" id="Seg_7546" s="T607">быть-PRS.[3SG]</ta>
            <ta e="T609" id="Seg_7547" s="T608">говорить-PST2-3SG</ta>
            <ta e="T611" id="Seg_7548" s="T610">вот</ta>
            <ta e="T612" id="Seg_7549" s="T611">тот</ta>
            <ta e="T613" id="Seg_7550" s="T612">два</ta>
            <ta e="T614" id="Seg_7551" s="T613">бодрый-PROPR-ACC</ta>
            <ta e="T615" id="Seg_7552" s="T614">два</ta>
            <ta e="T616" id="Seg_7553" s="T615">сторона-3SG-DAT/LOC</ta>
            <ta e="T617" id="Seg_7554" s="T616">посадить-PST2.[3SG]</ta>
            <ta e="T618" id="Seg_7555" s="T617">EMPH</ta>
            <ta e="T620" id="Seg_7556" s="T619">вот</ta>
            <ta e="T621" id="Seg_7557" s="T620">тот</ta>
            <ta e="T622" id="Seg_7558" s="T621">глотать-NMNZ.[NOM]</ta>
            <ta e="T623" id="Seg_7559" s="T622">делать-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T624" id="Seg_7560" s="T623">горло-3SG-ACC</ta>
            <ta e="T625" id="Seg_7561" s="T624">колоть-CVB.SIM</ta>
            <ta e="T626" id="Seg_7562" s="T625">бить-EP-PST2-3PL</ta>
            <ta e="T627" id="Seg_7563" s="T626">пальма-INSTR</ta>
            <ta e="T629" id="Seg_7564" s="T628">теперь</ta>
            <ta e="T630" id="Seg_7565" s="T629">тот.[NOM]</ta>
            <ta e="T631" id="Seg_7566" s="T630">побить-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T632" id="Seg_7567" s="T631">говорить-PRS.[3SG]</ta>
            <ta e="T633" id="Seg_7568" s="T632">кино-PL-DAT/LOC</ta>
            <ta e="T634" id="Seg_7569" s="T633">как</ta>
            <ta e="T636" id="Seg_7570" s="T635">тот</ta>
            <ta e="T637" id="Seg_7571" s="T636">побеждать-PST2-3PL</ta>
            <ta e="T638" id="Seg_7572" s="T637">говорят</ta>
            <ta e="T639" id="Seg_7573" s="T638">тот-3SG-3PL-ACC</ta>
            <ta e="T641" id="Seg_7574" s="T640">вот</ta>
            <ta e="T642" id="Seg_7575" s="T641">другой.из.двух-3PL-ACC</ta>
            <ta e="T643" id="Seg_7576" s="T642">убить-ITER-PST2-3PL</ta>
            <ta e="T645" id="Seg_7577" s="T644">семь</ta>
            <ta e="T646" id="Seg_7578" s="T645">человек-3SG.[NOM]</ta>
            <ta e="T647" id="Seg_7579" s="T646">быть-PST2.[3SG]</ta>
            <ta e="T648" id="Seg_7580" s="T647">Q</ta>
            <ta e="T649" id="Seg_7581" s="T648">сколько</ta>
            <ta e="T650" id="Seg_7582" s="T649">Q</ta>
            <ta e="T652" id="Seg_7583" s="T651">вот</ta>
            <ta e="T653" id="Seg_7584" s="T652">потом</ta>
            <ta e="T654" id="Seg_7585" s="T653">жена-PL.[NOM]</ta>
            <ta e="T655" id="Seg_7586" s="T654">оставаться-PTCP.PST-3PL-DAT/LOC</ta>
            <ta e="T656" id="Seg_7587" s="T655">этот</ta>
            <ta e="T657" id="Seg_7588" s="T656">жена-PL.[NOM]</ta>
            <ta e="T658" id="Seg_7589" s="T657">вот</ta>
            <ta e="T659" id="Seg_7590" s="T658">ссориться-PRS-3PL</ta>
            <ta e="T660" id="Seg_7591" s="T659">говорят</ta>
            <ta e="T661" id="Seg_7592" s="T660">нож-3PL-ACC</ta>
            <ta e="T662" id="Seg_7593" s="T661">отнимать-EP-RECP/COLL-CVB.SEQ-3PL</ta>
            <ta e="T664" id="Seg_7594" s="T663">1SG.[NOM]</ta>
            <ta e="T665" id="Seg_7595" s="T664">мужчина-EP-1SG.[NOM]</ta>
            <ta e="T666" id="Seg_7596" s="T665">нож-3SG.[NOM]</ta>
            <ta e="T667" id="Seg_7597" s="T666">1SG.[NOM]</ta>
            <ta e="T668" id="Seg_7598" s="T667">мужчина-EP-1SG.[NOM]</ta>
            <ta e="T669" id="Seg_7599" s="T668">нож-3SG.[NOM]</ta>
            <ta e="T670" id="Seg_7600" s="T669">говорить-EP-RECP/COLL-EMOT-PRS-3PL</ta>
            <ta e="T671" id="Seg_7601" s="T670">говорят</ta>
            <ta e="T672" id="Seg_7602" s="T671">бедненький-PL.[NOM]</ta>
            <ta e="T674" id="Seg_7603" s="T673">что</ta>
            <ta e="T675" id="Seg_7604" s="T674">сука</ta>
            <ta e="T676" id="Seg_7605" s="T675">девушка-3PL.[NOM]=Q</ta>
            <ta e="T677" id="Seg_7606" s="T676">тот-PL-ACC</ta>
            <ta e="T678" id="Seg_7607" s="T677">умирать-MULT-CVB.SEQ</ta>
            <ta e="T679" id="Seg_7608" s="T678">бросать-EP-IMP.2PL</ta>
            <ta e="T680" id="Seg_7609" s="T679">говорить-PST2.[3SG]</ta>
            <ta e="T681" id="Seg_7610" s="T680">EMPH</ta>
            <ta e="T682" id="Seg_7611" s="T681">что.[NOM]</ta>
            <ta e="T683" id="Seg_7612" s="T682">жестокий.[NOM]</ta>
            <ta e="T684" id="Seg_7613" s="T683">старик-3SG.[NOM]</ta>
            <ta e="T685" id="Seg_7614" s="T684">MOD</ta>
            <ta e="T686" id="Seg_7615" s="T685">EMPH</ta>
            <ta e="T687" id="Seg_7616" s="T686">наш</ta>
            <ta e="T688" id="Seg_7617" s="T687">Поротов.[NOM]</ta>
            <ta e="T690" id="Seg_7618" s="T689">умирать-MULT-CVB.SEQ</ta>
            <ta e="T691" id="Seg_7619" s="T690">бросать-PST2-3PL</ta>
            <ta e="T692" id="Seg_7620" s="T691">беда-PROPR-PL.[NOM]</ta>
            <ta e="T694" id="Seg_7621" s="T693">потом</ta>
            <ta e="T695" id="Seg_7622" s="T694">тот</ta>
            <ta e="T696" id="Seg_7623" s="T695">старуха-ACC</ta>
            <ta e="T697" id="Seg_7624" s="T696">вот</ta>
            <ta e="T698" id="Seg_7625" s="T697">убить-CVB.SIM</ta>
            <ta e="T699" id="Seg_7626" s="T698">пытаться-PRS-3PL</ta>
            <ta e="T700" id="Seg_7627" s="T699">шаман.[NOM]</ta>
            <ta e="T701" id="Seg_7628" s="T700">старуха-ACC</ta>
            <ta e="T703" id="Seg_7629" s="T702">вода-DAT/LOC</ta>
            <ta e="T704" id="Seg_7630" s="T703">EMPH</ta>
            <ta e="T705" id="Seg_7631" s="T704">тонуть-CAUS-PRS-3PL</ta>
            <ta e="T706" id="Seg_7632" s="T705">вот</ta>
            <ta e="T707" id="Seg_7633" s="T706">якор-VBZ-CVB.SEQ</ta>
            <ta e="T708" id="Seg_7634" s="T707">после</ta>
            <ta e="T710" id="Seg_7635" s="T709">совсем</ta>
            <ta e="T711" id="Seg_7636" s="T710">тонуть-NEG.[3SG]</ta>
            <ta e="T713" id="Seg_7637" s="T712">испробовать.всё-CVB.SIM</ta>
            <ta e="T714" id="Seg_7638" s="T713">пытаться-CVB.SEQ</ta>
            <ta e="T715" id="Seg_7639" s="T714">огонь-3SG-ACC</ta>
            <ta e="T716" id="Seg_7640" s="T715">зажигать-CVB.SEQ</ta>
            <ta e="T717" id="Seg_7641" s="T716">EMPH</ta>
            <ta e="T718" id="Seg_7642" s="T717">огонь.[NOM]</ta>
            <ta e="T719" id="Seg_7643" s="T718">нутро-3SG-DAT/LOC</ta>
            <ta e="T720" id="Seg_7644" s="T719">бросать-PRS-3PL</ta>
            <ta e="T722" id="Seg_7645" s="T721">огонь-3PL.[NOM]</ta>
            <ta e="T723" id="Seg_7646" s="T722">чернеть-INCH</ta>
            <ta e="T724" id="Seg_7647" s="T723">спать-PRS.[3SG]</ta>
            <ta e="T725" id="Seg_7648" s="T724">говорят</ta>
            <ta e="T727" id="Seg_7649" s="T726">там</ta>
            <ta e="T728" id="Seg_7650" s="T727">EMPH</ta>
            <ta e="T729" id="Seg_7651" s="T728">говорить-PST2.[3SG]</ta>
            <ta e="T730" id="Seg_7652" s="T729">EMPH</ta>
            <ta e="T731" id="Seg_7653" s="T730">миленький-PL.[NOM]</ta>
            <ta e="T732" id="Seg_7654" s="T731">1SG.[NOM]</ta>
            <ta e="T733" id="Seg_7655" s="T732">солнце.[NOM]</ta>
            <ta e="T734" id="Seg_7656" s="T733">старуха.[NOM]</ta>
            <ta e="T735" id="Seg_7657" s="T734">быть-PST1-1SG</ta>
            <ta e="T736" id="Seg_7658" s="T735">эй</ta>
            <ta e="T737" id="Seg_7659" s="T736">злой.дух.[NOM]</ta>
            <ta e="T738" id="Seg_7660" s="T737">быть-PST2.NEG-1SG</ta>
            <ta e="T740" id="Seg_7661" s="T739">люди-1SG-ACC</ta>
            <ta e="T741" id="Seg_7662" s="T740">истребить-PST2-3PL</ta>
            <ta e="T742" id="Seg_7663" s="T741">этот</ta>
            <ta e="T743" id="Seg_7664" s="T742">MOD</ta>
            <ta e="T744" id="Seg_7665" s="T743">человек-ACC</ta>
            <ta e="T745" id="Seg_7666" s="T744">животное-ACC</ta>
            <ta e="T746" id="Seg_7667" s="T745">видеть-CVB.PURP-1SG</ta>
            <ta e="T747" id="Seg_7668" s="T746">земля-VBZ-CVB.SEQ</ta>
            <ta e="T748" id="Seg_7669" s="T747">приходить-PST2-EP-1SG</ta>
            <ta e="T750" id="Seg_7670" s="T749">1SG-ACC</ta>
            <ta e="T751" id="Seg_7671" s="T750">мука-VBZ-NEG-IMP.2PL</ta>
            <ta e="T752" id="Seg_7672" s="T751">1SG.[NOM]</ta>
            <ta e="T753" id="Seg_7673" s="T752">такой</ta>
            <ta e="T754" id="Seg_7674" s="T753">вот</ta>
            <ta e="T755" id="Seg_7675" s="T754">умирать-FUT-1SG</ta>
            <ta e="T756" id="Seg_7676" s="T755">NEG-3SG</ta>
            <ta e="T757" id="Seg_7677" s="T756">говорить-PRS.[3SG]</ta>
            <ta e="T759" id="Seg_7678" s="T758">девушка.[NOM]</ta>
            <ta e="T760" id="Seg_7679" s="T759">ребенок-3SG.[NOM]</ta>
            <ta e="T761" id="Seg_7680" s="T760">впервые</ta>
            <ta e="T762" id="Seg_7681" s="T761">грязный.[NOM]</ta>
            <ta e="T763" id="Seg_7682" s="T762">становиться-PST2.[3SG]</ta>
            <ta e="T764" id="Seg_7683" s="T763">девушка.[NOM]</ta>
            <ta e="T765" id="Seg_7684" s="T764">ребенок-3SG.[NOM]</ta>
            <ta e="T766" id="Seg_7685" s="T765">шея-1SG-INSTR</ta>
            <ta e="T767" id="Seg_7686" s="T766">перешагивать-EP-IMP.3SG</ta>
            <ta e="T768" id="Seg_7687" s="T767">тогда</ta>
            <ta e="T769" id="Seg_7688" s="T768">умирать-FUT-1SG</ta>
            <ta e="T770" id="Seg_7689" s="T769">говорить-PRS.[3SG]</ta>
            <ta e="T771" id="Seg_7690" s="T770">тот</ta>
            <ta e="T772" id="Seg_7691" s="T771">шея-1SG-INSTR</ta>
            <ta e="T773" id="Seg_7692" s="T772">перешагивать-EP-CAUS-PST2-3PL</ta>
            <ta e="T775" id="Seg_7693" s="T774">вот</ta>
            <ta e="T776" id="Seg_7694" s="T775">тот</ta>
            <ta e="T777" id="Seg_7695" s="T776">старуха-3PL.[NOM]</ta>
            <ta e="T778" id="Seg_7696" s="T777">умирать-CVB.SEQ</ta>
            <ta e="T779" id="Seg_7697" s="T778">оставаться-PST2.[3SG]</ta>
            <ta e="T780" id="Seg_7698" s="T779">вот</ta>
            <ta e="T781" id="Seg_7699" s="T780">последний-3SG.[NOM]</ta>
            <ta e="T782" id="Seg_7700" s="T781">тот</ta>
            <ta e="T783" id="Seg_7701" s="T782">рассказывать-PST2-3PL</ta>
            <ta e="T784" id="Seg_7702" s="T783">помнить-NEG-1SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_7703" s="T1">adv</ta>
            <ta e="T3" id="Seg_7704" s="T2">adv</ta>
            <ta e="T4" id="Seg_7705" s="T3">v-v:cvb</ta>
            <ta e="T5" id="Seg_7706" s="T4">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T6" id="Seg_7707" s="T5">n-n:(num).[n:case]</ta>
            <ta e="T8" id="Seg_7708" s="T7">n-n:(num).[n:case]</ta>
            <ta e="T9" id="Seg_7709" s="T8">v-v:tense-v:pred.pn</ta>
            <ta e="T10" id="Seg_7710" s="T9">n-n:(num).[n:case]</ta>
            <ta e="T12" id="Seg_7711" s="T11">interj</ta>
            <ta e="T13" id="Seg_7712" s="T12">propr-n:poss-n:case</ta>
            <ta e="T14" id="Seg_7713" s="T13">n.[n:case]</ta>
            <ta e="T15" id="Seg_7714" s="T14">v-v:tense.[v:pred.pn]</ta>
            <ta e="T16" id="Seg_7715" s="T15">v-v:tense-v:pred.pn</ta>
            <ta e="T18" id="Seg_7716" s="T17">que</ta>
            <ta e="T19" id="Seg_7717" s="T18">v-v:mood-v:pred.pn</ta>
            <ta e="T21" id="Seg_7718" s="T20">interj</ta>
            <ta e="T22" id="Seg_7719" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_7720" s="T22">dempro</ta>
            <ta e="T24" id="Seg_7721" s="T23">n.[n:case]</ta>
            <ta e="T25" id="Seg_7722" s="T24">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T26" id="Seg_7723" s="T25">v-v:tense-v:pred.pn</ta>
            <ta e="T27" id="Seg_7724" s="T26">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T28" id="Seg_7725" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_7726" s="T28">n-n:(num).[n:case]</ta>
            <ta e="T30" id="Seg_7727" s="T29">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T31" id="Seg_7728" s="T30">v-v:cvb</ta>
            <ta e="T33" id="Seg_7729" s="T32">interj</ta>
            <ta e="T34" id="Seg_7730" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_7731" s="T34">dempro</ta>
            <ta e="T36" id="Seg_7732" s="T35">n-n:poss-n:case</ta>
            <ta e="T37" id="Seg_7733" s="T36">n-n:(poss)</ta>
            <ta e="T38" id="Seg_7734" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_7735" s="T38">v-v:tense-v:pred.pn</ta>
            <ta e="T40" id="Seg_7736" s="T39">que</ta>
            <ta e="T41" id="Seg_7737" s="T40">v-v:mood-v:pred.pn=ptcl</ta>
            <ta e="T42" id="Seg_7738" s="T41">v-v:tense-v:pred.pn</ta>
            <ta e="T43" id="Seg_7739" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_7740" s="T43">dempro</ta>
            <ta e="T45" id="Seg_7741" s="T44">n-n:(num).[n:case]</ta>
            <ta e="T46" id="Seg_7742" s="T45">ptcl</ta>
            <ta e="T48" id="Seg_7743" s="T47">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T49" id="Seg_7744" s="T48">v-v:tense-v:pred.pn</ta>
            <ta e="T51" id="Seg_7745" s="T50">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T52" id="Seg_7746" s="T51">v-v:tense-v:pred.pn</ta>
            <ta e="T53" id="Seg_7747" s="T52">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T54" id="Seg_7748" s="T53">ptcl</ta>
            <ta e="T56" id="Seg_7749" s="T55">adv</ta>
            <ta e="T57" id="Seg_7750" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_7751" s="T57">adv</ta>
            <ta e="T59" id="Seg_7752" s="T58">v-v:tense-v:pred.pn</ta>
            <ta e="T61" id="Seg_7753" s="T60">v-v:cvb-v:pred.pn</ta>
            <ta e="T62" id="Seg_7754" s="T61">v-v:tense-v:poss.pn</ta>
            <ta e="T63" id="Seg_7755" s="T62">cardnum</ta>
            <ta e="T64" id="Seg_7756" s="T63">n.[n:case]</ta>
            <ta e="T65" id="Seg_7757" s="T64">v-v:tense-v:pred.pn</ta>
            <ta e="T66" id="Seg_7758" s="T65">ptcl</ta>
            <ta e="T68" id="Seg_7759" s="T67">n-n:poss-n:case</ta>
            <ta e="T69" id="Seg_7760" s="T68">n-n:(poss).[n:case]</ta>
            <ta e="T70" id="Seg_7761" s="T69">propr.[n:case]</ta>
            <ta e="T72" id="Seg_7762" s="T71">n-n:poss-n:case</ta>
            <ta e="T73" id="Seg_7763" s="T72">n-n:(poss).[n:case]</ta>
            <ta e="T74" id="Seg_7764" s="T73">que.[pro:case]</ta>
            <ta e="T75" id="Seg_7765" s="T74">v-v:tense-v:pred.pn=ptcl</ta>
            <ta e="T76" id="Seg_7766" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_7767" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_7768" s="T77">adj</ta>
            <ta e="T79" id="Seg_7769" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_7770" s="T79">n-n&gt;adj</ta>
            <ta e="T81" id="Seg_7771" s="T80">n.[n:case]</ta>
            <ta e="T82" id="Seg_7772" s="T81">que.[pro:case]</ta>
            <ta e="T83" id="Seg_7773" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_7774" s="T83">v-v:mood-v:pred.pn</ta>
            <ta e="T85" id="Seg_7775" s="T84">ptcl</ta>
            <ta e="T87" id="Seg_7776" s="T86">adv</ta>
            <ta e="T88" id="Seg_7777" s="T87">v-v:(neg)-v:pred.pn</ta>
            <ta e="T89" id="Seg_7778" s="T88">n-n:poss-n:case</ta>
            <ta e="T91" id="Seg_7779" s="T90">dempro-pro:case</ta>
            <ta e="T92" id="Seg_7780" s="T91">ptcl</ta>
            <ta e="T93" id="Seg_7781" s="T92">v-v:tense-v:pred.pn</ta>
            <ta e="T94" id="Seg_7782" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_7783" s="T94">v-v:cvb</ta>
            <ta e="T96" id="Seg_7784" s="T95">v-v:cvb</ta>
            <ta e="T97" id="Seg_7785" s="T96">v-v:tense-v:pred.pn</ta>
            <ta e="T98" id="Seg_7786" s="T97">ptcl</ta>
            <ta e="T100" id="Seg_7787" s="T99">dempro</ta>
            <ta e="T101" id="Seg_7788" s="T100">que</ta>
            <ta e="T102" id="Seg_7789" s="T101">v-v:tense-v:pred.pn=ptcl</ta>
            <ta e="T103" id="Seg_7790" s="T102">pers.[pro:case]</ta>
            <ta e="T104" id="Seg_7791" s="T103">n-n:poss-n:case</ta>
            <ta e="T106" id="Seg_7792" s="T105">que</ta>
            <ta e="T107" id="Seg_7793" s="T106">v.[v&gt;v]-v:tense-v:poss.pn</ta>
            <ta e="T109" id="Seg_7794" s="T108">v-v:ptcp</ta>
            <ta e="T110" id="Seg_7795" s="T109">n-n:poss-n:case</ta>
            <ta e="T111" id="Seg_7796" s="T110">que</ta>
            <ta e="T112" id="Seg_7797" s="T111">v-v:tense-v:pred.pn</ta>
            <ta e="T113" id="Seg_7798" s="T112">v-v:tense-v:pred.pn</ta>
            <ta e="T114" id="Seg_7799" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_7800" s="T114">n-n:(num).[n:case]</ta>
            <ta e="T117" id="Seg_7801" s="T116">interj</ta>
            <ta e="T118" id="Seg_7802" s="T117">adv</ta>
            <ta e="T119" id="Seg_7803" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_7804" s="T119">n.[n:case]</ta>
            <ta e="T121" id="Seg_7805" s="T120">v-v:tense-v:poss.pn</ta>
            <ta e="T122" id="Seg_7806" s="T121">n.[n:case]</ta>
            <ta e="T123" id="Seg_7807" s="T122">v-v:tense-v:poss.pn</ta>
            <ta e="T125" id="Seg_7808" s="T124">n.[n:case]</ta>
            <ta e="T126" id="Seg_7809" s="T125">v-v:tense-v:poss.pn</ta>
            <ta e="T127" id="Seg_7810" s="T126">adv</ta>
            <ta e="T128" id="Seg_7811" s="T127">quant</ta>
            <ta e="T129" id="Seg_7812" s="T128">n.[n:case]</ta>
            <ta e="T130" id="Seg_7813" s="T129">v-v:tense-v:poss.pn</ta>
            <ta e="T132" id="Seg_7814" s="T131">adv</ta>
            <ta e="T133" id="Seg_7815" s="T132">n-n:(poss).[n:case]</ta>
            <ta e="T134" id="Seg_7816" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_7817" s="T134">dempro.[pro:case]</ta>
            <ta e="T136" id="Seg_7818" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_7819" s="T136">v-v:cvb</ta>
            <ta e="T138" id="Seg_7820" s="T137">v-v:tense.[v:pred.pn]</ta>
            <ta e="T140" id="Seg_7821" s="T139">que</ta>
            <ta e="T141" id="Seg_7822" s="T140">adj-n:(poss).[n:case]=ptcl</ta>
            <ta e="T142" id="Seg_7823" s="T141">v-v:tense-v:pred.pn</ta>
            <ta e="T143" id="Seg_7824" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_7825" s="T143">n-n:(poss).[n:case]</ta>
            <ta e="T146" id="Seg_7826" s="T145">n-n:(poss).[n:case]</ta>
            <ta e="T147" id="Seg_7827" s="T146">adj.[n:case]</ta>
            <ta e="T148" id="Seg_7828" s="T147">adv</ta>
            <ta e="T149" id="Seg_7829" s="T148">dempro.[pro:case]</ta>
            <ta e="T150" id="Seg_7830" s="T149">post</ta>
            <ta e="T151" id="Seg_7831" s="T150">adv</ta>
            <ta e="T152" id="Seg_7832" s="T151">v-v:tense.[v:pred.pn]</ta>
            <ta e="T153" id="Seg_7833" s="T152">n-n:(poss).[n:case]</ta>
            <ta e="T155" id="Seg_7834" s="T154">adv</ta>
            <ta e="T156" id="Seg_7835" s="T155">n-n:(num).[n:case]</ta>
            <ta e="T157" id="Seg_7836" s="T156">v-v:tense-v:poss.pn</ta>
            <ta e="T159" id="Seg_7837" s="T158">v-v:tense-v:pred.pn</ta>
            <ta e="T160" id="Seg_7838" s="T159">ptcl</ta>
            <ta e="T161" id="Seg_7839" s="T160">que.[pro:case]</ta>
            <ta e="T162" id="Seg_7840" s="T161">n-n:(poss).[n:case]=ptcl</ta>
            <ta e="T163" id="Seg_7841" s="T162">n.[n:case]</ta>
            <ta e="T164" id="Seg_7842" s="T163">v-v:tense.[v:pred.pn]</ta>
            <ta e="T167" id="Seg_7843" s="T166">v-v:ptcp</ta>
            <ta e="T168" id="Seg_7844" s="T167">v-v:tense.[v:pred.pn]</ta>
            <ta e="T169" id="Seg_7845" s="T168">n.[n:case]</ta>
            <ta e="T171" id="Seg_7846" s="T170">ptcl</ta>
            <ta e="T172" id="Seg_7847" s="T171">dempro</ta>
            <ta e="T173" id="Seg_7848" s="T172">adv</ta>
            <ta e="T174" id="Seg_7849" s="T173">adv</ta>
            <ta e="T175" id="Seg_7850" s="T174">adv</ta>
            <ta e="T176" id="Seg_7851" s="T175">adv</ta>
            <ta e="T177" id="Seg_7852" s="T176">dempro</ta>
            <ta e="T178" id="Seg_7853" s="T177">v-v:cvb</ta>
            <ta e="T179" id="Seg_7854" s="T178">v-v:cvb</ta>
            <ta e="T180" id="Seg_7855" s="T179">dempro</ta>
            <ta e="T181" id="Seg_7856" s="T180">propr-n:(poss).[n:case]</ta>
            <ta e="T182" id="Seg_7857" s="T181">v-v:cvb</ta>
            <ta e="T183" id="Seg_7858" s="T182">v-v:tense.[v:pred.pn]</ta>
            <ta e="T184" id="Seg_7859" s="T183">ptcl</ta>
            <ta e="T186" id="Seg_7860" s="T185">n-n:(poss)</ta>
            <ta e="T187" id="Seg_7861" s="T186">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T188" id="Seg_7862" s="T187">n.[n:case]</ta>
            <ta e="T189" id="Seg_7863" s="T188">n-n:case</ta>
            <ta e="T190" id="Seg_7864" s="T189">v-v:tense-v:pred.pn</ta>
            <ta e="T191" id="Seg_7865" s="T190">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T192" id="Seg_7866" s="T191">v-v:cvb-v:pred.pn</ta>
            <ta e="T194" id="Seg_7867" s="T193">n-n:(num)-n:case</ta>
            <ta e="T196" id="Seg_7868" s="T195">v-v:tense-v:pred.pn</ta>
            <ta e="T198" id="Seg_7869" s="T197">adv</ta>
            <ta e="T199" id="Seg_7870" s="T198">v-v:ptcp</ta>
            <ta e="T200" id="Seg_7871" s="T199">n-n:(poss).[n:case]</ta>
            <ta e="T201" id="Seg_7872" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_7873" s="T201">v-v:tense-v:poss.pn</ta>
            <ta e="T203" id="Seg_7874" s="T202">adj</ta>
            <ta e="T204" id="Seg_7875" s="T203">adv</ta>
            <ta e="T205" id="Seg_7876" s="T204">v-v:tense-v:poss.pn</ta>
            <ta e="T206" id="Seg_7877" s="T205">ptcl</ta>
            <ta e="T207" id="Seg_7878" s="T206">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T208" id="Seg_7879" s="T207">n-n:(poss).[n:case]</ta>
            <ta e="T210" id="Seg_7880" s="T209">dempro.[pro:case]</ta>
            <ta e="T211" id="Seg_7881" s="T210">post</ta>
            <ta e="T212" id="Seg_7882" s="T211">propr.[n:case]</ta>
            <ta e="T213" id="Seg_7883" s="T212">propr.[n:case]</ta>
            <ta e="T214" id="Seg_7884" s="T213">propr.[n:case]</ta>
            <ta e="T215" id="Seg_7885" s="T214">que-pro:case</ta>
            <ta e="T216" id="Seg_7886" s="T215">que-pro:case</ta>
            <ta e="T217" id="Seg_7887" s="T216">propr.[n:case]</ta>
            <ta e="T218" id="Seg_7888" s="T217">v-v:tense-v:pred.pn</ta>
            <ta e="T219" id="Seg_7889" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_7890" s="T219">dempro.[pro:case]</ta>
            <ta e="T221" id="Seg_7891" s="T220">adv</ta>
            <ta e="T222" id="Seg_7892" s="T221">v-v:tense-v:pred.pn</ta>
            <ta e="T223" id="Seg_7893" s="T222">ptcl</ta>
            <ta e="T225" id="Seg_7894" s="T224">ptcl</ta>
            <ta e="T226" id="Seg_7895" s="T225">dempro</ta>
            <ta e="T227" id="Seg_7896" s="T226">propr-n:case</ta>
            <ta e="T229" id="Seg_7897" s="T228">dempro.[pro:case]</ta>
            <ta e="T230" id="Seg_7898" s="T229">v-v:ptcp</ta>
            <ta e="T231" id="Seg_7899" s="T230">n-n:poss-n:case</ta>
            <ta e="T232" id="Seg_7900" s="T231">v-v:tense-v:pred.pn</ta>
            <ta e="T233" id="Seg_7901" s="T232">adv</ta>
            <ta e="T234" id="Seg_7902" s="T233">adv</ta>
            <ta e="T235" id="Seg_7903" s="T234">adv</ta>
            <ta e="T236" id="Seg_7904" s="T235">ptcl</ta>
            <ta e="T237" id="Seg_7905" s="T236">dempro.[pro:case]</ta>
            <ta e="T238" id="Seg_7906" s="T237">v-v:tense-v:poss.pn</ta>
            <ta e="T240" id="Seg_7907" s="T239">adv</ta>
            <ta e="T241" id="Seg_7908" s="T240">adv</ta>
            <ta e="T242" id="Seg_7909" s="T241">dempro.[pro:case]</ta>
            <ta e="T243" id="Seg_7910" s="T242">dempro</ta>
            <ta e="T244" id="Seg_7911" s="T243">v-v:cvb</ta>
            <ta e="T245" id="Seg_7912" s="T244">ptcl-ptcl:(temp.pn)</ta>
            <ta e="T246" id="Seg_7913" s="T245">propr-n:(num).[n:case]</ta>
            <ta e="T247" id="Seg_7914" s="T246">v-v:cvb</ta>
            <ta e="T248" id="Seg_7915" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_7916" s="T248">v-v:tense-v:poss.pn</ta>
            <ta e="T250" id="Seg_7917" s="T249">ptcl</ta>
            <ta e="T252" id="Seg_7918" s="T251">interj</ta>
            <ta e="T253" id="Seg_7919" s="T252">que-pro:(num).[pro:case]</ta>
            <ta e="T254" id="Seg_7920" s="T253">adj-n:(num).[n:case]</ta>
            <ta e="T255" id="Seg_7921" s="T254">v-v:tense-v:pred.pn</ta>
            <ta e="T256" id="Seg_7922" s="T255">dempro</ta>
            <ta e="T258" id="Seg_7923" s="T257">adv</ta>
            <ta e="T259" id="Seg_7924" s="T258">dempro.[pro:case]</ta>
            <ta e="T260" id="Seg_7925" s="T259">n.[n:case]</ta>
            <ta e="T261" id="Seg_7926" s="T260">adj</ta>
            <ta e="T262" id="Seg_7927" s="T261">n-n:poss-n:case</ta>
            <ta e="T263" id="Seg_7928" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_7929" s="T263">n.[n:case]</ta>
            <ta e="T265" id="Seg_7930" s="T264">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T266" id="Seg_7931" s="T265">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T267" id="Seg_7932" s="T266">cardnum</ta>
            <ta e="T268" id="Seg_7933" s="T267">cardnum</ta>
            <ta e="T269" id="Seg_7934" s="T268">n.[n:case]</ta>
            <ta e="T270" id="Seg_7935" s="T269">n-n:(poss).[n:case]</ta>
            <ta e="T272" id="Seg_7936" s="T271">adv</ta>
            <ta e="T273" id="Seg_7937" s="T272">v-v:tense-v:pred.pn</ta>
            <ta e="T274" id="Seg_7938" s="T273">dempro</ta>
            <ta e="T275" id="Seg_7939" s="T274">n-n:(num).[n:case]</ta>
            <ta e="T276" id="Seg_7940" s="T275">v-v:mood-v:temp.pn</ta>
            <ta e="T277" id="Seg_7941" s="T276">propr-n:(num).[n:case]</ta>
            <ta e="T278" id="Seg_7942" s="T277">v-v:tense-v:poss.pn</ta>
            <ta e="T280" id="Seg_7943" s="T279">propr-n:(num).[n:case]</ta>
            <ta e="T281" id="Seg_7944" s="T280">adj-n:(num).[n:case]</ta>
            <ta e="T283" id="Seg_7945" s="T282">dempro</ta>
            <ta e="T284" id="Seg_7946" s="T283">v-v:tense-v:poss.pn</ta>
            <ta e="T285" id="Seg_7947" s="T284">cardnum</ta>
            <ta e="T286" id="Seg_7948" s="T285">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T287" id="Seg_7949" s="T286">dempro-pro:(num).[pro:case]</ta>
            <ta e="T289" id="Seg_7950" s="T288">cardnum-n:(poss).[n:case]</ta>
            <ta e="T290" id="Seg_7951" s="T289">n.[n:case]</ta>
            <ta e="T292" id="Seg_7952" s="T291">cardnum-n:(poss).[n:case]</ta>
            <ta e="T293" id="Seg_7953" s="T292">n.[n:case]</ta>
            <ta e="T295" id="Seg_7954" s="T294">ptcl</ta>
            <ta e="T296" id="Seg_7955" s="T295">dempro</ta>
            <ta e="T297" id="Seg_7956" s="T296">n-n:case</ta>
            <ta e="T298" id="Seg_7957" s="T297">v-v:tense-v:poss.pn</ta>
            <ta e="T299" id="Seg_7958" s="T298">v-v:ptcp-v:(ins)-v:(case)</ta>
            <ta e="T300" id="Seg_7959" s="T299">v-v:tense-v:pred.pn</ta>
            <ta e="T301" id="Seg_7960" s="T300">dempro-pro:(num)-pro:case</ta>
            <ta e="T302" id="Seg_7961" s="T301">n.[n:case]</ta>
            <ta e="T303" id="Seg_7962" s="T302">v-v:cvb</ta>
            <ta e="T304" id="Seg_7963" s="T303">v-v:tense-v:poss.pn</ta>
            <ta e="T305" id="Seg_7964" s="T304">adj.[n:case]</ta>
            <ta e="T306" id="Seg_7965" s="T305">n-n:(num).[n:case]</ta>
            <ta e="T308" id="Seg_7966" s="T307">que</ta>
            <ta e="T309" id="Seg_7967" s="T308">ptcl</ta>
            <ta e="T310" id="Seg_7968" s="T309">que-pro:(poss).[pro:case]</ta>
            <ta e="T311" id="Seg_7969" s="T310">n-n:(num).[n:case]</ta>
            <ta e="T312" id="Seg_7970" s="T311">ptcl</ta>
            <ta e="T313" id="Seg_7971" s="T312">conj</ta>
            <ta e="T315" id="Seg_7972" s="T314">dempro</ta>
            <ta e="T316" id="Seg_7973" s="T315">v-v:cvb</ta>
            <ta e="T317" id="Seg_7974" s="T316">v-v:cvb-v:pred.pn</ta>
            <ta e="T318" id="Seg_7975" s="T317">adv</ta>
            <ta e="T319" id="Seg_7976" s="T318">v-v:tense-v:poss.pn</ta>
            <ta e="T320" id="Seg_7977" s="T319">n-n:(num).[n:case]</ta>
            <ta e="T321" id="Seg_7978" s="T320">cardnum-n:(poss).[n:case]</ta>
            <ta e="T322" id="Seg_7979" s="T321">ptcl</ta>
            <ta e="T323" id="Seg_7980" s="T322">adj.[n:case]</ta>
            <ta e="T325" id="Seg_7981" s="T324">cardnum-n:(poss).[n:case]</ta>
            <ta e="T326" id="Seg_7982" s="T325">adj.[n:case]</ta>
            <ta e="T327" id="Seg_7983" s="T326">cardnum-n:(poss).[n:case]</ta>
            <ta e="T328" id="Seg_7984" s="T327">v-v&gt;n-n&gt;n.[n:case]</ta>
            <ta e="T329" id="Seg_7985" s="T328">n-n:(num).[n:case]</ta>
            <ta e="T330" id="Seg_7986" s="T329">posspr.[pro:case]</ta>
            <ta e="T331" id="Seg_7987" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_7988" s="T331">v-v:tense-v:pred.pn</ta>
            <ta e="T334" id="Seg_7989" s="T333">dempro-pro:(num).[pro:case]</ta>
            <ta e="T335" id="Seg_7990" s="T334">ptcl</ta>
            <ta e="T336" id="Seg_7991" s="T335">n-n:case</ta>
            <ta e="T337" id="Seg_7992" s="T336">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T338" id="Seg_7993" s="T337">v-v:cvb</ta>
            <ta e="T339" id="Seg_7994" s="T338">v-v:cvb-v:pred.pn</ta>
            <ta e="T340" id="Seg_7995" s="T339">dempro</ta>
            <ta e="T341" id="Seg_7996" s="T340">n-n:(num).[n:case]</ta>
            <ta e="T342" id="Seg_7997" s="T341">propr</ta>
            <ta e="T343" id="Seg_7998" s="T342">v-v:cvb</ta>
            <ta e="T344" id="Seg_7999" s="T343">n-n:case</ta>
            <ta e="T345" id="Seg_8000" s="T344">propr</ta>
            <ta e="T346" id="Seg_8001" s="T345">v-v:cvb</ta>
            <ta e="T347" id="Seg_8002" s="T346">n.[n:case]</ta>
            <ta e="T348" id="Seg_8003" s="T347">ptcl</ta>
            <ta e="T349" id="Seg_8004" s="T348">v-v:tense-v:poss.pn</ta>
            <ta e="T351" id="Seg_8005" s="T350">dempro-pro:case</ta>
            <ta e="T352" id="Seg_8006" s="T351">v-v:tense-v:pred.pn</ta>
            <ta e="T353" id="Seg_8007" s="T352">dempro</ta>
            <ta e="T354" id="Seg_8008" s="T353">n-n:(num).[n:case]</ta>
            <ta e="T356" id="Seg_8009" s="T355">interj</ta>
            <ta e="T357" id="Seg_8010" s="T356">n.[n:case]</ta>
            <ta e="T358" id="Seg_8011" s="T357">dempro</ta>
            <ta e="T359" id="Seg_8012" s="T358">dempro</ta>
            <ta e="T360" id="Seg_8013" s="T359">n.[n:case]</ta>
            <ta e="T361" id="Seg_8014" s="T360">n-n&gt;adj</ta>
            <ta e="T362" id="Seg_8015" s="T361">n-n:(num).[n:case]</ta>
            <ta e="T363" id="Seg_8016" s="T362">v-v:tense-v:pred.pn</ta>
            <ta e="T364" id="Seg_8017" s="T363">v-v:ptcp-v&gt;n-n:(num).[n:case]</ta>
            <ta e="T365" id="Seg_8018" s="T364">v-v:pred.pn</ta>
            <ta e="T366" id="Seg_8019" s="T365">adj-n:(num).[n:case]</ta>
            <ta e="T368" id="Seg_8020" s="T367">ptcl</ta>
            <ta e="T369" id="Seg_8021" s="T368">dempro</ta>
            <ta e="T370" id="Seg_8022" s="T369">v-v:tense-v:pred.pn</ta>
            <ta e="T371" id="Seg_8023" s="T370">propr-n:case</ta>
            <ta e="T372" id="Seg_8024" s="T371">v-v:tense-v:poss.pn</ta>
            <ta e="T374" id="Seg_8025" s="T373">dempro</ta>
            <ta e="T375" id="Seg_8026" s="T374">v-v:cvb</ta>
            <ta e="T376" id="Seg_8027" s="T375">v-v:tense-v:pred.pn</ta>
            <ta e="T377" id="Seg_8028" s="T376">propr-n:case</ta>
            <ta e="T378" id="Seg_8029" s="T377">v-v:cvb-v:pred.pn</ta>
            <ta e="T379" id="Seg_8030" s="T378">ptcl</ta>
            <ta e="T380" id="Seg_8031" s="T379">v-v:tense-v:pred.pn</ta>
            <ta e="T381" id="Seg_8032" s="T380">interj</ta>
            <ta e="T382" id="Seg_8033" s="T381">dempro-pro:(num).[pro:case]</ta>
            <ta e="T383" id="Seg_8034" s="T382">v-v:tense-v:pred.pn</ta>
            <ta e="T384" id="Seg_8035" s="T383">n-n:case</ta>
            <ta e="T385" id="Seg_8036" s="T384">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T386" id="Seg_8037" s="T385">adj</ta>
            <ta e="T387" id="Seg_8038" s="T386">n-n:case</ta>
            <ta e="T388" id="Seg_8039" s="T387">v-v:cvb-v:pred.pn</ta>
            <ta e="T389" id="Seg_8040" s="T388">adj</ta>
            <ta e="T390" id="Seg_8041" s="T389">n.[n:case]</ta>
            <ta e="T391" id="Seg_8042" s="T390">post</ta>
            <ta e="T392" id="Seg_8043" s="T391">v-v:tense-v:pred.pn</ta>
            <ta e="T393" id="Seg_8044" s="T392">v-v:cvb</ta>
            <ta e="T395" id="Seg_8045" s="T394">dempro</ta>
            <ta e="T396" id="Seg_8046" s="T395">n-n:case</ta>
            <ta e="T397" id="Seg_8047" s="T396">v-v:cvb-v:pred.pn</ta>
            <ta e="T398" id="Seg_8048" s="T397">dempro</ta>
            <ta e="T399" id="Seg_8049" s="T398">n-n:(num).[n:case]</ta>
            <ta e="T400" id="Seg_8050" s="T399">ptcl</ta>
            <ta e="T402" id="Seg_8051" s="T401">ptcl</ta>
            <ta e="T403" id="Seg_8052" s="T402">adj.[n:case]</ta>
            <ta e="T404" id="Seg_8053" s="T403">v-v&gt;v-v:cvb</ta>
            <ta e="T405" id="Seg_8054" s="T404">v.[v:mood.pn]</ta>
            <ta e="T406" id="Seg_8055" s="T405">adv</ta>
            <ta e="T407" id="Seg_8056" s="T406">n-n:case</ta>
            <ta e="T408" id="Seg_8057" s="T407">v-v:cvb-v:pred.pn</ta>
            <ta e="T409" id="Seg_8058" s="T408">v-v:cvb</ta>
            <ta e="T411" id="Seg_8059" s="T410">cardnum-n:(poss).[n:case]</ta>
            <ta e="T412" id="Seg_8060" s="T411">v-v&gt;n-n&gt;n-n:(poss).[n:case]</ta>
            <ta e="T413" id="Seg_8061" s="T412">post</ta>
            <ta e="T414" id="Seg_8062" s="T413">v-v&gt;v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T415" id="Seg_8063" s="T414">n-n:(num)-n:poss-n:case</ta>
            <ta e="T416" id="Seg_8064" s="T415">adv</ta>
            <ta e="T417" id="Seg_8065" s="T416">v-v:cvb</ta>
            <ta e="T418" id="Seg_8066" s="T417">post</ta>
            <ta e="T419" id="Seg_8067" s="T418">ptcl</ta>
            <ta e="T420" id="Seg_8068" s="T419">n.[n:case]</ta>
            <ta e="T421" id="Seg_8069" s="T420">n.[n:case]</ta>
            <ta e="T422" id="Seg_8070" s="T421">v-v:tense-v:pred.pn</ta>
            <ta e="T423" id="Seg_8071" s="T422">dempro</ta>
            <ta e="T424" id="Seg_8072" s="T423">n.[n:case]</ta>
            <ta e="T426" id="Seg_8073" s="T425">n.[n:case]</ta>
            <ta e="T427" id="Seg_8074" s="T426">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T429" id="Seg_8075" s="T428">ptcl</ta>
            <ta e="T430" id="Seg_8076" s="T429">n-n&gt;v-v:cvb</ta>
            <ta e="T431" id="Seg_8077" s="T430">post</ta>
            <ta e="T432" id="Seg_8078" s="T431">ptcl</ta>
            <ta e="T433" id="Seg_8079" s="T432">n-n:poss-n:case</ta>
            <ta e="T434" id="Seg_8080" s="T433">ptcl</ta>
            <ta e="T435" id="Seg_8081" s="T434">que-pro:case</ta>
            <ta e="T436" id="Seg_8082" s="T435">v-v:tense.[v:pred.pn]</ta>
            <ta e="T438" id="Seg_8083" s="T437">n</ta>
            <ta e="T439" id="Seg_8084" s="T438">v-v:mood-v:pred.pn</ta>
            <ta e="T440" id="Seg_8085" s="T439">adv</ta>
            <ta e="T441" id="Seg_8086" s="T440">adv</ta>
            <ta e="T442" id="Seg_8087" s="T441">n</ta>
            <ta e="T443" id="Seg_8088" s="T442">n</ta>
            <ta e="T445" id="Seg_8089" s="T444">n-n:case</ta>
            <ta e="T446" id="Seg_8090" s="T445">n-n:case</ta>
            <ta e="T447" id="Seg_8091" s="T446">post</ta>
            <ta e="T448" id="Seg_8092" s="T447">post</ta>
            <ta e="T449" id="Seg_8093" s="T448">v-v:cvb-v-v:cvb</ta>
            <ta e="T450" id="Seg_8094" s="T449">ptcl</ta>
            <ta e="T451" id="Seg_8095" s="T450">n-n:case</ta>
            <ta e="T452" id="Seg_8096" s="T451">v-v:tense.[v:pred.pn]</ta>
            <ta e="T453" id="Seg_8097" s="T452">ptcl</ta>
            <ta e="T455" id="Seg_8098" s="T454">v-v:cvb</ta>
            <ta e="T456" id="Seg_8099" s="T455">v-v:ptcp</ta>
            <ta e="T457" id="Seg_8100" s="T456">n-n:(num).[n:case]</ta>
            <ta e="T458" id="Seg_8101" s="T457">n.[n:case]</ta>
            <ta e="T459" id="Seg_8102" s="T458">v-v:tense-v:poss.pn</ta>
            <ta e="T460" id="Seg_8103" s="T459">v-v:cvb</ta>
            <ta e="T461" id="Seg_8104" s="T460">dempro</ta>
            <ta e="T462" id="Seg_8105" s="T461">v-v:tense-v:poss.pn</ta>
            <ta e="T463" id="Seg_8106" s="T462">v-v:cvb</ta>
            <ta e="T464" id="Seg_8107" s="T463">dempro</ta>
            <ta e="T465" id="Seg_8108" s="T464">n</ta>
            <ta e="T466" id="Seg_8109" s="T465">v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T467" id="Seg_8110" s="T466">ptcl</ta>
            <ta e="T469" id="Seg_8111" s="T468">adv</ta>
            <ta e="T470" id="Seg_8112" s="T469">v-v&gt;n.[n:case]</ta>
            <ta e="T471" id="Seg_8113" s="T470">v-v:mood-v:temp.pn</ta>
            <ta e="T472" id="Seg_8114" s="T471">dempro</ta>
            <ta e="T473" id="Seg_8115" s="T472">n-n:poss-n:case</ta>
            <ta e="T474" id="Seg_8116" s="T473">n-n:poss-n:case</ta>
            <ta e="T475" id="Seg_8117" s="T474">interj</ta>
            <ta e="T476" id="Seg_8118" s="T475">v-v:(tense)-v:(ins)-v:mood.pn</ta>
            <ta e="T477" id="Seg_8119" s="T476">v-v:cvb</ta>
            <ta e="T478" id="Seg_8120" s="T477">n-n:(ins)</ta>
            <ta e="T479" id="Seg_8121" s="T478">interj</ta>
            <ta e="T480" id="Seg_8122" s="T479">que-pro:(ins)-pro:case</ta>
            <ta e="T481" id="Seg_8123" s="T480">n-n:case</ta>
            <ta e="T483" id="Seg_8124" s="T482">adv</ta>
            <ta e="T484" id="Seg_8125" s="T483">ptcl</ta>
            <ta e="T485" id="Seg_8126" s="T484">v-v:tense-v:poss.pn</ta>
            <ta e="T486" id="Seg_8127" s="T485">v-v:tense.[v:pred.pn]</ta>
            <ta e="T487" id="Seg_8128" s="T486">ptcl</ta>
            <ta e="T488" id="Seg_8129" s="T487">adv</ta>
            <ta e="T489" id="Seg_8130" s="T488">v-v:tense-v:poss.pn</ta>
            <ta e="T490" id="Seg_8131" s="T489">v-v:tense-v:poss.pn</ta>
            <ta e="T491" id="Seg_8132" s="T490">que</ta>
            <ta e="T492" id="Seg_8133" s="T491">ptcl</ta>
            <ta e="T494" id="Seg_8134" s="T493">ptcl</ta>
            <ta e="T495" id="Seg_8135" s="T494">dempro</ta>
            <ta e="T496" id="Seg_8136" s="T495">v-v:tense-v:pred.pn</ta>
            <ta e="T497" id="Seg_8137" s="T496">dempro</ta>
            <ta e="T498" id="Seg_8138" s="T497">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T499" id="Seg_8139" s="T498">ptcl</ta>
            <ta e="T500" id="Seg_8140" s="T499">v-v:tense-v:pred.pn</ta>
            <ta e="T501" id="Seg_8141" s="T500">n-n&gt;v-v:mood.pn</ta>
            <ta e="T502" id="Seg_8142" s="T501">v-v:tense.[v:pred.pn]</ta>
            <ta e="T503" id="Seg_8143" s="T502">ptcl</ta>
            <ta e="T504" id="Seg_8144" s="T503">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T506" id="Seg_8145" s="T505">ptcl</ta>
            <ta e="T507" id="Seg_8146" s="T506">dempro</ta>
            <ta e="T508" id="Seg_8147" s="T507">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T509" id="Seg_8148" s="T508">v-v:mood-v:temp.pn</ta>
            <ta e="T510" id="Seg_8149" s="T509">ptcl</ta>
            <ta e="T511" id="Seg_8150" s="T510">v-v:tense-v:pred.pn</ta>
            <ta e="T512" id="Seg_8151" s="T511">ptcl</ta>
            <ta e="T513" id="Seg_8152" s="T512">dempro</ta>
            <ta e="T514" id="Seg_8153" s="T513">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T516" id="Seg_8154" s="T515">v-v:cvb-v:pred.pn</ta>
            <ta e="T517" id="Seg_8155" s="T516">n-n:(num)-n:case</ta>
            <ta e="T518" id="Seg_8156" s="T517">v-v&gt;v-v&gt;v-v:cvb</ta>
            <ta e="T519" id="Seg_8157" s="T518">ptcl</ta>
            <ta e="T520" id="Seg_8158" s="T519">ptcl</ta>
            <ta e="T521" id="Seg_8159" s="T520">n.[n:case]</ta>
            <ta e="T522" id="Seg_8160" s="T521">v-v:tense.[v:pred.pn]</ta>
            <ta e="T523" id="Seg_8161" s="T522">v-v&gt;v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T525" id="Seg_8162" s="T524">interj</ta>
            <ta e="T526" id="Seg_8163" s="T525">dempro-pro:(num).[pro:case]</ta>
            <ta e="T527" id="Seg_8164" s="T526">dempro-pro:(num).[n:case]</ta>
            <ta e="T528" id="Seg_8165" s="T527">n-n:case</ta>
            <ta e="T529" id="Seg_8166" s="T528">v-v:mood.pn</ta>
            <ta e="T530" id="Seg_8167" s="T529">v-v:cvb</ta>
            <ta e="T532" id="Seg_8168" s="T531">ptcl</ta>
            <ta e="T533" id="Seg_8169" s="T532">dempro</ta>
            <ta e="T534" id="Seg_8170" s="T533">n-n:case</ta>
            <ta e="T535" id="Seg_8171" s="T534">v-v:cvb</ta>
            <ta e="T536" id="Seg_8172" s="T535">v-v:tense-v:pred.pn</ta>
            <ta e="T537" id="Seg_8173" s="T536">ptcl</ta>
            <ta e="T538" id="Seg_8174" s="T537">n-n:case</ta>
            <ta e="T539" id="Seg_8175" s="T538">v-v:tense-v:poss.pn</ta>
            <ta e="T540" id="Seg_8176" s="T539">pers.[pro:case]</ta>
            <ta e="T541" id="Seg_8177" s="T540">dempro.[pro:case]</ta>
            <ta e="T542" id="Seg_8178" s="T541">n-n&gt;n-n:(poss).[n:case]</ta>
            <ta e="T543" id="Seg_8179" s="T542">v-v:tense-v:pred.pn</ta>
            <ta e="T545" id="Seg_8180" s="T544">n.[n:case]</ta>
            <ta e="T546" id="Seg_8181" s="T545">n.[n:case]</ta>
            <ta e="T547" id="Seg_8182" s="T546">dempro.[pro:case]</ta>
            <ta e="T548" id="Seg_8183" s="T547">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T550" id="Seg_8184" s="T549">adv</ta>
            <ta e="T551" id="Seg_8185" s="T550">dempro</ta>
            <ta e="T552" id="Seg_8186" s="T551">n.[n:case]</ta>
            <ta e="T553" id="Seg_8187" s="T552">n.[n:case]</ta>
            <ta e="T554" id="Seg_8188" s="T553">n-n&gt;v-v:cvb</ta>
            <ta e="T555" id="Seg_8189" s="T554">v-v:tense-v:poss.pn</ta>
            <ta e="T556" id="Seg_8190" s="T555">pers-pro:case</ta>
            <ta e="T557" id="Seg_8191" s="T556">pers-pro:case</ta>
            <ta e="T558" id="Seg_8192" s="T557">v-v:cvb</ta>
            <ta e="T559" id="Seg_8193" s="T558">dempro</ta>
            <ta e="T560" id="Seg_8194" s="T559">v-v:tense-v:pred.pn</ta>
            <ta e="T561" id="Seg_8195" s="T560">ptcl</ta>
            <ta e="T562" id="Seg_8196" s="T561">n-n:case</ta>
            <ta e="T563" id="Seg_8197" s="T562">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T565" id="Seg_8198" s="T564">n-n:poss-n:case</ta>
            <ta e="T566" id="Seg_8199" s="T565">ptcl</ta>
            <ta e="T567" id="Seg_8200" s="T566">n-n:poss-n:case</ta>
            <ta e="T568" id="Seg_8201" s="T567">adv</ta>
            <ta e="T569" id="Seg_8202" s="T568">v-v:tense-v:pred.pn</ta>
            <ta e="T570" id="Seg_8203" s="T569">v-v:tense.[v:pred.pn]</ta>
            <ta e="T571" id="Seg_8204" s="T570">n-n:(num)-n:case</ta>
            <ta e="T572" id="Seg_8205" s="T571">v-v:cvb</ta>
            <ta e="T573" id="Seg_8206" s="T572">v-v:cvb</ta>
            <ta e="T574" id="Seg_8207" s="T573">v-v:cvb</ta>
            <ta e="T575" id="Seg_8208" s="T574">v-v:cvb</ta>
            <ta e="T576" id="Seg_8209" s="T575">v-v:tense-v:pred.pn</ta>
            <ta e="T578" id="Seg_8210" s="T577">interj</ta>
            <ta e="T579" id="Seg_8211" s="T578">que.[pro:case]</ta>
            <ta e="T580" id="Seg_8212" s="T579">n-n&gt;adj</ta>
            <ta e="T581" id="Seg_8213" s="T580">n-n:poss-n:case</ta>
            <ta e="T582" id="Seg_8214" s="T581">v-v:(ins)-v&gt;v-v:(ins)-v:ptcp</ta>
            <ta e="T583" id="Seg_8215" s="T582">n-n:(num)-n:(pred.pn)=ptcl</ta>
            <ta e="T584" id="Seg_8216" s="T583">ptcl</ta>
            <ta e="T585" id="Seg_8217" s="T584">v-v:mood.pn</ta>
            <ta e="T586" id="Seg_8218" s="T585">v-v:cvb</ta>
            <ta e="T587" id="Seg_8219" s="T586">n.[n:case]</ta>
            <ta e="T588" id="Seg_8220" s="T587">ptcl</ta>
            <ta e="T589" id="Seg_8221" s="T588">v-v:(ins)-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T591" id="Seg_8222" s="T590">ptcl</ta>
            <ta e="T592" id="Seg_8223" s="T591">dempro</ta>
            <ta e="T593" id="Seg_8224" s="T592">v</ta>
            <ta e="T594" id="Seg_8225" s="T593">v-v:(ins)-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T595" id="Seg_8226" s="T594">post</ta>
            <ta e="T596" id="Seg_8227" s="T595">ptcl</ta>
            <ta e="T597" id="Seg_8228" s="T596">dempro</ta>
            <ta e="T598" id="Seg_8229" s="T597">interj</ta>
            <ta e="T599" id="Seg_8230" s="T598">n-n:(poss).[n:case]</ta>
            <ta e="T600" id="Seg_8231" s="T599">que</ta>
            <ta e="T601" id="Seg_8232" s="T600">que</ta>
            <ta e="T602" id="Seg_8233" s="T601">v-v:tense.[v:pred.pn]</ta>
            <ta e="T603" id="Seg_8234" s="T602">ptcl</ta>
            <ta e="T604" id="Seg_8235" s="T603">n-n:(poss).[n:case]</ta>
            <ta e="T605" id="Seg_8236" s="T604">v-v:tense.[v:pred.pn]</ta>
            <ta e="T607" id="Seg_8237" s="T606">que</ta>
            <ta e="T608" id="Seg_8238" s="T607">v-v:tense.[v:pred.pn]</ta>
            <ta e="T609" id="Seg_8239" s="T608">v-v:tense-v:poss.pn</ta>
            <ta e="T611" id="Seg_8240" s="T610">ptcl</ta>
            <ta e="T612" id="Seg_8241" s="T611">dempro</ta>
            <ta e="T613" id="Seg_8242" s="T612">cardnum</ta>
            <ta e="T614" id="Seg_8243" s="T613">adj-adj&gt;adj-n:case</ta>
            <ta e="T615" id="Seg_8244" s="T614">cardnum</ta>
            <ta e="T616" id="Seg_8245" s="T615">n-n:poss-n:case</ta>
            <ta e="T617" id="Seg_8246" s="T616">v-v:tense.[v:pred.pn]</ta>
            <ta e="T618" id="Seg_8247" s="T617">ptcl</ta>
            <ta e="T620" id="Seg_8248" s="T619">ptcl</ta>
            <ta e="T621" id="Seg_8249" s="T620">dempro</ta>
            <ta e="T622" id="Seg_8250" s="T621">v-v&gt;n.[n:case]</ta>
            <ta e="T623" id="Seg_8251" s="T622">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T624" id="Seg_8252" s="T623">n-n:poss-n:case</ta>
            <ta e="T625" id="Seg_8253" s="T624">v-v:cvb</ta>
            <ta e="T626" id="Seg_8254" s="T625">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T627" id="Seg_8255" s="T626">n-n:case</ta>
            <ta e="T629" id="Seg_8256" s="T628">adv</ta>
            <ta e="T630" id="Seg_8257" s="T629">dempro.[pro:case]</ta>
            <ta e="T631" id="Seg_8258" s="T630">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T632" id="Seg_8259" s="T631">v-v:tense.[v:pred.pn]</ta>
            <ta e="T633" id="Seg_8260" s="T632">n-n:(num)-n:case</ta>
            <ta e="T634" id="Seg_8261" s="T633">post</ta>
            <ta e="T636" id="Seg_8262" s="T635">dempro</ta>
            <ta e="T637" id="Seg_8263" s="T636">v-v:tense-v:pred.pn</ta>
            <ta e="T638" id="Seg_8264" s="T637">ptcl</ta>
            <ta e="T639" id="Seg_8265" s="T638">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T641" id="Seg_8266" s="T640">ptcl</ta>
            <ta e="T642" id="Seg_8267" s="T641">adj-n:poss-n:case</ta>
            <ta e="T643" id="Seg_8268" s="T642">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T645" id="Seg_8269" s="T644">cardnum</ta>
            <ta e="T646" id="Seg_8270" s="T645">n-n:(poss).[n:case]</ta>
            <ta e="T647" id="Seg_8271" s="T646">v-v:tense.[v:pred.pn]</ta>
            <ta e="T648" id="Seg_8272" s="T647">ptcl</ta>
            <ta e="T649" id="Seg_8273" s="T648">que</ta>
            <ta e="T650" id="Seg_8274" s="T649">ptcl</ta>
            <ta e="T652" id="Seg_8275" s="T651">ptcl</ta>
            <ta e="T653" id="Seg_8276" s="T652">adv</ta>
            <ta e="T654" id="Seg_8277" s="T653">n-n:(num).[n:case]</ta>
            <ta e="T655" id="Seg_8278" s="T654">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T656" id="Seg_8279" s="T655">dempro</ta>
            <ta e="T657" id="Seg_8280" s="T656">n-n:(num).[n:case]</ta>
            <ta e="T658" id="Seg_8281" s="T657">ptcl</ta>
            <ta e="T659" id="Seg_8282" s="T658">v-v:tense-v:pred.pn</ta>
            <ta e="T660" id="Seg_8283" s="T659">ptcl</ta>
            <ta e="T661" id="Seg_8284" s="T660">n-n:poss-n:case</ta>
            <ta e="T662" id="Seg_8285" s="T661">v-v:(ins)-v&gt;v-v:cvb-v:pred.pn</ta>
            <ta e="T664" id="Seg_8286" s="T663">pers.[pro:case]</ta>
            <ta e="T665" id="Seg_8287" s="T664">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T666" id="Seg_8288" s="T665">n-n:(poss).[n:case]</ta>
            <ta e="T667" id="Seg_8289" s="T666">pers.[pro:case]</ta>
            <ta e="T668" id="Seg_8290" s="T667">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T669" id="Seg_8291" s="T668">n-n:(poss).[n:case]</ta>
            <ta e="T670" id="Seg_8292" s="T669">v-v:(ins)-v&gt;v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T671" id="Seg_8293" s="T670">ptcl</ta>
            <ta e="T672" id="Seg_8294" s="T671">n-n:(num).[n:case]</ta>
            <ta e="T674" id="Seg_8295" s="T673">que</ta>
            <ta e="T675" id="Seg_8296" s="T674">n</ta>
            <ta e="T676" id="Seg_8297" s="T675">n-n:(poss).[n:case]=ptcl</ta>
            <ta e="T677" id="Seg_8298" s="T676">dempro-pro:(num)-pro:case</ta>
            <ta e="T678" id="Seg_8299" s="T677">v-v&gt;v-v:cvb</ta>
            <ta e="T679" id="Seg_8300" s="T678">v-v:(ins)-v:mood.pn</ta>
            <ta e="T680" id="Seg_8301" s="T679">v-v:tense.[v:pred.pn]</ta>
            <ta e="T681" id="Seg_8302" s="T680">ptcl</ta>
            <ta e="T682" id="Seg_8303" s="T681">que.[pro:case]</ta>
            <ta e="T683" id="Seg_8304" s="T682">adj.[n:case]</ta>
            <ta e="T684" id="Seg_8305" s="T683">n-n:(poss).[n:case]</ta>
            <ta e="T685" id="Seg_8306" s="T684">ptcl</ta>
            <ta e="T686" id="Seg_8307" s="T685">ptcl</ta>
            <ta e="T687" id="Seg_8308" s="T686">posspr</ta>
            <ta e="T688" id="Seg_8309" s="T687">propr.[n:case]</ta>
            <ta e="T690" id="Seg_8310" s="T689">v-v&gt;v-v:cvb</ta>
            <ta e="T691" id="Seg_8311" s="T690">v-v:tense-v:pred.pn</ta>
            <ta e="T692" id="Seg_8312" s="T691">n-n&gt;adj-n:(num).[n:case]</ta>
            <ta e="T694" id="Seg_8313" s="T693">adv</ta>
            <ta e="T695" id="Seg_8314" s="T694">dempro</ta>
            <ta e="T696" id="Seg_8315" s="T695">n-n:case</ta>
            <ta e="T697" id="Seg_8316" s="T696">ptcl</ta>
            <ta e="T698" id="Seg_8317" s="T697">v-v:cvb</ta>
            <ta e="T699" id="Seg_8318" s="T698">v-v:tense-v:pred.pn</ta>
            <ta e="T700" id="Seg_8319" s="T699">n.[n:case]</ta>
            <ta e="T701" id="Seg_8320" s="T700">n-n:case</ta>
            <ta e="T703" id="Seg_8321" s="T702">n-n:case</ta>
            <ta e="T704" id="Seg_8322" s="T703">ptcl</ta>
            <ta e="T705" id="Seg_8323" s="T704">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T706" id="Seg_8324" s="T705">ptcl</ta>
            <ta e="T707" id="Seg_8325" s="T706">n-n&gt;v-v:cvb</ta>
            <ta e="T708" id="Seg_8326" s="T707">post</ta>
            <ta e="T710" id="Seg_8327" s="T709">adv</ta>
            <ta e="T711" id="Seg_8328" s="T710">v-v:(neg).[v:pred.pn]</ta>
            <ta e="T713" id="Seg_8329" s="T712">v-v:cvb</ta>
            <ta e="T714" id="Seg_8330" s="T713">v-v:cvb</ta>
            <ta e="T715" id="Seg_8331" s="T714">n-n:poss-n:case</ta>
            <ta e="T716" id="Seg_8332" s="T715">v-v:cvb</ta>
            <ta e="T717" id="Seg_8333" s="T716">ptcl</ta>
            <ta e="T718" id="Seg_8334" s="T717">n.[n:case]</ta>
            <ta e="T719" id="Seg_8335" s="T718">n-n:poss-n:case</ta>
            <ta e="T720" id="Seg_8336" s="T719">v-v:tense-v:pred.pn</ta>
            <ta e="T722" id="Seg_8337" s="T721">n-n:(poss).[n:case]</ta>
            <ta e="T723" id="Seg_8338" s="T722">v-v&gt;adv</ta>
            <ta e="T724" id="Seg_8339" s="T723">v-v:tense.[v:pred.pn]</ta>
            <ta e="T725" id="Seg_8340" s="T724">ptcl</ta>
            <ta e="T727" id="Seg_8341" s="T726">adv</ta>
            <ta e="T728" id="Seg_8342" s="T727">ptcl</ta>
            <ta e="T729" id="Seg_8343" s="T728">v-v:tense.[v:pred.pn]</ta>
            <ta e="T730" id="Seg_8344" s="T729">ptcl</ta>
            <ta e="T731" id="Seg_8345" s="T730">adj-n:(num).[n:case]</ta>
            <ta e="T732" id="Seg_8346" s="T731">pers.[pro:case]</ta>
            <ta e="T733" id="Seg_8347" s="T732">n.[n:case]</ta>
            <ta e="T734" id="Seg_8348" s="T733">n.[n:case]</ta>
            <ta e="T735" id="Seg_8349" s="T734">v-v:tense-v:poss.pn</ta>
            <ta e="T736" id="Seg_8350" s="T735">interj</ta>
            <ta e="T737" id="Seg_8351" s="T736">n.[n:case]</ta>
            <ta e="T738" id="Seg_8352" s="T737">v-v:neg-v:pred.pn</ta>
            <ta e="T740" id="Seg_8353" s="T739">n-n:poss-n:case</ta>
            <ta e="T741" id="Seg_8354" s="T740">v-v:tense-v:poss.pn</ta>
            <ta e="T742" id="Seg_8355" s="T741">dempro</ta>
            <ta e="T743" id="Seg_8356" s="T742">ptcl</ta>
            <ta e="T744" id="Seg_8357" s="T743">n-n:case</ta>
            <ta e="T745" id="Seg_8358" s="T744">n-n:case</ta>
            <ta e="T746" id="Seg_8359" s="T745">v-v:cvb-v:pred.pn</ta>
            <ta e="T747" id="Seg_8360" s="T746">n-n&gt;v-v:cvb</ta>
            <ta e="T748" id="Seg_8361" s="T747">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T750" id="Seg_8362" s="T749">pers-pro:case</ta>
            <ta e="T751" id="Seg_8363" s="T750">n-n&gt;v-v:(neg)-v:mood.pn</ta>
            <ta e="T752" id="Seg_8364" s="T751">pers.[pro:case]</ta>
            <ta e="T753" id="Seg_8365" s="T752">dempro</ta>
            <ta e="T754" id="Seg_8366" s="T753">ptcl</ta>
            <ta e="T755" id="Seg_8367" s="T754">v-v:tense-v:poss.pn</ta>
            <ta e="T756" id="Seg_8368" s="T755">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T757" id="Seg_8369" s="T756">v-v:tense.[v:pred.pn]</ta>
            <ta e="T759" id="Seg_8370" s="T758">n.[n:case]</ta>
            <ta e="T760" id="Seg_8371" s="T759">n-n:(poss).[n:case]</ta>
            <ta e="T761" id="Seg_8372" s="T760">adv</ta>
            <ta e="T762" id="Seg_8373" s="T761">adj.[n:case]</ta>
            <ta e="T763" id="Seg_8374" s="T762">v-v:tense.[v:pred.pn]</ta>
            <ta e="T764" id="Seg_8375" s="T763">n.[n:case]</ta>
            <ta e="T765" id="Seg_8376" s="T764">n-n:(poss).[n:case]</ta>
            <ta e="T766" id="Seg_8377" s="T765">n-n:poss-n:case</ta>
            <ta e="T767" id="Seg_8378" s="T766">v-v:(ins)-v:mood.pn</ta>
            <ta e="T768" id="Seg_8379" s="T767">adv</ta>
            <ta e="T769" id="Seg_8380" s="T768">v-v:tense-v:poss.pn</ta>
            <ta e="T770" id="Seg_8381" s="T769">v-v:tense.[v:pred.pn]</ta>
            <ta e="T771" id="Seg_8382" s="T770">dempro</ta>
            <ta e="T772" id="Seg_8383" s="T771">n-n:poss-n:case</ta>
            <ta e="T773" id="Seg_8384" s="T772">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T775" id="Seg_8385" s="T774">ptcl</ta>
            <ta e="T776" id="Seg_8386" s="T775">dempro</ta>
            <ta e="T777" id="Seg_8387" s="T776">n-n:(poss).[n:case]</ta>
            <ta e="T778" id="Seg_8388" s="T777">v-v:cvb</ta>
            <ta e="T779" id="Seg_8389" s="T778">v-v:tense.[v:pred.pn]</ta>
            <ta e="T780" id="Seg_8390" s="T779">ptcl</ta>
            <ta e="T781" id="Seg_8391" s="T780">adj-n:(poss).[n:case]</ta>
            <ta e="T782" id="Seg_8392" s="T781">dempro</ta>
            <ta e="T783" id="Seg_8393" s="T782">v-v:tense-v:poss.pn</ta>
            <ta e="T784" id="Seg_8394" s="T783">v-v:(neg)-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_8395" s="T1">adv</ta>
            <ta e="T3" id="Seg_8396" s="T2">adv</ta>
            <ta e="T4" id="Seg_8397" s="T3">v</ta>
            <ta e="T5" id="Seg_8398" s="T4">v</ta>
            <ta e="T6" id="Seg_8399" s="T5">n</ta>
            <ta e="T8" id="Seg_8400" s="T7">n</ta>
            <ta e="T9" id="Seg_8401" s="T8">v</ta>
            <ta e="T10" id="Seg_8402" s="T9">n</ta>
            <ta e="T12" id="Seg_8403" s="T11">interj</ta>
            <ta e="T13" id="Seg_8404" s="T12">propr</ta>
            <ta e="T14" id="Seg_8405" s="T13">n</ta>
            <ta e="T15" id="Seg_8406" s="T14">v</ta>
            <ta e="T16" id="Seg_8407" s="T15">v</ta>
            <ta e="T18" id="Seg_8408" s="T17">que</ta>
            <ta e="T19" id="Seg_8409" s="T18">v</ta>
            <ta e="T21" id="Seg_8410" s="T20">interj</ta>
            <ta e="T22" id="Seg_8411" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_8412" s="T22">dempro</ta>
            <ta e="T24" id="Seg_8413" s="T23">n</ta>
            <ta e="T25" id="Seg_8414" s="T24">v</ta>
            <ta e="T26" id="Seg_8415" s="T25">v</ta>
            <ta e="T27" id="Seg_8416" s="T26">v</ta>
            <ta e="T28" id="Seg_8417" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_8418" s="T28">n</ta>
            <ta e="T30" id="Seg_8419" s="T29">ptcl</ta>
            <ta e="T31" id="Seg_8420" s="T30">v</ta>
            <ta e="T33" id="Seg_8421" s="T32">interj</ta>
            <ta e="T34" id="Seg_8422" s="T33">ptcl</ta>
            <ta e="T35" id="Seg_8423" s="T34">dempro</ta>
            <ta e="T36" id="Seg_8424" s="T35">n</ta>
            <ta e="T37" id="Seg_8425" s="T36">n</ta>
            <ta e="T38" id="Seg_8426" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_8427" s="T38">v</ta>
            <ta e="T40" id="Seg_8428" s="T39">que</ta>
            <ta e="T41" id="Seg_8429" s="T40">cop</ta>
            <ta e="T42" id="Seg_8430" s="T41">v</ta>
            <ta e="T43" id="Seg_8431" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_8432" s="T43">dempro</ta>
            <ta e="T45" id="Seg_8433" s="T44">n</ta>
            <ta e="T46" id="Seg_8434" s="T45">ptcl</ta>
            <ta e="T48" id="Seg_8435" s="T47">v</ta>
            <ta e="T49" id="Seg_8436" s="T48">v</ta>
            <ta e="T51" id="Seg_8437" s="T50">v</ta>
            <ta e="T52" id="Seg_8438" s="T51">v</ta>
            <ta e="T53" id="Seg_8439" s="T52">n</ta>
            <ta e="T54" id="Seg_8440" s="T53">ptcl</ta>
            <ta e="T56" id="Seg_8441" s="T55">adv</ta>
            <ta e="T57" id="Seg_8442" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_8443" s="T57">adv</ta>
            <ta e="T59" id="Seg_8444" s="T58">v</ta>
            <ta e="T61" id="Seg_8445" s="T60">v</ta>
            <ta e="T62" id="Seg_8446" s="T61">v</ta>
            <ta e="T63" id="Seg_8447" s="T62">cardnum</ta>
            <ta e="T64" id="Seg_8448" s="T63">n</ta>
            <ta e="T65" id="Seg_8449" s="T64">v</ta>
            <ta e="T66" id="Seg_8450" s="T65">ptcl</ta>
            <ta e="T68" id="Seg_8451" s="T67">n</ta>
            <ta e="T69" id="Seg_8452" s="T68">n</ta>
            <ta e="T70" id="Seg_8453" s="T69">propr</ta>
            <ta e="T72" id="Seg_8454" s="T71">n</ta>
            <ta e="T73" id="Seg_8455" s="T72">n</ta>
            <ta e="T74" id="Seg_8456" s="T73">que</ta>
            <ta e="T75" id="Seg_8457" s="T74">v</ta>
            <ta e="T76" id="Seg_8458" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_8459" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_8460" s="T77">adj</ta>
            <ta e="T79" id="Seg_8461" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_8462" s="T79">adj</ta>
            <ta e="T81" id="Seg_8463" s="T80">n</ta>
            <ta e="T82" id="Seg_8464" s="T81">que</ta>
            <ta e="T83" id="Seg_8465" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_8466" s="T83">v</ta>
            <ta e="T85" id="Seg_8467" s="T84">ptcl</ta>
            <ta e="T87" id="Seg_8468" s="T86">adv</ta>
            <ta e="T88" id="Seg_8469" s="T87">v</ta>
            <ta e="T89" id="Seg_8470" s="T88">n</ta>
            <ta e="T91" id="Seg_8471" s="T90">dempro</ta>
            <ta e="T92" id="Seg_8472" s="T91">ptcl</ta>
            <ta e="T93" id="Seg_8473" s="T92">v</ta>
            <ta e="T94" id="Seg_8474" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_8475" s="T94">v</ta>
            <ta e="T96" id="Seg_8476" s="T95">v</ta>
            <ta e="T97" id="Seg_8477" s="T96">v</ta>
            <ta e="T98" id="Seg_8478" s="T97">ptcl</ta>
            <ta e="T100" id="Seg_8479" s="T99">dempro</ta>
            <ta e="T101" id="Seg_8480" s="T100">que</ta>
            <ta e="T102" id="Seg_8481" s="T101">v</ta>
            <ta e="T103" id="Seg_8482" s="T102">pers</ta>
            <ta e="T104" id="Seg_8483" s="T103">n</ta>
            <ta e="T106" id="Seg_8484" s="T105">que</ta>
            <ta e="T107" id="Seg_8485" s="T106">v</ta>
            <ta e="T109" id="Seg_8486" s="T108">v</ta>
            <ta e="T110" id="Seg_8487" s="T109">n</ta>
            <ta e="T111" id="Seg_8488" s="T110">que</ta>
            <ta e="T112" id="Seg_8489" s="T111">v</ta>
            <ta e="T113" id="Seg_8490" s="T112">v</ta>
            <ta e="T114" id="Seg_8491" s="T113">ptcl</ta>
            <ta e="T115" id="Seg_8492" s="T114">n</ta>
            <ta e="T117" id="Seg_8493" s="T116">interj</ta>
            <ta e="T118" id="Seg_8494" s="T117">adv</ta>
            <ta e="T119" id="Seg_8495" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_8496" s="T119">n</ta>
            <ta e="T121" id="Seg_8497" s="T120">v</ta>
            <ta e="T122" id="Seg_8498" s="T121">n</ta>
            <ta e="T123" id="Seg_8499" s="T122">v</ta>
            <ta e="T125" id="Seg_8500" s="T124">n</ta>
            <ta e="T126" id="Seg_8501" s="T125">v</ta>
            <ta e="T127" id="Seg_8502" s="T126">adv</ta>
            <ta e="T128" id="Seg_8503" s="T127">quant</ta>
            <ta e="T129" id="Seg_8504" s="T128">n</ta>
            <ta e="T130" id="Seg_8505" s="T129">v</ta>
            <ta e="T132" id="Seg_8506" s="T131">adv</ta>
            <ta e="T133" id="Seg_8507" s="T132">n</ta>
            <ta e="T134" id="Seg_8508" s="T133">ptcl</ta>
            <ta e="T135" id="Seg_8509" s="T134">dempro</ta>
            <ta e="T136" id="Seg_8510" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_8511" s="T136">v</ta>
            <ta e="T138" id="Seg_8512" s="T137">v</ta>
            <ta e="T140" id="Seg_8513" s="T139">que</ta>
            <ta e="T141" id="Seg_8514" s="T140">adj</ta>
            <ta e="T142" id="Seg_8515" s="T141">v</ta>
            <ta e="T143" id="Seg_8516" s="T142">ptcl</ta>
            <ta e="T144" id="Seg_8517" s="T143">n</ta>
            <ta e="T146" id="Seg_8518" s="T145">n</ta>
            <ta e="T147" id="Seg_8519" s="T146">adj</ta>
            <ta e="T148" id="Seg_8520" s="T147">adv</ta>
            <ta e="T149" id="Seg_8521" s="T148">dempro</ta>
            <ta e="T150" id="Seg_8522" s="T149">post</ta>
            <ta e="T151" id="Seg_8523" s="T150">adv</ta>
            <ta e="T152" id="Seg_8524" s="T151">cop</ta>
            <ta e="T153" id="Seg_8525" s="T152">n</ta>
            <ta e="T155" id="Seg_8526" s="T154">adv</ta>
            <ta e="T156" id="Seg_8527" s="T155">n</ta>
            <ta e="T157" id="Seg_8528" s="T156">v</ta>
            <ta e="T159" id="Seg_8529" s="T158">v</ta>
            <ta e="T160" id="Seg_8530" s="T159">ptcl</ta>
            <ta e="T161" id="Seg_8531" s="T160">que</ta>
            <ta e="T162" id="Seg_8532" s="T161">n</ta>
            <ta e="T163" id="Seg_8533" s="T162">n</ta>
            <ta e="T164" id="Seg_8534" s="T163">v</ta>
            <ta e="T167" id="Seg_8535" s="T166">v</ta>
            <ta e="T168" id="Seg_8536" s="T167">v</ta>
            <ta e="T169" id="Seg_8537" s="T168">n</ta>
            <ta e="T171" id="Seg_8538" s="T170">ptcl</ta>
            <ta e="T172" id="Seg_8539" s="T171">dempro</ta>
            <ta e="T173" id="Seg_8540" s="T172">adv</ta>
            <ta e="T174" id="Seg_8541" s="T173">adv</ta>
            <ta e="T175" id="Seg_8542" s="T174">adv</ta>
            <ta e="T176" id="Seg_8543" s="T175">adv</ta>
            <ta e="T177" id="Seg_8544" s="T176">dempro</ta>
            <ta e="T178" id="Seg_8545" s="T177">v</ta>
            <ta e="T179" id="Seg_8546" s="T178">v</ta>
            <ta e="T180" id="Seg_8547" s="T179">dempro</ta>
            <ta e="T181" id="Seg_8548" s="T180">propr</ta>
            <ta e="T182" id="Seg_8549" s="T181">cop</ta>
            <ta e="T183" id="Seg_8550" s="T182">aux</ta>
            <ta e="T184" id="Seg_8551" s="T183">ptcl</ta>
            <ta e="T186" id="Seg_8552" s="T185">n</ta>
            <ta e="T187" id="Seg_8553" s="T186">ptcl</ta>
            <ta e="T188" id="Seg_8554" s="T187">n</ta>
            <ta e="T189" id="Seg_8555" s="T188">n</ta>
            <ta e="T190" id="Seg_8556" s="T189">v</ta>
            <ta e="T191" id="Seg_8557" s="T190">emphpro</ta>
            <ta e="T192" id="Seg_8558" s="T191">v</ta>
            <ta e="T194" id="Seg_8559" s="T193">n</ta>
            <ta e="T196" id="Seg_8560" s="T195">v</ta>
            <ta e="T198" id="Seg_8561" s="T197">adv</ta>
            <ta e="T199" id="Seg_8562" s="T198">v</ta>
            <ta e="T200" id="Seg_8563" s="T199">n</ta>
            <ta e="T201" id="Seg_8564" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_8565" s="T201">cop</ta>
            <ta e="T203" id="Seg_8566" s="T202">adj</ta>
            <ta e="T204" id="Seg_8567" s="T203">adv</ta>
            <ta e="T205" id="Seg_8568" s="T204">v</ta>
            <ta e="T206" id="Seg_8569" s="T205">ptcl</ta>
            <ta e="T207" id="Seg_8570" s="T206">dempro</ta>
            <ta e="T208" id="Seg_8571" s="T207">n</ta>
            <ta e="T210" id="Seg_8572" s="T209">dempro</ta>
            <ta e="T211" id="Seg_8573" s="T210">post</ta>
            <ta e="T212" id="Seg_8574" s="T211">propr</ta>
            <ta e="T213" id="Seg_8575" s="T212">propr</ta>
            <ta e="T214" id="Seg_8576" s="T213">propr</ta>
            <ta e="T215" id="Seg_8577" s="T214">que</ta>
            <ta e="T216" id="Seg_8578" s="T215">que</ta>
            <ta e="T217" id="Seg_8579" s="T216">propr</ta>
            <ta e="T218" id="Seg_8580" s="T217">v</ta>
            <ta e="T219" id="Seg_8581" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_8582" s="T219">dempro</ta>
            <ta e="T221" id="Seg_8583" s="T220">adv</ta>
            <ta e="T222" id="Seg_8584" s="T221">v</ta>
            <ta e="T223" id="Seg_8585" s="T222">ptcl</ta>
            <ta e="T225" id="Seg_8586" s="T224">ptcl</ta>
            <ta e="T226" id="Seg_8587" s="T225">dempro</ta>
            <ta e="T227" id="Seg_8588" s="T226">propr</ta>
            <ta e="T229" id="Seg_8589" s="T228">dempro</ta>
            <ta e="T230" id="Seg_8590" s="T229">v</ta>
            <ta e="T231" id="Seg_8591" s="T230">n</ta>
            <ta e="T232" id="Seg_8592" s="T231">v</ta>
            <ta e="T233" id="Seg_8593" s="T232">adv</ta>
            <ta e="T234" id="Seg_8594" s="T233">adv</ta>
            <ta e="T235" id="Seg_8595" s="T234">adv</ta>
            <ta e="T236" id="Seg_8596" s="T235">ptcl</ta>
            <ta e="T237" id="Seg_8597" s="T236">dempro</ta>
            <ta e="T238" id="Seg_8598" s="T237">cop</ta>
            <ta e="T240" id="Seg_8599" s="T239">adv</ta>
            <ta e="T241" id="Seg_8600" s="T240">adv</ta>
            <ta e="T242" id="Seg_8601" s="T241">dempro</ta>
            <ta e="T243" id="Seg_8602" s="T242">dempro</ta>
            <ta e="T244" id="Seg_8603" s="T243">v</ta>
            <ta e="T245" id="Seg_8604" s="T244">ptcl</ta>
            <ta e="T246" id="Seg_8605" s="T245">propr</ta>
            <ta e="T247" id="Seg_8606" s="T246">v</ta>
            <ta e="T248" id="Seg_8607" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_8608" s="T248">cop</ta>
            <ta e="T250" id="Seg_8609" s="T249">ptcl</ta>
            <ta e="T252" id="Seg_8610" s="T251">interj</ta>
            <ta e="T253" id="Seg_8611" s="T252">que</ta>
            <ta e="T254" id="Seg_8612" s="T253">n</ta>
            <ta e="T255" id="Seg_8613" s="T254">v</ta>
            <ta e="T256" id="Seg_8614" s="T255">dempro</ta>
            <ta e="T258" id="Seg_8615" s="T257">adv</ta>
            <ta e="T259" id="Seg_8616" s="T258">dempro</ta>
            <ta e="T260" id="Seg_8617" s="T259">n</ta>
            <ta e="T261" id="Seg_8618" s="T260">adj</ta>
            <ta e="T262" id="Seg_8619" s="T261">n</ta>
            <ta e="T263" id="Seg_8620" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_8621" s="T263">n</ta>
            <ta e="T265" id="Seg_8622" s="T264">n</ta>
            <ta e="T266" id="Seg_8623" s="T265">ptcl</ta>
            <ta e="T267" id="Seg_8624" s="T266">cardnum</ta>
            <ta e="T268" id="Seg_8625" s="T267">cardnum</ta>
            <ta e="T269" id="Seg_8626" s="T268">n</ta>
            <ta e="T270" id="Seg_8627" s="T269">n</ta>
            <ta e="T272" id="Seg_8628" s="T271">adv</ta>
            <ta e="T273" id="Seg_8629" s="T272">v</ta>
            <ta e="T274" id="Seg_8630" s="T273">dempro</ta>
            <ta e="T275" id="Seg_8631" s="T274">n</ta>
            <ta e="T276" id="Seg_8632" s="T275">v</ta>
            <ta e="T277" id="Seg_8633" s="T276">propr</ta>
            <ta e="T278" id="Seg_8634" s="T277">v</ta>
            <ta e="T280" id="Seg_8635" s="T279">propr</ta>
            <ta e="T281" id="Seg_8636" s="T280">n</ta>
            <ta e="T283" id="Seg_8637" s="T282">dempro</ta>
            <ta e="T284" id="Seg_8638" s="T283">v</ta>
            <ta e="T285" id="Seg_8639" s="T284">cardnum</ta>
            <ta e="T286" id="Seg_8640" s="T285">adj</ta>
            <ta e="T287" id="Seg_8641" s="T286">dempro</ta>
            <ta e="T289" id="Seg_8642" s="T288">cardnum</ta>
            <ta e="T290" id="Seg_8643" s="T289">n</ta>
            <ta e="T292" id="Seg_8644" s="T291">cardnum</ta>
            <ta e="T293" id="Seg_8645" s="T292">n</ta>
            <ta e="T295" id="Seg_8646" s="T294">ptcl</ta>
            <ta e="T296" id="Seg_8647" s="T295">dempro</ta>
            <ta e="T297" id="Seg_8648" s="T296">n</ta>
            <ta e="T298" id="Seg_8649" s="T297">v</ta>
            <ta e="T299" id="Seg_8650" s="T298">v</ta>
            <ta e="T300" id="Seg_8651" s="T299">v</ta>
            <ta e="T301" id="Seg_8652" s="T300">dempro</ta>
            <ta e="T302" id="Seg_8653" s="T301">n</ta>
            <ta e="T303" id="Seg_8654" s="T302">v</ta>
            <ta e="T304" id="Seg_8655" s="T303">v</ta>
            <ta e="T305" id="Seg_8656" s="T304">adj</ta>
            <ta e="T306" id="Seg_8657" s="T305">n</ta>
            <ta e="T308" id="Seg_8658" s="T307">que</ta>
            <ta e="T309" id="Seg_8659" s="T308">ptcl</ta>
            <ta e="T310" id="Seg_8660" s="T309">que</ta>
            <ta e="T311" id="Seg_8661" s="T310">n</ta>
            <ta e="T312" id="Seg_8662" s="T311">ptcl</ta>
            <ta e="T313" id="Seg_8663" s="T312">conj</ta>
            <ta e="T315" id="Seg_8664" s="T314">dempro</ta>
            <ta e="T316" id="Seg_8665" s="T315">v</ta>
            <ta e="T317" id="Seg_8666" s="T316">v</ta>
            <ta e="T318" id="Seg_8667" s="T317">adv</ta>
            <ta e="T319" id="Seg_8668" s="T318">v</ta>
            <ta e="T320" id="Seg_8669" s="T319">n</ta>
            <ta e="T321" id="Seg_8670" s="T320">cardnum</ta>
            <ta e="T322" id="Seg_8671" s="T321">ptcl</ta>
            <ta e="T323" id="Seg_8672" s="T322">adj</ta>
            <ta e="T325" id="Seg_8673" s="T324">cardnum</ta>
            <ta e="T326" id="Seg_8674" s="T325">adj</ta>
            <ta e="T327" id="Seg_8675" s="T326">cardnum</ta>
            <ta e="T328" id="Seg_8676" s="T327">n</ta>
            <ta e="T329" id="Seg_8677" s="T328">n</ta>
            <ta e="T330" id="Seg_8678" s="T329">posspr</ta>
            <ta e="T331" id="Seg_8679" s="T330">ptcl</ta>
            <ta e="T332" id="Seg_8680" s="T331">v</ta>
            <ta e="T334" id="Seg_8681" s="T333">dempro</ta>
            <ta e="T335" id="Seg_8682" s="T334">ptcl</ta>
            <ta e="T336" id="Seg_8683" s="T335">n</ta>
            <ta e="T337" id="Seg_8684" s="T336">v</ta>
            <ta e="T338" id="Seg_8685" s="T337">v</ta>
            <ta e="T339" id="Seg_8686" s="T338">v</ta>
            <ta e="T340" id="Seg_8687" s="T339">dempro</ta>
            <ta e="T341" id="Seg_8688" s="T340">n</ta>
            <ta e="T342" id="Seg_8689" s="T341">propr</ta>
            <ta e="T343" id="Seg_8690" s="T342">v</ta>
            <ta e="T344" id="Seg_8691" s="T343">n</ta>
            <ta e="T345" id="Seg_8692" s="T344">propr</ta>
            <ta e="T346" id="Seg_8693" s="T345">v</ta>
            <ta e="T347" id="Seg_8694" s="T346">n</ta>
            <ta e="T348" id="Seg_8695" s="T347">ptcl</ta>
            <ta e="T349" id="Seg_8696" s="T348">cop</ta>
            <ta e="T351" id="Seg_8697" s="T350">dempro</ta>
            <ta e="T352" id="Seg_8698" s="T351">v</ta>
            <ta e="T353" id="Seg_8699" s="T352">dempro</ta>
            <ta e="T354" id="Seg_8700" s="T353">n</ta>
            <ta e="T356" id="Seg_8701" s="T355">interj</ta>
            <ta e="T357" id="Seg_8702" s="T356">n</ta>
            <ta e="T358" id="Seg_8703" s="T357">dempro</ta>
            <ta e="T359" id="Seg_8704" s="T358">dempro</ta>
            <ta e="T360" id="Seg_8705" s="T359">n</ta>
            <ta e="T361" id="Seg_8706" s="T360">adj</ta>
            <ta e="T362" id="Seg_8707" s="T361">n</ta>
            <ta e="T363" id="Seg_8708" s="T362">v</ta>
            <ta e="T364" id="Seg_8709" s="T363">n</ta>
            <ta e="T365" id="Seg_8710" s="T364">v</ta>
            <ta e="T366" id="Seg_8711" s="T365">n</ta>
            <ta e="T368" id="Seg_8712" s="T367">ptcl</ta>
            <ta e="T369" id="Seg_8713" s="T368">dempro</ta>
            <ta e="T370" id="Seg_8714" s="T369">v</ta>
            <ta e="T371" id="Seg_8715" s="T370">propr</ta>
            <ta e="T372" id="Seg_8716" s="T371">v</ta>
            <ta e="T374" id="Seg_8717" s="T373">dempro</ta>
            <ta e="T375" id="Seg_8718" s="T374">v</ta>
            <ta e="T376" id="Seg_8719" s="T375">v</ta>
            <ta e="T377" id="Seg_8720" s="T376">propr</ta>
            <ta e="T378" id="Seg_8721" s="T377">v</ta>
            <ta e="T379" id="Seg_8722" s="T378">ptcl</ta>
            <ta e="T380" id="Seg_8723" s="T379">v</ta>
            <ta e="T381" id="Seg_8724" s="T380">interj</ta>
            <ta e="T382" id="Seg_8725" s="T381">dempro</ta>
            <ta e="T383" id="Seg_8726" s="T382">v</ta>
            <ta e="T384" id="Seg_8727" s="T383">n</ta>
            <ta e="T385" id="Seg_8728" s="T384">v</ta>
            <ta e="T386" id="Seg_8729" s="T385">adj</ta>
            <ta e="T387" id="Seg_8730" s="T386">n</ta>
            <ta e="T388" id="Seg_8731" s="T387">v</ta>
            <ta e="T389" id="Seg_8732" s="T388">adj</ta>
            <ta e="T390" id="Seg_8733" s="T389">n</ta>
            <ta e="T391" id="Seg_8734" s="T390">post</ta>
            <ta e="T392" id="Seg_8735" s="T391">v</ta>
            <ta e="T393" id="Seg_8736" s="T392">v</ta>
            <ta e="T395" id="Seg_8737" s="T394">dempro</ta>
            <ta e="T396" id="Seg_8738" s="T395">n</ta>
            <ta e="T397" id="Seg_8739" s="T396">v</ta>
            <ta e="T398" id="Seg_8740" s="T397">dempro</ta>
            <ta e="T399" id="Seg_8741" s="T398">n</ta>
            <ta e="T400" id="Seg_8742" s="T399">ptcl</ta>
            <ta e="T402" id="Seg_8743" s="T401">ptcl</ta>
            <ta e="T403" id="Seg_8744" s="T402">adj</ta>
            <ta e="T404" id="Seg_8745" s="T403">v</ta>
            <ta e="T405" id="Seg_8746" s="T404">aux</ta>
            <ta e="T406" id="Seg_8747" s="T405">adv</ta>
            <ta e="T407" id="Seg_8748" s="T406">n</ta>
            <ta e="T408" id="Seg_8749" s="T407">v</ta>
            <ta e="T409" id="Seg_8750" s="T408">v</ta>
            <ta e="T411" id="Seg_8751" s="T410">cardnum</ta>
            <ta e="T412" id="Seg_8752" s="T411">n</ta>
            <ta e="T413" id="Seg_8753" s="T412">post</ta>
            <ta e="T414" id="Seg_8754" s="T413">v</ta>
            <ta e="T415" id="Seg_8755" s="T414">n</ta>
            <ta e="T416" id="Seg_8756" s="T415">adv</ta>
            <ta e="T417" id="Seg_8757" s="T416">v</ta>
            <ta e="T418" id="Seg_8758" s="T417">post</ta>
            <ta e="T419" id="Seg_8759" s="T418">ptcl</ta>
            <ta e="T420" id="Seg_8760" s="T419">n</ta>
            <ta e="T421" id="Seg_8761" s="T420">n</ta>
            <ta e="T422" id="Seg_8762" s="T421">v</ta>
            <ta e="T423" id="Seg_8763" s="T422">dempro</ta>
            <ta e="T424" id="Seg_8764" s="T423">n</ta>
            <ta e="T426" id="Seg_8765" s="T425">n</ta>
            <ta e="T427" id="Seg_8766" s="T426">adj</ta>
            <ta e="T429" id="Seg_8767" s="T428">ptcl</ta>
            <ta e="T430" id="Seg_8768" s="T429">v</ta>
            <ta e="T431" id="Seg_8769" s="T430">post</ta>
            <ta e="T432" id="Seg_8770" s="T431">ptcl</ta>
            <ta e="T433" id="Seg_8771" s="T432">n</ta>
            <ta e="T434" id="Seg_8772" s="T433">ptcl</ta>
            <ta e="T435" id="Seg_8773" s="T434">que</ta>
            <ta e="T436" id="Seg_8774" s="T435">v</ta>
            <ta e="T438" id="Seg_8775" s="T437">n</ta>
            <ta e="T439" id="Seg_8776" s="T438">v</ta>
            <ta e="T440" id="Seg_8777" s="T439">adv</ta>
            <ta e="T441" id="Seg_8778" s="T440">adv</ta>
            <ta e="T442" id="Seg_8779" s="T441">n</ta>
            <ta e="T443" id="Seg_8780" s="T442">n</ta>
            <ta e="T445" id="Seg_8781" s="T444">n</ta>
            <ta e="T446" id="Seg_8782" s="T445">n</ta>
            <ta e="T447" id="Seg_8783" s="T446">post</ta>
            <ta e="T448" id="Seg_8784" s="T447">post</ta>
            <ta e="T449" id="Seg_8785" s="T448">v</ta>
            <ta e="T450" id="Seg_8786" s="T449">ptcl</ta>
            <ta e="T451" id="Seg_8787" s="T450">n</ta>
            <ta e="T452" id="Seg_8788" s="T451">v</ta>
            <ta e="T453" id="Seg_8789" s="T452">ptcl</ta>
            <ta e="T455" id="Seg_8790" s="T454">v</ta>
            <ta e="T456" id="Seg_8791" s="T455">aux</ta>
            <ta e="T457" id="Seg_8792" s="T456">n</ta>
            <ta e="T458" id="Seg_8793" s="T457">n</ta>
            <ta e="T459" id="Seg_8794" s="T458">v</ta>
            <ta e="T460" id="Seg_8795" s="T459">v</ta>
            <ta e="T461" id="Seg_8796" s="T460">dempro</ta>
            <ta e="T462" id="Seg_8797" s="T461">v</ta>
            <ta e="T463" id="Seg_8798" s="T462">v</ta>
            <ta e="T464" id="Seg_8799" s="T463">dempro</ta>
            <ta e="T465" id="Seg_8800" s="T464">n</ta>
            <ta e="T466" id="Seg_8801" s="T465">v</ta>
            <ta e="T467" id="Seg_8802" s="T466">ptcl</ta>
            <ta e="T469" id="Seg_8803" s="T468">adv</ta>
            <ta e="T470" id="Seg_8804" s="T469">n</ta>
            <ta e="T471" id="Seg_8805" s="T470">v</ta>
            <ta e="T472" id="Seg_8806" s="T471">dempro</ta>
            <ta e="T473" id="Seg_8807" s="T472">n</ta>
            <ta e="T474" id="Seg_8808" s="T473">n</ta>
            <ta e="T475" id="Seg_8809" s="T474">interj</ta>
            <ta e="T476" id="Seg_8810" s="T475">v</ta>
            <ta e="T477" id="Seg_8811" s="T476">v</ta>
            <ta e="T478" id="Seg_8812" s="T477">n</ta>
            <ta e="T479" id="Seg_8813" s="T478">interj</ta>
            <ta e="T480" id="Seg_8814" s="T479">que</ta>
            <ta e="T481" id="Seg_8815" s="T480">n</ta>
            <ta e="T483" id="Seg_8816" s="T482">adv</ta>
            <ta e="T484" id="Seg_8817" s="T483">ptcl</ta>
            <ta e="T485" id="Seg_8818" s="T484">v</ta>
            <ta e="T486" id="Seg_8819" s="T485">v</ta>
            <ta e="T487" id="Seg_8820" s="T486">ptcl</ta>
            <ta e="T488" id="Seg_8821" s="T487">adv</ta>
            <ta e="T489" id="Seg_8822" s="T488">v</ta>
            <ta e="T490" id="Seg_8823" s="T489">v</ta>
            <ta e="T491" id="Seg_8824" s="T490">que</ta>
            <ta e="T492" id="Seg_8825" s="T491">ptcl</ta>
            <ta e="T494" id="Seg_8826" s="T493">ptcl</ta>
            <ta e="T495" id="Seg_8827" s="T494">dempro</ta>
            <ta e="T496" id="Seg_8828" s="T495">v</ta>
            <ta e="T497" id="Seg_8829" s="T496">dempro</ta>
            <ta e="T498" id="Seg_8830" s="T497">v</ta>
            <ta e="T499" id="Seg_8831" s="T498">ptcl</ta>
            <ta e="T500" id="Seg_8832" s="T499">v</ta>
            <ta e="T501" id="Seg_8833" s="T500">v</ta>
            <ta e="T502" id="Seg_8834" s="T501">v</ta>
            <ta e="T503" id="Seg_8835" s="T502">ptcl</ta>
            <ta e="T504" id="Seg_8836" s="T503">v</ta>
            <ta e="T506" id="Seg_8837" s="T505">ptcl</ta>
            <ta e="T507" id="Seg_8838" s="T506">dempro</ta>
            <ta e="T508" id="Seg_8839" s="T507">v</ta>
            <ta e="T509" id="Seg_8840" s="T508">aux</ta>
            <ta e="T510" id="Seg_8841" s="T509">ptcl</ta>
            <ta e="T511" id="Seg_8842" s="T510">v</ta>
            <ta e="T512" id="Seg_8843" s="T511">ptcl</ta>
            <ta e="T513" id="Seg_8844" s="T512">dempro</ta>
            <ta e="T514" id="Seg_8845" s="T513">n</ta>
            <ta e="T516" id="Seg_8846" s="T515">v</ta>
            <ta e="T517" id="Seg_8847" s="T516">n</ta>
            <ta e="T518" id="Seg_8848" s="T517">v</ta>
            <ta e="T519" id="Seg_8849" s="T518">ptcl</ta>
            <ta e="T520" id="Seg_8850" s="T519">ptcl</ta>
            <ta e="T521" id="Seg_8851" s="T520">n</ta>
            <ta e="T522" id="Seg_8852" s="T521">v</ta>
            <ta e="T523" id="Seg_8853" s="T522">v</ta>
            <ta e="T525" id="Seg_8854" s="T524">interj</ta>
            <ta e="T526" id="Seg_8855" s="T525">dempro</ta>
            <ta e="T527" id="Seg_8856" s="T526">dempro</ta>
            <ta e="T528" id="Seg_8857" s="T527">n</ta>
            <ta e="T529" id="Seg_8858" s="T528">v</ta>
            <ta e="T530" id="Seg_8859" s="T529">v</ta>
            <ta e="T532" id="Seg_8860" s="T531">ptcl</ta>
            <ta e="T533" id="Seg_8861" s="T532">dempro</ta>
            <ta e="T534" id="Seg_8862" s="T533">n</ta>
            <ta e="T535" id="Seg_8863" s="T534">v</ta>
            <ta e="T536" id="Seg_8864" s="T535">v</ta>
            <ta e="T537" id="Seg_8865" s="T536">ptcl</ta>
            <ta e="T538" id="Seg_8866" s="T537">n</ta>
            <ta e="T539" id="Seg_8867" s="T538">v</ta>
            <ta e="T540" id="Seg_8868" s="T539">pers</ta>
            <ta e="T541" id="Seg_8869" s="T540">dempro</ta>
            <ta e="T542" id="Seg_8870" s="T541">n</ta>
            <ta e="T543" id="Seg_8871" s="T542">v</ta>
            <ta e="T545" id="Seg_8872" s="T544">n</ta>
            <ta e="T546" id="Seg_8873" s="T545">n</ta>
            <ta e="T547" id="Seg_8874" s="T546">dempro</ta>
            <ta e="T548" id="Seg_8875" s="T547">n</ta>
            <ta e="T550" id="Seg_8876" s="T549">adv</ta>
            <ta e="T551" id="Seg_8877" s="T550">dempro</ta>
            <ta e="T552" id="Seg_8878" s="T551">n</ta>
            <ta e="T553" id="Seg_8879" s="T552">n</ta>
            <ta e="T554" id="Seg_8880" s="T553">v</ta>
            <ta e="T555" id="Seg_8881" s="T554">v</ta>
            <ta e="T556" id="Seg_8882" s="T555">pers</ta>
            <ta e="T557" id="Seg_8883" s="T556">pers</ta>
            <ta e="T558" id="Seg_8884" s="T557">v</ta>
            <ta e="T559" id="Seg_8885" s="T558">dempro</ta>
            <ta e="T560" id="Seg_8886" s="T559">v</ta>
            <ta e="T561" id="Seg_8887" s="T560">ptcl</ta>
            <ta e="T562" id="Seg_8888" s="T561">n</ta>
            <ta e="T563" id="Seg_8889" s="T562">v</ta>
            <ta e="T565" id="Seg_8890" s="T564">n</ta>
            <ta e="T566" id="Seg_8891" s="T565">ptcl</ta>
            <ta e="T567" id="Seg_8892" s="T566">n</ta>
            <ta e="T568" id="Seg_8893" s="T567">adv</ta>
            <ta e="T569" id="Seg_8894" s="T568">v</ta>
            <ta e="T570" id="Seg_8895" s="T569">aux</ta>
            <ta e="T571" id="Seg_8896" s="T570">n</ta>
            <ta e="T572" id="Seg_8897" s="T571">v</ta>
            <ta e="T573" id="Seg_8898" s="T572">v</ta>
            <ta e="T574" id="Seg_8899" s="T573">v</ta>
            <ta e="T575" id="Seg_8900" s="T574">v</ta>
            <ta e="T576" id="Seg_8901" s="T575">v</ta>
            <ta e="T578" id="Seg_8902" s="T577">interj</ta>
            <ta e="T579" id="Seg_8903" s="T578">que</ta>
            <ta e="T580" id="Seg_8904" s="T579">adj</ta>
            <ta e="T581" id="Seg_8905" s="T580">n</ta>
            <ta e="T582" id="Seg_8906" s="T581">v</ta>
            <ta e="T583" id="Seg_8907" s="T582">n</ta>
            <ta e="T584" id="Seg_8908" s="T583">ptcl</ta>
            <ta e="T585" id="Seg_8909" s="T584">v</ta>
            <ta e="T586" id="Seg_8910" s="T585">v</ta>
            <ta e="T587" id="Seg_8911" s="T586">n</ta>
            <ta e="T588" id="Seg_8912" s="T587">ptcl</ta>
            <ta e="T589" id="Seg_8913" s="T588">v</ta>
            <ta e="T591" id="Seg_8914" s="T590">ptcl</ta>
            <ta e="T592" id="Seg_8915" s="T591">dempro</ta>
            <ta e="T593" id="Seg_8916" s="T592">v</ta>
            <ta e="T594" id="Seg_8917" s="T593">v</ta>
            <ta e="T595" id="Seg_8918" s="T594">post</ta>
            <ta e="T596" id="Seg_8919" s="T595">ptcl</ta>
            <ta e="T597" id="Seg_8920" s="T596">dempro</ta>
            <ta e="T598" id="Seg_8921" s="T597">interj</ta>
            <ta e="T599" id="Seg_8922" s="T598">n</ta>
            <ta e="T600" id="Seg_8923" s="T599">que</ta>
            <ta e="T601" id="Seg_8924" s="T600">que</ta>
            <ta e="T602" id="Seg_8925" s="T601">cop</ta>
            <ta e="T603" id="Seg_8926" s="T602">ptcl</ta>
            <ta e="T604" id="Seg_8927" s="T603">n</ta>
            <ta e="T605" id="Seg_8928" s="T604">v</ta>
            <ta e="T607" id="Seg_8929" s="T606">que</ta>
            <ta e="T608" id="Seg_8930" s="T607">cop</ta>
            <ta e="T609" id="Seg_8931" s="T608">v</ta>
            <ta e="T611" id="Seg_8932" s="T610">ptcl</ta>
            <ta e="T612" id="Seg_8933" s="T611">dempro</ta>
            <ta e="T613" id="Seg_8934" s="T612">cardnum</ta>
            <ta e="T614" id="Seg_8935" s="T613">n</ta>
            <ta e="T615" id="Seg_8936" s="T614">cardnum</ta>
            <ta e="T616" id="Seg_8937" s="T615">n</ta>
            <ta e="T617" id="Seg_8938" s="T616">v</ta>
            <ta e="T618" id="Seg_8939" s="T617">ptcl</ta>
            <ta e="T620" id="Seg_8940" s="T619">ptcl</ta>
            <ta e="T621" id="Seg_8941" s="T620">dempro</ta>
            <ta e="T622" id="Seg_8942" s="T621">n</ta>
            <ta e="T623" id="Seg_8943" s="T622">v</ta>
            <ta e="T624" id="Seg_8944" s="T623">n</ta>
            <ta e="T625" id="Seg_8945" s="T624">v</ta>
            <ta e="T626" id="Seg_8946" s="T625">v</ta>
            <ta e="T627" id="Seg_8947" s="T626">n</ta>
            <ta e="T629" id="Seg_8948" s="T628">adv</ta>
            <ta e="T630" id="Seg_8949" s="T629">dempro</ta>
            <ta e="T631" id="Seg_8950" s="T630">v</ta>
            <ta e="T632" id="Seg_8951" s="T631">v</ta>
            <ta e="T633" id="Seg_8952" s="T632">n</ta>
            <ta e="T634" id="Seg_8953" s="T633">post</ta>
            <ta e="T636" id="Seg_8954" s="T635">dempro</ta>
            <ta e="T637" id="Seg_8955" s="T636">v</ta>
            <ta e="T638" id="Seg_8956" s="T637">ptcl</ta>
            <ta e="T639" id="Seg_8957" s="T638">dempro</ta>
            <ta e="T641" id="Seg_8958" s="T640">ptcl</ta>
            <ta e="T642" id="Seg_8959" s="T641">n</ta>
            <ta e="T643" id="Seg_8960" s="T642">v</ta>
            <ta e="T645" id="Seg_8961" s="T644">cardnum</ta>
            <ta e="T646" id="Seg_8962" s="T645">n</ta>
            <ta e="T647" id="Seg_8963" s="T646">aux</ta>
            <ta e="T648" id="Seg_8964" s="T647">ptcl</ta>
            <ta e="T649" id="Seg_8965" s="T648">que</ta>
            <ta e="T650" id="Seg_8966" s="T649">ptcl</ta>
            <ta e="T652" id="Seg_8967" s="T651">ptcl</ta>
            <ta e="T653" id="Seg_8968" s="T652">adv</ta>
            <ta e="T654" id="Seg_8969" s="T653">n</ta>
            <ta e="T655" id="Seg_8970" s="T654">v</ta>
            <ta e="T656" id="Seg_8971" s="T655">dempro</ta>
            <ta e="T657" id="Seg_8972" s="T656">n</ta>
            <ta e="T658" id="Seg_8973" s="T657">ptcl</ta>
            <ta e="T659" id="Seg_8974" s="T658">v</ta>
            <ta e="T660" id="Seg_8975" s="T659">ptcl</ta>
            <ta e="T661" id="Seg_8976" s="T660">n</ta>
            <ta e="T662" id="Seg_8977" s="T661">v</ta>
            <ta e="T664" id="Seg_8978" s="T663">pers</ta>
            <ta e="T665" id="Seg_8979" s="T664">n</ta>
            <ta e="T666" id="Seg_8980" s="T665">n</ta>
            <ta e="T667" id="Seg_8981" s="T666">pers</ta>
            <ta e="T668" id="Seg_8982" s="T667">n</ta>
            <ta e="T669" id="Seg_8983" s="T668">n</ta>
            <ta e="T670" id="Seg_8984" s="T669">v</ta>
            <ta e="T671" id="Seg_8985" s="T670">ptcl</ta>
            <ta e="T672" id="Seg_8986" s="T671">n</ta>
            <ta e="T674" id="Seg_8987" s="T673">que</ta>
            <ta e="T675" id="Seg_8988" s="T674">n</ta>
            <ta e="T676" id="Seg_8989" s="T675">n</ta>
            <ta e="T677" id="Seg_8990" s="T676">dempro</ta>
            <ta e="T678" id="Seg_8991" s="T677">v</ta>
            <ta e="T679" id="Seg_8992" s="T678">aux</ta>
            <ta e="T680" id="Seg_8993" s="T679">v</ta>
            <ta e="T681" id="Seg_8994" s="T680">ptcl</ta>
            <ta e="T682" id="Seg_8995" s="T681">que</ta>
            <ta e="T683" id="Seg_8996" s="T682">adj</ta>
            <ta e="T684" id="Seg_8997" s="T683">n</ta>
            <ta e="T685" id="Seg_8998" s="T684">ptcl</ta>
            <ta e="T686" id="Seg_8999" s="T685">ptcl</ta>
            <ta e="T687" id="Seg_9000" s="T686">posspr</ta>
            <ta e="T688" id="Seg_9001" s="T687">propr</ta>
            <ta e="T690" id="Seg_9002" s="T689">v</ta>
            <ta e="T691" id="Seg_9003" s="T690">aux</ta>
            <ta e="T692" id="Seg_9004" s="T691">n</ta>
            <ta e="T694" id="Seg_9005" s="T693">adv</ta>
            <ta e="T695" id="Seg_9006" s="T694">dempro</ta>
            <ta e="T696" id="Seg_9007" s="T695">n</ta>
            <ta e="T697" id="Seg_9008" s="T696">ptcl</ta>
            <ta e="T698" id="Seg_9009" s="T697">v</ta>
            <ta e="T699" id="Seg_9010" s="T698">v</ta>
            <ta e="T700" id="Seg_9011" s="T699">n</ta>
            <ta e="T701" id="Seg_9012" s="T700">n</ta>
            <ta e="T703" id="Seg_9013" s="T702">n</ta>
            <ta e="T704" id="Seg_9014" s="T703">ptcl</ta>
            <ta e="T705" id="Seg_9015" s="T704">v</ta>
            <ta e="T706" id="Seg_9016" s="T705">ptcl</ta>
            <ta e="T707" id="Seg_9017" s="T706">v</ta>
            <ta e="T708" id="Seg_9018" s="T707">post</ta>
            <ta e="T710" id="Seg_9019" s="T709">adv</ta>
            <ta e="T711" id="Seg_9020" s="T710">v</ta>
            <ta e="T713" id="Seg_9021" s="T712">v</ta>
            <ta e="T714" id="Seg_9022" s="T713">v</ta>
            <ta e="T715" id="Seg_9023" s="T714">n</ta>
            <ta e="T716" id="Seg_9024" s="T715">v</ta>
            <ta e="T717" id="Seg_9025" s="T716">ptcl</ta>
            <ta e="T718" id="Seg_9026" s="T717">n</ta>
            <ta e="T719" id="Seg_9027" s="T718">n</ta>
            <ta e="T720" id="Seg_9028" s="T719">v</ta>
            <ta e="T722" id="Seg_9029" s="T721">n</ta>
            <ta e="T723" id="Seg_9030" s="T722">adv</ta>
            <ta e="T724" id="Seg_9031" s="T723">v</ta>
            <ta e="T725" id="Seg_9032" s="T724">ptcl</ta>
            <ta e="T727" id="Seg_9033" s="T726">adv</ta>
            <ta e="T728" id="Seg_9034" s="T727">ptcl</ta>
            <ta e="T729" id="Seg_9035" s="T728">v</ta>
            <ta e="T730" id="Seg_9036" s="T729">ptcl</ta>
            <ta e="T731" id="Seg_9037" s="T730">n</ta>
            <ta e="T732" id="Seg_9038" s="T731">pers</ta>
            <ta e="T733" id="Seg_9039" s="T732">n</ta>
            <ta e="T734" id="Seg_9040" s="T733">n</ta>
            <ta e="T735" id="Seg_9041" s="T734">cop</ta>
            <ta e="T736" id="Seg_9042" s="T735">interj</ta>
            <ta e="T737" id="Seg_9043" s="T736">n</ta>
            <ta e="T738" id="Seg_9044" s="T737">cop</ta>
            <ta e="T740" id="Seg_9045" s="T739">n</ta>
            <ta e="T741" id="Seg_9046" s="T740">v</ta>
            <ta e="T742" id="Seg_9047" s="T741">dempro</ta>
            <ta e="T743" id="Seg_9048" s="T742">ptcl</ta>
            <ta e="T744" id="Seg_9049" s="T743">n</ta>
            <ta e="T745" id="Seg_9050" s="T744">n</ta>
            <ta e="T746" id="Seg_9051" s="T745">v</ta>
            <ta e="T747" id="Seg_9052" s="T746">v</ta>
            <ta e="T748" id="Seg_9053" s="T747">v</ta>
            <ta e="T750" id="Seg_9054" s="T749">pers</ta>
            <ta e="T751" id="Seg_9055" s="T750">v</ta>
            <ta e="T752" id="Seg_9056" s="T751">pers</ta>
            <ta e="T753" id="Seg_9057" s="T752">dempro</ta>
            <ta e="T754" id="Seg_9058" s="T753">ptcl</ta>
            <ta e="T755" id="Seg_9059" s="T754">v</ta>
            <ta e="T756" id="Seg_9060" s="T755">ptcl</ta>
            <ta e="T757" id="Seg_9061" s="T756">v</ta>
            <ta e="T759" id="Seg_9062" s="T758">n</ta>
            <ta e="T760" id="Seg_9063" s="T759">n</ta>
            <ta e="T761" id="Seg_9064" s="T760">adv</ta>
            <ta e="T762" id="Seg_9065" s="T761">adj</ta>
            <ta e="T763" id="Seg_9066" s="T762">cop</ta>
            <ta e="T764" id="Seg_9067" s="T763">n</ta>
            <ta e="T765" id="Seg_9068" s="T764">n</ta>
            <ta e="T766" id="Seg_9069" s="T765">n</ta>
            <ta e="T767" id="Seg_9070" s="T766">v</ta>
            <ta e="T768" id="Seg_9071" s="T767">adv</ta>
            <ta e="T769" id="Seg_9072" s="T768">v</ta>
            <ta e="T770" id="Seg_9073" s="T769">v</ta>
            <ta e="T771" id="Seg_9074" s="T770">dempro</ta>
            <ta e="T772" id="Seg_9075" s="T771">n</ta>
            <ta e="T773" id="Seg_9076" s="T772">v</ta>
            <ta e="T775" id="Seg_9077" s="T774">ptcl</ta>
            <ta e="T776" id="Seg_9078" s="T775">dempro</ta>
            <ta e="T777" id="Seg_9079" s="T776">n</ta>
            <ta e="T778" id="Seg_9080" s="T777">v</ta>
            <ta e="T779" id="Seg_9081" s="T778">aux</ta>
            <ta e="T780" id="Seg_9082" s="T779">ptcl</ta>
            <ta e="T781" id="Seg_9083" s="T780">adj</ta>
            <ta e="T782" id="Seg_9084" s="T781">dempro</ta>
            <ta e="T783" id="Seg_9085" s="T782">v</ta>
            <ta e="T784" id="Seg_9086" s="T783">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T13" id="Seg_9087" s="T12">RUS:cult</ta>
            <ta e="T70" id="Seg_9088" s="T69">RUS:cult</ta>
            <ta e="T120" id="Seg_9089" s="T119">RUS:cult</ta>
            <ta e="T133" id="Seg_9090" s="T132">RUS:cult</ta>
            <ta e="T144" id="Seg_9091" s="T143">RUS:cult</ta>
            <ta e="T156" id="Seg_9092" s="T155">RUS:cult</ta>
            <ta e="T163" id="Seg_9093" s="T162">RUS:cult</ta>
            <ta e="T169" id="Seg_9094" s="T168">RUS:cult</ta>
            <ta e="T181" id="Seg_9095" s="T180">RUS:cult</ta>
            <ta e="T186" id="Seg_9096" s="T185">RUS:cult</ta>
            <ta e="T194" id="Seg_9097" s="T193">RUS:cult</ta>
            <ta e="T208" id="Seg_9098" s="T207">RUS:cult</ta>
            <ta e="T212" id="Seg_9099" s="T211">RUS:cult</ta>
            <ta e="T213" id="Seg_9100" s="T212">RUS:cult</ta>
            <ta e="T214" id="Seg_9101" s="T213">RUS:cult</ta>
            <ta e="T217" id="Seg_9102" s="T216">RUS:cult</ta>
            <ta e="T227" id="Seg_9103" s="T226">RUS:cult</ta>
            <ta e="T260" id="Seg_9104" s="T259">RUS:cult</ta>
            <ta e="T293" id="Seg_9105" s="T292">RUS:cult</ta>
            <ta e="T302" id="Seg_9106" s="T301">RUS:cult</ta>
            <ta e="T308" id="Seg_9107" s="T307">RUS:mod</ta>
            <ta e="T311" id="Seg_9108" s="T310">RUS:cult</ta>
            <ta e="T313" id="Seg_9109" s="T312">RUS:gram</ta>
            <ta e="T342" id="Seg_9110" s="T341">RUS:cult</ta>
            <ta e="T345" id="Seg_9111" s="T344">RUS:cult</ta>
            <ta e="T357" id="Seg_9112" s="T356">EV:core</ta>
            <ta e="T371" id="Seg_9113" s="T370">RUS:cult</ta>
            <ta e="T377" id="Seg_9114" s="T376">RUS:cult</ta>
            <ta e="T407" id="Seg_9115" s="T406">RUS:core</ta>
            <ta e="T415" id="Seg_9116" s="T414">EV:core</ta>
            <ta e="T443" id="Seg_9117" s="T442">RUS:cult</ta>
            <ta e="T445" id="Seg_9118" s="T444">RUS:cult</ta>
            <ta e="T446" id="Seg_9119" s="T445">RUS:cult</ta>
            <ta e="T548" id="Seg_9120" s="T547">RUS:cult</ta>
            <ta e="T633" id="Seg_9121" s="T632">RUS:cult</ta>
            <ta e="T675" id="Seg_9122" s="T674">RUS:cult</ta>
            <ta e="T683" id="Seg_9123" s="T682">RUS:cult</ta>
            <ta e="T688" id="Seg_9124" s="T687">RUS:cult</ta>
            <ta e="T707" id="Seg_9125" s="T706">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T10" id="Seg_9126" s="T1">then once the men went out hunting, and the men and boys came back.</ta>
            <ta e="T16" id="Seg_9127" s="T11">"Oh in our Syndassko a house has been built!", they said.</ta>
            <ta e="T19" id="Seg_9128" s="T17">"Where can we go hunting?"</ta>
            <ta e="T31" id="Seg_9129" s="T20">Well, when that house was built they apparently were afraid to approach it, it was said that there were Russians.</ta>
            <ta e="T46" id="Seg_9130" s="T32">"Oooh, they turn our place into a place without hunt, what do we do?", those old men said.</ta>
            <ta e="T49" id="Seg_9131" s="T47">"We talk about it", they say.</ta>
            <ta e="T54" id="Seg_9132" s="T50">I hear them talking, my old men.</ta>
            <ta e="T59" id="Seg_9133" s="T55">And then they come.</ta>
            <ta e="T66" id="Seg_9134" s="T60">After they came they saw two people going [there].</ta>
            <ta e="T70" id="Seg_9135" s="T67">The woman was called Darya.</ta>
            <ta e="T85" id="Seg_9136" s="T71">The man was called… what did they say, he had a very odd name, he was called somehow.</ta>
            <ta e="T89" id="Seg_9137" s="T86">I don't remember his name at all.</ta>
            <ta e="T98" id="Seg_9138" s="T90">they came closer, being scared they came closer</ta>
            <ta e="T104" id="Seg_9139" s="T99">"Why did you come to our land?</ta>
            <ta e="T107" id="Seg_9140" s="T105">Why did you find it?</ta>
            <ta e="T115" id="Seg_9141" s="T108">"Why do you destroy our hunting grounds", the old men say.</ta>
            <ta e="T123" id="Seg_9142" s="T116">Well they will make a settlement here, people will be put here.</ta>
            <ta e="T130" id="Seg_9143" s="T124">People will be born here, many people will live here.</ta>
            <ta e="T138" id="Seg_9144" s="T131">Then there is our landing place, (?).</ta>
            <ta e="T144" id="Seg_9145" s="T139">"Why is our moorage so far away?", they said</ta>
            <ta e="T153" id="Seg_9146" s="T145">The fairway is small here, therefore the fairway is there.</ta>
            <ta e="T157" id="Seg_9147" s="T154">Steamers will come there.</ta>
            <ta e="T165" id="Seg_9148" s="T158">Are they surprised and think "what is a steamer"?</ta>
            <ta e="T169" id="Seg_9149" s="T166">Does one pull the steamer?</ta>
            <ta e="T184" id="Seg_9150" s="T170">Well, much later Syndassko came into being then.</ta>
            <ta e="T192" id="Seg_9151" s="T185">They didn't have a school, they were teaching in pole tents, building [them] up themselves.</ta>
            <ta e="T196" id="Seg_9152" s="T193">In Baloks they were selling.</ta>
            <ta e="T208" id="Seg_9153" s="T197">Then there was the house, which had been built, now it has collapsed, the shop.</ta>
            <ta e="T227" id="Seg_9154" s="T224">Yes, Syndassko.</ta>
            <ta e="T238" id="Seg_9155" s="T228">They took away the hunting grounds, that was later.</ta>
            <ta e="T250" id="Seg_9156" s="T239">Then earlier, when they hadn't arrived yet, the Lygiy people were living there, it is said.</ta>
            <ta e="T256" id="Seg_9157" s="T251">Eh, whatchamacallit, they were called 'the sewn [people]'.</ta>
            <ta e="T270" id="Seg_9158" s="T257">There on the other side of the landing place there are traces of houses, traces of two or three houses.</ta>
            <ta e="T278" id="Seg_9159" s="T271">There lived those, when the hunters were living there the Lygiy people came.</ta>
            <ta e="T281" id="Seg_9160" s="T279">The Lygiy people, the swen [people].</ta>
            <ta e="T287" id="Seg_9161" s="T282">They came, they had three women.</ta>
            <ta e="T293" id="Seg_9162" s="T288">One of them is a shaman, two of them are cooks.</ta>
            <ta e="T306" id="Seg_9163" s="T294">Well, they killed people, killing people when they came, they wanted to make [them] cooks (?), bad people.</ta>
            <ta e="T313" id="Seg_9164" s="T307">As if it were some ruffians.</ta>
            <ta e="T323" id="Seg_9165" s="T314">Well, when coming, they came hier, one of the hunters is agile.</ta>
            <ta e="T332" id="Seg_9166" s="T324">One of them has good eyesight, one of them is a shooter, our hunters, they say.</ta>
            <ta e="T349" id="Seg_9167" s="T333">They defeated all and the old men came to a place, called Maliy, there was a man named Porotov.</ta>
            <ta e="T354" id="Seg_9168" s="T350">The people went to him.</ta>
            <ta e="T366" id="Seg_9169" s="T355">"Well, friend, such dogs with martial snouts are chopping (?), murderers are coming, the sewn [people]."</ta>
            <ta e="T372" id="Seg_9170" s="T367">Well, there they escape to Maliy, they escaped.</ta>
            <ta e="T385" id="Seg_9171" s="T373">They came to Porotov, coming there they tell that such people have come and defeated the people.</ta>
            <ta e="T393" id="Seg_9172" s="T385">Coming from a broad lake (?), they came along a new river.</ta>
            <ta e="T400" id="Seg_9173" s="T394">They came to the old man, the people.</ta>
            <ta e="T409" id="Seg_9174" s="T401">"Well seeing man, have a look and go onto the hill!"</ta>
            <ta e="T424" id="Seg_9175" s="T410">One shooter of them brought his friends in, then they cooked blood soup, the old man.</ta>
            <ta e="T427" id="Seg_9176" s="T425">They have a pole tent.</ta>
            <ta e="T436" id="Seg_9177" s="T428">Having eaten the blood soup, he cut whatchamacallit into the blood soup.</ta>
            <ta e="T443" id="Seg_9178" s="T437">"Hyn" they said earlier, now "hyn" is fishing line.</ta>
            <ta e="T453" id="Seg_9179" s="T444">He cut up the fishing line like tobacco and threw it into the blood soup.</ta>
            <ta e="T467" id="Seg_9180" s="T454">"It'll be hungry people, they'll eat blodd soup", thinking, "they'll eat", thinking he cut [it] up.</ta>
            <ta e="T492" id="Seg_9181" s="T482">Only that way we will win", he said, then he said "we will win somehow."</ta>
            <ta e="T504" id="Seg_9182" s="T493">Well, they came, they ate blood soup, "eat blood soup", he said, they ate blood soup.</ta>
            <ta e="T514" id="Seg_9183" s="T505">Well, when they were cooking, those brutes came.</ta>
            <ta e="T523" id="Seg_9184" s="T515">They came and brought the women in, the old man receives [them] and brings [them] in.</ta>
            <ta e="T530" id="Seg_9185" s="T524">"Well, tell some news."</ta>
            <ta e="T537" id="Seg_9186" s="T531">Well, they come to the old man and tell: </ta>
            <ta e="T543" id="Seg_9187" s="T537">"We have brought an old woman, she is our guide, we say.</ta>
            <ta e="T548" id="Seg_9188" s="T544">An old shaman, those are our cooks.</ta>
            <ta e="T563" id="Seg_9189" s="T549">This woman led us to you", they tell, "they (?) defeated people."</ta>
            <ta e="T576" id="Seg_9190" s="T564">They wipe down the blood from their knives, apparently, the cut up people and eat them.</ta>
            <ta e="T589" id="Seg_9191" s="T577">"Well, you are people smeared with what kind of blood, well, eat", the old man says and feeds them.</ta>
            <ta e="T605" id="Seg_9192" s="T590">Well, having fed them, the sky becomes cloudy.</ta>
            <ta e="T609" id="Seg_9193" s="T606">"What's up", he said.</ta>
            <ta e="T618" id="Seg_9194" s="T610">Well, he seated two alert people on two sides.</ta>
            <ta e="T627" id="Seg_9195" s="T619">When he (?) wanted to swallow, they cut up his throat with a glaive.</ta>
            <ta e="T634" id="Seg_9196" s="T628">How they are fighting now, like in the movies.</ta>
            <ta e="T639" id="Seg_9197" s="T635">Well, so they defeated him.</ta>
            <ta e="T643" id="Seg_9198" s="T640">And the others they killed as well.</ta>
            <ta e="T650" id="Seg_9199" s="T644">They were seven people or how many.</ta>
            <ta e="T662" id="Seg_9200" s="T651">When the women stayed back, the women were quarreling, it is said, they took away the knives from each other.</ta>
            <ta e="T672" id="Seg_9201" s="T663">"That's the knife of my husband, that's the knife of my husband", the poor ones say to each other.</ta>
            <ta e="T688" id="Seg_9202" s="T673">"What kind of bitches, kill them", he said, this cruel old man, our Porotov.</ta>
            <ta e="T692" id="Seg_9203" s="T689">They killed the poor ones.</ta>
            <ta e="T701" id="Seg_9204" s="T693">Then they try to kill this old woman, the old shaman.</ta>
            <ta e="T708" id="Seg_9205" s="T702">They sink her in the water, having tied her to an anchor (?).</ta>
            <ta e="T711" id="Seg_9206" s="T709">She doesn't sink at all.</ta>
            <ta e="T720" id="Seg_9207" s="T712">They try everything, they light up a fire and throw her into the fire.</ta>
            <ta e="T725" id="Seg_9208" s="T721">The fire turns black [= goes out].</ta>
            <ta e="T730" id="Seg_9209" s="T726">And then she said: </ta>
            <ta e="T738" id="Seg_9210" s="T730">"My dear people, I am an old man of the sun, I am no devil.</ta>
            <ta e="T748" id="Seg_9211" s="T739">My people was defeated, I came through the world to see humans and animals.</ta>
            <ta e="T757" id="Seg_9212" s="T749">Don't torture me, I won't die anyway", she says.</ta>
            <ta e="T773" id="Seg_9213" s="T758">"A girl that gets her period for the first time, this girl shall step over my neck, they I'll die", she says, "they made her step over my neck (?)."</ta>
            <ta e="T779" id="Seg_9214" s="T774">And the old woman died.</ta>
            <ta e="T784" id="Seg_9215" s="T779">That's all, so it was told, I don't remember.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T10" id="Seg_9216" s="T1">Dann gingen die alten Männer einmal jagen, die alten Männer und die Jungen kamen.</ta>
            <ta e="T16" id="Seg_9217" s="T11">"Hey, bei uns in Syndassko wurde ein Haus gebaut", sagten sie.</ta>
            <ta e="T19" id="Seg_9218" s="T17">"Wo sollen wir jagen?"</ta>
            <ta e="T31" id="Seg_9219" s="T20">Oh, als das Haus gebaut wurde, hatte man Angst dorthin zu kommen, man sagte dort seien Russen.</ta>
            <ta e="T46" id="Seg_9220" s="T32">"Oh, sie machen unseren Ort beutelos, was soll mit uns geschehen", sagen die alten Männer.</ta>
            <ta e="T49" id="Seg_9221" s="T47">"Wir beraten uns", sagen sie.</ta>
            <ta e="T54" id="Seg_9222" s="T50">Ich höre, wie sie sich unterhalten, die alten Männer.</ta>
            <ta e="T59" id="Seg_9223" s="T55">Dann, nun, dann kommen sie.</ta>
            <ta e="T66" id="Seg_9224" s="T60">Sie kamen und sahen, [dass] zwei Menschen [dort] gehen.</ta>
            <ta e="T70" id="Seg_9225" s="T67">Der Name der Frau war Darja.</ta>
            <ta e="T85" id="Seg_9226" s="T71">Der Mann, wie hieß der noch, er hatte einen sehr komischen Namen, er hieß irgendwie.</ta>
            <ta e="T89" id="Seg_9227" s="T86">Ich erinnere seinen Namen überhaupt nicht.</ta>
            <ta e="T98" id="Seg_9228" s="T90">Zu ihm kamen sie näher, sie hatten Angst und kamen näher.</ta>
            <ta e="T104" id="Seg_9229" s="T99">"Warum seid ihr in unser Land gekommen?</ta>
            <ta e="T107" id="Seg_9230" s="T105">Warum habt ihr es für euch gefunden?</ta>
            <ta e="T115" id="Seg_9231" s="T108">"Warum zerstört ihr unsere Jagdgründe", sagen die alten Männer.</ta>
            <ta e="T123" id="Seg_9232" s="T116">Nun, man wird es hier besiedeln, Leute werden hier sein.</ta>
            <ta e="T130" id="Seg_9233" s="T124">Leute werden hier geboren, viele Leute werden hier leben.</ta>
            <ta e="T138" id="Seg_9234" s="T131">Dann gibt es unsere Anlegestelle, (?).</ta>
            <ta e="T144" id="Seg_9235" s="T139">"Warum", sagen sie, "ist unsere Anlegestelle so weit weg?"</ta>
            <ta e="T153" id="Seg_9236" s="T145">Die Fahrrinne ist hier klein, deshalb ist die Fahrrinne dort.</ta>
            <ta e="T157" id="Seg_9237" s="T154">Dorthin werden Dampfer kommen.</ta>
            <ta e="T165" id="Seg_9238" s="T158">Ob sie sich wohl wundern und denken "was ist ein Dampfer"?</ta>
            <ta e="T169" id="Seg_9239" s="T166">Zieht man den Dampfer?</ta>
            <ta e="T184" id="Seg_9240" s="T170">Nun, viel später dann entstand Syndassko.</ta>
            <ta e="T192" id="Seg_9241" s="T185">Man hatte keine Schule, sie unterrichten im Stangenzelt, sie bauten sie selbst.</ta>
            <ta e="T196" id="Seg_9242" s="T193">In Baloks wurde verkauft.</ta>
            <ta e="T208" id="Seg_9243" s="T197">Dann gab es noch das Haus, das gebaut worden war, jetzt ist es eingestürzt, der Laden.</ta>
            <ta e="T223" id="Seg_9244" s="T209">Deshalb nannte man Syndassko wohl Darija, man nennt es anders.</ta>
            <ta e="T227" id="Seg_9245" s="T224">Ja, Syndassko.</ta>
            <ta e="T238" id="Seg_9246" s="T228">Die Jagdgründe nahmen sie weg, das war alles später.</ta>
            <ta e="T250" id="Seg_9247" s="T239">Dann früher, sagt man, als sie noch nicht gekommen waren, lebten dort die Lygyj-Leute.</ta>
            <ta e="T256" id="Seg_9248" s="T251">Äh, Dings, man nannte sie die Benähten.</ta>
            <ta e="T270" id="Seg_9249" s="T257">Dort auf der anderen Seite der Anlegestelle gibt es Spuren von Häusern, Spuren von zwei, drei Häusen.</ta>
            <ta e="T278" id="Seg_9250" s="T271">Dort lebten sie, als die Jäger dort lebten, kamen die Lygyj-Leute.</ta>
            <ta e="T281" id="Seg_9251" s="T279">Die Lygyj-Leute, die Benähten.</ta>
            <ta e="T287" id="Seg_9252" s="T282">Sie kamen, sie hatten drei Frauen.</ta>
            <ta e="T293" id="Seg_9253" s="T288">Eine von ihnen ist Schamanin, zwei von ihnen sind Köchinne.</ta>
            <ta e="T306" id="Seg_9254" s="T294">Nun, sie töteten Menschen, sie töteten Menschen, als sie kamen, sie wollten [sie] zu Köchen machen (?), schlechte Menschen.</ta>
            <ta e="T313" id="Seg_9255" s="T307">Als ob es irgendwelche Raufbolde wären.</ta>
            <ta e="T323" id="Seg_9256" s="T314">Nun, als sie kamen, sie kamen hierher, von den Jägern ist einer flink.</ta>
            <ta e="T332" id="Seg_9257" s="T324">Einer von ihnen kann gut gucken, einer von ihnen ist ein Schützer, unsere Jäger, sagt man.</ta>
            <ta e="T349" id="Seg_9258" s="T333">Sie vernichteten alle und die alten Männer kamen zu einem Ort, der Malyj heißt, dort war ein Mensch mit dem Namen Porotov.</ta>
            <ta e="T354" id="Seg_9259" s="T350">Zu ihm gingen diese Leute.</ta>
            <ta e="T366" id="Seg_9260" s="T355">"Nun, Freund, solche Hunde mit kriegerischen Schnauzen hacken (?), Mörder kommen, die Benähten."</ta>
            <ta e="T372" id="Seg_9261" s="T367">Nun, da fliehen sie nach Malyj, sie flohen.</ta>
            <ta e="T385" id="Seg_9262" s="T373">Sie kamen dort zu Porotov, als sie ankommen, erzählen sie, das solche gekommen sind und die Menschen vernichtet haben.</ta>
            <ta e="T393" id="Seg_9263" s="T385">Von einem breiten See kommend (?), kamen sie einen neuen Fluss entlang.</ta>
            <ta e="T400" id="Seg_9264" s="T394">Sie kamen zu diesem alten Mann, die Leute.</ta>
            <ta e="T409" id="Seg_9265" s="T401">"Gut Sehender, schau nach und geh auf den Hügel hinauf!"</ta>
            <ta e="T424" id="Seg_9266" s="T410">Ein Schütze von ihnen brachte seine Freunde hinein, dann kochten sie Blutsuppe, der alte Mann.</ta>
            <ta e="T427" id="Seg_9267" s="T425">Sie haben ein Stangenzelt.</ta>
            <ta e="T436" id="Seg_9268" s="T428">Nachdem sie Blutsuppe gegessen hatte, schnitt er Dings in die Blutsuppe.</ta>
            <ta e="T443" id="Seg_9269" s="T437">"Hyn" sagte man früher, jetzt ist "hyn" Angelschnur.</ta>
            <ta e="T453" id="Seg_9270" s="T444">Die Angelschnur zerschnitt er Tabak und warf sie in die Blutsuppe.</ta>
            <ta e="T467" id="Seg_9271" s="T454">"Es werden hungrige Menschen sein, sie werden Blutsuppe essen", dachte er, "sie werden essen", dachte er und zerschnitt [sie].</ta>
            <ta e="T481" id="Seg_9272" s="T468">"Wenn er schlucken möchte, dann zerschneidet ihrem Anführer die Kehle mit der Glefe.</ta>
            <ta e="T492" id="Seg_9273" s="T482">Nur so besiegen wir sie", sagte er, dann sagte er "wir werden irgendwie gewinnen."</ta>
            <ta e="T504" id="Seg_9274" s="T493">Nun, sie kamen, sie aßen Blutsuppe, "esst Blutsuppe", sagte er, sie aßen Blutsuppe.</ta>
            <ta e="T514" id="Seg_9275" s="T505">Nun, als sie kochten, kamen diese Unmenschen.</ta>
            <ta e="T523" id="Seg_9276" s="T515">Sie kamen und brachten die Frauen hinein, der alte Mann empfängt [sie] und bringt [sie] hinein.</ta>
            <ta e="T530" id="Seg_9277" s="T524">"Nun, erzählt Neuigkeiten."</ta>
            <ta e="T537" id="Seg_9278" s="T531">Nun, sie kommen zum alten Mann und erzählen: </ta>
            <ta e="T543" id="Seg_9279" s="T537">"Wir haben eine alte Frau mitgebracht, das ist unsere Führerin, sagen wir.</ta>
            <ta e="T548" id="Seg_9280" s="T544">Eine alte Schamanin, das sind unsere Köchinnen.</ta>
            <ta e="T563" id="Seg_9281" s="T549">Diese Frau hat uns zu euch geführt", erzählen sie, "sie (?) haben Menschen vernichtet."</ta>
            <ta e="T576" id="Seg_9282" s="T564">Sie wischen offenbar das Blut von ihren Messer, sie zerschneiden Menschen und essen sie.</ta>
            <ta e="T589" id="Seg_9283" s="T577">"Oh, mit was für Blut beschmierte Leute seid ihr, nun, esst", sagt der alte Mann und verköstigt sie.</ta>
            <ta e="T605" id="Seg_9284" s="T590">Nun, nachdem er sie verköstigt hat, bezieht sich der Himmel mit Wolken.</ta>
            <ta e="T609" id="Seg_9285" s="T606">"Was ist?", sagte er.</ta>
            <ta e="T618" id="Seg_9286" s="T610">Nun, er setzte zwei bewegliche Leute an zwei Seiten.</ta>
            <ta e="T627" id="Seg_9287" s="T619">Als er (?) schlucken wollte, zerschnitten sie seine Kehle mit der Glefe.</ta>
            <ta e="T634" id="Seg_9288" s="T628">Wie sie jetzt kämpfen, wie in den Filmen.</ta>
            <ta e="T639" id="Seg_9289" s="T635">Nun, so besiegten sie ihn.</ta>
            <ta e="T643" id="Seg_9290" s="T640">Und die anderen töteten sie auch.</ta>
            <ta e="T650" id="Seg_9291" s="T644">Sieben Leute waren sie oder wie viele.</ta>
            <ta e="T662" id="Seg_9292" s="T651">Als die Frauen zurückblieben, stritten sich die Frauen, sagt man, und nahmen sich gegenseitig die Messer weg.</ta>
            <ta e="T672" id="Seg_9293" s="T663">"Das ist das Messer meines Mannes, das ist das Messer meines Messer", sagen die Armen zueinander.</ta>
            <ta e="T688" id="Seg_9294" s="T673">"Was für Schlampen, tötet die", sagte er, dieser grausame alte Mann, unser Porotov.</ta>
            <ta e="T692" id="Seg_9295" s="T689">Sie töteten die Armen.</ta>
            <ta e="T701" id="Seg_9296" s="T693">Dann versuchen sie, diese alte Frau zu töten, die Schamanin.</ta>
            <ta e="T708" id="Seg_9297" s="T702">Sie versenken sie im Wasser, nachdem sie sie an einen Anker gebunden haben (?).</ta>
            <ta e="T711" id="Seg_9298" s="T709">Sie geht überhaupt nicht unter.</ta>
            <ta e="T720" id="Seg_9299" s="T712">Sie probieren alles, sie zünden ein Feuer an und werfen sie ins Feuer.</ta>
            <ta e="T725" id="Seg_9300" s="T721">Das Feuer wird schwarz [= geht aus].</ta>
            <ta e="T730" id="Seg_9301" s="T726">Und dann sagte sie: </ta>
            <ta e="T738" id="Seg_9302" s="T730">"Meine Lieben, ich bin eine alte Frau der Sonne, ich bin keine Teufelin.</ta>
            <ta e="T748" id="Seg_9303" s="T739">Mein Volk wurde vernichtet, ich kam durch die Welt um Menschen und Tiere zu sehen.</ta>
            <ta e="T757" id="Seg_9304" s="T749">Quält mich nicht nicht, ich werde sowieso nicht sterben", sagt sie.</ta>
            <ta e="T773" id="Seg_9305" s="T758">"Ein Mädchen, das das erste Mal ihre Regel bekommt, das Mädchen soll über meinen Hals steigen, dann werde ich sterben", sagt sie, "sie brachten es dazu über meinen Hals zu steigen (?)."</ta>
            <ta e="T779" id="Seg_9306" s="T774">Und die alte Frau starb.</ta>
            <ta e="T784" id="Seg_9307" s="T779">Das ist alles, so wurde es erzählt, ich erinnere [es] nicht.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T10" id="Seg_9308" s="T1">однажды поехали на охоту, мужики и парни пришли</ta>
            <ta e="T16" id="Seg_9309" s="T11">"Да, в Сындаско дом поставили" говорят</ta>
            <ta e="T19" id="Seg_9310" s="T17">"Где мы будем охотиться то?"</ta>
            <ta e="T31" id="Seg_9311" s="T20">ну вот, дом поставили, когда дом поставили все испугались приезжать, сказали, что там русские</ta>
            <ta e="T46" id="Seg_9312" s="T32">"да, нашу землю без промысла оставили, как мы будем охотиться то" говорят они старики же, беседуют они</ta>
            <ta e="T49" id="Seg_9313" s="T47">беседуют они</ta>
            <ta e="T54" id="Seg_9314" s="T50">их разговор я слышу, стариков то</ta>
            <ta e="T59" id="Seg_9315" s="T55">потом пришли они</ta>
            <ta e="T66" id="Seg_9316" s="T60">когда они прошли, увидели два человека там ходят</ta>
            <ta e="T70" id="Seg_9317" s="T67">жену зовут Дарья</ta>
            <ta e="T85" id="Seg_9318" s="T71">а мужа, как же его звали то, не выговариваемое такое имя ,странно, как-то его звали там.</ta>
            <ta e="T89" id="Seg_9319" s="T86">совсем не помню его имя</ta>
            <ta e="T98" id="Seg_9320" s="T90">к нему они пордошли пугаясь.</ta>
            <ta e="T104" id="Seg_9321" s="T99">почему вы пришли на нашу землю?</ta>
            <ta e="T107" id="Seg_9322" s="T105">Зачем вы их нашли?</ta>
            <ta e="T115" id="Seg_9323" s="T108">"Наши охотничьи места ломаете" старики говорят</ta>
            <ta e="T123" id="Seg_9324" s="T116">ну вот здесь, население сделают, людей поставят, люди заселятся</ta>
            <ta e="T130" id="Seg_9325" s="T124">люди здесь будут жить, много людей будет жить</ta>
            <ta e="T138" id="Seg_9326" s="T131">поэтому причал, вот он шумя зашёл</ta>
            <ta e="T144" id="Seg_9327" s="T139">"Почему причал то так далеко"?</ta>
            <ta e="T153" id="Seg_9328" s="T145">маленькая здесь местность, поэтому там будет фарватер</ta>
            <ta e="T157" id="Seg_9329" s="T154">туда параходы будут приезжать</ta>
            <ta e="T165" id="Seg_9330" s="T158">удивились они что такое пароход?</ta>
            <ta e="T169" id="Seg_9331" s="T166">пароход тащить надо?</ta>
            <ta e="T184" id="Seg_9332" s="T170">потом потом, так возникла Сындасско</ta>
            <ta e="T192" id="Seg_9333" s="T185">школы не было, в чумах учили, сами ставили</ta>
            <ta e="T196" id="Seg_9334" s="T193">продавали в балках</ta>
            <ta e="T208" id="Seg_9335" s="T197">тогдашний дом, тот дом, который поставили, он ещё был, сейчас развалился, лавка там была</ta>
            <ta e="T223" id="Seg_9336" s="T209">поэтому Сындасско Дарья говорили или подругому называли её</ta>
            <ta e="T227" id="Seg_9337" s="T224">да, это Сындасско</ta>
            <ta e="T238" id="Seg_9338" s="T228">потом они отобрали всё потом потом это вот случилось то</ta>
            <ta e="T250" id="Seg_9339" s="T239">потом ещё до этого Лыгыйи жили тут</ta>
            <ta e="T256" id="Seg_9340" s="T251">зашитые их называли</ta>
            <ta e="T270" id="Seg_9341" s="T257">там за причалом останки дома есть, два или три дома</ta>
            <ta e="T278" id="Seg_9342" s="T271">там жили, когда там жили охотники, пришли Лыгыйи</ta>
            <ta e="T281" id="Seg_9343" s="T279">Лыгейдар, которые зашитые</ta>
            <ta e="T287" id="Seg_9344" s="T282">вот они пришли с тремя жёнами</ta>
            <ta e="T293" id="Seg_9345" s="T288">одна из них шаманка, вторая повар</ta>
            <ta e="T306" id="Seg_9346" s="T294">вот они людей убивали, убивая людей они сюда пришли, их поварами хотели сделать, плохие люди</ta>
            <ta e="T313" id="Seg_9347" s="T307">как буд-то наверное хулиганы</ta>
            <ta e="T323" id="Seg_9348" s="T314">вот они шли шли и пришли сюда к охотникам, один их них быстрый</ta>
            <ta e="T332" id="Seg_9349" s="T324">один из них хорошо видел, другой хорошо стрелял, охотники то наши говорят</ta>
            <ta e="T349" id="Seg_9350" s="T333">они всех прикончили и старики пришли к месту, к Малой земле, там был человек, Поротов его зовут</ta>
            <ta e="T354" id="Seg_9351" s="T350">к нему пошли эти люди</ta>
            <ta e="T366" id="Seg_9352" s="T355">но вот, слушай друг, войны какие-то идут, зашитые убийцы едут, зашитые</ta>
            <ta e="T372" id="Seg_9353" s="T367">вот они все побежали на Малую</ta>
            <ta e="T385" id="Seg_9354" s="T373">Но таким образом они добрались до Поротово, рассказывают, что такие приехали.</ta>
            <ta e="T393" id="Seg_9355" s="T385">много мест (где люди живут) они проехали, дошли до широкого озера дошли</ta>
            <ta e="T400" id="Seg_9356" s="T394">вот они дошли до этого старика, эти люди</ta>
            <ta e="T409" id="Seg_9357" s="T401">"Смотрящий, сиди смотри взойдя на сопку!"</ta>
            <ta e="T424" id="Seg_9358" s="T410">один стрелок завёл своих друзей и сварили наваристый бутугас (из гуся мясной суп)</ta>
            <ta e="T427" id="Seg_9359" s="T425">чумы у них были</ta>
            <ta e="T436" id="Seg_9360" s="T428">после того, как они ели бутугас, туда нарезали это</ta>
            <ta e="T443" id="Seg_9361" s="T437">хин, нитка называлась, сейчас леска называется</ta>
            <ta e="T453" id="Seg_9362" s="T444">он порезал леску, как табак и положил в суп</ta>
            <ta e="T467" id="Seg_9363" s="T454">голодные они, должны были съесть бутугас, думал он ,поэтому он резал</ta>
            <ta e="T481" id="Seg_9364" s="T468">когда он глотнёт, начальнику горло ему разрежте ножом, кинжалом</ta>
            <ta e="T492" id="Seg_9365" s="T482">тогда только мы сможем их победить</ta>
            <ta e="T504" id="Seg_9366" s="T493">вот они пришли, поели бутугас, едят они бутугас и всё время бутугас едят</ta>
            <ta e="T514" id="Seg_9367" s="T505">когда они кипятили, пришли эти нелюди</ta>
            <ta e="T523" id="Seg_9368" s="T515">когда они приехали, женщин туда завели, старик встречал всех</ta>
            <ta e="T530" id="Seg_9369" s="T524">"Ну давайте, рассказывайте всякие новости".</ta>
            <ta e="T537" id="Seg_9370" s="T531">и они этому старику рассказывают: </ta>
            <ta e="T543" id="Seg_9371" s="T537">"Вот старуху мы привезли с собой, она проводница наша".</ta>
            <ta e="T548" id="Seg_9372" s="T544">бабушка шаманка, эти повара</ta>
            <ta e="T563" id="Seg_9373" s="T549">"но это бабушка шаманка привела нас сюда, много мест прошли мы", вот рассказывают они,"до вас она довела, много мест прошли".</ta>
            <ta e="T576" id="Seg_9374" s="T564">кровь разная, от ножей тут вытирали, оказывается резая резая людей кушали</ta>
            <ta e="T589" id="Seg_9375" s="T577">тьфу, почему все в крови замазаны, кушайте говорит старик, кормит их</ta>
            <ta e="T605" id="Seg_9376" s="T590">после того, как он их накормил потом небо как-то там ,облака собираются</ta>
            <ta e="T609" id="Seg_9377" s="T606">что случилось-то сказал</ta>
            <ta e="T618" id="Seg_9378" s="T610">сказал, потом двоих подвижных посадил по обеим сторонам</ta>
            <ta e="T627" id="Seg_9379" s="T619">когда он проглотил, ударили его по шее кинжалом</ta>
            <ta e="T634" id="Seg_9380" s="T628">как дерутся, будто в кино</ta>
            <ta e="T639" id="Seg_9381" s="T635">таким образом они его побороли</ta>
            <ta e="T643" id="Seg_9382" s="T640">других тоже убили так</ta>
            <ta e="T650" id="Seg_9383" s="T644">семь их там было или сколько</ta>
            <ta e="T662" id="Seg_9384" s="T651">когда женщины остались там, женщины начали ругаться, ножи отбирать</ta>
            <ta e="T672" id="Seg_9385" s="T663">"нож моего мужа, нож моего мужа" говорили бедные</ta>
            <ta e="T688" id="Seg_9386" s="T673">"что это за суки, женщин убить надо" говорит он, какой же был всё таки жестокий старик наш Поротай</ta>
            <ta e="T692" id="Seg_9387" s="T689">и убили их бедных</ta>
            <ta e="T701" id="Seg_9388" s="T693">потом эту старуху шаманку начали убивать</ta>
            <ta e="T708" id="Seg_9389" s="T702">в воду её топили, в воде вместе с якорем</ta>
            <ta e="T711" id="Seg_9390" s="T709">никак не тонет</ta>
            <ta e="T720" id="Seg_9391" s="T712">пришлось даже печку топить и в печку её бросать</ta>
            <ta e="T725" id="Seg_9392" s="T721">а огонь сразу потух, становился чёрным</ta>
            <ta e="T730" id="Seg_9393" s="T726">и потом она сказала- </ta>
            <ta e="T738" id="Seg_9394" s="T730">"я бабушка солнце была, я не баба яга/ведьма (абаахи) вам</ta>
            <ta e="T748" id="Seg_9395" s="T739">мой народ уничтожили, поэтому я на людей, на животных и на мир хотела посмотреть</ta>
            <ta e="T757" id="Seg_9396" s="T749">не мучайте меня, я всё равно не умру</ta>
            <ta e="T773" id="Seg_9397" s="T758">"девочку, у которой только что прошли месячные, девочку, пусть через шею мою перейдёт, тогда только умру", говорит, поэтому через шею перешла</ta>
            <ta e="T779" id="Seg_9398" s="T774">и эта старушка умерла. </ta>
            <ta e="T784" id="Seg_9399" s="T779">И всё, так рассказывали, не помню </ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T250" id="Seg_9400" s="T239">[DCh]: It is not clear who the Lygiy people shall be.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
