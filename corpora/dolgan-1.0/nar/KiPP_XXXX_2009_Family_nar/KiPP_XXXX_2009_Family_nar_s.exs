<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDCFCC3034-3A84-A104-7F5E-67D26476BEC9">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>KiPP_XXXX_2009_Family_nar</transcription-name>
         <referenced-file url="KiPP_XXXX_2009_Family_nar.wav" />
         <referenced-file url="KiPP_XXXX_2009_Family_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\KiPP_XXXX_2009_Family_nar\KiPP_XXXX_2009_Family_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">997</ud-information>
            <ud-information attribute-name="# HIAT:w">630</ud-information>
            <ud-information attribute-name="# e">640</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">10</ud-information>
            <ud-information attribute-name="# HIAT:u">129</ud-information>
            <ud-information attribute-name="# sc">76</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KiPP">
            <abbreviation>KiPP</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="XXXX">
            <abbreviation>XXXX</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="SE">
            <abbreviation>SE</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="5.942" type="appl" />
         <tli id="T2" time="6.556" type="appl" />
         <tli id="T3" time="7.17" type="appl" />
         <tli id="T4" time="7.784" type="appl" />
         <tli id="T5" time="7.846578432474981" />
         <tli id="T6" time="8.693235578544924" />
         <tli id="T7" time="8.947666666666667" type="appl" />
         <tli id="T8" time="9.380333333333335" type="appl" />
         <tli id="T9" time="9.501334401689926" />
         <tli id="T10" time="9.797" type="appl" />
         <tli id="T11" time="9.813" type="appl" />
         <tli id="T12" time="10.0846709410722" />
         <tli id="T13" time="10.613213988530305" />
         <tli id="T14" time="12.16652985494209" />
         <tli id="T15" time="12.232264927471045" type="intp" />
         <tli id="T16" time="12.298" type="appl" />
         <tli id="T17" time="12.773" type="appl" />
         <tli id="T18" time="13.778" type="appl" />
         <tli id="T19" time="14.783000000000001" type="appl" />
         <tli id="T20" time="15.788" type="appl" />
         <tli id="T21" time="16.34925" type="appl" />
         <tli id="T22" time="16.9105" type="appl" />
         <tli id="T23" time="17.467" type="appl" />
         <tli id="T24" time="17.47175" type="appl" />
         <tli id="T25" time="18.033" type="appl" />
         <tli id="T26" time="18.229669683197546" />
         <tli id="T27" time="18.653" type="appl" />
         <tli id="T28" time="19.349999999999998" type="appl" />
         <tli id="T29" time="20.046999999999997" type="appl" />
         <tli id="T30" time="20.744" type="appl" />
         <tli id="T31" time="21.441" type="appl" />
         <tli id="T32" time="22.104" type="appl" />
         <tli id="T33" time="22.111" type="appl" />
         <tli id="T34" time="22.575" type="appl" />
         <tli id="T35" time="23.046" type="appl" />
         <tli id="T36" time="23.517" type="appl" />
         <tli id="T37" time="23.988" type="appl" />
         <tli id="T38" time="24.459" type="appl" />
         <tli id="T39" time="24.93" type="appl" />
         <tli id="T40" time="25.401" type="appl" />
         <tli id="T41" time="25.872" type="appl" />
         <tli id="T42" time="26.343" type="appl" />
         <tli id="T43" time="26.814" type="appl" />
         <tli id="T44" time="27.285" type="appl" />
         <tli id="T45" time="27.52933561695733" />
         <tli id="T46" time="27.63968919374789" />
         <tli id="T47" time="28.37966666666667" type="appl" />
         <tli id="T48" time="28.47301315641516" />
         <tli id="T49" time="29.026340267626225" />
         <tli id="T50" time="29.692999437760033" />
         <tli id="T51" time="29.949666473433282" />
         <tli id="T52" time="30.22833333333333" type="appl" />
         <tli id="T53" time="30.553666666666665" type="appl" />
         <tli id="T54" time="30.878999999999998" type="appl" />
         <tli id="T55" time="31.204333333333334" type="appl" />
         <tli id="T56" time="31.529666666666667" type="appl" />
         <tli id="T57" time="31.855" type="appl" />
         <tli id="T58" time="32.5528" type="appl" />
         <tli id="T59" time="33.2506" type="appl" />
         <tli id="T60" time="33.9484" type="appl" />
         <tli id="T61" time="34.6462" type="appl" />
         <tli id="T62" time="35.344" type="appl" />
         <tli id="T63" time="35.362" type="appl" />
         <tli id="T64" time="35.8314" type="appl" />
         <tli id="T65" time="36.3008" type="appl" />
         <tli id="T66" time="36.397" type="appl" />
         <tli id="T67" time="36.7702" type="appl" />
         <tli id="T68" time="37.207" type="appl" />
         <tli id="T69" time="37.2396" type="appl" />
         <tli id="T70" time="37.639000580359024" />
         <tli id="T71" time="37.964500290179515" type="intp" />
         <tli id="T72" time="38.29" type="appl" />
         <tli id="T73" time="38.881" type="appl" />
         <tli id="T74" time="39.695" type="appl" />
         <tli id="T75" time="40.509" type="appl" />
         <tli id="T76" time="41.323" type="appl" />
         <tli id="T77" time="42.773" type="appl" />
         <tli id="T78" time="44.20075" type="appl" />
         <tli id="T79" time="45.6285" type="appl" />
         <tli id="T80" time="47.056250000000006" type="appl" />
         <tli id="T81" time="48.57278713594962" />
         <tli id="T82" time="48.579453727650964" />
         <tli id="T83" time="48.9965" type="appl" />
         <tli id="T84" time="49.518" type="appl" />
         <tli id="T85" time="50.039500000000004" type="appl" />
         <tli id="T86" time="51.16609130777015" />
         <tli id="T87" time="51.17275789947149" />
         <tli id="T88" time="51.972748903632066" />
         <tli id="T89" time="54.839383335207465" />
         <tli id="T90" time="54.998" type="appl" />
         <tli id="T91" time="56.018" type="appl" />
         <tli id="T92" time="57.038" type="appl" />
         <tli id="T93" time="57.4505" type="appl" />
         <tli id="T94" time="57.863" type="appl" />
         <tli id="T95" time="58.275499999999994" type="appl" />
         <tli id="T96" time="58.687999999999995" type="appl" />
         <tli id="T97" time="59.1005" type="appl" />
         <tli id="T98" time="59.59300175612138" />
         <tli id="T99" time="60.7515" type="appl" />
         <tli id="T100" time="61.989999999999995" type="appl" />
         <tli id="T101" time="63.2285" type="appl" />
         <tli id="T102" time="64.42700338771542" />
         <tli id="T103" time="66.04033811046962" />
         <tli id="T104" time="67.13166666666666" type="appl" />
         <tli id="T105" time="68.05633333333333" type="appl" />
         <tli id="T106" time="69.8458812549196" />
         <tli id="T107" time="70.1475" type="appl" />
         <tli id="T108" time="71.314" type="appl" />
         <tli id="T109" time="72.911" type="appl" />
         <tli id="T110" time="74.508" type="appl" />
         <tli id="T111" time="78.265" type="appl" />
         <tli id="T112" time="78.7975" type="appl" />
         <tli id="T113" time="79.33" type="appl" />
         <tli id="T114" time="79.508" type="appl" />
         <tli id="T115" time="79.8625" type="appl" />
         <tli id="T116" time="79.99433333333333" type="appl" />
         <tli id="T117" time="80.395" type="appl" />
         <tli id="T118" time="80.48066666666666" type="appl" />
         <tli id="T119" time="80.717" type="appl" />
         <tli id="T120" time="81.00699923856017" />
         <tli id="T121" time="81.62" type="appl" />
         <tli id="T122" time="82.523" type="appl" />
         <tli id="T123" time="83.42599999999999" type="appl" />
         <tli id="T124" time="84.329" type="appl" />
         <tli id="T125" time="84.70266666666666" type="appl" />
         <tli id="T126" time="85.07633333333334" type="appl" />
         <tli id="T127" time="85.41666970422663" />
         <tli id="T128" time="86.2456968402114" />
         <tli id="T129" time="86.25236343191273" />
         <tli id="T130" time="86.875" type="appl" />
         <tli id="T131" time="87.583" type="appl" />
         <tli id="T132" time="88.291" type="appl" />
         <tli id="T133" time="88.999" type="appl" />
         <tli id="T134" time="89.70700000000001" type="appl" />
         <tli id="T135" time="90.415" type="appl" />
         <tli id="T136" time="91.123" type="appl" />
         <tli id="T137" time="91.831" type="appl" />
         <tli id="T138" time="92.539" type="appl" />
         <tli id="T139" time="93.57894771168334" />
         <tli id="T140" time="93.9134" type="appl" />
         <tli id="T141" time="94.57979999999999" type="appl" />
         <tli id="T142" time="95.2462" type="appl" />
         <tli id="T143" time="95.9126" type="appl" />
         <tli id="T144" time="97.05224198808051" />
         <tli id="T145" time="97.08635714285714" type="appl" />
         <tli id="T146" time="97.59371428571428" type="appl" />
         <tli id="T147" time="98.10107142857143" type="appl" />
         <tli id="T148" time="98.60842857142856" type="appl" />
         <tli id="T149" time="99.1157857142857" type="appl" />
         <tli id="T150" time="99.62314285714285" type="appl" />
         <tli id="T151" time="100.1305" type="appl" />
         <tli id="T152" time="100.63785714285714" type="appl" />
         <tli id="T153" time="101.14521428571429" type="appl" />
         <tli id="T154" time="101.65257142857143" type="appl" />
         <tli id="T155" time="102.15992857142857" type="appl" />
         <tli id="T156" time="102.66728571428571" type="appl" />
         <tli id="T157" time="103.17464285714286" type="appl" />
         <tli id="T158" time="104.52549128528055" />
         <tli id="T159" time="104.81077777777777" type="appl" />
         <tli id="T160" time="105.93955555555556" type="appl" />
         <tli id="T161" time="107.06833333333333" type="appl" />
         <tli id="T162" time="108.19711111111111" type="appl" />
         <tli id="T163" time="109.32588888888888" type="appl" />
         <tli id="T164" time="110.45466666666667" type="appl" />
         <tli id="T165" time="111.58344444444444" type="appl" />
         <tli id="T166" time="112.71222222222221" type="appl" />
         <tli id="T167" time="114.09205037670077" />
         <tli id="T168" time="114.353" type="appl" />
         <tli id="T169" time="114.865" type="appl" />
         <tli id="T170" time="115.377" type="appl" />
         <tli id="T171" time="115.889" type="appl" />
         <tli id="T172" time="116.401" type="appl" />
         <tli id="T173" time="116.913" type="appl" />
         <tli id="T174" time="117.425" type="appl" />
         <tli id="T175" time="118.22365756291816" />
         <tli id="T176" time="118.48455555555556" type="appl" />
         <tli id="T177" time="119.0321111111111" type="appl" />
         <tli id="T178" time="119.57966666666667" type="appl" />
         <tli id="T179" time="120.12722222222222" type="appl" />
         <tli id="T180" time="120.67477777777778" type="appl" />
         <tli id="T181" time="121.22233333333332" type="appl" />
         <tli id="T182" time="121.76988888888889" type="appl" />
         <tli id="T183" time="122.31744444444443" type="appl" />
         <tli id="T184" time="122.9049981507576" />
         <tli id="T185" time="123.38149999999999" type="appl" />
         <tli id="T186" time="123.898" type="appl" />
         <tli id="T187" time="124.4145" type="appl" />
         <tli id="T188" time="124.931" type="appl" />
         <tli id="T189" time="125.44749999999999" type="appl" />
         <tli id="T190" time="126.2985797818509" />
         <tli id="T191" time="126.4535" type="appl" />
         <tli id="T192" time="126.943" type="appl" />
         <tli id="T193" time="127.4325" type="appl" />
         <tli id="T194" time="127.922" type="appl" />
         <tli id="T195" time="128.4115" type="appl" />
         <tli id="T196" time="128.901" type="appl" />
         <tli id="T197" time="129.3905" type="appl" />
         <tli id="T198" time="129.88" type="appl" />
         <tli id="T199" time="130.36950000000002" type="appl" />
         <tli id="T200" time="130.859" type="appl" />
         <tli id="T201" time="131.77225" type="appl" />
         <tli id="T202" time="132.6855" type="appl" />
         <tli id="T203" time="133.59875" type="appl" />
         <tli id="T204" time="135.5984752052176" />
         <tli id="T205" time="135.81847273136174" />
         <tli id="T206" time="135.9706715407308" />
         <tli id="T207" time="136.64" type="appl" />
         <tli id="T208" time="137.34933333333333" type="appl" />
         <tli id="T209" time="138.05866666666668" type="appl" />
         <tli id="T210" time="139.10510244012147" />
         <tli id="T211" time="140.55349999999999" type="appl" />
         <tli id="T212" time="142.7850610592601" />
         <tli id="T213" time="143.0982" type="appl" />
         <tli id="T214" time="143.85739999999998" type="appl" />
         <tli id="T215" time="144.6166" type="appl" />
         <tli id="T216" time="145.3758" type="appl" />
         <tli id="T217" time="147.21167794894862" />
         <tli id="T218" time="147.65167300123696" />
         <tli id="T219" time="147.8215" type="appl" />
         <tli id="T220" time="148.66475" type="appl" />
         <tli id="T221" time="149.50799999999998" type="appl" />
         <tli id="T222" time="150.35125" type="appl" />
         <tli id="T223" time="151.1945" type="appl" />
         <tli id="T224" time="152.03775" type="appl" />
         <tli id="T225" time="153.0743333779904" />
         <tli id="T226" time="153.83314285714286" type="appl" />
         <tli id="T227" time="154.78528571428572" type="appl" />
         <tli id="T228" time="155.73742857142858" type="appl" />
         <tli id="T229" time="156.6895714285714" type="appl" />
         <tli id="T230" time="157.64171428571427" type="appl" />
         <tli id="T231" time="158.59385714285713" type="appl" />
         <tli id="T232" time="159.546" type="appl" />
         <tli id="T233" time="160.09125" type="appl" />
         <tli id="T234" time="160.63649999999998" type="appl" />
         <tli id="T235" time="161.18175" type="appl" />
         <tli id="T236" time="161.72699999999998" type="appl" />
         <tli id="T237" time="162.27224999999999" type="appl" />
         <tli id="T238" time="162.8175" type="appl" />
         <tli id="T239" time="163.36274999999998" type="appl" />
         <tli id="T240" time="164.91147891600136" />
         <tli id="T241" time="164.92849999999999" type="appl" />
         <tli id="T242" time="165.949" type="appl" />
         <tli id="T243" time="166.9695" type="appl" />
         <tli id="T244" time="168.3899944878521" />
         <tli id="T245" time="168.47566666666668" type="appl" />
         <tli id="T246" time="168.96133333333333" type="appl" />
         <tli id="T247" time="169.447" type="appl" />
         <tli id="T248" time="169.93266666666668" type="appl" />
         <tli id="T249" time="170.41833333333332" type="appl" />
         <tli id="T250" time="170.904" type="appl" />
         <tli id="T251" time="171.36299491084984" />
         <tli id="T252" time="171.5820028647883" />
         <tli id="T253" time="171.97133442428384" />
         <tli id="T254" time="172.01434175316965" />
         <tli id="T255" time="172.36966666666666" type="appl" />
         <tli id="T256" time="172.82133333333334" type="appl" />
         <tli id="T257" time="173.57804812774094" />
         <tli id="T258" time="173.5796626929186" />
         <tli id="T259" time="174.10485714285716" type="appl" />
         <tli id="T260" time="174.59671428571428" type="appl" />
         <tli id="T261" time="175.08857142857144" type="appl" />
         <tli id="T262" time="175.58042857142857" type="appl" />
         <tli id="T263" time="176.07228571428573" type="appl" />
         <tli id="T264" time="176.56414285714285" type="appl" />
         <tli id="T265" time="176.9560049345342" />
         <tli id="T266" time="177.68733333333333" type="appl" />
         <tli id="T267" time="178.31866666666667" type="appl" />
         <tli id="T268" time="179.69797930956932" />
         <tli id="T269" time="179.72971428571427" type="appl" />
         <tli id="T270" time="180.50942857142857" type="appl" />
         <tli id="T271" time="181.28914285714285" type="appl" />
         <tli id="T272" time="182.06885714285713" type="appl" />
         <tli id="T273" time="182.8485714285714" type="appl" />
         <tli id="T274" time="183.6282857142857" type="appl" />
         <tli id="T275" time="184.56800267151834" />
         <tli id="T276" time="185.40833333333333" type="appl" />
         <tli id="T277" time="186.40866666666665" type="appl" />
         <tli id="T278" time="187.64233008010163" />
         <tli id="T279" time="188.132" type="appl" />
         <tli id="T280" time="188.855" type="appl" />
         <tli id="T281" time="189.86453165410998" />
         <tli id="T282" time="190.1826" type="appl" />
         <tli id="T283" time="190.7872" type="appl" />
         <tli id="T284" time="191.3918" type="appl" />
         <tli id="T285" time="191.9964" type="appl" />
         <tli id="T286" time="192.7476632960088" />
         <tli id="T287" time="193.25" type="appl" />
         <tli id="T288" time="193.899" type="appl" />
         <tli id="T289" time="194.548" type="appl" />
         <tli id="T290" time="195.197" type="appl" />
         <tli id="T291" time="196.01933223550122" />
         <tli id="T292" time="196.427" type="appl" />
         <tli id="T293" time="197.008" type="appl" />
         <tli id="T294" time="197.589" type="appl" />
         <tli id="T295" time="198.17000000000002" type="appl" />
         <tli id="T296" time="198.751" type="appl" />
         <tli id="T297" time="199.33200000000002" type="appl" />
         <tli id="T298" time="200.571077926459" />
         <tli id="T299" time="200.822" type="appl" />
         <tli id="T300" time="201.731" type="appl" />
         <tli id="T301" time="202.64" type="appl" />
         <tli id="T302" time="203.549" type="appl" />
         <tli id="T303" time="204.458" type="appl" />
         <tli id="T304" time="205.87101832902283" />
         <tli id="T305" time="205.9322" type="appl" />
         <tli id="T672" time="205.98319999999998" type="intp" />
         <tli id="T306" time="206.4974" type="appl" />
         <tli id="T307" time="207.0626" type="appl" />
         <tli id="T308" time="207.6278" type="appl" />
         <tli id="T309" time="208.51765523445408" />
         <tli id="T310" time="208.7177142857143" type="appl" />
         <tli id="T311" time="209.24242857142858" type="appl" />
         <tli id="T312" time="209.76714285714286" type="appl" />
         <tli id="T313" time="210.29185714285717" type="appl" />
         <tli id="T314" time="210.81657142857145" type="appl" />
         <tli id="T315" time="211.34128571428573" type="appl" />
         <tli id="T316" time="212.13094793657936" />
         <tli id="T317" time="212.6000909090909" type="appl" />
         <tli id="T318" time="213.33418181818183" type="appl" />
         <tli id="T319" time="214.06827272727273" type="appl" />
         <tli id="T320" time="214.80236363636365" type="appl" />
         <tli id="T321" time="215.53645454545455" type="appl" />
         <tli id="T322" time="216.27054545454547" type="appl" />
         <tli id="T323" time="217.00463636363637" type="appl" />
         <tli id="T324" time="217.7387272727273" type="appl" />
         <tli id="T325" time="218.47281818181818" type="appl" />
         <tli id="T326" time="219.0108705723603" />
         <tli id="T327" time="221.6841738445969" />
         <tli id="T328" time="221.83664922973128" type="intp" />
         <tli id="T329" time="221.98912461486566" type="intp" />
         <tli id="T330" time="222.1416" type="appl" />
         <tli id="T331" time="222.87513333333334" type="appl" />
         <tli id="T332" time="223.60866666666666" type="appl" />
         <tli id="T333" time="224.3422" type="appl" />
         <tli id="T334" time="225.07573333333332" type="appl" />
         <tli id="T335" time="225.80926666666667" type="appl" />
         <tli id="T336" time="226.5428" type="appl" />
         <tli id="T337" time="227.27633333333333" type="appl" />
         <tli id="T338" time="228.00986666666665" type="appl" />
         <tli id="T339" time="228.74339999999998" type="appl" />
         <tli id="T340" time="229.47693333333333" type="appl" />
         <tli id="T341" time="230.21046666666666" type="appl" />
         <tli id="T342" time="231.60406229618803" />
         <tli id="T343" time="231.87053114809402" type="intp" />
         <tli id="T344" time="232.137" type="appl" />
         <tli id="T345" time="232.7335" type="appl" />
         <tli id="T346" time="233.32999999999998" type="appl" />
         <tli id="T347" time="233.9265" type="appl" />
         <tli id="T348" time="234.523" type="appl" />
         <tli id="T349" time="235.1195" type="appl" />
         <tli id="T350" time="235.716" type="appl" />
         <tli id="T351" time="236.3125" type="appl" />
         <tli id="T352" time="236.909" type="appl" />
         <tli id="T353" time="237.5055" type="appl" />
         <tli id="T354" time="238.85064747554253" />
         <tli id="T355" time="239.08793912238667" type="intp" />
         <tli id="T356" time="239.32523076923079" type="appl" />
         <tli id="T357" time="239.93684615384615" type="appl" />
         <tli id="T358" time="240.54846153846154" type="appl" />
         <tli id="T359" time="241.16007692307693" type="appl" />
         <tli id="T360" time="241.77169230769232" type="appl" />
         <tli id="T361" time="242.38330769230768" type="appl" />
         <tli id="T362" time="242.99492307692307" type="appl" />
         <tli id="T363" time="243.60653846153846" type="appl" />
         <tli id="T364" time="244.21815384615385" type="appl" />
         <tli id="T365" time="244.82976923076922" type="appl" />
         <tli id="T366" time="245.4413846153846" type="appl" />
         <tli id="T367" time="246.390562689756" />
         <tli id="T368" time="247.0965" type="appl" />
         <tli id="T369" time="248.6305375014056" />
         <tli id="T370" time="248.834" type="appl" />
         <tli id="T371" time="249.528" type="appl" />
         <tli id="T372" time="250.22199999999998" type="appl" />
         <tli id="T373" time="250.916" type="appl" />
         <tli id="T374" time="251.61" type="appl" />
         <tli id="T375" time="252.87048982345664" />
         <tli id="T376" time="253.80200000000002" type="appl" />
         <tli id="T377" time="255.4733251381002" />
         <tli id="T378" time="255.95600000000002" type="appl" />
         <tli id="T379" time="256.612" type="appl" />
         <tli id="T380" time="257.26800000000003" type="appl" />
         <tli id="T381" time="257.92400000000004" type="appl" />
         <tli id="T382" time="258.58" type="appl" />
         <tli id="T383" time="259.236" type="appl" />
         <tli id="T384" time="259.892" type="appl" />
         <tli id="T385" time="260.548" type="appl" />
         <tli id="T386" time="262.14371888001796" />
         <tli id="T387" time="262.31600000000003" type="appl" />
         <tli id="T388" time="263.0170423928933" />
         <tli id="T389" time="263.3903715281682" />
         <tli id="T390" time="268.2636500618464" />
         <tli id="T391" time="268.4224375463848" type="intp" />
         <tli id="T392" time="268.5812250309232" type="intp" />
         <tli id="T393" time="268.74001251546156" type="intp" />
         <tli id="T394" time="268.8988" type="appl" />
         <tli id="T395" time="269.7105" type="appl" />
         <tli id="T396" time="270.5222" type="appl" />
         <tli id="T397" time="271.33389999999997" type="appl" />
         <tli id="T398" time="272.1456" type="appl" />
         <tli id="T399" time="272.95730000000003" type="appl" />
         <tli id="T400" time="274.0223353058585" />
         <tli id="T401" time="274.54133333333334" type="appl" />
         <tli id="T402" time="275.3136666666667" type="appl" />
         <tli id="T403" time="276.086" type="appl" />
         <tli id="T404" time="276.85833333333335" type="appl" />
         <tli id="T405" time="277.6306666666667" type="appl" />
         <tli id="T406" time="278.403" type="appl" />
         <tli id="T407" time="278.41" type="appl" />
         <tli id="T408" time="279.44133686958844" />
         <tli id="T409" time="279.612" type="appl" />
         <tli id="T410" time="280.1066" type="appl" />
         <tli id="T411" time="280.6012" type="appl" />
         <tli id="T412" time="281.0958" type="appl" />
         <tli id="T413" time="281.5904" type="appl" />
         <tli id="T414" time="282.5983326194057" />
         <tli id="T415" time="283.3755" type="appl" />
         <tli id="T416" time="284.9434624985944" />
         <tli id="T417" time="285.30235714285715" type="appl" />
         <tli id="T418" time="285.9387142857143" type="appl" />
         <tli id="T419" time="286.57507142857145" type="appl" />
         <tli id="T420" time="287.21142857142854" type="appl" />
         <tli id="T421" time="287.8477857142857" type="appl" />
         <tli id="T422" time="288.48414285714284" type="appl" />
         <tli id="T423" time="289.1205" type="appl" />
         <tli id="T424" time="289.75685714285714" type="appl" />
         <tli id="T425" time="290.3932142857143" type="appl" />
         <tli id="T426" time="291.02957142857144" type="appl" />
         <tli id="T427" time="291.66592857142854" type="appl" />
         <tli id="T428" time="292.3022857142857" type="appl" />
         <tli id="T429" time="292.93864285714284" type="appl" />
         <tli id="T430" time="293.75669672776337" />
         <tli id="T431" time="294.70433333333335" type="appl" />
         <tli id="T432" time="295.83366666666666" type="appl" />
         <tli id="T433" time="297.3033235128753" />
         <tli id="T434" time="297.97833333333335" type="appl" />
         <tli id="T435" time="298.9936666666667" type="appl" />
         <tli id="T436" time="300.3166229618801" />
         <tli id="T437" time="300.67107142857145" type="appl" />
         <tli id="T438" time="301.3331428571429" type="appl" />
         <tli id="T439" time="301.9952142857143" type="appl" />
         <tli id="T440" time="302.6572857142857" type="appl" />
         <tli id="T441" time="303.31935714285714" type="appl" />
         <tli id="T442" time="303.9814285714286" type="appl" />
         <tli id="T443" time="304.6435" type="appl" />
         <tli id="T444" time="305.30557142857145" type="appl" />
         <tli id="T445" time="305.9676428571429" type="appl" />
         <tli id="T446" time="306.62971428571433" type="appl" />
         <tli id="T447" time="307.2917857142857" type="appl" />
         <tli id="T448" time="307.95385714285715" type="appl" />
         <tli id="T449" time="308.6159285714286" type="appl" />
         <tli id="T450" time="309.4846709095286" />
         <tli id="T451" time="310.0215" type="appl" />
         <tli id="T452" time="310.765" type="appl" />
         <tli id="T453" time="311.5085" type="appl" />
         <tli id="T454" time="312.676483976161" />
         <tli id="T455" time="312.8608888888889" type="appl" />
         <tli id="T456" time="313.4697777777778" type="appl" />
         <tli id="T457" time="314.07866666666666" type="appl" />
         <tli id="T458" time="314.68755555555555" type="appl" />
         <tli id="T459" time="315.2964444444445" type="appl" />
         <tli id="T460" time="315.9053333333334" type="appl" />
         <tli id="T461" time="316.51422222222226" type="appl" />
         <tli id="T462" time="317.12311111111114" type="appl" />
         <tli id="T463" time="318.2964207803891" />
         <tli id="T464" time="318.5946666666667" type="appl" />
         <tli id="T465" time="319.45733333333334" type="appl" />
         <tli id="T466" time="320.314" type="appl" />
         <tli id="T467" time="320.9297245024176" />
         <tli id="T468" time="321.4955" type="appl" />
         <tli id="T469" time="322.74634469965844" />
         <tli id="T470" time="323.76969256718763" />
         <tli id="T471" time="324.49635106263355" />
         <tli id="T472" time="324.52925" type="appl" />
         <tli id="T473" time="325.1665" type="appl" />
         <tli id="T474" time="325.80375" type="appl" />
         <tli id="T475" time="326.44100000000003" type="appl" />
         <tli id="T476" time="327.07825" type="appl" />
         <tli id="T477" time="327.7155" type="appl" />
         <tli id="T478" time="328.35275" type="appl" />
         <tli id="T479" time="329.39666054235073" />
         <tli id="T480" time="330.176" type="appl" />
         <tli id="T481" time="331.362" type="appl" />
         <tli id="T482" time="333.1829200494771" />
         <tli id="T483" time="333.22675" type="appl" />
         <tli id="T484" time="333.9055" type="appl" />
         <tli id="T485" time="334.58425" type="appl" />
         <tli id="T486" time="335.26300000000003" type="appl" />
         <tli id="T487" time="335.94175" type="appl" />
         <tli id="T488" time="336.6205" type="appl" />
         <tli id="T489" time="337.29925000000003" type="appl" />
         <tli id="T490" time="338.38286157652084" />
         <tli id="T491" time="338.5703334267472" />
         <tli id="T492" time="339.489515798943" />
         <tli id="T493" time="339.491" type="appl" />
         <tli id="T494" time="339.96167194791553" />
         <tli id="T495" time="340.193336009537" />
         <tli id="T496" time="340.346" type="appl" />
         <tli id="T497" time="340.8726" type="appl" />
         <tli id="T498" time="341.3992" type="appl" />
         <tli id="T499" time="341.9258" type="appl" />
         <tli id="T500" time="342.4524" type="appl" />
         <tli id="T501" time="342.7256773252136" />
         <tli id="T502" time="342.72966165541015" />
         <tli id="T503" time="343.64625" type="appl" />
         <tli id="T504" time="344.3095" type="appl" />
         <tli id="T505" time="344.97275" type="appl" />
         <tli id="T506" time="345.629" type="appl" />
         <tli id="T507" time="345.636" type="appl" />
         <tli id="T508" time="346.22683333333333" type="appl" />
         <tli id="T509" time="346.8246666666667" type="appl" />
         <tli id="T510" time="347.4225" type="appl" />
         <tli id="T511" time="348.0203333333333" type="appl" />
         <tli id="T512" time="348.6181666666667" type="appl" />
         <tli id="T513" time="349.216" type="appl" />
         <tli id="T514" time="349.8138333333333" type="appl" />
         <tli id="T515" time="350.4116666666667" type="appl" />
         <tli id="T516" time="351.0095" type="appl" />
         <tli id="T517" time="351.6073333333333" type="appl" />
         <tli id="T518" time="352.2051666666667" type="appl" />
         <tli id="T519" time="353.1960283368942" />
         <tli id="T520" time="357.526" type="appl" />
         <tli id="T521" time="358.1267" type="appl" />
         <tli id="T522" time="358.7274" type="appl" />
         <tli id="T523" time="359.3281" type="appl" />
         <tli id="T524" time="359.9288" type="appl" />
         <tli id="T525" time="360.5295" type="appl" />
         <tli id="T526" time="361.1302" type="appl" />
         <tli id="T527" time="361.7309" type="appl" />
         <tli id="T528" time="362.33160000000004" type="appl" />
         <tli id="T529" time="362.9323" type="appl" />
         <tli id="T530" time="363.686327040931" />
         <tli id="T531" time="365.5825557179804" />
         <tli id="T532" time="365.8401" type="appl" />
         <tli id="T533" time="366.2932" type="appl" />
         <tli id="T534" time="366.7463" type="appl" />
         <tli id="T535" time="367.1994" type="appl" />
         <tli id="T536" time="367.65250000000003" type="appl" />
         <tli id="T537" time="368.1056" type="appl" />
         <tli id="T538" time="368.5587" type="appl" />
         <tli id="T539" time="369.0118" type="appl" />
         <tli id="T540" time="369.4649" type="appl" />
         <tli id="T541" time="370.46250084336" />
         <tli id="T542" time="370.71225000000004" type="appl" />
         <tli id="T543" time="371.5065" type="appl" />
         <tli id="T544" time="372.30075" type="appl" />
         <tli id="T545" time="374.84245159113914" />
         <tli id="T546" time="375.0423010607594" type="intp" />
         <tli id="T547" time="375.2421505303797" type="intp" />
         <tli id="T548" time="375.442" type="appl" />
         <tli id="T549" time="376.22433333333333" type="appl" />
         <tli id="T550" time="377.00666666666666" type="appl" />
         <tli id="T551" time="378.3557454177443" />
         <tli id="T552" time="378.36879999999996" type="appl" />
         <tli id="T553" time="378.9486" type="appl" />
         <tli id="T554" time="379.5284" type="appl" />
         <tli id="T555" time="380.1082" type="appl" />
         <tli id="T556" time="380.688" type="appl" />
         <tli id="T557" time="381.26779999999997" type="appl" />
         <tli id="T558" time="381.8476" type="appl" />
         <tli id="T559" time="382.4274" type="appl" />
         <tli id="T560" time="383.0072" type="appl" />
         <tli id="T561" time="383.587" type="appl" />
         <tli id="T562" time="384.16679999999997" type="appl" />
         <tli id="T563" time="384.7466" type="appl" />
         <tli id="T564" time="385.3264" type="appl" />
         <tli id="T565" time="385.9062" type="appl" />
         <tli id="T566" time="386.54599186069805" />
         <tli id="T567" time="388.82896098054647" />
         <tli id="T568" time="388.99725" type="appl" />
         <tli id="T569" time="389.9805" type="appl" />
         <tli id="T570" time="390.96375" type="appl" />
         <tli id="T571" time="392.5155861913865" />
         <tli id="T572" time="392.69971428571426" type="appl" />
         <tli id="T573" time="393.4524285714286" type="appl" />
         <tli id="T574" time="394.20514285714285" type="appl" />
         <tli id="T575" time="394.95785714285716" type="appl" />
         <tli id="T576" time="395.7105714285714" type="appl" />
         <tli id="T577" time="396.46328571428575" type="appl" />
         <tli id="T578" time="397.8221931856516" />
         <tli id="T579" time="398.7375" type="appl" />
         <tli id="T580" time="400.259" type="appl" />
         <tli id="T581" time="401.293" type="appl" />
         <tli id="T582" time="402.4670107466898" />
         <tli id="T583" time="402.9841666666667" type="appl" />
         <tli id="T584" time="403.6413333333333" type="appl" />
         <tli id="T585" time="404.2985" type="appl" />
         <tli id="T586" time="404.9556666666667" type="appl" />
         <tli id="T587" time="405.6128333333333" type="appl" />
         <tli id="T588" time="407.095422242213" />
         <tli id="T589" time="407.12266666666665" type="appl" />
         <tli id="T590" time="407.9753333333333" type="appl" />
         <tli id="T591" time="409.3487302372652" />
         <tli id="T592" time="409.4223333333333" type="appl" />
         <tli id="T593" time="410.01666666666665" type="appl" />
         <tli id="T594" time="410.611" type="appl" />
         <tli id="T595" time="411.20533333333333" type="appl" />
         <tli id="T596" time="411.79966666666667" type="appl" />
         <tli id="T597" time="412.394" type="appl" />
         <tli id="T598" time="412.98833333333334" type="appl" />
         <tli id="T599" time="413.5826666666667" type="appl" />
         <tli id="T600" time="414.3103411109862" />
         <tli id="T601" time="414.73333333333335" type="appl" />
         <tli id="T602" time="415.2896666666667" type="appl" />
         <tli id="T603" time="415.846" type="appl" />
         <tli id="T604" time="416.40233333333333" type="appl" />
         <tli id="T605" time="416.95866666666666" type="appl" />
         <tli id="T606" time="418.1486312830316" />
         <tli id="T607" time="418.4355" type="appl" />
         <tli id="T608" time="419.356" type="appl" />
         <tli id="T609" time="420.2765" type="appl" />
         <tli id="T610" time="421.6285921511301" />
         <tli id="T611" time="422.669" type="appl" />
         <tli id="T612" time="423.24399999999997" type="appl" />
         <tli id="T613" time="423.81899999999996" type="appl" />
         <tli id="T614" time="424.394" type="appl" />
         <tli id="T615" time="424.969" type="appl" />
         <tli id="T616" time="426.24854020015744" />
         <tli id="T617" time="427.2795" type="appl" />
         <tli id="T618" time="429.19499144253206" />
         <tli id="T619" time="429.96366666666665" type="appl" />
         <tli id="T620" time="430.9123333333333" type="appl" />
         <tli id="T621" time="432.1884734060497" />
         <tli id="T622" time="432.7463333333333" type="appl" />
         <tli id="T623" time="433.63166666666666" type="appl" />
         <tli id="T624" time="434.99510851231304" />
         <tli id="T625" time="435.23539999999997" type="appl" />
         <tli id="T626" time="435.9538" type="appl" />
         <tli id="T627" time="436.6722" type="appl" />
         <tli id="T628" time="437.3906" type="appl" />
         <tli id="T629" time="438.8417319239851" />
         <tli id="T630" time="442.11502844934216" />
         <tli id="T631" time="442.202" type="appl" />
         <tli id="T632" time="443.379" type="appl" />
         <tli id="T633" time="444.556" type="appl" />
         <tli id="T634" time="445.733" type="appl" />
         <tli id="T635" time="447.148" type="appl" />
         <tli id="T636" time="449.1363036622906" />
         <tli id="T637" time="449.26619999999997" type="appl" />
         <tli id="T638" time="449.9694" type="appl" />
         <tli id="T639" time="450.6726" type="appl" />
         <tli id="T640" time="451.3758" type="appl" />
         <tli id="T641" time="452.079" type="appl" />
         <tli id="T642" time="452.7822" type="appl" />
         <tli id="T643" time="453.4854" type="appl" />
         <tli id="T644" time="454.1886" type="appl" />
         <tli id="T645" time="454.89180000000005" type="appl" />
         <tli id="T646" time="455.51498193452716" />
         <tli id="T647" time="456.1466666666667" type="appl" />
         <tli id="T648" time="456.6983333333333" type="appl" />
         <tli id="T649" time="457.2166815490414" />
         <tli id="T650" time="457.691875" type="appl" />
         <tli id="T651" time="458.13375" type="appl" />
         <tli id="T652" time="458.575625" type="appl" />
         <tli id="T653" time="459.01750000000004" type="appl" />
         <tli id="T654" time="459.459375" type="appl" />
         <tli id="T655" time="459.90125" type="appl" />
         <tli id="T656" time="460.34312500000004" type="appl" />
         <tli id="T657" time="461.2081470819745" />
         <tli id="T658" time="461.9715" type="appl" />
         <tli id="T659" time="463.158" type="appl" />
         <tli id="T660" time="464.34450000000004" type="appl" />
         <tli id="T661" time="465.66143033846845" />
         <tli id="T662" time="466.46166666666664" type="appl" />
         <tli id="T663" time="467.39233333333334" type="appl" />
         <tli id="T664" time="469.21472371528165" />
         <tli id="T665" time="469.28433333333334" type="appl" />
         <tli id="T666" time="470.24566666666664" type="appl" />
         <tli id="T667" time="471.207" type="appl" />
         <tli id="T668" time="472.6643203414905" />
         <tli id="T669" time="473.4099890398347" />
         <tli id="T670" time="473.4123327634797" />
         <tli id="T671" time="474.158" type="appl" />
         <tli id="T0" time="474.288" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-KiPP"
                      id="tx-KiPP"
                      speaker="KiPP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KiPP">
            <ts e="T4" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <nts id="Seg_3" n="HIAT:ip">–</nts>
                  <nts id="Seg_4" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_6" n="HIAT:w" s="T1">Muntan</ts>
                  <nts id="Seg_7" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_9" n="HIAT:w" s="T2">isteːčči</ts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_12" n="HIAT:w" s="T3">du͡o</ts>
                  <nts id="Seg_13" n="HIAT:ip">?</nts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T9" id="Seg_15" n="sc" s="T6">
               <ts e="T9" id="Seg_17" n="HIAT:u" s="T6">
                  <nts id="Seg_18" n="HIAT:ip">–</nts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_21" n="HIAT:w" s="T6">Heː</ts>
                  <nts id="Seg_22" n="HIAT:ip">.</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T25" id="Seg_24" n="sc" s="T14">
               <ts e="T20" id="Seg_26" n="HIAT:u" s="T14">
                  <nts id="Seg_27" n="HIAT:ip">–</nts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_30" n="HIAT:w" s="T14">Dʼe</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_33" n="HIAT:w" s="T15">tugu</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_36" n="HIAT:w" s="T17">ɨjɨtar</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_39" n="HIAT:w" s="T18">ke</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_42" n="HIAT:w" s="T19">do</ts>
                  <nts id="Seg_43" n="HIAT:ip">?</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_46" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_48" n="HIAT:w" s="T20">Maŋnaj</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_51" n="HIAT:w" s="T21">lʼuboppɨn</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_54" n="HIAT:w" s="T22">ɨjɨtalɨːr</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_57" n="HIAT:w" s="T24">du͡o</ts>
                  <nts id="Seg_58" n="HIAT:ip">.</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T33" id="Seg_60" n="sc" s="T27">
               <ts e="T31" id="Seg_62" n="HIAT:u" s="T27">
                  <nts id="Seg_63" n="HIAT:ip">–</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_65" n="HIAT:ip">(</nts>
                  <ts e="T28" id="Seg_67" n="HIAT:w" s="T27">I-</ts>
                  <nts id="Seg_68" n="HIAT:ip">)</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_70" n="HIAT:ip">(</nts>
                  <nts id="Seg_71" n="HIAT:ip">(</nts>
                  <ats e="T29" id="Seg_72" n="HIAT:non-pho" s="T28">LAUGH</ats>
                  <nts id="Seg_73" n="HIAT:ip">)</nts>
                  <nts id="Seg_74" n="HIAT:ip">)</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_77" n="HIAT:w" s="T29">ister</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_79" n="HIAT:ip">(</nts>
                  <nts id="Seg_80" n="HIAT:ip">(</nts>
                  <ats e="T31" id="Seg_81" n="HIAT:non-pho" s="T30">LAUGH</ats>
                  <nts id="Seg_82" n="HIAT:ip">)</nts>
                  <nts id="Seg_83" n="HIAT:ip">)</nts>
                  <nts id="Seg_84" n="HIAT:ip">.</nts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_87" n="HIAT:u" s="T31">
                  <ts e="T33" id="Seg_89" n="HIAT:w" s="T31">Eː</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T62" id="Seg_92" n="sc" s="T49">
               <ts e="T51" id="Seg_94" n="HIAT:u" s="T49">
                  <nts id="Seg_95" n="HIAT:ip">–</nts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_98" n="HIAT:w" s="T49">Jeːrakata</ts>
                  <nts id="Seg_99" n="HIAT:ip">.</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_102" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_104" n="HIAT:w" s="T51">Kačča</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_107" n="HIAT:w" s="T52">bütü͡ömüj</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_110" n="HIAT:w" s="T53">ol</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_113" n="HIAT:w" s="T54">ki͡ehege</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_116" n="HIAT:w" s="T55">da</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_119" n="HIAT:w" s="T56">di͡eri</ts>
                  <nts id="Seg_120" n="HIAT:ip">.</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_123" n="HIAT:u" s="T57">
                  <nts id="Seg_124" n="HIAT:ip">(</nts>
                  <nts id="Seg_125" n="HIAT:ip">(</nts>
                  <ats e="T62" id="Seg_126" n="HIAT:non-pho" s="T57">LAUGH</ats>
                  <nts id="Seg_127" n="HIAT:ip">)</nts>
                  <nts id="Seg_128" n="HIAT:ip">)</nts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T68" id="Seg_131" n="sc" s="T66">
               <ts e="T68" id="Seg_133" n="HIAT:u" s="T66">
                  <nts id="Seg_134" n="HIAT:ip">–</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_137" n="HIAT:w" s="T66">Korgoː</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T81" id="Seg_140" n="sc" s="T70">
               <ts e="T73" id="Seg_142" n="HIAT:u" s="T70">
                  <nts id="Seg_143" n="HIAT:ip">–</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_146" n="HIAT:w" s="T70">Korgoːgo</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_149" n="HIAT:w" s="T72">töröːbütüm</ts>
                  <nts id="Seg_150" n="HIAT:ip">.</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_153" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_155" n="HIAT:w" s="T73">Hubunna</ts>
                  <nts id="Seg_156" n="HIAT:ip">,</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_159" n="HIAT:w" s="T74">bejem</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_162" n="HIAT:w" s="T75">hirber</ts>
                  <nts id="Seg_163" n="HIAT:ip">.</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_166" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_168" n="HIAT:w" s="T76">Bulunnʼabɨn</ts>
                  <nts id="Seg_169" n="HIAT:ip">.</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_172" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_174" n="HIAT:w" s="T77">Bilegin</ts>
                  <nts id="Seg_175" n="HIAT:ip">,</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_178" n="HIAT:w" s="T78">bulunnʼa</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_181" n="HIAT:w" s="T79">aːtɨn</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_183" n="HIAT:ip">(</nts>
                  <nts id="Seg_184" n="HIAT:ip">(</nts>
                  <ats e="T81" id="Seg_185" n="HIAT:non-pho" s="T80">LAUGH</ats>
                  <nts id="Seg_186" n="HIAT:ip">)</nts>
                  <nts id="Seg_187" n="HIAT:ip">)</nts>
                  <nts id="Seg_188" n="HIAT:ip">?</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T92" id="Seg_190" n="sc" s="T89">
               <ts e="T92" id="Seg_192" n="HIAT:u" s="T89">
                  <nts id="Seg_193" n="HIAT:ip">–</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_196" n="HIAT:w" s="T89">Bʼez</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_199" n="HIAT:w" s="T90">atca</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_201" n="HIAT:ip">(</nts>
                  <nts id="Seg_202" n="HIAT:ip">(</nts>
                  <ats e="T92" id="Seg_203" n="HIAT:non-pho" s="T91">LAUGH</ats>
                  <nts id="Seg_204" n="HIAT:ip">)</nts>
                  <nts id="Seg_205" n="HIAT:ip">)</nts>
                  <nts id="Seg_206" n="HIAT:ip">.</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T108" id="Seg_208" n="sc" s="T106">
               <ts e="T108" id="Seg_210" n="HIAT:u" s="T106">
                  <nts id="Seg_211" n="HIAT:ip">–</nts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_214" n="HIAT:w" s="T106">Teːtete</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_217" n="HIAT:w" s="T107">hu͡ok</ts>
                  <nts id="Seg_218" n="HIAT:ip">.</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T117" id="Seg_220" n="sc" s="T111">
               <ts e="T117" id="Seg_222" n="HIAT:u" s="T111">
                  <nts id="Seg_223" n="HIAT:ip">–</nts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_226" n="HIAT:w" s="T111">Dʼe</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_229" n="HIAT:w" s="T112">min</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_232" n="HIAT:w" s="T113">baːbuskam</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_235" n="HIAT:w" s="T115">iːtte</ts>
                  <nts id="Seg_236" n="HIAT:ip">.</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T124" id="Seg_238" n="sc" s="T120">
               <ts e="T124" id="Seg_240" n="HIAT:u" s="T120">
                  <nts id="Seg_241" n="HIAT:ip">–</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_244" n="HIAT:w" s="T120">Baːbuska</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_247" n="HIAT:w" s="T121">vaspʼitɨvajdaːbɨta</ts>
                  <nts id="Seg_248" n="HIAT:ip">,</nts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_251" n="HIAT:w" s="T122">maːmam</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_254" n="HIAT:w" s="T123">maːmata</ts>
                  <nts id="Seg_255" n="HIAT:ip">.</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T254" id="Seg_257" n="sc" s="T128">
               <ts e="T139" id="Seg_259" n="HIAT:u" s="T128">
                  <nts id="Seg_260" n="HIAT:ip">–</nts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_263" n="HIAT:w" s="T128">Onton</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_266" n="HIAT:w" s="T129">bihigi</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_269" n="HIAT:w" s="T130">kergetter</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_272" n="HIAT:w" s="T131">barɨta</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_275" n="HIAT:w" s="T132">ölönnör</ts>
                  <nts id="Seg_276" n="HIAT:ip">,</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_279" n="HIAT:w" s="T133">tulaːjak</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_282" n="HIAT:w" s="T134">hɨldʼɨbɨppɨt</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_285" n="HIAT:w" s="T135">emeːksini</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_288" n="HIAT:w" s="T136">gɨtta</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_291" n="HIAT:w" s="T137">baːbuskabɨn</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_294" n="HIAT:w" s="T138">gɨtta</ts>
                  <nts id="Seg_295" n="HIAT:ip">.</nts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_298" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_300" n="HIAT:w" s="T139">Onton</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_302" n="HIAT:ip">(</nts>
                  <ts e="T141" id="Seg_304" n="HIAT:w" s="T140">Rɨb-</ts>
                  <nts id="Seg_305" n="HIAT:ip">)</nts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_308" n="HIAT:w" s="T141">Rɨbnaj</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_311" n="HIAT:w" s="T142">di͡ek</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_314" n="HIAT:w" s="T143">barbɨtɨm</ts>
                  <nts id="Seg_315" n="HIAT:ip">.</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_318" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_320" n="HIAT:w" s="T144">Onton</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_323" n="HIAT:w" s="T145">ol</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_326" n="HIAT:w" s="T146">barbɨppɨt</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_328" n="HIAT:ip">(</nts>
                  <ts e="T148" id="Seg_330" n="HIAT:w" s="T147">i-</ts>
                  <nts id="Seg_331" n="HIAT:ip">)</nts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_334" n="HIAT:w" s="T148">ebebin</ts>
                  <nts id="Seg_335" n="HIAT:ip">,</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_338" n="HIAT:w" s="T149">inʼebin</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_341" n="HIAT:w" s="T150">gɨtta</ts>
                  <nts id="Seg_342" n="HIAT:ip">,</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_345" n="HIAT:w" s="T151">inʼe</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_348" n="HIAT:w" s="T152">di͡eččibin</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_351" n="HIAT:w" s="T153">ontubun</ts>
                  <nts id="Seg_352" n="HIAT:ip">,</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_355" n="HIAT:w" s="T154">maːmam</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_358" n="HIAT:w" s="T155">maːmam</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_361" n="HIAT:w" s="T156">hin</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_364" n="HIAT:w" s="T157">biːr</ts>
                  <nts id="Seg_365" n="HIAT:ip">.</nts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_368" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_370" n="HIAT:w" s="T158">Dʼe</ts>
                  <nts id="Seg_371" n="HIAT:ip">,</nts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_374" n="HIAT:w" s="T159">onton</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_377" n="HIAT:w" s="T160">töttörü</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_380" n="HIAT:w" s="T161">kelbippit</ts>
                  <nts id="Seg_381" n="HIAT:ip">,</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_384" n="HIAT:w" s="T162">maːmam</ts>
                  <nts id="Seg_385" n="HIAT:ip">,</nts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_388" n="HIAT:w" s="T163">emeːksin</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_390" n="HIAT:ip">(</nts>
                  <ts e="T165" id="Seg_392" n="HIAT:w" s="T164">baː-</ts>
                  <nts id="Seg_393" n="HIAT:ip">)</nts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_396" n="HIAT:w" s="T165">baːbuskam</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_399" n="HIAT:w" s="T166">ölörüger</ts>
                  <nts id="Seg_400" n="HIAT:ip">.</nts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_403" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_405" n="HIAT:w" s="T167">Ölön</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_408" n="HIAT:w" s="T168">baran</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_411" n="HIAT:w" s="T169">tillibite</ts>
                  <nts id="Seg_412" n="HIAT:ip">,</nts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_415" n="HIAT:w" s="T170">ol</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_418" n="HIAT:w" s="T171">dojduga</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_421" n="HIAT:w" s="T172">onton</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_424" n="HIAT:w" s="T173">töttörü</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_427" n="HIAT:w" s="T174">kelbippit</ts>
                  <nts id="Seg_428" n="HIAT:ip">.</nts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_431" n="HIAT:u" s="T175">
                  <ts e="T176" id="Seg_433" n="HIAT:w" s="T175">Ol</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_436" n="HIAT:w" s="T176">kelbippit</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_439" n="HIAT:w" s="T177">kenne</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_442" n="HIAT:w" s="T178">u͡on</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_445" n="HIAT:w" s="T179">alta</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_448" n="HIAT:w" s="T180">dʼɨllaːkpar</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_451" n="HIAT:w" s="T181">minigin</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_454" n="HIAT:w" s="T182">erge</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_457" n="HIAT:w" s="T183">bi͡erbite</ts>
                  <nts id="Seg_458" n="HIAT:ip">.</nts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_461" n="HIAT:u" s="T184">
                  <nts id="Seg_462" n="HIAT:ip">"</nts>
                  <ts e="T185" id="Seg_464" n="HIAT:w" s="T184">Min</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_467" n="HIAT:w" s="T185">öllöppüne</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_470" n="HIAT:w" s="T186">enigin</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_473" n="HIAT:w" s="T187">kulut</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_476" n="HIAT:w" s="T188">gɨnɨ͡aktara</ts>
                  <nts id="Seg_477" n="HIAT:ip">"</nts>
                  <nts id="Seg_478" n="HIAT:ip">,</nts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_481" n="HIAT:w" s="T189">di͡ete</ts>
                  <nts id="Seg_482" n="HIAT:ip">.</nts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_485" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_487" n="HIAT:w" s="T190">Čort</ts>
                  <nts id="Seg_488" n="HIAT:ip">,</nts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_491" n="HIAT:w" s="T191">tuːgu</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_494" n="HIAT:w" s="T192">da</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_497" n="HIAT:w" s="T193">ɨraːspɨn</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_500" n="HIAT:w" s="T194">haːtar</ts>
                  <nts id="Seg_501" n="HIAT:ip">,</nts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_504" n="HIAT:w" s="T195">huːna</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_507" n="HIAT:w" s="T196">da</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_510" n="HIAT:w" s="T197">ilikpin</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_513" n="HIAT:w" s="T198">erge</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_516" n="HIAT:w" s="T199">barbɨtɨm</ts>
                  <nts id="Seg_517" n="HIAT:ip">.</nts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_520" n="HIAT:u" s="T200">
                  <ts e="T201" id="Seg_522" n="HIAT:w" s="T200">ɨtɨː-ɨtɨː</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_525" n="HIAT:w" s="T201">erge</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_528" n="HIAT:w" s="T202">bi͡ereller</ts>
                  <nts id="Seg_529" n="HIAT:ip">,</nts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_532" n="HIAT:w" s="T203">ututallar</ts>
                  <nts id="Seg_533" n="HIAT:ip">.</nts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T210" id="Seg_536" n="HIAT:u" s="T204">
                  <ts e="T205" id="Seg_538" n="HIAT:w" s="T204">ɨtɨː-ɨtɨː</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_541" n="HIAT:w" s="T205">utujabɨn</ts>
                  <nts id="Seg_542" n="HIAT:ip">,</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_545" n="HIAT:w" s="T206">erbitten</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_548" n="HIAT:w" s="T207">kuttanabɨn</ts>
                  <nts id="Seg_549" n="HIAT:ip">,</nts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_552" n="HIAT:w" s="T208">taŋastɨːn</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_555" n="HIAT:w" s="T209">utujabɨn</ts>
                  <nts id="Seg_556" n="HIAT:ip">.</nts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_559" n="HIAT:u" s="T210">
                  <ts e="T211" id="Seg_561" n="HIAT:w" s="T210">Eː</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_564" n="HIAT:w" s="T211">dʼe</ts>
                  <nts id="Seg_565" n="HIAT:ip">.</nts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_568" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_570" n="HIAT:w" s="T212">Onton</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_573" n="HIAT:w" s="T213">dʼɨlɨ</ts>
                  <nts id="Seg_574" n="HIAT:ip">,</nts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_577" n="HIAT:w" s="T214">dʼɨlɨ</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_580" n="HIAT:w" s="T215">taŋastaːk</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_583" n="HIAT:w" s="T216">utujbutum</ts>
                  <nts id="Seg_584" n="HIAT:ip">.</nts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_587" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_589" n="HIAT:w" s="T217">Onton</ts>
                  <nts id="Seg_590" n="HIAT:ip">,</nts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_593" n="HIAT:w" s="T218">dʼe</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_596" n="HIAT:w" s="T219">kojut</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_599" n="HIAT:w" s="T220">ü͡örenebin</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_602" n="HIAT:w" s="T221">bu͡o</ts>
                  <nts id="Seg_603" n="HIAT:ip">,</nts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_605" n="HIAT:ip">(</nts>
                  <ts e="T223" id="Seg_607" n="HIAT:w" s="T222">er-</ts>
                  <nts id="Seg_608" n="HIAT:ip">)</nts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_610" n="HIAT:ip">(</nts>
                  <ts e="T224" id="Seg_612" n="HIAT:w" s="T223">er-</ts>
                  <nts id="Seg_613" n="HIAT:ip">)</nts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_616" n="HIAT:w" s="T224">erber</ts>
                  <nts id="Seg_617" n="HIAT:ip">.</nts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T232" id="Seg_620" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_622" n="HIAT:w" s="T225">Ol</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_625" n="HIAT:w" s="T226">aːta</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_628" n="HIAT:w" s="T227">pʼervaja</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_631" n="HIAT:w" s="T228">lʼubovʼ</ts>
                  <nts id="Seg_632" n="HIAT:ip">,</nts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_635" n="HIAT:w" s="T229">patom</ts>
                  <nts id="Seg_636" n="HIAT:ip">,</nts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_639" n="HIAT:w" s="T230">celɨj</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_642" n="HIAT:w" s="T231">god</ts>
                  <nts id="Seg_643" n="HIAT:ip">.</nts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T240" id="Seg_646" n="HIAT:u" s="T232">
                  <ts e="T233" id="Seg_648" n="HIAT:w" s="T232">U͡on</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_651" n="HIAT:w" s="T233">hette</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_654" n="HIAT:w" s="T234">u͡on</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_657" n="HIAT:w" s="T235">agɨs</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_660" n="HIAT:w" s="T236">u͡on</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_663" n="HIAT:w" s="T237">agɨs</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_666" n="HIAT:w" s="T238">dʼɨllaːkpar</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_669" n="HIAT:w" s="T239">ogolommutum</ts>
                  <nts id="Seg_670" n="HIAT:ip">.</nts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_673" n="HIAT:u" s="T240">
                  <ts e="T241" id="Seg_675" n="HIAT:w" s="T240">Pi͡erbej</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_678" n="HIAT:w" s="T241">ogom</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_681" n="HIAT:w" s="T242">munna</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_684" n="HIAT:w" s="T243">baːr</ts>
                  <nts id="Seg_685" n="HIAT:ip">.</nts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T254" id="Seg_688" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_690" n="HIAT:w" s="T244">Onton</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_693" n="HIAT:w" s="T245">ogolonnum</ts>
                  <nts id="Seg_694" n="HIAT:ip">,</nts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_697" n="HIAT:w" s="T246">ogolonnum</ts>
                  <nts id="Seg_698" n="HIAT:ip">,</nts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_701" n="HIAT:w" s="T247">dʼɨl</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_704" n="HIAT:w" s="T248">aːjɨ</ts>
                  <nts id="Seg_705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_707" n="HIAT:w" s="T249">ogolonobun</ts>
                  <nts id="Seg_708" n="HIAT:ip">,</nts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_711" n="HIAT:w" s="T250">dʼɨl</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_714" n="HIAT:w" s="T251">aːjɨ</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_717" n="HIAT:w" s="T252">ogolonobun</ts>
                  <nts id="Seg_718" n="HIAT:ip">.</nts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T406" id="Seg_720" n="sc" s="T258">
               <ts e="T265" id="Seg_722" n="HIAT:u" s="T258">
                  <nts id="Seg_723" n="HIAT:ip">–</nts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_726" n="HIAT:w" s="T258">Eː</ts>
                  <nts id="Seg_727" n="HIAT:ip">,</nts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_730" n="HIAT:w" s="T259">hubu</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_733" n="HIAT:w" s="T260">hirge</ts>
                  <nts id="Seg_734" n="HIAT:ip">,</nts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_737" n="HIAT:w" s="T261">tɨ͡aga</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_740" n="HIAT:w" s="T262">hɨldʼammɨt</ts>
                  <nts id="Seg_741" n="HIAT:ip">,</nts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_744" n="HIAT:w" s="T263">tɨ͡aga</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_747" n="HIAT:w" s="T264">etibit</ts>
                  <nts id="Seg_748" n="HIAT:ip">.</nts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T268" id="Seg_751" n="HIAT:u" s="T265">
                  <ts e="T266" id="Seg_753" n="HIAT:w" s="T265">Ogonnʼorum</ts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_756" n="HIAT:w" s="T266">kassʼiːr</ts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_759" n="HIAT:w" s="T267">ete</ts>
                  <nts id="Seg_760" n="HIAT:ip">.</nts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T275" id="Seg_763" n="HIAT:u" s="T268">
                  <ts e="T269" id="Seg_765" n="HIAT:w" s="T268">Hürdeːk</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_768" n="HIAT:w" s="T269">üčügej</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_771" n="HIAT:w" s="T270">kihi</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_774" n="HIAT:w" s="T271">ete</ts>
                  <nts id="Seg_775" n="HIAT:ip">,</nts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_778" n="HIAT:w" s="T272">spasoːbnɨj</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_781" n="HIAT:w" s="T273">bagajɨ</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_784" n="HIAT:w" s="T274">bulčut</ts>
                  <nts id="Seg_785" n="HIAT:ip">.</nts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T278" id="Seg_788" n="HIAT:u" s="T275">
                  <ts e="T276" id="Seg_790" n="HIAT:w" s="T275">Buldu</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_793" n="HIAT:w" s="T276">daː</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_796" n="HIAT:w" s="T277">bultaːbɨta</ts>
                  <nts id="Seg_797" n="HIAT:ip">.</nts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T281" id="Seg_800" n="HIAT:u" s="T278">
                  <ts e="T279" id="Seg_802" n="HIAT:w" s="T278">Dʼi͡e</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_805" n="HIAT:w" s="T279">da</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_808" n="HIAT:w" s="T280">tupputa</ts>
                  <nts id="Seg_809" n="HIAT:ip">.</nts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286" id="Seg_812" n="HIAT:u" s="T281">
                  <ts e="T282" id="Seg_814" n="HIAT:w" s="T281">Kassʼiːrga</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_817" n="HIAT:w" s="T282">u͡on</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_820" n="HIAT:w" s="T283">bi͡es</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_823" n="HIAT:w" s="T284">dʼɨlɨ</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_826" n="HIAT:w" s="T285">olorbuta</ts>
                  <nts id="Seg_827" n="HIAT:ip">.</nts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T291" id="Seg_830" n="HIAT:u" s="T286">
                  <ts e="T287" id="Seg_832" n="HIAT:w" s="T286">Onton</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_835" n="HIAT:w" s="T287">biːr</ts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_838" n="HIAT:w" s="T288">dʼɨlɨ</ts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_841" n="HIAT:w" s="T289">partorgaga</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_844" n="HIAT:w" s="T290">olorbuta</ts>
                  <nts id="Seg_845" n="HIAT:ip">.</nts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T298" id="Seg_848" n="HIAT:u" s="T291">
                  <ts e="T292" id="Seg_850" n="HIAT:w" s="T291">Onton</ts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_853" n="HIAT:w" s="T292">dʼi͡eleri</ts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_856" n="HIAT:w" s="T293">tupputa</ts>
                  <nts id="Seg_857" n="HIAT:ip">,</nts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_860" n="HIAT:w" s="T294">bu</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_863" n="HIAT:w" s="T295">turar</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_866" n="HIAT:w" s="T296">tupput</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_869" n="HIAT:w" s="T297">dʼi͡elere</ts>
                  <nts id="Seg_870" n="HIAT:ip">.</nts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T304" id="Seg_873" n="HIAT:u" s="T298">
                  <ts e="T299" id="Seg_875" n="HIAT:w" s="T298">Onton</ts>
                  <nts id="Seg_876" n="HIAT:ip">,</nts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_879" n="HIAT:w" s="T299">u͡on</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_882" n="HIAT:w" s="T300">ikki</ts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_885" n="HIAT:w" s="T301">ogoloːkput</ts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_888" n="HIAT:w" s="T302">onton</ts>
                  <nts id="Seg_889" n="HIAT:ip">,</nts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_892" n="HIAT:w" s="T303">barɨta</ts>
                  <nts id="Seg_893" n="HIAT:ip">.</nts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T309" id="Seg_896" n="HIAT:u" s="T304">
                  <ts e="T305" id="Seg_898" n="HIAT:w" s="T304">Biːr</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_901" n="HIAT:w" s="T305">de</ts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_904" n="HIAT:w" s="T672">kihini</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_907" n="HIAT:w" s="T306">kihi͡eke</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_910" n="HIAT:w" s="T307">bi͡erbetegim</ts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_913" n="HIAT:w" s="T308">ogolorbun</ts>
                  <nts id="Seg_914" n="HIAT:ip">.</nts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T316" id="Seg_917" n="HIAT:u" s="T309">
                  <ts e="T310" id="Seg_919" n="HIAT:w" s="T309">Bejem</ts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_922" n="HIAT:w" s="T310">iːti͡ek</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_925" n="HIAT:w" s="T311">bu͡olammɨn</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_928" n="HIAT:w" s="T312">kihi͡eke</ts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_931" n="HIAT:w" s="T313">bi͡erbeppin</ts>
                  <nts id="Seg_932" n="HIAT:ip">,</nts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_935" n="HIAT:w" s="T314">ahɨnabɨn</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_938" n="HIAT:w" s="T315">ogolorbun</ts>
                  <nts id="Seg_939" n="HIAT:ip">.</nts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_942" n="HIAT:u" s="T316">
                  <ts e="T317" id="Seg_944" n="HIAT:w" s="T316">Olorbun</ts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_947" n="HIAT:w" s="T317">iːtine</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_950" n="HIAT:w" s="T318">iːtibitinen</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_953" n="HIAT:w" s="T319">bihikteːk</ts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_956" n="HIAT:w" s="T320">ogoloːk</ts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_959" n="HIAT:w" s="T321">ilimneheːččibin</ts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_962" n="HIAT:w" s="T322">ogonnʼorbun</ts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_965" n="HIAT:w" s="T323">gɨtta</ts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_968" n="HIAT:w" s="T324">ɨ͡arɨj</ts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_971" n="HIAT:w" s="T325">bu͡olbutun</ts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_974" n="HIAT:w" s="T326">kenne</ts>
                  <nts id="Seg_975" n="HIAT:ip">.</nts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T342" id="Seg_978" n="HIAT:u" s="T327">
                  <ts e="T328" id="Seg_980" n="HIAT:w" s="T327">Dʼe</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_983" n="HIAT:w" s="T328">ogolorbut</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_986" n="HIAT:w" s="T329">ulaːtannar</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_989" n="HIAT:w" s="T330">ü͡örene</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_992" n="HIAT:w" s="T331">barɨtalaːbɨttara</ts>
                  <nts id="Seg_993" n="HIAT:ip">,</nts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_996" n="HIAT:w" s="T332">onton</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_999" n="HIAT:w" s="T333">kojut</ts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1002" n="HIAT:w" s="T334">ikki</ts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1005" n="HIAT:w" s="T335">ulakan</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1008" n="HIAT:w" s="T336">ogom</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1011" n="HIAT:w" s="T337">taba</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1014" n="HIAT:w" s="T338">ü͡öregiger</ts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1017" n="HIAT:w" s="T339">barbɨttara</ts>
                  <nts id="Seg_1018" n="HIAT:ip">,</nts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1021" n="HIAT:w" s="T340">u͡olbut</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1024" n="HIAT:w" s="T341">kɨːspɨt</ts>
                  <nts id="Seg_1025" n="HIAT:ip">.</nts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T354" id="Seg_1028" n="HIAT:u" s="T342">
                  <ts e="T343" id="Seg_1030" n="HIAT:w" s="T342">Onton</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1033" n="HIAT:w" s="T343">biːr</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1036" n="HIAT:w" s="T344">ogobut</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1039" n="HIAT:w" s="T345">ulaːtan</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1042" n="HIAT:w" s="T346">Krasnajarska</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1045" n="HIAT:w" s="T347">ü͡örene</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1048" n="HIAT:w" s="T348">barbɨta</ts>
                  <nts id="Seg_1049" n="HIAT:ip">,</nts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1052" n="HIAT:w" s="T349">anɨ</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1055" n="HIAT:w" s="T350">tʼerapʼevka</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1058" n="HIAT:w" s="T351">üleliː</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1061" n="HIAT:w" s="T352">hɨtar</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1064" n="HIAT:w" s="T353">Dudʼinkaga</ts>
                  <nts id="Seg_1065" n="HIAT:ip">.</nts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T367" id="Seg_1068" n="HIAT:u" s="T354">
                  <ts e="T355" id="Seg_1070" n="HIAT:w" s="T354">Onton</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1073" n="HIAT:w" s="T355">biːr</ts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1076" n="HIAT:w" s="T356">biːr</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1079" n="HIAT:w" s="T357">ogom</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1082" n="HIAT:w" s="T358">bu͡ollagɨna</ts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1085" n="HIAT:w" s="T359">emi͡e</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1088" n="HIAT:w" s="T360">ü͡örenen</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1091" n="HIAT:w" s="T361">ikki</ts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1094" n="HIAT:w" s="T362">institutu</ts>
                  <nts id="Seg_1095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1097" n="HIAT:w" s="T363">büppüte</ts>
                  <nts id="Seg_1098" n="HIAT:ip">,</nts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1101" n="HIAT:w" s="T364">onton</ts>
                  <nts id="Seg_1102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1104" n="HIAT:w" s="T365">Krasnajarskajga</ts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1107" n="HIAT:w" s="T366">baːr</ts>
                  <nts id="Seg_1108" n="HIAT:ip">.</nts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T369" id="Seg_1111" n="HIAT:u" s="T367">
                  <nts id="Seg_1112" n="HIAT:ip">(</nts>
                  <ts e="T368" id="Seg_1114" n="HIAT:w" s="T367">Sʼer-</ts>
                  <nts id="Seg_1115" n="HIAT:ip">)</nts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1118" n="HIAT:w" s="T368">Sʼergʼej</ts>
                  <nts id="Seg_1119" n="HIAT:ip">.</nts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T375" id="Seg_1122" n="HIAT:u" s="T369">
                  <ts e="T370" id="Seg_1124" n="HIAT:w" s="T369">Ontum</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1127" n="HIAT:w" s="T370">dʼaktardaːk</ts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1130" n="HIAT:w" s="T371">ogo</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1133" n="HIAT:w" s="T372">ol</ts>
                  <nts id="Seg_1134" n="HIAT:ip">,</nts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1137" n="HIAT:w" s="T373">ikki</ts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1140" n="HIAT:w" s="T374">u͡ollaːk</ts>
                  <nts id="Seg_1141" n="HIAT:ip">.</nts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T377" id="Seg_1144" n="HIAT:u" s="T375">
                  <ts e="T376" id="Seg_1146" n="HIAT:w" s="T375">Kazaːktartan</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1149" n="HIAT:w" s="T376">dʼaktardammɨt</ts>
                  <nts id="Seg_1150" n="HIAT:ip">.</nts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T386" id="Seg_1153" n="HIAT:u" s="T377">
                  <nts id="Seg_1154" n="HIAT:ip">(</nts>
                  <ts e="T378" id="Seg_1156" n="HIAT:w" s="T377">Araŋastaːktan</ts>
                  <nts id="Seg_1157" n="HIAT:ip">)</nts>
                  <nts id="Seg_1158" n="HIAT:ip">,</nts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1161" n="HIAT:w" s="T378">anɨ</ts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1164" n="HIAT:w" s="T379">bu͡ollagɨna</ts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1167" n="HIAT:w" s="T380">axranaga</ts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1170" n="HIAT:w" s="T381">axranaga</ts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1173" n="HIAT:w" s="T382">üleliːr</ts>
                  <nts id="Seg_1174" n="HIAT:ip">,</nts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1177" n="HIAT:w" s="T383">mʼilʼisʼijanɨ</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1180" n="HIAT:w" s="T384">büten</ts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1183" n="HIAT:w" s="T385">baran</ts>
                  <nts id="Seg_1184" n="HIAT:ip">.</nts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_1187" n="HIAT:u" s="T386">
                  <ts e="T387" id="Seg_1189" n="HIAT:w" s="T386">Urut</ts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1192" n="HIAT:w" s="T387">taba</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1195" n="HIAT:w" s="T388">ülehite</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1198" n="HIAT:w" s="T389">bu͡olbuta</ts>
                  <nts id="Seg_1199" n="HIAT:ip">.</nts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T400" id="Seg_1202" n="HIAT:u" s="T390">
                  <ts e="T391" id="Seg_1204" n="HIAT:w" s="T390">Onton</ts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1207" n="HIAT:w" s="T391">biːr</ts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1210" n="HIAT:w" s="T392">ogom</ts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1213" n="HIAT:w" s="T393">poːvarga</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1216" n="HIAT:w" s="T394">Nosku͡oga</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1219" n="HIAT:w" s="T395">üleliːr</ts>
                  <nts id="Seg_1220" n="HIAT:ip">,</nts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1223" n="HIAT:w" s="T396">glavnaj</ts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1226" n="HIAT:w" s="T397">poːvar</ts>
                  <nts id="Seg_1227" n="HIAT:ip">,</nts>
                  <nts id="Seg_1228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1230" n="HIAT:w" s="T398">biːr</ts>
                  <nts id="Seg_1231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1233" n="HIAT:w" s="T399">kɨːhɨm</ts>
                  <nts id="Seg_1234" n="HIAT:ip">.</nts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T406" id="Seg_1237" n="HIAT:u" s="T400">
                  <ts e="T401" id="Seg_1239" n="HIAT:w" s="T400">Biːr</ts>
                  <nts id="Seg_1240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1241" n="HIAT:ip">(</nts>
                  <ts e="T402" id="Seg_1243" n="HIAT:w" s="T401">kɨ-</ts>
                  <nts id="Seg_1244" n="HIAT:ip">)</nts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1247" n="HIAT:w" s="T402">tu͡ok</ts>
                  <nts id="Seg_1248" n="HIAT:ip">,</nts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1250" n="HIAT:ip">(</nts>
                  <ts e="T404" id="Seg_1252" n="HIAT:w" s="T403">Nas-</ts>
                  <nts id="Seg_1253" n="HIAT:ip">)</nts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1256" n="HIAT:w" s="T404">Nasku͡oga</ts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1259" n="HIAT:w" s="T405">Marʼina</ts>
                  <nts id="Seg_1260" n="HIAT:ip">.</nts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T467" id="Seg_1262" n="sc" s="T409">
               <ts e="T414" id="Seg_1264" n="HIAT:u" s="T409">
                  <nts id="Seg_1265" n="HIAT:ip">–</nts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1268" n="HIAT:w" s="T409">Hu͡o</ts>
                  <nts id="Seg_1269" n="HIAT:ip">,</nts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1272" n="HIAT:w" s="T410">ü͡öremmite</ts>
                  <nts id="Seg_1273" n="HIAT:ip">,</nts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1276" n="HIAT:w" s="T411">heː</ts>
                  <nts id="Seg_1277" n="HIAT:ip">,</nts>
                  <nts id="Seg_1278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1280" n="HIAT:w" s="T412">ol</ts>
                  <nts id="Seg_1281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1283" n="HIAT:w" s="T413">di͡et</ts>
                  <nts id="Seg_1284" n="HIAT:ip">.</nts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T416" id="Seg_1287" n="HIAT:u" s="T414">
                  <ts e="T415" id="Seg_1289" n="HIAT:w" s="T414">Krasnajarskajga</ts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1292" n="HIAT:w" s="T415">ü͡öremmite</ts>
                  <nts id="Seg_1293" n="HIAT:ip">.</nts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T430" id="Seg_1296" n="HIAT:u" s="T416">
                  <ts e="T417" id="Seg_1298" n="HIAT:w" s="T416">Onton</ts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1300" n="HIAT:ip">(</nts>
                  <ts e="T418" id="Seg_1302" n="HIAT:w" s="T417">ma-</ts>
                  <nts id="Seg_1303" n="HIAT:ip">)</nts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1306" n="HIAT:w" s="T418">du͡oktuːr</ts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1309" n="HIAT:w" s="T419">kɨːhɨm</ts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1312" n="HIAT:w" s="T420">Dudʼinkaga</ts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1315" n="HIAT:w" s="T421">üleliːr</ts>
                  <nts id="Seg_1316" n="HIAT:ip">,</nts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1319" n="HIAT:w" s="T422">tʼerapʼevka</ts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1322" n="HIAT:w" s="T423">üleliːr</ts>
                  <nts id="Seg_1323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1325" n="HIAT:w" s="T424">anɨ</ts>
                  <nts id="Seg_1326" n="HIAT:ip">,</nts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1329" n="HIAT:w" s="T425">üs</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1332" n="HIAT:w" s="T426">ogoloːk</ts>
                  <nts id="Seg_1333" n="HIAT:ip">,</nts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1336" n="HIAT:w" s="T427">kɨrdʼagas</ts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1339" n="HIAT:w" s="T428">dʼaktar</ts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1342" n="HIAT:w" s="T429">pʼensʼijalaːk</ts>
                  <nts id="Seg_1343" n="HIAT:ip">.</nts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T433" id="Seg_1346" n="HIAT:u" s="T430">
                  <ts e="T431" id="Seg_1348" n="HIAT:w" s="T430">Ogom</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1351" n="HIAT:w" s="T431">barɨta</ts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1354" n="HIAT:w" s="T432">pʼensʼijalaːk</ts>
                  <nts id="Seg_1355" n="HIAT:ip">.</nts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T436" id="Seg_1358" n="HIAT:u" s="T433">
                  <ts e="T434" id="Seg_1360" n="HIAT:w" s="T433">Üs</ts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1363" n="HIAT:w" s="T434">ogom</ts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1366" n="HIAT:w" s="T435">pʼensʼijalaːktar</ts>
                  <nts id="Seg_1367" n="HIAT:ip">.</nts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T450" id="Seg_1370" n="HIAT:u" s="T436">
                  <ts e="T437" id="Seg_1372" n="HIAT:w" s="T436">Ulakattarɨm</ts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1375" n="HIAT:w" s="T437">anɨ</ts>
                  <nts id="Seg_1376" n="HIAT:ip">,</nts>
                  <nts id="Seg_1377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1379" n="HIAT:w" s="T438">biːrgehe</ts>
                  <nts id="Seg_1380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1382" n="HIAT:w" s="T439">bu͡ollagɨna</ts>
                  <nts id="Seg_1383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1385" n="HIAT:w" s="T440">rɨbaktɨːr</ts>
                  <nts id="Seg_1386" n="HIAT:ip">,</nts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1389" n="HIAT:w" s="T441">biːrgehe</ts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1392" n="HIAT:w" s="T442">bu͡olla</ts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1395" n="HIAT:w" s="T443">anɨ</ts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1398" n="HIAT:w" s="T444">haːhɨrbɨt</ts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1401" n="HIAT:w" s="T445">pʼensʼijalaːk</ts>
                  <nts id="Seg_1402" n="HIAT:ip">,</nts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1405" n="HIAT:w" s="T446">Bi͡ere</ts>
                  <nts id="Seg_1406" n="HIAT:ip">,</nts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1409" n="HIAT:w" s="T447">ere</ts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1412" n="HIAT:w" s="T448">hubukaːn</ts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1415" n="HIAT:w" s="T449">ölbüte</ts>
                  <nts id="Seg_1416" n="HIAT:ip">.</nts>
                  <nts id="Seg_1417" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T454" id="Seg_1419" n="HIAT:u" s="T450">
                  <ts e="T451" id="Seg_1421" n="HIAT:w" s="T450">Ikki</ts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1424" n="HIAT:w" s="T451">ogoloːk</ts>
                  <nts id="Seg_1425" n="HIAT:ip">,</nts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1428" n="HIAT:w" s="T452">ühüstere</ts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1431" n="HIAT:w" s="T453">ölbüte</ts>
                  <nts id="Seg_1432" n="HIAT:ip">.</nts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T463" id="Seg_1435" n="HIAT:u" s="T454">
                  <ts e="T455" id="Seg_1437" n="HIAT:w" s="T454">Ulakan</ts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1440" n="HIAT:w" s="T455">u͡olum</ts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1443" n="HIAT:w" s="T456">üs</ts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1446" n="HIAT:w" s="T457">ogoloːk</ts>
                  <nts id="Seg_1447" n="HIAT:ip">,</nts>
                  <nts id="Seg_1448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1450" n="HIAT:w" s="T458">ogo</ts>
                  <nts id="Seg_1451" n="HIAT:ip">,</nts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1454" n="HIAT:w" s="T459">biːr</ts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1457" n="HIAT:w" s="T460">ogoto</ts>
                  <nts id="Seg_1458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1460" n="HIAT:w" s="T461">dʼaktardaːk</ts>
                  <nts id="Seg_1461" n="HIAT:ip">,</nts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1464" n="HIAT:w" s="T462">ogoloːk</ts>
                  <nts id="Seg_1465" n="HIAT:ip">.</nts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T467" id="Seg_1468" n="HIAT:u" s="T463">
                  <ts e="T464" id="Seg_1470" n="HIAT:w" s="T463">Aŋardara</ts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1473" n="HIAT:w" s="T464">ogolono</ts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1476" n="HIAT:w" s="T465">ilikter</ts>
                  <nts id="Seg_1477" n="HIAT:ip">.</nts>
                  <nts id="Seg_1478" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T494" id="Seg_1479" n="sc" s="T469">
               <ts e="T471" id="Seg_1481" n="HIAT:u" s="T469">
                  <nts id="Seg_1482" n="HIAT:ip">–</nts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1485" n="HIAT:w" s="T469">Heː</ts>
                  <nts id="Seg_1486" n="HIAT:ip">.</nts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T479" id="Seg_1489" n="HIAT:u" s="T471">
                  <ts e="T472" id="Seg_1491" n="HIAT:w" s="T471">Onton</ts>
                  <nts id="Seg_1492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1494" n="HIAT:w" s="T472">biːr</ts>
                  <nts id="Seg_1495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1497" n="HIAT:w" s="T473">ogom</ts>
                  <nts id="Seg_1498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1500" n="HIAT:w" s="T474">ke</ts>
                  <nts id="Seg_1501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1503" n="HIAT:w" s="T475">kimi͡eke</ts>
                  <nts id="Seg_1504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1506" n="HIAT:w" s="T476">baːr</ts>
                  <nts id="Seg_1507" n="HIAT:ip">,</nts>
                  <nts id="Seg_1508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1510" n="HIAT:w" s="T477">Hoːpstajga</ts>
                  <nts id="Seg_1511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1513" n="HIAT:w" s="T478">baːr</ts>
                  <nts id="Seg_1514" n="HIAT:ip">.</nts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T482" id="Seg_1517" n="HIAT:u" s="T479">
                  <ts e="T480" id="Seg_1519" n="HIAT:w" s="T479">Kimi͡eke</ts>
                  <nts id="Seg_1520" n="HIAT:ip">,</nts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1523" n="HIAT:w" s="T480">vaspʼitatʼelga</ts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1526" n="HIAT:w" s="T481">üleliːr</ts>
                  <nts id="Seg_1527" n="HIAT:ip">.</nts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T490" id="Seg_1530" n="HIAT:u" s="T482">
                  <ts e="T483" id="Seg_1532" n="HIAT:w" s="T482">Onton</ts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1535" n="HIAT:w" s="T483">biːr</ts>
                  <nts id="Seg_1536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1538" n="HIAT:w" s="T484">ogom</ts>
                  <nts id="Seg_1539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1541" n="HIAT:w" s="T485">munna</ts>
                  <nts id="Seg_1542" n="HIAT:ip">,</nts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1545" n="HIAT:w" s="T486">biːr</ts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1548" n="HIAT:w" s="T487">kɨːhɨm</ts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1551" n="HIAT:w" s="T488">munna</ts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1554" n="HIAT:w" s="T489">vaspʼitatʼeliːr</ts>
                  <nts id="Seg_1555" n="HIAT:ip">.</nts>
                  <nts id="Seg_1556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T494" id="Seg_1558" n="HIAT:u" s="T490">
                  <ts e="T491" id="Seg_1560" n="HIAT:w" s="T490">Biːr</ts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1563" n="HIAT:w" s="T491">ogom</ts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1566" n="HIAT:w" s="T492">du͡o</ts>
                  <nts id="Seg_1567" n="HIAT:ip">…</nts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T501" id="Seg_1569" n="sc" s="T496">
               <ts e="T501" id="Seg_1571" n="HIAT:u" s="T496">
                  <nts id="Seg_1572" n="HIAT:ip">–</nts>
                  <nts id="Seg_1573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1575" n="HIAT:w" s="T496">Heː</ts>
                  <nts id="Seg_1576" n="HIAT:ip">,</nts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1579" n="HIAT:w" s="T497">sku͡olaga</ts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1582" n="HIAT:w" s="T498">munna</ts>
                  <nts id="Seg_1583" n="HIAT:ip">,</nts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1586" n="HIAT:w" s="T499">Du͡omna</ts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1589" n="HIAT:w" s="T500">Pʼetrovna</ts>
                  <nts id="Seg_1590" n="HIAT:ip">.</nts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T667" id="Seg_1592" n="sc" s="T506">
               <ts e="T519" id="Seg_1594" n="HIAT:u" s="T506">
                  <nts id="Seg_1595" n="HIAT:ip">–</nts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1598" n="HIAT:w" s="T506">Eː</ts>
                  <nts id="Seg_1599" n="HIAT:ip">,</nts>
                  <nts id="Seg_1600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1602" n="HIAT:w" s="T508">onton</ts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1605" n="HIAT:w" s="T509">biːr</ts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1608" n="HIAT:w" s="T510">ɨlgɨn</ts>
                  <nts id="Seg_1609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1611" n="HIAT:w" s="T511">u͡olum</ts>
                  <nts id="Seg_1612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1614" n="HIAT:w" s="T512">dʼaktara</ts>
                  <nts id="Seg_1615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1617" n="HIAT:w" s="T513">emi͡e</ts>
                  <nts id="Seg_1618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1620" n="HIAT:w" s="T514">munna</ts>
                  <nts id="Seg_1621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1623" n="HIAT:w" s="T515">učitallɨːr</ts>
                  <nts id="Seg_1624" n="HIAT:ip">,</nts>
                  <nts id="Seg_1625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1627" n="HIAT:w" s="T516">Naːdʼa</ts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1630" n="HIAT:w" s="T517">Kʼirgʼizava</ts>
                  <nts id="Seg_1631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1633" n="HIAT:w" s="T518">i͡e</ts>
                  <nts id="Seg_1634" n="HIAT:ip">.</nts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T520" id="Seg_1637" n="HIAT:u" s="T519">
                  <ts e="T520" id="Seg_1639" n="HIAT:w" s="T519">Nʼevʼestka</ts>
                  <nts id="Seg_1640" n="HIAT:ip">.</nts>
                  <nts id="Seg_1641" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T530" id="Seg_1643" n="HIAT:u" s="T520">
                  <ts e="T521" id="Seg_1645" n="HIAT:w" s="T520">Onton</ts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1648" n="HIAT:w" s="T521">biːr</ts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1651" n="HIAT:w" s="T522">u͡olum</ts>
                  <nts id="Seg_1652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1654" n="HIAT:w" s="T523">bu͡ollagɨna</ts>
                  <nts id="Seg_1655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1657" n="HIAT:w" s="T524">kim</ts>
                  <nts id="Seg_1658" n="HIAT:ip">,</nts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1661" n="HIAT:w" s="T525">bʼezdʼexotčʼik</ts>
                  <nts id="Seg_1662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1664" n="HIAT:w" s="T526">büppüte</ts>
                  <nts id="Seg_1665" n="HIAT:ip">,</nts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1668" n="HIAT:w" s="T527">ü͡örenen</ts>
                  <nts id="Seg_1669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1671" n="HIAT:w" s="T528">üleliːr</ts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1674" n="HIAT:w" s="T529">ete</ts>
                  <nts id="Seg_1675" n="HIAT:ip">.</nts>
                  <nts id="Seg_1676" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T531" id="Seg_1678" n="HIAT:u" s="T530">
                  <ts e="T531" id="Seg_1680" n="HIAT:w" s="T530">Iːgireler</ts>
                  <nts id="Seg_1681" n="HIAT:ip">.</nts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T541" id="Seg_1684" n="HIAT:u" s="T531">
                  <ts e="T532" id="Seg_1686" n="HIAT:w" s="T531">Biːrgehe</ts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1689" n="HIAT:w" s="T532">elʼektrʼik</ts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1692" n="HIAT:w" s="T533">iti</ts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1695" n="HIAT:w" s="T534">üleliː</ts>
                  <nts id="Seg_1696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1698" n="HIAT:w" s="T535">hɨldʼar</ts>
                  <nts id="Seg_1699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1701" n="HIAT:w" s="T536">anɨga</ts>
                  <nts id="Seg_1702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1704" n="HIAT:w" s="T537">di͡eri</ts>
                  <nts id="Seg_1705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1707" n="HIAT:w" s="T538">ü͡örenen</ts>
                  <nts id="Seg_1708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1710" n="HIAT:w" s="T539">büten</ts>
                  <nts id="Seg_1711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1713" n="HIAT:w" s="T540">baran</ts>
                  <nts id="Seg_1714" n="HIAT:ip">.</nts>
                  <nts id="Seg_1715" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T545" id="Seg_1717" n="HIAT:u" s="T541">
                  <ts e="T542" id="Seg_1719" n="HIAT:w" s="T541">Tak</ts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1722" n="HIAT:w" s="T542">da</ts>
                  <nts id="Seg_1723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_1725" n="HIAT:w" s="T543">barɨta</ts>
                  <nts id="Seg_1726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1728" n="HIAT:w" s="T544">ü͡öremmittere</ts>
                  <nts id="Seg_1729" n="HIAT:ip">.</nts>
                  <nts id="Seg_1730" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T551" id="Seg_1732" n="HIAT:u" s="T545">
                  <ts e="T546" id="Seg_1734" n="HIAT:w" s="T545">Onton</ts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_1737" n="HIAT:w" s="T546">ke</ts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1740" n="HIAT:w" s="T547">biːr</ts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1743" n="HIAT:w" s="T548">ogom</ts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_1746" n="HIAT:w" s="T549">Kiri͡eska</ts>
                  <nts id="Seg_1747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_1749" n="HIAT:w" s="T550">baːr</ts>
                  <nts id="Seg_1750" n="HIAT:ip">.</nts>
                  <nts id="Seg_1751" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T566" id="Seg_1753" n="HIAT:u" s="T551">
                  <nts id="Seg_1754" n="HIAT:ip">(</nts>
                  <ts e="T552" id="Seg_1756" n="HIAT:w" s="T551">Han-</ts>
                  <nts id="Seg_1757" n="HIAT:ip">)</nts>
                  <nts id="Seg_1758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_1760" n="HIAT:w" s="T552">eː</ts>
                  <nts id="Seg_1761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_1763" n="HIAT:w" s="T553">mʼilʼisʼije</ts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_1766" n="HIAT:w" s="T554">ü͡öregin</ts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_1769" n="HIAT:w" s="T555">büppüt</ts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_1772" n="HIAT:w" s="T556">ete</ts>
                  <nts id="Seg_1773" n="HIAT:ip">,</nts>
                  <nts id="Seg_1774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_1776" n="HIAT:w" s="T557">ontuŋ</ts>
                  <nts id="Seg_1777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_1779" n="HIAT:w" s="T558">hir</ts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_1782" n="HIAT:w" s="T559">aːjɨ</ts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_1785" n="HIAT:w" s="T560">hiri</ts>
                  <nts id="Seg_1786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_1788" n="HIAT:w" s="T561">baratan</ts>
                  <nts id="Seg_1789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_1791" n="HIAT:w" s="T562">baratan</ts>
                  <nts id="Seg_1792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_1794" n="HIAT:w" s="T563">baran</ts>
                  <nts id="Seg_1795" n="HIAT:ip">,</nts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_1798" n="HIAT:w" s="T564">Kiri͡eske</ts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_1801" n="HIAT:w" s="T565">dʼaktardaːk</ts>
                  <nts id="Seg_1802" n="HIAT:ip">.</nts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T567" id="Seg_1805" n="HIAT:u" s="T566">
                  <ts e="T567" id="Seg_1807" n="HIAT:w" s="T566">Alʼeksʼej</ts>
                  <nts id="Seg_1808" n="HIAT:ip">.</nts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T571" id="Seg_1811" n="HIAT:u" s="T567">
                  <ts e="T568" id="Seg_1813" n="HIAT:w" s="T567">Kördüŋ</ts>
                  <nts id="Seg_1814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_1816" n="HIAT:w" s="T568">itte</ts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_1819" n="HIAT:w" s="T569">eni</ts>
                  <nts id="Seg_1820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_1822" n="HIAT:w" s="T570">araːha</ts>
                  <nts id="Seg_1823" n="HIAT:ip">.</nts>
                  <nts id="Seg_1824" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T578" id="Seg_1826" n="HIAT:u" s="T571">
                  <ts e="T572" id="Seg_1828" n="HIAT:w" s="T571">Onton</ts>
                  <nts id="Seg_1829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_1831" n="HIAT:w" s="T572">aŋardara</ts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_1834" n="HIAT:w" s="T573">bulčuttar</ts>
                  <nts id="Seg_1835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_1837" n="HIAT:w" s="T574">üs</ts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_1840" n="HIAT:w" s="T575">u͡olum</ts>
                  <nts id="Seg_1841" n="HIAT:ip">,</nts>
                  <nts id="Seg_1842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_1844" n="HIAT:w" s="T576">bulčuttar</ts>
                  <nts id="Seg_1845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_1847" n="HIAT:w" s="T577">bu͡olla</ts>
                  <nts id="Seg_1848" n="HIAT:ip">.</nts>
                  <nts id="Seg_1849" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T580" id="Seg_1851" n="HIAT:u" s="T578">
                  <ts e="T579" id="Seg_1853" n="HIAT:w" s="T578">Ölü͡öse</ts>
                  <nts id="Seg_1854" n="HIAT:ip">,</nts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_1857" n="HIAT:w" s="T579">Hergeːj</ts>
                  <nts id="Seg_1858" n="HIAT:ip">.</nts>
                  <nts id="Seg_1859" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T582" id="Seg_1861" n="HIAT:u" s="T580">
                  <ts e="T581" id="Seg_1863" n="HIAT:w" s="T580">Miːse</ts>
                  <nts id="Seg_1864" n="HIAT:ip">,</nts>
                  <nts id="Seg_1865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_1867" n="HIAT:w" s="T581">Biːtʼe</ts>
                  <nts id="Seg_1868" n="HIAT:ip">.</nts>
                  <nts id="Seg_1869" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T588" id="Seg_1871" n="HIAT:u" s="T582">
                  <ts e="T583" id="Seg_1873" n="HIAT:w" s="T582">Biːtʼe</ts>
                  <nts id="Seg_1874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_1876" n="HIAT:w" s="T583">utuja</ts>
                  <nts id="Seg_1877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_1879" n="HIAT:w" s="T584">hɨtar</ts>
                  <nts id="Seg_1880" n="HIAT:ip">,</nts>
                  <nts id="Seg_1881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_1883" n="HIAT:w" s="T585">dʼaktara</ts>
                  <nts id="Seg_1884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_1886" n="HIAT:w" s="T586">bu</ts>
                  <nts id="Seg_1887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_1889" n="HIAT:w" s="T587">turar</ts>
                  <nts id="Seg_1890" n="HIAT:ip">.</nts>
                  <nts id="Seg_1891" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T591" id="Seg_1893" n="HIAT:u" s="T588">
                  <ts e="T589" id="Seg_1895" n="HIAT:w" s="T588">Elete</ts>
                  <nts id="Seg_1896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_1898" n="HIAT:w" s="T589">du͡o</ts>
                  <nts id="Seg_1899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_1901" n="HIAT:w" s="T590">onton</ts>
                  <nts id="Seg_1902" n="HIAT:ip">.</nts>
                  <nts id="Seg_1903" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T600" id="Seg_1905" n="HIAT:u" s="T591">
                  <ts e="T592" id="Seg_1907" n="HIAT:w" s="T591">U͡on</ts>
                  <nts id="Seg_1908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_1910" n="HIAT:w" s="T592">ikki</ts>
                  <nts id="Seg_1911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_1913" n="HIAT:w" s="T593">ogoloːkpun</ts>
                  <nts id="Seg_1914" n="HIAT:ip">,</nts>
                  <nts id="Seg_1915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_1917" n="HIAT:w" s="T594">hürbe</ts>
                  <nts id="Seg_1918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_1920" n="HIAT:w" s="T595">orduga</ts>
                  <nts id="Seg_1921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_1923" n="HIAT:w" s="T596">bi͡es</ts>
                  <nts id="Seg_1924" n="HIAT:ip">,</nts>
                  <nts id="Seg_1925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_1927" n="HIAT:w" s="T597">eː</ts>
                  <nts id="Seg_1928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_1930" n="HIAT:w" s="T598">hette</ts>
                  <nts id="Seg_1931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_1933" n="HIAT:w" s="T599">bɨnuktaːkpɨn</ts>
                  <nts id="Seg_1934" n="HIAT:ip">.</nts>
                  <nts id="Seg_1935" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T606" id="Seg_1937" n="HIAT:u" s="T600">
                  <ts e="T601" id="Seg_1939" n="HIAT:w" s="T600">Oččogo</ts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_1942" n="HIAT:w" s="T601">dʼirʼevnʼijabit</ts>
                  <nts id="Seg_1943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_1945" n="HIAT:w" s="T602">innʼe</ts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_1948" n="HIAT:w" s="T603">tü͡örd-u͡ontan</ts>
                  <nts id="Seg_1949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_1951" n="HIAT:w" s="T604">taksa</ts>
                  <nts id="Seg_1952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_1954" n="HIAT:w" s="T605">kihi</ts>
                  <nts id="Seg_1955" n="HIAT:ip">.</nts>
                  <nts id="Seg_1956" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T610" id="Seg_1958" n="HIAT:u" s="T606">
                  <ts e="T607" id="Seg_1960" n="HIAT:w" s="T606">Kiniːtterbin</ts>
                  <nts id="Seg_1961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_1963" n="HIAT:w" s="T607">barɨtɨn</ts>
                  <nts id="Seg_1964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_1966" n="HIAT:w" s="T608">bullakpɨna</ts>
                  <nts id="Seg_1967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_1969" n="HIAT:w" s="T609">elbekter</ts>
                  <nts id="Seg_1970" n="HIAT:ip">.</nts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T611" id="Seg_1973" n="HIAT:u" s="T610">
                  <ts e="T611" id="Seg_1975" n="HIAT:w" s="T610">Aːktakpɨna</ts>
                  <nts id="Seg_1976" n="HIAT:ip">.</nts>
                  <nts id="Seg_1977" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T616" id="Seg_1979" n="HIAT:u" s="T611">
                  <ts e="T612" id="Seg_1981" n="HIAT:w" s="T611">Biːr</ts>
                  <nts id="Seg_1982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_1984" n="HIAT:w" s="T612">kiniːt</ts>
                  <nts id="Seg_1985" n="HIAT:ip">,</nts>
                  <nts id="Seg_1986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_1988" n="HIAT:w" s="T613">ikki</ts>
                  <nts id="Seg_1989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_1991" n="HIAT:w" s="T614">kiniːt</ts>
                  <nts id="Seg_1992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_1994" n="HIAT:w" s="T615">pʼedagoːgtar</ts>
                  <nts id="Seg_1995" n="HIAT:ip">.</nts>
                  <nts id="Seg_1996" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T618" id="Seg_1998" n="HIAT:u" s="T616">
                  <ts e="T617" id="Seg_2000" n="HIAT:w" s="T616">Ogolordoːktor</ts>
                  <nts id="Seg_2001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2003" n="HIAT:w" s="T617">barɨlara</ts>
                  <nts id="Seg_2004" n="HIAT:ip">.</nts>
                  <nts id="Seg_2005" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T621" id="Seg_2007" n="HIAT:u" s="T618">
                  <ts e="T619" id="Seg_2009" n="HIAT:w" s="T618">Kɨrgɨttara</ts>
                  <nts id="Seg_2010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2012" n="HIAT:w" s="T619">ü͡örene</ts>
                  <nts id="Seg_2013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2015" n="HIAT:w" s="T620">barbɨttara</ts>
                  <nts id="Seg_2016" n="HIAT:ip">.</nts>
                  <nts id="Seg_2017" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T624" id="Seg_2019" n="HIAT:u" s="T621">
                  <ts e="T622" id="Seg_2021" n="HIAT:w" s="T621">Biːr</ts>
                  <nts id="Seg_2022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2024" n="HIAT:w" s="T622">kiniːtim</ts>
                  <nts id="Seg_2025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2027" n="HIAT:w" s="T623">saːdʼikka</ts>
                  <nts id="Seg_2028" n="HIAT:ip">.</nts>
                  <nts id="Seg_2029" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T629" id="Seg_2031" n="HIAT:u" s="T624">
                  <ts e="T625" id="Seg_2033" n="HIAT:w" s="T624">Kimnere</ts>
                  <nts id="Seg_2034" n="HIAT:ip">,</nts>
                  <nts id="Seg_2035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2037" n="HIAT:w" s="T625">kim</ts>
                  <nts id="Seg_2038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2040" n="HIAT:w" s="T626">etej</ts>
                  <nts id="Seg_2041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2043" n="HIAT:w" s="T627">ke</ts>
                  <nts id="Seg_2044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2046" n="HIAT:w" s="T628">dogo</ts>
                  <nts id="Seg_2047" n="HIAT:ip">?</nts>
                  <nts id="Seg_2048" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T630" id="Seg_2050" n="HIAT:u" s="T629">
                  <ts e="T630" id="Seg_2052" n="HIAT:w" s="T629">Zabʼedusajdar</ts>
                  <nts id="Seg_2053" n="HIAT:ip">.</nts>
                  <nts id="Seg_2054" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T634" id="Seg_2056" n="HIAT:u" s="T630">
                  <ts e="T631" id="Seg_2058" n="HIAT:w" s="T630">Ikki</ts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2061" n="HIAT:w" s="T631">kiniːtim</ts>
                  <nts id="Seg_2062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2064" n="HIAT:w" s="T632">učiːtallar</ts>
                  <nts id="Seg_2065" n="HIAT:ip">,</nts>
                  <nts id="Seg_2066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2068" n="HIAT:w" s="T633">pʼedagogtar</ts>
                  <nts id="Seg_2069" n="HIAT:ip">.</nts>
                  <nts id="Seg_2070" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T636" id="Seg_2072" n="HIAT:u" s="T634">
                  <ts e="T635" id="Seg_2074" n="HIAT:w" s="T634">Barɨta</ts>
                  <nts id="Seg_2075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2077" n="HIAT:w" s="T635">ülehitter</ts>
                  <nts id="Seg_2078" n="HIAT:ip">.</nts>
                  <nts id="Seg_2079" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T646" id="Seg_2081" n="HIAT:u" s="T636">
                  <ts e="T637" id="Seg_2083" n="HIAT:w" s="T636">Onton</ts>
                  <nts id="Seg_2084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2086" n="HIAT:w" s="T637">biːr</ts>
                  <nts id="Seg_2087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2089" n="HIAT:w" s="T638">kütü͡ötüm</ts>
                  <nts id="Seg_2090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2092" n="HIAT:w" s="T639">iti</ts>
                  <nts id="Seg_2093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2095" n="HIAT:w" s="T640">taŋaralaːta</ts>
                  <nts id="Seg_2096" n="HIAT:ip">,</nts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2099" n="HIAT:w" s="T641">biːr</ts>
                  <nts id="Seg_2100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2102" n="HIAT:w" s="T642">kütü͡ötüm</ts>
                  <nts id="Seg_2103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2105" n="HIAT:w" s="T643">ɨ͡arɨj</ts>
                  <nts id="Seg_2106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2108" n="HIAT:w" s="T644">bu͡olbut</ts>
                  <nts id="Seg_2109" n="HIAT:ip">,</nts>
                  <nts id="Seg_2110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2112" n="HIAT:w" s="T645">ogo</ts>
                  <nts id="Seg_2113" n="HIAT:ip">.</nts>
                  <nts id="Seg_2114" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T649" id="Seg_2116" n="HIAT:u" s="T646">
                  <ts e="T647" id="Seg_2118" n="HIAT:w" s="T646">Domna</ts>
                  <nts id="Seg_2119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2121" n="HIAT:w" s="T647">Pʼetroːvna</ts>
                  <nts id="Seg_2122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2124" n="HIAT:w" s="T648">gi͡ene</ts>
                  <nts id="Seg_2125" n="HIAT:ip">.</nts>
                  <nts id="Seg_2126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T657" id="Seg_2128" n="HIAT:u" s="T649">
                  <ts e="T650" id="Seg_2130" n="HIAT:w" s="T649">Ol</ts>
                  <nts id="Seg_2131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2133" n="HIAT:w" s="T650">ereːti</ts>
                  <nts id="Seg_2134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2136" n="HIAT:w" s="T651">buldu</ts>
                  <nts id="Seg_2137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2139" n="HIAT:w" s="T652">delbi</ts>
                  <nts id="Seg_2140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2142" n="HIAT:w" s="T653">ölörör</ts>
                  <nts id="Seg_2143" n="HIAT:ip">,</nts>
                  <nts id="Seg_2144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2146" n="HIAT:w" s="T654">ataga</ts>
                  <nts id="Seg_2147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2149" n="HIAT:w" s="T655">hu͡ok</ts>
                  <nts id="Seg_2150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2152" n="HIAT:w" s="T656">bejete</ts>
                  <nts id="Seg_2153" n="HIAT:ip">.</nts>
                  <nts id="Seg_2154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T661" id="Seg_2156" n="HIAT:u" s="T657">
                  <ts e="T658" id="Seg_2158" n="HIAT:w" s="T657">Biːr</ts>
                  <nts id="Seg_2159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2161" n="HIAT:w" s="T658">kütü͡ötüm</ts>
                  <nts id="Seg_2162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2164" n="HIAT:w" s="T659">Hu͡opostajga</ts>
                  <nts id="Seg_2165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2167" n="HIAT:w" s="T660">axotnʼik</ts>
                  <nts id="Seg_2168" n="HIAT:ip">.</nts>
                  <nts id="Seg_2169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T664" id="Seg_2171" n="HIAT:u" s="T661">
                  <ts e="T662" id="Seg_2173" n="HIAT:w" s="T661">Kɨːha</ts>
                  <nts id="Seg_2174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2176" n="HIAT:w" s="T662">ü͡örene</ts>
                  <nts id="Seg_2177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2179" n="HIAT:w" s="T663">barbɨt</ts>
                  <nts id="Seg_2180" n="HIAT:ip">.</nts>
                  <nts id="Seg_2181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T667" id="Seg_2183" n="HIAT:u" s="T664">
                  <ts e="T665" id="Seg_2185" n="HIAT:w" s="T664">Dʼe</ts>
                  <nts id="Seg_2186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2188" n="HIAT:w" s="T665">tugu</ts>
                  <nts id="Seg_2189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2191" n="HIAT:w" s="T666">kepsi͡eppinij</ts>
                  <nts id="Seg_2192" n="HIAT:ip">?</nts>
                  <nts id="Seg_2193" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T671" id="Seg_2194" n="sc" s="T670">
               <ts e="T671" id="Seg_2196" n="HIAT:u" s="T670">
                  <nts id="Seg_2197" n="HIAT:ip">–</nts>
                  <nts id="Seg_2198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2200" n="HIAT:w" s="T670">Dʼe</ts>
                  <nts id="Seg_2201" n="HIAT:ip">.</nts>
                  <nts id="Seg_2202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KiPP">
            <ts e="T4" id="Seg_2203" n="sc" s="T1">
               <ts e="T2" id="Seg_2205" n="e" s="T1">– Muntan </ts>
               <ts e="T3" id="Seg_2207" n="e" s="T2">isteːčči </ts>
               <ts e="T4" id="Seg_2209" n="e" s="T3">du͡o? </ts>
            </ts>
            <ts e="T9" id="Seg_2210" n="sc" s="T6">
               <ts e="T9" id="Seg_2212" n="e" s="T6">– Heː. </ts>
            </ts>
            <ts e="T25" id="Seg_2213" n="sc" s="T14">
               <ts e="T15" id="Seg_2215" n="e" s="T14">– Dʼe </ts>
               <ts e="T17" id="Seg_2217" n="e" s="T15">tugu </ts>
               <ts e="T18" id="Seg_2219" n="e" s="T17">ɨjɨtar </ts>
               <ts e="T19" id="Seg_2221" n="e" s="T18">ke </ts>
               <ts e="T20" id="Seg_2223" n="e" s="T19">do? </ts>
               <ts e="T21" id="Seg_2225" n="e" s="T20">Maŋnaj </ts>
               <ts e="T22" id="Seg_2227" n="e" s="T21">lʼuboppɨn </ts>
               <ts e="T24" id="Seg_2229" n="e" s="T22">ɨjɨtalɨːr </ts>
               <ts e="T25" id="Seg_2231" n="e" s="T24">du͡o. </ts>
            </ts>
            <ts e="T33" id="Seg_2232" n="sc" s="T27">
               <ts e="T28" id="Seg_2234" n="e" s="T27">– (I-) </ts>
               <ts e="T29" id="Seg_2236" n="e" s="T28">((LAUGH)) </ts>
               <ts e="T30" id="Seg_2238" n="e" s="T29">ister </ts>
               <ts e="T31" id="Seg_2240" n="e" s="T30">((LAUGH)). </ts>
               <ts e="T33" id="Seg_2242" n="e" s="T31">Eː. </ts>
            </ts>
            <ts e="T62" id="Seg_2243" n="sc" s="T49">
               <ts e="T51" id="Seg_2245" n="e" s="T49">– Jeːrakata. </ts>
               <ts e="T52" id="Seg_2247" n="e" s="T51">Kačča </ts>
               <ts e="T53" id="Seg_2249" n="e" s="T52">bütü͡ömüj </ts>
               <ts e="T54" id="Seg_2251" n="e" s="T53">ol </ts>
               <ts e="T55" id="Seg_2253" n="e" s="T54">ki͡ehege </ts>
               <ts e="T56" id="Seg_2255" n="e" s="T55">da </ts>
               <ts e="T57" id="Seg_2257" n="e" s="T56">di͡eri. </ts>
               <ts e="T62" id="Seg_2259" n="e" s="T57">((LAUGH)). </ts>
            </ts>
            <ts e="T68" id="Seg_2260" n="sc" s="T66">
               <ts e="T68" id="Seg_2262" n="e" s="T66">– Korgoː. </ts>
            </ts>
            <ts e="T81" id="Seg_2263" n="sc" s="T70">
               <ts e="T72" id="Seg_2265" n="e" s="T70">– Korgoːgo </ts>
               <ts e="T73" id="Seg_2267" n="e" s="T72">töröːbütüm. </ts>
               <ts e="T74" id="Seg_2269" n="e" s="T73">Hubunna, </ts>
               <ts e="T75" id="Seg_2271" n="e" s="T74">bejem </ts>
               <ts e="T76" id="Seg_2273" n="e" s="T75">hirber. </ts>
               <ts e="T77" id="Seg_2275" n="e" s="T76">Bulunnʼabɨn. </ts>
               <ts e="T78" id="Seg_2277" n="e" s="T77">Bilegin, </ts>
               <ts e="T79" id="Seg_2279" n="e" s="T78">bulunnʼa </ts>
               <ts e="T80" id="Seg_2281" n="e" s="T79">aːtɨn </ts>
               <ts e="T81" id="Seg_2283" n="e" s="T80">((LAUGH))? </ts>
            </ts>
            <ts e="T92" id="Seg_2284" n="sc" s="T89">
               <ts e="T90" id="Seg_2286" n="e" s="T89">– Bʼez </ts>
               <ts e="T91" id="Seg_2288" n="e" s="T90">atca </ts>
               <ts e="T92" id="Seg_2290" n="e" s="T91">((LAUGH)). </ts>
            </ts>
            <ts e="T108" id="Seg_2291" n="sc" s="T106">
               <ts e="T107" id="Seg_2293" n="e" s="T106">– Teːtete </ts>
               <ts e="T108" id="Seg_2295" n="e" s="T107">hu͡ok. </ts>
            </ts>
            <ts e="T117" id="Seg_2296" n="sc" s="T111">
               <ts e="T112" id="Seg_2298" n="e" s="T111">– Dʼe </ts>
               <ts e="T113" id="Seg_2300" n="e" s="T112">min </ts>
               <ts e="T115" id="Seg_2302" n="e" s="T113">baːbuskam </ts>
               <ts e="T117" id="Seg_2304" n="e" s="T115">iːtte. </ts>
            </ts>
            <ts e="T124" id="Seg_2305" n="sc" s="T120">
               <ts e="T121" id="Seg_2307" n="e" s="T120">– Baːbuska </ts>
               <ts e="T122" id="Seg_2309" n="e" s="T121">vaspʼitɨvajdaːbɨta, </ts>
               <ts e="T123" id="Seg_2311" n="e" s="T122">maːmam </ts>
               <ts e="T124" id="Seg_2313" n="e" s="T123">maːmata. </ts>
            </ts>
            <ts e="T254" id="Seg_2314" n="sc" s="T128">
               <ts e="T129" id="Seg_2316" n="e" s="T128">– Onton </ts>
               <ts e="T130" id="Seg_2318" n="e" s="T129">bihigi </ts>
               <ts e="T131" id="Seg_2320" n="e" s="T130">kergetter </ts>
               <ts e="T132" id="Seg_2322" n="e" s="T131">barɨta </ts>
               <ts e="T133" id="Seg_2324" n="e" s="T132">ölönnör, </ts>
               <ts e="T134" id="Seg_2326" n="e" s="T133">tulaːjak </ts>
               <ts e="T135" id="Seg_2328" n="e" s="T134">hɨldʼɨbɨppɨt </ts>
               <ts e="T136" id="Seg_2330" n="e" s="T135">emeːksini </ts>
               <ts e="T137" id="Seg_2332" n="e" s="T136">gɨtta </ts>
               <ts e="T138" id="Seg_2334" n="e" s="T137">baːbuskabɨn </ts>
               <ts e="T139" id="Seg_2336" n="e" s="T138">gɨtta. </ts>
               <ts e="T140" id="Seg_2338" n="e" s="T139">Onton </ts>
               <ts e="T141" id="Seg_2340" n="e" s="T140">(Rɨb-) </ts>
               <ts e="T142" id="Seg_2342" n="e" s="T141">Rɨbnaj </ts>
               <ts e="T143" id="Seg_2344" n="e" s="T142">di͡ek </ts>
               <ts e="T144" id="Seg_2346" n="e" s="T143">barbɨtɨm. </ts>
               <ts e="T145" id="Seg_2348" n="e" s="T144">Onton </ts>
               <ts e="T146" id="Seg_2350" n="e" s="T145">ol </ts>
               <ts e="T147" id="Seg_2352" n="e" s="T146">barbɨppɨt </ts>
               <ts e="T148" id="Seg_2354" n="e" s="T147">(i-) </ts>
               <ts e="T149" id="Seg_2356" n="e" s="T148">ebebin, </ts>
               <ts e="T150" id="Seg_2358" n="e" s="T149">inʼebin </ts>
               <ts e="T151" id="Seg_2360" n="e" s="T150">gɨtta, </ts>
               <ts e="T152" id="Seg_2362" n="e" s="T151">inʼe </ts>
               <ts e="T153" id="Seg_2364" n="e" s="T152">di͡eččibin </ts>
               <ts e="T154" id="Seg_2366" n="e" s="T153">ontubun, </ts>
               <ts e="T155" id="Seg_2368" n="e" s="T154">maːmam </ts>
               <ts e="T156" id="Seg_2370" n="e" s="T155">maːmam </ts>
               <ts e="T157" id="Seg_2372" n="e" s="T156">hin </ts>
               <ts e="T158" id="Seg_2374" n="e" s="T157">biːr. </ts>
               <ts e="T159" id="Seg_2376" n="e" s="T158">Dʼe, </ts>
               <ts e="T160" id="Seg_2378" n="e" s="T159">onton </ts>
               <ts e="T161" id="Seg_2380" n="e" s="T160">töttörü </ts>
               <ts e="T162" id="Seg_2382" n="e" s="T161">kelbippit, </ts>
               <ts e="T163" id="Seg_2384" n="e" s="T162">maːmam, </ts>
               <ts e="T164" id="Seg_2386" n="e" s="T163">emeːksin </ts>
               <ts e="T165" id="Seg_2388" n="e" s="T164">(baː-) </ts>
               <ts e="T166" id="Seg_2390" n="e" s="T165">baːbuskam </ts>
               <ts e="T167" id="Seg_2392" n="e" s="T166">ölörüger. </ts>
               <ts e="T168" id="Seg_2394" n="e" s="T167">Ölön </ts>
               <ts e="T169" id="Seg_2396" n="e" s="T168">baran </ts>
               <ts e="T170" id="Seg_2398" n="e" s="T169">tillibite, </ts>
               <ts e="T171" id="Seg_2400" n="e" s="T170">ol </ts>
               <ts e="T172" id="Seg_2402" n="e" s="T171">dojduga </ts>
               <ts e="T173" id="Seg_2404" n="e" s="T172">onton </ts>
               <ts e="T174" id="Seg_2406" n="e" s="T173">töttörü </ts>
               <ts e="T175" id="Seg_2408" n="e" s="T174">kelbippit. </ts>
               <ts e="T176" id="Seg_2410" n="e" s="T175">Ol </ts>
               <ts e="T177" id="Seg_2412" n="e" s="T176">kelbippit </ts>
               <ts e="T178" id="Seg_2414" n="e" s="T177">kenne </ts>
               <ts e="T179" id="Seg_2416" n="e" s="T178">u͡on </ts>
               <ts e="T180" id="Seg_2418" n="e" s="T179">alta </ts>
               <ts e="T181" id="Seg_2420" n="e" s="T180">dʼɨllaːkpar </ts>
               <ts e="T182" id="Seg_2422" n="e" s="T181">minigin </ts>
               <ts e="T183" id="Seg_2424" n="e" s="T182">erge </ts>
               <ts e="T184" id="Seg_2426" n="e" s="T183">bi͡erbite. </ts>
               <ts e="T185" id="Seg_2428" n="e" s="T184">"Min </ts>
               <ts e="T186" id="Seg_2430" n="e" s="T185">öllöppüne </ts>
               <ts e="T187" id="Seg_2432" n="e" s="T186">enigin </ts>
               <ts e="T188" id="Seg_2434" n="e" s="T187">kulut </ts>
               <ts e="T189" id="Seg_2436" n="e" s="T188">gɨnɨ͡aktara", </ts>
               <ts e="T190" id="Seg_2438" n="e" s="T189">di͡ete. </ts>
               <ts e="T191" id="Seg_2440" n="e" s="T190">Čort, </ts>
               <ts e="T192" id="Seg_2442" n="e" s="T191">tuːgu </ts>
               <ts e="T193" id="Seg_2444" n="e" s="T192">da </ts>
               <ts e="T194" id="Seg_2446" n="e" s="T193">ɨraːspɨn </ts>
               <ts e="T195" id="Seg_2448" n="e" s="T194">haːtar, </ts>
               <ts e="T196" id="Seg_2450" n="e" s="T195">huːna </ts>
               <ts e="T197" id="Seg_2452" n="e" s="T196">da </ts>
               <ts e="T198" id="Seg_2454" n="e" s="T197">ilikpin </ts>
               <ts e="T199" id="Seg_2456" n="e" s="T198">erge </ts>
               <ts e="T200" id="Seg_2458" n="e" s="T199">barbɨtɨm. </ts>
               <ts e="T201" id="Seg_2460" n="e" s="T200">ɨtɨː-ɨtɨː </ts>
               <ts e="T202" id="Seg_2462" n="e" s="T201">erge </ts>
               <ts e="T203" id="Seg_2464" n="e" s="T202">bi͡ereller, </ts>
               <ts e="T204" id="Seg_2466" n="e" s="T203">ututallar. </ts>
               <ts e="T205" id="Seg_2468" n="e" s="T204">ɨtɨː-ɨtɨː </ts>
               <ts e="T206" id="Seg_2470" n="e" s="T205">utujabɨn, </ts>
               <ts e="T207" id="Seg_2472" n="e" s="T206">erbitten </ts>
               <ts e="T208" id="Seg_2474" n="e" s="T207">kuttanabɨn, </ts>
               <ts e="T209" id="Seg_2476" n="e" s="T208">taŋastɨːn </ts>
               <ts e="T210" id="Seg_2478" n="e" s="T209">utujabɨn. </ts>
               <ts e="T211" id="Seg_2480" n="e" s="T210">Eː </ts>
               <ts e="T212" id="Seg_2482" n="e" s="T211">dʼe. </ts>
               <ts e="T213" id="Seg_2484" n="e" s="T212">Onton </ts>
               <ts e="T214" id="Seg_2486" n="e" s="T213">dʼɨlɨ, </ts>
               <ts e="T215" id="Seg_2488" n="e" s="T214">dʼɨlɨ </ts>
               <ts e="T216" id="Seg_2490" n="e" s="T215">taŋastaːk </ts>
               <ts e="T217" id="Seg_2492" n="e" s="T216">utujbutum. </ts>
               <ts e="T218" id="Seg_2494" n="e" s="T217">Onton, </ts>
               <ts e="T219" id="Seg_2496" n="e" s="T218">dʼe </ts>
               <ts e="T220" id="Seg_2498" n="e" s="T219">kojut </ts>
               <ts e="T221" id="Seg_2500" n="e" s="T220">ü͡örenebin </ts>
               <ts e="T222" id="Seg_2502" n="e" s="T221">bu͡o, </ts>
               <ts e="T223" id="Seg_2504" n="e" s="T222">(er-) </ts>
               <ts e="T224" id="Seg_2506" n="e" s="T223">(er-) </ts>
               <ts e="T225" id="Seg_2508" n="e" s="T224">erber. </ts>
               <ts e="T226" id="Seg_2510" n="e" s="T225">Ol </ts>
               <ts e="T227" id="Seg_2512" n="e" s="T226">aːta </ts>
               <ts e="T228" id="Seg_2514" n="e" s="T227">pʼervaja </ts>
               <ts e="T229" id="Seg_2516" n="e" s="T228">lʼubovʼ, </ts>
               <ts e="T230" id="Seg_2518" n="e" s="T229">patom, </ts>
               <ts e="T231" id="Seg_2520" n="e" s="T230">celɨj </ts>
               <ts e="T232" id="Seg_2522" n="e" s="T231">god. </ts>
               <ts e="T233" id="Seg_2524" n="e" s="T232">U͡on </ts>
               <ts e="T234" id="Seg_2526" n="e" s="T233">hette </ts>
               <ts e="T235" id="Seg_2528" n="e" s="T234">u͡on </ts>
               <ts e="T236" id="Seg_2530" n="e" s="T235">agɨs </ts>
               <ts e="T237" id="Seg_2532" n="e" s="T236">u͡on </ts>
               <ts e="T238" id="Seg_2534" n="e" s="T237">agɨs </ts>
               <ts e="T239" id="Seg_2536" n="e" s="T238">dʼɨllaːkpar </ts>
               <ts e="T240" id="Seg_2538" n="e" s="T239">ogolommutum. </ts>
               <ts e="T241" id="Seg_2540" n="e" s="T240">Pi͡erbej </ts>
               <ts e="T242" id="Seg_2542" n="e" s="T241">ogom </ts>
               <ts e="T243" id="Seg_2544" n="e" s="T242">munna </ts>
               <ts e="T244" id="Seg_2546" n="e" s="T243">baːr. </ts>
               <ts e="T245" id="Seg_2548" n="e" s="T244">Onton </ts>
               <ts e="T246" id="Seg_2550" n="e" s="T245">ogolonnum, </ts>
               <ts e="T247" id="Seg_2552" n="e" s="T246">ogolonnum, </ts>
               <ts e="T248" id="Seg_2554" n="e" s="T247">dʼɨl </ts>
               <ts e="T249" id="Seg_2556" n="e" s="T248">aːjɨ </ts>
               <ts e="T250" id="Seg_2558" n="e" s="T249">ogolonobun, </ts>
               <ts e="T251" id="Seg_2560" n="e" s="T250">dʼɨl </ts>
               <ts e="T252" id="Seg_2562" n="e" s="T251">aːjɨ </ts>
               <ts e="T254" id="Seg_2564" n="e" s="T252">ogolonobun. </ts>
            </ts>
            <ts e="T406" id="Seg_2565" n="sc" s="T258">
               <ts e="T259" id="Seg_2567" n="e" s="T258">– Eː, </ts>
               <ts e="T260" id="Seg_2569" n="e" s="T259">hubu </ts>
               <ts e="T261" id="Seg_2571" n="e" s="T260">hirge, </ts>
               <ts e="T262" id="Seg_2573" n="e" s="T261">tɨ͡aga </ts>
               <ts e="T263" id="Seg_2575" n="e" s="T262">hɨldʼammɨt, </ts>
               <ts e="T264" id="Seg_2577" n="e" s="T263">tɨ͡aga </ts>
               <ts e="T265" id="Seg_2579" n="e" s="T264">etibit. </ts>
               <ts e="T266" id="Seg_2581" n="e" s="T265">Ogonnʼorum </ts>
               <ts e="T267" id="Seg_2583" n="e" s="T266">kassʼiːr </ts>
               <ts e="T268" id="Seg_2585" n="e" s="T267">ete. </ts>
               <ts e="T269" id="Seg_2587" n="e" s="T268">Hürdeːk </ts>
               <ts e="T270" id="Seg_2589" n="e" s="T269">üčügej </ts>
               <ts e="T271" id="Seg_2591" n="e" s="T270">kihi </ts>
               <ts e="T272" id="Seg_2593" n="e" s="T271">ete, </ts>
               <ts e="T273" id="Seg_2595" n="e" s="T272">spasoːbnɨj </ts>
               <ts e="T274" id="Seg_2597" n="e" s="T273">bagajɨ </ts>
               <ts e="T275" id="Seg_2599" n="e" s="T274">bulčut. </ts>
               <ts e="T276" id="Seg_2601" n="e" s="T275">Buldu </ts>
               <ts e="T277" id="Seg_2603" n="e" s="T276">daː </ts>
               <ts e="T278" id="Seg_2605" n="e" s="T277">bultaːbɨta. </ts>
               <ts e="T279" id="Seg_2607" n="e" s="T278">Dʼi͡e </ts>
               <ts e="T280" id="Seg_2609" n="e" s="T279">da </ts>
               <ts e="T281" id="Seg_2611" n="e" s="T280">tupputa. </ts>
               <ts e="T282" id="Seg_2613" n="e" s="T281">Kassʼiːrga </ts>
               <ts e="T283" id="Seg_2615" n="e" s="T282">u͡on </ts>
               <ts e="T284" id="Seg_2617" n="e" s="T283">bi͡es </ts>
               <ts e="T285" id="Seg_2619" n="e" s="T284">dʼɨlɨ </ts>
               <ts e="T286" id="Seg_2621" n="e" s="T285">olorbuta. </ts>
               <ts e="T287" id="Seg_2623" n="e" s="T286">Onton </ts>
               <ts e="T288" id="Seg_2625" n="e" s="T287">biːr </ts>
               <ts e="T289" id="Seg_2627" n="e" s="T288">dʼɨlɨ </ts>
               <ts e="T290" id="Seg_2629" n="e" s="T289">partorgaga </ts>
               <ts e="T291" id="Seg_2631" n="e" s="T290">olorbuta. </ts>
               <ts e="T292" id="Seg_2633" n="e" s="T291">Onton </ts>
               <ts e="T293" id="Seg_2635" n="e" s="T292">dʼi͡eleri </ts>
               <ts e="T294" id="Seg_2637" n="e" s="T293">tupputa, </ts>
               <ts e="T295" id="Seg_2639" n="e" s="T294">bu </ts>
               <ts e="T296" id="Seg_2641" n="e" s="T295">turar </ts>
               <ts e="T297" id="Seg_2643" n="e" s="T296">tupput </ts>
               <ts e="T298" id="Seg_2645" n="e" s="T297">dʼi͡elere. </ts>
               <ts e="T299" id="Seg_2647" n="e" s="T298">Onton, </ts>
               <ts e="T300" id="Seg_2649" n="e" s="T299">u͡on </ts>
               <ts e="T301" id="Seg_2651" n="e" s="T300">ikki </ts>
               <ts e="T302" id="Seg_2653" n="e" s="T301">ogoloːkput </ts>
               <ts e="T303" id="Seg_2655" n="e" s="T302">onton, </ts>
               <ts e="T304" id="Seg_2657" n="e" s="T303">barɨta. </ts>
               <ts e="T305" id="Seg_2659" n="e" s="T304">Biːr </ts>
               <ts e="T672" id="Seg_2661" n="e" s="T305">de </ts>
               <ts e="T306" id="Seg_2663" n="e" s="T672">kihini </ts>
               <ts e="T307" id="Seg_2665" n="e" s="T306">kihi͡eke </ts>
               <ts e="T308" id="Seg_2667" n="e" s="T307">bi͡erbetegim </ts>
               <ts e="T309" id="Seg_2669" n="e" s="T308">ogolorbun. </ts>
               <ts e="T310" id="Seg_2671" n="e" s="T309">Bejem </ts>
               <ts e="T311" id="Seg_2673" n="e" s="T310">iːti͡ek </ts>
               <ts e="T312" id="Seg_2675" n="e" s="T311">bu͡olammɨn </ts>
               <ts e="T313" id="Seg_2677" n="e" s="T312">kihi͡eke </ts>
               <ts e="T314" id="Seg_2679" n="e" s="T313">bi͡erbeppin, </ts>
               <ts e="T315" id="Seg_2681" n="e" s="T314">ahɨnabɨn </ts>
               <ts e="T316" id="Seg_2683" n="e" s="T315">ogolorbun. </ts>
               <ts e="T317" id="Seg_2685" n="e" s="T316">Olorbun </ts>
               <ts e="T318" id="Seg_2687" n="e" s="T317">iːtine </ts>
               <ts e="T319" id="Seg_2689" n="e" s="T318">iːtibitinen </ts>
               <ts e="T320" id="Seg_2691" n="e" s="T319">bihikteːk </ts>
               <ts e="T321" id="Seg_2693" n="e" s="T320">ogoloːk </ts>
               <ts e="T322" id="Seg_2695" n="e" s="T321">ilimneheːččibin </ts>
               <ts e="T323" id="Seg_2697" n="e" s="T322">ogonnʼorbun </ts>
               <ts e="T324" id="Seg_2699" n="e" s="T323">gɨtta </ts>
               <ts e="T325" id="Seg_2701" n="e" s="T324">ɨ͡arɨj </ts>
               <ts e="T326" id="Seg_2703" n="e" s="T325">bu͡olbutun </ts>
               <ts e="T327" id="Seg_2705" n="e" s="T326">kenne. </ts>
               <ts e="T328" id="Seg_2707" n="e" s="T327">Dʼe </ts>
               <ts e="T329" id="Seg_2709" n="e" s="T328">ogolorbut </ts>
               <ts e="T330" id="Seg_2711" n="e" s="T329">ulaːtannar </ts>
               <ts e="T331" id="Seg_2713" n="e" s="T330">ü͡örene </ts>
               <ts e="T332" id="Seg_2715" n="e" s="T331">barɨtalaːbɨttara, </ts>
               <ts e="T333" id="Seg_2717" n="e" s="T332">onton </ts>
               <ts e="T334" id="Seg_2719" n="e" s="T333">kojut </ts>
               <ts e="T335" id="Seg_2721" n="e" s="T334">ikki </ts>
               <ts e="T336" id="Seg_2723" n="e" s="T335">ulakan </ts>
               <ts e="T337" id="Seg_2725" n="e" s="T336">ogom </ts>
               <ts e="T338" id="Seg_2727" n="e" s="T337">taba </ts>
               <ts e="T339" id="Seg_2729" n="e" s="T338">ü͡öregiger </ts>
               <ts e="T340" id="Seg_2731" n="e" s="T339">barbɨttara, </ts>
               <ts e="T341" id="Seg_2733" n="e" s="T340">u͡olbut </ts>
               <ts e="T342" id="Seg_2735" n="e" s="T341">kɨːspɨt. </ts>
               <ts e="T343" id="Seg_2737" n="e" s="T342">Onton </ts>
               <ts e="T344" id="Seg_2739" n="e" s="T343">biːr </ts>
               <ts e="T345" id="Seg_2741" n="e" s="T344">ogobut </ts>
               <ts e="T346" id="Seg_2743" n="e" s="T345">ulaːtan </ts>
               <ts e="T347" id="Seg_2745" n="e" s="T346">Krasnajarska </ts>
               <ts e="T348" id="Seg_2747" n="e" s="T347">ü͡örene </ts>
               <ts e="T349" id="Seg_2749" n="e" s="T348">barbɨta, </ts>
               <ts e="T350" id="Seg_2751" n="e" s="T349">anɨ </ts>
               <ts e="T351" id="Seg_2753" n="e" s="T350">tʼerapʼevka </ts>
               <ts e="T352" id="Seg_2755" n="e" s="T351">üleliː </ts>
               <ts e="T353" id="Seg_2757" n="e" s="T352">hɨtar </ts>
               <ts e="T354" id="Seg_2759" n="e" s="T353">Dudʼinkaga. </ts>
               <ts e="T355" id="Seg_2761" n="e" s="T354">Onton </ts>
               <ts e="T356" id="Seg_2763" n="e" s="T355">biːr </ts>
               <ts e="T357" id="Seg_2765" n="e" s="T356">biːr </ts>
               <ts e="T358" id="Seg_2767" n="e" s="T357">ogom </ts>
               <ts e="T359" id="Seg_2769" n="e" s="T358">bu͡ollagɨna </ts>
               <ts e="T360" id="Seg_2771" n="e" s="T359">emi͡e </ts>
               <ts e="T361" id="Seg_2773" n="e" s="T360">ü͡örenen </ts>
               <ts e="T362" id="Seg_2775" n="e" s="T361">ikki </ts>
               <ts e="T363" id="Seg_2777" n="e" s="T362">institutu </ts>
               <ts e="T364" id="Seg_2779" n="e" s="T363">büppüte, </ts>
               <ts e="T365" id="Seg_2781" n="e" s="T364">onton </ts>
               <ts e="T366" id="Seg_2783" n="e" s="T365">Krasnajarskajga </ts>
               <ts e="T367" id="Seg_2785" n="e" s="T366">baːr. </ts>
               <ts e="T368" id="Seg_2787" n="e" s="T367">(Sʼer-) </ts>
               <ts e="T369" id="Seg_2789" n="e" s="T368">Sʼergʼej. </ts>
               <ts e="T370" id="Seg_2791" n="e" s="T369">Ontum </ts>
               <ts e="T371" id="Seg_2793" n="e" s="T370">dʼaktardaːk </ts>
               <ts e="T372" id="Seg_2795" n="e" s="T371">ogo </ts>
               <ts e="T373" id="Seg_2797" n="e" s="T372">ol, </ts>
               <ts e="T374" id="Seg_2799" n="e" s="T373">ikki </ts>
               <ts e="T375" id="Seg_2801" n="e" s="T374">u͡ollaːk. </ts>
               <ts e="T376" id="Seg_2803" n="e" s="T375">Kazaːktartan </ts>
               <ts e="T377" id="Seg_2805" n="e" s="T376">dʼaktardammɨt. </ts>
               <ts e="T378" id="Seg_2807" n="e" s="T377">(Araŋastaːktan), </ts>
               <ts e="T379" id="Seg_2809" n="e" s="T378">anɨ </ts>
               <ts e="T380" id="Seg_2811" n="e" s="T379">bu͡ollagɨna </ts>
               <ts e="T381" id="Seg_2813" n="e" s="T380">axranaga </ts>
               <ts e="T382" id="Seg_2815" n="e" s="T381">axranaga </ts>
               <ts e="T383" id="Seg_2817" n="e" s="T382">üleliːr, </ts>
               <ts e="T384" id="Seg_2819" n="e" s="T383">mʼilʼisʼijanɨ </ts>
               <ts e="T385" id="Seg_2821" n="e" s="T384">büten </ts>
               <ts e="T386" id="Seg_2823" n="e" s="T385">baran. </ts>
               <ts e="T387" id="Seg_2825" n="e" s="T386">Urut </ts>
               <ts e="T388" id="Seg_2827" n="e" s="T387">taba </ts>
               <ts e="T389" id="Seg_2829" n="e" s="T388">ülehite </ts>
               <ts e="T390" id="Seg_2831" n="e" s="T389">bu͡olbuta. </ts>
               <ts e="T391" id="Seg_2833" n="e" s="T390">Onton </ts>
               <ts e="T392" id="Seg_2835" n="e" s="T391">biːr </ts>
               <ts e="T393" id="Seg_2837" n="e" s="T392">ogom </ts>
               <ts e="T394" id="Seg_2839" n="e" s="T393">poːvarga </ts>
               <ts e="T395" id="Seg_2841" n="e" s="T394">Nosku͡oga </ts>
               <ts e="T396" id="Seg_2843" n="e" s="T395">üleliːr, </ts>
               <ts e="T397" id="Seg_2845" n="e" s="T396">glavnaj </ts>
               <ts e="T398" id="Seg_2847" n="e" s="T397">poːvar, </ts>
               <ts e="T399" id="Seg_2849" n="e" s="T398">biːr </ts>
               <ts e="T400" id="Seg_2851" n="e" s="T399">kɨːhɨm. </ts>
               <ts e="T401" id="Seg_2853" n="e" s="T400">Biːr </ts>
               <ts e="T402" id="Seg_2855" n="e" s="T401">(kɨ-) </ts>
               <ts e="T403" id="Seg_2857" n="e" s="T402">tu͡ok, </ts>
               <ts e="T404" id="Seg_2859" n="e" s="T403">(Nas-) </ts>
               <ts e="T405" id="Seg_2861" n="e" s="T404">Nasku͡oga </ts>
               <ts e="T406" id="Seg_2863" n="e" s="T405">Marʼina. </ts>
            </ts>
            <ts e="T467" id="Seg_2864" n="sc" s="T409">
               <ts e="T410" id="Seg_2866" n="e" s="T409">– Hu͡o, </ts>
               <ts e="T411" id="Seg_2868" n="e" s="T410">ü͡öremmite, </ts>
               <ts e="T412" id="Seg_2870" n="e" s="T411">heː, </ts>
               <ts e="T413" id="Seg_2872" n="e" s="T412">ol </ts>
               <ts e="T414" id="Seg_2874" n="e" s="T413">di͡et. </ts>
               <ts e="T415" id="Seg_2876" n="e" s="T414">Krasnajarskajga </ts>
               <ts e="T416" id="Seg_2878" n="e" s="T415">ü͡öremmite. </ts>
               <ts e="T417" id="Seg_2880" n="e" s="T416">Onton </ts>
               <ts e="T418" id="Seg_2882" n="e" s="T417">(ma-) </ts>
               <ts e="T419" id="Seg_2884" n="e" s="T418">du͡oktuːr </ts>
               <ts e="T420" id="Seg_2886" n="e" s="T419">kɨːhɨm </ts>
               <ts e="T421" id="Seg_2888" n="e" s="T420">Dudʼinkaga </ts>
               <ts e="T422" id="Seg_2890" n="e" s="T421">üleliːr, </ts>
               <ts e="T423" id="Seg_2892" n="e" s="T422">tʼerapʼevka </ts>
               <ts e="T424" id="Seg_2894" n="e" s="T423">üleliːr </ts>
               <ts e="T425" id="Seg_2896" n="e" s="T424">anɨ, </ts>
               <ts e="T426" id="Seg_2898" n="e" s="T425">üs </ts>
               <ts e="T427" id="Seg_2900" n="e" s="T426">ogoloːk, </ts>
               <ts e="T428" id="Seg_2902" n="e" s="T427">kɨrdʼagas </ts>
               <ts e="T429" id="Seg_2904" n="e" s="T428">dʼaktar </ts>
               <ts e="T430" id="Seg_2906" n="e" s="T429">pʼensʼijalaːk. </ts>
               <ts e="T431" id="Seg_2908" n="e" s="T430">Ogom </ts>
               <ts e="T432" id="Seg_2910" n="e" s="T431">barɨta </ts>
               <ts e="T433" id="Seg_2912" n="e" s="T432">pʼensʼijalaːk. </ts>
               <ts e="T434" id="Seg_2914" n="e" s="T433">Üs </ts>
               <ts e="T435" id="Seg_2916" n="e" s="T434">ogom </ts>
               <ts e="T436" id="Seg_2918" n="e" s="T435">pʼensʼijalaːktar. </ts>
               <ts e="T437" id="Seg_2920" n="e" s="T436">Ulakattarɨm </ts>
               <ts e="T438" id="Seg_2922" n="e" s="T437">anɨ, </ts>
               <ts e="T439" id="Seg_2924" n="e" s="T438">biːrgehe </ts>
               <ts e="T440" id="Seg_2926" n="e" s="T439">bu͡ollagɨna </ts>
               <ts e="T441" id="Seg_2928" n="e" s="T440">rɨbaktɨːr, </ts>
               <ts e="T442" id="Seg_2930" n="e" s="T441">biːrgehe </ts>
               <ts e="T443" id="Seg_2932" n="e" s="T442">bu͡olla </ts>
               <ts e="T444" id="Seg_2934" n="e" s="T443">anɨ </ts>
               <ts e="T445" id="Seg_2936" n="e" s="T444">haːhɨrbɨt </ts>
               <ts e="T446" id="Seg_2938" n="e" s="T445">pʼensʼijalaːk, </ts>
               <ts e="T447" id="Seg_2940" n="e" s="T446">Bi͡ere, </ts>
               <ts e="T448" id="Seg_2942" n="e" s="T447">ere </ts>
               <ts e="T449" id="Seg_2944" n="e" s="T448">hubukaːn </ts>
               <ts e="T450" id="Seg_2946" n="e" s="T449">ölbüte. </ts>
               <ts e="T451" id="Seg_2948" n="e" s="T450">Ikki </ts>
               <ts e="T452" id="Seg_2950" n="e" s="T451">ogoloːk, </ts>
               <ts e="T453" id="Seg_2952" n="e" s="T452">ühüstere </ts>
               <ts e="T454" id="Seg_2954" n="e" s="T453">ölbüte. </ts>
               <ts e="T455" id="Seg_2956" n="e" s="T454">Ulakan </ts>
               <ts e="T456" id="Seg_2958" n="e" s="T455">u͡olum </ts>
               <ts e="T457" id="Seg_2960" n="e" s="T456">üs </ts>
               <ts e="T458" id="Seg_2962" n="e" s="T457">ogoloːk, </ts>
               <ts e="T459" id="Seg_2964" n="e" s="T458">ogo, </ts>
               <ts e="T460" id="Seg_2966" n="e" s="T459">biːr </ts>
               <ts e="T461" id="Seg_2968" n="e" s="T460">ogoto </ts>
               <ts e="T462" id="Seg_2970" n="e" s="T461">dʼaktardaːk, </ts>
               <ts e="T463" id="Seg_2972" n="e" s="T462">ogoloːk. </ts>
               <ts e="T464" id="Seg_2974" n="e" s="T463">Aŋardara </ts>
               <ts e="T465" id="Seg_2976" n="e" s="T464">ogolono </ts>
               <ts e="T467" id="Seg_2978" n="e" s="T465">ilikter. </ts>
            </ts>
            <ts e="T494" id="Seg_2979" n="sc" s="T469">
               <ts e="T471" id="Seg_2981" n="e" s="T469">– Heː. </ts>
               <ts e="T472" id="Seg_2983" n="e" s="T471">Onton </ts>
               <ts e="T473" id="Seg_2985" n="e" s="T472">biːr </ts>
               <ts e="T474" id="Seg_2987" n="e" s="T473">ogom </ts>
               <ts e="T475" id="Seg_2989" n="e" s="T474">ke </ts>
               <ts e="T476" id="Seg_2991" n="e" s="T475">kimi͡eke </ts>
               <ts e="T477" id="Seg_2993" n="e" s="T476">baːr, </ts>
               <ts e="T478" id="Seg_2995" n="e" s="T477">Hoːpstajga </ts>
               <ts e="T479" id="Seg_2997" n="e" s="T478">baːr. </ts>
               <ts e="T480" id="Seg_2999" n="e" s="T479">Kimi͡eke, </ts>
               <ts e="T481" id="Seg_3001" n="e" s="T480">vaspʼitatʼelga </ts>
               <ts e="T482" id="Seg_3003" n="e" s="T481">üleliːr. </ts>
               <ts e="T483" id="Seg_3005" n="e" s="T482">Onton </ts>
               <ts e="T484" id="Seg_3007" n="e" s="T483">biːr </ts>
               <ts e="T485" id="Seg_3009" n="e" s="T484">ogom </ts>
               <ts e="T486" id="Seg_3011" n="e" s="T485">munna, </ts>
               <ts e="T487" id="Seg_3013" n="e" s="T486">biːr </ts>
               <ts e="T488" id="Seg_3015" n="e" s="T487">kɨːhɨm </ts>
               <ts e="T489" id="Seg_3017" n="e" s="T488">munna </ts>
               <ts e="T490" id="Seg_3019" n="e" s="T489">vaspʼitatʼeliːr. </ts>
               <ts e="T491" id="Seg_3021" n="e" s="T490">Biːr </ts>
               <ts e="T492" id="Seg_3023" n="e" s="T491">ogom </ts>
               <ts e="T494" id="Seg_3025" n="e" s="T492">du͡o… </ts>
            </ts>
            <ts e="T501" id="Seg_3026" n="sc" s="T496">
               <ts e="T497" id="Seg_3028" n="e" s="T496">– Heː, </ts>
               <ts e="T498" id="Seg_3030" n="e" s="T497">sku͡olaga </ts>
               <ts e="T499" id="Seg_3032" n="e" s="T498">munna, </ts>
               <ts e="T500" id="Seg_3034" n="e" s="T499">Du͡omna </ts>
               <ts e="T501" id="Seg_3036" n="e" s="T500">Pʼetrovna. </ts>
            </ts>
            <ts e="T667" id="Seg_3037" n="sc" s="T506">
               <ts e="T508" id="Seg_3039" n="e" s="T506">– Eː, </ts>
               <ts e="T509" id="Seg_3041" n="e" s="T508">onton </ts>
               <ts e="T510" id="Seg_3043" n="e" s="T509">biːr </ts>
               <ts e="T511" id="Seg_3045" n="e" s="T510">ɨlgɨn </ts>
               <ts e="T512" id="Seg_3047" n="e" s="T511">u͡olum </ts>
               <ts e="T513" id="Seg_3049" n="e" s="T512">dʼaktara </ts>
               <ts e="T514" id="Seg_3051" n="e" s="T513">emi͡e </ts>
               <ts e="T515" id="Seg_3053" n="e" s="T514">munna </ts>
               <ts e="T516" id="Seg_3055" n="e" s="T515">učitallɨːr, </ts>
               <ts e="T517" id="Seg_3057" n="e" s="T516">Naːdʼa </ts>
               <ts e="T518" id="Seg_3059" n="e" s="T517">Kʼirgʼizava </ts>
               <ts e="T519" id="Seg_3061" n="e" s="T518">i͡e. </ts>
               <ts e="T520" id="Seg_3063" n="e" s="T519">Nʼevʼestka. </ts>
               <ts e="T521" id="Seg_3065" n="e" s="T520">Onton </ts>
               <ts e="T522" id="Seg_3067" n="e" s="T521">biːr </ts>
               <ts e="T523" id="Seg_3069" n="e" s="T522">u͡olum </ts>
               <ts e="T524" id="Seg_3071" n="e" s="T523">bu͡ollagɨna </ts>
               <ts e="T525" id="Seg_3073" n="e" s="T524">kim, </ts>
               <ts e="T526" id="Seg_3075" n="e" s="T525">bʼezdʼexotčʼik </ts>
               <ts e="T527" id="Seg_3077" n="e" s="T526">büppüte, </ts>
               <ts e="T528" id="Seg_3079" n="e" s="T527">ü͡örenen </ts>
               <ts e="T529" id="Seg_3081" n="e" s="T528">üleliːr </ts>
               <ts e="T530" id="Seg_3083" n="e" s="T529">ete. </ts>
               <ts e="T531" id="Seg_3085" n="e" s="T530">Iːgireler. </ts>
               <ts e="T532" id="Seg_3087" n="e" s="T531">Biːrgehe </ts>
               <ts e="T533" id="Seg_3089" n="e" s="T532">elʼektrʼik </ts>
               <ts e="T534" id="Seg_3091" n="e" s="T533">iti </ts>
               <ts e="T535" id="Seg_3093" n="e" s="T534">üleliː </ts>
               <ts e="T536" id="Seg_3095" n="e" s="T535">hɨldʼar </ts>
               <ts e="T537" id="Seg_3097" n="e" s="T536">anɨga </ts>
               <ts e="T538" id="Seg_3099" n="e" s="T537">di͡eri </ts>
               <ts e="T539" id="Seg_3101" n="e" s="T538">ü͡örenen </ts>
               <ts e="T540" id="Seg_3103" n="e" s="T539">büten </ts>
               <ts e="T541" id="Seg_3105" n="e" s="T540">baran. </ts>
               <ts e="T542" id="Seg_3107" n="e" s="T541">Tak </ts>
               <ts e="T543" id="Seg_3109" n="e" s="T542">da </ts>
               <ts e="T544" id="Seg_3111" n="e" s="T543">barɨta </ts>
               <ts e="T545" id="Seg_3113" n="e" s="T544">ü͡öremmittere. </ts>
               <ts e="T546" id="Seg_3115" n="e" s="T545">Onton </ts>
               <ts e="T547" id="Seg_3117" n="e" s="T546">ke </ts>
               <ts e="T548" id="Seg_3119" n="e" s="T547">biːr </ts>
               <ts e="T549" id="Seg_3121" n="e" s="T548">ogom </ts>
               <ts e="T550" id="Seg_3123" n="e" s="T549">Kiri͡eska </ts>
               <ts e="T551" id="Seg_3125" n="e" s="T550">baːr. </ts>
               <ts e="T552" id="Seg_3127" n="e" s="T551">(Han-) </ts>
               <ts e="T553" id="Seg_3129" n="e" s="T552">eː </ts>
               <ts e="T554" id="Seg_3131" n="e" s="T553">mʼilʼisʼije </ts>
               <ts e="T555" id="Seg_3133" n="e" s="T554">ü͡öregin </ts>
               <ts e="T556" id="Seg_3135" n="e" s="T555">büppüt </ts>
               <ts e="T557" id="Seg_3137" n="e" s="T556">ete, </ts>
               <ts e="T558" id="Seg_3139" n="e" s="T557">ontuŋ </ts>
               <ts e="T559" id="Seg_3141" n="e" s="T558">hir </ts>
               <ts e="T560" id="Seg_3143" n="e" s="T559">aːjɨ </ts>
               <ts e="T561" id="Seg_3145" n="e" s="T560">hiri </ts>
               <ts e="T562" id="Seg_3147" n="e" s="T561">baratan </ts>
               <ts e="T563" id="Seg_3149" n="e" s="T562">baratan </ts>
               <ts e="T564" id="Seg_3151" n="e" s="T563">baran, </ts>
               <ts e="T565" id="Seg_3153" n="e" s="T564">Kiri͡eske </ts>
               <ts e="T566" id="Seg_3155" n="e" s="T565">dʼaktardaːk. </ts>
               <ts e="T567" id="Seg_3157" n="e" s="T566">Alʼeksʼej. </ts>
               <ts e="T568" id="Seg_3159" n="e" s="T567">Kördüŋ </ts>
               <ts e="T569" id="Seg_3161" n="e" s="T568">itte </ts>
               <ts e="T570" id="Seg_3163" n="e" s="T569">eni </ts>
               <ts e="T571" id="Seg_3165" n="e" s="T570">araːha. </ts>
               <ts e="T572" id="Seg_3167" n="e" s="T571">Onton </ts>
               <ts e="T573" id="Seg_3169" n="e" s="T572">aŋardara </ts>
               <ts e="T574" id="Seg_3171" n="e" s="T573">bulčuttar </ts>
               <ts e="T575" id="Seg_3173" n="e" s="T574">üs </ts>
               <ts e="T576" id="Seg_3175" n="e" s="T575">u͡olum, </ts>
               <ts e="T577" id="Seg_3177" n="e" s="T576">bulčuttar </ts>
               <ts e="T578" id="Seg_3179" n="e" s="T577">bu͡olla. </ts>
               <ts e="T579" id="Seg_3181" n="e" s="T578">Ölü͡öse, </ts>
               <ts e="T580" id="Seg_3183" n="e" s="T579">Hergeːj. </ts>
               <ts e="T581" id="Seg_3185" n="e" s="T580">Miːse, </ts>
               <ts e="T582" id="Seg_3187" n="e" s="T581">Biːtʼe. </ts>
               <ts e="T583" id="Seg_3189" n="e" s="T582">Biːtʼe </ts>
               <ts e="T584" id="Seg_3191" n="e" s="T583">utuja </ts>
               <ts e="T585" id="Seg_3193" n="e" s="T584">hɨtar, </ts>
               <ts e="T586" id="Seg_3195" n="e" s="T585">dʼaktara </ts>
               <ts e="T587" id="Seg_3197" n="e" s="T586">bu </ts>
               <ts e="T588" id="Seg_3199" n="e" s="T587">turar. </ts>
               <ts e="T589" id="Seg_3201" n="e" s="T588">Elete </ts>
               <ts e="T590" id="Seg_3203" n="e" s="T589">du͡o </ts>
               <ts e="T591" id="Seg_3205" n="e" s="T590">onton. </ts>
               <ts e="T592" id="Seg_3207" n="e" s="T591">U͡on </ts>
               <ts e="T593" id="Seg_3209" n="e" s="T592">ikki </ts>
               <ts e="T594" id="Seg_3211" n="e" s="T593">ogoloːkpun, </ts>
               <ts e="T595" id="Seg_3213" n="e" s="T594">hürbe </ts>
               <ts e="T596" id="Seg_3215" n="e" s="T595">orduga </ts>
               <ts e="T597" id="Seg_3217" n="e" s="T596">bi͡es, </ts>
               <ts e="T598" id="Seg_3219" n="e" s="T597">eː </ts>
               <ts e="T599" id="Seg_3221" n="e" s="T598">hette </ts>
               <ts e="T600" id="Seg_3223" n="e" s="T599">bɨnuktaːkpɨn. </ts>
               <ts e="T601" id="Seg_3225" n="e" s="T600">Oččogo </ts>
               <ts e="T602" id="Seg_3227" n="e" s="T601">dʼirʼevnʼijabit </ts>
               <ts e="T603" id="Seg_3229" n="e" s="T602">innʼe </ts>
               <ts e="T604" id="Seg_3231" n="e" s="T603">tü͡örd-u͡ontan </ts>
               <ts e="T605" id="Seg_3233" n="e" s="T604">taksa </ts>
               <ts e="T606" id="Seg_3235" n="e" s="T605">kihi. </ts>
               <ts e="T607" id="Seg_3237" n="e" s="T606">Kiniːtterbin </ts>
               <ts e="T608" id="Seg_3239" n="e" s="T607">barɨtɨn </ts>
               <ts e="T609" id="Seg_3241" n="e" s="T608">bullakpɨna </ts>
               <ts e="T610" id="Seg_3243" n="e" s="T609">elbekter. </ts>
               <ts e="T611" id="Seg_3245" n="e" s="T610">Aːktakpɨna. </ts>
               <ts e="T612" id="Seg_3247" n="e" s="T611">Biːr </ts>
               <ts e="T613" id="Seg_3249" n="e" s="T612">kiniːt, </ts>
               <ts e="T614" id="Seg_3251" n="e" s="T613">ikki </ts>
               <ts e="T615" id="Seg_3253" n="e" s="T614">kiniːt </ts>
               <ts e="T616" id="Seg_3255" n="e" s="T615">pʼedagoːgtar. </ts>
               <ts e="T617" id="Seg_3257" n="e" s="T616">Ogolordoːktor </ts>
               <ts e="T618" id="Seg_3259" n="e" s="T617">barɨlara. </ts>
               <ts e="T619" id="Seg_3261" n="e" s="T618">Kɨrgɨttara </ts>
               <ts e="T620" id="Seg_3263" n="e" s="T619">ü͡örene </ts>
               <ts e="T621" id="Seg_3265" n="e" s="T620">barbɨttara. </ts>
               <ts e="T622" id="Seg_3267" n="e" s="T621">Biːr </ts>
               <ts e="T623" id="Seg_3269" n="e" s="T622">kiniːtim </ts>
               <ts e="T624" id="Seg_3271" n="e" s="T623">saːdʼikka. </ts>
               <ts e="T625" id="Seg_3273" n="e" s="T624">Kimnere, </ts>
               <ts e="T626" id="Seg_3275" n="e" s="T625">kim </ts>
               <ts e="T627" id="Seg_3277" n="e" s="T626">etej </ts>
               <ts e="T628" id="Seg_3279" n="e" s="T627">ke </ts>
               <ts e="T629" id="Seg_3281" n="e" s="T628">dogo? </ts>
               <ts e="T630" id="Seg_3283" n="e" s="T629">Zabʼedusajdar. </ts>
               <ts e="T631" id="Seg_3285" n="e" s="T630">Ikki </ts>
               <ts e="T632" id="Seg_3287" n="e" s="T631">kiniːtim </ts>
               <ts e="T633" id="Seg_3289" n="e" s="T632">učiːtallar, </ts>
               <ts e="T634" id="Seg_3291" n="e" s="T633">pʼedagogtar. </ts>
               <ts e="T635" id="Seg_3293" n="e" s="T634">Barɨta </ts>
               <ts e="T636" id="Seg_3295" n="e" s="T635">ülehitter. </ts>
               <ts e="T637" id="Seg_3297" n="e" s="T636">Onton </ts>
               <ts e="T638" id="Seg_3299" n="e" s="T637">biːr </ts>
               <ts e="T639" id="Seg_3301" n="e" s="T638">kütü͡ötüm </ts>
               <ts e="T640" id="Seg_3303" n="e" s="T639">iti </ts>
               <ts e="T641" id="Seg_3305" n="e" s="T640">taŋaralaːta, </ts>
               <ts e="T642" id="Seg_3307" n="e" s="T641">biːr </ts>
               <ts e="T643" id="Seg_3309" n="e" s="T642">kütü͡ötüm </ts>
               <ts e="T644" id="Seg_3311" n="e" s="T643">ɨ͡arɨj </ts>
               <ts e="T645" id="Seg_3313" n="e" s="T644">bu͡olbut, </ts>
               <ts e="T646" id="Seg_3315" n="e" s="T645">ogo. </ts>
               <ts e="T647" id="Seg_3317" n="e" s="T646">Domna </ts>
               <ts e="T648" id="Seg_3319" n="e" s="T647">Pʼetroːvna </ts>
               <ts e="T649" id="Seg_3321" n="e" s="T648">gi͡ene. </ts>
               <ts e="T650" id="Seg_3323" n="e" s="T649">Ol </ts>
               <ts e="T651" id="Seg_3325" n="e" s="T650">ereːti </ts>
               <ts e="T652" id="Seg_3327" n="e" s="T651">buldu </ts>
               <ts e="T653" id="Seg_3329" n="e" s="T652">delbi </ts>
               <ts e="T654" id="Seg_3331" n="e" s="T653">ölörör, </ts>
               <ts e="T655" id="Seg_3333" n="e" s="T654">ataga </ts>
               <ts e="T656" id="Seg_3335" n="e" s="T655">hu͡ok </ts>
               <ts e="T657" id="Seg_3337" n="e" s="T656">bejete. </ts>
               <ts e="T658" id="Seg_3339" n="e" s="T657">Biːr </ts>
               <ts e="T659" id="Seg_3341" n="e" s="T658">kütü͡ötüm </ts>
               <ts e="T660" id="Seg_3343" n="e" s="T659">Hu͡opostajga </ts>
               <ts e="T661" id="Seg_3345" n="e" s="T660">axotnʼik. </ts>
               <ts e="T662" id="Seg_3347" n="e" s="T661">Kɨːha </ts>
               <ts e="T663" id="Seg_3349" n="e" s="T662">ü͡örene </ts>
               <ts e="T664" id="Seg_3351" n="e" s="T663">barbɨt. </ts>
               <ts e="T665" id="Seg_3353" n="e" s="T664">Dʼe </ts>
               <ts e="T666" id="Seg_3355" n="e" s="T665">tugu </ts>
               <ts e="T667" id="Seg_3357" n="e" s="T666">kepsi͡eppinij? </ts>
            </ts>
            <ts e="T671" id="Seg_3358" n="sc" s="T670">
               <ts e="T671" id="Seg_3360" n="e" s="T670">– Dʼe. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KiPP">
            <ta e="T4" id="Seg_3361" s="T1">KiPP_XXXX_2009_Family_nar.KiPP.001 (001.001)</ta>
            <ta e="T9" id="Seg_3362" s="T6">KiPP_XXXX_2009_Family_nar.KiPP.002 (001.003)</ta>
            <ta e="T20" id="Seg_3363" s="T14">KiPP_XXXX_2009_Family_nar.KiPP.003 (001.006)</ta>
            <ta e="T25" id="Seg_3364" s="T20">KiPP_XXXX_2009_Family_nar.KiPP.004 (001.007)</ta>
            <ta e="T31" id="Seg_3365" s="T27">KiPP_XXXX_2009_Family_nar.KiPP.005 (001.009)</ta>
            <ta e="T33" id="Seg_3366" s="T31">KiPP_XXXX_2009_Family_nar.KiPP.006 (001.010)</ta>
            <ta e="T51" id="Seg_3367" s="T49">KiPP_XXXX_2009_Family_nar.KiPP.007 (001.013)</ta>
            <ta e="T57" id="Seg_3368" s="T51">KiPP_XXXX_2009_Family_nar.KiPP.008 (001.014)</ta>
            <ta e="T62" id="Seg_3369" s="T57">KiPP_XXXX_2009_Family_nar.KiPP.009 (001.014)</ta>
            <ta e="T68" id="Seg_3370" s="T66">KiPP_XXXX_2009_Family_nar.KiPP.010 (001.017)</ta>
            <ta e="T73" id="Seg_3371" s="T70">KiPP_XXXX_2009_Family_nar.KiPP.011 (001.018)</ta>
            <ta e="T76" id="Seg_3372" s="T73">KiPP_XXXX_2009_Family_nar.KiPP.012 (001.019)</ta>
            <ta e="T77" id="Seg_3373" s="T76">KiPP_XXXX_2009_Family_nar.KiPP.013 (001.020)</ta>
            <ta e="T81" id="Seg_3374" s="T77">KiPP_XXXX_2009_Family_nar.KiPP.014 (001.021)</ta>
            <ta e="T92" id="Seg_3375" s="T89">KiPP_XXXX_2009_Family_nar.KiPP.015 (001.024)</ta>
            <ta e="T108" id="Seg_3376" s="T106">KiPP_XXXX_2009_Family_nar.KiPP.016 (001.029)</ta>
            <ta e="T117" id="Seg_3377" s="T111">KiPP_XXXX_2009_Family_nar.KiPP.017 (001.031)</ta>
            <ta e="T124" id="Seg_3378" s="T120">KiPP_XXXX_2009_Family_nar.KiPP.018 (001.033)</ta>
            <ta e="T139" id="Seg_3379" s="T128">KiPP_XXXX_2009_Family_nar.KiPP.019 (001.035)</ta>
            <ta e="T144" id="Seg_3380" s="T139">KiPP_XXXX_2009_Family_nar.KiPP.020 (001.036)</ta>
            <ta e="T158" id="Seg_3381" s="T144">KiPP_XXXX_2009_Family_nar.KiPP.021 (001.037)</ta>
            <ta e="T167" id="Seg_3382" s="T158">KiPP_XXXX_2009_Family_nar.KiPP.022 (001.038)</ta>
            <ta e="T175" id="Seg_3383" s="T167">KiPP_XXXX_2009_Family_nar.KiPP.023 (001.039)</ta>
            <ta e="T184" id="Seg_3384" s="T175">KiPP_XXXX_2009_Family_nar.KiPP.024 (001.040)</ta>
            <ta e="T190" id="Seg_3385" s="T184">KiPP_XXXX_2009_Family_nar.KiPP.025 (001.041)</ta>
            <ta e="T200" id="Seg_3386" s="T190">KiPP_XXXX_2009_Family_nar.KiPP.026 (001.042)</ta>
            <ta e="T204" id="Seg_3387" s="T200">KiPP_XXXX_2009_Family_nar.KiPP.027 (001.043)</ta>
            <ta e="T210" id="Seg_3388" s="T204">KiPP_XXXX_2009_Family_nar.KiPP.028 (001.044)</ta>
            <ta e="T212" id="Seg_3389" s="T210">KiPP_XXXX_2009_Family_nar.KiPP.029 (001.045)</ta>
            <ta e="T217" id="Seg_3390" s="T212">KiPP_XXXX_2009_Family_nar.KiPP.030 (001.046)</ta>
            <ta e="T225" id="Seg_3391" s="T217">KiPP_XXXX_2009_Family_nar.KiPP.031 (001.047)</ta>
            <ta e="T232" id="Seg_3392" s="T225">KiPP_XXXX_2009_Family_nar.KiPP.032 (001.048)</ta>
            <ta e="T240" id="Seg_3393" s="T232">KiPP_XXXX_2009_Family_nar.KiPP.033 (001.049)</ta>
            <ta e="T244" id="Seg_3394" s="T240">KiPP_XXXX_2009_Family_nar.KiPP.034 (001.050)</ta>
            <ta e="T254" id="Seg_3395" s="T244">KiPP_XXXX_2009_Family_nar.KiPP.035 (001.051)</ta>
            <ta e="T265" id="Seg_3396" s="T258">KiPP_XXXX_2009_Family_nar.KiPP.036 (001.053)</ta>
            <ta e="T268" id="Seg_3397" s="T265">KiPP_XXXX_2009_Family_nar.KiPP.037 (001.054)</ta>
            <ta e="T275" id="Seg_3398" s="T268">KiPP_XXXX_2009_Family_nar.KiPP.038 (001.055)</ta>
            <ta e="T278" id="Seg_3399" s="T275">KiPP_XXXX_2009_Family_nar.KiPP.039 (001.056)</ta>
            <ta e="T281" id="Seg_3400" s="T278">KiPP_XXXX_2009_Family_nar.KiPP.040 (001.057)</ta>
            <ta e="T286" id="Seg_3401" s="T281">KiPP_XXXX_2009_Family_nar.KiPP.041 (001.058)</ta>
            <ta e="T291" id="Seg_3402" s="T286">KiPP_XXXX_2009_Family_nar.KiPP.042 (001.059)</ta>
            <ta e="T298" id="Seg_3403" s="T291">KiPP_XXXX_2009_Family_nar.KiPP.043 (001.060)</ta>
            <ta e="T304" id="Seg_3404" s="T298">KiPP_XXXX_2009_Family_nar.KiPP.044 (001.061)</ta>
            <ta e="T309" id="Seg_3405" s="T304">KiPP_XXXX_2009_Family_nar.KiPP.045 (001.062)</ta>
            <ta e="T316" id="Seg_3406" s="T309">KiPP_XXXX_2009_Family_nar.KiPP.046 (001.063)</ta>
            <ta e="T327" id="Seg_3407" s="T316">KiPP_XXXX_2009_Family_nar.KiPP.047 (001.064)</ta>
            <ta e="T342" id="Seg_3408" s="T327">KiPP_XXXX_2009_Family_nar.KiPP.048 (001.065)</ta>
            <ta e="T354" id="Seg_3409" s="T342">KiPP_XXXX_2009_Family_nar.KiPP.049 (001.066)</ta>
            <ta e="T367" id="Seg_3410" s="T354">KiPP_XXXX_2009_Family_nar.KiPP.050 (001.067)</ta>
            <ta e="T369" id="Seg_3411" s="T367">KiPP_XXXX_2009_Family_nar.KiPP.051 (001.068)</ta>
            <ta e="T375" id="Seg_3412" s="T369">KiPP_XXXX_2009_Family_nar.KiPP.052 (001.069)</ta>
            <ta e="T377" id="Seg_3413" s="T375">KiPP_XXXX_2009_Family_nar.KiPP.053 (001.070)</ta>
            <ta e="T386" id="Seg_3414" s="T377">KiPP_XXXX_2009_Family_nar.KiPP.054 (001.071)</ta>
            <ta e="T390" id="Seg_3415" s="T386">KiPP_XXXX_2009_Family_nar.KiPP.055 (001.072)</ta>
            <ta e="T400" id="Seg_3416" s="T390">KiPP_XXXX_2009_Family_nar.KiPP.056 (001.073)</ta>
            <ta e="T406" id="Seg_3417" s="T400">KiPP_XXXX_2009_Family_nar.KiPP.057 (001.074)</ta>
            <ta e="T414" id="Seg_3418" s="T409">KiPP_XXXX_2009_Family_nar.KiPP.058 (001.076)</ta>
            <ta e="T416" id="Seg_3419" s="T414">KiPP_XXXX_2009_Family_nar.KiPP.059 (001.077)</ta>
            <ta e="T430" id="Seg_3420" s="T416">KiPP_XXXX_2009_Family_nar.KiPP.060 (001.078)</ta>
            <ta e="T433" id="Seg_3421" s="T430">KiPP_XXXX_2009_Family_nar.KiPP.061 (001.079)</ta>
            <ta e="T436" id="Seg_3422" s="T433">KiPP_XXXX_2009_Family_nar.KiPP.062 (001.080)</ta>
            <ta e="T450" id="Seg_3423" s="T436">KiPP_XXXX_2009_Family_nar.KiPP.063 (001.081)</ta>
            <ta e="T454" id="Seg_3424" s="T450">KiPP_XXXX_2009_Family_nar.KiPP.064 (001.082)</ta>
            <ta e="T463" id="Seg_3425" s="T454">KiPP_XXXX_2009_Family_nar.KiPP.065 (001.083)</ta>
            <ta e="T467" id="Seg_3426" s="T463">KiPP_XXXX_2009_Family_nar.KiPP.066 (001.084)</ta>
            <ta e="T471" id="Seg_3427" s="T469">KiPP_XXXX_2009_Family_nar.KiPP.067 (001.086)</ta>
            <ta e="T479" id="Seg_3428" s="T471">KiPP_XXXX_2009_Family_nar.KiPP.068 (001.087)</ta>
            <ta e="T482" id="Seg_3429" s="T479">KiPP_XXXX_2009_Family_nar.KiPP.069 (001.088)</ta>
            <ta e="T490" id="Seg_3430" s="T482">KiPP_XXXX_2009_Family_nar.KiPP.070 (001.089)</ta>
            <ta e="T494" id="Seg_3431" s="T490">KiPP_XXXX_2009_Family_nar.KiPP.071 (001.090)</ta>
            <ta e="T501" id="Seg_3432" s="T496">KiPP_XXXX_2009_Family_nar.KiPP.072 (001.092)</ta>
            <ta e="T519" id="Seg_3433" s="T506">KiPP_XXXX_2009_Family_nar.KiPP.073 (001.094)</ta>
            <ta e="T520" id="Seg_3434" s="T519">KiPP_XXXX_2009_Family_nar.KiPP.074 (001.095)</ta>
            <ta e="T530" id="Seg_3435" s="T520">KiPP_XXXX_2009_Family_nar.KiPP.075 (001.096)</ta>
            <ta e="T531" id="Seg_3436" s="T530">KiPP_XXXX_2009_Family_nar.KiPP.076 (001.097)</ta>
            <ta e="T541" id="Seg_3437" s="T531">KiPP_XXXX_2009_Family_nar.KiPP.077 (001.098)</ta>
            <ta e="T545" id="Seg_3438" s="T541">KiPP_XXXX_2009_Family_nar.KiPP.078 (001.099)</ta>
            <ta e="T551" id="Seg_3439" s="T545">KiPP_XXXX_2009_Family_nar.KiPP.079 (001.100)</ta>
            <ta e="T566" id="Seg_3440" s="T551">KiPP_XXXX_2009_Family_nar.KiPP.080 (001.101)</ta>
            <ta e="T567" id="Seg_3441" s="T566">KiPP_XXXX_2009_Family_nar.KiPP.081 (001.102)</ta>
            <ta e="T571" id="Seg_3442" s="T567">KiPP_XXXX_2009_Family_nar.KiPP.082 (001.103)</ta>
            <ta e="T578" id="Seg_3443" s="T571">KiPP_XXXX_2009_Family_nar.KiPP.083 (001.104)</ta>
            <ta e="T580" id="Seg_3444" s="T578">KiPP_XXXX_2009_Family_nar.KiPP.084 (001.105)</ta>
            <ta e="T582" id="Seg_3445" s="T580">KiPP_XXXX_2009_Family_nar.KiPP.085 (001.106)</ta>
            <ta e="T588" id="Seg_3446" s="T582">KiPP_XXXX_2009_Family_nar.KiPP.086 (001.107)</ta>
            <ta e="T591" id="Seg_3447" s="T588">KiPP_XXXX_2009_Family_nar.KiPP.087 (001.108)</ta>
            <ta e="T600" id="Seg_3448" s="T591">KiPP_XXXX_2009_Family_nar.KiPP.088 (001.109)</ta>
            <ta e="T606" id="Seg_3449" s="T600">KiPP_XXXX_2009_Family_nar.KiPP.089 (001.110)</ta>
            <ta e="T610" id="Seg_3450" s="T606">KiPP_XXXX_2009_Family_nar.KiPP.090 (001.111)</ta>
            <ta e="T611" id="Seg_3451" s="T610">KiPP_XXXX_2009_Family_nar.KiPP.091 (001.112)</ta>
            <ta e="T616" id="Seg_3452" s="T611">KiPP_XXXX_2009_Family_nar.KiPP.092 (001.113)</ta>
            <ta e="T618" id="Seg_3453" s="T616">KiPP_XXXX_2009_Family_nar.KiPP.093 (001.114)</ta>
            <ta e="T621" id="Seg_3454" s="T618">KiPP_XXXX_2009_Family_nar.KiPP.094 (001.115)</ta>
            <ta e="T624" id="Seg_3455" s="T621">KiPP_XXXX_2009_Family_nar.KiPP.095 (001.116)</ta>
            <ta e="T629" id="Seg_3456" s="T624">KiPP_XXXX_2009_Family_nar.KiPP.096 (001.117)</ta>
            <ta e="T630" id="Seg_3457" s="T629">KiPP_XXXX_2009_Family_nar.KiPP.097 (001.118)</ta>
            <ta e="T634" id="Seg_3458" s="T630">KiPP_XXXX_2009_Family_nar.KiPP.098 (001.119)</ta>
            <ta e="T636" id="Seg_3459" s="T634">KiPP_XXXX_2009_Family_nar.KiPP.099 (001.120)</ta>
            <ta e="T646" id="Seg_3460" s="T636">KiPP_XXXX_2009_Family_nar.KiPP.100 (001.121)</ta>
            <ta e="T649" id="Seg_3461" s="T646">KiPP_XXXX_2009_Family_nar.KiPP.101 (001.122)</ta>
            <ta e="T657" id="Seg_3462" s="T649">KiPP_XXXX_2009_Family_nar.KiPP.102 (001.123)</ta>
            <ta e="T661" id="Seg_3463" s="T657">KiPP_XXXX_2009_Family_nar.KiPP.103 (001.124)</ta>
            <ta e="T664" id="Seg_3464" s="T661">KiPP_XXXX_2009_Family_nar.KiPP.104 (001.125)</ta>
            <ta e="T667" id="Seg_3465" s="T664">KiPP_XXXX_2009_Family_nar.KiPP.105 (001.126)</ta>
            <ta e="T671" id="Seg_3466" s="T670">KiPP_XXXX_2009_Family_nar.KiPP.106 (001.128)</ta>
         </annotation>
         <annotation name="st" tierref="st-KiPP" />
         <annotation name="ts" tierref="ts-KiPP">
            <ta e="T4" id="Seg_3467" s="T1">– ((NOISE)) Muntan isteːčči du͡o? </ta>
            <ta e="T9" id="Seg_3468" s="T6">– Heː. </ta>
            <ta e="T20" id="Seg_3469" s="T14">– Dʼe tugu ɨjɨtar ke do? </ta>
            <ta e="T25" id="Seg_3470" s="T20">Maŋnaj lʼuboppɨn ɨjɨtalɨːr du͡o. </ta>
            <ta e="T31" id="Seg_3471" s="T27">– (I-) ((LAUGH)) ister ((LAUGH)). </ta>
            <ta e="T33" id="Seg_3472" s="T31">Eː. </ta>
            <ta e="T51" id="Seg_3473" s="T49">– Jeːrakata. </ta>
            <ta e="T57" id="Seg_3474" s="T51">Kačča bütü͡ömüj ol ki͡ehege da di͡eri. </ta>
            <ta e="T62" id="Seg_3475" s="T57">((LAUGH)). </ta>
            <ta e="T68" id="Seg_3476" s="T66">– Korgoː. </ta>
            <ta e="T73" id="Seg_3477" s="T70">– Korgoːgo töröːbütüm. </ta>
            <ta e="T76" id="Seg_3478" s="T73">Hubunna, bejem hirber. </ta>
            <ta e="T77" id="Seg_3479" s="T76">Bulunnʼabɨn. </ta>
            <ta e="T81" id="Seg_3480" s="T77">Bilegin, bulunnʼa aːtɨn ((LAUGH))? </ta>
            <ta e="T92" id="Seg_3481" s="T89">– Bʼez atca ((LAUGH)). </ta>
            <ta e="T108" id="Seg_3482" s="T106">– Teːtete hu͡ok. </ta>
            <ta e="T117" id="Seg_3483" s="T111">– Dʼe min baːbuskam iːtte. </ta>
            <ta e="T124" id="Seg_3484" s="T120">– Baːbuska vaspʼitɨvajdaːbɨta, maːmam maːmata. </ta>
            <ta e="T139" id="Seg_3485" s="T128">– Onton bihigi kergetter barɨta ölönnör, tulaːjak hɨldʼɨbɨppɨt emeːksini gɨtta baːbuskabɨn gɨtta. </ta>
            <ta e="T144" id="Seg_3486" s="T139">Onton (Rɨb-) Rɨbnaj di͡ek barbɨtɨm. </ta>
            <ta e="T158" id="Seg_3487" s="T144">Onton ol barbɨppɨt (i-) ebebin, inʼebin gɨtta, inʼe di͡eččibin ontubun, maːmam maːmam hin biːr. </ta>
            <ta e="T167" id="Seg_3488" s="T158">Dʼe, onton töttörü kelbippit, maːmam, emeːksin (baː-) baːbuskam ölörüger. </ta>
            <ta e="T175" id="Seg_3489" s="T167">Ölön baran tillibite, ol dojduga onton töttörü kelbippit. </ta>
            <ta e="T184" id="Seg_3490" s="T175">Ol kelbippit kenne u͡on alta dʼɨllaːkpar minigin erge bi͡erbite. </ta>
            <ta e="T190" id="Seg_3491" s="T184">"Min öllöppüne enigin kulut gɨnɨ͡aktara", di͡ete. </ta>
            <ta e="T200" id="Seg_3492" s="T190">Čort, tuːgu da ɨraːspɨn haːtar, huːna da ilikpin erge barbɨtɨm. </ta>
            <ta e="T204" id="Seg_3493" s="T200">ɨtɨː-ɨtɨː erge bi͡ereller, ututallar. </ta>
            <ta e="T210" id="Seg_3494" s="T204">ɨtɨː-ɨtɨː utujabɨn, erbitten kuttanabɨn, taŋastɨːn utujabɨn. </ta>
            <ta e="T212" id="Seg_3495" s="T210">Eː dʼe. </ta>
            <ta e="T217" id="Seg_3496" s="T212">Onton dʼɨlɨ, dʼɨlɨ taŋastaːk utujbutum. </ta>
            <ta e="T225" id="Seg_3497" s="T217">Onton, dʼe kojut ü͡örenebin bu͡o, (er-) (er-) erber. </ta>
            <ta e="T232" id="Seg_3498" s="T225">Ol aːta pʼervaja lʼubovʼ, patom, celɨj god. </ta>
            <ta e="T240" id="Seg_3499" s="T232">U͡on hette u͡on agɨs u͡on agɨs dʼɨllaːkpar ogolommutum. </ta>
            <ta e="T244" id="Seg_3500" s="T240">Pi͡erbej ogom munna baːr. </ta>
            <ta e="T254" id="Seg_3501" s="T244">Onton ogolonnum, ogolonnum, dʼɨl aːjɨ ogolonobun, dʼɨl aːjɨ ogolonobun. </ta>
            <ta e="T265" id="Seg_3502" s="T258">– Eː, hubu hirge, tɨ͡aga hɨldʼammɨt, tɨ͡aga etibit. </ta>
            <ta e="T268" id="Seg_3503" s="T265">Ogonnʼorum kassʼiːr ete. </ta>
            <ta e="T275" id="Seg_3504" s="T268">Hürdeːk üčügej kihi ete, spasoːbnɨj bagajɨ bulčut. </ta>
            <ta e="T278" id="Seg_3505" s="T275">Buldu daː bultaːbɨta. </ta>
            <ta e="T281" id="Seg_3506" s="T278">Dʼi͡e da tupputa. </ta>
            <ta e="T286" id="Seg_3507" s="T281">Kassʼiːrga u͡on bi͡es dʼɨlɨ olorbuta. </ta>
            <ta e="T291" id="Seg_3508" s="T286">Onton biːr dʼɨlɨ partorgaga olorbuta. </ta>
            <ta e="T298" id="Seg_3509" s="T291">Onton dʼi͡eleri tupputa, bu turar tupput dʼi͡elere. </ta>
            <ta e="T304" id="Seg_3510" s="T298">Onton, u͡on ikki ogoloːkput onton, barɨta. </ta>
            <ta e="T309" id="Seg_3511" s="T304">Biːr de kihini kihi͡eke bi͡erbetegim ogolorbun. </ta>
            <ta e="T316" id="Seg_3512" s="T309">Bejem iːti͡ek bu͡olammɨn kihi͡eke bi͡erbeppin, ahɨnabɨn ogolorbun. </ta>
            <ta e="T327" id="Seg_3513" s="T316">Olorbun iːtine iːtibitinen bihikteːk ogoloːk ilimneheːččibin ogonnʼorbun gɨtta ɨ͡arɨj bu͡olbutun kenne. </ta>
            <ta e="T342" id="Seg_3514" s="T327">Dʼe ogolorbut ulaːtannar ü͡örene barɨtalaːbɨttara, onton kojut ikki ulakan ogom taba ü͡öregiger barbɨttara, u͡olbut kɨːspɨt. </ta>
            <ta e="T354" id="Seg_3515" s="T342">Onton biːr ogobut ulaːtan Krasnajarska ü͡örene barbɨta, anɨ tʼerapʼevka üleliː hɨtar Dudʼinkaga. </ta>
            <ta e="T367" id="Seg_3516" s="T354">Onton biːr biːr ogom bu͡ollagɨna emi͡e ü͡örenen ikki institutu büppüte, onton Krasnajarskajga baːr. </ta>
            <ta e="T369" id="Seg_3517" s="T367">(Sʼer-) Sʼergʼej. </ta>
            <ta e="T375" id="Seg_3518" s="T369">Ontum dʼaktardaːk ogo ol, ikki u͡ollaːk. </ta>
            <ta e="T377" id="Seg_3519" s="T375">Kazaːktartan dʼaktardammɨt. </ta>
            <ta e="T386" id="Seg_3520" s="T377">(Araŋastaːktan), anɨ bu͡ollagɨna axranaga axranaga üleliːr, mʼilʼisʼijanɨ büten baran. </ta>
            <ta e="T390" id="Seg_3521" s="T386">Urut taba ülehite bu͡olbuta. </ta>
            <ta e="T400" id="Seg_3522" s="T390">Onton biːr ogom poːvarga Nosku͡oga üleliːr, glavnaj poːvar, biːr kɨːhɨm. </ta>
            <ta e="T406" id="Seg_3523" s="T400">Biːr (kɨ-) tu͡ok, (Nas-) Nasku͡oga Marʼina. </ta>
            <ta e="T414" id="Seg_3524" s="T409">– Hu͡o, ü͡öremmite, heː, ol di͡et. </ta>
            <ta e="T416" id="Seg_3525" s="T414">Krasnajarskajga ü͡öremmite. </ta>
            <ta e="T430" id="Seg_3526" s="T416">Onton (ma-) du͡oktuːr kɨːhɨm Dudʼinkaga üleliːr, tʼerapʼevka üleliːr anɨ, üs ogoloːk, kɨrdʼagas dʼaktar pʼensʼijalaːk. </ta>
            <ta e="T433" id="Seg_3527" s="T430">Ogom barɨta pʼensʼijalaːk. ((LAUGH))</ta>
            <ta e="T436" id="Seg_3528" s="T433">Üs ogom pʼensʼijalaːktar. </ta>
            <ta e="T450" id="Seg_3529" s="T436">Ulakattarɨm anɨ, biːrgehe bu͡ollagɨna rɨbaktɨːr, biːrgehe bu͡olla anɨ haːhɨrbɨt pʼensʼijalaːk, Bi͡ere, ere hubukaːn ölbüte. </ta>
            <ta e="T454" id="Seg_3530" s="T450">Ikki ogoloːk, ühüstere ölbüte. </ta>
            <ta e="T463" id="Seg_3531" s="T454">Ulakan u͡olum üs ogoloːk, ogo, biːr ogoto dʼaktardaːk, ogoloːk. </ta>
            <ta e="T467" id="Seg_3532" s="T463">Aŋardara ogolono ilikter. </ta>
            <ta e="T471" id="Seg_3533" s="T469">– Heː. </ta>
            <ta e="T479" id="Seg_3534" s="T471">Onton biːr ogom ke kimi͡eke baːr, Hoːpstajga baːr. </ta>
            <ta e="T482" id="Seg_3535" s="T479">Kimi͡eke, vaspʼitatʼelga üleliːr. </ta>
            <ta e="T490" id="Seg_3536" s="T482">Onton biːr ogom munna, biːr kɨːhɨm munna vaspʼitatʼeliːr. </ta>
            <ta e="T494" id="Seg_3537" s="T490">Biːr ogom du͡o… </ta>
            <ta e="T501" id="Seg_3538" s="T496">– Heː, sku͡olaga munna, Du͡omna Pʼetrovna. </ta>
            <ta e="T519" id="Seg_3539" s="T506">– Eː, onton biːr ɨlgɨn u͡olum dʼaktara emi͡e munna učitallɨːr, Naːdʼa Kʼirgʼizava i͡e. </ta>
            <ta e="T520" id="Seg_3540" s="T519">Nʼevʼestka. </ta>
            <ta e="T530" id="Seg_3541" s="T520">Onton biːr u͡olum bu͡ollagɨna kim, bʼezdʼexotčʼik büppüte, ü͡örenen üleliːr ete. </ta>
            <ta e="T531" id="Seg_3542" s="T530">Iːgireler. </ta>
            <ta e="T541" id="Seg_3543" s="T531">Biːrgehe elʼektrʼik iti üleliː hɨldʼar anɨga di͡eri ü͡örenen büten baran. </ta>
            <ta e="T545" id="Seg_3544" s="T541">Tak da barɨta ü͡öremmittere. </ta>
            <ta e="T551" id="Seg_3545" s="T545">Onton ke biːr ogom Kiri͡eska baːr. </ta>
            <ta e="T566" id="Seg_3546" s="T551">(Han-) eː mʼilʼisʼije ü͡öregin büppüt ete, ontuŋ hir aːjɨ hiri baratan baratan baran, Kiri͡eske dʼaktardaːk. </ta>
            <ta e="T567" id="Seg_3547" s="T566">Alʼeksʼej. </ta>
            <ta e="T571" id="Seg_3548" s="T567">Kördüŋ itte eni araːha. </ta>
            <ta e="T578" id="Seg_3549" s="T571">Onton aŋardara bulčuttar üs u͡olum, bulčuttar bu͡olla. </ta>
            <ta e="T580" id="Seg_3550" s="T578">Ölü͡öse, Hergeːj. </ta>
            <ta e="T582" id="Seg_3551" s="T580">Miːse, Biːtʼe. </ta>
            <ta e="T588" id="Seg_3552" s="T582">Biːtʼe utuja hɨtar, dʼaktara bu turar. </ta>
            <ta e="T591" id="Seg_3553" s="T588">Elete du͡o onton. </ta>
            <ta e="T600" id="Seg_3554" s="T591">U͡on ikki ogoloːkpun, hürbe orduga bi͡es, eː hette bɨnuktaːkpɨn. </ta>
            <ta e="T606" id="Seg_3555" s="T600">Oččogo dʼirʼevnʼijabit innʼe tü͡örd-u͡ontan taksa kihi. </ta>
            <ta e="T610" id="Seg_3556" s="T606">Kiniːtterbin barɨtɨn bullakpɨna elbekter. </ta>
            <ta e="T611" id="Seg_3557" s="T610">Aːktakpɨna. </ta>
            <ta e="T616" id="Seg_3558" s="T611">Biːr kiniːt, ikki kiniːt pʼedagoːgtar. </ta>
            <ta e="T618" id="Seg_3559" s="T616">Ogolordoːktor barɨlara. </ta>
            <ta e="T621" id="Seg_3560" s="T618">Kɨrgɨttara ü͡örene barbɨttara. </ta>
            <ta e="T624" id="Seg_3561" s="T621">Biːr kiniːtim saːdʼikka. </ta>
            <ta e="T629" id="Seg_3562" s="T624">Kimnere, kim etej ke dogo? </ta>
            <ta e="T630" id="Seg_3563" s="T629">Zabʼedusajdar. </ta>
            <ta e="T634" id="Seg_3564" s="T630">Ikki kiniːtim učiːtallar, pʼedagogtar. </ta>
            <ta e="T636" id="Seg_3565" s="T634">Barɨta ülehitter. </ta>
            <ta e="T646" id="Seg_3566" s="T636">Onton biːr kütü͡ötüm iti taŋaralaːta, biːr kütü͡ötüm ɨ͡arɨj bu͡olbut, ogo. </ta>
            <ta e="T649" id="Seg_3567" s="T646">Domna Pʼetroːvna gi͡ene. </ta>
            <ta e="T657" id="Seg_3568" s="T649">Ol ereːti buldu delbi ölörör, ataga hu͡ok bejete. </ta>
            <ta e="T661" id="Seg_3569" s="T657">Biːr kütü͡ötüm Hu͡opostajga axotnʼik. </ta>
            <ta e="T664" id="Seg_3570" s="T661">Kɨːha ü͡örene barbɨt. </ta>
            <ta e="T667" id="Seg_3571" s="T664">Dʼe tugu kepsi͡eppinij? </ta>
            <ta e="T671" id="Seg_3572" s="T670">– Dʼe. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-KiPP">
            <ta e="T2" id="Seg_3573" s="T1">mun-tan</ta>
            <ta e="T3" id="Seg_3574" s="T2">ist-eːčči</ta>
            <ta e="T4" id="Seg_3575" s="T3">du͡o</ta>
            <ta e="T9" id="Seg_3576" s="T6">heː</ta>
            <ta e="T15" id="Seg_3577" s="T14">dʼe</ta>
            <ta e="T17" id="Seg_3578" s="T15">tug-u</ta>
            <ta e="T18" id="Seg_3579" s="T17">ɨjɨt-ar</ta>
            <ta e="T19" id="Seg_3580" s="T18">ke</ta>
            <ta e="T20" id="Seg_3581" s="T19">do</ta>
            <ta e="T21" id="Seg_3582" s="T20">maŋnaj</ta>
            <ta e="T22" id="Seg_3583" s="T21">lʼubop-pɨ-n</ta>
            <ta e="T24" id="Seg_3584" s="T22">ɨjɨt-alɨː-r</ta>
            <ta e="T25" id="Seg_3585" s="T24">du͡o</ta>
            <ta e="T30" id="Seg_3586" s="T29">ist-er</ta>
            <ta e="T33" id="Seg_3587" s="T31">eː</ta>
            <ta e="T51" id="Seg_3588" s="T49">jeːrakata</ta>
            <ta e="T52" id="Seg_3589" s="T51">kačča</ta>
            <ta e="T53" id="Seg_3590" s="T52">büt-ü͡ö-m=üj</ta>
            <ta e="T54" id="Seg_3591" s="T53">ol</ta>
            <ta e="T55" id="Seg_3592" s="T54">ki͡ehe-ge</ta>
            <ta e="T56" id="Seg_3593" s="T55">da</ta>
            <ta e="T57" id="Seg_3594" s="T56">di͡eri</ta>
            <ta e="T68" id="Seg_3595" s="T66">Korgoː</ta>
            <ta e="T72" id="Seg_3596" s="T70">Korgoː-go</ta>
            <ta e="T73" id="Seg_3597" s="T72">töröː-büt-ü-m</ta>
            <ta e="T74" id="Seg_3598" s="T73">hu-bunna</ta>
            <ta e="T75" id="Seg_3599" s="T74">beje-m</ta>
            <ta e="T76" id="Seg_3600" s="T75">hir-be-r</ta>
            <ta e="T77" id="Seg_3601" s="T76">bulunnʼa-bɨn</ta>
            <ta e="T78" id="Seg_3602" s="T77">bil-e-gin</ta>
            <ta e="T79" id="Seg_3603" s="T78">bulunnʼa</ta>
            <ta e="T80" id="Seg_3604" s="T79">aːt-ɨ-n</ta>
            <ta e="T107" id="Seg_3605" s="T106">teːte-te</ta>
            <ta e="T108" id="Seg_3606" s="T107">hu͡ok</ta>
            <ta e="T112" id="Seg_3607" s="T111">dʼe</ta>
            <ta e="T113" id="Seg_3608" s="T112">min</ta>
            <ta e="T115" id="Seg_3609" s="T113">baːbuska-m</ta>
            <ta e="T117" id="Seg_3610" s="T115">iːt-t-e</ta>
            <ta e="T121" id="Seg_3611" s="T120">baːbuska</ta>
            <ta e="T122" id="Seg_3612" s="T121">vaspʼitɨvaj-daː-bɨt-a</ta>
            <ta e="T123" id="Seg_3613" s="T122">maːma-m</ta>
            <ta e="T124" id="Seg_3614" s="T123">maːma-ta</ta>
            <ta e="T129" id="Seg_3615" s="T128">onton</ta>
            <ta e="T130" id="Seg_3616" s="T129">bihigi</ta>
            <ta e="T131" id="Seg_3617" s="T130">kerget-ter</ta>
            <ta e="T132" id="Seg_3618" s="T131">barɨta</ta>
            <ta e="T133" id="Seg_3619" s="T132">öl-ön-nör</ta>
            <ta e="T134" id="Seg_3620" s="T133">tulaːjak</ta>
            <ta e="T135" id="Seg_3621" s="T134">hɨldʼ-ɨ-bɨp-pɨt</ta>
            <ta e="T136" id="Seg_3622" s="T135">emeːksin-i</ta>
            <ta e="T137" id="Seg_3623" s="T136">gɨtta</ta>
            <ta e="T138" id="Seg_3624" s="T137">baːbuska-bɨ-n</ta>
            <ta e="T139" id="Seg_3625" s="T138">gɨtta</ta>
            <ta e="T140" id="Seg_3626" s="T139">onton</ta>
            <ta e="T142" id="Seg_3627" s="T141">Rɨbnaj</ta>
            <ta e="T143" id="Seg_3628" s="T142">di͡ek</ta>
            <ta e="T144" id="Seg_3629" s="T143">bar-bɨt-ɨ-m</ta>
            <ta e="T145" id="Seg_3630" s="T144">onton</ta>
            <ta e="T146" id="Seg_3631" s="T145">ol</ta>
            <ta e="T147" id="Seg_3632" s="T146">bar-bɨp-pɨt</ta>
            <ta e="T149" id="Seg_3633" s="T148">ebe-bi-n</ta>
            <ta e="T150" id="Seg_3634" s="T149">inʼe-bi-n</ta>
            <ta e="T151" id="Seg_3635" s="T150">gɨtta</ta>
            <ta e="T152" id="Seg_3636" s="T151">inʼe</ta>
            <ta e="T153" id="Seg_3637" s="T152">di͡e-čči-bin</ta>
            <ta e="T154" id="Seg_3638" s="T153">on-tu-bu-n</ta>
            <ta e="T155" id="Seg_3639" s="T154">maːma-m</ta>
            <ta e="T156" id="Seg_3640" s="T155">maːma-m</ta>
            <ta e="T157" id="Seg_3641" s="T156">hin</ta>
            <ta e="T158" id="Seg_3642" s="T157">biːr</ta>
            <ta e="T159" id="Seg_3643" s="T158">dʼe</ta>
            <ta e="T160" id="Seg_3644" s="T159">onton</ta>
            <ta e="T161" id="Seg_3645" s="T160">töttörü</ta>
            <ta e="T162" id="Seg_3646" s="T161">kel-bip-pit</ta>
            <ta e="T163" id="Seg_3647" s="T162">maːma-m</ta>
            <ta e="T164" id="Seg_3648" s="T163">emeːksin</ta>
            <ta e="T166" id="Seg_3649" s="T165">baːbuska-m</ta>
            <ta e="T167" id="Seg_3650" s="T166">öl-ör-ü-ger</ta>
            <ta e="T168" id="Seg_3651" s="T167">öl-ön</ta>
            <ta e="T169" id="Seg_3652" s="T168">baran</ta>
            <ta e="T170" id="Seg_3653" s="T169">till-i-bit-e</ta>
            <ta e="T171" id="Seg_3654" s="T170">ol</ta>
            <ta e="T172" id="Seg_3655" s="T171">dojdu-ga</ta>
            <ta e="T173" id="Seg_3656" s="T172">onton</ta>
            <ta e="T174" id="Seg_3657" s="T173">töttörü</ta>
            <ta e="T175" id="Seg_3658" s="T174">kel-bip-pit</ta>
            <ta e="T176" id="Seg_3659" s="T175">ol</ta>
            <ta e="T177" id="Seg_3660" s="T176">kel-bip-pit</ta>
            <ta e="T178" id="Seg_3661" s="T177">kenne</ta>
            <ta e="T179" id="Seg_3662" s="T178">u͡on</ta>
            <ta e="T180" id="Seg_3663" s="T179">alta</ta>
            <ta e="T181" id="Seg_3664" s="T180">dʼɨl-laːk-pa-r</ta>
            <ta e="T182" id="Seg_3665" s="T181">minigi-n</ta>
            <ta e="T183" id="Seg_3666" s="T182">er-ge</ta>
            <ta e="T184" id="Seg_3667" s="T183">bi͡er-bit-e</ta>
            <ta e="T185" id="Seg_3668" s="T184">min</ta>
            <ta e="T186" id="Seg_3669" s="T185">öl-löp-püne</ta>
            <ta e="T187" id="Seg_3670" s="T186">enigi-n</ta>
            <ta e="T188" id="Seg_3671" s="T187">kulut</ta>
            <ta e="T189" id="Seg_3672" s="T188">gɨn-ɨ͡ak-tara</ta>
            <ta e="T190" id="Seg_3673" s="T189">di͡e-t-e</ta>
            <ta e="T191" id="Seg_3674" s="T190">čort</ta>
            <ta e="T192" id="Seg_3675" s="T191">tuːg-u</ta>
            <ta e="T193" id="Seg_3676" s="T192">da</ta>
            <ta e="T194" id="Seg_3677" s="T193">ɨraːs-pɨn</ta>
            <ta e="T195" id="Seg_3678" s="T194">haːtar</ta>
            <ta e="T196" id="Seg_3679" s="T195">huːn-a</ta>
            <ta e="T197" id="Seg_3680" s="T196">da</ta>
            <ta e="T198" id="Seg_3681" s="T197">ilik-pin</ta>
            <ta e="T199" id="Seg_3682" s="T198">er-ge</ta>
            <ta e="T200" id="Seg_3683" s="T199">bar-bɨt-ɨ-m</ta>
            <ta e="T201" id="Seg_3684" s="T200">ɨt-ɨː-ɨt-ɨː</ta>
            <ta e="T202" id="Seg_3685" s="T201">er-ge</ta>
            <ta e="T203" id="Seg_3686" s="T202">bi͡er-el-ler</ta>
            <ta e="T204" id="Seg_3687" s="T203">utu-t-al-lar</ta>
            <ta e="T205" id="Seg_3688" s="T204">ɨt-ɨː-ɨt-ɨː</ta>
            <ta e="T206" id="Seg_3689" s="T205">utuj-a-bɨn</ta>
            <ta e="T207" id="Seg_3690" s="T206">er-bi-tten</ta>
            <ta e="T208" id="Seg_3691" s="T207">kuttan-a-bɨn</ta>
            <ta e="T209" id="Seg_3692" s="T208">taŋas-tɨːn</ta>
            <ta e="T210" id="Seg_3693" s="T209">utuj-a-bɨn</ta>
            <ta e="T211" id="Seg_3694" s="T210">eː</ta>
            <ta e="T212" id="Seg_3695" s="T211">dʼe</ta>
            <ta e="T213" id="Seg_3696" s="T212">onton</ta>
            <ta e="T214" id="Seg_3697" s="T213">dʼɨl-ɨ</ta>
            <ta e="T215" id="Seg_3698" s="T214">dʼɨl-ɨ</ta>
            <ta e="T216" id="Seg_3699" s="T215">taŋas-taːk</ta>
            <ta e="T217" id="Seg_3700" s="T216">utuj-but-u-m</ta>
            <ta e="T218" id="Seg_3701" s="T217">onton</ta>
            <ta e="T219" id="Seg_3702" s="T218">dʼe</ta>
            <ta e="T220" id="Seg_3703" s="T219">kojut</ta>
            <ta e="T221" id="Seg_3704" s="T220">ü͡ören-e-bin</ta>
            <ta e="T222" id="Seg_3705" s="T221">bu͡o</ta>
            <ta e="T223" id="Seg_3706" s="T222">er</ta>
            <ta e="T224" id="Seg_3707" s="T223">er</ta>
            <ta e="T225" id="Seg_3708" s="T224">er-be-r</ta>
            <ta e="T226" id="Seg_3709" s="T225">ol</ta>
            <ta e="T227" id="Seg_3710" s="T226">aːt-a</ta>
            <ta e="T233" id="Seg_3711" s="T232">u͡on</ta>
            <ta e="T234" id="Seg_3712" s="T233">hette</ta>
            <ta e="T235" id="Seg_3713" s="T234">u͡on</ta>
            <ta e="T236" id="Seg_3714" s="T235">agɨs</ta>
            <ta e="T237" id="Seg_3715" s="T236">u͡on</ta>
            <ta e="T238" id="Seg_3716" s="T237">agɨs</ta>
            <ta e="T239" id="Seg_3717" s="T238">dʼɨl-laːk-pa-r</ta>
            <ta e="T240" id="Seg_3718" s="T239">ogo-lom-mut-u-m</ta>
            <ta e="T241" id="Seg_3719" s="T240">pi͡erbej</ta>
            <ta e="T242" id="Seg_3720" s="T241">ogo-m</ta>
            <ta e="T243" id="Seg_3721" s="T242">munna</ta>
            <ta e="T244" id="Seg_3722" s="T243">baːr</ta>
            <ta e="T245" id="Seg_3723" s="T244">onton</ta>
            <ta e="T246" id="Seg_3724" s="T245">ogo-lon-nu-m</ta>
            <ta e="T247" id="Seg_3725" s="T246">ogo-lon-nu-m</ta>
            <ta e="T248" id="Seg_3726" s="T247">dʼɨl</ta>
            <ta e="T249" id="Seg_3727" s="T248">aːjɨ</ta>
            <ta e="T250" id="Seg_3728" s="T249">ogo-lon-o-bun</ta>
            <ta e="T251" id="Seg_3729" s="T250">dʼɨl</ta>
            <ta e="T252" id="Seg_3730" s="T251">aːjɨ</ta>
            <ta e="T254" id="Seg_3731" s="T252">ogo-lon-o-bun</ta>
            <ta e="T259" id="Seg_3732" s="T258">eː</ta>
            <ta e="T260" id="Seg_3733" s="T259">hubu</ta>
            <ta e="T261" id="Seg_3734" s="T260">hir-ge</ta>
            <ta e="T262" id="Seg_3735" s="T261">tɨ͡a-ga</ta>
            <ta e="T263" id="Seg_3736" s="T262">hɨldʼ-am-mɨt</ta>
            <ta e="T264" id="Seg_3737" s="T263">tɨ͡a-ga</ta>
            <ta e="T265" id="Seg_3738" s="T264">e-ti-bit</ta>
            <ta e="T266" id="Seg_3739" s="T265">ogonnʼor-u-m</ta>
            <ta e="T267" id="Seg_3740" s="T266">kassʼiːr</ta>
            <ta e="T268" id="Seg_3741" s="T267">e-t-e</ta>
            <ta e="T269" id="Seg_3742" s="T268">hürdeːk</ta>
            <ta e="T270" id="Seg_3743" s="T269">üčügej</ta>
            <ta e="T271" id="Seg_3744" s="T270">kihi</ta>
            <ta e="T272" id="Seg_3745" s="T271">e-t-e</ta>
            <ta e="T273" id="Seg_3746" s="T272">spasoːbnɨj</ta>
            <ta e="T274" id="Seg_3747" s="T273">bagajɨ</ta>
            <ta e="T275" id="Seg_3748" s="T274">bulčut</ta>
            <ta e="T276" id="Seg_3749" s="T275">buld-u</ta>
            <ta e="T277" id="Seg_3750" s="T276">daː</ta>
            <ta e="T278" id="Seg_3751" s="T277">bultaː-bɨt-a</ta>
            <ta e="T279" id="Seg_3752" s="T278">dʼi͡e</ta>
            <ta e="T280" id="Seg_3753" s="T279">da</ta>
            <ta e="T281" id="Seg_3754" s="T280">tup-put-a</ta>
            <ta e="T282" id="Seg_3755" s="T281">kassʼiːr-ga</ta>
            <ta e="T283" id="Seg_3756" s="T282">u͡on</ta>
            <ta e="T284" id="Seg_3757" s="T283">bi͡es</ta>
            <ta e="T285" id="Seg_3758" s="T284">dʼɨl-ɨ</ta>
            <ta e="T286" id="Seg_3759" s="T285">olor-but-a</ta>
            <ta e="T287" id="Seg_3760" s="T286">onton</ta>
            <ta e="T288" id="Seg_3761" s="T287">biːr</ta>
            <ta e="T289" id="Seg_3762" s="T288">dʼɨl-ɨ</ta>
            <ta e="T290" id="Seg_3763" s="T289">partorga-ga</ta>
            <ta e="T291" id="Seg_3764" s="T290">olor-but-a</ta>
            <ta e="T292" id="Seg_3765" s="T291">onton</ta>
            <ta e="T293" id="Seg_3766" s="T292">dʼi͡e-ler-i</ta>
            <ta e="T294" id="Seg_3767" s="T293">tup-put-a</ta>
            <ta e="T295" id="Seg_3768" s="T294">bu</ta>
            <ta e="T296" id="Seg_3769" s="T295">tur-ar</ta>
            <ta e="T297" id="Seg_3770" s="T296">tup-put</ta>
            <ta e="T298" id="Seg_3771" s="T297">dʼi͡e-ler-e</ta>
            <ta e="T299" id="Seg_3772" s="T298">onton</ta>
            <ta e="T300" id="Seg_3773" s="T299">u͡on</ta>
            <ta e="T301" id="Seg_3774" s="T300">ikki</ta>
            <ta e="T302" id="Seg_3775" s="T301">ogo-loːk-put</ta>
            <ta e="T303" id="Seg_3776" s="T302">onton</ta>
            <ta e="T304" id="Seg_3777" s="T303">barɨta</ta>
            <ta e="T305" id="Seg_3778" s="T304">biːr</ta>
            <ta e="T672" id="Seg_3779" s="T305">de</ta>
            <ta e="T306" id="Seg_3780" s="T672">kihi-ni</ta>
            <ta e="T307" id="Seg_3781" s="T306">kihi͡e-ke</ta>
            <ta e="T308" id="Seg_3782" s="T307">bi͡er-beteg-i-m</ta>
            <ta e="T309" id="Seg_3783" s="T308">ogo-lor-bu-n</ta>
            <ta e="T310" id="Seg_3784" s="T309">beje-m</ta>
            <ta e="T311" id="Seg_3785" s="T310">iːt-i͡ek</ta>
            <ta e="T312" id="Seg_3786" s="T311">bu͡ol-am-mɨn</ta>
            <ta e="T313" id="Seg_3787" s="T312">kihi͡e-ke</ta>
            <ta e="T314" id="Seg_3788" s="T313">bi͡er-bep-pin</ta>
            <ta e="T315" id="Seg_3789" s="T314">ahɨn-a-bɨn</ta>
            <ta e="T316" id="Seg_3790" s="T315">ogo-lor-bu-n</ta>
            <ta e="T317" id="Seg_3791" s="T316">o-lor-bu-n</ta>
            <ta e="T318" id="Seg_3792" s="T317">iːt-i-n-e</ta>
            <ta e="T319" id="Seg_3793" s="T318">iːt-i-bit-i-nen</ta>
            <ta e="T320" id="Seg_3794" s="T319">bihik-teːk</ta>
            <ta e="T321" id="Seg_3795" s="T320">ogo-loːk</ta>
            <ta e="T322" id="Seg_3796" s="T321">ilim-n-e-h-eːčči-bin</ta>
            <ta e="T323" id="Seg_3797" s="T322">ogonnʼor-bu-n</ta>
            <ta e="T324" id="Seg_3798" s="T323">gɨtta</ta>
            <ta e="T325" id="Seg_3799" s="T324">ɨ͡arɨj</ta>
            <ta e="T326" id="Seg_3800" s="T325">bu͡ol-but-u-n</ta>
            <ta e="T327" id="Seg_3801" s="T326">kenne</ta>
            <ta e="T328" id="Seg_3802" s="T327">dʼe</ta>
            <ta e="T329" id="Seg_3803" s="T328">ogo-lor-but</ta>
            <ta e="T330" id="Seg_3804" s="T329">ulaːt-an-nar</ta>
            <ta e="T331" id="Seg_3805" s="T330">ü͡ören-e</ta>
            <ta e="T332" id="Seg_3806" s="T331">bar-ɨtalaː-bɨt-tara</ta>
            <ta e="T333" id="Seg_3807" s="T332">onton</ta>
            <ta e="T334" id="Seg_3808" s="T333">kojut</ta>
            <ta e="T335" id="Seg_3809" s="T334">ikki</ta>
            <ta e="T336" id="Seg_3810" s="T335">ulakan</ta>
            <ta e="T337" id="Seg_3811" s="T336">ogo-m</ta>
            <ta e="T338" id="Seg_3812" s="T337">taba</ta>
            <ta e="T339" id="Seg_3813" s="T338">ü͡öreg-i-ger</ta>
            <ta e="T340" id="Seg_3814" s="T339">bar-bɨt-tara</ta>
            <ta e="T341" id="Seg_3815" s="T340">u͡ol-but</ta>
            <ta e="T342" id="Seg_3816" s="T341">kɨːs-pɨt</ta>
            <ta e="T343" id="Seg_3817" s="T342">onton</ta>
            <ta e="T344" id="Seg_3818" s="T343">biːr</ta>
            <ta e="T345" id="Seg_3819" s="T344">ogo-but</ta>
            <ta e="T346" id="Seg_3820" s="T345">ulaːt-an</ta>
            <ta e="T347" id="Seg_3821" s="T346">Krasnajars-ka</ta>
            <ta e="T348" id="Seg_3822" s="T347">ü͡ören-e</ta>
            <ta e="T349" id="Seg_3823" s="T348">bar-bɨt-a</ta>
            <ta e="T350" id="Seg_3824" s="T349">anɨ</ta>
            <ta e="T351" id="Seg_3825" s="T350">tʼerapʼev-ka</ta>
            <ta e="T352" id="Seg_3826" s="T351">ülel-iː</ta>
            <ta e="T353" id="Seg_3827" s="T352">hɨt-ar</ta>
            <ta e="T354" id="Seg_3828" s="T353">Dudʼinka-ga</ta>
            <ta e="T355" id="Seg_3829" s="T354">onton</ta>
            <ta e="T356" id="Seg_3830" s="T355">biːr</ta>
            <ta e="T357" id="Seg_3831" s="T356">biːr</ta>
            <ta e="T358" id="Seg_3832" s="T357">ogo-m</ta>
            <ta e="T359" id="Seg_3833" s="T358">bu͡ollagɨna</ta>
            <ta e="T360" id="Seg_3834" s="T359">emi͡e</ta>
            <ta e="T361" id="Seg_3835" s="T360">ü͡ören-en</ta>
            <ta e="T362" id="Seg_3836" s="T361">ikki</ta>
            <ta e="T363" id="Seg_3837" s="T362">institut-u</ta>
            <ta e="T364" id="Seg_3838" s="T363">büp-püt-e</ta>
            <ta e="T365" id="Seg_3839" s="T364">onton</ta>
            <ta e="T366" id="Seg_3840" s="T365">Krasnajarskaj-ga</ta>
            <ta e="T367" id="Seg_3841" s="T366">baːr</ta>
            <ta e="T369" id="Seg_3842" s="T368">Sʼergʼej</ta>
            <ta e="T370" id="Seg_3843" s="T369">on-tu-m</ta>
            <ta e="T371" id="Seg_3844" s="T370">dʼaktar-daːk</ta>
            <ta e="T372" id="Seg_3845" s="T371">ogo</ta>
            <ta e="T373" id="Seg_3846" s="T372">ol</ta>
            <ta e="T374" id="Seg_3847" s="T373">ikki</ta>
            <ta e="T375" id="Seg_3848" s="T374">u͡ol-laːk</ta>
            <ta e="T376" id="Seg_3849" s="T375">kazaːk-tar-tan</ta>
            <ta e="T377" id="Seg_3850" s="T376">dʼaktar-dam-mɨt</ta>
            <ta e="T378" id="Seg_3851" s="T377">araŋas-taːk-tan</ta>
            <ta e="T379" id="Seg_3852" s="T378">anɨ</ta>
            <ta e="T380" id="Seg_3853" s="T379">bu͡ollagɨna</ta>
            <ta e="T381" id="Seg_3854" s="T380">axrana-ga</ta>
            <ta e="T382" id="Seg_3855" s="T381">axrana-ga</ta>
            <ta e="T383" id="Seg_3856" s="T382">üleliː-r</ta>
            <ta e="T384" id="Seg_3857" s="T383">mʼilʼisʼija-nɨ</ta>
            <ta e="T385" id="Seg_3858" s="T384">büt-en</ta>
            <ta e="T386" id="Seg_3859" s="T385">baran</ta>
            <ta e="T387" id="Seg_3860" s="T386">urut</ta>
            <ta e="T388" id="Seg_3861" s="T387">taba</ta>
            <ta e="T389" id="Seg_3862" s="T388">ülehit-e</ta>
            <ta e="T390" id="Seg_3863" s="T389">bu͡ol-but-a</ta>
            <ta e="T391" id="Seg_3864" s="T390">onton</ta>
            <ta e="T392" id="Seg_3865" s="T391">biːr</ta>
            <ta e="T393" id="Seg_3866" s="T392">ogo-m</ta>
            <ta e="T394" id="Seg_3867" s="T393">poːvar-ga</ta>
            <ta e="T395" id="Seg_3868" s="T394">Nosku͡o-ga</ta>
            <ta e="T396" id="Seg_3869" s="T395">üleliː-r</ta>
            <ta e="T397" id="Seg_3870" s="T396">glavnaj</ta>
            <ta e="T398" id="Seg_3871" s="T397">poːvar</ta>
            <ta e="T399" id="Seg_3872" s="T398">biːr</ta>
            <ta e="T400" id="Seg_3873" s="T399">kɨːh-ɨ-m</ta>
            <ta e="T401" id="Seg_3874" s="T400">biːr</ta>
            <ta e="T403" id="Seg_3875" s="T402">tu͡ok</ta>
            <ta e="T405" id="Seg_3876" s="T404">Nasku͡o-ga</ta>
            <ta e="T406" id="Seg_3877" s="T405">Marʼina</ta>
            <ta e="T410" id="Seg_3878" s="T409">hu͡o</ta>
            <ta e="T411" id="Seg_3879" s="T410">ü͡örem-mit-e</ta>
            <ta e="T412" id="Seg_3880" s="T411">heː</ta>
            <ta e="T413" id="Seg_3881" s="T412">ol</ta>
            <ta e="T414" id="Seg_3882" s="T413">di͡et</ta>
            <ta e="T415" id="Seg_3883" s="T414">Krasnajarskaj-ga</ta>
            <ta e="T416" id="Seg_3884" s="T415">ü͡örem-mit-e</ta>
            <ta e="T417" id="Seg_3885" s="T416">onton</ta>
            <ta e="T419" id="Seg_3886" s="T418">du͡oktuːr</ta>
            <ta e="T420" id="Seg_3887" s="T419">kɨːh-ɨ-m</ta>
            <ta e="T421" id="Seg_3888" s="T420">Dudʼinka-ga</ta>
            <ta e="T422" id="Seg_3889" s="T421">üleliː-r</ta>
            <ta e="T423" id="Seg_3890" s="T422">tʼerapʼev-ka</ta>
            <ta e="T424" id="Seg_3891" s="T423">üleliː-r</ta>
            <ta e="T425" id="Seg_3892" s="T424">anɨ</ta>
            <ta e="T426" id="Seg_3893" s="T425">üs</ta>
            <ta e="T427" id="Seg_3894" s="T426">ogo-loːk</ta>
            <ta e="T428" id="Seg_3895" s="T427">kɨrdʼagas</ta>
            <ta e="T429" id="Seg_3896" s="T428">dʼaktar</ta>
            <ta e="T430" id="Seg_3897" s="T429">pʼensʼija-laːk</ta>
            <ta e="T431" id="Seg_3898" s="T430">ogo-m</ta>
            <ta e="T432" id="Seg_3899" s="T431">barɨ-ta</ta>
            <ta e="T433" id="Seg_3900" s="T432">pʼensʼija-laːk</ta>
            <ta e="T434" id="Seg_3901" s="T433">üs</ta>
            <ta e="T435" id="Seg_3902" s="T434">ogo-m</ta>
            <ta e="T436" id="Seg_3903" s="T435">pʼensʼija-laːk-tar</ta>
            <ta e="T437" id="Seg_3904" s="T436">ulakat-tar-ɨ-m</ta>
            <ta e="T438" id="Seg_3905" s="T437">anɨ</ta>
            <ta e="T439" id="Seg_3906" s="T438">biːrgeh-e</ta>
            <ta e="T440" id="Seg_3907" s="T439">bu͡ollagɨna</ta>
            <ta e="T441" id="Seg_3908" s="T440">rɨbak-tɨː-r</ta>
            <ta e="T442" id="Seg_3909" s="T441">biːrgeh-e</ta>
            <ta e="T443" id="Seg_3910" s="T442">bu͡olla</ta>
            <ta e="T444" id="Seg_3911" s="T443">anɨ</ta>
            <ta e="T445" id="Seg_3912" s="T444">haːhɨr-bɨt</ta>
            <ta e="T446" id="Seg_3913" s="T445">pʼensʼija-laːk</ta>
            <ta e="T447" id="Seg_3914" s="T446">Bi͡ere</ta>
            <ta e="T448" id="Seg_3915" s="T447">er-e</ta>
            <ta e="T449" id="Seg_3916" s="T448">hubu-kaːn</ta>
            <ta e="T450" id="Seg_3917" s="T449">öl-büt-e</ta>
            <ta e="T451" id="Seg_3918" s="T450">ikki</ta>
            <ta e="T452" id="Seg_3919" s="T451">ogo-loːk</ta>
            <ta e="T453" id="Seg_3920" s="T452">üh-üs-tere</ta>
            <ta e="T454" id="Seg_3921" s="T453">öl-büt-e</ta>
            <ta e="T455" id="Seg_3922" s="T454">ulakan</ta>
            <ta e="T456" id="Seg_3923" s="T455">u͡ol-u-m</ta>
            <ta e="T457" id="Seg_3924" s="T456">üs</ta>
            <ta e="T458" id="Seg_3925" s="T457">ogo-loːk</ta>
            <ta e="T459" id="Seg_3926" s="T458">ogo</ta>
            <ta e="T460" id="Seg_3927" s="T459">biːr</ta>
            <ta e="T461" id="Seg_3928" s="T460">ogo-to</ta>
            <ta e="T462" id="Seg_3929" s="T461">dʼaktar-daːk</ta>
            <ta e="T463" id="Seg_3930" s="T462">ogo-loːk</ta>
            <ta e="T464" id="Seg_3931" s="T463">aŋar-dara</ta>
            <ta e="T465" id="Seg_3932" s="T464">ogo-lon-o</ta>
            <ta e="T467" id="Seg_3933" s="T465">ilik-ter</ta>
            <ta e="T471" id="Seg_3934" s="T469">heː</ta>
            <ta e="T472" id="Seg_3935" s="T471">onton</ta>
            <ta e="T473" id="Seg_3936" s="T472">biːr</ta>
            <ta e="T474" id="Seg_3937" s="T473">ogo-m</ta>
            <ta e="T475" id="Seg_3938" s="T474">ke</ta>
            <ta e="T476" id="Seg_3939" s="T475">kimi͡e-ke</ta>
            <ta e="T477" id="Seg_3940" s="T476">baːr</ta>
            <ta e="T478" id="Seg_3941" s="T477">Hoːpstaj-ga</ta>
            <ta e="T479" id="Seg_3942" s="T478">baːr</ta>
            <ta e="T480" id="Seg_3943" s="T479">kimi͡e-ke</ta>
            <ta e="T481" id="Seg_3944" s="T480">vaspʼitatʼel-ga</ta>
            <ta e="T482" id="Seg_3945" s="T481">üleliː-r</ta>
            <ta e="T483" id="Seg_3946" s="T482">onton</ta>
            <ta e="T484" id="Seg_3947" s="T483">biːr</ta>
            <ta e="T485" id="Seg_3948" s="T484">ogo-m</ta>
            <ta e="T486" id="Seg_3949" s="T485">munna</ta>
            <ta e="T487" id="Seg_3950" s="T486">biːr</ta>
            <ta e="T488" id="Seg_3951" s="T487">kɨːh-ɨ-m</ta>
            <ta e="T489" id="Seg_3952" s="T488">munna</ta>
            <ta e="T490" id="Seg_3953" s="T489">vaspʼitatʼe-liː-r</ta>
            <ta e="T491" id="Seg_3954" s="T490">biːr</ta>
            <ta e="T492" id="Seg_3955" s="T491">ogo-m</ta>
            <ta e="T494" id="Seg_3956" s="T492">du͡o</ta>
            <ta e="T497" id="Seg_3957" s="T496">heː</ta>
            <ta e="T498" id="Seg_3958" s="T497">sku͡ola-ga</ta>
            <ta e="T499" id="Seg_3959" s="T498">munna</ta>
            <ta e="T500" id="Seg_3960" s="T499">Du͡omna</ta>
            <ta e="T501" id="Seg_3961" s="T500">Pʼetrovna</ta>
            <ta e="T508" id="Seg_3962" s="T506">eː</ta>
            <ta e="T509" id="Seg_3963" s="T508">onton</ta>
            <ta e="T510" id="Seg_3964" s="T509">biːr</ta>
            <ta e="T511" id="Seg_3965" s="T510">ɨlgɨn</ta>
            <ta e="T512" id="Seg_3966" s="T511">u͡ol-u-m</ta>
            <ta e="T513" id="Seg_3967" s="T512">dʼaktar-a</ta>
            <ta e="T514" id="Seg_3968" s="T513">emi͡e</ta>
            <ta e="T515" id="Seg_3969" s="T514">munna</ta>
            <ta e="T516" id="Seg_3970" s="T515">učital-lɨː-r</ta>
            <ta e="T517" id="Seg_3971" s="T516">Naːdʼa</ta>
            <ta e="T518" id="Seg_3972" s="T517">Kʼirgʼizava</ta>
            <ta e="T519" id="Seg_3973" s="T518">i͡e</ta>
            <ta e="T521" id="Seg_3974" s="T520">onton</ta>
            <ta e="T522" id="Seg_3975" s="T521">biːr</ta>
            <ta e="T523" id="Seg_3976" s="T522">u͡ol-u-m</ta>
            <ta e="T524" id="Seg_3977" s="T523">bu͡ollagɨna</ta>
            <ta e="T525" id="Seg_3978" s="T524">kim</ta>
            <ta e="T526" id="Seg_3979" s="T525">bʼezdʼexotčʼik</ta>
            <ta e="T527" id="Seg_3980" s="T526">büp-püt-e</ta>
            <ta e="T528" id="Seg_3981" s="T527">ü͡ören-en</ta>
            <ta e="T529" id="Seg_3982" s="T528">üleliː-r</ta>
            <ta e="T530" id="Seg_3983" s="T529">e-t-e</ta>
            <ta e="T531" id="Seg_3984" s="T530">iːgire-ler</ta>
            <ta e="T532" id="Seg_3985" s="T531">biːrgeh-e</ta>
            <ta e="T533" id="Seg_3986" s="T532">elʼektrʼik</ta>
            <ta e="T534" id="Seg_3987" s="T533">iti</ta>
            <ta e="T535" id="Seg_3988" s="T534">ülel-iː</ta>
            <ta e="T536" id="Seg_3989" s="T535">hɨldʼ-ar</ta>
            <ta e="T537" id="Seg_3990" s="T536">anɨ-ga</ta>
            <ta e="T538" id="Seg_3991" s="T537">di͡eri</ta>
            <ta e="T539" id="Seg_3992" s="T538">ü͡ören-en</ta>
            <ta e="T540" id="Seg_3993" s="T539">büt-en</ta>
            <ta e="T541" id="Seg_3994" s="T540">baran</ta>
            <ta e="T542" id="Seg_3995" s="T541">tak</ta>
            <ta e="T543" id="Seg_3996" s="T542">da</ta>
            <ta e="T544" id="Seg_3997" s="T543">barɨ-ta</ta>
            <ta e="T545" id="Seg_3998" s="T544">ü͡örem-mit-tere</ta>
            <ta e="T546" id="Seg_3999" s="T545">onton</ta>
            <ta e="T547" id="Seg_4000" s="T546">ke</ta>
            <ta e="T548" id="Seg_4001" s="T547">biːr</ta>
            <ta e="T549" id="Seg_4002" s="T548">ogo-m</ta>
            <ta e="T550" id="Seg_4003" s="T549">Kiri͡es-ka</ta>
            <ta e="T551" id="Seg_4004" s="T550">baːr</ta>
            <ta e="T553" id="Seg_4005" s="T552">eː</ta>
            <ta e="T554" id="Seg_4006" s="T553">mʼilʼisʼije</ta>
            <ta e="T555" id="Seg_4007" s="T554">ü͡öreg-i-n</ta>
            <ta e="T556" id="Seg_4008" s="T555">büp-püt</ta>
            <ta e="T557" id="Seg_4009" s="T556">e-t-e</ta>
            <ta e="T558" id="Seg_4010" s="T557">on-tu-ŋ</ta>
            <ta e="T559" id="Seg_4011" s="T558">hir</ta>
            <ta e="T560" id="Seg_4012" s="T559">aːjɨ</ta>
            <ta e="T561" id="Seg_4013" s="T560">hir-i</ta>
            <ta e="T562" id="Seg_4014" s="T561">bar-a-t-an</ta>
            <ta e="T563" id="Seg_4015" s="T562">bar-a-t-an</ta>
            <ta e="T564" id="Seg_4016" s="T563">baran</ta>
            <ta e="T565" id="Seg_4017" s="T564">Kiri͡es-ke</ta>
            <ta e="T566" id="Seg_4018" s="T565">dʼaktar-daːk</ta>
            <ta e="T567" id="Seg_4019" s="T566">Alʼeksʼej</ta>
            <ta e="T568" id="Seg_4020" s="T567">kör-dü-ŋ</ta>
            <ta e="T569" id="Seg_4021" s="T568">itte</ta>
            <ta e="T570" id="Seg_4022" s="T569">eni</ta>
            <ta e="T571" id="Seg_4023" s="T570">araːha</ta>
            <ta e="T572" id="Seg_4024" s="T571">onton</ta>
            <ta e="T573" id="Seg_4025" s="T572">aŋar-dara</ta>
            <ta e="T574" id="Seg_4026" s="T573">bulčut-tar</ta>
            <ta e="T575" id="Seg_4027" s="T574">üs</ta>
            <ta e="T576" id="Seg_4028" s="T575">u͡ol-u-m</ta>
            <ta e="T577" id="Seg_4029" s="T576">bulčut-tar</ta>
            <ta e="T578" id="Seg_4030" s="T577">bu͡olla</ta>
            <ta e="T579" id="Seg_4031" s="T578">Ölü͡öse</ta>
            <ta e="T580" id="Seg_4032" s="T579">Hergeːj</ta>
            <ta e="T581" id="Seg_4033" s="T580">Miːse</ta>
            <ta e="T582" id="Seg_4034" s="T581">Biːtʼe</ta>
            <ta e="T583" id="Seg_4035" s="T582">Biːtʼe</ta>
            <ta e="T584" id="Seg_4036" s="T583">utuj-a</ta>
            <ta e="T585" id="Seg_4037" s="T584">hɨt-ar</ta>
            <ta e="T586" id="Seg_4038" s="T585">dʼaktar-a</ta>
            <ta e="T587" id="Seg_4039" s="T586">bu</ta>
            <ta e="T588" id="Seg_4040" s="T587">tur-ar</ta>
            <ta e="T589" id="Seg_4041" s="T588">ele-te</ta>
            <ta e="T590" id="Seg_4042" s="T589">du͡o</ta>
            <ta e="T591" id="Seg_4043" s="T590">onton</ta>
            <ta e="T592" id="Seg_4044" s="T591">u͡on</ta>
            <ta e="T593" id="Seg_4045" s="T592">ikki</ta>
            <ta e="T594" id="Seg_4046" s="T593">ogo-loːk-pun</ta>
            <ta e="T595" id="Seg_4047" s="T594">hürbe</ta>
            <ta e="T596" id="Seg_4048" s="T595">ordug-a</ta>
            <ta e="T597" id="Seg_4049" s="T596">bi͡es</ta>
            <ta e="T598" id="Seg_4050" s="T597">eː</ta>
            <ta e="T599" id="Seg_4051" s="T598">hette</ta>
            <ta e="T600" id="Seg_4052" s="T599">bɨnuk-taːk-pɨn</ta>
            <ta e="T601" id="Seg_4053" s="T600">oččogo</ta>
            <ta e="T602" id="Seg_4054" s="T601">dʼirʼevnʼija-bit</ta>
            <ta e="T603" id="Seg_4055" s="T602">innʼe</ta>
            <ta e="T604" id="Seg_4056" s="T603">tü͡örd-u͡on-tan</ta>
            <ta e="T605" id="Seg_4057" s="T604">taksa</ta>
            <ta e="T606" id="Seg_4058" s="T605">kihi</ta>
            <ta e="T607" id="Seg_4059" s="T606">kiniːt-ter-bi-n</ta>
            <ta e="T608" id="Seg_4060" s="T607">barɨ-tɨ-n</ta>
            <ta e="T609" id="Seg_4061" s="T608">bul-lak-pɨna</ta>
            <ta e="T610" id="Seg_4062" s="T609">elbek-ter</ta>
            <ta e="T611" id="Seg_4063" s="T610">aːk-tak-pɨna</ta>
            <ta e="T612" id="Seg_4064" s="T611">biːr</ta>
            <ta e="T613" id="Seg_4065" s="T612">kiniːt</ta>
            <ta e="T614" id="Seg_4066" s="T613">ikki</ta>
            <ta e="T615" id="Seg_4067" s="T614">kiniːt</ta>
            <ta e="T616" id="Seg_4068" s="T615">pʼedagoːg-tar</ta>
            <ta e="T617" id="Seg_4069" s="T616">ogo-lor-doːk-tor</ta>
            <ta e="T618" id="Seg_4070" s="T617">barɨ-lara</ta>
            <ta e="T619" id="Seg_4071" s="T618">kɨrgɨt-tara</ta>
            <ta e="T620" id="Seg_4072" s="T619">ü͡ören-e</ta>
            <ta e="T621" id="Seg_4073" s="T620">bar-bɨt-tara</ta>
            <ta e="T622" id="Seg_4074" s="T621">biːr</ta>
            <ta e="T623" id="Seg_4075" s="T622">kiniːt-i-m</ta>
            <ta e="T624" id="Seg_4076" s="T623">saːdʼik-ka</ta>
            <ta e="T625" id="Seg_4077" s="T624">kim-nere</ta>
            <ta e="T626" id="Seg_4078" s="T625">kim</ta>
            <ta e="T627" id="Seg_4079" s="T626">e-t-e=j</ta>
            <ta e="T628" id="Seg_4080" s="T627">ke</ta>
            <ta e="T629" id="Seg_4081" s="T628">dogo</ta>
            <ta e="T630" id="Seg_4082" s="T629">zabʼedusaj-dar</ta>
            <ta e="T631" id="Seg_4083" s="T630">ikki</ta>
            <ta e="T632" id="Seg_4084" s="T631">kiniːt-i-m</ta>
            <ta e="T633" id="Seg_4085" s="T632">učiːtal-lar</ta>
            <ta e="T634" id="Seg_4086" s="T633">pʼedagog-tar</ta>
            <ta e="T635" id="Seg_4087" s="T634">barɨ-ta</ta>
            <ta e="T636" id="Seg_4088" s="T635">ülehit-ter</ta>
            <ta e="T637" id="Seg_4089" s="T636">onton</ta>
            <ta e="T638" id="Seg_4090" s="T637">biːr</ta>
            <ta e="T639" id="Seg_4091" s="T638">kütü͡öt-ü-m</ta>
            <ta e="T640" id="Seg_4092" s="T639">iti</ta>
            <ta e="T641" id="Seg_4093" s="T640">taŋara-laː-t-a</ta>
            <ta e="T642" id="Seg_4094" s="T641">biːr</ta>
            <ta e="T643" id="Seg_4095" s="T642">kütü͡öt-ü-m</ta>
            <ta e="T644" id="Seg_4096" s="T643">ɨ͡arɨj</ta>
            <ta e="T645" id="Seg_4097" s="T644">bu͡ol-but</ta>
            <ta e="T646" id="Seg_4098" s="T645">ogo</ta>
            <ta e="T647" id="Seg_4099" s="T646">Domna</ta>
            <ta e="T648" id="Seg_4100" s="T647">Pʼetroːvna</ta>
            <ta e="T649" id="Seg_4101" s="T648">gi͡en-e</ta>
            <ta e="T650" id="Seg_4102" s="T649">ol</ta>
            <ta e="T651" id="Seg_4103" s="T650">ereːti</ta>
            <ta e="T652" id="Seg_4104" s="T651">buld-u</ta>
            <ta e="T653" id="Seg_4105" s="T652">delbi</ta>
            <ta e="T654" id="Seg_4106" s="T653">ölör-ör</ta>
            <ta e="T655" id="Seg_4107" s="T654">atag-a</ta>
            <ta e="T656" id="Seg_4108" s="T655">hu͡ok</ta>
            <ta e="T657" id="Seg_4109" s="T656">beje-te</ta>
            <ta e="T658" id="Seg_4110" s="T657">biːr</ta>
            <ta e="T659" id="Seg_4111" s="T658">kütü͡öt-ü-m</ta>
            <ta e="T660" id="Seg_4112" s="T659">Hu͡opostaj-ga</ta>
            <ta e="T661" id="Seg_4113" s="T660">axotnʼik</ta>
            <ta e="T662" id="Seg_4114" s="T661">kɨːh-a</ta>
            <ta e="T663" id="Seg_4115" s="T662">ü͡ören-e</ta>
            <ta e="T664" id="Seg_4116" s="T663">bar-bɨt</ta>
            <ta e="T665" id="Seg_4117" s="T664">dʼe</ta>
            <ta e="T666" id="Seg_4118" s="T665">tug-u</ta>
            <ta e="T667" id="Seg_4119" s="T666">keps-i͡ep-pi-n=ij</ta>
            <ta e="T671" id="Seg_4120" s="T670">dʼe</ta>
         </annotation>
         <annotation name="mp" tierref="mp-KiPP">
            <ta e="T2" id="Seg_4121" s="T1">bu-ttAn</ta>
            <ta e="T3" id="Seg_4122" s="T2">ihit-AːččI</ta>
            <ta e="T4" id="Seg_4123" s="T3">du͡o</ta>
            <ta e="T9" id="Seg_4124" s="T6">eː</ta>
            <ta e="T15" id="Seg_4125" s="T14">dʼe</ta>
            <ta e="T17" id="Seg_4126" s="T15">tu͡ok-nI</ta>
            <ta e="T18" id="Seg_4127" s="T17">ɨjɨt-Ar</ta>
            <ta e="T19" id="Seg_4128" s="T18">ka</ta>
            <ta e="T20" id="Seg_4129" s="T19">doː</ta>
            <ta e="T21" id="Seg_4130" s="T20">maŋnaj</ta>
            <ta e="T22" id="Seg_4131" s="T21">lʼuboj-BI-n</ta>
            <ta e="T24" id="Seg_4132" s="T22">ɨjɨt-AlAː-Ar</ta>
            <ta e="T25" id="Seg_4133" s="T24">du͡o</ta>
            <ta e="T30" id="Seg_4134" s="T29">ihit-Ar</ta>
            <ta e="T33" id="Seg_4135" s="T31">eː</ta>
            <ta e="T51" id="Seg_4136" s="T49">jeːrakata</ta>
            <ta e="T52" id="Seg_4137" s="T51">kaččaga</ta>
            <ta e="T53" id="Seg_4138" s="T52">büt-IAK-m=Ij</ta>
            <ta e="T54" id="Seg_4139" s="T53">ol</ta>
            <ta e="T55" id="Seg_4140" s="T54">ki͡ehe-GA</ta>
            <ta e="T56" id="Seg_4141" s="T55">da</ta>
            <ta e="T57" id="Seg_4142" s="T56">di͡eri</ta>
            <ta e="T68" id="Seg_4143" s="T66">Korgo</ta>
            <ta e="T72" id="Seg_4144" s="T70">Korgo-GA</ta>
            <ta e="T73" id="Seg_4145" s="T72">töröː-BIT-I-m</ta>
            <ta e="T74" id="Seg_4146" s="T73">h-manna</ta>
            <ta e="T75" id="Seg_4147" s="T74">beje-m</ta>
            <ta e="T76" id="Seg_4148" s="T75">hir-BA-r</ta>
            <ta e="T77" id="Seg_4149" s="T76">bulunnʼa-BIn</ta>
            <ta e="T78" id="Seg_4150" s="T77">bil-A-GIn</ta>
            <ta e="T79" id="Seg_4151" s="T78">bulunnʼa</ta>
            <ta e="T80" id="Seg_4152" s="T79">aːt-tI-n</ta>
            <ta e="T107" id="Seg_4153" s="T106">teːte-tA</ta>
            <ta e="T108" id="Seg_4154" s="T107">hu͡ok</ta>
            <ta e="T112" id="Seg_4155" s="T111">dʼe</ta>
            <ta e="T113" id="Seg_4156" s="T112">min</ta>
            <ta e="T115" id="Seg_4157" s="T113">baːbɨska-m</ta>
            <ta e="T117" id="Seg_4158" s="T115">iːt-TI-tA</ta>
            <ta e="T121" id="Seg_4159" s="T120">baːbɨska</ta>
            <ta e="T122" id="Seg_4160" s="T121">vaspʼitɨvaj-LAː-BIT-tA</ta>
            <ta e="T123" id="Seg_4161" s="T122">maːma-m</ta>
            <ta e="T124" id="Seg_4162" s="T123">maːma-tA</ta>
            <ta e="T129" id="Seg_4163" s="T128">onton</ta>
            <ta e="T130" id="Seg_4164" s="T129">bihigi</ta>
            <ta e="T131" id="Seg_4165" s="T130">kergen-LAr</ta>
            <ta e="T132" id="Seg_4166" s="T131">barɨta</ta>
            <ta e="T133" id="Seg_4167" s="T132">öl-An-LAr</ta>
            <ta e="T134" id="Seg_4168" s="T133">tulaːjak</ta>
            <ta e="T135" id="Seg_4169" s="T134">hɨrɨt-I-BIT-BIt</ta>
            <ta e="T136" id="Seg_4170" s="T135">emeːksin-nI</ta>
            <ta e="T137" id="Seg_4171" s="T136">kɨtta</ta>
            <ta e="T138" id="Seg_4172" s="T137">baːbɨska-BI-n</ta>
            <ta e="T139" id="Seg_4173" s="T138">kɨtta</ta>
            <ta e="T140" id="Seg_4174" s="T139">onton</ta>
            <ta e="T142" id="Seg_4175" s="T141">Rɨmnaj</ta>
            <ta e="T143" id="Seg_4176" s="T142">dek</ta>
            <ta e="T144" id="Seg_4177" s="T143">bar-BIT-I-m</ta>
            <ta e="T145" id="Seg_4178" s="T144">onton</ta>
            <ta e="T146" id="Seg_4179" s="T145">ol</ta>
            <ta e="T147" id="Seg_4180" s="T146">bar-BIT-BIt</ta>
            <ta e="T149" id="Seg_4181" s="T148">ebe-BI-n</ta>
            <ta e="T150" id="Seg_4182" s="T149">inʼe-BI-n</ta>
            <ta e="T151" id="Seg_4183" s="T150">kɨtta</ta>
            <ta e="T152" id="Seg_4184" s="T151">inʼe</ta>
            <ta e="T153" id="Seg_4185" s="T152">di͡e-AːččI-BIn</ta>
            <ta e="T154" id="Seg_4186" s="T153">ol-tI-BI-n</ta>
            <ta e="T155" id="Seg_4187" s="T154">maːma-m</ta>
            <ta e="T156" id="Seg_4188" s="T155">maːma-m</ta>
            <ta e="T157" id="Seg_4189" s="T156">hin</ta>
            <ta e="T158" id="Seg_4190" s="T157">biːr</ta>
            <ta e="T159" id="Seg_4191" s="T158">dʼe</ta>
            <ta e="T160" id="Seg_4192" s="T159">onton</ta>
            <ta e="T161" id="Seg_4193" s="T160">töttörü</ta>
            <ta e="T162" id="Seg_4194" s="T161">kel-BIT-BIt</ta>
            <ta e="T163" id="Seg_4195" s="T162">maːma-m</ta>
            <ta e="T164" id="Seg_4196" s="T163">emeːksin</ta>
            <ta e="T166" id="Seg_4197" s="T165">baːbɨska-m</ta>
            <ta e="T167" id="Seg_4198" s="T166">öl-Ar-tI-GAr</ta>
            <ta e="T168" id="Seg_4199" s="T167">öl-An</ta>
            <ta e="T169" id="Seg_4200" s="T168">baran</ta>
            <ta e="T170" id="Seg_4201" s="T169">tilin-I-BIT-tA</ta>
            <ta e="T171" id="Seg_4202" s="T170">ol</ta>
            <ta e="T172" id="Seg_4203" s="T171">dojdu-GA</ta>
            <ta e="T173" id="Seg_4204" s="T172">onton</ta>
            <ta e="T174" id="Seg_4205" s="T173">töttörü</ta>
            <ta e="T175" id="Seg_4206" s="T174">kel-BIT-BIt</ta>
            <ta e="T176" id="Seg_4207" s="T175">ol</ta>
            <ta e="T177" id="Seg_4208" s="T176">kel-BIT-BIt</ta>
            <ta e="T178" id="Seg_4209" s="T177">genne</ta>
            <ta e="T179" id="Seg_4210" s="T178">u͡on</ta>
            <ta e="T180" id="Seg_4211" s="T179">alta</ta>
            <ta e="T181" id="Seg_4212" s="T180">dʼɨl-LAːK-BA-r</ta>
            <ta e="T182" id="Seg_4213" s="T181">min-n</ta>
            <ta e="T183" id="Seg_4214" s="T182">er-GA</ta>
            <ta e="T184" id="Seg_4215" s="T183">bi͡er-BIT-tA</ta>
            <ta e="T185" id="Seg_4216" s="T184">min</ta>
            <ta e="T186" id="Seg_4217" s="T185">öl-TAK-BInA</ta>
            <ta e="T187" id="Seg_4218" s="T186">en-n</ta>
            <ta e="T188" id="Seg_4219" s="T187">kulut</ta>
            <ta e="T189" id="Seg_4220" s="T188">gɨn-IAK-LArA</ta>
            <ta e="T190" id="Seg_4221" s="T189">di͡e-TI-tA</ta>
            <ta e="T191" id="Seg_4222" s="T190">čort</ta>
            <ta e="T192" id="Seg_4223" s="T191">tu͡ok-nI</ta>
            <ta e="T193" id="Seg_4224" s="T192">da</ta>
            <ta e="T194" id="Seg_4225" s="T193">ɨraːs-BIn</ta>
            <ta e="T195" id="Seg_4226" s="T194">haːtar</ta>
            <ta e="T196" id="Seg_4227" s="T195">huːn-A</ta>
            <ta e="T197" id="Seg_4228" s="T196">da</ta>
            <ta e="T198" id="Seg_4229" s="T197">ilik-BIn</ta>
            <ta e="T199" id="Seg_4230" s="T198">er-GA</ta>
            <ta e="T200" id="Seg_4231" s="T199">bar-BIT-I-m</ta>
            <ta e="T201" id="Seg_4232" s="T200">ɨtaː-A-ɨtaː-A</ta>
            <ta e="T202" id="Seg_4233" s="T201">er-GA</ta>
            <ta e="T203" id="Seg_4234" s="T202">bi͡er-Ar-LAr</ta>
            <ta e="T204" id="Seg_4235" s="T203">utuj-t-Ar-LAr</ta>
            <ta e="T205" id="Seg_4236" s="T204">ɨtaː-A-ɨtaː-A</ta>
            <ta e="T206" id="Seg_4237" s="T205">utuj-A-BIn</ta>
            <ta e="T207" id="Seg_4238" s="T206">er-BI-ttAn</ta>
            <ta e="T208" id="Seg_4239" s="T207">kuttan-A-BIn</ta>
            <ta e="T209" id="Seg_4240" s="T208">taŋas-LIːN</ta>
            <ta e="T210" id="Seg_4241" s="T209">utuj-A-BIn</ta>
            <ta e="T211" id="Seg_4242" s="T210">eː</ta>
            <ta e="T212" id="Seg_4243" s="T211">dʼe</ta>
            <ta e="T213" id="Seg_4244" s="T212">onton</ta>
            <ta e="T214" id="Seg_4245" s="T213">dʼɨl-nI</ta>
            <ta e="T215" id="Seg_4246" s="T214">dʼɨl-nI</ta>
            <ta e="T216" id="Seg_4247" s="T215">taŋas-LAːK</ta>
            <ta e="T217" id="Seg_4248" s="T216">utuj-BIT-I-m</ta>
            <ta e="T218" id="Seg_4249" s="T217">onton</ta>
            <ta e="T219" id="Seg_4250" s="T218">dʼe</ta>
            <ta e="T220" id="Seg_4251" s="T219">kojut</ta>
            <ta e="T221" id="Seg_4252" s="T220">ü͡ören-A-BIn</ta>
            <ta e="T222" id="Seg_4253" s="T221">bu͡o</ta>
            <ta e="T223" id="Seg_4254" s="T222">er</ta>
            <ta e="T224" id="Seg_4255" s="T223">er</ta>
            <ta e="T225" id="Seg_4256" s="T224">er-BA-r</ta>
            <ta e="T226" id="Seg_4257" s="T225">ol</ta>
            <ta e="T227" id="Seg_4258" s="T226">aːt-tA</ta>
            <ta e="T233" id="Seg_4259" s="T232">u͡on</ta>
            <ta e="T234" id="Seg_4260" s="T233">hette</ta>
            <ta e="T235" id="Seg_4261" s="T234">u͡on</ta>
            <ta e="T236" id="Seg_4262" s="T235">agɨs</ta>
            <ta e="T237" id="Seg_4263" s="T236">u͡on</ta>
            <ta e="T238" id="Seg_4264" s="T237">agɨs</ta>
            <ta e="T239" id="Seg_4265" s="T238">dʼɨl-LAːK-BA-r</ta>
            <ta e="T240" id="Seg_4266" s="T239">ogo-LAN-BIT-I-m</ta>
            <ta e="T241" id="Seg_4267" s="T240">pi͡erbej</ta>
            <ta e="T242" id="Seg_4268" s="T241">ogo-m</ta>
            <ta e="T243" id="Seg_4269" s="T242">manna</ta>
            <ta e="T244" id="Seg_4270" s="T243">baːr</ta>
            <ta e="T245" id="Seg_4271" s="T244">onton</ta>
            <ta e="T246" id="Seg_4272" s="T245">ogo-LAN-TI-m</ta>
            <ta e="T247" id="Seg_4273" s="T246">ogo-LAN-TI-m</ta>
            <ta e="T248" id="Seg_4274" s="T247">dʼɨl</ta>
            <ta e="T249" id="Seg_4275" s="T248">aːjɨ</ta>
            <ta e="T250" id="Seg_4276" s="T249">ogo-LAN-A-BIn</ta>
            <ta e="T251" id="Seg_4277" s="T250">dʼɨl</ta>
            <ta e="T252" id="Seg_4278" s="T251">aːjɨ</ta>
            <ta e="T254" id="Seg_4279" s="T252">ogo-LAN-A-BIn</ta>
            <ta e="T259" id="Seg_4280" s="T258">eː</ta>
            <ta e="T260" id="Seg_4281" s="T259">hubu</ta>
            <ta e="T261" id="Seg_4282" s="T260">hir-GA</ta>
            <ta e="T262" id="Seg_4283" s="T261">tɨ͡a-GA</ta>
            <ta e="T263" id="Seg_4284" s="T262">hɨrɨt-An-BIt</ta>
            <ta e="T264" id="Seg_4285" s="T263">tɨ͡a-GA</ta>
            <ta e="T265" id="Seg_4286" s="T264">e-TI-BIt</ta>
            <ta e="T266" id="Seg_4287" s="T265">ogonnʼor-I-m</ta>
            <ta e="T267" id="Seg_4288" s="T266">kasʼsʼir</ta>
            <ta e="T268" id="Seg_4289" s="T267">e-TI-tA</ta>
            <ta e="T269" id="Seg_4290" s="T268">hürdeːk</ta>
            <ta e="T270" id="Seg_4291" s="T269">üčügej</ta>
            <ta e="T271" id="Seg_4292" s="T270">kihi</ta>
            <ta e="T272" id="Seg_4293" s="T271">e-TI-tA</ta>
            <ta e="T273" id="Seg_4294" s="T272">spasobnaj</ta>
            <ta e="T274" id="Seg_4295" s="T273">bagajɨ</ta>
            <ta e="T275" id="Seg_4296" s="T274">bulčut</ta>
            <ta e="T276" id="Seg_4297" s="T275">bult-nI</ta>
            <ta e="T277" id="Seg_4298" s="T276">da</ta>
            <ta e="T278" id="Seg_4299" s="T277">bultaː-BIT-tA</ta>
            <ta e="T279" id="Seg_4300" s="T278">dʼi͡e</ta>
            <ta e="T280" id="Seg_4301" s="T279">da</ta>
            <ta e="T281" id="Seg_4302" s="T280">tut-BIT-tA</ta>
            <ta e="T282" id="Seg_4303" s="T281">kasʼsʼir-GA</ta>
            <ta e="T283" id="Seg_4304" s="T282">u͡on</ta>
            <ta e="T284" id="Seg_4305" s="T283">bi͡es</ta>
            <ta e="T285" id="Seg_4306" s="T284">dʼɨl-nI</ta>
            <ta e="T286" id="Seg_4307" s="T285">olor-BIT-tA</ta>
            <ta e="T287" id="Seg_4308" s="T286">onton</ta>
            <ta e="T288" id="Seg_4309" s="T287">biːr</ta>
            <ta e="T289" id="Seg_4310" s="T288">dʼɨl-nI</ta>
            <ta e="T290" id="Seg_4311" s="T289">partorg-GA</ta>
            <ta e="T291" id="Seg_4312" s="T290">olor-BIT-tA</ta>
            <ta e="T292" id="Seg_4313" s="T291">onton</ta>
            <ta e="T293" id="Seg_4314" s="T292">dʼi͡e-LAr-nI</ta>
            <ta e="T294" id="Seg_4315" s="T293">tut-BIT-tA</ta>
            <ta e="T295" id="Seg_4316" s="T294">bu</ta>
            <ta e="T296" id="Seg_4317" s="T295">tur-Ar</ta>
            <ta e="T297" id="Seg_4318" s="T296">tut-BIT</ta>
            <ta e="T298" id="Seg_4319" s="T297">dʼi͡e-LAr-tA</ta>
            <ta e="T299" id="Seg_4320" s="T298">onton</ta>
            <ta e="T300" id="Seg_4321" s="T299">u͡on</ta>
            <ta e="T301" id="Seg_4322" s="T300">ikki</ta>
            <ta e="T302" id="Seg_4323" s="T301">ogo-LAːK-BIt</ta>
            <ta e="T303" id="Seg_4324" s="T302">onton</ta>
            <ta e="T304" id="Seg_4325" s="T303">barɨta</ta>
            <ta e="T305" id="Seg_4326" s="T304">biːr</ta>
            <ta e="T672" id="Seg_4327" s="T305">da</ta>
            <ta e="T306" id="Seg_4328" s="T672">kihi-nI</ta>
            <ta e="T307" id="Seg_4329" s="T306">kihi-GA</ta>
            <ta e="T308" id="Seg_4330" s="T307">bi͡er-BAtAK-I-m</ta>
            <ta e="T309" id="Seg_4331" s="T308">ogo-LAr-BI-n</ta>
            <ta e="T310" id="Seg_4332" s="T309">beje-m</ta>
            <ta e="T311" id="Seg_4333" s="T310">iːt-IAK</ta>
            <ta e="T312" id="Seg_4334" s="T311">bu͡ol-An-BIn</ta>
            <ta e="T313" id="Seg_4335" s="T312">kihi-GA</ta>
            <ta e="T314" id="Seg_4336" s="T313">bi͡er-BAT-BIn</ta>
            <ta e="T315" id="Seg_4337" s="T314">ahɨn-A-BIn</ta>
            <ta e="T316" id="Seg_4338" s="T315">ogo-LAr-BI-n</ta>
            <ta e="T317" id="Seg_4339" s="T316">ol-LAr-BI-n</ta>
            <ta e="T318" id="Seg_4340" s="T317">iːt-I-n-A</ta>
            <ta e="T319" id="Seg_4341" s="T318">iːt-I-BIT-I-nAn</ta>
            <ta e="T320" id="Seg_4342" s="T319">bihik-LAːK</ta>
            <ta e="T321" id="Seg_4343" s="T320">ogo-LAːK</ta>
            <ta e="T322" id="Seg_4344" s="T321">ilim-LAː-A-s-AːččI-BIn</ta>
            <ta e="T323" id="Seg_4345" s="T322">ogonnʼor-BI-n</ta>
            <ta e="T324" id="Seg_4346" s="T323">kɨtta</ta>
            <ta e="T325" id="Seg_4347" s="T324">ɨ͡arɨj</ta>
            <ta e="T326" id="Seg_4348" s="T325">bu͡ol-BIT-tI-n</ta>
            <ta e="T327" id="Seg_4349" s="T326">genne</ta>
            <ta e="T328" id="Seg_4350" s="T327">dʼe</ta>
            <ta e="T329" id="Seg_4351" s="T328">ogo-LAr-BIt</ta>
            <ta e="T330" id="Seg_4352" s="T329">ulaːt-An-LAr</ta>
            <ta e="T331" id="Seg_4353" s="T330">ü͡ören-A</ta>
            <ta e="T332" id="Seg_4354" s="T331">bar-ItAlAː-BIT-LArA</ta>
            <ta e="T333" id="Seg_4355" s="T332">onton</ta>
            <ta e="T334" id="Seg_4356" s="T333">kojut</ta>
            <ta e="T335" id="Seg_4357" s="T334">ikki</ta>
            <ta e="T336" id="Seg_4358" s="T335">ulakan</ta>
            <ta e="T337" id="Seg_4359" s="T336">ogo-m</ta>
            <ta e="T338" id="Seg_4360" s="T337">taba</ta>
            <ta e="T339" id="Seg_4361" s="T338">ü͡örek-tI-GAr</ta>
            <ta e="T340" id="Seg_4362" s="T339">bar-BIT-LArA</ta>
            <ta e="T341" id="Seg_4363" s="T340">u͡ol-BIt</ta>
            <ta e="T342" id="Seg_4364" s="T341">kɨːs-BIt</ta>
            <ta e="T343" id="Seg_4365" s="T342">onton</ta>
            <ta e="T344" id="Seg_4366" s="T343">biːr</ta>
            <ta e="T345" id="Seg_4367" s="T344">ogo-BIt</ta>
            <ta e="T346" id="Seg_4368" s="T345">ulaːt-An</ta>
            <ta e="T347" id="Seg_4369" s="T346">Krasnajarskaj-GA</ta>
            <ta e="T348" id="Seg_4370" s="T347">ü͡ören-A</ta>
            <ta e="T349" id="Seg_4371" s="T348">bar-BIT-tA</ta>
            <ta e="T350" id="Seg_4372" s="T349">anɨ</ta>
            <ta e="T351" id="Seg_4373" s="T350">tʼerapʼevt-GA</ta>
            <ta e="T352" id="Seg_4374" s="T351">üleleː-A</ta>
            <ta e="T353" id="Seg_4375" s="T352">hɨt-Ar</ta>
            <ta e="T354" id="Seg_4376" s="T353">Dudʼinka-GA</ta>
            <ta e="T355" id="Seg_4377" s="T354">onton</ta>
            <ta e="T356" id="Seg_4378" s="T355">biːr</ta>
            <ta e="T357" id="Seg_4379" s="T356">biːr</ta>
            <ta e="T358" id="Seg_4380" s="T357">ogo-m</ta>
            <ta e="T359" id="Seg_4381" s="T358">bu͡ollagɨna</ta>
            <ta e="T360" id="Seg_4382" s="T359">emi͡e</ta>
            <ta e="T361" id="Seg_4383" s="T360">ü͡ören-An</ta>
            <ta e="T362" id="Seg_4384" s="T361">ikki</ta>
            <ta e="T363" id="Seg_4385" s="T362">instʼitut-nI</ta>
            <ta e="T364" id="Seg_4386" s="T363">büt-BIT-tA</ta>
            <ta e="T365" id="Seg_4387" s="T364">onton</ta>
            <ta e="T366" id="Seg_4388" s="T365">Krasnajarskaj-GA</ta>
            <ta e="T367" id="Seg_4389" s="T366">baːr</ta>
            <ta e="T369" id="Seg_4390" s="T368">Sʼergʼej</ta>
            <ta e="T370" id="Seg_4391" s="T369">ol-tI-m</ta>
            <ta e="T371" id="Seg_4392" s="T370">dʼaktar-LAːK</ta>
            <ta e="T372" id="Seg_4393" s="T371">ogo</ta>
            <ta e="T373" id="Seg_4394" s="T372">ol</ta>
            <ta e="T374" id="Seg_4395" s="T373">ikki</ta>
            <ta e="T375" id="Seg_4396" s="T374">u͡ol-LAːK</ta>
            <ta e="T376" id="Seg_4397" s="T375">kazaːk-LAr-ttAn</ta>
            <ta e="T377" id="Seg_4398" s="T376">dʼaktar-LAN-BIT</ta>
            <ta e="T378" id="Seg_4399" s="T377">araŋas-LAːK-ttAn</ta>
            <ta e="T379" id="Seg_4400" s="T378">anɨ</ta>
            <ta e="T380" id="Seg_4401" s="T379">bu͡ollagɨna</ta>
            <ta e="T381" id="Seg_4402" s="T380">axrana-GA</ta>
            <ta e="T382" id="Seg_4403" s="T381">axrana-GA</ta>
            <ta e="T383" id="Seg_4404" s="T382">üleleː-Ar</ta>
            <ta e="T384" id="Seg_4405" s="T383">mʼilʼisʼija-nI</ta>
            <ta e="T385" id="Seg_4406" s="T384">büt-An</ta>
            <ta e="T386" id="Seg_4407" s="T385">baran</ta>
            <ta e="T387" id="Seg_4408" s="T386">urut</ta>
            <ta e="T388" id="Seg_4409" s="T387">taba</ta>
            <ta e="T389" id="Seg_4410" s="T388">ülehit-tA</ta>
            <ta e="T390" id="Seg_4411" s="T389">bu͡ol-BIT-tA</ta>
            <ta e="T391" id="Seg_4412" s="T390">onton</ta>
            <ta e="T392" id="Seg_4413" s="T391">biːr</ta>
            <ta e="T393" id="Seg_4414" s="T392">ogo-m</ta>
            <ta e="T394" id="Seg_4415" s="T393">poːvar-GA</ta>
            <ta e="T395" id="Seg_4416" s="T394">Nosku͡o-GA</ta>
            <ta e="T396" id="Seg_4417" s="T395">üleleː-Ar</ta>
            <ta e="T397" id="Seg_4418" s="T396">glavnaj</ta>
            <ta e="T398" id="Seg_4419" s="T397">poːvar</ta>
            <ta e="T399" id="Seg_4420" s="T398">biːr</ta>
            <ta e="T400" id="Seg_4421" s="T399">kɨːs-I-m</ta>
            <ta e="T401" id="Seg_4422" s="T400">biːr</ta>
            <ta e="T403" id="Seg_4423" s="T402">tu͡ok</ta>
            <ta e="T405" id="Seg_4424" s="T404">Nosku͡o-GA</ta>
            <ta e="T406" id="Seg_4425" s="T405">Marɨːna</ta>
            <ta e="T410" id="Seg_4426" s="T409">hu͡ok</ta>
            <ta e="T411" id="Seg_4427" s="T410">ü͡ören-BIT-tA</ta>
            <ta e="T412" id="Seg_4428" s="T411">eː</ta>
            <ta e="T413" id="Seg_4429" s="T412">ol</ta>
            <ta e="T414" id="Seg_4430" s="T413">dek</ta>
            <ta e="T415" id="Seg_4431" s="T414">Krasnajarskaj-GA</ta>
            <ta e="T416" id="Seg_4432" s="T415">ü͡ören-BIT-tA</ta>
            <ta e="T417" id="Seg_4433" s="T416">onton</ta>
            <ta e="T419" id="Seg_4434" s="T418">du͡oktuːr</ta>
            <ta e="T420" id="Seg_4435" s="T419">kɨːs-I-m</ta>
            <ta e="T421" id="Seg_4436" s="T420">Dudʼinka-GA</ta>
            <ta e="T422" id="Seg_4437" s="T421">üleleː-Ar</ta>
            <ta e="T423" id="Seg_4438" s="T422">tʼerapʼevt-GA</ta>
            <ta e="T424" id="Seg_4439" s="T423">üleleː-Ar</ta>
            <ta e="T425" id="Seg_4440" s="T424">anɨ</ta>
            <ta e="T426" id="Seg_4441" s="T425">üs</ta>
            <ta e="T427" id="Seg_4442" s="T426">ogo-LAːK</ta>
            <ta e="T428" id="Seg_4443" s="T427">kɨrdʼagas</ta>
            <ta e="T429" id="Seg_4444" s="T428">dʼaktar</ta>
            <ta e="T430" id="Seg_4445" s="T429">pʼensija-LAːK</ta>
            <ta e="T431" id="Seg_4446" s="T430">ogo-m</ta>
            <ta e="T432" id="Seg_4447" s="T431">barɨ-tA</ta>
            <ta e="T433" id="Seg_4448" s="T432">pʼensija-LAːK</ta>
            <ta e="T434" id="Seg_4449" s="T433">üs</ta>
            <ta e="T435" id="Seg_4450" s="T434">ogo-m</ta>
            <ta e="T436" id="Seg_4451" s="T435">pʼensija-LAːK-LAr</ta>
            <ta e="T437" id="Seg_4452" s="T436">ulakan-LAr-I-m</ta>
            <ta e="T438" id="Seg_4453" s="T437">anɨ</ta>
            <ta e="T439" id="Seg_4454" s="T438">biːrges-tA</ta>
            <ta e="T440" id="Seg_4455" s="T439">bu͡ollagɨna</ta>
            <ta e="T441" id="Seg_4456" s="T440">rɨbak-LAː-Ar</ta>
            <ta e="T442" id="Seg_4457" s="T441">biːrges-tA</ta>
            <ta e="T443" id="Seg_4458" s="T442">bu͡olla</ta>
            <ta e="T444" id="Seg_4459" s="T443">anɨ</ta>
            <ta e="T445" id="Seg_4460" s="T444">haːhɨr-BIT</ta>
            <ta e="T446" id="Seg_4461" s="T445">pʼensija-LAːK</ta>
            <ta e="T447" id="Seg_4462" s="T446">Bi͡ere</ta>
            <ta e="T448" id="Seg_4463" s="T447">er-tA</ta>
            <ta e="T449" id="Seg_4464" s="T448">hubu-kAːN</ta>
            <ta e="T450" id="Seg_4465" s="T449">öl-BIT-tA</ta>
            <ta e="T451" id="Seg_4466" s="T450">ikki</ta>
            <ta e="T452" id="Seg_4467" s="T451">ogo-LAːK</ta>
            <ta e="T453" id="Seg_4468" s="T452">üs-Is-LArA</ta>
            <ta e="T454" id="Seg_4469" s="T453">öl-BIT-tA</ta>
            <ta e="T455" id="Seg_4470" s="T454">ulakan</ta>
            <ta e="T456" id="Seg_4471" s="T455">u͡ol-I-m</ta>
            <ta e="T457" id="Seg_4472" s="T456">üs</ta>
            <ta e="T458" id="Seg_4473" s="T457">ogo-LAːK</ta>
            <ta e="T459" id="Seg_4474" s="T458">ogo</ta>
            <ta e="T460" id="Seg_4475" s="T459">biːr</ta>
            <ta e="T461" id="Seg_4476" s="T460">ogo-tA</ta>
            <ta e="T462" id="Seg_4477" s="T461">dʼaktar-LAːK</ta>
            <ta e="T463" id="Seg_4478" s="T462">ogo-LAːK</ta>
            <ta e="T464" id="Seg_4479" s="T463">aŋar-LArA</ta>
            <ta e="T465" id="Seg_4480" s="T464">ogo-LAN-A</ta>
            <ta e="T467" id="Seg_4481" s="T465">ilik-LAr</ta>
            <ta e="T471" id="Seg_4482" s="T469">eː</ta>
            <ta e="T472" id="Seg_4483" s="T471">onton</ta>
            <ta e="T473" id="Seg_4484" s="T472">biːr</ta>
            <ta e="T474" id="Seg_4485" s="T473">ogo-m</ta>
            <ta e="T475" id="Seg_4486" s="T474">ka</ta>
            <ta e="T476" id="Seg_4487" s="T475">kim-GA</ta>
            <ta e="T477" id="Seg_4488" s="T476">baːr</ta>
            <ta e="T478" id="Seg_4489" s="T477">Sapočnaj-GA</ta>
            <ta e="T479" id="Seg_4490" s="T478">baːr</ta>
            <ta e="T480" id="Seg_4491" s="T479">kim-GA</ta>
            <ta e="T481" id="Seg_4492" s="T480">vaspʼitatʼelʼ-GA</ta>
            <ta e="T482" id="Seg_4493" s="T481">üleleː-Ar</ta>
            <ta e="T483" id="Seg_4494" s="T482">onton</ta>
            <ta e="T484" id="Seg_4495" s="T483">biːr</ta>
            <ta e="T485" id="Seg_4496" s="T484">ogo-m</ta>
            <ta e="T486" id="Seg_4497" s="T485">manna</ta>
            <ta e="T487" id="Seg_4498" s="T486">biːr</ta>
            <ta e="T488" id="Seg_4499" s="T487">kɨːs-I-m</ta>
            <ta e="T489" id="Seg_4500" s="T488">manna</ta>
            <ta e="T490" id="Seg_4501" s="T489">vaspʼitatʼelʼ-LAː-Ar</ta>
            <ta e="T491" id="Seg_4502" s="T490">biːr</ta>
            <ta e="T492" id="Seg_4503" s="T491">ogo-m</ta>
            <ta e="T494" id="Seg_4504" s="T492">du͡o</ta>
            <ta e="T497" id="Seg_4505" s="T496">eː</ta>
            <ta e="T498" id="Seg_4506" s="T497">usku͡ola-GA</ta>
            <ta e="T499" id="Seg_4507" s="T498">manna</ta>
            <ta e="T500" id="Seg_4508" s="T499">Domna</ta>
            <ta e="T501" id="Seg_4509" s="T500">Pʼetrovna</ta>
            <ta e="T508" id="Seg_4510" s="T506">eː</ta>
            <ta e="T509" id="Seg_4511" s="T508">onton</ta>
            <ta e="T510" id="Seg_4512" s="T509">biːr</ta>
            <ta e="T511" id="Seg_4513" s="T510">ɨlgɨn</ta>
            <ta e="T512" id="Seg_4514" s="T511">u͡ol-I-m</ta>
            <ta e="T513" id="Seg_4515" s="T512">dʼaktar-tA</ta>
            <ta e="T514" id="Seg_4516" s="T513">emi͡e</ta>
            <ta e="T515" id="Seg_4517" s="T514">manna</ta>
            <ta e="T516" id="Seg_4518" s="T515">učuːtal-LAː-Ar</ta>
            <ta e="T517" id="Seg_4519" s="T516">Nadja</ta>
            <ta e="T518" id="Seg_4520" s="T517">Kʼirgʼizava</ta>
            <ta e="T519" id="Seg_4521" s="T518">eː</ta>
            <ta e="T521" id="Seg_4522" s="T520">onton</ta>
            <ta e="T522" id="Seg_4523" s="T521">biːr</ta>
            <ta e="T523" id="Seg_4524" s="T522">u͡ol-I-m</ta>
            <ta e="T524" id="Seg_4525" s="T523">bu͡ollagɨna</ta>
            <ta e="T525" id="Seg_4526" s="T524">kim</ta>
            <ta e="T526" id="Seg_4527" s="T525">bʼezdʼexotčʼik</ta>
            <ta e="T527" id="Seg_4528" s="T526">büt-BIT-tA</ta>
            <ta e="T528" id="Seg_4529" s="T527">ü͡ören-An</ta>
            <ta e="T529" id="Seg_4530" s="T528">üleleː-Ar</ta>
            <ta e="T530" id="Seg_4531" s="T529">e-TI-tA</ta>
            <ta e="T531" id="Seg_4532" s="T530">igire-LAr</ta>
            <ta e="T532" id="Seg_4533" s="T531">biːrges-tA</ta>
            <ta e="T533" id="Seg_4534" s="T532">elʼektrik</ta>
            <ta e="T534" id="Seg_4535" s="T533">iti</ta>
            <ta e="T535" id="Seg_4536" s="T534">üleleː-A</ta>
            <ta e="T536" id="Seg_4537" s="T535">hɨrɨt-Ar</ta>
            <ta e="T537" id="Seg_4538" s="T536">anɨ-GA</ta>
            <ta e="T538" id="Seg_4539" s="T537">di͡eri</ta>
            <ta e="T539" id="Seg_4540" s="T538">ü͡ören-An</ta>
            <ta e="T540" id="Seg_4541" s="T539">büt-An</ta>
            <ta e="T541" id="Seg_4542" s="T540">baran</ta>
            <ta e="T542" id="Seg_4543" s="T541">taːk</ta>
            <ta e="T543" id="Seg_4544" s="T542">da</ta>
            <ta e="T544" id="Seg_4545" s="T543">barɨ-tA</ta>
            <ta e="T545" id="Seg_4546" s="T544">ü͡ören-BIT-LArA</ta>
            <ta e="T546" id="Seg_4547" s="T545">onton</ta>
            <ta e="T547" id="Seg_4548" s="T546">ka</ta>
            <ta e="T548" id="Seg_4549" s="T547">biːr</ta>
            <ta e="T549" id="Seg_4550" s="T548">ogo-m</ta>
            <ta e="T550" id="Seg_4551" s="T549">Kires-GA</ta>
            <ta e="T551" id="Seg_4552" s="T550">baːr</ta>
            <ta e="T553" id="Seg_4553" s="T552">eː</ta>
            <ta e="T554" id="Seg_4554" s="T553">mʼilʼisʼija</ta>
            <ta e="T555" id="Seg_4555" s="T554">ü͡örek-tI-n</ta>
            <ta e="T556" id="Seg_4556" s="T555">büt-BIT</ta>
            <ta e="T557" id="Seg_4557" s="T556">e-TI-tA</ta>
            <ta e="T558" id="Seg_4558" s="T557">ol-tI-ŋ</ta>
            <ta e="T559" id="Seg_4559" s="T558">hir</ta>
            <ta e="T560" id="Seg_4560" s="T559">aːjɨ</ta>
            <ta e="T561" id="Seg_4561" s="T560">hir-nI</ta>
            <ta e="T562" id="Seg_4562" s="T561">bar-A-t-An</ta>
            <ta e="T563" id="Seg_4563" s="T562">bar-A-t-An</ta>
            <ta e="T564" id="Seg_4564" s="T563">baran</ta>
            <ta e="T565" id="Seg_4565" s="T564">Kires-GA</ta>
            <ta e="T566" id="Seg_4566" s="T565">dʼaktar-LAːK</ta>
            <ta e="T567" id="Seg_4567" s="T566">Ölöksü͡öj</ta>
            <ta e="T568" id="Seg_4568" s="T567">kör-TI-ŋ</ta>
            <ta e="T569" id="Seg_4569" s="T568">itte</ta>
            <ta e="T570" id="Seg_4570" s="T569">eni</ta>
            <ta e="T571" id="Seg_4571" s="T570">araːha</ta>
            <ta e="T572" id="Seg_4572" s="T571">onton</ta>
            <ta e="T573" id="Seg_4573" s="T572">aŋar-LArA</ta>
            <ta e="T574" id="Seg_4574" s="T573">bulčut-LAr</ta>
            <ta e="T575" id="Seg_4575" s="T574">üs</ta>
            <ta e="T576" id="Seg_4576" s="T575">u͡ol-I-m</ta>
            <ta e="T577" id="Seg_4577" s="T576">bulčut-LAr</ta>
            <ta e="T578" id="Seg_4578" s="T577">bu͡olla</ta>
            <ta e="T579" id="Seg_4579" s="T578">Ölü͡öse</ta>
            <ta e="T580" id="Seg_4580" s="T579">Sʼergʼej</ta>
            <ta e="T581" id="Seg_4581" s="T580">Miːse</ta>
            <ta e="T582" id="Seg_4582" s="T581">Biːče</ta>
            <ta e="T583" id="Seg_4583" s="T582">Biːče</ta>
            <ta e="T584" id="Seg_4584" s="T583">utuj-A</ta>
            <ta e="T585" id="Seg_4585" s="T584">hɨt-Ar</ta>
            <ta e="T586" id="Seg_4586" s="T585">dʼaktar-tA</ta>
            <ta e="T587" id="Seg_4587" s="T586">bu</ta>
            <ta e="T588" id="Seg_4588" s="T587">tur-Ar</ta>
            <ta e="T589" id="Seg_4589" s="T588">ele-tA</ta>
            <ta e="T590" id="Seg_4590" s="T589">du͡o</ta>
            <ta e="T591" id="Seg_4591" s="T590">onton</ta>
            <ta e="T592" id="Seg_4592" s="T591">u͡on</ta>
            <ta e="T593" id="Seg_4593" s="T592">ikki</ta>
            <ta e="T594" id="Seg_4594" s="T593">ogo-LAːK-BIn</ta>
            <ta e="T595" id="Seg_4595" s="T594">hüːrbe</ta>
            <ta e="T596" id="Seg_4596" s="T595">orduk-tA</ta>
            <ta e="T597" id="Seg_4597" s="T596">bi͡es</ta>
            <ta e="T598" id="Seg_4598" s="T597">eː</ta>
            <ta e="T599" id="Seg_4599" s="T598">hette</ta>
            <ta e="T600" id="Seg_4600" s="T599">vnuk-LAːK-BIn</ta>
            <ta e="T601" id="Seg_4601" s="T600">oččogo</ta>
            <ta e="T602" id="Seg_4602" s="T601">deri͡ebine-BIt</ta>
            <ta e="T603" id="Seg_4603" s="T602">innʼe</ta>
            <ta e="T604" id="Seg_4604" s="T603">tü͡ört-u͡on-ttAn</ta>
            <ta e="T605" id="Seg_4605" s="T604">taksa</ta>
            <ta e="T606" id="Seg_4606" s="T605">kihi</ta>
            <ta e="T607" id="Seg_4607" s="T606">kiniːt-LAr-BI-n</ta>
            <ta e="T608" id="Seg_4608" s="T607">barɨ-tI-n</ta>
            <ta e="T609" id="Seg_4609" s="T608">bul-TAK-BInA</ta>
            <ta e="T610" id="Seg_4610" s="T609">elbek-LAr</ta>
            <ta e="T611" id="Seg_4611" s="T610">aːk-TAK-BInA</ta>
            <ta e="T612" id="Seg_4612" s="T611">biːr</ta>
            <ta e="T613" id="Seg_4613" s="T612">kiniːt</ta>
            <ta e="T614" id="Seg_4614" s="T613">ikki</ta>
            <ta e="T615" id="Seg_4615" s="T614">kiniːt</ta>
            <ta e="T616" id="Seg_4616" s="T615">pʼedagoːg-LAr</ta>
            <ta e="T617" id="Seg_4617" s="T616">ogo-LAr-LAːK-LAr</ta>
            <ta e="T618" id="Seg_4618" s="T617">barɨ-LArA</ta>
            <ta e="T619" id="Seg_4619" s="T618">kɨːs-LArA</ta>
            <ta e="T620" id="Seg_4620" s="T619">ü͡ören-A</ta>
            <ta e="T621" id="Seg_4621" s="T620">bar-BIT-LArA</ta>
            <ta e="T622" id="Seg_4622" s="T621">biːr</ta>
            <ta e="T623" id="Seg_4623" s="T622">kiniːt-I-m</ta>
            <ta e="T624" id="Seg_4624" s="T623">saːdik-GA</ta>
            <ta e="T625" id="Seg_4625" s="T624">kim-LArA</ta>
            <ta e="T626" id="Seg_4626" s="T625">kim</ta>
            <ta e="T627" id="Seg_4627" s="T626">e-TI-tA=Ij</ta>
            <ta e="T628" id="Seg_4628" s="T627">ka</ta>
            <ta e="T629" id="Seg_4629" s="T628">dogor</ta>
            <ta e="T630" id="Seg_4630" s="T629">zabʼedusaj-LAr</ta>
            <ta e="T631" id="Seg_4631" s="T630">ikki</ta>
            <ta e="T632" id="Seg_4632" s="T631">kiniːt-I-m</ta>
            <ta e="T633" id="Seg_4633" s="T632">učuːtal-LAr</ta>
            <ta e="T634" id="Seg_4634" s="T633">pʼedagoːg-LAr</ta>
            <ta e="T635" id="Seg_4635" s="T634">barɨ-tA</ta>
            <ta e="T636" id="Seg_4636" s="T635">ülehit-LAr</ta>
            <ta e="T637" id="Seg_4637" s="T636">onton</ta>
            <ta e="T638" id="Seg_4638" s="T637">biːr</ta>
            <ta e="T639" id="Seg_4639" s="T638">kütü͡öt-I-m</ta>
            <ta e="T640" id="Seg_4640" s="T639">iti</ta>
            <ta e="T641" id="Seg_4641" s="T640">taŋara-LAː-TI-tA</ta>
            <ta e="T642" id="Seg_4642" s="T641">biːr</ta>
            <ta e="T643" id="Seg_4643" s="T642">kütü͡öt-I-m</ta>
            <ta e="T644" id="Seg_4644" s="T643">ɨ͡arɨj</ta>
            <ta e="T645" id="Seg_4645" s="T644">bu͡ol-BIT</ta>
            <ta e="T646" id="Seg_4646" s="T645">ogo</ta>
            <ta e="T647" id="Seg_4647" s="T646">Domna</ta>
            <ta e="T648" id="Seg_4648" s="T647">Pʼetrovna</ta>
            <ta e="T649" id="Seg_4649" s="T648">gi͡en-tA</ta>
            <ta e="T650" id="Seg_4650" s="T649">ol</ta>
            <ta e="T651" id="Seg_4651" s="T650">ereːti</ta>
            <ta e="T652" id="Seg_4652" s="T651">bult-nI</ta>
            <ta e="T653" id="Seg_4653" s="T652">delbi</ta>
            <ta e="T654" id="Seg_4654" s="T653">ölör-Ar</ta>
            <ta e="T655" id="Seg_4655" s="T654">atak-tA</ta>
            <ta e="T656" id="Seg_4656" s="T655">hu͡ok</ta>
            <ta e="T657" id="Seg_4657" s="T656">beje-tA</ta>
            <ta e="T658" id="Seg_4658" s="T657">biːr</ta>
            <ta e="T659" id="Seg_4659" s="T658">kütü͡öt-I-m</ta>
            <ta e="T660" id="Seg_4660" s="T659">Sapočnaj-GA</ta>
            <ta e="T661" id="Seg_4661" s="T660">axoːtnʼik</ta>
            <ta e="T662" id="Seg_4662" s="T661">kɨːs-tA</ta>
            <ta e="T663" id="Seg_4663" s="T662">ü͡ören-A</ta>
            <ta e="T664" id="Seg_4664" s="T663">bar-BIT</ta>
            <ta e="T665" id="Seg_4665" s="T664">dʼe</ta>
            <ta e="T666" id="Seg_4666" s="T665">tu͡ok-nI</ta>
            <ta e="T667" id="Seg_4667" s="T666">kepseː-IAK-BI-n=Ij</ta>
            <ta e="T671" id="Seg_4668" s="T670">dʼe</ta>
         </annotation>
         <annotation name="ge" tierref="ge-KiPP">
            <ta e="T2" id="Seg_4669" s="T1">this-ABL</ta>
            <ta e="T3" id="Seg_4670" s="T2">hear-HAB.[3SG]</ta>
            <ta e="T4" id="Seg_4671" s="T3">Q</ta>
            <ta e="T9" id="Seg_4672" s="T6">AFFIRM</ta>
            <ta e="T15" id="Seg_4673" s="T14">well</ta>
            <ta e="T17" id="Seg_4674" s="T15">what-ACC</ta>
            <ta e="T18" id="Seg_4675" s="T17">ask-PRS.[3SG]</ta>
            <ta e="T19" id="Seg_4676" s="T18">well</ta>
            <ta e="T20" id="Seg_4677" s="T19">well</ta>
            <ta e="T21" id="Seg_4678" s="T20">at.first</ta>
            <ta e="T22" id="Seg_4679" s="T21">any-1SG-ACC</ta>
            <ta e="T24" id="Seg_4680" s="T22">ask-FREQ-PRS.[3SG]</ta>
            <ta e="T25" id="Seg_4681" s="T24">MOD</ta>
            <ta e="T30" id="Seg_4682" s="T29">hear-PRS.[3SG]</ta>
            <ta e="T33" id="Seg_4683" s="T31">eh</ta>
            <ta e="T51" id="Seg_4684" s="T49">INTJ</ta>
            <ta e="T52" id="Seg_4685" s="T51">when</ta>
            <ta e="T53" id="Seg_4686" s="T52">stop-FUT-1SG=Q</ta>
            <ta e="T54" id="Seg_4687" s="T53">that</ta>
            <ta e="T55" id="Seg_4688" s="T54">evening-DAT/LOC</ta>
            <ta e="T56" id="Seg_4689" s="T55">EMPH</ta>
            <ta e="T57" id="Seg_4690" s="T56">until</ta>
            <ta e="T68" id="Seg_4691" s="T66">Korgo.[NOM]</ta>
            <ta e="T72" id="Seg_4692" s="T70">Korgo-DAT/LOC</ta>
            <ta e="T73" id="Seg_4693" s="T72">give.birth-PST2-EP-1SG</ta>
            <ta e="T74" id="Seg_4694" s="T73">EMPH-here</ta>
            <ta e="T75" id="Seg_4695" s="T74">self-1SG.[NOM]</ta>
            <ta e="T76" id="Seg_4696" s="T75">earth-1SG-DAT/LOC</ta>
            <ta e="T77" id="Seg_4697" s="T76">illegitimate-1SG</ta>
            <ta e="T78" id="Seg_4698" s="T77">know-PRS-2SG</ta>
            <ta e="T79" id="Seg_4699" s="T78">illegitimate.[NOM]</ta>
            <ta e="T80" id="Seg_4700" s="T79">name-3SG-ACC</ta>
            <ta e="T107" id="Seg_4701" s="T106">father-POSS</ta>
            <ta e="T108" id="Seg_4702" s="T107">NEG</ta>
            <ta e="T112" id="Seg_4703" s="T111">well</ta>
            <ta e="T113" id="Seg_4704" s="T112">1SG.[NOM]</ta>
            <ta e="T115" id="Seg_4705" s="T113">granny-1SG.[NOM]</ta>
            <ta e="T117" id="Seg_4706" s="T115">bring.up-PST1-3SG</ta>
            <ta e="T121" id="Seg_4707" s="T120">granny.[NOM]</ta>
            <ta e="T122" id="Seg_4708" s="T121">educate-VBZ-PST2-3SG</ta>
            <ta e="T123" id="Seg_4709" s="T122">mum-1SG.[NOM]</ta>
            <ta e="T124" id="Seg_4710" s="T123">mum-3SG.[NOM]</ta>
            <ta e="T129" id="Seg_4711" s="T128">then</ta>
            <ta e="T130" id="Seg_4712" s="T129">1PL.[NOM]</ta>
            <ta e="T131" id="Seg_4713" s="T130">family-PL.[NOM]</ta>
            <ta e="T132" id="Seg_4714" s="T131">completely</ta>
            <ta e="T133" id="Seg_4715" s="T132">die-CVB.SEQ-3PL</ta>
            <ta e="T134" id="Seg_4716" s="T133">orphan.[NOM]</ta>
            <ta e="T135" id="Seg_4717" s="T134">live-EP-PST2-1PL</ta>
            <ta e="T136" id="Seg_4718" s="T135">old.woman-ACC</ta>
            <ta e="T137" id="Seg_4719" s="T136">with</ta>
            <ta e="T138" id="Seg_4720" s="T137">granny-1SG-ACC</ta>
            <ta e="T139" id="Seg_4721" s="T138">with</ta>
            <ta e="T140" id="Seg_4722" s="T139">then</ta>
            <ta e="T142" id="Seg_4723" s="T141">Novorybnoe.[NOM]</ta>
            <ta e="T143" id="Seg_4724" s="T142">to</ta>
            <ta e="T144" id="Seg_4725" s="T143">go-PST2-EP-1SG</ta>
            <ta e="T145" id="Seg_4726" s="T144">then</ta>
            <ta e="T146" id="Seg_4727" s="T145">that</ta>
            <ta e="T147" id="Seg_4728" s="T146">go-PST2-1PL</ta>
            <ta e="T149" id="Seg_4729" s="T148">grandmother-1SG-ACC</ta>
            <ta e="T150" id="Seg_4730" s="T149">mother-1SG-ACC</ta>
            <ta e="T151" id="Seg_4731" s="T150">with</ta>
            <ta e="T152" id="Seg_4732" s="T151">mother.[NOM]</ta>
            <ta e="T153" id="Seg_4733" s="T152">say-HAB-1SG</ta>
            <ta e="T154" id="Seg_4734" s="T153">that-3SG-1SG-ACC</ta>
            <ta e="T155" id="Seg_4735" s="T154">mum-1SG.[NOM]</ta>
            <ta e="T156" id="Seg_4736" s="T155">mum-1SG.[NOM]</ta>
            <ta e="T157" id="Seg_4737" s="T156">however</ta>
            <ta e="T158" id="Seg_4738" s="T157">one</ta>
            <ta e="T159" id="Seg_4739" s="T158">well</ta>
            <ta e="T160" id="Seg_4740" s="T159">then</ta>
            <ta e="T161" id="Seg_4741" s="T160">back</ta>
            <ta e="T162" id="Seg_4742" s="T161">come-PST2-1PL</ta>
            <ta e="T163" id="Seg_4743" s="T162">mum-1SG.[NOM]</ta>
            <ta e="T164" id="Seg_4744" s="T163">old.woman.[NOM]</ta>
            <ta e="T166" id="Seg_4745" s="T165">granny-1SG.[NOM]</ta>
            <ta e="T167" id="Seg_4746" s="T166">die-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T168" id="Seg_4747" s="T167">die-CVB.SEQ</ta>
            <ta e="T169" id="Seg_4748" s="T168">after</ta>
            <ta e="T170" id="Seg_4749" s="T169">revive-EP-PST2-3SG</ta>
            <ta e="T171" id="Seg_4750" s="T170">that</ta>
            <ta e="T172" id="Seg_4751" s="T171">place-DAT/LOC</ta>
            <ta e="T173" id="Seg_4752" s="T172">then</ta>
            <ta e="T174" id="Seg_4753" s="T173">back</ta>
            <ta e="T175" id="Seg_4754" s="T174">come-PST2-1PL</ta>
            <ta e="T176" id="Seg_4755" s="T175">that</ta>
            <ta e="T177" id="Seg_4756" s="T176">come-PTCP.PST-1PL.[NOM]</ta>
            <ta e="T178" id="Seg_4757" s="T177">after</ta>
            <ta e="T179" id="Seg_4758" s="T178">ten</ta>
            <ta e="T180" id="Seg_4759" s="T179">six</ta>
            <ta e="T181" id="Seg_4760" s="T180">year-PROPR-1SG-DAT/LOC</ta>
            <ta e="T182" id="Seg_4761" s="T181">1SG-ACC</ta>
            <ta e="T183" id="Seg_4762" s="T182">husband-DAT/LOC</ta>
            <ta e="T184" id="Seg_4763" s="T183">give-PST2-3SG</ta>
            <ta e="T185" id="Seg_4764" s="T184">1SG.[NOM]</ta>
            <ta e="T186" id="Seg_4765" s="T185">die-TEMP-1SG</ta>
            <ta e="T187" id="Seg_4766" s="T186">2SG-ACC</ta>
            <ta e="T188" id="Seg_4767" s="T187">slave.[NOM]</ta>
            <ta e="T189" id="Seg_4768" s="T188">make-FUT-3PL</ta>
            <ta e="T190" id="Seg_4769" s="T189">say-PST1-3SG</ta>
            <ta e="T191" id="Seg_4770" s="T190">devil</ta>
            <ta e="T192" id="Seg_4771" s="T191">what-ACC</ta>
            <ta e="T193" id="Seg_4772" s="T192">NEG</ta>
            <ta e="T194" id="Seg_4773" s="T193">clean-1SG</ta>
            <ta e="T195" id="Seg_4774" s="T194">though</ta>
            <ta e="T196" id="Seg_4775" s="T195">wash.oneself-CVB.SIM</ta>
            <ta e="T197" id="Seg_4776" s="T196">NEG</ta>
            <ta e="T198" id="Seg_4777" s="T197">not.yet-1SG</ta>
            <ta e="T199" id="Seg_4778" s="T198">husband-DAT/LOC</ta>
            <ta e="T200" id="Seg_4779" s="T199">go-PST2-EP-1SG</ta>
            <ta e="T201" id="Seg_4780" s="T200">cry-CVB.SIM-cry-CVB.SIM</ta>
            <ta e="T202" id="Seg_4781" s="T201">husband-DAT/LOC</ta>
            <ta e="T203" id="Seg_4782" s="T202">give-PRS-3PL</ta>
            <ta e="T204" id="Seg_4783" s="T203">sleep-CAUS-PRS-3PL</ta>
            <ta e="T205" id="Seg_4784" s="T204">cry-CVB.SIM-cry-CVB.SIM</ta>
            <ta e="T206" id="Seg_4785" s="T205">fall.asleep-PRS-1SG</ta>
            <ta e="T207" id="Seg_4786" s="T206">husband-1SG-ABL</ta>
            <ta e="T208" id="Seg_4787" s="T207">be.afraid-PRS-1SG</ta>
            <ta e="T209" id="Seg_4788" s="T208">clothes-COM</ta>
            <ta e="T210" id="Seg_4789" s="T209">sleep-PRS-1SG</ta>
            <ta e="T211" id="Seg_4790" s="T210">eh</ta>
            <ta e="T212" id="Seg_4791" s="T211">well</ta>
            <ta e="T213" id="Seg_4792" s="T212">then</ta>
            <ta e="T214" id="Seg_4793" s="T213">year-ACC</ta>
            <ta e="T215" id="Seg_4794" s="T214">year-ACC</ta>
            <ta e="T216" id="Seg_4795" s="T215">clothes-PROPR</ta>
            <ta e="T217" id="Seg_4796" s="T216">sleep-PST2-EP-1SG</ta>
            <ta e="T218" id="Seg_4797" s="T217">then</ta>
            <ta e="T219" id="Seg_4798" s="T218">well</ta>
            <ta e="T220" id="Seg_4799" s="T219">later</ta>
            <ta e="T221" id="Seg_4800" s="T220">learn-PRS-1SG</ta>
            <ta e="T222" id="Seg_4801" s="T221">EMPH</ta>
            <ta e="T223" id="Seg_4802" s="T222">husband</ta>
            <ta e="T224" id="Seg_4803" s="T223">husband</ta>
            <ta e="T225" id="Seg_4804" s="T224">husband-1SG-DAT/LOC</ta>
            <ta e="T226" id="Seg_4805" s="T225">that</ta>
            <ta e="T227" id="Seg_4806" s="T226">name-3SG.[NOM]</ta>
            <ta e="T233" id="Seg_4807" s="T232">ten</ta>
            <ta e="T234" id="Seg_4808" s="T233">seven</ta>
            <ta e="T235" id="Seg_4809" s="T234">ten</ta>
            <ta e="T236" id="Seg_4810" s="T235">eight</ta>
            <ta e="T237" id="Seg_4811" s="T236">ten</ta>
            <ta e="T238" id="Seg_4812" s="T237">eight</ta>
            <ta e="T239" id="Seg_4813" s="T238">year-PROPR-1SG-DAT/LOC</ta>
            <ta e="T240" id="Seg_4814" s="T239">child-VBZ-PST2-EP-1SG</ta>
            <ta e="T241" id="Seg_4815" s="T240">first</ta>
            <ta e="T242" id="Seg_4816" s="T241">child-1SG.[NOM]</ta>
            <ta e="T243" id="Seg_4817" s="T242">here</ta>
            <ta e="T244" id="Seg_4818" s="T243">there.is</ta>
            <ta e="T245" id="Seg_4819" s="T244">then</ta>
            <ta e="T246" id="Seg_4820" s="T245">child-VBZ-PST1-1SG</ta>
            <ta e="T247" id="Seg_4821" s="T246">child-VBZ-PST1-1SG</ta>
            <ta e="T248" id="Seg_4822" s="T247">year.[NOM]</ta>
            <ta e="T249" id="Seg_4823" s="T248">every</ta>
            <ta e="T250" id="Seg_4824" s="T249">child-VBZ-PRS-1SG</ta>
            <ta e="T251" id="Seg_4825" s="T250">year.[NOM]</ta>
            <ta e="T252" id="Seg_4826" s="T251">every</ta>
            <ta e="T254" id="Seg_4827" s="T252">child-VBZ-PRS-1SG</ta>
            <ta e="T259" id="Seg_4828" s="T258">AFFIRM</ta>
            <ta e="T260" id="Seg_4829" s="T259">this.EMPH</ta>
            <ta e="T261" id="Seg_4830" s="T260">earth-DAT/LOC</ta>
            <ta e="T262" id="Seg_4831" s="T261">tundra-DAT/LOC</ta>
            <ta e="T263" id="Seg_4832" s="T262">go-CVB.SEQ-1PL</ta>
            <ta e="T264" id="Seg_4833" s="T263">tundra-DAT/LOC</ta>
            <ta e="T265" id="Seg_4834" s="T264">be-PST1-1PL</ta>
            <ta e="T266" id="Seg_4835" s="T265">old.man-EP-1SG.[NOM]</ta>
            <ta e="T267" id="Seg_4836" s="T266">cashier.[NOM]</ta>
            <ta e="T268" id="Seg_4837" s="T267">be-PST1-3SG</ta>
            <ta e="T269" id="Seg_4838" s="T268">very</ta>
            <ta e="T270" id="Seg_4839" s="T269">good</ta>
            <ta e="T271" id="Seg_4840" s="T270">human.being.[NOM]</ta>
            <ta e="T272" id="Seg_4841" s="T271">be-PST1-3SG</ta>
            <ta e="T273" id="Seg_4842" s="T272">capable</ta>
            <ta e="T274" id="Seg_4843" s="T273">very</ta>
            <ta e="T275" id="Seg_4844" s="T274">hunter.[NOM]</ta>
            <ta e="T276" id="Seg_4845" s="T275">animal-ACC</ta>
            <ta e="T277" id="Seg_4846" s="T276">EMPH</ta>
            <ta e="T278" id="Seg_4847" s="T277">hunt-PST2-3SG</ta>
            <ta e="T279" id="Seg_4848" s="T278">house.[NOM]</ta>
            <ta e="T280" id="Seg_4849" s="T279">and</ta>
            <ta e="T281" id="Seg_4850" s="T280">build-PST2-3SG</ta>
            <ta e="T282" id="Seg_4851" s="T281">cashier-DAT/LOC</ta>
            <ta e="T283" id="Seg_4852" s="T282">ten</ta>
            <ta e="T284" id="Seg_4853" s="T283">five</ta>
            <ta e="T285" id="Seg_4854" s="T284">year-ACC</ta>
            <ta e="T286" id="Seg_4855" s="T285">sit-PST2-3SG</ta>
            <ta e="T287" id="Seg_4856" s="T286">then</ta>
            <ta e="T288" id="Seg_4857" s="T287">one</ta>
            <ta e="T289" id="Seg_4858" s="T288">year-ACC</ta>
            <ta e="T290" id="Seg_4859" s="T289">partorg-DAT/LOC</ta>
            <ta e="T291" id="Seg_4860" s="T290">live-PST2-3SG</ta>
            <ta e="T292" id="Seg_4861" s="T291">then</ta>
            <ta e="T293" id="Seg_4862" s="T292">house-PL-ACC</ta>
            <ta e="T294" id="Seg_4863" s="T293">build-PST2-3SG</ta>
            <ta e="T295" id="Seg_4864" s="T294">this</ta>
            <ta e="T296" id="Seg_4865" s="T295">stand-PRS.[3SG]</ta>
            <ta e="T297" id="Seg_4866" s="T296">build-PTCP.PST</ta>
            <ta e="T298" id="Seg_4867" s="T297">house-PL-3SG.[NOM]</ta>
            <ta e="T299" id="Seg_4868" s="T298">then</ta>
            <ta e="T300" id="Seg_4869" s="T299">ten</ta>
            <ta e="T301" id="Seg_4870" s="T300">two</ta>
            <ta e="T302" id="Seg_4871" s="T301">child-PROPR-1PL</ta>
            <ta e="T303" id="Seg_4872" s="T302">then</ta>
            <ta e="T304" id="Seg_4873" s="T303">completely</ta>
            <ta e="T305" id="Seg_4874" s="T304">one</ta>
            <ta e="T672" id="Seg_4875" s="T305">NEG</ta>
            <ta e="T306" id="Seg_4876" s="T672">human.being-ACC</ta>
            <ta e="T307" id="Seg_4877" s="T306">human.being-DAT/LOC</ta>
            <ta e="T308" id="Seg_4878" s="T307">give-PST2.NEG-EP-1SG</ta>
            <ta e="T309" id="Seg_4879" s="T308">child-PL-1SG-ACC</ta>
            <ta e="T310" id="Seg_4880" s="T309">self-1SG.[NOM]</ta>
            <ta e="T311" id="Seg_4881" s="T310">bring.up-PTCP.FUT.[NOM]</ta>
            <ta e="T312" id="Seg_4882" s="T311">be-CVB.SEQ-1SG</ta>
            <ta e="T313" id="Seg_4883" s="T312">human.being-DAT/LOC</ta>
            <ta e="T314" id="Seg_4884" s="T313">give-NEG-1SG</ta>
            <ta e="T315" id="Seg_4885" s="T314">feel.sorry-PRS-1SG</ta>
            <ta e="T316" id="Seg_4886" s="T315">child-PL-1SG-ACC</ta>
            <ta e="T317" id="Seg_4887" s="T316">that-PL-1SG-ACC</ta>
            <ta e="T318" id="Seg_4888" s="T317">bring.up-EP-MED-CVB.SIM</ta>
            <ta e="T319" id="Seg_4889" s="T318">bring.up-EP-PTCP.PST-EP-INSTR</ta>
            <ta e="T320" id="Seg_4890" s="T319">cradle-PROPR</ta>
            <ta e="T321" id="Seg_4891" s="T320">child-PROPR.[NOM]</ta>
            <ta e="T322" id="Seg_4892" s="T321">net-VBZ-EP-RECP/COLL-HAB-1SG</ta>
            <ta e="T323" id="Seg_4893" s="T322">old.man-1SG-ACC</ta>
            <ta e="T324" id="Seg_4894" s="T323">with</ta>
            <ta e="T325" id="Seg_4895" s="T324">sick.[NOM]</ta>
            <ta e="T326" id="Seg_4896" s="T325">become-PTCP.PST-3SG-ACC</ta>
            <ta e="T327" id="Seg_4897" s="T326">after</ta>
            <ta e="T328" id="Seg_4898" s="T327">well</ta>
            <ta e="T329" id="Seg_4899" s="T328">child-PL-1PL.[NOM]</ta>
            <ta e="T330" id="Seg_4900" s="T329">grow-CVB.SEQ-3PL</ta>
            <ta e="T331" id="Seg_4901" s="T330">learn-CVB.SIM</ta>
            <ta e="T332" id="Seg_4902" s="T331">go-FREQ-PST2-3PL</ta>
            <ta e="T333" id="Seg_4903" s="T332">then</ta>
            <ta e="T334" id="Seg_4904" s="T333">later</ta>
            <ta e="T335" id="Seg_4905" s="T334">two</ta>
            <ta e="T336" id="Seg_4906" s="T335">big</ta>
            <ta e="T337" id="Seg_4907" s="T336">child-1SG.[NOM]</ta>
            <ta e="T338" id="Seg_4908" s="T337">reindeer.[NOM]</ta>
            <ta e="T339" id="Seg_4909" s="T338">education-3SG-DAT/LOC</ta>
            <ta e="T340" id="Seg_4910" s="T339">go-PST2-3PL</ta>
            <ta e="T341" id="Seg_4911" s="T340">son-1PL.[NOM]</ta>
            <ta e="T342" id="Seg_4912" s="T341">daughter-1PL.[NOM]</ta>
            <ta e="T343" id="Seg_4913" s="T342">then</ta>
            <ta e="T344" id="Seg_4914" s="T343">one</ta>
            <ta e="T345" id="Seg_4915" s="T344">child-1PL.[NOM]</ta>
            <ta e="T346" id="Seg_4916" s="T345">grow-CVB.SEQ</ta>
            <ta e="T347" id="Seg_4917" s="T346">Krasnoyarsk-DAT/LOC</ta>
            <ta e="T348" id="Seg_4918" s="T347">learn-CVB.SIM</ta>
            <ta e="T349" id="Seg_4919" s="T348">go-PST2-3SG</ta>
            <ta e="T350" id="Seg_4920" s="T349">now</ta>
            <ta e="T351" id="Seg_4921" s="T350">therapist-DAT/LOC</ta>
            <ta e="T352" id="Seg_4922" s="T351">work-CVB.SIM</ta>
            <ta e="T353" id="Seg_4923" s="T352">lie-PRS.[3SG]</ta>
            <ta e="T354" id="Seg_4924" s="T353">Dudinka-DAT/LOC</ta>
            <ta e="T355" id="Seg_4925" s="T354">then</ta>
            <ta e="T356" id="Seg_4926" s="T355">one</ta>
            <ta e="T357" id="Seg_4927" s="T356">one</ta>
            <ta e="T358" id="Seg_4928" s="T357">child-1SG.[NOM]</ta>
            <ta e="T359" id="Seg_4929" s="T358">though</ta>
            <ta e="T360" id="Seg_4930" s="T359">again</ta>
            <ta e="T361" id="Seg_4931" s="T360">learn-CVB.SEQ</ta>
            <ta e="T362" id="Seg_4932" s="T361">two</ta>
            <ta e="T363" id="Seg_4933" s="T362">institute-ACC</ta>
            <ta e="T364" id="Seg_4934" s="T363">stop-PST2-3SG</ta>
            <ta e="T365" id="Seg_4935" s="T364">then</ta>
            <ta e="T366" id="Seg_4936" s="T365">Krasnoyarsk-DAT/LOC</ta>
            <ta e="T367" id="Seg_4937" s="T366">there.is</ta>
            <ta e="T369" id="Seg_4938" s="T368">Sergey.[NOM]</ta>
            <ta e="T370" id="Seg_4939" s="T369">that-3SG-1SG.[NOM]</ta>
            <ta e="T371" id="Seg_4940" s="T370">woman-PROPR</ta>
            <ta e="T372" id="Seg_4941" s="T371">child.[NOM]</ta>
            <ta e="T373" id="Seg_4942" s="T372">that</ta>
            <ta e="T374" id="Seg_4943" s="T373">two</ta>
            <ta e="T375" id="Seg_4944" s="T374">son-PROPR.[NOM]</ta>
            <ta e="T376" id="Seg_4945" s="T375">Kazakh-PL-ABL</ta>
            <ta e="T377" id="Seg_4946" s="T376">woman-VBZ-PST2.[3SG]</ta>
            <ta e="T378" id="Seg_4947" s="T377">storage-PROPR-ABL</ta>
            <ta e="T379" id="Seg_4948" s="T378">now</ta>
            <ta e="T380" id="Seg_4949" s="T379">though</ta>
            <ta e="T381" id="Seg_4950" s="T380">guard-DAT/LOC</ta>
            <ta e="T382" id="Seg_4951" s="T381">guard-DAT/LOC</ta>
            <ta e="T383" id="Seg_4952" s="T382">work-PRS.[3SG]</ta>
            <ta e="T384" id="Seg_4953" s="T383">police-ACC</ta>
            <ta e="T385" id="Seg_4954" s="T384">stop-CVB.SEQ</ta>
            <ta e="T386" id="Seg_4955" s="T385">after</ta>
            <ta e="T387" id="Seg_4956" s="T386">before</ta>
            <ta e="T388" id="Seg_4957" s="T387">reindeer.[NOM]</ta>
            <ta e="T389" id="Seg_4958" s="T388">worker-3SG.[NOM]</ta>
            <ta e="T390" id="Seg_4959" s="T389">be-PST2-3SG</ta>
            <ta e="T391" id="Seg_4960" s="T390">then</ta>
            <ta e="T392" id="Seg_4961" s="T391">one</ta>
            <ta e="T393" id="Seg_4962" s="T392">child-1SG.[NOM]</ta>
            <ta e="T394" id="Seg_4963" s="T393">cook-DAT/LOC</ta>
            <ta e="T395" id="Seg_4964" s="T394">Khatanga-DAT/LOC</ta>
            <ta e="T396" id="Seg_4965" s="T395">work-PRS.[3SG]</ta>
            <ta e="T397" id="Seg_4966" s="T396">important</ta>
            <ta e="T398" id="Seg_4967" s="T397">cook.[NOM]</ta>
            <ta e="T399" id="Seg_4968" s="T398">one</ta>
            <ta e="T400" id="Seg_4969" s="T399">daughter-EP-1SG.[NOM]</ta>
            <ta e="T401" id="Seg_4970" s="T400">one</ta>
            <ta e="T403" id="Seg_4971" s="T402">what.[NOM]</ta>
            <ta e="T405" id="Seg_4972" s="T404">Khatanga-DAT/LOC</ta>
            <ta e="T406" id="Seg_4973" s="T405">Marina</ta>
            <ta e="T410" id="Seg_4974" s="T409">no</ta>
            <ta e="T411" id="Seg_4975" s="T410">learn-PST2-3SG</ta>
            <ta e="T412" id="Seg_4976" s="T411">AFFIRM</ta>
            <ta e="T413" id="Seg_4977" s="T412">that.[NOM]</ta>
            <ta e="T414" id="Seg_4978" s="T413">to</ta>
            <ta e="T415" id="Seg_4979" s="T414">Krasnoyarsk-DAT/LOC</ta>
            <ta e="T416" id="Seg_4980" s="T415">learn-PST2-3SG</ta>
            <ta e="T417" id="Seg_4981" s="T416">then</ta>
            <ta e="T419" id="Seg_4982" s="T418">doctor.[NOM]</ta>
            <ta e="T420" id="Seg_4983" s="T419">daughter-EP-1SG.[NOM]</ta>
            <ta e="T421" id="Seg_4984" s="T420">Dudinka-DAT/LOC</ta>
            <ta e="T422" id="Seg_4985" s="T421">work-PRS.[3SG]</ta>
            <ta e="T423" id="Seg_4986" s="T422">therapist-DAT/LOC</ta>
            <ta e="T424" id="Seg_4987" s="T423">work-PRS.[3SG]</ta>
            <ta e="T425" id="Seg_4988" s="T424">now</ta>
            <ta e="T426" id="Seg_4989" s="T425">three</ta>
            <ta e="T427" id="Seg_4990" s="T426">child-PROPR.[NOM]</ta>
            <ta e="T428" id="Seg_4991" s="T427">old</ta>
            <ta e="T429" id="Seg_4992" s="T428">woman.[NOM]</ta>
            <ta e="T430" id="Seg_4993" s="T429">retirement-PROPR</ta>
            <ta e="T431" id="Seg_4994" s="T430">child-1SG.[NOM]</ta>
            <ta e="T432" id="Seg_4995" s="T431">every-3SG.[NOM]</ta>
            <ta e="T433" id="Seg_4996" s="T432">retirement-PROPR.[NOM]</ta>
            <ta e="T434" id="Seg_4997" s="T433">three</ta>
            <ta e="T435" id="Seg_4998" s="T434">child-1SG.[NOM]</ta>
            <ta e="T436" id="Seg_4999" s="T435">retirement-PROPR-3PL</ta>
            <ta e="T437" id="Seg_5000" s="T436">big-PL-EP-1SG.[NOM]</ta>
            <ta e="T438" id="Seg_5001" s="T437">now</ta>
            <ta e="T439" id="Seg_5002" s="T438">one.out.of.two-3SG.[NOM]</ta>
            <ta e="T440" id="Seg_5003" s="T439">though</ta>
            <ta e="T441" id="Seg_5004" s="T440">fisherman-VBZ-PRS.[3SG]</ta>
            <ta e="T442" id="Seg_5005" s="T441">one.out.of.two-3SG.[NOM]</ta>
            <ta e="T443" id="Seg_5006" s="T442">MOD</ta>
            <ta e="T444" id="Seg_5007" s="T443">now</ta>
            <ta e="T445" id="Seg_5008" s="T444">be.old-PST2.[3SG]</ta>
            <ta e="T446" id="Seg_5009" s="T445">retirement-PROPR</ta>
            <ta e="T447" id="Seg_5010" s="T446">Vera.[NOM]</ta>
            <ta e="T448" id="Seg_5011" s="T447">husband-3SG.[NOM]</ta>
            <ta e="T449" id="Seg_5012" s="T448">this.EMPH-INTNS</ta>
            <ta e="T450" id="Seg_5013" s="T449">die-PST2-3SG</ta>
            <ta e="T451" id="Seg_5014" s="T450">two</ta>
            <ta e="T452" id="Seg_5015" s="T451">child-PROPR.[NOM]</ta>
            <ta e="T453" id="Seg_5016" s="T452">three-ORD-3PL.[NOM]</ta>
            <ta e="T454" id="Seg_5017" s="T453">die-PST2-3SG</ta>
            <ta e="T455" id="Seg_5018" s="T454">big</ta>
            <ta e="T456" id="Seg_5019" s="T455">son-EP-1SG.[NOM]</ta>
            <ta e="T457" id="Seg_5020" s="T456">three</ta>
            <ta e="T458" id="Seg_5021" s="T457">child-PROPR.[NOM]</ta>
            <ta e="T459" id="Seg_5022" s="T458">child.[NOM]</ta>
            <ta e="T460" id="Seg_5023" s="T459">one</ta>
            <ta e="T461" id="Seg_5024" s="T460">child-3SG.[NOM]</ta>
            <ta e="T462" id="Seg_5025" s="T461">woman-PROPR</ta>
            <ta e="T463" id="Seg_5026" s="T462">child-PROPR.[NOM]</ta>
            <ta e="T464" id="Seg_5027" s="T463">other.of.two-3PL.[NOM]</ta>
            <ta e="T465" id="Seg_5028" s="T464">child-VBZ-CVB.SIM</ta>
            <ta e="T467" id="Seg_5029" s="T465">not.yet-3PL</ta>
            <ta e="T471" id="Seg_5030" s="T469">AFFIRM</ta>
            <ta e="T472" id="Seg_5031" s="T471">then</ta>
            <ta e="T473" id="Seg_5032" s="T472">one</ta>
            <ta e="T474" id="Seg_5033" s="T473">child-1SG.[NOM]</ta>
            <ta e="T475" id="Seg_5034" s="T474">well</ta>
            <ta e="T476" id="Seg_5035" s="T475">who-DAT/LOC</ta>
            <ta e="T477" id="Seg_5036" s="T476">there.is</ta>
            <ta e="T478" id="Seg_5037" s="T477">Sopochnoe-DAT/LOC</ta>
            <ta e="T479" id="Seg_5038" s="T478">there.is</ta>
            <ta e="T480" id="Seg_5039" s="T479">who-DAT/LOC</ta>
            <ta e="T481" id="Seg_5040" s="T480">educator-DAT/LOC</ta>
            <ta e="T482" id="Seg_5041" s="T481">work-PRS.[3SG]</ta>
            <ta e="T483" id="Seg_5042" s="T482">then</ta>
            <ta e="T484" id="Seg_5043" s="T483">one</ta>
            <ta e="T485" id="Seg_5044" s="T484">child-1SG.[NOM]</ta>
            <ta e="T486" id="Seg_5045" s="T485">here</ta>
            <ta e="T487" id="Seg_5046" s="T486">one</ta>
            <ta e="T488" id="Seg_5047" s="T487">daughter-EP-1SG.[NOM]</ta>
            <ta e="T489" id="Seg_5048" s="T488">here</ta>
            <ta e="T490" id="Seg_5049" s="T489">educator-VBZ-PRS.[3SG]</ta>
            <ta e="T491" id="Seg_5050" s="T490">one</ta>
            <ta e="T492" id="Seg_5051" s="T491">child-1SG.[NOM]</ta>
            <ta e="T494" id="Seg_5052" s="T492">MOD</ta>
            <ta e="T497" id="Seg_5053" s="T496">AFFIRM</ta>
            <ta e="T498" id="Seg_5054" s="T497">school-DAT/LOC</ta>
            <ta e="T499" id="Seg_5055" s="T498">here</ta>
            <ta e="T500" id="Seg_5056" s="T499">Domna</ta>
            <ta e="T501" id="Seg_5057" s="T500">Petrovna.[NOM]</ta>
            <ta e="T508" id="Seg_5058" s="T506">eh</ta>
            <ta e="T509" id="Seg_5059" s="T508">then</ta>
            <ta e="T510" id="Seg_5060" s="T509">one</ta>
            <ta e="T511" id="Seg_5061" s="T510">little</ta>
            <ta e="T512" id="Seg_5062" s="T511">son-EP-1SG.[NOM]</ta>
            <ta e="T513" id="Seg_5063" s="T512">woman-3SG.[NOM]</ta>
            <ta e="T514" id="Seg_5064" s="T513">also</ta>
            <ta e="T515" id="Seg_5065" s="T514">here</ta>
            <ta e="T516" id="Seg_5066" s="T515">teacher-VBZ-PRS.[3SG]</ta>
            <ta e="T517" id="Seg_5067" s="T516">Nadia</ta>
            <ta e="T518" id="Seg_5068" s="T517">Kirgizova</ta>
            <ta e="T519" id="Seg_5069" s="T518">EMPH</ta>
            <ta e="T521" id="Seg_5070" s="T520">then</ta>
            <ta e="T522" id="Seg_5071" s="T521">one</ta>
            <ta e="T523" id="Seg_5072" s="T522">son-EP-1SG.[NOM]</ta>
            <ta e="T524" id="Seg_5073" s="T523">though</ta>
            <ta e="T525" id="Seg_5074" s="T524">who.[NOM]</ta>
            <ta e="T526" id="Seg_5075" s="T525">all.terrain.vehicle.driver.[NOM]</ta>
            <ta e="T527" id="Seg_5076" s="T526">stop-PST2-3SG</ta>
            <ta e="T528" id="Seg_5077" s="T527">learn-CVB.SEQ</ta>
            <ta e="T529" id="Seg_5078" s="T528">work-PTCP.PRS</ta>
            <ta e="T530" id="Seg_5079" s="T529">be-PST1-3SG</ta>
            <ta e="T531" id="Seg_5080" s="T530">twin-PL.[NOM]</ta>
            <ta e="T532" id="Seg_5081" s="T531">one.out.of.two-3SG.[NOM]</ta>
            <ta e="T533" id="Seg_5082" s="T532">electrician.[NOM]</ta>
            <ta e="T534" id="Seg_5083" s="T533">that</ta>
            <ta e="T535" id="Seg_5084" s="T534">work-CVB.SIM</ta>
            <ta e="T536" id="Seg_5085" s="T535">go-PRS.[3SG]</ta>
            <ta e="T537" id="Seg_5086" s="T536">now-DAT/LOC</ta>
            <ta e="T538" id="Seg_5087" s="T537">until</ta>
            <ta e="T539" id="Seg_5088" s="T538">learn-CVB.SEQ</ta>
            <ta e="T540" id="Seg_5089" s="T539">stop-CVB.SEQ</ta>
            <ta e="T541" id="Seg_5090" s="T540">after</ta>
            <ta e="T542" id="Seg_5091" s="T541">so</ta>
            <ta e="T543" id="Seg_5092" s="T542">EMPH</ta>
            <ta e="T544" id="Seg_5093" s="T543">every-3SG.[NOM]</ta>
            <ta e="T545" id="Seg_5094" s="T544">learn-PST2-3PL</ta>
            <ta e="T546" id="Seg_5095" s="T545">then</ta>
            <ta e="T547" id="Seg_5096" s="T546">well</ta>
            <ta e="T548" id="Seg_5097" s="T547">one</ta>
            <ta e="T549" id="Seg_5098" s="T548">child-1SG.[NOM]</ta>
            <ta e="T550" id="Seg_5099" s="T549">Kresty-DAT/LOC</ta>
            <ta e="T551" id="Seg_5100" s="T550">there.is</ta>
            <ta e="T553" id="Seg_5101" s="T552">eh</ta>
            <ta e="T554" id="Seg_5102" s="T553">police.[NOM]</ta>
            <ta e="T555" id="Seg_5103" s="T554">education-3SG-ACC</ta>
            <ta e="T556" id="Seg_5104" s="T555">stop-PTCP.PST</ta>
            <ta e="T557" id="Seg_5105" s="T556">be-PST1-3SG</ta>
            <ta e="T558" id="Seg_5106" s="T557">that-3SG-2SG.[NOM]</ta>
            <ta e="T559" id="Seg_5107" s="T558">place.[NOM]</ta>
            <ta e="T560" id="Seg_5108" s="T559">every</ta>
            <ta e="T561" id="Seg_5109" s="T560">place-ACC</ta>
            <ta e="T562" id="Seg_5110" s="T561">go-EP-CAUS-CVB.SEQ</ta>
            <ta e="T563" id="Seg_5111" s="T562">go-EP-CAUS-CVB.SEQ</ta>
            <ta e="T564" id="Seg_5112" s="T563">after</ta>
            <ta e="T565" id="Seg_5113" s="T564">Kresty-DAT/LOC</ta>
            <ta e="T566" id="Seg_5114" s="T565">woman-PROPR.[NOM]</ta>
            <ta e="T567" id="Seg_5115" s="T566">Alexey.[NOM]</ta>
            <ta e="T568" id="Seg_5116" s="T567">see-PST1-2SG</ta>
            <ta e="T569" id="Seg_5117" s="T568">EMPH</ta>
            <ta e="T570" id="Seg_5118" s="T569">apparently</ta>
            <ta e="T571" id="Seg_5119" s="T570">probably</ta>
            <ta e="T572" id="Seg_5120" s="T571">then</ta>
            <ta e="T573" id="Seg_5121" s="T572">other.of.two-3PL.[NOM]</ta>
            <ta e="T574" id="Seg_5122" s="T573">hunter-PL.[NOM]</ta>
            <ta e="T575" id="Seg_5123" s="T574">three</ta>
            <ta e="T576" id="Seg_5124" s="T575">boy-EP-1SG.[NOM]</ta>
            <ta e="T577" id="Seg_5125" s="T576">hunter-PL.[NOM]</ta>
            <ta e="T578" id="Seg_5126" s="T577">MOD</ta>
            <ta e="T579" id="Seg_5127" s="T578">Alyosha.[NOM]</ta>
            <ta e="T580" id="Seg_5128" s="T579">Sergey.[NOM]</ta>
            <ta e="T581" id="Seg_5129" s="T580">Misha.[NOM]</ta>
            <ta e="T582" id="Seg_5130" s="T581">Vitya.[NOM]</ta>
            <ta e="T583" id="Seg_5131" s="T582">Vitya.[NOM]</ta>
            <ta e="T584" id="Seg_5132" s="T583">sleep-CVB.SIM</ta>
            <ta e="T585" id="Seg_5133" s="T584">lie-PRS.[3SG]</ta>
            <ta e="T586" id="Seg_5134" s="T585">woman-3SG.[NOM]</ta>
            <ta e="T587" id="Seg_5135" s="T586">this</ta>
            <ta e="T588" id="Seg_5136" s="T587">stand-PRS.[3SG]</ta>
            <ta e="T589" id="Seg_5137" s="T588">last-3SG.[NOM]</ta>
            <ta e="T590" id="Seg_5138" s="T589">MOD</ta>
            <ta e="T591" id="Seg_5139" s="T590">then</ta>
            <ta e="T592" id="Seg_5140" s="T591">ten</ta>
            <ta e="T593" id="Seg_5141" s="T592">two</ta>
            <ta e="T594" id="Seg_5142" s="T593">child-PROPR-1SG</ta>
            <ta e="T595" id="Seg_5143" s="T594">twenty</ta>
            <ta e="T596" id="Seg_5144" s="T595">rest-3SG.[NOM]</ta>
            <ta e="T597" id="Seg_5145" s="T596">five</ta>
            <ta e="T598" id="Seg_5146" s="T597">eh</ta>
            <ta e="T599" id="Seg_5147" s="T598">seven</ta>
            <ta e="T600" id="Seg_5148" s="T599">grandchild-PROPR-1SG</ta>
            <ta e="T601" id="Seg_5149" s="T600">then</ta>
            <ta e="T602" id="Seg_5150" s="T601">village-1PL.[NOM]</ta>
            <ta e="T603" id="Seg_5151" s="T602">so</ta>
            <ta e="T604" id="Seg_5152" s="T603">four-ten-ABL</ta>
            <ta e="T605" id="Seg_5153" s="T604">over</ta>
            <ta e="T606" id="Seg_5154" s="T605">human.being.[NOM]</ta>
            <ta e="T607" id="Seg_5155" s="T606">daughter_in_law-PL-1SG-ACC</ta>
            <ta e="T608" id="Seg_5156" s="T607">whole-3SG-ACC</ta>
            <ta e="T609" id="Seg_5157" s="T608">find-TEMP-1SG</ta>
            <ta e="T610" id="Seg_5158" s="T609">many-3PL</ta>
            <ta e="T611" id="Seg_5159" s="T610">count-TEMP-1SG</ta>
            <ta e="T612" id="Seg_5160" s="T611">one</ta>
            <ta e="T613" id="Seg_5161" s="T612">daughter_in_law.[NOM]</ta>
            <ta e="T614" id="Seg_5162" s="T613">two</ta>
            <ta e="T615" id="Seg_5163" s="T614">daughter_in_law.[NOM]</ta>
            <ta e="T616" id="Seg_5164" s="T615">pedagog-PL.[NOM]</ta>
            <ta e="T617" id="Seg_5165" s="T616">child-PL-PROPR-3PL</ta>
            <ta e="T618" id="Seg_5166" s="T617">every-3PL.[NOM]</ta>
            <ta e="T619" id="Seg_5167" s="T618">daughter-3PL.[NOM]</ta>
            <ta e="T620" id="Seg_5168" s="T619">learn-CVB.SIM</ta>
            <ta e="T621" id="Seg_5169" s="T620">go-PST2-3PL</ta>
            <ta e="T622" id="Seg_5170" s="T621">one</ta>
            <ta e="T623" id="Seg_5171" s="T622">daughter_in_law-EP-1SG.[NOM]</ta>
            <ta e="T624" id="Seg_5172" s="T623">kindergarten-DAT/LOC</ta>
            <ta e="T625" id="Seg_5173" s="T624">who-3PL.[NOM]</ta>
            <ta e="T626" id="Seg_5174" s="T625">who.[NOM]</ta>
            <ta e="T627" id="Seg_5175" s="T626">be-PST1-3SG=Q</ta>
            <ta e="T628" id="Seg_5176" s="T627">well</ta>
            <ta e="T629" id="Seg_5177" s="T628">friend</ta>
            <ta e="T630" id="Seg_5178" s="T629">leading-PL.[NOM]</ta>
            <ta e="T631" id="Seg_5179" s="T630">two</ta>
            <ta e="T632" id="Seg_5180" s="T631">daughter_in_law-EP-1SG.[NOM]</ta>
            <ta e="T633" id="Seg_5181" s="T632">teacher-PL.[NOM]</ta>
            <ta e="T634" id="Seg_5182" s="T633">pedagog-PL.[NOM]</ta>
            <ta e="T635" id="Seg_5183" s="T634">every-3SG.[NOM]</ta>
            <ta e="T636" id="Seg_5184" s="T635">worker-PL.[NOM]</ta>
            <ta e="T637" id="Seg_5185" s="T636">then</ta>
            <ta e="T638" id="Seg_5186" s="T637">one</ta>
            <ta e="T639" id="Seg_5187" s="T638">son_in_law-EP-1SG.[NOM]</ta>
            <ta e="T640" id="Seg_5188" s="T639">that</ta>
            <ta e="T641" id="Seg_5189" s="T640">sky-VBZ-PST1-3SG</ta>
            <ta e="T642" id="Seg_5190" s="T641">one</ta>
            <ta e="T643" id="Seg_5191" s="T642">son_in_law-EP-1SG.[NOM]</ta>
            <ta e="T644" id="Seg_5192" s="T643">sick.[NOM]</ta>
            <ta e="T645" id="Seg_5193" s="T644">be-PST2.[3SG]</ta>
            <ta e="T646" id="Seg_5194" s="T645">child.[NOM]</ta>
            <ta e="T647" id="Seg_5195" s="T646">Domna</ta>
            <ta e="T648" id="Seg_5196" s="T647">Petrovna.[NOM]</ta>
            <ta e="T649" id="Seg_5197" s="T648">own-3SG.[NOM]</ta>
            <ta e="T650" id="Seg_5198" s="T649">that.[NOM]</ta>
            <ta e="T651" id="Seg_5199" s="T650">in.spite.of</ta>
            <ta e="T652" id="Seg_5200" s="T651">animal-ACC</ta>
            <ta e="T653" id="Seg_5201" s="T652">enough</ta>
            <ta e="T654" id="Seg_5202" s="T653">kill-PRS.[3SG]</ta>
            <ta e="T655" id="Seg_5203" s="T654">leg-POSS</ta>
            <ta e="T656" id="Seg_5204" s="T655">NEG</ta>
            <ta e="T657" id="Seg_5205" s="T656">self-3SG.[NOM]</ta>
            <ta e="T658" id="Seg_5206" s="T657">one</ta>
            <ta e="T659" id="Seg_5207" s="T658">son_in_law-EP-1SG.[NOM]</ta>
            <ta e="T660" id="Seg_5208" s="T659">Sopochnoe-DAT/LOC</ta>
            <ta e="T661" id="Seg_5209" s="T660">hunter.[NOM]</ta>
            <ta e="T662" id="Seg_5210" s="T661">daughter-3SG.[NOM]</ta>
            <ta e="T663" id="Seg_5211" s="T662">learn-CVB.SIM</ta>
            <ta e="T664" id="Seg_5212" s="T663">go-PST2.[3SG]</ta>
            <ta e="T665" id="Seg_5213" s="T664">well</ta>
            <ta e="T666" id="Seg_5214" s="T665">what-ACC</ta>
            <ta e="T667" id="Seg_5215" s="T666">tell-PTCP.FUT-1SG-ACC=Q</ta>
            <ta e="T671" id="Seg_5216" s="T670">well</ta>
         </annotation>
         <annotation name="gg" tierref="gg-KiPP">
            <ta e="T2" id="Seg_5217" s="T1">dieses-ABL</ta>
            <ta e="T3" id="Seg_5218" s="T2">hören-HAB.[3SG]</ta>
            <ta e="T4" id="Seg_5219" s="T3">Q</ta>
            <ta e="T9" id="Seg_5220" s="T6">AFFIRM</ta>
            <ta e="T15" id="Seg_5221" s="T14">doch</ta>
            <ta e="T17" id="Seg_5222" s="T15">was-ACC</ta>
            <ta e="T18" id="Seg_5223" s="T17">fragen-PRS.[3SG]</ta>
            <ta e="T19" id="Seg_5224" s="T18">nun</ta>
            <ta e="T20" id="Seg_5225" s="T19">nun</ta>
            <ta e="T21" id="Seg_5226" s="T20">zuerst</ta>
            <ta e="T22" id="Seg_5227" s="T21">beliebig-1SG-ACC</ta>
            <ta e="T24" id="Seg_5228" s="T22">fragen-FREQ-PRS.[3SG]</ta>
            <ta e="T25" id="Seg_5229" s="T24">MOD</ta>
            <ta e="T30" id="Seg_5230" s="T29">hören-PRS.[3SG]</ta>
            <ta e="T33" id="Seg_5231" s="T31">äh</ta>
            <ta e="T51" id="Seg_5232" s="T49">INTJ</ta>
            <ta e="T52" id="Seg_5233" s="T51">wann</ta>
            <ta e="T53" id="Seg_5234" s="T52">aufhören-FUT-1SG=Q</ta>
            <ta e="T54" id="Seg_5235" s="T53">jenes</ta>
            <ta e="T55" id="Seg_5236" s="T54">Abend-DAT/LOC</ta>
            <ta e="T56" id="Seg_5237" s="T55">EMPH</ta>
            <ta e="T57" id="Seg_5238" s="T56">bis.zu</ta>
            <ta e="T68" id="Seg_5239" s="T66">Korgo.[NOM]</ta>
            <ta e="T72" id="Seg_5240" s="T70">Korgo-DAT/LOC</ta>
            <ta e="T73" id="Seg_5241" s="T72">gebären-PST2-EP-1SG</ta>
            <ta e="T74" id="Seg_5242" s="T73">EMPH-hier</ta>
            <ta e="T75" id="Seg_5243" s="T74">selbst-1SG.[NOM]</ta>
            <ta e="T76" id="Seg_5244" s="T75">Erde-1SG-DAT/LOC</ta>
            <ta e="T77" id="Seg_5245" s="T76">unehelich-1SG</ta>
            <ta e="T78" id="Seg_5246" s="T77">wissen-PRS-2SG</ta>
            <ta e="T79" id="Seg_5247" s="T78">unehelich.[NOM]</ta>
            <ta e="T80" id="Seg_5248" s="T79">Name-3SG-ACC</ta>
            <ta e="T107" id="Seg_5249" s="T106">Vater-POSS</ta>
            <ta e="T108" id="Seg_5250" s="T107">NEG</ta>
            <ta e="T112" id="Seg_5251" s="T111">doch</ta>
            <ta e="T113" id="Seg_5252" s="T112">1SG.[NOM]</ta>
            <ta e="T115" id="Seg_5253" s="T113">Oma-1SG.[NOM]</ta>
            <ta e="T117" id="Seg_5254" s="T115">aufziehen-PST1-3SG</ta>
            <ta e="T121" id="Seg_5255" s="T120">Oma.[NOM]</ta>
            <ta e="T122" id="Seg_5256" s="T121">erziehen-VBZ-PST2-3SG</ta>
            <ta e="T123" id="Seg_5257" s="T122">Mama-1SG.[NOM]</ta>
            <ta e="T124" id="Seg_5258" s="T123">Mama-3SG.[NOM]</ta>
            <ta e="T129" id="Seg_5259" s="T128">dann</ta>
            <ta e="T130" id="Seg_5260" s="T129">1PL.[NOM]</ta>
            <ta e="T131" id="Seg_5261" s="T130">Familie-PL.[NOM]</ta>
            <ta e="T132" id="Seg_5262" s="T131">ganz</ta>
            <ta e="T133" id="Seg_5263" s="T132">sterben-CVB.SEQ-3PL</ta>
            <ta e="T134" id="Seg_5264" s="T133">Waise.[NOM]</ta>
            <ta e="T135" id="Seg_5265" s="T134">leben-EP-PST2-1PL</ta>
            <ta e="T136" id="Seg_5266" s="T135">Alte-ACC</ta>
            <ta e="T137" id="Seg_5267" s="T136">mit</ta>
            <ta e="T138" id="Seg_5268" s="T137">Oma-1SG-ACC</ta>
            <ta e="T139" id="Seg_5269" s="T138">mit</ta>
            <ta e="T140" id="Seg_5270" s="T139">dann</ta>
            <ta e="T142" id="Seg_5271" s="T141">Novorybnoe.[NOM]</ta>
            <ta e="T143" id="Seg_5272" s="T142">zu</ta>
            <ta e="T144" id="Seg_5273" s="T143">gehen-PST2-EP-1SG</ta>
            <ta e="T145" id="Seg_5274" s="T144">dann</ta>
            <ta e="T146" id="Seg_5275" s="T145">jenes</ta>
            <ta e="T147" id="Seg_5276" s="T146">gehen-PST2-1PL</ta>
            <ta e="T149" id="Seg_5277" s="T148">Großmutter-1SG-ACC</ta>
            <ta e="T150" id="Seg_5278" s="T149">Mutter-1SG-ACC</ta>
            <ta e="T151" id="Seg_5279" s="T150">mit</ta>
            <ta e="T152" id="Seg_5280" s="T151">Mutter.[NOM]</ta>
            <ta e="T153" id="Seg_5281" s="T152">sagen-HAB-1SG</ta>
            <ta e="T154" id="Seg_5282" s="T153">jenes-3SG-1SG-ACC</ta>
            <ta e="T155" id="Seg_5283" s="T154">Mama-1SG.[NOM]</ta>
            <ta e="T156" id="Seg_5284" s="T155">Mama-1SG.[NOM]</ta>
            <ta e="T157" id="Seg_5285" s="T156">doch</ta>
            <ta e="T158" id="Seg_5286" s="T157">eins</ta>
            <ta e="T159" id="Seg_5287" s="T158">doch</ta>
            <ta e="T160" id="Seg_5288" s="T159">dann</ta>
            <ta e="T161" id="Seg_5289" s="T160">zurück</ta>
            <ta e="T162" id="Seg_5290" s="T161">kommen-PST2-1PL</ta>
            <ta e="T163" id="Seg_5291" s="T162">Mama-1SG.[NOM]</ta>
            <ta e="T164" id="Seg_5292" s="T163">Alte.[NOM]</ta>
            <ta e="T166" id="Seg_5293" s="T165">Oma-1SG.[NOM]</ta>
            <ta e="T167" id="Seg_5294" s="T166">sterben-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T168" id="Seg_5295" s="T167">sterben-CVB.SEQ</ta>
            <ta e="T169" id="Seg_5296" s="T168">nachdem</ta>
            <ta e="T170" id="Seg_5297" s="T169">wieder.aufleben-EP-PST2-3SG</ta>
            <ta e="T171" id="Seg_5298" s="T170">jenes</ta>
            <ta e="T172" id="Seg_5299" s="T171">Ort-DAT/LOC</ta>
            <ta e="T173" id="Seg_5300" s="T172">dann</ta>
            <ta e="T174" id="Seg_5301" s="T173">zurück</ta>
            <ta e="T175" id="Seg_5302" s="T174">kommen-PST2-1PL</ta>
            <ta e="T176" id="Seg_5303" s="T175">jenes</ta>
            <ta e="T177" id="Seg_5304" s="T176">kommen-PTCP.PST-1PL.[NOM]</ta>
            <ta e="T178" id="Seg_5305" s="T177">nachdem</ta>
            <ta e="T179" id="Seg_5306" s="T178">zehn</ta>
            <ta e="T180" id="Seg_5307" s="T179">sechs</ta>
            <ta e="T181" id="Seg_5308" s="T180">Jahr-PROPR-1SG-DAT/LOC</ta>
            <ta e="T182" id="Seg_5309" s="T181">1SG-ACC</ta>
            <ta e="T183" id="Seg_5310" s="T182">Ehemann-DAT/LOC</ta>
            <ta e="T184" id="Seg_5311" s="T183">geben-PST2-3SG</ta>
            <ta e="T185" id="Seg_5312" s="T184">1SG.[NOM]</ta>
            <ta e="T186" id="Seg_5313" s="T185">sterben-TEMP-1SG</ta>
            <ta e="T187" id="Seg_5314" s="T186">2SG-ACC</ta>
            <ta e="T188" id="Seg_5315" s="T187">Sklave.[NOM]</ta>
            <ta e="T189" id="Seg_5316" s="T188">machen-FUT-3PL</ta>
            <ta e="T190" id="Seg_5317" s="T189">sagen-PST1-3SG</ta>
            <ta e="T191" id="Seg_5318" s="T190">Teufel</ta>
            <ta e="T192" id="Seg_5319" s="T191">was-ACC</ta>
            <ta e="T193" id="Seg_5320" s="T192">NEG</ta>
            <ta e="T194" id="Seg_5321" s="T193">sauber-1SG</ta>
            <ta e="T195" id="Seg_5322" s="T194">wenn.auch</ta>
            <ta e="T196" id="Seg_5323" s="T195">sich.waschen-CVB.SIM</ta>
            <ta e="T197" id="Seg_5324" s="T196">NEG</ta>
            <ta e="T198" id="Seg_5325" s="T197">noch.nicht-1SG</ta>
            <ta e="T199" id="Seg_5326" s="T198">Ehemann-DAT/LOC</ta>
            <ta e="T200" id="Seg_5327" s="T199">gehen-PST2-EP-1SG</ta>
            <ta e="T201" id="Seg_5328" s="T200">weinen-CVB.SIM-weinen-CVB.SIM</ta>
            <ta e="T202" id="Seg_5329" s="T201">Ehemann-DAT/LOC</ta>
            <ta e="T203" id="Seg_5330" s="T202">geben-PRS-3PL</ta>
            <ta e="T204" id="Seg_5331" s="T203">schlafen-CAUS-PRS-3PL</ta>
            <ta e="T205" id="Seg_5332" s="T204">weinen-CVB.SIM-weinen-CVB.SIM</ta>
            <ta e="T206" id="Seg_5333" s="T205">einschlafen-PRS-1SG</ta>
            <ta e="T207" id="Seg_5334" s="T206">Ehemann-1SG-ABL</ta>
            <ta e="T208" id="Seg_5335" s="T207">Angst.haben-PRS-1SG</ta>
            <ta e="T209" id="Seg_5336" s="T208">Kleidung-COM</ta>
            <ta e="T210" id="Seg_5337" s="T209">schlafen-PRS-1SG</ta>
            <ta e="T211" id="Seg_5338" s="T210">äh</ta>
            <ta e="T212" id="Seg_5339" s="T211">doch</ta>
            <ta e="T213" id="Seg_5340" s="T212">dann</ta>
            <ta e="T214" id="Seg_5341" s="T213">Jahr-ACC</ta>
            <ta e="T215" id="Seg_5342" s="T214">Jahr-ACC</ta>
            <ta e="T216" id="Seg_5343" s="T215">Kleidung-PROPR</ta>
            <ta e="T217" id="Seg_5344" s="T216">schlafen-PST2-EP-1SG</ta>
            <ta e="T218" id="Seg_5345" s="T217">dann</ta>
            <ta e="T219" id="Seg_5346" s="T218">doch</ta>
            <ta e="T220" id="Seg_5347" s="T219">später</ta>
            <ta e="T221" id="Seg_5348" s="T220">lernen-PRS-1SG</ta>
            <ta e="T222" id="Seg_5349" s="T221">EMPH</ta>
            <ta e="T223" id="Seg_5350" s="T222">Ehemann</ta>
            <ta e="T224" id="Seg_5351" s="T223">Ehemann</ta>
            <ta e="T225" id="Seg_5352" s="T224">Ehemann-1SG-DAT/LOC</ta>
            <ta e="T226" id="Seg_5353" s="T225">jenes</ta>
            <ta e="T227" id="Seg_5354" s="T226">Name-3SG.[NOM]</ta>
            <ta e="T233" id="Seg_5355" s="T232">zehn</ta>
            <ta e="T234" id="Seg_5356" s="T233">sieben</ta>
            <ta e="T235" id="Seg_5357" s="T234">zehn</ta>
            <ta e="T236" id="Seg_5358" s="T235">acht</ta>
            <ta e="T237" id="Seg_5359" s="T236">zehn</ta>
            <ta e="T238" id="Seg_5360" s="T237">acht</ta>
            <ta e="T239" id="Seg_5361" s="T238">Jahr-PROPR-1SG-DAT/LOC</ta>
            <ta e="T240" id="Seg_5362" s="T239">Kind-VBZ-PST2-EP-1SG</ta>
            <ta e="T241" id="Seg_5363" s="T240">erster</ta>
            <ta e="T242" id="Seg_5364" s="T241">Kind-1SG.[NOM]</ta>
            <ta e="T243" id="Seg_5365" s="T242">hier</ta>
            <ta e="T244" id="Seg_5366" s="T243">es.gibt</ta>
            <ta e="T245" id="Seg_5367" s="T244">dann</ta>
            <ta e="T246" id="Seg_5368" s="T245">Kind-VBZ-PST1-1SG</ta>
            <ta e="T247" id="Seg_5369" s="T246">Kind-VBZ-PST1-1SG</ta>
            <ta e="T248" id="Seg_5370" s="T247">Jahr.[NOM]</ta>
            <ta e="T249" id="Seg_5371" s="T248">jeder</ta>
            <ta e="T250" id="Seg_5372" s="T249">Kind-VBZ-PRS-1SG</ta>
            <ta e="T251" id="Seg_5373" s="T250">Jahr.[NOM]</ta>
            <ta e="T252" id="Seg_5374" s="T251">jeder</ta>
            <ta e="T254" id="Seg_5375" s="T252">Kind-VBZ-PRS-1SG</ta>
            <ta e="T259" id="Seg_5376" s="T258">AFFIRM</ta>
            <ta e="T260" id="Seg_5377" s="T259">dieses.EMPH</ta>
            <ta e="T261" id="Seg_5378" s="T260">Erde-DAT/LOC</ta>
            <ta e="T262" id="Seg_5379" s="T261">Tundra-DAT/LOC</ta>
            <ta e="T263" id="Seg_5380" s="T262">gehen-CVB.SEQ-1PL</ta>
            <ta e="T264" id="Seg_5381" s="T263">Tundra-DAT/LOC</ta>
            <ta e="T265" id="Seg_5382" s="T264">sein-PST1-1PL</ta>
            <ta e="T266" id="Seg_5383" s="T265">alter.Mann-EP-1SG.[NOM]</ta>
            <ta e="T267" id="Seg_5384" s="T266">Kassierer.[NOM]</ta>
            <ta e="T268" id="Seg_5385" s="T267">sein-PST1-3SG</ta>
            <ta e="T269" id="Seg_5386" s="T268">sehr</ta>
            <ta e="T270" id="Seg_5387" s="T269">gut</ta>
            <ta e="T271" id="Seg_5388" s="T270">Mensch.[NOM]</ta>
            <ta e="T272" id="Seg_5389" s="T271">sein-PST1-3SG</ta>
            <ta e="T273" id="Seg_5390" s="T272">fähig</ta>
            <ta e="T274" id="Seg_5391" s="T273">sehr</ta>
            <ta e="T275" id="Seg_5392" s="T274">Jäger.[NOM]</ta>
            <ta e="T276" id="Seg_5393" s="T275">Tier-ACC</ta>
            <ta e="T277" id="Seg_5394" s="T276">EMPH</ta>
            <ta e="T278" id="Seg_5395" s="T277">jagen-PST2-3SG</ta>
            <ta e="T279" id="Seg_5396" s="T278">Haus.[NOM]</ta>
            <ta e="T280" id="Seg_5397" s="T279">und</ta>
            <ta e="T281" id="Seg_5398" s="T280">bauen-PST2-3SG</ta>
            <ta e="T282" id="Seg_5399" s="T281">Kassierer-DAT/LOC</ta>
            <ta e="T283" id="Seg_5400" s="T282">zehn</ta>
            <ta e="T284" id="Seg_5401" s="T283">fünf</ta>
            <ta e="T285" id="Seg_5402" s="T284">Jahr-ACC</ta>
            <ta e="T286" id="Seg_5403" s="T285">sitzen-PST2-3SG</ta>
            <ta e="T287" id="Seg_5404" s="T286">dann</ta>
            <ta e="T288" id="Seg_5405" s="T287">eins</ta>
            <ta e="T289" id="Seg_5406" s="T288">Jahr-ACC</ta>
            <ta e="T290" id="Seg_5407" s="T289">Partorg-DAT/LOC</ta>
            <ta e="T291" id="Seg_5408" s="T290">leben-PST2-3SG</ta>
            <ta e="T292" id="Seg_5409" s="T291">dann</ta>
            <ta e="T293" id="Seg_5410" s="T292">Haus-PL-ACC</ta>
            <ta e="T294" id="Seg_5411" s="T293">bauen-PST2-3SG</ta>
            <ta e="T295" id="Seg_5412" s="T294">dieses</ta>
            <ta e="T296" id="Seg_5413" s="T295">stehen-PRS.[3SG]</ta>
            <ta e="T297" id="Seg_5414" s="T296">bauen-PTCP.PST</ta>
            <ta e="T298" id="Seg_5415" s="T297">Haus-PL-3SG.[NOM]</ta>
            <ta e="T299" id="Seg_5416" s="T298">dann</ta>
            <ta e="T300" id="Seg_5417" s="T299">zehn</ta>
            <ta e="T301" id="Seg_5418" s="T300">zwei</ta>
            <ta e="T302" id="Seg_5419" s="T301">Kind-PROPR-1PL</ta>
            <ta e="T303" id="Seg_5420" s="T302">dann</ta>
            <ta e="T304" id="Seg_5421" s="T303">ganz</ta>
            <ta e="T305" id="Seg_5422" s="T304">eins</ta>
            <ta e="T672" id="Seg_5423" s="T305">NEG</ta>
            <ta e="T306" id="Seg_5424" s="T672">Mensch-ACC</ta>
            <ta e="T307" id="Seg_5425" s="T306">Mensch-DAT/LOC</ta>
            <ta e="T308" id="Seg_5426" s="T307">geben-PST2.NEG-EP-1SG</ta>
            <ta e="T309" id="Seg_5427" s="T308">Kind-PL-1SG-ACC</ta>
            <ta e="T310" id="Seg_5428" s="T309">selbst-1SG.[NOM]</ta>
            <ta e="T311" id="Seg_5429" s="T310">aufziehen-PTCP.FUT.[NOM]</ta>
            <ta e="T312" id="Seg_5430" s="T311">sein-CVB.SEQ-1SG</ta>
            <ta e="T313" id="Seg_5431" s="T312">Mensch-DAT/LOC</ta>
            <ta e="T314" id="Seg_5432" s="T313">geben-NEG-1SG</ta>
            <ta e="T315" id="Seg_5433" s="T314">bemitleiden-PRS-1SG</ta>
            <ta e="T316" id="Seg_5434" s="T315">Kind-PL-1SG-ACC</ta>
            <ta e="T317" id="Seg_5435" s="T316">jenes-PL-1SG-ACC</ta>
            <ta e="T318" id="Seg_5436" s="T317">aufziehen-EP-MED-CVB.SIM</ta>
            <ta e="T319" id="Seg_5437" s="T318">aufziehen-EP-PTCP.PST-EP-INSTR</ta>
            <ta e="T320" id="Seg_5438" s="T319">Wiege-PROPR</ta>
            <ta e="T321" id="Seg_5439" s="T320">Kind-PROPR.[NOM]</ta>
            <ta e="T322" id="Seg_5440" s="T321">Netz-VBZ-EP-RECP/COLL-HAB-1SG</ta>
            <ta e="T323" id="Seg_5441" s="T322">alter.Mann-1SG-ACC</ta>
            <ta e="T324" id="Seg_5442" s="T323">mit</ta>
            <ta e="T325" id="Seg_5443" s="T324">krank.[NOM]</ta>
            <ta e="T326" id="Seg_5444" s="T325">werden-PTCP.PST-3SG-ACC</ta>
            <ta e="T327" id="Seg_5445" s="T326">nachdem</ta>
            <ta e="T328" id="Seg_5446" s="T327">doch</ta>
            <ta e="T329" id="Seg_5447" s="T328">Kind-PL-1PL.[NOM]</ta>
            <ta e="T330" id="Seg_5448" s="T329">wachsen-CVB.SEQ-3PL</ta>
            <ta e="T331" id="Seg_5449" s="T330">lernen-CVB.SIM</ta>
            <ta e="T332" id="Seg_5450" s="T331">gehen-FREQ-PST2-3PL</ta>
            <ta e="T333" id="Seg_5451" s="T332">dann</ta>
            <ta e="T334" id="Seg_5452" s="T333">später</ta>
            <ta e="T335" id="Seg_5453" s="T334">zwei</ta>
            <ta e="T336" id="Seg_5454" s="T335">groß</ta>
            <ta e="T337" id="Seg_5455" s="T336">Kind-1SG.[NOM]</ta>
            <ta e="T338" id="Seg_5456" s="T337">Rentier.[NOM]</ta>
            <ta e="T339" id="Seg_5457" s="T338">Ausbildung-3SG-DAT/LOC</ta>
            <ta e="T340" id="Seg_5458" s="T339">gehen-PST2-3PL</ta>
            <ta e="T341" id="Seg_5459" s="T340">Sohn-1PL.[NOM]</ta>
            <ta e="T342" id="Seg_5460" s="T341">Tochter-1PL.[NOM]</ta>
            <ta e="T343" id="Seg_5461" s="T342">dann</ta>
            <ta e="T344" id="Seg_5462" s="T343">eins</ta>
            <ta e="T345" id="Seg_5463" s="T344">Kind-1PL.[NOM]</ta>
            <ta e="T346" id="Seg_5464" s="T345">wachsen-CVB.SEQ</ta>
            <ta e="T347" id="Seg_5465" s="T346">Krasnojarsk-DAT/LOC</ta>
            <ta e="T348" id="Seg_5466" s="T347">lernen-CVB.SIM</ta>
            <ta e="T349" id="Seg_5467" s="T348">gehen-PST2-3SG</ta>
            <ta e="T350" id="Seg_5468" s="T349">jetzt</ta>
            <ta e="T351" id="Seg_5469" s="T350">Therapeut-DAT/LOC</ta>
            <ta e="T352" id="Seg_5470" s="T351">arbeiten-CVB.SIM</ta>
            <ta e="T353" id="Seg_5471" s="T352">liegen-PRS.[3SG]</ta>
            <ta e="T354" id="Seg_5472" s="T353">Dudinka-DAT/LOC</ta>
            <ta e="T355" id="Seg_5473" s="T354">dann</ta>
            <ta e="T356" id="Seg_5474" s="T355">eins</ta>
            <ta e="T357" id="Seg_5475" s="T356">eins</ta>
            <ta e="T358" id="Seg_5476" s="T357">Kind-1SG.[NOM]</ta>
            <ta e="T359" id="Seg_5477" s="T358">aber</ta>
            <ta e="T360" id="Seg_5478" s="T359">wieder</ta>
            <ta e="T361" id="Seg_5479" s="T360">lernen-CVB.SEQ</ta>
            <ta e="T362" id="Seg_5480" s="T361">zwei</ta>
            <ta e="T363" id="Seg_5481" s="T362">Institut-ACC</ta>
            <ta e="T364" id="Seg_5482" s="T363">aufhören-PST2-3SG</ta>
            <ta e="T365" id="Seg_5483" s="T364">dann</ta>
            <ta e="T366" id="Seg_5484" s="T365">Krasnojarsk-DAT/LOC</ta>
            <ta e="T367" id="Seg_5485" s="T366">es.gibt</ta>
            <ta e="T369" id="Seg_5486" s="T368">Sergej.[NOM]</ta>
            <ta e="T370" id="Seg_5487" s="T369">jenes-3SG-1SG.[NOM]</ta>
            <ta e="T371" id="Seg_5488" s="T370">Frau-PROPR</ta>
            <ta e="T372" id="Seg_5489" s="T371">Kind.[NOM]</ta>
            <ta e="T373" id="Seg_5490" s="T372">jenes</ta>
            <ta e="T374" id="Seg_5491" s="T373">zwei</ta>
            <ta e="T375" id="Seg_5492" s="T374">Sohn-PROPR.[NOM]</ta>
            <ta e="T376" id="Seg_5493" s="T375">Kasache-PL-ABL</ta>
            <ta e="T377" id="Seg_5494" s="T376">Frau-VBZ-PST2.[3SG]</ta>
            <ta e="T378" id="Seg_5495" s="T377">Speicher-PROPR-ABL</ta>
            <ta e="T379" id="Seg_5496" s="T378">jetzt</ta>
            <ta e="T380" id="Seg_5497" s="T379">aber</ta>
            <ta e="T381" id="Seg_5498" s="T380">Wache-DAT/LOC</ta>
            <ta e="T382" id="Seg_5499" s="T381">Wache-DAT/LOC</ta>
            <ta e="T383" id="Seg_5500" s="T382">arbeiten-PRS.[3SG]</ta>
            <ta e="T384" id="Seg_5501" s="T383">Polizei-ACC</ta>
            <ta e="T385" id="Seg_5502" s="T384">aufhören-CVB.SEQ</ta>
            <ta e="T386" id="Seg_5503" s="T385">nachdem</ta>
            <ta e="T387" id="Seg_5504" s="T386">früher</ta>
            <ta e="T388" id="Seg_5505" s="T387">Rentier.[NOM]</ta>
            <ta e="T389" id="Seg_5506" s="T388">Arbeiter-3SG.[NOM]</ta>
            <ta e="T390" id="Seg_5507" s="T389">sein-PST2-3SG</ta>
            <ta e="T391" id="Seg_5508" s="T390">dann</ta>
            <ta e="T392" id="Seg_5509" s="T391">eins</ta>
            <ta e="T393" id="Seg_5510" s="T392">Kind-1SG.[NOM]</ta>
            <ta e="T394" id="Seg_5511" s="T393">Koch-DAT/LOC</ta>
            <ta e="T395" id="Seg_5512" s="T394">Chatanga-DAT/LOC</ta>
            <ta e="T396" id="Seg_5513" s="T395">arbeiten-PRS.[3SG]</ta>
            <ta e="T397" id="Seg_5514" s="T396">wichtig</ta>
            <ta e="T398" id="Seg_5515" s="T397">Koch.[NOM]</ta>
            <ta e="T399" id="Seg_5516" s="T398">eins</ta>
            <ta e="T400" id="Seg_5517" s="T399">Tochter-EP-1SG.[NOM]</ta>
            <ta e="T401" id="Seg_5518" s="T400">eins</ta>
            <ta e="T403" id="Seg_5519" s="T402">was.[NOM]</ta>
            <ta e="T405" id="Seg_5520" s="T404">Chatanga-DAT/LOC</ta>
            <ta e="T406" id="Seg_5521" s="T405">Marina</ta>
            <ta e="T410" id="Seg_5522" s="T409">nein</ta>
            <ta e="T411" id="Seg_5523" s="T410">lernen-PST2-3SG</ta>
            <ta e="T412" id="Seg_5524" s="T411">AFFIRM</ta>
            <ta e="T413" id="Seg_5525" s="T412">jenes.[NOM]</ta>
            <ta e="T414" id="Seg_5526" s="T413">zu</ta>
            <ta e="T415" id="Seg_5527" s="T414">Krasnojarsk-DAT/LOC</ta>
            <ta e="T416" id="Seg_5528" s="T415">lernen-PST2-3SG</ta>
            <ta e="T417" id="Seg_5529" s="T416">dann</ta>
            <ta e="T419" id="Seg_5530" s="T418">Doktor.[NOM]</ta>
            <ta e="T420" id="Seg_5531" s="T419">Tochter-EP-1SG.[NOM]</ta>
            <ta e="T421" id="Seg_5532" s="T420">Dudinka-DAT/LOC</ta>
            <ta e="T422" id="Seg_5533" s="T421">arbeiten-PRS.[3SG]</ta>
            <ta e="T423" id="Seg_5534" s="T422">Therapeut-DAT/LOC</ta>
            <ta e="T424" id="Seg_5535" s="T423">arbeiten-PRS.[3SG]</ta>
            <ta e="T425" id="Seg_5536" s="T424">jetzt</ta>
            <ta e="T426" id="Seg_5537" s="T425">drei</ta>
            <ta e="T427" id="Seg_5538" s="T426">Kind-PROPR.[NOM]</ta>
            <ta e="T428" id="Seg_5539" s="T427">alt</ta>
            <ta e="T429" id="Seg_5540" s="T428">Frau.[NOM]</ta>
            <ta e="T430" id="Seg_5541" s="T429">Rente-PROPR</ta>
            <ta e="T431" id="Seg_5542" s="T430">Kind-1SG.[NOM]</ta>
            <ta e="T432" id="Seg_5543" s="T431">jeder-3SG.[NOM]</ta>
            <ta e="T433" id="Seg_5544" s="T432">Rente-PROPR.[NOM]</ta>
            <ta e="T434" id="Seg_5545" s="T433">drei</ta>
            <ta e="T435" id="Seg_5546" s="T434">Kind-1SG.[NOM]</ta>
            <ta e="T436" id="Seg_5547" s="T435">Rente-PROPR-3PL</ta>
            <ta e="T437" id="Seg_5548" s="T436">groß-PL-EP-1SG.[NOM]</ta>
            <ta e="T438" id="Seg_5549" s="T437">jetzt</ta>
            <ta e="T439" id="Seg_5550" s="T438">einer.von.zwei-3SG.[NOM]</ta>
            <ta e="T440" id="Seg_5551" s="T439">aber</ta>
            <ta e="T441" id="Seg_5552" s="T440">Fischer-VBZ-PRS.[3SG]</ta>
            <ta e="T442" id="Seg_5553" s="T441">einer.von.zwei-3SG.[NOM]</ta>
            <ta e="T443" id="Seg_5554" s="T442">MOD</ta>
            <ta e="T444" id="Seg_5555" s="T443">jetzt</ta>
            <ta e="T445" id="Seg_5556" s="T444">alt.sein-PST2.[3SG]</ta>
            <ta e="T446" id="Seg_5557" s="T445">Rente-PROPR</ta>
            <ta e="T447" id="Seg_5558" s="T446">Vera.[NOM]</ta>
            <ta e="T448" id="Seg_5559" s="T447">Ehemann-3SG.[NOM]</ta>
            <ta e="T449" id="Seg_5560" s="T448">dieses.EMPH-INTNS</ta>
            <ta e="T450" id="Seg_5561" s="T449">sterben-PST2-3SG</ta>
            <ta e="T451" id="Seg_5562" s="T450">zwei</ta>
            <ta e="T452" id="Seg_5563" s="T451">Kind-PROPR.[NOM]</ta>
            <ta e="T453" id="Seg_5564" s="T452">drei-ORD-3PL.[NOM]</ta>
            <ta e="T454" id="Seg_5565" s="T453">sterben-PST2-3SG</ta>
            <ta e="T455" id="Seg_5566" s="T454">groß</ta>
            <ta e="T456" id="Seg_5567" s="T455">Sohn-EP-1SG.[NOM]</ta>
            <ta e="T457" id="Seg_5568" s="T456">drei</ta>
            <ta e="T458" id="Seg_5569" s="T457">Kind-PROPR.[NOM]</ta>
            <ta e="T459" id="Seg_5570" s="T458">Kind.[NOM]</ta>
            <ta e="T460" id="Seg_5571" s="T459">eins</ta>
            <ta e="T461" id="Seg_5572" s="T460">Kind-3SG.[NOM]</ta>
            <ta e="T462" id="Seg_5573" s="T461">Frau-PROPR</ta>
            <ta e="T463" id="Seg_5574" s="T462">Kind-PROPR.[NOM]</ta>
            <ta e="T464" id="Seg_5575" s="T463">anderer.von.zwei-3PL.[NOM]</ta>
            <ta e="T465" id="Seg_5576" s="T464">Kind-VBZ-CVB.SIM</ta>
            <ta e="T467" id="Seg_5577" s="T465">noch.nicht-3PL</ta>
            <ta e="T471" id="Seg_5578" s="T469">AFFIRM</ta>
            <ta e="T472" id="Seg_5579" s="T471">dann</ta>
            <ta e="T473" id="Seg_5580" s="T472">eins</ta>
            <ta e="T474" id="Seg_5581" s="T473">Kind-1SG.[NOM]</ta>
            <ta e="T475" id="Seg_5582" s="T474">nun</ta>
            <ta e="T476" id="Seg_5583" s="T475">wer-DAT/LOC</ta>
            <ta e="T477" id="Seg_5584" s="T476">es.gibt</ta>
            <ta e="T478" id="Seg_5585" s="T477">Sopotschnoje-DAT/LOC</ta>
            <ta e="T479" id="Seg_5586" s="T478">es.gibt</ta>
            <ta e="T480" id="Seg_5587" s="T479">wer-DAT/LOC</ta>
            <ta e="T481" id="Seg_5588" s="T480">Erzieher-DAT/LOC</ta>
            <ta e="T482" id="Seg_5589" s="T481">arbeiten-PRS.[3SG]</ta>
            <ta e="T483" id="Seg_5590" s="T482">dann</ta>
            <ta e="T484" id="Seg_5591" s="T483">eins</ta>
            <ta e="T485" id="Seg_5592" s="T484">Kind-1SG.[NOM]</ta>
            <ta e="T486" id="Seg_5593" s="T485">hier</ta>
            <ta e="T487" id="Seg_5594" s="T486">eins</ta>
            <ta e="T488" id="Seg_5595" s="T487">Tochter-EP-1SG.[NOM]</ta>
            <ta e="T489" id="Seg_5596" s="T488">hier</ta>
            <ta e="T490" id="Seg_5597" s="T489">Erzieher-VBZ-PRS.[3SG]</ta>
            <ta e="T491" id="Seg_5598" s="T490">eins</ta>
            <ta e="T492" id="Seg_5599" s="T491">Kind-1SG.[NOM]</ta>
            <ta e="T494" id="Seg_5600" s="T492">MOD</ta>
            <ta e="T497" id="Seg_5601" s="T496">AFFIRM</ta>
            <ta e="T498" id="Seg_5602" s="T497">Schule-DAT/LOC</ta>
            <ta e="T499" id="Seg_5603" s="T498">hier</ta>
            <ta e="T500" id="Seg_5604" s="T499">Domna</ta>
            <ta e="T501" id="Seg_5605" s="T500">Petrovna.[NOM]</ta>
            <ta e="T508" id="Seg_5606" s="T506">äh</ta>
            <ta e="T509" id="Seg_5607" s="T508">dann</ta>
            <ta e="T510" id="Seg_5608" s="T509">eins</ta>
            <ta e="T511" id="Seg_5609" s="T510">klein</ta>
            <ta e="T512" id="Seg_5610" s="T511">Sohn-EP-1SG.[NOM]</ta>
            <ta e="T513" id="Seg_5611" s="T512">Frau-3SG.[NOM]</ta>
            <ta e="T514" id="Seg_5612" s="T513">auch</ta>
            <ta e="T515" id="Seg_5613" s="T514">hier</ta>
            <ta e="T516" id="Seg_5614" s="T515">Lehrer-VBZ-PRS.[3SG]</ta>
            <ta e="T517" id="Seg_5615" s="T516">Nadja</ta>
            <ta e="T518" id="Seg_5616" s="T517">Kirgizova</ta>
            <ta e="T519" id="Seg_5617" s="T518">EMPH</ta>
            <ta e="T521" id="Seg_5618" s="T520">dann</ta>
            <ta e="T522" id="Seg_5619" s="T521">eins</ta>
            <ta e="T523" id="Seg_5620" s="T522">Sohn-EP-1SG.[NOM]</ta>
            <ta e="T524" id="Seg_5621" s="T523">aber</ta>
            <ta e="T525" id="Seg_5622" s="T524">wer.[NOM]</ta>
            <ta e="T526" id="Seg_5623" s="T525">Geländewagenfahrer.[NOM]</ta>
            <ta e="T527" id="Seg_5624" s="T526">aufhören-PST2-3SG</ta>
            <ta e="T528" id="Seg_5625" s="T527">lernen-CVB.SEQ</ta>
            <ta e="T529" id="Seg_5626" s="T528">arbeiten-PTCP.PRS</ta>
            <ta e="T530" id="Seg_5627" s="T529">sein-PST1-3SG</ta>
            <ta e="T531" id="Seg_5628" s="T530">Zwilling-PL.[NOM]</ta>
            <ta e="T532" id="Seg_5629" s="T531">einer.von.zwei-3SG.[NOM]</ta>
            <ta e="T533" id="Seg_5630" s="T532">Elektriker.[NOM]</ta>
            <ta e="T534" id="Seg_5631" s="T533">dieses</ta>
            <ta e="T535" id="Seg_5632" s="T534">arbeiten-CVB.SIM</ta>
            <ta e="T536" id="Seg_5633" s="T535">gehen-PRS.[3SG]</ta>
            <ta e="T537" id="Seg_5634" s="T536">jetzt-DAT/LOC</ta>
            <ta e="T538" id="Seg_5635" s="T537">bis.zu</ta>
            <ta e="T539" id="Seg_5636" s="T538">lernen-CVB.SEQ</ta>
            <ta e="T540" id="Seg_5637" s="T539">aufhören-CVB.SEQ</ta>
            <ta e="T541" id="Seg_5638" s="T540">nachdem</ta>
            <ta e="T542" id="Seg_5639" s="T541">so</ta>
            <ta e="T543" id="Seg_5640" s="T542">EMPH</ta>
            <ta e="T544" id="Seg_5641" s="T543">jeder-3SG.[NOM]</ta>
            <ta e="T545" id="Seg_5642" s="T544">lernen-PST2-3PL</ta>
            <ta e="T546" id="Seg_5643" s="T545">dann</ta>
            <ta e="T547" id="Seg_5644" s="T546">nun</ta>
            <ta e="T548" id="Seg_5645" s="T547">eins</ta>
            <ta e="T549" id="Seg_5646" s="T548">Kind-1SG.[NOM]</ta>
            <ta e="T550" id="Seg_5647" s="T549">Kresty-DAT/LOC</ta>
            <ta e="T551" id="Seg_5648" s="T550">es.gibt</ta>
            <ta e="T553" id="Seg_5649" s="T552">äh</ta>
            <ta e="T554" id="Seg_5650" s="T553">Polizei.[NOM]</ta>
            <ta e="T555" id="Seg_5651" s="T554">Ausbildung-3SG-ACC</ta>
            <ta e="T556" id="Seg_5652" s="T555">aufhören-PTCP.PST</ta>
            <ta e="T557" id="Seg_5653" s="T556">sein-PST1-3SG</ta>
            <ta e="T558" id="Seg_5654" s="T557">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T559" id="Seg_5655" s="T558">Ort.[NOM]</ta>
            <ta e="T560" id="Seg_5656" s="T559">jeder</ta>
            <ta e="T561" id="Seg_5657" s="T560">Ort-ACC</ta>
            <ta e="T562" id="Seg_5658" s="T561">gehen-EP-CAUS-CVB.SEQ</ta>
            <ta e="T563" id="Seg_5659" s="T562">gehen-EP-CAUS-CVB.SEQ</ta>
            <ta e="T564" id="Seg_5660" s="T563">nachdem</ta>
            <ta e="T565" id="Seg_5661" s="T564">Kresty-DAT/LOC</ta>
            <ta e="T566" id="Seg_5662" s="T565">Frau-PROPR.[NOM]</ta>
            <ta e="T567" id="Seg_5663" s="T566">Alexej.[NOM]</ta>
            <ta e="T568" id="Seg_5664" s="T567">sehen-PST1-2SG</ta>
            <ta e="T569" id="Seg_5665" s="T568">EMPH</ta>
            <ta e="T570" id="Seg_5666" s="T569">offenbar</ta>
            <ta e="T571" id="Seg_5667" s="T570">wahrscheinlich</ta>
            <ta e="T572" id="Seg_5668" s="T571">dann</ta>
            <ta e="T573" id="Seg_5669" s="T572">anderer.von.zwei-3PL.[NOM]</ta>
            <ta e="T574" id="Seg_5670" s="T573">Jäger-PL.[NOM]</ta>
            <ta e="T575" id="Seg_5671" s="T574">drei</ta>
            <ta e="T576" id="Seg_5672" s="T575">Junge-EP-1SG.[NOM]</ta>
            <ta e="T577" id="Seg_5673" s="T576">Jäger-PL.[NOM]</ta>
            <ta e="T578" id="Seg_5674" s="T577">MOD</ta>
            <ta e="T579" id="Seg_5675" s="T578">Aljoscha.[NOM]</ta>
            <ta e="T580" id="Seg_5676" s="T579">Sergej.[NOM]</ta>
            <ta e="T581" id="Seg_5677" s="T580">Mischa.[NOM]</ta>
            <ta e="T582" id="Seg_5678" s="T581">Vitja.[NOM]</ta>
            <ta e="T583" id="Seg_5679" s="T582">Vitja.[NOM]</ta>
            <ta e="T584" id="Seg_5680" s="T583">schlafen-CVB.SIM</ta>
            <ta e="T585" id="Seg_5681" s="T584">liegen-PRS.[3SG]</ta>
            <ta e="T586" id="Seg_5682" s="T585">Frau-3SG.[NOM]</ta>
            <ta e="T587" id="Seg_5683" s="T586">dieses</ta>
            <ta e="T588" id="Seg_5684" s="T587">stehen-PRS.[3SG]</ta>
            <ta e="T589" id="Seg_5685" s="T588">letzter-3SG.[NOM]</ta>
            <ta e="T590" id="Seg_5686" s="T589">MOD</ta>
            <ta e="T591" id="Seg_5687" s="T590">dann</ta>
            <ta e="T592" id="Seg_5688" s="T591">zehn</ta>
            <ta e="T593" id="Seg_5689" s="T592">zwei</ta>
            <ta e="T594" id="Seg_5690" s="T593">Kind-PROPR-1SG</ta>
            <ta e="T595" id="Seg_5691" s="T594">zwanzig</ta>
            <ta e="T596" id="Seg_5692" s="T595">Rest-3SG.[NOM]</ta>
            <ta e="T597" id="Seg_5693" s="T596">fünf</ta>
            <ta e="T598" id="Seg_5694" s="T597">äh</ta>
            <ta e="T599" id="Seg_5695" s="T598">sieben</ta>
            <ta e="T600" id="Seg_5696" s="T599">Enkel-PROPR-1SG</ta>
            <ta e="T601" id="Seg_5697" s="T600">dann</ta>
            <ta e="T602" id="Seg_5698" s="T601">Dorf-1PL.[NOM]</ta>
            <ta e="T603" id="Seg_5699" s="T602">so</ta>
            <ta e="T604" id="Seg_5700" s="T603">vier-zehn-ABL</ta>
            <ta e="T605" id="Seg_5701" s="T604">über</ta>
            <ta e="T606" id="Seg_5702" s="T605">Mensch.[NOM]</ta>
            <ta e="T607" id="Seg_5703" s="T606">Schwiegertochter-PL-1SG-ACC</ta>
            <ta e="T608" id="Seg_5704" s="T607">ganz-3SG-ACC</ta>
            <ta e="T609" id="Seg_5705" s="T608">finden-TEMP-1SG</ta>
            <ta e="T610" id="Seg_5706" s="T609">viel-3PL</ta>
            <ta e="T611" id="Seg_5707" s="T610">zählen-TEMP-1SG</ta>
            <ta e="T612" id="Seg_5708" s="T611">eins</ta>
            <ta e="T613" id="Seg_5709" s="T612">Schwiegertochter.[NOM]</ta>
            <ta e="T614" id="Seg_5710" s="T613">zwei</ta>
            <ta e="T615" id="Seg_5711" s="T614">Schwiegertochter.[NOM]</ta>
            <ta e="T616" id="Seg_5712" s="T615">Pädagoge-PL.[NOM]</ta>
            <ta e="T617" id="Seg_5713" s="T616">Kind-PL-PROPR-3PL</ta>
            <ta e="T618" id="Seg_5714" s="T617">jeder-3PL.[NOM]</ta>
            <ta e="T619" id="Seg_5715" s="T618">Tochter-3PL.[NOM]</ta>
            <ta e="T620" id="Seg_5716" s="T619">lernen-CVB.SIM</ta>
            <ta e="T621" id="Seg_5717" s="T620">gehen-PST2-3PL</ta>
            <ta e="T622" id="Seg_5718" s="T621">eins</ta>
            <ta e="T623" id="Seg_5719" s="T622">Schwiegertochter-EP-1SG.[NOM]</ta>
            <ta e="T624" id="Seg_5720" s="T623">Kindergarten-DAT/LOC</ta>
            <ta e="T625" id="Seg_5721" s="T624">wer-3PL.[NOM]</ta>
            <ta e="T626" id="Seg_5722" s="T625">wer.[NOM]</ta>
            <ta e="T627" id="Seg_5723" s="T626">sein-PST1-3SG=Q</ta>
            <ta e="T628" id="Seg_5724" s="T627">nun</ta>
            <ta e="T629" id="Seg_5725" s="T628">Freund</ta>
            <ta e="T630" id="Seg_5726" s="T629">leitend-PL.[NOM]</ta>
            <ta e="T631" id="Seg_5727" s="T630">zwei</ta>
            <ta e="T632" id="Seg_5728" s="T631">Schwiegertochter-EP-1SG.[NOM]</ta>
            <ta e="T633" id="Seg_5729" s="T632">Lehrer-PL.[NOM]</ta>
            <ta e="T634" id="Seg_5730" s="T633">Pädagoge-PL.[NOM]</ta>
            <ta e="T635" id="Seg_5731" s="T634">jeder-3SG.[NOM]</ta>
            <ta e="T636" id="Seg_5732" s="T635">Arbeiter-PL.[NOM]</ta>
            <ta e="T637" id="Seg_5733" s="T636">dann</ta>
            <ta e="T638" id="Seg_5734" s="T637">eins</ta>
            <ta e="T639" id="Seg_5735" s="T638">Schwiegersohn-EP-1SG.[NOM]</ta>
            <ta e="T640" id="Seg_5736" s="T639">dieses</ta>
            <ta e="T641" id="Seg_5737" s="T640">Himmel-VBZ-PST1-3SG</ta>
            <ta e="T642" id="Seg_5738" s="T641">eins</ta>
            <ta e="T643" id="Seg_5739" s="T642">Schwiegersohn-EP-1SG.[NOM]</ta>
            <ta e="T644" id="Seg_5740" s="T643">krank.[NOM]</ta>
            <ta e="T645" id="Seg_5741" s="T644">sein-PST2.[3SG]</ta>
            <ta e="T646" id="Seg_5742" s="T645">Kind.[NOM]</ta>
            <ta e="T647" id="Seg_5743" s="T646">Domna</ta>
            <ta e="T648" id="Seg_5744" s="T647">Petrovna.[NOM]</ta>
            <ta e="T649" id="Seg_5745" s="T648">eigen-3SG.[NOM]</ta>
            <ta e="T650" id="Seg_5746" s="T649">jenes.[NOM]</ta>
            <ta e="T651" id="Seg_5747" s="T650">trotz</ta>
            <ta e="T652" id="Seg_5748" s="T651">Tier-ACC</ta>
            <ta e="T653" id="Seg_5749" s="T652">genug</ta>
            <ta e="T654" id="Seg_5750" s="T653">töten-PRS.[3SG]</ta>
            <ta e="T655" id="Seg_5751" s="T654">Bein-POSS</ta>
            <ta e="T656" id="Seg_5752" s="T655">NEG</ta>
            <ta e="T657" id="Seg_5753" s="T656">selbst-3SG.[NOM]</ta>
            <ta e="T658" id="Seg_5754" s="T657">eins</ta>
            <ta e="T659" id="Seg_5755" s="T658">Schwiegersohn-EP-1SG.[NOM]</ta>
            <ta e="T660" id="Seg_5756" s="T659">Sopotschnoje-DAT/LOC</ta>
            <ta e="T661" id="Seg_5757" s="T660">Jäger.[NOM]</ta>
            <ta e="T662" id="Seg_5758" s="T661">Tochter-3SG.[NOM]</ta>
            <ta e="T663" id="Seg_5759" s="T662">lernen-CVB.SIM</ta>
            <ta e="T664" id="Seg_5760" s="T663">gehen-PST2.[3SG]</ta>
            <ta e="T665" id="Seg_5761" s="T664">doch</ta>
            <ta e="T666" id="Seg_5762" s="T665">was-ACC</ta>
            <ta e="T667" id="Seg_5763" s="T666">erzählen-PTCP.FUT-1SG-ACC=Q</ta>
            <ta e="T671" id="Seg_5764" s="T670">doch</ta>
         </annotation>
         <annotation name="gr" tierref="gr-KiPP">
            <ta e="T2" id="Seg_5765" s="T1">этот-ABL</ta>
            <ta e="T3" id="Seg_5766" s="T2">слышать-HAB.[3SG]</ta>
            <ta e="T4" id="Seg_5767" s="T3">Q</ta>
            <ta e="T9" id="Seg_5768" s="T6">AFFIRM</ta>
            <ta e="T15" id="Seg_5769" s="T14">вот</ta>
            <ta e="T17" id="Seg_5770" s="T15">что-ACC</ta>
            <ta e="T18" id="Seg_5771" s="T17">спрашивать-PRS.[3SG]</ta>
            <ta e="T19" id="Seg_5772" s="T18">вот</ta>
            <ta e="T20" id="Seg_5773" s="T19">ну</ta>
            <ta e="T21" id="Seg_5774" s="T20">сначала</ta>
            <ta e="T22" id="Seg_5775" s="T21">любой-1SG-ACC</ta>
            <ta e="T24" id="Seg_5776" s="T22">спрашивать-FREQ-PRS.[3SG]</ta>
            <ta e="T25" id="Seg_5777" s="T24">MOD</ta>
            <ta e="T30" id="Seg_5778" s="T29">слышать-PRS.[3SG]</ta>
            <ta e="T33" id="Seg_5779" s="T31">ээ</ta>
            <ta e="T51" id="Seg_5780" s="T49">INTJ</ta>
            <ta e="T52" id="Seg_5781" s="T51">когда</ta>
            <ta e="T53" id="Seg_5782" s="T52">кончать-FUT-1SG=Q</ta>
            <ta e="T54" id="Seg_5783" s="T53">тот</ta>
            <ta e="T55" id="Seg_5784" s="T54">вечер-DAT/LOC</ta>
            <ta e="T56" id="Seg_5785" s="T55">EMPH</ta>
            <ta e="T57" id="Seg_5786" s="T56">пока</ta>
            <ta e="T68" id="Seg_5787" s="T66">Корго.[NOM]</ta>
            <ta e="T72" id="Seg_5788" s="T70">Корго-DAT/LOC</ta>
            <ta e="T73" id="Seg_5789" s="T72">родить-PST2-EP-1SG</ta>
            <ta e="T74" id="Seg_5790" s="T73">EMPH-здесь</ta>
            <ta e="T75" id="Seg_5791" s="T74">сам-1SG.[NOM]</ta>
            <ta e="T76" id="Seg_5792" s="T75">земля-1SG-DAT/LOC</ta>
            <ta e="T77" id="Seg_5793" s="T76">незаконнорожденный-1SG</ta>
            <ta e="T78" id="Seg_5794" s="T77">знать-PRS-2SG</ta>
            <ta e="T79" id="Seg_5795" s="T78">незаконнорожденный.[NOM]</ta>
            <ta e="T80" id="Seg_5796" s="T79">имя-3SG-ACC</ta>
            <ta e="T107" id="Seg_5797" s="T106">отец-POSS</ta>
            <ta e="T108" id="Seg_5798" s="T107">NEG</ta>
            <ta e="T112" id="Seg_5799" s="T111">вот</ta>
            <ta e="T113" id="Seg_5800" s="T112">1SG.[NOM]</ta>
            <ta e="T115" id="Seg_5801" s="T113">бабушка-1SG.[NOM]</ta>
            <ta e="T117" id="Seg_5802" s="T115">воспитывать-PST1-3SG</ta>
            <ta e="T121" id="Seg_5803" s="T120">бабушка.[NOM]</ta>
            <ta e="T122" id="Seg_5804" s="T121">воспитывать-VBZ-PST2-3SG</ta>
            <ta e="T123" id="Seg_5805" s="T122">мама-1SG.[NOM]</ta>
            <ta e="T124" id="Seg_5806" s="T123">мама-3SG.[NOM]</ta>
            <ta e="T129" id="Seg_5807" s="T128">потом</ta>
            <ta e="T130" id="Seg_5808" s="T129">1PL.[NOM]</ta>
            <ta e="T131" id="Seg_5809" s="T130">семья-PL.[NOM]</ta>
            <ta e="T132" id="Seg_5810" s="T131">полностью</ta>
            <ta e="T133" id="Seg_5811" s="T132">умирать-CVB.SEQ-3PL</ta>
            <ta e="T134" id="Seg_5812" s="T133">сирота.[NOM]</ta>
            <ta e="T135" id="Seg_5813" s="T134">жить-EP-PST2-1PL</ta>
            <ta e="T136" id="Seg_5814" s="T135">старуха-ACC</ta>
            <ta e="T137" id="Seg_5815" s="T136">с</ta>
            <ta e="T138" id="Seg_5816" s="T137">бабушка-1SG-ACC</ta>
            <ta e="T139" id="Seg_5817" s="T138">с</ta>
            <ta e="T140" id="Seg_5818" s="T139">потом</ta>
            <ta e="T142" id="Seg_5819" s="T141">Новорыбное.[NOM]</ta>
            <ta e="T143" id="Seg_5820" s="T142">к</ta>
            <ta e="T144" id="Seg_5821" s="T143">идти-PST2-EP-1SG</ta>
            <ta e="T145" id="Seg_5822" s="T144">потом</ta>
            <ta e="T146" id="Seg_5823" s="T145">тот</ta>
            <ta e="T147" id="Seg_5824" s="T146">идти-PST2-1PL</ta>
            <ta e="T149" id="Seg_5825" s="T148">бабушка-1SG-ACC</ta>
            <ta e="T150" id="Seg_5826" s="T149">мать-1SG-ACC</ta>
            <ta e="T151" id="Seg_5827" s="T150">с</ta>
            <ta e="T152" id="Seg_5828" s="T151">мать.[NOM]</ta>
            <ta e="T153" id="Seg_5829" s="T152">говорить-HAB-1SG</ta>
            <ta e="T154" id="Seg_5830" s="T153">тот-3SG-1SG-ACC</ta>
            <ta e="T155" id="Seg_5831" s="T154">мама-1SG.[NOM]</ta>
            <ta e="T156" id="Seg_5832" s="T155">мама-1SG.[NOM]</ta>
            <ta e="T157" id="Seg_5833" s="T156">ведь</ta>
            <ta e="T158" id="Seg_5834" s="T157">один</ta>
            <ta e="T159" id="Seg_5835" s="T158">вот</ta>
            <ta e="T160" id="Seg_5836" s="T159">потом</ta>
            <ta e="T161" id="Seg_5837" s="T160">назад</ta>
            <ta e="T162" id="Seg_5838" s="T161">приходить-PST2-1PL</ta>
            <ta e="T163" id="Seg_5839" s="T162">мама-1SG.[NOM]</ta>
            <ta e="T164" id="Seg_5840" s="T163">старуха.[NOM]</ta>
            <ta e="T166" id="Seg_5841" s="T165">бабушка-1SG.[NOM]</ta>
            <ta e="T167" id="Seg_5842" s="T166">умирать-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T168" id="Seg_5843" s="T167">умирать-CVB.SEQ</ta>
            <ta e="T169" id="Seg_5844" s="T168">после</ta>
            <ta e="T170" id="Seg_5845" s="T169">оживать-EP-PST2-3SG</ta>
            <ta e="T171" id="Seg_5846" s="T170">тот</ta>
            <ta e="T172" id="Seg_5847" s="T171">место-DAT/LOC</ta>
            <ta e="T173" id="Seg_5848" s="T172">потом</ta>
            <ta e="T174" id="Seg_5849" s="T173">назад</ta>
            <ta e="T175" id="Seg_5850" s="T174">приходить-PST2-1PL</ta>
            <ta e="T176" id="Seg_5851" s="T175">тот</ta>
            <ta e="T177" id="Seg_5852" s="T176">приходить-PTCP.PST-1PL.[NOM]</ta>
            <ta e="T178" id="Seg_5853" s="T177">после.того</ta>
            <ta e="T179" id="Seg_5854" s="T178">десять</ta>
            <ta e="T180" id="Seg_5855" s="T179">шесть</ta>
            <ta e="T181" id="Seg_5856" s="T180">год-PROPR-1SG-DAT/LOC</ta>
            <ta e="T182" id="Seg_5857" s="T181">1SG-ACC</ta>
            <ta e="T183" id="Seg_5858" s="T182">муж-DAT/LOC</ta>
            <ta e="T184" id="Seg_5859" s="T183">давать-PST2-3SG</ta>
            <ta e="T185" id="Seg_5860" s="T184">1SG.[NOM]</ta>
            <ta e="T186" id="Seg_5861" s="T185">умирать-TEMP-1SG</ta>
            <ta e="T187" id="Seg_5862" s="T186">2SG-ACC</ta>
            <ta e="T188" id="Seg_5863" s="T187">слуга.[NOM]</ta>
            <ta e="T189" id="Seg_5864" s="T188">делать-FUT-3PL</ta>
            <ta e="T190" id="Seg_5865" s="T189">говорить-PST1-3SG</ta>
            <ta e="T191" id="Seg_5866" s="T190">черт</ta>
            <ta e="T192" id="Seg_5867" s="T191">что-ACC</ta>
            <ta e="T193" id="Seg_5868" s="T192">NEG</ta>
            <ta e="T194" id="Seg_5869" s="T193">чистый-1SG</ta>
            <ta e="T195" id="Seg_5870" s="T194">хоть</ta>
            <ta e="T196" id="Seg_5871" s="T195">мыться-CVB.SIM</ta>
            <ta e="T197" id="Seg_5872" s="T196">NEG</ta>
            <ta e="T198" id="Seg_5873" s="T197">еще.не-1SG</ta>
            <ta e="T199" id="Seg_5874" s="T198">муж-DAT/LOC</ta>
            <ta e="T200" id="Seg_5875" s="T199">идти-PST2-EP-1SG</ta>
            <ta e="T201" id="Seg_5876" s="T200">плакать-CVB.SIM-плакать-CVB.SIM</ta>
            <ta e="T202" id="Seg_5877" s="T201">муж-DAT/LOC</ta>
            <ta e="T203" id="Seg_5878" s="T202">давать-PRS-3PL</ta>
            <ta e="T204" id="Seg_5879" s="T203">спать-CAUS-PRS-3PL</ta>
            <ta e="T205" id="Seg_5880" s="T204">плакать-CVB.SIM-плакать-CVB.SIM</ta>
            <ta e="T206" id="Seg_5881" s="T205">уснуть-PRS-1SG</ta>
            <ta e="T207" id="Seg_5882" s="T206">муж-1SG-ABL</ta>
            <ta e="T208" id="Seg_5883" s="T207">бояться-PRS-1SG</ta>
            <ta e="T209" id="Seg_5884" s="T208">одежда-COM</ta>
            <ta e="T210" id="Seg_5885" s="T209">спать-PRS-1SG</ta>
            <ta e="T211" id="Seg_5886" s="T210">ээ</ta>
            <ta e="T212" id="Seg_5887" s="T211">вот</ta>
            <ta e="T213" id="Seg_5888" s="T212">потом</ta>
            <ta e="T214" id="Seg_5889" s="T213">год-ACC</ta>
            <ta e="T215" id="Seg_5890" s="T214">год-ACC</ta>
            <ta e="T216" id="Seg_5891" s="T215">одежда-PROPR</ta>
            <ta e="T217" id="Seg_5892" s="T216">спать-PST2-EP-1SG</ta>
            <ta e="T218" id="Seg_5893" s="T217">потом</ta>
            <ta e="T219" id="Seg_5894" s="T218">вот</ta>
            <ta e="T220" id="Seg_5895" s="T219">позже</ta>
            <ta e="T221" id="Seg_5896" s="T220">учиться-PRS-1SG</ta>
            <ta e="T222" id="Seg_5897" s="T221">EMPH</ta>
            <ta e="T223" id="Seg_5898" s="T222">муж</ta>
            <ta e="T224" id="Seg_5899" s="T223">муж</ta>
            <ta e="T225" id="Seg_5900" s="T224">муж-1SG-DAT/LOC</ta>
            <ta e="T226" id="Seg_5901" s="T225">тот</ta>
            <ta e="T227" id="Seg_5902" s="T226">имя-3SG.[NOM]</ta>
            <ta e="T233" id="Seg_5903" s="T232">десять</ta>
            <ta e="T234" id="Seg_5904" s="T233">семь</ta>
            <ta e="T235" id="Seg_5905" s="T234">десять</ta>
            <ta e="T236" id="Seg_5906" s="T235">восемь</ta>
            <ta e="T237" id="Seg_5907" s="T236">десять</ta>
            <ta e="T238" id="Seg_5908" s="T237">восемь</ta>
            <ta e="T239" id="Seg_5909" s="T238">год-PROPR-1SG-DAT/LOC</ta>
            <ta e="T240" id="Seg_5910" s="T239">ребенок-VBZ-PST2-EP-1SG</ta>
            <ta e="T241" id="Seg_5911" s="T240">первый</ta>
            <ta e="T242" id="Seg_5912" s="T241">ребенок-1SG.[NOM]</ta>
            <ta e="T243" id="Seg_5913" s="T242">здесь</ta>
            <ta e="T244" id="Seg_5914" s="T243">есть</ta>
            <ta e="T245" id="Seg_5915" s="T244">потом</ta>
            <ta e="T246" id="Seg_5916" s="T245">ребенок-VBZ-PST1-1SG</ta>
            <ta e="T247" id="Seg_5917" s="T246">ребенок-VBZ-PST1-1SG</ta>
            <ta e="T248" id="Seg_5918" s="T247">год.[NOM]</ta>
            <ta e="T249" id="Seg_5919" s="T248">каждый</ta>
            <ta e="T250" id="Seg_5920" s="T249">ребенок-VBZ-PRS-1SG</ta>
            <ta e="T251" id="Seg_5921" s="T250">год.[NOM]</ta>
            <ta e="T252" id="Seg_5922" s="T251">каждый</ta>
            <ta e="T254" id="Seg_5923" s="T252">ребенок-VBZ-PRS-1SG</ta>
            <ta e="T259" id="Seg_5924" s="T258">AFFIRM</ta>
            <ta e="T260" id="Seg_5925" s="T259">этот.EMPH</ta>
            <ta e="T261" id="Seg_5926" s="T260">земля-DAT/LOC</ta>
            <ta e="T262" id="Seg_5927" s="T261">тундра-DAT/LOC</ta>
            <ta e="T263" id="Seg_5928" s="T262">идти-CVB.SEQ-1PL</ta>
            <ta e="T264" id="Seg_5929" s="T263">тундра-DAT/LOC</ta>
            <ta e="T265" id="Seg_5930" s="T264">быть-PST1-1PL</ta>
            <ta e="T266" id="Seg_5931" s="T265">старик-EP-1SG.[NOM]</ta>
            <ta e="T267" id="Seg_5932" s="T266">кассир.[NOM]</ta>
            <ta e="T268" id="Seg_5933" s="T267">быть-PST1-3SG</ta>
            <ta e="T269" id="Seg_5934" s="T268">очень</ta>
            <ta e="T270" id="Seg_5935" s="T269">хороший</ta>
            <ta e="T271" id="Seg_5936" s="T270">человек.[NOM]</ta>
            <ta e="T272" id="Seg_5937" s="T271">быть-PST1-3SG</ta>
            <ta e="T273" id="Seg_5938" s="T272">способный</ta>
            <ta e="T274" id="Seg_5939" s="T273">очень</ta>
            <ta e="T275" id="Seg_5940" s="T274">охотник.[NOM]</ta>
            <ta e="T276" id="Seg_5941" s="T275">животное-ACC</ta>
            <ta e="T277" id="Seg_5942" s="T276">EMPH</ta>
            <ta e="T278" id="Seg_5943" s="T277">охотить-PST2-3SG</ta>
            <ta e="T279" id="Seg_5944" s="T278">дом.[NOM]</ta>
            <ta e="T280" id="Seg_5945" s="T279">да</ta>
            <ta e="T281" id="Seg_5946" s="T280">строить-PST2-3SG</ta>
            <ta e="T282" id="Seg_5947" s="T281">кассир-DAT/LOC</ta>
            <ta e="T283" id="Seg_5948" s="T282">десять</ta>
            <ta e="T284" id="Seg_5949" s="T283">пять</ta>
            <ta e="T285" id="Seg_5950" s="T284">год-ACC</ta>
            <ta e="T286" id="Seg_5951" s="T285">сидеть-PST2-3SG</ta>
            <ta e="T287" id="Seg_5952" s="T286">потом</ta>
            <ta e="T288" id="Seg_5953" s="T287">один</ta>
            <ta e="T289" id="Seg_5954" s="T288">год-ACC</ta>
            <ta e="T290" id="Seg_5955" s="T289">парторг-DAT/LOC</ta>
            <ta e="T291" id="Seg_5956" s="T290">жить-PST2-3SG</ta>
            <ta e="T292" id="Seg_5957" s="T291">потом</ta>
            <ta e="T293" id="Seg_5958" s="T292">дом-PL-ACC</ta>
            <ta e="T294" id="Seg_5959" s="T293">строить-PST2-3SG</ta>
            <ta e="T295" id="Seg_5960" s="T294">этот</ta>
            <ta e="T296" id="Seg_5961" s="T295">стоять-PRS.[3SG]</ta>
            <ta e="T297" id="Seg_5962" s="T296">строить-PTCP.PST</ta>
            <ta e="T298" id="Seg_5963" s="T297">дом-PL-3SG.[NOM]</ta>
            <ta e="T299" id="Seg_5964" s="T298">потом</ta>
            <ta e="T300" id="Seg_5965" s="T299">десять</ta>
            <ta e="T301" id="Seg_5966" s="T300">два</ta>
            <ta e="T302" id="Seg_5967" s="T301">ребенок-PROPR-1PL</ta>
            <ta e="T303" id="Seg_5968" s="T302">потом</ta>
            <ta e="T304" id="Seg_5969" s="T303">полностью</ta>
            <ta e="T305" id="Seg_5970" s="T304">один</ta>
            <ta e="T672" id="Seg_5971" s="T305">NEG</ta>
            <ta e="T306" id="Seg_5972" s="T672">человек-ACC</ta>
            <ta e="T307" id="Seg_5973" s="T306">человек-DAT/LOC</ta>
            <ta e="T308" id="Seg_5974" s="T307">давать-PST2.NEG-EP-1SG</ta>
            <ta e="T309" id="Seg_5975" s="T308">ребенок-PL-1SG-ACC</ta>
            <ta e="T310" id="Seg_5976" s="T309">сам-1SG.[NOM]</ta>
            <ta e="T311" id="Seg_5977" s="T310">воспитывать-PTCP.FUT.[NOM]</ta>
            <ta e="T312" id="Seg_5978" s="T311">быть-CVB.SEQ-1SG</ta>
            <ta e="T313" id="Seg_5979" s="T312">человек-DAT/LOC</ta>
            <ta e="T314" id="Seg_5980" s="T313">давать-NEG-1SG</ta>
            <ta e="T315" id="Seg_5981" s="T314">жалеть-PRS-1SG</ta>
            <ta e="T316" id="Seg_5982" s="T315">ребенок-PL-1SG-ACC</ta>
            <ta e="T317" id="Seg_5983" s="T316">тот-PL-1SG-ACC</ta>
            <ta e="T318" id="Seg_5984" s="T317">воспитывать-EP-MED-CVB.SIM</ta>
            <ta e="T319" id="Seg_5985" s="T318">воспитывать-EP-PTCP.PST-EP-INSTR</ta>
            <ta e="T320" id="Seg_5986" s="T319">колыбель-PROPR</ta>
            <ta e="T321" id="Seg_5987" s="T320">ребенок-PROPR.[NOM]</ta>
            <ta e="T322" id="Seg_5988" s="T321">сеть-VBZ-EP-RECP/COLL-HAB-1SG</ta>
            <ta e="T323" id="Seg_5989" s="T322">старик-1SG-ACC</ta>
            <ta e="T324" id="Seg_5990" s="T323">с</ta>
            <ta e="T325" id="Seg_5991" s="T324">больной.[NOM]</ta>
            <ta e="T326" id="Seg_5992" s="T325">становиться-PTCP.PST-3SG-ACC</ta>
            <ta e="T327" id="Seg_5993" s="T326">после.того</ta>
            <ta e="T328" id="Seg_5994" s="T327">вот</ta>
            <ta e="T329" id="Seg_5995" s="T328">ребенок-PL-1PL.[NOM]</ta>
            <ta e="T330" id="Seg_5996" s="T329">расти-CVB.SEQ-3PL</ta>
            <ta e="T331" id="Seg_5997" s="T330">учиться-CVB.SIM</ta>
            <ta e="T332" id="Seg_5998" s="T331">идти-FREQ-PST2-3PL</ta>
            <ta e="T333" id="Seg_5999" s="T332">потом</ta>
            <ta e="T334" id="Seg_6000" s="T333">позже</ta>
            <ta e="T335" id="Seg_6001" s="T334">два</ta>
            <ta e="T336" id="Seg_6002" s="T335">большой</ta>
            <ta e="T337" id="Seg_6003" s="T336">ребенок-1SG.[NOM]</ta>
            <ta e="T338" id="Seg_6004" s="T337">олень.[NOM]</ta>
            <ta e="T339" id="Seg_6005" s="T338">учеба-3SG-DAT/LOC</ta>
            <ta e="T340" id="Seg_6006" s="T339">идти-PST2-3PL</ta>
            <ta e="T341" id="Seg_6007" s="T340">сын-1PL.[NOM]</ta>
            <ta e="T342" id="Seg_6008" s="T341">дочь-1PL.[NOM]</ta>
            <ta e="T343" id="Seg_6009" s="T342">потом</ta>
            <ta e="T344" id="Seg_6010" s="T343">один</ta>
            <ta e="T345" id="Seg_6011" s="T344">ребенок-1PL.[NOM]</ta>
            <ta e="T346" id="Seg_6012" s="T345">расти-CVB.SEQ</ta>
            <ta e="T347" id="Seg_6013" s="T346">Красноярск-DAT/LOC</ta>
            <ta e="T348" id="Seg_6014" s="T347">учиться-CVB.SIM</ta>
            <ta e="T349" id="Seg_6015" s="T348">идти-PST2-3SG</ta>
            <ta e="T350" id="Seg_6016" s="T349">теперь</ta>
            <ta e="T351" id="Seg_6017" s="T350">терапевт-DAT/LOC</ta>
            <ta e="T352" id="Seg_6018" s="T351">работать-CVB.SIM</ta>
            <ta e="T353" id="Seg_6019" s="T352">лежать-PRS.[3SG]</ta>
            <ta e="T354" id="Seg_6020" s="T353">Дудинка-DAT/LOC</ta>
            <ta e="T355" id="Seg_6021" s="T354">потом</ta>
            <ta e="T356" id="Seg_6022" s="T355">один</ta>
            <ta e="T357" id="Seg_6023" s="T356">один</ta>
            <ta e="T358" id="Seg_6024" s="T357">ребенок-1SG.[NOM]</ta>
            <ta e="T359" id="Seg_6025" s="T358">однако</ta>
            <ta e="T360" id="Seg_6026" s="T359">опять</ta>
            <ta e="T361" id="Seg_6027" s="T360">учиться-CVB.SEQ</ta>
            <ta e="T362" id="Seg_6028" s="T361">два</ta>
            <ta e="T363" id="Seg_6029" s="T362">институт-ACC</ta>
            <ta e="T364" id="Seg_6030" s="T363">кончать-PST2-3SG</ta>
            <ta e="T365" id="Seg_6031" s="T364">потом</ta>
            <ta e="T366" id="Seg_6032" s="T365">Красноярск-DAT/LOC</ta>
            <ta e="T367" id="Seg_6033" s="T366">есть</ta>
            <ta e="T369" id="Seg_6034" s="T368">Сергей.[NOM]</ta>
            <ta e="T370" id="Seg_6035" s="T369">тот-3SG-1SG.[NOM]</ta>
            <ta e="T371" id="Seg_6036" s="T370">жена-PROPR</ta>
            <ta e="T372" id="Seg_6037" s="T371">ребенок.[NOM]</ta>
            <ta e="T373" id="Seg_6038" s="T372">тот</ta>
            <ta e="T374" id="Seg_6039" s="T373">два</ta>
            <ta e="T375" id="Seg_6040" s="T374">сын-PROPR.[NOM]</ta>
            <ta e="T376" id="Seg_6041" s="T375">Казах-PL-ABL</ta>
            <ta e="T377" id="Seg_6042" s="T376">жена-VBZ-PST2.[3SG]</ta>
            <ta e="T378" id="Seg_6043" s="T377">лабаз-PROPR-ABL</ta>
            <ta e="T379" id="Seg_6044" s="T378">теперь</ta>
            <ta e="T380" id="Seg_6045" s="T379">однако</ta>
            <ta e="T381" id="Seg_6046" s="T380">охрана-DAT/LOC</ta>
            <ta e="T382" id="Seg_6047" s="T381">охрана-DAT/LOC</ta>
            <ta e="T383" id="Seg_6048" s="T382">работать-PRS.[3SG]</ta>
            <ta e="T384" id="Seg_6049" s="T383">милиция-ACC</ta>
            <ta e="T385" id="Seg_6050" s="T384">кончать-CVB.SEQ</ta>
            <ta e="T386" id="Seg_6051" s="T385">после</ta>
            <ta e="T387" id="Seg_6052" s="T386">раньше</ta>
            <ta e="T388" id="Seg_6053" s="T387">олень.[NOM]</ta>
            <ta e="T389" id="Seg_6054" s="T388">работник-3SG.[NOM]</ta>
            <ta e="T390" id="Seg_6055" s="T389">быть-PST2-3SG</ta>
            <ta e="T391" id="Seg_6056" s="T390">потом</ta>
            <ta e="T392" id="Seg_6057" s="T391">один</ta>
            <ta e="T393" id="Seg_6058" s="T392">ребенок-1SG.[NOM]</ta>
            <ta e="T394" id="Seg_6059" s="T393">повар-DAT/LOC</ta>
            <ta e="T395" id="Seg_6060" s="T394">Хатанга-DAT/LOC</ta>
            <ta e="T396" id="Seg_6061" s="T395">работать-PRS.[3SG]</ta>
            <ta e="T397" id="Seg_6062" s="T396">главный</ta>
            <ta e="T398" id="Seg_6063" s="T397">повар.[NOM]</ta>
            <ta e="T399" id="Seg_6064" s="T398">один</ta>
            <ta e="T400" id="Seg_6065" s="T399">дочь-EP-1SG.[NOM]</ta>
            <ta e="T401" id="Seg_6066" s="T400">один</ta>
            <ta e="T403" id="Seg_6067" s="T402">что.[NOM]</ta>
            <ta e="T405" id="Seg_6068" s="T404">Хатанга-DAT/LOC</ta>
            <ta e="T406" id="Seg_6069" s="T405">Марина</ta>
            <ta e="T410" id="Seg_6070" s="T409">нет</ta>
            <ta e="T411" id="Seg_6071" s="T410">учиться-PST2-3SG</ta>
            <ta e="T412" id="Seg_6072" s="T411">AFFIRM</ta>
            <ta e="T413" id="Seg_6073" s="T412">тот.[NOM]</ta>
            <ta e="T414" id="Seg_6074" s="T413">к</ta>
            <ta e="T415" id="Seg_6075" s="T414">Красноярск-DAT/LOC</ta>
            <ta e="T416" id="Seg_6076" s="T415">учиться-PST2-3SG</ta>
            <ta e="T417" id="Seg_6077" s="T416">потом</ta>
            <ta e="T419" id="Seg_6078" s="T418">доктор.[NOM]</ta>
            <ta e="T420" id="Seg_6079" s="T419">дочь-EP-1SG.[NOM]</ta>
            <ta e="T421" id="Seg_6080" s="T420">Дудинка-DAT/LOC</ta>
            <ta e="T422" id="Seg_6081" s="T421">работать-PRS.[3SG]</ta>
            <ta e="T423" id="Seg_6082" s="T422">терапевт-DAT/LOC</ta>
            <ta e="T424" id="Seg_6083" s="T423">работать-PRS.[3SG]</ta>
            <ta e="T425" id="Seg_6084" s="T424">теперь</ta>
            <ta e="T426" id="Seg_6085" s="T425">три</ta>
            <ta e="T427" id="Seg_6086" s="T426">ребенок-PROPR.[NOM]</ta>
            <ta e="T428" id="Seg_6087" s="T427">старый</ta>
            <ta e="T429" id="Seg_6088" s="T428">жена.[NOM]</ta>
            <ta e="T430" id="Seg_6089" s="T429">пенсия-PROPR</ta>
            <ta e="T431" id="Seg_6090" s="T430">ребенок-1SG.[NOM]</ta>
            <ta e="T432" id="Seg_6091" s="T431">каждый-3SG.[NOM]</ta>
            <ta e="T433" id="Seg_6092" s="T432">пенсия-PROPR.[NOM]</ta>
            <ta e="T434" id="Seg_6093" s="T433">три</ta>
            <ta e="T435" id="Seg_6094" s="T434">ребенок-1SG.[NOM]</ta>
            <ta e="T436" id="Seg_6095" s="T435">пенсия-PROPR-3PL</ta>
            <ta e="T437" id="Seg_6096" s="T436">большой-PL-EP-1SG.[NOM]</ta>
            <ta e="T438" id="Seg_6097" s="T437">теперь</ta>
            <ta e="T439" id="Seg_6098" s="T438">один.из.двух-3SG.[NOM]</ta>
            <ta e="T440" id="Seg_6099" s="T439">однако</ta>
            <ta e="T441" id="Seg_6100" s="T440">рыбак-VBZ-PRS.[3SG]</ta>
            <ta e="T442" id="Seg_6101" s="T441">один.из.двух-3SG.[NOM]</ta>
            <ta e="T443" id="Seg_6102" s="T442">MOD</ta>
            <ta e="T444" id="Seg_6103" s="T443">теперь</ta>
            <ta e="T445" id="Seg_6104" s="T444">быть.в.годах-PST2.[3SG]</ta>
            <ta e="T446" id="Seg_6105" s="T445">пенсия-PROPR</ta>
            <ta e="T447" id="Seg_6106" s="T446">Вера.[NOM]</ta>
            <ta e="T448" id="Seg_6107" s="T447">муж-3SG.[NOM]</ta>
            <ta e="T449" id="Seg_6108" s="T448">этот.EMPH-INTNS</ta>
            <ta e="T450" id="Seg_6109" s="T449">умирать-PST2-3SG</ta>
            <ta e="T451" id="Seg_6110" s="T450">два</ta>
            <ta e="T452" id="Seg_6111" s="T451">ребенок-PROPR.[NOM]</ta>
            <ta e="T453" id="Seg_6112" s="T452">три-ORD-3PL.[NOM]</ta>
            <ta e="T454" id="Seg_6113" s="T453">умирать-PST2-3SG</ta>
            <ta e="T455" id="Seg_6114" s="T454">большой</ta>
            <ta e="T456" id="Seg_6115" s="T455">сын-EP-1SG.[NOM]</ta>
            <ta e="T457" id="Seg_6116" s="T456">три</ta>
            <ta e="T458" id="Seg_6117" s="T457">ребенок-PROPR.[NOM]</ta>
            <ta e="T459" id="Seg_6118" s="T458">ребенок.[NOM]</ta>
            <ta e="T460" id="Seg_6119" s="T459">один</ta>
            <ta e="T461" id="Seg_6120" s="T460">ребенок-3SG.[NOM]</ta>
            <ta e="T462" id="Seg_6121" s="T461">жена-PROPR</ta>
            <ta e="T463" id="Seg_6122" s="T462">ребенок-PROPR.[NOM]</ta>
            <ta e="T464" id="Seg_6123" s="T463">другой.из.двух-3PL.[NOM]</ta>
            <ta e="T465" id="Seg_6124" s="T464">ребенок-VBZ-CVB.SIM</ta>
            <ta e="T467" id="Seg_6125" s="T465">еще.не-3PL</ta>
            <ta e="T471" id="Seg_6126" s="T469">AFFIRM</ta>
            <ta e="T472" id="Seg_6127" s="T471">потом</ta>
            <ta e="T473" id="Seg_6128" s="T472">один</ta>
            <ta e="T474" id="Seg_6129" s="T473">ребенок-1SG.[NOM]</ta>
            <ta e="T475" id="Seg_6130" s="T474">вот</ta>
            <ta e="T476" id="Seg_6131" s="T475">кто-DAT/LOC</ta>
            <ta e="T477" id="Seg_6132" s="T476">есть</ta>
            <ta e="T478" id="Seg_6133" s="T477">Сопочное-DAT/LOC</ta>
            <ta e="T479" id="Seg_6134" s="T478">есть</ta>
            <ta e="T480" id="Seg_6135" s="T479">кто-DAT/LOC</ta>
            <ta e="T481" id="Seg_6136" s="T480">воспитатель-DAT/LOC</ta>
            <ta e="T482" id="Seg_6137" s="T481">работать-PRS.[3SG]</ta>
            <ta e="T483" id="Seg_6138" s="T482">потом</ta>
            <ta e="T484" id="Seg_6139" s="T483">один</ta>
            <ta e="T485" id="Seg_6140" s="T484">ребенок-1SG.[NOM]</ta>
            <ta e="T486" id="Seg_6141" s="T485">здесь</ta>
            <ta e="T487" id="Seg_6142" s="T486">один</ta>
            <ta e="T488" id="Seg_6143" s="T487">дочь-EP-1SG.[NOM]</ta>
            <ta e="T489" id="Seg_6144" s="T488">здесь</ta>
            <ta e="T490" id="Seg_6145" s="T489">воспитатель-VBZ-PRS.[3SG]</ta>
            <ta e="T491" id="Seg_6146" s="T490">один</ta>
            <ta e="T492" id="Seg_6147" s="T491">ребенок-1SG.[NOM]</ta>
            <ta e="T494" id="Seg_6148" s="T492">MOD</ta>
            <ta e="T497" id="Seg_6149" s="T496">AFFIRM</ta>
            <ta e="T498" id="Seg_6150" s="T497">школа-DAT/LOC</ta>
            <ta e="T499" id="Seg_6151" s="T498">здесь</ta>
            <ta e="T500" id="Seg_6152" s="T499">Домна</ta>
            <ta e="T501" id="Seg_6153" s="T500">Петровна.[NOM]</ta>
            <ta e="T508" id="Seg_6154" s="T506">ээ</ta>
            <ta e="T509" id="Seg_6155" s="T508">потом</ta>
            <ta e="T510" id="Seg_6156" s="T509">один</ta>
            <ta e="T511" id="Seg_6157" s="T510">маленький</ta>
            <ta e="T512" id="Seg_6158" s="T511">сын-EP-1SG.[NOM]</ta>
            <ta e="T513" id="Seg_6159" s="T512">жена-3SG.[NOM]</ta>
            <ta e="T514" id="Seg_6160" s="T513">тоже</ta>
            <ta e="T515" id="Seg_6161" s="T514">здесь</ta>
            <ta e="T516" id="Seg_6162" s="T515">учитель-VBZ-PRS.[3SG]</ta>
            <ta e="T517" id="Seg_6163" s="T516">Надя</ta>
            <ta e="T518" id="Seg_6164" s="T517">Киргизова</ta>
            <ta e="T519" id="Seg_6165" s="T518">EMPH</ta>
            <ta e="T521" id="Seg_6166" s="T520">потом</ta>
            <ta e="T522" id="Seg_6167" s="T521">один</ta>
            <ta e="T523" id="Seg_6168" s="T522">сын-EP-1SG.[NOM]</ta>
            <ta e="T524" id="Seg_6169" s="T523">однако</ta>
            <ta e="T525" id="Seg_6170" s="T524">кто.[NOM]</ta>
            <ta e="T526" id="Seg_6171" s="T525">вездеходчик.[NOM]</ta>
            <ta e="T527" id="Seg_6172" s="T526">кончать-PST2-3SG</ta>
            <ta e="T528" id="Seg_6173" s="T527">учиться-CVB.SEQ</ta>
            <ta e="T529" id="Seg_6174" s="T528">работать-PTCP.PRS</ta>
            <ta e="T530" id="Seg_6175" s="T529">быть-PST1-3SG</ta>
            <ta e="T531" id="Seg_6176" s="T530">близнец-PL.[NOM]</ta>
            <ta e="T532" id="Seg_6177" s="T531">один.из.двух-3SG.[NOM]</ta>
            <ta e="T533" id="Seg_6178" s="T532">электрик.[NOM]</ta>
            <ta e="T534" id="Seg_6179" s="T533">тот</ta>
            <ta e="T535" id="Seg_6180" s="T534">работать-CVB.SIM</ta>
            <ta e="T536" id="Seg_6181" s="T535">идти-PRS.[3SG]</ta>
            <ta e="T537" id="Seg_6182" s="T536">теперь-DAT/LOC</ta>
            <ta e="T538" id="Seg_6183" s="T537">пока</ta>
            <ta e="T539" id="Seg_6184" s="T538">учиться-CVB.SEQ</ta>
            <ta e="T540" id="Seg_6185" s="T539">кончать-CVB.SEQ</ta>
            <ta e="T541" id="Seg_6186" s="T540">после</ta>
            <ta e="T542" id="Seg_6187" s="T541">так</ta>
            <ta e="T543" id="Seg_6188" s="T542">EMPH</ta>
            <ta e="T544" id="Seg_6189" s="T543">каждый-3SG.[NOM]</ta>
            <ta e="T545" id="Seg_6190" s="T544">учиться-PST2-3PL</ta>
            <ta e="T546" id="Seg_6191" s="T545">потом</ta>
            <ta e="T547" id="Seg_6192" s="T546">вот</ta>
            <ta e="T548" id="Seg_6193" s="T547">один</ta>
            <ta e="T549" id="Seg_6194" s="T548">ребенок-1SG.[NOM]</ta>
            <ta e="T550" id="Seg_6195" s="T549">Кресты-DAT/LOC</ta>
            <ta e="T551" id="Seg_6196" s="T550">есть</ta>
            <ta e="T553" id="Seg_6197" s="T552">ээ</ta>
            <ta e="T554" id="Seg_6198" s="T553">милиция.[NOM]</ta>
            <ta e="T555" id="Seg_6199" s="T554">учеба-3SG-ACC</ta>
            <ta e="T556" id="Seg_6200" s="T555">кончать-PTCP.PST</ta>
            <ta e="T557" id="Seg_6201" s="T556">быть-PST1-3SG</ta>
            <ta e="T558" id="Seg_6202" s="T557">тот-3SG-2SG.[NOM]</ta>
            <ta e="T559" id="Seg_6203" s="T558">место.[NOM]</ta>
            <ta e="T560" id="Seg_6204" s="T559">каждый</ta>
            <ta e="T561" id="Seg_6205" s="T560">место-ACC</ta>
            <ta e="T562" id="Seg_6206" s="T561">идти-EP-CAUS-CVB.SEQ</ta>
            <ta e="T563" id="Seg_6207" s="T562">идти-EP-CAUS-CVB.SEQ</ta>
            <ta e="T564" id="Seg_6208" s="T563">после</ta>
            <ta e="T565" id="Seg_6209" s="T564">Кресты-DAT/LOC</ta>
            <ta e="T566" id="Seg_6210" s="T565">жена-PROPR.[NOM]</ta>
            <ta e="T567" id="Seg_6211" s="T566">Алексей.[NOM]</ta>
            <ta e="T568" id="Seg_6212" s="T567">видеть-PST1-2SG</ta>
            <ta e="T569" id="Seg_6213" s="T568">EMPH</ta>
            <ta e="T570" id="Seg_6214" s="T569">очевидно</ta>
            <ta e="T571" id="Seg_6215" s="T570">наверно</ta>
            <ta e="T572" id="Seg_6216" s="T571">потом</ta>
            <ta e="T573" id="Seg_6217" s="T572">другой.из.двух-3PL.[NOM]</ta>
            <ta e="T574" id="Seg_6218" s="T573">охотник-PL.[NOM]</ta>
            <ta e="T575" id="Seg_6219" s="T574">три</ta>
            <ta e="T576" id="Seg_6220" s="T575">мальчик-EP-1SG.[NOM]</ta>
            <ta e="T577" id="Seg_6221" s="T576">охотник-PL.[NOM]</ta>
            <ta e="T578" id="Seg_6222" s="T577">MOD</ta>
            <ta e="T579" id="Seg_6223" s="T578">Алеша.[NOM]</ta>
            <ta e="T580" id="Seg_6224" s="T579">Сергей.[NOM]</ta>
            <ta e="T581" id="Seg_6225" s="T580">Миша.[NOM]</ta>
            <ta e="T582" id="Seg_6226" s="T581">Витя.[NOM]</ta>
            <ta e="T583" id="Seg_6227" s="T582">Витя.[NOM]</ta>
            <ta e="T584" id="Seg_6228" s="T583">спать-CVB.SIM</ta>
            <ta e="T585" id="Seg_6229" s="T584">лежать-PRS.[3SG]</ta>
            <ta e="T586" id="Seg_6230" s="T585">жена-3SG.[NOM]</ta>
            <ta e="T587" id="Seg_6231" s="T586">этот</ta>
            <ta e="T588" id="Seg_6232" s="T587">стоять-PRS.[3SG]</ta>
            <ta e="T589" id="Seg_6233" s="T588">последний-3SG.[NOM]</ta>
            <ta e="T590" id="Seg_6234" s="T589">MOD</ta>
            <ta e="T591" id="Seg_6235" s="T590">потом</ta>
            <ta e="T592" id="Seg_6236" s="T591">десять</ta>
            <ta e="T593" id="Seg_6237" s="T592">два</ta>
            <ta e="T594" id="Seg_6238" s="T593">ребенок-PROPR-1SG</ta>
            <ta e="T595" id="Seg_6239" s="T594">двадцать</ta>
            <ta e="T596" id="Seg_6240" s="T595">остаток-3SG.[NOM]</ta>
            <ta e="T597" id="Seg_6241" s="T596">пять</ta>
            <ta e="T598" id="Seg_6242" s="T597">ээ</ta>
            <ta e="T599" id="Seg_6243" s="T598">семь</ta>
            <ta e="T600" id="Seg_6244" s="T599">внук-PROPR-1SG</ta>
            <ta e="T601" id="Seg_6245" s="T600">тогда</ta>
            <ta e="T602" id="Seg_6246" s="T601">деревня-1PL.[NOM]</ta>
            <ta e="T603" id="Seg_6247" s="T602">так</ta>
            <ta e="T604" id="Seg_6248" s="T603">четыре-десять-ABL</ta>
            <ta e="T605" id="Seg_6249" s="T604">больше</ta>
            <ta e="T606" id="Seg_6250" s="T605">человек.[NOM]</ta>
            <ta e="T607" id="Seg_6251" s="T606">невестка-PL-1SG-ACC</ta>
            <ta e="T608" id="Seg_6252" s="T607">целый-3SG-ACC</ta>
            <ta e="T609" id="Seg_6253" s="T608">найти-TEMP-1SG</ta>
            <ta e="T610" id="Seg_6254" s="T609">много-3PL</ta>
            <ta e="T611" id="Seg_6255" s="T610">считать-TEMP-1SG</ta>
            <ta e="T612" id="Seg_6256" s="T611">один</ta>
            <ta e="T613" id="Seg_6257" s="T612">невестка.[NOM]</ta>
            <ta e="T614" id="Seg_6258" s="T613">два</ta>
            <ta e="T615" id="Seg_6259" s="T614">невестка.[NOM]</ta>
            <ta e="T616" id="Seg_6260" s="T615">педагог-PL.[NOM]</ta>
            <ta e="T617" id="Seg_6261" s="T616">ребенок-PL-PROPR-3PL</ta>
            <ta e="T618" id="Seg_6262" s="T617">каждый-3PL.[NOM]</ta>
            <ta e="T619" id="Seg_6263" s="T618">дочь-3PL.[NOM]</ta>
            <ta e="T620" id="Seg_6264" s="T619">учиться-CVB.SIM</ta>
            <ta e="T621" id="Seg_6265" s="T620">идти-PST2-3PL</ta>
            <ta e="T622" id="Seg_6266" s="T621">один</ta>
            <ta e="T623" id="Seg_6267" s="T622">невестка-EP-1SG.[NOM]</ta>
            <ta e="T624" id="Seg_6268" s="T623">садик-DAT/LOC</ta>
            <ta e="T625" id="Seg_6269" s="T624">кто-3PL.[NOM]</ta>
            <ta e="T626" id="Seg_6270" s="T625">кто.[NOM]</ta>
            <ta e="T627" id="Seg_6271" s="T626">быть-PST1-3SG=Q</ta>
            <ta e="T628" id="Seg_6272" s="T627">вот</ta>
            <ta e="T629" id="Seg_6273" s="T628">друг</ta>
            <ta e="T630" id="Seg_6274" s="T629">заведующий-PL.[NOM]</ta>
            <ta e="T631" id="Seg_6275" s="T630">два</ta>
            <ta e="T632" id="Seg_6276" s="T631">невестка-EP-1SG.[NOM]</ta>
            <ta e="T633" id="Seg_6277" s="T632">учитель-PL.[NOM]</ta>
            <ta e="T634" id="Seg_6278" s="T633">педагог-PL.[NOM]</ta>
            <ta e="T635" id="Seg_6279" s="T634">каждый-3SG.[NOM]</ta>
            <ta e="T636" id="Seg_6280" s="T635">работник-PL.[NOM]</ta>
            <ta e="T637" id="Seg_6281" s="T636">потом</ta>
            <ta e="T638" id="Seg_6282" s="T637">один</ta>
            <ta e="T639" id="Seg_6283" s="T638">зять-EP-1SG.[NOM]</ta>
            <ta e="T640" id="Seg_6284" s="T639">тот</ta>
            <ta e="T641" id="Seg_6285" s="T640">небо-VBZ-PST1-3SG</ta>
            <ta e="T642" id="Seg_6286" s="T641">один</ta>
            <ta e="T643" id="Seg_6287" s="T642">зять-EP-1SG.[NOM]</ta>
            <ta e="T644" id="Seg_6288" s="T643">больной.[NOM]</ta>
            <ta e="T645" id="Seg_6289" s="T644">быть-PST2.[3SG]</ta>
            <ta e="T646" id="Seg_6290" s="T645">ребенок.[NOM]</ta>
            <ta e="T647" id="Seg_6291" s="T646">Домна</ta>
            <ta e="T648" id="Seg_6292" s="T647">Петровна.[NOM]</ta>
            <ta e="T649" id="Seg_6293" s="T648">собственный-3SG.[NOM]</ta>
            <ta e="T650" id="Seg_6294" s="T649">тот.[NOM]</ta>
            <ta e="T651" id="Seg_6295" s="T650">несмотря.на</ta>
            <ta e="T652" id="Seg_6296" s="T651">животное-ACC</ta>
            <ta e="T653" id="Seg_6297" s="T652">вдоволь</ta>
            <ta e="T654" id="Seg_6298" s="T653">убить-PRS.[3SG]</ta>
            <ta e="T655" id="Seg_6299" s="T654">нога-POSS</ta>
            <ta e="T656" id="Seg_6300" s="T655">NEG</ta>
            <ta e="T657" id="Seg_6301" s="T656">сам-3SG.[NOM]</ta>
            <ta e="T658" id="Seg_6302" s="T657">один</ta>
            <ta e="T659" id="Seg_6303" s="T658">зять-EP-1SG.[NOM]</ta>
            <ta e="T660" id="Seg_6304" s="T659">Сопочное-DAT/LOC</ta>
            <ta e="T661" id="Seg_6305" s="T660">охотник.[NOM]</ta>
            <ta e="T662" id="Seg_6306" s="T661">дочь-3SG.[NOM]</ta>
            <ta e="T663" id="Seg_6307" s="T662">учиться-CVB.SIM</ta>
            <ta e="T664" id="Seg_6308" s="T663">идти-PST2.[3SG]</ta>
            <ta e="T665" id="Seg_6309" s="T664">вот</ta>
            <ta e="T666" id="Seg_6310" s="T665">что-ACC</ta>
            <ta e="T667" id="Seg_6311" s="T666">рассказывать-PTCP.FUT-1SG-ACC=Q</ta>
            <ta e="T671" id="Seg_6312" s="T670">вот</ta>
         </annotation>
         <annotation name="mc" tierref="mc-KiPP">
            <ta e="T2" id="Seg_6313" s="T1">dempro-pro:case</ta>
            <ta e="T3" id="Seg_6314" s="T2">v-v:mood.[v:pred.pn]</ta>
            <ta e="T4" id="Seg_6315" s="T3">ptcl</ta>
            <ta e="T9" id="Seg_6316" s="T6">ptcl</ta>
            <ta e="T15" id="Seg_6317" s="T14">ptcl</ta>
            <ta e="T17" id="Seg_6318" s="T15">que-pro:case</ta>
            <ta e="T18" id="Seg_6319" s="T17">v-v:tense.[v:pred.pn]</ta>
            <ta e="T19" id="Seg_6320" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_6321" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_6322" s="T20">adv</ta>
            <ta e="T22" id="Seg_6323" s="T21">adj-n:poss-n:case</ta>
            <ta e="T24" id="Seg_6324" s="T22">v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T25" id="Seg_6325" s="T24">ptcl</ta>
            <ta e="T30" id="Seg_6326" s="T29">v-v:tense.[v:pred.pn]</ta>
            <ta e="T33" id="Seg_6327" s="T31">interj</ta>
            <ta e="T51" id="Seg_6328" s="T49">interj</ta>
            <ta e="T52" id="Seg_6329" s="T51">que</ta>
            <ta e="T53" id="Seg_6330" s="T52">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T54" id="Seg_6331" s="T53">dempro</ta>
            <ta e="T55" id="Seg_6332" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_6333" s="T55">ptcl</ta>
            <ta e="T57" id="Seg_6334" s="T56">post</ta>
            <ta e="T68" id="Seg_6335" s="T66">propr.[n:case]</ta>
            <ta e="T72" id="Seg_6336" s="T70">propr-n:case</ta>
            <ta e="T73" id="Seg_6337" s="T72">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T74" id="Seg_6338" s="T73">adv&gt;adv-adv</ta>
            <ta e="T75" id="Seg_6339" s="T74">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T76" id="Seg_6340" s="T75">n-n:poss-n:case</ta>
            <ta e="T77" id="Seg_6341" s="T76">adj-n:(pred.pn)</ta>
            <ta e="T78" id="Seg_6342" s="T77">v-v:tense-v:pred.pn</ta>
            <ta e="T79" id="Seg_6343" s="T78">adj.[n:case]</ta>
            <ta e="T80" id="Seg_6344" s="T79">n-n:poss-n:case</ta>
            <ta e="T107" id="Seg_6345" s="T106">n-n:(poss)</ta>
            <ta e="T108" id="Seg_6346" s="T107">ptcl</ta>
            <ta e="T112" id="Seg_6347" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_6348" s="T112">pers.[pro:case]</ta>
            <ta e="T115" id="Seg_6349" s="T113">n-n:(poss).[n:case]</ta>
            <ta e="T117" id="Seg_6350" s="T115">v-v:tense-v:poss.pn</ta>
            <ta e="T121" id="Seg_6351" s="T120">n.[n:case]</ta>
            <ta e="T122" id="Seg_6352" s="T121">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T123" id="Seg_6353" s="T122">n-n:(poss).[n:case]</ta>
            <ta e="T124" id="Seg_6354" s="T123">n-n:(poss).[n:case]</ta>
            <ta e="T129" id="Seg_6355" s="T128">adv</ta>
            <ta e="T130" id="Seg_6356" s="T129">pers.[pro:case]</ta>
            <ta e="T131" id="Seg_6357" s="T130">n-n:(num).[n:case]</ta>
            <ta e="T132" id="Seg_6358" s="T131">adv</ta>
            <ta e="T133" id="Seg_6359" s="T132">v-v:cvb-v:pred.pn</ta>
            <ta e="T134" id="Seg_6360" s="T133">n.[n:case]</ta>
            <ta e="T135" id="Seg_6361" s="T134">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T136" id="Seg_6362" s="T135">n-n:case</ta>
            <ta e="T137" id="Seg_6363" s="T136">post</ta>
            <ta e="T138" id="Seg_6364" s="T137">n-n:poss-n:case</ta>
            <ta e="T139" id="Seg_6365" s="T138">post</ta>
            <ta e="T140" id="Seg_6366" s="T139">adv</ta>
            <ta e="T142" id="Seg_6367" s="T141">propr.[n:case]</ta>
            <ta e="T143" id="Seg_6368" s="T142">post</ta>
            <ta e="T144" id="Seg_6369" s="T143">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T145" id="Seg_6370" s="T144">adv</ta>
            <ta e="T146" id="Seg_6371" s="T145">dempro</ta>
            <ta e="T147" id="Seg_6372" s="T146">v-v:tense-v:pred.pn</ta>
            <ta e="T149" id="Seg_6373" s="T148">n-n:poss-n:case</ta>
            <ta e="T150" id="Seg_6374" s="T149">n-n:poss-n:case</ta>
            <ta e="T151" id="Seg_6375" s="T150">post</ta>
            <ta e="T152" id="Seg_6376" s="T151">n.[n:case]</ta>
            <ta e="T153" id="Seg_6377" s="T152">v-v:mood-v:pred.pn</ta>
            <ta e="T154" id="Seg_6378" s="T153">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T155" id="Seg_6379" s="T154">n-n:(poss).[n:case]</ta>
            <ta e="T156" id="Seg_6380" s="T155">n-n:(poss).[n:case]</ta>
            <ta e="T157" id="Seg_6381" s="T156">ptcl</ta>
            <ta e="T158" id="Seg_6382" s="T157">cardnum</ta>
            <ta e="T159" id="Seg_6383" s="T158">ptcl</ta>
            <ta e="T160" id="Seg_6384" s="T159">adv</ta>
            <ta e="T161" id="Seg_6385" s="T160">adv</ta>
            <ta e="T162" id="Seg_6386" s="T161">v-v:tense-v:pred.pn</ta>
            <ta e="T163" id="Seg_6387" s="T162">n-n:(poss).[n:case]</ta>
            <ta e="T164" id="Seg_6388" s="T163">n.[n:case]</ta>
            <ta e="T166" id="Seg_6389" s="T165">n-n:(poss).[n:case]</ta>
            <ta e="T167" id="Seg_6390" s="T166">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T168" id="Seg_6391" s="T167">v-v:cvb</ta>
            <ta e="T169" id="Seg_6392" s="T168">post</ta>
            <ta e="T170" id="Seg_6393" s="T169">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T171" id="Seg_6394" s="T170">dempro</ta>
            <ta e="T172" id="Seg_6395" s="T171">n-n:case</ta>
            <ta e="T173" id="Seg_6396" s="T172">adv</ta>
            <ta e="T174" id="Seg_6397" s="T173">adv</ta>
            <ta e="T175" id="Seg_6398" s="T174">v-v:tense-v:pred.pn</ta>
            <ta e="T176" id="Seg_6399" s="T175">dempro</ta>
            <ta e="T177" id="Seg_6400" s="T176">v-v:ptcp-v:(poss).[v:(case)]</ta>
            <ta e="T178" id="Seg_6401" s="T177">post</ta>
            <ta e="T179" id="Seg_6402" s="T178">cardnum</ta>
            <ta e="T180" id="Seg_6403" s="T179">cardnum</ta>
            <ta e="T181" id="Seg_6404" s="T180">n-n&gt;adj-n:poss-n:case</ta>
            <ta e="T182" id="Seg_6405" s="T181">pers-pro:case</ta>
            <ta e="T183" id="Seg_6406" s="T182">n-n:case</ta>
            <ta e="T184" id="Seg_6407" s="T183">v-v:tense-v:poss.pn</ta>
            <ta e="T185" id="Seg_6408" s="T184">pers.[pro:case]</ta>
            <ta e="T186" id="Seg_6409" s="T185">v-v:mood-v:temp.pn</ta>
            <ta e="T187" id="Seg_6410" s="T186">pers-pro:case</ta>
            <ta e="T188" id="Seg_6411" s="T187">n.[n:case]</ta>
            <ta e="T189" id="Seg_6412" s="T188">v-v:tense-v:poss.pn</ta>
            <ta e="T190" id="Seg_6413" s="T189">v-v:tense-v:poss.pn</ta>
            <ta e="T191" id="Seg_6414" s="T190">n</ta>
            <ta e="T192" id="Seg_6415" s="T191">que-pro:case</ta>
            <ta e="T193" id="Seg_6416" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_6417" s="T193">adj-n:(pred.pn)</ta>
            <ta e="T195" id="Seg_6418" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_6419" s="T195">v-v:cvb</ta>
            <ta e="T197" id="Seg_6420" s="T196">ptcl</ta>
            <ta e="T198" id="Seg_6421" s="T197">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T199" id="Seg_6422" s="T198">n-n:case</ta>
            <ta e="T200" id="Seg_6423" s="T199">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T201" id="Seg_6424" s="T200">v-v:cvb-v-v:cvb</ta>
            <ta e="T202" id="Seg_6425" s="T201">n-n:case</ta>
            <ta e="T203" id="Seg_6426" s="T202">v-v:tense-v:pred.pn</ta>
            <ta e="T204" id="Seg_6427" s="T203">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T205" id="Seg_6428" s="T204">v-v:cvb-v-v:cvb</ta>
            <ta e="T206" id="Seg_6429" s="T205">v-v:tense-v:pred.pn</ta>
            <ta e="T207" id="Seg_6430" s="T206">n-n:poss-n:case</ta>
            <ta e="T208" id="Seg_6431" s="T207">v-v:tense-v:pred.pn</ta>
            <ta e="T209" id="Seg_6432" s="T208">n-n:case</ta>
            <ta e="T210" id="Seg_6433" s="T209">v-v:tense-v:pred.pn</ta>
            <ta e="T211" id="Seg_6434" s="T210">interj</ta>
            <ta e="T212" id="Seg_6435" s="T211">ptcl</ta>
            <ta e="T213" id="Seg_6436" s="T212">adv</ta>
            <ta e="T214" id="Seg_6437" s="T213">n-n:case</ta>
            <ta e="T215" id="Seg_6438" s="T214">n-n:case</ta>
            <ta e="T216" id="Seg_6439" s="T215">n-n&gt;adj</ta>
            <ta e="T217" id="Seg_6440" s="T216">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T218" id="Seg_6441" s="T217">adv</ta>
            <ta e="T219" id="Seg_6442" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_6443" s="T219">adv</ta>
            <ta e="T221" id="Seg_6444" s="T220">v-v:tense-v:pred.pn</ta>
            <ta e="T222" id="Seg_6445" s="T221">ptcl</ta>
            <ta e="T223" id="Seg_6446" s="T222">n</ta>
            <ta e="T224" id="Seg_6447" s="T223">n</ta>
            <ta e="T225" id="Seg_6448" s="T224">n-n:poss-n:case</ta>
            <ta e="T226" id="Seg_6449" s="T225">dempro</ta>
            <ta e="T227" id="Seg_6450" s="T226">n-n:(poss).[n:case]</ta>
            <ta e="T233" id="Seg_6451" s="T232">cardnum</ta>
            <ta e="T234" id="Seg_6452" s="T233">cardnum</ta>
            <ta e="T235" id="Seg_6453" s="T234">cardnum</ta>
            <ta e="T236" id="Seg_6454" s="T235">cardnum</ta>
            <ta e="T237" id="Seg_6455" s="T236">cardnum</ta>
            <ta e="T238" id="Seg_6456" s="T237">cardnum</ta>
            <ta e="T239" id="Seg_6457" s="T238">n-n&gt;adj-n:poss-n:case</ta>
            <ta e="T240" id="Seg_6458" s="T239">n-n&gt;v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T241" id="Seg_6459" s="T240">ordnum</ta>
            <ta e="T242" id="Seg_6460" s="T241">n-n:(poss).[n:case]</ta>
            <ta e="T243" id="Seg_6461" s="T242">adv</ta>
            <ta e="T244" id="Seg_6462" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_6463" s="T244">adv</ta>
            <ta e="T246" id="Seg_6464" s="T245">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T247" id="Seg_6465" s="T246">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T248" id="Seg_6466" s="T247">n.[n:case]</ta>
            <ta e="T249" id="Seg_6467" s="T248">adj</ta>
            <ta e="T250" id="Seg_6468" s="T249">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T251" id="Seg_6469" s="T250">n.[n:case]</ta>
            <ta e="T252" id="Seg_6470" s="T251">adj</ta>
            <ta e="T254" id="Seg_6471" s="T252">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T259" id="Seg_6472" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_6473" s="T259">dempro</ta>
            <ta e="T261" id="Seg_6474" s="T260">n-n:case</ta>
            <ta e="T262" id="Seg_6475" s="T261">n-n:case</ta>
            <ta e="T263" id="Seg_6476" s="T262">v-v:cvb-v:pred.pn</ta>
            <ta e="T264" id="Seg_6477" s="T263">n-n:case</ta>
            <ta e="T265" id="Seg_6478" s="T264">v-v:tense-v:poss.pn</ta>
            <ta e="T266" id="Seg_6479" s="T265">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T267" id="Seg_6480" s="T266">n.[n:case]</ta>
            <ta e="T268" id="Seg_6481" s="T267">v-v:tense-v:poss.pn</ta>
            <ta e="T269" id="Seg_6482" s="T268">adv</ta>
            <ta e="T270" id="Seg_6483" s="T269">adj</ta>
            <ta e="T271" id="Seg_6484" s="T270">n.[n:case]</ta>
            <ta e="T272" id="Seg_6485" s="T271">v-v:tense-v:poss.pn</ta>
            <ta e="T273" id="Seg_6486" s="T272">adj</ta>
            <ta e="T274" id="Seg_6487" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_6488" s="T274">n.[n:case]</ta>
            <ta e="T276" id="Seg_6489" s="T275">n-n:case</ta>
            <ta e="T277" id="Seg_6490" s="T276">ptcl</ta>
            <ta e="T278" id="Seg_6491" s="T277">v-v:tense-v:poss.pn</ta>
            <ta e="T279" id="Seg_6492" s="T278">n.[n:case]</ta>
            <ta e="T280" id="Seg_6493" s="T279">conj</ta>
            <ta e="T281" id="Seg_6494" s="T280">v-v:tense-v:poss.pn</ta>
            <ta e="T282" id="Seg_6495" s="T281">n-n:case</ta>
            <ta e="T283" id="Seg_6496" s="T282">cardnum</ta>
            <ta e="T284" id="Seg_6497" s="T283">cardnum</ta>
            <ta e="T285" id="Seg_6498" s="T284">n-n:case</ta>
            <ta e="T286" id="Seg_6499" s="T285">v-v:tense-v:poss.pn</ta>
            <ta e="T287" id="Seg_6500" s="T286">adv</ta>
            <ta e="T288" id="Seg_6501" s="T287">cardnum</ta>
            <ta e="T289" id="Seg_6502" s="T288">n-n:case</ta>
            <ta e="T290" id="Seg_6503" s="T289">n-n:case</ta>
            <ta e="T291" id="Seg_6504" s="T290">v-v:tense-v:poss.pn</ta>
            <ta e="T292" id="Seg_6505" s="T291">adv</ta>
            <ta e="T293" id="Seg_6506" s="T292">n-n:(num)-n:case</ta>
            <ta e="T294" id="Seg_6507" s="T293">v-v:tense-v:poss.pn</ta>
            <ta e="T295" id="Seg_6508" s="T294">dempro</ta>
            <ta e="T296" id="Seg_6509" s="T295">v-v:tense.[v:pred.pn]</ta>
            <ta e="T297" id="Seg_6510" s="T296">v-v:ptcp</ta>
            <ta e="T298" id="Seg_6511" s="T297">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T299" id="Seg_6512" s="T298">adv</ta>
            <ta e="T300" id="Seg_6513" s="T299">cardnum</ta>
            <ta e="T301" id="Seg_6514" s="T300">cardnum</ta>
            <ta e="T302" id="Seg_6515" s="T301">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T303" id="Seg_6516" s="T302">adv</ta>
            <ta e="T304" id="Seg_6517" s="T303">adv</ta>
            <ta e="T305" id="Seg_6518" s="T304">cardnum</ta>
            <ta e="T672" id="Seg_6519" s="T305">ptcl</ta>
            <ta e="T306" id="Seg_6520" s="T672">n-n:case</ta>
            <ta e="T307" id="Seg_6521" s="T306">n-n:case</ta>
            <ta e="T308" id="Seg_6522" s="T307">v-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T309" id="Seg_6523" s="T308">n-n:(num)-n:poss-n:case</ta>
            <ta e="T310" id="Seg_6524" s="T309">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T311" id="Seg_6525" s="T310">v-v:ptcp.[v:(case)]</ta>
            <ta e="T312" id="Seg_6526" s="T311">v-v:cvb-v:pred.pn</ta>
            <ta e="T313" id="Seg_6527" s="T312">n-n:case</ta>
            <ta e="T314" id="Seg_6528" s="T313">v-v:(neg)-v:pred.pn</ta>
            <ta e="T315" id="Seg_6529" s="T314">v-v:tense-v:pred.pn</ta>
            <ta e="T316" id="Seg_6530" s="T315">n-n:(num)-n:poss-n:case</ta>
            <ta e="T317" id="Seg_6531" s="T316">dempro-pro:(num)-pro:(poss)-pro:case</ta>
            <ta e="T318" id="Seg_6532" s="T317">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T319" id="Seg_6533" s="T318">v-v:(ins)-v:ptcp-v:(ins)-v:(case)</ta>
            <ta e="T320" id="Seg_6534" s="T319">n-n&gt;adj</ta>
            <ta e="T321" id="Seg_6535" s="T320">n-n&gt;adj.[n:case]</ta>
            <ta e="T322" id="Seg_6536" s="T321">n-n&gt;v-v:(ins)-v&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T323" id="Seg_6537" s="T322">n-n:poss-n:case</ta>
            <ta e="T324" id="Seg_6538" s="T323">post</ta>
            <ta e="T325" id="Seg_6539" s="T324">adj.[n:case]</ta>
            <ta e="T326" id="Seg_6540" s="T325">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T327" id="Seg_6541" s="T326">post</ta>
            <ta e="T328" id="Seg_6542" s="T327">ptcl</ta>
            <ta e="T329" id="Seg_6543" s="T328">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T330" id="Seg_6544" s="T329">v-v:cvb-v:pred.pn</ta>
            <ta e="T331" id="Seg_6545" s="T330">v-v:cvb</ta>
            <ta e="T332" id="Seg_6546" s="T331">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T333" id="Seg_6547" s="T332">adv</ta>
            <ta e="T334" id="Seg_6548" s="T333">adv</ta>
            <ta e="T335" id="Seg_6549" s="T334">cardnum</ta>
            <ta e="T336" id="Seg_6550" s="T335">adj</ta>
            <ta e="T337" id="Seg_6551" s="T336">n-n:(poss).[n:case]</ta>
            <ta e="T338" id="Seg_6552" s="T337">n.[n:case]</ta>
            <ta e="T339" id="Seg_6553" s="T338">n-n:poss-n:case</ta>
            <ta e="T340" id="Seg_6554" s="T339">v-v:tense-v:poss.pn</ta>
            <ta e="T341" id="Seg_6555" s="T340">n-n:(poss).[n:case]</ta>
            <ta e="T342" id="Seg_6556" s="T341">n-n:(poss).[n:case]</ta>
            <ta e="T343" id="Seg_6557" s="T342">adv</ta>
            <ta e="T344" id="Seg_6558" s="T343">cardnum</ta>
            <ta e="T345" id="Seg_6559" s="T344">n-n:(poss).[n:case]</ta>
            <ta e="T346" id="Seg_6560" s="T345">v-v:cvb</ta>
            <ta e="T347" id="Seg_6561" s="T346">propr-n:case</ta>
            <ta e="T348" id="Seg_6562" s="T347">v-v:cvb</ta>
            <ta e="T349" id="Seg_6563" s="T348">v-v:tense-v:poss.pn</ta>
            <ta e="T350" id="Seg_6564" s="T349">adv</ta>
            <ta e="T351" id="Seg_6565" s="T350">n-n:case</ta>
            <ta e="T352" id="Seg_6566" s="T351">v-v:cvb</ta>
            <ta e="T353" id="Seg_6567" s="T352">v-v:tense.[v:pred.pn]</ta>
            <ta e="T354" id="Seg_6568" s="T353">propr-n:case</ta>
            <ta e="T355" id="Seg_6569" s="T354">adv</ta>
            <ta e="T356" id="Seg_6570" s="T355">cardnum</ta>
            <ta e="T357" id="Seg_6571" s="T356">cardnum</ta>
            <ta e="T358" id="Seg_6572" s="T357">n-n:(poss).[n:case]</ta>
            <ta e="T359" id="Seg_6573" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_6574" s="T359">ptcl</ta>
            <ta e="T361" id="Seg_6575" s="T360">v-v:cvb</ta>
            <ta e="T362" id="Seg_6576" s="T361">cardnum</ta>
            <ta e="T363" id="Seg_6577" s="T362">n-n:case</ta>
            <ta e="T364" id="Seg_6578" s="T363">v-v:tense-v:poss.pn</ta>
            <ta e="T365" id="Seg_6579" s="T364">adv</ta>
            <ta e="T366" id="Seg_6580" s="T365">propr-n:case</ta>
            <ta e="T367" id="Seg_6581" s="T366">ptcl</ta>
            <ta e="T369" id="Seg_6582" s="T368">propr.[n:case]</ta>
            <ta e="T370" id="Seg_6583" s="T369">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T371" id="Seg_6584" s="T370">n-n&gt;adj</ta>
            <ta e="T372" id="Seg_6585" s="T371">n.[n:case]</ta>
            <ta e="T373" id="Seg_6586" s="T372">dempro</ta>
            <ta e="T374" id="Seg_6587" s="T373">cardnum</ta>
            <ta e="T375" id="Seg_6588" s="T374">n-n&gt;adj.[n:case]</ta>
            <ta e="T376" id="Seg_6589" s="T375">n-n:(num)-n:case</ta>
            <ta e="T377" id="Seg_6590" s="T376">n-n&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T378" id="Seg_6591" s="T377">n-n&gt;adj-n:case</ta>
            <ta e="T379" id="Seg_6592" s="T378">adv</ta>
            <ta e="T380" id="Seg_6593" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_6594" s="T380">n-n:case</ta>
            <ta e="T382" id="Seg_6595" s="T381">n-n:case</ta>
            <ta e="T383" id="Seg_6596" s="T382">v-v:tense.[v:pred.pn]</ta>
            <ta e="T384" id="Seg_6597" s="T383">n-n:case</ta>
            <ta e="T385" id="Seg_6598" s="T384">v-v:cvb</ta>
            <ta e="T386" id="Seg_6599" s="T385">post</ta>
            <ta e="T387" id="Seg_6600" s="T386">adv</ta>
            <ta e="T388" id="Seg_6601" s="T387">n.[n:case]</ta>
            <ta e="T389" id="Seg_6602" s="T388">n-n:(poss).[n:case]</ta>
            <ta e="T390" id="Seg_6603" s="T389">v-v:tense-v:poss.pn</ta>
            <ta e="T391" id="Seg_6604" s="T390">adv</ta>
            <ta e="T392" id="Seg_6605" s="T391">cardnum</ta>
            <ta e="T393" id="Seg_6606" s="T392">n-n:(poss).[n:case]</ta>
            <ta e="T394" id="Seg_6607" s="T393">n-n:case</ta>
            <ta e="T395" id="Seg_6608" s="T394">propr-n:case</ta>
            <ta e="T396" id="Seg_6609" s="T395">v-v:tense.[v:pred.pn]</ta>
            <ta e="T397" id="Seg_6610" s="T396">adj</ta>
            <ta e="T398" id="Seg_6611" s="T397">n.[n:case]</ta>
            <ta e="T399" id="Seg_6612" s="T398">cardnum</ta>
            <ta e="T400" id="Seg_6613" s="T399">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T401" id="Seg_6614" s="T400">cardnum</ta>
            <ta e="T403" id="Seg_6615" s="T402">que.[pro:case]</ta>
            <ta e="T405" id="Seg_6616" s="T404">propr-n:case</ta>
            <ta e="T406" id="Seg_6617" s="T405">propr</ta>
            <ta e="T410" id="Seg_6618" s="T409">ptcl</ta>
            <ta e="T411" id="Seg_6619" s="T410">v-v:tense-v:poss.pn</ta>
            <ta e="T412" id="Seg_6620" s="T411">ptcl</ta>
            <ta e="T413" id="Seg_6621" s="T412">dempro.[pro:case]</ta>
            <ta e="T414" id="Seg_6622" s="T413">post</ta>
            <ta e="T415" id="Seg_6623" s="T414">propr-n:case</ta>
            <ta e="T416" id="Seg_6624" s="T415">v-v:tense-v:poss.pn</ta>
            <ta e="T417" id="Seg_6625" s="T416">adv</ta>
            <ta e="T419" id="Seg_6626" s="T418">n.[n:case]</ta>
            <ta e="T420" id="Seg_6627" s="T419">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T421" id="Seg_6628" s="T420">propr-n:case</ta>
            <ta e="T422" id="Seg_6629" s="T421">v-v:tense.[v:pred.pn]</ta>
            <ta e="T423" id="Seg_6630" s="T422">n-n:case</ta>
            <ta e="T424" id="Seg_6631" s="T423">v-v:tense.[v:pred.pn]</ta>
            <ta e="T425" id="Seg_6632" s="T424">adv</ta>
            <ta e="T426" id="Seg_6633" s="T425">cardnum</ta>
            <ta e="T427" id="Seg_6634" s="T426">n-n&gt;adj.[n:case]</ta>
            <ta e="T428" id="Seg_6635" s="T427">adj</ta>
            <ta e="T429" id="Seg_6636" s="T428">n.[n:case]</ta>
            <ta e="T430" id="Seg_6637" s="T429">n-n&gt;adj</ta>
            <ta e="T431" id="Seg_6638" s="T430">n-n:(poss).[n:case]</ta>
            <ta e="T432" id="Seg_6639" s="T431">adj-n:(poss).[n:case]</ta>
            <ta e="T433" id="Seg_6640" s="T432">n-n&gt;adj.[n:case]</ta>
            <ta e="T434" id="Seg_6641" s="T433">cardnum</ta>
            <ta e="T435" id="Seg_6642" s="T434">n-n:(poss).[n:case]</ta>
            <ta e="T436" id="Seg_6643" s="T435">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T437" id="Seg_6644" s="T436">adj-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T438" id="Seg_6645" s="T437">adv</ta>
            <ta e="T439" id="Seg_6646" s="T438">adj-n:(poss).[n:case]</ta>
            <ta e="T440" id="Seg_6647" s="T439">ptcl</ta>
            <ta e="T441" id="Seg_6648" s="T440">n-n&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T442" id="Seg_6649" s="T441">adj-n:(poss).[n:case]</ta>
            <ta e="T443" id="Seg_6650" s="T442">ptcl</ta>
            <ta e="T444" id="Seg_6651" s="T443">adv</ta>
            <ta e="T445" id="Seg_6652" s="T444">v-v:tense.[v:pred.pn]</ta>
            <ta e="T446" id="Seg_6653" s="T445">n-n&gt;adj</ta>
            <ta e="T447" id="Seg_6654" s="T446">propr.[n:case]</ta>
            <ta e="T448" id="Seg_6655" s="T447">n-n:(poss).[n:case]</ta>
            <ta e="T449" id="Seg_6656" s="T448">dempro-pro&gt;pro</ta>
            <ta e="T450" id="Seg_6657" s="T449">v-v:tense-v:poss.pn</ta>
            <ta e="T451" id="Seg_6658" s="T450">cardnum</ta>
            <ta e="T452" id="Seg_6659" s="T451">n-n&gt;adj.[n:case]</ta>
            <ta e="T453" id="Seg_6660" s="T452">cardnum-cardnum&gt;ordnum-n:(poss).[n:case]</ta>
            <ta e="T454" id="Seg_6661" s="T453">v-v:tense-v:poss.pn</ta>
            <ta e="T455" id="Seg_6662" s="T454">adj</ta>
            <ta e="T456" id="Seg_6663" s="T455">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T457" id="Seg_6664" s="T456">cardnum</ta>
            <ta e="T458" id="Seg_6665" s="T457">n-n&gt;adj.[n:case]</ta>
            <ta e="T459" id="Seg_6666" s="T458">n.[n:case]</ta>
            <ta e="T460" id="Seg_6667" s="T459">cardnum</ta>
            <ta e="T461" id="Seg_6668" s="T460">n-n:(poss).[n:case]</ta>
            <ta e="T462" id="Seg_6669" s="T461">n-n&gt;adj</ta>
            <ta e="T463" id="Seg_6670" s="T462">n-n&gt;adj.[n:case]</ta>
            <ta e="T464" id="Seg_6671" s="T463">adj-n:(poss).[n:case]</ta>
            <ta e="T465" id="Seg_6672" s="T464">n-n&gt;v-v:cvb</ta>
            <ta e="T467" id="Seg_6673" s="T465">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T471" id="Seg_6674" s="T469">ptcl</ta>
            <ta e="T472" id="Seg_6675" s="T471">adv</ta>
            <ta e="T473" id="Seg_6676" s="T472">cardnum</ta>
            <ta e="T474" id="Seg_6677" s="T473">n-n:(poss).[n:case]</ta>
            <ta e="T475" id="Seg_6678" s="T474">ptcl</ta>
            <ta e="T476" id="Seg_6679" s="T475">que-pro:case</ta>
            <ta e="T477" id="Seg_6680" s="T476">ptcl</ta>
            <ta e="T478" id="Seg_6681" s="T477">propr-n:case</ta>
            <ta e="T479" id="Seg_6682" s="T478">ptcl</ta>
            <ta e="T480" id="Seg_6683" s="T479">que-pro:case</ta>
            <ta e="T481" id="Seg_6684" s="T480">n-n:case</ta>
            <ta e="T482" id="Seg_6685" s="T481">v-v:tense.[v:pred.pn]</ta>
            <ta e="T483" id="Seg_6686" s="T482">adv</ta>
            <ta e="T484" id="Seg_6687" s="T483">cardnum</ta>
            <ta e="T485" id="Seg_6688" s="T484">n-n:(poss).[n:case]</ta>
            <ta e="T486" id="Seg_6689" s="T485">adv</ta>
            <ta e="T487" id="Seg_6690" s="T486">cardnum</ta>
            <ta e="T488" id="Seg_6691" s="T487">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T489" id="Seg_6692" s="T488">adv</ta>
            <ta e="T490" id="Seg_6693" s="T489">n-n&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T491" id="Seg_6694" s="T490">cardnum</ta>
            <ta e="T492" id="Seg_6695" s="T491">n-n:(poss).[n:case]</ta>
            <ta e="T494" id="Seg_6696" s="T492">ptcl</ta>
            <ta e="T497" id="Seg_6697" s="T496">ptcl</ta>
            <ta e="T498" id="Seg_6698" s="T497">n-n:case</ta>
            <ta e="T499" id="Seg_6699" s="T498">adv</ta>
            <ta e="T500" id="Seg_6700" s="T499">propr</ta>
            <ta e="T501" id="Seg_6701" s="T500">propr.[n:case]</ta>
            <ta e="T508" id="Seg_6702" s="T506">interj</ta>
            <ta e="T509" id="Seg_6703" s="T508">adv</ta>
            <ta e="T510" id="Seg_6704" s="T509">cardnum</ta>
            <ta e="T511" id="Seg_6705" s="T510">adj</ta>
            <ta e="T512" id="Seg_6706" s="T511">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T513" id="Seg_6707" s="T512">n-n:(poss).[n:case]</ta>
            <ta e="T514" id="Seg_6708" s="T513">ptcl</ta>
            <ta e="T515" id="Seg_6709" s="T514">adv</ta>
            <ta e="T516" id="Seg_6710" s="T515">n-n&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T517" id="Seg_6711" s="T516">propr</ta>
            <ta e="T518" id="Seg_6712" s="T517">propr</ta>
            <ta e="T519" id="Seg_6713" s="T518">ptcl</ta>
            <ta e="T521" id="Seg_6714" s="T520">adv</ta>
            <ta e="T522" id="Seg_6715" s="T521">cardnum</ta>
            <ta e="T523" id="Seg_6716" s="T522">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T524" id="Seg_6717" s="T523">ptcl</ta>
            <ta e="T525" id="Seg_6718" s="T524">que.[pro:case]</ta>
            <ta e="T526" id="Seg_6719" s="T525">n.[n:case]</ta>
            <ta e="T527" id="Seg_6720" s="T526">v-v:tense-v:poss.pn</ta>
            <ta e="T528" id="Seg_6721" s="T527">v-v:cvb</ta>
            <ta e="T529" id="Seg_6722" s="T528">v-v:ptcp</ta>
            <ta e="T530" id="Seg_6723" s="T529">v-v:tense-v:poss.pn</ta>
            <ta e="T531" id="Seg_6724" s="T530">n-n:(num).[n:case]</ta>
            <ta e="T532" id="Seg_6725" s="T531">adj-n:(poss).[n:case]</ta>
            <ta e="T533" id="Seg_6726" s="T532">n.[n:case]</ta>
            <ta e="T534" id="Seg_6727" s="T533">dempro</ta>
            <ta e="T535" id="Seg_6728" s="T534">v-v:cvb</ta>
            <ta e="T536" id="Seg_6729" s="T535">v-v:tense.[v:pred.pn]</ta>
            <ta e="T537" id="Seg_6730" s="T536">adv-n:case</ta>
            <ta e="T538" id="Seg_6731" s="T537">post</ta>
            <ta e="T539" id="Seg_6732" s="T538">v-v:cvb</ta>
            <ta e="T540" id="Seg_6733" s="T539">v-v:cvb</ta>
            <ta e="T541" id="Seg_6734" s="T540">post</ta>
            <ta e="T542" id="Seg_6735" s="T541">ptcl</ta>
            <ta e="T543" id="Seg_6736" s="T542">ptcl</ta>
            <ta e="T544" id="Seg_6737" s="T543">adj-n:(poss).[n:case]</ta>
            <ta e="T545" id="Seg_6738" s="T544">v-v:tense-v:poss.pn</ta>
            <ta e="T546" id="Seg_6739" s="T545">adv</ta>
            <ta e="T547" id="Seg_6740" s="T546">ptcl</ta>
            <ta e="T548" id="Seg_6741" s="T547">cardnum</ta>
            <ta e="T549" id="Seg_6742" s="T548">n-n:(poss).[n:case]</ta>
            <ta e="T550" id="Seg_6743" s="T549">propr-n:case</ta>
            <ta e="T551" id="Seg_6744" s="T550">ptcl</ta>
            <ta e="T553" id="Seg_6745" s="T552">interj</ta>
            <ta e="T554" id="Seg_6746" s="T553">n.[n:case]</ta>
            <ta e="T555" id="Seg_6747" s="T554">n-n:poss-n:case</ta>
            <ta e="T556" id="Seg_6748" s="T555">v-v:ptcp</ta>
            <ta e="T557" id="Seg_6749" s="T556">v-v:tense-v:poss.pn</ta>
            <ta e="T558" id="Seg_6750" s="T557">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T559" id="Seg_6751" s="T558">n.[n:case]</ta>
            <ta e="T560" id="Seg_6752" s="T559">adj</ta>
            <ta e="T561" id="Seg_6753" s="T560">n-n:case</ta>
            <ta e="T562" id="Seg_6754" s="T561">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T563" id="Seg_6755" s="T562">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T564" id="Seg_6756" s="T563">post</ta>
            <ta e="T565" id="Seg_6757" s="T564">propr-n:case</ta>
            <ta e="T566" id="Seg_6758" s="T565">n-n&gt;adj.[n:case]</ta>
            <ta e="T567" id="Seg_6759" s="T566">propr.[n:case]</ta>
            <ta e="T568" id="Seg_6760" s="T567">v-v:tense-v:poss.pn</ta>
            <ta e="T569" id="Seg_6761" s="T568">ptcl</ta>
            <ta e="T570" id="Seg_6762" s="T569">ptcl</ta>
            <ta e="T571" id="Seg_6763" s="T570">ptcl</ta>
            <ta e="T572" id="Seg_6764" s="T571">adv</ta>
            <ta e="T573" id="Seg_6765" s="T572">adj-n:(poss).[n:case]</ta>
            <ta e="T574" id="Seg_6766" s="T573">n-n:(num).[n:case]</ta>
            <ta e="T575" id="Seg_6767" s="T574">cardnum</ta>
            <ta e="T576" id="Seg_6768" s="T575">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T577" id="Seg_6769" s="T576">n-n:(num).[n:case]</ta>
            <ta e="T578" id="Seg_6770" s="T577">ptcl</ta>
            <ta e="T579" id="Seg_6771" s="T578">propr.[n:case]</ta>
            <ta e="T580" id="Seg_6772" s="T579">propr.[n:case]</ta>
            <ta e="T581" id="Seg_6773" s="T580">propr.[n:case]</ta>
            <ta e="T582" id="Seg_6774" s="T581">propr.[n:case]</ta>
            <ta e="T583" id="Seg_6775" s="T582">propr.[n:case]</ta>
            <ta e="T584" id="Seg_6776" s="T583">v-v:cvb</ta>
            <ta e="T585" id="Seg_6777" s="T584">v-v:tense.[v:pred.pn]</ta>
            <ta e="T586" id="Seg_6778" s="T585">n-n:(poss).[n:case]</ta>
            <ta e="T587" id="Seg_6779" s="T586">dempro</ta>
            <ta e="T588" id="Seg_6780" s="T587">v-v:tense.[v:pred.pn]</ta>
            <ta e="T589" id="Seg_6781" s="T588">adj-n:(poss).[n:case]</ta>
            <ta e="T590" id="Seg_6782" s="T589">ptcl</ta>
            <ta e="T591" id="Seg_6783" s="T590">adv</ta>
            <ta e="T592" id="Seg_6784" s="T591">cardnum</ta>
            <ta e="T593" id="Seg_6785" s="T592">cardnum</ta>
            <ta e="T594" id="Seg_6786" s="T593">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T595" id="Seg_6787" s="T594">cardnum</ta>
            <ta e="T596" id="Seg_6788" s="T595">n-n:(poss).[n:case]</ta>
            <ta e="T597" id="Seg_6789" s="T596">cardnum</ta>
            <ta e="T598" id="Seg_6790" s="T597">interj</ta>
            <ta e="T599" id="Seg_6791" s="T598">cardnum</ta>
            <ta e="T600" id="Seg_6792" s="T599">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T601" id="Seg_6793" s="T600">adv</ta>
            <ta e="T602" id="Seg_6794" s="T601">n-n:(poss).[n:case]</ta>
            <ta e="T603" id="Seg_6795" s="T602">adv</ta>
            <ta e="T604" id="Seg_6796" s="T603">cardnum-cardnum-n:case</ta>
            <ta e="T605" id="Seg_6797" s="T604">post</ta>
            <ta e="T606" id="Seg_6798" s="T605">n.[n:case]</ta>
            <ta e="T607" id="Seg_6799" s="T606">n-n:(num)-n:poss-n:case</ta>
            <ta e="T608" id="Seg_6800" s="T607">adj-n:poss-n:case</ta>
            <ta e="T609" id="Seg_6801" s="T608">v-v:mood-v:temp.pn</ta>
            <ta e="T610" id="Seg_6802" s="T609">quant-n:(pred.pn)</ta>
            <ta e="T611" id="Seg_6803" s="T610">v-v:mood-v:temp.pn</ta>
            <ta e="T612" id="Seg_6804" s="T611">cardnum</ta>
            <ta e="T613" id="Seg_6805" s="T612">n.[n:case]</ta>
            <ta e="T614" id="Seg_6806" s="T613">cardnum</ta>
            <ta e="T615" id="Seg_6807" s="T614">n.[n:case]</ta>
            <ta e="T616" id="Seg_6808" s="T615">n-n:(num).[n:case]</ta>
            <ta e="T617" id="Seg_6809" s="T616">n-n:(num)-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T618" id="Seg_6810" s="T617">adj-n:(poss).[n:case]</ta>
            <ta e="T619" id="Seg_6811" s="T618">n-n:(poss).[n:case]</ta>
            <ta e="T620" id="Seg_6812" s="T619">v-v:cvb</ta>
            <ta e="T621" id="Seg_6813" s="T620">v-v:tense-v:poss.pn</ta>
            <ta e="T622" id="Seg_6814" s="T621">cardnum</ta>
            <ta e="T623" id="Seg_6815" s="T622">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T624" id="Seg_6816" s="T623">n-n:case</ta>
            <ta e="T625" id="Seg_6817" s="T624">que-pro:(poss).[pro:case]</ta>
            <ta e="T626" id="Seg_6818" s="T625">que.[pro:case]</ta>
            <ta e="T627" id="Seg_6819" s="T626">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T628" id="Seg_6820" s="T627">ptcl</ta>
            <ta e="T629" id="Seg_6821" s="T628">n</ta>
            <ta e="T630" id="Seg_6822" s="T629">adj-n:(num).[n:case]</ta>
            <ta e="T631" id="Seg_6823" s="T630">cardnum</ta>
            <ta e="T632" id="Seg_6824" s="T631">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T633" id="Seg_6825" s="T632">n-n:(num).[n:case]</ta>
            <ta e="T634" id="Seg_6826" s="T633">n-n:(num).[n:case]</ta>
            <ta e="T635" id="Seg_6827" s="T634">adj-n:(poss).[n:case]</ta>
            <ta e="T636" id="Seg_6828" s="T635">n-n:(num).[n:case]</ta>
            <ta e="T637" id="Seg_6829" s="T636">adv</ta>
            <ta e="T638" id="Seg_6830" s="T637">cardnum</ta>
            <ta e="T639" id="Seg_6831" s="T638">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T640" id="Seg_6832" s="T639">dempro</ta>
            <ta e="T641" id="Seg_6833" s="T640">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T642" id="Seg_6834" s="T641">cardnum</ta>
            <ta e="T643" id="Seg_6835" s="T642">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T644" id="Seg_6836" s="T643">adj.[n:case]</ta>
            <ta e="T645" id="Seg_6837" s="T644">v-v:tense.[v:pred.pn]</ta>
            <ta e="T646" id="Seg_6838" s="T645">n.[n:case]</ta>
            <ta e="T647" id="Seg_6839" s="T646">propr</ta>
            <ta e="T648" id="Seg_6840" s="T647">propr.[n:case]</ta>
            <ta e="T649" id="Seg_6841" s="T648">adj-n:(poss).[n:case]</ta>
            <ta e="T650" id="Seg_6842" s="T649">dempro.[pro:case]</ta>
            <ta e="T651" id="Seg_6843" s="T650">post</ta>
            <ta e="T652" id="Seg_6844" s="T651">n-n:case</ta>
            <ta e="T653" id="Seg_6845" s="T652">adv</ta>
            <ta e="T654" id="Seg_6846" s="T653">v-v:tense.[v:pred.pn]</ta>
            <ta e="T655" id="Seg_6847" s="T654">n-n:(poss)</ta>
            <ta e="T656" id="Seg_6848" s="T655">ptcl</ta>
            <ta e="T657" id="Seg_6849" s="T656">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T658" id="Seg_6850" s="T657">cardnum</ta>
            <ta e="T659" id="Seg_6851" s="T658">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T660" id="Seg_6852" s="T659">propr-n:case</ta>
            <ta e="T661" id="Seg_6853" s="T660">n.[n:case]</ta>
            <ta e="T662" id="Seg_6854" s="T661">n-n:(poss).[n:case]</ta>
            <ta e="T663" id="Seg_6855" s="T662">v-v:cvb</ta>
            <ta e="T664" id="Seg_6856" s="T663">v-v:tense.[v:pred.pn]</ta>
            <ta e="T665" id="Seg_6857" s="T664">ptcl</ta>
            <ta e="T666" id="Seg_6858" s="T665">que-pro:case</ta>
            <ta e="T667" id="Seg_6859" s="T666">v-v:ptcp-v:(poss)-v:(case)=ptcl</ta>
            <ta e="T671" id="Seg_6860" s="T670">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps-KiPP">
            <ta e="T2" id="Seg_6861" s="T1">dempro</ta>
            <ta e="T3" id="Seg_6862" s="T2">v</ta>
            <ta e="T4" id="Seg_6863" s="T3">ptcl</ta>
            <ta e="T9" id="Seg_6864" s="T6">ptcl</ta>
            <ta e="T15" id="Seg_6865" s="T14">ptcl</ta>
            <ta e="T17" id="Seg_6866" s="T15">que</ta>
            <ta e="T18" id="Seg_6867" s="T17">v</ta>
            <ta e="T19" id="Seg_6868" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_6869" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_6870" s="T20">adv</ta>
            <ta e="T22" id="Seg_6871" s="T21">n</ta>
            <ta e="T24" id="Seg_6872" s="T22">v</ta>
            <ta e="T25" id="Seg_6873" s="T24">ptcl</ta>
            <ta e="T30" id="Seg_6874" s="T29">v</ta>
            <ta e="T33" id="Seg_6875" s="T31">interj</ta>
            <ta e="T51" id="Seg_6876" s="T49">interj</ta>
            <ta e="T52" id="Seg_6877" s="T51">que</ta>
            <ta e="T53" id="Seg_6878" s="T52">v</ta>
            <ta e="T54" id="Seg_6879" s="T53">dempro</ta>
            <ta e="T55" id="Seg_6880" s="T54">n</ta>
            <ta e="T56" id="Seg_6881" s="T55">ptcl</ta>
            <ta e="T57" id="Seg_6882" s="T56">post</ta>
            <ta e="T68" id="Seg_6883" s="T66">propr</ta>
            <ta e="T72" id="Seg_6884" s="T70">propr</ta>
            <ta e="T73" id="Seg_6885" s="T72">v</ta>
            <ta e="T74" id="Seg_6886" s="T73">adv</ta>
            <ta e="T75" id="Seg_6887" s="T74">emphpro</ta>
            <ta e="T76" id="Seg_6888" s="T75">n</ta>
            <ta e="T77" id="Seg_6889" s="T76">n</ta>
            <ta e="T78" id="Seg_6890" s="T77">v</ta>
            <ta e="T79" id="Seg_6891" s="T78">adj</ta>
            <ta e="T80" id="Seg_6892" s="T79">n</ta>
            <ta e="T107" id="Seg_6893" s="T106">n</ta>
            <ta e="T108" id="Seg_6894" s="T107">ptcl</ta>
            <ta e="T112" id="Seg_6895" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_6896" s="T112">pers</ta>
            <ta e="T115" id="Seg_6897" s="T113">n</ta>
            <ta e="T117" id="Seg_6898" s="T115">v</ta>
            <ta e="T121" id="Seg_6899" s="T120">n</ta>
            <ta e="T122" id="Seg_6900" s="T121">v</ta>
            <ta e="T123" id="Seg_6901" s="T122">n</ta>
            <ta e="T124" id="Seg_6902" s="T123">n</ta>
            <ta e="T129" id="Seg_6903" s="T128">adv</ta>
            <ta e="T130" id="Seg_6904" s="T129">pers</ta>
            <ta e="T131" id="Seg_6905" s="T130">n</ta>
            <ta e="T132" id="Seg_6906" s="T131">adv</ta>
            <ta e="T133" id="Seg_6907" s="T132">v</ta>
            <ta e="T134" id="Seg_6908" s="T133">n</ta>
            <ta e="T135" id="Seg_6909" s="T134">v</ta>
            <ta e="T136" id="Seg_6910" s="T135">n</ta>
            <ta e="T137" id="Seg_6911" s="T136">post</ta>
            <ta e="T138" id="Seg_6912" s="T137">n</ta>
            <ta e="T139" id="Seg_6913" s="T138">post</ta>
            <ta e="T140" id="Seg_6914" s="T139">adv</ta>
            <ta e="T142" id="Seg_6915" s="T141">propr</ta>
            <ta e="T143" id="Seg_6916" s="T142">post</ta>
            <ta e="T144" id="Seg_6917" s="T143">v</ta>
            <ta e="T145" id="Seg_6918" s="T144">adv</ta>
            <ta e="T146" id="Seg_6919" s="T145">dempro</ta>
            <ta e="T147" id="Seg_6920" s="T146">v</ta>
            <ta e="T149" id="Seg_6921" s="T148">n</ta>
            <ta e="T150" id="Seg_6922" s="T149">n</ta>
            <ta e="T151" id="Seg_6923" s="T150">post</ta>
            <ta e="T152" id="Seg_6924" s="T151">n</ta>
            <ta e="T153" id="Seg_6925" s="T152">v</ta>
            <ta e="T154" id="Seg_6926" s="T153">dempro</ta>
            <ta e="T155" id="Seg_6927" s="T154">n</ta>
            <ta e="T156" id="Seg_6928" s="T155">n</ta>
            <ta e="T157" id="Seg_6929" s="T156">ptcl</ta>
            <ta e="T158" id="Seg_6930" s="T157">cardnum</ta>
            <ta e="T159" id="Seg_6931" s="T158">ptcl</ta>
            <ta e="T160" id="Seg_6932" s="T159">adv</ta>
            <ta e="T161" id="Seg_6933" s="T160">adv</ta>
            <ta e="T162" id="Seg_6934" s="T161">v</ta>
            <ta e="T163" id="Seg_6935" s="T162">n</ta>
            <ta e="T164" id="Seg_6936" s="T163">n</ta>
            <ta e="T166" id="Seg_6937" s="T165">n</ta>
            <ta e="T167" id="Seg_6938" s="T166">v</ta>
            <ta e="T168" id="Seg_6939" s="T167">v</ta>
            <ta e="T169" id="Seg_6940" s="T168">post</ta>
            <ta e="T170" id="Seg_6941" s="T169">v</ta>
            <ta e="T171" id="Seg_6942" s="T170">dempro</ta>
            <ta e="T172" id="Seg_6943" s="T171">n</ta>
            <ta e="T173" id="Seg_6944" s="T172">adv</ta>
            <ta e="T174" id="Seg_6945" s="T173">adv</ta>
            <ta e="T175" id="Seg_6946" s="T174">v</ta>
            <ta e="T176" id="Seg_6947" s="T175">dempro</ta>
            <ta e="T177" id="Seg_6948" s="T176">v</ta>
            <ta e="T178" id="Seg_6949" s="T177">post</ta>
            <ta e="T179" id="Seg_6950" s="T178">cardnum</ta>
            <ta e="T180" id="Seg_6951" s="T179">cardnum</ta>
            <ta e="T181" id="Seg_6952" s="T180">adj</ta>
            <ta e="T182" id="Seg_6953" s="T181">pers</ta>
            <ta e="T183" id="Seg_6954" s="T182">n</ta>
            <ta e="T184" id="Seg_6955" s="T183">v</ta>
            <ta e="T185" id="Seg_6956" s="T184">pers</ta>
            <ta e="T186" id="Seg_6957" s="T185">v</ta>
            <ta e="T187" id="Seg_6958" s="T186">pers</ta>
            <ta e="T188" id="Seg_6959" s="T187">n</ta>
            <ta e="T189" id="Seg_6960" s="T188">v</ta>
            <ta e="T190" id="Seg_6961" s="T189">v</ta>
            <ta e="T191" id="Seg_6962" s="T190">n</ta>
            <ta e="T192" id="Seg_6963" s="T191">que</ta>
            <ta e="T193" id="Seg_6964" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_6965" s="T193">adj</ta>
            <ta e="T195" id="Seg_6966" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_6967" s="T195">v</ta>
            <ta e="T197" id="Seg_6968" s="T196">ptcl</ta>
            <ta e="T198" id="Seg_6969" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_6970" s="T198">n</ta>
            <ta e="T200" id="Seg_6971" s="T199">v</ta>
            <ta e="T201" id="Seg_6972" s="T200">v</ta>
            <ta e="T202" id="Seg_6973" s="T201">n</ta>
            <ta e="T203" id="Seg_6974" s="T202">v</ta>
            <ta e="T204" id="Seg_6975" s="T203">v</ta>
            <ta e="T205" id="Seg_6976" s="T204">v</ta>
            <ta e="T206" id="Seg_6977" s="T205">v</ta>
            <ta e="T207" id="Seg_6978" s="T206">n</ta>
            <ta e="T208" id="Seg_6979" s="T207">v</ta>
            <ta e="T209" id="Seg_6980" s="T208">n</ta>
            <ta e="T210" id="Seg_6981" s="T209">v</ta>
            <ta e="T211" id="Seg_6982" s="T210">interj</ta>
            <ta e="T212" id="Seg_6983" s="T211">ptcl</ta>
            <ta e="T213" id="Seg_6984" s="T212">adv</ta>
            <ta e="T214" id="Seg_6985" s="T213">n</ta>
            <ta e="T215" id="Seg_6986" s="T214">n</ta>
            <ta e="T216" id="Seg_6987" s="T215">adj</ta>
            <ta e="T217" id="Seg_6988" s="T216">v</ta>
            <ta e="T218" id="Seg_6989" s="T217">adv</ta>
            <ta e="T219" id="Seg_6990" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_6991" s="T219">adv</ta>
            <ta e="T221" id="Seg_6992" s="T220">v</ta>
            <ta e="T222" id="Seg_6993" s="T221">ptcl</ta>
            <ta e="T223" id="Seg_6994" s="T222">n</ta>
            <ta e="T224" id="Seg_6995" s="T223">n</ta>
            <ta e="T225" id="Seg_6996" s="T224">n</ta>
            <ta e="T226" id="Seg_6997" s="T225">dempro</ta>
            <ta e="T227" id="Seg_6998" s="T226">n</ta>
            <ta e="T233" id="Seg_6999" s="T232">cardnum</ta>
            <ta e="T234" id="Seg_7000" s="T233">cardnum</ta>
            <ta e="T235" id="Seg_7001" s="T234">cardnum</ta>
            <ta e="T236" id="Seg_7002" s="T235">cardnum</ta>
            <ta e="T237" id="Seg_7003" s="T236">cardnum</ta>
            <ta e="T238" id="Seg_7004" s="T237">cardnum</ta>
            <ta e="T239" id="Seg_7005" s="T238">adj</ta>
            <ta e="T240" id="Seg_7006" s="T239">v</ta>
            <ta e="T241" id="Seg_7007" s="T240">ordnum</ta>
            <ta e="T242" id="Seg_7008" s="T241">n</ta>
            <ta e="T243" id="Seg_7009" s="T242">adv</ta>
            <ta e="T244" id="Seg_7010" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_7011" s="T244">adv</ta>
            <ta e="T246" id="Seg_7012" s="T245">v</ta>
            <ta e="T247" id="Seg_7013" s="T246">v</ta>
            <ta e="T248" id="Seg_7014" s="T247">n</ta>
            <ta e="T249" id="Seg_7015" s="T248">adj</ta>
            <ta e="T250" id="Seg_7016" s="T249">v</ta>
            <ta e="T251" id="Seg_7017" s="T250">n</ta>
            <ta e="T252" id="Seg_7018" s="T251">adj</ta>
            <ta e="T254" id="Seg_7019" s="T252">v</ta>
            <ta e="T259" id="Seg_7020" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_7021" s="T259">dempro</ta>
            <ta e="T261" id="Seg_7022" s="T260">n</ta>
            <ta e="T262" id="Seg_7023" s="T261">n</ta>
            <ta e="T263" id="Seg_7024" s="T262">v</ta>
            <ta e="T264" id="Seg_7025" s="T263">n</ta>
            <ta e="T265" id="Seg_7026" s="T264">cop</ta>
            <ta e="T266" id="Seg_7027" s="T265">n</ta>
            <ta e="T267" id="Seg_7028" s="T266">n</ta>
            <ta e="T268" id="Seg_7029" s="T267">cop</ta>
            <ta e="T269" id="Seg_7030" s="T268">adv</ta>
            <ta e="T270" id="Seg_7031" s="T269">adj</ta>
            <ta e="T271" id="Seg_7032" s="T270">n</ta>
            <ta e="T272" id="Seg_7033" s="T271">cop</ta>
            <ta e="T273" id="Seg_7034" s="T272">adj</ta>
            <ta e="T274" id="Seg_7035" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_7036" s="T274">n</ta>
            <ta e="T276" id="Seg_7037" s="T275">n</ta>
            <ta e="T277" id="Seg_7038" s="T276">ptcl</ta>
            <ta e="T278" id="Seg_7039" s="T277">v</ta>
            <ta e="T279" id="Seg_7040" s="T278">n</ta>
            <ta e="T280" id="Seg_7041" s="T279">conj</ta>
            <ta e="T281" id="Seg_7042" s="T280">v</ta>
            <ta e="T282" id="Seg_7043" s="T281">n</ta>
            <ta e="T283" id="Seg_7044" s="T282">cardnum</ta>
            <ta e="T284" id="Seg_7045" s="T283">cardnum</ta>
            <ta e="T285" id="Seg_7046" s="T284">n</ta>
            <ta e="T286" id="Seg_7047" s="T285">v</ta>
            <ta e="T287" id="Seg_7048" s="T286">adv</ta>
            <ta e="T288" id="Seg_7049" s="T287">cardnum</ta>
            <ta e="T289" id="Seg_7050" s="T288">n</ta>
            <ta e="T290" id="Seg_7051" s="T289">n</ta>
            <ta e="T291" id="Seg_7052" s="T290">v</ta>
            <ta e="T292" id="Seg_7053" s="T291">adv</ta>
            <ta e="T293" id="Seg_7054" s="T292">n</ta>
            <ta e="T294" id="Seg_7055" s="T293">v</ta>
            <ta e="T295" id="Seg_7056" s="T294">dempro</ta>
            <ta e="T296" id="Seg_7057" s="T295">v</ta>
            <ta e="T297" id="Seg_7058" s="T296">v</ta>
            <ta e="T298" id="Seg_7059" s="T297">n</ta>
            <ta e="T299" id="Seg_7060" s="T298">adv</ta>
            <ta e="T300" id="Seg_7061" s="T299">cardnum</ta>
            <ta e="T301" id="Seg_7062" s="T300">cardnum</ta>
            <ta e="T302" id="Seg_7063" s="T301">adj</ta>
            <ta e="T303" id="Seg_7064" s="T302">adv</ta>
            <ta e="T304" id="Seg_7065" s="T303">adv</ta>
            <ta e="T305" id="Seg_7066" s="T304">cardnum</ta>
            <ta e="T672" id="Seg_7067" s="T305">ptcl</ta>
            <ta e="T306" id="Seg_7068" s="T672">n</ta>
            <ta e="T307" id="Seg_7069" s="T306">n</ta>
            <ta e="T308" id="Seg_7070" s="T307">v</ta>
            <ta e="T309" id="Seg_7071" s="T308">n</ta>
            <ta e="T310" id="Seg_7072" s="T309">emphpro</ta>
            <ta e="T311" id="Seg_7073" s="T310">n</ta>
            <ta e="T312" id="Seg_7074" s="T311">cop</ta>
            <ta e="T313" id="Seg_7075" s="T312">n</ta>
            <ta e="T314" id="Seg_7076" s="T313">v</ta>
            <ta e="T315" id="Seg_7077" s="T314">v</ta>
            <ta e="T316" id="Seg_7078" s="T315">n</ta>
            <ta e="T317" id="Seg_7079" s="T316">dempro</ta>
            <ta e="T318" id="Seg_7080" s="T317">v</ta>
            <ta e="T319" id="Seg_7081" s="T318">v</ta>
            <ta e="T320" id="Seg_7082" s="T319">adj</ta>
            <ta e="T321" id="Seg_7083" s="T320">adj</ta>
            <ta e="T322" id="Seg_7084" s="T321">v</ta>
            <ta e="T323" id="Seg_7085" s="T322">n</ta>
            <ta e="T324" id="Seg_7086" s="T323">post</ta>
            <ta e="T325" id="Seg_7087" s="T324">adj</ta>
            <ta e="T326" id="Seg_7088" s="T325">cop</ta>
            <ta e="T327" id="Seg_7089" s="T326">post</ta>
            <ta e="T328" id="Seg_7090" s="T327">ptcl</ta>
            <ta e="T329" id="Seg_7091" s="T328">n</ta>
            <ta e="T330" id="Seg_7092" s="T329">v</ta>
            <ta e="T331" id="Seg_7093" s="T330">v</ta>
            <ta e="T332" id="Seg_7094" s="T331">v</ta>
            <ta e="T333" id="Seg_7095" s="T332">adv</ta>
            <ta e="T334" id="Seg_7096" s="T333">adv</ta>
            <ta e="T335" id="Seg_7097" s="T334">cardnum</ta>
            <ta e="T336" id="Seg_7098" s="T335">adj</ta>
            <ta e="T337" id="Seg_7099" s="T336">n</ta>
            <ta e="T338" id="Seg_7100" s="T337">n</ta>
            <ta e="T339" id="Seg_7101" s="T338">n</ta>
            <ta e="T340" id="Seg_7102" s="T339">v</ta>
            <ta e="T341" id="Seg_7103" s="T340">n</ta>
            <ta e="T342" id="Seg_7104" s="T341">n</ta>
            <ta e="T343" id="Seg_7105" s="T342">adv</ta>
            <ta e="T344" id="Seg_7106" s="T343">cardnum</ta>
            <ta e="T345" id="Seg_7107" s="T344">n</ta>
            <ta e="T346" id="Seg_7108" s="T345">v</ta>
            <ta e="T347" id="Seg_7109" s="T346">propr</ta>
            <ta e="T348" id="Seg_7110" s="T347">v</ta>
            <ta e="T349" id="Seg_7111" s="T348">v</ta>
            <ta e="T350" id="Seg_7112" s="T349">adv</ta>
            <ta e="T351" id="Seg_7113" s="T350">n</ta>
            <ta e="T352" id="Seg_7114" s="T351">v</ta>
            <ta e="T353" id="Seg_7115" s="T352">aux</ta>
            <ta e="T354" id="Seg_7116" s="T353">propr</ta>
            <ta e="T355" id="Seg_7117" s="T354">adv</ta>
            <ta e="T356" id="Seg_7118" s="T355">cardnum</ta>
            <ta e="T357" id="Seg_7119" s="T356">cardnum</ta>
            <ta e="T358" id="Seg_7120" s="T357">n</ta>
            <ta e="T359" id="Seg_7121" s="T358">ptcl</ta>
            <ta e="T360" id="Seg_7122" s="T359">ptcl</ta>
            <ta e="T361" id="Seg_7123" s="T360">v</ta>
            <ta e="T362" id="Seg_7124" s="T361">cardnum</ta>
            <ta e="T363" id="Seg_7125" s="T362">n</ta>
            <ta e="T364" id="Seg_7126" s="T363">v</ta>
            <ta e="T365" id="Seg_7127" s="T364">adv</ta>
            <ta e="T366" id="Seg_7128" s="T365">propr</ta>
            <ta e="T367" id="Seg_7129" s="T366">ptcl</ta>
            <ta e="T369" id="Seg_7130" s="T368">propr</ta>
            <ta e="T370" id="Seg_7131" s="T369">dempro</ta>
            <ta e="T371" id="Seg_7132" s="T370">adj</ta>
            <ta e="T372" id="Seg_7133" s="T371">n</ta>
            <ta e="T373" id="Seg_7134" s="T372">dempro</ta>
            <ta e="T374" id="Seg_7135" s="T373">cardnum</ta>
            <ta e="T375" id="Seg_7136" s="T374">adj</ta>
            <ta e="T376" id="Seg_7137" s="T375">n</ta>
            <ta e="T377" id="Seg_7138" s="T376">v</ta>
            <ta e="T378" id="Seg_7139" s="T377">adj</ta>
            <ta e="T379" id="Seg_7140" s="T378">adv</ta>
            <ta e="T380" id="Seg_7141" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_7142" s="T380">n</ta>
            <ta e="T382" id="Seg_7143" s="T381">n</ta>
            <ta e="T383" id="Seg_7144" s="T382">v</ta>
            <ta e="T384" id="Seg_7145" s="T383">n</ta>
            <ta e="T385" id="Seg_7146" s="T384">v</ta>
            <ta e="T386" id="Seg_7147" s="T385">post</ta>
            <ta e="T387" id="Seg_7148" s="T386">adv</ta>
            <ta e="T388" id="Seg_7149" s="T387">n</ta>
            <ta e="T389" id="Seg_7150" s="T388">n</ta>
            <ta e="T390" id="Seg_7151" s="T389">cop</ta>
            <ta e="T391" id="Seg_7152" s="T390">adv</ta>
            <ta e="T392" id="Seg_7153" s="T391">cardnum</ta>
            <ta e="T393" id="Seg_7154" s="T392">n</ta>
            <ta e="T394" id="Seg_7155" s="T393">n</ta>
            <ta e="T395" id="Seg_7156" s="T394">propr</ta>
            <ta e="T396" id="Seg_7157" s="T395">v</ta>
            <ta e="T397" id="Seg_7158" s="T396">adj</ta>
            <ta e="T398" id="Seg_7159" s="T397">n</ta>
            <ta e="T399" id="Seg_7160" s="T398">cardnum</ta>
            <ta e="T400" id="Seg_7161" s="T399">n</ta>
            <ta e="T401" id="Seg_7162" s="T400">cardnum</ta>
            <ta e="T403" id="Seg_7163" s="T402">que</ta>
            <ta e="T405" id="Seg_7164" s="T404">propr</ta>
            <ta e="T406" id="Seg_7165" s="T405">propr</ta>
            <ta e="T410" id="Seg_7166" s="T409">ptcl</ta>
            <ta e="T411" id="Seg_7167" s="T410">v</ta>
            <ta e="T412" id="Seg_7168" s="T411">ptcl</ta>
            <ta e="T413" id="Seg_7169" s="T412">dempro</ta>
            <ta e="T414" id="Seg_7170" s="T413">post</ta>
            <ta e="T415" id="Seg_7171" s="T414">propr</ta>
            <ta e="T416" id="Seg_7172" s="T415">v</ta>
            <ta e="T417" id="Seg_7173" s="T416">adv</ta>
            <ta e="T419" id="Seg_7174" s="T418">n</ta>
            <ta e="T420" id="Seg_7175" s="T419">n</ta>
            <ta e="T421" id="Seg_7176" s="T420">propr</ta>
            <ta e="T422" id="Seg_7177" s="T421">v</ta>
            <ta e="T423" id="Seg_7178" s="T422">n</ta>
            <ta e="T424" id="Seg_7179" s="T423">v</ta>
            <ta e="T425" id="Seg_7180" s="T424">adv</ta>
            <ta e="T426" id="Seg_7181" s="T425">cardnum</ta>
            <ta e="T427" id="Seg_7182" s="T426">adj</ta>
            <ta e="T428" id="Seg_7183" s="T427">adj</ta>
            <ta e="T429" id="Seg_7184" s="T428">n</ta>
            <ta e="T430" id="Seg_7185" s="T429">adj</ta>
            <ta e="T431" id="Seg_7186" s="T430">n</ta>
            <ta e="T432" id="Seg_7187" s="T431">adj</ta>
            <ta e="T433" id="Seg_7188" s="T432">adj</ta>
            <ta e="T434" id="Seg_7189" s="T433">cardnum</ta>
            <ta e="T435" id="Seg_7190" s="T434">n</ta>
            <ta e="T436" id="Seg_7191" s="T435">adj</ta>
            <ta e="T437" id="Seg_7192" s="T436">n</ta>
            <ta e="T438" id="Seg_7193" s="T437">adv</ta>
            <ta e="T439" id="Seg_7194" s="T438">adj</ta>
            <ta e="T440" id="Seg_7195" s="T439">ptcl</ta>
            <ta e="T441" id="Seg_7196" s="T440">v</ta>
            <ta e="T442" id="Seg_7197" s="T441">adj</ta>
            <ta e="T443" id="Seg_7198" s="T442">ptcl</ta>
            <ta e="T444" id="Seg_7199" s="T443">adv</ta>
            <ta e="T445" id="Seg_7200" s="T444">v</ta>
            <ta e="T446" id="Seg_7201" s="T445">adj</ta>
            <ta e="T447" id="Seg_7202" s="T446">propr</ta>
            <ta e="T448" id="Seg_7203" s="T447">n</ta>
            <ta e="T449" id="Seg_7204" s="T448">dempro</ta>
            <ta e="T450" id="Seg_7205" s="T449">v</ta>
            <ta e="T451" id="Seg_7206" s="T450">cardnum</ta>
            <ta e="T452" id="Seg_7207" s="T451">adj</ta>
            <ta e="T453" id="Seg_7208" s="T452">ordnum</ta>
            <ta e="T454" id="Seg_7209" s="T453">v</ta>
            <ta e="T455" id="Seg_7210" s="T454">adj</ta>
            <ta e="T456" id="Seg_7211" s="T455">n</ta>
            <ta e="T457" id="Seg_7212" s="T456">cardnum</ta>
            <ta e="T458" id="Seg_7213" s="T457">adj</ta>
            <ta e="T459" id="Seg_7214" s="T458">n</ta>
            <ta e="T460" id="Seg_7215" s="T459">cardnum</ta>
            <ta e="T461" id="Seg_7216" s="T460">n</ta>
            <ta e="T462" id="Seg_7217" s="T461">adj</ta>
            <ta e="T463" id="Seg_7218" s="T462">adj</ta>
            <ta e="T464" id="Seg_7219" s="T463">n</ta>
            <ta e="T465" id="Seg_7220" s="T464">v</ta>
            <ta e="T467" id="Seg_7221" s="T465">ptcl</ta>
            <ta e="T471" id="Seg_7222" s="T469">ptcl</ta>
            <ta e="T472" id="Seg_7223" s="T471">adv</ta>
            <ta e="T473" id="Seg_7224" s="T472">cardnum</ta>
            <ta e="T474" id="Seg_7225" s="T473">n</ta>
            <ta e="T475" id="Seg_7226" s="T474">ptcl</ta>
            <ta e="T476" id="Seg_7227" s="T475">que</ta>
            <ta e="T477" id="Seg_7228" s="T476">ptcl</ta>
            <ta e="T478" id="Seg_7229" s="T477">propr</ta>
            <ta e="T479" id="Seg_7230" s="T478">ptcl</ta>
            <ta e="T480" id="Seg_7231" s="T479">que</ta>
            <ta e="T481" id="Seg_7232" s="T480">n</ta>
            <ta e="T482" id="Seg_7233" s="T481">v</ta>
            <ta e="T483" id="Seg_7234" s="T482">adv</ta>
            <ta e="T484" id="Seg_7235" s="T483">cardnum</ta>
            <ta e="T485" id="Seg_7236" s="T484">n</ta>
            <ta e="T486" id="Seg_7237" s="T485">adv</ta>
            <ta e="T487" id="Seg_7238" s="T486">cardnum</ta>
            <ta e="T488" id="Seg_7239" s="T487">n</ta>
            <ta e="T489" id="Seg_7240" s="T488">adv</ta>
            <ta e="T490" id="Seg_7241" s="T489">v</ta>
            <ta e="T491" id="Seg_7242" s="T490">cardnum</ta>
            <ta e="T492" id="Seg_7243" s="T491">n</ta>
            <ta e="T494" id="Seg_7244" s="T492">ptcl</ta>
            <ta e="T497" id="Seg_7245" s="T496">ptcl</ta>
            <ta e="T498" id="Seg_7246" s="T497">n</ta>
            <ta e="T499" id="Seg_7247" s="T498">adv</ta>
            <ta e="T500" id="Seg_7248" s="T499">propr</ta>
            <ta e="T501" id="Seg_7249" s="T500">propr</ta>
            <ta e="T508" id="Seg_7250" s="T506">interj</ta>
            <ta e="T509" id="Seg_7251" s="T508">adv</ta>
            <ta e="T510" id="Seg_7252" s="T509">cardnum</ta>
            <ta e="T511" id="Seg_7253" s="T510">adj</ta>
            <ta e="T512" id="Seg_7254" s="T511">n</ta>
            <ta e="T513" id="Seg_7255" s="T512">n</ta>
            <ta e="T514" id="Seg_7256" s="T513">ptcl</ta>
            <ta e="T515" id="Seg_7257" s="T514">adv</ta>
            <ta e="T516" id="Seg_7258" s="T515">v</ta>
            <ta e="T517" id="Seg_7259" s="T516">propr</ta>
            <ta e="T518" id="Seg_7260" s="T517">propr</ta>
            <ta e="T519" id="Seg_7261" s="T518">ptcl</ta>
            <ta e="T521" id="Seg_7262" s="T520">adv</ta>
            <ta e="T522" id="Seg_7263" s="T521">cardnum</ta>
            <ta e="T523" id="Seg_7264" s="T522">n</ta>
            <ta e="T524" id="Seg_7265" s="T523">ptcl</ta>
            <ta e="T525" id="Seg_7266" s="T524">que</ta>
            <ta e="T526" id="Seg_7267" s="T525">n</ta>
            <ta e="T527" id="Seg_7268" s="T526">v</ta>
            <ta e="T528" id="Seg_7269" s="T527">v</ta>
            <ta e="T529" id="Seg_7270" s="T528">v</ta>
            <ta e="T530" id="Seg_7271" s="T529">aux</ta>
            <ta e="T531" id="Seg_7272" s="T530">n</ta>
            <ta e="T532" id="Seg_7273" s="T531">adj</ta>
            <ta e="T533" id="Seg_7274" s="T532">n</ta>
            <ta e="T534" id="Seg_7275" s="T533">dempro</ta>
            <ta e="T535" id="Seg_7276" s="T534">v</ta>
            <ta e="T536" id="Seg_7277" s="T535">aux</ta>
            <ta e="T537" id="Seg_7278" s="T536">adv</ta>
            <ta e="T538" id="Seg_7279" s="T537">post</ta>
            <ta e="T539" id="Seg_7280" s="T538">v</ta>
            <ta e="T540" id="Seg_7281" s="T539">v</ta>
            <ta e="T541" id="Seg_7282" s="T540">post</ta>
            <ta e="T542" id="Seg_7283" s="T541">ptcl</ta>
            <ta e="T543" id="Seg_7284" s="T542">ptcl</ta>
            <ta e="T544" id="Seg_7285" s="T543">adj</ta>
            <ta e="T545" id="Seg_7286" s="T544">v</ta>
            <ta e="T546" id="Seg_7287" s="T545">adv</ta>
            <ta e="T547" id="Seg_7288" s="T546">ptcl</ta>
            <ta e="T548" id="Seg_7289" s="T547">cardnum</ta>
            <ta e="T549" id="Seg_7290" s="T548">n</ta>
            <ta e="T550" id="Seg_7291" s="T549">propr</ta>
            <ta e="T551" id="Seg_7292" s="T550">ptcl</ta>
            <ta e="T553" id="Seg_7293" s="T552">interj</ta>
            <ta e="T554" id="Seg_7294" s="T553">n</ta>
            <ta e="T555" id="Seg_7295" s="T554">n</ta>
            <ta e="T556" id="Seg_7296" s="T555">v</ta>
            <ta e="T557" id="Seg_7297" s="T556">aux</ta>
            <ta e="T558" id="Seg_7298" s="T557">dempro</ta>
            <ta e="T559" id="Seg_7299" s="T558">n</ta>
            <ta e="T560" id="Seg_7300" s="T559">adj</ta>
            <ta e="T561" id="Seg_7301" s="T560">n</ta>
            <ta e="T562" id="Seg_7302" s="T561">v</ta>
            <ta e="T563" id="Seg_7303" s="T562">v</ta>
            <ta e="T564" id="Seg_7304" s="T563">post</ta>
            <ta e="T565" id="Seg_7305" s="T564">propr</ta>
            <ta e="T566" id="Seg_7306" s="T565">adj</ta>
            <ta e="T567" id="Seg_7307" s="T566">propr</ta>
            <ta e="T568" id="Seg_7308" s="T567">v</ta>
            <ta e="T569" id="Seg_7309" s="T568">ptcl</ta>
            <ta e="T570" id="Seg_7310" s="T569">ptcl</ta>
            <ta e="T571" id="Seg_7311" s="T570">ptcl</ta>
            <ta e="T572" id="Seg_7312" s="T571">adv</ta>
            <ta e="T573" id="Seg_7313" s="T572">n</ta>
            <ta e="T574" id="Seg_7314" s="T573">n</ta>
            <ta e="T575" id="Seg_7315" s="T574">cardnum</ta>
            <ta e="T576" id="Seg_7316" s="T575">n</ta>
            <ta e="T577" id="Seg_7317" s="T576">n</ta>
            <ta e="T578" id="Seg_7318" s="T577">ptcl</ta>
            <ta e="T579" id="Seg_7319" s="T578">propr</ta>
            <ta e="T580" id="Seg_7320" s="T579">propr</ta>
            <ta e="T581" id="Seg_7321" s="T580">propr</ta>
            <ta e="T582" id="Seg_7322" s="T581">propr</ta>
            <ta e="T583" id="Seg_7323" s="T582">propr</ta>
            <ta e="T584" id="Seg_7324" s="T583">v</ta>
            <ta e="T585" id="Seg_7325" s="T584">aux</ta>
            <ta e="T586" id="Seg_7326" s="T585">n</ta>
            <ta e="T587" id="Seg_7327" s="T586">dempro</ta>
            <ta e="T588" id="Seg_7328" s="T587">v</ta>
            <ta e="T589" id="Seg_7329" s="T588">adj</ta>
            <ta e="T590" id="Seg_7330" s="T589">ptcl</ta>
            <ta e="T591" id="Seg_7331" s="T590">adv</ta>
            <ta e="T592" id="Seg_7332" s="T591">cardnum</ta>
            <ta e="T593" id="Seg_7333" s="T592">cardnum</ta>
            <ta e="T594" id="Seg_7334" s="T593">adj</ta>
            <ta e="T595" id="Seg_7335" s="T594">cardnum</ta>
            <ta e="T596" id="Seg_7336" s="T595">n</ta>
            <ta e="T597" id="Seg_7337" s="T596">cardnum</ta>
            <ta e="T598" id="Seg_7338" s="T597">interj</ta>
            <ta e="T599" id="Seg_7339" s="T598">cardnum</ta>
            <ta e="T600" id="Seg_7340" s="T599">adj</ta>
            <ta e="T601" id="Seg_7341" s="T600">adv</ta>
            <ta e="T602" id="Seg_7342" s="T601">n</ta>
            <ta e="T603" id="Seg_7343" s="T602">adv</ta>
            <ta e="T604" id="Seg_7344" s="T603">cardnum</ta>
            <ta e="T605" id="Seg_7345" s="T604">post</ta>
            <ta e="T606" id="Seg_7346" s="T605">n</ta>
            <ta e="T607" id="Seg_7347" s="T606">n</ta>
            <ta e="T608" id="Seg_7348" s="T607">adj</ta>
            <ta e="T609" id="Seg_7349" s="T608">v</ta>
            <ta e="T610" id="Seg_7350" s="T609">quant</ta>
            <ta e="T611" id="Seg_7351" s="T610">v</ta>
            <ta e="T612" id="Seg_7352" s="T611">cardnum</ta>
            <ta e="T613" id="Seg_7353" s="T612">n</ta>
            <ta e="T614" id="Seg_7354" s="T613">cardnum</ta>
            <ta e="T615" id="Seg_7355" s="T614">n</ta>
            <ta e="T616" id="Seg_7356" s="T615">n</ta>
            <ta e="T617" id="Seg_7357" s="T616">adj</ta>
            <ta e="T618" id="Seg_7358" s="T617">adj</ta>
            <ta e="T619" id="Seg_7359" s="T618">n</ta>
            <ta e="T620" id="Seg_7360" s="T619">v</ta>
            <ta e="T621" id="Seg_7361" s="T620">v</ta>
            <ta e="T622" id="Seg_7362" s="T621">cardnum</ta>
            <ta e="T623" id="Seg_7363" s="T622">n</ta>
            <ta e="T624" id="Seg_7364" s="T623">n</ta>
            <ta e="T625" id="Seg_7365" s="T624">que</ta>
            <ta e="T626" id="Seg_7366" s="T625">que</ta>
            <ta e="T627" id="Seg_7367" s="T626">cop</ta>
            <ta e="T628" id="Seg_7368" s="T627">ptcl</ta>
            <ta e="T629" id="Seg_7369" s="T628">n</ta>
            <ta e="T630" id="Seg_7370" s="T629">n</ta>
            <ta e="T631" id="Seg_7371" s="T630">cardnum</ta>
            <ta e="T632" id="Seg_7372" s="T631">n</ta>
            <ta e="T633" id="Seg_7373" s="T632">n</ta>
            <ta e="T634" id="Seg_7374" s="T633">n</ta>
            <ta e="T635" id="Seg_7375" s="T634">adj</ta>
            <ta e="T636" id="Seg_7376" s="T635">n</ta>
            <ta e="T637" id="Seg_7377" s="T636">adv</ta>
            <ta e="T638" id="Seg_7378" s="T637">cardnum</ta>
            <ta e="T639" id="Seg_7379" s="T638">n</ta>
            <ta e="T640" id="Seg_7380" s="T639">dempro</ta>
            <ta e="T641" id="Seg_7381" s="T640">v</ta>
            <ta e="T642" id="Seg_7382" s="T641">cardnum</ta>
            <ta e="T643" id="Seg_7383" s="T642">n</ta>
            <ta e="T644" id="Seg_7384" s="T643">adj</ta>
            <ta e="T645" id="Seg_7385" s="T644">cop</ta>
            <ta e="T646" id="Seg_7386" s="T645">n</ta>
            <ta e="T647" id="Seg_7387" s="T646">propr</ta>
            <ta e="T648" id="Seg_7388" s="T647">propr</ta>
            <ta e="T649" id="Seg_7389" s="T648">adj</ta>
            <ta e="T650" id="Seg_7390" s="T649">dempro</ta>
            <ta e="T651" id="Seg_7391" s="T650">post</ta>
            <ta e="T652" id="Seg_7392" s="T651">n</ta>
            <ta e="T653" id="Seg_7393" s="T652">adv</ta>
            <ta e="T654" id="Seg_7394" s="T653">v</ta>
            <ta e="T655" id="Seg_7395" s="T654">n</ta>
            <ta e="T656" id="Seg_7396" s="T655">ptcl</ta>
            <ta e="T657" id="Seg_7397" s="T656">emphpro</ta>
            <ta e="T658" id="Seg_7398" s="T657">cardnum</ta>
            <ta e="T659" id="Seg_7399" s="T658">n</ta>
            <ta e="T660" id="Seg_7400" s="T659">propr</ta>
            <ta e="T661" id="Seg_7401" s="T660">n</ta>
            <ta e="T662" id="Seg_7402" s="T661">n</ta>
            <ta e="T663" id="Seg_7403" s="T662">v</ta>
            <ta e="T664" id="Seg_7404" s="T663">v</ta>
            <ta e="T665" id="Seg_7405" s="T664">ptcl</ta>
            <ta e="T666" id="Seg_7406" s="T665">que</ta>
            <ta e="T667" id="Seg_7407" s="T666">v</ta>
            <ta e="T671" id="Seg_7408" s="T670">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-KiPP" />
         <annotation name="SyF" tierref="SyF-KiPP" />
         <annotation name="IST" tierref="IST-KiPP" />
         <annotation name="Top" tierref="Top-KiPP" />
         <annotation name="Foc" tierref="Foc-KiPP" />
         <annotation name="BOR" tierref="BOR-KiPP">
            <ta e="T22" id="Seg_7409" s="T21">RUS:core</ta>
            <ta e="T115" id="Seg_7410" s="T113">RUS:cult</ta>
            <ta e="T121" id="Seg_7411" s="T120">RUS:cult</ta>
            <ta e="T122" id="Seg_7412" s="T121">RUS:core</ta>
            <ta e="T123" id="Seg_7413" s="T122">RUS:cult</ta>
            <ta e="T124" id="Seg_7414" s="T123">RUS:cult</ta>
            <ta e="T138" id="Seg_7415" s="T137">RUS:cult</ta>
            <ta e="T142" id="Seg_7416" s="T141">RUS:cult</ta>
            <ta e="T155" id="Seg_7417" s="T154">RUS:cult</ta>
            <ta e="T156" id="Seg_7418" s="T155">RUS:cult</ta>
            <ta e="T163" id="Seg_7419" s="T162">RUS:cult</ta>
            <ta e="T166" id="Seg_7420" s="T165">RUS:cult</ta>
            <ta e="T191" id="Seg_7421" s="T190">RUS:cult</ta>
            <ta e="T241" id="Seg_7422" s="T240">RUS:core</ta>
            <ta e="T267" id="Seg_7423" s="T266">RUS:cult</ta>
            <ta e="T273" id="Seg_7424" s="T272">RUS:cult</ta>
            <ta e="T280" id="Seg_7425" s="T279">RUS:gram</ta>
            <ta e="T282" id="Seg_7426" s="T281">RUS:cult</ta>
            <ta e="T290" id="Seg_7427" s="T289">RUS:cult</ta>
            <ta e="T347" id="Seg_7428" s="T346">RUS:cult</ta>
            <ta e="T351" id="Seg_7429" s="T350">RUS:cult</ta>
            <ta e="T354" id="Seg_7430" s="T353">RUS:cult</ta>
            <ta e="T363" id="Seg_7431" s="T362">RUS:cult</ta>
            <ta e="T366" id="Seg_7432" s="T365">RUS:cult</ta>
            <ta e="T369" id="Seg_7433" s="T368">RUS:cult</ta>
            <ta e="T376" id="Seg_7434" s="T375">RUS:cult</ta>
            <ta e="T381" id="Seg_7435" s="T380">RUS:cult</ta>
            <ta e="T382" id="Seg_7436" s="T381">RUS:cult</ta>
            <ta e="T384" id="Seg_7437" s="T383">RUS:cult</ta>
            <ta e="T394" id="Seg_7438" s="T393">RUS:cult</ta>
            <ta e="T397" id="Seg_7439" s="T396">RUS:cult</ta>
            <ta e="T398" id="Seg_7440" s="T397">RUS:cult</ta>
            <ta e="T406" id="Seg_7441" s="T405">RUS:cult</ta>
            <ta e="T415" id="Seg_7442" s="T414">RUS:cult</ta>
            <ta e="T419" id="Seg_7443" s="T418">RUS:cult</ta>
            <ta e="T421" id="Seg_7444" s="T420">RUS:cult</ta>
            <ta e="T423" id="Seg_7445" s="T422">RUS:cult</ta>
            <ta e="T430" id="Seg_7446" s="T429">RUS:cult</ta>
            <ta e="T433" id="Seg_7447" s="T432">RUS:cult</ta>
            <ta e="T436" id="Seg_7448" s="T435">RUS:cult</ta>
            <ta e="T441" id="Seg_7449" s="T440">RUS:core</ta>
            <ta e="T446" id="Seg_7450" s="T445">RUS:cult</ta>
            <ta e="T447" id="Seg_7451" s="T446">RUS:cult</ta>
            <ta e="T449" id="Seg_7452" s="T448">EV:gram (DIM)</ta>
            <ta e="T478" id="Seg_7453" s="T477">RUS:cult</ta>
            <ta e="T481" id="Seg_7454" s="T480">RUS:cult</ta>
            <ta e="T490" id="Seg_7455" s="T489">RUS:cult</ta>
            <ta e="T498" id="Seg_7456" s="T497">RUS:cult</ta>
            <ta e="T501" id="Seg_7457" s="T500">RUS:cult</ta>
            <ta e="T516" id="Seg_7458" s="T515">RUS:cult</ta>
            <ta e="T517" id="Seg_7459" s="T516">RUS:cult</ta>
            <ta e="T518" id="Seg_7460" s="T517">RUS:cult</ta>
            <ta e="T526" id="Seg_7461" s="T525">RUS:cult</ta>
            <ta e="T533" id="Seg_7462" s="T532">RUS:cult</ta>
            <ta e="T542" id="Seg_7463" s="T541">RUS:disc</ta>
            <ta e="T550" id="Seg_7464" s="T549">RUS:cult</ta>
            <ta e="T554" id="Seg_7465" s="T553">RUS:cult</ta>
            <ta e="T565" id="Seg_7466" s="T564">RUS:cult</ta>
            <ta e="T567" id="Seg_7467" s="T566">RUS:cult</ta>
            <ta e="T579" id="Seg_7468" s="T578">RUS:cult</ta>
            <ta e="T580" id="Seg_7469" s="T579">RUS:cult</ta>
            <ta e="T581" id="Seg_7470" s="T580">RUS:cult</ta>
            <ta e="T582" id="Seg_7471" s="T581">RUS:cult</ta>
            <ta e="T583" id="Seg_7472" s="T582">RUS:cult</ta>
            <ta e="T600" id="Seg_7473" s="T599">RUS:cult</ta>
            <ta e="T602" id="Seg_7474" s="T601">RUS:cult</ta>
            <ta e="T616" id="Seg_7475" s="T615">RUS:cult</ta>
            <ta e="T624" id="Seg_7476" s="T623">RUS:cult</ta>
            <ta e="T629" id="Seg_7477" s="T628">EV:core</ta>
            <ta e="T630" id="Seg_7478" s="T629">RUS:cult</ta>
            <ta e="T633" id="Seg_7479" s="T632">RUS:cult</ta>
            <ta e="T634" id="Seg_7480" s="T633">RUS:cult</ta>
            <ta e="T648" id="Seg_7481" s="T647">RUS:cult</ta>
            <ta e="T660" id="Seg_7482" s="T659">RUS:cult</ta>
            <ta e="T661" id="Seg_7483" s="T660">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-KiPP" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-KiPP" />
         <annotation name="CS" tierref="CS-KiPP" />
         <annotation name="fe" tierref="fe-KiPP">
            <ta e="T4" id="Seg_7484" s="T1">– Is it audible from here?</ta>
            <ta e="T9" id="Seg_7485" s="T6">– Yes.</ta>
            <ta e="T20" id="Seg_7486" s="T14">– Well, what is she asking?</ta>
            <ta e="T25" id="Seg_7487" s="T20">At first she asks anything.</ta>
            <ta e="T31" id="Seg_7488" s="T27">– (…) hears (…).</ta>
            <ta e="T33" id="Seg_7489" s="T31">Eh.</ta>
            <ta e="T51" id="Seg_7490" s="T49">– Oh dear.</ta>
            <ta e="T57" id="Seg_7491" s="T51">When will I stop then, in the evening.</ta>
            <ta e="T68" id="Seg_7492" s="T66">– Korgo.</ta>
            <ta e="T73" id="Seg_7493" s="T70">– I was born in Korgo.</ta>
            <ta e="T76" id="Seg_7494" s="T73">There, in my own land.</ta>
            <ta e="T77" id="Seg_7495" s="T76">I am illegitimate.</ta>
            <ta e="T81" id="Seg_7496" s="T77">Do you know, what means "bulunnʼa"?</ta>
            <ta e="T92" id="Seg_7497" s="T89">– Without father.</ta>
            <ta e="T108" id="Seg_7498" s="T106">– Without father.</ta>
            <ta e="T117" id="Seg_7499" s="T111">– Well, my granny brought me up.</ta>
            <ta e="T124" id="Seg_7500" s="T120">– Granny brought me up, my mum's mum.</ta>
            <ta e="T139" id="Seg_7501" s="T128">– Then our whole family had died, we lived as orphans with the old woman, with my granny.</ta>
            <ta e="T144" id="Seg_7502" s="T139">Then I went to Novorybnoe.</ta>
            <ta e="T158" id="Seg_7503" s="T144">Then we went with my granny, with my mother, I use to call her mother, she is my mother, though.</ta>
            <ta e="T167" id="Seg_7504" s="T158">Well, then we came back, when my mother, the old woman, my granny died.</ta>
            <ta e="T175" id="Seg_7505" s="T167">After dying she revived, we came back to that place.</ta>
            <ta e="T184" id="Seg_7506" s="T175">After we had come there, when I was sixteen, I was married.</ta>
            <ta e="T190" id="Seg_7507" s="T184">"If I die, they'll make a slave out of you", she said.</ta>
            <ta e="T200" id="Seg_7508" s="T190">My God, I am clean like nothing else, I didn't have my period, I married.</ta>
            <ta e="T204" id="Seg_7509" s="T200">Crying they marry me, they let me sleep.</ta>
            <ta e="T210" id="Seg_7510" s="T204">Crying I fall asleep, I am afraid of my husband, I sleep with clothes on.</ta>
            <ta e="T212" id="Seg_7511" s="T210">Eh, well.</ta>
            <ta e="T217" id="Seg_7512" s="T212">Then one year I was sleeping with clothes.</ta>
            <ta e="T225" id="Seg_7513" s="T217">Then, well, later I accustomed to my husband.</ta>
            <ta e="T232" id="Seg_7514" s="T225">That means first love, then, a whole year.</ta>
            <ta e="T240" id="Seg_7515" s="T232">Being seventeen, eighteen years old I had a child.</ta>
            <ta e="T244" id="Seg_7516" s="T240">My first child is here.</ta>
            <ta e="T254" id="Seg_7517" s="T244">They I had one child after another, every year I have child.</ta>
            <ta e="T265" id="Seg_7518" s="T258">– Yes, in this land, we went through the tundra, we were in the tundra.</ta>
            <ta e="T268" id="Seg_7519" s="T265">My husband was a cashier.</ta>
            <ta e="T275" id="Seg_7520" s="T268">He was a very goog man, a very capable hunter.</ta>
            <ta e="T278" id="Seg_7521" s="T275">He hunted animals.</ta>
            <ta e="T281" id="Seg_7522" s="T278">And he built houses.</ta>
            <ta e="T286" id="Seg_7523" s="T281">As a cashier he worked for fifteen years.</ta>
            <ta e="T291" id="Seg_7524" s="T286">Then he worked one year as a party organizator.</ta>
            <ta e="T298" id="Seg_7525" s="T291">Then he built houses, here the houses built by him are standing.</ta>
            <ta e="T304" id="Seg_7526" s="T298">Then, we have twelve children, at all.</ta>
            <ta e="T309" id="Seg_7527" s="T304">Not a single child I gave to the people.</ta>
            <ta e="T316" id="Seg_7528" s="T309">Being myself an adoptive child, I don't give them to people, I feel sorry for my children.</ta>
            <ta e="T327" id="Seg_7529" s="T316">Bringing them up, with one child in the cradle I used to go fishing with my husband, after he had become sick.</ta>
            <ta e="T342" id="Seg_7530" s="T327">Well, our children grew up and went to study, my two big children went for reindeer education, our son and daughter.</ta>
            <ta e="T354" id="Seg_7531" s="T342">Then one child, having grown up, went to Krasnoyarsk to study, now she works as a therapist in Dudinka.</ta>
            <ta e="T367" id="Seg_7532" s="T354">Then one of my children studied and finished two institutes, it is in Krasnoyarsk.</ta>
            <ta e="T369" id="Seg_7533" s="T367">Sergey.</ta>
            <ta e="T375" id="Seg_7534" s="T369">That one is married, he has got two sons.</ta>
            <ta e="T377" id="Seg_7535" s="T375">He is married to a Kazakh [woman].</ta>
            <ta e="T386" id="Seg_7536" s="T377">(From the storage?), now he works as a guard, after he had finished the police [education].</ta>
            <ta e="T390" id="Seg_7537" s="T386">Earlier he was a reindeer worker.</ta>
            <ta e="T400" id="Seg_7538" s="T390">Then one of my children works as a cook in Khatanga, a main cook, one of my daughters.</ta>
            <ta e="T406" id="Seg_7539" s="T400">In Khatanga, Marina.</ta>
            <ta e="T414" id="Seg_7540" s="T409">– No, she has studied, yes, there.</ta>
            <ta e="T416" id="Seg_7541" s="T414">She studied in Krasnoyarsk.</ta>
            <ta e="T430" id="Seg_7542" s="T416">Then my daughter, who is a doctor, works in Dudinka, she works as a therapist now, she has got three children, she is an old woman with pension. </ta>
            <ta e="T433" id="Seg_7543" s="T430">All of my children have retired.</ta>
            <ta e="T436" id="Seg_7544" s="T433">Three of my children are retired.</ta>
            <ta e="T450" id="Seg_7545" s="T436">My bigger ones now, one of them is a fisherman, one of them is now old and has retired, Vera, her husband has died recently. </ta>
            <ta e="T454" id="Seg_7546" s="T450">She has got two children, the third has died.</ta>
            <ta e="T463" id="Seg_7547" s="T454">My big son has got three children, one of his children is married and has got children.</ta>
            <ta e="T467" id="Seg_7548" s="T463">The others don't have children yet.</ta>
            <ta e="T471" id="Seg_7549" s="T469">– Yes.</ta>
            <ta e="T479" id="Seg_7550" s="T471">Then one of my children is in whatchamacallit, it is in Sopochnoe.</ta>
            <ta e="T482" id="Seg_7551" s="T479">It works as a whatchamachallit, as an educator.</ta>
            <ta e="T490" id="Seg_7552" s="T482">Then of my children is here, one of my daughters works as an educator here.</ta>
            <ta e="T494" id="Seg_7553" s="T490">One of my children…</ta>
            <ta e="T501" id="Seg_7554" s="T496">– Yes, in school here, Domna Petrovna.</ta>
            <ta e="T519" id="Seg_7555" s="T506">– Eh, then the wife of my small son also works here as a teacher, Nadya Kirgizova.</ta>
            <ta e="T520" id="Seg_7556" s="T519">Daughter-in-law.</ta>
            <ta e="T530" id="Seg_7557" s="T520">Then of my sons has finished [the education] for an all terrain vehicle driver, having finished learning he worked.</ta>
            <ta e="T531" id="Seg_7558" s="T530">Twins.</ta>
            <ta e="T541" id="Seg_7559" s="T531">One of them works as an electrician until today, after he had finished his education.</ta>
            <ta e="T545" id="Seg_7560" s="T541">So all of them finished their education.</ta>
            <ta e="T551" id="Seg_7561" s="T545">Then one of my children is in Kresty.</ta>
            <ta e="T566" id="Seg_7562" s="T551">He had finished the police education, he went everywhere, he is married in Kresty.</ta>
            <ta e="T567" id="Seg_7563" s="T566">Alexey.</ta>
            <ta e="T571" id="Seg_7564" s="T567">You did see him probably.</ta>
            <ta e="T578" id="Seg_7565" s="T571">Then the others, three of my sons, are hunters, though.</ta>
            <ta e="T580" id="Seg_7566" s="T578">Alyosha, Sergey.</ta>
            <ta e="T582" id="Seg_7567" s="T580">Misha, Vitya.</ta>
            <ta e="T588" id="Seg_7568" s="T582">Vitja is sleeping, his wife is standing there.</ta>
            <ta e="T591" id="Seg_7569" s="T588">That's all, then.</ta>
            <ta e="T600" id="Seg_7570" s="T591">I have got twelve children, I have got twenty-five, eh, twenty-seven grandchildren.</ta>
            <ta e="T606" id="Seg_7571" s="T600">Then our village is more than forty people.</ta>
            <ta e="T610" id="Seg_7572" s="T606">When I find all my daughters-in-law, it's a lot.</ta>
            <ta e="T611" id="Seg_7573" s="T610">When I count them.</ta>
            <ta e="T616" id="Seg_7574" s="T611">One daughter-in-law, two daughters-in-law are pedagogs.</ta>
            <ta e="T618" id="Seg_7575" s="T616">They all have got children.</ta>
            <ta e="T621" id="Seg_7576" s="T618">Their daughters went to study.</ta>
            <ta e="T624" id="Seg_7577" s="T621">One of my daughters-in-law is in kindergarten.</ta>
            <ta e="T629" id="Seg_7578" s="T624">Whatchamacallit, how was it called, jeez?</ta>
            <ta e="T630" id="Seg_7579" s="T629">Leading.</ta>
            <ta e="T634" id="Seg_7580" s="T630">Two of my daughters-in-law are teachers, pedagogs.</ta>
            <ta e="T636" id="Seg_7581" s="T634">All of them work.</ta>
            <ta e="T646" id="Seg_7582" s="T636">Then one son-in-law has died, one son-in-law is sick.</ta>
            <ta e="T649" id="Seg_7583" s="T646">That of Domna Petrovna.</ta>
            <ta e="T657" id="Seg_7584" s="T649">Nevertheless he kills enough animals, he himself without legs.</ta>
            <ta e="T661" id="Seg_7585" s="T657">One son-in-law is a hunter in Sopochnoe.</ta>
            <ta e="T664" id="Seg_7586" s="T661">His daughter went to study.</ta>
            <ta e="T667" id="Seg_7587" s="T664">Well, what shall I tell?</ta>
            <ta e="T671" id="Seg_7588" s="T670">– Well.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KiPP">
            <ta e="T4" id="Seg_7589" s="T1">– Hört man es von hier?</ta>
            <ta e="T9" id="Seg_7590" s="T6">– Ja.</ta>
            <ta e="T20" id="Seg_7591" s="T14">– Nun, was fragt sie?</ta>
            <ta e="T25" id="Seg_7592" s="T20">Zuerst fragt sie etwas Beliebiges.</ta>
            <ta e="T31" id="Seg_7593" s="T27">– (…) hört (…).</ta>
            <ta e="T33" id="Seg_7594" s="T31">Äh. </ta>
            <ta e="T51" id="Seg_7595" s="T49">– Oh man.</ta>
            <ta e="T57" id="Seg_7596" s="T51">Wann bin ich dann fertig, am Abend.</ta>
            <ta e="T68" id="Seg_7597" s="T66">– Korgo.</ta>
            <ta e="T73" id="Seg_7598" s="T70">– In Korgo wurde ich geboren.</ta>
            <ta e="T76" id="Seg_7599" s="T73">Hier in meinem Land.</ta>
            <ta e="T77" id="Seg_7600" s="T76">Ich bin unehelich.</ta>
            <ta e="T81" id="Seg_7601" s="T77">Weißt du, was "bulunnʼa" bedeutet?</ta>
            <ta e="T92" id="Seg_7602" s="T89">– Ohne Vater.</ta>
            <ta e="T108" id="Seg_7603" s="T106">– Ohne Vater.</ta>
            <ta e="T117" id="Seg_7604" s="T111">– Meine Großmutter hat mich großgezogen.</ta>
            <ta e="T124" id="Seg_7605" s="T120">– Die Großmutter hat mich großgezogen, die Mutter meiner Mutter.</ta>
            <ta e="T139" id="Seg_7606" s="T128">– Damals war unsere ganze Familie gestorben, als Waisen lebten wir mit der alten Frau, mit meiner Großmutter.</ta>
            <ta e="T144" id="Seg_7607" s="T139">Dann bin ich nach Novorybnoe gegangen.</ta>
            <ta e="T158" id="Seg_7608" s="T144">Dann gingen wir mit meiner Großmutter, mit meiner Mutter, ich nenne sie Mutter, es ist schließlich meine Mutter.</ta>
            <ta e="T167" id="Seg_7609" s="T158">Nun, dann kamen wir zurück, als meine Mutter, die alte Frau, meine Großmutter starb.</ta>
            <ta e="T175" id="Seg_7610" s="T167">Nach dem Tod lebte sie auf, wir kamen an jenen Ort zurück.</ta>
            <ta e="T184" id="Seg_7611" s="T175">Nachdem wir gekommen waren, als ich sechzehn war, wurde ich verheiratet.</ta>
            <ta e="T190" id="Seg_7612" s="T184">"Wenn ich sterbe, machen sie dich zum Sklaven", sagte sie.</ta>
            <ta e="T200" id="Seg_7613" s="T190">Mein Gott, sauber wie nichts bin ich, ich hatte noch nicht die Regel, ich heiratete.</ta>
            <ta e="T204" id="Seg_7614" s="T200">Weinend verheiraten sie mich, sie lassen mich schlafen.</ta>
            <ta e="T210" id="Seg_7615" s="T204">Weinend schlafe ich ein, ich habe Angst vor meinem Mann, mit Kleidung schlafe ich.</ta>
            <ta e="T212" id="Seg_7616" s="T210">Nun ja.</ta>
            <ta e="T217" id="Seg_7617" s="T212">Ein Jahr schlief ich dann mit Kleidung.</ta>
            <ta e="T225" id="Seg_7618" s="T217">Dann, nun, später gewöhne ich mich an meinen Mann.</ta>
            <ta e="T232" id="Seg_7619" s="T225">Das heißt dann erste Liebe, dann, ein ganzes Jahr.</ta>
            <ta e="T240" id="Seg_7620" s="T232">Mit siebzehn, mit achtzehn bekam ich ein Kind.</ta>
            <ta e="T244" id="Seg_7621" s="T240">Mein erstes Kind ist hier.</ta>
            <ta e="T254" id="Seg_7622" s="T244">Dann bekam ich Kind um Kind, jedes Jahr bekomme ich ein Kind.</ta>
            <ta e="T265" id="Seg_7623" s="T258">– Ja, in diesem Land, wir sind in der Tundra gefahren, wir waren in der Tundra.</ta>
            <ta e="T268" id="Seg_7624" s="T265">Mein Mann war Kassierer.</ta>
            <ta e="T275" id="Seg_7625" s="T268">Er war ein sehr guter Mensch, ein sehr fähiger Jäger.</ta>
            <ta e="T278" id="Seg_7626" s="T275">Er hat Tiere gejagt.</ta>
            <ta e="T281" id="Seg_7627" s="T278">Und er hat Häuser gebaut.</ta>
            <ta e="T286" id="Seg_7628" s="T281">Als Kassierer hat er fünfzehn Jahre gearbeitet.</ta>
            <ta e="T291" id="Seg_7629" s="T286">Dann hat er ein Jahr als Parteikoordinator gearbeitet.</ta>
            <ta e="T298" id="Seg_7630" s="T291">Dann hat er Häuser gebaut, hier stehen seine gebauten Häuser.</ta>
            <ta e="T304" id="Seg_7631" s="T298">Dann, wir haben zwölf Kinder, insgesamt.</ta>
            <ta e="T309" id="Seg_7632" s="T304">Nicht eins meiner Kinder habe ich den Leuten gegeben.</ta>
            <ta e="T316" id="Seg_7633" s="T309">Da ich selbst ein angenommenes Kind war, gebe ich sie niemanden, mir tun meine Kinder Leid.</ta>
            <ta e="T327" id="Seg_7634" s="T316">Diese aufziehend, mit einem Kind in der Wiege ging ich mit meinem Mann Fischen, nachdem er krank geworden war.</ta>
            <ta e="T342" id="Seg_7635" s="T327">Nun, unsere Kinder wuchsen und gingen studieren, die zwei großen Kinder gingen zur Rentierausbildung, Sohn und Tochter.</ta>
            <ta e="T354" id="Seg_7636" s="T342">Dann ein Kind, nachdem es groß geworden war, ging nach Krasnojarsk zur Ausbildung, jetzt arbeitet sie als Therapeutin in Dudinka.</ta>
            <ta e="T367" id="Seg_7637" s="T354">Dann ein Kind hat studiert und zwei Institue abgeschlossen, es ist in Krasnojarsk.</ta>
            <ta e="T369" id="Seg_7638" s="T367">Sergej.</ta>
            <ta e="T375" id="Seg_7639" s="T369">Der ist verheiratet, er hat zwei Söhne.</ta>
            <ta e="T377" id="Seg_7640" s="T375">Er hat eine Kasachin geheiratet.</ta>
            <ta e="T386" id="Seg_7641" s="T377">(Vom Speicher?), jetzt arbeitet er als Wache, nachdem er die Polizei[schule] beendet hat.</ta>
            <ta e="T390" id="Seg_7642" s="T386">Früher war er Rentierarbeiter.</ta>
            <ta e="T400" id="Seg_7643" s="T390">Dann mein eines Kind arbeitet als Köchin in Chatanga, Hauptköchin, meine eine Tochter.</ta>
            <ta e="T406" id="Seg_7644" s="T400">In Chatanga, Marina.</ta>
            <ta e="T414" id="Seg_7645" s="T409">– Nein, sie hat studiert, ja, dort.</ta>
            <ta e="T416" id="Seg_7646" s="T414">Sie hat in Krasnojarsk studiert.</ta>
            <ta e="T430" id="Seg_7647" s="T416">Dann meine Doktortochter arbeitet in Dudinka, sie arbeitet als Therapeutin, sie hat drei Kinder, sie ist eine alte Frau mit Rente.</ta>
            <ta e="T433" id="Seg_7648" s="T430">Alle meine Kinder sind in Rente. </ta>
            <ta e="T436" id="Seg_7649" s="T433">Drei meiner Kinder sind in Rente.</ta>
            <ta e="T450" id="Seg_7650" s="T436">Meine Größeren jetzt, einer von ihnen geht fischen, eine von ihnen ist alt und in Rente, Vera, ihr Mann ist gerade gestorben.</ta>
            <ta e="T454" id="Seg_7651" s="T450">Sie hat zwei Kinder, das Dritte ist gestorben.</ta>
            <ta e="T463" id="Seg_7652" s="T454">Mein ältester Sohn hat drei Kinder, ein Kind von ihm ist verheiratet und hat Kinder.</ta>
            <ta e="T467" id="Seg_7653" s="T463">Die anderen haben noch keine Kinder.</ta>
            <ta e="T471" id="Seg_7654" s="T469">– Ja.</ta>
            <ta e="T479" id="Seg_7655" s="T471">Dann ein Kind von mir ist dort, es ist in Sopotschnoje.</ta>
            <ta e="T482" id="Seg_7656" s="T479">Es arbeitet als Dings, als Erzieher.</ta>
            <ta e="T490" id="Seg_7657" s="T482">Dann mein eines Kind hier, meine eine Tochter ist Erzieherin hier.</ta>
            <ta e="T494" id="Seg_7658" s="T490">Mein eines Kind…</ta>
            <ta e="T501" id="Seg_7659" s="T496">– Ja, in der Schule hier, Domna Petrovna.</ta>
            <ta e="T519" id="Seg_7660" s="T506">– Äh, dann die Frau meines kleines Sohnes arbeitet auch hier als Lehrerin, Nadja Kirgizova.</ta>
            <ta e="T520" id="Seg_7661" s="T519">Schwiegertochter.</ta>
            <ta e="T530" id="Seg_7662" s="T520">Dann mein einer Sohn hat [die Ausbildung zum] Geländewagenfahrer abgeschlossen, nachdem er ausgelernt hatte, arbeitete er.</ta>
            <ta e="T531" id="Seg_7663" s="T530">Zwillinge.</ta>
            <ta e="T541" id="Seg_7664" s="T531">Einer arbeitet als Elektriker bis heute, nachdem er die Ausbildung abgeschlossen hatte.</ta>
            <ta e="T545" id="Seg_7665" s="T541">So haben alle ausgelernt.</ta>
            <ta e="T551" id="Seg_7666" s="T545">Dann ist eines meiner Kinder in Kresty.</ta>
            <ta e="T566" id="Seg_7667" s="T551">Er hatte die Polizeiausbildung abgeschlossen, der ist überall herumgekommen, er ist in Kresty verheiratet.</ta>
            <ta e="T567" id="Seg_7668" s="T566">Alexej.</ta>
            <ta e="T571" id="Seg_7669" s="T567">Du hast ihn wahrscheinlich gesehen.</ta>
            <ta e="T578" id="Seg_7670" s="T571">Dann die anderen sind Jäger, drei meiner Söhne, Jäger.</ta>
            <ta e="T580" id="Seg_7671" s="T578">Aljoscha, Sergej.</ta>
            <ta e="T582" id="Seg_7672" s="T580">Mischa, Vitja.</ta>
            <ta e="T588" id="Seg_7673" s="T582">Vitja schläft, seine Frau steht da.</ta>
            <ta e="T591" id="Seg_7674" s="T588">Das ist dann wohl alles.</ta>
            <ta e="T600" id="Seg_7675" s="T591">Ich habe zwölf Kinder, ich habe fünfundzwanzig, äh, siebenundzwanzig Enkel.</ta>
            <ta e="T606" id="Seg_7676" s="T600">Dann unser Dorf sind so über vierzig Leute.</ta>
            <ta e="T610" id="Seg_7677" s="T606">Wenn ich alle meine Schwiegertöchter finde, sind es viele.</ta>
            <ta e="T611" id="Seg_7678" s="T610">Wenn ich sie zähle.</ta>
            <ta e="T616" id="Seg_7679" s="T611">Eine Schwiegertochter, zwei Schwiegertöchter sind Pädagogen.</ta>
            <ta e="T618" id="Seg_7680" s="T616">Sie haben alle Kinder.</ta>
            <ta e="T621" id="Seg_7681" s="T618">Ihre Töchter sind studieren gegangen.</ta>
            <ta e="T624" id="Seg_7682" s="T621">Meine eine Schwiegertochter ist im Kindergarten.</ta>
            <ta e="T629" id="Seg_7683" s="T624">Diese, wie hieß das noch mal, Mensch?</ta>
            <ta e="T630" id="Seg_7684" s="T629">Leitende.</ta>
            <ta e="T634" id="Seg_7685" s="T630">Zwei meiner Schwiegertöchter sind Lehrerinnen, Pädagogen.</ta>
            <ta e="T636" id="Seg_7686" s="T634">Alle arbeiten.</ta>
            <ta e="T646" id="Seg_7687" s="T636">Dann ein Schwiegersohn ist gestorben, ein Schwiegersohn ist krank.</ta>
            <ta e="T649" id="Seg_7688" s="T646">Der von Domna Petrovna.</ta>
            <ta e="T657" id="Seg_7689" s="T649">Trotzdem tötet er genug Tiere, ohne Beine.</ta>
            <ta e="T661" id="Seg_7690" s="T657">Einer meiner Schwiegersöhne ist Jäger in Sopotschnoje.</ta>
            <ta e="T664" id="Seg_7691" s="T661">Seine Tochter ist studieren gegangen.</ta>
            <ta e="T667" id="Seg_7692" s="T664">Nun, was soll ich erzählen?</ta>
            <ta e="T671" id="Seg_7693" s="T670">– Gut.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KiPP" />
         <annotation name="ltr" tierref="ltr-KiPP">
            <ta e="T4" id="Seg_7694" s="T1">otsuda slishno?</ta>
            <ta e="T20" id="Seg_7695" s="T14">chto ona sprashivaet to?</ta>
            <ta e="T25" id="Seg_7696" s="T20">snachala pro ljubov sprashivaet</ta>
            <ta e="T31" id="Seg_7697" s="T27">slyshet</ta>
            <ta e="T33" id="Seg_7698" s="T31">eee</ta>
            <ta e="T57" id="Seg_7699" s="T51">kogda ja togda zakonchu, do vechera chto li</ta>
            <ta e="T73" id="Seg_7700" s="T70">Na Korgo rodilas'</ta>
            <ta e="T76" id="Seg_7701" s="T73">zdes na svoej zemle</ta>
            <ta e="T77" id="Seg_7702" s="T76">ja nebrachnaja</ta>
            <ta e="T81" id="Seg_7703" s="T77">znaesh' chto takoe bulunja</ta>
            <ta e="T92" id="Seg_7704" s="T89">bez otsA</ta>
            <ta e="T108" id="Seg_7705" s="T106">bez otsa</ta>
            <ta e="T117" id="Seg_7706" s="T111">menja babushka vospityvala</ta>
            <ta e="T124" id="Seg_7707" s="T120">mmoja vospityvala mama mamy</ta>
            <ta e="T139" id="Seg_7708" s="T128">togda nashi semja pomerli my ostalis' v dvoem sirotami zhili s babushkoj</ta>
            <ta e="T144" id="Seg_7709" s="T139">potom v novorynoe poehala</ta>
            <ta e="T158" id="Seg_7710" s="T144">potom poehali s babushkoj s mamoj, ja ee nazyvala mamoj, mama, mama vse ravno</ta>
            <ta e="T167" id="Seg_7711" s="T158">potom obratno pereehali, kogda prababushka umirala</ta>
            <ta e="T175" id="Seg_7712" s="T167">posle smerti ona ozhila v toj mestnosti potom obratno pereehali</ta>
            <ta e="T184" id="Seg_7713" s="T175">posle togo kak pereehali kogda mne bylo 16 let vydali zamuzh</ta>
            <ta e="T190" id="Seg_7714" s="T184">esli ja umru tebja delajut slugoj, skazala</ta>
            <ta e="T200" id="Seg_7715" s="T190">devstvennitsa tem bolee chistaja, mesjachnye ne posli kogda zamuzh vyshla</ta>
            <ta e="T204" id="Seg_7716" s="T200">s plachem otdajut zamuzh, nuzhno bylo spat'</ta>
            <ta e="T210" id="Seg_7717" s="T204">s plachem spat' lozhus' muzha bojus' v odezhde splju</ta>
            <ta e="T217" id="Seg_7718" s="T212">god v odezhde spala</ta>
            <ta e="T225" id="Seg_7719" s="T217">potom popozzhe privykla k muzhu</ta>
            <ta e="T232" id="Seg_7720" s="T225">eto nazyvaetsja pervaja pervaja ljubov</ta>
            <ta e="T240" id="Seg_7721" s="T232">v semnadtsat' v vosemnadtsat let rodila</ta>
            <ta e="T244" id="Seg_7722" s="T240">pervyj rebenok zdes</ta>
            <ta e="T254" id="Seg_7723" s="T244">potom rozhala rozhala kazhdyj god rozhala</ta>
            <ta e="T265" id="Seg_7724" s="T258">zdes na etoj zemle v tundra kogda argishili</ta>
            <ta e="T268" id="Seg_7725" s="T265">muzh byl kassiron</ta>
            <ta e="T275" id="Seg_7726" s="T268">ochen' horoshij byl chelovek ochen' sposobnyj ohotnik</ta>
            <ta e="T278" id="Seg_7727" s="T275">dobychu dobyval</ta>
            <ta e="T281" id="Seg_7728" s="T278">doma stroil</ta>
            <ta e="T286" id="Seg_7729" s="T281">kassirom 15 let prorabotal</ta>
            <ta e="T291" id="Seg_7730" s="T286">potom odin god byl parttorgom</ta>
            <ta e="T298" id="Seg_7731" s="T291">potom doma stroil vot oni stojat</ta>
            <ta e="T304" id="Seg_7732" s="T298">potom 12 detej vsego</ta>
            <ta e="T309" id="Seg_7733" s="T304">ni odnogo rebjonka ljudam ne otdala</ta>
            <ta e="T316" id="Seg_7734" s="T309">sama byla sirotoj poetomu ljudam ne otdavala zhalela detej</ta>
            <ta e="T327" id="Seg_7735" s="T316">etih rastila rastila s malenkim rebenkom v ljulke s muzhem ezdila na rybalku, posle togo kak zabolel</ta>
            <ta e="T342" id="Seg_7736" s="T327">deti vyrastiv pouezzhali uchitjsa potom pozhze dvoe starshih detej uehali uchitsja na veterinara, syn i doch</ta>
            <ta e="T354" id="Seg_7737" s="T342">potom odin rebenok povzroslev poehal uchitsja v Krasnojarsk teper ona rabotaet terapevtom </ta>
            <ta e="T367" id="Seg_7738" s="T354">potom esho odin rebenok zakonchil 2 institua v krasnojarske, ostalsja</ta>
            <ta e="T369" id="Seg_7739" s="T367">Sergej</ta>
            <ta e="T375" id="Seg_7740" s="T369">on zhenatyj dvoe synovwej</ta>
            <ta e="T377" id="Seg_7741" s="T375">zhenilsja na Kazashke</ta>
            <ta e="T386" id="Seg_7742" s="T377">s kazahstana teper rabotaet v ohrane shkolu millitsiii okonchiv</ta>
            <ta e="T390" id="Seg_7743" s="T386">ranshe veterinarom byl</ta>
            <ta e="T400" id="Seg_7744" s="T390">potom odin rebenok povarom rabotaet v Khatange, glavnym povarom odna dochka</ta>
            <ta e="T406" id="Seg_7745" s="T400">v Katange MArina</ta>
            <ta e="T416" id="Seg_7746" s="T414">v Krasnojarske uchilasj</ta>
            <ta e="T430" id="Seg_7747" s="T416">potom dochka vrach rabotaet v Dudinke terapevtom seichas, troe detej v vozraste zhenshina na pensii</ta>
            <ta e="T433" id="Seg_7748" s="T430">vse deti pensionery</ta>
            <ta e="T436" id="Seg_7749" s="T433">troe detej na pensii</ta>
            <ta e="T450" id="Seg_7750" s="T436">starshie moi odin iz nih rybachit, drugaja postarela i na pensii Vera, ee muzh nedavno umer</ta>
            <ta e="T454" id="Seg_7751" s="T450">dvoe detej tretij umer</ta>
            <ta e="T463" id="Seg_7752" s="T454">u starshego syna troe detej, odin rebenok zhenatyj s rebenkom</ta>
            <ta e="T467" id="Seg_7753" s="T463">ostalnye bez detej</ta>
            <ta e="T479" id="Seg_7754" s="T471">potom esh'o odin rebenok v sopochnom est</ta>
            <ta e="T482" id="Seg_7755" s="T479">mm vospitatelem rabotaet</ta>
            <ta e="T490" id="Seg_7756" s="T482">potom odin rebenok zdes odna doch' vospitatelem</ta>
            <ta e="T494" id="Seg_7757" s="T490">odin rebonok </ta>
            <ta e="T501" id="Seg_7758" s="T496">v shkole zdes, domna petrovna</ta>
            <ta e="T519" id="Seg_7759" s="T506">potom odin mladshego syna zhena v shkole uchitelem rabotaet Nadja Kirgizova</ta>
            <ta e="T520" id="Seg_7760" s="T519">moja nevestka</ta>
            <ta e="T530" id="Seg_7761" s="T520">odin syn na vezehochike uchilsja, otuchivchis rabotal</ta>
            <ta e="T531" id="Seg_7762" s="T530">bliznetsy</ta>
            <ta e="T541" id="Seg_7763" s="T531">odin elektrikom rabotaet do sih por, posle ucheby</ta>
            <ta e="T545" id="Seg_7764" s="T541">tak do vse otuchilis</ta>
            <ta e="T551" id="Seg_7765" s="T545">potom odin rebenok v Krestah est</ta>
            <ta e="T566" id="Seg_7766" s="T551">shkolu militsii okonchil, on vezde obyvav zhenilsja v Krestah</ta>
            <ta e="T571" id="Seg_7767" s="T567">videla mozhet byt</ta>
            <ta e="T578" id="Seg_7768" s="T571">potom ostalnye ohotniki tri syna</ta>
            <ta e="T588" id="Seg_7769" s="T582">Vija spit tut, zhena vot tut vot stoit</ta>
            <ta e="T591" id="Seg_7770" s="T588">vse potom</ta>
            <ta e="T600" id="Seg_7771" s="T591">dvenadsat detej bolshe dvadsati pjati semi vnuki</ta>
            <ta e="T606" id="Seg_7772" s="T600">potom v nashej derevne bolshe soroka</ta>
            <ta e="T610" id="Seg_7773" s="T606">esli vseh nevestok naidu mnogo…</ta>
            <ta e="T611" id="Seg_7774" s="T610">esli poshitat'. </ta>
            <ta e="T616" id="Seg_7775" s="T611">Odna nevestka dve nevestki pedagogi</ta>
            <ta e="T618" id="Seg_7776" s="T616">vse s detmi</ta>
            <ta e="T621" id="Seg_7777" s="T618">docheri poehali uchitsja</ta>
            <ta e="T624" id="Seg_7778" s="T621">odna v sadike</ta>
            <ta e="T629" id="Seg_7779" s="T624">kak eto nazyvaetjsa</ta>
            <ta e="T630" id="Seg_7780" s="T629">zavedushaja</ta>
            <ta e="T634" id="Seg_7781" s="T630">dve nevestki uchitela pedagogi</ta>
            <ta e="T636" id="Seg_7782" s="T634">vse rabotniki</ta>
            <ta e="T646" id="Seg_7783" s="T636">odin zjat umer (tanga), odin zjat bolnym stal</ta>
            <ta e="T649" id="Seg_7784" s="T646">Domny Petrovny. </ta>
            <ta e="T657" id="Seg_7785" s="T649">Nesmotrja na eto, mnogo dobychi dobyvaet bez nog</ta>
            <ta e="T661" id="Seg_7786" s="T657">odin zjat v sopochnoj ohotnik</ta>
            <ta e="T664" id="Seg_7787" s="T661">dochka uehala uchitsja</ta>
            <ta e="T667" id="Seg_7788" s="T664">chto esh'o rasskazat'</ta>
            <ta e="T671" id="Seg_7789" s="T670">vse!</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KiPP">
            <ta e="T175" id="Seg_7790" s="T167">[DCh]: Not really clear who is meant here.</ta>
            <ta e="T291" id="Seg_7791" s="T286">[DCh]: "Partorg" is an acronyme and stands for "partiyniy organizator" 'party coordinator'.</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-XXXX"
                      id="tx-XXXX"
                      speaker="XXXX"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-XXXX">
            <ts e="T11" id="Seg_7792" n="sc" s="T5">
               <ts e="T11" id="Seg_7794" n="HIAT:u" s="T5">
                  <nts id="Seg_7795" n="HIAT:ip">–</nts>
                  <nts id="Seg_7796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_7797" n="HIAT:ip">(</nts>
                  <nts id="Seg_7798" n="HIAT:ip">(</nts>
                  <ats e="T7" id="Seg_7799" n="HIAT:non-pho" s="T5">…</ats>
                  <nts id="Seg_7800" n="HIAT:ip">)</nts>
                  <nts id="Seg_7801" n="HIAT:ip">)</nts>
                  <nts id="Seg_7802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_7804" n="HIAT:w" s="T7">bara</ts>
                  <nts id="Seg_7805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_7807" n="HIAT:w" s="T8">turar</ts>
                  <nts id="Seg_7808" n="HIAT:ip">?</nts>
                  <nts id="Seg_7809" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T26" id="Seg_7810" n="sc" s="T23">
               <ts e="T26" id="Seg_7812" n="HIAT:u" s="T23">
                  <nts id="Seg_7813" n="HIAT:ip">–</nts>
                  <nts id="Seg_7814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_7816" n="HIAT:w" s="T23">Amakaːnnaːk</ts>
                  <nts id="Seg_7817" n="HIAT:ip">.</nts>
                  <nts id="Seg_7818" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T45" id="Seg_7819" n="sc" s="T32">
               <ts e="T45" id="Seg_7821" n="HIAT:u" s="T32">
                  <nts id="Seg_7822" n="HIAT:ip">–</nts>
                  <nts id="Seg_7823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_7825" n="HIAT:w" s="T32">Dʼe</ts>
                  <nts id="Seg_7826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_7828" n="HIAT:w" s="T34">kas</ts>
                  <nts id="Seg_7829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_7831" n="HIAT:w" s="T35">kas</ts>
                  <nts id="Seg_7832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_7834" n="HIAT:w" s="T36">maŋnaj</ts>
                  <nts id="Seg_7835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_7837" n="HIAT:w" s="T37">gɨtta</ts>
                  <nts id="Seg_7838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_7840" n="HIAT:w" s="T38">kepseː</ts>
                  <nts id="Seg_7841" n="HIAT:ip">,</nts>
                  <nts id="Seg_7842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_7844" n="HIAT:w" s="T39">kas</ts>
                  <nts id="Seg_7845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_7847" n="HIAT:w" s="T40">dʼɨllaːk</ts>
                  <nts id="Seg_7848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_7850" n="HIAT:w" s="T41">etiginij</ts>
                  <nts id="Seg_7851" n="HIAT:ip">,</nts>
                  <nts id="Seg_7852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_7854" n="HIAT:w" s="T42">kanna</ts>
                  <nts id="Seg_7855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_7857" n="HIAT:w" s="T43">kanna</ts>
                  <nts id="Seg_7858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_7860" n="HIAT:w" s="T44">töröllübükkünüj</ts>
                  <nts id="Seg_7861" n="HIAT:ip">?</nts>
                  <nts id="Seg_7862" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T50" id="Seg_7863" n="sc" s="T46">
               <ts e="T50" id="Seg_7865" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_7867" n="HIAT:w" s="T46">Kaččaga</ts>
                  <nts id="Seg_7868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_7870" n="HIAT:w" s="T47">erge</ts>
                  <nts id="Seg_7871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_7873" n="HIAT:w" s="T49">taksɨbɨkkɨnɨj</ts>
                  <nts id="Seg_7874" n="HIAT:ip">?</nts>
                  <nts id="Seg_7875" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T62" id="Seg_7876" n="sc" s="T57">
               <ts e="T62" id="Seg_7878" n="HIAT:u" s="T57">
                  <nts id="Seg_7879" n="HIAT:ip">–</nts>
                  <nts id="Seg_7880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_7882" n="HIAT:w" s="T57">Dʼe</ts>
                  <nts id="Seg_7883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_7885" n="HIAT:w" s="T58">haːtar</ts>
                  <nts id="Seg_7886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_7887" n="HIAT:ip">(</nts>
                  <nts id="Seg_7888" n="HIAT:ip">(</nts>
                  <ats e="T60" id="Seg_7889" n="HIAT:non-pho" s="T59">…</ats>
                  <nts id="Seg_7890" n="HIAT:ip">)</nts>
                  <nts id="Seg_7891" n="HIAT:ip">)</nts>
                  <nts id="Seg_7892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_7893" n="HIAT:ip">(</nts>
                  <ts e="T61" id="Seg_7895" n="HIAT:w" s="T60">kɨratɨːgɨn</ts>
                  <nts id="Seg_7896" n="HIAT:ip">)</nts>
                  <nts id="Seg_7897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_7899" n="HIAT:w" s="T61">bu͡o</ts>
                  <nts id="Seg_7900" n="HIAT:ip">.</nts>
                  <nts id="Seg_7901" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T70" id="Seg_7902" n="sc" s="T63">
               <ts e="T70" id="Seg_7904" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_7906" n="HIAT:w" s="T63">Hapsi͡em</ts>
                  <nts id="Seg_7907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_7909" n="HIAT:w" s="T64">uhunnuk</ts>
                  <nts id="Seg_7910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_7911" n="HIAT:ip">(</nts>
                  <nts id="Seg_7912" n="HIAT:ip">(</nts>
                  <ats e="T67" id="Seg_7913" n="HIAT:non-pho" s="T65">…</ats>
                  <nts id="Seg_7914" n="HIAT:ip">)</nts>
                  <nts id="Seg_7915" n="HIAT:ip">)</nts>
                  <nts id="Seg_7916" n="HIAT:ip">,</nts>
                  <nts id="Seg_7917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_7919" n="HIAT:w" s="T67">kanna</ts>
                  <nts id="Seg_7920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_7922" n="HIAT:w" s="T69">töröːbükkün</ts>
                  <nts id="Seg_7923" n="HIAT:ip">?</nts>
                  <nts id="Seg_7924" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T86" id="Seg_7925" n="sc" s="T81">
               <ts e="T86" id="Seg_7927" n="HIAT:u" s="T81">
                  <nts id="Seg_7928" n="HIAT:ip">–</nts>
                  <nts id="Seg_7929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_7931" n="HIAT:w" s="T81">Bulunnʼa</ts>
                  <nts id="Seg_7932" n="HIAT:ip">,</nts>
                  <nts id="Seg_7933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_7935" n="HIAT:w" s="T83">eta</ts>
                  <nts id="Seg_7936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_7938" n="HIAT:w" s="T84">pa-russki</ts>
                  <nts id="Seg_7939" n="HIAT:ip">.</nts>
                  <nts id="Seg_7940" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T89" id="Seg_7941" n="sc" s="T87">
               <ts e="T89" id="Seg_7943" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_7945" n="HIAT:w" s="T87">Kajdak</ts>
                  <nts id="Seg_7946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_7948" n="HIAT:w" s="T88">di͡eppinij</ts>
                  <nts id="Seg_7949" n="HIAT:ip">?</nts>
                  <nts id="Seg_7950" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T106" id="Seg_7951" n="sc" s="T92">
               <ts e="T98" id="Seg_7953" n="HIAT:u" s="T92">
                  <nts id="Seg_7954" n="HIAT:ip">–</nts>
                  <nts id="Seg_7955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_7957" n="HIAT:w" s="T92">Kim</ts>
                  <nts id="Seg_7958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_7960" n="HIAT:w" s="T93">etej</ts>
                  <nts id="Seg_7961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_7963" n="HIAT:w" s="T94">ke</ts>
                  <nts id="Seg_7964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_7966" n="HIAT:w" s="T95">muntuŋ</ts>
                  <nts id="Seg_7967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_7969" n="HIAT:w" s="T96">aːta</ts>
                  <nts id="Seg_7970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_7972" n="HIAT:w" s="T97">bu͡o</ts>
                  <nts id="Seg_7973" n="HIAT:ip">?</nts>
                  <nts id="Seg_7974" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_7976" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_7978" n="HIAT:w" s="T98">Oː</ts>
                  <nts id="Seg_7979" n="HIAT:ip">,</nts>
                  <nts id="Seg_7980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_7982" n="HIAT:w" s="T99">bludnaja</ts>
                  <nts id="Seg_7983" n="HIAT:ip">,</nts>
                  <nts id="Seg_7984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_7985" n="HIAT:ip">(</nts>
                  <nts id="Seg_7986" n="HIAT:ip">(</nts>
                  <ats e="T101" id="Seg_7987" n="HIAT:non-pho" s="T100">…</ats>
                  <nts id="Seg_7988" n="HIAT:ip">)</nts>
                  <nts id="Seg_7989" n="HIAT:ip">)</nts>
                  <nts id="Seg_7990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_7992" n="HIAT:w" s="T101">vnʼebračnaja</ts>
                  <nts id="Seg_7993" n="HIAT:ip">.</nts>
                  <nts id="Seg_7994" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_7996" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_7998" n="HIAT:w" s="T102">Vnʼebračnɨjtar</ts>
                  <nts id="Seg_7999" n="HIAT:ip">.</nts>
                  <nts id="Seg_8000" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_8002" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_8004" n="HIAT:w" s="T103">Eni</ts>
                  <nts id="Seg_8005" n="HIAT:ip">,</nts>
                  <nts id="Seg_8006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_8007" n="HIAT:ip">(</nts>
                  <nts id="Seg_8008" n="HIAT:ip">(</nts>
                  <ats e="T105" id="Seg_8009" n="HIAT:non-pho" s="T104">…</ats>
                  <nts id="Seg_8010" n="HIAT:ip">)</nts>
                  <nts id="Seg_8011" n="HIAT:ip">)</nts>
                  <nts id="Seg_8012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_8014" n="HIAT:w" s="T105">eta</ts>
                  <nts id="Seg_8015" n="HIAT:ip">.</nts>
                  <nts id="Seg_8016" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T110" id="Seg_8017" n="sc" s="T108">
               <ts e="T110" id="Seg_8019" n="HIAT:u" s="T108">
                  <nts id="Seg_8020" n="HIAT:ip">–</nts>
                  <nts id="Seg_8021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_8023" n="HIAT:w" s="T108">Paːpa</ts>
                  <nts id="Seg_8024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_8026" n="HIAT:w" s="T109">sdʼelal</ts>
                  <nts id="Seg_8027" n="HIAT:ip">.</nts>
                  <nts id="Seg_8028" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T120" id="Seg_8029" n="sc" s="T114">
               <ts e="T120" id="Seg_8031" n="HIAT:u" s="T114">
                  <nts id="Seg_8032" n="HIAT:ip">–</nts>
                  <nts id="Seg_8033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_8035" n="HIAT:w" s="T114">Jejo</ts>
                  <nts id="Seg_8036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_8038" n="HIAT:w" s="T116">babuška</ts>
                  <nts id="Seg_8039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_8041" n="HIAT:w" s="T118">vaspʼitala</ts>
                  <nts id="Seg_8042" n="HIAT:ip">.</nts>
                  <nts id="Seg_8043" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T127" id="Seg_8044" n="sc" s="T124">
               <ts e="T127" id="Seg_8046" n="HIAT:u" s="T124">
                  <nts id="Seg_8047" n="HIAT:ip">–</nts>
                  <nts id="Seg_8048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_8050" n="HIAT:w" s="T124">Itini</ts>
                  <nts id="Seg_8051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_8053" n="HIAT:w" s="T125">barɨtɨn</ts>
                  <nts id="Seg_8054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_8056" n="HIAT:w" s="T126">kepseː</ts>
                  <nts id="Seg_8057" n="HIAT:ip">.</nts>
                  <nts id="Seg_8058" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-XXXX">
            <ts e="T11" id="Seg_8059" n="sc" s="T5">
               <ts e="T7" id="Seg_8061" n="e" s="T5">– ((…)) </ts>
               <ts e="T8" id="Seg_8063" n="e" s="T7">bara </ts>
               <ts e="T11" id="Seg_8065" n="e" s="T8">turar? </ts>
            </ts>
            <ts e="T26" id="Seg_8066" n="sc" s="T23">
               <ts e="T26" id="Seg_8068" n="e" s="T23">– Amakaːnnaːk. </ts>
            </ts>
            <ts e="T45" id="Seg_8069" n="sc" s="T32">
               <ts e="T34" id="Seg_8071" n="e" s="T32">– Dʼe </ts>
               <ts e="T35" id="Seg_8073" n="e" s="T34">kas </ts>
               <ts e="T36" id="Seg_8075" n="e" s="T35">kas </ts>
               <ts e="T37" id="Seg_8077" n="e" s="T36">maŋnaj </ts>
               <ts e="T38" id="Seg_8079" n="e" s="T37">gɨtta </ts>
               <ts e="T39" id="Seg_8081" n="e" s="T38">kepseː, </ts>
               <ts e="T40" id="Seg_8083" n="e" s="T39">kas </ts>
               <ts e="T41" id="Seg_8085" n="e" s="T40">dʼɨllaːk </ts>
               <ts e="T42" id="Seg_8087" n="e" s="T41">etiginij, </ts>
               <ts e="T43" id="Seg_8089" n="e" s="T42">kanna </ts>
               <ts e="T44" id="Seg_8091" n="e" s="T43">kanna </ts>
               <ts e="T45" id="Seg_8093" n="e" s="T44">töröllübükkünüj? </ts>
            </ts>
            <ts e="T50" id="Seg_8094" n="sc" s="T46">
               <ts e="T47" id="Seg_8096" n="e" s="T46">Kaččaga </ts>
               <ts e="T49" id="Seg_8098" n="e" s="T47">erge </ts>
               <ts e="T50" id="Seg_8100" n="e" s="T49">taksɨbɨkkɨnɨj? </ts>
            </ts>
            <ts e="T62" id="Seg_8101" n="sc" s="T57">
               <ts e="T58" id="Seg_8103" n="e" s="T57">– Dʼe </ts>
               <ts e="T59" id="Seg_8105" n="e" s="T58">haːtar </ts>
               <ts e="T60" id="Seg_8107" n="e" s="T59">((…)) </ts>
               <ts e="T61" id="Seg_8109" n="e" s="T60">(kɨratɨːgɨn) </ts>
               <ts e="T62" id="Seg_8111" n="e" s="T61">bu͡o. </ts>
            </ts>
            <ts e="T70" id="Seg_8112" n="sc" s="T63">
               <ts e="T64" id="Seg_8114" n="e" s="T63">Hapsi͡em </ts>
               <ts e="T65" id="Seg_8116" n="e" s="T64">uhunnuk </ts>
               <ts e="T67" id="Seg_8118" n="e" s="T65">((…)), </ts>
               <ts e="T69" id="Seg_8120" n="e" s="T67">kanna </ts>
               <ts e="T70" id="Seg_8122" n="e" s="T69">töröːbükkün? </ts>
            </ts>
            <ts e="T86" id="Seg_8123" n="sc" s="T81">
               <ts e="T83" id="Seg_8125" n="e" s="T81">– Bulunnʼa, </ts>
               <ts e="T84" id="Seg_8127" n="e" s="T83">eta </ts>
               <ts e="T86" id="Seg_8129" n="e" s="T84">pa-russki. </ts>
            </ts>
            <ts e="T89" id="Seg_8130" n="sc" s="T87">
               <ts e="T88" id="Seg_8132" n="e" s="T87">Kajdak </ts>
               <ts e="T89" id="Seg_8134" n="e" s="T88">di͡eppinij? </ts>
            </ts>
            <ts e="T106" id="Seg_8135" n="sc" s="T92">
               <ts e="T93" id="Seg_8137" n="e" s="T92">– Kim </ts>
               <ts e="T94" id="Seg_8139" n="e" s="T93">etej </ts>
               <ts e="T95" id="Seg_8141" n="e" s="T94">ke </ts>
               <ts e="T96" id="Seg_8143" n="e" s="T95">muntuŋ </ts>
               <ts e="T97" id="Seg_8145" n="e" s="T96">aːta </ts>
               <ts e="T98" id="Seg_8147" n="e" s="T97">bu͡o? </ts>
               <ts e="T99" id="Seg_8149" n="e" s="T98">Oː, </ts>
               <ts e="T100" id="Seg_8151" n="e" s="T99">bludnaja, </ts>
               <ts e="T101" id="Seg_8153" n="e" s="T100">((…)) </ts>
               <ts e="T102" id="Seg_8155" n="e" s="T101">vnʼebračnaja. </ts>
               <ts e="T103" id="Seg_8157" n="e" s="T102">Vnʼebračnɨjtar. </ts>
               <ts e="T104" id="Seg_8159" n="e" s="T103">Eni, </ts>
               <ts e="T105" id="Seg_8161" n="e" s="T104">((…)) </ts>
               <ts e="T106" id="Seg_8163" n="e" s="T105">eta. </ts>
            </ts>
            <ts e="T110" id="Seg_8164" n="sc" s="T108">
               <ts e="T109" id="Seg_8166" n="e" s="T108">– Paːpa </ts>
               <ts e="T110" id="Seg_8168" n="e" s="T109">sdʼelal. </ts>
            </ts>
            <ts e="T120" id="Seg_8169" n="sc" s="T114">
               <ts e="T116" id="Seg_8171" n="e" s="T114">– Jejo </ts>
               <ts e="T118" id="Seg_8173" n="e" s="T116">babuška </ts>
               <ts e="T120" id="Seg_8175" n="e" s="T118">vaspʼitala. </ts>
            </ts>
            <ts e="T127" id="Seg_8176" n="sc" s="T124">
               <ts e="T125" id="Seg_8178" n="e" s="T124">– Itini </ts>
               <ts e="T126" id="Seg_8180" n="e" s="T125">barɨtɨn </ts>
               <ts e="T127" id="Seg_8182" n="e" s="T126">kepseː. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-XXXX">
            <ta e="T11" id="Seg_8183" s="T5">KiPP_XXXX_2009_Family_nar.XXXX.001 (001.002)</ta>
            <ta e="T26" id="Seg_8184" s="T23">KiPP_XXXX_2009_Family_nar.XXXX.002 (001.008)</ta>
            <ta e="T45" id="Seg_8185" s="T32">KiPP_XXXX_2009_Family_nar.XXXX.003 (001.011)</ta>
            <ta e="T50" id="Seg_8186" s="T46">KiPP_XXXX_2009_Family_nar.XXXX.004 (001.012)</ta>
            <ta e="T62" id="Seg_8187" s="T57">KiPP_XXXX_2009_Family_nar.XXXX.005 (001.015)</ta>
            <ta e="T70" id="Seg_8188" s="T63">KiPP_XXXX_2009_Family_nar.XXXX.006 (001.016)</ta>
            <ta e="T86" id="Seg_8189" s="T81">KiPP_XXXX_2009_Family_nar.XXXX.007 (001.022)</ta>
            <ta e="T89" id="Seg_8190" s="T87">KiPP_XXXX_2009_Family_nar.XXXX.008 (001.023)</ta>
            <ta e="T98" id="Seg_8191" s="T92">KiPP_XXXX_2009_Family_nar.XXXX.009 (001.025)</ta>
            <ta e="T102" id="Seg_8192" s="T98">KiPP_XXXX_2009_Family_nar.XXXX.010 (001.026)</ta>
            <ta e="T103" id="Seg_8193" s="T102">KiPP_XXXX_2009_Family_nar.XXXX.011 (001.027)</ta>
            <ta e="T106" id="Seg_8194" s="T103">KiPP_XXXX_2009_Family_nar.XXXX.012 (001.028)</ta>
            <ta e="T110" id="Seg_8195" s="T108">KiPP_XXXX_2009_Family_nar.XXXX.013 (001.030)</ta>
            <ta e="T120" id="Seg_8196" s="T114">KiPP_XXXX_2009_Family_nar.XXXX.014 (001.032)</ta>
            <ta e="T127" id="Seg_8197" s="T124">KiPP_XXXX_2009_Family_nar.XXXX.015 (001.034)</ta>
         </annotation>
         <annotation name="st" tierref="st-XXXX" />
         <annotation name="ts" tierref="ts-XXXX">
            <ta e="T11" id="Seg_8198" s="T5">– ((…)) bara turar? </ta>
            <ta e="T26" id="Seg_8199" s="T23">– Amakaːnnaːk. </ta>
            <ta e="T45" id="Seg_8200" s="T32">– Dʼe kas kas maŋnaj gɨtta kepseː, kas dʼɨllaːk etiginij, kanna kanna töröllübükkünüj? </ta>
            <ta e="T50" id="Seg_8201" s="T46">Kaččaga erge taksɨbɨkkɨnɨj? </ta>
            <ta e="T62" id="Seg_8202" s="T57">– Dʼe haːtar ((…)) (kɨratɨːgɨn) bu͡o. </ta>
            <ta e="T70" id="Seg_8203" s="T63">Hapsi͡em uhunnuk ((…)), kanna töröːbükkün? </ta>
            <ta e="T86" id="Seg_8204" s="T81">– Bulunnʼa, eta pa-russki. </ta>
            <ta e="T89" id="Seg_8205" s="T87">Kajdak di͡eppinij? </ta>
            <ta e="T98" id="Seg_8206" s="T92">– Kim etej ke muntuŋ aːta bu͡o? </ta>
            <ta e="T102" id="Seg_8207" s="T98">Oː, bludnaja, ((…)) vnʼebračnaja. </ta>
            <ta e="T103" id="Seg_8208" s="T102">Vnʼebračnɨjtar. </ta>
            <ta e="T106" id="Seg_8209" s="T103">Eni, ((…)) eta. </ta>
            <ta e="T110" id="Seg_8210" s="T108">– Paːpa sdʼelal. </ta>
            <ta e="T120" id="Seg_8211" s="T114">– Jejo babuška vaspʼitala. </ta>
            <ta e="T127" id="Seg_8212" s="T124">– Itini barɨtɨn kepseː. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-XXXX">
            <ta e="T8" id="Seg_8213" s="T7">bar-a</ta>
            <ta e="T11" id="Seg_8214" s="T8">tur-ar</ta>
            <ta e="T26" id="Seg_8215" s="T23">ama-kaːn-naːk</ta>
            <ta e="T34" id="Seg_8216" s="T32">dʼe</ta>
            <ta e="T35" id="Seg_8217" s="T34">kas</ta>
            <ta e="T36" id="Seg_8218" s="T35">kas</ta>
            <ta e="T37" id="Seg_8219" s="T36">maŋnaj</ta>
            <ta e="T38" id="Seg_8220" s="T37">gɨtta</ta>
            <ta e="T39" id="Seg_8221" s="T38">kepseː</ta>
            <ta e="T40" id="Seg_8222" s="T39">kas</ta>
            <ta e="T41" id="Seg_8223" s="T40">dʼɨl-laːk</ta>
            <ta e="T42" id="Seg_8224" s="T41">e-ti-gin=ij</ta>
            <ta e="T43" id="Seg_8225" s="T42">kanna</ta>
            <ta e="T44" id="Seg_8226" s="T43">kanna</ta>
            <ta e="T45" id="Seg_8227" s="T44">tör-ö-ll-ü-bük-kün=üj</ta>
            <ta e="T47" id="Seg_8228" s="T46">kaččaga</ta>
            <ta e="T49" id="Seg_8229" s="T47">er-ge</ta>
            <ta e="T50" id="Seg_8230" s="T49">taks-ɨ-bɨk-kɨn=ɨj</ta>
            <ta e="T58" id="Seg_8231" s="T57">dʼe</ta>
            <ta e="T59" id="Seg_8232" s="T58">haːtar</ta>
            <ta e="T61" id="Seg_8233" s="T60">kɨra-t-ɨː-gɨn</ta>
            <ta e="T62" id="Seg_8234" s="T61">bu͡o</ta>
            <ta e="T64" id="Seg_8235" s="T63">hapsi͡em</ta>
            <ta e="T65" id="Seg_8236" s="T64">uhun-nuk</ta>
            <ta e="T69" id="Seg_8237" s="T67">kanna</ta>
            <ta e="T70" id="Seg_8238" s="T69">töröː-bük-kün</ta>
            <ta e="T83" id="Seg_8239" s="T81">bulunnʼa</ta>
            <ta e="T88" id="Seg_8240" s="T87">kajdak</ta>
            <ta e="T89" id="Seg_8241" s="T88">d-i͡ep-pin=ij</ta>
            <ta e="T93" id="Seg_8242" s="T92">kim</ta>
            <ta e="T94" id="Seg_8243" s="T93">e-t-e=j</ta>
            <ta e="T95" id="Seg_8244" s="T94">ke</ta>
            <ta e="T96" id="Seg_8245" s="T95">mun-tu-ŋ</ta>
            <ta e="T97" id="Seg_8246" s="T96">aːt-a</ta>
            <ta e="T98" id="Seg_8247" s="T97">bu͡o</ta>
            <ta e="T99" id="Seg_8248" s="T98">oː</ta>
            <ta e="T103" id="Seg_8249" s="T102">vnʼebračnɨj-tar</ta>
            <ta e="T104" id="Seg_8250" s="T103">eni</ta>
            <ta e="T125" id="Seg_8251" s="T124">iti-ni</ta>
            <ta e="T126" id="Seg_8252" s="T125">barɨ-tɨ-n</ta>
            <ta e="T127" id="Seg_8253" s="T126">kepseː</ta>
         </annotation>
         <annotation name="mp" tierref="mp-XXXX">
            <ta e="T8" id="Seg_8254" s="T7">bar-A</ta>
            <ta e="T11" id="Seg_8255" s="T8">tur-Ar</ta>
            <ta e="T26" id="Seg_8256" s="T23">ama-kAːN-LAːK</ta>
            <ta e="T34" id="Seg_8257" s="T32">dʼe</ta>
            <ta e="T35" id="Seg_8258" s="T34">kas</ta>
            <ta e="T36" id="Seg_8259" s="T35">kas</ta>
            <ta e="T37" id="Seg_8260" s="T36">maŋnaj</ta>
            <ta e="T38" id="Seg_8261" s="T37">kɨtta</ta>
            <ta e="T39" id="Seg_8262" s="T38">kepseː</ta>
            <ta e="T40" id="Seg_8263" s="T39">kas</ta>
            <ta e="T41" id="Seg_8264" s="T40">dʼɨl-LAːK</ta>
            <ta e="T42" id="Seg_8265" s="T41">e-TI-GIn=Ij</ta>
            <ta e="T43" id="Seg_8266" s="T42">kanna</ta>
            <ta e="T44" id="Seg_8267" s="T43">kanna</ta>
            <ta e="T45" id="Seg_8268" s="T44">töröː-A-LIN-I-BIT-GIn=Ij</ta>
            <ta e="T47" id="Seg_8269" s="T46">kaččaga</ta>
            <ta e="T49" id="Seg_8270" s="T47">er-GA</ta>
            <ta e="T50" id="Seg_8271" s="T49">tagɨs-I-BIT-GIn=Ij</ta>
            <ta e="T58" id="Seg_8272" s="T57">dʼe</ta>
            <ta e="T59" id="Seg_8273" s="T58">haːtar</ta>
            <ta e="T61" id="Seg_8274" s="T60">kɨra-LAː-A-GIn</ta>
            <ta e="T62" id="Seg_8275" s="T61">bu͡o</ta>
            <ta e="T64" id="Seg_8276" s="T63">hapsi͡em</ta>
            <ta e="T65" id="Seg_8277" s="T64">uhun-LIk</ta>
            <ta e="T69" id="Seg_8278" s="T67">kanna</ta>
            <ta e="T70" id="Seg_8279" s="T69">töröː-BIT-GIn</ta>
            <ta e="T83" id="Seg_8280" s="T81">bulunnʼa</ta>
            <ta e="T88" id="Seg_8281" s="T87">kajdak</ta>
            <ta e="T89" id="Seg_8282" s="T88">di͡e-IAK-BIn=Ij</ta>
            <ta e="T93" id="Seg_8283" s="T92">kim</ta>
            <ta e="T94" id="Seg_8284" s="T93">e-TI-tA=Ij</ta>
            <ta e="T95" id="Seg_8285" s="T94">ka</ta>
            <ta e="T96" id="Seg_8286" s="T95">bu-tI-ŋ</ta>
            <ta e="T97" id="Seg_8287" s="T96">aːt-tA</ta>
            <ta e="T98" id="Seg_8288" s="T97">bu͡o</ta>
            <ta e="T99" id="Seg_8289" s="T98">oː</ta>
            <ta e="T103" id="Seg_8290" s="T102">vnʼebračnɨj-LAr</ta>
            <ta e="T104" id="Seg_8291" s="T103">eni</ta>
            <ta e="T125" id="Seg_8292" s="T124">iti-nI</ta>
            <ta e="T126" id="Seg_8293" s="T125">barɨ-tI-n</ta>
            <ta e="T127" id="Seg_8294" s="T126">kepseː</ta>
         </annotation>
         <annotation name="ge" tierref="ge-XXXX">
            <ta e="T8" id="Seg_8295" s="T7">go-CVB.SIM</ta>
            <ta e="T11" id="Seg_8296" s="T8">stand-PRS.[3SG]</ta>
            <ta e="T26" id="Seg_8297" s="T23">normal-INTNS-PROPR.[NOM]</ta>
            <ta e="T34" id="Seg_8298" s="T32">well</ta>
            <ta e="T35" id="Seg_8299" s="T34">how.much</ta>
            <ta e="T36" id="Seg_8300" s="T35">how.much</ta>
            <ta e="T37" id="Seg_8301" s="T36">at.first</ta>
            <ta e="T38" id="Seg_8302" s="T37">with</ta>
            <ta e="T39" id="Seg_8303" s="T38">tell.[IMP.2SG]</ta>
            <ta e="T40" id="Seg_8304" s="T39">how.much</ta>
            <ta e="T41" id="Seg_8305" s="T40">year-PROPR.[NOM]</ta>
            <ta e="T42" id="Seg_8306" s="T41">be-PST1-2SG=Q</ta>
            <ta e="T43" id="Seg_8307" s="T42">where</ta>
            <ta e="T44" id="Seg_8308" s="T43">where</ta>
            <ta e="T45" id="Seg_8309" s="T44">be.born-EP-PASS/REFL-EP-PST2-2SG=Q</ta>
            <ta e="T47" id="Seg_8310" s="T46">when</ta>
            <ta e="T49" id="Seg_8311" s="T47">husband-DAT/LOC</ta>
            <ta e="T50" id="Seg_8312" s="T49">go.out-EP-PST2-2SG=Q</ta>
            <ta e="T58" id="Seg_8313" s="T57">well</ta>
            <ta e="T59" id="Seg_8314" s="T58">though</ta>
            <ta e="T61" id="Seg_8315" s="T60">small-VBZ-PRS-2SG</ta>
            <ta e="T62" id="Seg_8316" s="T61">EMPH</ta>
            <ta e="T64" id="Seg_8317" s="T63">at.all</ta>
            <ta e="T65" id="Seg_8318" s="T64">long-ADVZ</ta>
            <ta e="T69" id="Seg_8319" s="T67">where</ta>
            <ta e="T70" id="Seg_8320" s="T69">give.birth-PST2-2SG</ta>
            <ta e="T83" id="Seg_8321" s="T81">illegitimate.[NOM]</ta>
            <ta e="T88" id="Seg_8322" s="T87">how</ta>
            <ta e="T89" id="Seg_8323" s="T88">say-FUT-1SG=Q</ta>
            <ta e="T93" id="Seg_8324" s="T92">who.[NOM]</ta>
            <ta e="T94" id="Seg_8325" s="T93">be-PST1-3SG=Q</ta>
            <ta e="T95" id="Seg_8326" s="T94">well</ta>
            <ta e="T96" id="Seg_8327" s="T95">this-3SG-2SG.[NOM]</ta>
            <ta e="T97" id="Seg_8328" s="T96">name-3SG.[NOM]</ta>
            <ta e="T98" id="Seg_8329" s="T97">EMPH</ta>
            <ta e="T99" id="Seg_8330" s="T98">oh</ta>
            <ta e="T103" id="Seg_8331" s="T102">illegitimate-PL.[NOM]</ta>
            <ta e="T104" id="Seg_8332" s="T103">apparently</ta>
            <ta e="T125" id="Seg_8333" s="T124">that-ACC</ta>
            <ta e="T126" id="Seg_8334" s="T125">whole-3SG-ACC</ta>
            <ta e="T127" id="Seg_8335" s="T126">tell.[IMP.2SG]</ta>
         </annotation>
         <annotation name="gg" tierref="gg-XXXX">
            <ta e="T8" id="Seg_8336" s="T7">gehen-CVB.SIM</ta>
            <ta e="T11" id="Seg_8337" s="T8">stehen-PRS.[3SG]</ta>
            <ta e="T26" id="Seg_8338" s="T23">normal-INTNS-PROPR.[NOM]</ta>
            <ta e="T34" id="Seg_8339" s="T32">doch</ta>
            <ta e="T35" id="Seg_8340" s="T34">wie.viel</ta>
            <ta e="T36" id="Seg_8341" s="T35">wie.viel</ta>
            <ta e="T37" id="Seg_8342" s="T36">zuerst</ta>
            <ta e="T38" id="Seg_8343" s="T37">mit</ta>
            <ta e="T39" id="Seg_8344" s="T38">erzählen.[IMP.2SG]</ta>
            <ta e="T40" id="Seg_8345" s="T39">wie.viel</ta>
            <ta e="T41" id="Seg_8346" s="T40">Jahr-PROPR.[NOM]</ta>
            <ta e="T42" id="Seg_8347" s="T41">sein-PST1-2SG=Q</ta>
            <ta e="T43" id="Seg_8348" s="T42">wo</ta>
            <ta e="T44" id="Seg_8349" s="T43">wo</ta>
            <ta e="T45" id="Seg_8350" s="T44">geboren.werden-EP-PASS/REFL-EP-PST2-2SG=Q</ta>
            <ta e="T47" id="Seg_8351" s="T46">wann</ta>
            <ta e="T49" id="Seg_8352" s="T47">Ehemann-DAT/LOC</ta>
            <ta e="T50" id="Seg_8353" s="T49">hinausgehen-EP-PST2-2SG=Q</ta>
            <ta e="T58" id="Seg_8354" s="T57">doch</ta>
            <ta e="T59" id="Seg_8355" s="T58">wenn.auch</ta>
            <ta e="T61" id="Seg_8356" s="T60">klein-VBZ-PRS-2SG</ta>
            <ta e="T62" id="Seg_8357" s="T61">EMPH</ta>
            <ta e="T64" id="Seg_8358" s="T63">ganz</ta>
            <ta e="T65" id="Seg_8359" s="T64">lang-ADVZ</ta>
            <ta e="T69" id="Seg_8360" s="T67">wo</ta>
            <ta e="T70" id="Seg_8361" s="T69">gebären-PST2-2SG</ta>
            <ta e="T83" id="Seg_8362" s="T81">unehelich.[NOM]</ta>
            <ta e="T88" id="Seg_8363" s="T87">wie</ta>
            <ta e="T89" id="Seg_8364" s="T88">sagen-FUT-1SG=Q</ta>
            <ta e="T93" id="Seg_8365" s="T92">wer.[NOM]</ta>
            <ta e="T94" id="Seg_8366" s="T93">sein-PST1-3SG=Q</ta>
            <ta e="T95" id="Seg_8367" s="T94">nun</ta>
            <ta e="T96" id="Seg_8368" s="T95">dieses-3SG-2SG.[NOM]</ta>
            <ta e="T97" id="Seg_8369" s="T96">Name-3SG.[NOM]</ta>
            <ta e="T98" id="Seg_8370" s="T97">EMPH</ta>
            <ta e="T99" id="Seg_8371" s="T98">oh</ta>
            <ta e="T103" id="Seg_8372" s="T102">unehelich-PL.[NOM]</ta>
            <ta e="T104" id="Seg_8373" s="T103">offenbar</ta>
            <ta e="T125" id="Seg_8374" s="T124">dieses-ACC</ta>
            <ta e="T126" id="Seg_8375" s="T125">ganz-3SG-ACC</ta>
            <ta e="T127" id="Seg_8376" s="T126">erzählen.[IMP.2SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-XXXX">
            <ta e="T8" id="Seg_8377" s="T7">идти-CVB.SIM</ta>
            <ta e="T11" id="Seg_8378" s="T8">стоять-PRS.[3SG]</ta>
            <ta e="T26" id="Seg_8379" s="T23">нормальный-INTNS-PROPR.[NOM]</ta>
            <ta e="T34" id="Seg_8380" s="T32">вот</ta>
            <ta e="T35" id="Seg_8381" s="T34">сколько</ta>
            <ta e="T36" id="Seg_8382" s="T35">сколько</ta>
            <ta e="T37" id="Seg_8383" s="T36">сначала</ta>
            <ta e="T38" id="Seg_8384" s="T37">с</ta>
            <ta e="T39" id="Seg_8385" s="T38">рассказывать.[IMP.2SG]</ta>
            <ta e="T40" id="Seg_8386" s="T39">сколько</ta>
            <ta e="T41" id="Seg_8387" s="T40">год-PROPR.[NOM]</ta>
            <ta e="T42" id="Seg_8388" s="T41">быть-PST1-2SG=Q</ta>
            <ta e="T43" id="Seg_8389" s="T42">где</ta>
            <ta e="T44" id="Seg_8390" s="T43">где</ta>
            <ta e="T45" id="Seg_8391" s="T44">родиться-EP-PASS/REFL-EP-PST2-2SG=Q</ta>
            <ta e="T47" id="Seg_8392" s="T46">когда</ta>
            <ta e="T49" id="Seg_8393" s="T47">муж-DAT/LOC</ta>
            <ta e="T50" id="Seg_8394" s="T49">выйти-EP-PST2-2SG=Q</ta>
            <ta e="T58" id="Seg_8395" s="T57">вот</ta>
            <ta e="T59" id="Seg_8396" s="T58">хоть</ta>
            <ta e="T61" id="Seg_8397" s="T60">маленький-VBZ-PRS-2SG</ta>
            <ta e="T62" id="Seg_8398" s="T61">EMPH</ta>
            <ta e="T64" id="Seg_8399" s="T63">совсем</ta>
            <ta e="T65" id="Seg_8400" s="T64">длинный-ADVZ</ta>
            <ta e="T69" id="Seg_8401" s="T67">где</ta>
            <ta e="T70" id="Seg_8402" s="T69">родить-PST2-2SG</ta>
            <ta e="T83" id="Seg_8403" s="T81">незаконнорожденный.[NOM]</ta>
            <ta e="T88" id="Seg_8404" s="T87">как</ta>
            <ta e="T89" id="Seg_8405" s="T88">говорить-FUT-1SG=Q</ta>
            <ta e="T93" id="Seg_8406" s="T92">кто.[NOM]</ta>
            <ta e="T94" id="Seg_8407" s="T93">быть-PST1-3SG=Q</ta>
            <ta e="T95" id="Seg_8408" s="T94">вот</ta>
            <ta e="T96" id="Seg_8409" s="T95">этот-3SG-2SG.[NOM]</ta>
            <ta e="T97" id="Seg_8410" s="T96">имя-3SG.[NOM]</ta>
            <ta e="T98" id="Seg_8411" s="T97">EMPH</ta>
            <ta e="T99" id="Seg_8412" s="T98">о</ta>
            <ta e="T103" id="Seg_8413" s="T102">внебрачный-PL.[NOM]</ta>
            <ta e="T104" id="Seg_8414" s="T103">очевидно</ta>
            <ta e="T125" id="Seg_8415" s="T124">тот-ACC</ta>
            <ta e="T126" id="Seg_8416" s="T125">целый-3SG-ACC</ta>
            <ta e="T127" id="Seg_8417" s="T126">рассказывать.[IMP.2SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-XXXX">
            <ta e="T8" id="Seg_8418" s="T7">v-v:cvb</ta>
            <ta e="T11" id="Seg_8419" s="T8">v-v:tense.[v:pred.pn]</ta>
            <ta e="T26" id="Seg_8420" s="T23">adj-adj&gt;adj-adj&gt;adj.[n:case]</ta>
            <ta e="T34" id="Seg_8421" s="T32">ptcl</ta>
            <ta e="T35" id="Seg_8422" s="T34">que</ta>
            <ta e="T36" id="Seg_8423" s="T35">que</ta>
            <ta e="T37" id="Seg_8424" s="T36">adv</ta>
            <ta e="T38" id="Seg_8425" s="T37">post</ta>
            <ta e="T39" id="Seg_8426" s="T38">v.[v:mood.pn]</ta>
            <ta e="T40" id="Seg_8427" s="T39">que</ta>
            <ta e="T41" id="Seg_8428" s="T40">n-n&gt;adj.[n:case]</ta>
            <ta e="T42" id="Seg_8429" s="T41">v-v:tense-v:pred.pn=ptcl</ta>
            <ta e="T43" id="Seg_8430" s="T42">que</ta>
            <ta e="T44" id="Seg_8431" s="T43">que</ta>
            <ta e="T45" id="Seg_8432" s="T44">v-v:(ins)-v&gt;v-v:(ins)-v:tense-v:pred.pn=ptcl</ta>
            <ta e="T47" id="Seg_8433" s="T46">que</ta>
            <ta e="T49" id="Seg_8434" s="T47">n-n:case</ta>
            <ta e="T50" id="Seg_8435" s="T49">v-v:(ins)-v:tense-v:pred.pn=ptcl</ta>
            <ta e="T58" id="Seg_8436" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_8437" s="T58">ptcl</ta>
            <ta e="T61" id="Seg_8438" s="T60">adj-adj&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T62" id="Seg_8439" s="T61">ptcl</ta>
            <ta e="T64" id="Seg_8440" s="T63">adv</ta>
            <ta e="T65" id="Seg_8441" s="T64">adj-adj&gt;adv</ta>
            <ta e="T69" id="Seg_8442" s="T67">que</ta>
            <ta e="T70" id="Seg_8443" s="T69">v-v:tense-v:pred.pn</ta>
            <ta e="T83" id="Seg_8444" s="T81">adj.[n:case]</ta>
            <ta e="T88" id="Seg_8445" s="T87">que</ta>
            <ta e="T89" id="Seg_8446" s="T88">v-v:tense-v:pred.pn=ptcl</ta>
            <ta e="T93" id="Seg_8447" s="T92">que.[pro:case]</ta>
            <ta e="T94" id="Seg_8448" s="T93">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T95" id="Seg_8449" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_8450" s="T95">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T97" id="Seg_8451" s="T96">n-n:(poss).[n:case]</ta>
            <ta e="T98" id="Seg_8452" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_8453" s="T98">interj</ta>
            <ta e="T103" id="Seg_8454" s="T102">adj-n:(num).[n:case]</ta>
            <ta e="T104" id="Seg_8455" s="T103">ptcl</ta>
            <ta e="T125" id="Seg_8456" s="T124">dempro-pro:case</ta>
            <ta e="T126" id="Seg_8457" s="T125">adj-n:poss-n:case</ta>
            <ta e="T127" id="Seg_8458" s="T126">v.[v:mood.pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps-XXXX">
            <ta e="T8" id="Seg_8459" s="T7">v</ta>
            <ta e="T11" id="Seg_8460" s="T8">aux</ta>
            <ta e="T26" id="Seg_8461" s="T23">n</ta>
            <ta e="T34" id="Seg_8462" s="T32">ptcl</ta>
            <ta e="T35" id="Seg_8463" s="T34">que</ta>
            <ta e="T36" id="Seg_8464" s="T35">que</ta>
            <ta e="T37" id="Seg_8465" s="T36">adv</ta>
            <ta e="T38" id="Seg_8466" s="T37">post</ta>
            <ta e="T39" id="Seg_8467" s="T38">v</ta>
            <ta e="T40" id="Seg_8468" s="T39">que</ta>
            <ta e="T41" id="Seg_8469" s="T40">adj</ta>
            <ta e="T42" id="Seg_8470" s="T41">cop</ta>
            <ta e="T43" id="Seg_8471" s="T42">que</ta>
            <ta e="T44" id="Seg_8472" s="T43">que</ta>
            <ta e="T45" id="Seg_8473" s="T44">v</ta>
            <ta e="T47" id="Seg_8474" s="T46">que</ta>
            <ta e="T49" id="Seg_8475" s="T47">n</ta>
            <ta e="T50" id="Seg_8476" s="T49">v</ta>
            <ta e="T58" id="Seg_8477" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_8478" s="T58">ptcl</ta>
            <ta e="T61" id="Seg_8479" s="T60">v</ta>
            <ta e="T62" id="Seg_8480" s="T61">ptcl</ta>
            <ta e="T64" id="Seg_8481" s="T63">adv</ta>
            <ta e="T65" id="Seg_8482" s="T64">adv</ta>
            <ta e="T69" id="Seg_8483" s="T67">que</ta>
            <ta e="T70" id="Seg_8484" s="T69">v</ta>
            <ta e="T83" id="Seg_8485" s="T81">adj</ta>
            <ta e="T88" id="Seg_8486" s="T87">que</ta>
            <ta e="T89" id="Seg_8487" s="T88">v</ta>
            <ta e="T93" id="Seg_8488" s="T92">que</ta>
            <ta e="T94" id="Seg_8489" s="T93">cop</ta>
            <ta e="T95" id="Seg_8490" s="T94">ptcl</ta>
            <ta e="T96" id="Seg_8491" s="T95">dempro</ta>
            <ta e="T97" id="Seg_8492" s="T96">n</ta>
            <ta e="T98" id="Seg_8493" s="T97">ptcl</ta>
            <ta e="T99" id="Seg_8494" s="T98">interj</ta>
            <ta e="T103" id="Seg_8495" s="T102">n</ta>
            <ta e="T104" id="Seg_8496" s="T103">ptcl</ta>
            <ta e="T125" id="Seg_8497" s="T124">dempro</ta>
            <ta e="T126" id="Seg_8498" s="T125">adj</ta>
            <ta e="T127" id="Seg_8499" s="T126">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-XXXX" />
         <annotation name="SyF" tierref="SyF-XXXX" />
         <annotation name="IST" tierref="IST-XXXX" />
         <annotation name="Top" tierref="Top-XXXX" />
         <annotation name="Foc" tierref="Foc-XXXX" />
         <annotation name="BOR" tierref="BOR-XXXX">
            <ta e="T26" id="Seg_8500" s="T23">EV:gram (DIM)</ta>
            <ta e="T64" id="Seg_8501" s="T63">RUS:mod</ta>
            <ta e="T103" id="Seg_8502" s="T102">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-XXXX" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-XXXX" />
         <annotation name="CS" tierref="CS-XXXX" />
         <annotation name="fe" tierref="fe-XXXX">
            <ta e="T11" id="Seg_8503" s="T5">– (…) runs?</ta>
            <ta e="T26" id="Seg_8504" s="T23">– Something normal.</ta>
            <ta e="T45" id="Seg_8505" s="T32">– Well, tell in the beginning, how old are you, where were you born?</ta>
            <ta e="T50" id="Seg_8506" s="T46">Wenn did you marry?</ta>
            <ta e="T62" id="Seg_8507" s="T57">– Well, maybe you (shorten?) [it]?</ta>
            <ta e="T70" id="Seg_8508" s="T63">Very long at all (…), where were you born?</ta>
            <ta e="T86" id="Seg_8509" s="T81">– "Bulunnʼa", that's in Russian.</ta>
            <ta e="T89" id="Seg_8510" s="T87">How shall I say?</ta>
            <ta e="T98" id="Seg_8511" s="T92">– How it was called then?</ta>
            <ta e="T102" id="Seg_8512" s="T98">Oh, lost, illegitimate.</ta>
            <ta e="T103" id="Seg_8513" s="T102">Illegitimates.</ta>
            <ta e="T106" id="Seg_8514" s="T103">Apparently, (…) that.</ta>
            <ta e="T110" id="Seg_8515" s="T108">– Papa made [it].</ta>
            <ta e="T120" id="Seg_8516" s="T114">– Her granny brought her up.</ta>
            <ta e="T127" id="Seg_8517" s="T124">– Tell all that.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-XXXX">
            <ta e="T11" id="Seg_8518" s="T5">– (…) läuft?</ta>
            <ta e="T26" id="Seg_8519" s="T23">– Was Normales.</ta>
            <ta e="T45" id="Seg_8520" s="T32">– Erzähl doch zuerst, wie alt bist du, wo bist du geboren?</ta>
            <ta e="T50" id="Seg_8521" s="T46">Wann hast du geheiratet?</ta>
            <ta e="T62" id="Seg_8522" s="T57">– Nun, vielleicht (verkürzt?) du [es]?</ta>
            <ta e="T70" id="Seg_8523" s="T63">Ganz lang (…), wo wurdest du geboren?</ta>
            <ta e="T86" id="Seg_8524" s="T81">– "Bulunnʼa", das ist auf Russisch.</ta>
            <ta e="T89" id="Seg_8525" s="T87">Wie soll ich sagen?</ta>
            <ta e="T98" id="Seg_8526" s="T92">– Wie hieß das noch mal?</ta>
            <ta e="T102" id="Seg_8527" s="T98">Oh, verloren, (…) unehelich.</ta>
            <ta e="T103" id="Seg_8528" s="T102">Uneheliche.</ta>
            <ta e="T106" id="Seg_8529" s="T103">Wahrscheinlich, (…) das.</ta>
            <ta e="T110" id="Seg_8530" s="T108">– Papa hat [es] gemacht.</ta>
            <ta e="T120" id="Seg_8531" s="T114">– Ihre Großmutter hat sie großgezogen.</ta>
            <ta e="T127" id="Seg_8532" s="T124">– Das erzähl alles.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-XXXX" />
         <annotation name="ltr" tierref="ltr-XXXX">
            <ta e="T45" id="Seg_8533" s="T32">skolko skolko snachala rasskazhi skolko tebe let, gde gde ty rodilas'</ta>
            <ta e="T50" id="Seg_8534" s="T46">kogda vyshla zamuzh</ta>
            <ta e="T62" id="Seg_8535" s="T57">mozhet byt', ukorotish?</ta>
            <ta e="T86" id="Seg_8536" s="T81">bulonja eto po russki</ta>
         </annotation>
         <annotation name="nt" tierref="nt-XXXX" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-SE"
                      id="tx-SE"
                      speaker="SE"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-SE">
            <ts e="T12" id="Seg_8537" n="sc" s="T10">
               <ts e="T12" id="Seg_8539" n="HIAT:u" s="T10">
                  <nts id="Seg_8540" n="HIAT:ip">–</nts>
                  <nts id="Seg_8541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_8543" n="HIAT:w" s="T10">Da</ts>
                  <nts id="Seg_8544" n="HIAT:ip">.</nts>
                  <nts id="Seg_8545" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T14" id="Seg_8546" n="sc" s="T13">
               <ts e="T14" id="Seg_8548" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_8550" n="HIAT:w" s="T13">Vklʼučʼila</ts>
                  <nts id="Seg_8551" n="HIAT:ip">.</nts>
                  <nts id="Seg_8552" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T257" id="Seg_8553" n="sc" s="T254">
               <ts e="T257" id="Seg_8555" n="HIAT:u" s="T254">
                  <nts id="Seg_8556" n="HIAT:ip">–</nts>
                  <nts id="Seg_8557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_8559" n="HIAT:w" s="T254">Manna</ts>
                  <nts id="Seg_8560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_8562" n="HIAT:w" s="T255">ogolonon</ts>
                  <nts id="Seg_8563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_8564" n="HIAT:ip">(</nts>
                  <ts e="T257" id="Seg_8566" n="HIAT:w" s="T256">olo-</ts>
                  <nts id="Seg_8567" n="HIAT:ip">)</nts>
                  <nts id="Seg_8568" n="HIAT:ip">…</nts>
                  <nts id="Seg_8569" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T408" id="Seg_8570" n="sc" s="T407">
               <ts e="T408" id="Seg_8572" n="HIAT:u" s="T407">
                  <nts id="Seg_8573" n="HIAT:ip">–</nts>
                  <nts id="Seg_8574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_8576" n="HIAT:w" s="T407">Instʼitukka</ts>
                  <nts id="Seg_8577" n="HIAT:ip">?</nts>
                  <nts id="Seg_8578" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T469" id="Seg_8579" n="sc" s="T467">
               <ts e="T469" id="Seg_8581" n="HIAT:u" s="T467">
                  <nts id="Seg_8582" n="HIAT:ip">–</nts>
                  <nts id="Seg_8583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_8585" n="HIAT:w" s="T467">Ulakan</ts>
                  <nts id="Seg_8586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_8588" n="HIAT:w" s="T468">kergen</ts>
                  <nts id="Seg_8589" n="HIAT:ip">.</nts>
                  <nts id="Seg_8590" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T495" id="Seg_8591" n="sc" s="T493">
               <ts e="T495" id="Seg_8593" n="HIAT:u" s="T493">
                  <nts id="Seg_8594" n="HIAT:ip">–</nts>
                  <nts id="Seg_8595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_8597" n="HIAT:w" s="T493">Školaga</ts>
                  <nts id="Seg_8598" n="HIAT:ip">?</nts>
                  <nts id="Seg_8599" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T507" id="Seg_8600" n="sc" s="T502">
               <ts e="T507" id="Seg_8602" n="HIAT:u" s="T502">
                  <nts id="Seg_8603" n="HIAT:ip">–</nts>
                  <nts id="Seg_8604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_8606" n="HIAT:w" s="T502">Aː</ts>
                  <nts id="Seg_8607" n="HIAT:ip">,</nts>
                  <nts id="Seg_8608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_8610" n="HIAT:w" s="T503">vʼidʼela</ts>
                  <nts id="Seg_8611" n="HIAT:ip">,</nts>
                  <nts id="Seg_8612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_8614" n="HIAT:w" s="T504">vʼidʼela</ts>
                  <nts id="Seg_8615" n="HIAT:ip">,</nts>
                  <nts id="Seg_8616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_8618" n="HIAT:w" s="T505">kördüm</ts>
                  <nts id="Seg_8619" n="HIAT:ip">.</nts>
                  <nts id="Seg_8620" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T669" id="Seg_8621" n="sc" s="T668">
               <ts e="T669" id="Seg_8623" n="HIAT:u" s="T668">
                  <nts id="Seg_8624" n="HIAT:ip">–</nts>
                  <nts id="Seg_8625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_8627" n="HIAT:w" s="T668">Pasʼiba</ts>
                  <nts id="Seg_8628" n="HIAT:ip">.</nts>
                  <nts id="Seg_8629" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-SE">
            <ts e="T12" id="Seg_8630" n="sc" s="T10">
               <ts e="T12" id="Seg_8632" n="e" s="T10">– Da. </ts>
            </ts>
            <ts e="T14" id="Seg_8633" n="sc" s="T13">
               <ts e="T14" id="Seg_8635" n="e" s="T13">Vklʼučʼila. </ts>
            </ts>
            <ts e="T257" id="Seg_8636" n="sc" s="T254">
               <ts e="T255" id="Seg_8638" n="e" s="T254">– Manna </ts>
               <ts e="T256" id="Seg_8640" n="e" s="T255">ogolonon </ts>
               <ts e="T257" id="Seg_8642" n="e" s="T256">(olo-)… </ts>
            </ts>
            <ts e="T408" id="Seg_8643" n="sc" s="T407">
               <ts e="T408" id="Seg_8645" n="e" s="T407">– Instʼitukka? </ts>
            </ts>
            <ts e="T469" id="Seg_8646" n="sc" s="T467">
               <ts e="T468" id="Seg_8648" n="e" s="T467">– Ulakan </ts>
               <ts e="T469" id="Seg_8650" n="e" s="T468">kergen. </ts>
            </ts>
            <ts e="T495" id="Seg_8651" n="sc" s="T493">
               <ts e="T495" id="Seg_8653" n="e" s="T493">– Školaga? </ts>
            </ts>
            <ts e="T507" id="Seg_8654" n="sc" s="T502">
               <ts e="T503" id="Seg_8656" n="e" s="T502">– Aː, </ts>
               <ts e="T504" id="Seg_8658" n="e" s="T503">vʼidʼela, </ts>
               <ts e="T505" id="Seg_8660" n="e" s="T504">vʼidʼela, </ts>
               <ts e="T507" id="Seg_8662" n="e" s="T505">kördüm. </ts>
            </ts>
            <ts e="T669" id="Seg_8663" n="sc" s="T668">
               <ts e="T669" id="Seg_8665" n="e" s="T668">– Pasʼiba. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-SE">
            <ta e="T12" id="Seg_8666" s="T10">KiPP_XXXX_2009_Family_nar.ES.001 (001.004)</ta>
            <ta e="T14" id="Seg_8667" s="T13">KiPP_XXXX_2009_Family_nar.ES.002 (001.005)</ta>
            <ta e="T257" id="Seg_8668" s="T254">KiPP_XXXX_2009_Family_nar.ES.003 (001.052)</ta>
            <ta e="T408" id="Seg_8669" s="T407">KiPP_XXXX_2009_Family_nar.ES.004 (001.075)</ta>
            <ta e="T469" id="Seg_8670" s="T467">KiPP_XXXX_2009_Family_nar.ES.005 (001.085)</ta>
            <ta e="T495" id="Seg_8671" s="T493">KiPP_XXXX_2009_Family_nar.ES.006 (001.091)</ta>
            <ta e="T507" id="Seg_8672" s="T502">KiPP_XXXX_2009_Family_nar.ES.007 (001.093)</ta>
            <ta e="T669" id="Seg_8673" s="T668">KiPP_XXXX_2009_Family_nar.ES.008 (001.127)</ta>
         </annotation>
         <annotation name="st" tierref="st-SE" />
         <annotation name="ts" tierref="ts-SE">
            <ta e="T12" id="Seg_8674" s="T10">– Da. </ta>
            <ta e="T14" id="Seg_8675" s="T13">Vklʼučʼila. </ta>
            <ta e="T257" id="Seg_8676" s="T254">– Manna ogolonon (olo-)… </ta>
            <ta e="T408" id="Seg_8677" s="T407">– Instʼitukka? </ta>
            <ta e="T469" id="Seg_8678" s="T467">– Ulakan kergen. </ta>
            <ta e="T495" id="Seg_8679" s="T493">– Školaga? </ta>
            <ta e="T507" id="Seg_8680" s="T502">– Aː, vʼidʼela, vʼidʼela, kördüm. </ta>
            <ta e="T669" id="Seg_8681" s="T668">– Pasʼiba. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-SE">
            <ta e="T12" id="Seg_8682" s="T10">da</ta>
            <ta e="T255" id="Seg_8683" s="T254">manna</ta>
            <ta e="T256" id="Seg_8684" s="T255">ogo-lon-on</ta>
            <ta e="T408" id="Seg_8685" s="T407">instʼituk-ka</ta>
            <ta e="T468" id="Seg_8686" s="T467">Ulakan</ta>
            <ta e="T469" id="Seg_8687" s="T468">kergen</ta>
            <ta e="T495" id="Seg_8688" s="T493">usku͡ola-ga</ta>
            <ta e="T503" id="Seg_8689" s="T502">aː</ta>
            <ta e="T507" id="Seg_8690" s="T505">kör-dü-m</ta>
            <ta e="T669" id="Seg_8691" s="T668">pasʼiba</ta>
         </annotation>
         <annotation name="mp" tierref="mp-SE">
            <ta e="T12" id="Seg_8692" s="T10">da</ta>
            <ta e="T255" id="Seg_8693" s="T254">manna</ta>
            <ta e="T256" id="Seg_8694" s="T255">ogo-LAN-An</ta>
            <ta e="T408" id="Seg_8695" s="T407">instʼitut-GA</ta>
            <ta e="T468" id="Seg_8696" s="T467">ulakan</ta>
            <ta e="T469" id="Seg_8697" s="T468">kergen</ta>
            <ta e="T495" id="Seg_8698" s="T493">usku͡ola-GA</ta>
            <ta e="T503" id="Seg_8699" s="T502">aː</ta>
            <ta e="T507" id="Seg_8700" s="T505">kör-TI-m</ta>
            <ta e="T669" id="Seg_8701" s="T668">pasiːba</ta>
         </annotation>
         <annotation name="ge" tierref="ge-SE">
            <ta e="T12" id="Seg_8702" s="T10">yes</ta>
            <ta e="T255" id="Seg_8703" s="T254">here</ta>
            <ta e="T256" id="Seg_8704" s="T255">child-VBZ-CVB.SEQ</ta>
            <ta e="T408" id="Seg_8705" s="T407">institute-DAT/LOC</ta>
            <ta e="T468" id="Seg_8706" s="T467">big</ta>
            <ta e="T469" id="Seg_8707" s="T468">family.[NOM]</ta>
            <ta e="T495" id="Seg_8708" s="T493">school-DAT/LOC</ta>
            <ta e="T503" id="Seg_8709" s="T502">ah</ta>
            <ta e="T507" id="Seg_8710" s="T505">see-PST1-1SG</ta>
            <ta e="T669" id="Seg_8711" s="T668">thanks</ta>
         </annotation>
         <annotation name="gg" tierref="gg-SE">
            <ta e="T12" id="Seg_8712" s="T10">ja</ta>
            <ta e="T255" id="Seg_8713" s="T254">hier</ta>
            <ta e="T256" id="Seg_8714" s="T255">Kind-VBZ-CVB.SEQ</ta>
            <ta e="T408" id="Seg_8715" s="T407">Institut-DAT/LOC</ta>
            <ta e="T468" id="Seg_8716" s="T467">groß</ta>
            <ta e="T469" id="Seg_8717" s="T468">Familie.[NOM]</ta>
            <ta e="T495" id="Seg_8718" s="T493">Schule-DAT/LOC</ta>
            <ta e="T503" id="Seg_8719" s="T502">ah</ta>
            <ta e="T507" id="Seg_8720" s="T505">sehen-PST1-1SG</ta>
            <ta e="T669" id="Seg_8721" s="T668">danke</ta>
         </annotation>
         <annotation name="gr" tierref="gr-SE">
            <ta e="T12" id="Seg_8722" s="T10">да</ta>
            <ta e="T255" id="Seg_8723" s="T254">здесь</ta>
            <ta e="T256" id="Seg_8724" s="T255">ребенок-VBZ-CVB.SEQ</ta>
            <ta e="T408" id="Seg_8725" s="T407">институт-DAT/LOC</ta>
            <ta e="T468" id="Seg_8726" s="T467">большой</ta>
            <ta e="T469" id="Seg_8727" s="T468">семья.[NOM]</ta>
            <ta e="T495" id="Seg_8728" s="T493">школа-DAT/LOC</ta>
            <ta e="T503" id="Seg_8729" s="T502">ах</ta>
            <ta e="T507" id="Seg_8730" s="T505">видеть-PST1-1SG</ta>
            <ta e="T669" id="Seg_8731" s="T668">спасибо</ta>
         </annotation>
         <annotation name="mc" tierref="mc-SE">
            <ta e="T12" id="Seg_8732" s="T10">ptcl</ta>
            <ta e="T255" id="Seg_8733" s="T254">adv</ta>
            <ta e="T256" id="Seg_8734" s="T255">n-n&gt;v-v:cvb</ta>
            <ta e="T408" id="Seg_8735" s="T407">n-n:case</ta>
            <ta e="T468" id="Seg_8736" s="T467">adj</ta>
            <ta e="T469" id="Seg_8737" s="T468">n.[n:case]</ta>
            <ta e="T495" id="Seg_8738" s="T493">n-n:case</ta>
            <ta e="T503" id="Seg_8739" s="T502">interj</ta>
            <ta e="T507" id="Seg_8740" s="T505">v-v:tense-v:poss.pn</ta>
            <ta e="T669" id="Seg_8741" s="T668">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps-SE">
            <ta e="T12" id="Seg_8742" s="T10">ptcl</ta>
            <ta e="T255" id="Seg_8743" s="T254">adv</ta>
            <ta e="T256" id="Seg_8744" s="T255">v</ta>
            <ta e="T408" id="Seg_8745" s="T407">n</ta>
            <ta e="T468" id="Seg_8746" s="T467">adj</ta>
            <ta e="T469" id="Seg_8747" s="T468">n</ta>
            <ta e="T495" id="Seg_8748" s="T493">n</ta>
            <ta e="T503" id="Seg_8749" s="T502">interj</ta>
            <ta e="T507" id="Seg_8750" s="T505">v</ta>
            <ta e="T669" id="Seg_8751" s="T668">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-SE" />
         <annotation name="SyF" tierref="SyF-SE" />
         <annotation name="IST" tierref="IST-SE" />
         <annotation name="Top" tierref="Top-SE" />
         <annotation name="Foc" tierref="Foc-SE" />
         <annotation name="BOR" tierref="BOR-SE">
            <ta e="T12" id="Seg_8752" s="T10">RUS:disc</ta>
            <ta e="T408" id="Seg_8753" s="T407">RUS:cult</ta>
            <ta e="T495" id="Seg_8754" s="T493">RUS:cult</ta>
            <ta e="T669" id="Seg_8755" s="T668">RUS:disc</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-SE" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-SE" />
         <annotation name="CS" tierref="CS-SE" />
         <annotation name="fe" tierref="fe-SE">
            <ta e="T12" id="Seg_8756" s="T10">– Yes.</ta>
            <ta e="T14" id="Seg_8757" s="T13">I turned it on.</ta>
            <ta e="T257" id="Seg_8758" s="T254">– Here having a child (…).</ta>
            <ta e="T408" id="Seg_8759" s="T407">– At the institute?</ta>
            <ta e="T469" id="Seg_8760" s="T467">– A big family.</ta>
            <ta e="T495" id="Seg_8761" s="T493">– In school?</ta>
            <ta e="T507" id="Seg_8762" s="T502">– Ah, I saw here, I saw here.</ta>
            <ta e="T669" id="Seg_8763" s="T668">– Thank you.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-SE">
            <ta e="T12" id="Seg_8764" s="T10">– Ja.</ta>
            <ta e="T14" id="Seg_8765" s="T13">Ich habe es eingeschaltet.</ta>
            <ta e="T257" id="Seg_8766" s="T254">– Hier Kinder kriegend (…).</ta>
            <ta e="T408" id="Seg_8767" s="T407">– Am Institut?</ta>
            <ta e="T469" id="Seg_8768" s="T467">– Eine große Familie.</ta>
            <ta e="T495" id="Seg_8769" s="T493">– In der Schule?</ta>
            <ta e="T507" id="Seg_8770" s="T502">– Ah, ich hab sie gesehen, ich hab sie gesehen.</ta>
            <ta e="T669" id="Seg_8771" s="T668">– Danke.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-SE" />
         <annotation name="ltr" tierref="ltr-SE" />
         <annotation name="nt" tierref="nt-SE" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T672" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-KiPP"
                          name="ref"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-KiPP"
                          name="st"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KiPP"
                          name="ts"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KiPP"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KiPP"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-KiPP"
                          name="mb"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-KiPP"
                          name="mp"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-KiPP"
                          name="ge"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-KiPP"
                          name="gg"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-KiPP"
                          name="gr"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-KiPP"
                          name="mc"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-KiPP"
                          name="ps"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-KiPP"
                          name="SeR"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-KiPP"
                          name="SyF"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-KiPP"
                          name="IST"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-KiPP"
                          name="Top"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-KiPP"
                          name="Foc"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-KiPP"
                          name="BOR"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-KiPP"
                          name="BOR-Phon"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-KiPP"
                          name="BOR-Morph"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-KiPP"
                          name="CS"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KiPP"
                          name="fe"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KiPP"
                          name="fg"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KiPP"
                          name="fr"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-KiPP"
                          name="ltr"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KiPP"
                          name="nt"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-XXXX"
                          name="ref"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-XXXX"
                          name="st"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-XXXX"
                          name="ts"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-XXXX"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-XXXX"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-XXXX"
                          name="mb"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-XXXX"
                          name="mp"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-XXXX"
                          name="ge"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-XXXX"
                          name="gg"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-XXXX"
                          name="gr"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-XXXX"
                          name="mc"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-XXXX"
                          name="ps"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-XXXX"
                          name="SeR"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-XXXX"
                          name="SyF"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-XXXX"
                          name="IST"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-XXXX"
                          name="Top"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-XXXX"
                          name="Foc"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-XXXX"
                          name="BOR"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-XXXX"
                          name="BOR-Phon"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-XXXX"
                          name="BOR-Morph"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-XXXX"
                          name="CS"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-XXXX"
                          name="fe"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-XXXX"
                          name="fg"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-XXXX"
                          name="fr"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-XXXX"
                          name="ltr"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-XXXX"
                          name="nt"
                          segmented-tier-id="tx-XXXX"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-SE"
                          name="ref"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-SE"
                          name="st"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-SE"
                          name="ts"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-SE"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-SE"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-SE"
                          name="mb"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-SE"
                          name="mp"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-SE"
                          name="ge"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-SE"
                          name="gg"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-SE"
                          name="gr"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-SE"
                          name="mc"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-SE"
                          name="ps"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-SE"
                          name="SeR"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-SE"
                          name="SyF"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-SE"
                          name="IST"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-SE"
                          name="Top"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-SE"
                          name="Foc"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-SE"
                          name="BOR"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-SE"
                          name="BOR-Phon"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-SE"
                          name="BOR-Morph"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-SE"
                          name="CS"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-SE"
                          name="fe"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-SE"
                          name="fg"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-SE"
                          name="fr"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-SE"
                          name="ltr"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-SE"
                          name="nt"
                          segmented-tier-id="tx-SE"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
