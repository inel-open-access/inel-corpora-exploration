<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDFC2F8FC7-292A-765E-34A2-9E6C9CB5D4BC">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>PoNA_1990_TojooAirplane_nar</transcription-name>
         <referenced-file url="PoNA_19900810_TojooAirplane_nar.wav" />
         <referenced-file url="PoNA_19900810_TojooAirplane_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\PoNA_19900810_TojooAirplane_nar\PoNA_19900810_TojooAirplane_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">391</ud-information>
            <ud-information attribute-name="# HIAT:w">297</ud-information>
            <ud-information attribute-name="# e">297</ud-information>
            <ud-information attribute-name="# HIAT:u">42</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PoNA">
            <abbreviation>PoNA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.8229999999999791" type="appl" />
         <tli id="T2" time="1.6460000000000719" type="appl" />
         <tli id="T3" time="2.469000000000051" type="appl" />
         <tli id="T4" time="2.8999596068131166" />
         <tli id="T5" time="5.122000000000071" type="appl" />
         <tli id="T6" time="6.951999999999998" type="appl" />
         <tli id="T7" time="7.692000000000007" type="appl" />
         <tli id="T8" time="8.43100000000004" type="appl" />
         <tli id="T9" time="9.17100000000005" type="appl" />
         <tli id="T10" time="9.911000000000058" type="appl" />
         <tli id="T11" time="10.651000000000067" type="appl" />
         <tli id="T12" time="11.389999999999986" type="appl" />
         <tli id="T13" time="12.129999999999995" type="appl" />
         <tli id="T14" time="13.263000000000034" type="appl" />
         <tli id="T15" time="14.394999999999982" type="appl" />
         <tli id="T16" time="15.52800000000002" type="appl" />
         <tli id="T17" time="16.661000000000058" type="appl" />
         <tli id="T18" time="17.793999999999983" type="appl" />
         <tli id="T19" time="18.926000000000045" type="appl" />
         <tli id="T20" time="20.059000000000083" type="appl" />
         <tli id="T21" time="21.54636654993102" />
         <tli id="T22" time="22.190000000000055" type="appl" />
         <tli id="T23" time="23.187999999999988" type="appl" />
         <tli id="T24" time="24.186000000000035" type="appl" />
         <tli id="T25" time="25.184000000000083" type="appl" />
         <tli id="T26" time="26.182000000000016" type="appl" />
         <tli id="T27" time="27.080002037695255" />
         <tli id="T28" time="27.97199999999998" type="appl" />
         <tli id="T29" time="28.764999999999986" type="appl" />
         <tli id="T30" time="29.557000000000016" type="appl" />
         <tli id="T31" time="30.350000000000023" type="appl" />
         <tli id="T32" time="30.722006972711178" />
         <tli id="T33" time="31.83800000000008" type="appl" />
         <tli id="T34" time="32.53399999999999" type="appl" />
         <tli id="T35" time="33.23000000000002" type="appl" />
         <tli id="T36" time="33.926000000000045" type="appl" />
         <tli id="T37" time="34.62099999999998" type="appl" />
         <tli id="T38" time="35.31700000000001" type="appl" />
         <tli id="T39" time="36.013000000000034" type="appl" />
         <tli id="T40" time="36.562335779727064" />
         <tli id="T41" time="37.25800000000004" type="appl" />
         <tli id="T42" time="37.807000000000016" type="appl" />
         <tli id="T43" time="38.355999999999995" type="appl" />
         <tli id="T44" time="38.905000000000086" type="appl" />
         <tli id="T45" time="39.454000000000065" type="appl" />
         <tli id="T46" time="40.00300000000004" type="appl" />
         <tli id="T47" time="40.57200000000006" type="intp" />
         <tli id="T48" time="41.141000000000076" type="appl" />
         <tli id="T49" time="41.73000000000002" type="appl" />
         <tli id="T50" time="42.317999999999984" type="appl" />
         <tli id="T51" time="42.953666026493835" />
         <tli id="T52" time="43.749000000000024" type="appl" />
         <tli id="T53" time="44.59000000000003" type="appl" />
         <tli id="T54" time="44.74604340443596" />
         <tli id="T55" time="46.08800000000008" type="appl" />
         <tli id="T56" time="46.74400000000003" type="appl" />
         <tli id="T57" time="47.399" type="appl" />
         <tli id="T58" time="48.055000000000064" type="appl" />
         <tli id="T59" time="49.32597961105805" />
         <tli id="T60" time="49.50700000000006" type="appl" />
         <tli id="T61" time="50.303" type="appl" />
         <tli id="T62" time="51.099000000000046" type="appl" />
         <tli id="T63" time="51.89499999999998" type="appl" />
         <tli id="T64" time="52.69200000000001" type="appl" />
         <tli id="T65" time="53.488000000000056" type="appl" />
         <tli id="T66" time="54.28399999999999" type="appl" />
         <tli id="T67" time="55.08000000000004" type="appl" />
         <tli id="T68" time="55.87600000000009" type="appl" />
         <tli id="T69" time="56.01255314125013" />
         <tli id="T70" time="57.59800000000007" type="appl" />
         <tli id="T71" time="58.52500000000009" type="appl" />
         <tli id="T72" time="59.45100000000002" type="appl" />
         <tli id="T73" time="60.37800000000004" type="appl" />
         <tli id="T74" time="61.30400000000009" type="appl" />
         <tli id="T75" time="62.230999999999995" type="appl" />
         <tli id="T76" time="63.15700000000004" type="appl" />
         <tli id="T77" time="64.08400000000006" type="appl" />
         <tli id="T78" time="64.43243586172132" />
         <tli id="T79" time="65.68500000000006" type="appl" />
         <tli id="T80" time="66.36000000000001" type="appl" />
         <tli id="T81" time="67.03500000000008" type="appl" />
         <tli id="T82" time="67.71000000000004" type="appl" />
         <tli id="T83" time="68.38499999999999" type="appl" />
         <tli id="T84" time="69.06000000000006" type="appl" />
         <tli id="T85" time="69.73500000000001" type="appl" />
         <tli id="T86" time="70.41000000000008" type="appl" />
         <tli id="T87" time="71.08500000000004" type="appl" />
         <tli id="T88" time="71.75999999999999" type="appl" />
         <tli id="T89" time="72.43500000000006" type="appl" />
         <tli id="T90" time="73.11000000000001" type="appl" />
         <tli id="T91" time="73.78500000000008" type="appl" />
         <tli id="T92" time="73.89230409635996" />
         <tli id="T93" time="74.37720273090663" type="intp" />
         <tli id="T94" time="75.87227651756339" />
         <tli id="T95" time="76.0203213304651" />
         <tli id="T96" time="76.23400000000004" type="appl" />
         <tli id="T97" time="77.12099999999998" type="appl" />
         <tli id="T98" time="78.00800000000004" type="appl" />
         <tli id="T99" time="78.89499999999998" type="appl" />
         <tli id="T100" time="79.78200000000004" type="appl" />
         <tli id="T101" time="80.668" type="appl" />
         <tli id="T102" time="81.55500000000006" type="appl" />
         <tli id="T103" time="82.44200000000001" type="appl" />
         <tli id="T104" time="83.32900000000006" type="appl" />
         <tli id="T105" time="84.21600000000001" type="appl" />
         <tli id="T106" time="85.32299774419296" />
         <tli id="T107" time="86.06100000000004" type="appl" />
         <tli id="T108" time="87.01999999999998" type="appl" />
         <tli id="T109" time="87.97800000000007" type="appl" />
         <tli id="T110" time="88.93600000000004" type="appl" />
         <tli id="T111" time="89.89499999999998" type="appl" />
         <tli id="T112" time="90.85300000000007" type="appl" />
         <tli id="T113" time="91.81100000000004" type="appl" />
         <tli id="T114" time="92.769" type="appl" />
         <tli id="T115" time="93.72800000000007" type="appl" />
         <tli id="T116" time="94.68600000000004" type="appl" />
         <tli id="T117" time="95.644" type="appl" />
         <tli id="T118" time="96.60300000000007" type="appl" />
         <tli id="T119" time="97.39433351026598" />
         <tli id="T120" time="99.053" type="appl" />
         <tli id="T121" time="99.92527481959036" />
         <tli id="T122" time="101.09300000000007" type="appl" />
         <tli id="T123" time="101.64100000000008" type="appl" />
         <tli id="T124" time="102.18900000000008" type="appl" />
         <tli id="T125" time="102.73700000000008" type="appl" />
         <tli id="T126" time="103.28600000000006" type="appl" />
         <tli id="T127" time="103.83400000000006" type="appl" />
         <tli id="T128" time="104.38200000000006" type="appl" />
         <tli id="T129" time="104.93000000000006" type="appl" />
         <tli id="T130" time="105.47800000000007" type="appl" />
         <tli id="T131" time="105.6651948689377" />
         <tli id="T132" time="106.85900000000004" type="appl" />
         <tli id="T133" time="107.69200000000001" type="appl" />
         <tli id="T134" time="108.52500000000009" type="appl" />
         <tli id="T135" time="109.35800000000006" type="appl" />
         <tli id="T136" time="110.19200000000001" type="appl" />
         <tli id="T137" time="111.02500000000009" type="appl" />
         <tli id="T138" time="111.85800000000006" type="appl" />
         <tli id="T139" time="112.69100000000003" type="appl" />
         <tli id="T140" time="113.524" type="appl" />
         <tli id="T141" time="114.83840042979942" />
         <tli id="T142" time="115.20600000000002" type="appl" />
         <tli id="T143" time="116.05500000000006" type="appl" />
         <tli id="T144" time="116.904" type="appl" />
         <tli id="T145" time="117.75300000000004" type="appl" />
         <tli id="T146" time="117.97835669372812" />
         <tli id="T147" time="120.202" type="appl" />
         <tli id="T148" time="121.80200000000002" type="appl" />
         <tli id="T149" time="123.40100000000007" type="appl" />
         <tli id="T150" time="124.36493440252575" />
         <tli id="T151" time="125.64700000000005" type="appl" />
         <tli id="T152" time="126.29200000000003" type="appl" />
         <tli id="T153" time="126.93799999999999" type="appl" />
         <tli id="T154" time="127.58300000000008" type="appl" />
         <tli id="T155" time="128.22900000000004" type="appl" />
         <tli id="T156" time="128.875" type="appl" />
         <tli id="T157" time="129.51999999999998" type="appl" />
         <tli id="T158" time="130.16600000000005" type="appl" />
         <tli id="T159" time="130.81100000000004" type="appl" />
         <tli id="T160" time="130.9715090470126" />
         <tli id="T161" time="132.57500000000005" type="appl" />
         <tli id="T162" time="133.69299999999998" type="appl" />
         <tli id="T163" time="134.812" type="appl" />
         <tli id="T164" time="135.93000000000006" type="appl" />
         <tli id="T165" time="137.6380828557784" />
         <tli id="T166" time="138.26700000000005" type="appl" />
         <tli id="T167" time="139.485" type="appl" />
         <tli id="T168" time="140.70400000000006" type="appl" />
         <tli id="T169" time="141.50200000000007" type="appl" />
         <tli id="T170" time="142.30100000000004" type="appl" />
         <tli id="T171" time="143.10000000000002" type="appl" />
         <tli id="T172" time="143.89800000000002" type="appl" />
         <tli id="T173" time="144.69600000000003" type="appl" />
         <tli id="T174" time="145.495" type="appl" />
         <tli id="T175" time="146.29399999999998" type="appl" />
         <tli id="T176" time="147.09199999999998" type="appl" />
         <tli id="T177" time="147.89" type="appl" />
         <tli id="T178" time="147.96460568555662" />
         <tli id="T179" time="149.52600000000007" type="appl" />
         <tli id="T180" time="150.36200000000008" type="appl" />
         <tli id="T181" time="151.19900000000007" type="appl" />
         <tli id="T182" time="152.03500000000008" type="appl" />
         <tli id="T183" time="152.2178797755492" />
         <tli id="T184" time="153.8520000000001" type="appl" />
         <tli id="T185" time="154.83100000000002" type="appl" />
         <tli id="T186" time="155.44450149899183" />
         <tli id="T187" time="156.77800000000002" type="appl" />
         <tli id="T188" time="157.745" type="appl" />
         <tli id="T189" time="158.712" type="appl" />
         <tli id="T190" time="159.678" type="appl" />
         <tli id="T191" time="160.64499999999998" type="appl" />
         <tli id="T192" time="161.15108867929533" />
         <tli id="T193" time="162.54399999999998" type="appl" />
         <tli id="T194" time="163.4770000000001" type="appl" />
         <tli id="T195" time="164.42500000000007" type="appl" />
         <tli id="T196" time="165.37300000000005" type="appl" />
         <tli id="T197" time="166.32100000000003" type="appl" />
         <tli id="T198" time="167.26999999999998" type="appl" />
         <tli id="T199" time="168.21800000000007" type="appl" />
         <tli id="T200" time="169.16600000000005" type="appl" />
         <tli id="T201" time="170.58429061869893" />
         <tli id="T202" time="171.029" type="appl" />
         <tli id="T203" time="171.94400000000007" type="appl" />
         <tli id="T204" time="172.85900000000004" type="appl" />
         <tli id="T205" time="173.774" type="appl" />
         <tli id="T206" time="174.68900000000008" type="appl" />
         <tli id="T207" time="175.60400000000004" type="appl" />
         <tli id="T208" time="176.519" type="appl" />
         <tli id="T209" time="177.43400000000008" type="appl" />
         <tli id="T210" time="178.34900000000005" type="appl" />
         <tli id="T211" time="179.264" type="appl" />
         <tli id="T212" time="180.1790000000001" type="appl" />
         <tli id="T213" time="181.09400000000005" type="appl" />
         <tli id="T214" time="182.00900000000001" type="appl" />
         <tli id="T215" time="182.92399999999998" type="appl" />
         <tli id="T216" time="183.83900000000006" type="appl" />
         <tli id="T217" time="184.19743433619868" />
         <tli id="T218" time="185.64800000000002" type="appl" />
         <tli id="T219" time="186.543" type="appl" />
         <tli id="T220" time="187.438" type="appl" />
         <tli id="T221" time="187.63071984771304" />
         <tli id="T222" time="188.8760000000001" type="appl" />
         <tli id="T223" time="189.42000000000007" type="appl" />
         <tli id="T224" time="189.96500000000003" type="appl" />
         <tli id="T225" time="190.50900000000001" type="appl" />
         <tli id="T226" time="191.053" type="appl" />
         <tli id="T227" time="191.59800000000007" type="appl" />
         <tli id="T228" time="192.14200000000005" type="appl" />
         <tli id="T229" time="192.68600000000004" type="appl" />
         <tli id="T230" time="194.519" type="appl" />
         <tli id="T231" time="196.35300000000007" type="appl" />
         <tli id="T232" time="198.18600000000004" type="appl" />
         <tli id="T233" time="200.019" type="appl" />
         <tli id="T234" time="201.85300000000007" type="appl" />
         <tli id="T235" time="203.02383877215325" />
         <tli id="T236" time="204.37" type="appl" />
         <tli id="T237" time="205.0540000000001" type="appl" />
         <tli id="T238" time="205.73900000000003" type="appl" />
         <tli id="T239" time="206.423" type="appl" />
         <tli id="T240" time="207.10700000000008" type="appl" />
         <tli id="T241" time="207.79200000000003" type="appl" />
         <tli id="T242" time="208.476" type="appl" />
         <tli id="T243" time="208.61042762389894" />
         <tli id="T244" time="209.93900000000008" type="appl" />
         <tli id="T245" time="210.71800000000007" type="appl" />
         <tli id="T246" time="211.49800000000005" type="appl" />
         <tli id="T247" time="212.27700000000004" type="appl" />
         <tli id="T248" time="213.05600000000004" type="appl" />
         <tli id="T249" time="213.83500000000004" type="appl" />
         <tli id="T250" time="214.615" type="appl" />
         <tli id="T251" time="215.394" type="appl" />
         <tli id="T252" time="216.173" type="appl" />
         <tli id="T253" time="216.92600000000004" type="appl" />
         <tli id="T254" time="217.6790000000001" type="appl" />
         <tli id="T255" time="218.43200000000002" type="appl" />
         <tli id="T256" time="219.18500000000006" type="appl" />
         <tli id="T257" time="219.938" type="appl" />
         <tli id="T258" time="220.69000000000005" type="appl" />
         <tli id="T259" time="221.44299999999998" type="appl" />
         <tli id="T260" time="222.19600000000003" type="appl" />
         <tli id="T261" time="222.94900000000007" type="appl" />
         <tli id="T262" time="223.702" type="appl" />
         <tli id="T263" time="224.01687969595667" />
         <tli id="T264" time="225.19000000000005" type="appl" />
         <tli id="T265" time="225.92399999999998" type="appl" />
         <tli id="T266" time="226.65800000000002" type="appl" />
         <tli id="T267" time="227.39300000000003" type="appl" />
         <tli id="T268" time="228.12800000000004" type="appl" />
         <tli id="T269" time="228.25015406452297" />
         <tli id="T270" time="229.96400000000006" type="appl" />
         <tli id="T271" time="231.067" type="appl" />
         <tli id="T272" time="232.16899999999998" type="appl" />
         <tli id="T273" time="233.27100000000007" type="appl" />
         <tli id="T274" time="234.37400000000002" type="appl" />
         <tli id="T275" time="234.77006324949593" />
         <tli id="T276" time="236.08900000000006" type="appl" />
         <tli id="T277" time="236.702" type="appl" />
         <tli id="T278" time="237.31400000000008" type="appl" />
         <tli id="T279" time="237.92700000000002" type="appl" />
         <tli id="T280" time="238.54000000000008" type="appl" />
         <tli id="T281" time="239.15300000000002" type="appl" />
         <tli id="T282" time="239.76600000000008" type="appl" />
         <tli id="T283" time="240.37900000000002" type="appl" />
         <tli id="T284" time="240.99200000000008" type="appl" />
         <tli id="T285" time="241.60400000000004" type="appl" />
         <tli id="T286" time="242.21699999999998" type="appl" />
         <tli id="T287" time="242.53662173670804" />
         <tli id="T288" time="243.72900000000004" type="appl" />
         <tli id="T289" time="244.62800000000004" type="appl" />
         <tli id="T290" time="245.52800000000002" type="appl" />
         <tli id="T291" time="246.42700000000002" type="appl" />
         <tli id="T292" time="247.32600000000002" type="appl" />
         <tli id="T293" time="248.22500000000002" type="appl" />
         <tli id="T294" time="249.12400000000002" type="appl" />
         <tli id="T295" time="250.024" type="appl" />
         <tli id="T296" time="250.923" type="appl" />
         <tli id="T297" time="251.822" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PoNA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T297" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Anɨ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">ihilleːŋ</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">nöŋü͡ö</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">öhü</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Aːta</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_21" n="HIAT:ip">"</nts>
                  <ts e="T6" id="Seg_23" n="HIAT:w" s="T5">Dʼarapalaːn</ts>
                  <nts id="Seg_24" n="HIAT:ip">"</nts>
                  <nts id="Seg_25" n="HIAT:ip">.</nts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_28" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_30" n="HIAT:w" s="T6">Tojoː</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_33" n="HIAT:w" s="T7">ühüs</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_36" n="HIAT:w" s="T8">künüger</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_39" n="HIAT:w" s="T9">emi͡e</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_42" n="HIAT:w" s="T10">körbüt</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_45" n="HIAT:w" s="T11">biːr</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">dʼiktini</ts>
                  <nts id="Seg_49" n="HIAT:ip">.</nts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_52" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_54" n="HIAT:w" s="T13">Künüs</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_57" n="HIAT:w" s="T14">ahaːn</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_60" n="HIAT:w" s="T15">büteːt</ts>
                  <nts id="Seg_61" n="HIAT:ip">,</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">tahaːra</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">hüreliː</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">hɨldʼɨbɨt</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">uːlʼica</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_76" n="HIAT:w" s="T20">üstün</ts>
                  <nts id="Seg_77" n="HIAT:ip">.</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_80" n="HIAT:u" s="T21">
                  <nts id="Seg_81" n="HIAT:ip">"</nts>
                  <ts e="T22" id="Seg_83" n="HIAT:w" s="T21">Dʼarapalaːn</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">kötön</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_89" n="HIAT:w" s="T23">keli͡e</ts>
                  <nts id="Seg_90" n="HIAT:ip">"</nts>
                  <nts id="Seg_91" n="HIAT:ip">,</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_94" n="HIAT:w" s="T24">di͡en</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_97" n="HIAT:w" s="T25">öhü</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_100" n="HIAT:w" s="T26">istibit</ts>
                  <nts id="Seg_101" n="HIAT:ip">.</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_104" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_106" n="HIAT:w" s="T27">Barɨ</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_109" n="HIAT:w" s="T28">kihiler</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_112" n="HIAT:w" s="T29">küːteller</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_115" n="HIAT:w" s="T30">ebit</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_118" n="HIAT:w" s="T31">dʼarapalaːnɨ</ts>
                  <nts id="Seg_119" n="HIAT:ip">.</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_122" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_124" n="HIAT:w" s="T32">Haŋa</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_127" n="HIAT:w" s="T33">kihiler</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">kelellerin</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">köröːrü</ts>
                  <nts id="Seg_134" n="HIAT:ip">,</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_137" n="HIAT:w" s="T36">kantan</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_140" n="HIAT:w" s="T37">ire</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_143" n="HIAT:w" s="T38">huruk</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_146" n="HIAT:w" s="T39">ɨlaːrɨ</ts>
                  <nts id="Seg_147" n="HIAT:ip">.</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_150" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_152" n="HIAT:w" s="T40">Tojoː</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_155" n="HIAT:w" s="T41">kahan</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_158" n="HIAT:w" s="T42">daː</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_161" n="HIAT:w" s="T43">körö</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_164" n="HIAT:w" s="T44">ilik</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_167" n="HIAT:w" s="T45">ebit</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_170" n="HIAT:w" s="T46">dʼarapalaːnɨ</ts>
                  <nts id="Seg_171" n="HIAT:ip">.</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_174" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_176" n="HIAT:w" s="T47">Kajtak</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_179" n="HIAT:w" s="T48">ebitej</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_182" n="HIAT:w" s="T49">iti</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_185" n="HIAT:w" s="T50">kötörö</ts>
                  <nts id="Seg_186" n="HIAT:ip">?</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_189" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_191" n="HIAT:w" s="T51">Tu͡ok</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_194" n="HIAT:w" s="T52">uraːŋkaj</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_197" n="HIAT:w" s="T53">biler</ts>
                  <nts id="Seg_198" n="HIAT:ip">?</nts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_201" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_203" n="HIAT:w" s="T54">Hol</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_206" n="HIAT:w" s="T55">daː</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_209" n="HIAT:w" s="T56">bu͡ollar</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_212" n="HIAT:w" s="T57">hin</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_215" n="HIAT:w" s="T58">küːter</ts>
                  <nts id="Seg_216" n="HIAT:ip">.</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_219" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_221" n="HIAT:w" s="T59">Töhö</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_224" n="HIAT:w" s="T60">daː</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_227" n="HIAT:w" s="T61">ör</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_230" n="HIAT:w" s="T62">bu͡olbatak</ts>
                  <nts id="Seg_231" n="HIAT:ip">,</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_234" n="HIAT:w" s="T63">etiŋ</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_237" n="HIAT:w" s="T64">eterin</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_240" n="HIAT:w" s="T65">kördük</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_243" n="HIAT:w" s="T66">kallaːŋŋa</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_246" n="HIAT:w" s="T67">aːjdaːn</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_249" n="HIAT:w" s="T68">bu͡olbut</ts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_253" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_255" n="HIAT:w" s="T69">Onton</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_258" n="HIAT:w" s="T70">tu͡ok</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_261" n="HIAT:w" s="T71">da</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_264" n="HIAT:w" s="T72">kördük</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_267" n="HIAT:w" s="T73">kötör</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_270" n="HIAT:w" s="T74">kötö</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_273" n="HIAT:w" s="T75">hɨldʼɨbɨt</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_276" n="HIAT:w" s="T76">bɨlɨttar</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_279" n="HIAT:w" s="T77">ürdüleriger</ts>
                  <nts id="Seg_280" n="HIAT:ip">.</nts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_283" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_285" n="HIAT:w" s="T78">Tojoː</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_288" n="HIAT:w" s="T79">kuttammɨt</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_291" n="HIAT:w" s="T80">če</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_294" n="HIAT:w" s="T81">turbut</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_297" n="HIAT:w" s="T82">hiriger</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_299" n="HIAT:ip">"</nts>
                  <ts e="T84" id="Seg_301" n="HIAT:w" s="T83">lak</ts>
                  <nts id="Seg_302" n="HIAT:ip">"</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_305" n="HIAT:w" s="T84">gɨna</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_308" n="HIAT:w" s="T85">olorbut</ts>
                  <nts id="Seg_309" n="HIAT:ip">,</nts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_312" n="HIAT:w" s="T86">karaktarɨn</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_315" n="HIAT:w" s="T87">happɨt</ts>
                  <nts id="Seg_316" n="HIAT:ip">,</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_319" n="HIAT:w" s="T88">toŋmut</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_322" n="HIAT:w" s="T89">kördük</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_325" n="HIAT:w" s="T90">titiriː</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_328" n="HIAT:w" s="T91">olorbut</ts>
                  <nts id="Seg_329" n="HIAT:ip">.</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_332" n="HIAT:u" s="T92">
                  <nts id="Seg_333" n="HIAT:ip">"</nts>
                  <ts e="T93" id="Seg_335" n="HIAT:w" s="T92">Tu͡ok</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_338" n="HIAT:w" s="T93">bu͡olluŋ</ts>
                  <nts id="Seg_339" n="HIAT:ip">?</nts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_342" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_344" n="HIAT:w" s="T94">Hüːr</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_347" n="HIAT:w" s="T95">türgennik</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_350" n="HIAT:w" s="T96">kɨtɨlga</ts>
                  <nts id="Seg_351" n="HIAT:ip">,</nts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_354" n="HIAT:w" s="T97">körö</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_357" n="HIAT:w" s="T98">bar</ts>
                  <nts id="Seg_358" n="HIAT:ip">,</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_361" n="HIAT:w" s="T99">kajdak</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_364" n="HIAT:w" s="T100">samalʼot</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_367" n="HIAT:w" s="T101">olororun</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_370" n="HIAT:w" s="T102">uːga</ts>
                  <nts id="Seg_371" n="HIAT:ip">"</nts>
                  <nts id="Seg_372" n="HIAT:ip">,</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_375" n="HIAT:w" s="T103">učitʼelʼin</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_378" n="HIAT:w" s="T104">haŋatɨn</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_381" n="HIAT:w" s="T105">istibit</ts>
                  <nts id="Seg_382" n="HIAT:ip">.</nts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_385" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_387" n="HIAT:w" s="T106">Tojoː</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_390" n="HIAT:w" s="T107">karaktarɨn</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_393" n="HIAT:w" s="T108">arɨjbɨta</ts>
                  <nts id="Seg_394" n="HIAT:ip">,</nts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_397" n="HIAT:w" s="T109">Valačaːnka</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_400" n="HIAT:w" s="T110">barɨ</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_403" n="HIAT:w" s="T111">nehili͡ege</ts>
                  <nts id="Seg_404" n="HIAT:ip">,</nts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_407" n="HIAT:w" s="T112">turar</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_410" n="HIAT:w" s="T113">turbat</ts>
                  <nts id="Seg_411" n="HIAT:ip">,</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_414" n="HIAT:w" s="T114">gini</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_417" n="HIAT:w" s="T115">attɨnan</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_420" n="HIAT:w" s="T116">hüːreleːn</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_423" n="HIAT:w" s="T117">aːhan</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_426" n="HIAT:w" s="T118">ebitter</ts>
                  <nts id="Seg_427" n="HIAT:ip">.</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T121" id="Seg_430" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_432" n="HIAT:w" s="T119">Kihiler</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_435" n="HIAT:w" s="T120">kuttammattar</ts>
                  <nts id="Seg_436" n="HIAT:ip">.</nts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_439" n="HIAT:u" s="T121">
                  <nts id="Seg_440" n="HIAT:ip">"</nts>
                  <ts e="T122" id="Seg_442" n="HIAT:w" s="T121">Min</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_445" n="HIAT:w" s="T122">hin</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_448" n="HIAT:w" s="T123">tu͡ok</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_451" n="HIAT:w" s="T124">daː</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_454" n="HIAT:w" s="T125">bu͡olu͡om</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_457" n="HIAT:w" s="T126">hu͡oga</ts>
                  <nts id="Seg_458" n="HIAT:ip">,</nts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_461" n="HIAT:w" s="T127">badaga</ts>
                  <nts id="Seg_462" n="HIAT:ip">"</nts>
                  <nts id="Seg_463" n="HIAT:ip">,</nts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_466" n="HIAT:w" s="T128">diː</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_469" n="HIAT:w" s="T129">hanaːbɨt</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_472" n="HIAT:w" s="T130">Tojoː</ts>
                  <nts id="Seg_473" n="HIAT:ip">.</nts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_476" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_478" n="HIAT:w" s="T131">Tura</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_481" n="HIAT:w" s="T132">ekkireːt</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_484" n="HIAT:w" s="T133">ele</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_487" n="HIAT:w" s="T134">küːhünen</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_490" n="HIAT:w" s="T135">teppit</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_493" n="HIAT:w" s="T136">hüːren</ts>
                  <nts id="Seg_494" n="HIAT:ip">,</nts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_497" n="HIAT:w" s="T137">ɨstaŋalɨː-ɨstaŋalɨː</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_500" n="HIAT:w" s="T138">kɨra</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_503" n="HIAT:w" s="T139">čalbaktar</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_506" n="HIAT:w" s="T140">ürdülerinen</ts>
                  <nts id="Seg_507" n="HIAT:ip">.</nts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_510" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_512" n="HIAT:w" s="T141">Ebe</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_515" n="HIAT:w" s="T142">kɨtɨlɨgar</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_518" n="HIAT:w" s="T143">kihi</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_521" n="HIAT:w" s="T144">kihitin</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_524" n="HIAT:w" s="T145">bilsibet</ts>
                  <nts id="Seg_525" n="HIAT:ip">.</nts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_528" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_530" n="HIAT:w" s="T146">Külsüː</ts>
                  <nts id="Seg_531" n="HIAT:ip">,</nts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_534" n="HIAT:w" s="T147">kepsetiː</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_537" n="HIAT:w" s="T148">bu͡olan</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_540" n="HIAT:w" s="T149">ispit</ts>
                  <nts id="Seg_541" n="HIAT:ip">.</nts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_544" n="HIAT:u" s="T150">
                  <ts e="T151" id="Seg_546" n="HIAT:w" s="T150">Ebe</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_549" n="HIAT:w" s="T151">uːtugar</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_552" n="HIAT:w" s="T152">čaːjka</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_555" n="HIAT:w" s="T153">olororun</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_558" n="HIAT:w" s="T154">kördük</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_561" n="HIAT:w" s="T155">dʼarapalaːn</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_564" n="HIAT:w" s="T156">köbö</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_567" n="HIAT:w" s="T157">hɨldʼɨbɨt</ts>
                  <nts id="Seg_568" n="HIAT:ip">,</nts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_571" n="HIAT:w" s="T158">aːjdaːna</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_574" n="HIAT:w" s="T159">bert</ts>
                  <nts id="Seg_575" n="HIAT:ip">.</nts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_578" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_580" n="HIAT:w" s="T160">Kennitiger</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_583" n="HIAT:w" s="T161">uː</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_586" n="HIAT:w" s="T162">dolgunnurar</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_589" n="HIAT:w" s="T163">küːsteːk</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_592" n="HIAT:w" s="T164">tɨ͡alɨttan</ts>
                  <nts id="Seg_593" n="HIAT:ip">.</nts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_596" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_598" n="HIAT:w" s="T165">Tojoː</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_601" n="HIAT:w" s="T166">kuttana</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_604" n="HIAT:w" s="T167">hataːbɨt</ts>
                  <nts id="Seg_605" n="HIAT:ip">.</nts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_608" n="HIAT:u" s="T168">
                  <nts id="Seg_609" n="HIAT:ip">"</nts>
                  <ts e="T169" id="Seg_611" n="HIAT:w" s="T168">Dʼarapalaːn</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_614" n="HIAT:w" s="T169">uːttan</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_617" n="HIAT:w" s="T170">taksan</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_620" n="HIAT:w" s="T171">kɨtɨlga</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_623" n="HIAT:w" s="T172">taksɨ͡aga</ts>
                  <nts id="Seg_624" n="HIAT:ip">"</nts>
                  <nts id="Seg_625" n="HIAT:ip">,</nts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_628" n="HIAT:w" s="T173">di͡en</ts>
                  <nts id="Seg_629" n="HIAT:ip">,</nts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_631" n="HIAT:ip">"</nts>
                  <ts e="T175" id="Seg_633" n="HIAT:w" s="T174">min</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_636" n="HIAT:w" s="T175">di͡ek</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_639" n="HIAT:w" s="T176">keli͡ege</ts>
                  <nts id="Seg_640" n="HIAT:ip">"</nts>
                  <nts id="Seg_641" n="HIAT:ip">,</nts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_644" n="HIAT:w" s="T177">di͡en</ts>
                  <nts id="Seg_645" n="HIAT:ip">.</nts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T183" id="Seg_648" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_650" n="HIAT:w" s="T178">Turar</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_653" n="HIAT:w" s="T179">kihiler</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_656" n="HIAT:w" s="T180">attɨlarɨttan</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_659" n="HIAT:w" s="T181">ɨraːkkaːn</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_662" n="HIAT:w" s="T182">tejbit</ts>
                  <nts id="Seg_663" n="HIAT:ip">.</nts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_666" n="HIAT:u" s="T183">
                  <ts e="T184" id="Seg_668" n="HIAT:w" s="T183">Tuspa</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_671" n="HIAT:w" s="T184">hogotogun</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_674" n="HIAT:w" s="T185">turbut</ts>
                  <nts id="Seg_675" n="HIAT:ip">.</nts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_678" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_680" n="HIAT:w" s="T186">Dʼarapalaːna</ts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_683" n="HIAT:w" s="T187">tɨː</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_686" n="HIAT:w" s="T188">kördük</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_689" n="HIAT:w" s="T189">munnutunan</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_692" n="HIAT:w" s="T190">kɨtɨlga</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_695" n="HIAT:w" s="T191">toktoːbut</ts>
                  <nts id="Seg_696" n="HIAT:ip">.</nts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_699" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_701" n="HIAT:w" s="T192">Aːjdaːn</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_704" n="HIAT:w" s="T193">büppüt</ts>
                  <nts id="Seg_705" n="HIAT:ip">.</nts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T201" id="Seg_708" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_710" n="HIAT:w" s="T194">Ürdütten</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_713" n="HIAT:w" s="T195">aːn</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_716" n="HIAT:w" s="T196">arɨllɨbɨt</ts>
                  <nts id="Seg_717" n="HIAT:ip">,</nts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_720" n="HIAT:w" s="T197">kihi</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_723" n="HIAT:w" s="T198">töbötö</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_726" n="HIAT:w" s="T199">köstö</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_729" n="HIAT:w" s="T200">bi͡erbit</ts>
                  <nts id="Seg_730" n="HIAT:ip">.</nts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_733" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_735" n="HIAT:w" s="T201">Kihi</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_738" n="HIAT:w" s="T202">taksa</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_741" n="HIAT:w" s="T203">kötöːt</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_744" n="HIAT:w" s="T204">dʼarapalaːn</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_747" n="HIAT:w" s="T205">kɨnatɨn</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_750" n="HIAT:w" s="T206">ürdüger</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_753" n="HIAT:w" s="T207">turan</ts>
                  <nts id="Seg_754" n="HIAT:ip">,</nts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_757" n="HIAT:w" s="T208">bɨragɨllɨbɨt</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_760" n="HIAT:w" s="T209">bɨː</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_763" n="HIAT:w" s="T210">uhugun</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_766" n="HIAT:w" s="T211">kaban</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_769" n="HIAT:w" s="T212">ɨlbɨt</ts>
                  <nts id="Seg_770" n="HIAT:ip">,</nts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_773" n="HIAT:w" s="T213">samolʼotɨn</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_776" n="HIAT:w" s="T214">tabalɨː</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_779" n="HIAT:w" s="T215">baːjan</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_782" n="HIAT:w" s="T216">keːspit</ts>
                  <nts id="Seg_783" n="HIAT:ip">.</nts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T221" id="Seg_786" n="HIAT:u" s="T217">
                  <nts id="Seg_787" n="HIAT:ip">"</nts>
                  <ts e="T218" id="Seg_789" n="HIAT:w" s="T217">Dʼe</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_792" n="HIAT:w" s="T218">intiri͡es</ts>
                  <nts id="Seg_793" n="HIAT:ip">,</nts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_796" n="HIAT:w" s="T219">tɨː</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_799" n="HIAT:w" s="T220">kördük</ts>
                  <nts id="Seg_800" n="HIAT:ip">.</nts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_803" n="HIAT:u" s="T221">
                  <ts e="T222" id="Seg_805" n="HIAT:w" s="T221">Tabalɨː</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_808" n="HIAT:w" s="T222">da</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_811" n="HIAT:w" s="T223">baːjɨllar</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_814" n="HIAT:w" s="T224">ebit</ts>
                  <nts id="Seg_815" n="HIAT:ip">"</nts>
                  <nts id="Seg_816" n="HIAT:ip">,</nts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_819" n="HIAT:w" s="T225">diː</ts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_822" n="HIAT:w" s="T226">hanɨː</ts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_825" n="HIAT:w" s="T227">turbut</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_828" n="HIAT:w" s="T228">Tojoː</ts>
                  <nts id="Seg_829" n="HIAT:ip">.</nts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_832" n="HIAT:u" s="T229">
                  <ts e="T230" id="Seg_834" n="HIAT:w" s="T229">Ogolor</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_837" n="HIAT:w" s="T230">bu͡ollaktarɨna</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_840" n="HIAT:w" s="T231">ütüːlühe-ütüːlühe</ts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_843" n="HIAT:w" s="T232">dʼarapalaːn</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_846" n="HIAT:w" s="T233">di͡ek</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_849" n="HIAT:w" s="T234">hüreleːbitter</ts>
                  <nts id="Seg_850" n="HIAT:ip">.</nts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_853" n="HIAT:u" s="T235">
                  <ts e="T236" id="Seg_855" n="HIAT:w" s="T235">Tojoː</ts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_858" n="HIAT:w" s="T236">hogotogun</ts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_861" n="HIAT:w" s="T237">turan</ts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_864" n="HIAT:w" s="T238">emi͡e</ts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_867" n="HIAT:w" s="T239">dʼi͡etin</ts>
                  <nts id="Seg_868" n="HIAT:ip">,</nts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_871" n="HIAT:w" s="T240">inʼetin</ts>
                  <nts id="Seg_872" n="HIAT:ip">,</nts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_875" n="HIAT:w" s="T241">agatɨn</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_878" n="HIAT:w" s="T242">öjdöːbüt</ts>
                  <nts id="Seg_879" n="HIAT:ip">.</nts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T252" id="Seg_882" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_884" n="HIAT:w" s="T243">Ginner</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_887" n="HIAT:w" s="T244">ire</ts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_889" n="HIAT:ip">(</nts>
                  <ts e="T246" id="Seg_891" n="HIAT:w" s="T245">körbüt-</ts>
                  <nts id="Seg_892" n="HIAT:ip">)</nts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_895" n="HIAT:w" s="T246">körböttör</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_898" n="HIAT:w" s="T247">dʼarapalaːnɨ</ts>
                  <nts id="Seg_899" n="HIAT:ip">,</nts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_902" n="HIAT:w" s="T248">kajtak</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_905" n="HIAT:w" s="T249">olororun</ts>
                  <nts id="Seg_906" n="HIAT:ip">,</nts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_909" n="HIAT:w" s="T250">kajtak</ts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_912" n="HIAT:w" s="T251">kötörün</ts>
                  <nts id="Seg_913" n="HIAT:ip">.</nts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_916" n="HIAT:u" s="T252">
                  <ts e="T253" id="Seg_918" n="HIAT:w" s="T252">Össü͡ö</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_921" n="HIAT:w" s="T253">olus</ts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_924" n="HIAT:w" s="T254">dʼikti</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_927" n="HIAT:w" s="T255">bu͡olu͡ok</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_930" n="HIAT:w" s="T256">ete</ts>
                  <nts id="Seg_931" n="HIAT:ip">,</nts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_934" n="HIAT:w" s="T257">dʼi͡etiger</ts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_937" n="HIAT:w" s="T258">kötön</ts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_940" n="HIAT:w" s="T259">kelbite</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_943" n="HIAT:w" s="T260">bu͡ollar</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_946" n="HIAT:w" s="T261">iti</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_949" n="HIAT:w" s="T262">dʼarapalaːna</ts>
                  <nts id="Seg_950" n="HIAT:ip">.</nts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T269" id="Seg_953" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_955" n="HIAT:w" s="T263">Kötön</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_958" n="HIAT:w" s="T264">ihen</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_961" n="HIAT:w" s="T265">barɨ</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_964" n="HIAT:w" s="T266">mu͡oranɨ</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_967" n="HIAT:w" s="T267">körü͡ögün</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_970" n="HIAT:w" s="T268">bagarbɨt</ts>
                  <nts id="Seg_971" n="HIAT:ip">.</nts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T275" id="Seg_974" n="HIAT:u" s="T269">
                  <ts e="T270" id="Seg_976" n="HIAT:w" s="T269">Olorbut</ts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_979" n="HIAT:w" s="T270">ologun</ts>
                  <nts id="Seg_980" n="HIAT:ip">,</nts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_983" n="HIAT:w" s="T271">uraha</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_986" n="HIAT:w" s="T272">dʼi͡etin</ts>
                  <nts id="Seg_987" n="HIAT:ip">,</nts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_990" n="HIAT:w" s="T273">oːnnʼoːbut</ts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_993" n="HIAT:w" s="T274">hirderin</ts>
                  <nts id="Seg_994" n="HIAT:ip">.</nts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_997" n="HIAT:u" s="T275">
                  <ts e="T276" id="Seg_999" n="HIAT:w" s="T275">Dʼarapalaːn</ts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1002" n="HIAT:w" s="T276">kötü͡ör</ts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1005" n="HIAT:w" s="T277">di͡eri</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1008" n="HIAT:w" s="T278">Tojoː</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1011" n="HIAT:w" s="T279">dogottorun</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1014" n="HIAT:w" s="T280">gɨtta</ts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1017" n="HIAT:w" s="T281">kɨtɨlga</ts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1020" n="HIAT:w" s="T282">turbuttar</ts>
                  <nts id="Seg_1021" n="HIAT:ip">,</nts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1024" n="HIAT:w" s="T283">ahɨː</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1027" n="HIAT:w" s="T284">da</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1030" n="HIAT:w" s="T285">barbakka</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1033" n="HIAT:w" s="T286">intʼernaːttarɨgar</ts>
                  <nts id="Seg_1034" n="HIAT:ip">.</nts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T297" id="Seg_1037" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_1039" n="HIAT:w" s="T287">Dʼarapalaːn</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1042" n="HIAT:w" s="T288">tuhunan</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1045" n="HIAT:w" s="T289">kepsetiː</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1048" n="HIAT:w" s="T290">tahaːra</ts>
                  <nts id="Seg_1049" n="HIAT:ip">,</nts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1052" n="HIAT:w" s="T291">barɨ</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1055" n="HIAT:w" s="T292">dʼi͡e</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1058" n="HIAT:w" s="T293">aːjɨ</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1061" n="HIAT:w" s="T294">kahan</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1064" n="HIAT:w" s="T295">da</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1067" n="HIAT:w" s="T296">büppet</ts>
                  <nts id="Seg_1068" n="HIAT:ip">.</nts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T297" id="Seg_1070" n="sc" s="T0">
               <ts e="T1" id="Seg_1072" n="e" s="T0">Anɨ </ts>
               <ts e="T2" id="Seg_1074" n="e" s="T1">ihilleːŋ </ts>
               <ts e="T3" id="Seg_1076" n="e" s="T2">nöŋü͡ö </ts>
               <ts e="T4" id="Seg_1078" n="e" s="T3">öhü. </ts>
               <ts e="T5" id="Seg_1080" n="e" s="T4">Aːta </ts>
               <ts e="T6" id="Seg_1082" n="e" s="T5">"Dʼarapalaːn". </ts>
               <ts e="T7" id="Seg_1084" n="e" s="T6">Tojoː </ts>
               <ts e="T8" id="Seg_1086" n="e" s="T7">ühüs </ts>
               <ts e="T9" id="Seg_1088" n="e" s="T8">künüger </ts>
               <ts e="T10" id="Seg_1090" n="e" s="T9">emi͡e </ts>
               <ts e="T11" id="Seg_1092" n="e" s="T10">körbüt </ts>
               <ts e="T12" id="Seg_1094" n="e" s="T11">biːr </ts>
               <ts e="T13" id="Seg_1096" n="e" s="T12">dʼiktini. </ts>
               <ts e="T14" id="Seg_1098" n="e" s="T13">Künüs </ts>
               <ts e="T15" id="Seg_1100" n="e" s="T14">ahaːn </ts>
               <ts e="T16" id="Seg_1102" n="e" s="T15">büteːt, </ts>
               <ts e="T17" id="Seg_1104" n="e" s="T16">tahaːra </ts>
               <ts e="T18" id="Seg_1106" n="e" s="T17">hüreliː </ts>
               <ts e="T19" id="Seg_1108" n="e" s="T18">hɨldʼɨbɨt </ts>
               <ts e="T20" id="Seg_1110" n="e" s="T19">uːlʼica </ts>
               <ts e="T21" id="Seg_1112" n="e" s="T20">üstün. </ts>
               <ts e="T22" id="Seg_1114" n="e" s="T21">"Dʼarapalaːn </ts>
               <ts e="T23" id="Seg_1116" n="e" s="T22">kötön </ts>
               <ts e="T24" id="Seg_1118" n="e" s="T23">keli͡e", </ts>
               <ts e="T25" id="Seg_1120" n="e" s="T24">di͡en </ts>
               <ts e="T26" id="Seg_1122" n="e" s="T25">öhü </ts>
               <ts e="T27" id="Seg_1124" n="e" s="T26">istibit. </ts>
               <ts e="T28" id="Seg_1126" n="e" s="T27">Barɨ </ts>
               <ts e="T29" id="Seg_1128" n="e" s="T28">kihiler </ts>
               <ts e="T30" id="Seg_1130" n="e" s="T29">küːteller </ts>
               <ts e="T31" id="Seg_1132" n="e" s="T30">ebit </ts>
               <ts e="T32" id="Seg_1134" n="e" s="T31">dʼarapalaːnɨ. </ts>
               <ts e="T33" id="Seg_1136" n="e" s="T32">Haŋa </ts>
               <ts e="T34" id="Seg_1138" n="e" s="T33">kihiler </ts>
               <ts e="T35" id="Seg_1140" n="e" s="T34">kelellerin </ts>
               <ts e="T36" id="Seg_1142" n="e" s="T35">köröːrü, </ts>
               <ts e="T37" id="Seg_1144" n="e" s="T36">kantan </ts>
               <ts e="T38" id="Seg_1146" n="e" s="T37">ire </ts>
               <ts e="T39" id="Seg_1148" n="e" s="T38">huruk </ts>
               <ts e="T40" id="Seg_1150" n="e" s="T39">ɨlaːrɨ. </ts>
               <ts e="T41" id="Seg_1152" n="e" s="T40">Tojoː </ts>
               <ts e="T42" id="Seg_1154" n="e" s="T41">kahan </ts>
               <ts e="T43" id="Seg_1156" n="e" s="T42">daː </ts>
               <ts e="T44" id="Seg_1158" n="e" s="T43">körö </ts>
               <ts e="T45" id="Seg_1160" n="e" s="T44">ilik </ts>
               <ts e="T46" id="Seg_1162" n="e" s="T45">ebit </ts>
               <ts e="T47" id="Seg_1164" n="e" s="T46">dʼarapalaːnɨ. </ts>
               <ts e="T48" id="Seg_1166" n="e" s="T47">Kajtak </ts>
               <ts e="T49" id="Seg_1168" n="e" s="T48">ebitej </ts>
               <ts e="T50" id="Seg_1170" n="e" s="T49">iti </ts>
               <ts e="T51" id="Seg_1172" n="e" s="T50">kötörö? </ts>
               <ts e="T52" id="Seg_1174" n="e" s="T51">Tu͡ok </ts>
               <ts e="T53" id="Seg_1176" n="e" s="T52">uraːŋkaj </ts>
               <ts e="T54" id="Seg_1178" n="e" s="T53">biler? </ts>
               <ts e="T55" id="Seg_1180" n="e" s="T54">Hol </ts>
               <ts e="T56" id="Seg_1182" n="e" s="T55">daː </ts>
               <ts e="T57" id="Seg_1184" n="e" s="T56">bu͡ollar </ts>
               <ts e="T58" id="Seg_1186" n="e" s="T57">hin </ts>
               <ts e="T59" id="Seg_1188" n="e" s="T58">küːter. </ts>
               <ts e="T60" id="Seg_1190" n="e" s="T59">Töhö </ts>
               <ts e="T61" id="Seg_1192" n="e" s="T60">daː </ts>
               <ts e="T62" id="Seg_1194" n="e" s="T61">ör </ts>
               <ts e="T63" id="Seg_1196" n="e" s="T62">bu͡olbatak, </ts>
               <ts e="T64" id="Seg_1198" n="e" s="T63">etiŋ </ts>
               <ts e="T65" id="Seg_1200" n="e" s="T64">eterin </ts>
               <ts e="T66" id="Seg_1202" n="e" s="T65">kördük </ts>
               <ts e="T67" id="Seg_1204" n="e" s="T66">kallaːŋŋa </ts>
               <ts e="T68" id="Seg_1206" n="e" s="T67">aːjdaːn </ts>
               <ts e="T69" id="Seg_1208" n="e" s="T68">bu͡olbut. </ts>
               <ts e="T70" id="Seg_1210" n="e" s="T69">Onton </ts>
               <ts e="T71" id="Seg_1212" n="e" s="T70">tu͡ok </ts>
               <ts e="T72" id="Seg_1214" n="e" s="T71">da </ts>
               <ts e="T73" id="Seg_1216" n="e" s="T72">kördük </ts>
               <ts e="T74" id="Seg_1218" n="e" s="T73">kötör </ts>
               <ts e="T75" id="Seg_1220" n="e" s="T74">kötö </ts>
               <ts e="T76" id="Seg_1222" n="e" s="T75">hɨldʼɨbɨt </ts>
               <ts e="T77" id="Seg_1224" n="e" s="T76">bɨlɨttar </ts>
               <ts e="T78" id="Seg_1226" n="e" s="T77">ürdüleriger. </ts>
               <ts e="T79" id="Seg_1228" n="e" s="T78">Tojoː </ts>
               <ts e="T80" id="Seg_1230" n="e" s="T79">kuttammɨt </ts>
               <ts e="T81" id="Seg_1232" n="e" s="T80">če </ts>
               <ts e="T82" id="Seg_1234" n="e" s="T81">turbut </ts>
               <ts e="T83" id="Seg_1236" n="e" s="T82">hiriger </ts>
               <ts e="T84" id="Seg_1238" n="e" s="T83">"lak" </ts>
               <ts e="T85" id="Seg_1240" n="e" s="T84">gɨna </ts>
               <ts e="T86" id="Seg_1242" n="e" s="T85">olorbut, </ts>
               <ts e="T87" id="Seg_1244" n="e" s="T86">karaktarɨn </ts>
               <ts e="T88" id="Seg_1246" n="e" s="T87">happɨt, </ts>
               <ts e="T89" id="Seg_1248" n="e" s="T88">toŋmut </ts>
               <ts e="T90" id="Seg_1250" n="e" s="T89">kördük </ts>
               <ts e="T91" id="Seg_1252" n="e" s="T90">titiriː </ts>
               <ts e="T92" id="Seg_1254" n="e" s="T91">olorbut. </ts>
               <ts e="T93" id="Seg_1256" n="e" s="T92">"Tu͡ok </ts>
               <ts e="T94" id="Seg_1258" n="e" s="T93">bu͡olluŋ? </ts>
               <ts e="T95" id="Seg_1260" n="e" s="T94">Hüːr </ts>
               <ts e="T96" id="Seg_1262" n="e" s="T95">türgennik </ts>
               <ts e="T97" id="Seg_1264" n="e" s="T96">kɨtɨlga, </ts>
               <ts e="T98" id="Seg_1266" n="e" s="T97">körö </ts>
               <ts e="T99" id="Seg_1268" n="e" s="T98">bar, </ts>
               <ts e="T100" id="Seg_1270" n="e" s="T99">kajdak </ts>
               <ts e="T101" id="Seg_1272" n="e" s="T100">samalʼot </ts>
               <ts e="T102" id="Seg_1274" n="e" s="T101">olororun </ts>
               <ts e="T103" id="Seg_1276" n="e" s="T102">uːga", </ts>
               <ts e="T104" id="Seg_1278" n="e" s="T103">učitʼelʼin </ts>
               <ts e="T105" id="Seg_1280" n="e" s="T104">haŋatɨn </ts>
               <ts e="T106" id="Seg_1282" n="e" s="T105">istibit. </ts>
               <ts e="T107" id="Seg_1284" n="e" s="T106">Tojoː </ts>
               <ts e="T108" id="Seg_1286" n="e" s="T107">karaktarɨn </ts>
               <ts e="T109" id="Seg_1288" n="e" s="T108">arɨjbɨta, </ts>
               <ts e="T110" id="Seg_1290" n="e" s="T109">Valačaːnka </ts>
               <ts e="T111" id="Seg_1292" n="e" s="T110">barɨ </ts>
               <ts e="T112" id="Seg_1294" n="e" s="T111">nehili͡ege, </ts>
               <ts e="T113" id="Seg_1296" n="e" s="T112">turar </ts>
               <ts e="T114" id="Seg_1298" n="e" s="T113">turbat, </ts>
               <ts e="T115" id="Seg_1300" n="e" s="T114">gini </ts>
               <ts e="T116" id="Seg_1302" n="e" s="T115">attɨnan </ts>
               <ts e="T117" id="Seg_1304" n="e" s="T116">hüːreleːn </ts>
               <ts e="T118" id="Seg_1306" n="e" s="T117">aːhan </ts>
               <ts e="T119" id="Seg_1308" n="e" s="T118">ebitter. </ts>
               <ts e="T120" id="Seg_1310" n="e" s="T119">Kihiler </ts>
               <ts e="T121" id="Seg_1312" n="e" s="T120">kuttammattar. </ts>
               <ts e="T122" id="Seg_1314" n="e" s="T121">"Min </ts>
               <ts e="T123" id="Seg_1316" n="e" s="T122">hin </ts>
               <ts e="T124" id="Seg_1318" n="e" s="T123">tu͡ok </ts>
               <ts e="T125" id="Seg_1320" n="e" s="T124">daː </ts>
               <ts e="T126" id="Seg_1322" n="e" s="T125">bu͡olu͡om </ts>
               <ts e="T127" id="Seg_1324" n="e" s="T126">hu͡oga, </ts>
               <ts e="T128" id="Seg_1326" n="e" s="T127">badaga", </ts>
               <ts e="T129" id="Seg_1328" n="e" s="T128">diː </ts>
               <ts e="T130" id="Seg_1330" n="e" s="T129">hanaːbɨt </ts>
               <ts e="T131" id="Seg_1332" n="e" s="T130">Tojoː. </ts>
               <ts e="T132" id="Seg_1334" n="e" s="T131">Tura </ts>
               <ts e="T133" id="Seg_1336" n="e" s="T132">ekkireːt </ts>
               <ts e="T134" id="Seg_1338" n="e" s="T133">ele </ts>
               <ts e="T135" id="Seg_1340" n="e" s="T134">küːhünen </ts>
               <ts e="T136" id="Seg_1342" n="e" s="T135">teppit </ts>
               <ts e="T137" id="Seg_1344" n="e" s="T136">hüːren, </ts>
               <ts e="T138" id="Seg_1346" n="e" s="T137">ɨstaŋalɨː-ɨstaŋalɨː </ts>
               <ts e="T139" id="Seg_1348" n="e" s="T138">kɨra </ts>
               <ts e="T140" id="Seg_1350" n="e" s="T139">čalbaktar </ts>
               <ts e="T141" id="Seg_1352" n="e" s="T140">ürdülerinen. </ts>
               <ts e="T142" id="Seg_1354" n="e" s="T141">Ebe </ts>
               <ts e="T143" id="Seg_1356" n="e" s="T142">kɨtɨlɨgar </ts>
               <ts e="T144" id="Seg_1358" n="e" s="T143">kihi </ts>
               <ts e="T145" id="Seg_1360" n="e" s="T144">kihitin </ts>
               <ts e="T146" id="Seg_1362" n="e" s="T145">bilsibet. </ts>
               <ts e="T147" id="Seg_1364" n="e" s="T146">Külsüː, </ts>
               <ts e="T148" id="Seg_1366" n="e" s="T147">kepsetiː </ts>
               <ts e="T149" id="Seg_1368" n="e" s="T148">bu͡olan </ts>
               <ts e="T150" id="Seg_1370" n="e" s="T149">ispit. </ts>
               <ts e="T151" id="Seg_1372" n="e" s="T150">Ebe </ts>
               <ts e="T152" id="Seg_1374" n="e" s="T151">uːtugar </ts>
               <ts e="T153" id="Seg_1376" n="e" s="T152">čaːjka </ts>
               <ts e="T154" id="Seg_1378" n="e" s="T153">olororun </ts>
               <ts e="T155" id="Seg_1380" n="e" s="T154">kördük </ts>
               <ts e="T156" id="Seg_1382" n="e" s="T155">dʼarapalaːn </ts>
               <ts e="T157" id="Seg_1384" n="e" s="T156">köbö </ts>
               <ts e="T158" id="Seg_1386" n="e" s="T157">hɨldʼɨbɨt, </ts>
               <ts e="T159" id="Seg_1388" n="e" s="T158">aːjdaːna </ts>
               <ts e="T160" id="Seg_1390" n="e" s="T159">bert. </ts>
               <ts e="T161" id="Seg_1392" n="e" s="T160">Kennitiger </ts>
               <ts e="T162" id="Seg_1394" n="e" s="T161">uː </ts>
               <ts e="T163" id="Seg_1396" n="e" s="T162">dolgunnurar </ts>
               <ts e="T164" id="Seg_1398" n="e" s="T163">küːsteːk </ts>
               <ts e="T165" id="Seg_1400" n="e" s="T164">tɨ͡alɨttan. </ts>
               <ts e="T166" id="Seg_1402" n="e" s="T165">Tojoː </ts>
               <ts e="T167" id="Seg_1404" n="e" s="T166">kuttana </ts>
               <ts e="T168" id="Seg_1406" n="e" s="T167">hataːbɨt. </ts>
               <ts e="T169" id="Seg_1408" n="e" s="T168">"Dʼarapalaːn </ts>
               <ts e="T170" id="Seg_1410" n="e" s="T169">uːttan </ts>
               <ts e="T171" id="Seg_1412" n="e" s="T170">taksan </ts>
               <ts e="T172" id="Seg_1414" n="e" s="T171">kɨtɨlga </ts>
               <ts e="T173" id="Seg_1416" n="e" s="T172">taksɨ͡aga", </ts>
               <ts e="T174" id="Seg_1418" n="e" s="T173">di͡en, </ts>
               <ts e="T175" id="Seg_1420" n="e" s="T174">"min </ts>
               <ts e="T176" id="Seg_1422" n="e" s="T175">di͡ek </ts>
               <ts e="T177" id="Seg_1424" n="e" s="T176">keli͡ege", </ts>
               <ts e="T178" id="Seg_1426" n="e" s="T177">di͡en. </ts>
               <ts e="T179" id="Seg_1428" n="e" s="T178">Turar </ts>
               <ts e="T180" id="Seg_1430" n="e" s="T179">kihiler </ts>
               <ts e="T181" id="Seg_1432" n="e" s="T180">attɨlarɨttan </ts>
               <ts e="T182" id="Seg_1434" n="e" s="T181">ɨraːkkaːn </ts>
               <ts e="T183" id="Seg_1436" n="e" s="T182">tejbit. </ts>
               <ts e="T184" id="Seg_1438" n="e" s="T183">Tuspa </ts>
               <ts e="T185" id="Seg_1440" n="e" s="T184">hogotogun </ts>
               <ts e="T186" id="Seg_1442" n="e" s="T185">turbut. </ts>
               <ts e="T187" id="Seg_1444" n="e" s="T186">Dʼarapalaːna </ts>
               <ts e="T188" id="Seg_1446" n="e" s="T187">tɨː </ts>
               <ts e="T189" id="Seg_1448" n="e" s="T188">kördük </ts>
               <ts e="T190" id="Seg_1450" n="e" s="T189">munnutunan </ts>
               <ts e="T191" id="Seg_1452" n="e" s="T190">kɨtɨlga </ts>
               <ts e="T192" id="Seg_1454" n="e" s="T191">toktoːbut. </ts>
               <ts e="T193" id="Seg_1456" n="e" s="T192">Aːjdaːn </ts>
               <ts e="T194" id="Seg_1458" n="e" s="T193">büppüt. </ts>
               <ts e="T195" id="Seg_1460" n="e" s="T194">Ürdütten </ts>
               <ts e="T196" id="Seg_1462" n="e" s="T195">aːn </ts>
               <ts e="T197" id="Seg_1464" n="e" s="T196">arɨllɨbɨt, </ts>
               <ts e="T198" id="Seg_1466" n="e" s="T197">kihi </ts>
               <ts e="T199" id="Seg_1468" n="e" s="T198">töbötö </ts>
               <ts e="T200" id="Seg_1470" n="e" s="T199">köstö </ts>
               <ts e="T201" id="Seg_1472" n="e" s="T200">bi͡erbit. </ts>
               <ts e="T202" id="Seg_1474" n="e" s="T201">Kihi </ts>
               <ts e="T203" id="Seg_1476" n="e" s="T202">taksa </ts>
               <ts e="T204" id="Seg_1478" n="e" s="T203">kötöːt </ts>
               <ts e="T205" id="Seg_1480" n="e" s="T204">dʼarapalaːn </ts>
               <ts e="T206" id="Seg_1482" n="e" s="T205">kɨnatɨn </ts>
               <ts e="T207" id="Seg_1484" n="e" s="T206">ürdüger </ts>
               <ts e="T208" id="Seg_1486" n="e" s="T207">turan, </ts>
               <ts e="T209" id="Seg_1488" n="e" s="T208">bɨragɨllɨbɨt </ts>
               <ts e="T210" id="Seg_1490" n="e" s="T209">bɨː </ts>
               <ts e="T211" id="Seg_1492" n="e" s="T210">uhugun </ts>
               <ts e="T212" id="Seg_1494" n="e" s="T211">kaban </ts>
               <ts e="T213" id="Seg_1496" n="e" s="T212">ɨlbɨt, </ts>
               <ts e="T214" id="Seg_1498" n="e" s="T213">samolʼotɨn </ts>
               <ts e="T215" id="Seg_1500" n="e" s="T214">tabalɨː </ts>
               <ts e="T216" id="Seg_1502" n="e" s="T215">baːjan </ts>
               <ts e="T217" id="Seg_1504" n="e" s="T216">keːspit. </ts>
               <ts e="T218" id="Seg_1506" n="e" s="T217">"Dʼe </ts>
               <ts e="T219" id="Seg_1508" n="e" s="T218">intiri͡es, </ts>
               <ts e="T220" id="Seg_1510" n="e" s="T219">tɨː </ts>
               <ts e="T221" id="Seg_1512" n="e" s="T220">kördük. </ts>
               <ts e="T222" id="Seg_1514" n="e" s="T221">Tabalɨː </ts>
               <ts e="T223" id="Seg_1516" n="e" s="T222">da </ts>
               <ts e="T224" id="Seg_1518" n="e" s="T223">baːjɨllar </ts>
               <ts e="T225" id="Seg_1520" n="e" s="T224">ebit", </ts>
               <ts e="T226" id="Seg_1522" n="e" s="T225">diː </ts>
               <ts e="T227" id="Seg_1524" n="e" s="T226">hanɨː </ts>
               <ts e="T228" id="Seg_1526" n="e" s="T227">turbut </ts>
               <ts e="T229" id="Seg_1528" n="e" s="T228">Tojoː. </ts>
               <ts e="T230" id="Seg_1530" n="e" s="T229">Ogolor </ts>
               <ts e="T231" id="Seg_1532" n="e" s="T230">bu͡ollaktarɨna </ts>
               <ts e="T232" id="Seg_1534" n="e" s="T231">ütüːlühe-ütüːlühe </ts>
               <ts e="T233" id="Seg_1536" n="e" s="T232">dʼarapalaːn </ts>
               <ts e="T234" id="Seg_1538" n="e" s="T233">di͡ek </ts>
               <ts e="T235" id="Seg_1540" n="e" s="T234">hüreleːbitter. </ts>
               <ts e="T236" id="Seg_1542" n="e" s="T235">Tojoː </ts>
               <ts e="T237" id="Seg_1544" n="e" s="T236">hogotogun </ts>
               <ts e="T238" id="Seg_1546" n="e" s="T237">turan </ts>
               <ts e="T239" id="Seg_1548" n="e" s="T238">emi͡e </ts>
               <ts e="T240" id="Seg_1550" n="e" s="T239">dʼi͡etin, </ts>
               <ts e="T241" id="Seg_1552" n="e" s="T240">inʼetin, </ts>
               <ts e="T242" id="Seg_1554" n="e" s="T241">agatɨn </ts>
               <ts e="T243" id="Seg_1556" n="e" s="T242">öjdöːbüt. </ts>
               <ts e="T244" id="Seg_1558" n="e" s="T243">Ginner </ts>
               <ts e="T245" id="Seg_1560" n="e" s="T244">ire </ts>
               <ts e="T246" id="Seg_1562" n="e" s="T245">(körbüt-) </ts>
               <ts e="T247" id="Seg_1564" n="e" s="T246">körböttör </ts>
               <ts e="T248" id="Seg_1566" n="e" s="T247">dʼarapalaːnɨ, </ts>
               <ts e="T249" id="Seg_1568" n="e" s="T248">kajtak </ts>
               <ts e="T250" id="Seg_1570" n="e" s="T249">olororun, </ts>
               <ts e="T251" id="Seg_1572" n="e" s="T250">kajtak </ts>
               <ts e="T252" id="Seg_1574" n="e" s="T251">kötörün. </ts>
               <ts e="T253" id="Seg_1576" n="e" s="T252">Össü͡ö </ts>
               <ts e="T254" id="Seg_1578" n="e" s="T253">olus </ts>
               <ts e="T255" id="Seg_1580" n="e" s="T254">dʼikti </ts>
               <ts e="T256" id="Seg_1582" n="e" s="T255">bu͡olu͡ok </ts>
               <ts e="T257" id="Seg_1584" n="e" s="T256">ete, </ts>
               <ts e="T258" id="Seg_1586" n="e" s="T257">dʼi͡etiger </ts>
               <ts e="T259" id="Seg_1588" n="e" s="T258">kötön </ts>
               <ts e="T260" id="Seg_1590" n="e" s="T259">kelbite </ts>
               <ts e="T261" id="Seg_1592" n="e" s="T260">bu͡ollar </ts>
               <ts e="T262" id="Seg_1594" n="e" s="T261">iti </ts>
               <ts e="T263" id="Seg_1596" n="e" s="T262">dʼarapalaːna. </ts>
               <ts e="T264" id="Seg_1598" n="e" s="T263">Kötön </ts>
               <ts e="T265" id="Seg_1600" n="e" s="T264">ihen </ts>
               <ts e="T266" id="Seg_1602" n="e" s="T265">barɨ </ts>
               <ts e="T267" id="Seg_1604" n="e" s="T266">mu͡oranɨ </ts>
               <ts e="T268" id="Seg_1606" n="e" s="T267">körü͡ögün </ts>
               <ts e="T269" id="Seg_1608" n="e" s="T268">bagarbɨt. </ts>
               <ts e="T270" id="Seg_1610" n="e" s="T269">Olorbut </ts>
               <ts e="T271" id="Seg_1612" n="e" s="T270">ologun, </ts>
               <ts e="T272" id="Seg_1614" n="e" s="T271">uraha </ts>
               <ts e="T273" id="Seg_1616" n="e" s="T272">dʼi͡etin, </ts>
               <ts e="T274" id="Seg_1618" n="e" s="T273">oːnnʼoːbut </ts>
               <ts e="T275" id="Seg_1620" n="e" s="T274">hirderin. </ts>
               <ts e="T276" id="Seg_1622" n="e" s="T275">Dʼarapalaːn </ts>
               <ts e="T277" id="Seg_1624" n="e" s="T276">kötü͡ör </ts>
               <ts e="T278" id="Seg_1626" n="e" s="T277">di͡eri </ts>
               <ts e="T279" id="Seg_1628" n="e" s="T278">Tojoː </ts>
               <ts e="T280" id="Seg_1630" n="e" s="T279">dogottorun </ts>
               <ts e="T281" id="Seg_1632" n="e" s="T280">gɨtta </ts>
               <ts e="T282" id="Seg_1634" n="e" s="T281">kɨtɨlga </ts>
               <ts e="T283" id="Seg_1636" n="e" s="T282">turbuttar, </ts>
               <ts e="T284" id="Seg_1638" n="e" s="T283">ahɨː </ts>
               <ts e="T285" id="Seg_1640" n="e" s="T284">da </ts>
               <ts e="T286" id="Seg_1642" n="e" s="T285">barbakka </ts>
               <ts e="T287" id="Seg_1644" n="e" s="T286">intʼernaːttarɨgar. </ts>
               <ts e="T288" id="Seg_1646" n="e" s="T287">Dʼarapalaːn </ts>
               <ts e="T289" id="Seg_1648" n="e" s="T288">tuhunan </ts>
               <ts e="T290" id="Seg_1650" n="e" s="T289">kepsetiː </ts>
               <ts e="T291" id="Seg_1652" n="e" s="T290">tahaːra, </ts>
               <ts e="T292" id="Seg_1654" n="e" s="T291">barɨ </ts>
               <ts e="T293" id="Seg_1656" n="e" s="T292">dʼi͡e </ts>
               <ts e="T294" id="Seg_1658" n="e" s="T293">aːjɨ </ts>
               <ts e="T295" id="Seg_1660" n="e" s="T294">kahan </ts>
               <ts e="T296" id="Seg_1662" n="e" s="T295">da </ts>
               <ts e="T297" id="Seg_1664" n="e" s="T296">büppet. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_1665" s="T0">PoNA_19900810_TojooAirplane_nar.001 (001.001)</ta>
            <ta e="T6" id="Seg_1666" s="T4">PoNA_19900810_TojooAirplane_nar.002 (001.002)</ta>
            <ta e="T13" id="Seg_1667" s="T6">PoNA_19900810_TojooAirplane_nar.003 (001.003)</ta>
            <ta e="T21" id="Seg_1668" s="T13">PoNA_19900810_TojooAirplane_nar.004 (001.004)</ta>
            <ta e="T27" id="Seg_1669" s="T21">PoNA_19900810_TojooAirplane_nar.005 (001.005)</ta>
            <ta e="T32" id="Seg_1670" s="T27">PoNA_19900810_TojooAirplane_nar.006 (001.006)</ta>
            <ta e="T40" id="Seg_1671" s="T32">PoNA_19900810_TojooAirplane_nar.007 (001.007)</ta>
            <ta e="T47" id="Seg_1672" s="T40">PoNA_19900810_TojooAirplane_nar.008 (001.008)</ta>
            <ta e="T51" id="Seg_1673" s="T47">PoNA_19900810_TojooAirplane_nar.009 (001.009)</ta>
            <ta e="T54" id="Seg_1674" s="T51">PoNA_19900810_TojooAirplane_nar.010 (001.010)</ta>
            <ta e="T59" id="Seg_1675" s="T54">PoNA_19900810_TojooAirplane_nar.011 (001.011)</ta>
            <ta e="T69" id="Seg_1676" s="T59">PoNA_19900810_TojooAirplane_nar.012 (001.012)</ta>
            <ta e="T78" id="Seg_1677" s="T69">PoNA_19900810_TojooAirplane_nar.013 (001.013)</ta>
            <ta e="T92" id="Seg_1678" s="T78">PoNA_19900810_TojooAirplane_nar.014 (001.014)</ta>
            <ta e="T94" id="Seg_1679" s="T92">PoNA_19900810_TojooAirplane_nar.015 (001.015)</ta>
            <ta e="T106" id="Seg_1680" s="T94">PoNA_19900810_TojooAirplane_nar.016 (001.016)</ta>
            <ta e="T119" id="Seg_1681" s="T106">PoNA_19900810_TojooAirplane_nar.017 (001.017)</ta>
            <ta e="T121" id="Seg_1682" s="T119">PoNA_19900810_TojooAirplane_nar.018 (001.018)</ta>
            <ta e="T131" id="Seg_1683" s="T121">PoNA_19900810_TojooAirplane_nar.019 (001.019)</ta>
            <ta e="T141" id="Seg_1684" s="T131">PoNA_19900810_TojooAirplane_nar.020 (001.020)</ta>
            <ta e="T146" id="Seg_1685" s="T141">PoNA_19900810_TojooAirplane_nar.021 (001.021)</ta>
            <ta e="T150" id="Seg_1686" s="T146">PoNA_19900810_TojooAirplane_nar.022 (001.022)</ta>
            <ta e="T160" id="Seg_1687" s="T150">PoNA_19900810_TojooAirplane_nar.023 (001.023)</ta>
            <ta e="T165" id="Seg_1688" s="T160">PoNA_19900810_TojooAirplane_nar.024 (001.024)</ta>
            <ta e="T168" id="Seg_1689" s="T165">PoNA_19900810_TojooAirplane_nar.025 (001.025)</ta>
            <ta e="T178" id="Seg_1690" s="T168">PoNA_19900810_TojooAirplane_nar.026 (001.026)</ta>
            <ta e="T183" id="Seg_1691" s="T178">PoNA_19900810_TojooAirplane_nar.027 (001.027)</ta>
            <ta e="T186" id="Seg_1692" s="T183">PoNA_19900810_TojooAirplane_nar.028 (001.028)</ta>
            <ta e="T192" id="Seg_1693" s="T186">PoNA_19900810_TojooAirplane_nar.029 (001.029)</ta>
            <ta e="T194" id="Seg_1694" s="T192">PoNA_19900810_TojooAirplane_nar.030 (001.030)</ta>
            <ta e="T201" id="Seg_1695" s="T194">PoNA_19900810_TojooAirplane_nar.031 (001.031)</ta>
            <ta e="T217" id="Seg_1696" s="T201">PoNA_19900810_TojooAirplane_nar.032 (001.032)</ta>
            <ta e="T221" id="Seg_1697" s="T217">PoNA_19900810_TojooAirplane_nar.033 (001.033)</ta>
            <ta e="T229" id="Seg_1698" s="T221">PoNA_19900810_TojooAirplane_nar.034 (001.034)</ta>
            <ta e="T235" id="Seg_1699" s="T229">PoNA_19900810_TojooAirplane_nar.035 (001.035)</ta>
            <ta e="T243" id="Seg_1700" s="T235">PoNA_19900810_TojooAirplane_nar.036 (001.036)</ta>
            <ta e="T252" id="Seg_1701" s="T243">PoNA_19900810_TojooAirplane_nar.037 (001.037)</ta>
            <ta e="T263" id="Seg_1702" s="T252">PoNA_19900810_TojooAirplane_nar.038 (001.038)</ta>
            <ta e="T269" id="Seg_1703" s="T263">PoNA_19900810_TojooAirplane_nar.039 (001.039)</ta>
            <ta e="T275" id="Seg_1704" s="T269">PoNA_19900810_TojooAirplane_nar.040 (001.040)</ta>
            <ta e="T287" id="Seg_1705" s="T275">PoNA_19900810_TojooAirplane_nar.041 (001.041)</ta>
            <ta e="T297" id="Seg_1706" s="T287">PoNA_19900810_TojooAirplane_nar.042 (001.042)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_1707" s="T0">Аны иһиллээӈ нөӈүө өһү. </ta>
            <ta e="T6" id="Seg_1708" s="T4">Аата "ДЬАРАПАЛААН".</ta>
            <ta e="T13" id="Seg_1709" s="T6">Тойоо үһүс күнүгэр эмиэ көрбүт биир дьиктини.</ta>
            <ta e="T21" id="Seg_1710" s="T13">Күнүс аһаан бүтээт, таһаара һүрэлии һылдьыбыт улица үстүн.</ta>
            <ta e="T27" id="Seg_1711" s="T21">"Дьарапалаан көтөн кэлиэ," – диэн өһү истибит.</ta>
            <ta e="T32" id="Seg_1712" s="T27">Бары киһилэр күүтэллэр эбит дьарапалааны.</ta>
            <ta e="T40" id="Seg_1713" s="T32">Һаӈа киһилэр кэлэллэрин көрөөрү, кантан ирэ (эрэ) һурук ылаары.</ta>
            <ta e="T47" id="Seg_1714" s="T40">Тойоо каһан даа көрө илик эбит дьарапалааны.</ta>
            <ta e="T51" id="Seg_1715" s="T47">Кайтак эбитэй ити көтөрө?</ta>
            <ta e="T54" id="Seg_1716" s="T51">Туок урааӈкай билэр?</ta>
            <ta e="T59" id="Seg_1717" s="T54">Һол даа буоллар һин күүтэр.</ta>
            <ta e="T69" id="Seg_1718" s="T59">Төһө даа өр буолбатак, этиӈ этэрин көрдүк каллаӈӈа аайдаан буолбут.</ta>
            <ta e="T78" id="Seg_1719" s="T69">Онтон туок да көрдүк көтөр… көтө һылдьыбыт былыттар үрдүлэригэр.</ta>
            <ta e="T92" id="Seg_1720" s="T78">Тойоо куттаммыт чэ турбут һиригэр "лак" гына олорбут, карактарын һаппыт, тоӈмут көрдүк титирии олорбут.</ta>
            <ta e="T94" id="Seg_1721" s="T92">"Туок буоллуӈ? </ta>
            <ta e="T106" id="Seg_1722" s="T94">Һүүр түргэнник кытылга, көрө бар, кайдак самолёт олорорун ууга," – учителин һаӈатын истибит.</ta>
            <ta e="T119" id="Seg_1723" s="T106">Тойоо карактарын арыйбыта, Волочаанка бары нэһилиэгэ, турар турбат, гини аттынан һүүрэлээн ааһан эбиттэр.</ta>
            <ta e="T121" id="Seg_1724" s="T119">Киһилэр куттамматтар.</ta>
            <ta e="T131" id="Seg_1725" s="T121">"Мин һин туок даа буолуом һуога, бадага," – дии һанаабыт Тойоо.</ta>
            <ta e="T141" id="Seg_1726" s="T131">Тура эккирээт элэ күүһүнэн тэппит һүүрэн, ыстаӈалыы-ыстаӈалыы кыра чалбактар үрдүлэринэн.</ta>
            <ta e="T146" id="Seg_1727" s="T141">Эбэ кытылыгар киһи киһитин билсибэт.</ta>
            <ta e="T150" id="Seg_1728" s="T146">Күлсүү, кэпсэтии буолан испит.</ta>
            <ta e="T160" id="Seg_1729" s="T150">Эбэ уутугар чайка олорорун көрдүк дьарапалаан көбө һылдьыбыт, аайдаана бэрт.</ta>
            <ta e="T165" id="Seg_1730" s="T160">Кэннитигэр уу долгуннурар күүстээк тыалыттан.</ta>
            <ta e="T168" id="Seg_1731" s="T165">Тойоо куттана һатаабыт.</ta>
            <ta e="T178" id="Seg_1732" s="T168">"Дьарапалаан ууттан таксан кытылга таксыага, – диэн, – мин диэк кэлиэгэ", – диэн.</ta>
            <ta e="T183" id="Seg_1733" s="T178">Турар киһилэр аттыларыттан ырааккаан тэйбит.</ta>
            <ta e="T186" id="Seg_1734" s="T183">Туспа һоготогун турбут.</ta>
            <ta e="T192" id="Seg_1735" s="T186">Дьарапалаана тыы көрдүк муннутунан кытылга токтообут.</ta>
            <ta e="T194" id="Seg_1736" s="T192">Аайдаан бүппүт.</ta>
            <ta e="T201" id="Seg_1737" s="T194">Үрдүттэн аан арыллыбыт, киһи төбөтө көстө биэрбит.</ta>
            <ta e="T217" id="Seg_1738" s="T201">Киһи такса көтөөт дьарапалаан кынатын үрдүгэр туран, бырагыллыбыт быы (быа) уһугун кабан ылбыт, самолётын табалыы баайан кээспит.</ta>
            <ta e="T221" id="Seg_1739" s="T217">"Дьэ интириэс, тыы көрдүк.</ta>
            <ta e="T229" id="Seg_1740" s="T221">Табалыы да баайыллар эбит", – дии һаныы турбут Тойоо.</ta>
            <ta e="T235" id="Seg_1741" s="T229">Оголор буоллактарына үтүүлүһэ (үтүөлэһэ)-үтүүлүһэ (үтүөлэһэ) дьарапалаан диэк һүрэлээбиттэр.</ta>
            <ta e="T243" id="Seg_1742" s="T235">Тойоо һоготогун туран эмиэ дьиэтин, иньэтин, агатын өйдөөбүт.</ta>
            <ta e="T252" id="Seg_1743" s="T243">Гиннэр (гинилэр) ирэ (эрэ) көрбүт көрбөттөр дьарапалааны: кайтак (кайдак) олорорун, кайтак (кайдак) көтөрүн. </ta>
            <ta e="T263" id="Seg_1744" s="T252">Өссүө олус дьикти буолуок этэ, дьиэтигэр көтөн кэлбитэ буоллар ити дьарапалаана. </ta>
            <ta e="T269" id="Seg_1745" s="T263">Көтөн иһэн бары муораны көрүөгүн багарбыт.</ta>
            <ta e="T275" id="Seg_1746" s="T269">Олорбут ологун, ураһа дьиэтин, оонньообут һирдэрин.</ta>
            <ta e="T287" id="Seg_1747" s="T275">Дьарапалаан көтүөр диэри Тойоо доготторун гытта кытылга турбуттар, аһыы да барбакка интернаттарыгар.</ta>
            <ta e="T297" id="Seg_1748" s="T287">Дьарапалаан туһунан кэпсэтии таһаара, бары дьиэ аайы каһан да бүппэт.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_1749" s="T0">Anɨ ihilleːŋ nöŋü͡ö öhü. </ta>
            <ta e="T6" id="Seg_1750" s="T4">Aːta "Dʼarapalaːn". </ta>
            <ta e="T13" id="Seg_1751" s="T6">Tojoː ühüs künüger emi͡e körbüt biːr dʼiktini. </ta>
            <ta e="T21" id="Seg_1752" s="T13">Künüs ahaːn büteːt, tahaːra hüreliː hɨldʼɨbɨt uːlʼica üstün. </ta>
            <ta e="T27" id="Seg_1753" s="T21">"Dʼarapalaːn kötön keli͡e", di͡en öhü istibit. </ta>
            <ta e="T32" id="Seg_1754" s="T27">Barɨ kihiler küːteller ebit dʼarapalaːnɨ. </ta>
            <ta e="T40" id="Seg_1755" s="T32">Haŋa kihiler kelellerin köröːrü, kantan ire huruk ɨlaːrɨ. </ta>
            <ta e="T47" id="Seg_1756" s="T40">Tojoː kahan daː körö ilik ebit dʼarapalaːnɨ. </ta>
            <ta e="T51" id="Seg_1757" s="T47">Kajtak ebitej iti kötörö? </ta>
            <ta e="T54" id="Seg_1758" s="T51">Tu͡ok uraːŋkaj biler? </ta>
            <ta e="T59" id="Seg_1759" s="T54">Hol daː bu͡ollar hin küːter. </ta>
            <ta e="T69" id="Seg_1760" s="T59">Töhö daː ör bu͡olbatak, etiŋ eterin kördük kallaːŋŋa aːjdaːn bu͡olbut. </ta>
            <ta e="T78" id="Seg_1761" s="T69">Onton tu͡ok da kördük kötör kötö hɨldʼɨbɨt bɨlɨttar ürdüleriger. </ta>
            <ta e="T92" id="Seg_1762" s="T78">Tojoː kuttammɨt če turbut hiriger "lak" gɨna olorbut, karaktarɨn happɨt, toŋmut kördük titiriː olorbut. </ta>
            <ta e="T94" id="Seg_1763" s="T92">"Tu͡ok bu͡olluŋ? </ta>
            <ta e="T106" id="Seg_1764" s="T94">Hüːr türgennik kɨtɨlga, körö bar, kajdak samalʼot olororun uːga", učitʼelʼin haŋatɨn istibit. </ta>
            <ta e="T119" id="Seg_1765" s="T106">Tojoː karaktarɨn arɨjbɨta, Valačaːnka barɨ nehili͡ege, turar turbat, gini attɨnan hüːreleːn aːhan ebitter. </ta>
            <ta e="T121" id="Seg_1766" s="T119">Kihiler kuttammattar. </ta>
            <ta e="T131" id="Seg_1767" s="T121">"Min hin tu͡ok daː bu͡olu͡om hu͡oga, badaga", diː hanaːbɨt Tojoː. </ta>
            <ta e="T141" id="Seg_1768" s="T131">Tura ekkireːt ele küːhünen teppit hüːren, ɨstaŋalɨː-ɨstaŋalɨː kɨra čalbaktar ürdülerinen. </ta>
            <ta e="T146" id="Seg_1769" s="T141">Ebe kɨtɨlɨgar kihi kihitin bilsibet. </ta>
            <ta e="T150" id="Seg_1770" s="T146">Külsüː, kepsetiː bu͡olan ispit. </ta>
            <ta e="T160" id="Seg_1771" s="T150">Ebe uːtugar čaːjka olororun kördük dʼarapalaːn köbö hɨldʼɨbɨt, aːjdaːna bert. </ta>
            <ta e="T165" id="Seg_1772" s="T160">Kennitiger uː dolgunnurar küːsteːk tɨ͡alɨttan. </ta>
            <ta e="T168" id="Seg_1773" s="T165">Tojoː kuttana hataːbɨt. </ta>
            <ta e="T178" id="Seg_1774" s="T168">"Dʼarapalaːn uːttan taksan kɨtɨlga taksɨ͡aga", di͡en, "min di͡ek keli͡ege", di͡en. </ta>
            <ta e="T183" id="Seg_1775" s="T178">Turar kihiler attɨlarɨttan ɨraːkkaːn tejbit. </ta>
            <ta e="T186" id="Seg_1776" s="T183">Tuspa hogotogun turbut. </ta>
            <ta e="T192" id="Seg_1777" s="T186">Dʼarapalaːna tɨː kördük munnutunan kɨtɨlga toktoːbut. </ta>
            <ta e="T194" id="Seg_1778" s="T192">Aːjdaːn büppüt. </ta>
            <ta e="T201" id="Seg_1779" s="T194">Ürdütten aːn arɨllɨbɨt, kihi töbötö köstö bi͡erbit. </ta>
            <ta e="T217" id="Seg_1780" s="T201">Kihi taksa kötöːt dʼarapalaːn kɨnatɨn ürdüger turan, bɨragɨllɨbɨt bɨː uhugun kaban ɨlbɨt, samolʼotɨn tabalɨː baːjan keːspit. </ta>
            <ta e="T221" id="Seg_1781" s="T217">"Dʼe intiri͡es, tɨː kördük. </ta>
            <ta e="T229" id="Seg_1782" s="T221">Tabalɨː da baːjɨllar ebit", diː hanɨː turbut Tojoː. </ta>
            <ta e="T235" id="Seg_1783" s="T229">Ogolor bu͡ollaktarɨna ütüːlühe-ütüːlühe dʼarapalaːn di͡ek hüreleːbitter. </ta>
            <ta e="T243" id="Seg_1784" s="T235">Tojoː hogotogun turan emi͡e dʼi͡etin, inʼetin, agatɨn öjdöːbüt. </ta>
            <ta e="T252" id="Seg_1785" s="T243">Ginner ire (körbüt-) körböttör dʼarapalaːnɨ, kajtak olororun, kajtak kötörün. </ta>
            <ta e="T263" id="Seg_1786" s="T252">Össü͡ö olus dʼikti bu͡olu͡ok ete, dʼi͡etiger kötön kelbite bu͡ollar iti dʼarapalaːna. </ta>
            <ta e="T269" id="Seg_1787" s="T263">Kötön ihen barɨ mu͡oranɨ körü͡ögün bagarbɨt. </ta>
            <ta e="T275" id="Seg_1788" s="T269">Olorbut ologun, uraha dʼi͡etin, oːnnʼoːbut hirderin. </ta>
            <ta e="T287" id="Seg_1789" s="T275">Dʼarapalaːn kötü͡ör di͡eri Tojoː dogottorun gɨtta kɨtɨlga turbuttar, ahɨː da barbakka intʼernaːttarɨgar. </ta>
            <ta e="T297" id="Seg_1790" s="T287">Dʼarapalaːn tuhunan kepsetiː tahaːra, barɨ dʼi͡e aːjɨ kahan da büppet. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1791" s="T0">anɨ</ta>
            <ta e="T2" id="Seg_1792" s="T1">ihilleː-ŋ</ta>
            <ta e="T3" id="Seg_1793" s="T2">nöŋü͡ö</ta>
            <ta e="T4" id="Seg_1794" s="T3">öh-ü</ta>
            <ta e="T5" id="Seg_1795" s="T4">aːt-a</ta>
            <ta e="T6" id="Seg_1796" s="T5">dʼarapalaːn</ta>
            <ta e="T7" id="Seg_1797" s="T6">Tojoː</ta>
            <ta e="T8" id="Seg_1798" s="T7">üh-üs</ta>
            <ta e="T9" id="Seg_1799" s="T8">kün-ü-ger</ta>
            <ta e="T10" id="Seg_1800" s="T9">emi͡e</ta>
            <ta e="T11" id="Seg_1801" s="T10">kör-büt</ta>
            <ta e="T12" id="Seg_1802" s="T11">biːr</ta>
            <ta e="T13" id="Seg_1803" s="T12">dʼikti-ni</ta>
            <ta e="T14" id="Seg_1804" s="T13">künüs</ta>
            <ta e="T15" id="Seg_1805" s="T14">ahaː-n</ta>
            <ta e="T16" id="Seg_1806" s="T15">büt-eːt</ta>
            <ta e="T17" id="Seg_1807" s="T16">tahaːra</ta>
            <ta e="T18" id="Seg_1808" s="T17">hür-el-iː</ta>
            <ta e="T19" id="Seg_1809" s="T18">hɨldʼ-ɨ-bɨt</ta>
            <ta e="T20" id="Seg_1810" s="T19">uːlʼica</ta>
            <ta e="T21" id="Seg_1811" s="T20">üstün</ta>
            <ta e="T22" id="Seg_1812" s="T21">dʼarapalaːn</ta>
            <ta e="T23" id="Seg_1813" s="T22">köt-ön</ta>
            <ta e="T24" id="Seg_1814" s="T23">kel-i͡e</ta>
            <ta e="T25" id="Seg_1815" s="T24">di͡e-n</ta>
            <ta e="T26" id="Seg_1816" s="T25">öh-ü</ta>
            <ta e="T27" id="Seg_1817" s="T26">ist-i-bit</ta>
            <ta e="T28" id="Seg_1818" s="T27">barɨ</ta>
            <ta e="T29" id="Seg_1819" s="T28">kihi-ler</ta>
            <ta e="T30" id="Seg_1820" s="T29">küːt-el-ler</ta>
            <ta e="T31" id="Seg_1821" s="T30">e-bit</ta>
            <ta e="T32" id="Seg_1822" s="T31">dʼarapalaːn-ɨ</ta>
            <ta e="T33" id="Seg_1823" s="T32">haŋa</ta>
            <ta e="T34" id="Seg_1824" s="T33">kihi-ler</ta>
            <ta e="T35" id="Seg_1825" s="T34">kel-el-leri-n</ta>
            <ta e="T36" id="Seg_1826" s="T35">kör-öːrü</ta>
            <ta e="T37" id="Seg_1827" s="T36">kantan</ta>
            <ta e="T38" id="Seg_1828" s="T37">ire</ta>
            <ta e="T39" id="Seg_1829" s="T38">huruk</ta>
            <ta e="T40" id="Seg_1830" s="T39">ɨl-aːrɨ</ta>
            <ta e="T41" id="Seg_1831" s="T40">Tojoː</ta>
            <ta e="T42" id="Seg_1832" s="T41">kahan</ta>
            <ta e="T43" id="Seg_1833" s="T42">daː</ta>
            <ta e="T44" id="Seg_1834" s="T43">kör-ö</ta>
            <ta e="T45" id="Seg_1835" s="T44">ilik</ta>
            <ta e="T46" id="Seg_1836" s="T45">e-bit</ta>
            <ta e="T47" id="Seg_1837" s="T46">dʼarapalaːn-ɨ</ta>
            <ta e="T48" id="Seg_1838" s="T47">kajtak</ta>
            <ta e="T49" id="Seg_1839" s="T48">e-bit-e=j</ta>
            <ta e="T50" id="Seg_1840" s="T49">iti</ta>
            <ta e="T51" id="Seg_1841" s="T50">kötör-ö</ta>
            <ta e="T52" id="Seg_1842" s="T51">tu͡ok</ta>
            <ta e="T53" id="Seg_1843" s="T52">uraːŋkaj</ta>
            <ta e="T54" id="Seg_1844" s="T53">bil-er</ta>
            <ta e="T55" id="Seg_1845" s="T54">hol</ta>
            <ta e="T56" id="Seg_1846" s="T55">daː</ta>
            <ta e="T57" id="Seg_1847" s="T56">bu͡ol-lar</ta>
            <ta e="T58" id="Seg_1848" s="T57">hin</ta>
            <ta e="T59" id="Seg_1849" s="T58">küːt-er</ta>
            <ta e="T60" id="Seg_1850" s="T59">töhö</ta>
            <ta e="T61" id="Seg_1851" s="T60">daː</ta>
            <ta e="T62" id="Seg_1852" s="T61">ör</ta>
            <ta e="T63" id="Seg_1853" s="T62">bu͡ol-batak</ta>
            <ta e="T64" id="Seg_1854" s="T63">etiŋ</ta>
            <ta e="T65" id="Seg_1855" s="T64">et-er-i-n</ta>
            <ta e="T66" id="Seg_1856" s="T65">kördük</ta>
            <ta e="T67" id="Seg_1857" s="T66">kallaːŋ-ŋa</ta>
            <ta e="T68" id="Seg_1858" s="T67">aːjdaːn</ta>
            <ta e="T69" id="Seg_1859" s="T68">bu͡ol-but</ta>
            <ta e="T70" id="Seg_1860" s="T69">onton</ta>
            <ta e="T71" id="Seg_1861" s="T70">tu͡ok</ta>
            <ta e="T72" id="Seg_1862" s="T71">da</ta>
            <ta e="T73" id="Seg_1863" s="T72">kördük</ta>
            <ta e="T74" id="Seg_1864" s="T73">kötör</ta>
            <ta e="T75" id="Seg_1865" s="T74">köt-ö</ta>
            <ta e="T76" id="Seg_1866" s="T75">hɨldʼ-ɨ-bɨt</ta>
            <ta e="T77" id="Seg_1867" s="T76">bɨlɨt-tar</ta>
            <ta e="T78" id="Seg_1868" s="T77">ürdü-leri-ger</ta>
            <ta e="T79" id="Seg_1869" s="T78">Tojoː</ta>
            <ta e="T80" id="Seg_1870" s="T79">kuttam-mɨt</ta>
            <ta e="T81" id="Seg_1871" s="T80">če</ta>
            <ta e="T82" id="Seg_1872" s="T81">tur-but</ta>
            <ta e="T83" id="Seg_1873" s="T82">hir-i-ger</ta>
            <ta e="T84" id="Seg_1874" s="T83">lak</ta>
            <ta e="T85" id="Seg_1875" s="T84">gɨn-a</ta>
            <ta e="T86" id="Seg_1876" s="T85">olor-but</ta>
            <ta e="T87" id="Seg_1877" s="T86">karak-tar-ɨ-n</ta>
            <ta e="T88" id="Seg_1878" s="T87">hap-pɨt</ta>
            <ta e="T89" id="Seg_1879" s="T88">toŋ-mut</ta>
            <ta e="T90" id="Seg_1880" s="T89">kördük</ta>
            <ta e="T91" id="Seg_1881" s="T90">titir-iː</ta>
            <ta e="T92" id="Seg_1882" s="T91">olor-but</ta>
            <ta e="T93" id="Seg_1883" s="T92">tu͡ok</ta>
            <ta e="T94" id="Seg_1884" s="T93">bu͡ol-lu-ŋ</ta>
            <ta e="T95" id="Seg_1885" s="T94">hüːr</ta>
            <ta e="T96" id="Seg_1886" s="T95">türgen-nik</ta>
            <ta e="T97" id="Seg_1887" s="T96">kɨtɨl-ga</ta>
            <ta e="T98" id="Seg_1888" s="T97">kör-ö</ta>
            <ta e="T99" id="Seg_1889" s="T98">bar</ta>
            <ta e="T100" id="Seg_1890" s="T99">kajdak</ta>
            <ta e="T101" id="Seg_1891" s="T100">samalʼot</ta>
            <ta e="T102" id="Seg_1892" s="T101">olor-or-u-n</ta>
            <ta e="T103" id="Seg_1893" s="T102">uː-ga</ta>
            <ta e="T104" id="Seg_1894" s="T103">učitʼelʼ-i-n</ta>
            <ta e="T105" id="Seg_1895" s="T104">haŋa-tɨ-n</ta>
            <ta e="T106" id="Seg_1896" s="T105">ist-i-bit</ta>
            <ta e="T107" id="Seg_1897" s="T106">Tojoː</ta>
            <ta e="T108" id="Seg_1898" s="T107">karak-tar-ɨ-n</ta>
            <ta e="T109" id="Seg_1899" s="T108">arɨj-bɨt-a</ta>
            <ta e="T110" id="Seg_1900" s="T109">Valačaːnka</ta>
            <ta e="T111" id="Seg_1901" s="T110">barɨ</ta>
            <ta e="T112" id="Seg_1902" s="T111">nehili͡ege</ta>
            <ta e="T113" id="Seg_1903" s="T112">tur-ar</ta>
            <ta e="T114" id="Seg_1904" s="T113">tur-bat</ta>
            <ta e="T115" id="Seg_1905" s="T114">gini</ta>
            <ta e="T116" id="Seg_1906" s="T115">attɨ-nan</ta>
            <ta e="T117" id="Seg_1907" s="T116">hüːr-eleː-n</ta>
            <ta e="T118" id="Seg_1908" s="T117">aːh-an</ta>
            <ta e="T119" id="Seg_1909" s="T118">e-bit-ter</ta>
            <ta e="T120" id="Seg_1910" s="T119">kihi-ler</ta>
            <ta e="T121" id="Seg_1911" s="T120">kuttam-mat-tar</ta>
            <ta e="T122" id="Seg_1912" s="T121">min</ta>
            <ta e="T123" id="Seg_1913" s="T122">hin</ta>
            <ta e="T124" id="Seg_1914" s="T123">tu͡ok</ta>
            <ta e="T125" id="Seg_1915" s="T124">daː</ta>
            <ta e="T126" id="Seg_1916" s="T125">bu͡ol-u͡o-m</ta>
            <ta e="T127" id="Seg_1917" s="T126">hu͡og-a</ta>
            <ta e="T128" id="Seg_1918" s="T127">badaga</ta>
            <ta e="T129" id="Seg_1919" s="T128">d-iː</ta>
            <ta e="T130" id="Seg_1920" s="T129">hanaː-bɨt</ta>
            <ta e="T131" id="Seg_1921" s="T130">Tojoː</ta>
            <ta e="T132" id="Seg_1922" s="T131">tur-a</ta>
            <ta e="T133" id="Seg_1923" s="T132">ekkir-eːt</ta>
            <ta e="T134" id="Seg_1924" s="T133">ele</ta>
            <ta e="T135" id="Seg_1925" s="T134">küːh-ü-nen</ta>
            <ta e="T136" id="Seg_1926" s="T135">tep-pit</ta>
            <ta e="T137" id="Seg_1927" s="T136">hüːr-en</ta>
            <ta e="T138" id="Seg_1928" s="T137">ɨstaŋal-ɨː-ɨstaŋal-ɨː</ta>
            <ta e="T139" id="Seg_1929" s="T138">kɨra</ta>
            <ta e="T140" id="Seg_1930" s="T139">čalbak-tar</ta>
            <ta e="T141" id="Seg_1931" s="T140">ürdü-leri-nen</ta>
            <ta e="T142" id="Seg_1932" s="T141">ebe</ta>
            <ta e="T143" id="Seg_1933" s="T142">kɨtɨl-ɨ-gar</ta>
            <ta e="T144" id="Seg_1934" s="T143">kihi</ta>
            <ta e="T145" id="Seg_1935" s="T144">kihi-ti-n</ta>
            <ta e="T146" id="Seg_1936" s="T145">bil-s-i-bet</ta>
            <ta e="T147" id="Seg_1937" s="T146">kül-s-üː</ta>
            <ta e="T148" id="Seg_1938" s="T147">kepset-iː</ta>
            <ta e="T149" id="Seg_1939" s="T148">bu͡ol-an</ta>
            <ta e="T150" id="Seg_1940" s="T149">is-pit</ta>
            <ta e="T151" id="Seg_1941" s="T150">ebe</ta>
            <ta e="T152" id="Seg_1942" s="T151">uː-tu-gar</ta>
            <ta e="T153" id="Seg_1943" s="T152">čaːjka</ta>
            <ta e="T154" id="Seg_1944" s="T153">olor-or-u-n</ta>
            <ta e="T155" id="Seg_1945" s="T154">kördük</ta>
            <ta e="T156" id="Seg_1946" s="T155">dʼarapalaːn</ta>
            <ta e="T157" id="Seg_1947" s="T156">köb-ö</ta>
            <ta e="T158" id="Seg_1948" s="T157">hɨldʼ-ɨ-bɨt</ta>
            <ta e="T159" id="Seg_1949" s="T158">aːjdaːn-a</ta>
            <ta e="T160" id="Seg_1950" s="T159">bert</ta>
            <ta e="T161" id="Seg_1951" s="T160">kenni-ti-ger</ta>
            <ta e="T162" id="Seg_1952" s="T161">uː</ta>
            <ta e="T163" id="Seg_1953" s="T162">dolgun-n-u-r-ar</ta>
            <ta e="T164" id="Seg_1954" s="T163">küːsteːk</ta>
            <ta e="T165" id="Seg_1955" s="T164">tɨ͡al-ɨ-ttan</ta>
            <ta e="T166" id="Seg_1956" s="T165">Tojoː</ta>
            <ta e="T167" id="Seg_1957" s="T166">kuttan-a</ta>
            <ta e="T168" id="Seg_1958" s="T167">hataː-bɨt</ta>
            <ta e="T169" id="Seg_1959" s="T168">dʼarapalaːn</ta>
            <ta e="T170" id="Seg_1960" s="T169">uː-ttan</ta>
            <ta e="T171" id="Seg_1961" s="T170">taks-an</ta>
            <ta e="T172" id="Seg_1962" s="T171">kɨtɨl-ga</ta>
            <ta e="T173" id="Seg_1963" s="T172">taks-ɨ͡ag-a</ta>
            <ta e="T174" id="Seg_1964" s="T173">di͡e-n</ta>
            <ta e="T175" id="Seg_1965" s="T174">min</ta>
            <ta e="T176" id="Seg_1966" s="T175">di͡ek</ta>
            <ta e="T177" id="Seg_1967" s="T176">kel-i͡eg-e</ta>
            <ta e="T178" id="Seg_1968" s="T177">di͡e-n</ta>
            <ta e="T179" id="Seg_1969" s="T178">tur-ar</ta>
            <ta e="T180" id="Seg_1970" s="T179">kihi-ler</ta>
            <ta e="T181" id="Seg_1971" s="T180">attɨ-larɨ-ttan</ta>
            <ta e="T182" id="Seg_1972" s="T181">ɨraːk-kaːn</ta>
            <ta e="T183" id="Seg_1973" s="T182">tej-bit</ta>
            <ta e="T184" id="Seg_1974" s="T183">tuspa</ta>
            <ta e="T185" id="Seg_1975" s="T184">hogotogun</ta>
            <ta e="T186" id="Seg_1976" s="T185">tur-but</ta>
            <ta e="T187" id="Seg_1977" s="T186">dʼarapalaːn-a</ta>
            <ta e="T188" id="Seg_1978" s="T187">tɨː</ta>
            <ta e="T189" id="Seg_1979" s="T188">kördük</ta>
            <ta e="T190" id="Seg_1980" s="T189">munnu-tu-nan</ta>
            <ta e="T191" id="Seg_1981" s="T190">kɨtɨl-ga</ta>
            <ta e="T192" id="Seg_1982" s="T191">toktoː-but</ta>
            <ta e="T193" id="Seg_1983" s="T192">aːjdaːn</ta>
            <ta e="T194" id="Seg_1984" s="T193">büp-püt</ta>
            <ta e="T195" id="Seg_1985" s="T194">ürd-ü-tten</ta>
            <ta e="T196" id="Seg_1986" s="T195">aːn</ta>
            <ta e="T197" id="Seg_1987" s="T196">arɨ-ll-ɨ-bɨt</ta>
            <ta e="T198" id="Seg_1988" s="T197">kihi</ta>
            <ta e="T199" id="Seg_1989" s="T198">töbö-tö</ta>
            <ta e="T200" id="Seg_1990" s="T199">köst-ö</ta>
            <ta e="T201" id="Seg_1991" s="T200">bi͡er-bit</ta>
            <ta e="T202" id="Seg_1992" s="T201">kihi</ta>
            <ta e="T203" id="Seg_1993" s="T202">taks-a</ta>
            <ta e="T204" id="Seg_1994" s="T203">köt-öːt</ta>
            <ta e="T205" id="Seg_1995" s="T204">dʼarapalaːn</ta>
            <ta e="T206" id="Seg_1996" s="T205">kɨnat-ɨ-n</ta>
            <ta e="T207" id="Seg_1997" s="T206">ürd-ü-ger</ta>
            <ta e="T208" id="Seg_1998" s="T207">tur-an</ta>
            <ta e="T209" id="Seg_1999" s="T208">bɨrag-ɨ-ll-ɨ-bɨt</ta>
            <ta e="T210" id="Seg_2000" s="T209">bɨː</ta>
            <ta e="T211" id="Seg_2001" s="T210">uhug-u-n</ta>
            <ta e="T212" id="Seg_2002" s="T211">kab-an</ta>
            <ta e="T213" id="Seg_2003" s="T212">ɨl-bɨt</ta>
            <ta e="T214" id="Seg_2004" s="T213">samolʼot-ɨ-n</ta>
            <ta e="T215" id="Seg_2005" s="T214">taba-lɨː</ta>
            <ta e="T216" id="Seg_2006" s="T215">baːj-an</ta>
            <ta e="T217" id="Seg_2007" s="T216">keːs-pit</ta>
            <ta e="T218" id="Seg_2008" s="T217">dʼe</ta>
            <ta e="T219" id="Seg_2009" s="T218">intiri͡es</ta>
            <ta e="T220" id="Seg_2010" s="T219">tɨː</ta>
            <ta e="T221" id="Seg_2011" s="T220">kördük</ta>
            <ta e="T222" id="Seg_2012" s="T221">taba-lɨː</ta>
            <ta e="T223" id="Seg_2013" s="T222">da</ta>
            <ta e="T224" id="Seg_2014" s="T223">baːj-ɨ-ll-ar</ta>
            <ta e="T225" id="Seg_2015" s="T224">e-bit</ta>
            <ta e="T226" id="Seg_2016" s="T225">d-iː</ta>
            <ta e="T227" id="Seg_2017" s="T226">han-ɨː</ta>
            <ta e="T228" id="Seg_2018" s="T227">tur-but</ta>
            <ta e="T229" id="Seg_2019" s="T228">Tojoː</ta>
            <ta e="T230" id="Seg_2020" s="T229">ogo-lor</ta>
            <ta e="T231" id="Seg_2021" s="T230">bu͡ollaktarɨna</ta>
            <ta e="T232" id="Seg_2022" s="T231">üt-üːlü-h-e-üt-üːlü-h-e</ta>
            <ta e="T233" id="Seg_2023" s="T232">dʼarapalaːn</ta>
            <ta e="T234" id="Seg_2024" s="T233">di͡ek</ta>
            <ta e="T235" id="Seg_2025" s="T234">hür-eleː-bit-ter</ta>
            <ta e="T236" id="Seg_2026" s="T235">Tojoː</ta>
            <ta e="T237" id="Seg_2027" s="T236">hogotogun</ta>
            <ta e="T238" id="Seg_2028" s="T237">tur-an</ta>
            <ta e="T239" id="Seg_2029" s="T238">emi͡e</ta>
            <ta e="T240" id="Seg_2030" s="T239">dʼi͡e-ti-n</ta>
            <ta e="T241" id="Seg_2031" s="T240">inʼe-ti-n</ta>
            <ta e="T242" id="Seg_2032" s="T241">aga-tɨ-n</ta>
            <ta e="T243" id="Seg_2033" s="T242">öjdöː-büt</ta>
            <ta e="T244" id="Seg_2034" s="T243">ginner</ta>
            <ta e="T245" id="Seg_2035" s="T244">ire</ta>
            <ta e="T246" id="Seg_2036" s="T245">kör-büt</ta>
            <ta e="T247" id="Seg_2037" s="T246">kör-böt-tör</ta>
            <ta e="T248" id="Seg_2038" s="T247">dʼarapalaːn-ɨ</ta>
            <ta e="T249" id="Seg_2039" s="T248">kajtak</ta>
            <ta e="T250" id="Seg_2040" s="T249">olor-or-u-n</ta>
            <ta e="T251" id="Seg_2041" s="T250">kajtak</ta>
            <ta e="T252" id="Seg_2042" s="T251">köt-ör-ü-n</ta>
            <ta e="T253" id="Seg_2043" s="T252">össü͡ö</ta>
            <ta e="T254" id="Seg_2044" s="T253">olus</ta>
            <ta e="T255" id="Seg_2045" s="T254">dʼikti</ta>
            <ta e="T256" id="Seg_2046" s="T255">bu͡ol-u͡ok</ta>
            <ta e="T257" id="Seg_2047" s="T256">e-t-e</ta>
            <ta e="T258" id="Seg_2048" s="T257">dʼi͡e-ti-ger</ta>
            <ta e="T259" id="Seg_2049" s="T258">köt-ön</ta>
            <ta e="T260" id="Seg_2050" s="T259">kel-bit-e</ta>
            <ta e="T261" id="Seg_2051" s="T260">bu͡ol-lar</ta>
            <ta e="T262" id="Seg_2052" s="T261">iti</ta>
            <ta e="T263" id="Seg_2053" s="T262">dʼarapalaːn-a</ta>
            <ta e="T264" id="Seg_2054" s="T263">köt-ön</ta>
            <ta e="T265" id="Seg_2055" s="T264">ih-en</ta>
            <ta e="T266" id="Seg_2056" s="T265">barɨ</ta>
            <ta e="T267" id="Seg_2057" s="T266">mu͡ora-nɨ</ta>
            <ta e="T268" id="Seg_2058" s="T267">kör-ü͡ög-ü-n</ta>
            <ta e="T269" id="Seg_2059" s="T268">bagar-bɨt</ta>
            <ta e="T270" id="Seg_2060" s="T269">olor-but</ta>
            <ta e="T271" id="Seg_2061" s="T270">olog-u-n</ta>
            <ta e="T272" id="Seg_2062" s="T271">uraha</ta>
            <ta e="T273" id="Seg_2063" s="T272">dʼi͡e-ti-n</ta>
            <ta e="T274" id="Seg_2064" s="T273">oːnnʼoː-but</ta>
            <ta e="T275" id="Seg_2065" s="T274">hir-der-i-n</ta>
            <ta e="T276" id="Seg_2066" s="T275">dʼarapalaːn</ta>
            <ta e="T277" id="Seg_2067" s="T276">köt-ü͡ö-r</ta>
            <ta e="T278" id="Seg_2068" s="T277">di͡eri</ta>
            <ta e="T279" id="Seg_2069" s="T278">Tojoː</ta>
            <ta e="T280" id="Seg_2070" s="T279">dogot-tor-u-n</ta>
            <ta e="T281" id="Seg_2071" s="T280">gɨtta</ta>
            <ta e="T282" id="Seg_2072" s="T281">kɨtɨl-ga</ta>
            <ta e="T283" id="Seg_2073" s="T282">tur-but-tar</ta>
            <ta e="T284" id="Seg_2074" s="T283">ah-ɨː</ta>
            <ta e="T285" id="Seg_2075" s="T284">da</ta>
            <ta e="T286" id="Seg_2076" s="T285">bar-bakka</ta>
            <ta e="T287" id="Seg_2077" s="T286">intʼernaːt-tarɨ-gar</ta>
            <ta e="T288" id="Seg_2078" s="T287">dʼarapalaːn</ta>
            <ta e="T289" id="Seg_2079" s="T288">tuh-u-nan</ta>
            <ta e="T290" id="Seg_2080" s="T289">kepset-iː</ta>
            <ta e="T291" id="Seg_2081" s="T290">tahaːra</ta>
            <ta e="T292" id="Seg_2082" s="T291">barɨ</ta>
            <ta e="T293" id="Seg_2083" s="T292">dʼi͡e</ta>
            <ta e="T294" id="Seg_2084" s="T293">aːjɨ</ta>
            <ta e="T295" id="Seg_2085" s="T294">kahan</ta>
            <ta e="T296" id="Seg_2086" s="T295">da</ta>
            <ta e="T297" id="Seg_2087" s="T296">büp-pet</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2088" s="T0">anɨ</ta>
            <ta e="T2" id="Seg_2089" s="T1">ihilleː-ŋ</ta>
            <ta e="T3" id="Seg_2090" s="T2">nöŋü͡ö</ta>
            <ta e="T4" id="Seg_2091" s="T3">ös-nI</ta>
            <ta e="T5" id="Seg_2092" s="T4">aːt-tA</ta>
            <ta e="T6" id="Seg_2093" s="T5">dʼarapalaːn</ta>
            <ta e="T7" id="Seg_2094" s="T6">Tojoː</ta>
            <ta e="T8" id="Seg_2095" s="T7">üs-Is</ta>
            <ta e="T9" id="Seg_2096" s="T8">kün-tI-GAr</ta>
            <ta e="T10" id="Seg_2097" s="T9">emi͡e</ta>
            <ta e="T11" id="Seg_2098" s="T10">kör-BIT</ta>
            <ta e="T12" id="Seg_2099" s="T11">biːr</ta>
            <ta e="T13" id="Seg_2100" s="T12">dʼikti-nI</ta>
            <ta e="T14" id="Seg_2101" s="T13">künüs</ta>
            <ta e="T15" id="Seg_2102" s="T14">ahaː-An</ta>
            <ta e="T16" id="Seg_2103" s="T15">büt-AːT</ta>
            <ta e="T17" id="Seg_2104" s="T16">tahaːra</ta>
            <ta e="T18" id="Seg_2105" s="T17">hüːr-AlAː-A</ta>
            <ta e="T19" id="Seg_2106" s="T18">hɨrɨt-I-BIT</ta>
            <ta e="T20" id="Seg_2107" s="T19">ulica</ta>
            <ta e="T21" id="Seg_2108" s="T20">üstün</ta>
            <ta e="T22" id="Seg_2109" s="T21">dʼarapalaːn</ta>
            <ta e="T23" id="Seg_2110" s="T22">köt-An</ta>
            <ta e="T24" id="Seg_2111" s="T23">kel-IAK.[tA]</ta>
            <ta e="T25" id="Seg_2112" s="T24">di͡e-An</ta>
            <ta e="T26" id="Seg_2113" s="T25">ös-nI</ta>
            <ta e="T27" id="Seg_2114" s="T26">ihit-I-BIT</ta>
            <ta e="T28" id="Seg_2115" s="T27">barɨ</ta>
            <ta e="T29" id="Seg_2116" s="T28">kihi-LAr</ta>
            <ta e="T30" id="Seg_2117" s="T29">köhüt-Ar-LAr</ta>
            <ta e="T31" id="Seg_2118" s="T30">e-BIT</ta>
            <ta e="T32" id="Seg_2119" s="T31">dʼarapalaːn-nI</ta>
            <ta e="T33" id="Seg_2120" s="T32">haŋa</ta>
            <ta e="T34" id="Seg_2121" s="T33">kihi-LAr</ta>
            <ta e="T35" id="Seg_2122" s="T34">kel-Ar-LArI-n</ta>
            <ta e="T36" id="Seg_2123" s="T35">kör-AːrI</ta>
            <ta e="T37" id="Seg_2124" s="T36">kantan</ta>
            <ta e="T38" id="Seg_2125" s="T37">ere</ta>
            <ta e="T39" id="Seg_2126" s="T38">huruk</ta>
            <ta e="T40" id="Seg_2127" s="T39">ɨl-AːrI</ta>
            <ta e="T41" id="Seg_2128" s="T40">Tojoː</ta>
            <ta e="T42" id="Seg_2129" s="T41">kahan</ta>
            <ta e="T43" id="Seg_2130" s="T42">da</ta>
            <ta e="T44" id="Seg_2131" s="T43">kör-A</ta>
            <ta e="T45" id="Seg_2132" s="T44">ilik</ta>
            <ta e="T46" id="Seg_2133" s="T45">e-BIT</ta>
            <ta e="T47" id="Seg_2134" s="T46">dʼarapalaːn-nI</ta>
            <ta e="T48" id="Seg_2135" s="T47">kajdak</ta>
            <ta e="T49" id="Seg_2136" s="T48">e-BIT-tA=Ij</ta>
            <ta e="T50" id="Seg_2137" s="T49">iti</ta>
            <ta e="T51" id="Seg_2138" s="T50">kötör-tA</ta>
            <ta e="T52" id="Seg_2139" s="T51">tu͡ok</ta>
            <ta e="T53" id="Seg_2140" s="T52">uraːŋkaj</ta>
            <ta e="T54" id="Seg_2141" s="T53">bil-Ar</ta>
            <ta e="T55" id="Seg_2142" s="T54">hol</ta>
            <ta e="T56" id="Seg_2143" s="T55">da</ta>
            <ta e="T57" id="Seg_2144" s="T56">bu͡ol-TAR</ta>
            <ta e="T58" id="Seg_2145" s="T57">hin</ta>
            <ta e="T59" id="Seg_2146" s="T58">köhüt-Ar</ta>
            <ta e="T60" id="Seg_2147" s="T59">töhö</ta>
            <ta e="T61" id="Seg_2148" s="T60">da</ta>
            <ta e="T62" id="Seg_2149" s="T61">ör</ta>
            <ta e="T63" id="Seg_2150" s="T62">bu͡ol-BAtAK</ta>
            <ta e="T64" id="Seg_2151" s="T63">etiŋ</ta>
            <ta e="T65" id="Seg_2152" s="T64">et-Ar-tI-n</ta>
            <ta e="T66" id="Seg_2153" s="T65">kördük</ta>
            <ta e="T67" id="Seg_2154" s="T66">kallaːn-GA</ta>
            <ta e="T68" id="Seg_2155" s="T67">ajdaːn</ta>
            <ta e="T69" id="Seg_2156" s="T68">bu͡ol-BIT</ta>
            <ta e="T70" id="Seg_2157" s="T69">onton</ta>
            <ta e="T71" id="Seg_2158" s="T70">tu͡ok</ta>
            <ta e="T72" id="Seg_2159" s="T71">da</ta>
            <ta e="T73" id="Seg_2160" s="T72">kördük</ta>
            <ta e="T74" id="Seg_2161" s="T73">kötör</ta>
            <ta e="T75" id="Seg_2162" s="T74">köt-A</ta>
            <ta e="T76" id="Seg_2163" s="T75">hɨrɨt-I-BIT</ta>
            <ta e="T77" id="Seg_2164" s="T76">bɨlɨt-LAr</ta>
            <ta e="T78" id="Seg_2165" s="T77">ürüt-LArI-GAr</ta>
            <ta e="T79" id="Seg_2166" s="T78">Tojoː</ta>
            <ta e="T80" id="Seg_2167" s="T79">kuttan-BIT</ta>
            <ta e="T81" id="Seg_2168" s="T80">dʼe</ta>
            <ta e="T82" id="Seg_2169" s="T81">tur-BIT</ta>
            <ta e="T83" id="Seg_2170" s="T82">hir-tI-GAr</ta>
            <ta e="T84" id="Seg_2171" s="T83">laŋ</ta>
            <ta e="T85" id="Seg_2172" s="T84">gɨn-A</ta>
            <ta e="T86" id="Seg_2173" s="T85">olor-BIT</ta>
            <ta e="T87" id="Seg_2174" s="T86">karak-LAr-tI-n</ta>
            <ta e="T88" id="Seg_2175" s="T87">hap-BIT</ta>
            <ta e="T89" id="Seg_2176" s="T88">toŋ-BIT</ta>
            <ta e="T90" id="Seg_2177" s="T89">kördük</ta>
            <ta e="T91" id="Seg_2178" s="T90">titireː-A</ta>
            <ta e="T92" id="Seg_2179" s="T91">olor-BIT</ta>
            <ta e="T93" id="Seg_2180" s="T92">tu͡ok</ta>
            <ta e="T94" id="Seg_2181" s="T93">bu͡ol-TI-ŋ</ta>
            <ta e="T95" id="Seg_2182" s="T94">hüːr</ta>
            <ta e="T96" id="Seg_2183" s="T95">türgen-LIk</ta>
            <ta e="T97" id="Seg_2184" s="T96">kɨtɨl-GA</ta>
            <ta e="T98" id="Seg_2185" s="T97">kör-A</ta>
            <ta e="T99" id="Seg_2186" s="T98">bar</ta>
            <ta e="T100" id="Seg_2187" s="T99">kajdak</ta>
            <ta e="T101" id="Seg_2188" s="T100">samalʼot</ta>
            <ta e="T102" id="Seg_2189" s="T101">olor-Ar-tI-n</ta>
            <ta e="T103" id="Seg_2190" s="T102">uː-GA</ta>
            <ta e="T104" id="Seg_2191" s="T103">učuːtal-tI-n</ta>
            <ta e="T105" id="Seg_2192" s="T104">haŋa-tI-n</ta>
            <ta e="T106" id="Seg_2193" s="T105">ihit-I-BIT</ta>
            <ta e="T107" id="Seg_2194" s="T106">Tojoː</ta>
            <ta e="T108" id="Seg_2195" s="T107">karak-LAr-tI-n</ta>
            <ta e="T109" id="Seg_2196" s="T108">arɨj-BIT-tA</ta>
            <ta e="T110" id="Seg_2197" s="T109">Voločanka</ta>
            <ta e="T111" id="Seg_2198" s="T110">barɨ</ta>
            <ta e="T112" id="Seg_2199" s="T111">nehili͡ege</ta>
            <ta e="T113" id="Seg_2200" s="T112">tur-Ar</ta>
            <ta e="T114" id="Seg_2201" s="T113">tur-BAT</ta>
            <ta e="T115" id="Seg_2202" s="T114">gini</ta>
            <ta e="T116" id="Seg_2203" s="T115">attɨ-nAn</ta>
            <ta e="T117" id="Seg_2204" s="T116">hüːr-AlAː-An</ta>
            <ta e="T118" id="Seg_2205" s="T117">aːs-An</ta>
            <ta e="T119" id="Seg_2206" s="T118">e-BIT-LAr</ta>
            <ta e="T120" id="Seg_2207" s="T119">kihi-LAr</ta>
            <ta e="T121" id="Seg_2208" s="T120">kuttan-BAT-LAr</ta>
            <ta e="T122" id="Seg_2209" s="T121">min</ta>
            <ta e="T123" id="Seg_2210" s="T122">hin</ta>
            <ta e="T124" id="Seg_2211" s="T123">tu͡ok</ta>
            <ta e="T125" id="Seg_2212" s="T124">da</ta>
            <ta e="T126" id="Seg_2213" s="T125">bu͡ol-IAK-m</ta>
            <ta e="T127" id="Seg_2214" s="T126">hu͡ok-tA</ta>
            <ta e="T128" id="Seg_2215" s="T127">badaga</ta>
            <ta e="T129" id="Seg_2216" s="T128">di͡e-A</ta>
            <ta e="T130" id="Seg_2217" s="T129">hanaː-BIT</ta>
            <ta e="T131" id="Seg_2218" s="T130">Tojoː</ta>
            <ta e="T132" id="Seg_2219" s="T131">tur-A</ta>
            <ta e="T133" id="Seg_2220" s="T132">ekkireː-AːT</ta>
            <ta e="T134" id="Seg_2221" s="T133">ele</ta>
            <ta e="T135" id="Seg_2222" s="T134">küːs-tI-nAn</ta>
            <ta e="T136" id="Seg_2223" s="T135">tep-BIT</ta>
            <ta e="T137" id="Seg_2224" s="T136">hüːr-An</ta>
            <ta e="T138" id="Seg_2225" s="T137">ɨstaŋalaː-A-ɨstaŋalaː-A</ta>
            <ta e="T139" id="Seg_2226" s="T138">kɨra</ta>
            <ta e="T140" id="Seg_2227" s="T139">čalbak-LAr</ta>
            <ta e="T141" id="Seg_2228" s="T140">ürüt-LArI-nAn</ta>
            <ta e="T142" id="Seg_2229" s="T141">ebe</ta>
            <ta e="T143" id="Seg_2230" s="T142">kɨtɨl-tI-GAr</ta>
            <ta e="T144" id="Seg_2231" s="T143">kihi</ta>
            <ta e="T145" id="Seg_2232" s="T144">kihi-tI-n</ta>
            <ta e="T146" id="Seg_2233" s="T145">bil-s-I-BAT</ta>
            <ta e="T147" id="Seg_2234" s="T146">kül-s-Iː</ta>
            <ta e="T148" id="Seg_2235" s="T147">kepset-Iː</ta>
            <ta e="T149" id="Seg_2236" s="T148">bu͡ol-An</ta>
            <ta e="T150" id="Seg_2237" s="T149">is-BIT</ta>
            <ta e="T151" id="Seg_2238" s="T150">ebe</ta>
            <ta e="T152" id="Seg_2239" s="T151">uː-tI-GAr</ta>
            <ta e="T153" id="Seg_2240" s="T152">čajka</ta>
            <ta e="T154" id="Seg_2241" s="T153">olor-Ar-tI-n</ta>
            <ta e="T155" id="Seg_2242" s="T154">kördük</ta>
            <ta e="T156" id="Seg_2243" s="T155">dʼarapalaːn</ta>
            <ta e="T157" id="Seg_2244" s="T156">köp-A</ta>
            <ta e="T158" id="Seg_2245" s="T157">hɨrɨt-I-BIT</ta>
            <ta e="T159" id="Seg_2246" s="T158">ajdaːn-tA</ta>
            <ta e="T160" id="Seg_2247" s="T159">bert</ta>
            <ta e="T161" id="Seg_2248" s="T160">kelin-tI-GAr</ta>
            <ta e="T162" id="Seg_2249" s="T161">uː</ta>
            <ta e="T163" id="Seg_2250" s="T162">dolgun-LAː-I-r-Ar</ta>
            <ta e="T164" id="Seg_2251" s="T163">küːsteːk</ta>
            <ta e="T165" id="Seg_2252" s="T164">tɨ͡al-tI-ttAn</ta>
            <ta e="T166" id="Seg_2253" s="T165">Tojoː</ta>
            <ta e="T167" id="Seg_2254" s="T166">kuttan-A</ta>
            <ta e="T168" id="Seg_2255" s="T167">hataː-BIT</ta>
            <ta e="T169" id="Seg_2256" s="T168">dʼarapalaːn</ta>
            <ta e="T170" id="Seg_2257" s="T169">uː-ttAn</ta>
            <ta e="T171" id="Seg_2258" s="T170">tagɨs-An</ta>
            <ta e="T172" id="Seg_2259" s="T171">kɨtɨl-GA</ta>
            <ta e="T173" id="Seg_2260" s="T172">tagɨs-IAK-tA</ta>
            <ta e="T174" id="Seg_2261" s="T173">di͡e-An</ta>
            <ta e="T175" id="Seg_2262" s="T174">min</ta>
            <ta e="T176" id="Seg_2263" s="T175">dek</ta>
            <ta e="T177" id="Seg_2264" s="T176">kel-IAK-tA</ta>
            <ta e="T178" id="Seg_2265" s="T177">di͡e-An</ta>
            <ta e="T179" id="Seg_2266" s="T178">tur-Ar</ta>
            <ta e="T180" id="Seg_2267" s="T179">kihi-LAr</ta>
            <ta e="T181" id="Seg_2268" s="T180">attɨ-LArI-ttAn</ta>
            <ta e="T182" id="Seg_2269" s="T181">ɨraːk-kAːN</ta>
            <ta e="T183" id="Seg_2270" s="T182">tej-BIT</ta>
            <ta e="T184" id="Seg_2271" s="T183">tuspa</ta>
            <ta e="T185" id="Seg_2272" s="T184">hogotogun</ta>
            <ta e="T186" id="Seg_2273" s="T185">tur-BIT</ta>
            <ta e="T187" id="Seg_2274" s="T186">dʼarapalaːn-tA</ta>
            <ta e="T188" id="Seg_2275" s="T187">tɨː</ta>
            <ta e="T189" id="Seg_2276" s="T188">kördük</ta>
            <ta e="T190" id="Seg_2277" s="T189">munnu-tI-nAn</ta>
            <ta e="T191" id="Seg_2278" s="T190">kɨtɨl-GA</ta>
            <ta e="T192" id="Seg_2279" s="T191">toktoː-BIT</ta>
            <ta e="T193" id="Seg_2280" s="T192">ajdaːn</ta>
            <ta e="T194" id="Seg_2281" s="T193">büt-BIT</ta>
            <ta e="T195" id="Seg_2282" s="T194">ürüt-tI-ttAn</ta>
            <ta e="T196" id="Seg_2283" s="T195">aːn</ta>
            <ta e="T197" id="Seg_2284" s="T196">arɨj-LIN-I-BIT</ta>
            <ta e="T198" id="Seg_2285" s="T197">kihi</ta>
            <ta e="T199" id="Seg_2286" s="T198">töbö-tA</ta>
            <ta e="T200" id="Seg_2287" s="T199">köhün-A</ta>
            <ta e="T201" id="Seg_2288" s="T200">bi͡er-BIT</ta>
            <ta e="T202" id="Seg_2289" s="T201">kihi</ta>
            <ta e="T203" id="Seg_2290" s="T202">tagɨs-A</ta>
            <ta e="T204" id="Seg_2291" s="T203">köt-AːT</ta>
            <ta e="T205" id="Seg_2292" s="T204">dʼarapalaːn</ta>
            <ta e="T206" id="Seg_2293" s="T205">kɨnat-tI-n</ta>
            <ta e="T207" id="Seg_2294" s="T206">ürüt-tI-GAr</ta>
            <ta e="T208" id="Seg_2295" s="T207">tur-An</ta>
            <ta e="T209" id="Seg_2296" s="T208">bɨrak-I-LIN-I-BIT</ta>
            <ta e="T210" id="Seg_2297" s="T209">bɨ͡a</ta>
            <ta e="T211" id="Seg_2298" s="T210">uhuk-tI-n</ta>
            <ta e="T212" id="Seg_2299" s="T211">kap-An</ta>
            <ta e="T213" id="Seg_2300" s="T212">ɨl-BIT</ta>
            <ta e="T214" id="Seg_2301" s="T213">samalʼot-tI-n</ta>
            <ta e="T215" id="Seg_2302" s="T214">taba-LIː</ta>
            <ta e="T216" id="Seg_2303" s="T215">baːj-An</ta>
            <ta e="T217" id="Seg_2304" s="T216">keːs-BIT</ta>
            <ta e="T218" id="Seg_2305" s="T217">dʼe</ta>
            <ta e="T219" id="Seg_2306" s="T218">intiri͡es</ta>
            <ta e="T220" id="Seg_2307" s="T219">tɨː</ta>
            <ta e="T221" id="Seg_2308" s="T220">kördük</ta>
            <ta e="T222" id="Seg_2309" s="T221">taba-LIː</ta>
            <ta e="T223" id="Seg_2310" s="T222">da</ta>
            <ta e="T224" id="Seg_2311" s="T223">baːj-I-LIN-Ar</ta>
            <ta e="T225" id="Seg_2312" s="T224">e-BIT</ta>
            <ta e="T226" id="Seg_2313" s="T225">di͡e-A</ta>
            <ta e="T227" id="Seg_2314" s="T226">hanaː-A</ta>
            <ta e="T228" id="Seg_2315" s="T227">tur-BIT</ta>
            <ta e="T229" id="Seg_2316" s="T228">Tojoː</ta>
            <ta e="T230" id="Seg_2317" s="T229">ogo-LAr</ta>
            <ta e="T231" id="Seg_2318" s="T230">bu͡ollagɨna</ta>
            <ta e="T232" id="Seg_2319" s="T231">üt-IAlAː-s-A-üt-IAlAː-s-A</ta>
            <ta e="T233" id="Seg_2320" s="T232">dʼarapalaːn</ta>
            <ta e="T234" id="Seg_2321" s="T233">dek</ta>
            <ta e="T235" id="Seg_2322" s="T234">hüːr-AlAː-BIT-LAr</ta>
            <ta e="T236" id="Seg_2323" s="T235">Tojoː</ta>
            <ta e="T237" id="Seg_2324" s="T236">hogotogun</ta>
            <ta e="T238" id="Seg_2325" s="T237">tur-An</ta>
            <ta e="T239" id="Seg_2326" s="T238">emi͡e</ta>
            <ta e="T240" id="Seg_2327" s="T239">dʼi͡e-tI-n</ta>
            <ta e="T241" id="Seg_2328" s="T240">inʼe-tI-n</ta>
            <ta e="T242" id="Seg_2329" s="T241">aga-tI-n</ta>
            <ta e="T243" id="Seg_2330" s="T242">öjdöː-BIT</ta>
            <ta e="T244" id="Seg_2331" s="T243">giniler</ta>
            <ta e="T245" id="Seg_2332" s="T244">ere</ta>
            <ta e="T246" id="Seg_2333" s="T245">kör-BIT</ta>
            <ta e="T247" id="Seg_2334" s="T246">kör-BAT-LAr</ta>
            <ta e="T248" id="Seg_2335" s="T247">dʼarapalaːn-nI</ta>
            <ta e="T249" id="Seg_2336" s="T248">kajdak</ta>
            <ta e="T250" id="Seg_2337" s="T249">olor-Ar-tI-n</ta>
            <ta e="T251" id="Seg_2338" s="T250">kajdak</ta>
            <ta e="T252" id="Seg_2339" s="T251">köt-Ar-tI-n</ta>
            <ta e="T253" id="Seg_2340" s="T252">össü͡ö</ta>
            <ta e="T254" id="Seg_2341" s="T253">olus</ta>
            <ta e="T255" id="Seg_2342" s="T254">dʼikti</ta>
            <ta e="T256" id="Seg_2343" s="T255">bu͡ol-IAK</ta>
            <ta e="T257" id="Seg_2344" s="T256">e-TI-tA</ta>
            <ta e="T258" id="Seg_2345" s="T257">dʼi͡e-tI-GAr</ta>
            <ta e="T259" id="Seg_2346" s="T258">köt-An</ta>
            <ta e="T260" id="Seg_2347" s="T259">kel-BIT-tA</ta>
            <ta e="T261" id="Seg_2348" s="T260">bu͡ol-TAR</ta>
            <ta e="T262" id="Seg_2349" s="T261">iti</ta>
            <ta e="T263" id="Seg_2350" s="T262">dʼarapalaːn-tA</ta>
            <ta e="T264" id="Seg_2351" s="T263">köt-An</ta>
            <ta e="T265" id="Seg_2352" s="T264">is-An</ta>
            <ta e="T266" id="Seg_2353" s="T265">barɨ</ta>
            <ta e="T267" id="Seg_2354" s="T266">mu͡ora-nI</ta>
            <ta e="T268" id="Seg_2355" s="T267">kör-IAK-tI-n</ta>
            <ta e="T269" id="Seg_2356" s="T268">bagar-BIT</ta>
            <ta e="T270" id="Seg_2357" s="T269">olor-BIT</ta>
            <ta e="T271" id="Seg_2358" s="T270">olok-tI-n</ta>
            <ta e="T272" id="Seg_2359" s="T271">uraha</ta>
            <ta e="T273" id="Seg_2360" s="T272">dʼi͡e-tI-n</ta>
            <ta e="T274" id="Seg_2361" s="T273">oːnnʼoː-BIT</ta>
            <ta e="T275" id="Seg_2362" s="T274">hir-LAr-tI-n</ta>
            <ta e="T276" id="Seg_2363" s="T275">dʼarapalaːn</ta>
            <ta e="T277" id="Seg_2364" s="T276">köt-IAK.[tI]-r</ta>
            <ta e="T278" id="Seg_2365" s="T277">di͡eri</ta>
            <ta e="T279" id="Seg_2366" s="T278">Tojoː</ta>
            <ta e="T280" id="Seg_2367" s="T279">dogor-LAr-tI-n</ta>
            <ta e="T281" id="Seg_2368" s="T280">kɨtta</ta>
            <ta e="T282" id="Seg_2369" s="T281">kɨtɨl-GA</ta>
            <ta e="T283" id="Seg_2370" s="T282">tur-BIT-LAr</ta>
            <ta e="T284" id="Seg_2371" s="T283">ahaː-A</ta>
            <ta e="T285" id="Seg_2372" s="T284">da</ta>
            <ta e="T286" id="Seg_2373" s="T285">bar-BAkkA</ta>
            <ta e="T287" id="Seg_2374" s="T286">internat-LArI-GAr</ta>
            <ta e="T288" id="Seg_2375" s="T287">dʼarapalaːn</ta>
            <ta e="T289" id="Seg_2376" s="T288">tus-tI-nAn</ta>
            <ta e="T290" id="Seg_2377" s="T289">kepset-Iː</ta>
            <ta e="T291" id="Seg_2378" s="T290">tahaːra</ta>
            <ta e="T292" id="Seg_2379" s="T291">barɨ</ta>
            <ta e="T293" id="Seg_2380" s="T292">dʼi͡e</ta>
            <ta e="T294" id="Seg_2381" s="T293">aːjɨ</ta>
            <ta e="T295" id="Seg_2382" s="T294">kahan</ta>
            <ta e="T296" id="Seg_2383" s="T295">da</ta>
            <ta e="T297" id="Seg_2384" s="T296">büt-BAT</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2385" s="T0">now</ta>
            <ta e="T2" id="Seg_2386" s="T1">listen-IMP.2PL</ta>
            <ta e="T3" id="Seg_2387" s="T2">next</ta>
            <ta e="T4" id="Seg_2388" s="T3">story-ACC</ta>
            <ta e="T5" id="Seg_2389" s="T4">name-3SG.[NOM]</ta>
            <ta e="T6" id="Seg_2390" s="T5">airplane</ta>
            <ta e="T7" id="Seg_2391" s="T6">Toyoo.[NOM]</ta>
            <ta e="T8" id="Seg_2392" s="T7">three-ORD</ta>
            <ta e="T9" id="Seg_2393" s="T8">day-3SG-DAT/LOC</ta>
            <ta e="T10" id="Seg_2394" s="T9">again</ta>
            <ta e="T11" id="Seg_2395" s="T10">see-PST2.[3SG]</ta>
            <ta e="T12" id="Seg_2396" s="T11">one</ta>
            <ta e="T13" id="Seg_2397" s="T12">miracle-ACC</ta>
            <ta e="T14" id="Seg_2398" s="T13">by.day</ta>
            <ta e="T15" id="Seg_2399" s="T14">eat-CVB.SEQ</ta>
            <ta e="T16" id="Seg_2400" s="T15">stop-CVB.ANT</ta>
            <ta e="T17" id="Seg_2401" s="T16">outside</ta>
            <ta e="T18" id="Seg_2402" s="T17">run-FREQ-CVB.SIM</ta>
            <ta e="T19" id="Seg_2403" s="T18">go-EP-PST2.[3SG]</ta>
            <ta e="T20" id="Seg_2404" s="T19">street.[NOM]</ta>
            <ta e="T21" id="Seg_2405" s="T20">along</ta>
            <ta e="T22" id="Seg_2406" s="T21">airplane.[NOM]</ta>
            <ta e="T23" id="Seg_2407" s="T22">fly-CVB.SEQ</ta>
            <ta e="T24" id="Seg_2408" s="T23">come-FUT.[3SG]</ta>
            <ta e="T25" id="Seg_2409" s="T24">say-CVB.SEQ</ta>
            <ta e="T26" id="Seg_2410" s="T25">news-ACC</ta>
            <ta e="T27" id="Seg_2411" s="T26">hear-EP-PST2.[3SG]</ta>
            <ta e="T28" id="Seg_2412" s="T27">every</ta>
            <ta e="T29" id="Seg_2413" s="T28">human.being-PL.[NOM]</ta>
            <ta e="T30" id="Seg_2414" s="T29">wait-PRS-3PL</ta>
            <ta e="T31" id="Seg_2415" s="T30">be-PST2.[3SG]</ta>
            <ta e="T32" id="Seg_2416" s="T31">airplane-ACC</ta>
            <ta e="T33" id="Seg_2417" s="T32">new</ta>
            <ta e="T34" id="Seg_2418" s="T33">human.being-PL.[NOM]</ta>
            <ta e="T35" id="Seg_2419" s="T34">come-PTCP.PRS-3PL-ACC</ta>
            <ta e="T36" id="Seg_2420" s="T35">see-CVB.PURP</ta>
            <ta e="T37" id="Seg_2421" s="T36">where.from</ta>
            <ta e="T38" id="Seg_2422" s="T37">INDEF</ta>
            <ta e="T39" id="Seg_2423" s="T38">letter.[NOM]</ta>
            <ta e="T40" id="Seg_2424" s="T39">get-CVB.PURP</ta>
            <ta e="T41" id="Seg_2425" s="T40">Toyoo.[NOM]</ta>
            <ta e="T42" id="Seg_2426" s="T41">when</ta>
            <ta e="T43" id="Seg_2427" s="T42">NEG</ta>
            <ta e="T44" id="Seg_2428" s="T43">see-CVB.SIM</ta>
            <ta e="T45" id="Seg_2429" s="T44">not.yet.[3SG]</ta>
            <ta e="T46" id="Seg_2430" s="T45">be-PST2.[3SG]</ta>
            <ta e="T47" id="Seg_2431" s="T46">airplane-ACC</ta>
            <ta e="T48" id="Seg_2432" s="T47">how</ta>
            <ta e="T49" id="Seg_2433" s="T48">be-PST2-3SG=Q</ta>
            <ta e="T50" id="Seg_2434" s="T49">that</ta>
            <ta e="T51" id="Seg_2435" s="T50">bird-3SG.[NOM]</ta>
            <ta e="T52" id="Seg_2436" s="T51">what.[NOM]</ta>
            <ta e="T53" id="Seg_2437" s="T52">stranger.[NOM]</ta>
            <ta e="T54" id="Seg_2438" s="T53">know-PRS.[3SG]</ta>
            <ta e="T55" id="Seg_2439" s="T54">that.EMPH.[NOM]</ta>
            <ta e="T56" id="Seg_2440" s="T55">and</ta>
            <ta e="T57" id="Seg_2441" s="T56">be-COND.[3SG]</ta>
            <ta e="T58" id="Seg_2442" s="T57">however</ta>
            <ta e="T59" id="Seg_2443" s="T58">wait-PRS.[3SG]</ta>
            <ta e="T60" id="Seg_2444" s="T59">how.much</ta>
            <ta e="T61" id="Seg_2445" s="T60">INDEF</ta>
            <ta e="T62" id="Seg_2446" s="T61">long</ta>
            <ta e="T63" id="Seg_2447" s="T62">be-PST2.NEG.[3SG]</ta>
            <ta e="T64" id="Seg_2448" s="T63">thunder.[NOM]</ta>
            <ta e="T65" id="Seg_2449" s="T64">speak-PTCP.PRS-3SG-ACC</ta>
            <ta e="T66" id="Seg_2450" s="T65">similar</ta>
            <ta e="T67" id="Seg_2451" s="T66">sky-DAT/LOC</ta>
            <ta e="T68" id="Seg_2452" s="T67">noise.[NOM]</ta>
            <ta e="T69" id="Seg_2453" s="T68">become-PST2.[3SG]</ta>
            <ta e="T70" id="Seg_2454" s="T69">then</ta>
            <ta e="T71" id="Seg_2455" s="T70">what.[NOM]</ta>
            <ta e="T72" id="Seg_2456" s="T71">NEG</ta>
            <ta e="T73" id="Seg_2457" s="T72">similar</ta>
            <ta e="T74" id="Seg_2458" s="T73">bird.[NOM]</ta>
            <ta e="T75" id="Seg_2459" s="T74">fly-CVB.SIM</ta>
            <ta e="T76" id="Seg_2460" s="T75">go-EP-PST2.[3SG]</ta>
            <ta e="T77" id="Seg_2461" s="T76">cloud-PL.[NOM]</ta>
            <ta e="T78" id="Seg_2462" s="T77">upper.part-3PL-DAT/LOC</ta>
            <ta e="T79" id="Seg_2463" s="T78">Toyoo.[NOM]</ta>
            <ta e="T80" id="Seg_2464" s="T79">startle-PST2.[3SG]</ta>
            <ta e="T81" id="Seg_2465" s="T80">well</ta>
            <ta e="T82" id="Seg_2466" s="T81">stand-PTCP.PST</ta>
            <ta e="T83" id="Seg_2467" s="T82">place-3SG-DAT/LOC</ta>
            <ta e="T84" id="Seg_2468" s="T83">splash</ta>
            <ta e="T85" id="Seg_2469" s="T84">make-CVB.SIM</ta>
            <ta e="T86" id="Seg_2470" s="T85">sit.down-PST2.[3SG]</ta>
            <ta e="T87" id="Seg_2471" s="T86">eye-PL-3SG-ACC</ta>
            <ta e="T88" id="Seg_2472" s="T87">cover-PST2.[3SG]</ta>
            <ta e="T89" id="Seg_2473" s="T88">freeze-PTCP.PST.[NOM]</ta>
            <ta e="T90" id="Seg_2474" s="T89">similar</ta>
            <ta e="T91" id="Seg_2475" s="T90">shiver-CVB.SIM</ta>
            <ta e="T92" id="Seg_2476" s="T91">sit-PST2.[3SG]</ta>
            <ta e="T93" id="Seg_2477" s="T92">what.[NOM]</ta>
            <ta e="T94" id="Seg_2478" s="T93">be-PST1-2SG</ta>
            <ta e="T95" id="Seg_2479" s="T94">run.[IMP.2SG]</ta>
            <ta e="T96" id="Seg_2480" s="T95">fast-ADVZ</ta>
            <ta e="T97" id="Seg_2481" s="T96">shore-DAT/LOC</ta>
            <ta e="T98" id="Seg_2482" s="T97">see-CVB.SIM</ta>
            <ta e="T99" id="Seg_2483" s="T98">go.[IMP.2SG]</ta>
            <ta e="T100" id="Seg_2484" s="T99">how</ta>
            <ta e="T101" id="Seg_2485" s="T100">airplane.[NOM]</ta>
            <ta e="T102" id="Seg_2486" s="T101">land-PTCP.PRS-3SG-ACC</ta>
            <ta e="T103" id="Seg_2487" s="T102">water-DAT/LOC</ta>
            <ta e="T104" id="Seg_2488" s="T103">teacher-3SG-GEN</ta>
            <ta e="T105" id="Seg_2489" s="T104">voice-3SG-ACC</ta>
            <ta e="T106" id="Seg_2490" s="T105">hear-EP-PST2.[3SG]</ta>
            <ta e="T107" id="Seg_2491" s="T106">Toyoo.[NOM]</ta>
            <ta e="T108" id="Seg_2492" s="T107">eye-PL-3SG-ACC</ta>
            <ta e="T109" id="Seg_2493" s="T108">open-PST2-3SG</ta>
            <ta e="T110" id="Seg_2494" s="T109">Volochanka.[NOM]</ta>
            <ta e="T111" id="Seg_2495" s="T110">every</ta>
            <ta e="T112" id="Seg_2496" s="T111">population.[NOM]</ta>
            <ta e="T113" id="Seg_2497" s="T112">stand-PRS.[3SG]</ta>
            <ta e="T114" id="Seg_2498" s="T113">stand-NEG.[3SG]</ta>
            <ta e="T115" id="Seg_2499" s="T114">3SG.[NOM]</ta>
            <ta e="T116" id="Seg_2500" s="T115">place.beneath-INSTR</ta>
            <ta e="T117" id="Seg_2501" s="T116">run-FREQ-CVB.SEQ</ta>
            <ta e="T118" id="Seg_2502" s="T117">pass.by-CVB.SEQ</ta>
            <ta e="T119" id="Seg_2503" s="T118">be-PST2-3PL</ta>
            <ta e="T120" id="Seg_2504" s="T119">human.being-PL.[NOM]</ta>
            <ta e="T121" id="Seg_2505" s="T120">be.afraid-NEG-3PL</ta>
            <ta e="T122" id="Seg_2506" s="T121">1SG.[NOM]</ta>
            <ta e="T123" id="Seg_2507" s="T122">however</ta>
            <ta e="T124" id="Seg_2508" s="T123">what.[NOM]</ta>
            <ta e="T125" id="Seg_2509" s="T124">and</ta>
            <ta e="T126" id="Seg_2510" s="T125">be-FUT-1SG</ta>
            <ta e="T127" id="Seg_2511" s="T126">NEG-3SG</ta>
            <ta e="T128" id="Seg_2512" s="T127">probably</ta>
            <ta e="T129" id="Seg_2513" s="T128">think-CVB.SIM</ta>
            <ta e="T130" id="Seg_2514" s="T129">think-PST2.[3SG]</ta>
            <ta e="T131" id="Seg_2515" s="T130">Toyoo.[NOM]</ta>
            <ta e="T132" id="Seg_2516" s="T131">stand.up-CVB.SIM</ta>
            <ta e="T133" id="Seg_2517" s="T132">jump-CVB.ANT</ta>
            <ta e="T134" id="Seg_2518" s="T133">last</ta>
            <ta e="T135" id="Seg_2519" s="T134">power-3SG-INSTR</ta>
            <ta e="T136" id="Seg_2520" s="T135">push-PST2.[3SG]</ta>
            <ta e="T137" id="Seg_2521" s="T136">run-CVB.SEQ</ta>
            <ta e="T138" id="Seg_2522" s="T137">jump-CVB.SIM-jump-CVB.SIM</ta>
            <ta e="T139" id="Seg_2523" s="T138">small</ta>
            <ta e="T140" id="Seg_2524" s="T139">puddle-PL.[NOM]</ta>
            <ta e="T141" id="Seg_2525" s="T140">upper.part-3PL-INSTR</ta>
            <ta e="T142" id="Seg_2526" s="T141">river.[NOM]</ta>
            <ta e="T143" id="Seg_2527" s="T142">shore-3SG-DAT/LOC</ta>
            <ta e="T144" id="Seg_2528" s="T143">human.being.[NOM]</ta>
            <ta e="T145" id="Seg_2529" s="T144">human.being-3SG-ACC</ta>
            <ta e="T146" id="Seg_2530" s="T145">know-RECP/COLL-EP-NEG.[3SG]</ta>
            <ta e="T147" id="Seg_2531" s="T146">laugh-RECP/COLL-NMNZ.[NOM]</ta>
            <ta e="T148" id="Seg_2532" s="T147">chat-NMNZ.[NOM]</ta>
            <ta e="T149" id="Seg_2533" s="T148">become-CVB.SEQ</ta>
            <ta e="T150" id="Seg_2534" s="T149">go-PST2.[3SG]</ta>
            <ta e="T151" id="Seg_2535" s="T150">river.[NOM]</ta>
            <ta e="T152" id="Seg_2536" s="T151">water-3SG-DAT/LOC</ta>
            <ta e="T153" id="Seg_2537" s="T152">gull.[NOM]</ta>
            <ta e="T154" id="Seg_2538" s="T153">sit-PTCP.PRS-3SG-ACC</ta>
            <ta e="T155" id="Seg_2539" s="T154">similar</ta>
            <ta e="T156" id="Seg_2540" s="T155">airplane.[NOM]</ta>
            <ta e="T157" id="Seg_2541" s="T156">swim-CVB.SIM</ta>
            <ta e="T158" id="Seg_2542" s="T157">go-EP-PST2.[3SG]</ta>
            <ta e="T159" id="Seg_2543" s="T158">noise-3SG.[NOM]</ta>
            <ta e="T160" id="Seg_2544" s="T159">very</ta>
            <ta e="T161" id="Seg_2545" s="T160">back-3SG-DAT/LOC</ta>
            <ta e="T162" id="Seg_2546" s="T161">water.[NOM]</ta>
            <ta e="T163" id="Seg_2547" s="T162">wave-VBZ-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T164" id="Seg_2548" s="T163">strong</ta>
            <ta e="T165" id="Seg_2549" s="T164">wind-3SG-ABL</ta>
            <ta e="T166" id="Seg_2550" s="T165">Toyoo.[NOM]</ta>
            <ta e="T167" id="Seg_2551" s="T166">be.afraid-CVB.SIM</ta>
            <ta e="T168" id="Seg_2552" s="T167">can-PST2.[3SG]</ta>
            <ta e="T169" id="Seg_2553" s="T168">airplane.[NOM]</ta>
            <ta e="T170" id="Seg_2554" s="T169">water-ABL</ta>
            <ta e="T171" id="Seg_2555" s="T170">go.out-CVB.SEQ</ta>
            <ta e="T172" id="Seg_2556" s="T171">shore-DAT/LOC</ta>
            <ta e="T173" id="Seg_2557" s="T172">go.out-FUT-3SG</ta>
            <ta e="T174" id="Seg_2558" s="T173">think-CVB.SEQ</ta>
            <ta e="T175" id="Seg_2559" s="T174">1SG.[NOM]</ta>
            <ta e="T176" id="Seg_2560" s="T175">to</ta>
            <ta e="T177" id="Seg_2561" s="T176">come-FUT-3SG</ta>
            <ta e="T178" id="Seg_2562" s="T177">think-CVB.SEQ</ta>
            <ta e="T179" id="Seg_2563" s="T178">stand-PTCP.PRS</ta>
            <ta e="T180" id="Seg_2564" s="T179">human.being-PL.[NOM]</ta>
            <ta e="T181" id="Seg_2565" s="T180">place.beneath-3PL-ABL</ta>
            <ta e="T182" id="Seg_2566" s="T181">far.away-INTNS</ta>
            <ta e="T183" id="Seg_2567" s="T182">jump.away-PST2.[3SG]</ta>
            <ta e="T184" id="Seg_2568" s="T183">individually</ta>
            <ta e="T185" id="Seg_2569" s="T184">lonely</ta>
            <ta e="T186" id="Seg_2570" s="T185">stand-PST2.[3SG]</ta>
            <ta e="T187" id="Seg_2571" s="T186">airplane-3SG.[NOM]</ta>
            <ta e="T188" id="Seg_2572" s="T187">small.boat.[NOM]</ta>
            <ta e="T189" id="Seg_2573" s="T188">similar</ta>
            <ta e="T190" id="Seg_2574" s="T189">nose-3SG-INSTR</ta>
            <ta e="T191" id="Seg_2575" s="T190">shore-DAT/LOC</ta>
            <ta e="T192" id="Seg_2576" s="T191">stop-PST2.[3SG]</ta>
            <ta e="T193" id="Seg_2577" s="T192">noise.[NOM]</ta>
            <ta e="T194" id="Seg_2578" s="T193">stop-PST2.[3SG]</ta>
            <ta e="T195" id="Seg_2579" s="T194">upper.part-3SG-ABL</ta>
            <ta e="T196" id="Seg_2580" s="T195">door.[NOM]</ta>
            <ta e="T197" id="Seg_2581" s="T196">open-PASS/REFL-EP-PST2.[3SG]</ta>
            <ta e="T198" id="Seg_2582" s="T197">human.being.[NOM]</ta>
            <ta e="T199" id="Seg_2583" s="T198">head-3SG.[NOM]</ta>
            <ta e="T200" id="Seg_2584" s="T199">to.be.on.view-CVB.SIM</ta>
            <ta e="T201" id="Seg_2585" s="T200">give-PST2.[3SG]</ta>
            <ta e="T202" id="Seg_2586" s="T201">human.being.[NOM]</ta>
            <ta e="T203" id="Seg_2587" s="T202">go.out-CVB.SIM</ta>
            <ta e="T204" id="Seg_2588" s="T203">run-CVB.ANT</ta>
            <ta e="T205" id="Seg_2589" s="T204">airplane.[NOM]</ta>
            <ta e="T206" id="Seg_2590" s="T205">wing-3SG-GEN</ta>
            <ta e="T207" id="Seg_2591" s="T206">upper.part-3SG-DAT/LOC</ta>
            <ta e="T208" id="Seg_2592" s="T207">stand.up-CVB.SEQ</ta>
            <ta e="T209" id="Seg_2593" s="T208">throw-EP-PASS/REFL-EP-PTCP.PST</ta>
            <ta e="T210" id="Seg_2594" s="T209">string.[NOM]</ta>
            <ta e="T211" id="Seg_2595" s="T210">end-3SG-ACC</ta>
            <ta e="T212" id="Seg_2596" s="T211">catch-CVB.SEQ</ta>
            <ta e="T213" id="Seg_2597" s="T212">take-PST2.[3SG]</ta>
            <ta e="T214" id="Seg_2598" s="T213">airplane-3SG-ACC</ta>
            <ta e="T215" id="Seg_2599" s="T214">reindeer-SIM</ta>
            <ta e="T216" id="Seg_2600" s="T215">tie-CVB.SEQ</ta>
            <ta e="T217" id="Seg_2601" s="T216">throw-PST2.[3SG]</ta>
            <ta e="T218" id="Seg_2602" s="T217">well</ta>
            <ta e="T219" id="Seg_2603" s="T218">interesting.[NOM]</ta>
            <ta e="T220" id="Seg_2604" s="T219">small.boat.[NOM]</ta>
            <ta e="T221" id="Seg_2605" s="T220">similar</ta>
            <ta e="T222" id="Seg_2606" s="T221">reindeer-SIM</ta>
            <ta e="T223" id="Seg_2607" s="T222">and</ta>
            <ta e="T224" id="Seg_2608" s="T223">tie-EP-PASS/REFL-PRS.[3SG]</ta>
            <ta e="T225" id="Seg_2609" s="T224">be-PST2.[3SG]</ta>
            <ta e="T226" id="Seg_2610" s="T225">think-CVB.SIM</ta>
            <ta e="T227" id="Seg_2611" s="T226">think-CVB.SIM</ta>
            <ta e="T228" id="Seg_2612" s="T227">stand-PST2.[3SG]</ta>
            <ta e="T229" id="Seg_2613" s="T228">Toyoo.[NOM]</ta>
            <ta e="T230" id="Seg_2614" s="T229">child-PL.[NOM]</ta>
            <ta e="T231" id="Seg_2615" s="T230">though</ta>
            <ta e="T232" id="Seg_2616" s="T231">push-FREQ-RECP/COLL-CVB.SIM-push-FREQ-RECP/COLL-CVB.SIM</ta>
            <ta e="T233" id="Seg_2617" s="T232">airplane.[NOM]</ta>
            <ta e="T234" id="Seg_2618" s="T233">to</ta>
            <ta e="T235" id="Seg_2619" s="T234">run-FREQ-PST2-3PL</ta>
            <ta e="T236" id="Seg_2620" s="T235">Toyoo.[NOM]</ta>
            <ta e="T237" id="Seg_2621" s="T236">lonely</ta>
            <ta e="T238" id="Seg_2622" s="T237">stand-CVB.SEQ</ta>
            <ta e="T239" id="Seg_2623" s="T238">again</ta>
            <ta e="T240" id="Seg_2624" s="T239">house-3SG-ACC</ta>
            <ta e="T241" id="Seg_2625" s="T240">mother-3SG-ACC</ta>
            <ta e="T242" id="Seg_2626" s="T241">father-3SG-ACC</ta>
            <ta e="T243" id="Seg_2627" s="T242">remember-PST2.[3SG]</ta>
            <ta e="T244" id="Seg_2628" s="T243">3PL.[NOM]</ta>
            <ta e="T245" id="Seg_2629" s="T244">EMPH</ta>
            <ta e="T246" id="Seg_2630" s="T245">see-PST2</ta>
            <ta e="T247" id="Seg_2631" s="T246">see-NEG-3PL</ta>
            <ta e="T248" id="Seg_2632" s="T247">airplane-ACC</ta>
            <ta e="T249" id="Seg_2633" s="T248">how</ta>
            <ta e="T250" id="Seg_2634" s="T249">land-PTCP.PRS-3SG-ACC</ta>
            <ta e="T251" id="Seg_2635" s="T250">how</ta>
            <ta e="T252" id="Seg_2636" s="T251">fly-PTCP.PRS-3SG-ACC</ta>
            <ta e="T253" id="Seg_2637" s="T252">still</ta>
            <ta e="T254" id="Seg_2638" s="T253">very</ta>
            <ta e="T255" id="Seg_2639" s="T254">miracle.[NOM]</ta>
            <ta e="T256" id="Seg_2640" s="T255">be-PTCP.FUT</ta>
            <ta e="T257" id="Seg_2641" s="T256">be-PST1-3SG</ta>
            <ta e="T258" id="Seg_2642" s="T257">house-3SG-DAT/LOC</ta>
            <ta e="T259" id="Seg_2643" s="T258">fly-CVB.SEQ</ta>
            <ta e="T260" id="Seg_2644" s="T259">come-PST2-3SG</ta>
            <ta e="T261" id="Seg_2645" s="T260">be-COND.[3SG]</ta>
            <ta e="T262" id="Seg_2646" s="T261">that</ta>
            <ta e="T263" id="Seg_2647" s="T262">airplane-3SG.[NOM]</ta>
            <ta e="T264" id="Seg_2648" s="T263">fly-CVB.SEQ</ta>
            <ta e="T265" id="Seg_2649" s="T264">go-CVB.SEQ</ta>
            <ta e="T266" id="Seg_2650" s="T265">whole</ta>
            <ta e="T267" id="Seg_2651" s="T266">tundra-ACC</ta>
            <ta e="T268" id="Seg_2652" s="T267">see-PTCP.FUT-3SG-ACC</ta>
            <ta e="T269" id="Seg_2653" s="T268">want-PST2.[3SG]</ta>
            <ta e="T270" id="Seg_2654" s="T269">live-PTCP.PST</ta>
            <ta e="T271" id="Seg_2655" s="T270">settlement-3SG-ACC</ta>
            <ta e="T272" id="Seg_2656" s="T271">pole.[NOM]</ta>
            <ta e="T273" id="Seg_2657" s="T272">tent-3SG-ACC</ta>
            <ta e="T274" id="Seg_2658" s="T273">play-PTCP.PST</ta>
            <ta e="T275" id="Seg_2659" s="T274">place-PL-3SG-ACC</ta>
            <ta e="T276" id="Seg_2660" s="T275">airplane.[NOM]</ta>
            <ta e="T277" id="Seg_2661" s="T276">fly-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T278" id="Seg_2662" s="T277">until</ta>
            <ta e="T279" id="Seg_2663" s="T278">Toyoo.[NOM]</ta>
            <ta e="T280" id="Seg_2664" s="T279">friend-PL-3SG-ACC</ta>
            <ta e="T281" id="Seg_2665" s="T280">with</ta>
            <ta e="T282" id="Seg_2666" s="T281">shore-DAT/LOC</ta>
            <ta e="T283" id="Seg_2667" s="T282">stand-PST2-3PL</ta>
            <ta e="T284" id="Seg_2668" s="T283">eat-CVB.SIM</ta>
            <ta e="T285" id="Seg_2669" s="T284">NEG</ta>
            <ta e="T286" id="Seg_2670" s="T285">go-NEG.CVB.SIM</ta>
            <ta e="T287" id="Seg_2671" s="T286">boarding.school-3PL-DAT/LOC</ta>
            <ta e="T288" id="Seg_2672" s="T287">airplane.[NOM]</ta>
            <ta e="T289" id="Seg_2673" s="T288">side-3SG-INSTR</ta>
            <ta e="T290" id="Seg_2674" s="T289">chat-NMNZ.[NOM]</ta>
            <ta e="T291" id="Seg_2675" s="T290">outside</ta>
            <ta e="T292" id="Seg_2676" s="T291">every</ta>
            <ta e="T293" id="Seg_2677" s="T292">house.[NOM]</ta>
            <ta e="T294" id="Seg_2678" s="T293">every</ta>
            <ta e="T295" id="Seg_2679" s="T294">when</ta>
            <ta e="T296" id="Seg_2680" s="T295">NEG</ta>
            <ta e="T297" id="Seg_2681" s="T296">stop-NEG.[3SG]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_2682" s="T0">jetzt</ta>
            <ta e="T2" id="Seg_2683" s="T1">zuhören-IMP.2PL</ta>
            <ta e="T3" id="Seg_2684" s="T2">nächster</ta>
            <ta e="T4" id="Seg_2685" s="T3">Erzählung-ACC</ta>
            <ta e="T5" id="Seg_2686" s="T4">Name-3SG.[NOM]</ta>
            <ta e="T6" id="Seg_2687" s="T5">Flugzeug</ta>
            <ta e="T7" id="Seg_2688" s="T6">Tojoo.[NOM]</ta>
            <ta e="T8" id="Seg_2689" s="T7">drei-ORD</ta>
            <ta e="T9" id="Seg_2690" s="T8">Tag-3SG-DAT/LOC</ta>
            <ta e="T10" id="Seg_2691" s="T9">wieder</ta>
            <ta e="T11" id="Seg_2692" s="T10">sehen-PST2.[3SG]</ta>
            <ta e="T12" id="Seg_2693" s="T11">eins</ta>
            <ta e="T13" id="Seg_2694" s="T12">Wunder-ACC</ta>
            <ta e="T14" id="Seg_2695" s="T13">am.Tag</ta>
            <ta e="T15" id="Seg_2696" s="T14">essen-CVB.SEQ</ta>
            <ta e="T16" id="Seg_2697" s="T15">aufhören-CVB.ANT</ta>
            <ta e="T17" id="Seg_2698" s="T16">draußen</ta>
            <ta e="T18" id="Seg_2699" s="T17">laufen-FREQ-CVB.SIM</ta>
            <ta e="T19" id="Seg_2700" s="T18">gehen-EP-PST2.[3SG]</ta>
            <ta e="T20" id="Seg_2701" s="T19">Straße.[NOM]</ta>
            <ta e="T21" id="Seg_2702" s="T20">entlang</ta>
            <ta e="T22" id="Seg_2703" s="T21">Flugzeug.[NOM]</ta>
            <ta e="T23" id="Seg_2704" s="T22">fliegen-CVB.SEQ</ta>
            <ta e="T24" id="Seg_2705" s="T23">kommen-FUT.[3SG]</ta>
            <ta e="T25" id="Seg_2706" s="T24">sagen-CVB.SEQ</ta>
            <ta e="T26" id="Seg_2707" s="T25">Neuigkeit-ACC</ta>
            <ta e="T27" id="Seg_2708" s="T26">hören-EP-PST2.[3SG]</ta>
            <ta e="T28" id="Seg_2709" s="T27">jeder</ta>
            <ta e="T29" id="Seg_2710" s="T28">Mensch-PL.[NOM]</ta>
            <ta e="T30" id="Seg_2711" s="T29">warten-PRS-3PL</ta>
            <ta e="T31" id="Seg_2712" s="T30">sein-PST2.[3SG]</ta>
            <ta e="T32" id="Seg_2713" s="T31">Flugzeug-ACC</ta>
            <ta e="T33" id="Seg_2714" s="T32">neu</ta>
            <ta e="T34" id="Seg_2715" s="T33">Mensch-PL.[NOM]</ta>
            <ta e="T35" id="Seg_2716" s="T34">kommen-PTCP.PRS-3PL-ACC</ta>
            <ta e="T36" id="Seg_2717" s="T35">sehen-CVB.PURP</ta>
            <ta e="T37" id="Seg_2718" s="T36">woher</ta>
            <ta e="T38" id="Seg_2719" s="T37">INDEF</ta>
            <ta e="T39" id="Seg_2720" s="T38">Brief.[NOM]</ta>
            <ta e="T40" id="Seg_2721" s="T39">bekommen-CVB.PURP</ta>
            <ta e="T41" id="Seg_2722" s="T40">Tojoo.[NOM]</ta>
            <ta e="T42" id="Seg_2723" s="T41">wann</ta>
            <ta e="T43" id="Seg_2724" s="T42">NEG</ta>
            <ta e="T44" id="Seg_2725" s="T43">sehen-CVB.SIM</ta>
            <ta e="T45" id="Seg_2726" s="T44">noch.nicht.[3SG]</ta>
            <ta e="T46" id="Seg_2727" s="T45">sein-PST2.[3SG]</ta>
            <ta e="T47" id="Seg_2728" s="T46">Flugzeug-ACC</ta>
            <ta e="T48" id="Seg_2729" s="T47">wie</ta>
            <ta e="T49" id="Seg_2730" s="T48">sein-PST2-3SG=Q</ta>
            <ta e="T50" id="Seg_2731" s="T49">dieses</ta>
            <ta e="T51" id="Seg_2732" s="T50">Vogel-3SG.[NOM]</ta>
            <ta e="T52" id="Seg_2733" s="T51">was.[NOM]</ta>
            <ta e="T53" id="Seg_2734" s="T52">Fremder.[NOM]</ta>
            <ta e="T54" id="Seg_2735" s="T53">wissen-PRS.[3SG]</ta>
            <ta e="T55" id="Seg_2736" s="T54">jenes.EMPH.[NOM]</ta>
            <ta e="T56" id="Seg_2737" s="T55">und</ta>
            <ta e="T57" id="Seg_2738" s="T56">sein-COND.[3SG]</ta>
            <ta e="T58" id="Seg_2739" s="T57">doch</ta>
            <ta e="T59" id="Seg_2740" s="T58">warten-PRS.[3SG]</ta>
            <ta e="T60" id="Seg_2741" s="T59">wie.viel</ta>
            <ta e="T61" id="Seg_2742" s="T60">INDEF</ta>
            <ta e="T62" id="Seg_2743" s="T61">lange</ta>
            <ta e="T63" id="Seg_2744" s="T62">sein-PST2.NEG.[3SG]</ta>
            <ta e="T64" id="Seg_2745" s="T63">Donner.[NOM]</ta>
            <ta e="T65" id="Seg_2746" s="T64">sprechen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T66" id="Seg_2747" s="T65">ähnlich</ta>
            <ta e="T67" id="Seg_2748" s="T66">Himmel-DAT/LOC</ta>
            <ta e="T68" id="Seg_2749" s="T67">Lärm.[NOM]</ta>
            <ta e="T69" id="Seg_2750" s="T68">werden-PST2.[3SG]</ta>
            <ta e="T70" id="Seg_2751" s="T69">dann</ta>
            <ta e="T71" id="Seg_2752" s="T70">was.[NOM]</ta>
            <ta e="T72" id="Seg_2753" s="T71">NEG</ta>
            <ta e="T73" id="Seg_2754" s="T72">ähnlich</ta>
            <ta e="T74" id="Seg_2755" s="T73">Vogel.[NOM]</ta>
            <ta e="T75" id="Seg_2756" s="T74">fliegen-CVB.SIM</ta>
            <ta e="T76" id="Seg_2757" s="T75">gehen-EP-PST2.[3SG]</ta>
            <ta e="T77" id="Seg_2758" s="T76">Wolke-PL.[NOM]</ta>
            <ta e="T78" id="Seg_2759" s="T77">oberer.Teil-3PL-DAT/LOC</ta>
            <ta e="T79" id="Seg_2760" s="T78">Tojoo.[NOM]</ta>
            <ta e="T80" id="Seg_2761" s="T79">erschrecken-PST2.[3SG]</ta>
            <ta e="T81" id="Seg_2762" s="T80">doch</ta>
            <ta e="T82" id="Seg_2763" s="T81">stehen-PTCP.PST</ta>
            <ta e="T83" id="Seg_2764" s="T82">Ort-3SG-DAT/LOC</ta>
            <ta e="T84" id="Seg_2765" s="T83">platsch</ta>
            <ta e="T85" id="Seg_2766" s="T84">machen-CVB.SIM</ta>
            <ta e="T86" id="Seg_2767" s="T85">sich.setzen-PST2.[3SG]</ta>
            <ta e="T87" id="Seg_2768" s="T86">Auge-PL-3SG-ACC</ta>
            <ta e="T88" id="Seg_2769" s="T87">bedecken-PST2.[3SG]</ta>
            <ta e="T89" id="Seg_2770" s="T88">frieren-PTCP.PST.[NOM]</ta>
            <ta e="T90" id="Seg_2771" s="T89">ähnlich</ta>
            <ta e="T91" id="Seg_2772" s="T90">zittern-CVB.SIM</ta>
            <ta e="T92" id="Seg_2773" s="T91">sitzen-PST2.[3SG]</ta>
            <ta e="T93" id="Seg_2774" s="T92">was.[NOM]</ta>
            <ta e="T94" id="Seg_2775" s="T93">sein-PST1-2SG</ta>
            <ta e="T95" id="Seg_2776" s="T94">laufen.[IMP.2SG]</ta>
            <ta e="T96" id="Seg_2777" s="T95">schnell-ADVZ</ta>
            <ta e="T97" id="Seg_2778" s="T96">Ufer-DAT/LOC</ta>
            <ta e="T98" id="Seg_2779" s="T97">sehen-CVB.SIM</ta>
            <ta e="T99" id="Seg_2780" s="T98">gehen.[IMP.2SG]</ta>
            <ta e="T100" id="Seg_2781" s="T99">wie</ta>
            <ta e="T101" id="Seg_2782" s="T100">Flugzeug.[NOM]</ta>
            <ta e="T102" id="Seg_2783" s="T101">landen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T103" id="Seg_2784" s="T102">Wasser-DAT/LOC</ta>
            <ta e="T104" id="Seg_2785" s="T103">Lehrer-3SG-GEN</ta>
            <ta e="T105" id="Seg_2786" s="T104">Stimme-3SG-ACC</ta>
            <ta e="T106" id="Seg_2787" s="T105">hören-EP-PST2.[3SG]</ta>
            <ta e="T107" id="Seg_2788" s="T106">Tojoo.[NOM]</ta>
            <ta e="T108" id="Seg_2789" s="T107">Auge-PL-3SG-ACC</ta>
            <ta e="T109" id="Seg_2790" s="T108">öffnen-PST2-3SG</ta>
            <ta e="T110" id="Seg_2791" s="T109">Volochanka.[NOM]</ta>
            <ta e="T111" id="Seg_2792" s="T110">jeder</ta>
            <ta e="T112" id="Seg_2793" s="T111">Bevölkerung.[NOM]</ta>
            <ta e="T113" id="Seg_2794" s="T112">stehen-PRS.[3SG]</ta>
            <ta e="T114" id="Seg_2795" s="T113">stehen-NEG.[3SG]</ta>
            <ta e="T115" id="Seg_2796" s="T114">3SG.[NOM]</ta>
            <ta e="T116" id="Seg_2797" s="T115">Platz.neben-INSTR</ta>
            <ta e="T117" id="Seg_2798" s="T116">laufen-FREQ-CVB.SEQ</ta>
            <ta e="T118" id="Seg_2799" s="T117">vorbeigehen-CVB.SEQ</ta>
            <ta e="T119" id="Seg_2800" s="T118">sein-PST2-3PL</ta>
            <ta e="T120" id="Seg_2801" s="T119">Mensch-PL.[NOM]</ta>
            <ta e="T121" id="Seg_2802" s="T120">Angst.haben-NEG-3PL</ta>
            <ta e="T122" id="Seg_2803" s="T121">1SG.[NOM]</ta>
            <ta e="T123" id="Seg_2804" s="T122">doch</ta>
            <ta e="T124" id="Seg_2805" s="T123">was.[NOM]</ta>
            <ta e="T125" id="Seg_2806" s="T124">und</ta>
            <ta e="T126" id="Seg_2807" s="T125">sein-FUT-1SG</ta>
            <ta e="T127" id="Seg_2808" s="T126">NEG-3SG</ta>
            <ta e="T128" id="Seg_2809" s="T127">wohl</ta>
            <ta e="T129" id="Seg_2810" s="T128">denken-CVB.SIM</ta>
            <ta e="T130" id="Seg_2811" s="T129">denken-PST2.[3SG]</ta>
            <ta e="T131" id="Seg_2812" s="T130">Tojoo.[NOM]</ta>
            <ta e="T132" id="Seg_2813" s="T131">aufstehen-CVB.SIM</ta>
            <ta e="T133" id="Seg_2814" s="T132">springen-CVB.ANT</ta>
            <ta e="T134" id="Seg_2815" s="T133">letzter</ta>
            <ta e="T135" id="Seg_2816" s="T134">Kraft-3SG-INSTR</ta>
            <ta e="T136" id="Seg_2817" s="T135">stoßen-PST2.[3SG]</ta>
            <ta e="T137" id="Seg_2818" s="T136">laufen-CVB.SEQ</ta>
            <ta e="T138" id="Seg_2819" s="T137">springen-CVB.SIM-springen-CVB.SIM</ta>
            <ta e="T139" id="Seg_2820" s="T138">klein</ta>
            <ta e="T140" id="Seg_2821" s="T139">Pfütze-PL.[NOM]</ta>
            <ta e="T141" id="Seg_2822" s="T140">oberer.Teil-3PL-INSTR</ta>
            <ta e="T142" id="Seg_2823" s="T141">Fluss.[NOM]</ta>
            <ta e="T143" id="Seg_2824" s="T142">Ufer-3SG-DAT/LOC</ta>
            <ta e="T144" id="Seg_2825" s="T143">Mensch.[NOM]</ta>
            <ta e="T145" id="Seg_2826" s="T144">Mensch-3SG-ACC</ta>
            <ta e="T146" id="Seg_2827" s="T145">wissen-RECP/COLL-EP-NEG.[3SG]</ta>
            <ta e="T147" id="Seg_2828" s="T146">lachen-RECP/COLL-NMNZ.[NOM]</ta>
            <ta e="T148" id="Seg_2829" s="T147">sich.unterhalten-NMNZ.[NOM]</ta>
            <ta e="T149" id="Seg_2830" s="T148">werden-CVB.SEQ</ta>
            <ta e="T150" id="Seg_2831" s="T149">gehen-PST2.[3SG]</ta>
            <ta e="T151" id="Seg_2832" s="T150">Fluss.[NOM]</ta>
            <ta e="T152" id="Seg_2833" s="T151">Wasser-3SG-DAT/LOC</ta>
            <ta e="T153" id="Seg_2834" s="T152">Möwe.[NOM]</ta>
            <ta e="T154" id="Seg_2835" s="T153">sitzen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T155" id="Seg_2836" s="T154">ähnlich</ta>
            <ta e="T156" id="Seg_2837" s="T155">Flugzeug.[NOM]</ta>
            <ta e="T157" id="Seg_2838" s="T156">schwimmen-CVB.SIM</ta>
            <ta e="T158" id="Seg_2839" s="T157">gehen-EP-PST2.[3SG]</ta>
            <ta e="T159" id="Seg_2840" s="T158">Lärm-3SG.[NOM]</ta>
            <ta e="T160" id="Seg_2841" s="T159">sehr</ta>
            <ta e="T161" id="Seg_2842" s="T160">Hinterteil-3SG-DAT/LOC</ta>
            <ta e="T162" id="Seg_2843" s="T161">Wasser.[NOM]</ta>
            <ta e="T163" id="Seg_2844" s="T162">Welle-VBZ-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T164" id="Seg_2845" s="T163">stark</ta>
            <ta e="T165" id="Seg_2846" s="T164">Wind-3SG-ABL</ta>
            <ta e="T166" id="Seg_2847" s="T165">Tojoo.[NOM]</ta>
            <ta e="T167" id="Seg_2848" s="T166">Angst.haben-CVB.SIM</ta>
            <ta e="T168" id="Seg_2849" s="T167">können-PST2.[3SG]</ta>
            <ta e="T169" id="Seg_2850" s="T168">Flugzeug.[NOM]</ta>
            <ta e="T170" id="Seg_2851" s="T169">Wasser-ABL</ta>
            <ta e="T171" id="Seg_2852" s="T170">hinausgehen-CVB.SEQ</ta>
            <ta e="T172" id="Seg_2853" s="T171">Ufer-DAT/LOC</ta>
            <ta e="T173" id="Seg_2854" s="T172">hinausgehen-FUT-3SG</ta>
            <ta e="T174" id="Seg_2855" s="T173">denken-CVB.SEQ</ta>
            <ta e="T175" id="Seg_2856" s="T174">1SG.[NOM]</ta>
            <ta e="T176" id="Seg_2857" s="T175">zu</ta>
            <ta e="T177" id="Seg_2858" s="T176">kommen-FUT-3SG</ta>
            <ta e="T178" id="Seg_2859" s="T177">denken-CVB.SEQ</ta>
            <ta e="T179" id="Seg_2860" s="T178">stehen-PTCP.PRS</ta>
            <ta e="T180" id="Seg_2861" s="T179">Mensch-PL.[NOM]</ta>
            <ta e="T181" id="Seg_2862" s="T180">Platz.neben-3PL-ABL</ta>
            <ta e="T182" id="Seg_2863" s="T181">weit.weg-INTNS</ta>
            <ta e="T183" id="Seg_2864" s="T182">wegspringen-PST2.[3SG]</ta>
            <ta e="T184" id="Seg_2865" s="T183">einzeln</ta>
            <ta e="T185" id="Seg_2866" s="T184">einsam</ta>
            <ta e="T186" id="Seg_2867" s="T185">stehen-PST2.[3SG]</ta>
            <ta e="T187" id="Seg_2868" s="T186">Flugzeug-3SG.[NOM]</ta>
            <ta e="T188" id="Seg_2869" s="T187">kleines.Boot.[NOM]</ta>
            <ta e="T189" id="Seg_2870" s="T188">ähnlich</ta>
            <ta e="T190" id="Seg_2871" s="T189">Nase-3SG-INSTR</ta>
            <ta e="T191" id="Seg_2872" s="T190">Ufer-DAT/LOC</ta>
            <ta e="T192" id="Seg_2873" s="T191">halten-PST2.[3SG]</ta>
            <ta e="T193" id="Seg_2874" s="T192">Lärm.[NOM]</ta>
            <ta e="T194" id="Seg_2875" s="T193">aufhören-PST2.[3SG]</ta>
            <ta e="T195" id="Seg_2876" s="T194">oberer.Teil-3SG-ABL</ta>
            <ta e="T196" id="Seg_2877" s="T195">Tür.[NOM]</ta>
            <ta e="T197" id="Seg_2878" s="T196">öffnen-PASS/REFL-EP-PST2.[3SG]</ta>
            <ta e="T198" id="Seg_2879" s="T197">Mensch.[NOM]</ta>
            <ta e="T199" id="Seg_2880" s="T198">Kopf-3SG.[NOM]</ta>
            <ta e="T200" id="Seg_2881" s="T199">zu.sehen.sein-CVB.SIM</ta>
            <ta e="T201" id="Seg_2882" s="T200">geben-PST2.[3SG]</ta>
            <ta e="T202" id="Seg_2883" s="T201">Mensch.[NOM]</ta>
            <ta e="T203" id="Seg_2884" s="T202">hinausgehen-CVB.SIM</ta>
            <ta e="T204" id="Seg_2885" s="T203">rennen-CVB.ANT</ta>
            <ta e="T205" id="Seg_2886" s="T204">Flugzeug.[NOM]</ta>
            <ta e="T206" id="Seg_2887" s="T205">Flügel-3SG-GEN</ta>
            <ta e="T207" id="Seg_2888" s="T206">oberer.Teil-3SG-DAT/LOC</ta>
            <ta e="T208" id="Seg_2889" s="T207">aufstehen-CVB.SEQ</ta>
            <ta e="T209" id="Seg_2890" s="T208">werfen-EP-PASS/REFL-EP-PTCP.PST</ta>
            <ta e="T210" id="Seg_2891" s="T209">Schnur.[NOM]</ta>
            <ta e="T211" id="Seg_2892" s="T210">Ende-3SG-ACC</ta>
            <ta e="T212" id="Seg_2893" s="T211">fangen-CVB.SEQ</ta>
            <ta e="T213" id="Seg_2894" s="T212">nehmen-PST2.[3SG]</ta>
            <ta e="T214" id="Seg_2895" s="T213">Flugzeug-3SG-ACC</ta>
            <ta e="T215" id="Seg_2896" s="T214">Rentier-SIM</ta>
            <ta e="T216" id="Seg_2897" s="T215">binden-CVB.SEQ</ta>
            <ta e="T217" id="Seg_2898" s="T216">werfen-PST2.[3SG]</ta>
            <ta e="T218" id="Seg_2899" s="T217">doch</ta>
            <ta e="T219" id="Seg_2900" s="T218">interessant.[NOM]</ta>
            <ta e="T220" id="Seg_2901" s="T219">kleines.Boot.[NOM]</ta>
            <ta e="T221" id="Seg_2902" s="T220">ähnlich</ta>
            <ta e="T222" id="Seg_2903" s="T221">Rentier-SIM</ta>
            <ta e="T223" id="Seg_2904" s="T222">und</ta>
            <ta e="T224" id="Seg_2905" s="T223">binden-EP-PASS/REFL-PRS.[3SG]</ta>
            <ta e="T225" id="Seg_2906" s="T224">sein-PST2.[3SG]</ta>
            <ta e="T226" id="Seg_2907" s="T225">denken-CVB.SIM</ta>
            <ta e="T227" id="Seg_2908" s="T226">denken-CVB.SIM</ta>
            <ta e="T228" id="Seg_2909" s="T227">stehen-PST2.[3SG]</ta>
            <ta e="T229" id="Seg_2910" s="T228">Tojoo.[NOM]</ta>
            <ta e="T230" id="Seg_2911" s="T229">Kind-PL.[NOM]</ta>
            <ta e="T231" id="Seg_2912" s="T230">aber</ta>
            <ta e="T232" id="Seg_2913" s="T231">stoßen-FREQ-RECP/COLL-CVB.SIM-stoßen-FREQ-RECP/COLL-CVB.SIM</ta>
            <ta e="T233" id="Seg_2914" s="T232">Flugzeug.[NOM]</ta>
            <ta e="T234" id="Seg_2915" s="T233">zu</ta>
            <ta e="T235" id="Seg_2916" s="T234">laufen-FREQ-PST2-3PL</ta>
            <ta e="T236" id="Seg_2917" s="T235">Tojoo.[NOM]</ta>
            <ta e="T237" id="Seg_2918" s="T236">einsam</ta>
            <ta e="T238" id="Seg_2919" s="T237">stehen-CVB.SEQ</ta>
            <ta e="T239" id="Seg_2920" s="T238">wieder</ta>
            <ta e="T240" id="Seg_2921" s="T239">Haus-3SG-ACC</ta>
            <ta e="T241" id="Seg_2922" s="T240">Mutter-3SG-ACC</ta>
            <ta e="T242" id="Seg_2923" s="T241">Vater-3SG-ACC</ta>
            <ta e="T243" id="Seg_2924" s="T242">erinnern-PST2.[3SG]</ta>
            <ta e="T244" id="Seg_2925" s="T243">3PL.[NOM]</ta>
            <ta e="T245" id="Seg_2926" s="T244">EMPH</ta>
            <ta e="T246" id="Seg_2927" s="T245">sehen-PST2</ta>
            <ta e="T247" id="Seg_2928" s="T246">sehen-NEG-3PL</ta>
            <ta e="T248" id="Seg_2929" s="T247">Flugzeug-ACC</ta>
            <ta e="T249" id="Seg_2930" s="T248">wie</ta>
            <ta e="T250" id="Seg_2931" s="T249">landen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T251" id="Seg_2932" s="T250">wie</ta>
            <ta e="T252" id="Seg_2933" s="T251">fliegen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T253" id="Seg_2934" s="T252">noch</ta>
            <ta e="T254" id="Seg_2935" s="T253">sehr</ta>
            <ta e="T255" id="Seg_2936" s="T254">Wunder.[NOM]</ta>
            <ta e="T256" id="Seg_2937" s="T255">sein-PTCP.FUT</ta>
            <ta e="T257" id="Seg_2938" s="T256">sein-PST1-3SG</ta>
            <ta e="T258" id="Seg_2939" s="T257">Haus-3SG-DAT/LOC</ta>
            <ta e="T259" id="Seg_2940" s="T258">fliegen-CVB.SEQ</ta>
            <ta e="T260" id="Seg_2941" s="T259">kommen-PST2-3SG</ta>
            <ta e="T261" id="Seg_2942" s="T260">sein-COND.[3SG]</ta>
            <ta e="T262" id="Seg_2943" s="T261">dieses</ta>
            <ta e="T263" id="Seg_2944" s="T262">Flugzeug-3SG.[NOM]</ta>
            <ta e="T264" id="Seg_2945" s="T263">fliegen-CVB.SEQ</ta>
            <ta e="T265" id="Seg_2946" s="T264">gehen-CVB.SEQ</ta>
            <ta e="T266" id="Seg_2947" s="T265">ganz</ta>
            <ta e="T267" id="Seg_2948" s="T266">Tundra-ACC</ta>
            <ta e="T268" id="Seg_2949" s="T267">sehen-PTCP.FUT-3SG-ACC</ta>
            <ta e="T269" id="Seg_2950" s="T268">wollen-PST2.[3SG]</ta>
            <ta e="T270" id="Seg_2951" s="T269">leben-PTCP.PST</ta>
            <ta e="T271" id="Seg_2952" s="T270">Siedlung-3SG-ACC</ta>
            <ta e="T272" id="Seg_2953" s="T271">Stange.[NOM]</ta>
            <ta e="T273" id="Seg_2954" s="T272">Zelt-3SG-ACC</ta>
            <ta e="T274" id="Seg_2955" s="T273">spielen-PTCP.PST</ta>
            <ta e="T275" id="Seg_2956" s="T274">Ort-PL-3SG-ACC</ta>
            <ta e="T276" id="Seg_2957" s="T275">Flugzeug.[NOM]</ta>
            <ta e="T277" id="Seg_2958" s="T276">fliegen-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T278" id="Seg_2959" s="T277">bis.zu</ta>
            <ta e="T279" id="Seg_2960" s="T278">Tojoo.[NOM]</ta>
            <ta e="T280" id="Seg_2961" s="T279">Freund-PL-3SG-ACC</ta>
            <ta e="T281" id="Seg_2962" s="T280">mit</ta>
            <ta e="T282" id="Seg_2963" s="T281">Ufer-DAT/LOC</ta>
            <ta e="T283" id="Seg_2964" s="T282">stehen-PST2-3PL</ta>
            <ta e="T284" id="Seg_2965" s="T283">essen-CVB.SIM</ta>
            <ta e="T285" id="Seg_2966" s="T284">NEG</ta>
            <ta e="T286" id="Seg_2967" s="T285">gehen-NEG.CVB.SIM</ta>
            <ta e="T287" id="Seg_2968" s="T286">Internat-3PL-DAT/LOC</ta>
            <ta e="T288" id="Seg_2969" s="T287">Flugzeug.[NOM]</ta>
            <ta e="T289" id="Seg_2970" s="T288">Seite-3SG-INSTR</ta>
            <ta e="T290" id="Seg_2971" s="T289">sich.unterhalten-NMNZ.[NOM]</ta>
            <ta e="T291" id="Seg_2972" s="T290">draußen</ta>
            <ta e="T292" id="Seg_2973" s="T291">jeder</ta>
            <ta e="T293" id="Seg_2974" s="T292">Haus.[NOM]</ta>
            <ta e="T294" id="Seg_2975" s="T293">jeder</ta>
            <ta e="T295" id="Seg_2976" s="T294">wann</ta>
            <ta e="T296" id="Seg_2977" s="T295">NEG</ta>
            <ta e="T297" id="Seg_2978" s="T296">aufhören-NEG.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2979" s="T0">теперь</ta>
            <ta e="T2" id="Seg_2980" s="T1">слушать-IMP.2PL</ta>
            <ta e="T3" id="Seg_2981" s="T2">следующий</ta>
            <ta e="T4" id="Seg_2982" s="T3">рассказ-ACC</ta>
            <ta e="T5" id="Seg_2983" s="T4">имя-3SG.[NOM]</ta>
            <ta e="T6" id="Seg_2984" s="T5">самолет</ta>
            <ta e="T7" id="Seg_2985" s="T6">Тойоо.[NOM]</ta>
            <ta e="T8" id="Seg_2986" s="T7">три-ORD</ta>
            <ta e="T9" id="Seg_2987" s="T8">день-3SG-DAT/LOC</ta>
            <ta e="T10" id="Seg_2988" s="T9">опять</ta>
            <ta e="T11" id="Seg_2989" s="T10">видеть-PST2.[3SG]</ta>
            <ta e="T12" id="Seg_2990" s="T11">один</ta>
            <ta e="T13" id="Seg_2991" s="T12">чудо-ACC</ta>
            <ta e="T14" id="Seg_2992" s="T13">днем</ta>
            <ta e="T15" id="Seg_2993" s="T14">есть-CVB.SEQ</ta>
            <ta e="T16" id="Seg_2994" s="T15">кончать-CVB.ANT</ta>
            <ta e="T17" id="Seg_2995" s="T16">на.улице</ta>
            <ta e="T18" id="Seg_2996" s="T17">бегать-FREQ-CVB.SIM</ta>
            <ta e="T19" id="Seg_2997" s="T18">идти-EP-PST2.[3SG]</ta>
            <ta e="T20" id="Seg_2998" s="T19">улица.[NOM]</ta>
            <ta e="T21" id="Seg_2999" s="T20">вдоль</ta>
            <ta e="T22" id="Seg_3000" s="T21">самолет.[NOM]</ta>
            <ta e="T23" id="Seg_3001" s="T22">летать-CVB.SEQ</ta>
            <ta e="T24" id="Seg_3002" s="T23">приходить-FUT.[3SG]</ta>
            <ta e="T25" id="Seg_3003" s="T24">говорить-CVB.SEQ</ta>
            <ta e="T26" id="Seg_3004" s="T25">новость-ACC</ta>
            <ta e="T27" id="Seg_3005" s="T26">слышать-EP-PST2.[3SG]</ta>
            <ta e="T28" id="Seg_3006" s="T27">каждый</ta>
            <ta e="T29" id="Seg_3007" s="T28">человек-PL.[NOM]</ta>
            <ta e="T30" id="Seg_3008" s="T29">ждать-PRS-3PL</ta>
            <ta e="T31" id="Seg_3009" s="T30">быть-PST2.[3SG]</ta>
            <ta e="T32" id="Seg_3010" s="T31">самолет-ACC</ta>
            <ta e="T33" id="Seg_3011" s="T32">новый</ta>
            <ta e="T34" id="Seg_3012" s="T33">человек-PL.[NOM]</ta>
            <ta e="T35" id="Seg_3013" s="T34">приходить-PTCP.PRS-3PL-ACC</ta>
            <ta e="T36" id="Seg_3014" s="T35">видеть-CVB.PURP</ta>
            <ta e="T37" id="Seg_3015" s="T36">откуда</ta>
            <ta e="T38" id="Seg_3016" s="T37">INDEF</ta>
            <ta e="T39" id="Seg_3017" s="T38">письмо.[NOM]</ta>
            <ta e="T40" id="Seg_3018" s="T39">получить-CVB.PURP</ta>
            <ta e="T41" id="Seg_3019" s="T40">Тойоо.[NOM]</ta>
            <ta e="T42" id="Seg_3020" s="T41">когда</ta>
            <ta e="T43" id="Seg_3021" s="T42">NEG</ta>
            <ta e="T44" id="Seg_3022" s="T43">видеть-CVB.SIM</ta>
            <ta e="T45" id="Seg_3023" s="T44">еще.не.[3SG]</ta>
            <ta e="T46" id="Seg_3024" s="T45">быть-PST2.[3SG]</ta>
            <ta e="T47" id="Seg_3025" s="T46">самолет-ACC</ta>
            <ta e="T48" id="Seg_3026" s="T47">как</ta>
            <ta e="T49" id="Seg_3027" s="T48">быть-PST2-3SG=Q</ta>
            <ta e="T50" id="Seg_3028" s="T49">тот</ta>
            <ta e="T51" id="Seg_3029" s="T50">птица-3SG.[NOM]</ta>
            <ta e="T52" id="Seg_3030" s="T51">что.[NOM]</ta>
            <ta e="T53" id="Seg_3031" s="T52">чужеземец.[NOM]</ta>
            <ta e="T54" id="Seg_3032" s="T53">знать-PRS.[3SG]</ta>
            <ta e="T55" id="Seg_3033" s="T54">тот.EMPH.[NOM]</ta>
            <ta e="T56" id="Seg_3034" s="T55">да</ta>
            <ta e="T57" id="Seg_3035" s="T56">быть-COND.[3SG]</ta>
            <ta e="T58" id="Seg_3036" s="T57">ведь</ta>
            <ta e="T59" id="Seg_3037" s="T58">ждать-PRS.[3SG]</ta>
            <ta e="T60" id="Seg_3038" s="T59">сколько</ta>
            <ta e="T61" id="Seg_3039" s="T60">INDEF</ta>
            <ta e="T62" id="Seg_3040" s="T61">долго</ta>
            <ta e="T63" id="Seg_3041" s="T62">быть-PST2.NEG.[3SG]</ta>
            <ta e="T64" id="Seg_3042" s="T63">гром.[NOM]</ta>
            <ta e="T65" id="Seg_3043" s="T64">говорить-PTCP.PRS-3SG-ACC</ta>
            <ta e="T66" id="Seg_3044" s="T65">подобно</ta>
            <ta e="T67" id="Seg_3045" s="T66">небо-DAT/LOC</ta>
            <ta e="T68" id="Seg_3046" s="T67">шум.[NOM]</ta>
            <ta e="T69" id="Seg_3047" s="T68">становиться-PST2.[3SG]</ta>
            <ta e="T70" id="Seg_3048" s="T69">потом</ta>
            <ta e="T71" id="Seg_3049" s="T70">что.[NOM]</ta>
            <ta e="T72" id="Seg_3050" s="T71">NEG</ta>
            <ta e="T73" id="Seg_3051" s="T72">подобно</ta>
            <ta e="T74" id="Seg_3052" s="T73">птица.[NOM]</ta>
            <ta e="T75" id="Seg_3053" s="T74">летать-CVB.SIM</ta>
            <ta e="T76" id="Seg_3054" s="T75">идти-EP-PST2.[3SG]</ta>
            <ta e="T77" id="Seg_3055" s="T76">туча-PL.[NOM]</ta>
            <ta e="T78" id="Seg_3056" s="T77">верхняя.часть-3PL-DAT/LOC</ta>
            <ta e="T79" id="Seg_3057" s="T78">Тойоо.[NOM]</ta>
            <ta e="T80" id="Seg_3058" s="T79">испугаться-PST2.[3SG]</ta>
            <ta e="T81" id="Seg_3059" s="T80">вот</ta>
            <ta e="T82" id="Seg_3060" s="T81">стоять-PTCP.PST</ta>
            <ta e="T83" id="Seg_3061" s="T82">место-3SG-DAT/LOC</ta>
            <ta e="T84" id="Seg_3062" s="T83">всплеск</ta>
            <ta e="T85" id="Seg_3063" s="T84">делать-CVB.SIM</ta>
            <ta e="T86" id="Seg_3064" s="T85">сесть-PST2.[3SG]</ta>
            <ta e="T87" id="Seg_3065" s="T86">глаз-PL-3SG-ACC</ta>
            <ta e="T88" id="Seg_3066" s="T87">покрывать-PST2.[3SG]</ta>
            <ta e="T89" id="Seg_3067" s="T88">мерзнуть-PTCP.PST.[NOM]</ta>
            <ta e="T90" id="Seg_3068" s="T89">подобно</ta>
            <ta e="T91" id="Seg_3069" s="T90">дрожать-CVB.SIM</ta>
            <ta e="T92" id="Seg_3070" s="T91">сидеть-PST2.[3SG]</ta>
            <ta e="T93" id="Seg_3071" s="T92">что.[NOM]</ta>
            <ta e="T94" id="Seg_3072" s="T93">быть-PST1-2SG</ta>
            <ta e="T95" id="Seg_3073" s="T94">бегать.[IMP.2SG]</ta>
            <ta e="T96" id="Seg_3074" s="T95">быстрый-ADVZ</ta>
            <ta e="T97" id="Seg_3075" s="T96">берег-DAT/LOC</ta>
            <ta e="T98" id="Seg_3076" s="T97">видеть-CVB.SIM</ta>
            <ta e="T99" id="Seg_3077" s="T98">идти.[IMP.2SG]</ta>
            <ta e="T100" id="Seg_3078" s="T99">как</ta>
            <ta e="T101" id="Seg_3079" s="T100">самолет.[NOM]</ta>
            <ta e="T102" id="Seg_3080" s="T101">садиться-PTCP.PRS-3SG-ACC</ta>
            <ta e="T103" id="Seg_3081" s="T102">вода-DAT/LOC</ta>
            <ta e="T104" id="Seg_3082" s="T103">учитель-3SG-GEN</ta>
            <ta e="T105" id="Seg_3083" s="T104">голос-3SG-ACC</ta>
            <ta e="T106" id="Seg_3084" s="T105">слышать-EP-PST2.[3SG]</ta>
            <ta e="T107" id="Seg_3085" s="T106">Тойоо.[NOM]</ta>
            <ta e="T108" id="Seg_3086" s="T107">глаз-PL-3SG-ACC</ta>
            <ta e="T109" id="Seg_3087" s="T108">открывать-PST2-3SG</ta>
            <ta e="T110" id="Seg_3088" s="T109">Волочанка.[NOM]</ta>
            <ta e="T111" id="Seg_3089" s="T110">каждый</ta>
            <ta e="T112" id="Seg_3090" s="T111">население.[NOM]</ta>
            <ta e="T113" id="Seg_3091" s="T112">стоять-PRS.[3SG]</ta>
            <ta e="T114" id="Seg_3092" s="T113">стоять-NEG.[3SG]</ta>
            <ta e="T115" id="Seg_3093" s="T114">3SG.[NOM]</ta>
            <ta e="T116" id="Seg_3094" s="T115">место.около-INSTR</ta>
            <ta e="T117" id="Seg_3095" s="T116">бегать-FREQ-CVB.SEQ</ta>
            <ta e="T118" id="Seg_3096" s="T117">проехать-CVB.SEQ</ta>
            <ta e="T119" id="Seg_3097" s="T118">быть-PST2-3PL</ta>
            <ta e="T120" id="Seg_3098" s="T119">человек-PL.[NOM]</ta>
            <ta e="T121" id="Seg_3099" s="T120">бояться-NEG-3PL</ta>
            <ta e="T122" id="Seg_3100" s="T121">1SG.[NOM]</ta>
            <ta e="T123" id="Seg_3101" s="T122">ведь</ta>
            <ta e="T124" id="Seg_3102" s="T123">что.[NOM]</ta>
            <ta e="T125" id="Seg_3103" s="T124">да</ta>
            <ta e="T126" id="Seg_3104" s="T125">быть-FUT-1SG</ta>
            <ta e="T127" id="Seg_3105" s="T126">NEG-3SG</ta>
            <ta e="T128" id="Seg_3106" s="T127">видно</ta>
            <ta e="T129" id="Seg_3107" s="T128">думать-CVB.SIM</ta>
            <ta e="T130" id="Seg_3108" s="T129">думать-PST2.[3SG]</ta>
            <ta e="T131" id="Seg_3109" s="T130">Тойоо.[NOM]</ta>
            <ta e="T132" id="Seg_3110" s="T131">вставать-CVB.SIM</ta>
            <ta e="T133" id="Seg_3111" s="T132">прыгать-CVB.ANT</ta>
            <ta e="T134" id="Seg_3112" s="T133">последний</ta>
            <ta e="T135" id="Seg_3113" s="T134">сила-3SG-INSTR</ta>
            <ta e="T136" id="Seg_3114" s="T135">толкать-PST2.[3SG]</ta>
            <ta e="T137" id="Seg_3115" s="T136">бегать-CVB.SEQ</ta>
            <ta e="T138" id="Seg_3116" s="T137">прыгать-CVB.SIM-прыгать-CVB.SIM</ta>
            <ta e="T139" id="Seg_3117" s="T138">маленький</ta>
            <ta e="T140" id="Seg_3118" s="T139">лужа-PL.[NOM]</ta>
            <ta e="T141" id="Seg_3119" s="T140">верхняя.часть-3PL-INSTR</ta>
            <ta e="T142" id="Seg_3120" s="T141">река.[NOM]</ta>
            <ta e="T143" id="Seg_3121" s="T142">берег-3SG-DAT/LOC</ta>
            <ta e="T144" id="Seg_3122" s="T143">человек.[NOM]</ta>
            <ta e="T145" id="Seg_3123" s="T144">человек-3SG-ACC</ta>
            <ta e="T146" id="Seg_3124" s="T145">знать-RECP/COLL-EP-NEG.[3SG]</ta>
            <ta e="T147" id="Seg_3125" s="T146">смеяться-RECP/COLL-NMNZ.[NOM]</ta>
            <ta e="T148" id="Seg_3126" s="T147">разговаривать-NMNZ.[NOM]</ta>
            <ta e="T149" id="Seg_3127" s="T148">становиться-CVB.SEQ</ta>
            <ta e="T150" id="Seg_3128" s="T149">идти-PST2.[3SG]</ta>
            <ta e="T151" id="Seg_3129" s="T150">река.[NOM]</ta>
            <ta e="T152" id="Seg_3130" s="T151">вода-3SG-DAT/LOC</ta>
            <ta e="T153" id="Seg_3131" s="T152">чайка.[NOM]</ta>
            <ta e="T154" id="Seg_3132" s="T153">сидеть-PTCP.PRS-3SG-ACC</ta>
            <ta e="T155" id="Seg_3133" s="T154">подобно</ta>
            <ta e="T156" id="Seg_3134" s="T155">самолет.[NOM]</ta>
            <ta e="T157" id="Seg_3135" s="T156">плавать-CVB.SIM</ta>
            <ta e="T158" id="Seg_3136" s="T157">идти-EP-PST2.[3SG]</ta>
            <ta e="T159" id="Seg_3137" s="T158">шум-3SG.[NOM]</ta>
            <ta e="T160" id="Seg_3138" s="T159">очень</ta>
            <ta e="T161" id="Seg_3139" s="T160">задняя.часть-3SG-DAT/LOC</ta>
            <ta e="T162" id="Seg_3140" s="T161">вода.[NOM]</ta>
            <ta e="T163" id="Seg_3141" s="T162">волна-VBZ-EP-CAUS-PRS.[3SG]</ta>
            <ta e="T164" id="Seg_3142" s="T163">сильный</ta>
            <ta e="T165" id="Seg_3143" s="T164">ветер-3SG-ABL</ta>
            <ta e="T166" id="Seg_3144" s="T165">Тойоо.[NOM]</ta>
            <ta e="T167" id="Seg_3145" s="T166">бояться-CVB.SIM</ta>
            <ta e="T168" id="Seg_3146" s="T167">мочь-PST2.[3SG]</ta>
            <ta e="T169" id="Seg_3147" s="T168">самолет.[NOM]</ta>
            <ta e="T170" id="Seg_3148" s="T169">вода-ABL</ta>
            <ta e="T171" id="Seg_3149" s="T170">выйти-CVB.SEQ</ta>
            <ta e="T172" id="Seg_3150" s="T171">берег-DAT/LOC</ta>
            <ta e="T173" id="Seg_3151" s="T172">выйти-FUT-3SG</ta>
            <ta e="T174" id="Seg_3152" s="T173">думать-CVB.SEQ</ta>
            <ta e="T175" id="Seg_3153" s="T174">1SG.[NOM]</ta>
            <ta e="T176" id="Seg_3154" s="T175">к</ta>
            <ta e="T177" id="Seg_3155" s="T176">приходить-FUT-3SG</ta>
            <ta e="T178" id="Seg_3156" s="T177">думать-CVB.SEQ</ta>
            <ta e="T179" id="Seg_3157" s="T178">стоять-PTCP.PRS</ta>
            <ta e="T180" id="Seg_3158" s="T179">человек-PL.[NOM]</ta>
            <ta e="T181" id="Seg_3159" s="T180">место.около-3PL-ABL</ta>
            <ta e="T182" id="Seg_3160" s="T181">далеко-INTNS</ta>
            <ta e="T183" id="Seg_3161" s="T182">отскакивать-PST2.[3SG]</ta>
            <ta e="T184" id="Seg_3162" s="T183">отдельно</ta>
            <ta e="T185" id="Seg_3163" s="T184">одиноко</ta>
            <ta e="T186" id="Seg_3164" s="T185">стоять-PST2.[3SG]</ta>
            <ta e="T187" id="Seg_3165" s="T186">самолет-3SG.[NOM]</ta>
            <ta e="T188" id="Seg_3166" s="T187">лодочка.[NOM]</ta>
            <ta e="T189" id="Seg_3167" s="T188">подобно</ta>
            <ta e="T190" id="Seg_3168" s="T189">нос-3SG-INSTR</ta>
            <ta e="T191" id="Seg_3169" s="T190">берег-DAT/LOC</ta>
            <ta e="T192" id="Seg_3170" s="T191">останавливаться-PST2.[3SG]</ta>
            <ta e="T193" id="Seg_3171" s="T192">шум.[NOM]</ta>
            <ta e="T194" id="Seg_3172" s="T193">кончать-PST2.[3SG]</ta>
            <ta e="T195" id="Seg_3173" s="T194">верхняя.часть-3SG-ABL</ta>
            <ta e="T196" id="Seg_3174" s="T195">дверь.[NOM]</ta>
            <ta e="T197" id="Seg_3175" s="T196">открывать-PASS/REFL-EP-PST2.[3SG]</ta>
            <ta e="T198" id="Seg_3176" s="T197">человек.[NOM]</ta>
            <ta e="T199" id="Seg_3177" s="T198">голова-3SG.[NOM]</ta>
            <ta e="T200" id="Seg_3178" s="T199">быть.видно-CVB.SIM</ta>
            <ta e="T201" id="Seg_3179" s="T200">давать-PST2.[3SG]</ta>
            <ta e="T202" id="Seg_3180" s="T201">человек.[NOM]</ta>
            <ta e="T203" id="Seg_3181" s="T202">выйти-CVB.SIM</ta>
            <ta e="T204" id="Seg_3182" s="T203">бежать-CVB.ANT</ta>
            <ta e="T205" id="Seg_3183" s="T204">самолет.[NOM]</ta>
            <ta e="T206" id="Seg_3184" s="T205">крыло-3SG-GEN</ta>
            <ta e="T207" id="Seg_3185" s="T206">верхняя.часть-3SG-DAT/LOC</ta>
            <ta e="T208" id="Seg_3186" s="T207">вставать-CVB.SEQ</ta>
            <ta e="T209" id="Seg_3187" s="T208">бросать-EP-PASS/REFL-EP-PTCP.PST</ta>
            <ta e="T210" id="Seg_3188" s="T209">веревка.[NOM]</ta>
            <ta e="T211" id="Seg_3189" s="T210">конец-3SG-ACC</ta>
            <ta e="T212" id="Seg_3190" s="T211">поймать-CVB.SEQ</ta>
            <ta e="T213" id="Seg_3191" s="T212">взять-PST2.[3SG]</ta>
            <ta e="T214" id="Seg_3192" s="T213">самолет-3SG-ACC</ta>
            <ta e="T215" id="Seg_3193" s="T214">олень-SIM</ta>
            <ta e="T216" id="Seg_3194" s="T215">связывать-CVB.SEQ</ta>
            <ta e="T217" id="Seg_3195" s="T216">бросать-PST2.[3SG]</ta>
            <ta e="T218" id="Seg_3196" s="T217">вот</ta>
            <ta e="T219" id="Seg_3197" s="T218">интересный.[NOM]</ta>
            <ta e="T220" id="Seg_3198" s="T219">лодочка.[NOM]</ta>
            <ta e="T221" id="Seg_3199" s="T220">подобно</ta>
            <ta e="T222" id="Seg_3200" s="T221">олень-SIM</ta>
            <ta e="T223" id="Seg_3201" s="T222">да</ta>
            <ta e="T224" id="Seg_3202" s="T223">связывать-EP-PASS/REFL-PRS.[3SG]</ta>
            <ta e="T225" id="Seg_3203" s="T224">быть-PST2.[3SG]</ta>
            <ta e="T226" id="Seg_3204" s="T225">думать-CVB.SIM</ta>
            <ta e="T227" id="Seg_3205" s="T226">думать-CVB.SIM</ta>
            <ta e="T228" id="Seg_3206" s="T227">стоять-PST2.[3SG]</ta>
            <ta e="T229" id="Seg_3207" s="T228">Тойоо.[NOM]</ta>
            <ta e="T230" id="Seg_3208" s="T229">ребенок-PL.[NOM]</ta>
            <ta e="T231" id="Seg_3209" s="T230">однако</ta>
            <ta e="T232" id="Seg_3210" s="T231">толкать-FREQ-RECP/COLL-CVB.SIM-толкать-FREQ-RECP/COLL-CVB.SIM</ta>
            <ta e="T233" id="Seg_3211" s="T232">самолет.[NOM]</ta>
            <ta e="T234" id="Seg_3212" s="T233">к</ta>
            <ta e="T235" id="Seg_3213" s="T234">бегать-FREQ-PST2-3PL</ta>
            <ta e="T236" id="Seg_3214" s="T235">Тойоо.[NOM]</ta>
            <ta e="T237" id="Seg_3215" s="T236">одиноко</ta>
            <ta e="T238" id="Seg_3216" s="T237">стоять-CVB.SEQ</ta>
            <ta e="T239" id="Seg_3217" s="T238">опять</ta>
            <ta e="T240" id="Seg_3218" s="T239">дом-3SG-ACC</ta>
            <ta e="T241" id="Seg_3219" s="T240">мать-3SG-ACC</ta>
            <ta e="T242" id="Seg_3220" s="T241">отец-3SG-ACC</ta>
            <ta e="T243" id="Seg_3221" s="T242">помнить-PST2.[3SG]</ta>
            <ta e="T244" id="Seg_3222" s="T243">3PL.[NOM]</ta>
            <ta e="T245" id="Seg_3223" s="T244">EMPH</ta>
            <ta e="T246" id="Seg_3224" s="T245">видеть-PST2</ta>
            <ta e="T247" id="Seg_3225" s="T246">видеть-NEG-3PL</ta>
            <ta e="T248" id="Seg_3226" s="T247">самолет-ACC</ta>
            <ta e="T249" id="Seg_3227" s="T248">как</ta>
            <ta e="T250" id="Seg_3228" s="T249">садиться-PTCP.PRS-3SG-ACC</ta>
            <ta e="T251" id="Seg_3229" s="T250">как</ta>
            <ta e="T252" id="Seg_3230" s="T251">летать-PTCP.PRS-3SG-ACC</ta>
            <ta e="T253" id="Seg_3231" s="T252">еще</ta>
            <ta e="T254" id="Seg_3232" s="T253">очень</ta>
            <ta e="T255" id="Seg_3233" s="T254">чудо.[NOM]</ta>
            <ta e="T256" id="Seg_3234" s="T255">быть-PTCP.FUT</ta>
            <ta e="T257" id="Seg_3235" s="T256">быть-PST1-3SG</ta>
            <ta e="T258" id="Seg_3236" s="T257">дом-3SG-DAT/LOC</ta>
            <ta e="T259" id="Seg_3237" s="T258">летать-CVB.SEQ</ta>
            <ta e="T260" id="Seg_3238" s="T259">приходить-PST2-3SG</ta>
            <ta e="T261" id="Seg_3239" s="T260">быть-COND.[3SG]</ta>
            <ta e="T262" id="Seg_3240" s="T261">тот</ta>
            <ta e="T263" id="Seg_3241" s="T262">самолет-3SG.[NOM]</ta>
            <ta e="T264" id="Seg_3242" s="T263">летать-CVB.SEQ</ta>
            <ta e="T265" id="Seg_3243" s="T264">идти-CVB.SEQ</ta>
            <ta e="T266" id="Seg_3244" s="T265">целый</ta>
            <ta e="T267" id="Seg_3245" s="T266">тундра-ACC</ta>
            <ta e="T268" id="Seg_3246" s="T267">видеть-PTCP.FUT-3SG-ACC</ta>
            <ta e="T269" id="Seg_3247" s="T268">хотеть-PST2.[3SG]</ta>
            <ta e="T270" id="Seg_3248" s="T269">жить-PTCP.PST</ta>
            <ta e="T271" id="Seg_3249" s="T270">стойбище-3SG-ACC</ta>
            <ta e="T272" id="Seg_3250" s="T271">шест.[NOM]</ta>
            <ta e="T273" id="Seg_3251" s="T272">чум-3SG-ACC</ta>
            <ta e="T274" id="Seg_3252" s="T273">играть-PTCP.PST</ta>
            <ta e="T275" id="Seg_3253" s="T274">место-PL-3SG-ACC</ta>
            <ta e="T276" id="Seg_3254" s="T275">самолет.[NOM]</ta>
            <ta e="T277" id="Seg_3255" s="T276">летать-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T278" id="Seg_3256" s="T277">пока</ta>
            <ta e="T279" id="Seg_3257" s="T278">Тойоо.[NOM]</ta>
            <ta e="T280" id="Seg_3258" s="T279">друг-PL-3SG-ACC</ta>
            <ta e="T281" id="Seg_3259" s="T280">с</ta>
            <ta e="T282" id="Seg_3260" s="T281">берег-DAT/LOC</ta>
            <ta e="T283" id="Seg_3261" s="T282">стоять-PST2-3PL</ta>
            <ta e="T284" id="Seg_3262" s="T283">есть-CVB.SIM</ta>
            <ta e="T285" id="Seg_3263" s="T284">NEG</ta>
            <ta e="T286" id="Seg_3264" s="T285">идти-NEG.CVB.SIM</ta>
            <ta e="T287" id="Seg_3265" s="T286">интернат-3PL-DAT/LOC</ta>
            <ta e="T288" id="Seg_3266" s="T287">самолет.[NOM]</ta>
            <ta e="T289" id="Seg_3267" s="T288">сторона-3SG-INSTR</ta>
            <ta e="T290" id="Seg_3268" s="T289">разговаривать-NMNZ.[NOM]</ta>
            <ta e="T291" id="Seg_3269" s="T290">на.улице</ta>
            <ta e="T292" id="Seg_3270" s="T291">каждый</ta>
            <ta e="T293" id="Seg_3271" s="T292">дом.[NOM]</ta>
            <ta e="T294" id="Seg_3272" s="T293">каждый</ta>
            <ta e="T295" id="Seg_3273" s="T294">когда</ta>
            <ta e="T296" id="Seg_3274" s="T295">NEG</ta>
            <ta e="T297" id="Seg_3275" s="T296">кончать-NEG.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_3276" s="T0">adv</ta>
            <ta e="T2" id="Seg_3277" s="T1">v-v:mood.pn</ta>
            <ta e="T3" id="Seg_3278" s="T2">adj</ta>
            <ta e="T4" id="Seg_3279" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_3280" s="T4">n-n:(poss)-n:case</ta>
            <ta e="T6" id="Seg_3281" s="T5">n</ta>
            <ta e="T7" id="Seg_3282" s="T6">propr-n:case</ta>
            <ta e="T8" id="Seg_3283" s="T7">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T9" id="Seg_3284" s="T8">n-n:poss-n:case</ta>
            <ta e="T10" id="Seg_3285" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_3286" s="T10">v-v:tense-v:pred.pn</ta>
            <ta e="T12" id="Seg_3287" s="T11">cardnum</ta>
            <ta e="T13" id="Seg_3288" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_3289" s="T13">adv</ta>
            <ta e="T15" id="Seg_3290" s="T14">v-v:cvb</ta>
            <ta e="T16" id="Seg_3291" s="T15">v-v:cvb</ta>
            <ta e="T17" id="Seg_3292" s="T16">adv</ta>
            <ta e="T18" id="Seg_3293" s="T17">v-v&gt;v-v:cvb</ta>
            <ta e="T19" id="Seg_3294" s="T18">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T20" id="Seg_3295" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_3296" s="T20">post</ta>
            <ta e="T22" id="Seg_3297" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_3298" s="T22">v-v:cvb</ta>
            <ta e="T24" id="Seg_3299" s="T23">v-v:tense-v:poss.pn</ta>
            <ta e="T25" id="Seg_3300" s="T24">v-v:cvb</ta>
            <ta e="T26" id="Seg_3301" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_3302" s="T26">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T28" id="Seg_3303" s="T27">adj</ta>
            <ta e="T29" id="Seg_3304" s="T28">n-n:(num)-n:case</ta>
            <ta e="T30" id="Seg_3305" s="T29">v-v:tense-v:pred.pn</ta>
            <ta e="T31" id="Seg_3306" s="T30">v-v:tense-v:pred.pn</ta>
            <ta e="T32" id="Seg_3307" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_3308" s="T32">adj</ta>
            <ta e="T34" id="Seg_3309" s="T33">n-n:(num)-n:case</ta>
            <ta e="T35" id="Seg_3310" s="T34">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T36" id="Seg_3311" s="T35">v-v:cvb</ta>
            <ta e="T37" id="Seg_3312" s="T36">que</ta>
            <ta e="T38" id="Seg_3313" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_3314" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_3315" s="T39">v-v:cvb</ta>
            <ta e="T41" id="Seg_3316" s="T40">propr-n:case</ta>
            <ta e="T42" id="Seg_3317" s="T41">que</ta>
            <ta e="T43" id="Seg_3318" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_3319" s="T43">v-v:cvb</ta>
            <ta e="T45" id="Seg_3320" s="T44">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T46" id="Seg_3321" s="T45">v-v:tense-v:pred.pn</ta>
            <ta e="T47" id="Seg_3322" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_3323" s="T47">que</ta>
            <ta e="T49" id="Seg_3324" s="T48">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T50" id="Seg_3325" s="T49">dempro</ta>
            <ta e="T51" id="Seg_3326" s="T50">n-n:(poss)-n:case</ta>
            <ta e="T52" id="Seg_3327" s="T51">que-pro:case</ta>
            <ta e="T53" id="Seg_3328" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_3329" s="T53">v-v:tense-v:pred.pn</ta>
            <ta e="T55" id="Seg_3330" s="T54">dempro-pro:case</ta>
            <ta e="T56" id="Seg_3331" s="T55">conj</ta>
            <ta e="T57" id="Seg_3332" s="T56">v-v:mood-v:pred.pn</ta>
            <ta e="T58" id="Seg_3333" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_3334" s="T58">v-v:tense-v:pred.pn</ta>
            <ta e="T60" id="Seg_3335" s="T59">que</ta>
            <ta e="T61" id="Seg_3336" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_3337" s="T61">adv</ta>
            <ta e="T63" id="Seg_3338" s="T62">v-v:neg-v:pred.pn</ta>
            <ta e="T64" id="Seg_3339" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_3340" s="T64">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T66" id="Seg_3341" s="T65">post</ta>
            <ta e="T67" id="Seg_3342" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_3343" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_3344" s="T68">v-v:tense-v:pred.pn</ta>
            <ta e="T70" id="Seg_3345" s="T69">adv</ta>
            <ta e="T71" id="Seg_3346" s="T70">que-pro:case</ta>
            <ta e="T72" id="Seg_3347" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_3348" s="T72">post</ta>
            <ta e="T74" id="Seg_3349" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_3350" s="T74">v-v:cvb</ta>
            <ta e="T76" id="Seg_3351" s="T75">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T77" id="Seg_3352" s="T76">n-n:(num)-n:case</ta>
            <ta e="T78" id="Seg_3353" s="T77">n-n:poss-n:case</ta>
            <ta e="T79" id="Seg_3354" s="T78">propr-n:case</ta>
            <ta e="T80" id="Seg_3355" s="T79">v-v:tense-v:pred.pn</ta>
            <ta e="T81" id="Seg_3356" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_3357" s="T81">v-v:ptcp</ta>
            <ta e="T83" id="Seg_3358" s="T82">n-n:poss-n:case</ta>
            <ta e="T84" id="Seg_3359" s="T83">interj</ta>
            <ta e="T85" id="Seg_3360" s="T84">v-v:cvb</ta>
            <ta e="T86" id="Seg_3361" s="T85">v-v:tense-v:pred.pn</ta>
            <ta e="T87" id="Seg_3362" s="T86">n-n:(num)-n:poss-n:case</ta>
            <ta e="T88" id="Seg_3363" s="T87">v-v:tense-v:pred.pn</ta>
            <ta e="T89" id="Seg_3364" s="T88">v-v:ptcp-v:(case)</ta>
            <ta e="T90" id="Seg_3365" s="T89">post</ta>
            <ta e="T91" id="Seg_3366" s="T90">v-v:cvb</ta>
            <ta e="T92" id="Seg_3367" s="T91">v-v:tense-v:pred.pn</ta>
            <ta e="T93" id="Seg_3368" s="T92">que-pro:case</ta>
            <ta e="T94" id="Seg_3369" s="T93">v-v:tense-v:poss.pn</ta>
            <ta e="T95" id="Seg_3370" s="T94">v-v:mood.pn</ta>
            <ta e="T96" id="Seg_3371" s="T95">adj-adj&gt;adv</ta>
            <ta e="T97" id="Seg_3372" s="T96">n-n:case</ta>
            <ta e="T98" id="Seg_3373" s="T97">v-v:cvb</ta>
            <ta e="T99" id="Seg_3374" s="T98">v-v:mood.pn</ta>
            <ta e="T100" id="Seg_3375" s="T99">que</ta>
            <ta e="T101" id="Seg_3376" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_3377" s="T101">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T103" id="Seg_3378" s="T102">n-n:case</ta>
            <ta e="T104" id="Seg_3379" s="T103">n-n:poss-n:case</ta>
            <ta e="T105" id="Seg_3380" s="T104">n-n:poss-n:case</ta>
            <ta e="T106" id="Seg_3381" s="T105">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T107" id="Seg_3382" s="T106">propr-n:case</ta>
            <ta e="T108" id="Seg_3383" s="T107">n-n:(num)-n:poss-n:case</ta>
            <ta e="T109" id="Seg_3384" s="T108">v-v:tense-v:poss.pn</ta>
            <ta e="T110" id="Seg_3385" s="T109">propr-n:case</ta>
            <ta e="T111" id="Seg_3386" s="T110">adj</ta>
            <ta e="T112" id="Seg_3387" s="T111">n-n:case</ta>
            <ta e="T113" id="Seg_3388" s="T112">v-v:tense-v:pred.pn</ta>
            <ta e="T114" id="Seg_3389" s="T113">v-v:(neg)-v:pred.pn</ta>
            <ta e="T115" id="Seg_3390" s="T114">pers-pro:case</ta>
            <ta e="T116" id="Seg_3391" s="T115">n-n:case</ta>
            <ta e="T117" id="Seg_3392" s="T116">v-v&gt;v-v:cvb</ta>
            <ta e="T118" id="Seg_3393" s="T117">v-v:cvb</ta>
            <ta e="T119" id="Seg_3394" s="T118">v-v:tense-v:pred.pn</ta>
            <ta e="T120" id="Seg_3395" s="T119">n-n:(num)-n:case</ta>
            <ta e="T121" id="Seg_3396" s="T120">v-v:(neg)-v:pred.pn</ta>
            <ta e="T122" id="Seg_3397" s="T121">pers-pro:case</ta>
            <ta e="T123" id="Seg_3398" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_3399" s="T123">que-pro:case</ta>
            <ta e="T125" id="Seg_3400" s="T124">conj</ta>
            <ta e="T126" id="Seg_3401" s="T125">v-v:tense-v:poss.pn</ta>
            <ta e="T127" id="Seg_3402" s="T126">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T128" id="Seg_3403" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_3404" s="T128">v-v:cvb</ta>
            <ta e="T130" id="Seg_3405" s="T129">v-v:tense-v:pred.pn</ta>
            <ta e="T131" id="Seg_3406" s="T130">propr-n:case</ta>
            <ta e="T132" id="Seg_3407" s="T131">v-v:cvb</ta>
            <ta e="T133" id="Seg_3408" s="T132">v-v:cvb</ta>
            <ta e="T134" id="Seg_3409" s="T133">adj</ta>
            <ta e="T135" id="Seg_3410" s="T134">n-n:poss-n:case</ta>
            <ta e="T136" id="Seg_3411" s="T135">v-v:tense-v:pred.pn</ta>
            <ta e="T137" id="Seg_3412" s="T136">v-v:cvb</ta>
            <ta e="T138" id="Seg_3413" s="T137">v-v:cvb-v-v:cvb</ta>
            <ta e="T139" id="Seg_3414" s="T138">adj</ta>
            <ta e="T140" id="Seg_3415" s="T139">n-n:(num)-n:case</ta>
            <ta e="T141" id="Seg_3416" s="T140">n-n:poss-n:case</ta>
            <ta e="T142" id="Seg_3417" s="T141">n-n:case</ta>
            <ta e="T143" id="Seg_3418" s="T142">n-n:poss-n:case</ta>
            <ta e="T144" id="Seg_3419" s="T143">n-n:case</ta>
            <ta e="T145" id="Seg_3420" s="T144">n-n:poss-n:case</ta>
            <ta e="T146" id="Seg_3421" s="T145">v-v&gt;v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T147" id="Seg_3422" s="T146">v-v&gt;v-v&gt;n-n:case</ta>
            <ta e="T148" id="Seg_3423" s="T147">v-v&gt;n-n:case</ta>
            <ta e="T149" id="Seg_3424" s="T148">v-v:cvb</ta>
            <ta e="T150" id="Seg_3425" s="T149">v-v:tense-v:pred.pn</ta>
            <ta e="T151" id="Seg_3426" s="T150">n-n:case</ta>
            <ta e="T152" id="Seg_3427" s="T151">n-n:poss-n:case</ta>
            <ta e="T153" id="Seg_3428" s="T152">n-n:case</ta>
            <ta e="T154" id="Seg_3429" s="T153">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T155" id="Seg_3430" s="T154">post</ta>
            <ta e="T156" id="Seg_3431" s="T155">n-n:case</ta>
            <ta e="T157" id="Seg_3432" s="T156">v-v:cvb</ta>
            <ta e="T158" id="Seg_3433" s="T157">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T159" id="Seg_3434" s="T158">n-n:(poss)-n:case</ta>
            <ta e="T160" id="Seg_3435" s="T159">adv</ta>
            <ta e="T161" id="Seg_3436" s="T160">n-n:poss-n:case</ta>
            <ta e="T162" id="Seg_3437" s="T161">n-n:case</ta>
            <ta e="T163" id="Seg_3438" s="T162">n-n&gt;v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T164" id="Seg_3439" s="T163">adj</ta>
            <ta e="T165" id="Seg_3440" s="T164">n-n:poss-n:case</ta>
            <ta e="T166" id="Seg_3441" s="T165">propr-n:case</ta>
            <ta e="T167" id="Seg_3442" s="T166">v-v:cvb</ta>
            <ta e="T168" id="Seg_3443" s="T167">v-v:tense-v:pred.pn</ta>
            <ta e="T169" id="Seg_3444" s="T168">n-n:case</ta>
            <ta e="T170" id="Seg_3445" s="T169">n-n:case</ta>
            <ta e="T171" id="Seg_3446" s="T170">v-v:cvb</ta>
            <ta e="T172" id="Seg_3447" s="T171">n-n:case</ta>
            <ta e="T173" id="Seg_3448" s="T172">v-v:tense-v:poss.pn</ta>
            <ta e="T174" id="Seg_3449" s="T173">v-v:cvb</ta>
            <ta e="T175" id="Seg_3450" s="T174">pers-pro:case</ta>
            <ta e="T176" id="Seg_3451" s="T175">post</ta>
            <ta e="T177" id="Seg_3452" s="T176">v-v:tense-v:poss.pn</ta>
            <ta e="T178" id="Seg_3453" s="T177">v-v:cvb</ta>
            <ta e="T179" id="Seg_3454" s="T178">v-v:ptcp</ta>
            <ta e="T180" id="Seg_3455" s="T179">n-n:(num)-n:case</ta>
            <ta e="T181" id="Seg_3456" s="T180">n-n:poss-n:case</ta>
            <ta e="T182" id="Seg_3457" s="T181">adv-adv&gt;adv</ta>
            <ta e="T183" id="Seg_3458" s="T182">v-v:tense-v:pred.pn</ta>
            <ta e="T184" id="Seg_3459" s="T183">adv</ta>
            <ta e="T185" id="Seg_3460" s="T184">adv</ta>
            <ta e="T186" id="Seg_3461" s="T185">v-v:tense-v:pred.pn</ta>
            <ta e="T187" id="Seg_3462" s="T186">n-n:(poss)-n:case</ta>
            <ta e="T188" id="Seg_3463" s="T187">n-n:case</ta>
            <ta e="T189" id="Seg_3464" s="T188">post</ta>
            <ta e="T190" id="Seg_3465" s="T189">n-n:poss-n:case</ta>
            <ta e="T191" id="Seg_3466" s="T190">n-n:case</ta>
            <ta e="T192" id="Seg_3467" s="T191">v-v:tense-v:pred.pn</ta>
            <ta e="T193" id="Seg_3468" s="T192">n-n:case</ta>
            <ta e="T194" id="Seg_3469" s="T193">v-v:tense-v:pred.pn</ta>
            <ta e="T195" id="Seg_3470" s="T194">n-n:poss-n:case</ta>
            <ta e="T196" id="Seg_3471" s="T195">n-n:case</ta>
            <ta e="T197" id="Seg_3472" s="T196">v-v&gt;v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T198" id="Seg_3473" s="T197">n-n:case</ta>
            <ta e="T199" id="Seg_3474" s="T198">n-n:(poss)-n:case</ta>
            <ta e="T200" id="Seg_3475" s="T199">v-v:cvb</ta>
            <ta e="T201" id="Seg_3476" s="T200">v-v:tense-v:pred.pn</ta>
            <ta e="T202" id="Seg_3477" s="T201">n-n:case</ta>
            <ta e="T203" id="Seg_3478" s="T202">v-v:cvb</ta>
            <ta e="T204" id="Seg_3479" s="T203">v-v:cvb</ta>
            <ta e="T205" id="Seg_3480" s="T204">n-n:case</ta>
            <ta e="T206" id="Seg_3481" s="T205">n-n:poss-n:case</ta>
            <ta e="T207" id="Seg_3482" s="T206">n-n:poss-n:case</ta>
            <ta e="T208" id="Seg_3483" s="T207">v-v:cvb</ta>
            <ta e="T209" id="Seg_3484" s="T208">v-v:(ins)-v&gt;v-v:(ins)-v:ptcp</ta>
            <ta e="T210" id="Seg_3485" s="T209">n-n:case</ta>
            <ta e="T211" id="Seg_3486" s="T210">n-n:poss-n:case</ta>
            <ta e="T212" id="Seg_3487" s="T211">v-v:cvb</ta>
            <ta e="T213" id="Seg_3488" s="T212">v-v:tense-v:pred.pn</ta>
            <ta e="T214" id="Seg_3489" s="T213">n-n:poss-n:case</ta>
            <ta e="T215" id="Seg_3490" s="T214">n-n&gt;adv</ta>
            <ta e="T216" id="Seg_3491" s="T215">v-v:cvb</ta>
            <ta e="T217" id="Seg_3492" s="T216">v-v:tense-v:pred.pn</ta>
            <ta e="T218" id="Seg_3493" s="T217">ptcl</ta>
            <ta e="T219" id="Seg_3494" s="T218">adj-n:case</ta>
            <ta e="T220" id="Seg_3495" s="T219">n-n:case</ta>
            <ta e="T221" id="Seg_3496" s="T220">post</ta>
            <ta e="T222" id="Seg_3497" s="T221">n-n&gt;adv</ta>
            <ta e="T223" id="Seg_3498" s="T222">conj</ta>
            <ta e="T224" id="Seg_3499" s="T223">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T225" id="Seg_3500" s="T224">v-v:tense-v:pred.pn</ta>
            <ta e="T226" id="Seg_3501" s="T225">v-v:cvb</ta>
            <ta e="T227" id="Seg_3502" s="T226">v-v:cvb</ta>
            <ta e="T228" id="Seg_3503" s="T227">v-v:tense-v:pred.pn</ta>
            <ta e="T229" id="Seg_3504" s="T228">propr-n:case</ta>
            <ta e="T230" id="Seg_3505" s="T229">n-n:(num)-n:case</ta>
            <ta e="T231" id="Seg_3506" s="T230">ptcl</ta>
            <ta e="T232" id="Seg_3507" s="T231">v-v&gt;v-v&gt;v-v:cvb-v-v&gt;v-v&gt;v-v:cvb</ta>
            <ta e="T233" id="Seg_3508" s="T232">n-n:case</ta>
            <ta e="T234" id="Seg_3509" s="T233">post</ta>
            <ta e="T235" id="Seg_3510" s="T234">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T236" id="Seg_3511" s="T235">propr-n:case</ta>
            <ta e="T237" id="Seg_3512" s="T236">adv</ta>
            <ta e="T238" id="Seg_3513" s="T237">v-v:cvb</ta>
            <ta e="T239" id="Seg_3514" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_3515" s="T239">n-n:poss-n:case</ta>
            <ta e="T241" id="Seg_3516" s="T240">n-n:poss-n:case</ta>
            <ta e="T242" id="Seg_3517" s="T241">n-n:poss-n:case</ta>
            <ta e="T243" id="Seg_3518" s="T242">v-v:tense-v:pred.pn</ta>
            <ta e="T244" id="Seg_3519" s="T243">pers-pro:case</ta>
            <ta e="T245" id="Seg_3520" s="T244">ptcl</ta>
            <ta e="T246" id="Seg_3521" s="T245">v-v:tense</ta>
            <ta e="T247" id="Seg_3522" s="T246">v-v:(neg)-v:pred.pn</ta>
            <ta e="T248" id="Seg_3523" s="T247">n-n:case</ta>
            <ta e="T249" id="Seg_3524" s="T248">que</ta>
            <ta e="T250" id="Seg_3525" s="T249">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T251" id="Seg_3526" s="T250">que</ta>
            <ta e="T252" id="Seg_3527" s="T251">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T253" id="Seg_3528" s="T252">adv</ta>
            <ta e="T254" id="Seg_3529" s="T253">adv</ta>
            <ta e="T255" id="Seg_3530" s="T254">n-n:case</ta>
            <ta e="T256" id="Seg_3531" s="T255">v-v:ptcp</ta>
            <ta e="T257" id="Seg_3532" s="T256">v-v:tense-v:poss.pn</ta>
            <ta e="T258" id="Seg_3533" s="T257">n-n:poss-n:case</ta>
            <ta e="T259" id="Seg_3534" s="T258">v-v:cvb</ta>
            <ta e="T260" id="Seg_3535" s="T259">v-v:tense-v:poss.pn</ta>
            <ta e="T261" id="Seg_3536" s="T260">v-v:mood-v:pred.pn</ta>
            <ta e="T262" id="Seg_3537" s="T261">dempro</ta>
            <ta e="T263" id="Seg_3538" s="T262">n-n:(poss)-n:case</ta>
            <ta e="T264" id="Seg_3539" s="T263">v-v:cvb</ta>
            <ta e="T265" id="Seg_3540" s="T264">v-v:cvb</ta>
            <ta e="T266" id="Seg_3541" s="T265">adj</ta>
            <ta e="T267" id="Seg_3542" s="T266">n-n:case</ta>
            <ta e="T268" id="Seg_3543" s="T267">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T269" id="Seg_3544" s="T268">v-v:tense-v:pred.pn</ta>
            <ta e="T270" id="Seg_3545" s="T269">v-v:ptcp</ta>
            <ta e="T271" id="Seg_3546" s="T270">n-n:poss-n:case</ta>
            <ta e="T272" id="Seg_3547" s="T271">n-n:case</ta>
            <ta e="T273" id="Seg_3548" s="T272">n-n:poss-n:case</ta>
            <ta e="T274" id="Seg_3549" s="T273">v-v:ptcp</ta>
            <ta e="T275" id="Seg_3550" s="T274">n-n:(num)-n:poss-n:case</ta>
            <ta e="T276" id="Seg_3551" s="T275">n-n:case</ta>
            <ta e="T277" id="Seg_3552" s="T276">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T278" id="Seg_3553" s="T277">post</ta>
            <ta e="T279" id="Seg_3554" s="T278">propr-n:case</ta>
            <ta e="T280" id="Seg_3555" s="T279">n-n:(num)-n:poss-n:case</ta>
            <ta e="T281" id="Seg_3556" s="T280">post</ta>
            <ta e="T282" id="Seg_3557" s="T281">n-n:case</ta>
            <ta e="T283" id="Seg_3558" s="T282">v-v:tense-v:pred.pn</ta>
            <ta e="T284" id="Seg_3559" s="T283">v-v:cvb</ta>
            <ta e="T285" id="Seg_3560" s="T284">ptcl</ta>
            <ta e="T286" id="Seg_3561" s="T285">v-v:cvb</ta>
            <ta e="T287" id="Seg_3562" s="T286">n-n:poss-n:case</ta>
            <ta e="T288" id="Seg_3563" s="T287">n-n:case</ta>
            <ta e="T289" id="Seg_3564" s="T288">n-n:poss-n:case</ta>
            <ta e="T290" id="Seg_3565" s="T289">v-v&gt;n-n:case</ta>
            <ta e="T291" id="Seg_3566" s="T290">adv</ta>
            <ta e="T292" id="Seg_3567" s="T291">adj</ta>
            <ta e="T293" id="Seg_3568" s="T292">n-n:case</ta>
            <ta e="T294" id="Seg_3569" s="T293">adj</ta>
            <ta e="T295" id="Seg_3570" s="T294">que</ta>
            <ta e="T296" id="Seg_3571" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_3572" s="T296">v-v:(neg)-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_3573" s="T0">adv</ta>
            <ta e="T2" id="Seg_3574" s="T1">v</ta>
            <ta e="T3" id="Seg_3575" s="T2">adj</ta>
            <ta e="T4" id="Seg_3576" s="T3">n</ta>
            <ta e="T5" id="Seg_3577" s="T4">n</ta>
            <ta e="T6" id="Seg_3578" s="T5">n</ta>
            <ta e="T7" id="Seg_3579" s="T6">propr</ta>
            <ta e="T8" id="Seg_3580" s="T7">ordnum</ta>
            <ta e="T9" id="Seg_3581" s="T8">n</ta>
            <ta e="T10" id="Seg_3582" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_3583" s="T10">v</ta>
            <ta e="T12" id="Seg_3584" s="T11">cardnum</ta>
            <ta e="T13" id="Seg_3585" s="T12">n</ta>
            <ta e="T14" id="Seg_3586" s="T13">adv</ta>
            <ta e="T15" id="Seg_3587" s="T14">v</ta>
            <ta e="T16" id="Seg_3588" s="T15">v</ta>
            <ta e="T17" id="Seg_3589" s="T16">adv</ta>
            <ta e="T18" id="Seg_3590" s="T17">v</ta>
            <ta e="T19" id="Seg_3591" s="T18">aux</ta>
            <ta e="T20" id="Seg_3592" s="T19">n</ta>
            <ta e="T21" id="Seg_3593" s="T20">post</ta>
            <ta e="T22" id="Seg_3594" s="T21">n</ta>
            <ta e="T23" id="Seg_3595" s="T22">v</ta>
            <ta e="T24" id="Seg_3596" s="T23">v</ta>
            <ta e="T25" id="Seg_3597" s="T24">v</ta>
            <ta e="T26" id="Seg_3598" s="T25">n</ta>
            <ta e="T27" id="Seg_3599" s="T26">v</ta>
            <ta e="T28" id="Seg_3600" s="T27">adj</ta>
            <ta e="T29" id="Seg_3601" s="T28">n</ta>
            <ta e="T30" id="Seg_3602" s="T29">v</ta>
            <ta e="T31" id="Seg_3603" s="T30">aux</ta>
            <ta e="T32" id="Seg_3604" s="T31">n</ta>
            <ta e="T33" id="Seg_3605" s="T32">adj</ta>
            <ta e="T34" id="Seg_3606" s="T33">n</ta>
            <ta e="T35" id="Seg_3607" s="T34">v</ta>
            <ta e="T36" id="Seg_3608" s="T35">v</ta>
            <ta e="T37" id="Seg_3609" s="T36">que</ta>
            <ta e="T38" id="Seg_3610" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_3611" s="T38">n</ta>
            <ta e="T40" id="Seg_3612" s="T39">v</ta>
            <ta e="T41" id="Seg_3613" s="T40">propr</ta>
            <ta e="T42" id="Seg_3614" s="T41">que</ta>
            <ta e="T43" id="Seg_3615" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_3616" s="T43">v</ta>
            <ta e="T45" id="Seg_3617" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_3618" s="T45">aux</ta>
            <ta e="T47" id="Seg_3619" s="T46">n</ta>
            <ta e="T48" id="Seg_3620" s="T47">que</ta>
            <ta e="T49" id="Seg_3621" s="T48">cop</ta>
            <ta e="T50" id="Seg_3622" s="T49">dempro</ta>
            <ta e="T51" id="Seg_3623" s="T50">n</ta>
            <ta e="T52" id="Seg_3624" s="T51">que</ta>
            <ta e="T53" id="Seg_3625" s="T52">n</ta>
            <ta e="T54" id="Seg_3626" s="T53">v</ta>
            <ta e="T55" id="Seg_3627" s="T54">dempro</ta>
            <ta e="T56" id="Seg_3628" s="T55">conj</ta>
            <ta e="T57" id="Seg_3629" s="T56">cop</ta>
            <ta e="T58" id="Seg_3630" s="T57">ptcl</ta>
            <ta e="T59" id="Seg_3631" s="T58">v</ta>
            <ta e="T60" id="Seg_3632" s="T59">que</ta>
            <ta e="T61" id="Seg_3633" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_3634" s="T61">adv</ta>
            <ta e="T63" id="Seg_3635" s="T62">cop</ta>
            <ta e="T64" id="Seg_3636" s="T63">n</ta>
            <ta e="T65" id="Seg_3637" s="T64">v</ta>
            <ta e="T66" id="Seg_3638" s="T65">post</ta>
            <ta e="T67" id="Seg_3639" s="T66">n</ta>
            <ta e="T68" id="Seg_3640" s="T67">n</ta>
            <ta e="T69" id="Seg_3641" s="T68">cop</ta>
            <ta e="T70" id="Seg_3642" s="T69">adv</ta>
            <ta e="T71" id="Seg_3643" s="T70">que</ta>
            <ta e="T72" id="Seg_3644" s="T71">ptcl</ta>
            <ta e="T73" id="Seg_3645" s="T72">post</ta>
            <ta e="T74" id="Seg_3646" s="T73">n</ta>
            <ta e="T75" id="Seg_3647" s="T74">v</ta>
            <ta e="T76" id="Seg_3648" s="T75">aux</ta>
            <ta e="T77" id="Seg_3649" s="T76">n</ta>
            <ta e="T78" id="Seg_3650" s="T77">n</ta>
            <ta e="T79" id="Seg_3651" s="T78">propr</ta>
            <ta e="T80" id="Seg_3652" s="T79">v</ta>
            <ta e="T81" id="Seg_3653" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_3654" s="T81">v</ta>
            <ta e="T83" id="Seg_3655" s="T82">n</ta>
            <ta e="T84" id="Seg_3656" s="T83">interj</ta>
            <ta e="T85" id="Seg_3657" s="T84">v</ta>
            <ta e="T86" id="Seg_3658" s="T85">v</ta>
            <ta e="T87" id="Seg_3659" s="T86">n</ta>
            <ta e="T88" id="Seg_3660" s="T87">v</ta>
            <ta e="T89" id="Seg_3661" s="T88">v</ta>
            <ta e="T90" id="Seg_3662" s="T89">post</ta>
            <ta e="T91" id="Seg_3663" s="T90">v</ta>
            <ta e="T92" id="Seg_3664" s="T91">aux</ta>
            <ta e="T93" id="Seg_3665" s="T92">que</ta>
            <ta e="T94" id="Seg_3666" s="T93">cop</ta>
            <ta e="T95" id="Seg_3667" s="T94">v</ta>
            <ta e="T96" id="Seg_3668" s="T95">adv</ta>
            <ta e="T97" id="Seg_3669" s="T96">n</ta>
            <ta e="T98" id="Seg_3670" s="T97">v</ta>
            <ta e="T99" id="Seg_3671" s="T98">aux</ta>
            <ta e="T100" id="Seg_3672" s="T99">que</ta>
            <ta e="T101" id="Seg_3673" s="T100">n</ta>
            <ta e="T102" id="Seg_3674" s="T101">v</ta>
            <ta e="T103" id="Seg_3675" s="T102">n</ta>
            <ta e="T104" id="Seg_3676" s="T103">n</ta>
            <ta e="T105" id="Seg_3677" s="T104">n</ta>
            <ta e="T106" id="Seg_3678" s="T105">v</ta>
            <ta e="T107" id="Seg_3679" s="T106">propr</ta>
            <ta e="T108" id="Seg_3680" s="T107">n</ta>
            <ta e="T109" id="Seg_3681" s="T108">v</ta>
            <ta e="T110" id="Seg_3682" s="T109">propr</ta>
            <ta e="T111" id="Seg_3683" s="T110">adj</ta>
            <ta e="T112" id="Seg_3684" s="T111">n</ta>
            <ta e="T113" id="Seg_3685" s="T112">v</ta>
            <ta e="T114" id="Seg_3686" s="T113">v</ta>
            <ta e="T115" id="Seg_3687" s="T114">pers</ta>
            <ta e="T116" id="Seg_3688" s="T115">n</ta>
            <ta e="T117" id="Seg_3689" s="T116">v</ta>
            <ta e="T118" id="Seg_3690" s="T117">v</ta>
            <ta e="T119" id="Seg_3691" s="T118">aux</ta>
            <ta e="T120" id="Seg_3692" s="T119">n</ta>
            <ta e="T121" id="Seg_3693" s="T120">v</ta>
            <ta e="T122" id="Seg_3694" s="T121">pers</ta>
            <ta e="T123" id="Seg_3695" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_3696" s="T123">que</ta>
            <ta e="T125" id="Seg_3697" s="T124">conj</ta>
            <ta e="T126" id="Seg_3698" s="T125">cop</ta>
            <ta e="T127" id="Seg_3699" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_3700" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_3701" s="T128">v</ta>
            <ta e="T130" id="Seg_3702" s="T129">v</ta>
            <ta e="T131" id="Seg_3703" s="T130">propr</ta>
            <ta e="T132" id="Seg_3704" s="T131">v</ta>
            <ta e="T133" id="Seg_3705" s="T132">v</ta>
            <ta e="T134" id="Seg_3706" s="T133">adj</ta>
            <ta e="T135" id="Seg_3707" s="T134">n</ta>
            <ta e="T136" id="Seg_3708" s="T135">v</ta>
            <ta e="T137" id="Seg_3709" s="T136">v</ta>
            <ta e="T138" id="Seg_3710" s="T137">v</ta>
            <ta e="T139" id="Seg_3711" s="T138">adj</ta>
            <ta e="T140" id="Seg_3712" s="T139">n</ta>
            <ta e="T141" id="Seg_3713" s="T140">n</ta>
            <ta e="T142" id="Seg_3714" s="T141">n</ta>
            <ta e="T143" id="Seg_3715" s="T142">n</ta>
            <ta e="T144" id="Seg_3716" s="T143">n</ta>
            <ta e="T145" id="Seg_3717" s="T144">n</ta>
            <ta e="T146" id="Seg_3718" s="T145">v</ta>
            <ta e="T147" id="Seg_3719" s="T146">n</ta>
            <ta e="T148" id="Seg_3720" s="T147">n</ta>
            <ta e="T149" id="Seg_3721" s="T148">cop</ta>
            <ta e="T150" id="Seg_3722" s="T149">aux</ta>
            <ta e="T151" id="Seg_3723" s="T150">n</ta>
            <ta e="T152" id="Seg_3724" s="T151">n</ta>
            <ta e="T153" id="Seg_3725" s="T152">n</ta>
            <ta e="T154" id="Seg_3726" s="T153">v</ta>
            <ta e="T155" id="Seg_3727" s="T154">post</ta>
            <ta e="T156" id="Seg_3728" s="T155">n</ta>
            <ta e="T157" id="Seg_3729" s="T156">v</ta>
            <ta e="T158" id="Seg_3730" s="T157">aux</ta>
            <ta e="T159" id="Seg_3731" s="T158">n</ta>
            <ta e="T160" id="Seg_3732" s="T159">adv</ta>
            <ta e="T161" id="Seg_3733" s="T160">n</ta>
            <ta e="T162" id="Seg_3734" s="T161">n</ta>
            <ta e="T163" id="Seg_3735" s="T162">v</ta>
            <ta e="T164" id="Seg_3736" s="T163">adj</ta>
            <ta e="T165" id="Seg_3737" s="T164">n</ta>
            <ta e="T166" id="Seg_3738" s="T165">propr</ta>
            <ta e="T167" id="Seg_3739" s="T166">v</ta>
            <ta e="T168" id="Seg_3740" s="T167">v</ta>
            <ta e="T169" id="Seg_3741" s="T168">n</ta>
            <ta e="T170" id="Seg_3742" s="T169">n</ta>
            <ta e="T171" id="Seg_3743" s="T170">v</ta>
            <ta e="T172" id="Seg_3744" s="T171">n</ta>
            <ta e="T173" id="Seg_3745" s="T172">v</ta>
            <ta e="T174" id="Seg_3746" s="T173">v</ta>
            <ta e="T175" id="Seg_3747" s="T174">pers</ta>
            <ta e="T176" id="Seg_3748" s="T175">post</ta>
            <ta e="T177" id="Seg_3749" s="T176">v</ta>
            <ta e="T178" id="Seg_3750" s="T177">v</ta>
            <ta e="T179" id="Seg_3751" s="T178">v</ta>
            <ta e="T180" id="Seg_3752" s="T179">n</ta>
            <ta e="T181" id="Seg_3753" s="T180">n</ta>
            <ta e="T182" id="Seg_3754" s="T181">adv</ta>
            <ta e="T183" id="Seg_3755" s="T182">v</ta>
            <ta e="T184" id="Seg_3756" s="T183">adv</ta>
            <ta e="T185" id="Seg_3757" s="T184">adv</ta>
            <ta e="T186" id="Seg_3758" s="T185">v</ta>
            <ta e="T187" id="Seg_3759" s="T186">n</ta>
            <ta e="T188" id="Seg_3760" s="T187">n</ta>
            <ta e="T189" id="Seg_3761" s="T188">post</ta>
            <ta e="T190" id="Seg_3762" s="T189">n</ta>
            <ta e="T191" id="Seg_3763" s="T190">n</ta>
            <ta e="T192" id="Seg_3764" s="T191">v</ta>
            <ta e="T193" id="Seg_3765" s="T192">n</ta>
            <ta e="T194" id="Seg_3766" s="T193">v</ta>
            <ta e="T195" id="Seg_3767" s="T194">n</ta>
            <ta e="T196" id="Seg_3768" s="T195">n</ta>
            <ta e="T197" id="Seg_3769" s="T196">v</ta>
            <ta e="T198" id="Seg_3770" s="T197">n</ta>
            <ta e="T199" id="Seg_3771" s="T198">n</ta>
            <ta e="T200" id="Seg_3772" s="T199">v</ta>
            <ta e="T201" id="Seg_3773" s="T200">aux</ta>
            <ta e="T202" id="Seg_3774" s="T201">n</ta>
            <ta e="T203" id="Seg_3775" s="T202">v</ta>
            <ta e="T204" id="Seg_3776" s="T203">v</ta>
            <ta e="T205" id="Seg_3777" s="T204">n</ta>
            <ta e="T206" id="Seg_3778" s="T205">n</ta>
            <ta e="T207" id="Seg_3779" s="T206">n</ta>
            <ta e="T208" id="Seg_3780" s="T207">v</ta>
            <ta e="T209" id="Seg_3781" s="T208">v</ta>
            <ta e="T210" id="Seg_3782" s="T209">n</ta>
            <ta e="T211" id="Seg_3783" s="T210">n</ta>
            <ta e="T212" id="Seg_3784" s="T211">v</ta>
            <ta e="T213" id="Seg_3785" s="T212">aux</ta>
            <ta e="T214" id="Seg_3786" s="T213">n</ta>
            <ta e="T215" id="Seg_3787" s="T214">adv</ta>
            <ta e="T216" id="Seg_3788" s="T215">v</ta>
            <ta e="T217" id="Seg_3789" s="T216">aux</ta>
            <ta e="T218" id="Seg_3790" s="T217">ptcl</ta>
            <ta e="T219" id="Seg_3791" s="T218">adj</ta>
            <ta e="T220" id="Seg_3792" s="T219">n</ta>
            <ta e="T221" id="Seg_3793" s="T220">post</ta>
            <ta e="T222" id="Seg_3794" s="T221">adv</ta>
            <ta e="T223" id="Seg_3795" s="T222">conj</ta>
            <ta e="T224" id="Seg_3796" s="T223">v</ta>
            <ta e="T225" id="Seg_3797" s="T224">aux</ta>
            <ta e="T226" id="Seg_3798" s="T225">v</ta>
            <ta e="T227" id="Seg_3799" s="T226">v</ta>
            <ta e="T228" id="Seg_3800" s="T227">aux</ta>
            <ta e="T229" id="Seg_3801" s="T228">propr</ta>
            <ta e="T230" id="Seg_3802" s="T229">n</ta>
            <ta e="T231" id="Seg_3803" s="T230">ptcl</ta>
            <ta e="T232" id="Seg_3804" s="T231">v</ta>
            <ta e="T233" id="Seg_3805" s="T232">n</ta>
            <ta e="T234" id="Seg_3806" s="T233">post</ta>
            <ta e="T235" id="Seg_3807" s="T234">v</ta>
            <ta e="T236" id="Seg_3808" s="T235">propr</ta>
            <ta e="T237" id="Seg_3809" s="T236">adv</ta>
            <ta e="T238" id="Seg_3810" s="T237">v</ta>
            <ta e="T239" id="Seg_3811" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_3812" s="T239">n</ta>
            <ta e="T241" id="Seg_3813" s="T240">n</ta>
            <ta e="T242" id="Seg_3814" s="T241">n</ta>
            <ta e="T243" id="Seg_3815" s="T242">v</ta>
            <ta e="T244" id="Seg_3816" s="T243">pers</ta>
            <ta e="T245" id="Seg_3817" s="T244">ptcl</ta>
            <ta e="T246" id="Seg_3818" s="T245">v</ta>
            <ta e="T247" id="Seg_3819" s="T246">v</ta>
            <ta e="T248" id="Seg_3820" s="T247">n</ta>
            <ta e="T249" id="Seg_3821" s="T248">que</ta>
            <ta e="T250" id="Seg_3822" s="T249">v</ta>
            <ta e="T251" id="Seg_3823" s="T250">que</ta>
            <ta e="T252" id="Seg_3824" s="T251">v</ta>
            <ta e="T253" id="Seg_3825" s="T252">adv</ta>
            <ta e="T254" id="Seg_3826" s="T253">adv</ta>
            <ta e="T255" id="Seg_3827" s="T254">n</ta>
            <ta e="T256" id="Seg_3828" s="T255">cop</ta>
            <ta e="T257" id="Seg_3829" s="T256">aux</ta>
            <ta e="T258" id="Seg_3830" s="T257">n</ta>
            <ta e="T259" id="Seg_3831" s="T258">v</ta>
            <ta e="T260" id="Seg_3832" s="T259">v</ta>
            <ta e="T261" id="Seg_3833" s="T260">aux</ta>
            <ta e="T262" id="Seg_3834" s="T261">dempro</ta>
            <ta e="T263" id="Seg_3835" s="T262">n</ta>
            <ta e="T264" id="Seg_3836" s="T263">v</ta>
            <ta e="T265" id="Seg_3837" s="T264">aux</ta>
            <ta e="T266" id="Seg_3838" s="T265">adj</ta>
            <ta e="T267" id="Seg_3839" s="T266">n</ta>
            <ta e="T268" id="Seg_3840" s="T267">v</ta>
            <ta e="T269" id="Seg_3841" s="T268">v</ta>
            <ta e="T270" id="Seg_3842" s="T269">v</ta>
            <ta e="T271" id="Seg_3843" s="T270">n</ta>
            <ta e="T272" id="Seg_3844" s="T271">n</ta>
            <ta e="T273" id="Seg_3845" s="T272">n</ta>
            <ta e="T274" id="Seg_3846" s="T273">v</ta>
            <ta e="T275" id="Seg_3847" s="T274">n</ta>
            <ta e="T276" id="Seg_3848" s="T275">n</ta>
            <ta e="T277" id="Seg_3849" s="T276">v</ta>
            <ta e="T278" id="Seg_3850" s="T277">post</ta>
            <ta e="T279" id="Seg_3851" s="T278">propr</ta>
            <ta e="T280" id="Seg_3852" s="T279">n</ta>
            <ta e="T281" id="Seg_3853" s="T280">post</ta>
            <ta e="T282" id="Seg_3854" s="T281">n</ta>
            <ta e="T283" id="Seg_3855" s="T282">v</ta>
            <ta e="T284" id="Seg_3856" s="T283">v</ta>
            <ta e="T285" id="Seg_3857" s="T284">ptcl</ta>
            <ta e="T286" id="Seg_3858" s="T285">v</ta>
            <ta e="T287" id="Seg_3859" s="T286">n</ta>
            <ta e="T288" id="Seg_3860" s="T287">n</ta>
            <ta e="T289" id="Seg_3861" s="T288">n</ta>
            <ta e="T290" id="Seg_3862" s="T289">n</ta>
            <ta e="T291" id="Seg_3863" s="T290">adv</ta>
            <ta e="T292" id="Seg_3864" s="T291">adj</ta>
            <ta e="T293" id="Seg_3865" s="T292">n</ta>
            <ta e="T294" id="Seg_3866" s="T293">adj</ta>
            <ta e="T295" id="Seg_3867" s="T294">que</ta>
            <ta e="T296" id="Seg_3868" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_3869" s="T296">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T6" id="Seg_3870" s="T5">RUS:cult</ta>
            <ta e="T20" id="Seg_3871" s="T19">RUS:cult</ta>
            <ta e="T22" id="Seg_3872" s="T21">RUS:cult</ta>
            <ta e="T32" id="Seg_3873" s="T31">RUS:cult</ta>
            <ta e="T47" id="Seg_3874" s="T46">RUS:cult</ta>
            <ta e="T56" id="Seg_3875" s="T55">RUS:gram</ta>
            <ta e="T101" id="Seg_3876" s="T100">RUS:cult</ta>
            <ta e="T104" id="Seg_3877" s="T103">RUS:cult</ta>
            <ta e="T110" id="Seg_3878" s="T109">RUS:cult</ta>
            <ta e="T112" id="Seg_3879" s="T111">RUS:cult</ta>
            <ta e="T125" id="Seg_3880" s="T124">RUS:gram</ta>
            <ta e="T156" id="Seg_3881" s="T155">RUS:cult</ta>
            <ta e="T169" id="Seg_3882" s="T168">RUS:cult</ta>
            <ta e="T182" id="Seg_3883" s="T181">EV:gram (INTNS)</ta>
            <ta e="T187" id="Seg_3884" s="T186">RUS:cult</ta>
            <ta e="T205" id="Seg_3885" s="T204">RUS:cult</ta>
            <ta e="T214" id="Seg_3886" s="T213">RUS:cult</ta>
            <ta e="T219" id="Seg_3887" s="T218">RUS:cult</ta>
            <ta e="T223" id="Seg_3888" s="T222">RUS:gram</ta>
            <ta e="T233" id="Seg_3889" s="T232">RUS:cult</ta>
            <ta e="T248" id="Seg_3890" s="T247">RUS:cult</ta>
            <ta e="T253" id="Seg_3891" s="T252">RUS:mod</ta>
            <ta e="T263" id="Seg_3892" s="T262">RUS:cult</ta>
            <ta e="T276" id="Seg_3893" s="T275">RUS:cult</ta>
            <ta e="T280" id="Seg_3894" s="T279">EV:core</ta>
            <ta e="T288" id="Seg_3895" s="T287">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_3896" s="T0">Now listen to the next story.</ta>
            <ta e="T6" id="Seg_3897" s="T4">Its name is "The airplane".</ta>
            <ta e="T13" id="Seg_3898" s="T6">Toyoo saw on the third day again a miracle.</ta>
            <ta e="T21" id="Seg_3899" s="T13">By day, having finished eating, he ran around on the street.</ta>
            <ta e="T27" id="Seg_3900" s="T21">"An airplane comes flying", he heard the news.</ta>
            <ta e="T32" id="Seg_3901" s="T27">All people apparently are waiting for the airplane.</ta>
            <ta e="T40" id="Seg_3902" s="T32">In order to see new people coming, in order to receive letters from somewhere.</ta>
            <ta e="T47" id="Seg_3903" s="T40">Toyoo had never seen an airplane.</ta>
            <ta e="T51" id="Seg_3904" s="T47">How may this bird be?</ta>
            <ta e="T54" id="Seg_3905" s="T51">Who knows the stranger?</ta>
            <ta e="T59" id="Seg_3906" s="T54">Nevertheless he waits.</ta>
            <ta e="T69" id="Seg_3907" s="T59">It was not long, then a noise was sounding in the sky, as if it was thundering.</ta>
            <ta e="T78" id="Seg_3908" s="T69">Then a bird, not comparable to anything, flew over the clouds.</ta>
            <ta e="T92" id="Seg_3909" s="T78">Toyoo was frightenend and sat down at the place, where he was, making "splash", he covered his eyes and shivered, as if he was freezing.</ta>
            <ta e="T94" id="Seg_3910" s="T92">"What's up with you?</ta>
            <ta e="T106" id="Seg_3911" s="T94">Run quickly to the shore, look how the airplane will land in the water", he heard his teacher's voice.</ta>
            <ta e="T119" id="Seg_3912" s="T106">Toyoo opened his eyes, all people from Volochanka, who can and who not, ran past him.</ta>
            <ta e="T121" id="Seg_3913" s="T119">The people aren't afraid.</ta>
            <ta e="T131" id="Seg_3914" s="T121">"Nothing will happen to me", Toyoo thought.</ta>
            <ta e="T141" id="Seg_3915" s="T131">He jumped out with the last of his strength, he ran and jumped over small puddles.</ta>
            <ta e="T146" id="Seg_3916" s="T141">On the shore the people don't know each other.</ta>
            <ta e="T150" id="Seg_3917" s="T146">The laughter and chats become louder.</ta>
            <ta e="T160" id="Seg_3918" s="T150">On the river's water the airplane is swimming, as if a gull was sitting there, it is very loud.</ta>
            <ta e="T165" id="Seg_3919" s="T160">Behind it the water made waves because of the strong wind.</ta>
            <ta e="T168" id="Seg_3920" s="T165">Toyoo was a bit afraid.</ta>
            <ta e="T178" id="Seg_3921" s="T168">"The airplane will come out of the water onto the shore", he thought, "it will come towards me", he thought.</ta>
            <ta e="T183" id="Seg_3922" s="T178">He jumped far away from the standing people.</ta>
            <ta e="T186" id="Seg_3923" s="T183">He stood there completely alone.</ta>
            <ta e="T192" id="Seg_3924" s="T186">The airplane stopped like a small boat with its nose at the shore.</ta>
            <ta e="T194" id="Seg_3925" s="T192">The noise ceased.</ta>
            <ta e="T201" id="Seg_3926" s="T194">On the top a door opened, a human head was visible.</ta>
            <ta e="T217" id="Seg_3927" s="T201">The human jumped out, went onto the wing of the airplane, he grabbed the end the rope, which was thrown to him, and tied up the airplane like a reindeer.</ta>
            <ta e="T221" id="Seg_3928" s="T217">"Interesting, like a small boat.</ta>
            <ta e="T229" id="Seg_3929" s="T221">Like a reindeer it is tied up, apparently", Toyoo thought.</ta>
            <ta e="T235" id="Seg_3930" s="T229">But the children ran into each other and ran to the airplane.</ta>
            <ta e="T243" id="Seg_3931" s="T235">Toyoo stood there and remembered once more his home, his mother and his father.</ta>
            <ta e="T252" id="Seg_3932" s="T243">They don't see the airplane, though: how it is landing, how it is flying.</ta>
            <ta e="T263" id="Seg_3933" s="T252">It would be even more astonishing when this airplane would fly to his home.</ta>
            <ta e="T269" id="Seg_3934" s="T263">He wanted to see the whole tundra flying.</ta>
            <ta e="T275" id="Seg_3935" s="T269">The settlement, where he had lived, his tent, the placed, where he had played.</ta>
            <ta e="T287" id="Seg_3936" s="T275">Toyoo stood on the shore with his friends, until the airplane flew away, not even for eating they went to the boarding school.</ta>
            <ta e="T297" id="Seg_3937" s="T287">The chats about the airplane, outside and in every house, didn't stop. </ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_3938" s="T0">Jetzt hört die nächste Geschichte.</ta>
            <ta e="T6" id="Seg_3939" s="T4">Sie heißt "Das Flugzeug".</ta>
            <ta e="T13" id="Seg_3940" s="T6">Tojoo sah am dritten Tag wieder ein Wunder.</ta>
            <ta e="T21" id="Seg_3941" s="T13">Am Tage, nachdem er fertig war mit essen, lief er auf der Straße herum.</ta>
            <ta e="T27" id="Seg_3942" s="T21">"Ein Flugzeug kommt geflogen", hörte er die Nachricht.</ta>
            <ta e="T32" id="Seg_3943" s="T27">Alle Leute warten offenbar auf das Flugzeug.</ta>
            <ta e="T40" id="Seg_3944" s="T32">Um neue Menschen kommen zu sehen, um von irgendwoher Briefe zu bekommen.</ta>
            <ta e="T47" id="Seg_3945" s="T40">Tojoo hatte noch nie ein Flugzeug gesehen.</ta>
            <ta e="T51" id="Seg_3946" s="T47">Wie ist wohl dieser Vogel?</ta>
            <ta e="T54" id="Seg_3947" s="T51">Wer kennt den Fremden?</ta>
            <ta e="T59" id="Seg_3948" s="T54">Dennoch wartet er.</ta>
            <ta e="T69" id="Seg_3949" s="T59">Es war nicht lange, da ertönte am Himmel ein Geräusch, als ob es donnern würde. </ta>
            <ta e="T78" id="Seg_3950" s="T69">Dann flog ein mit nichts vergleichbarer Vogel über den Wolken entlang.</ta>
            <ta e="T92" id="Seg_3951" s="T78">Tojoo erschrak und setzte sich an dem Ort, wo er stand, mit einem Platsch hin, er bedeckte seine Augen und zitterte, als ob er fröre.</ta>
            <ta e="T94" id="Seg_3952" s="T92">"Was ist mit dir?</ta>
            <ta e="T106" id="Seg_3953" s="T94">Lauf schnell zum Ufer, schau, wie das Flugzeug im Wasser landet", hörte er die Stimme seines Lehrers.</ta>
            <ta e="T119" id="Seg_3954" s="T106">Tojoo öffnete die Augen, alle Leute aus Volochanka, wer kann und wer nicht, liefen an ihm vorbei.</ta>
            <ta e="T121" id="Seg_3955" s="T119">Die Leute haben keine Angst.</ta>
            <ta e="T131" id="Seg_3956" s="T121">"Mit mir wird auch nichts passieren", dachte Tojoo.</ta>
            <ta e="T141" id="Seg_3957" s="T131">Er sprang mit letzter Kraft auf, lief und sprang über kleine Pfützen hinweg.</ta>
            <ta e="T146" id="Seg_3958" s="T141">Am Ufer kennen sich die Leute nicht.</ta>
            <ta e="T150" id="Seg_3959" s="T146">Gelächter und Gespräche werden lauter. </ta>
            <ta e="T160" id="Seg_3960" s="T150">Im Wasser des Flusses schwimmt das Flugzeug, als säße dort eine Möwe, es ist sehr laut.</ta>
            <ta e="T165" id="Seg_3961" s="T160">Hinter ihm schlägt das Wasser vom starken Wind Wellen.</ta>
            <ta e="T168" id="Seg_3962" s="T165">Tojoo fürchtete sich etwas.</ta>
            <ta e="T178" id="Seg_3963" s="T168">"Das Flugzeug kommt aus dem Wasser aufs Ufer", dachte er, "es wird auf mich zukommen", dachte er.</ta>
            <ta e="T183" id="Seg_3964" s="T178">Von den stehenden Menschen sprang er sehr weit weg.</ta>
            <ta e="T186" id="Seg_3965" s="T183">Er stand ganz alleine da.</ta>
            <ta e="T192" id="Seg_3966" s="T186">Das Flugzeug hielt wie ein kleines Boot mit der Nase am Ufer an.</ta>
            <ta e="T194" id="Seg_3967" s="T192">Der Lärm hörte auf.</ta>
            <ta e="T201" id="Seg_3968" s="T194">Oben öffnete sich eine Tür, ein Menschenkopf war zu sehen.</ta>
            <ta e="T217" id="Seg_3969" s="T201">Der Mensch sprang hinaus, ging auf den Flügel des Flugzeugs, griff das Ende der ihm zugeworfenen Schnur und band das Flugzeug wie ein Rentier fest.</ta>
            <ta e="T221" id="Seg_3970" s="T217">"Interessant, wie ein kleines Boot.</ta>
            <ta e="T229" id="Seg_3971" s="T221">Wie ein Rentier wird es wohl festgebunden", dachte sich Tojoo.</ta>
            <ta e="T235" id="Seg_3972" s="T229">Die Kinder aber stießen einander und rannten zum Flugzeug.</ta>
            <ta e="T243" id="Seg_3973" s="T235">Tojoo stand dort und erinnerte sich wieder an sein Zuhause, seine Mutter und seinen Vater.</ta>
            <ta e="T252" id="Seg_3974" s="T243">Sie aber sehen das Flugzeug nicht: wie es landet, wie es fliegt.</ta>
            <ta e="T263" id="Seg_3975" s="T252">Noch erstaunlicher wäre es, wenn dieses Flugzeug nach Hause geflogen käme.</ta>
            <ta e="T269" id="Seg_3976" s="T263">Er wollte im Flug die ganze Tundra sehen.</ta>
            <ta e="T275" id="Seg_3977" s="T269">Die Siedlung, wo er gewohnt hatte, sein Zelt, die Orte, wo er gespielt hatte.</ta>
            <ta e="T287" id="Seg_3978" s="T275">Bis das Flugzeug wegflog, stand Tojoo mit seinen Freunden am Ufer, nicht einmal zum Essen gingen sie ins Internat.</ta>
            <ta e="T297" id="Seg_3979" s="T287">Die Gespräche über das Flugzeug draußen und in jedem Haus hören nicht auf.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_3980" s="T0">Сейчас послушайте следующий рассказ.</ta>
            <ta e="T6" id="Seg_3981" s="T4">Называется "Аэроплан" ("САМОЛЁТ").</ta>
            <ta e="T13" id="Seg_3982" s="T6">Тойоона на третий день опять увидел одну диковинку.</ta>
            <ta e="T21" id="Seg_3983" s="T13">Днём после обеда бегал прогуливаясь по улице.</ta>
            <ta e="T27" id="Seg_3984" s="T21">"Аэроплан (самолёт) прилетит," – весть услышал.</ta>
            <ta e="T32" id="Seg_3985" s="T27">Все люди ждут, оказывается, аэроплан.</ta>
            <ta e="T40" id="Seg_3986" s="T32">Новых людей прилетающих увидеть и откуда-нибудь письмо получить.</ta>
            <ta e="T47" id="Seg_3987" s="T40">Тойо ни разу не видел, оказывается, аэроплан.</ta>
            <ta e="T51" id="Seg_3988" s="T47">Как же он летает?</ta>
            <ta e="T54" id="Seg_3989" s="T51">Кто его знает?</ta>
            <ta e="T59" id="Seg_3990" s="T54">Несмотря на это все равно ждёт.</ta>
            <ta e="T69" id="Seg_3991" s="T59">Немного погодя, гром гремит будто в небе грохот раздался.</ta>
            <ta e="T78" id="Seg_3992" s="T69">Затем невиданная птица пролетела над облаками. </ta>
            <ta e="T92" id="Seg_3993" s="T78">Тойо испугался, где стоял там и плашмя сел, глаза закрыл, как замороженный задрожал сидя. </ta>
            <ta e="T94" id="Seg_3994" s="T92">"Что с тобой? </ta>
            <ta e="T106" id="Seg_3995" s="T94">Беги быстрее на берег, посмотри как самолёт садится в воду,"– учителя голос услышал.</ta>
            <ta e="T119" id="Seg_3996" s="T106">Тойо глаза открыл, Волочанки весь народ, кто может и не может, мимо него пробегают вот.</ta>
            <ta e="T121" id="Seg_3997" s="T119">Люди не боятся.</ta>
            <ta e="T131" id="Seg_3998" s="T121">"Со мной тоже ничего не будет наверно," – подумал про себя Тойо.</ta>
            <ta e="T141" id="Seg_3999" s="T131">Соскочив со всей прытью побежал перепрыгивая мелкиие лужи. </ta>
            <ta e="T146" id="Seg_4000" s="T141">На берегу реки люди друг друга не знают.</ta>
            <ta e="T150" id="Seg_4001" s="T146">Смех, разговоры усиливались.</ta>
            <ta e="T160" id="Seg_4002" s="T150">На реке, чайка сидит будто, аэроплан раскачивается с сильным гулом.</ta>
            <ta e="T165" id="Seg_4003" s="T160">Сзади вода волнами бьёт из-за сильного ветра.</ta>
            <ta e="T168" id="Seg_4004" s="T165">Тойо испугался немного.</ta>
            <ta e="T178" id="Seg_4005" s="T168">"Аэроплан из воды выплывая, на берег выйдет, – подумав – на меня пойдёт ведь" – сказав.</ta>
            <ta e="T183" id="Seg_4006" s="T178">От стоящих людей рядом подальше отпрянул.</ta>
            <ta e="T186" id="Seg_4007" s="T183">Отдельно один встал.</ta>
            <ta e="T192" id="Seg_4008" s="T186">Аэроплан словно ветка носом у берега пристал.</ta>
            <ta e="T194" id="Seg_4009" s="T192">Шум прекратился.</ta>
            <ta e="T201" id="Seg_4010" s="T194">С верху дверь открылась, человечья макушка появилась. </ta>
            <ta e="T217" id="Seg_4011" s="T201">Человек вспрыгнув аэроплану на крыло сверху забравшись, за выброшенную верёвку с конца схватив, самолёт будто оленя привезял. </ta>
            <ta e="T221" id="Seg_4012" s="T217">"Вот интересно, ветка будто.</ta>
            <ta e="T229" id="Seg_4013" s="T221">Словно оленя привязывают оказывается", – размышлял стоя Тойо.</ta>
            <ta e="T235" id="Seg_4014" s="T229">Дети тем временем, толкаясь к аэроплану ближе побежали.</ta>
            <ta e="T243" id="Seg_4015" s="T235">Тойо один стоя опять о доме, маме и папе вспомнил.</ta>
            <ta e="T252" id="Seg_4016" s="T243">Они только не видят аэроплан как садится, как взлетает.</ta>
            <ta e="T263" id="Seg_4017" s="T252">Ещё удивительнее было бы, если б домой прилетел на этом аэроплане.</ta>
            <ta e="T269" id="Seg_4018" s="T263">В полёте всю тундру охватить взором захотел.</ta>
            <ta e="T275" id="Seg_4019" s="T269">Родное стойбище, чум свой, места где играл. </ta>
            <ta e="T287" id="Seg_4020" s="T275">До самого вылета аэроплана Тойо с друзьями на берегу простояли, даже покушать так и не сходили в интернат.</ta>
            <ta e="T297" id="Seg_4021" s="T287">Об аэроплане разговоры на улице, в каждом доме никак не прекращаются. </ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
