<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDA5E9F2C0-9B8D-420D-7CF2-ED2C639A0A24">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>KuDP_2009_Hare_nar</transcription-name>
         <referenced-file url="KuDP_2009_Hare_nar.wav" />
         <referenced-file url="KuDP_2009_Hare_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\KuDP_2009_Hare_nar\KuDP_2009_Hare_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">325</ud-information>
            <ud-information attribute-name="# HIAT:w">282</ud-information>
            <ud-information attribute-name="# e">282</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">2</ud-information>
            <ud-information attribute-name="# HIAT:u">22</ud-information>
            <ud-information attribute-name="# sc">42</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="unknown">
            <abbreviation>unknown</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="2.95" type="appl" />
         <tli id="T2" time="3.3233333333333337" type="appl" />
         <tli id="T3" time="3.6966666666666668" type="appl" />
         <tli id="T4" time="4.07" type="appl" />
         <tli id="T5" time="4.443333333333333" type="appl" />
         <tli id="T6" time="4.816666666666666" type="appl" />
         <tli id="T7" time="5.19" type="appl" />
         <tli id="T8" time="5.193333376090598" />
         <tli id="T9" time="5.657" type="appl" />
         <tli id="T10" time="6.114" type="appl" />
         <tli id="T11" time="6.571" type="appl" />
         <tli id="T12" time="7.0280000000000005" type="appl" />
         <tli id="T13" time="7.484999999999999" type="appl" />
         <tli id="T14" time="7.942" type="appl" />
         <tli id="T15" time="8.399000000000001" type="appl" />
         <tli id="T16" time="8.856" type="appl" />
         <tli id="T17" time="9.312999999999999" type="appl" />
         <tli id="T18" time="9.77" type="appl" />
         <tli id="T19" time="9.773333578852066" />
         <tli id="T20" time="10.128823529411765" type="appl" />
         <tli id="T21" time="10.477647058823528" type="appl" />
         <tli id="T22" time="10.826470588235294" type="appl" />
         <tli id="T23" time="11.17529411764706" type="appl" />
         <tli id="T24" time="11.524117647058823" type="appl" />
         <tli id="T25" time="11.872941176470588" type="appl" />
         <tli id="T26" time="12.221764705882354" type="appl" />
         <tli id="T27" time="12.570588235294117" type="appl" />
         <tli id="T28" time="12.919411764705883" type="appl" />
         <tli id="T29" time="13.268235294117648" type="appl" />
         <tli id="T30" time="13.617058823529412" type="appl" />
         <tli id="T31" time="13.965882352941177" type="appl" />
         <tli id="T32" time="14.314705882352943" type="appl" />
         <tli id="T33" time="14.663529411764706" type="appl" />
         <tli id="T34" time="15.012352941176472" type="appl" />
         <tli id="T35" time="15.361176470588237" type="appl" />
         <tli id="T36" time="15.996657778907746" />
         <tli id="T37" time="15.999999151853094" />
         <tli id="T38" time="16.412857142857142" type="appl" />
         <tli id="T39" time="16.865714285714287" type="appl" />
         <tli id="T40" time="17.318571428571428" type="appl" />
         <tli id="T41" time="17.771428571428572" type="appl" />
         <tli id="T42" time="18.224285714285713" type="appl" />
         <tli id="T43" time="18.677142857142854" type="appl" />
         <tli id="T44" time="19.16999856370104" />
         <tli id="T45" time="19.170001818813503" />
         <tli id="T46" time="19.767142857142858" type="appl" />
         <tli id="T47" time="20.304285714285715" type="appl" />
         <tli id="T48" time="20.841428571428573" type="appl" />
         <tli id="T49" time="21.378571428571426" type="appl" />
         <tli id="T301" time="21.864714285714285" type="intp" />
         <tli id="T50" time="21.915714285714284" type="appl" />
         <tli id="T51" time="22.45285714285714" type="appl" />
         <tli id="T52" time="23.02333234243852" />
         <tli id="T53" time="23.023333969994752" />
         <tli id="T303" time="23.362000000000002" type="intp" />
         <tli id="T54" time="23.413" type="appl" />
         <tli id="T302" time="23.464" type="intp" />
         <tli id="T55" time="23.776" type="appl" />
         <tli id="T56" time="24.139" type="appl" />
         <tli id="T57" time="24.502" type="appl" />
         <tli id="T58" time="24.865000000000002" type="appl" />
         <tli id="T59" time="25.228" type="appl" />
         <tli id="T60" time="25.591" type="appl" />
         <tli id="T61" time="25.954" type="appl" />
         <tli id="T62" time="26.317" type="appl" />
         <tli id="T63" time="26.693333304476973" />
         <tli id="T64" time="26.76" type="appl" />
         <tli id="T65" time="27.382857142857144" type="appl" />
         <tli id="T66" time="28.005714285714287" type="appl" />
         <tli id="T67" time="28.62857142857143" type="appl" />
         <tli id="T68" time="29.251428571428573" type="appl" />
         <tli id="T69" time="29.874285714285715" type="appl" />
         <tli id="T70" time="30.497142857142858" type="appl" />
         <tli id="T71" time="30.680014392900528" />
         <tli id="T299" time="32.399045795906346" />
         <tli id="T72" time="32.972362244146666" />
         <tli id="T73" time="33.32568517155058" />
         <tli id="T74" time="33.59234398468561" />
         <tli id="T75" time="34.185659843911054" />
         <tli id="T76" time="34.372321013105584" />
         <tli id="T77" time="34.68564511853924" />
         <tli id="T78" time="34.92363636363636" type="appl" />
         <tli id="T79" time="35.46090909090909" type="appl" />
         <tli id="T80" time="35.99818181818182" type="appl" />
         <tli id="T81" time="36.53545454545454" type="appl" />
         <tli id="T82" time="37.07272727272727" type="appl" />
         <tli id="T83" time="37.450003807889125" />
         <tli id="T84" time="38.07887851568252" />
         <tli id="T85" time="38.470625" type="appl" />
         <tli id="T86" time="38.97125" type="appl" />
         <tli id="T87" time="39.471875" type="appl" />
         <tli id="T88" time="39.9725" type="appl" />
         <tli id="T89" time="40.473124999999996" type="appl" />
         <tli id="T90" time="40.973749999999995" type="appl" />
         <tli id="T91" time="41.474374999999995" type="appl" />
         <tli id="T92" time="41.974999999999994" type="appl" />
         <tli id="T93" time="42.475624999999994" type="appl" />
         <tli id="T94" time="42.97625" type="appl" />
         <tli id="T95" time="43.476875" type="appl" />
         <tli id="T96" time="43.9775" type="appl" />
         <tli id="T97" time="44.478125" type="appl" />
         <tli id="T98" time="44.97875" type="appl" />
         <tli id="T99" time="45.479375" type="appl" />
         <tli id="T100" time="45.98" type="appl" />
         <tli id="T101" time="46.37863407451038" />
         <tli id="T102" time="47.19" type="appl" />
         <tli id="T103" time="48.22" type="appl" />
         <tli id="T104" time="48.89001193330327" />
         <tli id="T105" time="49.5599954564175" />
         <tli id="T106" time="49.68333333333334" type="appl" />
         <tli id="T107" time="49.98666666666667" type="appl" />
         <tli id="T108" time="50.290000000000006" type="appl" />
         <tli id="T109" time="50.593333333333334" type="appl" />
         <tli id="T110" time="50.89666666666667" type="appl" />
         <tli id="T111" time="51.2" type="appl" />
         <tli id="T112" time="51.50333333333334" type="appl" />
         <tli id="T113" time="51.80666666666667" type="appl" />
         <tli id="T114" time="52.11" type="appl" />
         <tli id="T115" time="52.413333333333334" type="appl" />
         <tli id="T116" time="52.71666666666667" type="appl" />
         <tli id="T117" time="53.02" type="appl" />
         <tli id="T118" time="53.32333333333334" type="appl" />
         <tli id="T119" time="53.62666666666667" type="appl" />
         <tli id="T120" time="53.93000000000001" type="appl" />
         <tli id="T121" time="54.233333333333334" type="appl" />
         <tli id="T122" time="54.53666666666667" type="appl" />
         <tli id="T123" time="54.84" type="appl" />
         <tli id="T124" time="54.88" type="appl" />
         <tli id="T125" time="55.36913043478261" type="appl" />
         <tli id="T126" time="55.85826086956522" type="appl" />
         <tli id="T127" time="56.34739130434783" type="appl" />
         <tli id="T128" time="56.83652173913043" type="appl" />
         <tli id="T129" time="57.32565217391304" type="appl" />
         <tli id="T130" time="57.81478260869565" type="appl" />
         <tli id="T131" time="58.30391304347826" type="appl" />
         <tli id="T132" time="58.79304347826087" type="appl" />
         <tli id="T133" time="59.28217391304348" type="appl" />
         <tli id="T134" time="59.77130434782609" type="appl" />
         <tli id="T135" time="60.2604347826087" type="appl" />
         <tli id="T136" time="60.7495652173913" type="appl" />
         <tli id="T137" time="61.23869565217391" type="appl" />
         <tli id="T138" time="61.72782608695652" type="appl" />
         <tli id="T139" time="62.21695652173913" type="appl" />
         <tli id="T140" time="62.70608695652174" type="appl" />
         <tli id="T141" time="63.19521739130435" type="appl" />
         <tli id="T142" time="63.68434782608695" type="appl" />
         <tli id="T143" time="64.17347826086956" type="appl" />
         <tli id="T144" time="64.66260869565217" type="appl" />
         <tli id="T145" time="65.15173913043478" type="appl" />
         <tli id="T146" time="65.64086956521739" type="appl" />
         <tli id="T147" time="65.88333957742785" />
         <tli id="T148" time="66.48666165237078" />
         <tli id="T149" time="66.98" type="appl" />
         <tli id="T150" time="67.6" type="appl" />
         <tli id="T151" time="68.30000017371337" />
         <tli id="T152" time="68.30333340887756" />
         <tli id="T153" time="68.75" type="appl" />
         <tli id="T154" time="69.14999999999999" type="appl" />
         <tli id="T155" time="69.55" type="appl" />
         <tli id="T156" time="69.94999999999999" type="appl" />
         <tli id="T157" time="70.35" type="appl" />
         <tli id="T158" time="70.75" type="appl" />
         <tli id="T159" time="71.14999999999999" type="appl" />
         <tli id="T160" time="71.55" type="appl" />
         <tli id="T161" time="71.95" type="appl" />
         <tli id="T162" time="72.35" type="appl" />
         <tli id="T163" time="72.75" type="appl" />
         <tli id="T164" time="73.15" type="appl" />
         <tli id="T165" time="73.55" type="appl" />
         <tli id="T166" time="73.95" type="appl" />
         <tli id="T167" time="74.35" type="appl" />
         <tli id="T168" time="74.77666489425343" />
         <tli id="T169" time="74.7833313645818" />
         <tli id="T170" time="74.89779413930202" />
         <tli id="T300" time="75.01779060521278" />
         <tli id="T171" time="75.33111471064643" />
         <tli id="T172" time="76.22" type="appl" />
         <tli id="T173" time="76.70333333333333" type="appl" />
         <tli id="T174" time="77.18666666666667" type="appl" />
         <tli id="T175" time="77.67" type="appl" />
         <tli id="T176" time="78.15333333333334" type="appl" />
         <tli id="T177" time="78.63666666666667" type="appl" />
         <tli id="T178" time="79.12" type="appl" />
         <tli id="T179" time="79.60333333333332" type="appl" />
         <tli id="T180" time="80.08666666666667" type="appl" />
         <tli id="T181" time="80.57" type="appl" />
         <tli id="T182" time="81.05333333333333" type="appl" />
         <tli id="T183" time="81.53666666666666" type="appl" />
         <tli id="T184" time="82.02" type="appl" />
         <tli id="T185" time="82.50333333333333" type="appl" />
         <tli id="T186" time="82.98666666666666" type="appl" />
         <tli id="T187" time="83.47" type="appl" />
         <tli id="T188" time="83.95333333333333" type="appl" />
         <tli id="T189" time="84.43666666666667" type="appl" />
         <tli id="T190" time="84.92" type="appl" />
         <tli id="T191" time="85.38666666666667" type="appl" />
         <tli id="T192" time="85.85333333333334" type="appl" />
         <tli id="T193" time="86.32000000000001" type="appl" />
         <tli id="T194" time="86.78666666666666" type="appl" />
         <tli id="T195" time="87.25333333333333" type="appl" />
         <tli id="T196" time="87.72" type="appl" />
         <tli id="T197" time="88.18666666666667" type="appl" />
         <tli id="T198" time="88.65333333333334" type="appl" />
         <tli id="T199" time="89.12" type="appl" />
         <tli id="T200" time="89.58666666666667" type="appl" />
         <tli id="T201" time="90.05333333333333" type="appl" />
         <tli id="T202" time="90.52" type="appl" />
         <tli id="T203" time="90.98666666666666" type="appl" />
         <tli id="T204" time="91.45333333333333" type="appl" />
         <tli id="T205" time="91.92" type="appl" />
         <tli id="T206" time="92.38666666666667" type="appl" />
         <tli id="T207" time="92.85333333333334" type="appl" />
         <tli id="T208" time="93.32" type="appl" />
         <tli id="T209" time="93.78666666666666" type="appl" />
         <tli id="T210" time="94.25333333333333" type="appl" />
         <tli id="T211" time="94.45333798869828" />
         <tli id="T212" time="95.11232899701811" />
         <tli id="T213" time="95.43705" type="appl" />
         <tli id="T214" time="95.8351" type="appl" />
         <tli id="T215" time="96.23315" type="appl" />
         <tli id="T216" time="96.6312" type="appl" />
         <tli id="T217" time="97.02925" type="appl" />
         <tli id="T218" time="97.4273" type="appl" />
         <tli id="T219" time="97.82535" type="appl" />
         <tli id="T220" time="98.2234" type="appl" />
         <tli id="T221" time="98.62145" type="appl" />
         <tli id="T222" time="99.0195" type="appl" />
         <tli id="T223" time="99.41755" type="appl" />
         <tli id="T224" time="99.8156" type="appl" />
         <tli id="T225" time="100.21365" type="appl" />
         <tli id="T226" time="100.6117" type="appl" />
         <tli id="T227" time="101.00975" type="appl" />
         <tli id="T228" time="101.4078" type="appl" />
         <tli id="T229" time="101.80584999999999" type="appl" />
         <tli id="T230" time="102.2039" type="appl" />
         <tli id="T231" time="102.60195" type="appl" />
         <tli id="T232" time="103.03999916019363" />
         <tli id="T233" time="103.11" type="appl" />
         <tli id="T234" time="103.648" type="appl" />
         <tli id="T235" time="104.18599999999999" type="appl" />
         <tli id="T236" time="104.724" type="appl" />
         <tli id="T237" time="105.262" type="appl" />
         <tli id="T238" time="105.8" type="appl" />
         <tli id="T239" time="105.80666897939368" />
         <tli id="T240" time="106.27705882352942" type="appl" />
         <tli id="T241" time="106.65411764705883" type="appl" />
         <tli id="T242" time="107.03117647058824" type="appl" />
         <tli id="T243" time="107.40823529411765" type="appl" />
         <tli id="T244" time="107.78529411764707" type="appl" />
         <tli id="T245" time="108.16235294117648" type="appl" />
         <tli id="T246" time="108.53941176470589" type="appl" />
         <tli id="T247" time="108.9164705882353" type="appl" />
         <tli id="T248" time="109.29352941176471" type="appl" />
         <tli id="T249" time="109.67058823529412" type="appl" />
         <tli id="T250" time="110.04764705882353" type="appl" />
         <tli id="T251" time="110.42470588235294" type="appl" />
         <tli id="T252" time="110.80176470588236" type="appl" />
         <tli id="T253" time="111.17882352941177" type="appl" />
         <tli id="T254" time="111.55588235294118" type="appl" />
         <tli id="T255" time="111.93294117647059" type="appl" />
         <tli id="T256" time="112.13667135270947" />
         <tli id="T257" time="113.07666970991019" />
         <tli id="T258" time="113.08333618023856" />
         <tli id="T259" time="113.46777777777777" type="appl" />
         <tli id="T260" time="113.95666666666666" type="appl" />
         <tli id="T261" time="114.44555555555556" type="appl" />
         <tli id="T262" time="114.93444444444444" type="appl" />
         <tli id="T263" time="115.42333333333333" type="appl" />
         <tli id="T264" time="115.91222222222223" type="appl" />
         <tli id="T265" time="116.4011111111111" type="appl" />
         <tli id="T266" time="116.68323015756148" />
         <tli id="T267" time="117.88319481666913" />
         <tli id="T268" time="117.98210526315789" type="appl" />
         <tli id="T269" time="118.44421052631579" type="appl" />
         <tli id="T270" time="118.90631578947368" type="appl" />
         <tli id="T271" time="119.36842105263158" type="appl" />
         <tli id="T272" time="119.83052631578947" type="appl" />
         <tli id="T273" time="120.29263157894736" type="appl" />
         <tli id="T274" time="120.75473684210526" type="appl" />
         <tli id="T275" time="121.21684210526315" type="appl" />
         <tli id="T276" time="121.67894736842105" type="appl" />
         <tli id="T277" time="122.14105263157894" type="appl" />
         <tli id="T278" time="122.60315789473684" type="appl" />
         <tli id="T279" time="123.06526315789473" type="appl" />
         <tli id="T280" time="123.52736842105263" type="appl" />
         <tli id="T281" time="123.98947368421052" type="appl" />
         <tli id="T282" time="124.45157894736842" type="appl" />
         <tli id="T283" time="124.91368421052631" type="appl" />
         <tli id="T284" time="125.37578947368421" type="appl" />
         <tli id="T285" time="125.8378947368421" type="appl" />
         <tli id="T286" time="126.21334010547048" />
         <tli id="T287" time="126.43333362630688" />
         <tli id="T288" time="127.09636363636363" type="appl" />
         <tli id="T289" time="127.71272727272728" type="appl" />
         <tli id="T290" time="128.32909090909092" type="appl" />
         <tli id="T291" time="128.94545454545454" type="appl" />
         <tli id="T292" time="129.56181818181818" type="appl" />
         <tli id="T293" time="130.17818181818183" type="appl" />
         <tli id="T294" time="130.79454545454544" type="appl" />
         <tli id="T295" time="131.4109090909091" type="appl" />
         <tli id="T296" time="132.02727272727273" type="appl" />
         <tli id="T297" time="132.64363636363635" type="appl" />
         <tli id="T298" time="132.77608953026063" />
         <tli id="T0" time="135.816" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="unknown"
                      type="t">
         <timeline-fork end="T72" start="T299">
            <tli id="T299.tx.1" />
            <tli id="T299.tx.2" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T7" id="Seg_0" n="sc" s="T1">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Biːrde</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">bihigi</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">Reginanɨ</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">gɨtta</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">hɨldʼar</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_19" n="HIAT:w" s="T6">etibit</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T18" id="Seg_22" n="sc" s="T8">
               <ts e="T18" id="Seg_24" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_26" n="HIAT:w" s="T8">Onton</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_29" n="HIAT:w" s="T9">onno</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_32" n="HIAT:w" s="T10">tʼotʼa</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_35" n="HIAT:w" s="T11">eː</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_38" n="HIAT:w" s="T12">bihigi</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_41" n="HIAT:w" s="T13">hɨldʼar</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_44" n="HIAT:w" s="T14">etibit</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_47" n="HIAT:w" s="T15">ogonu</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_50" n="HIAT:w" s="T16">karajar</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_53" n="HIAT:w" s="T17">etibit</ts>
                  <nts id="Seg_54" n="HIAT:ip">.</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T36" id="Seg_56" n="sc" s="T19">
               <ts e="T36" id="Seg_58" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_60" n="HIAT:w" s="T19">Onton</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_63" n="HIAT:w" s="T20">Kira</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_66" n="HIAT:w" s="T21">maːmata</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_69" n="HIAT:w" s="T22">ɨgɨrbɨta</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_72" n="HIAT:w" s="T23">minigin</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_75" n="HIAT:w" s="T24">katatsalɨː</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_78" n="HIAT:w" s="T25">baragɨt</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_81" n="HIAT:w" s="T26">du͡o</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_84" n="HIAT:w" s="T27">tɨ͡aga</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_87" n="HIAT:w" s="T28">bihigi</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_90" n="HIAT:w" s="T29">eː</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_93" n="HIAT:w" s="T30">katatsalɨː</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_96" n="HIAT:w" s="T31">baran</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_99" n="HIAT:w" s="T32">du</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_102" n="HIAT:w" s="T33">minigin</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_105" n="HIAT:w" s="T34">gɨtta</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_108" n="HIAT:w" s="T35">tɨ͡aga</ts>
                  <nts id="Seg_109" n="HIAT:ip">.</nts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T44" id="Seg_111" n="sc" s="T37">
               <ts e="T44" id="Seg_113" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_115" n="HIAT:w" s="T37">Onton</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_118" n="HIAT:w" s="T38">min</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_121" n="HIAT:w" s="T39">di͡ebitim</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_124" n="HIAT:w" s="T40">heː</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_127" n="HIAT:w" s="T41">onton</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_130" n="HIAT:w" s="T42">bihigi</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_133" n="HIAT:w" s="T43">bartibɨt</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T52" id="Seg_136" n="sc" s="T45">
               <ts e="T52" id="Seg_138" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_140" n="HIAT:w" s="T45">A</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_143" n="HIAT:w" s="T46">Polina</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_146" n="HIAT:w" s="T47">Alekseevnalaːk</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_149" n="HIAT:w" s="T48">bejelerin</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_152" n="HIAT:w" s="T49">semjalarɨn</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_155" n="HIAT:w" s="T301">kɨtta</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_158" n="HIAT:w" s="T50">kaːlbɨt</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_161" n="HIAT:w" s="T51">etilere</ts>
                  <nts id="Seg_162" n="HIAT:ip">.</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T63" id="Seg_164" n="sc" s="T53">
               <ts e="T63" id="Seg_166" n="HIAT:u" s="T53">
                  <ts e="T303" id="Seg_168" n="HIAT:w" s="T53">A</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_170" n="HIAT:ip">(</nts>
                  <ts e="T54" id="Seg_172" n="HIAT:w" s="T303">ti-</ts>
                  <nts id="Seg_173" n="HIAT:ip">)</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_176" n="HIAT:w" s="T54">kelbit</ts>
                  <nts id="Seg_177" n="HIAT:ip">,</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_180" n="HIAT:w" s="T302">keler</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_183" n="HIAT:w" s="T55">turar</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_186" n="HIAT:w" s="T56">etilere</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_189" n="HIAT:w" s="T57">a</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_192" n="HIAT:w" s="T58">tʼotʼa</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_195" n="HIAT:w" s="T59">Nastʼa</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_198" n="HIAT:w" s="T60">ginilerin</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_201" n="HIAT:w" s="T61">ɨllarar</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_204" n="HIAT:w" s="T62">ete</ts>
                  <nts id="Seg_205" n="HIAT:ip">.</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T71" id="Seg_207" n="sc" s="T64">
               <ts e="T71" id="Seg_209" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_211" n="HIAT:w" s="T64">Anɨ</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_214" n="HIAT:w" s="T65">kimneri</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_217" n="HIAT:w" s="T66">tu͡ok</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_220" n="HIAT:w" s="T67">di͡eččiler</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_223" n="HIAT:w" s="T68">burančik</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_226" n="HIAT:w" s="T69">burančiktaːktar</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_229" n="HIAT:w" s="T70">tu͡okta</ts>
                  <nts id="Seg_230" n="HIAT:ip">.</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T83" id="Seg_232" n="sc" s="T299">
               <ts e="T83" id="Seg_234" n="HIAT:u" s="T299">
                  <nts id="Seg_235" n="HIAT:ip">(</nts>
                  <ts e="T299.tx.1" id="Seg_237" n="HIAT:w" s="T299">Onton</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299.tx.2" id="Seg_240" n="HIAT:w" s="T299.tx.1">gi-</ts>
                  <nts id="Seg_241" n="HIAT:ip">)</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_243" n="HIAT:ip">(</nts>
                  <nts id="Seg_244" n="HIAT:ip">(</nts>
                  <ats e="T72" id="Seg_245" n="HIAT:non-pho" s="T299.tx.2">NOISE</ats>
                  <nts id="Seg_246" n="HIAT:ip">)</nts>
                  <nts id="Seg_247" n="HIAT:ip">)</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_250" n="HIAT:w" s="T72">onton</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_253" n="HIAT:w" s="T73">giniler</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_256" n="HIAT:w" s="T74">kelbittere</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_259" n="HIAT:w" s="T75">Polina</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_262" n="HIAT:w" s="T76">Alekseevnalaːk</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_265" n="HIAT:w" s="T77">onton</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_268" n="HIAT:w" s="T78">bihigi</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_271" n="HIAT:w" s="T79">onno</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_274" n="HIAT:w" s="T80">že</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_277" n="HIAT:w" s="T81">dalše</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_280" n="HIAT:w" s="T82">barbɨppɨt</ts>
                  <nts id="Seg_281" n="HIAT:ip">.</nts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T100" id="Seg_283" n="sc" s="T84">
               <ts e="T100" id="Seg_285" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_287" n="HIAT:w" s="T84">Onton</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_290" n="HIAT:w" s="T85">onno</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_293" n="HIAT:w" s="T86">kim</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_296" n="HIAT:w" s="T87">kimneri</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_299" n="HIAT:w" s="T88">bu</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_302" n="HIAT:w" s="T89">ke</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_305" n="HIAT:w" s="T90">toktoːbupput</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_308" n="HIAT:w" s="T91">onton</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_311" n="HIAT:w" s="T92">onno</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_314" n="HIAT:w" s="T93">kim</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_316" n="HIAT:ip">(</nts>
                  <ts e="T95" id="Seg_318" n="HIAT:w" s="T94">kur-</ts>
                  <nts id="Seg_319" n="HIAT:ip">)</nts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_322" n="HIAT:w" s="T95">zaːjčik</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_325" n="HIAT:w" s="T96">kimin</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_328" n="HIAT:w" s="T97">üːtün</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_331" n="HIAT:w" s="T98">körbüt</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_334" n="HIAT:w" s="T99">etibit</ts>
                  <nts id="Seg_335" n="HIAT:ip">.</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T104" id="Seg_337" n="sc" s="T101">
               <ts e="T104" id="Seg_339" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_341" n="HIAT:w" s="T101">Onton</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_343" n="HIAT:ip">(</nts>
                  <ts e="T103" id="Seg_345" n="HIAT:w" s="T102">katatsalaː-</ts>
                  <nts id="Seg_346" n="HIAT:ip">)</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_349" n="HIAT:w" s="T103">katatsalaːbɨppɨt</ts>
                  <nts id="Seg_350" n="HIAT:ip">.</nts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T123" id="Seg_352" n="sc" s="T105">
               <ts e="T123" id="Seg_354" n="HIAT:u" s="T105">
                  <ts e="T106" id="Seg_356" n="HIAT:w" s="T105">Ulakan</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_359" n="HIAT:w" s="T106">bagajɨ</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_362" n="HIAT:w" s="T107">gorka</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_365" n="HIAT:w" s="T108">ete</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_368" n="HIAT:w" s="T109">onno</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_371" n="HIAT:w" s="T110">bihigi</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_374" n="HIAT:w" s="T111">že</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_377" n="HIAT:w" s="T112">zamečajdaːbat</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_380" n="HIAT:w" s="T113">etibit</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_383" n="HIAT:w" s="T114">onno</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_386" n="HIAT:w" s="T115">voobše</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_389" n="HIAT:w" s="T116">beloe</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_392" n="HIAT:w" s="T117">ete</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_395" n="HIAT:w" s="T118">tu͡ok</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_398" n="HIAT:w" s="T119">da</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_401" n="HIAT:w" s="T120">hu͡ok</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_404" n="HIAT:w" s="T121">ete</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_407" n="HIAT:w" s="T122">köstübet</ts>
                  <nts id="Seg_408" n="HIAT:ip">.</nts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T147" id="Seg_410" n="sc" s="T124">
               <ts e="T147" id="Seg_412" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_414" n="HIAT:w" s="T124">Onton</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_417" n="HIAT:w" s="T125">katatsalaːbɨppɨt</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_420" n="HIAT:w" s="T126">onton</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_423" n="HIAT:w" s="T127">onno</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_426" n="HIAT:w" s="T128">kim</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_429" n="HIAT:w" s="T129">Polina</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_432" n="HIAT:w" s="T130">Alekseevna</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_435" n="HIAT:w" s="T131">baːr</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_438" n="HIAT:w" s="T132">ete</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_441" n="HIAT:w" s="T133">semjatɨn</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_444" n="HIAT:w" s="T134">gɨtta</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_447" n="HIAT:w" s="T135">onton</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_450" n="HIAT:w" s="T136">tʼotʼa</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_453" n="HIAT:w" s="T137">Nastʼa</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_456" n="HIAT:w" s="T138">onton</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_459" n="HIAT:w" s="T139">Nʼukuːska</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_462" n="HIAT:w" s="T140">Ludmila</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_465" n="HIAT:w" s="T141">Nikolaevna</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_468" n="HIAT:w" s="T142">ogoto</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_471" n="HIAT:w" s="T143">onton</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_474" n="HIAT:w" s="T144">Annuška</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_477" n="HIAT:w" s="T145">tʼotʼa</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_480" n="HIAT:w" s="T146">Nastʼa</ts>
                  <nts id="Seg_481" n="HIAT:ip">.</nts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T151" id="Seg_483" n="sc" s="T148">
               <ts e="T151" id="Seg_485" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_487" n="HIAT:w" s="T148">Onton</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_490" n="HIAT:w" s="T149">bihigi</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_493" n="HIAT:w" s="T150">katatsalaːbɨppɨt</ts>
                  <nts id="Seg_494" n="HIAT:ip">.</nts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T168" id="Seg_496" n="sc" s="T152">
               <ts e="T168" id="Seg_498" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_500" n="HIAT:w" s="T152">Onton</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_503" n="HIAT:w" s="T153">kim</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_506" n="HIAT:w" s="T154">ete</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_509" n="HIAT:w" s="T155">kim</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_512" n="HIAT:w" s="T156">ete</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_515" n="HIAT:w" s="T157">eː</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_518" n="HIAT:w" s="T158">dʼadʼa</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_521" n="HIAT:w" s="T159">Parako</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_524" n="HIAT:w" s="T160">ogolorgo</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_527" n="HIAT:w" s="T161">kim</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_530" n="HIAT:w" s="T162">oŋoror</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_533" n="HIAT:w" s="T163">ete</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_536" n="HIAT:w" s="T164">kim</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_539" n="HIAT:w" s="T165">ete</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_542" n="HIAT:w" s="T166">ke</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_545" n="HIAT:w" s="T167">figurkalarɨ</ts>
                  <nts id="Seg_546" n="HIAT:ip">.</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T211" id="Seg_548" n="sc" s="T169">
               <ts e="T190" id="Seg_550" n="HIAT:u" s="T169">
                  <ts e="T170" id="Seg_552" n="HIAT:w" s="T169">Onton</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_555" n="HIAT:w" s="T170">ke</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_557" n="HIAT:ip">(</nts>
                  <nts id="Seg_558" n="HIAT:ip">(</nts>
                  <ats e="T171" id="Seg_559" n="HIAT:non-pho" s="T300">NOISE</ats>
                  <nts id="Seg_560" n="HIAT:ip">)</nts>
                  <nts id="Seg_561" n="HIAT:ip">)</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_564" n="HIAT:w" s="T171">eː</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_567" n="HIAT:w" s="T172">Kira</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_570" n="HIAT:w" s="T173">kime</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_573" n="HIAT:w" s="T174">figurkata</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_576" n="HIAT:w" s="T175">zaːjčik</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_579" n="HIAT:w" s="T176">ete</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_582" n="HIAT:w" s="T177">a</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_585" n="HIAT:w" s="T178">kim</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_588" n="HIAT:w" s="T179">gi͡ene</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_591" n="HIAT:w" s="T180">kim</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_594" n="HIAT:w" s="T181">ete</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_597" n="HIAT:w" s="T182">kim</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_600" n="HIAT:w" s="T183">gi͡ene</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_603" n="HIAT:w" s="T184">Nadjex</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_606" n="HIAT:w" s="T185">gi͡ene</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_609" n="HIAT:w" s="T186">kim</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_612" n="HIAT:w" s="T187">meːne</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_615" n="HIAT:w" s="T188">kurpaːska</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_618" n="HIAT:w" s="T189">ete</ts>
                  <nts id="Seg_619" n="HIAT:ip">.</nts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T211" id="Seg_622" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_624" n="HIAT:w" s="T190">Onton</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_627" n="HIAT:w" s="T191">uːrbut</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_630" n="HIAT:w" s="T192">etilere</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_633" n="HIAT:w" s="T193">onno</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_636" n="HIAT:w" s="T194">ikki͡ettere</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_639" n="HIAT:w" s="T195">a</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_642" n="HIAT:w" s="T196">kurpaːskanɨ</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_645" n="HIAT:w" s="T197">kimi͡eke</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_648" n="HIAT:w" s="T198">uːrbut</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_651" n="HIAT:w" s="T199">etilere</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_654" n="HIAT:w" s="T200">kim</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_657" n="HIAT:w" s="T201">etej</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_660" n="HIAT:w" s="T202">ke</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_663" n="HIAT:w" s="T203">zaːjčik</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_666" n="HIAT:w" s="T204">kimiger</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_669" n="HIAT:w" s="T205">onton</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_672" n="HIAT:w" s="T206">iti</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_675" n="HIAT:w" s="T207">kim</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_678" n="HIAT:w" s="T208">nu</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_681" n="HIAT:w" s="T209">ka</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_684" n="HIAT:w" s="T210">jamkatɨgar</ts>
                  <nts id="Seg_685" n="HIAT:ip">.</nts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T232" id="Seg_687" n="sc" s="T212">
               <ts e="T232" id="Seg_689" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_691" n="HIAT:w" s="T212">Onton</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_694" n="HIAT:w" s="T213">onno</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_697" n="HIAT:w" s="T214">bihigi</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_700" n="HIAT:w" s="T215">onton</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_703" n="HIAT:w" s="T216">onno</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_705" n="HIAT:ip">(</nts>
                  <ts e="T218" id="Seg_707" n="HIAT:w" s="T217">ikk-</ts>
                  <nts id="Seg_708" n="HIAT:ip">)</nts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_711" n="HIAT:w" s="T218">onton</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_714" n="HIAT:w" s="T219">kojut</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_717" n="HIAT:w" s="T220">kas</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_720" n="HIAT:w" s="T221">biːr</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_723" n="HIAT:w" s="T222">kas</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_726" n="HIAT:w" s="T223">da</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_729" n="HIAT:w" s="T224">nedelʼa</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_732" n="HIAT:w" s="T225">aːspɨta</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_735" n="HIAT:w" s="T226">össü͡ö</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_738" n="HIAT:w" s="T227">ikkisterin</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_741" n="HIAT:w" s="T228">po</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_744" n="HIAT:w" s="T229">vtoromu</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_747" n="HIAT:w" s="T230">razu</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_750" n="HIAT:w" s="T231">barbɨppɨt</ts>
                  <nts id="Seg_751" n="HIAT:ip">.</nts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T238" id="Seg_753" n="sc" s="T233">
               <ts e="T238" id="Seg_755" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_757" n="HIAT:w" s="T233">Onton</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_760" n="HIAT:w" s="T234">onno</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_763" n="HIAT:w" s="T235">barbɨppɨt</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_766" n="HIAT:w" s="T236">hol</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_769" n="HIAT:w" s="T237">mestobɨtɨgar</ts>
                  <nts id="Seg_770" n="HIAT:ip">.</nts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T256" id="Seg_772" n="sc" s="T239">
               <ts e="T256" id="Seg_774" n="HIAT:u" s="T239">
                  <ts e="T240" id="Seg_776" n="HIAT:w" s="T239">Ol</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_779" n="HIAT:w" s="T240">mestoga</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_782" n="HIAT:w" s="T241">onno</ts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_785" n="HIAT:w" s="T242">kim</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_788" n="HIAT:w" s="T243">baːr</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_791" n="HIAT:w" s="T244">tot</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_794" n="HIAT:w" s="T245">že</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_797" n="HIAT:w" s="T246">zaːjčik</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_800" n="HIAT:w" s="T247">baːr</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_803" n="HIAT:w" s="T248">ete</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_806" n="HIAT:w" s="T249">bu͡o</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_809" n="HIAT:w" s="T250">ol</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_812" n="HIAT:w" s="T251">zaːjčik</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_815" n="HIAT:w" s="T252">iːktillibit</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_818" n="HIAT:w" s="T253">ete</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_821" n="HIAT:w" s="T254">iːkteːk</ts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_824" n="HIAT:w" s="T255">ete</ts>
                  <nts id="Seg_825" n="HIAT:ip">.</nts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T266" id="Seg_827" n="sc" s="T257">
               <ts e="T266" id="Seg_829" n="HIAT:u" s="T257">
                  <ts e="T258" id="Seg_831" n="HIAT:w" s="T257">A</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_834" n="HIAT:w" s="T258">kim</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_837" n="HIAT:w" s="T259">da</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_840" n="HIAT:w" s="T260">kurpaːskɨ</ts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_843" n="HIAT:w" s="T261">menʼiːtin</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_846" n="HIAT:w" s="T262">kim</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_849" n="HIAT:w" s="T263">ere</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_852" n="HIAT:w" s="T264">turbut</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_855" n="HIAT:w" s="T265">etibit</ts>
                  <nts id="Seg_856" n="HIAT:ip">.</nts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T286" id="Seg_858" n="sc" s="T267">
               <ts e="T286" id="Seg_860" n="HIAT:u" s="T267">
                  <ts e="T268" id="Seg_862" n="HIAT:w" s="T267">Onton</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_865" n="HIAT:w" s="T268">kojut</ts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_868" n="HIAT:w" s="T269">bihigi</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_871" n="HIAT:w" s="T270">katatsalɨː</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_874" n="HIAT:w" s="T271">barbɨppɨt</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_877" n="HIAT:w" s="T272">a</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_880" n="HIAT:w" s="T273">kim</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_883" n="HIAT:w" s="T274">de</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_886" n="HIAT:w" s="T275">eː</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_889" n="HIAT:w" s="T276">dʼadʼa</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_892" n="HIAT:w" s="T277">Prokula</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_895" n="HIAT:w" s="T278">ogoloru</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_898" n="HIAT:w" s="T279">ɨlpɨt</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_901" n="HIAT:w" s="T280">etilere</ts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_904" n="HIAT:w" s="T281">eː</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_907" n="HIAT:w" s="T282">hu͡ok</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_910" n="HIAT:w" s="T283">ol</ts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_913" n="HIAT:w" s="T284">atɨn</ts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_916" n="HIAT:w" s="T285">kimi͡eke</ts>
                  <nts id="Seg_917" n="HIAT:ip">.</nts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T298" id="Seg_919" n="sc" s="T287">
               <ts e="T298" id="Seg_921" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_923" n="HIAT:w" s="T287">Bihigi</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_926" n="HIAT:w" s="T288">katatsalɨː</ts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_929" n="HIAT:w" s="T289">barbɨppɨt</ts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_932" n="HIAT:w" s="T290">onton</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_935" n="HIAT:w" s="T291">töttörü</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_938" n="HIAT:w" s="T292">keltibit</ts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_941" n="HIAT:w" s="T293">kimŋe</ts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_944" n="HIAT:w" s="T294">poselokka</ts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_947" n="HIAT:w" s="T295">lɨːžalardaːk</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_950" n="HIAT:w" s="T296">onton</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_953" n="HIAT:w" s="T297">keltibit</ts>
                  <nts id="Seg_954" n="HIAT:ip">.</nts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T7" id="Seg_956" n="sc" s="T1">
               <ts e="T2" id="Seg_958" n="e" s="T1">Biːrde </ts>
               <ts e="T3" id="Seg_960" n="e" s="T2">bihigi </ts>
               <ts e="T4" id="Seg_962" n="e" s="T3">Reginanɨ </ts>
               <ts e="T5" id="Seg_964" n="e" s="T4">gɨtta </ts>
               <ts e="T6" id="Seg_966" n="e" s="T5">hɨldʼar </ts>
               <ts e="T7" id="Seg_968" n="e" s="T6">etibit. </ts>
            </ts>
            <ts e="T18" id="Seg_969" n="sc" s="T8">
               <ts e="T9" id="Seg_971" n="e" s="T8">Onton </ts>
               <ts e="T10" id="Seg_973" n="e" s="T9">onno </ts>
               <ts e="T11" id="Seg_975" n="e" s="T10">tʼotʼa </ts>
               <ts e="T12" id="Seg_977" n="e" s="T11">eː </ts>
               <ts e="T13" id="Seg_979" n="e" s="T12">bihigi </ts>
               <ts e="T14" id="Seg_981" n="e" s="T13">hɨldʼar </ts>
               <ts e="T15" id="Seg_983" n="e" s="T14">etibit </ts>
               <ts e="T16" id="Seg_985" n="e" s="T15">ogonu </ts>
               <ts e="T17" id="Seg_987" n="e" s="T16">karajar </ts>
               <ts e="T18" id="Seg_989" n="e" s="T17">etibit. </ts>
            </ts>
            <ts e="T36" id="Seg_990" n="sc" s="T19">
               <ts e="T20" id="Seg_992" n="e" s="T19">Onton </ts>
               <ts e="T21" id="Seg_994" n="e" s="T20">Kira </ts>
               <ts e="T22" id="Seg_996" n="e" s="T21">maːmata </ts>
               <ts e="T23" id="Seg_998" n="e" s="T22">ɨgɨrbɨta </ts>
               <ts e="T24" id="Seg_1000" n="e" s="T23">minigin </ts>
               <ts e="T25" id="Seg_1002" n="e" s="T24">katatsalɨː </ts>
               <ts e="T26" id="Seg_1004" n="e" s="T25">baragɨt </ts>
               <ts e="T27" id="Seg_1006" n="e" s="T26">du͡o </ts>
               <ts e="T28" id="Seg_1008" n="e" s="T27">tɨ͡aga </ts>
               <ts e="T29" id="Seg_1010" n="e" s="T28">bihigi </ts>
               <ts e="T30" id="Seg_1012" n="e" s="T29">eː </ts>
               <ts e="T31" id="Seg_1014" n="e" s="T30">katatsalɨː </ts>
               <ts e="T32" id="Seg_1016" n="e" s="T31">baran </ts>
               <ts e="T33" id="Seg_1018" n="e" s="T32">du </ts>
               <ts e="T34" id="Seg_1020" n="e" s="T33">minigin </ts>
               <ts e="T35" id="Seg_1022" n="e" s="T34">gɨtta </ts>
               <ts e="T36" id="Seg_1024" n="e" s="T35">tɨ͡aga. </ts>
            </ts>
            <ts e="T44" id="Seg_1025" n="sc" s="T37">
               <ts e="T38" id="Seg_1027" n="e" s="T37">Onton </ts>
               <ts e="T39" id="Seg_1029" n="e" s="T38">min </ts>
               <ts e="T40" id="Seg_1031" n="e" s="T39">di͡ebitim </ts>
               <ts e="T41" id="Seg_1033" n="e" s="T40">heː </ts>
               <ts e="T42" id="Seg_1035" n="e" s="T41">onton </ts>
               <ts e="T43" id="Seg_1037" n="e" s="T42">bihigi </ts>
               <ts e="T44" id="Seg_1039" n="e" s="T43">bartibɨt. </ts>
            </ts>
            <ts e="T52" id="Seg_1040" n="sc" s="T45">
               <ts e="T46" id="Seg_1042" n="e" s="T45">A </ts>
               <ts e="T47" id="Seg_1044" n="e" s="T46">Polina </ts>
               <ts e="T48" id="Seg_1046" n="e" s="T47">Alekseevnalaːk </ts>
               <ts e="T49" id="Seg_1048" n="e" s="T48">bejelerin </ts>
               <ts e="T301" id="Seg_1050" n="e" s="T49">semjalarɨn </ts>
               <ts e="T50" id="Seg_1052" n="e" s="T301">kɨtta </ts>
               <ts e="T51" id="Seg_1054" n="e" s="T50">kaːlbɨt </ts>
               <ts e="T52" id="Seg_1056" n="e" s="T51">etilere. </ts>
            </ts>
            <ts e="T63" id="Seg_1057" n="sc" s="T53">
               <ts e="T303" id="Seg_1059" n="e" s="T53">A </ts>
               <ts e="T54" id="Seg_1061" n="e" s="T303">(ti-) </ts>
               <ts e="T302" id="Seg_1063" n="e" s="T54">kelbit, </ts>
               <ts e="T55" id="Seg_1065" n="e" s="T302">keler </ts>
               <ts e="T56" id="Seg_1067" n="e" s="T55">turar </ts>
               <ts e="T57" id="Seg_1069" n="e" s="T56">etilere </ts>
               <ts e="T58" id="Seg_1071" n="e" s="T57">a </ts>
               <ts e="T59" id="Seg_1073" n="e" s="T58">tʼotʼa </ts>
               <ts e="T60" id="Seg_1075" n="e" s="T59">Nastʼa </ts>
               <ts e="T61" id="Seg_1077" n="e" s="T60">ginilerin </ts>
               <ts e="T62" id="Seg_1079" n="e" s="T61">ɨllarar </ts>
               <ts e="T63" id="Seg_1081" n="e" s="T62">ete. </ts>
            </ts>
            <ts e="T71" id="Seg_1082" n="sc" s="T64">
               <ts e="T65" id="Seg_1084" n="e" s="T64">Anɨ </ts>
               <ts e="T66" id="Seg_1086" n="e" s="T65">kimneri </ts>
               <ts e="T67" id="Seg_1088" n="e" s="T66">tu͡ok </ts>
               <ts e="T68" id="Seg_1090" n="e" s="T67">di͡eččiler </ts>
               <ts e="T69" id="Seg_1092" n="e" s="T68">burančik </ts>
               <ts e="T70" id="Seg_1094" n="e" s="T69">burančiktaːktar </ts>
               <ts e="T71" id="Seg_1096" n="e" s="T70">tu͡okta. </ts>
            </ts>
            <ts e="T83" id="Seg_1097" n="sc" s="T299">
               <ts e="T72" id="Seg_1099" n="e" s="T299">(Onton gi-) ((NOISE)) </ts>
               <ts e="T73" id="Seg_1101" n="e" s="T72">onton </ts>
               <ts e="T74" id="Seg_1103" n="e" s="T73">giniler </ts>
               <ts e="T75" id="Seg_1105" n="e" s="T74">kelbittere </ts>
               <ts e="T76" id="Seg_1107" n="e" s="T75">Polina </ts>
               <ts e="T77" id="Seg_1109" n="e" s="T76">Alekseevnalaːk </ts>
               <ts e="T78" id="Seg_1111" n="e" s="T77">onton </ts>
               <ts e="T79" id="Seg_1113" n="e" s="T78">bihigi </ts>
               <ts e="T80" id="Seg_1115" n="e" s="T79">onno </ts>
               <ts e="T81" id="Seg_1117" n="e" s="T80">že </ts>
               <ts e="T82" id="Seg_1119" n="e" s="T81">dalše </ts>
               <ts e="T83" id="Seg_1121" n="e" s="T82">barbɨppɨt. </ts>
            </ts>
            <ts e="T100" id="Seg_1122" n="sc" s="T84">
               <ts e="T85" id="Seg_1124" n="e" s="T84">Onton </ts>
               <ts e="T86" id="Seg_1126" n="e" s="T85">onno </ts>
               <ts e="T87" id="Seg_1128" n="e" s="T86">kim </ts>
               <ts e="T88" id="Seg_1130" n="e" s="T87">kimneri </ts>
               <ts e="T89" id="Seg_1132" n="e" s="T88">bu </ts>
               <ts e="T90" id="Seg_1134" n="e" s="T89">ke </ts>
               <ts e="T91" id="Seg_1136" n="e" s="T90">toktoːbupput </ts>
               <ts e="T92" id="Seg_1138" n="e" s="T91">onton </ts>
               <ts e="T93" id="Seg_1140" n="e" s="T92">onno </ts>
               <ts e="T94" id="Seg_1142" n="e" s="T93">kim </ts>
               <ts e="T95" id="Seg_1144" n="e" s="T94">(kur-) </ts>
               <ts e="T96" id="Seg_1146" n="e" s="T95">zaːjčik </ts>
               <ts e="T97" id="Seg_1148" n="e" s="T96">kimin </ts>
               <ts e="T98" id="Seg_1150" n="e" s="T97">üːtün </ts>
               <ts e="T99" id="Seg_1152" n="e" s="T98">körbüt </ts>
               <ts e="T100" id="Seg_1154" n="e" s="T99">etibit. </ts>
            </ts>
            <ts e="T104" id="Seg_1155" n="sc" s="T101">
               <ts e="T102" id="Seg_1157" n="e" s="T101">Onton </ts>
               <ts e="T103" id="Seg_1159" n="e" s="T102">(katatsalaː-) </ts>
               <ts e="T104" id="Seg_1161" n="e" s="T103">katatsalaːbɨppɨt. </ts>
            </ts>
            <ts e="T123" id="Seg_1162" n="sc" s="T105">
               <ts e="T106" id="Seg_1164" n="e" s="T105">Ulakan </ts>
               <ts e="T107" id="Seg_1166" n="e" s="T106">bagajɨ </ts>
               <ts e="T108" id="Seg_1168" n="e" s="T107">gorka </ts>
               <ts e="T109" id="Seg_1170" n="e" s="T108">ete </ts>
               <ts e="T110" id="Seg_1172" n="e" s="T109">onno </ts>
               <ts e="T111" id="Seg_1174" n="e" s="T110">bihigi </ts>
               <ts e="T112" id="Seg_1176" n="e" s="T111">že </ts>
               <ts e="T113" id="Seg_1178" n="e" s="T112">zamečajdaːbat </ts>
               <ts e="T114" id="Seg_1180" n="e" s="T113">etibit </ts>
               <ts e="T115" id="Seg_1182" n="e" s="T114">onno </ts>
               <ts e="T116" id="Seg_1184" n="e" s="T115">voobše </ts>
               <ts e="T117" id="Seg_1186" n="e" s="T116">beloe </ts>
               <ts e="T118" id="Seg_1188" n="e" s="T117">ete </ts>
               <ts e="T119" id="Seg_1190" n="e" s="T118">tu͡ok </ts>
               <ts e="T120" id="Seg_1192" n="e" s="T119">da </ts>
               <ts e="T121" id="Seg_1194" n="e" s="T120">hu͡ok </ts>
               <ts e="T122" id="Seg_1196" n="e" s="T121">ete </ts>
               <ts e="T123" id="Seg_1198" n="e" s="T122">köstübet. </ts>
            </ts>
            <ts e="T147" id="Seg_1199" n="sc" s="T124">
               <ts e="T125" id="Seg_1201" n="e" s="T124">Onton </ts>
               <ts e="T126" id="Seg_1203" n="e" s="T125">katatsalaːbɨppɨt </ts>
               <ts e="T127" id="Seg_1205" n="e" s="T126">onton </ts>
               <ts e="T128" id="Seg_1207" n="e" s="T127">onno </ts>
               <ts e="T129" id="Seg_1209" n="e" s="T128">kim </ts>
               <ts e="T130" id="Seg_1211" n="e" s="T129">Polina </ts>
               <ts e="T131" id="Seg_1213" n="e" s="T130">Alekseevna </ts>
               <ts e="T132" id="Seg_1215" n="e" s="T131">baːr </ts>
               <ts e="T133" id="Seg_1217" n="e" s="T132">ete </ts>
               <ts e="T134" id="Seg_1219" n="e" s="T133">semjatɨn </ts>
               <ts e="T135" id="Seg_1221" n="e" s="T134">gɨtta </ts>
               <ts e="T136" id="Seg_1223" n="e" s="T135">onton </ts>
               <ts e="T137" id="Seg_1225" n="e" s="T136">tʼotʼa </ts>
               <ts e="T138" id="Seg_1227" n="e" s="T137">Nastʼa </ts>
               <ts e="T139" id="Seg_1229" n="e" s="T138">onton </ts>
               <ts e="T140" id="Seg_1231" n="e" s="T139">Nʼukuːska </ts>
               <ts e="T141" id="Seg_1233" n="e" s="T140">Ludmila </ts>
               <ts e="T142" id="Seg_1235" n="e" s="T141">Nikolaevna </ts>
               <ts e="T143" id="Seg_1237" n="e" s="T142">ogoto </ts>
               <ts e="T144" id="Seg_1239" n="e" s="T143">onton </ts>
               <ts e="T145" id="Seg_1241" n="e" s="T144">Annuška </ts>
               <ts e="T146" id="Seg_1243" n="e" s="T145">tʼotʼa </ts>
               <ts e="T147" id="Seg_1245" n="e" s="T146">Nastʼa. </ts>
            </ts>
            <ts e="T151" id="Seg_1246" n="sc" s="T148">
               <ts e="T149" id="Seg_1248" n="e" s="T148">Onton </ts>
               <ts e="T150" id="Seg_1250" n="e" s="T149">bihigi </ts>
               <ts e="T151" id="Seg_1252" n="e" s="T150">katatsalaːbɨppɨt. </ts>
            </ts>
            <ts e="T168" id="Seg_1253" n="sc" s="T152">
               <ts e="T153" id="Seg_1255" n="e" s="T152">Onton </ts>
               <ts e="T154" id="Seg_1257" n="e" s="T153">kim </ts>
               <ts e="T155" id="Seg_1259" n="e" s="T154">ete </ts>
               <ts e="T156" id="Seg_1261" n="e" s="T155">kim </ts>
               <ts e="T157" id="Seg_1263" n="e" s="T156">ete </ts>
               <ts e="T158" id="Seg_1265" n="e" s="T157">eː </ts>
               <ts e="T159" id="Seg_1267" n="e" s="T158">dʼadʼa </ts>
               <ts e="T160" id="Seg_1269" n="e" s="T159">Parako </ts>
               <ts e="T161" id="Seg_1271" n="e" s="T160">ogolorgo </ts>
               <ts e="T162" id="Seg_1273" n="e" s="T161">kim </ts>
               <ts e="T163" id="Seg_1275" n="e" s="T162">oŋoror </ts>
               <ts e="T164" id="Seg_1277" n="e" s="T163">ete </ts>
               <ts e="T165" id="Seg_1279" n="e" s="T164">kim </ts>
               <ts e="T166" id="Seg_1281" n="e" s="T165">ete </ts>
               <ts e="T167" id="Seg_1283" n="e" s="T166">ke </ts>
               <ts e="T168" id="Seg_1285" n="e" s="T167">figurkalarɨ. </ts>
            </ts>
            <ts e="T211" id="Seg_1286" n="sc" s="T169">
               <ts e="T170" id="Seg_1288" n="e" s="T169">Onton </ts>
               <ts e="T300" id="Seg_1290" n="e" s="T170">ke </ts>
               <ts e="T171" id="Seg_1292" n="e" s="T300">((NOISE)) </ts>
               <ts e="T172" id="Seg_1294" n="e" s="T171">eː </ts>
               <ts e="T173" id="Seg_1296" n="e" s="T172">Kira </ts>
               <ts e="T174" id="Seg_1298" n="e" s="T173">kime </ts>
               <ts e="T175" id="Seg_1300" n="e" s="T174">figurkata </ts>
               <ts e="T176" id="Seg_1302" n="e" s="T175">zaːjčik </ts>
               <ts e="T177" id="Seg_1304" n="e" s="T176">ete </ts>
               <ts e="T178" id="Seg_1306" n="e" s="T177">a </ts>
               <ts e="T179" id="Seg_1308" n="e" s="T178">kim </ts>
               <ts e="T180" id="Seg_1310" n="e" s="T179">gi͡ene </ts>
               <ts e="T181" id="Seg_1312" n="e" s="T180">kim </ts>
               <ts e="T182" id="Seg_1314" n="e" s="T181">ete </ts>
               <ts e="T183" id="Seg_1316" n="e" s="T182">kim </ts>
               <ts e="T184" id="Seg_1318" n="e" s="T183">gi͡ene </ts>
               <ts e="T185" id="Seg_1320" n="e" s="T184">Nadjex </ts>
               <ts e="T186" id="Seg_1322" n="e" s="T185">gi͡ene </ts>
               <ts e="T187" id="Seg_1324" n="e" s="T186">kim </ts>
               <ts e="T188" id="Seg_1326" n="e" s="T187">meːne </ts>
               <ts e="T189" id="Seg_1328" n="e" s="T188">kurpaːska </ts>
               <ts e="T190" id="Seg_1330" n="e" s="T189">ete. </ts>
               <ts e="T191" id="Seg_1332" n="e" s="T190">Onton </ts>
               <ts e="T192" id="Seg_1334" n="e" s="T191">uːrbut </ts>
               <ts e="T193" id="Seg_1336" n="e" s="T192">etilere </ts>
               <ts e="T194" id="Seg_1338" n="e" s="T193">onno </ts>
               <ts e="T195" id="Seg_1340" n="e" s="T194">ikki͡ettere </ts>
               <ts e="T196" id="Seg_1342" n="e" s="T195">a </ts>
               <ts e="T197" id="Seg_1344" n="e" s="T196">kurpaːskanɨ </ts>
               <ts e="T198" id="Seg_1346" n="e" s="T197">kimi͡eke </ts>
               <ts e="T199" id="Seg_1348" n="e" s="T198">uːrbut </ts>
               <ts e="T200" id="Seg_1350" n="e" s="T199">etilere </ts>
               <ts e="T201" id="Seg_1352" n="e" s="T200">kim </ts>
               <ts e="T202" id="Seg_1354" n="e" s="T201">etej </ts>
               <ts e="T203" id="Seg_1356" n="e" s="T202">ke </ts>
               <ts e="T204" id="Seg_1358" n="e" s="T203">zaːjčik </ts>
               <ts e="T205" id="Seg_1360" n="e" s="T204">kimiger </ts>
               <ts e="T206" id="Seg_1362" n="e" s="T205">onton </ts>
               <ts e="T207" id="Seg_1364" n="e" s="T206">iti </ts>
               <ts e="T208" id="Seg_1366" n="e" s="T207">kim </ts>
               <ts e="T209" id="Seg_1368" n="e" s="T208">nu </ts>
               <ts e="T210" id="Seg_1370" n="e" s="T209">ka </ts>
               <ts e="T211" id="Seg_1372" n="e" s="T210">jamkatɨgar. </ts>
            </ts>
            <ts e="T232" id="Seg_1373" n="sc" s="T212">
               <ts e="T213" id="Seg_1375" n="e" s="T212">Onton </ts>
               <ts e="T214" id="Seg_1377" n="e" s="T213">onno </ts>
               <ts e="T215" id="Seg_1379" n="e" s="T214">bihigi </ts>
               <ts e="T216" id="Seg_1381" n="e" s="T215">onton </ts>
               <ts e="T217" id="Seg_1383" n="e" s="T216">onno </ts>
               <ts e="T218" id="Seg_1385" n="e" s="T217">(ikk-) </ts>
               <ts e="T219" id="Seg_1387" n="e" s="T218">onton </ts>
               <ts e="T220" id="Seg_1389" n="e" s="T219">kojut </ts>
               <ts e="T221" id="Seg_1391" n="e" s="T220">kas </ts>
               <ts e="T222" id="Seg_1393" n="e" s="T221">biːr </ts>
               <ts e="T223" id="Seg_1395" n="e" s="T222">kas </ts>
               <ts e="T224" id="Seg_1397" n="e" s="T223">da </ts>
               <ts e="T225" id="Seg_1399" n="e" s="T224">nedelʼa </ts>
               <ts e="T226" id="Seg_1401" n="e" s="T225">aːspɨta </ts>
               <ts e="T227" id="Seg_1403" n="e" s="T226">össü͡ö </ts>
               <ts e="T228" id="Seg_1405" n="e" s="T227">ikkisterin </ts>
               <ts e="T229" id="Seg_1407" n="e" s="T228">po </ts>
               <ts e="T230" id="Seg_1409" n="e" s="T229">vtoromu </ts>
               <ts e="T231" id="Seg_1411" n="e" s="T230">razu </ts>
               <ts e="T232" id="Seg_1413" n="e" s="T231">barbɨppɨt. </ts>
            </ts>
            <ts e="T238" id="Seg_1414" n="sc" s="T233">
               <ts e="T234" id="Seg_1416" n="e" s="T233">Onton </ts>
               <ts e="T235" id="Seg_1418" n="e" s="T234">onno </ts>
               <ts e="T236" id="Seg_1420" n="e" s="T235">barbɨppɨt </ts>
               <ts e="T237" id="Seg_1422" n="e" s="T236">hol </ts>
               <ts e="T238" id="Seg_1424" n="e" s="T237">mestobɨtɨgar. </ts>
            </ts>
            <ts e="T256" id="Seg_1425" n="sc" s="T239">
               <ts e="T240" id="Seg_1427" n="e" s="T239">Ol </ts>
               <ts e="T241" id="Seg_1429" n="e" s="T240">mestoga </ts>
               <ts e="T242" id="Seg_1431" n="e" s="T241">onno </ts>
               <ts e="T243" id="Seg_1433" n="e" s="T242">kim </ts>
               <ts e="T244" id="Seg_1435" n="e" s="T243">baːr </ts>
               <ts e="T245" id="Seg_1437" n="e" s="T244">tot </ts>
               <ts e="T246" id="Seg_1439" n="e" s="T245">že </ts>
               <ts e="T247" id="Seg_1441" n="e" s="T246">zaːjčik </ts>
               <ts e="T248" id="Seg_1443" n="e" s="T247">baːr </ts>
               <ts e="T249" id="Seg_1445" n="e" s="T248">ete </ts>
               <ts e="T250" id="Seg_1447" n="e" s="T249">bu͡o </ts>
               <ts e="T251" id="Seg_1449" n="e" s="T250">ol </ts>
               <ts e="T252" id="Seg_1451" n="e" s="T251">zaːjčik </ts>
               <ts e="T253" id="Seg_1453" n="e" s="T252">iːktillibit </ts>
               <ts e="T254" id="Seg_1455" n="e" s="T253">ete </ts>
               <ts e="T255" id="Seg_1457" n="e" s="T254">iːkteːk </ts>
               <ts e="T256" id="Seg_1459" n="e" s="T255">ete. </ts>
            </ts>
            <ts e="T266" id="Seg_1460" n="sc" s="T257">
               <ts e="T258" id="Seg_1462" n="e" s="T257">A </ts>
               <ts e="T259" id="Seg_1464" n="e" s="T258">kim </ts>
               <ts e="T260" id="Seg_1466" n="e" s="T259">da </ts>
               <ts e="T261" id="Seg_1468" n="e" s="T260">kurpaːskɨ </ts>
               <ts e="T262" id="Seg_1470" n="e" s="T261">menʼiːtin </ts>
               <ts e="T263" id="Seg_1472" n="e" s="T262">kim </ts>
               <ts e="T264" id="Seg_1474" n="e" s="T263">ere </ts>
               <ts e="T265" id="Seg_1476" n="e" s="T264">turbut </ts>
               <ts e="T266" id="Seg_1478" n="e" s="T265">etibit. </ts>
            </ts>
            <ts e="T286" id="Seg_1479" n="sc" s="T267">
               <ts e="T268" id="Seg_1481" n="e" s="T267">Onton </ts>
               <ts e="T269" id="Seg_1483" n="e" s="T268">kojut </ts>
               <ts e="T270" id="Seg_1485" n="e" s="T269">bihigi </ts>
               <ts e="T271" id="Seg_1487" n="e" s="T270">katatsalɨː </ts>
               <ts e="T272" id="Seg_1489" n="e" s="T271">barbɨppɨt </ts>
               <ts e="T273" id="Seg_1491" n="e" s="T272">a </ts>
               <ts e="T274" id="Seg_1493" n="e" s="T273">kim </ts>
               <ts e="T275" id="Seg_1495" n="e" s="T274">de </ts>
               <ts e="T276" id="Seg_1497" n="e" s="T275">eː </ts>
               <ts e="T277" id="Seg_1499" n="e" s="T276">dʼadʼa </ts>
               <ts e="T278" id="Seg_1501" n="e" s="T277">Prokula </ts>
               <ts e="T279" id="Seg_1503" n="e" s="T278">ogoloru </ts>
               <ts e="T280" id="Seg_1505" n="e" s="T279">ɨlpɨt </ts>
               <ts e="T281" id="Seg_1507" n="e" s="T280">etilere </ts>
               <ts e="T282" id="Seg_1509" n="e" s="T281">eː </ts>
               <ts e="T283" id="Seg_1511" n="e" s="T282">hu͡ok </ts>
               <ts e="T284" id="Seg_1513" n="e" s="T283">ol </ts>
               <ts e="T285" id="Seg_1515" n="e" s="T284">atɨn </ts>
               <ts e="T286" id="Seg_1517" n="e" s="T285">kimi͡eke. </ts>
            </ts>
            <ts e="T298" id="Seg_1518" n="sc" s="T287">
               <ts e="T288" id="Seg_1520" n="e" s="T287">Bihigi </ts>
               <ts e="T289" id="Seg_1522" n="e" s="T288">katatsalɨː </ts>
               <ts e="T290" id="Seg_1524" n="e" s="T289">barbɨppɨt </ts>
               <ts e="T291" id="Seg_1526" n="e" s="T290">onton </ts>
               <ts e="T292" id="Seg_1528" n="e" s="T291">töttörü </ts>
               <ts e="T293" id="Seg_1530" n="e" s="T292">keltibit </ts>
               <ts e="T294" id="Seg_1532" n="e" s="T293">kimŋe </ts>
               <ts e="T295" id="Seg_1534" n="e" s="T294">poselokka </ts>
               <ts e="T296" id="Seg_1536" n="e" s="T295">lɨːžalardaːk </ts>
               <ts e="T297" id="Seg_1538" n="e" s="T296">onton </ts>
               <ts e="T298" id="Seg_1540" n="e" s="T297">keltibit. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_1541" s="T1">KuDP_2009_Hare_nar.001 (001)</ta>
            <ta e="T18" id="Seg_1542" s="T8">KuDP_2009_Hare_nar.002 (002)</ta>
            <ta e="T36" id="Seg_1543" s="T19">KuDP_2009_Hare_nar.003 (003)</ta>
            <ta e="T44" id="Seg_1544" s="T37">KuDP_2009_Hare_nar.004 (004)</ta>
            <ta e="T52" id="Seg_1545" s="T45">KuDP_2009_Hare_nar.005 (005)</ta>
            <ta e="T63" id="Seg_1546" s="T53">KuDP_2009_Hare_nar.006 (006)</ta>
            <ta e="T71" id="Seg_1547" s="T64">KuDP_2009_Hare_nar.007 (007)</ta>
            <ta e="T83" id="Seg_1548" s="T299">KuDP_2009_Hare_nar.008 (008)</ta>
            <ta e="T100" id="Seg_1549" s="T84">KuDP_2009_Hare_nar.009 (009)</ta>
            <ta e="T104" id="Seg_1550" s="T101">KuDP_2009_Hare_nar.010 (010)</ta>
            <ta e="T123" id="Seg_1551" s="T105">KuDP_2009_Hare_nar.011 (011)</ta>
            <ta e="T147" id="Seg_1552" s="T124">KuDP_2009_Hare_nar.012 (012)</ta>
            <ta e="T151" id="Seg_1553" s="T148">KuDP_2009_Hare_nar.013 (013)</ta>
            <ta e="T168" id="Seg_1554" s="T152">KuDP_2009_Hare_nar.014 (014)</ta>
            <ta e="T190" id="Seg_1555" s="T169">KuDP_2009_Hare_nar.015 (015)</ta>
            <ta e="T211" id="Seg_1556" s="T190">KuDP_2009_Hare_nar.016 (016)</ta>
            <ta e="T232" id="Seg_1557" s="T212">KuDP_2009_Hare_nar.017 (017)</ta>
            <ta e="T238" id="Seg_1558" s="T233">KuDP_2009_Hare_nar.018 (018)</ta>
            <ta e="T256" id="Seg_1559" s="T239">KuDP_2009_Hare_nar.019 (019)</ta>
            <ta e="T266" id="Seg_1560" s="T257">KuDP_2009_Hare_nar.020 (020)</ta>
            <ta e="T286" id="Seg_1561" s="T267">KuDP_2009_Hare_nar.021 (021)</ta>
            <ta e="T298" id="Seg_1562" s="T287">KuDP_2009_Hare_nar.022 (022)</ta>
         </annotation>
         <annotation name="st" tierref="st" />
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_1563" s="T1">Biːrde bihigi Reginanɨ gɨtta hɨldʼar etibit. </ta>
            <ta e="T18" id="Seg_1564" s="T8">Onton onno tʼotʼa eː bihigi hɨldʼar etibit ogonu karajar etibit. </ta>
            <ta e="T36" id="Seg_1565" s="T19">Onton Kira maːmata ɨgɨrbɨta minigin katatsalɨː baragɨt du͡o tɨ͡aga bihigi eː katatsalɨː baran du minigin gɨtta tɨ͡aga. </ta>
            <ta e="T44" id="Seg_1566" s="T37">Onton min di͡ebitim heː onton bihigi bartibɨt. </ta>
            <ta e="T52" id="Seg_1567" s="T45">A Polina Alekseevnalaːk bejelerin semjalarɨn kɨtta kaːlbɨt etilere. </ta>
            <ta e="T63" id="Seg_1568" s="T53">A (ti-) kelbit, keler turar etilere a tʼotʼa Nastʼa ginilerin ɨllarar ete. </ta>
            <ta e="T71" id="Seg_1569" s="T64">Anɨ kimneri tu͡ok di͡eččiler burančik burančiktaːktar tu͡okta. </ta>
            <ta e="T83" id="Seg_1570" s="T299">(Onton gi-) ((NOISE)) onton giniler kelbittere Polina Alekseevnalaːk onton bihigi onno že dalše barbɨppɨt. </ta>
            <ta e="T100" id="Seg_1571" s="T84">Onton onno kim kimneri bu ke toktoːbupput onton onno kim (kur-) zaːjčik kimin üːtün körbüt etibit. </ta>
            <ta e="T104" id="Seg_1572" s="T101">Onton (katatsalaː-) katatsalaːbɨppɨt. </ta>
            <ta e="T123" id="Seg_1573" s="T105">Ulakan bagajɨ gorka ete onno bihigi že zamečajdaːbat etibit onno voobše beloe ete tu͡ok da hu͡ok ete köstübet. </ta>
            <ta e="T147" id="Seg_1574" s="T124">Onton katatsalaːbɨppɨt onton onno kim Polina Alekseevna baːr ete semjatɨn gɨtta onton tʼotʼa Nastʼa onton Nʼukuːska Ludmila Nikolaevna ogoto onton Annuška tʼotʼa Nastʼa. </ta>
            <ta e="T151" id="Seg_1575" s="T148">Onton bihigi katatsalaːbɨppɨt. </ta>
            <ta e="T168" id="Seg_1576" s="T152">Onton kim ete kim ete eː dʼadʼa Parako ogolorgo kim oŋoror ete kim ete ke figurkalarɨ. </ta>
            <ta e="T190" id="Seg_1577" s="T169">Onton ke ((NOISE)) eː Kira kime figurkata zaːjčik ete a kim gi͡ene kim ete kim gi͡ene Nadjex gi͡ene kim meːne kurpaːska ete. </ta>
            <ta e="T211" id="Seg_1578" s="T190">Onton uːrbut etilere onno ikki͡ettere a kurpaːskanɨ kimi͡eke uːrbut etilere kim etej ke zaːjčik kimiger onton iti kim nu ka jamkatɨgar. </ta>
            <ta e="T232" id="Seg_1579" s="T212">Onton onno bihigi onton onno (ikk-) onton kojut kas biːr kas da nedelʼa aːspɨta össü͡ö ikkisterin po vtoromu razu barbɨppɨt. </ta>
            <ta e="T238" id="Seg_1580" s="T233">Onton onno barbɨppɨt hol mestobɨtɨgar. </ta>
            <ta e="T256" id="Seg_1581" s="T239">Ol mestoga onno kim baːr tot že zaːjčik baːr ete bu͡o ol zaːjčik iːktillibit ete iːkteːk ete. </ta>
            <ta e="T266" id="Seg_1582" s="T257">A kim da kurpaːskɨ menʼiːtin kim ere turbut etibit. </ta>
            <ta e="T286" id="Seg_1583" s="T267">Onton kojut bihigi katatsalɨː barbɨppɨt a kim de eː dʼadʼa Prokula ogoloru ɨlpɨt etilere eː hu͡ok ol atɨn kimi͡eke. </ta>
            <ta e="T298" id="Seg_1584" s="T287">Bihigi katatsalɨː barbɨppɨt onton töttörü keltibit kimŋe poselokka lɨːžalardaːk onton keltibit. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_1585" s="T1">biːrde</ta>
            <ta e="T3" id="Seg_1586" s="T2">bihigi</ta>
            <ta e="T4" id="Seg_1587" s="T3">Regina-nɨ</ta>
            <ta e="T5" id="Seg_1588" s="T4">gɨtta</ta>
            <ta e="T6" id="Seg_1589" s="T5">hɨldʼ-ar</ta>
            <ta e="T7" id="Seg_1590" s="T6">e-ti-bit</ta>
            <ta e="T9" id="Seg_1591" s="T8">on-ton</ta>
            <ta e="T10" id="Seg_1592" s="T9">onno</ta>
            <ta e="T11" id="Seg_1593" s="T10">tʼotʼa</ta>
            <ta e="T12" id="Seg_1594" s="T11">eː</ta>
            <ta e="T13" id="Seg_1595" s="T12">bihigi</ta>
            <ta e="T14" id="Seg_1596" s="T13">hɨldʼ-ar</ta>
            <ta e="T15" id="Seg_1597" s="T14">e-ti-bit</ta>
            <ta e="T16" id="Seg_1598" s="T15">ogo-nu</ta>
            <ta e="T17" id="Seg_1599" s="T16">karaj-ar</ta>
            <ta e="T18" id="Seg_1600" s="T17">e-ti-bit</ta>
            <ta e="T20" id="Seg_1601" s="T19">on-ton</ta>
            <ta e="T21" id="Seg_1602" s="T20">Kira</ta>
            <ta e="T22" id="Seg_1603" s="T21">maːma-ta</ta>
            <ta e="T23" id="Seg_1604" s="T22">ɨgɨr-bɨt-a</ta>
            <ta e="T24" id="Seg_1605" s="T23">minigi-n</ta>
            <ta e="T25" id="Seg_1606" s="T24">katatsa-l-ɨː</ta>
            <ta e="T26" id="Seg_1607" s="T25">bar-a-gɨt</ta>
            <ta e="T27" id="Seg_1608" s="T26">du͡o</ta>
            <ta e="T28" id="Seg_1609" s="T27">tɨ͡a-ga</ta>
            <ta e="T29" id="Seg_1610" s="T28">bihigi</ta>
            <ta e="T30" id="Seg_1611" s="T29">eː</ta>
            <ta e="T31" id="Seg_1612" s="T30">katatsa-l-ɨː</ta>
            <ta e="T32" id="Seg_1613" s="T31">bar-an</ta>
            <ta e="T33" id="Seg_1614" s="T32">du</ta>
            <ta e="T34" id="Seg_1615" s="T33">minigi-n</ta>
            <ta e="T35" id="Seg_1616" s="T34">gɨtta</ta>
            <ta e="T36" id="Seg_1617" s="T35">tɨ͡a-ga</ta>
            <ta e="T38" id="Seg_1618" s="T37">on-ton</ta>
            <ta e="T39" id="Seg_1619" s="T38">min</ta>
            <ta e="T40" id="Seg_1620" s="T39">di͡e-bit-i-m</ta>
            <ta e="T41" id="Seg_1621" s="T40">heː</ta>
            <ta e="T42" id="Seg_1622" s="T41">on-ton</ta>
            <ta e="T43" id="Seg_1623" s="T42">bihigi</ta>
            <ta e="T44" id="Seg_1624" s="T43">bar-ti-bɨt</ta>
            <ta e="T46" id="Seg_1625" s="T45">a</ta>
            <ta e="T47" id="Seg_1626" s="T46">Polina</ta>
            <ta e="T48" id="Seg_1627" s="T47">Alekseevna-laːk</ta>
            <ta e="T49" id="Seg_1628" s="T48">beje-leri-n</ta>
            <ta e="T301" id="Seg_1629" s="T49">semja-larɨ-n</ta>
            <ta e="T50" id="Seg_1630" s="T301">kɨtta</ta>
            <ta e="T51" id="Seg_1631" s="T50">kaːl-bɨt</ta>
            <ta e="T52" id="Seg_1632" s="T51">e-ti-lere</ta>
            <ta e="T303" id="Seg_1633" s="T53">a</ta>
            <ta e="T302" id="Seg_1634" s="T54">kel-bit</ta>
            <ta e="T55" id="Seg_1635" s="T302">kel-er</ta>
            <ta e="T56" id="Seg_1636" s="T55">tur-ar</ta>
            <ta e="T57" id="Seg_1637" s="T56">e-ti-lere</ta>
            <ta e="T58" id="Seg_1638" s="T57">a</ta>
            <ta e="T59" id="Seg_1639" s="T58">tʼotʼa</ta>
            <ta e="T60" id="Seg_1640" s="T59">Nastʼa</ta>
            <ta e="T61" id="Seg_1641" s="T60">giniler-i-n</ta>
            <ta e="T62" id="Seg_1642" s="T61">ɨl-lar-ar</ta>
            <ta e="T63" id="Seg_1643" s="T62">e-t-e</ta>
            <ta e="T65" id="Seg_1644" s="T64">anɨ</ta>
            <ta e="T66" id="Seg_1645" s="T65">kim-ner-i</ta>
            <ta e="T67" id="Seg_1646" s="T66">tu͡ok</ta>
            <ta e="T68" id="Seg_1647" s="T67">di͡e-čči-ler</ta>
            <ta e="T69" id="Seg_1648" s="T68">burančik</ta>
            <ta e="T70" id="Seg_1649" s="T69">burančik-taːk-tar</ta>
            <ta e="T71" id="Seg_1650" s="T70">tu͡ok-ta</ta>
            <ta e="T73" id="Seg_1651" s="T72">on-ton</ta>
            <ta e="T74" id="Seg_1652" s="T73">giniler</ta>
            <ta e="T75" id="Seg_1653" s="T74">kel-bit-tere</ta>
            <ta e="T76" id="Seg_1654" s="T75">Polina</ta>
            <ta e="T77" id="Seg_1655" s="T76">Alekseevna-laːk</ta>
            <ta e="T78" id="Seg_1656" s="T77">on-ton</ta>
            <ta e="T79" id="Seg_1657" s="T78">bihigi</ta>
            <ta e="T80" id="Seg_1658" s="T79">onno</ta>
            <ta e="T81" id="Seg_1659" s="T80">že</ta>
            <ta e="T82" id="Seg_1660" s="T81">dalše</ta>
            <ta e="T83" id="Seg_1661" s="T82">bar-bɨp-pɨt</ta>
            <ta e="T85" id="Seg_1662" s="T84">on-ton</ta>
            <ta e="T86" id="Seg_1663" s="T85">onno</ta>
            <ta e="T87" id="Seg_1664" s="T86">kim</ta>
            <ta e="T88" id="Seg_1665" s="T87">kim-ner-i</ta>
            <ta e="T89" id="Seg_1666" s="T88">bu</ta>
            <ta e="T90" id="Seg_1667" s="T89">ke</ta>
            <ta e="T91" id="Seg_1668" s="T90">toktoː-bup-put</ta>
            <ta e="T92" id="Seg_1669" s="T91">on-ton</ta>
            <ta e="T93" id="Seg_1670" s="T92">onno</ta>
            <ta e="T94" id="Seg_1671" s="T93">kim</ta>
            <ta e="T96" id="Seg_1672" s="T95">zaːjčik</ta>
            <ta e="T97" id="Seg_1673" s="T96">kim-i-n</ta>
            <ta e="T98" id="Seg_1674" s="T97">üːt-ü-n</ta>
            <ta e="T99" id="Seg_1675" s="T98">kör-büt</ta>
            <ta e="T100" id="Seg_1676" s="T99">e-ti-bit</ta>
            <ta e="T102" id="Seg_1677" s="T101">on-ton</ta>
            <ta e="T103" id="Seg_1678" s="T102">katatsa-laː</ta>
            <ta e="T104" id="Seg_1679" s="T103">katatsa-laː-bɨp-pɨt</ta>
            <ta e="T106" id="Seg_1680" s="T105">ulakan</ta>
            <ta e="T107" id="Seg_1681" s="T106">bagajɨ</ta>
            <ta e="T108" id="Seg_1682" s="T107">gorka</ta>
            <ta e="T109" id="Seg_1683" s="T108">e-t-e</ta>
            <ta e="T110" id="Seg_1684" s="T109">onno</ta>
            <ta e="T111" id="Seg_1685" s="T110">bihigi</ta>
            <ta e="T112" id="Seg_1686" s="T111">že</ta>
            <ta e="T113" id="Seg_1687" s="T112">zamečaj-daː-bat</ta>
            <ta e="T114" id="Seg_1688" s="T113">e-ti-bit</ta>
            <ta e="T115" id="Seg_1689" s="T114">onno</ta>
            <ta e="T118" id="Seg_1690" s="T117">e-t-e</ta>
            <ta e="T119" id="Seg_1691" s="T118">tu͡ok</ta>
            <ta e="T120" id="Seg_1692" s="T119">da</ta>
            <ta e="T121" id="Seg_1693" s="T120">hu͡ok</ta>
            <ta e="T122" id="Seg_1694" s="T121">e-t-e</ta>
            <ta e="T123" id="Seg_1695" s="T122">köst-ü-bet</ta>
            <ta e="T125" id="Seg_1696" s="T124">on-ton</ta>
            <ta e="T126" id="Seg_1697" s="T125">katatsa-laː-bɨp-pɨt</ta>
            <ta e="T127" id="Seg_1698" s="T126">on-ton</ta>
            <ta e="T128" id="Seg_1699" s="T127">onno</ta>
            <ta e="T129" id="Seg_1700" s="T128">kim</ta>
            <ta e="T130" id="Seg_1701" s="T129">Polina</ta>
            <ta e="T131" id="Seg_1702" s="T130">Alekseevna</ta>
            <ta e="T132" id="Seg_1703" s="T131">baːr</ta>
            <ta e="T133" id="Seg_1704" s="T132">e-t-e</ta>
            <ta e="T134" id="Seg_1705" s="T133">semja-tɨ-n</ta>
            <ta e="T135" id="Seg_1706" s="T134">gɨtta</ta>
            <ta e="T136" id="Seg_1707" s="T135">on-ton</ta>
            <ta e="T137" id="Seg_1708" s="T136">tʼotʼa</ta>
            <ta e="T138" id="Seg_1709" s="T137">Nastʼa</ta>
            <ta e="T139" id="Seg_1710" s="T138">on-ton</ta>
            <ta e="T140" id="Seg_1711" s="T139">Nʼukuːska</ta>
            <ta e="T141" id="Seg_1712" s="T140">Ludmila</ta>
            <ta e="T142" id="Seg_1713" s="T141">Nikolaevna</ta>
            <ta e="T143" id="Seg_1714" s="T142">ogo-to</ta>
            <ta e="T144" id="Seg_1715" s="T143">on-ton</ta>
            <ta e="T145" id="Seg_1716" s="T144">Annuška</ta>
            <ta e="T146" id="Seg_1717" s="T145">tʼotʼa</ta>
            <ta e="T147" id="Seg_1718" s="T146">Nastʼa</ta>
            <ta e="T149" id="Seg_1719" s="T148">on-ton</ta>
            <ta e="T150" id="Seg_1720" s="T149">bihigi</ta>
            <ta e="T151" id="Seg_1721" s="T150">katatsa-laː-bɨp-pɨt</ta>
            <ta e="T153" id="Seg_1722" s="T152">on-ton</ta>
            <ta e="T154" id="Seg_1723" s="T153">kim</ta>
            <ta e="T155" id="Seg_1724" s="T154">e-t-e</ta>
            <ta e="T156" id="Seg_1725" s="T155">kim</ta>
            <ta e="T157" id="Seg_1726" s="T156">e-t-e</ta>
            <ta e="T158" id="Seg_1727" s="T157">eː</ta>
            <ta e="T159" id="Seg_1728" s="T158">dʼadʼa</ta>
            <ta e="T160" id="Seg_1729" s="T159">Parako</ta>
            <ta e="T161" id="Seg_1730" s="T160">ogo-lor-go</ta>
            <ta e="T162" id="Seg_1731" s="T161">kim</ta>
            <ta e="T163" id="Seg_1732" s="T162">oŋor-or</ta>
            <ta e="T164" id="Seg_1733" s="T163">e-t-e</ta>
            <ta e="T165" id="Seg_1734" s="T164">kim</ta>
            <ta e="T166" id="Seg_1735" s="T165">e-t-e</ta>
            <ta e="T167" id="Seg_1736" s="T166">ke</ta>
            <ta e="T168" id="Seg_1737" s="T167">figurka-lar-ɨ</ta>
            <ta e="T170" id="Seg_1738" s="T169">on-ton</ta>
            <ta e="T300" id="Seg_1739" s="T170">ke</ta>
            <ta e="T172" id="Seg_1740" s="T171">eː</ta>
            <ta e="T173" id="Seg_1741" s="T172">Kira</ta>
            <ta e="T174" id="Seg_1742" s="T173">kim-e</ta>
            <ta e="T175" id="Seg_1743" s="T174">figurka-ta</ta>
            <ta e="T176" id="Seg_1744" s="T175">zaːjčik</ta>
            <ta e="T177" id="Seg_1745" s="T176">e-t-e</ta>
            <ta e="T178" id="Seg_1746" s="T177">a</ta>
            <ta e="T179" id="Seg_1747" s="T178">kim</ta>
            <ta e="T180" id="Seg_1748" s="T179">gi͡en-e</ta>
            <ta e="T181" id="Seg_1749" s="T180">kim</ta>
            <ta e="T182" id="Seg_1750" s="T181">e-t-e</ta>
            <ta e="T183" id="Seg_1751" s="T182">kim</ta>
            <ta e="T184" id="Seg_1752" s="T183">gi͡en-e</ta>
            <ta e="T185" id="Seg_1753" s="T184">Nadjex</ta>
            <ta e="T186" id="Seg_1754" s="T185">gi͡en-e</ta>
            <ta e="T187" id="Seg_1755" s="T186">kim</ta>
            <ta e="T188" id="Seg_1756" s="T187">meːne</ta>
            <ta e="T189" id="Seg_1757" s="T188">kurpaːska</ta>
            <ta e="T190" id="Seg_1758" s="T189">e-t-e</ta>
            <ta e="T191" id="Seg_1759" s="T190">on-ton</ta>
            <ta e="T192" id="Seg_1760" s="T191">uːr-but</ta>
            <ta e="T193" id="Seg_1761" s="T192">e-ti-lere</ta>
            <ta e="T194" id="Seg_1762" s="T193">onno</ta>
            <ta e="T195" id="Seg_1763" s="T194">ikk-i͡et-tere</ta>
            <ta e="T196" id="Seg_1764" s="T195">a</ta>
            <ta e="T197" id="Seg_1765" s="T196">kurpaːska-nɨ</ta>
            <ta e="T198" id="Seg_1766" s="T197">kimi͡e-ke</ta>
            <ta e="T199" id="Seg_1767" s="T198">uːr-but</ta>
            <ta e="T200" id="Seg_1768" s="T199">e-ti-lere</ta>
            <ta e="T201" id="Seg_1769" s="T200">kim</ta>
            <ta e="T202" id="Seg_1770" s="T201">e-t-e=j</ta>
            <ta e="T203" id="Seg_1771" s="T202">ke</ta>
            <ta e="T204" id="Seg_1772" s="T203">zaːjčik</ta>
            <ta e="T205" id="Seg_1773" s="T204">kim-i-ger</ta>
            <ta e="T206" id="Seg_1774" s="T205">onton</ta>
            <ta e="T207" id="Seg_1775" s="T206">iti</ta>
            <ta e="T208" id="Seg_1776" s="T207">kim</ta>
            <ta e="T209" id="Seg_1777" s="T208">nu</ta>
            <ta e="T210" id="Seg_1778" s="T209">ka</ta>
            <ta e="T211" id="Seg_1779" s="T210">jamka-tɨ-gar</ta>
            <ta e="T213" id="Seg_1780" s="T212">on-ton</ta>
            <ta e="T214" id="Seg_1781" s="T213">onno</ta>
            <ta e="T215" id="Seg_1782" s="T214">bihigi</ta>
            <ta e="T216" id="Seg_1783" s="T215">on-ton</ta>
            <ta e="T217" id="Seg_1784" s="T216">onno</ta>
            <ta e="T219" id="Seg_1785" s="T218">onton</ta>
            <ta e="T220" id="Seg_1786" s="T219">kojut</ta>
            <ta e="T221" id="Seg_1787" s="T220">kas</ta>
            <ta e="T222" id="Seg_1788" s="T221">biːr</ta>
            <ta e="T223" id="Seg_1789" s="T222">kas</ta>
            <ta e="T224" id="Seg_1790" s="T223">da</ta>
            <ta e="T225" id="Seg_1791" s="T224">nedelʼa</ta>
            <ta e="T226" id="Seg_1792" s="T225">aːs-pɨt-a</ta>
            <ta e="T227" id="Seg_1793" s="T226">össü͡ö</ta>
            <ta e="T228" id="Seg_1794" s="T227">ikk-is-teri-n</ta>
            <ta e="T232" id="Seg_1795" s="T231">bar-bɨp-pɨt</ta>
            <ta e="T234" id="Seg_1796" s="T233">on-ton</ta>
            <ta e="T235" id="Seg_1797" s="T234">onno</ta>
            <ta e="T236" id="Seg_1798" s="T235">bar-bɨp-pɨt</ta>
            <ta e="T237" id="Seg_1799" s="T236">hol</ta>
            <ta e="T238" id="Seg_1800" s="T237">mesto-bɨtɨ-gar</ta>
            <ta e="T240" id="Seg_1801" s="T239">ol</ta>
            <ta e="T241" id="Seg_1802" s="T240">mesto-ga</ta>
            <ta e="T242" id="Seg_1803" s="T241">onno</ta>
            <ta e="T243" id="Seg_1804" s="T242">kim</ta>
            <ta e="T244" id="Seg_1805" s="T243">baːr</ta>
            <ta e="T248" id="Seg_1806" s="T247">baːr</ta>
            <ta e="T249" id="Seg_1807" s="T248">e-t-e</ta>
            <ta e="T250" id="Seg_1808" s="T249">bu͡o</ta>
            <ta e="T251" id="Seg_1809" s="T250">ol</ta>
            <ta e="T252" id="Seg_1810" s="T251">zaːjčik</ta>
            <ta e="T253" id="Seg_1811" s="T252">iːk-t-i-ll-i-bit</ta>
            <ta e="T254" id="Seg_1812" s="T253">e-t-e</ta>
            <ta e="T255" id="Seg_1813" s="T254">iːk-teːk</ta>
            <ta e="T256" id="Seg_1814" s="T255">e-t-e</ta>
            <ta e="T258" id="Seg_1815" s="T257">a</ta>
            <ta e="T259" id="Seg_1816" s="T258">kim</ta>
            <ta e="T260" id="Seg_1817" s="T259">da</ta>
            <ta e="T261" id="Seg_1818" s="T260">kurpaːskɨ</ta>
            <ta e="T262" id="Seg_1819" s="T261">menʼiː-ti-n</ta>
            <ta e="T263" id="Seg_1820" s="T262">kim</ta>
            <ta e="T264" id="Seg_1821" s="T263">ere</ta>
            <ta e="T265" id="Seg_1822" s="T264">tur-but</ta>
            <ta e="T266" id="Seg_1823" s="T265">e-ti-bit</ta>
            <ta e="T268" id="Seg_1824" s="T267">on-ton</ta>
            <ta e="T269" id="Seg_1825" s="T268">kojut</ta>
            <ta e="T270" id="Seg_1826" s="T269">bihigi</ta>
            <ta e="T271" id="Seg_1827" s="T270">katatsa-l-ɨː</ta>
            <ta e="T272" id="Seg_1828" s="T271">bar-bɨp-pɨt</ta>
            <ta e="T273" id="Seg_1829" s="T272">a</ta>
            <ta e="T274" id="Seg_1830" s="T273">kim</ta>
            <ta e="T275" id="Seg_1831" s="T274">de</ta>
            <ta e="T276" id="Seg_1832" s="T275">eː</ta>
            <ta e="T277" id="Seg_1833" s="T276">dʼadʼa</ta>
            <ta e="T278" id="Seg_1834" s="T277">Prokula</ta>
            <ta e="T279" id="Seg_1835" s="T278">ogo-lor-u</ta>
            <ta e="T280" id="Seg_1836" s="T279">ɨl-pɨt</ta>
            <ta e="T281" id="Seg_1837" s="T280">e-ti-lere</ta>
            <ta e="T282" id="Seg_1838" s="T281">eː</ta>
            <ta e="T283" id="Seg_1839" s="T282">hu͡ok</ta>
            <ta e="T284" id="Seg_1840" s="T283">ol</ta>
            <ta e="T285" id="Seg_1841" s="T284">atɨn</ta>
            <ta e="T286" id="Seg_1842" s="T285">kimi͡e-ke</ta>
            <ta e="T288" id="Seg_1843" s="T287">bihigi</ta>
            <ta e="T289" id="Seg_1844" s="T288">katatsa-l-ɨː</ta>
            <ta e="T290" id="Seg_1845" s="T289">bar-bɨp-pɨt</ta>
            <ta e="T291" id="Seg_1846" s="T290">on-ton</ta>
            <ta e="T292" id="Seg_1847" s="T291">töttörü</ta>
            <ta e="T293" id="Seg_1848" s="T292">kel-ti-bit</ta>
            <ta e="T294" id="Seg_1849" s="T293">kim-ŋe</ta>
            <ta e="T295" id="Seg_1850" s="T294">poselok-ka</ta>
            <ta e="T296" id="Seg_1851" s="T295">lɨːža-lar-daːk</ta>
            <ta e="T297" id="Seg_1852" s="T296">on-ton</ta>
            <ta e="T298" id="Seg_1853" s="T297">kel-ti-bit</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1854" s="T1">biːrde</ta>
            <ta e="T3" id="Seg_1855" s="T2">bihigi</ta>
            <ta e="T4" id="Seg_1856" s="T3">Regina-nI</ta>
            <ta e="T5" id="Seg_1857" s="T4">kɨtta</ta>
            <ta e="T6" id="Seg_1858" s="T5">hɨrɨt-Ar</ta>
            <ta e="T7" id="Seg_1859" s="T6">e-TI-BIt</ta>
            <ta e="T9" id="Seg_1860" s="T8">ol-ttAn</ta>
            <ta e="T10" id="Seg_1861" s="T9">onno</ta>
            <ta e="T11" id="Seg_1862" s="T10">tʼotʼa</ta>
            <ta e="T12" id="Seg_1863" s="T11">eː</ta>
            <ta e="T13" id="Seg_1864" s="T12">bihigi</ta>
            <ta e="T14" id="Seg_1865" s="T13">hɨrɨt-Ar</ta>
            <ta e="T15" id="Seg_1866" s="T14">e-TI-BIt</ta>
            <ta e="T16" id="Seg_1867" s="T15">ogo-nI</ta>
            <ta e="T17" id="Seg_1868" s="T16">karaj-Ar</ta>
            <ta e="T18" id="Seg_1869" s="T17">e-TI-BIt</ta>
            <ta e="T20" id="Seg_1870" s="T19">ol-ttAn</ta>
            <ta e="T21" id="Seg_1871" s="T20">Kira</ta>
            <ta e="T22" id="Seg_1872" s="T21">maːma-tA</ta>
            <ta e="T23" id="Seg_1873" s="T22">ɨgɨr-BIT-tA</ta>
            <ta e="T24" id="Seg_1874" s="T23">min-n</ta>
            <ta e="T25" id="Seg_1875" s="T24">katatsa-LAː-A</ta>
            <ta e="T26" id="Seg_1876" s="T25">bar-A-GIt</ta>
            <ta e="T27" id="Seg_1877" s="T26">du͡o</ta>
            <ta e="T28" id="Seg_1878" s="T27">tɨ͡a-GA</ta>
            <ta e="T29" id="Seg_1879" s="T28">bihigi</ta>
            <ta e="T30" id="Seg_1880" s="T29">eː</ta>
            <ta e="T31" id="Seg_1881" s="T30">katatsa-LAː-A</ta>
            <ta e="T32" id="Seg_1882" s="T31">bar-An</ta>
            <ta e="T33" id="Seg_1883" s="T32">du͡o</ta>
            <ta e="T34" id="Seg_1884" s="T33">min-n</ta>
            <ta e="T35" id="Seg_1885" s="T34">kɨtta</ta>
            <ta e="T36" id="Seg_1886" s="T35">tɨ͡a-GA</ta>
            <ta e="T38" id="Seg_1887" s="T37">ol-ttAn</ta>
            <ta e="T39" id="Seg_1888" s="T38">min</ta>
            <ta e="T40" id="Seg_1889" s="T39">di͡e-BIT-I-m</ta>
            <ta e="T41" id="Seg_1890" s="T40">eː</ta>
            <ta e="T42" id="Seg_1891" s="T41">ol-ttAn</ta>
            <ta e="T43" id="Seg_1892" s="T42">bihigi</ta>
            <ta e="T44" id="Seg_1893" s="T43">bar-TI-BIt</ta>
            <ta e="T46" id="Seg_1894" s="T45">a</ta>
            <ta e="T47" id="Seg_1895" s="T46">Polina</ta>
            <ta e="T48" id="Seg_1896" s="T47">Alekseevna-LAːK</ta>
            <ta e="T49" id="Seg_1897" s="T48">beje-LArI-n</ta>
            <ta e="T301" id="Seg_1898" s="T49">semja-LArI-n</ta>
            <ta e="T50" id="Seg_1899" s="T301">kɨtta</ta>
            <ta e="T51" id="Seg_1900" s="T50">kaːl-BIT</ta>
            <ta e="T52" id="Seg_1901" s="T51">e-TI-LArA</ta>
            <ta e="T303" id="Seg_1902" s="T53">a</ta>
            <ta e="T302" id="Seg_1903" s="T54">kel-BIT</ta>
            <ta e="T55" id="Seg_1904" s="T302">kel-Ar</ta>
            <ta e="T56" id="Seg_1905" s="T55">tur-Ar</ta>
            <ta e="T57" id="Seg_1906" s="T56">e-TI-LArA</ta>
            <ta e="T58" id="Seg_1907" s="T57">a</ta>
            <ta e="T59" id="Seg_1908" s="T58">tʼotʼa</ta>
            <ta e="T60" id="Seg_1909" s="T59">Naːsta</ta>
            <ta e="T61" id="Seg_1910" s="T60">giniler-I-n</ta>
            <ta e="T62" id="Seg_1911" s="T61">ɨl-TAr-Ar</ta>
            <ta e="T63" id="Seg_1912" s="T62">e-TI-tA</ta>
            <ta e="T65" id="Seg_1913" s="T64">anɨ</ta>
            <ta e="T66" id="Seg_1914" s="T65">kim-LAr-nI</ta>
            <ta e="T67" id="Seg_1915" s="T66">tu͡ok</ta>
            <ta e="T68" id="Seg_1916" s="T67">di͡e-AːččI-LAr</ta>
            <ta e="T69" id="Seg_1917" s="T68">burančik</ta>
            <ta e="T70" id="Seg_1918" s="T69">burančik-LAːK-LAr</ta>
            <ta e="T71" id="Seg_1919" s="T70">tu͡ok-TA</ta>
            <ta e="T73" id="Seg_1920" s="T72">ol-ttAn</ta>
            <ta e="T74" id="Seg_1921" s="T73">giniler</ta>
            <ta e="T75" id="Seg_1922" s="T74">kel-BIT-LArA</ta>
            <ta e="T76" id="Seg_1923" s="T75">Polina</ta>
            <ta e="T77" id="Seg_1924" s="T76">Alekseevna-LAːK</ta>
            <ta e="T78" id="Seg_1925" s="T77">ol-ttAn</ta>
            <ta e="T79" id="Seg_1926" s="T78">bihigi</ta>
            <ta e="T80" id="Seg_1927" s="T79">onno</ta>
            <ta e="T81" id="Seg_1928" s="T80">že</ta>
            <ta e="T82" id="Seg_1929" s="T81">dalʼše</ta>
            <ta e="T83" id="Seg_1930" s="T82">bar-BIT-BIt</ta>
            <ta e="T85" id="Seg_1931" s="T84">ol-ttAn</ta>
            <ta e="T86" id="Seg_1932" s="T85">onno</ta>
            <ta e="T87" id="Seg_1933" s="T86">kim</ta>
            <ta e="T88" id="Seg_1934" s="T87">kim-LAr-nI</ta>
            <ta e="T89" id="Seg_1935" s="T88">bu</ta>
            <ta e="T90" id="Seg_1936" s="T89">ka</ta>
            <ta e="T91" id="Seg_1937" s="T90">toktoː-BIT-BIt</ta>
            <ta e="T92" id="Seg_1938" s="T91">ol-ttAn</ta>
            <ta e="T93" id="Seg_1939" s="T92">onno</ta>
            <ta e="T94" id="Seg_1940" s="T93">kim</ta>
            <ta e="T96" id="Seg_1941" s="T95">zaːjčik</ta>
            <ta e="T97" id="Seg_1942" s="T96">kim-tI-n</ta>
            <ta e="T98" id="Seg_1943" s="T97">üːt-tI-n</ta>
            <ta e="T99" id="Seg_1944" s="T98">kör-BIT</ta>
            <ta e="T100" id="Seg_1945" s="T99">e-TI-BIt</ta>
            <ta e="T102" id="Seg_1946" s="T101">ol-ttAn</ta>
            <ta e="T103" id="Seg_1947" s="T102">katatsa-LAː</ta>
            <ta e="T104" id="Seg_1948" s="T103">katatsa-LAː-BIT-BIt</ta>
            <ta e="T106" id="Seg_1949" s="T105">ulakan</ta>
            <ta e="T107" id="Seg_1950" s="T106">bagajɨ</ta>
            <ta e="T108" id="Seg_1951" s="T107">gorka</ta>
            <ta e="T109" id="Seg_1952" s="T108">e-TI-tA</ta>
            <ta e="T110" id="Seg_1953" s="T109">onno</ta>
            <ta e="T111" id="Seg_1954" s="T110">bihigi</ta>
            <ta e="T112" id="Seg_1955" s="T111">že</ta>
            <ta e="T113" id="Seg_1956" s="T112">zamečaj-LAː-BAT</ta>
            <ta e="T114" id="Seg_1957" s="T113">e-TI-BIt</ta>
            <ta e="T115" id="Seg_1958" s="T114">onno</ta>
            <ta e="T118" id="Seg_1959" s="T117">e-TI-tA</ta>
            <ta e="T119" id="Seg_1960" s="T118">tu͡ok</ta>
            <ta e="T120" id="Seg_1961" s="T119">da</ta>
            <ta e="T121" id="Seg_1962" s="T120">hu͡ok</ta>
            <ta e="T122" id="Seg_1963" s="T121">e-TI-tA</ta>
            <ta e="T123" id="Seg_1964" s="T122">köhün-I-BAT</ta>
            <ta e="T125" id="Seg_1965" s="T124">ol-ttAn</ta>
            <ta e="T126" id="Seg_1966" s="T125">katatsa-LAː-BIT-BIt</ta>
            <ta e="T127" id="Seg_1967" s="T126">ol-ttAn</ta>
            <ta e="T128" id="Seg_1968" s="T127">onno</ta>
            <ta e="T129" id="Seg_1969" s="T128">kim</ta>
            <ta e="T130" id="Seg_1970" s="T129">Polina</ta>
            <ta e="T131" id="Seg_1971" s="T130">Alekseevna</ta>
            <ta e="T132" id="Seg_1972" s="T131">baːr</ta>
            <ta e="T133" id="Seg_1973" s="T132">e-TI-tA</ta>
            <ta e="T134" id="Seg_1974" s="T133">semja-tI-n</ta>
            <ta e="T135" id="Seg_1975" s="T134">kɨtta</ta>
            <ta e="T136" id="Seg_1976" s="T135">ol-ttAn</ta>
            <ta e="T137" id="Seg_1977" s="T136">tʼotʼa</ta>
            <ta e="T138" id="Seg_1978" s="T137">Naːsta</ta>
            <ta e="T139" id="Seg_1979" s="T138">ol-ttAn</ta>
            <ta e="T140" id="Seg_1980" s="T139">Nʼukuːska</ta>
            <ta e="T141" id="Seg_1981" s="T140">Ludmila</ta>
            <ta e="T142" id="Seg_1982" s="T141">Nʼikalajevna</ta>
            <ta e="T143" id="Seg_1983" s="T142">ogo-tA</ta>
            <ta e="T144" id="Seg_1984" s="T143">ol-ttAn</ta>
            <ta e="T145" id="Seg_1985" s="T144">Annuška</ta>
            <ta e="T146" id="Seg_1986" s="T145">tʼotʼa</ta>
            <ta e="T147" id="Seg_1987" s="T146">Naːsta</ta>
            <ta e="T149" id="Seg_1988" s="T148">ol-ttAn</ta>
            <ta e="T150" id="Seg_1989" s="T149">bihigi</ta>
            <ta e="T151" id="Seg_1990" s="T150">katatsa-LAː-BIT-BIt</ta>
            <ta e="T153" id="Seg_1991" s="T152">ol-ttAn</ta>
            <ta e="T154" id="Seg_1992" s="T153">kim</ta>
            <ta e="T155" id="Seg_1993" s="T154">e-TI-tA</ta>
            <ta e="T156" id="Seg_1994" s="T155">kim</ta>
            <ta e="T157" id="Seg_1995" s="T156">e-TI-tA</ta>
            <ta e="T158" id="Seg_1996" s="T157">eː</ta>
            <ta e="T159" id="Seg_1997" s="T158">dʼadʼa</ta>
            <ta e="T160" id="Seg_1998" s="T159">Parako</ta>
            <ta e="T161" id="Seg_1999" s="T160">ogo-LAr-GA</ta>
            <ta e="T162" id="Seg_2000" s="T161">kim</ta>
            <ta e="T163" id="Seg_2001" s="T162">oŋor-Ar</ta>
            <ta e="T164" id="Seg_2002" s="T163">e-TI-tA</ta>
            <ta e="T165" id="Seg_2003" s="T164">kim</ta>
            <ta e="T166" id="Seg_2004" s="T165">e-TI-tA</ta>
            <ta e="T167" id="Seg_2005" s="T166">ka</ta>
            <ta e="T168" id="Seg_2006" s="T167">figurka-LAr-nI</ta>
            <ta e="T170" id="Seg_2007" s="T169">ol-ttAn</ta>
            <ta e="T300" id="Seg_2008" s="T170">ka</ta>
            <ta e="T172" id="Seg_2009" s="T171">eː</ta>
            <ta e="T173" id="Seg_2010" s="T172">Kira</ta>
            <ta e="T174" id="Seg_2011" s="T173">kim-tA</ta>
            <ta e="T175" id="Seg_2012" s="T174">figurka-tA</ta>
            <ta e="T176" id="Seg_2013" s="T175">zaːjčik</ta>
            <ta e="T177" id="Seg_2014" s="T176">e-TI-tA</ta>
            <ta e="T178" id="Seg_2015" s="T177">a</ta>
            <ta e="T179" id="Seg_2016" s="T178">kim</ta>
            <ta e="T180" id="Seg_2017" s="T179">gi͡en-tA</ta>
            <ta e="T181" id="Seg_2018" s="T180">kim</ta>
            <ta e="T182" id="Seg_2019" s="T181">e-TI-tA</ta>
            <ta e="T183" id="Seg_2020" s="T182">kim</ta>
            <ta e="T184" id="Seg_2021" s="T183">gi͡en-tA</ta>
            <ta e="T185" id="Seg_2022" s="T184">Nadjex</ta>
            <ta e="T186" id="Seg_2023" s="T185">gi͡en-tA</ta>
            <ta e="T187" id="Seg_2024" s="T186">kim</ta>
            <ta e="T188" id="Seg_2025" s="T187">meːne</ta>
            <ta e="T189" id="Seg_2026" s="T188">kurpaːskɨ</ta>
            <ta e="T190" id="Seg_2027" s="T189">e-TI-tA</ta>
            <ta e="T191" id="Seg_2028" s="T190">ol-ttAn</ta>
            <ta e="T192" id="Seg_2029" s="T191">uːr-BIT</ta>
            <ta e="T193" id="Seg_2030" s="T192">e-TI-LArA</ta>
            <ta e="T194" id="Seg_2031" s="T193">onno</ta>
            <ta e="T195" id="Seg_2032" s="T194">ikki-IAn-LArA</ta>
            <ta e="T196" id="Seg_2033" s="T195">a</ta>
            <ta e="T197" id="Seg_2034" s="T196">kurpaːskɨ-nI</ta>
            <ta e="T198" id="Seg_2035" s="T197">kim-GA</ta>
            <ta e="T199" id="Seg_2036" s="T198">uːr-BIT</ta>
            <ta e="T200" id="Seg_2037" s="T199">e-TI-LArA</ta>
            <ta e="T201" id="Seg_2038" s="T200">kim</ta>
            <ta e="T202" id="Seg_2039" s="T201">e-TI-tA=Ij</ta>
            <ta e="T203" id="Seg_2040" s="T202">ka</ta>
            <ta e="T204" id="Seg_2041" s="T203">zaːjčik</ta>
            <ta e="T205" id="Seg_2042" s="T204">kim-tI-GAr</ta>
            <ta e="T206" id="Seg_2043" s="T205">onton</ta>
            <ta e="T207" id="Seg_2044" s="T206">iti</ta>
            <ta e="T208" id="Seg_2045" s="T207">kim</ta>
            <ta e="T209" id="Seg_2046" s="T208">nu</ta>
            <ta e="T210" id="Seg_2047" s="T209">ka</ta>
            <ta e="T211" id="Seg_2048" s="T210">jamka-tI-GAr</ta>
            <ta e="T213" id="Seg_2049" s="T212">ol-ttAn</ta>
            <ta e="T214" id="Seg_2050" s="T213">onno</ta>
            <ta e="T215" id="Seg_2051" s="T214">bihigi</ta>
            <ta e="T216" id="Seg_2052" s="T215">ol-ttAn</ta>
            <ta e="T217" id="Seg_2053" s="T216">onno</ta>
            <ta e="T219" id="Seg_2054" s="T218">onton</ta>
            <ta e="T220" id="Seg_2055" s="T219">kojut</ta>
            <ta e="T221" id="Seg_2056" s="T220">kas</ta>
            <ta e="T222" id="Seg_2057" s="T221">biːr</ta>
            <ta e="T223" id="Seg_2058" s="T222">kas</ta>
            <ta e="T224" id="Seg_2059" s="T223">da</ta>
            <ta e="T225" id="Seg_2060" s="T224">nedi͡ele</ta>
            <ta e="T226" id="Seg_2061" s="T225">aːs-BIT-tA</ta>
            <ta e="T227" id="Seg_2062" s="T226">össü͡ö</ta>
            <ta e="T228" id="Seg_2063" s="T227">ikki-Is-LArI-n</ta>
            <ta e="T232" id="Seg_2064" s="T231">bar-BIT-BIt</ta>
            <ta e="T234" id="Seg_2065" s="T233">ol-ttAn</ta>
            <ta e="T235" id="Seg_2066" s="T234">onno</ta>
            <ta e="T236" id="Seg_2067" s="T235">bar-BIT-BIt</ta>
            <ta e="T237" id="Seg_2068" s="T236">hol</ta>
            <ta e="T238" id="Seg_2069" s="T237">mesto-BItI-GAr</ta>
            <ta e="T240" id="Seg_2070" s="T239">ol</ta>
            <ta e="T241" id="Seg_2071" s="T240">mesto-GA</ta>
            <ta e="T242" id="Seg_2072" s="T241">onno</ta>
            <ta e="T243" id="Seg_2073" s="T242">kim</ta>
            <ta e="T244" id="Seg_2074" s="T243">baːr</ta>
            <ta e="T248" id="Seg_2075" s="T247">baːr</ta>
            <ta e="T249" id="Seg_2076" s="T248">e-TI-tA</ta>
            <ta e="T250" id="Seg_2077" s="T249">bu͡o</ta>
            <ta e="T251" id="Seg_2078" s="T250">ol</ta>
            <ta e="T252" id="Seg_2079" s="T251">zaːjčik</ta>
            <ta e="T253" id="Seg_2080" s="T252">iːk-LAː-I-LIN-I-BIT</ta>
            <ta e="T254" id="Seg_2081" s="T253">e-TI-tA</ta>
            <ta e="T255" id="Seg_2082" s="T254">iːk-LAːK</ta>
            <ta e="T256" id="Seg_2083" s="T255">e-TI-tA</ta>
            <ta e="T258" id="Seg_2084" s="T257">a</ta>
            <ta e="T259" id="Seg_2085" s="T258">kim</ta>
            <ta e="T260" id="Seg_2086" s="T259">da</ta>
            <ta e="T261" id="Seg_2087" s="T260">kurpaːskɨ</ta>
            <ta e="T262" id="Seg_2088" s="T261">menʼiː-tI-n</ta>
            <ta e="T263" id="Seg_2089" s="T262">kim</ta>
            <ta e="T264" id="Seg_2090" s="T263">ere</ta>
            <ta e="T265" id="Seg_2091" s="T264">tur-BIT</ta>
            <ta e="T266" id="Seg_2092" s="T265">e-TI-BIt</ta>
            <ta e="T268" id="Seg_2093" s="T267">ol-ttAn</ta>
            <ta e="T269" id="Seg_2094" s="T268">kojut</ta>
            <ta e="T270" id="Seg_2095" s="T269">bihigi</ta>
            <ta e="T271" id="Seg_2096" s="T270">katatsa-LAː-A</ta>
            <ta e="T272" id="Seg_2097" s="T271">bar-BIT-BIt</ta>
            <ta e="T273" id="Seg_2098" s="T272">a</ta>
            <ta e="T274" id="Seg_2099" s="T273">kim</ta>
            <ta e="T275" id="Seg_2100" s="T274">da</ta>
            <ta e="T276" id="Seg_2101" s="T275">eː</ta>
            <ta e="T277" id="Seg_2102" s="T276">dʼadʼa</ta>
            <ta e="T278" id="Seg_2103" s="T277">Parako</ta>
            <ta e="T279" id="Seg_2104" s="T278">ogo-LAr-nI</ta>
            <ta e="T280" id="Seg_2105" s="T279">ɨl-BIT</ta>
            <ta e="T281" id="Seg_2106" s="T280">e-TI-LArA</ta>
            <ta e="T282" id="Seg_2107" s="T281">eː</ta>
            <ta e="T283" id="Seg_2108" s="T282">hu͡ok</ta>
            <ta e="T284" id="Seg_2109" s="T283">ol</ta>
            <ta e="T285" id="Seg_2110" s="T284">atɨn</ta>
            <ta e="T286" id="Seg_2111" s="T285">kim-GA</ta>
            <ta e="T288" id="Seg_2112" s="T287">bihigi</ta>
            <ta e="T289" id="Seg_2113" s="T288">katatsa-LAː-A</ta>
            <ta e="T290" id="Seg_2114" s="T289">bar-BIT-BIt</ta>
            <ta e="T291" id="Seg_2115" s="T290">ol-ttAn</ta>
            <ta e="T292" id="Seg_2116" s="T291">töttörü</ta>
            <ta e="T293" id="Seg_2117" s="T292">kel-TI-BIt</ta>
            <ta e="T294" id="Seg_2118" s="T293">kim-GA</ta>
            <ta e="T295" id="Seg_2119" s="T294">poselok-GA</ta>
            <ta e="T296" id="Seg_2120" s="T295">lɨːža-LAr-LAːK</ta>
            <ta e="T297" id="Seg_2121" s="T296">ol-ttAn</ta>
            <ta e="T298" id="Seg_2122" s="T297">kel-TI-BIt</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_2123" s="T1">once</ta>
            <ta e="T3" id="Seg_2124" s="T2">1PL.[NOM]</ta>
            <ta e="T4" id="Seg_2125" s="T3">Regina-ACC</ta>
            <ta e="T5" id="Seg_2126" s="T4">with</ta>
            <ta e="T6" id="Seg_2127" s="T5">go-PTCP.PRS</ta>
            <ta e="T7" id="Seg_2128" s="T6">be-PST1-1PL</ta>
            <ta e="T9" id="Seg_2129" s="T8">that-ABL</ta>
            <ta e="T10" id="Seg_2130" s="T9">there</ta>
            <ta e="T11" id="Seg_2131" s="T10">aunt.[NOM]</ta>
            <ta e="T12" id="Seg_2132" s="T11">eh</ta>
            <ta e="T13" id="Seg_2133" s="T12">1PL.[NOM]</ta>
            <ta e="T14" id="Seg_2134" s="T13">go-PTCP.PRS</ta>
            <ta e="T15" id="Seg_2135" s="T14">be-PST1-1PL</ta>
            <ta e="T16" id="Seg_2136" s="T15">child-ACC</ta>
            <ta e="T17" id="Seg_2137" s="T16">care.about-PTCP.PRS</ta>
            <ta e="T18" id="Seg_2138" s="T17">be-PST1-1PL</ta>
            <ta e="T20" id="Seg_2139" s="T19">that-ABL</ta>
            <ta e="T21" id="Seg_2140" s="T20">Kira.[NOM]</ta>
            <ta e="T22" id="Seg_2141" s="T21">mum-3SG.[NOM]</ta>
            <ta e="T23" id="Seg_2142" s="T22">call-PST2-3SG</ta>
            <ta e="T24" id="Seg_2143" s="T23">1SG-ACC</ta>
            <ta e="T25" id="Seg_2144" s="T24">ride-VBZ-CVB.SIM</ta>
            <ta e="T26" id="Seg_2145" s="T25">go-PRS-2PL</ta>
            <ta e="T27" id="Seg_2146" s="T26">Q</ta>
            <ta e="T28" id="Seg_2147" s="T27">tundra-DAT/LOC</ta>
            <ta e="T29" id="Seg_2148" s="T28">1PL.[NOM]</ta>
            <ta e="T30" id="Seg_2149" s="T29">eh</ta>
            <ta e="T31" id="Seg_2150" s="T30">ride-VBZ-CVB.SIM</ta>
            <ta e="T32" id="Seg_2151" s="T31">go-CVB.SEQ</ta>
            <ta e="T33" id="Seg_2152" s="T32">Q</ta>
            <ta e="T34" id="Seg_2153" s="T33">1SG-ACC</ta>
            <ta e="T35" id="Seg_2154" s="T34">with</ta>
            <ta e="T36" id="Seg_2155" s="T35">tundra-DAT/LOC</ta>
            <ta e="T38" id="Seg_2156" s="T37">that-ABL</ta>
            <ta e="T39" id="Seg_2157" s="T38">1SG.[NOM]</ta>
            <ta e="T40" id="Seg_2158" s="T39">say-PST2-EP-1SG</ta>
            <ta e="T41" id="Seg_2159" s="T40">AFFIRM</ta>
            <ta e="T42" id="Seg_2160" s="T41">that-ABL</ta>
            <ta e="T43" id="Seg_2161" s="T42">1PL.[NOM]</ta>
            <ta e="T44" id="Seg_2162" s="T43">go-PST1-1PL</ta>
            <ta e="T46" id="Seg_2163" s="T45">and</ta>
            <ta e="T47" id="Seg_2164" s="T46">Polina</ta>
            <ta e="T48" id="Seg_2165" s="T47">Alekseevna-PROPR</ta>
            <ta e="T49" id="Seg_2166" s="T48">self-3PL-ACC</ta>
            <ta e="T301" id="Seg_2167" s="T49">family-3PL-ACC</ta>
            <ta e="T50" id="Seg_2168" s="T301">with</ta>
            <ta e="T51" id="Seg_2169" s="T50">stay-PTCP.PST</ta>
            <ta e="T52" id="Seg_2170" s="T51">be-PST1-3PL</ta>
            <ta e="T303" id="Seg_2171" s="T53">and</ta>
            <ta e="T302" id="Seg_2172" s="T54">come-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_2173" s="T302">come-PTCP.PRS</ta>
            <ta e="T56" id="Seg_2174" s="T55">stand-PTCP.PRS</ta>
            <ta e="T57" id="Seg_2175" s="T56">be-PST1-3PL</ta>
            <ta e="T58" id="Seg_2176" s="T57">and</ta>
            <ta e="T59" id="Seg_2177" s="T58">aunt</ta>
            <ta e="T60" id="Seg_2178" s="T59">Nastya.[NOM]</ta>
            <ta e="T61" id="Seg_2179" s="T60">3PL-EP-ACC</ta>
            <ta e="T62" id="Seg_2180" s="T61">take-CAUS-PTCP.PRS</ta>
            <ta e="T63" id="Seg_2181" s="T62">be-PST1-3SG</ta>
            <ta e="T65" id="Seg_2182" s="T64">now</ta>
            <ta e="T66" id="Seg_2183" s="T65">who-PL-ACC</ta>
            <ta e="T67" id="Seg_2184" s="T66">what.[NOM]</ta>
            <ta e="T68" id="Seg_2185" s="T67">say-HAB-3PL</ta>
            <ta e="T69" id="Seg_2186" s="T68">snow.scooter</ta>
            <ta e="T70" id="Seg_2187" s="T69">snow.scooter-PROPR-3PL</ta>
            <ta e="T71" id="Seg_2188" s="T70">what-PART</ta>
            <ta e="T73" id="Seg_2189" s="T72">that-ABL</ta>
            <ta e="T74" id="Seg_2190" s="T73">3PL.[NOM]</ta>
            <ta e="T75" id="Seg_2191" s="T74">come-PST2-3PL</ta>
            <ta e="T76" id="Seg_2192" s="T75">Polina</ta>
            <ta e="T77" id="Seg_2193" s="T76">Alekseevna-PROPR.[NOM]</ta>
            <ta e="T78" id="Seg_2194" s="T77">that-ABL</ta>
            <ta e="T79" id="Seg_2195" s="T78">1PL.[NOM]</ta>
            <ta e="T80" id="Seg_2196" s="T79">there</ta>
            <ta e="T81" id="Seg_2197" s="T80">EMPH</ta>
            <ta e="T82" id="Seg_2198" s="T81">further</ta>
            <ta e="T83" id="Seg_2199" s="T82">go-PST2-1PL</ta>
            <ta e="T85" id="Seg_2200" s="T84">that-ABL</ta>
            <ta e="T86" id="Seg_2201" s="T85">there</ta>
            <ta e="T87" id="Seg_2202" s="T86">who.[NOM]</ta>
            <ta e="T88" id="Seg_2203" s="T87">who-PL-ACC</ta>
            <ta e="T89" id="Seg_2204" s="T88">this</ta>
            <ta e="T90" id="Seg_2205" s="T89">well</ta>
            <ta e="T91" id="Seg_2206" s="T90">stop-PST2-1PL</ta>
            <ta e="T92" id="Seg_2207" s="T91">that-ABL</ta>
            <ta e="T93" id="Seg_2208" s="T92">there</ta>
            <ta e="T94" id="Seg_2209" s="T93">who.[NOM]</ta>
            <ta e="T96" id="Seg_2210" s="T95">hare.[NOM]</ta>
            <ta e="T97" id="Seg_2211" s="T96">who-3SG-ACC</ta>
            <ta e="T98" id="Seg_2212" s="T97">hole-3SG-ACC</ta>
            <ta e="T99" id="Seg_2213" s="T98">see-PTCP.PST</ta>
            <ta e="T100" id="Seg_2214" s="T99">be-PST1-1PL</ta>
            <ta e="T102" id="Seg_2215" s="T101">that-ABL</ta>
            <ta e="T103" id="Seg_2216" s="T102">ride-VBZ</ta>
            <ta e="T104" id="Seg_2217" s="T103">ride-VBZ-PST2-1PL</ta>
            <ta e="T106" id="Seg_2218" s="T105">big</ta>
            <ta e="T107" id="Seg_2219" s="T106">very</ta>
            <ta e="T108" id="Seg_2220" s="T107">hill.[NOM]</ta>
            <ta e="T109" id="Seg_2221" s="T108">be-PST1-3SG</ta>
            <ta e="T110" id="Seg_2222" s="T109">there</ta>
            <ta e="T111" id="Seg_2223" s="T110">1PL.[NOM]</ta>
            <ta e="T112" id="Seg_2224" s="T111">EMPH</ta>
            <ta e="T113" id="Seg_2225" s="T112">notice-VBZ-NEG.PTCP</ta>
            <ta e="T114" id="Seg_2226" s="T113">be-PST1-1PL</ta>
            <ta e="T115" id="Seg_2227" s="T114">there</ta>
            <ta e="T118" id="Seg_2228" s="T117">be-PST1-3SG</ta>
            <ta e="T119" id="Seg_2229" s="T118">what.[NOM]</ta>
            <ta e="T120" id="Seg_2230" s="T119">NEG</ta>
            <ta e="T121" id="Seg_2231" s="T120">NEG</ta>
            <ta e="T122" id="Seg_2232" s="T121">be-PST1-3SG</ta>
            <ta e="T123" id="Seg_2233" s="T122">to.be.on.view-EP-NEG.PTCP</ta>
            <ta e="T125" id="Seg_2234" s="T124">that-ABL</ta>
            <ta e="T126" id="Seg_2235" s="T125">ride-VBZ-PST2-1PL</ta>
            <ta e="T127" id="Seg_2236" s="T126">that-ABL</ta>
            <ta e="T128" id="Seg_2237" s="T127">there</ta>
            <ta e="T129" id="Seg_2238" s="T128">who.[NOM]</ta>
            <ta e="T130" id="Seg_2239" s="T129">Polina</ta>
            <ta e="T131" id="Seg_2240" s="T130">Alekseevna.[NOM]</ta>
            <ta e="T132" id="Seg_2241" s="T131">there.is</ta>
            <ta e="T133" id="Seg_2242" s="T132">be-PST1-3SG</ta>
            <ta e="T134" id="Seg_2243" s="T133">family-3SG-ACC</ta>
            <ta e="T135" id="Seg_2244" s="T134">with</ta>
            <ta e="T136" id="Seg_2245" s="T135">that-ABL</ta>
            <ta e="T137" id="Seg_2246" s="T136">aunt</ta>
            <ta e="T138" id="Seg_2247" s="T137">Nastya.[NOM]</ta>
            <ta e="T139" id="Seg_2248" s="T138">that-ABL</ta>
            <ta e="T140" id="Seg_2249" s="T139">Nicolas.[NOM]</ta>
            <ta e="T141" id="Seg_2250" s="T140">Lyudmila</ta>
            <ta e="T142" id="Seg_2251" s="T141">Nikolaevna.[NOM]</ta>
            <ta e="T143" id="Seg_2252" s="T142">child-3SG.[NOM]</ta>
            <ta e="T144" id="Seg_2253" s="T143">that-ABL</ta>
            <ta e="T145" id="Seg_2254" s="T144">Annushka</ta>
            <ta e="T146" id="Seg_2255" s="T145">aunt</ta>
            <ta e="T147" id="Seg_2256" s="T146">Nastya.[NOM]</ta>
            <ta e="T149" id="Seg_2257" s="T148">that-ABL</ta>
            <ta e="T150" id="Seg_2258" s="T149">1PL.[NOM]</ta>
            <ta e="T151" id="Seg_2259" s="T150">ride-VBZ-PST2-1PL</ta>
            <ta e="T153" id="Seg_2260" s="T152">that-ABL</ta>
            <ta e="T154" id="Seg_2261" s="T153">who.[NOM]</ta>
            <ta e="T155" id="Seg_2262" s="T154">be-PST1-3SG</ta>
            <ta e="T156" id="Seg_2263" s="T155">who.[NOM]</ta>
            <ta e="T157" id="Seg_2264" s="T156">be-PST1-3SG</ta>
            <ta e="T158" id="Seg_2265" s="T157">eh</ta>
            <ta e="T159" id="Seg_2266" s="T158">uncle</ta>
            <ta e="T160" id="Seg_2267" s="T159">Prokopij.[NOM]</ta>
            <ta e="T161" id="Seg_2268" s="T160">child-PL-DAT/LOC</ta>
            <ta e="T162" id="Seg_2269" s="T161">who.[NOM]</ta>
            <ta e="T163" id="Seg_2270" s="T162">make-PTCP.PRS</ta>
            <ta e="T164" id="Seg_2271" s="T163">be-PST1-3SG</ta>
            <ta e="T165" id="Seg_2272" s="T164">who.[NOM]</ta>
            <ta e="T166" id="Seg_2273" s="T165">be-PST1-3SG</ta>
            <ta e="T167" id="Seg_2274" s="T166">well</ta>
            <ta e="T168" id="Seg_2275" s="T167">figure-PL-ACC</ta>
            <ta e="T170" id="Seg_2276" s="T169">that-ABL</ta>
            <ta e="T300" id="Seg_2277" s="T170">well</ta>
            <ta e="T172" id="Seg_2278" s="T171">eh</ta>
            <ta e="T173" id="Seg_2279" s="T172">Kira.[NOM]</ta>
            <ta e="T174" id="Seg_2280" s="T173">who-3SG</ta>
            <ta e="T175" id="Seg_2281" s="T174">figure-3SG.[NOM]</ta>
            <ta e="T176" id="Seg_2282" s="T175">hare.[NOM]</ta>
            <ta e="T177" id="Seg_2283" s="T176">be-PST1-3SG</ta>
            <ta e="T178" id="Seg_2284" s="T177">and</ta>
            <ta e="T179" id="Seg_2285" s="T178">who.[NOM]</ta>
            <ta e="T180" id="Seg_2286" s="T179">own-3SG</ta>
            <ta e="T181" id="Seg_2287" s="T180">who.[NOM]</ta>
            <ta e="T182" id="Seg_2288" s="T181">be-PST1-3SG</ta>
            <ta e="T183" id="Seg_2289" s="T182">who.[NOM]</ta>
            <ta e="T184" id="Seg_2290" s="T183">own-3SG</ta>
            <ta e="T185" id="Seg_2291" s="T184">Nadia.[NOM]</ta>
            <ta e="T186" id="Seg_2292" s="T185">own-3SG</ta>
            <ta e="T187" id="Seg_2293" s="T186">who.[NOM]</ta>
            <ta e="T188" id="Seg_2294" s="T187">simply</ta>
            <ta e="T189" id="Seg_2295" s="T188">partridge.[NOM]</ta>
            <ta e="T190" id="Seg_2296" s="T189">be-PST1-3SG</ta>
            <ta e="T191" id="Seg_2297" s="T190">that-ABL</ta>
            <ta e="T192" id="Seg_2298" s="T191">lay-PTCP.PST</ta>
            <ta e="T193" id="Seg_2299" s="T192">be-PST1-3PL</ta>
            <ta e="T194" id="Seg_2300" s="T193">there</ta>
            <ta e="T195" id="Seg_2301" s="T194">two-COLL-3PL.[NOM]</ta>
            <ta e="T196" id="Seg_2302" s="T195">and</ta>
            <ta e="T197" id="Seg_2303" s="T196">partridge-ACC</ta>
            <ta e="T198" id="Seg_2304" s="T197">who-DAT/LOC</ta>
            <ta e="T199" id="Seg_2305" s="T198">lay-PTCP.PST</ta>
            <ta e="T200" id="Seg_2306" s="T199">be-PST1-3PL</ta>
            <ta e="T201" id="Seg_2307" s="T200">who.[NOM]</ta>
            <ta e="T202" id="Seg_2308" s="T201">be-PST1-3SG=Q</ta>
            <ta e="T203" id="Seg_2309" s="T202">well</ta>
            <ta e="T204" id="Seg_2310" s="T203">hare.[NOM]</ta>
            <ta e="T205" id="Seg_2311" s="T204">who-3SG-DAT/LOC</ta>
            <ta e="T206" id="Seg_2312" s="T205">then</ta>
            <ta e="T207" id="Seg_2313" s="T206">that.[NOM]</ta>
            <ta e="T208" id="Seg_2314" s="T207">who.[NOM]</ta>
            <ta e="T209" id="Seg_2315" s="T208">so</ta>
            <ta e="T210" id="Seg_2316" s="T209">well</ta>
            <ta e="T211" id="Seg_2317" s="T210">hole-3SG-DAT/LOC</ta>
            <ta e="T213" id="Seg_2318" s="T212">that-ABL</ta>
            <ta e="T214" id="Seg_2319" s="T213">there</ta>
            <ta e="T215" id="Seg_2320" s="T214">1PL.[NOM]</ta>
            <ta e="T216" id="Seg_2321" s="T215">that-ABL</ta>
            <ta e="T217" id="Seg_2322" s="T216">there</ta>
            <ta e="T219" id="Seg_2323" s="T218">then</ta>
            <ta e="T220" id="Seg_2324" s="T219">later</ta>
            <ta e="T221" id="Seg_2325" s="T220">how.much</ta>
            <ta e="T222" id="Seg_2326" s="T221">one</ta>
            <ta e="T223" id="Seg_2327" s="T222">how.much</ta>
            <ta e="T224" id="Seg_2328" s="T223">INDEF</ta>
            <ta e="T225" id="Seg_2329" s="T224">week.[NOM]</ta>
            <ta e="T226" id="Seg_2330" s="T225">pass.by-PST2-3SG</ta>
            <ta e="T227" id="Seg_2331" s="T226">still</ta>
            <ta e="T228" id="Seg_2332" s="T227">two-ORD-3PL-ACC</ta>
            <ta e="T232" id="Seg_2333" s="T231">go-PST2-1PL</ta>
            <ta e="T234" id="Seg_2334" s="T233">that-ABL</ta>
            <ta e="T235" id="Seg_2335" s="T234">thither</ta>
            <ta e="T236" id="Seg_2336" s="T235">go-PST2-1PL</ta>
            <ta e="T237" id="Seg_2337" s="T236">that.EMPH.[NOM]</ta>
            <ta e="T238" id="Seg_2338" s="T237">place-1PL-DAT/LOC</ta>
            <ta e="T240" id="Seg_2339" s="T239">that</ta>
            <ta e="T241" id="Seg_2340" s="T240">place-DAT/LOC</ta>
            <ta e="T242" id="Seg_2341" s="T241">there</ta>
            <ta e="T243" id="Seg_2342" s="T242">who.[NOM]</ta>
            <ta e="T244" id="Seg_2343" s="T243">there.is</ta>
            <ta e="T248" id="Seg_2344" s="T247">there.is</ta>
            <ta e="T249" id="Seg_2345" s="T248">be-PST1-3SG</ta>
            <ta e="T250" id="Seg_2346" s="T249">EMPH</ta>
            <ta e="T251" id="Seg_2347" s="T250">that</ta>
            <ta e="T252" id="Seg_2348" s="T251">hare.[NOM]</ta>
            <ta e="T253" id="Seg_2349" s="T252">urine-VBZ-EP-PASS/REFL-EP-PTCP.PST</ta>
            <ta e="T254" id="Seg_2350" s="T253">be-PST1-3SG</ta>
            <ta e="T255" id="Seg_2351" s="T254">urine-PROPR.[NOM]</ta>
            <ta e="T256" id="Seg_2352" s="T255">be-PST1-3SG</ta>
            <ta e="T258" id="Seg_2353" s="T257">and</ta>
            <ta e="T259" id="Seg_2354" s="T258">who.[NOM]</ta>
            <ta e="T260" id="Seg_2355" s="T259">INDEF</ta>
            <ta e="T261" id="Seg_2356" s="T260">partridge.[NOM]</ta>
            <ta e="T262" id="Seg_2357" s="T261">head-3SG-ACC</ta>
            <ta e="T263" id="Seg_2358" s="T262">who.[NOM]</ta>
            <ta e="T264" id="Seg_2359" s="T263">INDEF</ta>
            <ta e="T265" id="Seg_2360" s="T264">pull-PTCP.PST</ta>
            <ta e="T266" id="Seg_2361" s="T265">be-PST1-1PL</ta>
            <ta e="T268" id="Seg_2362" s="T267">that-ABL</ta>
            <ta e="T269" id="Seg_2363" s="T268">later</ta>
            <ta e="T270" id="Seg_2364" s="T269">1PL.[NOM]</ta>
            <ta e="T271" id="Seg_2365" s="T270">ride-VBZ-CVB.SIM</ta>
            <ta e="T272" id="Seg_2366" s="T271">go-PST2-1PL</ta>
            <ta e="T273" id="Seg_2367" s="T272">and</ta>
            <ta e="T274" id="Seg_2368" s="T273">who.[NOM]</ta>
            <ta e="T275" id="Seg_2369" s="T274">INDEF</ta>
            <ta e="T276" id="Seg_2370" s="T275">eh</ta>
            <ta e="T277" id="Seg_2371" s="T276">uncle</ta>
            <ta e="T278" id="Seg_2372" s="T277">Prokopij.[NOM]</ta>
            <ta e="T279" id="Seg_2373" s="T278">child-PL-ACC</ta>
            <ta e="T280" id="Seg_2374" s="T279">take-PTCP.PST</ta>
            <ta e="T281" id="Seg_2375" s="T280">be-PST1-3PL</ta>
            <ta e="T282" id="Seg_2376" s="T281">eh</ta>
            <ta e="T283" id="Seg_2377" s="T282">NEG</ta>
            <ta e="T284" id="Seg_2378" s="T283">that</ta>
            <ta e="T285" id="Seg_2379" s="T284">different</ta>
            <ta e="T286" id="Seg_2380" s="T285">who-DAT/LOC</ta>
            <ta e="T288" id="Seg_2381" s="T287">1PL.[NOM]</ta>
            <ta e="T289" id="Seg_2382" s="T288">ride-VBZ-CVB.SIM</ta>
            <ta e="T290" id="Seg_2383" s="T289">go-PST2-1PL</ta>
            <ta e="T291" id="Seg_2384" s="T290">that-ABL</ta>
            <ta e="T292" id="Seg_2385" s="T291">back</ta>
            <ta e="T293" id="Seg_2386" s="T292">come-PST1-1PL</ta>
            <ta e="T294" id="Seg_2387" s="T293">who-DAT/LOC</ta>
            <ta e="T295" id="Seg_2388" s="T294">village-DAT/LOC</ta>
            <ta e="T296" id="Seg_2389" s="T295">ski-PL-PROPR</ta>
            <ta e="T297" id="Seg_2390" s="T296">that-ABL</ta>
            <ta e="T298" id="Seg_2391" s="T297">come-PST1-1PL</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T2" id="Seg_2392" s="T1">einmal</ta>
            <ta e="T3" id="Seg_2393" s="T2">1PL.[NOM]</ta>
            <ta e="T4" id="Seg_2394" s="T3">Regina-ACC</ta>
            <ta e="T5" id="Seg_2395" s="T4">mit</ta>
            <ta e="T6" id="Seg_2396" s="T5">gehen-PTCP.PRS</ta>
            <ta e="T7" id="Seg_2397" s="T6">sein-PST1-1PL</ta>
            <ta e="T9" id="Seg_2398" s="T8">jenes-ABL</ta>
            <ta e="T10" id="Seg_2399" s="T9">dort</ta>
            <ta e="T11" id="Seg_2400" s="T10">Tante.[NOM]</ta>
            <ta e="T12" id="Seg_2401" s="T11">äh</ta>
            <ta e="T13" id="Seg_2402" s="T12">1PL.[NOM]</ta>
            <ta e="T14" id="Seg_2403" s="T13">gehen-PTCP.PRS</ta>
            <ta e="T15" id="Seg_2404" s="T14">sein-PST1-1PL</ta>
            <ta e="T16" id="Seg_2405" s="T15">Kind-ACC</ta>
            <ta e="T17" id="Seg_2406" s="T16">sich.kümmern-PTCP.PRS</ta>
            <ta e="T18" id="Seg_2407" s="T17">sein-PST1-1PL</ta>
            <ta e="T20" id="Seg_2408" s="T19">jenes-ABL</ta>
            <ta e="T21" id="Seg_2409" s="T20">Kira.[NOM]</ta>
            <ta e="T22" id="Seg_2410" s="T21">Mama-3SG.[NOM]</ta>
            <ta e="T23" id="Seg_2411" s="T22">rufen-PST2-3SG</ta>
            <ta e="T24" id="Seg_2412" s="T23">1SG-ACC</ta>
            <ta e="T25" id="Seg_2413" s="T24">fahren-VBZ-CVB.SIM</ta>
            <ta e="T26" id="Seg_2414" s="T25">gehen-PRS-2PL</ta>
            <ta e="T27" id="Seg_2415" s="T26">Q</ta>
            <ta e="T28" id="Seg_2416" s="T27">Tundra-DAT/LOC</ta>
            <ta e="T29" id="Seg_2417" s="T28">1PL.[NOM]</ta>
            <ta e="T30" id="Seg_2418" s="T29">äh</ta>
            <ta e="T31" id="Seg_2419" s="T30">fahren-VBZ-CVB.SIM</ta>
            <ta e="T32" id="Seg_2420" s="T31">gehen-CVB.SEQ</ta>
            <ta e="T33" id="Seg_2421" s="T32">Q</ta>
            <ta e="T34" id="Seg_2422" s="T33">1SG-ACC</ta>
            <ta e="T35" id="Seg_2423" s="T34">mit</ta>
            <ta e="T36" id="Seg_2424" s="T35">Tundra-DAT/LOC</ta>
            <ta e="T38" id="Seg_2425" s="T37">jenes-ABL</ta>
            <ta e="T39" id="Seg_2426" s="T38">1SG.[NOM]</ta>
            <ta e="T40" id="Seg_2427" s="T39">sagen-PST2-EP-1SG</ta>
            <ta e="T41" id="Seg_2428" s="T40">AFFIRM</ta>
            <ta e="T42" id="Seg_2429" s="T41">jenes-ABL</ta>
            <ta e="T43" id="Seg_2430" s="T42">1PL.[NOM]</ta>
            <ta e="T44" id="Seg_2431" s="T43">gehen-PST1-1PL</ta>
            <ta e="T46" id="Seg_2432" s="T45">und</ta>
            <ta e="T47" id="Seg_2433" s="T46">Polina</ta>
            <ta e="T48" id="Seg_2434" s="T47">Alekseevna-PROPR</ta>
            <ta e="T49" id="Seg_2435" s="T48">selbst-3PL-ACC</ta>
            <ta e="T301" id="Seg_2436" s="T49">Familie-3PL-ACC</ta>
            <ta e="T50" id="Seg_2437" s="T301">mit</ta>
            <ta e="T51" id="Seg_2438" s="T50">bleiben-PTCP.PST</ta>
            <ta e="T52" id="Seg_2439" s="T51">sein-PST1-3PL</ta>
            <ta e="T303" id="Seg_2440" s="T53">und</ta>
            <ta e="T302" id="Seg_2441" s="T54">kommen-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_2442" s="T302">kommen-PTCP.PRS</ta>
            <ta e="T56" id="Seg_2443" s="T55">stehen-PTCP.PRS</ta>
            <ta e="T57" id="Seg_2444" s="T56">sein-PST1-3PL</ta>
            <ta e="T58" id="Seg_2445" s="T57">und</ta>
            <ta e="T59" id="Seg_2446" s="T58">Tante</ta>
            <ta e="T60" id="Seg_2447" s="T59">Nastja.[NOM]</ta>
            <ta e="T61" id="Seg_2448" s="T60">3PL-EP-ACC</ta>
            <ta e="T62" id="Seg_2449" s="T61">nehmen-CAUS-PTCP.PRS</ta>
            <ta e="T63" id="Seg_2450" s="T62">sein-PST1-3SG</ta>
            <ta e="T65" id="Seg_2451" s="T64">jetzt</ta>
            <ta e="T66" id="Seg_2452" s="T65">wer-PL-ACC</ta>
            <ta e="T67" id="Seg_2453" s="T66">was.[NOM]</ta>
            <ta e="T68" id="Seg_2454" s="T67">sagen-HAB-3PL</ta>
            <ta e="T69" id="Seg_2455" s="T68">Schneemobil</ta>
            <ta e="T70" id="Seg_2456" s="T69">Schneemobil-PROPR-3PL</ta>
            <ta e="T71" id="Seg_2457" s="T70">was-PART</ta>
            <ta e="T73" id="Seg_2458" s="T72">jenes-ABL</ta>
            <ta e="T74" id="Seg_2459" s="T73">3PL.[NOM]</ta>
            <ta e="T75" id="Seg_2460" s="T74">kommen-PST2-3PL</ta>
            <ta e="T76" id="Seg_2461" s="T75">Polina</ta>
            <ta e="T77" id="Seg_2462" s="T76">Alekseevna-PROPR.[NOM]</ta>
            <ta e="T78" id="Seg_2463" s="T77">jenes-ABL</ta>
            <ta e="T79" id="Seg_2464" s="T78">1PL.[NOM]</ta>
            <ta e="T80" id="Seg_2465" s="T79">dort</ta>
            <ta e="T81" id="Seg_2466" s="T80">EMPH</ta>
            <ta e="T82" id="Seg_2467" s="T81">weiter</ta>
            <ta e="T83" id="Seg_2468" s="T82">gehen-PST2-1PL</ta>
            <ta e="T85" id="Seg_2469" s="T84">jenes-ABL</ta>
            <ta e="T86" id="Seg_2470" s="T85">dort</ta>
            <ta e="T87" id="Seg_2471" s="T86">wer.[NOM]</ta>
            <ta e="T88" id="Seg_2472" s="T87">wer-PL-ACC</ta>
            <ta e="T89" id="Seg_2473" s="T88">dieses</ta>
            <ta e="T90" id="Seg_2474" s="T89">nun</ta>
            <ta e="T91" id="Seg_2475" s="T90">halten-PST2-1PL</ta>
            <ta e="T92" id="Seg_2476" s="T91">jenes-ABL</ta>
            <ta e="T93" id="Seg_2477" s="T92">dort</ta>
            <ta e="T94" id="Seg_2478" s="T93">wer.[NOM]</ta>
            <ta e="T96" id="Seg_2479" s="T95">Häschen.[NOM]</ta>
            <ta e="T97" id="Seg_2480" s="T96">wer-3SG-ACC</ta>
            <ta e="T98" id="Seg_2481" s="T97">Loch-3SG-ACC</ta>
            <ta e="T99" id="Seg_2482" s="T98">sehen-PTCP.PST</ta>
            <ta e="T100" id="Seg_2483" s="T99">sein-PST1-1PL</ta>
            <ta e="T102" id="Seg_2484" s="T101">jenes-ABL</ta>
            <ta e="T103" id="Seg_2485" s="T102">fahren-VBZ</ta>
            <ta e="T104" id="Seg_2486" s="T103">fahren-VBZ-PST2-1PL</ta>
            <ta e="T106" id="Seg_2487" s="T105">groß</ta>
            <ta e="T107" id="Seg_2488" s="T106">sehr</ta>
            <ta e="T108" id="Seg_2489" s="T107">Hügel.[NOM]</ta>
            <ta e="T109" id="Seg_2490" s="T108">sein-PST1-3SG</ta>
            <ta e="T110" id="Seg_2491" s="T109">dort</ta>
            <ta e="T111" id="Seg_2492" s="T110">1PL.[NOM]</ta>
            <ta e="T112" id="Seg_2493" s="T111">EMPH</ta>
            <ta e="T113" id="Seg_2494" s="T112">bemerken-VBZ-NEG.PTCP</ta>
            <ta e="T114" id="Seg_2495" s="T113">sein-PST1-1PL</ta>
            <ta e="T115" id="Seg_2496" s="T114">dort</ta>
            <ta e="T118" id="Seg_2497" s="T117">sein-PST1-3SG</ta>
            <ta e="T119" id="Seg_2498" s="T118">was.[NOM]</ta>
            <ta e="T120" id="Seg_2499" s="T119">NEG</ta>
            <ta e="T121" id="Seg_2500" s="T120">NEG</ta>
            <ta e="T122" id="Seg_2501" s="T121">sein-PST1-3SG</ta>
            <ta e="T123" id="Seg_2502" s="T122">zu.sehen.sein-EP-NEG.PTCP</ta>
            <ta e="T125" id="Seg_2503" s="T124">jenes-ABL</ta>
            <ta e="T126" id="Seg_2504" s="T125">fahren-VBZ-PST2-1PL</ta>
            <ta e="T127" id="Seg_2505" s="T126">jenes-ABL</ta>
            <ta e="T128" id="Seg_2506" s="T127">dort</ta>
            <ta e="T129" id="Seg_2507" s="T128">wer.[NOM]</ta>
            <ta e="T130" id="Seg_2508" s="T129">Polina</ta>
            <ta e="T131" id="Seg_2509" s="T130">Alekseevna.[NOM]</ta>
            <ta e="T132" id="Seg_2510" s="T131">es.gibt</ta>
            <ta e="T133" id="Seg_2511" s="T132">sein-PST1-3SG</ta>
            <ta e="T134" id="Seg_2512" s="T133">Familie-3SG-ACC</ta>
            <ta e="T135" id="Seg_2513" s="T134">mit</ta>
            <ta e="T136" id="Seg_2514" s="T135">jenes-ABL</ta>
            <ta e="T137" id="Seg_2515" s="T136">Tante</ta>
            <ta e="T138" id="Seg_2516" s="T137">Nastja.[NOM]</ta>
            <ta e="T139" id="Seg_2517" s="T138">jenes-ABL</ta>
            <ta e="T140" id="Seg_2518" s="T139">Nikolas.[NOM]</ta>
            <ta e="T141" id="Seg_2519" s="T140">Ljudmila</ta>
            <ta e="T142" id="Seg_2520" s="T141">Nikolajevna.[NOM]</ta>
            <ta e="T143" id="Seg_2521" s="T142">Kind-3SG.[NOM]</ta>
            <ta e="T144" id="Seg_2522" s="T143">jenes-ABL</ta>
            <ta e="T145" id="Seg_2523" s="T144">Annuschka</ta>
            <ta e="T146" id="Seg_2524" s="T145">Tante</ta>
            <ta e="T147" id="Seg_2525" s="T146">Nastja.[NOM]</ta>
            <ta e="T149" id="Seg_2526" s="T148">jenes-ABL</ta>
            <ta e="T150" id="Seg_2527" s="T149">1PL.[NOM]</ta>
            <ta e="T151" id="Seg_2528" s="T150">fahren-VBZ-PST2-1PL</ta>
            <ta e="T153" id="Seg_2529" s="T152">jenes-ABL</ta>
            <ta e="T154" id="Seg_2530" s="T153">wer.[NOM]</ta>
            <ta e="T155" id="Seg_2531" s="T154">sein-PST1-3SG</ta>
            <ta e="T156" id="Seg_2532" s="T155">wer.[NOM]</ta>
            <ta e="T157" id="Seg_2533" s="T156">sein-PST1-3SG</ta>
            <ta e="T158" id="Seg_2534" s="T157">äh</ta>
            <ta e="T159" id="Seg_2535" s="T158">Onkel</ta>
            <ta e="T160" id="Seg_2536" s="T159">Prokopij.[NOM]</ta>
            <ta e="T161" id="Seg_2537" s="T160">Kind-PL-DAT/LOC</ta>
            <ta e="T162" id="Seg_2538" s="T161">wer.[NOM]</ta>
            <ta e="T163" id="Seg_2539" s="T162">machen-PTCP.PRS</ta>
            <ta e="T164" id="Seg_2540" s="T163">sein-PST1-3SG</ta>
            <ta e="T165" id="Seg_2541" s="T164">wer.[NOM]</ta>
            <ta e="T166" id="Seg_2542" s="T165">sein-PST1-3SG</ta>
            <ta e="T167" id="Seg_2543" s="T166">nun</ta>
            <ta e="T168" id="Seg_2544" s="T167">Figur-PL-ACC</ta>
            <ta e="T170" id="Seg_2545" s="T169">jenes-ABL</ta>
            <ta e="T300" id="Seg_2546" s="T170">nun</ta>
            <ta e="T172" id="Seg_2547" s="T171">äh</ta>
            <ta e="T173" id="Seg_2548" s="T172">Kira.[NOM]</ta>
            <ta e="T174" id="Seg_2549" s="T173">wer-3SG</ta>
            <ta e="T175" id="Seg_2550" s="T174">Figur-3SG.[NOM]</ta>
            <ta e="T176" id="Seg_2551" s="T175">Häschen.[NOM]</ta>
            <ta e="T177" id="Seg_2552" s="T176">sein-PST1-3SG</ta>
            <ta e="T178" id="Seg_2553" s="T177">und</ta>
            <ta e="T179" id="Seg_2554" s="T178">wer.[NOM]</ta>
            <ta e="T180" id="Seg_2555" s="T179">eigen-3SG</ta>
            <ta e="T181" id="Seg_2556" s="T180">wer.[NOM]</ta>
            <ta e="T182" id="Seg_2557" s="T181">sein-PST1-3SG</ta>
            <ta e="T183" id="Seg_2558" s="T182">wer.[NOM]</ta>
            <ta e="T184" id="Seg_2559" s="T183">eigen-3SG</ta>
            <ta e="T185" id="Seg_2560" s="T184">Nadia.[NOM]</ta>
            <ta e="T186" id="Seg_2561" s="T185">eigen-3SG</ta>
            <ta e="T187" id="Seg_2562" s="T186">wer.[NOM]</ta>
            <ta e="T188" id="Seg_2563" s="T187">einfach</ta>
            <ta e="T189" id="Seg_2564" s="T188">Rebhuhn.[NOM]</ta>
            <ta e="T190" id="Seg_2565" s="T189">sein-PST1-3SG</ta>
            <ta e="T191" id="Seg_2566" s="T190">jenes-ABL</ta>
            <ta e="T192" id="Seg_2567" s="T191">legen-PTCP.PST</ta>
            <ta e="T193" id="Seg_2568" s="T192">sein-PST1-3PL</ta>
            <ta e="T194" id="Seg_2569" s="T193">dort</ta>
            <ta e="T195" id="Seg_2570" s="T194">zwei-COLL-3PL.[NOM]</ta>
            <ta e="T196" id="Seg_2571" s="T195">und</ta>
            <ta e="T197" id="Seg_2572" s="T196">Rebhuhn-ACC</ta>
            <ta e="T198" id="Seg_2573" s="T197">wer-DAT/LOC</ta>
            <ta e="T199" id="Seg_2574" s="T198">legen-PTCP.PST</ta>
            <ta e="T200" id="Seg_2575" s="T199">sein-PST1-3PL</ta>
            <ta e="T201" id="Seg_2576" s="T200">wer.[NOM]</ta>
            <ta e="T202" id="Seg_2577" s="T201">sein-PST1-3SG=Q</ta>
            <ta e="T203" id="Seg_2578" s="T202">nun</ta>
            <ta e="T204" id="Seg_2579" s="T203">Häschen.[NOM]</ta>
            <ta e="T205" id="Seg_2580" s="T204">wer-3SG-DAT/LOC</ta>
            <ta e="T206" id="Seg_2581" s="T205">dann</ta>
            <ta e="T207" id="Seg_2582" s="T206">dieses.[NOM]</ta>
            <ta e="T208" id="Seg_2583" s="T207">wer.[NOM]</ta>
            <ta e="T209" id="Seg_2584" s="T208">also</ta>
            <ta e="T210" id="Seg_2585" s="T209">nun</ta>
            <ta e="T211" id="Seg_2586" s="T210">Loch-3SG-DAT/LOC</ta>
            <ta e="T213" id="Seg_2587" s="T212">jenes-ABL</ta>
            <ta e="T214" id="Seg_2588" s="T213">dort</ta>
            <ta e="T215" id="Seg_2589" s="T214">1PL.[NOM]</ta>
            <ta e="T216" id="Seg_2590" s="T215">jenes-ABL</ta>
            <ta e="T217" id="Seg_2591" s="T216">dort</ta>
            <ta e="T219" id="Seg_2592" s="T218">dann</ta>
            <ta e="T220" id="Seg_2593" s="T219">später</ta>
            <ta e="T221" id="Seg_2594" s="T220">wie.viel</ta>
            <ta e="T222" id="Seg_2595" s="T221">eins</ta>
            <ta e="T223" id="Seg_2596" s="T222">wie.viel</ta>
            <ta e="T224" id="Seg_2597" s="T223">INDEF</ta>
            <ta e="T225" id="Seg_2598" s="T224">Woche.[NOM]</ta>
            <ta e="T226" id="Seg_2599" s="T225">vorbeigehen-PST2-3SG</ta>
            <ta e="T227" id="Seg_2600" s="T226">noch</ta>
            <ta e="T228" id="Seg_2601" s="T227">zwei-ORD-3PL-ACC</ta>
            <ta e="T232" id="Seg_2602" s="T231">gehen-PST2-1PL</ta>
            <ta e="T234" id="Seg_2603" s="T233">jenes-ABL</ta>
            <ta e="T235" id="Seg_2604" s="T234">dorthin</ta>
            <ta e="T236" id="Seg_2605" s="T235">gehen-PST2-1PL</ta>
            <ta e="T237" id="Seg_2606" s="T236">jenes.EMPH.[NOM]</ta>
            <ta e="T238" id="Seg_2607" s="T237">Platz-1PL-DAT/LOC</ta>
            <ta e="T240" id="Seg_2608" s="T239">jenes</ta>
            <ta e="T241" id="Seg_2609" s="T240">Platz-DAT/LOC</ta>
            <ta e="T242" id="Seg_2610" s="T241">dort</ta>
            <ta e="T243" id="Seg_2611" s="T242">wer.[NOM]</ta>
            <ta e="T244" id="Seg_2612" s="T243">es.gibt</ta>
            <ta e="T248" id="Seg_2613" s="T247">es.gibt</ta>
            <ta e="T249" id="Seg_2614" s="T248">sein-PST1-3SG</ta>
            <ta e="T250" id="Seg_2615" s="T249">EMPH</ta>
            <ta e="T251" id="Seg_2616" s="T250">jenes</ta>
            <ta e="T252" id="Seg_2617" s="T251">Häschen.[NOM]</ta>
            <ta e="T253" id="Seg_2618" s="T252">Harn-VBZ-EP-PASS/REFL-EP-PTCP.PST</ta>
            <ta e="T254" id="Seg_2619" s="T253">sein-PST1-3SG</ta>
            <ta e="T255" id="Seg_2620" s="T254">Harn-PROPR.[NOM]</ta>
            <ta e="T256" id="Seg_2621" s="T255">sein-PST1-3SG</ta>
            <ta e="T258" id="Seg_2622" s="T257">und</ta>
            <ta e="T259" id="Seg_2623" s="T258">wer.[NOM]</ta>
            <ta e="T260" id="Seg_2624" s="T259">INDEF</ta>
            <ta e="T261" id="Seg_2625" s="T260">Rebhuhn.[NOM]</ta>
            <ta e="T262" id="Seg_2626" s="T261">Kopf-3SG-ACC</ta>
            <ta e="T263" id="Seg_2627" s="T262">wer.[NOM]</ta>
            <ta e="T264" id="Seg_2628" s="T263">INDEF</ta>
            <ta e="T265" id="Seg_2629" s="T264">ziehen-PTCP.PST</ta>
            <ta e="T266" id="Seg_2630" s="T265">sein-PST1-1PL</ta>
            <ta e="T268" id="Seg_2631" s="T267">jenes-ABL</ta>
            <ta e="T269" id="Seg_2632" s="T268">später</ta>
            <ta e="T270" id="Seg_2633" s="T269">1PL.[NOM]</ta>
            <ta e="T271" id="Seg_2634" s="T270">fahren-VBZ-CVB.SIM</ta>
            <ta e="T272" id="Seg_2635" s="T271">gehen-PST2-1PL</ta>
            <ta e="T273" id="Seg_2636" s="T272">und</ta>
            <ta e="T274" id="Seg_2637" s="T273">wer.[NOM]</ta>
            <ta e="T275" id="Seg_2638" s="T274">INDEF</ta>
            <ta e="T276" id="Seg_2639" s="T275">äh</ta>
            <ta e="T277" id="Seg_2640" s="T276">Onkel</ta>
            <ta e="T278" id="Seg_2641" s="T277">Prokopij.[NOM]</ta>
            <ta e="T279" id="Seg_2642" s="T278">Kind-PL-ACC</ta>
            <ta e="T280" id="Seg_2643" s="T279">nehmen-PTCP.PST</ta>
            <ta e="T281" id="Seg_2644" s="T280">sein-PST1-3PL</ta>
            <ta e="T282" id="Seg_2645" s="T281">äh</ta>
            <ta e="T283" id="Seg_2646" s="T282">NEG</ta>
            <ta e="T284" id="Seg_2647" s="T283">jenes</ta>
            <ta e="T285" id="Seg_2648" s="T284">anders</ta>
            <ta e="T286" id="Seg_2649" s="T285">wer-DAT/LOC</ta>
            <ta e="T288" id="Seg_2650" s="T287">1PL.[NOM]</ta>
            <ta e="T289" id="Seg_2651" s="T288">fahren-VBZ-CVB.SIM</ta>
            <ta e="T290" id="Seg_2652" s="T289">gehen-PST2-1PL</ta>
            <ta e="T291" id="Seg_2653" s="T290">jenes-ABL</ta>
            <ta e="T292" id="Seg_2654" s="T291">zurück</ta>
            <ta e="T293" id="Seg_2655" s="T292">kommen-PST1-1PL</ta>
            <ta e="T294" id="Seg_2656" s="T293">wer-DAT/LOC</ta>
            <ta e="T295" id="Seg_2657" s="T294">Dorf-DAT/LOC</ta>
            <ta e="T296" id="Seg_2658" s="T295">Ski-PL-PROPR</ta>
            <ta e="T297" id="Seg_2659" s="T296">jenes-ABL</ta>
            <ta e="T298" id="Seg_2660" s="T297">kommen-PST1-1PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_2661" s="T1">однажды</ta>
            <ta e="T3" id="Seg_2662" s="T2">1PL.[NOM]</ta>
            <ta e="T4" id="Seg_2663" s="T3">Регина-ACC</ta>
            <ta e="T5" id="Seg_2664" s="T4">с</ta>
            <ta e="T6" id="Seg_2665" s="T5">идти-PTCP.PRS</ta>
            <ta e="T7" id="Seg_2666" s="T6">быть-PST1-1PL</ta>
            <ta e="T9" id="Seg_2667" s="T8">тот-ABL</ta>
            <ta e="T10" id="Seg_2668" s="T9">там</ta>
            <ta e="T11" id="Seg_2669" s="T10">тетя.[NOM]</ta>
            <ta e="T12" id="Seg_2670" s="T11">ээ</ta>
            <ta e="T13" id="Seg_2671" s="T12">1PL.[NOM]</ta>
            <ta e="T14" id="Seg_2672" s="T13">идти-PTCP.PRS</ta>
            <ta e="T15" id="Seg_2673" s="T14">быть-PST1-1PL</ta>
            <ta e="T16" id="Seg_2674" s="T15">ребенок-ACC</ta>
            <ta e="T17" id="Seg_2675" s="T16">заботиться-PTCP.PRS</ta>
            <ta e="T18" id="Seg_2676" s="T17">быть-PST1-1PL</ta>
            <ta e="T20" id="Seg_2677" s="T19">тот-ABL</ta>
            <ta e="T21" id="Seg_2678" s="T20">Кира.[NOM]</ta>
            <ta e="T22" id="Seg_2679" s="T21">мама-3SG.[NOM]</ta>
            <ta e="T23" id="Seg_2680" s="T22">звать-PST2-3SG</ta>
            <ta e="T24" id="Seg_2681" s="T23">1SG-ACC</ta>
            <ta e="T25" id="Seg_2682" s="T24">кататься-VBZ-CVB.SIM</ta>
            <ta e="T26" id="Seg_2683" s="T25">идти-PRS-2PL</ta>
            <ta e="T27" id="Seg_2684" s="T26">Q</ta>
            <ta e="T28" id="Seg_2685" s="T27">тундра-DAT/LOC</ta>
            <ta e="T29" id="Seg_2686" s="T28">1PL.[NOM]</ta>
            <ta e="T30" id="Seg_2687" s="T29">ээ</ta>
            <ta e="T31" id="Seg_2688" s="T30">кататься-VBZ-CVB.SIM</ta>
            <ta e="T32" id="Seg_2689" s="T31">идти-CVB.SEQ</ta>
            <ta e="T33" id="Seg_2690" s="T32">Q</ta>
            <ta e="T34" id="Seg_2691" s="T33">1SG-ACC</ta>
            <ta e="T35" id="Seg_2692" s="T34">с</ta>
            <ta e="T36" id="Seg_2693" s="T35">тундра-DAT/LOC</ta>
            <ta e="T38" id="Seg_2694" s="T37">тот-ABL</ta>
            <ta e="T39" id="Seg_2695" s="T38">1SG.[NOM]</ta>
            <ta e="T40" id="Seg_2696" s="T39">говорить-PST2-EP-1SG</ta>
            <ta e="T41" id="Seg_2697" s="T40">AFFIRM</ta>
            <ta e="T42" id="Seg_2698" s="T41">тот-ABL</ta>
            <ta e="T43" id="Seg_2699" s="T42">1PL.[NOM]</ta>
            <ta e="T44" id="Seg_2700" s="T43">идти-PST1-1PL</ta>
            <ta e="T46" id="Seg_2701" s="T45">а</ta>
            <ta e="T47" id="Seg_2702" s="T46">Полина</ta>
            <ta e="T48" id="Seg_2703" s="T47">Алексеевна-PROPR</ta>
            <ta e="T49" id="Seg_2704" s="T48">сам-3PL-ACC</ta>
            <ta e="T301" id="Seg_2705" s="T49">семья-3PL-ACC</ta>
            <ta e="T50" id="Seg_2706" s="T301">с</ta>
            <ta e="T51" id="Seg_2707" s="T50">оставаться-PTCP.PST</ta>
            <ta e="T52" id="Seg_2708" s="T51">быть-PST1-3PL</ta>
            <ta e="T303" id="Seg_2709" s="T53">а</ta>
            <ta e="T302" id="Seg_2710" s="T54">приходить-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_2711" s="T302">приходить-PTCP.PRS</ta>
            <ta e="T56" id="Seg_2712" s="T55">стоять-PTCP.PRS</ta>
            <ta e="T57" id="Seg_2713" s="T56">быть-PST1-3PL</ta>
            <ta e="T58" id="Seg_2714" s="T57">а</ta>
            <ta e="T59" id="Seg_2715" s="T58">тетя</ta>
            <ta e="T60" id="Seg_2716" s="T59">Настя.[NOM]</ta>
            <ta e="T61" id="Seg_2717" s="T60">3PL-EP-ACC</ta>
            <ta e="T62" id="Seg_2718" s="T61">взять-CAUS-PTCP.PRS</ta>
            <ta e="T63" id="Seg_2719" s="T62">быть-PST1-3SG</ta>
            <ta e="T65" id="Seg_2720" s="T64">теперь</ta>
            <ta e="T66" id="Seg_2721" s="T65">кто-PL-ACC</ta>
            <ta e="T67" id="Seg_2722" s="T66">что.[NOM]</ta>
            <ta e="T68" id="Seg_2723" s="T67">говорить-HAB-3PL</ta>
            <ta e="T69" id="Seg_2724" s="T68">буранчик</ta>
            <ta e="T70" id="Seg_2725" s="T69">буранчик-PROPR-3PL</ta>
            <ta e="T71" id="Seg_2726" s="T70">что-PART</ta>
            <ta e="T73" id="Seg_2727" s="T72">тот-ABL</ta>
            <ta e="T74" id="Seg_2728" s="T73">3PL.[NOM]</ta>
            <ta e="T75" id="Seg_2729" s="T74">приходить-PST2-3PL</ta>
            <ta e="T76" id="Seg_2730" s="T75">Полина</ta>
            <ta e="T77" id="Seg_2731" s="T76">Алексеевна-PROPR.[NOM]</ta>
            <ta e="T78" id="Seg_2732" s="T77">тот-ABL</ta>
            <ta e="T79" id="Seg_2733" s="T78">1PL.[NOM]</ta>
            <ta e="T80" id="Seg_2734" s="T79">там</ta>
            <ta e="T81" id="Seg_2735" s="T80">EMPH</ta>
            <ta e="T82" id="Seg_2736" s="T81">дальше</ta>
            <ta e="T83" id="Seg_2737" s="T82">идти-PST2-1PL</ta>
            <ta e="T85" id="Seg_2738" s="T84">тот-ABL</ta>
            <ta e="T86" id="Seg_2739" s="T85">там</ta>
            <ta e="T87" id="Seg_2740" s="T86">кто.[NOM]</ta>
            <ta e="T88" id="Seg_2741" s="T87">кто-PL-ACC</ta>
            <ta e="T89" id="Seg_2742" s="T88">этот</ta>
            <ta e="T90" id="Seg_2743" s="T89">вот</ta>
            <ta e="T91" id="Seg_2744" s="T90">останавливаться-PST2-1PL</ta>
            <ta e="T92" id="Seg_2745" s="T91">тот-ABL</ta>
            <ta e="T93" id="Seg_2746" s="T92">там</ta>
            <ta e="T94" id="Seg_2747" s="T93">кто.[NOM]</ta>
            <ta e="T96" id="Seg_2748" s="T95">зайчик.[NOM]</ta>
            <ta e="T97" id="Seg_2749" s="T96">кто-3SG-ACC</ta>
            <ta e="T98" id="Seg_2750" s="T97">дыра-3SG-ACC</ta>
            <ta e="T99" id="Seg_2751" s="T98">видеть-PTCP.PST</ta>
            <ta e="T100" id="Seg_2752" s="T99">быть-PST1-1PL</ta>
            <ta e="T102" id="Seg_2753" s="T101">тот-ABL</ta>
            <ta e="T103" id="Seg_2754" s="T102">кататься-VBZ</ta>
            <ta e="T104" id="Seg_2755" s="T103">кататься-VBZ-PST2-1PL</ta>
            <ta e="T106" id="Seg_2756" s="T105">большой</ta>
            <ta e="T107" id="Seg_2757" s="T106">очень</ta>
            <ta e="T108" id="Seg_2758" s="T107">горка.[NOM]</ta>
            <ta e="T109" id="Seg_2759" s="T108">быть-PST1-3SG</ta>
            <ta e="T110" id="Seg_2760" s="T109">там</ta>
            <ta e="T111" id="Seg_2761" s="T110">1PL.[NOM]</ta>
            <ta e="T112" id="Seg_2762" s="T111">EMPH</ta>
            <ta e="T113" id="Seg_2763" s="T112">замечать-VBZ-NEG.PTCP</ta>
            <ta e="T114" id="Seg_2764" s="T113">быть-PST1-1PL</ta>
            <ta e="T115" id="Seg_2765" s="T114">там</ta>
            <ta e="T118" id="Seg_2766" s="T117">быть-PST1-3SG</ta>
            <ta e="T119" id="Seg_2767" s="T118">что.[NOM]</ta>
            <ta e="T120" id="Seg_2768" s="T119">NEG</ta>
            <ta e="T121" id="Seg_2769" s="T120">NEG</ta>
            <ta e="T122" id="Seg_2770" s="T121">быть-PST1-3SG</ta>
            <ta e="T123" id="Seg_2771" s="T122">быть.видно-EP-NEG.PTCP</ta>
            <ta e="T125" id="Seg_2772" s="T124">тот-ABL</ta>
            <ta e="T126" id="Seg_2773" s="T125">кататься-VBZ-PST2-1PL</ta>
            <ta e="T127" id="Seg_2774" s="T126">тот-ABL</ta>
            <ta e="T128" id="Seg_2775" s="T127">там</ta>
            <ta e="T129" id="Seg_2776" s="T128">кто.[NOM]</ta>
            <ta e="T130" id="Seg_2777" s="T129">Полина</ta>
            <ta e="T131" id="Seg_2778" s="T130">Алексеевна.[NOM]</ta>
            <ta e="T132" id="Seg_2779" s="T131">есть</ta>
            <ta e="T133" id="Seg_2780" s="T132">быть-PST1-3SG</ta>
            <ta e="T134" id="Seg_2781" s="T133">семья-3SG-ACC</ta>
            <ta e="T135" id="Seg_2782" s="T134">с</ta>
            <ta e="T136" id="Seg_2783" s="T135">тот-ABL</ta>
            <ta e="T137" id="Seg_2784" s="T136">тетя</ta>
            <ta e="T138" id="Seg_2785" s="T137">Настя.[NOM]</ta>
            <ta e="T139" id="Seg_2786" s="T138">тот-ABL</ta>
            <ta e="T140" id="Seg_2787" s="T139">Николай.[NOM]</ta>
            <ta e="T141" id="Seg_2788" s="T140">Людмила</ta>
            <ta e="T142" id="Seg_2789" s="T141">Николаевна.[NOM]</ta>
            <ta e="T143" id="Seg_2790" s="T142">ребенок-3SG.[NOM]</ta>
            <ta e="T144" id="Seg_2791" s="T143">тот-ABL</ta>
            <ta e="T145" id="Seg_2792" s="T144">Аннушка</ta>
            <ta e="T146" id="Seg_2793" s="T145">тетя</ta>
            <ta e="T147" id="Seg_2794" s="T146">Настя.[NOM]</ta>
            <ta e="T149" id="Seg_2795" s="T148">тот-ABL</ta>
            <ta e="T150" id="Seg_2796" s="T149">1PL.[NOM]</ta>
            <ta e="T151" id="Seg_2797" s="T150">кататься-VBZ-PST2-1PL</ta>
            <ta e="T153" id="Seg_2798" s="T152">тот-ABL</ta>
            <ta e="T154" id="Seg_2799" s="T153">кто.[NOM]</ta>
            <ta e="T155" id="Seg_2800" s="T154">быть-PST1-3SG</ta>
            <ta e="T156" id="Seg_2801" s="T155">кто.[NOM]</ta>
            <ta e="T157" id="Seg_2802" s="T156">быть-PST1-3SG</ta>
            <ta e="T158" id="Seg_2803" s="T157">ээ</ta>
            <ta e="T159" id="Seg_2804" s="T158">дядя</ta>
            <ta e="T160" id="Seg_2805" s="T159">Прокопий.[NOM]</ta>
            <ta e="T161" id="Seg_2806" s="T160">ребенок-PL-DAT/LOC</ta>
            <ta e="T162" id="Seg_2807" s="T161">кто.[NOM]</ta>
            <ta e="T163" id="Seg_2808" s="T162">делать-PTCP.PRS</ta>
            <ta e="T164" id="Seg_2809" s="T163">быть-PST1-3SG</ta>
            <ta e="T165" id="Seg_2810" s="T164">кто.[NOM]</ta>
            <ta e="T166" id="Seg_2811" s="T165">быть-PST1-3SG</ta>
            <ta e="T167" id="Seg_2812" s="T166">вот</ta>
            <ta e="T168" id="Seg_2813" s="T167">фигурка-PL-ACC</ta>
            <ta e="T170" id="Seg_2814" s="T169">тот-ABL</ta>
            <ta e="T300" id="Seg_2815" s="T170">вот</ta>
            <ta e="T172" id="Seg_2816" s="T171">ээ</ta>
            <ta e="T173" id="Seg_2817" s="T172">Кира.[NOM]</ta>
            <ta e="T174" id="Seg_2818" s="T173">кто-3SG</ta>
            <ta e="T175" id="Seg_2819" s="T174">фигурка-3SG.[NOM]</ta>
            <ta e="T176" id="Seg_2820" s="T175">зайчик.[NOM]</ta>
            <ta e="T177" id="Seg_2821" s="T176">быть-PST1-3SG</ta>
            <ta e="T178" id="Seg_2822" s="T177">а</ta>
            <ta e="T179" id="Seg_2823" s="T178">кто.[NOM]</ta>
            <ta e="T180" id="Seg_2824" s="T179">собственный-3SG</ta>
            <ta e="T181" id="Seg_2825" s="T180">кто.[NOM]</ta>
            <ta e="T182" id="Seg_2826" s="T181">быть-PST1-3SG</ta>
            <ta e="T183" id="Seg_2827" s="T182">кто.[NOM]</ta>
            <ta e="T184" id="Seg_2828" s="T183">собственный-3SG</ta>
            <ta e="T185" id="Seg_2829" s="T184">Надя.[NOM]</ta>
            <ta e="T186" id="Seg_2830" s="T185">собственный-3SG</ta>
            <ta e="T187" id="Seg_2831" s="T186">кто.[NOM]</ta>
            <ta e="T188" id="Seg_2832" s="T187">просто</ta>
            <ta e="T189" id="Seg_2833" s="T188">куропатка.[NOM]</ta>
            <ta e="T190" id="Seg_2834" s="T189">быть-PST1-3SG</ta>
            <ta e="T191" id="Seg_2835" s="T190">тот-ABL</ta>
            <ta e="T192" id="Seg_2836" s="T191">класть-PTCP.PST</ta>
            <ta e="T193" id="Seg_2837" s="T192">быть-PST1-3PL</ta>
            <ta e="T194" id="Seg_2838" s="T193">там</ta>
            <ta e="T195" id="Seg_2839" s="T194">два-COLL-3PL.[NOM]</ta>
            <ta e="T196" id="Seg_2840" s="T195">а</ta>
            <ta e="T197" id="Seg_2841" s="T196">куропатка-ACC</ta>
            <ta e="T198" id="Seg_2842" s="T197">кто-DAT/LOC</ta>
            <ta e="T199" id="Seg_2843" s="T198">класть-PTCP.PST</ta>
            <ta e="T200" id="Seg_2844" s="T199">быть-PST1-3PL</ta>
            <ta e="T201" id="Seg_2845" s="T200">кто.[NOM]</ta>
            <ta e="T202" id="Seg_2846" s="T201">быть-PST1-3SG=Q</ta>
            <ta e="T203" id="Seg_2847" s="T202">вот</ta>
            <ta e="T204" id="Seg_2848" s="T203">зайчик.[NOM]</ta>
            <ta e="T205" id="Seg_2849" s="T204">кто-3SG-DAT/LOC</ta>
            <ta e="T206" id="Seg_2850" s="T205">потом</ta>
            <ta e="T207" id="Seg_2851" s="T206">тот.[NOM]</ta>
            <ta e="T208" id="Seg_2852" s="T207">кто.[NOM]</ta>
            <ta e="T209" id="Seg_2853" s="T208">ну</ta>
            <ta e="T210" id="Seg_2854" s="T209">вот</ta>
            <ta e="T211" id="Seg_2855" s="T210">яма-3SG-DAT/LOC</ta>
            <ta e="T213" id="Seg_2856" s="T212">тот-ABL</ta>
            <ta e="T214" id="Seg_2857" s="T213">там</ta>
            <ta e="T215" id="Seg_2858" s="T214">1PL.[NOM]</ta>
            <ta e="T216" id="Seg_2859" s="T215">тот-ABL</ta>
            <ta e="T217" id="Seg_2860" s="T216">там</ta>
            <ta e="T219" id="Seg_2861" s="T218">потом</ta>
            <ta e="T220" id="Seg_2862" s="T219">позже</ta>
            <ta e="T221" id="Seg_2863" s="T220">сколько</ta>
            <ta e="T222" id="Seg_2864" s="T221">один</ta>
            <ta e="T223" id="Seg_2865" s="T222">сколько</ta>
            <ta e="T224" id="Seg_2866" s="T223">INDEF</ta>
            <ta e="T225" id="Seg_2867" s="T224">неделя.[NOM]</ta>
            <ta e="T226" id="Seg_2868" s="T225">проехать-PST2-3SG</ta>
            <ta e="T227" id="Seg_2869" s="T226">еще</ta>
            <ta e="T228" id="Seg_2870" s="T227">два-ORD-3PL-ACC</ta>
            <ta e="T232" id="Seg_2871" s="T231">идти-PST2-1PL</ta>
            <ta e="T234" id="Seg_2872" s="T233">тот-ABL</ta>
            <ta e="T235" id="Seg_2873" s="T234">туда</ta>
            <ta e="T236" id="Seg_2874" s="T235">идти-PST2-1PL</ta>
            <ta e="T237" id="Seg_2875" s="T236">тот.EMPH.[NOM]</ta>
            <ta e="T238" id="Seg_2876" s="T237">место-1PL-DAT/LOC</ta>
            <ta e="T240" id="Seg_2877" s="T239">тот</ta>
            <ta e="T241" id="Seg_2878" s="T240">место-DAT/LOC</ta>
            <ta e="T242" id="Seg_2879" s="T241">там</ta>
            <ta e="T243" id="Seg_2880" s="T242">кто.[NOM]</ta>
            <ta e="T244" id="Seg_2881" s="T243">есть</ta>
            <ta e="T248" id="Seg_2882" s="T247">есть</ta>
            <ta e="T249" id="Seg_2883" s="T248">быть-PST1-3SG</ta>
            <ta e="T250" id="Seg_2884" s="T249">EMPH</ta>
            <ta e="T251" id="Seg_2885" s="T250">тот</ta>
            <ta e="T252" id="Seg_2886" s="T251">зайчик.[NOM]</ta>
            <ta e="T253" id="Seg_2887" s="T252">моча-VBZ-EP-PASS/REFL-EP-PTCP.PST</ta>
            <ta e="T254" id="Seg_2888" s="T253">быть-PST1-3SG</ta>
            <ta e="T255" id="Seg_2889" s="T254">моча-PROPR.[NOM]</ta>
            <ta e="T256" id="Seg_2890" s="T255">быть-PST1-3SG</ta>
            <ta e="T258" id="Seg_2891" s="T257">а</ta>
            <ta e="T259" id="Seg_2892" s="T258">кто.[NOM]</ta>
            <ta e="T260" id="Seg_2893" s="T259">INDEF</ta>
            <ta e="T261" id="Seg_2894" s="T260">куропатка.[NOM]</ta>
            <ta e="T262" id="Seg_2895" s="T261">голова-3SG-ACC</ta>
            <ta e="T263" id="Seg_2896" s="T262">кто.[NOM]</ta>
            <ta e="T264" id="Seg_2897" s="T263">INDEF</ta>
            <ta e="T265" id="Seg_2898" s="T264">тащить-PTCP.PST</ta>
            <ta e="T266" id="Seg_2899" s="T265">быть-PST1-1PL</ta>
            <ta e="T268" id="Seg_2900" s="T267">тот-ABL</ta>
            <ta e="T269" id="Seg_2901" s="T268">позже</ta>
            <ta e="T270" id="Seg_2902" s="T269">1PL.[NOM]</ta>
            <ta e="T271" id="Seg_2903" s="T270">кататься-VBZ-CVB.SIM</ta>
            <ta e="T272" id="Seg_2904" s="T271">идти-PST2-1PL</ta>
            <ta e="T273" id="Seg_2905" s="T272">а</ta>
            <ta e="T274" id="Seg_2906" s="T273">кто.[NOM]</ta>
            <ta e="T275" id="Seg_2907" s="T274">INDEF</ta>
            <ta e="T276" id="Seg_2908" s="T275">ээ</ta>
            <ta e="T277" id="Seg_2909" s="T276">дядя</ta>
            <ta e="T278" id="Seg_2910" s="T277">Прокопий.[NOM]</ta>
            <ta e="T279" id="Seg_2911" s="T278">ребенок-PL-ACC</ta>
            <ta e="T280" id="Seg_2912" s="T279">взять-PTCP.PST</ta>
            <ta e="T281" id="Seg_2913" s="T280">быть-PST1-3PL</ta>
            <ta e="T282" id="Seg_2914" s="T281">ээ</ta>
            <ta e="T283" id="Seg_2915" s="T282">NEG</ta>
            <ta e="T284" id="Seg_2916" s="T283">тот</ta>
            <ta e="T285" id="Seg_2917" s="T284">другой</ta>
            <ta e="T286" id="Seg_2918" s="T285">кто-DAT/LOC</ta>
            <ta e="T288" id="Seg_2919" s="T287">1PL.[NOM]</ta>
            <ta e="T289" id="Seg_2920" s="T288">кататься-VBZ-CVB.SIM</ta>
            <ta e="T290" id="Seg_2921" s="T289">идти-PST2-1PL</ta>
            <ta e="T291" id="Seg_2922" s="T290">тот-ABL</ta>
            <ta e="T292" id="Seg_2923" s="T291">назад</ta>
            <ta e="T293" id="Seg_2924" s="T292">приходить-PST1-1PL</ta>
            <ta e="T294" id="Seg_2925" s="T293">кто-DAT/LOC</ta>
            <ta e="T295" id="Seg_2926" s="T294">посёлок-DAT/LOC</ta>
            <ta e="T296" id="Seg_2927" s="T295">лыжа-PL-PROPR</ta>
            <ta e="T297" id="Seg_2928" s="T296">тот-ABL</ta>
            <ta e="T298" id="Seg_2929" s="T297">приходить-PST1-1PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_2930" s="T1">adv</ta>
            <ta e="T3" id="Seg_2931" s="T2">pers.[pro:case]</ta>
            <ta e="T4" id="Seg_2932" s="T3">propr-n:case</ta>
            <ta e="T5" id="Seg_2933" s="T4">post</ta>
            <ta e="T6" id="Seg_2934" s="T5">v-v:ptcp</ta>
            <ta e="T7" id="Seg_2935" s="T6">v-v:tense-v:poss.pn</ta>
            <ta e="T9" id="Seg_2936" s="T8">dempro-pro:case</ta>
            <ta e="T10" id="Seg_2937" s="T9">adv</ta>
            <ta e="T11" id="Seg_2938" s="T10">n.[n:case]</ta>
            <ta e="T12" id="Seg_2939" s="T11">interj</ta>
            <ta e="T13" id="Seg_2940" s="T12">pers.[pro:case]</ta>
            <ta e="T14" id="Seg_2941" s="T13">v-v:ptcp</ta>
            <ta e="T15" id="Seg_2942" s="T14">v-v:tense-v:poss.pn</ta>
            <ta e="T16" id="Seg_2943" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_2944" s="T16">v-v:ptcp</ta>
            <ta e="T18" id="Seg_2945" s="T17">v-v:tense-v:poss.pn</ta>
            <ta e="T20" id="Seg_2946" s="T19">dempro-pro:case</ta>
            <ta e="T21" id="Seg_2947" s="T20">propr.[n:case]</ta>
            <ta e="T22" id="Seg_2948" s="T21">n-n:(poss).[n:case]</ta>
            <ta e="T23" id="Seg_2949" s="T22">v-v:tense-v:poss.pn</ta>
            <ta e="T24" id="Seg_2950" s="T23">pers-pro:case</ta>
            <ta e="T25" id="Seg_2951" s="T24">v-v&gt;v-v:cvb</ta>
            <ta e="T26" id="Seg_2952" s="T25">v-v:tense-v:pred.pn</ta>
            <ta e="T27" id="Seg_2953" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_2954" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_2955" s="T28">pers.[pro:case]</ta>
            <ta e="T30" id="Seg_2956" s="T29">interj</ta>
            <ta e="T31" id="Seg_2957" s="T30">v-v&gt;v-v:cvb</ta>
            <ta e="T32" id="Seg_2958" s="T31">v-v:cvb</ta>
            <ta e="T33" id="Seg_2959" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_2960" s="T33">pers-pro:case</ta>
            <ta e="T35" id="Seg_2961" s="T34">post</ta>
            <ta e="T36" id="Seg_2962" s="T35">n-n:case</ta>
            <ta e="T38" id="Seg_2963" s="T37">dempro-pro:case</ta>
            <ta e="T39" id="Seg_2964" s="T38">pers.[pro:case]</ta>
            <ta e="T40" id="Seg_2965" s="T39">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T41" id="Seg_2966" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_2967" s="T41">dempro-pro:case</ta>
            <ta e="T43" id="Seg_2968" s="T42">pers.[pro:case]</ta>
            <ta e="T44" id="Seg_2969" s="T43">v-v:tense-v:poss.pn</ta>
            <ta e="T46" id="Seg_2970" s="T45">conj</ta>
            <ta e="T47" id="Seg_2971" s="T46">propr</ta>
            <ta e="T48" id="Seg_2972" s="T47">propr-propr&gt;adj</ta>
            <ta e="T49" id="Seg_2973" s="T48">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T301" id="Seg_2974" s="T49">n-n:poss-n:case</ta>
            <ta e="T50" id="Seg_2975" s="T301">post</ta>
            <ta e="T51" id="Seg_2976" s="T50">v-v:ptcp</ta>
            <ta e="T52" id="Seg_2977" s="T51">v-v:tense-v:poss.pn</ta>
            <ta e="T303" id="Seg_2978" s="T53">conj</ta>
            <ta e="T302" id="Seg_2979" s="T54">v-v:tense-v:pred.pn</ta>
            <ta e="T55" id="Seg_2980" s="T302">v-v:ptcp</ta>
            <ta e="T56" id="Seg_2981" s="T55">v-v:ptcp</ta>
            <ta e="T57" id="Seg_2982" s="T56">v-v:tense-v:poss.pn</ta>
            <ta e="T58" id="Seg_2983" s="T57">conj</ta>
            <ta e="T59" id="Seg_2984" s="T58">n</ta>
            <ta e="T60" id="Seg_2985" s="T59">propr.[n:case]</ta>
            <ta e="T61" id="Seg_2986" s="T60">pers-pro:(ins)-pro:case</ta>
            <ta e="T62" id="Seg_2987" s="T61">v-v&gt;v-v:ptcp</ta>
            <ta e="T63" id="Seg_2988" s="T62">v-v:tense-v:poss.pn</ta>
            <ta e="T65" id="Seg_2989" s="T64">adv</ta>
            <ta e="T66" id="Seg_2990" s="T65">que-pro:(num)-pro:case</ta>
            <ta e="T67" id="Seg_2991" s="T66">que.[pro:case]</ta>
            <ta e="T68" id="Seg_2992" s="T67">v-v:mood-v:pred.pn</ta>
            <ta e="T69" id="Seg_2993" s="T68">n</ta>
            <ta e="T70" id="Seg_2994" s="T69">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T71" id="Seg_2995" s="T70">que-pro:case</ta>
            <ta e="T73" id="Seg_2996" s="T72">dempro-pro:case</ta>
            <ta e="T74" id="Seg_2997" s="T73">pers.[pro:case]</ta>
            <ta e="T75" id="Seg_2998" s="T74">v-v:tense-v:poss.pn</ta>
            <ta e="T76" id="Seg_2999" s="T75">propr</ta>
            <ta e="T77" id="Seg_3000" s="T76">propr-propr&gt;adj.[n:case]</ta>
            <ta e="T78" id="Seg_3001" s="T77">dempro-pro:case</ta>
            <ta e="T79" id="Seg_3002" s="T78">pers.[pro:case]</ta>
            <ta e="T80" id="Seg_3003" s="T79">adv</ta>
            <ta e="T81" id="Seg_3004" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_3005" s="T81">adv</ta>
            <ta e="T83" id="Seg_3006" s="T82">v-v:tense-v:pred.pn</ta>
            <ta e="T85" id="Seg_3007" s="T84">dempro-pro:case</ta>
            <ta e="T86" id="Seg_3008" s="T85">adv</ta>
            <ta e="T87" id="Seg_3009" s="T86">que.[pro:case]</ta>
            <ta e="T88" id="Seg_3010" s="T87">que-pro:(num)-pro:case</ta>
            <ta e="T89" id="Seg_3011" s="T88">dempro</ta>
            <ta e="T90" id="Seg_3012" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_3013" s="T90">v-v:tense-v:pred.pn</ta>
            <ta e="T92" id="Seg_3014" s="T91">dempro-pro:case</ta>
            <ta e="T93" id="Seg_3015" s="T92">adv</ta>
            <ta e="T94" id="Seg_3016" s="T93">que.[pro:case]</ta>
            <ta e="T96" id="Seg_3017" s="T95">n.[n:case]</ta>
            <ta e="T97" id="Seg_3018" s="T96">que-pro:(poss)-pro:case</ta>
            <ta e="T98" id="Seg_3019" s="T97">n-n:poss-n:case</ta>
            <ta e="T99" id="Seg_3020" s="T98">v-v:ptcp</ta>
            <ta e="T100" id="Seg_3021" s="T99">v-v:tense-v:poss.pn</ta>
            <ta e="T102" id="Seg_3022" s="T101">dempro-pro:case</ta>
            <ta e="T103" id="Seg_3023" s="T102">v-v&gt;v</ta>
            <ta e="T104" id="Seg_3024" s="T103">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T106" id="Seg_3025" s="T105">adj</ta>
            <ta e="T107" id="Seg_3026" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_3027" s="T107">n.[n:case]</ta>
            <ta e="T109" id="Seg_3028" s="T108">v-v:tense-v:poss.pn</ta>
            <ta e="T110" id="Seg_3029" s="T109">adv</ta>
            <ta e="T111" id="Seg_3030" s="T110">pers.[pro:case]</ta>
            <ta e="T112" id="Seg_3031" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_3032" s="T112">v-v&gt;v-v:ptcp</ta>
            <ta e="T114" id="Seg_3033" s="T113">v-v:tense-v:poss.pn</ta>
            <ta e="T115" id="Seg_3034" s="T114">adv</ta>
            <ta e="T118" id="Seg_3035" s="T117">v-v:tense-v:poss.pn</ta>
            <ta e="T119" id="Seg_3036" s="T118">que.[pro:case]</ta>
            <ta e="T120" id="Seg_3037" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_3038" s="T120">ptcl</ta>
            <ta e="T122" id="Seg_3039" s="T121">v-v:tense-v:poss.pn</ta>
            <ta e="T123" id="Seg_3040" s="T122">v-v:(ins)-v:ptcp</ta>
            <ta e="T125" id="Seg_3041" s="T124">dempro-pro:case</ta>
            <ta e="T126" id="Seg_3042" s="T125">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T127" id="Seg_3043" s="T126">dempro-pro:case</ta>
            <ta e="T128" id="Seg_3044" s="T127">adv</ta>
            <ta e="T129" id="Seg_3045" s="T128">que.[pro:case]</ta>
            <ta e="T130" id="Seg_3046" s="T129">propr</ta>
            <ta e="T131" id="Seg_3047" s="T130">propr.[n:case]</ta>
            <ta e="T132" id="Seg_3048" s="T131">ptcl</ta>
            <ta e="T133" id="Seg_3049" s="T132">v-v:tense-v:poss.pn</ta>
            <ta e="T134" id="Seg_3050" s="T133">n-n:poss-n:case</ta>
            <ta e="T135" id="Seg_3051" s="T134">post</ta>
            <ta e="T136" id="Seg_3052" s="T135">dempro-pro:case</ta>
            <ta e="T137" id="Seg_3053" s="T136">n</ta>
            <ta e="T138" id="Seg_3054" s="T137">propr.[n:case]</ta>
            <ta e="T139" id="Seg_3055" s="T138">dempro-pro:case</ta>
            <ta e="T140" id="Seg_3056" s="T139">propr.[n:case]</ta>
            <ta e="T141" id="Seg_3057" s="T140">propr</ta>
            <ta e="T142" id="Seg_3058" s="T141">propr.[n:case]</ta>
            <ta e="T143" id="Seg_3059" s="T142">n-n:(poss).[n:case]</ta>
            <ta e="T144" id="Seg_3060" s="T143">dempro-pro:case</ta>
            <ta e="T145" id="Seg_3061" s="T144">propr</ta>
            <ta e="T146" id="Seg_3062" s="T145">n</ta>
            <ta e="T147" id="Seg_3063" s="T146">propr.[n:case]</ta>
            <ta e="T149" id="Seg_3064" s="T148">dempro-pro:case</ta>
            <ta e="T150" id="Seg_3065" s="T149">pers.[pro:case]</ta>
            <ta e="T151" id="Seg_3066" s="T150">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T153" id="Seg_3067" s="T152">dempro-pro:case</ta>
            <ta e="T154" id="Seg_3068" s="T153">que.[pro:case]</ta>
            <ta e="T155" id="Seg_3069" s="T154">v-v:tense-v:poss.pn</ta>
            <ta e="T156" id="Seg_3070" s="T155">que.[pro:case]</ta>
            <ta e="T157" id="Seg_3071" s="T156">v-v:tense-v:poss.pn</ta>
            <ta e="T158" id="Seg_3072" s="T157">interj</ta>
            <ta e="T159" id="Seg_3073" s="T158">n</ta>
            <ta e="T160" id="Seg_3074" s="T159">propr.[n:case]</ta>
            <ta e="T161" id="Seg_3075" s="T160">n-n:(num)-n:case</ta>
            <ta e="T162" id="Seg_3076" s="T161">que.[pro:case]</ta>
            <ta e="T163" id="Seg_3077" s="T162">v-v:ptcp</ta>
            <ta e="T164" id="Seg_3078" s="T163">v-v:tense-v:poss.pn</ta>
            <ta e="T165" id="Seg_3079" s="T164">que.[pro:case]</ta>
            <ta e="T166" id="Seg_3080" s="T165">v-v:tense-v:poss.pn</ta>
            <ta e="T167" id="Seg_3081" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_3082" s="T167">n-n:(num)-n:case</ta>
            <ta e="T170" id="Seg_3083" s="T169">dempro-pro:case</ta>
            <ta e="T300" id="Seg_3084" s="T170">ptcl</ta>
            <ta e="T172" id="Seg_3085" s="T171">interj</ta>
            <ta e="T173" id="Seg_3086" s="T172">propr.[n:case]</ta>
            <ta e="T174" id="Seg_3087" s="T173">que-pro:(poss)</ta>
            <ta e="T175" id="Seg_3088" s="T174">n-n:(poss).[n:case]</ta>
            <ta e="T176" id="Seg_3089" s="T175">n.[n:case]</ta>
            <ta e="T177" id="Seg_3090" s="T176">v-v:tense-v:poss.pn</ta>
            <ta e="T178" id="Seg_3091" s="T177">conj</ta>
            <ta e="T179" id="Seg_3092" s="T178">que.[pro:case]</ta>
            <ta e="T180" id="Seg_3093" s="T179">adj-n:(poss)</ta>
            <ta e="T181" id="Seg_3094" s="T180">que.[pro:case]</ta>
            <ta e="T182" id="Seg_3095" s="T181">v-v:tense-v:poss.pn</ta>
            <ta e="T183" id="Seg_3096" s="T182">que.[pro:case]</ta>
            <ta e="T184" id="Seg_3097" s="T183">adj-n:(poss)</ta>
            <ta e="T185" id="Seg_3098" s="T184">propr.[n:case]</ta>
            <ta e="T186" id="Seg_3099" s="T185">adj-n:(poss)</ta>
            <ta e="T187" id="Seg_3100" s="T186">que.[pro:case]</ta>
            <ta e="T188" id="Seg_3101" s="T187">adv</ta>
            <ta e="T189" id="Seg_3102" s="T188">n.[n:case]</ta>
            <ta e="T190" id="Seg_3103" s="T189">v-v:tense-v:poss.pn</ta>
            <ta e="T191" id="Seg_3104" s="T190">dempro-pro:case</ta>
            <ta e="T192" id="Seg_3105" s="T191">v-v:ptcp</ta>
            <ta e="T193" id="Seg_3106" s="T192">v-v:tense-v:poss.pn</ta>
            <ta e="T194" id="Seg_3107" s="T193">adv</ta>
            <ta e="T195" id="Seg_3108" s="T194">cardnum-cardnum&gt;collnum-n:(poss).[n:case]</ta>
            <ta e="T196" id="Seg_3109" s="T195">conj</ta>
            <ta e="T197" id="Seg_3110" s="T196">n-n:case</ta>
            <ta e="T198" id="Seg_3111" s="T197">que-pro:case</ta>
            <ta e="T199" id="Seg_3112" s="T198">v-v:ptcp</ta>
            <ta e="T200" id="Seg_3113" s="T199">v-v:tense-v:poss.pn</ta>
            <ta e="T201" id="Seg_3114" s="T200">que.[pro:case]</ta>
            <ta e="T202" id="Seg_3115" s="T201">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T203" id="Seg_3116" s="T202">ptcl</ta>
            <ta e="T204" id="Seg_3117" s="T203">n.[n:case]</ta>
            <ta e="T205" id="Seg_3118" s="T204">que-pro:(poss)-pro:case</ta>
            <ta e="T206" id="Seg_3119" s="T205">adv</ta>
            <ta e="T207" id="Seg_3120" s="T206">dempro.[pro:case]</ta>
            <ta e="T208" id="Seg_3121" s="T207">que.[pro:case]</ta>
            <ta e="T209" id="Seg_3122" s="T208">ptcl</ta>
            <ta e="T210" id="Seg_3123" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_3124" s="T210">n-n:poss-n:case</ta>
            <ta e="T213" id="Seg_3125" s="T212">dempro-pro:case</ta>
            <ta e="T214" id="Seg_3126" s="T213">adv</ta>
            <ta e="T215" id="Seg_3127" s="T214">pers.[pro:case]</ta>
            <ta e="T216" id="Seg_3128" s="T215">dempro-pro:case</ta>
            <ta e="T217" id="Seg_3129" s="T216">adv</ta>
            <ta e="T219" id="Seg_3130" s="T218">adv</ta>
            <ta e="T220" id="Seg_3131" s="T219">adv</ta>
            <ta e="T221" id="Seg_3132" s="T220">que</ta>
            <ta e="T222" id="Seg_3133" s="T221">cardnum</ta>
            <ta e="T223" id="Seg_3134" s="T222">que</ta>
            <ta e="T224" id="Seg_3135" s="T223">ptcl</ta>
            <ta e="T225" id="Seg_3136" s="T224">n.[n:case]</ta>
            <ta e="T226" id="Seg_3137" s="T225">v-v:tense-v:poss.pn</ta>
            <ta e="T227" id="Seg_3138" s="T226">adv</ta>
            <ta e="T228" id="Seg_3139" s="T227">cardnum-cardnum&gt;ordnum-n:poss-n:case</ta>
            <ta e="T232" id="Seg_3140" s="T231">v-v:tense-v:pred.pn</ta>
            <ta e="T234" id="Seg_3141" s="T233">dempro-pro:case</ta>
            <ta e="T235" id="Seg_3142" s="T234">adv</ta>
            <ta e="T236" id="Seg_3143" s="T235">v-v:tense-v:pred.pn</ta>
            <ta e="T237" id="Seg_3144" s="T236">dempro.[pro:case]</ta>
            <ta e="T238" id="Seg_3145" s="T237">n-n:poss-n:case</ta>
            <ta e="T240" id="Seg_3146" s="T239">dempro</ta>
            <ta e="T241" id="Seg_3147" s="T240">n-n:case</ta>
            <ta e="T242" id="Seg_3148" s="T241">adv</ta>
            <ta e="T243" id="Seg_3149" s="T242">que.[pro:case]</ta>
            <ta e="T244" id="Seg_3150" s="T243">ptcl</ta>
            <ta e="T248" id="Seg_3151" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_3152" s="T248">v-v:tense-v:poss.pn</ta>
            <ta e="T250" id="Seg_3153" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_3154" s="T250">dempro</ta>
            <ta e="T252" id="Seg_3155" s="T251">n.[n:case]</ta>
            <ta e="T253" id="Seg_3156" s="T252">n-n&gt;v-v:(ins)-v&gt;v-v:(ins)-v:ptcp.</ta>
            <ta e="T254" id="Seg_3157" s="T253">v-v:tense-v:poss.pn</ta>
            <ta e="T255" id="Seg_3158" s="T254">n-n&gt;adj.[n:case]</ta>
            <ta e="T256" id="Seg_3159" s="T255">v-v:tense-v:poss.pn</ta>
            <ta e="T258" id="Seg_3160" s="T257">conj</ta>
            <ta e="T259" id="Seg_3161" s="T258">que.[pro:case]</ta>
            <ta e="T260" id="Seg_3162" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_3163" s="T260">n.[n:case]</ta>
            <ta e="T262" id="Seg_3164" s="T261">n-n:poss-n:case</ta>
            <ta e="T263" id="Seg_3165" s="T262">que.[pro:case]</ta>
            <ta e="T264" id="Seg_3166" s="T263">ptcl</ta>
            <ta e="T265" id="Seg_3167" s="T264">v-v:ptcp</ta>
            <ta e="T266" id="Seg_3168" s="T265">v-v:tense-v:poss.pn</ta>
            <ta e="T268" id="Seg_3169" s="T267">dempro-pro:case</ta>
            <ta e="T269" id="Seg_3170" s="T268">adv</ta>
            <ta e="T270" id="Seg_3171" s="T269">pers.[pro:case]</ta>
            <ta e="T271" id="Seg_3172" s="T270">v-v&gt;v-v:cvb</ta>
            <ta e="T272" id="Seg_3173" s="T271">v-v:tense-v:pred.pn</ta>
            <ta e="T273" id="Seg_3174" s="T272">conj</ta>
            <ta e="T274" id="Seg_3175" s="T273">que.[pro:case]</ta>
            <ta e="T275" id="Seg_3176" s="T274">ptcl</ta>
            <ta e="T276" id="Seg_3177" s="T275">interj</ta>
            <ta e="T277" id="Seg_3178" s="T276">n</ta>
            <ta e="T278" id="Seg_3179" s="T277">propr.[n:case]</ta>
            <ta e="T279" id="Seg_3180" s="T278">n-n:(num)-n:case</ta>
            <ta e="T280" id="Seg_3181" s="T279">v-v:ptcp</ta>
            <ta e="T281" id="Seg_3182" s="T280">v-v:tense-v:poss.pn</ta>
            <ta e="T282" id="Seg_3183" s="T281">interj</ta>
            <ta e="T283" id="Seg_3184" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_3185" s="T283">dempro</ta>
            <ta e="T285" id="Seg_3186" s="T284">adj</ta>
            <ta e="T286" id="Seg_3187" s="T285">que-pro:case</ta>
            <ta e="T288" id="Seg_3188" s="T287">pers.[pro:case]</ta>
            <ta e="T289" id="Seg_3189" s="T288">v-v&gt;v-v:cvb</ta>
            <ta e="T290" id="Seg_3190" s="T289">v-v:tense-v:pred.pn</ta>
            <ta e="T291" id="Seg_3191" s="T290">dempro-pro:case</ta>
            <ta e="T292" id="Seg_3192" s="T291">adv</ta>
            <ta e="T293" id="Seg_3193" s="T292">v-v:tense-v:poss.pn</ta>
            <ta e="T294" id="Seg_3194" s="T293">que-pro:case</ta>
            <ta e="T295" id="Seg_3195" s="T294">n-n:case</ta>
            <ta e="T296" id="Seg_3196" s="T295">n-n:(num)-n&gt;adj</ta>
            <ta e="T297" id="Seg_3197" s="T296">dempro-pro:case</ta>
            <ta e="T298" id="Seg_3198" s="T297">v-v:tense-v:poss.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_3199" s="T1">adv</ta>
            <ta e="T3" id="Seg_3200" s="T2">pers</ta>
            <ta e="T4" id="Seg_3201" s="T3">propr</ta>
            <ta e="T5" id="Seg_3202" s="T4">post</ta>
            <ta e="T6" id="Seg_3203" s="T5">v</ta>
            <ta e="T7" id="Seg_3204" s="T6">aux</ta>
            <ta e="T9" id="Seg_3205" s="T8">dempro</ta>
            <ta e="T10" id="Seg_3206" s="T9">adv</ta>
            <ta e="T11" id="Seg_3207" s="T10">n</ta>
            <ta e="T12" id="Seg_3208" s="T11">interj</ta>
            <ta e="T13" id="Seg_3209" s="T12">pers</ta>
            <ta e="T14" id="Seg_3210" s="T13">v</ta>
            <ta e="T15" id="Seg_3211" s="T14">aux</ta>
            <ta e="T16" id="Seg_3212" s="T15">n</ta>
            <ta e="T17" id="Seg_3213" s="T16">v</ta>
            <ta e="T18" id="Seg_3214" s="T17">aux</ta>
            <ta e="T20" id="Seg_3215" s="T19">dempro</ta>
            <ta e="T21" id="Seg_3216" s="T20">propr</ta>
            <ta e="T22" id="Seg_3217" s="T21">n</ta>
            <ta e="T23" id="Seg_3218" s="T22">v</ta>
            <ta e="T24" id="Seg_3219" s="T23">pers</ta>
            <ta e="T25" id="Seg_3220" s="T24">v</ta>
            <ta e="T26" id="Seg_3221" s="T25">v</ta>
            <ta e="T27" id="Seg_3222" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_3223" s="T27">n</ta>
            <ta e="T29" id="Seg_3224" s="T28">pers</ta>
            <ta e="T30" id="Seg_3225" s="T29">interj</ta>
            <ta e="T31" id="Seg_3226" s="T30">v</ta>
            <ta e="T32" id="Seg_3227" s="T31">v</ta>
            <ta e="T33" id="Seg_3228" s="T32">ptcl</ta>
            <ta e="T34" id="Seg_3229" s="T33">pers</ta>
            <ta e="T35" id="Seg_3230" s="T34">post</ta>
            <ta e="T36" id="Seg_3231" s="T35">n</ta>
            <ta e="T38" id="Seg_3232" s="T37">dempro</ta>
            <ta e="T39" id="Seg_3233" s="T38">pers</ta>
            <ta e="T40" id="Seg_3234" s="T39">v</ta>
            <ta e="T41" id="Seg_3235" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_3236" s="T41">dempro</ta>
            <ta e="T43" id="Seg_3237" s="T42">pers</ta>
            <ta e="T44" id="Seg_3238" s="T43">v</ta>
            <ta e="T46" id="Seg_3239" s="T45">conj</ta>
            <ta e="T47" id="Seg_3240" s="T46">propr</ta>
            <ta e="T48" id="Seg_3241" s="T47">adj</ta>
            <ta e="T49" id="Seg_3242" s="T48">emphpro</ta>
            <ta e="T301" id="Seg_3243" s="T49">n</ta>
            <ta e="T50" id="Seg_3244" s="T301">post</ta>
            <ta e="T51" id="Seg_3245" s="T50">v</ta>
            <ta e="T52" id="Seg_3246" s="T51">aux</ta>
            <ta e="T303" id="Seg_3247" s="T53">conj</ta>
            <ta e="T302" id="Seg_3248" s="T54">v</ta>
            <ta e="T55" id="Seg_3249" s="T302">v</ta>
            <ta e="T56" id="Seg_3250" s="T55">aux</ta>
            <ta e="T57" id="Seg_3251" s="T56">aux</ta>
            <ta e="T58" id="Seg_3252" s="T57">conj</ta>
            <ta e="T59" id="Seg_3253" s="T58">n</ta>
            <ta e="T60" id="Seg_3254" s="T59">propr</ta>
            <ta e="T61" id="Seg_3255" s="T60">pers</ta>
            <ta e="T62" id="Seg_3256" s="T61">v</ta>
            <ta e="T63" id="Seg_3257" s="T62">aux</ta>
            <ta e="T65" id="Seg_3258" s="T64">adv</ta>
            <ta e="T66" id="Seg_3259" s="T65">que</ta>
            <ta e="T67" id="Seg_3260" s="T66">que</ta>
            <ta e="T68" id="Seg_3261" s="T67">v</ta>
            <ta e="T69" id="Seg_3262" s="T68">n</ta>
            <ta e="T70" id="Seg_3263" s="T69">adj</ta>
            <ta e="T71" id="Seg_3264" s="T70">que</ta>
            <ta e="T73" id="Seg_3265" s="T72">dempro</ta>
            <ta e="T74" id="Seg_3266" s="T73">pers</ta>
            <ta e="T75" id="Seg_3267" s="T74">v</ta>
            <ta e="T76" id="Seg_3268" s="T75">propr</ta>
            <ta e="T77" id="Seg_3269" s="T76">propr</ta>
            <ta e="T78" id="Seg_3270" s="T77">dempro</ta>
            <ta e="T79" id="Seg_3271" s="T78">pers</ta>
            <ta e="T80" id="Seg_3272" s="T79">adv</ta>
            <ta e="T81" id="Seg_3273" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_3274" s="T81">adv</ta>
            <ta e="T83" id="Seg_3275" s="T82">v</ta>
            <ta e="T85" id="Seg_3276" s="T84">dempro</ta>
            <ta e="T86" id="Seg_3277" s="T85">adv</ta>
            <ta e="T87" id="Seg_3278" s="T86">que</ta>
            <ta e="T88" id="Seg_3279" s="T87">que</ta>
            <ta e="T89" id="Seg_3280" s="T88">dempro</ta>
            <ta e="T90" id="Seg_3281" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_3282" s="T90">v</ta>
            <ta e="T92" id="Seg_3283" s="T91">dempro</ta>
            <ta e="T93" id="Seg_3284" s="T92">adv</ta>
            <ta e="T94" id="Seg_3285" s="T93">que</ta>
            <ta e="T96" id="Seg_3286" s="T95">n</ta>
            <ta e="T97" id="Seg_3287" s="T96">que</ta>
            <ta e="T98" id="Seg_3288" s="T97">n</ta>
            <ta e="T99" id="Seg_3289" s="T98">v</ta>
            <ta e="T100" id="Seg_3290" s="T99">aux</ta>
            <ta e="T102" id="Seg_3291" s="T101">dempro</ta>
            <ta e="T103" id="Seg_3292" s="T102">v</ta>
            <ta e="T104" id="Seg_3293" s="T103">v</ta>
            <ta e="T106" id="Seg_3294" s="T105">adj</ta>
            <ta e="T107" id="Seg_3295" s="T106">ptcl</ta>
            <ta e="T108" id="Seg_3296" s="T107">n</ta>
            <ta e="T109" id="Seg_3297" s="T108">cop</ta>
            <ta e="T110" id="Seg_3298" s="T109">adv</ta>
            <ta e="T111" id="Seg_3299" s="T110">pers</ta>
            <ta e="T112" id="Seg_3300" s="T111">ptcl</ta>
            <ta e="T113" id="Seg_3301" s="T112">v</ta>
            <ta e="T114" id="Seg_3302" s="T113">aux</ta>
            <ta e="T115" id="Seg_3303" s="T114">adv</ta>
            <ta e="T118" id="Seg_3304" s="T117">cop</ta>
            <ta e="T119" id="Seg_3305" s="T118">que</ta>
            <ta e="T120" id="Seg_3306" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_3307" s="T120">ptcl</ta>
            <ta e="T122" id="Seg_3308" s="T121">aux</ta>
            <ta e="T123" id="Seg_3309" s="T122">v</ta>
            <ta e="T125" id="Seg_3310" s="T124">dempro</ta>
            <ta e="T126" id="Seg_3311" s="T125">v</ta>
            <ta e="T127" id="Seg_3312" s="T126">dempro</ta>
            <ta e="T128" id="Seg_3313" s="T127">adv</ta>
            <ta e="T129" id="Seg_3314" s="T128">que</ta>
            <ta e="T130" id="Seg_3315" s="T129">propr</ta>
            <ta e="T131" id="Seg_3316" s="T130">propr</ta>
            <ta e="T132" id="Seg_3317" s="T131">ptcl</ta>
            <ta e="T133" id="Seg_3318" s="T132">cop</ta>
            <ta e="T134" id="Seg_3319" s="T133">n</ta>
            <ta e="T135" id="Seg_3320" s="T134">post</ta>
            <ta e="T136" id="Seg_3321" s="T135">dempro</ta>
            <ta e="T137" id="Seg_3322" s="T136">n</ta>
            <ta e="T138" id="Seg_3323" s="T137">propr</ta>
            <ta e="T139" id="Seg_3324" s="T138">dempro</ta>
            <ta e="T140" id="Seg_3325" s="T139">propr</ta>
            <ta e="T141" id="Seg_3326" s="T140">propr</ta>
            <ta e="T142" id="Seg_3327" s="T141">propr</ta>
            <ta e="T143" id="Seg_3328" s="T142">n</ta>
            <ta e="T144" id="Seg_3329" s="T143">dempro</ta>
            <ta e="T145" id="Seg_3330" s="T144">propr</ta>
            <ta e="T146" id="Seg_3331" s="T145">n</ta>
            <ta e="T147" id="Seg_3332" s="T146">propr</ta>
            <ta e="T149" id="Seg_3333" s="T148">dempro</ta>
            <ta e="T150" id="Seg_3334" s="T149">pers</ta>
            <ta e="T151" id="Seg_3335" s="T150">v</ta>
            <ta e="T153" id="Seg_3336" s="T152">dempro</ta>
            <ta e="T154" id="Seg_3337" s="T153">que</ta>
            <ta e="T155" id="Seg_3338" s="T154">cop</ta>
            <ta e="T156" id="Seg_3339" s="T155">que</ta>
            <ta e="T157" id="Seg_3340" s="T156">cop</ta>
            <ta e="T158" id="Seg_3341" s="T157">interj</ta>
            <ta e="T159" id="Seg_3342" s="T158">n</ta>
            <ta e="T160" id="Seg_3343" s="T159">propr</ta>
            <ta e="T161" id="Seg_3344" s="T160">n</ta>
            <ta e="T162" id="Seg_3345" s="T161">que</ta>
            <ta e="T163" id="Seg_3346" s="T162">v</ta>
            <ta e="T164" id="Seg_3347" s="T163">aux</ta>
            <ta e="T165" id="Seg_3348" s="T164">que</ta>
            <ta e="T166" id="Seg_3349" s="T165">cop</ta>
            <ta e="T167" id="Seg_3350" s="T166">ptcl</ta>
            <ta e="T168" id="Seg_3351" s="T167">n</ta>
            <ta e="T170" id="Seg_3352" s="T169">dempro</ta>
            <ta e="T300" id="Seg_3353" s="T170">ptcl</ta>
            <ta e="T172" id="Seg_3354" s="T171">interj</ta>
            <ta e="T173" id="Seg_3355" s="T172">propr</ta>
            <ta e="T174" id="Seg_3356" s="T173">que</ta>
            <ta e="T175" id="Seg_3357" s="T174">n</ta>
            <ta e="T176" id="Seg_3358" s="T175">n</ta>
            <ta e="T177" id="Seg_3359" s="T176">cop</ta>
            <ta e="T178" id="Seg_3360" s="T177">conj</ta>
            <ta e="T179" id="Seg_3361" s="T178">que</ta>
            <ta e="T180" id="Seg_3362" s="T179">adj</ta>
            <ta e="T181" id="Seg_3363" s="T180">que</ta>
            <ta e="T182" id="Seg_3364" s="T181">cop</ta>
            <ta e="T183" id="Seg_3365" s="T182">que</ta>
            <ta e="T184" id="Seg_3366" s="T183">adj</ta>
            <ta e="T185" id="Seg_3367" s="T184">propr</ta>
            <ta e="T186" id="Seg_3368" s="T185">adj</ta>
            <ta e="T187" id="Seg_3369" s="T186">que</ta>
            <ta e="T188" id="Seg_3370" s="T187">adv</ta>
            <ta e="T189" id="Seg_3371" s="T188">n</ta>
            <ta e="T190" id="Seg_3372" s="T189">cop</ta>
            <ta e="T191" id="Seg_3373" s="T190">dempro</ta>
            <ta e="T192" id="Seg_3374" s="T191">v</ta>
            <ta e="T193" id="Seg_3375" s="T192">aux</ta>
            <ta e="T194" id="Seg_3376" s="T193">adv</ta>
            <ta e="T195" id="Seg_3377" s="T194">cardnum</ta>
            <ta e="T196" id="Seg_3378" s="T195">conj</ta>
            <ta e="T197" id="Seg_3379" s="T196">n</ta>
            <ta e="T198" id="Seg_3380" s="T197">que</ta>
            <ta e="T199" id="Seg_3381" s="T198">v</ta>
            <ta e="T200" id="Seg_3382" s="T199">aux</ta>
            <ta e="T201" id="Seg_3383" s="T200">que</ta>
            <ta e="T202" id="Seg_3384" s="T201">cop</ta>
            <ta e="T203" id="Seg_3385" s="T202">ptcl</ta>
            <ta e="T204" id="Seg_3386" s="T203">n</ta>
            <ta e="T205" id="Seg_3387" s="T204">que</ta>
            <ta e="T206" id="Seg_3388" s="T205">adv</ta>
            <ta e="T207" id="Seg_3389" s="T206">dempro</ta>
            <ta e="T208" id="Seg_3390" s="T207">que</ta>
            <ta e="T209" id="Seg_3391" s="T208">ptcl</ta>
            <ta e="T210" id="Seg_3392" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_3393" s="T210">n</ta>
            <ta e="T213" id="Seg_3394" s="T212">dempro</ta>
            <ta e="T214" id="Seg_3395" s="T213">adv</ta>
            <ta e="T215" id="Seg_3396" s="T214">pers</ta>
            <ta e="T216" id="Seg_3397" s="T215">dempro</ta>
            <ta e="T217" id="Seg_3398" s="T216">adv</ta>
            <ta e="T219" id="Seg_3399" s="T218">adv</ta>
            <ta e="T220" id="Seg_3400" s="T219">adv</ta>
            <ta e="T221" id="Seg_3401" s="T220">que</ta>
            <ta e="T222" id="Seg_3402" s="T221">cardnum</ta>
            <ta e="T223" id="Seg_3403" s="T222">que</ta>
            <ta e="T224" id="Seg_3404" s="T223">ptcl</ta>
            <ta e="T225" id="Seg_3405" s="T224">n</ta>
            <ta e="T226" id="Seg_3406" s="T225">v</ta>
            <ta e="T227" id="Seg_3407" s="T226">adv</ta>
            <ta e="T228" id="Seg_3408" s="T227">ordnum</ta>
            <ta e="T232" id="Seg_3409" s="T231">v</ta>
            <ta e="T234" id="Seg_3410" s="T233">dempro</ta>
            <ta e="T235" id="Seg_3411" s="T234">adv</ta>
            <ta e="T236" id="Seg_3412" s="T235">v</ta>
            <ta e="T237" id="Seg_3413" s="T236">dempro</ta>
            <ta e="T238" id="Seg_3414" s="T237">n</ta>
            <ta e="T240" id="Seg_3415" s="T239">dempro</ta>
            <ta e="T241" id="Seg_3416" s="T240">n</ta>
            <ta e="T242" id="Seg_3417" s="T241">adv</ta>
            <ta e="T243" id="Seg_3418" s="T242">que</ta>
            <ta e="T244" id="Seg_3419" s="T243">ptcl</ta>
            <ta e="T248" id="Seg_3420" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_3421" s="T248">cop</ta>
            <ta e="T250" id="Seg_3422" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_3423" s="T250">dempro</ta>
            <ta e="T252" id="Seg_3424" s="T251">n</ta>
            <ta e="T253" id="Seg_3425" s="T252">v</ta>
            <ta e="T254" id="Seg_3426" s="T253">aux</ta>
            <ta e="T255" id="Seg_3427" s="T254">adj</ta>
            <ta e="T256" id="Seg_3428" s="T255">cop</ta>
            <ta e="T258" id="Seg_3429" s="T257">conj</ta>
            <ta e="T259" id="Seg_3430" s="T258">que</ta>
            <ta e="T260" id="Seg_3431" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_3432" s="T260">n</ta>
            <ta e="T262" id="Seg_3433" s="T261">n</ta>
            <ta e="T263" id="Seg_3434" s="T262">que</ta>
            <ta e="T264" id="Seg_3435" s="T263">ptcl</ta>
            <ta e="T265" id="Seg_3436" s="T264">v</ta>
            <ta e="T266" id="Seg_3437" s="T265">aux</ta>
            <ta e="T268" id="Seg_3438" s="T267">dempro</ta>
            <ta e="T269" id="Seg_3439" s="T268">adv</ta>
            <ta e="T270" id="Seg_3440" s="T269">pers</ta>
            <ta e="T271" id="Seg_3441" s="T270">v</ta>
            <ta e="T272" id="Seg_3442" s="T271">v</ta>
            <ta e="T273" id="Seg_3443" s="T272">conj</ta>
            <ta e="T274" id="Seg_3444" s="T273">que</ta>
            <ta e="T275" id="Seg_3445" s="T274">ptcl</ta>
            <ta e="T276" id="Seg_3446" s="T275">interj</ta>
            <ta e="T277" id="Seg_3447" s="T276">n</ta>
            <ta e="T278" id="Seg_3448" s="T277">propr</ta>
            <ta e="T279" id="Seg_3449" s="T278">n</ta>
            <ta e="T280" id="Seg_3450" s="T279">v</ta>
            <ta e="T281" id="Seg_3451" s="T280">aux</ta>
            <ta e="T282" id="Seg_3452" s="T281">interj</ta>
            <ta e="T283" id="Seg_3453" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_3454" s="T283">dempro</ta>
            <ta e="T285" id="Seg_3455" s="T284">adj</ta>
            <ta e="T286" id="Seg_3456" s="T285">que</ta>
            <ta e="T288" id="Seg_3457" s="T287">pers</ta>
            <ta e="T289" id="Seg_3458" s="T288">v</ta>
            <ta e="T290" id="Seg_3459" s="T289">v</ta>
            <ta e="T291" id="Seg_3460" s="T290">dempro</ta>
            <ta e="T292" id="Seg_3461" s="T291">adv</ta>
            <ta e="T293" id="Seg_3462" s="T292">v</ta>
            <ta e="T294" id="Seg_3463" s="T293">que</ta>
            <ta e="T295" id="Seg_3464" s="T294">n</ta>
            <ta e="T296" id="Seg_3465" s="T295">adj</ta>
            <ta e="T297" id="Seg_3466" s="T296">dempro</ta>
            <ta e="T298" id="Seg_3467" s="T297">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_3468" s="T3">RUS:cult</ta>
            <ta e="T11" id="Seg_3469" s="T10">RUS:core</ta>
            <ta e="T21" id="Seg_3470" s="T20">RUS:cult</ta>
            <ta e="T22" id="Seg_3471" s="T21">RUS:cult</ta>
            <ta e="T25" id="Seg_3472" s="T24">RUS:cult</ta>
            <ta e="T31" id="Seg_3473" s="T30">RUS:cult</ta>
            <ta e="T46" id="Seg_3474" s="T45">RUS:gram</ta>
            <ta e="T47" id="Seg_3475" s="T46">RUS:cult</ta>
            <ta e="T48" id="Seg_3476" s="T47">RUS:cult</ta>
            <ta e="T50" id="Seg_3477" s="T49">RUS:core</ta>
            <ta e="T54" id="Seg_3478" s="T53">RUS:gram</ta>
            <ta e="T58" id="Seg_3479" s="T57">RUS:gram</ta>
            <ta e="T59" id="Seg_3480" s="T58">RUS:core</ta>
            <ta e="T60" id="Seg_3481" s="T59">RUS:cult</ta>
            <ta e="T69" id="Seg_3482" s="T68">RUS:cult</ta>
            <ta e="T70" id="Seg_3483" s="T69">RUS:cult</ta>
            <ta e="T76" id="Seg_3484" s="T75">RUS:cult</ta>
            <ta e="T77" id="Seg_3485" s="T76">RUS:cult</ta>
            <ta e="T81" id="Seg_3486" s="T80">RUS:mod</ta>
            <ta e="T82" id="Seg_3487" s="T81">RUS:mod</ta>
            <ta e="T96" id="Seg_3488" s="T95">RUS:core</ta>
            <ta e="T103" id="Seg_3489" s="T102">RUS:cult</ta>
            <ta e="T104" id="Seg_3490" s="T103">RUS:cult</ta>
            <ta e="T108" id="Seg_3491" s="T107">RUS:core</ta>
            <ta e="T112" id="Seg_3492" s="T111">RUS:mod</ta>
            <ta e="T113" id="Seg_3493" s="T112">RUS:core</ta>
            <ta e="T126" id="Seg_3494" s="T125">RUS:cult</ta>
            <ta e="T130" id="Seg_3495" s="T129">RUS:cult</ta>
            <ta e="T131" id="Seg_3496" s="T130">RUS:cult</ta>
            <ta e="T134" id="Seg_3497" s="T133">RUS:core</ta>
            <ta e="T137" id="Seg_3498" s="T136">RUS:core</ta>
            <ta e="T138" id="Seg_3499" s="T137">RUS:cult</ta>
            <ta e="T140" id="Seg_3500" s="T139">RUS:cult</ta>
            <ta e="T141" id="Seg_3501" s="T140">RUS:cult</ta>
            <ta e="T142" id="Seg_3502" s="T141">RUS:cult</ta>
            <ta e="T145" id="Seg_3503" s="T144">RUS:cult</ta>
            <ta e="T146" id="Seg_3504" s="T145">RUS:core</ta>
            <ta e="T147" id="Seg_3505" s="T146">RUS:cult</ta>
            <ta e="T151" id="Seg_3506" s="T150">RUS:cult</ta>
            <ta e="T159" id="Seg_3507" s="T158">RUS:core</ta>
            <ta e="T160" id="Seg_3508" s="T159">RUS:cult</ta>
            <ta e="T168" id="Seg_3509" s="T167">RUS:cult</ta>
            <ta e="T173" id="Seg_3510" s="T172">RUS:cult</ta>
            <ta e="T175" id="Seg_3511" s="T174">RUS:cult</ta>
            <ta e="T176" id="Seg_3512" s="T175">RUS:core</ta>
            <ta e="T178" id="Seg_3513" s="T177">RUS:gram</ta>
            <ta e="T185" id="Seg_3514" s="T184">RUS:cult</ta>
            <ta e="T189" id="Seg_3515" s="T188">RUS:cult</ta>
            <ta e="T196" id="Seg_3516" s="T195">RUS:gram</ta>
            <ta e="T197" id="Seg_3517" s="T196">RUS:cult</ta>
            <ta e="T204" id="Seg_3518" s="T203">RUS:core</ta>
            <ta e="T209" id="Seg_3519" s="T208">RUS:disc</ta>
            <ta e="T211" id="Seg_3520" s="T210">RUS:core</ta>
            <ta e="T225" id="Seg_3521" s="T224">RUS:cult</ta>
            <ta e="T227" id="Seg_3522" s="T226">RUS:mod</ta>
            <ta e="T238" id="Seg_3523" s="T237">RUS:core</ta>
            <ta e="T241" id="Seg_3524" s="T240">RUS:core</ta>
            <ta e="T252" id="Seg_3525" s="T251">RUS:core</ta>
            <ta e="T258" id="Seg_3526" s="T257">RUS:gram</ta>
            <ta e="T261" id="Seg_3527" s="T260">RUS:cult</ta>
            <ta e="T271" id="Seg_3528" s="T270">RUS:cult</ta>
            <ta e="T273" id="Seg_3529" s="T272">RUS:gram</ta>
            <ta e="T277" id="Seg_3530" s="T276">RUS:core</ta>
            <ta e="T278" id="Seg_3531" s="T277">RUS:cult</ta>
            <ta e="T289" id="Seg_3532" s="T288">RUS:cult</ta>
            <ta e="T295" id="Seg_3533" s="T294">RUS:cult</ta>
            <ta e="T296" id="Seg_3534" s="T295">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_3535" s="T1">Once I was walking around with Regina.</ta>
            <ta e="T18" id="Seg_3536" s="T8">Then there aunt ehm… we were walking around, we were taking care of a child.</ta>
            <ta e="T36" id="Seg_3537" s="T19">Then Kira's mother called me: "Do you want to go for a ride in the tundra with us, ehm, to go for a ride in the tundra with me?" </ta>
            <ta e="T44" id="Seg_3538" s="T37">Then I said, yes, then we went.</ta>
            <ta e="T52" id="Seg_3539" s="T45">And Polina Alekseevna and her family stayed.</ta>
            <ta e="T63" id="Seg_3540" s="T53">And s/he came, they went [to aunt Nastya] and aunt Nastya took a photo of them (?).</ta>
            <ta e="T71" id="Seg_3541" s="T64">Now whatchamacallit, how does one say, a snow scooter, they have a snow scooter.</ta>
            <ta e="T83" id="Seg_3542" s="T299">Then they, Polina Alekseevna with her family came, then we went on.</ta>
            <ta e="T100" id="Seg_3543" s="T84">Then we stopped, then we saw there the whatchamacallit, the hole of a small hare.</ta>
            <ta e="T104" id="Seg_3544" s="T101">Then we rode and rode.</ta>
            <ta e="T123" id="Seg_3545" s="T105">There was a big hill, we didn't notice [it], it was completetly white there, there was nothing to be seen.</ta>
            <ta e="T147" id="Seg_3546" s="T124">Then we rode, there was Polina Alekseevna with her family, then aunt Nastya, Nukushka, the child of Lyudmila Nikolaevna, then Annushka, aunt Nastya.</ta>
            <ta e="T151" id="Seg_3547" s="T148">Then we rode.</ta>
            <ta e="T168" id="Seg_3548" s="T152">Then there was whatchamacallit, uncle Parako had made whatchamacallit for the children, figures.</ta>
            <ta e="T190" id="Seg_3549" s="T169">Then Kira's figure was a small hare, and whose one, Nadyas one was simply a partridge.</ta>
            <ta e="T211" id="Seg_3550" s="T190">Then they laid both of them, they laid the partridge to whatchamacallit, what was it, to the hare's whatchamacallit, then, well, into its hole.</ta>
            <ta e="T232" id="Seg_3551" s="T212">Then there we, then later, one or how many weeks had passed, we went there for a second time.</ta>
            <ta e="T238" id="Seg_3552" s="T233">Then we went to that place.</ta>
            <ta e="T256" id="Seg_3553" s="T239">At the place there was the same hare, that hare was peed on, it was full of urine.</ta>
            <ta e="T266" id="Seg_3554" s="T257">And somebody had torn off the partridge's head.</ta>
            <ta e="T286" id="Seg_3555" s="T267">Then later we went to ride and somebody, uncle Prokula had taken the children, eh no, that's something different.</ta>
            <ta e="T298" id="Seg_3556" s="T287">We went to ride, then we came back to the whatchamacallit, to the village, we came with skies.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_3557" s="T1">Einmal ging ich mit Regina herum.</ta>
            <ta e="T18" id="Seg_3558" s="T8">Dann dort die Tante, äh, wir liefen umher, wir passten auf ein Kind auf.</ta>
            <ta e="T36" id="Seg_3559" s="T19">Dann rief mich Kiras Mutter [an]: "Möchtet ihr mit uns in die Tundra fahren, äh, mit mit in die Tundra fahren?"</ta>
            <ta e="T44" id="Seg_3560" s="T37">Dann sagte ich ja, dann gingen wir.</ta>
            <ta e="T52" id="Seg_3561" s="T45">Und Polina Alekseevna und ihre Familie blieben da.</ta>
            <ta e="T63" id="Seg_3562" s="T53">Und er/sie kam, sie kamen [zu Tante Nastja] und Tante Nastja fotografierte sie (?).</ta>
            <ta e="T71" id="Seg_3563" s="T64">Jetzt Dings, wie sagt man, ein Schneemobil, sie haben ein Schneemobil. </ta>
            <ta e="T83" id="Seg_3564" s="T299">Dann kamen sie, Polina Alekseevna und ihre Familie, dann fuhren wir weiter.</ta>
            <ta e="T100" id="Seg_3565" s="T84">Dann hielten wir an, dann sahen wir Dings, die Höhle eines Häschens.</ta>
            <ta e="T104" id="Seg_3566" s="T101">Dann fuhren wir und fuhren wir.</ta>
            <ta e="T123" id="Seg_3567" s="T105">Da war ein großer Hügel, wir bemerkten [ihn] nicht, es war ganz weiß dort, es war nichts zu sehen. </ta>
            <ta e="T147" id="Seg_3568" s="T124">Dann fuhren wir, Polina Alekseevna mit ihrer Familia war da, dann Tante Nastja, Nukushka, das Kind von Ljudmila Nikolajevna, dann Anuschka, Tante Nastja.</ta>
            <ta e="T151" id="Seg_3569" s="T148">Dann fuhren wir. </ta>
            <ta e="T168" id="Seg_3570" s="T152">Dann da war Dings, Onkel Parako hatte seinen Kindern Dings gemacht, Figürchen.</ta>
            <ta e="T190" id="Seg_3571" s="T169">Und dann Kiras Figürchen war ein Häschen, und von wem, von Dings, Nadjas war einfach ein Rebhuhn.</ta>
            <ta e="T211" id="Seg_3572" s="T190">Dann legten sie beide hin, sie legten das Rebhuhn in Dings, was war es, in das Dings vom Häschen, dann, nun, in sein Loch.</ta>
            <ta e="T232" id="Seg_3573" s="T212">Dann wir dort, dann später, eine oder wie viel Wochen waren vergangen, wir fuhren ein zweites Mal dorthin.</ta>
            <ta e="T238" id="Seg_3574" s="T233">Dann fuhren wir zu jenem Ort.</ta>
            <ta e="T256" id="Seg_3575" s="T239">Dort war dasselbe Häschen, das Häschen war vollgepinkelt, es war voller Urin.</ta>
            <ta e="T266" id="Seg_3576" s="T257">Und irgendwer hatte den Kopf des Rebhuhns abgerissen.</ta>
            <ta e="T286" id="Seg_3577" s="T267">Dann später fuhren wir und jemand, Onkel Prokula hatte die Kinder genommen, äh nein, das ist etwas anderes.</ta>
            <ta e="T298" id="Seg_3578" s="T287">Wir fuhren [Ski], dann kamen wir zurück nach Dings, ins Dorf, wir kamen mit Skiern.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_3579" s="T1">Однажды мы с Региной гуляли.</ta>
            <ta e="T18" id="Seg_3580" s="T8">Потом там тётя ааа… мы гуляли, ребёнка нянчили.</ta>
            <ta e="T36" id="Seg_3581" s="T19">Потом Кирина мама меня позвала: "Кататься поедете с нами в тундру, ээ, поедете кататься со мной в тундру?"</ta>
            <ta e="T44" id="Seg_3582" s="T37">Потом я сказала да, потом мы пошли.</ta>
            <ta e="T52" id="Seg_3583" s="T45">А Полина Алексеевна и ее семья остались.</ta>
            <ta e="T63" id="Seg_3584" s="T53">Потом они шли [в сторону тёти Насти], и тётя настя фотографировала их (?).</ta>
            <ta e="T71" id="Seg_3585" s="T64">Сейчас это, как говорят, буранч у них был, буран или что.</ta>
            <ta e="T83" id="Seg_3586" s="T299">Потом они, Полина Алексеевна с семьёй пришли, потом мы дальше пошли.</ta>
            <ta e="T100" id="Seg_3587" s="T84">Потом мы остановились, потом увидели это, нору зайчика.</ta>
            <ta e="T104" id="Seg_3588" s="T101">Потом мы катались и катались.</ta>
            <ta e="T123" id="Seg_3589" s="T105">Большая горка была, там же мы не замечали, там вообще белое всё было, ничего не было видно.</ta>
            <ta e="T147" id="Seg_3590" s="T124">Потом мы дальше катались, там ещё Полина Алексеевна была со своей семьёй, потом тётя Настя, Никушка – ребёнок Людмилы Николаевны, потом Аннушка – тёти Насти.</ta>
            <ta e="T151" id="Seg_3591" s="T148">Потом мы катались.</ta>
            <ta e="T168" id="Seg_3592" s="T152">Потом что было, дядя Проня детям что делал, ну, фигурки.</ta>
            <ta e="T190" id="Seg_3593" s="T169">Потом у Киры фигурка зайчика была, а чья, Надина, просто куропатка была.</ta>
            <ta e="T211" id="Seg_3594" s="T190">Потом положили обе [фигурки], а куропатку положили, куда положили, что это было, к зайчику, ну, в эту, потом, ну, в нору.</ta>
            <ta e="T232" id="Seg_3595" s="T212">Потом мы, потом позже, одна или несколько недель прошло, второй раз туда пошли.</ta>
            <ta e="T238" id="Seg_3596" s="T233">Потом мы пошли на тоже место.</ta>
            <ta e="T256" id="Seg_3597" s="T239">На том же месте там это, тот же зайчик был, тот зайчик описаный был, весь в моче.</ta>
            <ta e="T266" id="Seg_3598" s="T257">А куропатке голову кто-то оторвал.</ta>
            <ta e="T286" id="Seg_3599" s="T267">Потом мы пошли кататься, а кто, ну, дядя Проку детей увёз, … ээ, нет, … это другое.</ta>
            <ta e="T298" id="Seg_3600" s="T287">Мы кататься пошли, потом обратно пришли в этот, в посёлок, обратно с лыжами пришли.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_3601" s="T1">однажды мы с Региной гуляли</ta>
            <ta e="T18" id="Seg_3602" s="T8">потом там тётя ааа…мы гуляли ребёнка нянчили</ta>
            <ta e="T36" id="Seg_3603" s="T19">потом Кирина мама меня позвала: "Кататься поедите с нами в тундру?"</ta>
            <ta e="T44" id="Seg_3604" s="T37">потом я сказала да, потом мы пошли</ta>
            <ta e="T52" id="Seg_3605" s="T45">а Полина Алексеевна со своей семьёй остались</ta>
            <ta e="T63" id="Seg_3606" s="T53">потом они шли в сторону тёти Насти и тётя настя фотографировала их</ta>
            <ta e="T71" id="Seg_3607" s="T64">сейчас как говорят буранчик у них был буранчик или что</ta>
            <ta e="T83" id="Seg_3608" s="T299">потом они, Полина Алексеевна с семьёй пришли, потом мы же дальше пошли</ta>
            <ta e="T100" id="Seg_3609" s="T84">потом мы остановились, потом видели зайчика дырку</ta>
            <ta e="T104" id="Seg_3610" s="T101">потом катались катались</ta>
            <ta e="T123" id="Seg_3611" s="T105">большая горка была, там же мы не замечали, там вообще белое было ничего не было</ta>
            <ta e="T147" id="Seg_3612" s="T124">потом мы дальше катались, там ещё Полина Алексеевна была со своей семьёй, потом тётя Нааста, Никушка- Людмилы Николаевны ребёнок, потом Аннушка тёти Насти</ta>
            <ta e="T151" id="Seg_3613" s="T148">потом мы катались</ta>
            <ta e="T168" id="Seg_3614" s="T152">потом что было, дядя Проня детям что делал, фигурки делал</ta>
            <ta e="T190" id="Seg_3615" s="T169">потом у Киры фигурка зайчик была, а что её, Надина, просто куропатка была</ta>
            <ta e="T211" id="Seg_3616" s="T190">потом положили, а куропатку положили, куда положили, что это было, зайчик, в этом, потом что, в яму</ta>
            <ta e="T232" id="Seg_3617" s="T212">потом мы потом несколько недель прошло по второму разу пошли</ta>
            <ta e="T238" id="Seg_3618" s="T233">потом мы пошли на тоже место</ta>
            <ta e="T256" id="Seg_3619" s="T239">на том же месте там это, тот же зайчик был, то зайчик описаный был, описаный</ta>
            <ta e="T266" id="Seg_3620" s="T257">а голову куропатке кто-то оторвал</ta>
            <ta e="T286" id="Seg_3621" s="T267">потом пошли кататься, дядя Проку детей увёз…ээээ нет… другое это</ta>
            <ta e="T298" id="Seg_3622" s="T287">мы кататься пошли, потом обратно пришли в посёлок, обратно с лыжами пришли</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T266" id="Seg_3623" s="T257">[DCh]: Person-number reference does not fit here.</ta>
            <ta e="T286" id="Seg_3624" s="T267">[DCh]: Person-number reference does not fit here.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T301" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T303" />
            <conversion-tli id="T54" />
            <conversion-tli id="T302" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T299" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T300" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
