<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID3AC2CABE-C3BC-9EE3-17FD-949A0A9491E2">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>SuAA_20XX_NameGiving_nar</transcription-name>
         <referenced-file url="SuAA_20XX_NameGiving_nar.wav" />
         <referenced-file url="SuAA_20XX_NameGiving_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\SuAA_20XX_NameGiving_nar\SuAA_20XX_NameGiving_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">298</ud-information>
            <ud-information attribute-name="# HIAT:w">232</ud-information>
            <ud-information attribute-name="# e">232</ud-information>
            <ud-information attribute-name="# HIAT:u">32</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="SuAA">
            <abbreviation>SuAA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.0" type="appl" />
         <tli id="T2" time="0.406125" type="appl" />
         <tli id="T3" time="0.81225" type="appl" />
         <tli id="T4" time="1.218375" type="appl" />
         <tli id="T5" time="1.6245" type="appl" />
         <tli id="T6" time="2.030625" type="appl" />
         <tli id="T7" time="2.43675" type="appl" />
         <tli id="T8" time="2.8428750000000003" type="appl" />
         <tli id="T9" time="3.249" type="appl" />
         <tli id="T10" time="3.6441250000000003" type="appl" />
         <tli id="T11" time="4.03925" type="appl" />
         <tli id="T12" time="4.434375" type="appl" />
         <tli id="T13" time="4.8295" type="appl" />
         <tli id="T14" time="5.224625" type="appl" />
         <tli id="T15" time="5.61975" type="appl" />
         <tli id="T16" time="6.014875" type="appl" />
         <tli id="T17" time="6.41" type="appl" />
         <tli id="T18" time="6.963" type="appl" />
         <tli id="T19" time="7.516" type="appl" />
         <tli id="T20" time="8.068999999999999" type="appl" />
         <tli id="T21" time="8.622" type="appl" />
         <tli id="T22" time="9.175" type="appl" />
         <tli id="T23" time="9.728" type="appl" />
         <tli id="T24" time="10.280999999999999" type="appl" />
         <tli id="T25" time="10.834" type="appl" />
         <tli id="T26" time="11.384571428571428" type="appl" />
         <tli id="T27" time="11.935142857142857" type="appl" />
         <tli id="T28" time="12.485714285714286" type="appl" />
         <tli id="T29" time="13.036285714285714" type="appl" />
         <tli id="T30" time="13.586857142857143" type="appl" />
         <tli id="T31" time="14.137428571428572" type="appl" />
         <tli id="T32" time="14.754665724300793" />
         <tli id="T33" time="15.360444444444445" type="appl" />
         <tli id="T34" time="16.032888888888888" type="appl" />
         <tli id="T35" time="16.705333333333332" type="appl" />
         <tli id="T36" time="17.377777777777776" type="appl" />
         <tli id="T37" time="18.05022222222222" type="appl" />
         <tli id="T38" time="18.722666666666665" type="appl" />
         <tli id="T39" time="19.39511111111111" type="appl" />
         <tli id="T40" time="20.067555555555554" type="appl" />
         <tli id="T41" time="21.053330836158413" />
         <tli id="T42" time="21.2961" type="appl" />
         <tli id="T43" time="21.8522" type="appl" />
         <tli id="T44" time="22.408299999999997" type="appl" />
         <tli id="T45" time="22.964399999999998" type="appl" />
         <tli id="T46" time="23.5205" type="appl" />
         <tli id="T47" time="24.0766" type="appl" />
         <tli id="T48" time="24.6327" type="appl" />
         <tli id="T49" time="25.1888" type="appl" />
         <tli id="T50" time="25.744899999999998" type="appl" />
         <tli id="T51" time="26.301" type="appl" />
         <tli id="T52" time="26.8571" type="appl" />
         <tli id="T53" time="27.4132" type="appl" />
         <tli id="T54" time="27.969299999999997" type="appl" />
         <tli id="T55" time="28.525399999999998" type="appl" />
         <tli id="T56" time="29.0815" type="appl" />
         <tli id="T57" time="29.6376" type="appl" />
         <tli id="T58" time="30.1937" type="appl" />
         <tli id="T59" time="30.7498" type="appl" />
         <tli id="T60" time="31.3059" type="appl" />
         <tli id="T61" time="31.855333708819025" />
         <tli id="T62" time="32.4848" type="appl" />
         <tli id="T63" time="33.1076" type="appl" />
         <tli id="T64" time="33.7304" type="appl" />
         <tli id="T65" time="34.3532" type="appl" />
         <tli id="T66" time="34.976" type="appl" />
         <tli id="T67" time="35.598800000000004" type="appl" />
         <tli id="T68" time="36.2216" type="appl" />
         <tli id="T69" time="36.8444" type="appl" />
         <tli id="T70" time="37.467200000000005" type="appl" />
         <tli id="T71" time="38.04333259408188" />
         <tli id="T72" time="38.51325" type="appl" />
         <tli id="T73" time="38.9365" type="appl" />
         <tli id="T74" time="39.359750000000005" type="appl" />
         <tli id="T75" time="39.783" type="appl" />
         <tli id="T76" time="40.20625" type="appl" />
         <tli id="T77" time="40.6295" type="appl" />
         <tli id="T78" time="41.05275" type="appl" />
         <tli id="T79" time="41.476" type="appl" />
         <tli id="T80" time="41.899249999999995" type="appl" />
         <tli id="T81" time="42.3225" type="appl" />
         <tli id="T82" time="42.74575" type="appl" />
         <tli id="T83" time="43.169" type="appl" />
         <tli id="T84" time="43.672" type="appl" />
         <tli id="T85" time="44.175" type="appl" />
         <tli id="T86" time="44.678" type="appl" />
         <tli id="T87" time="45.181" type="appl" />
         <tli id="T88" time="45.684" type="appl" />
         <tli id="T89" time="46.187" type="appl" />
         <tli id="T90" time="46.87666347061259" />
         <tli id="T91" time="47.32666666666667" type="appl" />
         <tli id="T92" time="47.96333333333333" type="appl" />
         <tli id="T93" time="48.62666574771643" />
         <tli id="T94" time="49.096000000000004" type="appl" />
         <tli id="T95" time="49.592" type="appl" />
         <tli id="T96" time="50.088" type="appl" />
         <tli id="T97" time="50.583999999999996" type="appl" />
         <tli id="T98" time="51.24666595749295" />
         <tli id="T99" time="51.78333333333333" type="appl" />
         <tli id="T100" time="52.486666666666665" type="appl" />
         <tli id="T101" time="53.2033328636537" />
         <tli id="T102" time="53.71628571428571" type="appl" />
         <tli id="T103" time="54.24257142857143" type="appl" />
         <tli id="T104" time="54.768857142857144" type="appl" />
         <tli id="T105" time="55.29514285714286" type="appl" />
         <tli id="T106" time="55.82142857142857" type="appl" />
         <tli id="T107" time="56.34771428571429" type="appl" />
         <tli id="T108" time="56.90733605757859" />
         <tli id="T109" time="57.533307692307694" type="appl" />
         <tli id="T110" time="58.19261538461539" type="appl" />
         <tli id="T111" time="58.85192307692308" type="appl" />
         <tli id="T112" time="59.51123076923077" type="appl" />
         <tli id="T113" time="60.17053846153846" type="appl" />
         <tli id="T114" time="60.82984615384615" type="appl" />
         <tli id="T115" time="61.48915384615384" type="appl" />
         <tli id="T116" time="62.14846153846153" type="appl" />
         <tli id="T117" time="62.807769230769225" type="appl" />
         <tli id="T118" time="63.46707692307692" type="appl" />
         <tli id="T119" time="64.12638461538461" type="appl" />
         <tli id="T120" time="64.7856923076923" type="appl" />
         <tli id="T121" time="65.445" type="appl" />
         <tli id="T122" time="66.05" type="appl" />
         <tli id="T123" time="66.655" type="appl" />
         <tli id="T124" time="67.26" type="appl" />
         <tli id="T125" time="67.76777777777778" type="appl" />
         <tli id="T126" time="68.27555555555556" type="appl" />
         <tli id="T127" time="68.78333333333333" type="appl" />
         <tli id="T128" time="69.2911111111111" type="appl" />
         <tli id="T129" time="69.7988888888889" type="appl" />
         <tli id="T130" time="70.30666666666667" type="appl" />
         <tli id="T131" time="70.81444444444445" type="appl" />
         <tli id="T132" time="71.32222222222222" type="appl" />
         <tli id="T133" time="71.83" type="appl" />
         <tli id="T134" time="72.651" type="appl" />
         <tli id="T135" time="73.47200000000001" type="appl" />
         <tli id="T136" time="74.293" type="appl" />
         <tli id="T137" time="74.7077" type="appl" />
         <tli id="T138" time="75.1224" type="appl" />
         <tli id="T139" time="75.53710000000001" type="appl" />
         <tli id="T140" time="75.9518" type="appl" />
         <tli id="T141" time="76.3665" type="appl" />
         <tli id="T142" time="76.7812" type="appl" />
         <tli id="T143" time="77.1959" type="appl" />
         <tli id="T144" time="77.6106" type="appl" />
         <tli id="T145" time="78.0253" type="appl" />
         <tli id="T146" time="78.48000096607143" />
         <tli id="T147" time="79.1606" type="appl" />
         <tli id="T148" time="79.8812" type="appl" />
         <tli id="T149" time="80.6018" type="appl" />
         <tli id="T150" time="81.3224" type="appl" />
         <tli id="T151" time="82.04966629179211" />
         <tli id="T152" time="82.49583333333334" type="appl" />
         <tli id="T153" time="82.94866666666667" type="appl" />
         <tli id="T154" time="83.4015" type="appl" />
         <tli id="T155" time="83.85433333333334" type="appl" />
         <tli id="T156" time="84.30716666666667" type="appl" />
         <tli id="T157" time="84.79333064688541" />
         <tli id="T158" time="85.26918181818182" type="appl" />
         <tli id="T159" time="85.77836363636364" type="appl" />
         <tli id="T160" time="86.28754545454547" type="appl" />
         <tli id="T161" time="86.79672727272728" type="appl" />
         <tli id="T162" time="87.3059090909091" type="appl" />
         <tli id="T163" time="87.81509090909091" type="appl" />
         <tli id="T164" time="88.32427272727273" type="appl" />
         <tli id="T165" time="88.83345454545454" type="appl" />
         <tli id="T166" time="89.34263636363637" type="appl" />
         <tli id="T167" time="89.85181818181819" type="appl" />
         <tli id="T168" time="90.361" type="appl" />
         <tli id="T169" time="91.65" type="appl" />
         <tli id="T170" time="92.10625" type="appl" />
         <tli id="T171" time="92.5625" type="appl" />
         <tli id="T172" time="93.01875" type="appl" />
         <tli id="T173" time="93.475" type="appl" />
         <tli id="T174" time="93.93125" type="appl" />
         <tli id="T175" time="94.3875" type="appl" />
         <tli id="T176" time="94.84375" type="appl" />
         <tli id="T177" time="95.36666389911397" />
         <tli id="T178" time="95.82849999999999" type="appl" />
         <tli id="T179" time="96.357" type="appl" />
         <tli id="T180" time="96.88550000000001" type="appl" />
         <tli id="T181" time="97.4273336861084" />
         <tli id="T182" time="98.07266666666666" type="appl" />
         <tli id="T183" time="98.73133333333334" type="appl" />
         <tli id="T184" time="99.39" type="appl" />
         <tli id="T185" time="100.0292" type="appl" />
         <tli id="T186" time="100.6684" type="appl" />
         <tli id="T187" time="101.3076" type="appl" />
         <tli id="T188" time="101.9468" type="appl" />
         <tli id="T189" time="102.586" type="appl" />
         <tli id="T190" time="103.06875" type="appl" />
         <tli id="T191" time="103.5515" type="appl" />
         <tli id="T192" time="104.03425" type="appl" />
         <tli id="T193" time="104.517" type="appl" />
         <tli id="T194" time="104.99974999999999" type="appl" />
         <tli id="T195" time="105.48249999999999" type="appl" />
         <tli id="T196" time="105.96525" type="appl" />
         <tli id="T197" time="106.42800015237731" />
         <tli id="T198" time="106.9212" type="appl" />
         <tli id="T199" time="107.39439999999999" type="appl" />
         <tli id="T200" time="107.8676" type="appl" />
         <tli id="T201" time="108.3408" type="appl" />
         <tli id="T202" time="108.814" type="appl" />
         <tli id="T203" time="109.2872" type="appl" />
         <tli id="T204" time="109.7604" type="appl" />
         <tli id="T205" time="110.23360000000001" type="appl" />
         <tli id="T206" time="110.7068" type="appl" />
         <tli id="T207" time="111.18" type="appl" />
         <tli id="T208" time="111.608" type="appl" />
         <tli id="T209" time="112.036" type="appl" />
         <tli id="T210" time="112.464" type="appl" />
         <tli id="T211" time="112.892" type="appl" />
         <tli id="T212" time="113.32" type="appl" />
         <tli id="T213" time="113.848" type="appl" />
         <tli id="T214" time="114.37599999999999" type="appl" />
         <tli id="T215" time="114.904" type="appl" />
         <tli id="T216" time="115.43199999999999" type="appl" />
         <tli id="T217" time="116.02000193420946" />
         <tli id="T218" time="116.74674999999999" type="appl" />
         <tli id="T219" time="117.5335" type="appl" />
         <tli id="T220" time="118.32025" type="appl" />
         <tli id="T221" time="119.107" type="appl" />
         <tli id="T222" time="119.89375" type="appl" />
         <tli id="T223" time="120.6805" type="appl" />
         <tli id="T224" time="121.46725" type="appl" />
         <tli id="T225" time="122.254" type="appl" />
         <tli id="T226" time="122.85475000000001" type="appl" />
         <tli id="T227" time="123.4555" type="appl" />
         <tli id="T228" time="124.05625" type="appl" />
         <tli id="T229" time="124.65700000000001" type="appl" />
         <tli id="T230" time="125.25775" type="appl" />
         <tli id="T231" time="125.8585" type="appl" />
         <tli id="T232" time="126.45925" type="appl" />
         <tli id="T233" time="127.06" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="SuAA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T233" id="Seg_0" n="sc" s="T1">
               <ts e="T9" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Bu</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">ogonu</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">ühüs</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">künüger</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">ol</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_19" n="HIAT:w" s="T6">bihik</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_22" n="HIAT:w" s="T7">oŋohullubutugar</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_25" n="HIAT:w" s="T8">biliːller</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_29" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">Onton</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">behis</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_37" n="HIAT:w" s="T11">duː</ts>
                  <nts id="Seg_38" n="HIAT:ip">,</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_41" n="HIAT:w" s="T12">hettis</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_44" n="HIAT:w" s="T13">duː</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_47" n="HIAT:w" s="T14">künüger</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_50" n="HIAT:w" s="T15">kiːne</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_53" n="HIAT:w" s="T16">tüher</ts>
                  <nts id="Seg_54" n="HIAT:ip">.</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T25" id="Seg_57" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_59" n="HIAT:w" s="T17">Ol</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_62" n="HIAT:w" s="T18">kiːne</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_65" n="HIAT:w" s="T19">tüspütüger</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_68" n="HIAT:w" s="T20">du͡o</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_71" n="HIAT:w" s="T21">ol</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_74" n="HIAT:w" s="T22">ogonu</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_77" n="HIAT:w" s="T23">du͡o</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_80" n="HIAT:w" s="T24">hürektiːller</ts>
                  <nts id="Seg_81" n="HIAT:ip">.</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_84" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_86" n="HIAT:w" s="T25">Onton</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_89" n="HIAT:w" s="T26">bu</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_92" n="HIAT:w" s="T27">ogo</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_95" n="HIAT:w" s="T28">kiːne</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_98" n="HIAT:w" s="T29">tüstegine</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_101" n="HIAT:w" s="T30">hürektiːller</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_104" n="HIAT:w" s="T31">ogonu</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_108" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_110" n="HIAT:w" s="T32">Ol</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_113" n="HIAT:w" s="T33">hürekteːn</ts>
                  <nts id="Seg_114" n="HIAT:ip">,</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_117" n="HIAT:w" s="T34">mu͡oraga</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_120" n="HIAT:w" s="T35">bu͡ollagɨna</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_123" n="HIAT:w" s="T36">ɨraːk</ts>
                  <nts id="Seg_124" n="HIAT:ip">,</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_127" n="HIAT:w" s="T37">bu</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_130" n="HIAT:w" s="T38">agabɨt</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_133" n="HIAT:w" s="T39">hu͡ok</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_136" n="HIAT:w" s="T40">hiriger</ts>
                  <nts id="Seg_137" n="HIAT:ip">.</nts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_140" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_142" n="HIAT:w" s="T41">Kim</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_145" n="HIAT:w" s="T42">hürektiː</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_148" n="HIAT:w" s="T43">hatɨːr</ts>
                  <nts id="Seg_149" n="HIAT:ip">,</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_152" n="HIAT:w" s="T44">malʼitvanɨ</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_155" n="HIAT:w" s="T45">üŋüner</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_158" n="HIAT:w" s="T46">haŋanɨ</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_161" n="HIAT:w" s="T47">biler</ts>
                  <nts id="Seg_162" n="HIAT:ip">,</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_165" n="HIAT:w" s="T48">hürektiːr</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_168" n="HIAT:w" s="T49">üŋüner</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_171" n="HIAT:w" s="T50">haŋanɨ</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_174" n="HIAT:w" s="T51">biler</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_177" n="HIAT:w" s="T52">kihi</ts>
                  <nts id="Seg_178" n="HIAT:ip">,</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_181" n="HIAT:w" s="T53">ebete</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_184" n="HIAT:w" s="T54">duː</ts>
                  <nts id="Seg_185" n="HIAT:ip">,</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_188" n="HIAT:w" s="T55">ehete</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_191" n="HIAT:w" s="T56">duː</ts>
                  <nts id="Seg_192" n="HIAT:ip">,</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_195" n="HIAT:w" s="T57">ol</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_198" n="HIAT:w" s="T58">hürektiːr</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_201" n="HIAT:w" s="T59">tɨ͡aga</ts>
                  <nts id="Seg_202" n="HIAT:ip">,</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_205" n="HIAT:w" s="T60">mu͡oraga</ts>
                  <nts id="Seg_206" n="HIAT:ip">.</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_209" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_211" n="HIAT:w" s="T61">Oččogo</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_214" n="HIAT:w" s="T62">bu</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_217" n="HIAT:w" s="T63">hürekteːn</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_220" n="HIAT:w" s="T64">ajɨː</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_223" n="HIAT:w" s="T65">uːta</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_226" n="HIAT:w" s="T66">taŋaranɨ</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_229" n="HIAT:w" s="T67">uːrannar</ts>
                  <nts id="Seg_230" n="HIAT:ip">,</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_233" n="HIAT:w" s="T68">uːnɨ</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_236" n="HIAT:w" s="T69">kutallar</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_239" n="HIAT:w" s="T70">ihikke</ts>
                  <nts id="Seg_240" n="HIAT:ip">.</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_243" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_245" n="HIAT:w" s="T71">Onton</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_248" n="HIAT:w" s="T72">du͡o</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_251" n="HIAT:w" s="T73">taŋaranɨ</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_253" n="HIAT:ip">(</nts>
                  <ts e="T75" id="Seg_255" n="HIAT:w" s="T74">uːr-</ts>
                  <nts id="Seg_256" n="HIAT:ip">)</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_259" n="HIAT:w" s="T75">ugallar</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_262" n="HIAT:w" s="T76">onno</ts>
                  <nts id="Seg_263" n="HIAT:ip">,</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_266" n="HIAT:w" s="T77">oččogo</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_269" n="HIAT:w" s="T78">taŋara</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_272" n="HIAT:w" s="T79">uːta</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_275" n="HIAT:w" s="T80">bu͡olar</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_278" n="HIAT:w" s="T81">ol</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_281" n="HIAT:w" s="T82">uːŋ</ts>
                  <nts id="Seg_282" n="HIAT:ip">.</nts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_285" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_287" n="HIAT:w" s="T83">Ol</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_290" n="HIAT:w" s="T84">hibi͡etu͡oj</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_293" n="HIAT:w" s="T85">taŋara</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_296" n="HIAT:w" s="T86">uːtugar</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_299" n="HIAT:w" s="T87">huːjallar</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_302" n="HIAT:w" s="T88">bu</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_305" n="HIAT:w" s="T89">ogonu</ts>
                  <nts id="Seg_306" n="HIAT:ip">.</nts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_309" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_311" n="HIAT:w" s="T90">Töbököːnün</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_314" n="HIAT:w" s="T91">üste</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_317" n="HIAT:w" s="T92">kat</ts>
                  <nts id="Seg_318" n="HIAT:ip">.</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_321" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_323" n="HIAT:w" s="T93">Aga</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_325" n="HIAT:ip">"</nts>
                  <nts id="Seg_326" n="HIAT:ip">(</nts>
                  <ts e="T95" id="Seg_328" n="HIAT:w" s="T94">e-</ts>
                  <nts id="Seg_329" n="HIAT:ip">)</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_332" n="HIAT:w" s="T95">eteŋŋe</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_335" n="HIAT:w" s="T96">olor</ts>
                  <nts id="Seg_336" n="HIAT:ip">"</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_339" n="HIAT:w" s="T97">di͡enner</ts>
                  <nts id="Seg_340" n="HIAT:ip">.</nts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_343" n="HIAT:u" s="T98">
                  <nts id="Seg_344" n="HIAT:ip">"</nts>
                  <ts e="T99" id="Seg_346" n="HIAT:w" s="T98">Eteŋŋe</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_349" n="HIAT:w" s="T99">ü͡öskeː</ts>
                  <nts id="Seg_350" n="HIAT:ip">"</nts>
                  <nts id="Seg_351" n="HIAT:ip">,</nts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_354" n="HIAT:w" s="T100">di͡enner</ts>
                  <nts id="Seg_355" n="HIAT:ip">.</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_358" n="HIAT:u" s="T101">
                  <nts id="Seg_359" n="HIAT:ip">"</nts>
                  <ts e="T102" id="Seg_361" n="HIAT:w" s="T101">Onnuk</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_364" n="HIAT:w" s="T102">bulčut</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_367" n="HIAT:w" s="T103">bu͡ol</ts>
                  <nts id="Seg_368" n="HIAT:ip">,</nts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_371" n="HIAT:w" s="T104">mannɨk</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_374" n="HIAT:w" s="T105">aččit</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_377" n="HIAT:w" s="T106">bu͡ol</ts>
                  <nts id="Seg_378" n="HIAT:ip">"</nts>
                  <nts id="Seg_379" n="HIAT:ip">,</nts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_382" n="HIAT:w" s="T107">di͡enner</ts>
                  <nts id="Seg_383" n="HIAT:ip">.</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T121" id="Seg_386" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_388" n="HIAT:w" s="T108">Onton</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_391" n="HIAT:w" s="T109">bu͡ollagɨna</ts>
                  <nts id="Seg_392" n="HIAT:ip">,</nts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_395" n="HIAT:w" s="T110">kannɨk</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_398" n="HIAT:w" s="T111">ɨjga</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_401" n="HIAT:w" s="T112">ol</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_404" n="HIAT:w" s="T113">ogo</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_407" n="HIAT:w" s="T114">töröːbüte</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_410" n="HIAT:w" s="T115">bihi͡ene</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_413" n="HIAT:w" s="T116">bu</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_416" n="HIAT:w" s="T117">oːnnʼuːr</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_419" n="HIAT:w" s="T118">ogobut</ts>
                  <nts id="Seg_420" n="HIAT:ip">,</nts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_423" n="HIAT:w" s="T119">bu</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_426" n="HIAT:w" s="T120">onnʼuːrbutugar</ts>
                  <nts id="Seg_427" n="HIAT:ip">.</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_430" n="HIAT:u" s="T121">
                  <ts e="T122" id="Seg_432" n="HIAT:w" s="T121">Fʼevralʼ</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_435" n="HIAT:w" s="T122">ɨjga</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_438" n="HIAT:w" s="T123">töröːtö</ts>
                  <nts id="Seg_439" n="HIAT:ip">.</nts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_442" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_444" n="HIAT:w" s="T124">Bu</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_447" n="HIAT:w" s="T125">fʼevralʼ</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_450" n="HIAT:w" s="T126">ɨjga</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_453" n="HIAT:w" s="T127">bu͡ollagɨna</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_456" n="HIAT:w" s="T128">taŋaralar</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_459" n="HIAT:w" s="T129">baːllar</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_462" n="HIAT:w" s="T130">hibi͡etu͡oj</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_465" n="HIAT:w" s="T131">taŋaralar</ts>
                  <nts id="Seg_466" n="HIAT:ip">,</nts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_469" n="HIAT:w" s="T132">hibi͡etu͡ojdar</ts>
                  <nts id="Seg_470" n="HIAT:ip">.</nts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_473" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_475" n="HIAT:w" s="T133">Aksʼinʼja</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_478" n="HIAT:w" s="T134">di͡en</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_481" n="HIAT:w" s="T135">taŋaralar</ts>
                  <nts id="Seg_482" n="HIAT:ip">.</nts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_485" n="HIAT:u" s="T136">
                  <ts e="T137" id="Seg_487" n="HIAT:w" s="T136">Oččogo</ts>
                  <nts id="Seg_488" n="HIAT:ip">,</nts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_491" n="HIAT:w" s="T137">kɨːh</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_494" n="HIAT:w" s="T138">ogo</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_497" n="HIAT:w" s="T139">ebite</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_500" n="HIAT:w" s="T140">bu͡olla</ts>
                  <nts id="Seg_501" n="HIAT:ip">,</nts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_504" n="HIAT:w" s="T141">Aksi͡enne</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_507" n="HIAT:w" s="T142">di͡en</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_510" n="HIAT:w" s="T143">aːt</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_513" n="HIAT:w" s="T144">ebit</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_516" n="HIAT:w" s="T145">ebit</ts>
                  <nts id="Seg_517" n="HIAT:ip">.</nts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_520" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_522" n="HIAT:w" s="T146">Össü͡ö</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_525" n="HIAT:w" s="T147">baːllar</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_528" n="HIAT:w" s="T148">Strʼesʼinskʼij</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_531" n="HIAT:w" s="T149">di͡en</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_534" n="HIAT:w" s="T150">taŋaralar</ts>
                  <nts id="Seg_535" n="HIAT:ip">.</nts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_538" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_540" n="HIAT:w" s="T151">Oččogo</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_543" n="HIAT:w" s="T152">bu͡ollagɨna</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_546" n="HIAT:w" s="T153">du͡o</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_549" n="HIAT:w" s="T154">bu</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_552" n="HIAT:w" s="T155">fevralʼ</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_555" n="HIAT:w" s="T156">ɨjga</ts>
                  <nts id="Seg_556" n="HIAT:ip">.</nts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_559" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_561" n="HIAT:w" s="T157">Oččogo</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_564" n="HIAT:w" s="T158">bu͡o</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_567" n="HIAT:w" s="T159">ol</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_570" n="HIAT:w" s="T160">ihin</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_573" n="HIAT:w" s="T161">du͡o</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_576" n="HIAT:w" s="T162">bu</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_579" n="HIAT:w" s="T163">ogolorun</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_582" n="HIAT:w" s="T164">Hemen</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_585" n="HIAT:w" s="T165">di͡en</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_588" n="HIAT:w" s="T166">aːttɨːllar</ts>
                  <nts id="Seg_589" n="HIAT:ip">,</nts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_592" n="HIAT:w" s="T167">Hemeːče</ts>
                  <nts id="Seg_593" n="HIAT:ip">.</nts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_596" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_598" n="HIAT:w" s="T168">Hakalɨː</ts>
                  <nts id="Seg_599" n="HIAT:ip">.</nts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_602" n="HIAT:u" s="T169">
                  <ts e="T170" id="Seg_604" n="HIAT:w" s="T169">Hemeːče</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_607" n="HIAT:w" s="T170">di͡en</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_610" n="HIAT:w" s="T171">aːttɨːllar</ts>
                  <nts id="Seg_611" n="HIAT:ip">,</nts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_614" n="HIAT:w" s="T172">u͡ol</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_617" n="HIAT:w" s="T173">ogo</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_620" n="HIAT:w" s="T174">bu͡olbut</ts>
                  <nts id="Seg_621" n="HIAT:ip">,</nts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_624" n="HIAT:w" s="T175">töröːbüte</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_627" n="HIAT:w" s="T176">di͡en</ts>
                  <nts id="Seg_628" n="HIAT:ip">.</nts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_631" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_633" n="HIAT:w" s="T177">Ol</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_636" n="HIAT:w" s="T178">aːt</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_639" n="HIAT:w" s="T179">hu͡ol</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_642" n="HIAT:w" s="T180">bi͡ereller</ts>
                  <nts id="Seg_643" n="HIAT:ip">.</nts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_646" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_648" n="HIAT:w" s="T181">Hürekteːbit</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_651" n="HIAT:w" s="T182">aːta</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_654" n="HIAT:w" s="T183">Hemeːče</ts>
                  <nts id="Seg_655" n="HIAT:ip">.</nts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T189" id="Seg_658" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_660" n="HIAT:w" s="T184">Innʼe</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_663" n="HIAT:w" s="T185">gɨnan</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_666" n="HIAT:w" s="T186">Hemeniŋ</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_669" n="HIAT:w" s="T187">inʼetiger</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_672" n="HIAT:w" s="T188">bi͡ereller</ts>
                  <nts id="Seg_673" n="HIAT:ip">.</nts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T197" id="Seg_676" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_678" n="HIAT:w" s="T189">Bu</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_681" n="HIAT:w" s="T190">Hemen</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_684" n="HIAT:w" s="T191">inʼete</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_687" n="HIAT:w" s="T192">bu͡olla</ts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_690" n="HIAT:w" s="T193">hubuttan</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_693" n="HIAT:w" s="T194">bejetin</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_696" n="HIAT:w" s="T195">aːtɨn</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_699" n="HIAT:w" s="T196">aːttaːbattar</ts>
                  <nts id="Seg_700" n="HIAT:ip">.</nts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_703" n="HIAT:u" s="T197">
                  <ts e="T198" id="Seg_705" n="HIAT:w" s="T197">Bɨlɨrgɨ</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_708" n="HIAT:w" s="T198">üjege</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_711" n="HIAT:w" s="T199">kɨlɨːga</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_714" n="HIAT:w" s="T200">aːttaːččɨlar</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_717" n="HIAT:w" s="T201">ulakan</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_720" n="HIAT:w" s="T202">u͡olunan</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_723" n="HIAT:w" s="T203">duː</ts>
                  <nts id="Seg_724" n="HIAT:ip">,</nts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_727" n="HIAT:w" s="T204">ulakan</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_730" n="HIAT:w" s="T205">kɨːhɨnan</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_733" n="HIAT:w" s="T206">duː</ts>
                  <nts id="Seg_734" n="HIAT:ip">.</nts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T212" id="Seg_737" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_739" n="HIAT:w" s="T207">Ol</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_742" n="HIAT:w" s="T208">ihin</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_745" n="HIAT:w" s="T209">Hemeːče</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_748" n="HIAT:w" s="T210">inʼete</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_751" n="HIAT:w" s="T211">bu͡olar</ts>
                  <nts id="Seg_752" n="HIAT:ip">.</nts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T217" id="Seg_755" n="HIAT:u" s="T212">
                  <ts e="T213" id="Seg_757" n="HIAT:w" s="T212">Agata</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_760" n="HIAT:w" s="T213">bu͡ollagɨna</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_763" n="HIAT:w" s="T214">Hemeːče</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_766" n="HIAT:w" s="T215">agata</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_769" n="HIAT:w" s="T216">bu͡olar</ts>
                  <nts id="Seg_770" n="HIAT:ip">.</nts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_773" n="HIAT:u" s="T217">
                  <ts e="T218" id="Seg_775" n="HIAT:w" s="T217">Innʼe</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_778" n="HIAT:w" s="T218">gɨnan</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_781" n="HIAT:w" s="T219">ol</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_784" n="HIAT:w" s="T220">kördük</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_787" n="HIAT:w" s="T221">aːttaːnnar</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_790" n="HIAT:w" s="T222">kurum</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_793" n="HIAT:w" s="T223">oŋostonnor</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_796" n="HIAT:w" s="T224">kurumnaːn-tatɨmnaːn</ts>
                  <nts id="Seg_797" n="HIAT:ip">.</nts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_800" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_802" n="HIAT:w" s="T225">Bu</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_805" n="HIAT:w" s="T226">dʼonu</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_808" n="HIAT:w" s="T227">barɨtɨn</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_811" n="HIAT:w" s="T228">ɨgɨrannar</ts>
                  <nts id="Seg_812" n="HIAT:ip">,</nts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_815" n="HIAT:w" s="T229">ogo</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_818" n="HIAT:w" s="T230">töröːbüt</ts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_821" n="HIAT:w" s="T231">kuruma</ts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_824" n="HIAT:w" s="T232">bu͡olar</ts>
                  <nts id="Seg_825" n="HIAT:ip">.</nts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T233" id="Seg_827" n="sc" s="T1">
               <ts e="T2" id="Seg_829" n="e" s="T1">Bu </ts>
               <ts e="T3" id="Seg_831" n="e" s="T2">ogonu </ts>
               <ts e="T4" id="Seg_833" n="e" s="T3">ühüs </ts>
               <ts e="T5" id="Seg_835" n="e" s="T4">künüger </ts>
               <ts e="T6" id="Seg_837" n="e" s="T5">ol </ts>
               <ts e="T7" id="Seg_839" n="e" s="T6">bihik </ts>
               <ts e="T8" id="Seg_841" n="e" s="T7">oŋohullubutugar </ts>
               <ts e="T9" id="Seg_843" n="e" s="T8">biliːller. </ts>
               <ts e="T10" id="Seg_845" n="e" s="T9">Onton </ts>
               <ts e="T11" id="Seg_847" n="e" s="T10">behis </ts>
               <ts e="T12" id="Seg_849" n="e" s="T11">duː, </ts>
               <ts e="T13" id="Seg_851" n="e" s="T12">hettis </ts>
               <ts e="T14" id="Seg_853" n="e" s="T13">duː </ts>
               <ts e="T15" id="Seg_855" n="e" s="T14">künüger </ts>
               <ts e="T16" id="Seg_857" n="e" s="T15">kiːne </ts>
               <ts e="T17" id="Seg_859" n="e" s="T16">tüher. </ts>
               <ts e="T18" id="Seg_861" n="e" s="T17">Ol </ts>
               <ts e="T19" id="Seg_863" n="e" s="T18">kiːne </ts>
               <ts e="T20" id="Seg_865" n="e" s="T19">tüspütüger </ts>
               <ts e="T21" id="Seg_867" n="e" s="T20">du͡o </ts>
               <ts e="T22" id="Seg_869" n="e" s="T21">ol </ts>
               <ts e="T23" id="Seg_871" n="e" s="T22">ogonu </ts>
               <ts e="T24" id="Seg_873" n="e" s="T23">du͡o </ts>
               <ts e="T25" id="Seg_875" n="e" s="T24">hürektiːller. </ts>
               <ts e="T26" id="Seg_877" n="e" s="T25">Onton </ts>
               <ts e="T27" id="Seg_879" n="e" s="T26">bu </ts>
               <ts e="T28" id="Seg_881" n="e" s="T27">ogo </ts>
               <ts e="T29" id="Seg_883" n="e" s="T28">kiːne </ts>
               <ts e="T30" id="Seg_885" n="e" s="T29">tüstegine </ts>
               <ts e="T31" id="Seg_887" n="e" s="T30">hürektiːller </ts>
               <ts e="T32" id="Seg_889" n="e" s="T31">ogonu. </ts>
               <ts e="T33" id="Seg_891" n="e" s="T32">Ol </ts>
               <ts e="T34" id="Seg_893" n="e" s="T33">hürekteːn, </ts>
               <ts e="T35" id="Seg_895" n="e" s="T34">mu͡oraga </ts>
               <ts e="T36" id="Seg_897" n="e" s="T35">bu͡ollagɨna </ts>
               <ts e="T37" id="Seg_899" n="e" s="T36">ɨraːk, </ts>
               <ts e="T38" id="Seg_901" n="e" s="T37">bu </ts>
               <ts e="T39" id="Seg_903" n="e" s="T38">agabɨt </ts>
               <ts e="T40" id="Seg_905" n="e" s="T39">hu͡ok </ts>
               <ts e="T41" id="Seg_907" n="e" s="T40">hiriger. </ts>
               <ts e="T42" id="Seg_909" n="e" s="T41">Kim </ts>
               <ts e="T43" id="Seg_911" n="e" s="T42">hürektiː </ts>
               <ts e="T44" id="Seg_913" n="e" s="T43">hatɨːr, </ts>
               <ts e="T45" id="Seg_915" n="e" s="T44">malʼitvanɨ </ts>
               <ts e="T46" id="Seg_917" n="e" s="T45">üŋüner </ts>
               <ts e="T47" id="Seg_919" n="e" s="T46">haŋanɨ </ts>
               <ts e="T48" id="Seg_921" n="e" s="T47">biler, </ts>
               <ts e="T49" id="Seg_923" n="e" s="T48">hürektiːr </ts>
               <ts e="T50" id="Seg_925" n="e" s="T49">üŋüner </ts>
               <ts e="T51" id="Seg_927" n="e" s="T50">haŋanɨ </ts>
               <ts e="T52" id="Seg_929" n="e" s="T51">biler </ts>
               <ts e="T53" id="Seg_931" n="e" s="T52">kihi, </ts>
               <ts e="T54" id="Seg_933" n="e" s="T53">ebete </ts>
               <ts e="T55" id="Seg_935" n="e" s="T54">duː, </ts>
               <ts e="T56" id="Seg_937" n="e" s="T55">ehete </ts>
               <ts e="T57" id="Seg_939" n="e" s="T56">duː, </ts>
               <ts e="T58" id="Seg_941" n="e" s="T57">ol </ts>
               <ts e="T59" id="Seg_943" n="e" s="T58">hürektiːr </ts>
               <ts e="T60" id="Seg_945" n="e" s="T59">tɨ͡aga, </ts>
               <ts e="T61" id="Seg_947" n="e" s="T60">mu͡oraga. </ts>
               <ts e="T62" id="Seg_949" n="e" s="T61">Oččogo </ts>
               <ts e="T63" id="Seg_951" n="e" s="T62">bu </ts>
               <ts e="T64" id="Seg_953" n="e" s="T63">hürekteːn </ts>
               <ts e="T65" id="Seg_955" n="e" s="T64">ajɨː </ts>
               <ts e="T66" id="Seg_957" n="e" s="T65">uːta </ts>
               <ts e="T67" id="Seg_959" n="e" s="T66">taŋaranɨ </ts>
               <ts e="T68" id="Seg_961" n="e" s="T67">uːrannar, </ts>
               <ts e="T69" id="Seg_963" n="e" s="T68">uːnɨ </ts>
               <ts e="T70" id="Seg_965" n="e" s="T69">kutallar </ts>
               <ts e="T71" id="Seg_967" n="e" s="T70">ihikke. </ts>
               <ts e="T72" id="Seg_969" n="e" s="T71">Onton </ts>
               <ts e="T73" id="Seg_971" n="e" s="T72">du͡o </ts>
               <ts e="T74" id="Seg_973" n="e" s="T73">taŋaranɨ </ts>
               <ts e="T75" id="Seg_975" n="e" s="T74">(uːr-) </ts>
               <ts e="T76" id="Seg_977" n="e" s="T75">ugallar </ts>
               <ts e="T77" id="Seg_979" n="e" s="T76">onno, </ts>
               <ts e="T78" id="Seg_981" n="e" s="T77">oččogo </ts>
               <ts e="T79" id="Seg_983" n="e" s="T78">taŋara </ts>
               <ts e="T80" id="Seg_985" n="e" s="T79">uːta </ts>
               <ts e="T81" id="Seg_987" n="e" s="T80">bu͡olar </ts>
               <ts e="T82" id="Seg_989" n="e" s="T81">ol </ts>
               <ts e="T83" id="Seg_991" n="e" s="T82">uːŋ. </ts>
               <ts e="T84" id="Seg_993" n="e" s="T83">Ol </ts>
               <ts e="T85" id="Seg_995" n="e" s="T84">hibi͡etu͡oj </ts>
               <ts e="T86" id="Seg_997" n="e" s="T85">taŋara </ts>
               <ts e="T87" id="Seg_999" n="e" s="T86">uːtugar </ts>
               <ts e="T88" id="Seg_1001" n="e" s="T87">huːjallar </ts>
               <ts e="T89" id="Seg_1003" n="e" s="T88">bu </ts>
               <ts e="T90" id="Seg_1005" n="e" s="T89">ogonu. </ts>
               <ts e="T91" id="Seg_1007" n="e" s="T90">Töbököːnün </ts>
               <ts e="T92" id="Seg_1009" n="e" s="T91">üste </ts>
               <ts e="T93" id="Seg_1011" n="e" s="T92">kat. </ts>
               <ts e="T94" id="Seg_1013" n="e" s="T93">Aga </ts>
               <ts e="T95" id="Seg_1015" n="e" s="T94">"(e-) </ts>
               <ts e="T96" id="Seg_1017" n="e" s="T95">eteŋŋe </ts>
               <ts e="T97" id="Seg_1019" n="e" s="T96">olor" </ts>
               <ts e="T98" id="Seg_1021" n="e" s="T97">di͡enner. </ts>
               <ts e="T99" id="Seg_1023" n="e" s="T98">"Eteŋŋe </ts>
               <ts e="T100" id="Seg_1025" n="e" s="T99">ü͡öskeː", </ts>
               <ts e="T101" id="Seg_1027" n="e" s="T100">di͡enner. </ts>
               <ts e="T102" id="Seg_1029" n="e" s="T101">"Onnuk </ts>
               <ts e="T103" id="Seg_1031" n="e" s="T102">bulčut </ts>
               <ts e="T104" id="Seg_1033" n="e" s="T103">bu͡ol, </ts>
               <ts e="T105" id="Seg_1035" n="e" s="T104">mannɨk </ts>
               <ts e="T106" id="Seg_1037" n="e" s="T105">aččit </ts>
               <ts e="T107" id="Seg_1039" n="e" s="T106">bu͡ol", </ts>
               <ts e="T108" id="Seg_1041" n="e" s="T107">di͡enner. </ts>
               <ts e="T109" id="Seg_1043" n="e" s="T108">Onton </ts>
               <ts e="T110" id="Seg_1045" n="e" s="T109">bu͡ollagɨna, </ts>
               <ts e="T111" id="Seg_1047" n="e" s="T110">kannɨk </ts>
               <ts e="T112" id="Seg_1049" n="e" s="T111">ɨjga </ts>
               <ts e="T113" id="Seg_1051" n="e" s="T112">ol </ts>
               <ts e="T114" id="Seg_1053" n="e" s="T113">ogo </ts>
               <ts e="T115" id="Seg_1055" n="e" s="T114">töröːbüte </ts>
               <ts e="T116" id="Seg_1057" n="e" s="T115">bihi͡ene </ts>
               <ts e="T117" id="Seg_1059" n="e" s="T116">bu </ts>
               <ts e="T118" id="Seg_1061" n="e" s="T117">oːnnʼuːr </ts>
               <ts e="T119" id="Seg_1063" n="e" s="T118">ogobut, </ts>
               <ts e="T120" id="Seg_1065" n="e" s="T119">bu </ts>
               <ts e="T121" id="Seg_1067" n="e" s="T120">onnʼuːrbutugar. </ts>
               <ts e="T122" id="Seg_1069" n="e" s="T121">Fʼevralʼ </ts>
               <ts e="T123" id="Seg_1071" n="e" s="T122">ɨjga </ts>
               <ts e="T124" id="Seg_1073" n="e" s="T123">töröːtö. </ts>
               <ts e="T125" id="Seg_1075" n="e" s="T124">Bu </ts>
               <ts e="T126" id="Seg_1077" n="e" s="T125">fʼevralʼ </ts>
               <ts e="T127" id="Seg_1079" n="e" s="T126">ɨjga </ts>
               <ts e="T128" id="Seg_1081" n="e" s="T127">bu͡ollagɨna </ts>
               <ts e="T129" id="Seg_1083" n="e" s="T128">taŋaralar </ts>
               <ts e="T130" id="Seg_1085" n="e" s="T129">baːllar </ts>
               <ts e="T131" id="Seg_1087" n="e" s="T130">hibi͡etu͡oj </ts>
               <ts e="T132" id="Seg_1089" n="e" s="T131">taŋaralar, </ts>
               <ts e="T133" id="Seg_1091" n="e" s="T132">hibi͡etu͡ojdar. </ts>
               <ts e="T134" id="Seg_1093" n="e" s="T133">Aksʼinʼja </ts>
               <ts e="T135" id="Seg_1095" n="e" s="T134">di͡en </ts>
               <ts e="T136" id="Seg_1097" n="e" s="T135">taŋaralar. </ts>
               <ts e="T137" id="Seg_1099" n="e" s="T136">Oččogo, </ts>
               <ts e="T138" id="Seg_1101" n="e" s="T137">kɨːh </ts>
               <ts e="T139" id="Seg_1103" n="e" s="T138">ogo </ts>
               <ts e="T140" id="Seg_1105" n="e" s="T139">ebite </ts>
               <ts e="T141" id="Seg_1107" n="e" s="T140">bu͡olla, </ts>
               <ts e="T142" id="Seg_1109" n="e" s="T141">Aksi͡enne </ts>
               <ts e="T143" id="Seg_1111" n="e" s="T142">di͡en </ts>
               <ts e="T144" id="Seg_1113" n="e" s="T143">aːt </ts>
               <ts e="T145" id="Seg_1115" n="e" s="T144">ebit </ts>
               <ts e="T146" id="Seg_1117" n="e" s="T145">ebit. </ts>
               <ts e="T147" id="Seg_1119" n="e" s="T146">Össü͡ö </ts>
               <ts e="T148" id="Seg_1121" n="e" s="T147">baːllar </ts>
               <ts e="T149" id="Seg_1123" n="e" s="T148">Strʼesʼinskʼij </ts>
               <ts e="T150" id="Seg_1125" n="e" s="T149">di͡en </ts>
               <ts e="T151" id="Seg_1127" n="e" s="T150">taŋaralar. </ts>
               <ts e="T152" id="Seg_1129" n="e" s="T151">Oččogo </ts>
               <ts e="T153" id="Seg_1131" n="e" s="T152">bu͡ollagɨna </ts>
               <ts e="T154" id="Seg_1133" n="e" s="T153">du͡o </ts>
               <ts e="T155" id="Seg_1135" n="e" s="T154">bu </ts>
               <ts e="T156" id="Seg_1137" n="e" s="T155">fevralʼ </ts>
               <ts e="T157" id="Seg_1139" n="e" s="T156">ɨjga. </ts>
               <ts e="T158" id="Seg_1141" n="e" s="T157">Oččogo </ts>
               <ts e="T159" id="Seg_1143" n="e" s="T158">bu͡o </ts>
               <ts e="T160" id="Seg_1145" n="e" s="T159">ol </ts>
               <ts e="T161" id="Seg_1147" n="e" s="T160">ihin </ts>
               <ts e="T162" id="Seg_1149" n="e" s="T161">du͡o </ts>
               <ts e="T163" id="Seg_1151" n="e" s="T162">bu </ts>
               <ts e="T164" id="Seg_1153" n="e" s="T163">ogolorun </ts>
               <ts e="T165" id="Seg_1155" n="e" s="T164">Hemen </ts>
               <ts e="T166" id="Seg_1157" n="e" s="T165">di͡en </ts>
               <ts e="T167" id="Seg_1159" n="e" s="T166">aːttɨːllar, </ts>
               <ts e="T168" id="Seg_1161" n="e" s="T167">Hemeːče. </ts>
               <ts e="T169" id="Seg_1163" n="e" s="T168">Hakalɨː. </ts>
               <ts e="T170" id="Seg_1165" n="e" s="T169">Hemeːče </ts>
               <ts e="T171" id="Seg_1167" n="e" s="T170">di͡en </ts>
               <ts e="T172" id="Seg_1169" n="e" s="T171">aːttɨːllar, </ts>
               <ts e="T173" id="Seg_1171" n="e" s="T172">u͡ol </ts>
               <ts e="T174" id="Seg_1173" n="e" s="T173">ogo </ts>
               <ts e="T175" id="Seg_1175" n="e" s="T174">bu͡olbut, </ts>
               <ts e="T176" id="Seg_1177" n="e" s="T175">töröːbüte </ts>
               <ts e="T177" id="Seg_1179" n="e" s="T176">di͡en. </ts>
               <ts e="T178" id="Seg_1181" n="e" s="T177">Ol </ts>
               <ts e="T179" id="Seg_1183" n="e" s="T178">aːt </ts>
               <ts e="T180" id="Seg_1185" n="e" s="T179">hu͡ol </ts>
               <ts e="T181" id="Seg_1187" n="e" s="T180">bi͡ereller. </ts>
               <ts e="T182" id="Seg_1189" n="e" s="T181">Hürekteːbit </ts>
               <ts e="T183" id="Seg_1191" n="e" s="T182">aːta </ts>
               <ts e="T184" id="Seg_1193" n="e" s="T183">Hemeːče. </ts>
               <ts e="T185" id="Seg_1195" n="e" s="T184">Innʼe </ts>
               <ts e="T186" id="Seg_1197" n="e" s="T185">gɨnan </ts>
               <ts e="T187" id="Seg_1199" n="e" s="T186">Hemeniŋ </ts>
               <ts e="T188" id="Seg_1201" n="e" s="T187">inʼetiger </ts>
               <ts e="T189" id="Seg_1203" n="e" s="T188">bi͡ereller. </ts>
               <ts e="T190" id="Seg_1205" n="e" s="T189">Bu </ts>
               <ts e="T191" id="Seg_1207" n="e" s="T190">Hemen </ts>
               <ts e="T192" id="Seg_1209" n="e" s="T191">inʼete </ts>
               <ts e="T193" id="Seg_1211" n="e" s="T192">bu͡olla </ts>
               <ts e="T194" id="Seg_1213" n="e" s="T193">hubuttan </ts>
               <ts e="T195" id="Seg_1215" n="e" s="T194">bejetin </ts>
               <ts e="T196" id="Seg_1217" n="e" s="T195">aːtɨn </ts>
               <ts e="T197" id="Seg_1219" n="e" s="T196">aːttaːbattar. </ts>
               <ts e="T198" id="Seg_1221" n="e" s="T197">Bɨlɨrgɨ </ts>
               <ts e="T199" id="Seg_1223" n="e" s="T198">üjege </ts>
               <ts e="T200" id="Seg_1225" n="e" s="T199">kɨlɨːga </ts>
               <ts e="T201" id="Seg_1227" n="e" s="T200">aːttaːččɨlar </ts>
               <ts e="T202" id="Seg_1229" n="e" s="T201">ulakan </ts>
               <ts e="T203" id="Seg_1231" n="e" s="T202">u͡olunan </ts>
               <ts e="T204" id="Seg_1233" n="e" s="T203">duː, </ts>
               <ts e="T205" id="Seg_1235" n="e" s="T204">ulakan </ts>
               <ts e="T206" id="Seg_1237" n="e" s="T205">kɨːhɨnan </ts>
               <ts e="T207" id="Seg_1239" n="e" s="T206">duː. </ts>
               <ts e="T208" id="Seg_1241" n="e" s="T207">Ol </ts>
               <ts e="T209" id="Seg_1243" n="e" s="T208">ihin </ts>
               <ts e="T210" id="Seg_1245" n="e" s="T209">Hemeːče </ts>
               <ts e="T211" id="Seg_1247" n="e" s="T210">inʼete </ts>
               <ts e="T212" id="Seg_1249" n="e" s="T211">bu͡olar. </ts>
               <ts e="T213" id="Seg_1251" n="e" s="T212">Agata </ts>
               <ts e="T214" id="Seg_1253" n="e" s="T213">bu͡ollagɨna </ts>
               <ts e="T215" id="Seg_1255" n="e" s="T214">Hemeːče </ts>
               <ts e="T216" id="Seg_1257" n="e" s="T215">agata </ts>
               <ts e="T217" id="Seg_1259" n="e" s="T216">bu͡olar. </ts>
               <ts e="T218" id="Seg_1261" n="e" s="T217">Innʼe </ts>
               <ts e="T219" id="Seg_1263" n="e" s="T218">gɨnan </ts>
               <ts e="T220" id="Seg_1265" n="e" s="T219">ol </ts>
               <ts e="T221" id="Seg_1267" n="e" s="T220">kördük </ts>
               <ts e="T222" id="Seg_1269" n="e" s="T221">aːttaːnnar </ts>
               <ts e="T223" id="Seg_1271" n="e" s="T222">kurum </ts>
               <ts e="T224" id="Seg_1273" n="e" s="T223">oŋostonnor </ts>
               <ts e="T225" id="Seg_1275" n="e" s="T224">kurumnaːn-tatɨmnaːn. </ts>
               <ts e="T226" id="Seg_1277" n="e" s="T225">Bu </ts>
               <ts e="T227" id="Seg_1279" n="e" s="T226">dʼonu </ts>
               <ts e="T228" id="Seg_1281" n="e" s="T227">barɨtɨn </ts>
               <ts e="T229" id="Seg_1283" n="e" s="T228">ɨgɨrannar, </ts>
               <ts e="T230" id="Seg_1285" n="e" s="T229">ogo </ts>
               <ts e="T231" id="Seg_1287" n="e" s="T230">töröːbüt </ts>
               <ts e="T232" id="Seg_1289" n="e" s="T231">kuruma </ts>
               <ts e="T233" id="Seg_1291" n="e" s="T232">bu͡olar. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T9" id="Seg_1292" s="T1">SuAA_20XX_NameGiving_nar.001 (001.001)</ta>
            <ta e="T17" id="Seg_1293" s="T9">SuAA_20XX_NameGiving_nar.002 (001.002)</ta>
            <ta e="T25" id="Seg_1294" s="T17">SuAA_20XX_NameGiving_nar.003 (001.003)</ta>
            <ta e="T32" id="Seg_1295" s="T25">SuAA_20XX_NameGiving_nar.004 (001.004)</ta>
            <ta e="T41" id="Seg_1296" s="T32">SuAA_20XX_NameGiving_nar.005 (001.005)</ta>
            <ta e="T61" id="Seg_1297" s="T41">SuAA_20XX_NameGiving_nar.006 (001.006)</ta>
            <ta e="T71" id="Seg_1298" s="T61">SuAA_20XX_NameGiving_nar.007 (001.007)</ta>
            <ta e="T83" id="Seg_1299" s="T71">SuAA_20XX_NameGiving_nar.008 (001.008)</ta>
            <ta e="T90" id="Seg_1300" s="T83">SuAA_20XX_NameGiving_nar.009 (001.009)</ta>
            <ta e="T93" id="Seg_1301" s="T90">SuAA_20XX_NameGiving_nar.010 (001.010)</ta>
            <ta e="T98" id="Seg_1302" s="T93">SuAA_20XX_NameGiving_nar.011 (001.011)</ta>
            <ta e="T101" id="Seg_1303" s="T98">SuAA_20XX_NameGiving_nar.012 (001.012)</ta>
            <ta e="T108" id="Seg_1304" s="T101">SuAA_20XX_NameGiving_nar.013 (001.013)</ta>
            <ta e="T121" id="Seg_1305" s="T108">SuAA_20XX_NameGiving_nar.014 (001.014)</ta>
            <ta e="T124" id="Seg_1306" s="T121">SuAA_20XX_NameGiving_nar.015 (001.015)</ta>
            <ta e="T133" id="Seg_1307" s="T124">SuAA_20XX_NameGiving_nar.016 (001.016)</ta>
            <ta e="T136" id="Seg_1308" s="T133">SuAA_20XX_NameGiving_nar.017 (001.017)</ta>
            <ta e="T146" id="Seg_1309" s="T136">SuAA_20XX_NameGiving_nar.018 (001.018)</ta>
            <ta e="T151" id="Seg_1310" s="T146">SuAA_20XX_NameGiving_nar.019 (001.019)</ta>
            <ta e="T157" id="Seg_1311" s="T151">SuAA_20XX_NameGiving_nar.020 (001.020)</ta>
            <ta e="T168" id="Seg_1312" s="T157">SuAA_20XX_NameGiving_nar.021 (001.021)</ta>
            <ta e="T169" id="Seg_1313" s="T168">SuAA_20XX_NameGiving_nar.022 (001.022)</ta>
            <ta e="T177" id="Seg_1314" s="T169">SuAA_20XX_NameGiving_nar.023 (001.023)</ta>
            <ta e="T181" id="Seg_1315" s="T177">SuAA_20XX_NameGiving_nar.024 (001.024)</ta>
            <ta e="T184" id="Seg_1316" s="T181">SuAA_20XX_NameGiving_nar.025 (001.025)</ta>
            <ta e="T189" id="Seg_1317" s="T184">SuAA_20XX_NameGiving_nar.026 (001.026)</ta>
            <ta e="T197" id="Seg_1318" s="T189">SuAA_20XX_NameGiving_nar.027 (001.027)</ta>
            <ta e="T207" id="Seg_1319" s="T197">SuAA_20XX_NameGiving_nar.028 (001.028)</ta>
            <ta e="T212" id="Seg_1320" s="T207">SuAA_20XX_NameGiving_nar.029 (001.029)</ta>
            <ta e="T217" id="Seg_1321" s="T212">SuAA_20XX_NameGiving_nar.030 (001.030)</ta>
            <ta e="T225" id="Seg_1322" s="T217">SuAA_20XX_NameGiving_nar.031 (001.031)</ta>
            <ta e="T233" id="Seg_1323" s="T225">SuAA_20XX_NameGiving_nar.032 (001.032)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T9" id="Seg_1324" s="T1">Бу огону үһүс күнүгэр ол биһик оӈуоллубутугар билииллэр.</ta>
            <ta e="T17" id="Seg_1325" s="T9">Онтон бэһис дуу, һэттис дуу күнүгэр киинэ түһэр. </ta>
            <ta e="T25" id="Seg_1326" s="T17">Ол киинэ түспүтүгэр дуо? ол огону дуо һүрэктииллэр.</ta>
            <ta e="T32" id="Seg_1327" s="T25">Онтон бу ого киинэ түстээннэ һүрэктииллэр огону.</ta>
            <ta e="T41" id="Seg_1328" s="T32">Ол һүрэктээн, муорага буоллагына ырааак, бу агабыт һуок һиригэр,</ta>
            <ta e="T61" id="Seg_1329" s="T41">ким һүрэктии һатыыр, молитваны үӈүнэр һаӈаны билэр, һүрэктиир үӈүнэр һаӈаны билэр киһи: эбэтэ дуу, эһэтэ дуу, ол һүрэктиир тыага, муорага. </ta>
            <ta e="T71" id="Seg_1330" s="T61">Оччого бу һүрэктээн айыы уута таӈараны уураннан, ууны куталлар иһиккэ. </ta>
            <ta e="T83" id="Seg_1331" s="T71">Онтон дуо таӈараны угаллар онно, оччого таӈара уута буолар ол ууӈ.</ta>
            <ta e="T90" id="Seg_1332" s="T83">Ол һибиэтуой таӈара уутугар һуйаллар бу огону. </ta>
            <ta e="T93" id="Seg_1333" s="T90">Төбөкөөнүн үстэ кат. </ta>
            <ta e="T98" id="Seg_1334" s="T93">Ага -э .. этэӈӈэ олор диэннэр.</ta>
            <ta e="T101" id="Seg_1335" s="T98">Этэӈӈэ үөскээ, диэннэр. </ta>
            <ta e="T108" id="Seg_1336" s="T101">Оннук булчут буол, маннык аччит буол диэннэр.</ta>
            <ta e="T121" id="Seg_1337" s="T108">Онтон буоллагына, каннык ыйга ол ого төрөөбүтэ биһиэнэ бу оонньуур огобут. Бу онньуурбутугар.</ta>
            <ta e="T124" id="Seg_1338" s="T121">Февраль ыйга төрөөтө. </ta>
            <ta e="T133" id="Seg_1339" s="T124">Бу февраль ыйга буоллагына таӈаралар бааллар һибиэтуой таӈаралар. Һибиэтуойдар. </ta>
            <ta e="T136" id="Seg_1340" s="T133">Аксинья диэн таӈаралар.</ta>
            <ta e="T146" id="Seg_1341" s="T136">Оччого, кыыс огото эбитэ буолаллар, Аксиэнньэ диэн аат эбит эбит. </ta>
            <ta e="T151" id="Seg_1342" s="T146">Öссүө бааллар Стресинский диэн таӈаралар.</ta>
            <ta e="T157" id="Seg_1343" s="T151">Оччого буоллагына дуо бу февраль ыйга. </ta>
            <ta e="T168" id="Seg_1344" s="T157">Оччого буо ол иһин дуо бу оголорун Һэмэн диэн ааттыыллар - Һэмээчэ.</ta>
            <ta e="T169" id="Seg_1345" s="T168">Һакалыы. </ta>
            <ta e="T177" id="Seg_1346" s="T169">Һэмээчэ диэн ааттыыллар. Уол ого буолбут, төрөөбүтүтэ диэн. </ta>
            <ta e="T181" id="Seg_1347" s="T177">Ол аат һуол биэрэллэр.</ta>
            <ta e="T184" id="Seg_1348" s="T181">Һүрэктээбит аата Һэмээчэ. </ta>
            <ta e="T189" id="Seg_1349" s="T184">Иннэ гынан Һэмэниӈ иньэтигэр биэрэллэр.</ta>
            <ta e="T197" id="Seg_1350" s="T189">Бу Һэмэн иньэтэ буолла һубуттан бэйэтин атын аттаабаттар.</ta>
            <ta e="T207" id="Seg_1351" s="T197">Былыргы үйэгэ кылыыга аттааччылар улакан уолунан дуу, улакан кыыһынан дуу. </ta>
            <ta e="T212" id="Seg_1352" s="T207">Ол иһин Һэмээчэ иньэтэ буолар.</ta>
            <ta e="T217" id="Seg_1353" s="T212">Агата буоллагына Һэмээчэ агата буолар. </ta>
            <ta e="T225" id="Seg_1354" s="T217">Иннэ гынан ол көрдүк аатааннар курум оӈуостоннор курумнаан-татымнааан.</ta>
            <ta e="T233" id="Seg_1355" s="T225">Бу дьону барытын ыгыраннар, ого төрөөбүт курума буолар.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T9" id="Seg_1356" s="T1">Bu ogonu ühüs künüger ol bihik oŋohullubutugar biliːller. </ta>
            <ta e="T17" id="Seg_1357" s="T9">Onton behis duː, hettis duː künüger kiːne tüher. </ta>
            <ta e="T25" id="Seg_1358" s="T17">Ol kiːne tüspütüger du͡o ol ogonu du͡o hürektiːller. </ta>
            <ta e="T32" id="Seg_1359" s="T25">Onton bu ogo kiːne tüstegine hürektiːller ogonu. </ta>
            <ta e="T41" id="Seg_1360" s="T32">Ol hürekteːn, mu͡oraga bu͡ollagɨna ɨraːk, bu agabɨt hu͡ok hiriger. </ta>
            <ta e="T61" id="Seg_1361" s="T41">Kim hürektiː hatɨːr, malʼitvanɨ üŋüner haŋanɨ biler, hürektiːr üŋüner haŋanɨ biler kihi, ebete duː, ehete duː, ol hürektiːr tɨ͡aga, mu͡oraga. </ta>
            <ta e="T71" id="Seg_1362" s="T61">Oččogo bu hürekteːn ajɨː uːta taŋaranɨ uːrannar, uːnɨ kutallar ihikke. </ta>
            <ta e="T83" id="Seg_1363" s="T71">Onton du͡o taŋaranɨ (uːr-) ugallar onno, oččogo taŋara uːta bu͡olar ol uːŋ. </ta>
            <ta e="T90" id="Seg_1364" s="T83">Ol hibi͡etu͡oj taŋara uːtugar huːjallar bu ogonu. </ta>
            <ta e="T93" id="Seg_1365" s="T90">Töbököːnün üste kat. </ta>
            <ta e="T98" id="Seg_1366" s="T93">Aga "(e-) eteŋŋe olor" di͡enner. </ta>
            <ta e="T101" id="Seg_1367" s="T98">"Eteŋŋe ü͡öskeː", di͡enner. </ta>
            <ta e="T108" id="Seg_1368" s="T101">"Onnuk bulčut bu͡ol, mannɨk aččit bu͡ol", di͡enner. </ta>
            <ta e="T121" id="Seg_1369" s="T108">Onton bu͡ollagɨna, kannɨk ɨjga ol ogo töröːbüte bihi͡ene bu oːnnʼuːr ogobut, bu onnʼuːrbutugar. </ta>
            <ta e="T124" id="Seg_1370" s="T121">Fʼevralʼ ɨjga töröːtö. </ta>
            <ta e="T133" id="Seg_1371" s="T124">Bu fʼevralʼ ɨjga bu͡ollagɨna taŋaralar baːllar hibi͡etu͡oj taŋaralar, hibi͡etu͡ojdar. </ta>
            <ta e="T136" id="Seg_1372" s="T133">Aksʼinʼja di͡en taŋaralar. </ta>
            <ta e="T146" id="Seg_1373" s="T136">Oččogo, kɨːh ogo ebite bu͡olla, Aksi͡enne di͡en aːt ebit ebit. </ta>
            <ta e="T151" id="Seg_1374" s="T146">Össü͡ö baːllar Strʼesʼinskʼij di͡en taŋaralar. </ta>
            <ta e="T157" id="Seg_1375" s="T151">Oččogo bu͡ollagɨna du͡o bu fevralʼ ɨjga. </ta>
            <ta e="T168" id="Seg_1376" s="T157">Oččogo bu͡o ol ihin du͡o bu ogolorun Hemen di͡en aːttɨːllar, Hemeːče. </ta>
            <ta e="T169" id="Seg_1377" s="T168">Hakalɨː. </ta>
            <ta e="T177" id="Seg_1378" s="T169">Hemeːče di͡en aːttɨːllar, u͡ol ogo bu͡olbut, töröːbüte di͡en. </ta>
            <ta e="T181" id="Seg_1379" s="T177">Ol aːt hu͡ol bi͡ereller. </ta>
            <ta e="T184" id="Seg_1380" s="T181">Hürekteːbit aːta Hemeːče. </ta>
            <ta e="T189" id="Seg_1381" s="T184">Innʼe gɨnan Hemeniŋ inʼetiger bi͡ereller. </ta>
            <ta e="T197" id="Seg_1382" s="T189">Bu Hemen inʼete bu͡olla hubuttan bejetin aːtɨn aːttaːbattar. </ta>
            <ta e="T207" id="Seg_1383" s="T197">Bɨlɨrgɨ üjege kɨlɨːga aːttaːččɨlar ulakan u͡olunan duː, ulakan kɨːhɨnan duː. </ta>
            <ta e="T212" id="Seg_1384" s="T207">Ol ihin Hemeːče inʼete bu͡olar. </ta>
            <ta e="T217" id="Seg_1385" s="T212">Agata bu͡ollagɨna Hemeːče agata bu͡olar. </ta>
            <ta e="T225" id="Seg_1386" s="T217">Innʼe gɨnan ol kördük aːttaːnnar kurum oŋostonnor kurumnaːn-tatɨmnaːn. </ta>
            <ta e="T233" id="Seg_1387" s="T225">Bu dʼonu barɨtɨn ɨgɨrannar, ogo töröːbüt kuruma bu͡olar. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_1388" s="T1">bu</ta>
            <ta e="T3" id="Seg_1389" s="T2">ogo-nu</ta>
            <ta e="T4" id="Seg_1390" s="T3">üh-üs</ta>
            <ta e="T5" id="Seg_1391" s="T4">kün-ü-ger</ta>
            <ta e="T6" id="Seg_1392" s="T5">ol</ta>
            <ta e="T7" id="Seg_1393" s="T6">bihik</ta>
            <ta e="T8" id="Seg_1394" s="T7">oŋohull-u-but-u-gar</ta>
            <ta e="T9" id="Seg_1395" s="T8">biliː-l-ler</ta>
            <ta e="T10" id="Seg_1396" s="T9">onton</ta>
            <ta e="T11" id="Seg_1397" s="T10">beh-is</ta>
            <ta e="T12" id="Seg_1398" s="T11">duː</ta>
            <ta e="T13" id="Seg_1399" s="T12">hett-is</ta>
            <ta e="T14" id="Seg_1400" s="T13">duː</ta>
            <ta e="T15" id="Seg_1401" s="T14">kün-ü-ger</ta>
            <ta e="T16" id="Seg_1402" s="T15">kiːn-e</ta>
            <ta e="T17" id="Seg_1403" s="T16">tüh-er</ta>
            <ta e="T18" id="Seg_1404" s="T17">ol</ta>
            <ta e="T19" id="Seg_1405" s="T18">kiːn-e</ta>
            <ta e="T20" id="Seg_1406" s="T19">tüs-püt-ü-ger</ta>
            <ta e="T21" id="Seg_1407" s="T20">du͡o</ta>
            <ta e="T22" id="Seg_1408" s="T21">ol</ta>
            <ta e="T23" id="Seg_1409" s="T22">ogo-nu</ta>
            <ta e="T24" id="Seg_1410" s="T23">du͡o</ta>
            <ta e="T25" id="Seg_1411" s="T24">hürektiː-l-ler</ta>
            <ta e="T26" id="Seg_1412" s="T25">onton</ta>
            <ta e="T27" id="Seg_1413" s="T26">bu</ta>
            <ta e="T28" id="Seg_1414" s="T27">ogo</ta>
            <ta e="T29" id="Seg_1415" s="T28">kiːn-e</ta>
            <ta e="T30" id="Seg_1416" s="T29">tüs-teg-ine</ta>
            <ta e="T31" id="Seg_1417" s="T30">hürektiː-l-ler</ta>
            <ta e="T32" id="Seg_1418" s="T31">ogo-nu</ta>
            <ta e="T33" id="Seg_1419" s="T32">ol</ta>
            <ta e="T34" id="Seg_1420" s="T33">hürekteː-n</ta>
            <ta e="T35" id="Seg_1421" s="T34">mu͡ora-ga</ta>
            <ta e="T36" id="Seg_1422" s="T35">bu͡ollagɨna</ta>
            <ta e="T37" id="Seg_1423" s="T36">ɨraːk</ta>
            <ta e="T38" id="Seg_1424" s="T37">bu</ta>
            <ta e="T39" id="Seg_1425" s="T38">agabɨt</ta>
            <ta e="T40" id="Seg_1426" s="T39">hu͡ok</ta>
            <ta e="T41" id="Seg_1427" s="T40">hir-i-ger</ta>
            <ta e="T42" id="Seg_1428" s="T41">kim</ta>
            <ta e="T43" id="Seg_1429" s="T42">hürekt-iː</ta>
            <ta e="T44" id="Seg_1430" s="T43">hatɨː-r</ta>
            <ta e="T45" id="Seg_1431" s="T44">malʼitva-nɨ</ta>
            <ta e="T46" id="Seg_1432" s="T45">üŋün-er</ta>
            <ta e="T47" id="Seg_1433" s="T46">haŋa-nɨ</ta>
            <ta e="T48" id="Seg_1434" s="T47">bil-er</ta>
            <ta e="T49" id="Seg_1435" s="T48">hürektiː-r</ta>
            <ta e="T50" id="Seg_1436" s="T49">üŋün-er</ta>
            <ta e="T51" id="Seg_1437" s="T50">haŋa-nɨ</ta>
            <ta e="T52" id="Seg_1438" s="T51">bil-er</ta>
            <ta e="T53" id="Seg_1439" s="T52">kihi</ta>
            <ta e="T54" id="Seg_1440" s="T53">ebe-te</ta>
            <ta e="T55" id="Seg_1441" s="T54">duː</ta>
            <ta e="T56" id="Seg_1442" s="T55">ehe-te</ta>
            <ta e="T57" id="Seg_1443" s="T56">duː</ta>
            <ta e="T58" id="Seg_1444" s="T57">ol</ta>
            <ta e="T59" id="Seg_1445" s="T58">hürektiː-r</ta>
            <ta e="T60" id="Seg_1446" s="T59">tɨ͡a-ga</ta>
            <ta e="T61" id="Seg_1447" s="T60">mu͡ora-ga</ta>
            <ta e="T62" id="Seg_1448" s="T61">oččogo</ta>
            <ta e="T63" id="Seg_1449" s="T62">bu</ta>
            <ta e="T64" id="Seg_1450" s="T63">hürekteː-n</ta>
            <ta e="T65" id="Seg_1451" s="T64">ajɨː</ta>
            <ta e="T66" id="Seg_1452" s="T65">uː-ta</ta>
            <ta e="T67" id="Seg_1453" s="T66">taŋara-nɨ</ta>
            <ta e="T68" id="Seg_1454" s="T67">uːr-an-nar</ta>
            <ta e="T69" id="Seg_1455" s="T68">uː-nɨ</ta>
            <ta e="T70" id="Seg_1456" s="T69">kut-al-lar</ta>
            <ta e="T71" id="Seg_1457" s="T70">ihik-ke</ta>
            <ta e="T72" id="Seg_1458" s="T71">onton</ta>
            <ta e="T73" id="Seg_1459" s="T72">du͡o</ta>
            <ta e="T74" id="Seg_1460" s="T73">taŋara-nɨ</ta>
            <ta e="T75" id="Seg_1461" s="T74">uːr</ta>
            <ta e="T76" id="Seg_1462" s="T75">ug-al-lar</ta>
            <ta e="T77" id="Seg_1463" s="T76">onno</ta>
            <ta e="T78" id="Seg_1464" s="T77">oččogo</ta>
            <ta e="T79" id="Seg_1465" s="T78">taŋara</ta>
            <ta e="T80" id="Seg_1466" s="T79">uː-ta</ta>
            <ta e="T81" id="Seg_1467" s="T80">bu͡ol-ar</ta>
            <ta e="T82" id="Seg_1468" s="T81">ol</ta>
            <ta e="T83" id="Seg_1469" s="T82">uː-ŋ</ta>
            <ta e="T84" id="Seg_1470" s="T83">ol</ta>
            <ta e="T85" id="Seg_1471" s="T84">hibi͡etu͡oj</ta>
            <ta e="T86" id="Seg_1472" s="T85">taŋara</ta>
            <ta e="T87" id="Seg_1473" s="T86">uː-tu-gar</ta>
            <ta e="T88" id="Seg_1474" s="T87">huːj-al-lar</ta>
            <ta e="T89" id="Seg_1475" s="T88">bu</ta>
            <ta e="T90" id="Seg_1476" s="T89">ogo-nu</ta>
            <ta e="T91" id="Seg_1477" s="T90">töbö-köːn-ü-n</ta>
            <ta e="T92" id="Seg_1478" s="T91">üs-te</ta>
            <ta e="T93" id="Seg_1479" s="T92">kat</ta>
            <ta e="T94" id="Seg_1480" s="T93">aga</ta>
            <ta e="T96" id="Seg_1481" s="T95">eteŋŋe</ta>
            <ta e="T97" id="Seg_1482" s="T96">olor</ta>
            <ta e="T98" id="Seg_1483" s="T97">di͡e-n-ner</ta>
            <ta e="T99" id="Seg_1484" s="T98">eteŋŋe</ta>
            <ta e="T100" id="Seg_1485" s="T99">ü͡öskeː</ta>
            <ta e="T101" id="Seg_1486" s="T100">di͡e-n-ner</ta>
            <ta e="T102" id="Seg_1487" s="T101">onnuk</ta>
            <ta e="T103" id="Seg_1488" s="T102">bulčut</ta>
            <ta e="T104" id="Seg_1489" s="T103">bu͡ol</ta>
            <ta e="T105" id="Seg_1490" s="T104">mannɨk</ta>
            <ta e="T106" id="Seg_1491" s="T105">ač-čit</ta>
            <ta e="T107" id="Seg_1492" s="T106">bu͡ol</ta>
            <ta e="T108" id="Seg_1493" s="T107">di͡e-n-ner</ta>
            <ta e="T109" id="Seg_1494" s="T108">onton</ta>
            <ta e="T110" id="Seg_1495" s="T109">bu͡ollagɨna</ta>
            <ta e="T111" id="Seg_1496" s="T110">kannɨk</ta>
            <ta e="T112" id="Seg_1497" s="T111">ɨj-ga</ta>
            <ta e="T113" id="Seg_1498" s="T112">ol</ta>
            <ta e="T114" id="Seg_1499" s="T113">ogo</ta>
            <ta e="T115" id="Seg_1500" s="T114">töröː-büt-e</ta>
            <ta e="T116" id="Seg_1501" s="T115">bihi͡ene</ta>
            <ta e="T117" id="Seg_1502" s="T116">bu</ta>
            <ta e="T118" id="Seg_1503" s="T117">oːnnʼuː-r</ta>
            <ta e="T119" id="Seg_1504" s="T118">ogo-but</ta>
            <ta e="T120" id="Seg_1505" s="T119">bu</ta>
            <ta e="T121" id="Seg_1506" s="T120">onnʼuː-r-butu-gar</ta>
            <ta e="T122" id="Seg_1507" s="T121">fʼevralʼ</ta>
            <ta e="T123" id="Seg_1508" s="T122">ɨj-ga</ta>
            <ta e="T124" id="Seg_1509" s="T123">töröː-t-ö</ta>
            <ta e="T125" id="Seg_1510" s="T124">bu</ta>
            <ta e="T126" id="Seg_1511" s="T125">fʼevralʼ</ta>
            <ta e="T127" id="Seg_1512" s="T126">ɨj-ga</ta>
            <ta e="T128" id="Seg_1513" s="T127">bu͡ollagɨna</ta>
            <ta e="T129" id="Seg_1514" s="T128">taŋara-lar</ta>
            <ta e="T130" id="Seg_1515" s="T129">baːl-lar</ta>
            <ta e="T131" id="Seg_1516" s="T130">hibi͡etu͡oj</ta>
            <ta e="T132" id="Seg_1517" s="T131">taŋara-lar</ta>
            <ta e="T133" id="Seg_1518" s="T132">hibi͡etu͡oj-dar</ta>
            <ta e="T134" id="Seg_1519" s="T133">Aksʼinʼja</ta>
            <ta e="T135" id="Seg_1520" s="T134">di͡e-n</ta>
            <ta e="T136" id="Seg_1521" s="T135">taŋara-lar</ta>
            <ta e="T137" id="Seg_1522" s="T136">oččogo</ta>
            <ta e="T138" id="Seg_1523" s="T137">kɨːh</ta>
            <ta e="T139" id="Seg_1524" s="T138">ogo</ta>
            <ta e="T140" id="Seg_1525" s="T139">e-bit-e</ta>
            <ta e="T141" id="Seg_1526" s="T140">bu͡olla</ta>
            <ta e="T142" id="Seg_1527" s="T141">Aksi͡enne</ta>
            <ta e="T143" id="Seg_1528" s="T142">di͡e-n</ta>
            <ta e="T144" id="Seg_1529" s="T143">aːt</ta>
            <ta e="T145" id="Seg_1530" s="T144">e-bit</ta>
            <ta e="T146" id="Seg_1531" s="T145">e-bit</ta>
            <ta e="T147" id="Seg_1532" s="T146">össü͡ö</ta>
            <ta e="T148" id="Seg_1533" s="T147">baːl-lar</ta>
            <ta e="T149" id="Seg_1534" s="T148">Strʼesʼinskʼij</ta>
            <ta e="T150" id="Seg_1535" s="T149">di͡e-n</ta>
            <ta e="T151" id="Seg_1536" s="T150">taŋara-lar</ta>
            <ta e="T152" id="Seg_1537" s="T151">oččogo</ta>
            <ta e="T153" id="Seg_1538" s="T152">bu͡ollagɨna</ta>
            <ta e="T154" id="Seg_1539" s="T153">du͡o</ta>
            <ta e="T155" id="Seg_1540" s="T154">bu</ta>
            <ta e="T156" id="Seg_1541" s="T155">fevralʼ</ta>
            <ta e="T157" id="Seg_1542" s="T156">ɨj-ga</ta>
            <ta e="T158" id="Seg_1543" s="T157">oččogo</ta>
            <ta e="T159" id="Seg_1544" s="T158">bu͡o</ta>
            <ta e="T160" id="Seg_1545" s="T159">ol</ta>
            <ta e="T161" id="Seg_1546" s="T160">ihin</ta>
            <ta e="T162" id="Seg_1547" s="T161">du͡o</ta>
            <ta e="T163" id="Seg_1548" s="T162">bu</ta>
            <ta e="T164" id="Seg_1549" s="T163">ogo-loru-n</ta>
            <ta e="T165" id="Seg_1550" s="T164">Hemen</ta>
            <ta e="T166" id="Seg_1551" s="T165">di͡e-n</ta>
            <ta e="T167" id="Seg_1552" s="T166">aːttɨː-l-lar</ta>
            <ta e="T168" id="Seg_1553" s="T167">Hemeːče</ta>
            <ta e="T169" id="Seg_1554" s="T168">haka-lɨː</ta>
            <ta e="T170" id="Seg_1555" s="T169">Hemeːče</ta>
            <ta e="T171" id="Seg_1556" s="T170">di͡e-n</ta>
            <ta e="T172" id="Seg_1557" s="T171">aːttɨː-l-lar</ta>
            <ta e="T173" id="Seg_1558" s="T172">u͡ol</ta>
            <ta e="T174" id="Seg_1559" s="T173">ogo</ta>
            <ta e="T175" id="Seg_1560" s="T174">bu͡ol-but</ta>
            <ta e="T176" id="Seg_1561" s="T175">töröː-büt-e</ta>
            <ta e="T177" id="Seg_1562" s="T176">di͡e-n</ta>
            <ta e="T178" id="Seg_1563" s="T177">ol</ta>
            <ta e="T179" id="Seg_1564" s="T178">aːt</ta>
            <ta e="T180" id="Seg_1565" s="T179">hu͡ol</ta>
            <ta e="T181" id="Seg_1566" s="T180">bi͡er-el-ler</ta>
            <ta e="T182" id="Seg_1567" s="T181">hürekteː-bit</ta>
            <ta e="T183" id="Seg_1568" s="T182">aːt-a</ta>
            <ta e="T184" id="Seg_1569" s="T183">Hemeːče</ta>
            <ta e="T185" id="Seg_1570" s="T184">innʼe</ta>
            <ta e="T186" id="Seg_1571" s="T185">gɨn-an</ta>
            <ta e="T187" id="Seg_1572" s="T186">Hemen-i-ŋ</ta>
            <ta e="T188" id="Seg_1573" s="T187">inʼe-ti-ger</ta>
            <ta e="T189" id="Seg_1574" s="T188">bi͡er-el-ler</ta>
            <ta e="T190" id="Seg_1575" s="T189">bu</ta>
            <ta e="T191" id="Seg_1576" s="T190">Hemen</ta>
            <ta e="T192" id="Seg_1577" s="T191">inʼe-te</ta>
            <ta e="T193" id="Seg_1578" s="T192">bu͡olla</ta>
            <ta e="T194" id="Seg_1579" s="T193">hubu-ttan</ta>
            <ta e="T195" id="Seg_1580" s="T194">beje-ti-n</ta>
            <ta e="T196" id="Seg_1581" s="T195">aːt-ɨ-n</ta>
            <ta e="T197" id="Seg_1582" s="T196">aːttaː-bat-tar</ta>
            <ta e="T198" id="Seg_1583" s="T197">bɨlɨr-gɨ</ta>
            <ta e="T199" id="Seg_1584" s="T198">üje-ge</ta>
            <ta e="T200" id="Seg_1585" s="T199">kɨlɨː-ga</ta>
            <ta e="T201" id="Seg_1586" s="T200">aːttaː-ččɨ-lar</ta>
            <ta e="T202" id="Seg_1587" s="T201">ulakan</ta>
            <ta e="T203" id="Seg_1588" s="T202">u͡ol-u-nan</ta>
            <ta e="T204" id="Seg_1589" s="T203">duː</ta>
            <ta e="T205" id="Seg_1590" s="T204">ulakan</ta>
            <ta e="T206" id="Seg_1591" s="T205">kɨːh-ɨ-nan</ta>
            <ta e="T207" id="Seg_1592" s="T206">duː</ta>
            <ta e="T208" id="Seg_1593" s="T207">ol</ta>
            <ta e="T209" id="Seg_1594" s="T208">ihin</ta>
            <ta e="T210" id="Seg_1595" s="T209">Hemeːče</ta>
            <ta e="T211" id="Seg_1596" s="T210">inʼe-te</ta>
            <ta e="T212" id="Seg_1597" s="T211">bu͡ol-ar</ta>
            <ta e="T213" id="Seg_1598" s="T212">aga-ta</ta>
            <ta e="T214" id="Seg_1599" s="T213">bu͡ollagɨna</ta>
            <ta e="T215" id="Seg_1600" s="T214">Hemeːče</ta>
            <ta e="T216" id="Seg_1601" s="T215">aga-ta</ta>
            <ta e="T217" id="Seg_1602" s="T216">bu͡ol-ar</ta>
            <ta e="T218" id="Seg_1603" s="T217">innʼe</ta>
            <ta e="T219" id="Seg_1604" s="T218">gɨn-an</ta>
            <ta e="T220" id="Seg_1605" s="T219">ol</ta>
            <ta e="T221" id="Seg_1606" s="T220">kördük</ta>
            <ta e="T222" id="Seg_1607" s="T221">aːttaː-n-nar</ta>
            <ta e="T223" id="Seg_1608" s="T222">kurum</ta>
            <ta e="T224" id="Seg_1609" s="T223">oŋost-on-nor</ta>
            <ta e="T225" id="Seg_1610" s="T224">kurumnaː-n-tatɨmnaː-n</ta>
            <ta e="T226" id="Seg_1611" s="T225">bu</ta>
            <ta e="T227" id="Seg_1612" s="T226">dʼon-u</ta>
            <ta e="T228" id="Seg_1613" s="T227">barɨ-tɨ-n</ta>
            <ta e="T229" id="Seg_1614" s="T228">ɨgɨr-an-nar</ta>
            <ta e="T230" id="Seg_1615" s="T229">ogo</ta>
            <ta e="T231" id="Seg_1616" s="T230">töröː-büt</ta>
            <ta e="T232" id="Seg_1617" s="T231">kurum-a</ta>
            <ta e="T233" id="Seg_1618" s="T232">bu͡ol-ar</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_1619" s="T1">bu</ta>
            <ta e="T3" id="Seg_1620" s="T2">ogo-nI</ta>
            <ta e="T4" id="Seg_1621" s="T3">üs-Is</ta>
            <ta e="T5" id="Seg_1622" s="T4">kün-tI-GAr</ta>
            <ta e="T6" id="Seg_1623" s="T5">ol</ta>
            <ta e="T7" id="Seg_1624" s="T6">bihik</ta>
            <ta e="T8" id="Seg_1625" s="T7">oŋohulun-I-BIT-tI-GAr</ta>
            <ta e="T9" id="Seg_1626" s="T8">bileː-Ar-LAr</ta>
            <ta e="T10" id="Seg_1627" s="T9">onton</ta>
            <ta e="T11" id="Seg_1628" s="T10">bi͡es-Is</ta>
            <ta e="T12" id="Seg_1629" s="T11">du͡o</ta>
            <ta e="T13" id="Seg_1630" s="T12">hette-Is</ta>
            <ta e="T14" id="Seg_1631" s="T13">du͡o</ta>
            <ta e="T15" id="Seg_1632" s="T14">kün-tI-GAr</ta>
            <ta e="T16" id="Seg_1633" s="T15">kiːn-tA</ta>
            <ta e="T17" id="Seg_1634" s="T16">tüs-Ar</ta>
            <ta e="T18" id="Seg_1635" s="T17">ol</ta>
            <ta e="T19" id="Seg_1636" s="T18">kiːn-tA</ta>
            <ta e="T20" id="Seg_1637" s="T19">tüs-BIT-tI-GAr</ta>
            <ta e="T21" id="Seg_1638" s="T20">du͡o</ta>
            <ta e="T22" id="Seg_1639" s="T21">ol</ta>
            <ta e="T23" id="Seg_1640" s="T22">ogo-nI</ta>
            <ta e="T24" id="Seg_1641" s="T23">du͡o</ta>
            <ta e="T25" id="Seg_1642" s="T24">hürekteː-Ar-LAr</ta>
            <ta e="T26" id="Seg_1643" s="T25">onton</ta>
            <ta e="T27" id="Seg_1644" s="T26">bu</ta>
            <ta e="T28" id="Seg_1645" s="T27">ogo</ta>
            <ta e="T29" id="Seg_1646" s="T28">kiːn-tA</ta>
            <ta e="T30" id="Seg_1647" s="T29">tüs-TAK-InA</ta>
            <ta e="T31" id="Seg_1648" s="T30">hürekteː-Ar-LAr</ta>
            <ta e="T32" id="Seg_1649" s="T31">ogo-nI</ta>
            <ta e="T33" id="Seg_1650" s="T32">ol</ta>
            <ta e="T34" id="Seg_1651" s="T33">hürekteː-An</ta>
            <ta e="T35" id="Seg_1652" s="T34">mu͡ora-GA</ta>
            <ta e="T36" id="Seg_1653" s="T35">bu͡ollagɨna</ta>
            <ta e="T37" id="Seg_1654" s="T36">ɨraːk</ta>
            <ta e="T38" id="Seg_1655" s="T37">bu</ta>
            <ta e="T39" id="Seg_1656" s="T38">agabɨt</ta>
            <ta e="T40" id="Seg_1657" s="T39">hu͡ok</ta>
            <ta e="T41" id="Seg_1658" s="T40">hir-tI-GAr</ta>
            <ta e="T42" id="Seg_1659" s="T41">kim</ta>
            <ta e="T43" id="Seg_1660" s="T42">hürekteː-A</ta>
            <ta e="T44" id="Seg_1661" s="T43">hataː-Ar</ta>
            <ta e="T45" id="Seg_1662" s="T44">malʼitva-nI</ta>
            <ta e="T46" id="Seg_1663" s="T45">üŋün-Ar</ta>
            <ta e="T47" id="Seg_1664" s="T46">haŋa-nI</ta>
            <ta e="T48" id="Seg_1665" s="T47">bil-Ar</ta>
            <ta e="T49" id="Seg_1666" s="T48">hürekteː-Ar</ta>
            <ta e="T50" id="Seg_1667" s="T49">üŋün-Ar</ta>
            <ta e="T51" id="Seg_1668" s="T50">haŋa-nI</ta>
            <ta e="T52" id="Seg_1669" s="T51">bil-Ar</ta>
            <ta e="T53" id="Seg_1670" s="T52">kihi</ta>
            <ta e="T54" id="Seg_1671" s="T53">ebe-tA</ta>
            <ta e="T55" id="Seg_1672" s="T54">du͡o</ta>
            <ta e="T56" id="Seg_1673" s="T55">ehe-tA</ta>
            <ta e="T57" id="Seg_1674" s="T56">du͡o</ta>
            <ta e="T58" id="Seg_1675" s="T57">ol</ta>
            <ta e="T59" id="Seg_1676" s="T58">hürekteː-Ar</ta>
            <ta e="T60" id="Seg_1677" s="T59">tɨ͡a-GA</ta>
            <ta e="T61" id="Seg_1678" s="T60">mu͡ora-GA</ta>
            <ta e="T62" id="Seg_1679" s="T61">oččogo</ta>
            <ta e="T63" id="Seg_1680" s="T62">bu</ta>
            <ta e="T64" id="Seg_1681" s="T63">hürekteː-An</ta>
            <ta e="T65" id="Seg_1682" s="T64">ajɨː</ta>
            <ta e="T66" id="Seg_1683" s="T65">uː-tA</ta>
            <ta e="T67" id="Seg_1684" s="T66">taŋara-nI</ta>
            <ta e="T68" id="Seg_1685" s="T67">uːr-An-LAr</ta>
            <ta e="T69" id="Seg_1686" s="T68">uː-nI</ta>
            <ta e="T70" id="Seg_1687" s="T69">kut-Ar-LAr</ta>
            <ta e="T71" id="Seg_1688" s="T70">ihit-GA</ta>
            <ta e="T72" id="Seg_1689" s="T71">onton</ta>
            <ta e="T73" id="Seg_1690" s="T72">du͡o</ta>
            <ta e="T74" id="Seg_1691" s="T73">taŋara-nI</ta>
            <ta e="T75" id="Seg_1692" s="T74">uːr</ta>
            <ta e="T76" id="Seg_1693" s="T75">uk-Ar-LAr</ta>
            <ta e="T77" id="Seg_1694" s="T76">onno</ta>
            <ta e="T78" id="Seg_1695" s="T77">oččogo</ta>
            <ta e="T79" id="Seg_1696" s="T78">taŋara</ta>
            <ta e="T80" id="Seg_1697" s="T79">uː-tA</ta>
            <ta e="T81" id="Seg_1698" s="T80">bu͡ol-Ar</ta>
            <ta e="T82" id="Seg_1699" s="T81">ol</ta>
            <ta e="T83" id="Seg_1700" s="T82">uː-ŋ</ta>
            <ta e="T84" id="Seg_1701" s="T83">ol</ta>
            <ta e="T85" id="Seg_1702" s="T84">hibi͡etu͡oj</ta>
            <ta e="T86" id="Seg_1703" s="T85">taŋara</ta>
            <ta e="T87" id="Seg_1704" s="T86">uː-tI-GAr</ta>
            <ta e="T88" id="Seg_1705" s="T87">huːj-Ar-LAr</ta>
            <ta e="T89" id="Seg_1706" s="T88">bu</ta>
            <ta e="T90" id="Seg_1707" s="T89">ogo-nI</ta>
            <ta e="T91" id="Seg_1708" s="T90">töbö-kAːN-tI-n</ta>
            <ta e="T92" id="Seg_1709" s="T91">üs-TA</ta>
            <ta e="T93" id="Seg_1710" s="T92">kat</ta>
            <ta e="T94" id="Seg_1711" s="T93">aga</ta>
            <ta e="T96" id="Seg_1712" s="T95">eteŋŋe</ta>
            <ta e="T97" id="Seg_1713" s="T96">olor</ta>
            <ta e="T98" id="Seg_1714" s="T97">di͡e-An-LAr</ta>
            <ta e="T99" id="Seg_1715" s="T98">eteŋŋe</ta>
            <ta e="T100" id="Seg_1716" s="T99">ü͡öskeː</ta>
            <ta e="T101" id="Seg_1717" s="T100">di͡e-An-LAr</ta>
            <ta e="T102" id="Seg_1718" s="T101">onnuk</ta>
            <ta e="T103" id="Seg_1719" s="T102">bulčut</ta>
            <ta e="T104" id="Seg_1720" s="T103">bu͡ol</ta>
            <ta e="T105" id="Seg_1721" s="T104">mannɨk</ta>
            <ta e="T106" id="Seg_1722" s="T105">as-ČIt</ta>
            <ta e="T107" id="Seg_1723" s="T106">bu͡ol</ta>
            <ta e="T108" id="Seg_1724" s="T107">di͡e-An-LAr</ta>
            <ta e="T109" id="Seg_1725" s="T108">onton</ta>
            <ta e="T110" id="Seg_1726" s="T109">bu͡ollagɨna</ta>
            <ta e="T111" id="Seg_1727" s="T110">kannɨk</ta>
            <ta e="T112" id="Seg_1728" s="T111">ɨj-GA</ta>
            <ta e="T113" id="Seg_1729" s="T112">ol</ta>
            <ta e="T114" id="Seg_1730" s="T113">ogo</ta>
            <ta e="T115" id="Seg_1731" s="T114">töröː-BIT-tA</ta>
            <ta e="T116" id="Seg_1732" s="T115">bihi͡ene</ta>
            <ta e="T117" id="Seg_1733" s="T116">bu</ta>
            <ta e="T118" id="Seg_1734" s="T117">oːnnʼoː-Ar</ta>
            <ta e="T119" id="Seg_1735" s="T118">ogo-BIt</ta>
            <ta e="T120" id="Seg_1736" s="T119">bu</ta>
            <ta e="T121" id="Seg_1737" s="T120">oːnnʼoː-Ar-BItI-GAr</ta>
            <ta e="T122" id="Seg_1738" s="T121">fʼevralʼ</ta>
            <ta e="T123" id="Seg_1739" s="T122">ɨj-GA</ta>
            <ta e="T124" id="Seg_1740" s="T123">töröː-TI-tA</ta>
            <ta e="T125" id="Seg_1741" s="T124">bu</ta>
            <ta e="T126" id="Seg_1742" s="T125">fʼevralʼ</ta>
            <ta e="T127" id="Seg_1743" s="T126">ɨj-GA</ta>
            <ta e="T128" id="Seg_1744" s="T127">bu͡ollagɨna</ta>
            <ta e="T129" id="Seg_1745" s="T128">taŋara-LAr</ta>
            <ta e="T130" id="Seg_1746" s="T129">baːr-LAr</ta>
            <ta e="T131" id="Seg_1747" s="T130">hibi͡etu͡oj</ta>
            <ta e="T132" id="Seg_1748" s="T131">taŋara-LAr</ta>
            <ta e="T133" id="Seg_1749" s="T132">hibi͡etu͡oj-LAr</ta>
            <ta e="T134" id="Seg_1750" s="T133">Aksʼinʼja</ta>
            <ta e="T135" id="Seg_1751" s="T134">di͡e-An</ta>
            <ta e="T136" id="Seg_1752" s="T135">taŋara-LAr</ta>
            <ta e="T137" id="Seg_1753" s="T136">oččogo</ta>
            <ta e="T138" id="Seg_1754" s="T137">kɨːs</ta>
            <ta e="T139" id="Seg_1755" s="T138">ogo</ta>
            <ta e="T140" id="Seg_1756" s="T139">e-BIT-tA</ta>
            <ta e="T141" id="Seg_1757" s="T140">bu͡olla</ta>
            <ta e="T142" id="Seg_1758" s="T141">Aksʼinʼja</ta>
            <ta e="T143" id="Seg_1759" s="T142">di͡e-An</ta>
            <ta e="T144" id="Seg_1760" s="T143">aːt</ta>
            <ta e="T145" id="Seg_1761" s="T144">e-BIT</ta>
            <ta e="T146" id="Seg_1762" s="T145">e-BIT</ta>
            <ta e="T147" id="Seg_1763" s="T146">össü͡ö</ta>
            <ta e="T148" id="Seg_1764" s="T147">baːr-LAr</ta>
            <ta e="T149" id="Seg_1765" s="T148">Strʼesʼinskʼij</ta>
            <ta e="T150" id="Seg_1766" s="T149">di͡e-An</ta>
            <ta e="T151" id="Seg_1767" s="T150">taŋara-LAr</ta>
            <ta e="T152" id="Seg_1768" s="T151">oččogo</ta>
            <ta e="T153" id="Seg_1769" s="T152">bu͡ollagɨna</ta>
            <ta e="T154" id="Seg_1770" s="T153">du͡o</ta>
            <ta e="T155" id="Seg_1771" s="T154">bu</ta>
            <ta e="T156" id="Seg_1772" s="T155">fʼevralʼ</ta>
            <ta e="T157" id="Seg_1773" s="T156">ɨj-GA</ta>
            <ta e="T158" id="Seg_1774" s="T157">oččogo</ta>
            <ta e="T159" id="Seg_1775" s="T158">bu͡o</ta>
            <ta e="T160" id="Seg_1776" s="T159">ol</ta>
            <ta e="T161" id="Seg_1777" s="T160">ihin</ta>
            <ta e="T162" id="Seg_1778" s="T161">du͡o</ta>
            <ta e="T163" id="Seg_1779" s="T162">bu</ta>
            <ta e="T164" id="Seg_1780" s="T163">ogo-LArI-n</ta>
            <ta e="T165" id="Seg_1781" s="T164">Hemen</ta>
            <ta e="T166" id="Seg_1782" s="T165">di͡e-An</ta>
            <ta e="T167" id="Seg_1783" s="T166">aːttaː-Ar-LAr</ta>
            <ta e="T168" id="Seg_1784" s="T167">Hemeːče</ta>
            <ta e="T169" id="Seg_1785" s="T168">haka-LIː</ta>
            <ta e="T170" id="Seg_1786" s="T169">Hemeːče</ta>
            <ta e="T171" id="Seg_1787" s="T170">di͡e-An</ta>
            <ta e="T172" id="Seg_1788" s="T171">aːttaː-Ar-LAr</ta>
            <ta e="T173" id="Seg_1789" s="T172">u͡ol</ta>
            <ta e="T174" id="Seg_1790" s="T173">ogo</ta>
            <ta e="T175" id="Seg_1791" s="T174">bu͡ol-BIT</ta>
            <ta e="T176" id="Seg_1792" s="T175">töröː-BIT-tA</ta>
            <ta e="T177" id="Seg_1793" s="T176">di͡e-An</ta>
            <ta e="T178" id="Seg_1794" s="T177">ol</ta>
            <ta e="T179" id="Seg_1795" s="T178">aːt</ta>
            <ta e="T180" id="Seg_1796" s="T179">hu͡ol</ta>
            <ta e="T181" id="Seg_1797" s="T180">bi͡er-Ar-LAr</ta>
            <ta e="T182" id="Seg_1798" s="T181">hürekteː-BIT</ta>
            <ta e="T183" id="Seg_1799" s="T182">aːt-tA</ta>
            <ta e="T184" id="Seg_1800" s="T183">Hemeːče</ta>
            <ta e="T185" id="Seg_1801" s="T184">innʼe</ta>
            <ta e="T186" id="Seg_1802" s="T185">gɨn-An</ta>
            <ta e="T187" id="Seg_1803" s="T186">Hemen-I-ŋ</ta>
            <ta e="T188" id="Seg_1804" s="T187">inʼe-tI-GAr</ta>
            <ta e="T189" id="Seg_1805" s="T188">bi͡er-Ar-LAr</ta>
            <ta e="T190" id="Seg_1806" s="T189">bu</ta>
            <ta e="T191" id="Seg_1807" s="T190">Hemen</ta>
            <ta e="T192" id="Seg_1808" s="T191">inʼe-tA</ta>
            <ta e="T193" id="Seg_1809" s="T192">bu͡olla</ta>
            <ta e="T194" id="Seg_1810" s="T193">hubu-ttAn</ta>
            <ta e="T195" id="Seg_1811" s="T194">beje-tI-n</ta>
            <ta e="T196" id="Seg_1812" s="T195">aːt-tI-n</ta>
            <ta e="T197" id="Seg_1813" s="T196">aːttaː-BAT-LAr</ta>
            <ta e="T198" id="Seg_1814" s="T197">bɨlɨr-GI</ta>
            <ta e="T199" id="Seg_1815" s="T198">üje-GA</ta>
            <ta e="T200" id="Seg_1816" s="T199">kɨlɨː-GA</ta>
            <ta e="T201" id="Seg_1817" s="T200">aːttaː-AːččI-LAr</ta>
            <ta e="T202" id="Seg_1818" s="T201">ulakan</ta>
            <ta e="T203" id="Seg_1819" s="T202">u͡ol-tI-nAn</ta>
            <ta e="T204" id="Seg_1820" s="T203">du͡o</ta>
            <ta e="T205" id="Seg_1821" s="T204">ulakan</ta>
            <ta e="T206" id="Seg_1822" s="T205">kɨːs-tI-nAn</ta>
            <ta e="T207" id="Seg_1823" s="T206">du͡o</ta>
            <ta e="T208" id="Seg_1824" s="T207">ol</ta>
            <ta e="T209" id="Seg_1825" s="T208">ihin</ta>
            <ta e="T210" id="Seg_1826" s="T209">Hemeːče</ta>
            <ta e="T211" id="Seg_1827" s="T210">inʼe-tA</ta>
            <ta e="T212" id="Seg_1828" s="T211">bu͡ol-Ar</ta>
            <ta e="T213" id="Seg_1829" s="T212">aga-tA</ta>
            <ta e="T214" id="Seg_1830" s="T213">bu͡ollagɨna</ta>
            <ta e="T215" id="Seg_1831" s="T214">Hemeːče</ta>
            <ta e="T216" id="Seg_1832" s="T215">aga-tA</ta>
            <ta e="T217" id="Seg_1833" s="T216">bu͡ol-Ar</ta>
            <ta e="T218" id="Seg_1834" s="T217">innʼe</ta>
            <ta e="T219" id="Seg_1835" s="T218">gɨn-An</ta>
            <ta e="T220" id="Seg_1836" s="T219">ol</ta>
            <ta e="T221" id="Seg_1837" s="T220">kördük</ta>
            <ta e="T222" id="Seg_1838" s="T221">aːttaː-An-LAr</ta>
            <ta e="T223" id="Seg_1839" s="T222">kurum</ta>
            <ta e="T224" id="Seg_1840" s="T223">oŋohun-An-LAr</ta>
            <ta e="T225" id="Seg_1841" s="T224">kurumnaː-An-tatɨmnaː-An</ta>
            <ta e="T226" id="Seg_1842" s="T225">bu</ta>
            <ta e="T227" id="Seg_1843" s="T226">dʼon-nI</ta>
            <ta e="T228" id="Seg_1844" s="T227">barɨ-tI-n</ta>
            <ta e="T229" id="Seg_1845" s="T228">ɨgɨr-An-LAr</ta>
            <ta e="T230" id="Seg_1846" s="T229">ogo</ta>
            <ta e="T231" id="Seg_1847" s="T230">töröː-BIT</ta>
            <ta e="T232" id="Seg_1848" s="T231">kurum-tA</ta>
            <ta e="T233" id="Seg_1849" s="T232">bu͡ol-Ar</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_1850" s="T1">this</ta>
            <ta e="T3" id="Seg_1851" s="T2">child-ACC</ta>
            <ta e="T4" id="Seg_1852" s="T3">three-ORD</ta>
            <ta e="T5" id="Seg_1853" s="T4">day-3SG-DAT/LOC</ta>
            <ta e="T6" id="Seg_1854" s="T5">that</ta>
            <ta e="T7" id="Seg_1855" s="T6">cradle.[NOM]</ta>
            <ta e="T8" id="Seg_1856" s="T7">be.made-EP-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T9" id="Seg_1857" s="T8">lay-PRS-3PL</ta>
            <ta e="T10" id="Seg_1858" s="T9">then</ta>
            <ta e="T11" id="Seg_1859" s="T10">five-ORD</ta>
            <ta e="T12" id="Seg_1860" s="T11">Q</ta>
            <ta e="T13" id="Seg_1861" s="T12">seven-ORD</ta>
            <ta e="T14" id="Seg_1862" s="T13">Q</ta>
            <ta e="T15" id="Seg_1863" s="T14">day-3SG-DAT/LOC</ta>
            <ta e="T16" id="Seg_1864" s="T15">navel-3SG.[NOM]</ta>
            <ta e="T17" id="Seg_1865" s="T16">fall-PRS.[3SG]</ta>
            <ta e="T18" id="Seg_1866" s="T17">that</ta>
            <ta e="T19" id="Seg_1867" s="T18">navel-3SG.[NOM]</ta>
            <ta e="T20" id="Seg_1868" s="T19">fall-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T21" id="Seg_1869" s="T20">MOD</ta>
            <ta e="T22" id="Seg_1870" s="T21">that</ta>
            <ta e="T23" id="Seg_1871" s="T22">child-ACC</ta>
            <ta e="T24" id="Seg_1872" s="T23">MOD</ta>
            <ta e="T25" id="Seg_1873" s="T24">baptize-PRS-3PL</ta>
            <ta e="T26" id="Seg_1874" s="T25">then</ta>
            <ta e="T27" id="Seg_1875" s="T26">this</ta>
            <ta e="T28" id="Seg_1876" s="T27">child.[NOM]</ta>
            <ta e="T29" id="Seg_1877" s="T28">navel-3SG.[NOM]</ta>
            <ta e="T30" id="Seg_1878" s="T29">fall-TEMP-3SG</ta>
            <ta e="T31" id="Seg_1879" s="T30">baptize-PRS-3PL</ta>
            <ta e="T32" id="Seg_1880" s="T31">child-ACC</ta>
            <ta e="T33" id="Seg_1881" s="T32">that</ta>
            <ta e="T34" id="Seg_1882" s="T33">baptize-CVB.SEQ</ta>
            <ta e="T35" id="Seg_1883" s="T34">tundra-DAT/LOC</ta>
            <ta e="T36" id="Seg_1884" s="T35">though</ta>
            <ta e="T37" id="Seg_1885" s="T36">far.away</ta>
            <ta e="T38" id="Seg_1886" s="T37">this</ta>
            <ta e="T39" id="Seg_1887" s="T38">clergyman.[NOM]</ta>
            <ta e="T40" id="Seg_1888" s="T39">NEG.EX</ta>
            <ta e="T41" id="Seg_1889" s="T40">place-3SG-DAT/LOC</ta>
            <ta e="T42" id="Seg_1890" s="T41">who.[NOM]</ta>
            <ta e="T43" id="Seg_1891" s="T42">baptize-CVB.SIM</ta>
            <ta e="T44" id="Seg_1892" s="T43">can-PRS.[3SG]</ta>
            <ta e="T45" id="Seg_1893" s="T44">prayer-ACC</ta>
            <ta e="T46" id="Seg_1894" s="T45">pray-PTCP.PRS</ta>
            <ta e="T47" id="Seg_1895" s="T46">word-ACC</ta>
            <ta e="T48" id="Seg_1896" s="T47">know-PRS.[3SG]</ta>
            <ta e="T49" id="Seg_1897" s="T48">baptize-PTCP.PRS</ta>
            <ta e="T50" id="Seg_1898" s="T49">pray-PTCP.PRS</ta>
            <ta e="T51" id="Seg_1899" s="T50">word-ACC</ta>
            <ta e="T52" id="Seg_1900" s="T51">know-PTCP.PRS</ta>
            <ta e="T53" id="Seg_1901" s="T52">human.being.[NOM]</ta>
            <ta e="T54" id="Seg_1902" s="T53">grandmother-3SG.[NOM]</ta>
            <ta e="T55" id="Seg_1903" s="T54">Q</ta>
            <ta e="T56" id="Seg_1904" s="T55">grandfather-3SG.[NOM]</ta>
            <ta e="T57" id="Seg_1905" s="T56">Q</ta>
            <ta e="T58" id="Seg_1906" s="T57">that.[NOM]</ta>
            <ta e="T59" id="Seg_1907" s="T58">baptize-PRS.[3SG]</ta>
            <ta e="T60" id="Seg_1908" s="T59">tundra-DAT/LOC</ta>
            <ta e="T61" id="Seg_1909" s="T60">tundra-DAT/LOC</ta>
            <ta e="T62" id="Seg_1910" s="T61">then</ta>
            <ta e="T63" id="Seg_1911" s="T62">this</ta>
            <ta e="T64" id="Seg_1912" s="T63">baptize-CVB.SEQ</ta>
            <ta e="T65" id="Seg_1913" s="T64">good</ta>
            <ta e="T66" id="Seg_1914" s="T65">water-3SG.[NOM]</ta>
            <ta e="T67" id="Seg_1915" s="T66">icon-ACC</ta>
            <ta e="T68" id="Seg_1916" s="T67">lay-CVB.SEQ-3PL</ta>
            <ta e="T69" id="Seg_1917" s="T68">water-ACC</ta>
            <ta e="T70" id="Seg_1918" s="T69">pour-PRS-3PL</ta>
            <ta e="T71" id="Seg_1919" s="T70">jar-DAT/LOC</ta>
            <ta e="T72" id="Seg_1920" s="T71">then</ta>
            <ta e="T73" id="Seg_1921" s="T72">MOD</ta>
            <ta e="T74" id="Seg_1922" s="T73">icon-ACC</ta>
            <ta e="T75" id="Seg_1923" s="T74">lay</ta>
            <ta e="T76" id="Seg_1924" s="T75">stick-PRS-3PL</ta>
            <ta e="T77" id="Seg_1925" s="T76">thither</ta>
            <ta e="T78" id="Seg_1926" s="T77">then</ta>
            <ta e="T79" id="Seg_1927" s="T78">god.[NOM]</ta>
            <ta e="T80" id="Seg_1928" s="T79">water-3SG.[NOM]</ta>
            <ta e="T81" id="Seg_1929" s="T80">be-PRS.[3SG]</ta>
            <ta e="T82" id="Seg_1930" s="T81">that</ta>
            <ta e="T83" id="Seg_1931" s="T82">water-2SG.[NOM]</ta>
            <ta e="T84" id="Seg_1932" s="T83">that</ta>
            <ta e="T85" id="Seg_1933" s="T84">holy.[NOM]</ta>
            <ta e="T86" id="Seg_1934" s="T85">god.[NOM]</ta>
            <ta e="T87" id="Seg_1935" s="T86">water-3SG-DAT/LOC</ta>
            <ta e="T88" id="Seg_1936" s="T87">wash-PRS-3PL</ta>
            <ta e="T89" id="Seg_1937" s="T88">this</ta>
            <ta e="T90" id="Seg_1938" s="T89">child-ACC</ta>
            <ta e="T91" id="Seg_1939" s="T90">head-DIM-3SG-ACC</ta>
            <ta e="T92" id="Seg_1940" s="T91">three-MLTP</ta>
            <ta e="T93" id="Seg_1941" s="T92">time.[NOM]</ta>
            <ta e="T94" id="Seg_1942" s="T93">father.[NOM]</ta>
            <ta e="T96" id="Seg_1943" s="T95">happy.[NOM]</ta>
            <ta e="T97" id="Seg_1944" s="T96">live.[IMP.2SG]</ta>
            <ta e="T98" id="Seg_1945" s="T97">say-CVB.SEQ-3PL</ta>
            <ta e="T99" id="Seg_1946" s="T98">happy.[NOM]</ta>
            <ta e="T100" id="Seg_1947" s="T99">arise.[IMP.2SG]</ta>
            <ta e="T101" id="Seg_1948" s="T100">say-CVB.SEQ-3PL</ta>
            <ta e="T102" id="Seg_1949" s="T101">such.[NOM]</ta>
            <ta e="T103" id="Seg_1950" s="T102">hunter.[NOM]</ta>
            <ta e="T104" id="Seg_1951" s="T103">be.[IMP.2SG]</ta>
            <ta e="T105" id="Seg_1952" s="T104">such</ta>
            <ta e="T106" id="Seg_1953" s="T105">food-AG.[NOM]</ta>
            <ta e="T107" id="Seg_1954" s="T106">be.[IMP.2SG]</ta>
            <ta e="T108" id="Seg_1955" s="T107">say-CVB.SEQ-3PL</ta>
            <ta e="T109" id="Seg_1956" s="T108">then</ta>
            <ta e="T110" id="Seg_1957" s="T109">though</ta>
            <ta e="T111" id="Seg_1958" s="T110">what.kind.of</ta>
            <ta e="T112" id="Seg_1959" s="T111">month-DAT/LOC</ta>
            <ta e="T113" id="Seg_1960" s="T112">that</ta>
            <ta e="T114" id="Seg_1961" s="T113">child.[NOM]</ta>
            <ta e="T115" id="Seg_1962" s="T114">be.born-PST2-3SG</ta>
            <ta e="T116" id="Seg_1963" s="T115">our</ta>
            <ta e="T117" id="Seg_1964" s="T116">this</ta>
            <ta e="T118" id="Seg_1965" s="T117">play-PTCP.PRS</ta>
            <ta e="T119" id="Seg_1966" s="T118">child-1PL.[NOM]</ta>
            <ta e="T120" id="Seg_1967" s="T119">this</ta>
            <ta e="T121" id="Seg_1968" s="T120">play-PTCP.PRS-1PL-DAT/LOC</ta>
            <ta e="T122" id="Seg_1969" s="T121">February.[NOM]</ta>
            <ta e="T123" id="Seg_1970" s="T122">month-DAT/LOC</ta>
            <ta e="T124" id="Seg_1971" s="T123">be.born-PST1-3SG</ta>
            <ta e="T125" id="Seg_1972" s="T124">this</ta>
            <ta e="T126" id="Seg_1973" s="T125">February.[NOM]</ta>
            <ta e="T127" id="Seg_1974" s="T126">month-DAT/LOC</ta>
            <ta e="T128" id="Seg_1975" s="T127">though</ta>
            <ta e="T129" id="Seg_1976" s="T128">icon-PL.[NOM]</ta>
            <ta e="T130" id="Seg_1977" s="T129">there.is-3PL</ta>
            <ta e="T131" id="Seg_1978" s="T130">holy.[NOM]</ta>
            <ta e="T132" id="Seg_1979" s="T131">icon-PL.[NOM]</ta>
            <ta e="T133" id="Seg_1980" s="T132">holy-PL.[NOM]</ta>
            <ta e="T134" id="Seg_1981" s="T133">Aksinya</ta>
            <ta e="T135" id="Seg_1982" s="T134">say-CVB.SEQ</ta>
            <ta e="T136" id="Seg_1983" s="T135">icon-PL.[NOM]</ta>
            <ta e="T137" id="Seg_1984" s="T136">then</ta>
            <ta e="T138" id="Seg_1985" s="T137">girl.[NOM]</ta>
            <ta e="T139" id="Seg_1986" s="T138">child.[NOM]</ta>
            <ta e="T140" id="Seg_1987" s="T139">be-PST2-3SG</ta>
            <ta e="T141" id="Seg_1988" s="T140">MOD</ta>
            <ta e="T142" id="Seg_1989" s="T141">Aksinya.[NOM]</ta>
            <ta e="T143" id="Seg_1990" s="T142">say-CVB.SEQ</ta>
            <ta e="T144" id="Seg_1991" s="T143">name.[NOM]</ta>
            <ta e="T145" id="Seg_1992" s="T144">be-PST2.[3SG]</ta>
            <ta e="T146" id="Seg_1993" s="T145">be-PST2.[3SG]</ta>
            <ta e="T147" id="Seg_1994" s="T146">still</ta>
            <ta e="T148" id="Seg_1995" s="T147">there.is-3PL</ta>
            <ta e="T149" id="Seg_1996" s="T148">Stresinskiy</ta>
            <ta e="T150" id="Seg_1997" s="T149">say-CVB.SEQ</ta>
            <ta e="T151" id="Seg_1998" s="T150">icon-PL.[NOM]</ta>
            <ta e="T152" id="Seg_1999" s="T151">then</ta>
            <ta e="T153" id="Seg_2000" s="T152">though</ta>
            <ta e="T154" id="Seg_2001" s="T153">MOD</ta>
            <ta e="T155" id="Seg_2002" s="T154">this</ta>
            <ta e="T156" id="Seg_2003" s="T155">February</ta>
            <ta e="T157" id="Seg_2004" s="T156">month-DAT/LOC</ta>
            <ta e="T158" id="Seg_2005" s="T157">then</ta>
            <ta e="T159" id="Seg_2006" s="T158">EMPH</ta>
            <ta e="T160" id="Seg_2007" s="T159">that.[NOM]</ta>
            <ta e="T161" id="Seg_2008" s="T160">because.of</ta>
            <ta e="T162" id="Seg_2009" s="T161">MOD</ta>
            <ta e="T163" id="Seg_2010" s="T162">this</ta>
            <ta e="T164" id="Seg_2011" s="T163">child-3PL-ACC</ta>
            <ta e="T165" id="Seg_2012" s="T164">Semyon.[NOM]</ta>
            <ta e="T166" id="Seg_2013" s="T165">say-CVB.SEQ</ta>
            <ta e="T167" id="Seg_2014" s="T166">name-PRS-3PL</ta>
            <ta e="T168" id="Seg_2015" s="T167">Hemeeche.[NOM]</ta>
            <ta e="T169" id="Seg_2016" s="T168">Dolgan-SIM</ta>
            <ta e="T170" id="Seg_2017" s="T169">Hemeeche.[NOM]</ta>
            <ta e="T171" id="Seg_2018" s="T170">say-CVB.SEQ</ta>
            <ta e="T172" id="Seg_2019" s="T171">name-PRS-3PL</ta>
            <ta e="T173" id="Seg_2020" s="T172">boy.[NOM]</ta>
            <ta e="T174" id="Seg_2021" s="T173">child.[NOM]</ta>
            <ta e="T175" id="Seg_2022" s="T174">become-PST2.[3SG]</ta>
            <ta e="T176" id="Seg_2023" s="T175">be.born-PST2-3SG</ta>
            <ta e="T177" id="Seg_2024" s="T176">say-CVB.SEQ</ta>
            <ta e="T178" id="Seg_2025" s="T177">that</ta>
            <ta e="T179" id="Seg_2026" s="T178">name.[NOM]</ta>
            <ta e="T180" id="Seg_2027" s="T179">mark.[NOM]</ta>
            <ta e="T181" id="Seg_2028" s="T180">give-PRS-3PL</ta>
            <ta e="T182" id="Seg_2029" s="T181">baptize-PTCP.PST</ta>
            <ta e="T183" id="Seg_2030" s="T182">name-3SG.[NOM]</ta>
            <ta e="T184" id="Seg_2031" s="T183">Hemeeche.[NOM]</ta>
            <ta e="T185" id="Seg_2032" s="T184">so</ta>
            <ta e="T186" id="Seg_2033" s="T185">make-CVB.SEQ</ta>
            <ta e="T187" id="Seg_2034" s="T186">Semyon-EP-2SG.[NOM]</ta>
            <ta e="T188" id="Seg_2035" s="T187">mother-3SG-DAT/LOC</ta>
            <ta e="T189" id="Seg_2036" s="T188">give-PRS-3PL</ta>
            <ta e="T190" id="Seg_2037" s="T189">this</ta>
            <ta e="T191" id="Seg_2038" s="T190">Semyon.[NOM]</ta>
            <ta e="T192" id="Seg_2039" s="T191">mother-3SG.[NOM]</ta>
            <ta e="T193" id="Seg_2040" s="T192">MOD</ta>
            <ta e="T194" id="Seg_2041" s="T193">this.EMPH-ABL</ta>
            <ta e="T195" id="Seg_2042" s="T194">self-3SG-GEN</ta>
            <ta e="T196" id="Seg_2043" s="T195">name-3SG-ACC</ta>
            <ta e="T197" id="Seg_2044" s="T196">name-NEG-3PL</ta>
            <ta e="T198" id="Seg_2045" s="T197">long.ago-ADJZ</ta>
            <ta e="T199" id="Seg_2046" s="T198">time-DAT/LOC</ta>
            <ta e="T200" id="Seg_2047" s="T199">respect-DAT/LOC</ta>
            <ta e="T201" id="Seg_2048" s="T200">name-HAB-3PL</ta>
            <ta e="T202" id="Seg_2049" s="T201">big.[NOM]</ta>
            <ta e="T203" id="Seg_2050" s="T202">son-3SG-INSTR</ta>
            <ta e="T204" id="Seg_2051" s="T203">Q</ta>
            <ta e="T205" id="Seg_2052" s="T204">big.[NOM]</ta>
            <ta e="T206" id="Seg_2053" s="T205">daughter-3SG-INSTR</ta>
            <ta e="T207" id="Seg_2054" s="T206">Q</ta>
            <ta e="T208" id="Seg_2055" s="T207">that.[NOM]</ta>
            <ta e="T209" id="Seg_2056" s="T208">because.of</ta>
            <ta e="T210" id="Seg_2057" s="T209">Hemeeche.[NOM]</ta>
            <ta e="T211" id="Seg_2058" s="T210">mother-3SG.[NOM]</ta>
            <ta e="T212" id="Seg_2059" s="T211">be-PRS.[3SG]</ta>
            <ta e="T213" id="Seg_2060" s="T212">father-3SG.[NOM]</ta>
            <ta e="T214" id="Seg_2061" s="T213">though</ta>
            <ta e="T215" id="Seg_2062" s="T214">Hemeeche.[NOM]</ta>
            <ta e="T216" id="Seg_2063" s="T215">father-3SG.[NOM]</ta>
            <ta e="T217" id="Seg_2064" s="T216">become-PRS.[3SG]</ta>
            <ta e="T218" id="Seg_2065" s="T217">so</ta>
            <ta e="T219" id="Seg_2066" s="T218">make-CVB.SEQ</ta>
            <ta e="T220" id="Seg_2067" s="T219">that.[NOM]</ta>
            <ta e="T221" id="Seg_2068" s="T220">similar</ta>
            <ta e="T222" id="Seg_2069" s="T221">name-CVB.SEQ-3PL</ta>
            <ta e="T223" id="Seg_2070" s="T222">celebration.[NOM]</ta>
            <ta e="T224" id="Seg_2071" s="T223">make-CVB.SEQ-3PL</ta>
            <ta e="T225" id="Seg_2072" s="T224">celebrate-CVB.SEQ-%%-CVB.SEQ</ta>
            <ta e="T226" id="Seg_2073" s="T225">this</ta>
            <ta e="T227" id="Seg_2074" s="T226">people-ACC</ta>
            <ta e="T228" id="Seg_2075" s="T227">whole-3SG-ACC</ta>
            <ta e="T229" id="Seg_2076" s="T228">invite-CVB.SEQ-3PL</ta>
            <ta e="T230" id="Seg_2077" s="T229">child.[NOM]</ta>
            <ta e="T231" id="Seg_2078" s="T230">be.born-PTCP.PST</ta>
            <ta e="T232" id="Seg_2079" s="T231">celebration-3SG.[NOM]</ta>
            <ta e="T233" id="Seg_2080" s="T232">be-PRS.[3SG]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T2" id="Seg_2081" s="T1">dieses</ta>
            <ta e="T3" id="Seg_2082" s="T2">Kind-ACC</ta>
            <ta e="T4" id="Seg_2083" s="T3">drei-ORD</ta>
            <ta e="T5" id="Seg_2084" s="T4">Tag-3SG-DAT/LOC</ta>
            <ta e="T6" id="Seg_2085" s="T5">jenes</ta>
            <ta e="T7" id="Seg_2086" s="T6">Wiege.[NOM]</ta>
            <ta e="T8" id="Seg_2087" s="T7">gemacht.werden-EP-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T9" id="Seg_2088" s="T8">legen-PRS-3PL</ta>
            <ta e="T10" id="Seg_2089" s="T9">dann</ta>
            <ta e="T11" id="Seg_2090" s="T10">fünf-ORD</ta>
            <ta e="T12" id="Seg_2091" s="T11">Q</ta>
            <ta e="T13" id="Seg_2092" s="T12">sieben-ORD</ta>
            <ta e="T14" id="Seg_2093" s="T13">Q</ta>
            <ta e="T15" id="Seg_2094" s="T14">Tag-3SG-DAT/LOC</ta>
            <ta e="T16" id="Seg_2095" s="T15">Nabel-3SG.[NOM]</ta>
            <ta e="T17" id="Seg_2096" s="T16">fallen-PRS.[3SG]</ta>
            <ta e="T18" id="Seg_2097" s="T17">jenes</ta>
            <ta e="T19" id="Seg_2098" s="T18">Nabel-3SG.[NOM]</ta>
            <ta e="T20" id="Seg_2099" s="T19">fallen-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T21" id="Seg_2100" s="T20">MOD</ta>
            <ta e="T22" id="Seg_2101" s="T21">jenes</ta>
            <ta e="T23" id="Seg_2102" s="T22">Kind-ACC</ta>
            <ta e="T24" id="Seg_2103" s="T23">MOD</ta>
            <ta e="T25" id="Seg_2104" s="T24">taufen-PRS-3PL</ta>
            <ta e="T26" id="Seg_2105" s="T25">dann</ta>
            <ta e="T27" id="Seg_2106" s="T26">dieses</ta>
            <ta e="T28" id="Seg_2107" s="T27">Kind.[NOM]</ta>
            <ta e="T29" id="Seg_2108" s="T28">Nabel-3SG.[NOM]</ta>
            <ta e="T30" id="Seg_2109" s="T29">fallen-TEMP-3SG</ta>
            <ta e="T31" id="Seg_2110" s="T30">taufen-PRS-3PL</ta>
            <ta e="T32" id="Seg_2111" s="T31">Kind-ACC</ta>
            <ta e="T33" id="Seg_2112" s="T32">jenes</ta>
            <ta e="T34" id="Seg_2113" s="T33">taufen-CVB.SEQ</ta>
            <ta e="T35" id="Seg_2114" s="T34">Tundra-DAT/LOC</ta>
            <ta e="T36" id="Seg_2115" s="T35">aber</ta>
            <ta e="T37" id="Seg_2116" s="T36">weit.weg</ta>
            <ta e="T38" id="Seg_2117" s="T37">dieses</ta>
            <ta e="T39" id="Seg_2118" s="T38">Geistlicher.[NOM]</ta>
            <ta e="T40" id="Seg_2119" s="T39">NEG.EX</ta>
            <ta e="T41" id="Seg_2120" s="T40">Ort-3SG-DAT/LOC</ta>
            <ta e="T42" id="Seg_2121" s="T41">wer.[NOM]</ta>
            <ta e="T43" id="Seg_2122" s="T42">taufen-CVB.SIM</ta>
            <ta e="T44" id="Seg_2123" s="T43">können-PRS.[3SG]</ta>
            <ta e="T45" id="Seg_2124" s="T44">Gebet-ACC</ta>
            <ta e="T46" id="Seg_2125" s="T45">beten-PTCP.PRS</ta>
            <ta e="T47" id="Seg_2126" s="T46">Wort-ACC</ta>
            <ta e="T48" id="Seg_2127" s="T47">wissen-PRS.[3SG]</ta>
            <ta e="T49" id="Seg_2128" s="T48">taufen-PTCP.PRS</ta>
            <ta e="T50" id="Seg_2129" s="T49">beten-PTCP.PRS</ta>
            <ta e="T51" id="Seg_2130" s="T50">Wort-ACC</ta>
            <ta e="T52" id="Seg_2131" s="T51">wissen-PTCP.PRS</ta>
            <ta e="T53" id="Seg_2132" s="T52">Mensch.[NOM]</ta>
            <ta e="T54" id="Seg_2133" s="T53">Großmutter-3SG.[NOM]</ta>
            <ta e="T55" id="Seg_2134" s="T54">Q</ta>
            <ta e="T56" id="Seg_2135" s="T55">Großvater-3SG.[NOM]</ta>
            <ta e="T57" id="Seg_2136" s="T56">Q</ta>
            <ta e="T58" id="Seg_2137" s="T57">jenes.[NOM]</ta>
            <ta e="T59" id="Seg_2138" s="T58">taufen-PRS.[3SG]</ta>
            <ta e="T60" id="Seg_2139" s="T59">Tundra-DAT/LOC</ta>
            <ta e="T61" id="Seg_2140" s="T60">Tundra-DAT/LOC</ta>
            <ta e="T62" id="Seg_2141" s="T61">dann</ta>
            <ta e="T63" id="Seg_2142" s="T62">dieses</ta>
            <ta e="T64" id="Seg_2143" s="T63">taufen-CVB.SEQ</ta>
            <ta e="T65" id="Seg_2144" s="T64">gut</ta>
            <ta e="T66" id="Seg_2145" s="T65">Wasser-3SG.[NOM]</ta>
            <ta e="T67" id="Seg_2146" s="T66">Ikone-ACC</ta>
            <ta e="T68" id="Seg_2147" s="T67">legen-CVB.SEQ-3PL</ta>
            <ta e="T69" id="Seg_2148" s="T68">Wasser-ACC</ta>
            <ta e="T70" id="Seg_2149" s="T69">gießen-PRS-3PL</ta>
            <ta e="T71" id="Seg_2150" s="T70">Gefäß-DAT/LOC</ta>
            <ta e="T72" id="Seg_2151" s="T71">dann</ta>
            <ta e="T73" id="Seg_2152" s="T72">MOD</ta>
            <ta e="T74" id="Seg_2153" s="T73">Ikone-ACC</ta>
            <ta e="T75" id="Seg_2154" s="T74">legen</ta>
            <ta e="T76" id="Seg_2155" s="T75">stecken-PRS-3PL</ta>
            <ta e="T77" id="Seg_2156" s="T76">dorthin</ta>
            <ta e="T78" id="Seg_2157" s="T77">dann</ta>
            <ta e="T79" id="Seg_2158" s="T78">Gott.[NOM]</ta>
            <ta e="T80" id="Seg_2159" s="T79">Wasser-3SG.[NOM]</ta>
            <ta e="T81" id="Seg_2160" s="T80">sein-PRS.[3SG]</ta>
            <ta e="T82" id="Seg_2161" s="T81">jenes</ta>
            <ta e="T83" id="Seg_2162" s="T82">Wasser-2SG.[NOM]</ta>
            <ta e="T84" id="Seg_2163" s="T83">jenes</ta>
            <ta e="T85" id="Seg_2164" s="T84">heilig.[NOM]</ta>
            <ta e="T86" id="Seg_2165" s="T85">Gott.[NOM]</ta>
            <ta e="T87" id="Seg_2166" s="T86">Wasser-3SG-DAT/LOC</ta>
            <ta e="T88" id="Seg_2167" s="T87">abwaschen-PRS-3PL</ta>
            <ta e="T89" id="Seg_2168" s="T88">dieses</ta>
            <ta e="T90" id="Seg_2169" s="T89">Kind-ACC</ta>
            <ta e="T91" id="Seg_2170" s="T90">Kopf-DIM-3SG-ACC</ta>
            <ta e="T92" id="Seg_2171" s="T91">drei-MLTP</ta>
            <ta e="T93" id="Seg_2172" s="T92">Mal.[NOM]</ta>
            <ta e="T94" id="Seg_2173" s="T93">Vater.[NOM]</ta>
            <ta e="T96" id="Seg_2174" s="T95">glücklich.[NOM]</ta>
            <ta e="T97" id="Seg_2175" s="T96">leben.[IMP.2SG]</ta>
            <ta e="T98" id="Seg_2176" s="T97">sagen-CVB.SEQ-3PL</ta>
            <ta e="T99" id="Seg_2177" s="T98">glücklich.[NOM]</ta>
            <ta e="T100" id="Seg_2178" s="T99">entstehen.[IMP.2SG]</ta>
            <ta e="T101" id="Seg_2179" s="T100">sagen-CVB.SEQ-3PL</ta>
            <ta e="T102" id="Seg_2180" s="T101">solch.[NOM]</ta>
            <ta e="T103" id="Seg_2181" s="T102">Jäger.[NOM]</ta>
            <ta e="T104" id="Seg_2182" s="T103">sein.[IMP.2SG]</ta>
            <ta e="T105" id="Seg_2183" s="T104">solch</ta>
            <ta e="T106" id="Seg_2184" s="T105">Nahrung-AG.[NOM]</ta>
            <ta e="T107" id="Seg_2185" s="T106">sein.[IMP.2SG]</ta>
            <ta e="T108" id="Seg_2186" s="T107">sagen-CVB.SEQ-3PL</ta>
            <ta e="T109" id="Seg_2187" s="T108">dann</ta>
            <ta e="T110" id="Seg_2188" s="T109">aber</ta>
            <ta e="T111" id="Seg_2189" s="T110">was.für.ein</ta>
            <ta e="T112" id="Seg_2190" s="T111">Monat-DAT/LOC</ta>
            <ta e="T113" id="Seg_2191" s="T112">jenes</ta>
            <ta e="T114" id="Seg_2192" s="T113">Kind.[NOM]</ta>
            <ta e="T115" id="Seg_2193" s="T114">geboren.werden-PST2-3SG</ta>
            <ta e="T116" id="Seg_2194" s="T115">unser</ta>
            <ta e="T117" id="Seg_2195" s="T116">dieses</ta>
            <ta e="T118" id="Seg_2196" s="T117">spielen-PTCP.PRS</ta>
            <ta e="T119" id="Seg_2197" s="T118">Kind-1PL.[NOM]</ta>
            <ta e="T120" id="Seg_2198" s="T119">dieses</ta>
            <ta e="T121" id="Seg_2199" s="T120">spielen-PTCP.PRS-1PL-DAT/LOC</ta>
            <ta e="T122" id="Seg_2200" s="T121">Februar.[NOM]</ta>
            <ta e="T123" id="Seg_2201" s="T122">Monat-DAT/LOC</ta>
            <ta e="T124" id="Seg_2202" s="T123">geboren.werden-PST1-3SG</ta>
            <ta e="T125" id="Seg_2203" s="T124">dieses</ta>
            <ta e="T126" id="Seg_2204" s="T125">Februar.[NOM]</ta>
            <ta e="T127" id="Seg_2205" s="T126">Monat-DAT/LOC</ta>
            <ta e="T128" id="Seg_2206" s="T127">aber</ta>
            <ta e="T129" id="Seg_2207" s="T128">Ikone-PL.[NOM]</ta>
            <ta e="T130" id="Seg_2208" s="T129">es.gibt-3PL</ta>
            <ta e="T131" id="Seg_2209" s="T130">heilig.[NOM]</ta>
            <ta e="T132" id="Seg_2210" s="T131">Ikone-PL.[NOM]</ta>
            <ta e="T133" id="Seg_2211" s="T132">heilig-PL.[NOM]</ta>
            <ta e="T134" id="Seg_2212" s="T133">Aksinja</ta>
            <ta e="T135" id="Seg_2213" s="T134">sagen-CVB.SEQ</ta>
            <ta e="T136" id="Seg_2214" s="T135">Ikone-PL.[NOM]</ta>
            <ta e="T137" id="Seg_2215" s="T136">dann</ta>
            <ta e="T138" id="Seg_2216" s="T137">Mädchen.[NOM]</ta>
            <ta e="T139" id="Seg_2217" s="T138">Kind.[NOM]</ta>
            <ta e="T140" id="Seg_2218" s="T139">sein-PST2-3SG</ta>
            <ta e="T141" id="Seg_2219" s="T140">MOD</ta>
            <ta e="T142" id="Seg_2220" s="T141">Aksinja.[NOM]</ta>
            <ta e="T143" id="Seg_2221" s="T142">sagen-CVB.SEQ</ta>
            <ta e="T144" id="Seg_2222" s="T143">Name.[NOM]</ta>
            <ta e="T145" id="Seg_2223" s="T144">sein-PST2.[3SG]</ta>
            <ta e="T146" id="Seg_2224" s="T145">sein-PST2.[3SG]</ta>
            <ta e="T147" id="Seg_2225" s="T146">noch</ta>
            <ta e="T148" id="Seg_2226" s="T147">es.gibt-3PL</ta>
            <ta e="T149" id="Seg_2227" s="T148">Stresinskij</ta>
            <ta e="T150" id="Seg_2228" s="T149">sagen-CVB.SEQ</ta>
            <ta e="T151" id="Seg_2229" s="T150">Ikone-PL.[NOM]</ta>
            <ta e="T152" id="Seg_2230" s="T151">dann</ta>
            <ta e="T153" id="Seg_2231" s="T152">aber</ta>
            <ta e="T154" id="Seg_2232" s="T153">MOD</ta>
            <ta e="T155" id="Seg_2233" s="T154">dieses</ta>
            <ta e="T156" id="Seg_2234" s="T155">Februar</ta>
            <ta e="T157" id="Seg_2235" s="T156">Monat-DAT/LOC</ta>
            <ta e="T158" id="Seg_2236" s="T157">dann</ta>
            <ta e="T159" id="Seg_2237" s="T158">EMPH</ta>
            <ta e="T160" id="Seg_2238" s="T159">jenes.[NOM]</ta>
            <ta e="T161" id="Seg_2239" s="T160">wegen</ta>
            <ta e="T162" id="Seg_2240" s="T161">MOD</ta>
            <ta e="T163" id="Seg_2241" s="T162">dieses</ta>
            <ta e="T164" id="Seg_2242" s="T163">Kind-3PL-ACC</ta>
            <ta e="T165" id="Seg_2243" s="T164">Semjon.[NOM]</ta>
            <ta e="T166" id="Seg_2244" s="T165">sagen-CVB.SEQ</ta>
            <ta e="T167" id="Seg_2245" s="T166">nennen-PRS-3PL</ta>
            <ta e="T168" id="Seg_2246" s="T167">Hemeeche.[NOM]</ta>
            <ta e="T169" id="Seg_2247" s="T168">dolganisch-SIM</ta>
            <ta e="T170" id="Seg_2248" s="T169">Hemeeche.[NOM]</ta>
            <ta e="T171" id="Seg_2249" s="T170">sagen-CVB.SEQ</ta>
            <ta e="T172" id="Seg_2250" s="T171">nennen-PRS-3PL</ta>
            <ta e="T173" id="Seg_2251" s="T172">Junge.[NOM]</ta>
            <ta e="T174" id="Seg_2252" s="T173">Kind.[NOM]</ta>
            <ta e="T175" id="Seg_2253" s="T174">werden-PST2.[3SG]</ta>
            <ta e="T176" id="Seg_2254" s="T175">geboren.werden-PST2-3SG</ta>
            <ta e="T177" id="Seg_2255" s="T176">sagen-CVB.SEQ</ta>
            <ta e="T178" id="Seg_2256" s="T177">jenes</ta>
            <ta e="T179" id="Seg_2257" s="T178">Name.[NOM]</ta>
            <ta e="T180" id="Seg_2258" s="T179">Kennzeichen.[NOM]</ta>
            <ta e="T181" id="Seg_2259" s="T180">geben-PRS-3PL</ta>
            <ta e="T182" id="Seg_2260" s="T181">taufen-PTCP.PST</ta>
            <ta e="T183" id="Seg_2261" s="T182">Name-3SG.[NOM]</ta>
            <ta e="T184" id="Seg_2262" s="T183">Hemeeche.[NOM]</ta>
            <ta e="T185" id="Seg_2263" s="T184">so</ta>
            <ta e="T186" id="Seg_2264" s="T185">machen-CVB.SEQ</ta>
            <ta e="T187" id="Seg_2265" s="T186">Semjon-EP-2SG.[NOM]</ta>
            <ta e="T188" id="Seg_2266" s="T187">Mutter-3SG-DAT/LOC</ta>
            <ta e="T189" id="Seg_2267" s="T188">geben-PRS-3PL</ta>
            <ta e="T190" id="Seg_2268" s="T189">dieses</ta>
            <ta e="T191" id="Seg_2269" s="T190">Semjon.[NOM]</ta>
            <ta e="T192" id="Seg_2270" s="T191">Mutter-3SG.[NOM]</ta>
            <ta e="T193" id="Seg_2271" s="T192">MOD</ta>
            <ta e="T194" id="Seg_2272" s="T193">dieses.EMPH-ABL</ta>
            <ta e="T195" id="Seg_2273" s="T194">selbst-3SG-GEN</ta>
            <ta e="T196" id="Seg_2274" s="T195">Name-3SG-ACC</ta>
            <ta e="T197" id="Seg_2275" s="T196">nennen-NEG-3PL</ta>
            <ta e="T198" id="Seg_2276" s="T197">vor.langer.Zeit-ADJZ</ta>
            <ta e="T199" id="Seg_2277" s="T198">Zeit-DAT/LOC</ta>
            <ta e="T200" id="Seg_2278" s="T199">Achtung-DAT/LOC</ta>
            <ta e="T201" id="Seg_2279" s="T200">nennen-HAB-3PL</ta>
            <ta e="T202" id="Seg_2280" s="T201">groß.[NOM]</ta>
            <ta e="T203" id="Seg_2281" s="T202">Sohn-3SG-INSTR</ta>
            <ta e="T204" id="Seg_2282" s="T203">Q</ta>
            <ta e="T205" id="Seg_2283" s="T204">groß.[NOM]</ta>
            <ta e="T206" id="Seg_2284" s="T205">Tochter-3SG-INSTR</ta>
            <ta e="T207" id="Seg_2285" s="T206">Q</ta>
            <ta e="T208" id="Seg_2286" s="T207">jenes.[NOM]</ta>
            <ta e="T209" id="Seg_2287" s="T208">wegen</ta>
            <ta e="T210" id="Seg_2288" s="T209">Hemeeche.[NOM]</ta>
            <ta e="T211" id="Seg_2289" s="T210">Mutter-3SG.[NOM]</ta>
            <ta e="T212" id="Seg_2290" s="T211">sein-PRS.[3SG]</ta>
            <ta e="T213" id="Seg_2291" s="T212">Vater-3SG.[NOM]</ta>
            <ta e="T214" id="Seg_2292" s="T213">aber</ta>
            <ta e="T215" id="Seg_2293" s="T214">Hemeeche.[NOM]</ta>
            <ta e="T216" id="Seg_2294" s="T215">Vater-3SG.[NOM]</ta>
            <ta e="T217" id="Seg_2295" s="T216">werden-PRS.[3SG]</ta>
            <ta e="T218" id="Seg_2296" s="T217">so</ta>
            <ta e="T219" id="Seg_2297" s="T218">machen-CVB.SEQ</ta>
            <ta e="T220" id="Seg_2298" s="T219">jenes.[NOM]</ta>
            <ta e="T221" id="Seg_2299" s="T220">ähnlich</ta>
            <ta e="T222" id="Seg_2300" s="T221">nennen-CVB.SEQ-3PL</ta>
            <ta e="T223" id="Seg_2301" s="T222">Feier.[NOM]</ta>
            <ta e="T224" id="Seg_2302" s="T223">machen-CVB.SEQ-3PL</ta>
            <ta e="T225" id="Seg_2303" s="T224">feiern-CVB.SEQ-%%-CVB.SEQ</ta>
            <ta e="T226" id="Seg_2304" s="T225">dieses</ta>
            <ta e="T227" id="Seg_2305" s="T226">Leute-ACC</ta>
            <ta e="T228" id="Seg_2306" s="T227">ganz-3SG-ACC</ta>
            <ta e="T229" id="Seg_2307" s="T228">einladen-CVB.SEQ-3PL</ta>
            <ta e="T230" id="Seg_2308" s="T229">Kind.[NOM]</ta>
            <ta e="T231" id="Seg_2309" s="T230">geboren.werden-PTCP.PST</ta>
            <ta e="T232" id="Seg_2310" s="T231">Feier-3SG.[NOM]</ta>
            <ta e="T233" id="Seg_2311" s="T232">sein-PRS.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_2312" s="T1">этот</ta>
            <ta e="T3" id="Seg_2313" s="T2">ребенок-ACC</ta>
            <ta e="T4" id="Seg_2314" s="T3">три-ORD</ta>
            <ta e="T5" id="Seg_2315" s="T4">день-3SG-DAT/LOC</ta>
            <ta e="T6" id="Seg_2316" s="T5">тот</ta>
            <ta e="T7" id="Seg_2317" s="T6">колыбель.[NOM]</ta>
            <ta e="T8" id="Seg_2318" s="T7">делаться-EP-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T9" id="Seg_2319" s="T8">класть-PRS-3PL</ta>
            <ta e="T10" id="Seg_2320" s="T9">потом</ta>
            <ta e="T11" id="Seg_2321" s="T10">пять-ORD</ta>
            <ta e="T12" id="Seg_2322" s="T11">Q</ta>
            <ta e="T13" id="Seg_2323" s="T12">семь-ORD</ta>
            <ta e="T14" id="Seg_2324" s="T13">Q</ta>
            <ta e="T15" id="Seg_2325" s="T14">день-3SG-DAT/LOC</ta>
            <ta e="T16" id="Seg_2326" s="T15">пуп-3SG.[NOM]</ta>
            <ta e="T17" id="Seg_2327" s="T16">падать-PRS.[3SG]</ta>
            <ta e="T18" id="Seg_2328" s="T17">тот</ta>
            <ta e="T19" id="Seg_2329" s="T18">пуп-3SG.[NOM]</ta>
            <ta e="T20" id="Seg_2330" s="T19">падать-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T21" id="Seg_2331" s="T20">MOD</ta>
            <ta e="T22" id="Seg_2332" s="T21">тот</ta>
            <ta e="T23" id="Seg_2333" s="T22">ребенок-ACC</ta>
            <ta e="T24" id="Seg_2334" s="T23">MOD</ta>
            <ta e="T25" id="Seg_2335" s="T24">крестить-PRS-3PL</ta>
            <ta e="T26" id="Seg_2336" s="T25">потом</ta>
            <ta e="T27" id="Seg_2337" s="T26">этот</ta>
            <ta e="T28" id="Seg_2338" s="T27">ребенок.[NOM]</ta>
            <ta e="T29" id="Seg_2339" s="T28">пуп-3SG.[NOM]</ta>
            <ta e="T30" id="Seg_2340" s="T29">падать-TEMP-3SG</ta>
            <ta e="T31" id="Seg_2341" s="T30">крестить-PRS-3PL</ta>
            <ta e="T32" id="Seg_2342" s="T31">ребенок-ACC</ta>
            <ta e="T33" id="Seg_2343" s="T32">тот</ta>
            <ta e="T34" id="Seg_2344" s="T33">крестить-CVB.SEQ</ta>
            <ta e="T35" id="Seg_2345" s="T34">тундра-DAT/LOC</ta>
            <ta e="T36" id="Seg_2346" s="T35">однако</ta>
            <ta e="T37" id="Seg_2347" s="T36">далеко</ta>
            <ta e="T38" id="Seg_2348" s="T37">этот</ta>
            <ta e="T39" id="Seg_2349" s="T38">священник.[NOM]</ta>
            <ta e="T40" id="Seg_2350" s="T39">NEG.EX</ta>
            <ta e="T41" id="Seg_2351" s="T40">место-3SG-DAT/LOC</ta>
            <ta e="T42" id="Seg_2352" s="T41">кто.[NOM]</ta>
            <ta e="T43" id="Seg_2353" s="T42">крестить-CVB.SIM</ta>
            <ta e="T44" id="Seg_2354" s="T43">мочь-PRS.[3SG]</ta>
            <ta e="T45" id="Seg_2355" s="T44">молитва-ACC</ta>
            <ta e="T46" id="Seg_2356" s="T45">молиться-PTCP.PRS</ta>
            <ta e="T47" id="Seg_2357" s="T46">слово-ACC</ta>
            <ta e="T48" id="Seg_2358" s="T47">знать-PRS.[3SG]</ta>
            <ta e="T49" id="Seg_2359" s="T48">крестить-PTCP.PRS</ta>
            <ta e="T50" id="Seg_2360" s="T49">молиться-PTCP.PRS</ta>
            <ta e="T51" id="Seg_2361" s="T50">слово-ACC</ta>
            <ta e="T52" id="Seg_2362" s="T51">знать-PTCP.PRS</ta>
            <ta e="T53" id="Seg_2363" s="T52">человек.[NOM]</ta>
            <ta e="T54" id="Seg_2364" s="T53">бабушка-3SG.[NOM]</ta>
            <ta e="T55" id="Seg_2365" s="T54">Q</ta>
            <ta e="T56" id="Seg_2366" s="T55">дедушка-3SG.[NOM]</ta>
            <ta e="T57" id="Seg_2367" s="T56">Q</ta>
            <ta e="T58" id="Seg_2368" s="T57">тот.[NOM]</ta>
            <ta e="T59" id="Seg_2369" s="T58">крестить-PRS.[3SG]</ta>
            <ta e="T60" id="Seg_2370" s="T59">тундра-DAT/LOC</ta>
            <ta e="T61" id="Seg_2371" s="T60">тундра-DAT/LOC</ta>
            <ta e="T62" id="Seg_2372" s="T61">тогда</ta>
            <ta e="T63" id="Seg_2373" s="T62">этот</ta>
            <ta e="T64" id="Seg_2374" s="T63">крестить-CVB.SEQ</ta>
            <ta e="T65" id="Seg_2375" s="T64">нормальный</ta>
            <ta e="T66" id="Seg_2376" s="T65">вода-3SG.[NOM]</ta>
            <ta e="T67" id="Seg_2377" s="T66">икона-ACC</ta>
            <ta e="T68" id="Seg_2378" s="T67">класть-CVB.SEQ-3PL</ta>
            <ta e="T69" id="Seg_2379" s="T68">вода-ACC</ta>
            <ta e="T70" id="Seg_2380" s="T69">лить-PRS-3PL</ta>
            <ta e="T71" id="Seg_2381" s="T70">сосуд-DAT/LOC</ta>
            <ta e="T72" id="Seg_2382" s="T71">потом</ta>
            <ta e="T73" id="Seg_2383" s="T72">MOD</ta>
            <ta e="T74" id="Seg_2384" s="T73">икона-ACC</ta>
            <ta e="T75" id="Seg_2385" s="T74">класть</ta>
            <ta e="T76" id="Seg_2386" s="T75">вставлять-PRS-3PL</ta>
            <ta e="T77" id="Seg_2387" s="T76">туда</ta>
            <ta e="T78" id="Seg_2388" s="T77">тогда</ta>
            <ta e="T79" id="Seg_2389" s="T78">Бог.[NOM]</ta>
            <ta e="T80" id="Seg_2390" s="T79">вода-3SG.[NOM]</ta>
            <ta e="T81" id="Seg_2391" s="T80">быть-PRS.[3SG]</ta>
            <ta e="T82" id="Seg_2392" s="T81">тот</ta>
            <ta e="T83" id="Seg_2393" s="T82">вода-2SG.[NOM]</ta>
            <ta e="T84" id="Seg_2394" s="T83">тот</ta>
            <ta e="T85" id="Seg_2395" s="T84">святой.[NOM]</ta>
            <ta e="T86" id="Seg_2396" s="T85">Бог.[NOM]</ta>
            <ta e="T87" id="Seg_2397" s="T86">вода-3SG-DAT/LOC</ta>
            <ta e="T88" id="Seg_2398" s="T87">обмыть-PRS-3PL</ta>
            <ta e="T89" id="Seg_2399" s="T88">этот</ta>
            <ta e="T90" id="Seg_2400" s="T89">ребенок-ACC</ta>
            <ta e="T91" id="Seg_2401" s="T90">голова-DIM-3SG-ACC</ta>
            <ta e="T92" id="Seg_2402" s="T91">три-MLTP</ta>
            <ta e="T93" id="Seg_2403" s="T92">раз.[NOM]</ta>
            <ta e="T94" id="Seg_2404" s="T93">отец.[NOM]</ta>
            <ta e="T96" id="Seg_2405" s="T95">счастливый.[NOM]</ta>
            <ta e="T97" id="Seg_2406" s="T96">жить.[IMP.2SG]</ta>
            <ta e="T98" id="Seg_2407" s="T97">говорить-CVB.SEQ-3PL</ta>
            <ta e="T99" id="Seg_2408" s="T98">счастливый.[NOM]</ta>
            <ta e="T100" id="Seg_2409" s="T99">создаваться.[IMP.2SG]</ta>
            <ta e="T101" id="Seg_2410" s="T100">говорить-CVB.SEQ-3PL</ta>
            <ta e="T102" id="Seg_2411" s="T101">такой.[NOM]</ta>
            <ta e="T103" id="Seg_2412" s="T102">охотник.[NOM]</ta>
            <ta e="T104" id="Seg_2413" s="T103">быть.[IMP.2SG]</ta>
            <ta e="T105" id="Seg_2414" s="T104">такой</ta>
            <ta e="T106" id="Seg_2415" s="T105">пища-AG.[NOM]</ta>
            <ta e="T107" id="Seg_2416" s="T106">быть.[IMP.2SG]</ta>
            <ta e="T108" id="Seg_2417" s="T107">говорить-CVB.SEQ-3PL</ta>
            <ta e="T109" id="Seg_2418" s="T108">потом</ta>
            <ta e="T110" id="Seg_2419" s="T109">однако</ta>
            <ta e="T111" id="Seg_2420" s="T110">какой</ta>
            <ta e="T112" id="Seg_2421" s="T111">месяц-DAT/LOC</ta>
            <ta e="T113" id="Seg_2422" s="T112">тот</ta>
            <ta e="T114" id="Seg_2423" s="T113">ребенок.[NOM]</ta>
            <ta e="T115" id="Seg_2424" s="T114">родиться-PST2-3SG</ta>
            <ta e="T116" id="Seg_2425" s="T115">наш</ta>
            <ta e="T117" id="Seg_2426" s="T116">этот</ta>
            <ta e="T118" id="Seg_2427" s="T117">играть-PTCP.PRS</ta>
            <ta e="T119" id="Seg_2428" s="T118">ребенок-1PL.[NOM]</ta>
            <ta e="T120" id="Seg_2429" s="T119">этот</ta>
            <ta e="T121" id="Seg_2430" s="T120">играть-PTCP.PRS-1PL-DAT/LOC</ta>
            <ta e="T122" id="Seg_2431" s="T121">февраль.[NOM]</ta>
            <ta e="T123" id="Seg_2432" s="T122">месяц-DAT/LOC</ta>
            <ta e="T124" id="Seg_2433" s="T123">родиться-PST1-3SG</ta>
            <ta e="T125" id="Seg_2434" s="T124">этот</ta>
            <ta e="T126" id="Seg_2435" s="T125">февраль.[NOM]</ta>
            <ta e="T127" id="Seg_2436" s="T126">месяц-DAT/LOC</ta>
            <ta e="T128" id="Seg_2437" s="T127">однако</ta>
            <ta e="T129" id="Seg_2438" s="T128">икона-PL.[NOM]</ta>
            <ta e="T130" id="Seg_2439" s="T129">есть-3PL</ta>
            <ta e="T131" id="Seg_2440" s="T130">святой.[NOM]</ta>
            <ta e="T132" id="Seg_2441" s="T131">икона-PL.[NOM]</ta>
            <ta e="T133" id="Seg_2442" s="T132">святой-PL.[NOM]</ta>
            <ta e="T134" id="Seg_2443" s="T133">Аксинья</ta>
            <ta e="T135" id="Seg_2444" s="T134">говорить-CVB.SEQ</ta>
            <ta e="T136" id="Seg_2445" s="T135">икона-PL.[NOM]</ta>
            <ta e="T137" id="Seg_2446" s="T136">тогда</ta>
            <ta e="T138" id="Seg_2447" s="T137">девушка.[NOM]</ta>
            <ta e="T139" id="Seg_2448" s="T138">ребенок.[NOM]</ta>
            <ta e="T140" id="Seg_2449" s="T139">быть-PST2-3SG</ta>
            <ta e="T141" id="Seg_2450" s="T140">MOD</ta>
            <ta e="T142" id="Seg_2451" s="T141">Аксинья.[NOM]</ta>
            <ta e="T143" id="Seg_2452" s="T142">говорить-CVB.SEQ</ta>
            <ta e="T144" id="Seg_2453" s="T143">имя.[NOM]</ta>
            <ta e="T145" id="Seg_2454" s="T144">быть-PST2.[3SG]</ta>
            <ta e="T146" id="Seg_2455" s="T145">быть-PST2.[3SG]</ta>
            <ta e="T147" id="Seg_2456" s="T146">еще</ta>
            <ta e="T148" id="Seg_2457" s="T147">есть-3PL</ta>
            <ta e="T149" id="Seg_2458" s="T148">Стресинский</ta>
            <ta e="T150" id="Seg_2459" s="T149">говорить-CVB.SEQ</ta>
            <ta e="T151" id="Seg_2460" s="T150">икона-PL.[NOM]</ta>
            <ta e="T152" id="Seg_2461" s="T151">тогда</ta>
            <ta e="T153" id="Seg_2462" s="T152">однако</ta>
            <ta e="T154" id="Seg_2463" s="T153">MOD</ta>
            <ta e="T155" id="Seg_2464" s="T154">этот</ta>
            <ta e="T156" id="Seg_2465" s="T155">февраль</ta>
            <ta e="T157" id="Seg_2466" s="T156">месяц-DAT/LOC</ta>
            <ta e="T158" id="Seg_2467" s="T157">тогда</ta>
            <ta e="T159" id="Seg_2468" s="T158">EMPH</ta>
            <ta e="T160" id="Seg_2469" s="T159">тот.[NOM]</ta>
            <ta e="T161" id="Seg_2470" s="T160">из_за</ta>
            <ta e="T162" id="Seg_2471" s="T161">MOD</ta>
            <ta e="T163" id="Seg_2472" s="T162">этот</ta>
            <ta e="T164" id="Seg_2473" s="T163">ребенок-3PL-ACC</ta>
            <ta e="T165" id="Seg_2474" s="T164">Семен.[NOM]</ta>
            <ta e="T166" id="Seg_2475" s="T165">говорить-CVB.SEQ</ta>
            <ta e="T167" id="Seg_2476" s="T166">назвать-PRS-3PL</ta>
            <ta e="T168" id="Seg_2477" s="T167">Хэмээчэ.[NOM]</ta>
            <ta e="T169" id="Seg_2478" s="T168">долганский-SIM</ta>
            <ta e="T170" id="Seg_2479" s="T169">Хэмээчэ.[NOM]</ta>
            <ta e="T171" id="Seg_2480" s="T170">говорить-CVB.SEQ</ta>
            <ta e="T172" id="Seg_2481" s="T171">назвать-PRS-3PL</ta>
            <ta e="T173" id="Seg_2482" s="T172">мальчик.[NOM]</ta>
            <ta e="T174" id="Seg_2483" s="T173">ребенок.[NOM]</ta>
            <ta e="T175" id="Seg_2484" s="T174">становиться-PST2.[3SG]</ta>
            <ta e="T176" id="Seg_2485" s="T175">родиться-PST2-3SG</ta>
            <ta e="T177" id="Seg_2486" s="T176">говорить-CVB.SEQ</ta>
            <ta e="T178" id="Seg_2487" s="T177">тот</ta>
            <ta e="T179" id="Seg_2488" s="T178">имя.[NOM]</ta>
            <ta e="T180" id="Seg_2489" s="T179">метка.[NOM]</ta>
            <ta e="T181" id="Seg_2490" s="T180">давать-PRS-3PL</ta>
            <ta e="T182" id="Seg_2491" s="T181">крестить-PTCP.PST</ta>
            <ta e="T183" id="Seg_2492" s="T182">имя-3SG.[NOM]</ta>
            <ta e="T184" id="Seg_2493" s="T183">Хэмээчэ.[NOM]</ta>
            <ta e="T185" id="Seg_2494" s="T184">так</ta>
            <ta e="T186" id="Seg_2495" s="T185">делать-CVB.SEQ</ta>
            <ta e="T187" id="Seg_2496" s="T186">Семен-EP-2SG.[NOM]</ta>
            <ta e="T188" id="Seg_2497" s="T187">мать-3SG-DAT/LOC</ta>
            <ta e="T189" id="Seg_2498" s="T188">давать-PRS-3PL</ta>
            <ta e="T190" id="Seg_2499" s="T189">этот</ta>
            <ta e="T191" id="Seg_2500" s="T190">Семен.[NOM]</ta>
            <ta e="T192" id="Seg_2501" s="T191">мать-3SG.[NOM]</ta>
            <ta e="T193" id="Seg_2502" s="T192">MOD</ta>
            <ta e="T194" id="Seg_2503" s="T193">этот.EMPH-ABL</ta>
            <ta e="T195" id="Seg_2504" s="T194">сам-3SG-GEN</ta>
            <ta e="T196" id="Seg_2505" s="T195">имя-3SG-ACC</ta>
            <ta e="T197" id="Seg_2506" s="T196">назвать-NEG-3PL</ta>
            <ta e="T198" id="Seg_2507" s="T197">давно-ADJZ</ta>
            <ta e="T199" id="Seg_2508" s="T198">время-DAT/LOC</ta>
            <ta e="T200" id="Seg_2509" s="T199">уважение-DAT/LOC</ta>
            <ta e="T201" id="Seg_2510" s="T200">назвать-HAB-3PL</ta>
            <ta e="T202" id="Seg_2511" s="T201">большой.[NOM]</ta>
            <ta e="T203" id="Seg_2512" s="T202">сын-3SG-INSTR</ta>
            <ta e="T204" id="Seg_2513" s="T203">Q</ta>
            <ta e="T205" id="Seg_2514" s="T204">большой.[NOM]</ta>
            <ta e="T206" id="Seg_2515" s="T205">дочь-3SG-INSTR</ta>
            <ta e="T207" id="Seg_2516" s="T206">Q</ta>
            <ta e="T208" id="Seg_2517" s="T207">тот.[NOM]</ta>
            <ta e="T209" id="Seg_2518" s="T208">из_за</ta>
            <ta e="T210" id="Seg_2519" s="T209">Хэмээчэ.[NOM]</ta>
            <ta e="T211" id="Seg_2520" s="T210">мать-3SG.[NOM]</ta>
            <ta e="T212" id="Seg_2521" s="T211">быть-PRS.[3SG]</ta>
            <ta e="T213" id="Seg_2522" s="T212">отец-3SG.[NOM]</ta>
            <ta e="T214" id="Seg_2523" s="T213">однако</ta>
            <ta e="T215" id="Seg_2524" s="T214">Хэмээчэ.[NOM]</ta>
            <ta e="T216" id="Seg_2525" s="T215">отец-3SG.[NOM]</ta>
            <ta e="T217" id="Seg_2526" s="T216">становиться-PRS.[3SG]</ta>
            <ta e="T218" id="Seg_2527" s="T217">так</ta>
            <ta e="T219" id="Seg_2528" s="T218">делать-CVB.SEQ</ta>
            <ta e="T220" id="Seg_2529" s="T219">тот.[NOM]</ta>
            <ta e="T221" id="Seg_2530" s="T220">подобно</ta>
            <ta e="T222" id="Seg_2531" s="T221">назвать-CVB.SEQ-3PL</ta>
            <ta e="T223" id="Seg_2532" s="T222">праздник.[NOM]</ta>
            <ta e="T224" id="Seg_2533" s="T223">делать-CVB.SEQ-3PL</ta>
            <ta e="T225" id="Seg_2534" s="T224">праздновать-CVB.SEQ-%%-CVB.SEQ</ta>
            <ta e="T226" id="Seg_2535" s="T225">этот</ta>
            <ta e="T227" id="Seg_2536" s="T226">люди-ACC</ta>
            <ta e="T228" id="Seg_2537" s="T227">целый-3SG-ACC</ta>
            <ta e="T229" id="Seg_2538" s="T228">зазывать-CVB.SEQ-3PL</ta>
            <ta e="T230" id="Seg_2539" s="T229">ребенок.[NOM]</ta>
            <ta e="T231" id="Seg_2540" s="T230">родиться-PTCP.PST</ta>
            <ta e="T232" id="Seg_2541" s="T231">праздник-3SG.[NOM]</ta>
            <ta e="T233" id="Seg_2542" s="T232">быть-PRS.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_2543" s="T1">dempro</ta>
            <ta e="T3" id="Seg_2544" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_2545" s="T3">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T5" id="Seg_2546" s="T4">n-n:poss-n:case</ta>
            <ta e="T6" id="Seg_2547" s="T5">dempro</ta>
            <ta e="T7" id="Seg_2548" s="T6">n.[n:case]</ta>
            <ta e="T8" id="Seg_2549" s="T7">v-v:(ins)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T9" id="Seg_2550" s="T8">v-v:tense-v:pred.pn</ta>
            <ta e="T10" id="Seg_2551" s="T9">adv</ta>
            <ta e="T11" id="Seg_2552" s="T10">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T12" id="Seg_2553" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_2554" s="T12">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T14" id="Seg_2555" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_2556" s="T14">n-n:poss-n:case</ta>
            <ta e="T16" id="Seg_2557" s="T15">n-n:(poss).[n:case]</ta>
            <ta e="T17" id="Seg_2558" s="T16">v-v:tense.[v:pred.pn]</ta>
            <ta e="T18" id="Seg_2559" s="T17">dempro</ta>
            <ta e="T19" id="Seg_2560" s="T18">n-n:(poss).[n:case]</ta>
            <ta e="T20" id="Seg_2561" s="T19">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T21" id="Seg_2562" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_2563" s="T21">dempro</ta>
            <ta e="T23" id="Seg_2564" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_2565" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_2566" s="T24">v-v:tense-v:pred.pn</ta>
            <ta e="T26" id="Seg_2567" s="T25">adv</ta>
            <ta e="T27" id="Seg_2568" s="T26">dempro</ta>
            <ta e="T28" id="Seg_2569" s="T27">n.[n:case]</ta>
            <ta e="T29" id="Seg_2570" s="T28">n-n:(poss).[n:case]</ta>
            <ta e="T30" id="Seg_2571" s="T29">v-v:mood-v:temp.pn</ta>
            <ta e="T31" id="Seg_2572" s="T30">v-v:tense-v:pred.pn</ta>
            <ta e="T32" id="Seg_2573" s="T31">n-n:case</ta>
            <ta e="T33" id="Seg_2574" s="T32">dempro</ta>
            <ta e="T34" id="Seg_2575" s="T33">v-v:cvb</ta>
            <ta e="T35" id="Seg_2576" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_2577" s="T35">ptcl</ta>
            <ta e="T37" id="Seg_2578" s="T36">adv</ta>
            <ta e="T38" id="Seg_2579" s="T37">dempro</ta>
            <ta e="T39" id="Seg_2580" s="T38">n.[n:case]</ta>
            <ta e="T40" id="Seg_2581" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_2582" s="T40">n-n:poss-n:case</ta>
            <ta e="T42" id="Seg_2583" s="T41">que.[pro:case]</ta>
            <ta e="T43" id="Seg_2584" s="T42">v-v:cvb</ta>
            <ta e="T44" id="Seg_2585" s="T43">v-v:tense.[v:pred.pn]</ta>
            <ta e="T45" id="Seg_2586" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_2587" s="T45">v-v:ptcp</ta>
            <ta e="T47" id="Seg_2588" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_2589" s="T47">v-v:tense.[v:pred.pn]</ta>
            <ta e="T49" id="Seg_2590" s="T48">v-v:ptcp</ta>
            <ta e="T50" id="Seg_2591" s="T49">v-v:ptcp</ta>
            <ta e="T51" id="Seg_2592" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_2593" s="T51">v-v:ptcp</ta>
            <ta e="T53" id="Seg_2594" s="T52">n.[n:case]</ta>
            <ta e="T54" id="Seg_2595" s="T53">n-n:(poss).[n:case]</ta>
            <ta e="T55" id="Seg_2596" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_2597" s="T55">n-n:(poss).[n:case]</ta>
            <ta e="T57" id="Seg_2598" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_2599" s="T57">dempro.[pro:case]</ta>
            <ta e="T59" id="Seg_2600" s="T58">v-v:tense.[v:pred.pn]</ta>
            <ta e="T60" id="Seg_2601" s="T59">n-n:case</ta>
            <ta e="T61" id="Seg_2602" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_2603" s="T61">adv</ta>
            <ta e="T63" id="Seg_2604" s="T62">dempro</ta>
            <ta e="T64" id="Seg_2605" s="T63">v-v:cvb</ta>
            <ta e="T65" id="Seg_2606" s="T64">adj</ta>
            <ta e="T66" id="Seg_2607" s="T65">n-n:(poss).[n:case]</ta>
            <ta e="T67" id="Seg_2608" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_2609" s="T67">v-v:cvb-v:pred.pn</ta>
            <ta e="T69" id="Seg_2610" s="T68">n-n:case</ta>
            <ta e="T70" id="Seg_2611" s="T69">v-v:tense-v:pred.pn</ta>
            <ta e="T71" id="Seg_2612" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_2613" s="T71">adv</ta>
            <ta e="T73" id="Seg_2614" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_2615" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_2616" s="T74">v</ta>
            <ta e="T76" id="Seg_2617" s="T75">v-v:tense-v:pred.pn</ta>
            <ta e="T77" id="Seg_2618" s="T76">adv</ta>
            <ta e="T78" id="Seg_2619" s="T77">adv</ta>
            <ta e="T79" id="Seg_2620" s="T78">n.[n:case]</ta>
            <ta e="T80" id="Seg_2621" s="T79">n-n:(poss).[n:case]</ta>
            <ta e="T81" id="Seg_2622" s="T80">v-v:tense.[v:pred.pn]</ta>
            <ta e="T82" id="Seg_2623" s="T81">dempro</ta>
            <ta e="T83" id="Seg_2624" s="T82">n-n:(poss).[n:case]</ta>
            <ta e="T84" id="Seg_2625" s="T83">dempro</ta>
            <ta e="T85" id="Seg_2626" s="T84">adj.[n:case]</ta>
            <ta e="T86" id="Seg_2627" s="T85">n.[n:case]</ta>
            <ta e="T87" id="Seg_2628" s="T86">n-n:poss-n:case</ta>
            <ta e="T88" id="Seg_2629" s="T87">v-v:tense-v:pred.pn</ta>
            <ta e="T89" id="Seg_2630" s="T88">dempro</ta>
            <ta e="T90" id="Seg_2631" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_2632" s="T90">n-n&gt;n-n:poss-n:case</ta>
            <ta e="T92" id="Seg_2633" s="T91">cardnum-cardnum&gt;adv</ta>
            <ta e="T93" id="Seg_2634" s="T92">n.[n:case]</ta>
            <ta e="T94" id="Seg_2635" s="T93">n.[n:case]</ta>
            <ta e="T96" id="Seg_2636" s="T95">adj.[n:case]</ta>
            <ta e="T97" id="Seg_2637" s="T96">v.[v:mood.pn]</ta>
            <ta e="T98" id="Seg_2638" s="T97">v-v:cvb-v:pred.pn</ta>
            <ta e="T99" id="Seg_2639" s="T98">adj.[n:case]</ta>
            <ta e="T100" id="Seg_2640" s="T99">v.[v:mood.pn]</ta>
            <ta e="T101" id="Seg_2641" s="T100">v-v:cvb-v:pred.pn</ta>
            <ta e="T102" id="Seg_2642" s="T101">dempro.[n:case]</ta>
            <ta e="T103" id="Seg_2643" s="T102">n.[n:case]</ta>
            <ta e="T104" id="Seg_2644" s="T103">v.[v:mood.pn]</ta>
            <ta e="T105" id="Seg_2645" s="T104">dempro</ta>
            <ta e="T106" id="Seg_2646" s="T105">n-n&gt;n.[n:case]</ta>
            <ta e="T107" id="Seg_2647" s="T106">v.[v:mood.pn]</ta>
            <ta e="T108" id="Seg_2648" s="T107">v-v:cvb-v:pred.pn</ta>
            <ta e="T109" id="Seg_2649" s="T108">adv</ta>
            <ta e="T110" id="Seg_2650" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_2651" s="T110">que</ta>
            <ta e="T112" id="Seg_2652" s="T111">n-n:case</ta>
            <ta e="T113" id="Seg_2653" s="T112">dempro</ta>
            <ta e="T114" id="Seg_2654" s="T113">n.[n:case]</ta>
            <ta e="T115" id="Seg_2655" s="T114">v-v:tense-v:poss.pn</ta>
            <ta e="T116" id="Seg_2656" s="T115">posspr</ta>
            <ta e="T117" id="Seg_2657" s="T116">dempro</ta>
            <ta e="T118" id="Seg_2658" s="T117">v-v:ptcp</ta>
            <ta e="T119" id="Seg_2659" s="T118">n-n:(poss).[n:case]</ta>
            <ta e="T120" id="Seg_2660" s="T119">dempro</ta>
            <ta e="T121" id="Seg_2661" s="T120">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T122" id="Seg_2662" s="T121">n.[n:case]</ta>
            <ta e="T123" id="Seg_2663" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_2664" s="T123">v-v:tense-v:poss.pn</ta>
            <ta e="T125" id="Seg_2665" s="T124">dempro</ta>
            <ta e="T126" id="Seg_2666" s="T125">n.[n:case]</ta>
            <ta e="T127" id="Seg_2667" s="T126">n-n:case</ta>
            <ta e="T128" id="Seg_2668" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_2669" s="T128">n-n:(num).[n:case]</ta>
            <ta e="T130" id="Seg_2670" s="T129">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T131" id="Seg_2671" s="T130">adj.[n:case]</ta>
            <ta e="T132" id="Seg_2672" s="T131">n-n:(num).[n:case]</ta>
            <ta e="T133" id="Seg_2673" s="T132">adj-n:(num).[n:case]</ta>
            <ta e="T134" id="Seg_2674" s="T133">propr</ta>
            <ta e="T135" id="Seg_2675" s="T134">v-v:cvb</ta>
            <ta e="T136" id="Seg_2676" s="T135">n-n:(num).[n:case]</ta>
            <ta e="T137" id="Seg_2677" s="T136">adv</ta>
            <ta e="T138" id="Seg_2678" s="T137">n.[n:case]</ta>
            <ta e="T139" id="Seg_2679" s="T138">n.[n:case]</ta>
            <ta e="T140" id="Seg_2680" s="T139">v-v:tense-v:poss.pn</ta>
            <ta e="T141" id="Seg_2681" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_2682" s="T141">propr.[n:case]</ta>
            <ta e="T143" id="Seg_2683" s="T142">v-v:cvb</ta>
            <ta e="T144" id="Seg_2684" s="T143">n.[n:case]</ta>
            <ta e="T145" id="Seg_2685" s="T144">v-v:tense.[v:pred.pn]</ta>
            <ta e="T146" id="Seg_2686" s="T145">v-v:tense.[v:pred.pn]</ta>
            <ta e="T147" id="Seg_2687" s="T146">adv</ta>
            <ta e="T148" id="Seg_2688" s="T147">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T149" id="Seg_2689" s="T148">propr</ta>
            <ta e="T150" id="Seg_2690" s="T149">v-v:cvb</ta>
            <ta e="T151" id="Seg_2691" s="T150">n-n:(num).[n:case]</ta>
            <ta e="T152" id="Seg_2692" s="T151">adv</ta>
            <ta e="T153" id="Seg_2693" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_2694" s="T153">ptcl</ta>
            <ta e="T155" id="Seg_2695" s="T154">dempro</ta>
            <ta e="T156" id="Seg_2696" s="T155">n</ta>
            <ta e="T157" id="Seg_2697" s="T156">n-n:case</ta>
            <ta e="T158" id="Seg_2698" s="T157">adv</ta>
            <ta e="T159" id="Seg_2699" s="T158">ptcl</ta>
            <ta e="T160" id="Seg_2700" s="T159">dempro.[pro:case]</ta>
            <ta e="T161" id="Seg_2701" s="T160">post</ta>
            <ta e="T162" id="Seg_2702" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_2703" s="T162">dempro</ta>
            <ta e="T164" id="Seg_2704" s="T163">n-n:poss-n:case</ta>
            <ta e="T165" id="Seg_2705" s="T164">propr.[n:case]</ta>
            <ta e="T166" id="Seg_2706" s="T165">v-v:cvb</ta>
            <ta e="T167" id="Seg_2707" s="T166">v-v:tense-v:pred.pn</ta>
            <ta e="T168" id="Seg_2708" s="T167">propr.[n:case]</ta>
            <ta e="T169" id="Seg_2709" s="T168">adj-adj&gt;adv</ta>
            <ta e="T170" id="Seg_2710" s="T169">propr.[n:case]</ta>
            <ta e="T171" id="Seg_2711" s="T170">v-v:cvb</ta>
            <ta e="T172" id="Seg_2712" s="T171">v-v:tense-v:pred.pn</ta>
            <ta e="T173" id="Seg_2713" s="T172">n.[n:case]</ta>
            <ta e="T174" id="Seg_2714" s="T173">n.[n:case]</ta>
            <ta e="T175" id="Seg_2715" s="T174">v-v:tense.[v:pred.pn]</ta>
            <ta e="T176" id="Seg_2716" s="T175">v-v:tense-v:poss.pn</ta>
            <ta e="T177" id="Seg_2717" s="T176">v-v:cvb</ta>
            <ta e="T178" id="Seg_2718" s="T177">dempro</ta>
            <ta e="T179" id="Seg_2719" s="T178">n.[n:case]</ta>
            <ta e="T180" id="Seg_2720" s="T179">n.[n:case]</ta>
            <ta e="T181" id="Seg_2721" s="T180">v-v:tense-v:pred.pn</ta>
            <ta e="T182" id="Seg_2722" s="T181">v-v:ptcp</ta>
            <ta e="T183" id="Seg_2723" s="T182">n-n:(poss).[n:case]</ta>
            <ta e="T184" id="Seg_2724" s="T183">propr.[n:case]</ta>
            <ta e="T185" id="Seg_2725" s="T184">adv</ta>
            <ta e="T186" id="Seg_2726" s="T185">v-v:cvb</ta>
            <ta e="T187" id="Seg_2727" s="T186">propr-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T188" id="Seg_2728" s="T187">n-n:poss-n:case</ta>
            <ta e="T189" id="Seg_2729" s="T188">v-v:tense-v:pred.pn</ta>
            <ta e="T190" id="Seg_2730" s="T189">dempro</ta>
            <ta e="T191" id="Seg_2731" s="T190">propr.[n:case]</ta>
            <ta e="T192" id="Seg_2732" s="T191">n-n:(poss).[n:case]</ta>
            <ta e="T193" id="Seg_2733" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_2734" s="T193">dempro-pro:case</ta>
            <ta e="T195" id="Seg_2735" s="T194">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T196" id="Seg_2736" s="T195">n-n:poss-n:case</ta>
            <ta e="T197" id="Seg_2737" s="T196">v-v:(neg)-v:pred.pn</ta>
            <ta e="T198" id="Seg_2738" s="T197">adv-adv&gt;adj</ta>
            <ta e="T199" id="Seg_2739" s="T198">n-n:case</ta>
            <ta e="T200" id="Seg_2740" s="T199">n-n:case</ta>
            <ta e="T201" id="Seg_2741" s="T200">v-v:mood-v:pred.pn</ta>
            <ta e="T202" id="Seg_2742" s="T201">adj.[n:case]</ta>
            <ta e="T203" id="Seg_2743" s="T202">n-n:poss-n:case</ta>
            <ta e="T204" id="Seg_2744" s="T203">ptcl</ta>
            <ta e="T205" id="Seg_2745" s="T204">adj.[n:case]</ta>
            <ta e="T206" id="Seg_2746" s="T205">n-n:poss-n:case</ta>
            <ta e="T207" id="Seg_2747" s="T206">ptcl</ta>
            <ta e="T208" id="Seg_2748" s="T207">dempro.[pro:case]</ta>
            <ta e="T209" id="Seg_2749" s="T208">post</ta>
            <ta e="T210" id="Seg_2750" s="T209">propr.[n:case]</ta>
            <ta e="T211" id="Seg_2751" s="T210">n-n:(poss).[n:case]</ta>
            <ta e="T212" id="Seg_2752" s="T211">v-v:tense.[v:pred.pn]</ta>
            <ta e="T213" id="Seg_2753" s="T212">n-n:(poss).[n:case]</ta>
            <ta e="T214" id="Seg_2754" s="T213">ptcl</ta>
            <ta e="T215" id="Seg_2755" s="T214">propr.[n:case]</ta>
            <ta e="T216" id="Seg_2756" s="T215">n-n:(poss).[n:case]</ta>
            <ta e="T217" id="Seg_2757" s="T216">v-v:tense.[v:pred.pn]</ta>
            <ta e="T218" id="Seg_2758" s="T217">adv</ta>
            <ta e="T219" id="Seg_2759" s="T218">v-v:cvb</ta>
            <ta e="T220" id="Seg_2760" s="T219">dempro.[pro:case]</ta>
            <ta e="T221" id="Seg_2761" s="T220">post</ta>
            <ta e="T222" id="Seg_2762" s="T221">v-v:cvb-v:pred.pn</ta>
            <ta e="T223" id="Seg_2763" s="T222">n.[n:case]</ta>
            <ta e="T224" id="Seg_2764" s="T223">v-v:cvb-v:pred.pn</ta>
            <ta e="T225" id="Seg_2765" s="T224">v-v:cvb-v-v:cvb</ta>
            <ta e="T226" id="Seg_2766" s="T225">dempro</ta>
            <ta e="T227" id="Seg_2767" s="T226">n-n:case</ta>
            <ta e="T228" id="Seg_2768" s="T227">adj-n:poss-n:case</ta>
            <ta e="T229" id="Seg_2769" s="T228">v-v:cvb-v:pred.pn</ta>
            <ta e="T230" id="Seg_2770" s="T229">n.[n:case]</ta>
            <ta e="T231" id="Seg_2771" s="T230">v-v:ptcp</ta>
            <ta e="T232" id="Seg_2772" s="T231">n-n:(poss).[n:case]</ta>
            <ta e="T233" id="Seg_2773" s="T232">v-v:tense.[v:pred.pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_2774" s="T1">dempro</ta>
            <ta e="T3" id="Seg_2775" s="T2">n</ta>
            <ta e="T4" id="Seg_2776" s="T3">ordnum</ta>
            <ta e="T5" id="Seg_2777" s="T4">n</ta>
            <ta e="T6" id="Seg_2778" s="T5">dempro</ta>
            <ta e="T7" id="Seg_2779" s="T6">n</ta>
            <ta e="T8" id="Seg_2780" s="T7">v</ta>
            <ta e="T9" id="Seg_2781" s="T8">v</ta>
            <ta e="T10" id="Seg_2782" s="T9">adv</ta>
            <ta e="T11" id="Seg_2783" s="T10">ordnum</ta>
            <ta e="T12" id="Seg_2784" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_2785" s="T12">ordnum</ta>
            <ta e="T14" id="Seg_2786" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_2787" s="T14">n</ta>
            <ta e="T16" id="Seg_2788" s="T15">n</ta>
            <ta e="T17" id="Seg_2789" s="T16">v</ta>
            <ta e="T18" id="Seg_2790" s="T17">dempro</ta>
            <ta e="T19" id="Seg_2791" s="T18">n</ta>
            <ta e="T20" id="Seg_2792" s="T19">v</ta>
            <ta e="T21" id="Seg_2793" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_2794" s="T21">dempro</ta>
            <ta e="T23" id="Seg_2795" s="T22">n</ta>
            <ta e="T24" id="Seg_2796" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_2797" s="T24">v</ta>
            <ta e="T26" id="Seg_2798" s="T25">adv</ta>
            <ta e="T27" id="Seg_2799" s="T26">dempro</ta>
            <ta e="T28" id="Seg_2800" s="T27">n</ta>
            <ta e="T29" id="Seg_2801" s="T28">n</ta>
            <ta e="T30" id="Seg_2802" s="T29">v</ta>
            <ta e="T31" id="Seg_2803" s="T30">v</ta>
            <ta e="T32" id="Seg_2804" s="T31">n</ta>
            <ta e="T33" id="Seg_2805" s="T32">dempro</ta>
            <ta e="T34" id="Seg_2806" s="T33">v</ta>
            <ta e="T35" id="Seg_2807" s="T34">n</ta>
            <ta e="T36" id="Seg_2808" s="T35">ptcl</ta>
            <ta e="T37" id="Seg_2809" s="T36">adv</ta>
            <ta e="T38" id="Seg_2810" s="T37">dempro</ta>
            <ta e="T39" id="Seg_2811" s="T38">n</ta>
            <ta e="T40" id="Seg_2812" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_2813" s="T40">n</ta>
            <ta e="T42" id="Seg_2814" s="T41">que</ta>
            <ta e="T43" id="Seg_2815" s="T42">v</ta>
            <ta e="T44" id="Seg_2816" s="T43">v</ta>
            <ta e="T45" id="Seg_2817" s="T44">n</ta>
            <ta e="T46" id="Seg_2818" s="T45">v</ta>
            <ta e="T47" id="Seg_2819" s="T46">n</ta>
            <ta e="T48" id="Seg_2820" s="T47">v</ta>
            <ta e="T49" id="Seg_2821" s="T48">v</ta>
            <ta e="T50" id="Seg_2822" s="T49">v</ta>
            <ta e="T51" id="Seg_2823" s="T50">n</ta>
            <ta e="T52" id="Seg_2824" s="T51">v</ta>
            <ta e="T53" id="Seg_2825" s="T52">n</ta>
            <ta e="T54" id="Seg_2826" s="T53">n</ta>
            <ta e="T55" id="Seg_2827" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_2828" s="T55">n</ta>
            <ta e="T57" id="Seg_2829" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_2830" s="T57">dempro</ta>
            <ta e="T59" id="Seg_2831" s="T58">v</ta>
            <ta e="T60" id="Seg_2832" s="T59">n</ta>
            <ta e="T61" id="Seg_2833" s="T60">n</ta>
            <ta e="T62" id="Seg_2834" s="T61">adv</ta>
            <ta e="T63" id="Seg_2835" s="T62">dempro</ta>
            <ta e="T64" id="Seg_2836" s="T63">v</ta>
            <ta e="T65" id="Seg_2837" s="T64">adj</ta>
            <ta e="T66" id="Seg_2838" s="T65">n</ta>
            <ta e="T67" id="Seg_2839" s="T66">n</ta>
            <ta e="T68" id="Seg_2840" s="T67">v</ta>
            <ta e="T69" id="Seg_2841" s="T68">n</ta>
            <ta e="T70" id="Seg_2842" s="T69">v</ta>
            <ta e="T71" id="Seg_2843" s="T70">n</ta>
            <ta e="T72" id="Seg_2844" s="T71">adv</ta>
            <ta e="T73" id="Seg_2845" s="T72">ptcl</ta>
            <ta e="T74" id="Seg_2846" s="T73">n</ta>
            <ta e="T75" id="Seg_2847" s="T74">v</ta>
            <ta e="T76" id="Seg_2848" s="T75">v</ta>
            <ta e="T77" id="Seg_2849" s="T76">adv</ta>
            <ta e="T78" id="Seg_2850" s="T77">adv</ta>
            <ta e="T79" id="Seg_2851" s="T78">n</ta>
            <ta e="T80" id="Seg_2852" s="T79">n</ta>
            <ta e="T81" id="Seg_2853" s="T80">cop</ta>
            <ta e="T82" id="Seg_2854" s="T81">dempro</ta>
            <ta e="T83" id="Seg_2855" s="T82">n</ta>
            <ta e="T84" id="Seg_2856" s="T83">dempro</ta>
            <ta e="T85" id="Seg_2857" s="T84">adj</ta>
            <ta e="T86" id="Seg_2858" s="T85">n</ta>
            <ta e="T87" id="Seg_2859" s="T86">n</ta>
            <ta e="T88" id="Seg_2860" s="T87">v</ta>
            <ta e="T89" id="Seg_2861" s="T88">dempro</ta>
            <ta e="T90" id="Seg_2862" s="T89">n</ta>
            <ta e="T91" id="Seg_2863" s="T90">n</ta>
            <ta e="T92" id="Seg_2864" s="T91">adv</ta>
            <ta e="T93" id="Seg_2865" s="T92">n</ta>
            <ta e="T94" id="Seg_2866" s="T93">n</ta>
            <ta e="T96" id="Seg_2867" s="T95">adv</ta>
            <ta e="T97" id="Seg_2868" s="T96">v</ta>
            <ta e="T98" id="Seg_2869" s="T97">v</ta>
            <ta e="T99" id="Seg_2870" s="T98">adv</ta>
            <ta e="T100" id="Seg_2871" s="T99">v</ta>
            <ta e="T101" id="Seg_2872" s="T100">v</ta>
            <ta e="T102" id="Seg_2873" s="T101">dempro</ta>
            <ta e="T103" id="Seg_2874" s="T102">n</ta>
            <ta e="T104" id="Seg_2875" s="T103">cop</ta>
            <ta e="T105" id="Seg_2876" s="T104">dempro</ta>
            <ta e="T106" id="Seg_2877" s="T105">n</ta>
            <ta e="T107" id="Seg_2878" s="T106">cop</ta>
            <ta e="T108" id="Seg_2879" s="T107">v</ta>
            <ta e="T109" id="Seg_2880" s="T108">adv</ta>
            <ta e="T110" id="Seg_2881" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_2882" s="T110">que</ta>
            <ta e="T112" id="Seg_2883" s="T111">n</ta>
            <ta e="T113" id="Seg_2884" s="T112">dempro</ta>
            <ta e="T114" id="Seg_2885" s="T113">n</ta>
            <ta e="T115" id="Seg_2886" s="T114">v</ta>
            <ta e="T116" id="Seg_2887" s="T115">posspr</ta>
            <ta e="T117" id="Seg_2888" s="T116">dempro</ta>
            <ta e="T118" id="Seg_2889" s="T117">v</ta>
            <ta e="T119" id="Seg_2890" s="T118">n</ta>
            <ta e="T120" id="Seg_2891" s="T119">dempro</ta>
            <ta e="T121" id="Seg_2892" s="T120">v</ta>
            <ta e="T122" id="Seg_2893" s="T121">n</ta>
            <ta e="T123" id="Seg_2894" s="T122">n</ta>
            <ta e="T124" id="Seg_2895" s="T123">v</ta>
            <ta e="T125" id="Seg_2896" s="T124">dempro</ta>
            <ta e="T126" id="Seg_2897" s="T125">n</ta>
            <ta e="T127" id="Seg_2898" s="T126">n</ta>
            <ta e="T128" id="Seg_2899" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_2900" s="T128">n</ta>
            <ta e="T130" id="Seg_2901" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_2902" s="T130">adj</ta>
            <ta e="T132" id="Seg_2903" s="T131">n</ta>
            <ta e="T133" id="Seg_2904" s="T132">n</ta>
            <ta e="T134" id="Seg_2905" s="T133">propr</ta>
            <ta e="T135" id="Seg_2906" s="T134">v</ta>
            <ta e="T136" id="Seg_2907" s="T135">n</ta>
            <ta e="T137" id="Seg_2908" s="T136">adv</ta>
            <ta e="T138" id="Seg_2909" s="T137">n</ta>
            <ta e="T139" id="Seg_2910" s="T138">n</ta>
            <ta e="T140" id="Seg_2911" s="T139">cop</ta>
            <ta e="T141" id="Seg_2912" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_2913" s="T141">propr</ta>
            <ta e="T143" id="Seg_2914" s="T142">v</ta>
            <ta e="T144" id="Seg_2915" s="T143">n</ta>
            <ta e="T145" id="Seg_2916" s="T144">cop</ta>
            <ta e="T146" id="Seg_2917" s="T145">aux</ta>
            <ta e="T147" id="Seg_2918" s="T146">adv</ta>
            <ta e="T148" id="Seg_2919" s="T147">ptcl</ta>
            <ta e="T149" id="Seg_2920" s="T148">propr</ta>
            <ta e="T150" id="Seg_2921" s="T149">v</ta>
            <ta e="T151" id="Seg_2922" s="T150">n</ta>
            <ta e="T152" id="Seg_2923" s="T151">adv</ta>
            <ta e="T153" id="Seg_2924" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_2925" s="T153">ptcl</ta>
            <ta e="T155" id="Seg_2926" s="T154">dempro</ta>
            <ta e="T156" id="Seg_2927" s="T155">n</ta>
            <ta e="T157" id="Seg_2928" s="T156">n</ta>
            <ta e="T158" id="Seg_2929" s="T157">adv</ta>
            <ta e="T159" id="Seg_2930" s="T158">ptcl</ta>
            <ta e="T160" id="Seg_2931" s="T159">dempro</ta>
            <ta e="T161" id="Seg_2932" s="T160">post</ta>
            <ta e="T162" id="Seg_2933" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_2934" s="T162">dempro</ta>
            <ta e="T164" id="Seg_2935" s="T163">n</ta>
            <ta e="T165" id="Seg_2936" s="T164">propr</ta>
            <ta e="T166" id="Seg_2937" s="T165">v</ta>
            <ta e="T167" id="Seg_2938" s="T166">v</ta>
            <ta e="T168" id="Seg_2939" s="T167">propr</ta>
            <ta e="T169" id="Seg_2940" s="T168">adv</ta>
            <ta e="T170" id="Seg_2941" s="T169">propr</ta>
            <ta e="T171" id="Seg_2942" s="T170">v</ta>
            <ta e="T172" id="Seg_2943" s="T171">v</ta>
            <ta e="T173" id="Seg_2944" s="T172">n</ta>
            <ta e="T174" id="Seg_2945" s="T173">n</ta>
            <ta e="T175" id="Seg_2946" s="T174">cop</ta>
            <ta e="T176" id="Seg_2947" s="T175">v</ta>
            <ta e="T177" id="Seg_2948" s="T176">v</ta>
            <ta e="T178" id="Seg_2949" s="T177">dempro</ta>
            <ta e="T179" id="Seg_2950" s="T178">n</ta>
            <ta e="T180" id="Seg_2951" s="T179">n</ta>
            <ta e="T181" id="Seg_2952" s="T180">v</ta>
            <ta e="T182" id="Seg_2953" s="T181">v</ta>
            <ta e="T183" id="Seg_2954" s="T182">n</ta>
            <ta e="T184" id="Seg_2955" s="T183">propr</ta>
            <ta e="T185" id="Seg_2956" s="T184">adv</ta>
            <ta e="T186" id="Seg_2957" s="T185">v</ta>
            <ta e="T187" id="Seg_2958" s="T186">propr</ta>
            <ta e="T188" id="Seg_2959" s="T187">n</ta>
            <ta e="T189" id="Seg_2960" s="T188">v</ta>
            <ta e="T190" id="Seg_2961" s="T189">dempro</ta>
            <ta e="T191" id="Seg_2962" s="T190">propr</ta>
            <ta e="T192" id="Seg_2963" s="T191">n</ta>
            <ta e="T193" id="Seg_2964" s="T192">ptcl</ta>
            <ta e="T194" id="Seg_2965" s="T193">dempro</ta>
            <ta e="T195" id="Seg_2966" s="T194">emphpro</ta>
            <ta e="T196" id="Seg_2967" s="T195">n</ta>
            <ta e="T197" id="Seg_2968" s="T196">v</ta>
            <ta e="T198" id="Seg_2969" s="T197">adj</ta>
            <ta e="T199" id="Seg_2970" s="T198">n</ta>
            <ta e="T200" id="Seg_2971" s="T199">n</ta>
            <ta e="T201" id="Seg_2972" s="T200">v</ta>
            <ta e="T202" id="Seg_2973" s="T201">adj</ta>
            <ta e="T203" id="Seg_2974" s="T202">n</ta>
            <ta e="T204" id="Seg_2975" s="T203">ptcl</ta>
            <ta e="T205" id="Seg_2976" s="T204">adj</ta>
            <ta e="T206" id="Seg_2977" s="T205">n</ta>
            <ta e="T207" id="Seg_2978" s="T206">ptcl</ta>
            <ta e="T208" id="Seg_2979" s="T207">dempro</ta>
            <ta e="T209" id="Seg_2980" s="T208">post</ta>
            <ta e="T210" id="Seg_2981" s="T209">propr</ta>
            <ta e="T211" id="Seg_2982" s="T210">n</ta>
            <ta e="T212" id="Seg_2983" s="T211">cop</ta>
            <ta e="T213" id="Seg_2984" s="T212">n</ta>
            <ta e="T214" id="Seg_2985" s="T213">ptcl</ta>
            <ta e="T215" id="Seg_2986" s="T214">propr</ta>
            <ta e="T216" id="Seg_2987" s="T215">n</ta>
            <ta e="T217" id="Seg_2988" s="T216">cop</ta>
            <ta e="T218" id="Seg_2989" s="T217">adv</ta>
            <ta e="T219" id="Seg_2990" s="T218">v</ta>
            <ta e="T220" id="Seg_2991" s="T219">dempro</ta>
            <ta e="T221" id="Seg_2992" s="T220">post</ta>
            <ta e="T222" id="Seg_2993" s="T221">v</ta>
            <ta e="T223" id="Seg_2994" s="T222">n</ta>
            <ta e="T224" id="Seg_2995" s="T223">v</ta>
            <ta e="T225" id="Seg_2996" s="T224">v</ta>
            <ta e="T226" id="Seg_2997" s="T225">dempro</ta>
            <ta e="T227" id="Seg_2998" s="T226">n</ta>
            <ta e="T228" id="Seg_2999" s="T227">adj</ta>
            <ta e="T229" id="Seg_3000" s="T228">v</ta>
            <ta e="T230" id="Seg_3001" s="T229">n</ta>
            <ta e="T231" id="Seg_3002" s="T230">v</ta>
            <ta e="T232" id="Seg_3003" s="T231">n</ta>
            <ta e="T233" id="Seg_3004" s="T232">cop</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T45" id="Seg_3005" s="T44">RUS:cult</ta>
            <ta e="T85" id="Seg_3006" s="T84">RUS:cult</ta>
            <ta e="T91" id="Seg_3007" s="T90">EV:gram (DIM)</ta>
            <ta e="T122" id="Seg_3008" s="T121">RUS:cult</ta>
            <ta e="T126" id="Seg_3009" s="T125">RUS:cult</ta>
            <ta e="T131" id="Seg_3010" s="T130">RUS:cult</ta>
            <ta e="T133" id="Seg_3011" s="T132">RUS:cult</ta>
            <ta e="T134" id="Seg_3012" s="T133">RUS:cult</ta>
            <ta e="T142" id="Seg_3013" s="T141">RUS:cult</ta>
            <ta e="T147" id="Seg_3014" s="T146">RUS:mod</ta>
            <ta e="T149" id="Seg_3015" s="T148">RUS:cult</ta>
            <ta e="T156" id="Seg_3016" s="T155">RUS:cult</ta>
            <ta e="T165" id="Seg_3017" s="T164">RUS:cult</ta>
            <ta e="T168" id="Seg_3018" s="T167">RUS:cult</ta>
            <ta e="T170" id="Seg_3019" s="T169">RUS:cult</ta>
            <ta e="T184" id="Seg_3020" s="T183">RUS:cult</ta>
            <ta e="T187" id="Seg_3021" s="T186">RUS:cult</ta>
            <ta e="T191" id="Seg_3022" s="T190">RUS:cult</ta>
            <ta e="T210" id="Seg_3023" s="T209">RUS:cult</ta>
            <ta e="T215" id="Seg_3024" s="T214">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T9" id="Seg_3025" s="T1">The child is laid [there] on the third day, when the cradle has been made.</ta>
            <ta e="T17" id="Seg_3026" s="T9">Then at the fifth or the seventh day the navel drops.</ta>
            <ta e="T25" id="Seg_3027" s="T17">When the navel has dropped, the child is baptized.</ta>
            <ta e="T32" id="Seg_3028" s="T25">Then, when the child's navel is dropping down, they baptize the child.</ta>
            <ta e="T41" id="Seg_3029" s="T32">Baptizing, far away in there, at a place where there is no clergyman.</ta>
            <ta e="T61" id="Seg_3030" s="T41">Who knows to baptize, who knows a prayer, who knows a baptizing prayer, grandfather or grandmother, that one baptizes [it] in the tundra.</ta>
            <ta e="T71" id="Seg_3031" s="T61">Then baptizing one puts an icon into holy water, one pours water into a jar.</ta>
            <ta e="T83" id="Seg_3032" s="T71">Then one sticks the icon in, then the water becomes holy water.</ta>
            <ta e="T90" id="Seg_3033" s="T83">In the holy water they wash the child.</ta>
            <ta e="T93" id="Seg_3034" s="T90">His little head three times.</ta>
            <ta e="T98" id="Seg_3035" s="T93">The father, saying "live happily".</ta>
            <ta e="T101" id="Seg_3036" s="T98">"Grow up happily", they say.</ta>
            <ta e="T108" id="Seg_3037" s="T101">"Be such a hinter, such a provider", they say.</ta>
            <ta e="T121" id="Seg_3038" s="T108">But then, in which month the child was born, our playing(?) child, when we play(?).</ta>
            <ta e="T124" id="Seg_3039" s="T121">It was born in the month of February.</ta>
            <ta e="T133" id="Seg_3040" s="T124">In February there are icons, holy icons, saints.</ta>
            <ta e="T136" id="Seg_3041" s="T133">There is the icon of Aksinya.</ta>
            <ta e="T146" id="Seg_3042" s="T136">Then, if it is a girl, one will name it Aksinya.</ta>
            <ta e="T151" id="Seg_3043" s="T146">Then there are names with the name Stresinskiy.</ta>
            <ta e="T157" id="Seg_3044" s="T151">Then in the month of February.</ta>
            <ta e="T168" id="Seg_3045" s="T157">Then, because of this, they call the child Semyon, Hemeeche.</ta>
            <ta e="T169" id="Seg_3046" s="T168">In Dolgan.</ta>
            <ta e="T177" id="Seg_3047" s="T169">They call it Hemeeche, it is a boy, has been born.</ta>
            <ta e="T181" id="Seg_3048" s="T177">They gave him this name, this mark.</ta>
            <ta e="T184" id="Seg_3049" s="T181">The baptismal name is Hemeeche.</ta>
            <ta e="T189" id="Seg_3050" s="T184">And then one gives [a name] to Semyon's mother.</ta>
            <ta e="T197" id="Seg_3051" s="T189">Semyon's mother isn't called with her own name from then on.</ta>
            <ta e="T207" id="Seg_3052" s="T197">In olden times they called [her] after her oldest son or her oldest daughter [as a symbol] of respect.</ta>
            <ta e="T212" id="Seg_3053" s="T207">Therefore she is Hemeeche's mother.</ta>
            <ta e="T217" id="Seg_3054" s="T212">And the father becomes Hemeeche's father.</ta>
            <ta e="T225" id="Seg_3055" s="T217">And so they give [him] a name and make a celebration then.</ta>
            <ta e="T233" id="Seg_3056" s="T225">They invite all people, that's the celebration of the birth of the child.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T9" id="Seg_3057" s="T1">Das Kind legt man am dritten Tag [hinein], wenn die Wiege fertiggestellt ist.</ta>
            <ta e="T17" id="Seg_3058" s="T9">Dann am fünften oder am siebten Tag fällt der Nabel ab.</ta>
            <ta e="T25" id="Seg_3059" s="T17">Wenn der Nabel abgefallen ist, dann tauft man das Kind.</ta>
            <ta e="T32" id="Seg_3060" s="T25">Dann, wenn der Nabel des Kindes abfällt, tauft man das Kind.</ta>
            <ta e="T41" id="Seg_3061" s="T32">Taufend, weit weg in der Tundra, an einem Ort, wo kein Geistlicher ist. </ta>
            <ta e="T61" id="Seg_3062" s="T41">Wer taufen kann, wer ein Gebet kennt, wer ein Taufgebet kennt, eine Großmutter oder ein Großvater, der tauft [es] dann in der Tundra.</ta>
            <ta e="T71" id="Seg_3063" s="T61">Dann bei der Taufe legt man eine Ikone in heiliges Wasser, man gießt Wasser in ein Gefäß.</ta>
            <ta e="T83" id="Seg_3064" s="T71">Dann steckt man die Ikone dort hinein, dann wird aus dem Wasser heiliges Wasser.</ta>
            <ta e="T90" id="Seg_3065" s="T83">In dem heiligen Wasser wäscht man das Kind.</ta>
            <ta e="T93" id="Seg_3066" s="T90">Sein Köpfchen drei mal.</ta>
            <ta e="T98" id="Seg_3067" s="T93">Der Vater, "lebe glücklich", sagt man.</ta>
            <ta e="T101" id="Seg_3068" s="T98">"Wachse glücklich auf", sagt man.</ta>
            <ta e="T108" id="Seg_3069" s="T101">"Sei so ein Jäger, sei so ein Versorger", sagt man.</ta>
            <ta e="T121" id="Seg_3070" s="T108">Dann aber, in welchem Monat wurde das Kind geboren, unser spielendes(?) Kind, wenn wir spielen(?).</ta>
            <ta e="T124" id="Seg_3071" s="T121">Es wurde im Monat Februar geboren.</ta>
            <ta e="T133" id="Seg_3072" s="T124">Im Monat Februar aber gibt es Ikonen, heilige Ikonen, Heilige.</ta>
            <ta e="T136" id="Seg_3073" s="T133">Die Ikone der Aksinja. </ta>
            <ta e="T146" id="Seg_3074" s="T136">Dann, wenn es ein Mädchen ist, dann nennt man es Aksinja.</ta>
            <ta e="T151" id="Seg_3075" s="T146">Es gibt noch Ikonen mit dem Namen Stresinskij.</ta>
            <ta e="T157" id="Seg_3076" s="T151">Dann im Monat Februar.</ta>
            <ta e="T168" id="Seg_3077" s="T157">Dann nennt man das Kind deshalb Semjon, Hemeeche.</ta>
            <ta e="T169" id="Seg_3078" s="T168">Auf Dolganisch.</ta>
            <ta e="T177" id="Seg_3079" s="T169">Man nennt es Hemeeche, es ist ein Junge geworden, wurde geboren.</ta>
            <ta e="T181" id="Seg_3080" s="T177">Diesen Namen, dieses Kennzeichen gibt man.</ta>
            <ta e="T184" id="Seg_3081" s="T181">Der Taufname ist Hemeeche.</ta>
            <ta e="T189" id="Seg_3082" s="T184">Und dann gibt man der Mutter von Semjon [einen Namen]. </ta>
            <ta e="T197" id="Seg_3083" s="T189">Semjons Mutter nennt man von da an nicht mehr mit ihrem eigenen Namen.</ta>
            <ta e="T207" id="Seg_3084" s="T197">In alter Zeit nannte man [sie] aus Achtung nach ihrem ältesten Sohn oder ihrer ältesten Tochter.</ta>
            <ta e="T212" id="Seg_3085" s="T207">Deshalb ist sie Hemeeches Mutter.</ta>
            <ta e="T217" id="Seg_3086" s="T212">Der Vater aber wird Hemeeches Vater.</ta>
            <ta e="T225" id="Seg_3087" s="T217">Und so gibt man [ihm] einen Namen und macht dann eine Feier.</ta>
            <ta e="T233" id="Seg_3088" s="T225">Man lädt alle Leute ein, das ist die Feier zur Geburt des Kindes.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T9" id="Seg_3089" s="T1">Этого ребёнка на третий день тут, когда люлька готова, укладывают.</ta>
            <ta e="T17" id="Seg_3090" s="T9">После на пятый ли, на седьмой ли день пупок отпадает. </ta>
            <ta e="T25" id="Seg_3091" s="T17">Вот как пупок отпал ведь, того ребёнка крестят.</ta>
            <ta e="T32" id="Seg_3092" s="T25">Потом этого ребёнка, пупок как отпадёт, крестят малыша.</ta>
            <ta e="T41" id="Seg_3093" s="T32">И покрестив, в тундре ведь далеко, где священника не было, </ta>
            <ta e="T61" id="Seg_3094" s="T41">кто умеет крестить, молитву кто знает, при крещении молитву кто знает: дудушка ли, бабушка ли, тот и крестит в тундре. </ta>
            <ta e="T71" id="Seg_3095" s="T61">Тогда при крещении в крещённую воду икону опускают, воду наливают в эту миску. </ta>
            <ta e="T83" id="Seg_3096" s="T71">После икону опускают туда, тогда и становится крещённой водой становится та вода. </ta>
            <ta e="T90" id="Seg_3097" s="T83">В той святой воде крещённой воде моют этого ребёнака. </ta>
            <ta e="T93" id="Seg_3098" s="T90">Темечко трижды. </ta>
            <ta e="T98" id="Seg_3099" s="T93">Отец -э .. живи счастливо, желая.</ta>
            <ta e="T101" id="Seg_3100" s="T98">Счастливо родись, желая. </ta>
            <ta e="T108" id="Seg_3101" s="T101">Таким охотником будь, таким добыдчиком стань, желают.</ta>
            <ta e="T121" id="Seg_3102" s="T108">Потом вот в каком месяце этот ребёнок родился наш показываемый нами ребёнок. Вот сейчас когда показываем.</ta>
            <ta e="T124" id="Seg_3103" s="T121">В феврале месяце родился. </ta>
            <ta e="T133" id="Seg_3104" s="T124">В этом феврале месяце ведь есть святые иконы. Святые. </ta>
            <ta e="T136" id="Seg_3105" s="T133">Аксинья икона есть.</ta>
            <ta e="T146" id="Seg_3106" s="T136">Тогда, девочка если, дают имя Аксинья. </ta>
            <ta e="T151" id="Seg_3107" s="T146">Ещё есть Стресинские называемые иконы.</ta>
            <ta e="T157" id="Seg_3108" s="T151">Тогда вот в феврале месяце. </ta>
            <ta e="T168" id="Seg_3109" s="T157">Тогда вот поэтому этого ребёнка Семёном нарекают - Хэмээчэ.</ta>
            <ta e="T169" id="Seg_3110" s="T168">По-долгански. </ta>
            <ta e="T177" id="Seg_3111" s="T169">Хэмээчэ именем нарекают. Мальчиком стал, потому что родился. </ta>
            <ta e="T181" id="Seg_3112" s="T177">И тут имя метку дают.</ta>
            <ta e="T184" id="Seg_3113" s="T181">Крещённое имя Хэмээчэ. </ta>
            <ta e="T189" id="Seg_3114" s="T184">Отсюда Семёновой матери дают.</ta>
            <ta e="T197" id="Seg_3115" s="T189">Тут Семёна мать с этого времени своим именем не называют.</ta>
            <ta e="T207" id="Seg_3116" s="T197">В давние времена из уважения старшим сыном или старшей дочерью ли. </ta>
            <ta e="T212" id="Seg_3117" s="T207">Поэтому Семёна мать именуется.</ta>
            <ta e="T217" id="Seg_3118" s="T212">Отец то ведь Семёна отец становится. </ta>
            <ta e="T225" id="Seg_3119" s="T217">И тут же таким образом после нарекания, праздник устраивают.</ta>
            <ta e="T233" id="Seg_3120" s="T225">Тут весь народ собрав, именины ребёнка отмечают.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T121" id="Seg_3121" s="T108">[DCh]: The sense of the second part of the sentence is not really clear; the given translation does not fit the analysis.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
