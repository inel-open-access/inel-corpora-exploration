<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID509E7936-7F6E-14BF-B766-2D4213B3E484">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>PoKI_2009_Story_nar</transcription-name>
         <referenced-file url="PoKI_2009_Story_nar.wav" />
         <referenced-file url="PoKI_2009_Story_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\PoKI_2009_Story_nar\PoKI_2009_Story_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">145</ud-information>
            <ud-information attribute-name="# HIAT:w">121</ud-information>
            <ud-information attribute-name="# e">121</ud-information>
            <ud-information attribute-name="# HIAT:u">9</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PoKI">
            <abbreviation>PoKI</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.61" type="appl" />
         <tli id="T2" time="1.221" type="appl" />
         <tli id="T3" time="1.831" type="appl" />
         <tli id="T4" time="2.441" type="appl" />
         <tli id="T5" time="3.052" type="appl" />
         <tli id="T6" time="3.662" type="appl" />
         <tli id="T7" time="4.272" type="appl" />
         <tli id="T8" time="4.883" type="appl" />
         <tli id="T9" time="5.493" type="appl" />
         <tli id="T10" time="6.103" type="appl" />
         <tli id="T11" time="6.714" type="appl" />
         <tli id="T12" time="7.324" type="appl" />
         <tli id="T13" time="7.934" type="appl" />
         <tli id="T14" time="8.545" type="appl" />
         <tli id="T15" time="9.301663181639256" />
         <tli id="T16" time="9.531" type="appl" />
         <tli id="T17" time="9.908" type="appl" />
         <tli id="T18" time="10.284" type="appl" />
         <tli id="T19" time="10.66" type="appl" />
         <tli id="T20" time="11.037" type="appl" />
         <tli id="T21" time="11.413" type="appl" />
         <tli id="T22" time="11.741829268292683" type="intp" />
         <tli id="T23" time="12.070658536585366" type="intp" />
         <tli id="T24" time="12.39948780487805" type="intp" />
         <tli id="T25" time="12.728317073170732" type="intp" />
         <tli id="T26" time="13.057146341463415" type="intp" />
         <tli id="T27" time="13.385975609756098" type="intp" />
         <tli id="T28" time="13.714804878048781" type="intp" />
         <tli id="T29" time="14.043634146341464" type="intp" />
         <tli id="T30" time="14.372463414634147" type="intp" />
         <tli id="T31" time="14.70129268292683" type="intp" />
         <tli id="T32" time="15.030121951219513" type="intp" />
         <tli id="T33" time="15.358951219512196" type="intp" />
         <tli id="T34" time="15.68778048780488" type="intp" />
         <tli id="T35" time="16.016609756097562" type="intp" />
         <tli id="T36" time="16.345439024390245" type="intp" />
         <tli id="T37" time="16.674268292682928" type="intp" />
         <tli id="T38" time="17.00309756097561" type="intp" />
         <tli id="T39" time="17.331926829268294" type="intp" />
         <tli id="T40" time="17.660756097560977" type="intp" />
         <tli id="T41" time="17.98958536585366" type="intp" />
         <tli id="T42" time="18.318414634146343" type="intp" />
         <tli id="T43" time="18.647243902439026" type="intp" />
         <tli id="T44" time="18.649414233367576" />
         <tli id="T45" time="18.798247276426313" />
         <tli id="T46" time="18.84625893301978" />
         <tli id="T47" time="19.00625547153852" />
         <tli id="T48" time="20.29139024390244" type="intp" />
         <tli id="T49" time="20.620219512195124" type="intp" />
         <tli id="T50" time="20.949048780487807" type="intp" />
         <tli id="T51" time="21.27787804878049" type="intp" />
         <tli id="T52" time="21.606707317073173" type="intp" />
         <tli id="T53" time="21.935536585365856" type="intp" />
         <tli id="T54" time="22.26436585365854" type="intp" />
         <tli id="T55" time="22.593195121951222" type="intp" />
         <tli id="T56" time="22.922024390243905" type="intp" />
         <tli id="T57" time="23.250853658536588" type="intp" />
         <tli id="T58" time="23.57968292682927" type="intp" />
         <tli id="T59" time="23.908512195121954" type="intp" />
         <tli id="T60" time="24.237341463414637" type="intp" />
         <tli id="T61" time="24.472839677445766" />
         <tli id="T62" time="24.895" type="appl" />
         <tli id="T63" time="25.416" type="appl" />
         <tli id="T64" time="25.937" type="appl" />
         <tli id="T65" time="26.458" type="appl" />
         <tli id="T66" time="26.979" type="appl" />
         <tli id="T67" time="27.5" type="appl" />
         <tli id="T68" time="28.021" type="appl" />
         <tli id="T69" time="28.543" type="appl" />
         <tli id="T70" time="29.064" type="appl" />
         <tli id="T71" time="29.585" type="appl" />
         <tli id="T72" time="30.106" type="appl" />
         <tli id="T73" time="30.627" type="appl" />
         <tli id="T74" time="31.148" type="appl" />
         <tli id="T75" time="31.669" type="appl" />
         <tli id="T76" time="32.22999932738105" />
         <tli id="T77" time="32.584" type="appl" />
         <tli id="T78" time="32.979" type="appl" />
         <tli id="T79" time="33.373" type="appl" />
         <tli id="T80" time="33.768" type="appl" />
         <tli id="T81" time="34.162" type="appl" />
         <tli id="T82" time="34.557" type="appl" />
         <tli id="T83" time="34.951" type="appl" />
         <tli id="T84" time="35.346" type="appl" />
         <tli id="T85" time="35.74" type="appl" />
         <tli id="T86" time="36.11986666666667" type="intp" />
         <tli id="T87" time="36.49973333333333" type="intp" />
         <tli id="T88" time="36.879599999999996" type="intp" />
         <tli id="T89" time="37.25946666666666" type="intp" />
         <tli id="T90" time="37.639333333333326" type="intp" />
         <tli id="T91" time="38.01919999999999" type="intp" />
         <tli id="T92" time="38.399066666666656" type="intp" />
         <tli id="T93" time="38.77893333333333" type="intp" />
         <tli id="T94" time="39.15879999999999" type="intp" />
         <tli id="T95" time="39.53866666666666" type="intp" />
         <tli id="T96" time="39.91853333333333" type="intp" />
         <tli id="T97" time="40.2984" type="intp" />
         <tli id="T98" time="40.678266666666666" type="intp" />
         <tli id="T99" time="40.92480145209504" />
         <tli id="T100" time="41.438" type="appl" />
         <tli id="T101" time="42.195" type="appl" />
         <tli id="T102" time="42.953" type="appl" />
         <tli id="T103" time="43.71" type="appl" />
         <tli id="T104" time="44.468" type="appl" />
         <tli id="T105" time="45.225" type="appl" />
         <tli id="T106" time="45.983" type="appl" />
         <tli id="T107" time="46.74" type="appl" />
         <tli id="T108" time="47.498" type="appl" />
         <tli id="T109" time="48.255" type="appl" />
         <tli id="T110" time="49.013" type="appl" />
         <tli id="T111" time="49.4516875" type="intp" />
         <tli id="T112" time="49.890375" type="intp" />
         <tli id="T113" time="50.3290625" type="intp" />
         <tli id="T114" time="50.76775" type="intp" />
         <tli id="T115" time="51.2064375" type="intp" />
         <tli id="T116" time="51.645125" type="intp" />
         <tli id="T117" time="52.0838125" type="intp" />
         <tli id="T118" time="52.5225" type="intp" />
         <tli id="T119" time="52.9611875" type="intp" />
         <tli id="T120" time="53.399875" type="intp" />
         <tli id="T121" time="53.8385625" type="intp" />
         <tli id="T122" time="54.27725" type="intp" />
         <tli id="T123" time="54.7159375" type="intp" />
         <tli id="T124" time="55.154625" type="intp" />
         <tli id="T125" time="55.593312499999996" type="intp" />
         <tli id="T126" time="56.032" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PoKI"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T126" id="Seg_0" n="sc" s="T0">
               <ts e="T15" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">E</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">bihigi</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">kim</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">gorkaga</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">katatsalɨː</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">barar</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">etibit</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">Ponomarjovkaga</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">onton</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">bihigi</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">hɨrgalaːk</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_37" n="HIAT:w" s="T11">etibit</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_40" n="HIAT:w" s="T12">Rada</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_43" n="HIAT:w" s="T13">min</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_46" n="HIAT:w" s="T14">Spire</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_50" n="HIAT:u" s="T15">
                  <ts e="T16" id="Seg_52" n="HIAT:w" s="T15">Onton</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_55" n="HIAT:w" s="T16">bihigi</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_58" n="HIAT:w" s="T17">Spirjanɨ</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_61" n="HIAT:w" s="T18">kɨtta</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_64" n="HIAT:w" s="T19">kalɨjbɨt</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_67" n="HIAT:w" s="T20">etibit</ts>
                  <nts id="Seg_68" n="HIAT:ip">.</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_71" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_73" n="HIAT:w" s="T21">Onton</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_76" n="HIAT:w" s="T22">kalɨja</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_79" n="HIAT:w" s="T23">turaːktɨːbɨt</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_82" n="HIAT:w" s="T24">di͡en</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_85" n="HIAT:w" s="T25">onton</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_88" n="HIAT:w" s="T26">kim</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_91" n="HIAT:w" s="T27">e</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_94" n="HIAT:w" s="T28">kalɨja</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_97" n="HIAT:w" s="T29">turaːktɨːbɨt</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_100" n="HIAT:w" s="T30">otto</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_103" n="HIAT:w" s="T31">hɨrgabɨt</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_105" n="HIAT:ip">(</nts>
                  <ts e="T34" id="Seg_107" n="HIAT:w" s="T32">ka-</ts>
                  <nts id="Seg_108" n="HIAT:ip">)</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_111" n="HIAT:w" s="T34">otto</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_114" n="HIAT:w" s="T35">e</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_117" n="HIAT:w" s="T36">tu͡ok</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_120" n="HIAT:w" s="T37">ere</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_123" n="HIAT:w" s="T38">e</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_126" n="HIAT:w" s="T39">kim</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_129" n="HIAT:w" s="T40">mas</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_132" n="HIAT:w" s="T41">baːr</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_135" n="HIAT:w" s="T42">ete</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_138" n="HIAT:w" s="T43">onno</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_141" n="HIAT:w" s="T44">otto</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_143" n="HIAT:ip">(</nts>
                  <ts e="T47" id="Seg_145" n="HIAT:w" s="T45">tu͡o-</ts>
                  <nts id="Seg_146" n="HIAT:ip">)</nts>
                  <nts id="Seg_147" n="HIAT:ip">.</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_150" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_152" n="HIAT:w" s="T47">Kim</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_155" n="HIAT:w" s="T48">onton</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_158" n="HIAT:w" s="T49">hɨrgabɨt</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_161" n="HIAT:w" s="T50">kimneːbite</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_164" n="HIAT:w" s="T51">onno</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_167" n="HIAT:w" s="T52">kam</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_170" n="HIAT:w" s="T53">zastrjallaːbɨppɨt</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_173" n="HIAT:w" s="T54">onton</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_176" n="HIAT:w" s="T55">bihi</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_179" n="HIAT:w" s="T56">tühen</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_181" n="HIAT:ip">(</nts>
                  <ts e="T59" id="Seg_183" n="HIAT:w" s="T57">kaːl-</ts>
                  <nts id="Seg_184" n="HIAT:ip">)</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_187" n="HIAT:w" s="T59">kötön</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_190" n="HIAT:w" s="T60">barbɨppɨt</ts>
                  <nts id="Seg_191" n="HIAT:ip">.</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_194" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_196" n="HIAT:w" s="T61">Onton</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_199" n="HIAT:w" s="T62">hɨrgabɨt</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_202" n="HIAT:w" s="T63">kim</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_205" n="HIAT:w" s="T64">aldʼammɨt</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_208" n="HIAT:w" s="T65">ete</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_211" n="HIAT:w" s="T66">hɨrgabɨt</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_214" n="HIAT:w" s="T67">kim</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_217" n="HIAT:w" s="T68">hɨrgabɨt</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_220" n="HIAT:w" s="T69">kim</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_223" n="HIAT:w" s="T70">kiːl</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_226" n="HIAT:w" s="T71">e</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_229" n="HIAT:w" s="T72">munnuk</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_232" n="HIAT:w" s="T73">tokurujbut</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_235" n="HIAT:w" s="T74">ete</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_238" n="HIAT:w" s="T75">hɨrgabɨt</ts>
                  <nts id="Seg_239" n="HIAT:ip">.</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_242" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_244" n="HIAT:w" s="T76">Onton</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_247" n="HIAT:w" s="T77">onton</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_250" n="HIAT:w" s="T78">Spirja</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_253" n="HIAT:w" s="T79">tu͡ok</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_256" n="HIAT:w" s="T80">ere</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_259" n="HIAT:w" s="T81">bu͡ola</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_262" n="HIAT:w" s="T82">hɨtar</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_265" n="HIAT:w" s="T83">ete</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_268" n="HIAT:w" s="T84">onno</ts>
                  <nts id="Seg_269" n="HIAT:ip">.</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_272" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_274" n="HIAT:w" s="T85">Onton</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_277" n="HIAT:w" s="T86">min</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_280" n="HIAT:w" s="T87">ɨtɨː</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_283" n="HIAT:w" s="T88">hɨspɨtɨm</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_286" n="HIAT:w" s="T89">Spirja</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_289" n="HIAT:w" s="T90">onno</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_292" n="HIAT:w" s="T91">tu͡ok</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_295" n="HIAT:w" s="T92">ere</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_297" n="HIAT:ip">(</nts>
                  <ts e="T95" id="Seg_299" n="HIAT:w" s="T93">munn-</ts>
                  <nts id="Seg_300" n="HIAT:ip">)</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_303" n="HIAT:w" s="T95">kim</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_306" n="HIAT:w" s="T96">tɨːna</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_308" n="HIAT:ip">(</nts>
                  <ts e="T98" id="Seg_310" n="HIAT:w" s="T97">kaːjɨstallar</ts>
                  <nts id="Seg_311" n="HIAT:ip">)</nts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_314" n="HIAT:w" s="T98">ete</ts>
                  <nts id="Seg_315" n="HIAT:ip">.</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_318" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_320" n="HIAT:w" s="T99">Onton</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_323" n="HIAT:w" s="T100">onton</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_326" n="HIAT:w" s="T101">onno</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_329" n="HIAT:w" s="T102">onton</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_332" n="HIAT:w" s="T103">bihigi</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_335" n="HIAT:w" s="T104">Radanɨ</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_338" n="HIAT:w" s="T105">kim</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_341" n="HIAT:w" s="T106">bihigi</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_344" n="HIAT:w" s="T107">dʼi͡eleːbippit</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_347" n="HIAT:w" s="T108">onton</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_350" n="HIAT:w" s="T109">dʼi͡eleːbippit</ts>
                  <nts id="Seg_351" n="HIAT:ip">.</nts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_354" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_356" n="HIAT:w" s="T110">Onton</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_358" n="HIAT:ip">(</nts>
                  <ts e="T112" id="Seg_360" n="HIAT:w" s="T111">kimneː-</ts>
                  <nts id="Seg_361" n="HIAT:ip">)</nts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_364" n="HIAT:w" s="T112">onton</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_367" n="HIAT:w" s="T113">Spirja</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_370" n="HIAT:w" s="T114">kim</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_372" n="HIAT:ip">(</nts>
                  <ts e="T117" id="Seg_374" n="HIAT:w" s="T115">ospu-</ts>
                  <nts id="Seg_375" n="HIAT:ip">)</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_378" n="HIAT:w" s="T117">e</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_381" n="HIAT:w" s="T118">bartɨbɨt</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_384" n="HIAT:w" s="T119">onno</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_387" n="HIAT:w" s="T120">össü͡ö</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_390" n="HIAT:w" s="T121">katatsalɨː</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_393" n="HIAT:w" s="T122">tüspüppüt</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_396" n="HIAT:w" s="T123">onton</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_399" n="HIAT:w" s="T124">dʼi͡eleːbippit</ts>
                  <nts id="Seg_400" n="HIAT:ip">,</nts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_403" n="HIAT:w" s="T125">elete</ts>
                  <nts id="Seg_404" n="HIAT:ip">.</nts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T126" id="Seg_406" n="sc" s="T0">
               <ts e="T1" id="Seg_408" n="e" s="T0">E </ts>
               <ts e="T2" id="Seg_410" n="e" s="T1">bihigi </ts>
               <ts e="T3" id="Seg_412" n="e" s="T2">kim </ts>
               <ts e="T4" id="Seg_414" n="e" s="T3">gorkaga </ts>
               <ts e="T5" id="Seg_416" n="e" s="T4">katatsalɨː </ts>
               <ts e="T6" id="Seg_418" n="e" s="T5">barar </ts>
               <ts e="T7" id="Seg_420" n="e" s="T6">etibit </ts>
               <ts e="T8" id="Seg_422" n="e" s="T7">Ponomarjovkaga </ts>
               <ts e="T9" id="Seg_424" n="e" s="T8">onton </ts>
               <ts e="T10" id="Seg_426" n="e" s="T9">bihigi </ts>
               <ts e="T11" id="Seg_428" n="e" s="T10">hɨrgalaːk </ts>
               <ts e="T12" id="Seg_430" n="e" s="T11">etibit </ts>
               <ts e="T13" id="Seg_432" n="e" s="T12">Rada </ts>
               <ts e="T14" id="Seg_434" n="e" s="T13">min </ts>
               <ts e="T15" id="Seg_436" n="e" s="T14">Spire. </ts>
               <ts e="T16" id="Seg_438" n="e" s="T15">Onton </ts>
               <ts e="T17" id="Seg_440" n="e" s="T16">bihigi </ts>
               <ts e="T18" id="Seg_442" n="e" s="T17">Spirjanɨ </ts>
               <ts e="T19" id="Seg_444" n="e" s="T18">kɨtta </ts>
               <ts e="T20" id="Seg_446" n="e" s="T19">kalɨjbɨt </ts>
               <ts e="T21" id="Seg_448" n="e" s="T20">etibit. </ts>
               <ts e="T22" id="Seg_450" n="e" s="T21">Onton </ts>
               <ts e="T23" id="Seg_452" n="e" s="T22">kalɨja </ts>
               <ts e="T24" id="Seg_454" n="e" s="T23">turaːktɨːbɨt </ts>
               <ts e="T25" id="Seg_456" n="e" s="T24">di͡en </ts>
               <ts e="T26" id="Seg_458" n="e" s="T25">onton </ts>
               <ts e="T27" id="Seg_460" n="e" s="T26">kim </ts>
               <ts e="T28" id="Seg_462" n="e" s="T27">e </ts>
               <ts e="T29" id="Seg_464" n="e" s="T28">kalɨja </ts>
               <ts e="T30" id="Seg_466" n="e" s="T29">turaːktɨːbɨt </ts>
               <ts e="T31" id="Seg_468" n="e" s="T30">otto </ts>
               <ts e="T32" id="Seg_470" n="e" s="T31">hɨrgabɨt </ts>
               <ts e="T34" id="Seg_472" n="e" s="T32">(ka-) </ts>
               <ts e="T35" id="Seg_474" n="e" s="T34">otto </ts>
               <ts e="T36" id="Seg_476" n="e" s="T35">e </ts>
               <ts e="T37" id="Seg_478" n="e" s="T36">tu͡ok </ts>
               <ts e="T38" id="Seg_480" n="e" s="T37">ere </ts>
               <ts e="T39" id="Seg_482" n="e" s="T38">e </ts>
               <ts e="T40" id="Seg_484" n="e" s="T39">kim </ts>
               <ts e="T41" id="Seg_486" n="e" s="T40">mas </ts>
               <ts e="T42" id="Seg_488" n="e" s="T41">baːr </ts>
               <ts e="T43" id="Seg_490" n="e" s="T42">ete </ts>
               <ts e="T44" id="Seg_492" n="e" s="T43">onno </ts>
               <ts e="T45" id="Seg_494" n="e" s="T44">otto </ts>
               <ts e="T47" id="Seg_496" n="e" s="T45">(tu͡o-). </ts>
               <ts e="T48" id="Seg_498" n="e" s="T47">Kim </ts>
               <ts e="T49" id="Seg_500" n="e" s="T48">onton </ts>
               <ts e="T50" id="Seg_502" n="e" s="T49">hɨrgabɨt </ts>
               <ts e="T51" id="Seg_504" n="e" s="T50">kimneːbite </ts>
               <ts e="T52" id="Seg_506" n="e" s="T51">onno </ts>
               <ts e="T53" id="Seg_508" n="e" s="T52">kam </ts>
               <ts e="T54" id="Seg_510" n="e" s="T53">zastrjallaːbɨppɨt </ts>
               <ts e="T55" id="Seg_512" n="e" s="T54">onton </ts>
               <ts e="T56" id="Seg_514" n="e" s="T55">bihi </ts>
               <ts e="T57" id="Seg_516" n="e" s="T56">tühen </ts>
               <ts e="T59" id="Seg_518" n="e" s="T57">(kaːl-) </ts>
               <ts e="T60" id="Seg_520" n="e" s="T59">kötön </ts>
               <ts e="T61" id="Seg_522" n="e" s="T60">barbɨppɨt. </ts>
               <ts e="T62" id="Seg_524" n="e" s="T61">Onton </ts>
               <ts e="T63" id="Seg_526" n="e" s="T62">hɨrgabɨt </ts>
               <ts e="T64" id="Seg_528" n="e" s="T63">kim </ts>
               <ts e="T65" id="Seg_530" n="e" s="T64">aldʼammɨt </ts>
               <ts e="T66" id="Seg_532" n="e" s="T65">ete </ts>
               <ts e="T67" id="Seg_534" n="e" s="T66">hɨrgabɨt </ts>
               <ts e="T68" id="Seg_536" n="e" s="T67">kim </ts>
               <ts e="T69" id="Seg_538" n="e" s="T68">hɨrgabɨt </ts>
               <ts e="T70" id="Seg_540" n="e" s="T69">kim </ts>
               <ts e="T71" id="Seg_542" n="e" s="T70">kiːl </ts>
               <ts e="T72" id="Seg_544" n="e" s="T71">e </ts>
               <ts e="T73" id="Seg_546" n="e" s="T72">munnuk </ts>
               <ts e="T74" id="Seg_548" n="e" s="T73">tokurujbut </ts>
               <ts e="T75" id="Seg_550" n="e" s="T74">ete </ts>
               <ts e="T76" id="Seg_552" n="e" s="T75">hɨrgabɨt. </ts>
               <ts e="T77" id="Seg_554" n="e" s="T76">Onton </ts>
               <ts e="T78" id="Seg_556" n="e" s="T77">onton </ts>
               <ts e="T79" id="Seg_558" n="e" s="T78">Spirja </ts>
               <ts e="T80" id="Seg_560" n="e" s="T79">tu͡ok </ts>
               <ts e="T81" id="Seg_562" n="e" s="T80">ere </ts>
               <ts e="T82" id="Seg_564" n="e" s="T81">bu͡ola </ts>
               <ts e="T83" id="Seg_566" n="e" s="T82">hɨtar </ts>
               <ts e="T84" id="Seg_568" n="e" s="T83">ete </ts>
               <ts e="T85" id="Seg_570" n="e" s="T84">onno. </ts>
               <ts e="T86" id="Seg_572" n="e" s="T85">Onton </ts>
               <ts e="T87" id="Seg_574" n="e" s="T86">min </ts>
               <ts e="T88" id="Seg_576" n="e" s="T87">ɨtɨː </ts>
               <ts e="T89" id="Seg_578" n="e" s="T88">hɨspɨtɨm </ts>
               <ts e="T90" id="Seg_580" n="e" s="T89">Spirja </ts>
               <ts e="T91" id="Seg_582" n="e" s="T90">onno </ts>
               <ts e="T92" id="Seg_584" n="e" s="T91">tu͡ok </ts>
               <ts e="T93" id="Seg_586" n="e" s="T92">ere </ts>
               <ts e="T95" id="Seg_588" n="e" s="T93">(munn-) </ts>
               <ts e="T96" id="Seg_590" n="e" s="T95">kim </ts>
               <ts e="T97" id="Seg_592" n="e" s="T96">tɨːna </ts>
               <ts e="T98" id="Seg_594" n="e" s="T97">(kaːjɨstallar) </ts>
               <ts e="T99" id="Seg_596" n="e" s="T98">ete. </ts>
               <ts e="T100" id="Seg_598" n="e" s="T99">Onton </ts>
               <ts e="T101" id="Seg_600" n="e" s="T100">onton </ts>
               <ts e="T102" id="Seg_602" n="e" s="T101">onno </ts>
               <ts e="T103" id="Seg_604" n="e" s="T102">onton </ts>
               <ts e="T104" id="Seg_606" n="e" s="T103">bihigi </ts>
               <ts e="T105" id="Seg_608" n="e" s="T104">Radanɨ </ts>
               <ts e="T106" id="Seg_610" n="e" s="T105">kim </ts>
               <ts e="T107" id="Seg_612" n="e" s="T106">bihigi </ts>
               <ts e="T108" id="Seg_614" n="e" s="T107">dʼi͡eleːbippit </ts>
               <ts e="T109" id="Seg_616" n="e" s="T108">onton </ts>
               <ts e="T110" id="Seg_618" n="e" s="T109">dʼi͡eleːbippit. </ts>
               <ts e="T111" id="Seg_620" n="e" s="T110">Onton </ts>
               <ts e="T112" id="Seg_622" n="e" s="T111">(kimneː-) </ts>
               <ts e="T113" id="Seg_624" n="e" s="T112">onton </ts>
               <ts e="T114" id="Seg_626" n="e" s="T113">Spirja </ts>
               <ts e="T115" id="Seg_628" n="e" s="T114">kim </ts>
               <ts e="T117" id="Seg_630" n="e" s="T115">(ospu-) </ts>
               <ts e="T118" id="Seg_632" n="e" s="T117">e </ts>
               <ts e="T119" id="Seg_634" n="e" s="T118">bartɨbɨt </ts>
               <ts e="T120" id="Seg_636" n="e" s="T119">onno </ts>
               <ts e="T121" id="Seg_638" n="e" s="T120">össü͡ö </ts>
               <ts e="T122" id="Seg_640" n="e" s="T121">katatsalɨː </ts>
               <ts e="T123" id="Seg_642" n="e" s="T122">tüspüppüt </ts>
               <ts e="T124" id="Seg_644" n="e" s="T123">onton </ts>
               <ts e="T125" id="Seg_646" n="e" s="T124">dʼi͡eleːbippit, </ts>
               <ts e="T126" id="Seg_648" n="e" s="T125">elete. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T15" id="Seg_649" s="T0">PoKI_2009_Story_nar.001 (001.001)</ta>
            <ta e="T21" id="Seg_650" s="T15">PoKI_2009_Story_nar.002 (001.002)</ta>
            <ta e="T47" id="Seg_651" s="T21">PoKI_2009_Story_nar.003 (001.003)</ta>
            <ta e="T61" id="Seg_652" s="T47">PoKI_2009_Story_nar.004 (001.004)</ta>
            <ta e="T76" id="Seg_653" s="T61">PoKI_2009_Story_nar.005 (001.005)</ta>
            <ta e="T85" id="Seg_654" s="T76">PoKI_2009_Story_nar.006 (001.006)</ta>
            <ta e="T99" id="Seg_655" s="T85">PoKI_2009_Story_nar.007 (001.007)</ta>
            <ta e="T110" id="Seg_656" s="T99">PoKI_2009_Story_nar.008 (001.008)</ta>
            <ta e="T126" id="Seg_657" s="T110">PoKI_2009_Story_nar.009 (001.009)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T15" id="Seg_658" s="T0">Э биһиги ким горкага катассалы барар этибит Пономарёвкага онтон биһиги һыргалак этибит Рада мин Спиря.</ta>
            <ta e="T21" id="Seg_659" s="T15">Онтон биһиги Спиряны кытта калыйбыт этибит.</ta>
            <ta e="T47" id="Seg_660" s="T21">Һы калыйа турактыыбыт диэ онтон ким э калыйа турактыбыт отто һыргабыт ка.. отто э туок эрэ э ким мас баар этэ онно отто туо..</ta>
            <ta e="T61" id="Seg_661" s="T47">Отто һыргабыт кимнэбитэ онно кам застряллаабыппыт онтон биһи түһэн кал.. көтөн барбыппыт.</ta>
            <ta e="T76" id="Seg_662" s="T61">Онтон һыргабыт кимэ алдьаммыт этэ һыргабыт ким һыргабыт ким киилэ муннук токуруйбут этэ һыргабыт.</ta>
            <ta e="T85" id="Seg_663" s="T76">Онтон онтон Спиря туок эрэ буола һытар этэ онно.</ta>
            <ta e="T99" id="Seg_664" s="T85">Онтон мин ытыы һыспытым Спиря онно туок эрэ мунн.. ким тыына кайысталлар этэ.</ta>
            <ta e="T110" id="Seg_665" s="T99">Онтон онтон онно онтон биһиги Раданы ким биһиги дьиэлээбиппит онтон дьиэлээбиппит.</ta>
            <ta e="T126" id="Seg_666" s="T110">Онтон кимнэ онтон Спиря ким.. оспу.. э бартыбыт онно өссүө катасалы түспүппүт онтон дьиэлээбиппит, элэтэ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T15" id="Seg_667" s="T0">E bihigi kim gorkaga katatsalɨː barar etibit Ponomarjovkaga onton bihigi hɨrgalaːk etibit Rada min Spire. </ta>
            <ta e="T21" id="Seg_668" s="T15">Onton bihigi Spirjanɨ kɨtta kalɨjbɨt etibit. </ta>
            <ta e="T47" id="Seg_669" s="T21">Onton kalɨja turaːktɨːbɨt di͡en onton kim e kalɨja turaːktɨːbɨt otto hɨrgabɨt (ka-) otto e tu͡ok ere e kim mas baːr ete onno otto (tu͡o-). </ta>
            <ta e="T61" id="Seg_670" s="T47">Kim onton hɨrgabɨt kimneːbite onno kam zastrjallaːbɨppɨt onton bihi tühen (kaːl -) kötön barbɨppɨt. </ta>
            <ta e="T76" id="Seg_671" s="T61">Onton hɨrgabɨt kim aldʼammɨt ete hɨrgabɨt kim hɨrgabɨt kim kiːl e munnuk tokurujbut ete hɨrgabɨt. </ta>
            <ta e="T85" id="Seg_672" s="T76">Onton onton Spirja tu͡ok ere bu͡ola hɨtar ete onno. </ta>
            <ta e="T99" id="Seg_673" s="T85">Onton min ɨtɨː hɨspɨtɨm Spirja onno tu͡ok ere (munn-) kim tɨːna (kaːjɨstallar) ete. </ta>
            <ta e="T110" id="Seg_674" s="T99">Onton onton onno onton bihigi Radanɨ kim bihigi dʼi͡eleːbippit onton dʼi͡eleːbippit. </ta>
            <ta e="T126" id="Seg_675" s="T110">Onton (kimneː-) onton Spirja kim (ospu-) e bartɨbɨt onno össü͡ö katatsalɨː tüspüppüt onton dʼi͡eleːbippit, elete. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_676" s="T0">e</ta>
            <ta e="T2" id="Seg_677" s="T1">bihigi</ta>
            <ta e="T3" id="Seg_678" s="T2">kim</ta>
            <ta e="T4" id="Seg_679" s="T3">gorka-ga</ta>
            <ta e="T5" id="Seg_680" s="T4">katatsa-l-ɨː</ta>
            <ta e="T6" id="Seg_681" s="T5">bar-ar</ta>
            <ta e="T7" id="Seg_682" s="T6">e-ti-bit</ta>
            <ta e="T8" id="Seg_683" s="T7">Ponomarjovka-ga</ta>
            <ta e="T9" id="Seg_684" s="T8">onton</ta>
            <ta e="T10" id="Seg_685" s="T9">bihigi</ta>
            <ta e="T11" id="Seg_686" s="T10">hɨrga-laːk</ta>
            <ta e="T12" id="Seg_687" s="T11">e-ti-bit</ta>
            <ta e="T13" id="Seg_688" s="T12">Rada</ta>
            <ta e="T14" id="Seg_689" s="T13">min</ta>
            <ta e="T15" id="Seg_690" s="T14">Spire</ta>
            <ta e="T16" id="Seg_691" s="T15">onton</ta>
            <ta e="T17" id="Seg_692" s="T16">bihigi</ta>
            <ta e="T18" id="Seg_693" s="T17">Spirja-nɨ</ta>
            <ta e="T19" id="Seg_694" s="T18">kɨtta</ta>
            <ta e="T20" id="Seg_695" s="T19">kalɨj-bɨt</ta>
            <ta e="T21" id="Seg_696" s="T20">e-ti-bit</ta>
            <ta e="T22" id="Seg_697" s="T21">onton</ta>
            <ta e="T23" id="Seg_698" s="T22">kalɨj-a</ta>
            <ta e="T24" id="Seg_699" s="T23">tur-aːkt-ɨː-bɨt</ta>
            <ta e="T25" id="Seg_700" s="T24">di͡e-n</ta>
            <ta e="T26" id="Seg_701" s="T25">onton</ta>
            <ta e="T27" id="Seg_702" s="T26">kim</ta>
            <ta e="T28" id="Seg_703" s="T27">e</ta>
            <ta e="T29" id="Seg_704" s="T28">kalɨj-a</ta>
            <ta e="T30" id="Seg_705" s="T29">tur-aːkt-ɨː-bɨt</ta>
            <ta e="T31" id="Seg_706" s="T30">otto</ta>
            <ta e="T32" id="Seg_707" s="T31">hɨrga-bɨt</ta>
            <ta e="T35" id="Seg_708" s="T34">otto</ta>
            <ta e="T36" id="Seg_709" s="T35">e</ta>
            <ta e="T37" id="Seg_710" s="T36">tu͡ok</ta>
            <ta e="T38" id="Seg_711" s="T37">ere</ta>
            <ta e="T39" id="Seg_712" s="T38">e</ta>
            <ta e="T40" id="Seg_713" s="T39">kim</ta>
            <ta e="T41" id="Seg_714" s="T40">mas</ta>
            <ta e="T42" id="Seg_715" s="T41">baːr</ta>
            <ta e="T43" id="Seg_716" s="T42">e-t-e</ta>
            <ta e="T44" id="Seg_717" s="T43">onno</ta>
            <ta e="T45" id="Seg_718" s="T44">otto</ta>
            <ta e="T48" id="Seg_719" s="T47">kim</ta>
            <ta e="T49" id="Seg_720" s="T48">onton</ta>
            <ta e="T50" id="Seg_721" s="T49">hɨrga-bɨt</ta>
            <ta e="T51" id="Seg_722" s="T50">kim-neː-bit-e</ta>
            <ta e="T52" id="Seg_723" s="T51">onno</ta>
            <ta e="T53" id="Seg_724" s="T52">kam</ta>
            <ta e="T54" id="Seg_725" s="T53">zastrjal-laː-bɨp-pɨt</ta>
            <ta e="T55" id="Seg_726" s="T54">onton</ta>
            <ta e="T56" id="Seg_727" s="T55">bihi</ta>
            <ta e="T57" id="Seg_728" s="T56">tüh-en</ta>
            <ta e="T60" id="Seg_729" s="T59">köt-ön</ta>
            <ta e="T61" id="Seg_730" s="T60">bar-bɨp-pɨt</ta>
            <ta e="T62" id="Seg_731" s="T61">onton</ta>
            <ta e="T63" id="Seg_732" s="T62">hɨrga-bɨt</ta>
            <ta e="T64" id="Seg_733" s="T63">kim</ta>
            <ta e="T65" id="Seg_734" s="T64">aldʼam-mɨt</ta>
            <ta e="T66" id="Seg_735" s="T65">e-t-e</ta>
            <ta e="T67" id="Seg_736" s="T66">hɨrga-bɨt</ta>
            <ta e="T68" id="Seg_737" s="T67">kim</ta>
            <ta e="T69" id="Seg_738" s="T68">hɨrga-bɨt</ta>
            <ta e="T70" id="Seg_739" s="T69">kim</ta>
            <ta e="T71" id="Seg_740" s="T70">kiːl</ta>
            <ta e="T72" id="Seg_741" s="T71">e</ta>
            <ta e="T73" id="Seg_742" s="T72">munnuk</ta>
            <ta e="T74" id="Seg_743" s="T73">tokuruj-but</ta>
            <ta e="T75" id="Seg_744" s="T74">e-t-e</ta>
            <ta e="T76" id="Seg_745" s="T75">hɨrga-bɨt</ta>
            <ta e="T77" id="Seg_746" s="T76">onton</ta>
            <ta e="T78" id="Seg_747" s="T77">onton</ta>
            <ta e="T79" id="Seg_748" s="T78">Spirja</ta>
            <ta e="T80" id="Seg_749" s="T79">tu͡ok</ta>
            <ta e="T81" id="Seg_750" s="T80">ere</ta>
            <ta e="T82" id="Seg_751" s="T81">bu͡ol-a</ta>
            <ta e="T83" id="Seg_752" s="T82">hɨt-ar</ta>
            <ta e="T84" id="Seg_753" s="T83">e-t-e</ta>
            <ta e="T85" id="Seg_754" s="T84">onno</ta>
            <ta e="T86" id="Seg_755" s="T85">onton</ta>
            <ta e="T87" id="Seg_756" s="T86">min</ta>
            <ta e="T88" id="Seg_757" s="T87">ɨt-ɨː</ta>
            <ta e="T89" id="Seg_758" s="T88">hɨs-pɨt-ɨ-m</ta>
            <ta e="T90" id="Seg_759" s="T89">Spirja</ta>
            <ta e="T91" id="Seg_760" s="T90">onno</ta>
            <ta e="T92" id="Seg_761" s="T91">tu͡ok</ta>
            <ta e="T93" id="Seg_762" s="T92">ere</ta>
            <ta e="T96" id="Seg_763" s="T95">kim</ta>
            <ta e="T97" id="Seg_764" s="T96">tɨːn-a</ta>
            <ta e="T98" id="Seg_765" s="T97">kaːjɨstall-ar</ta>
            <ta e="T99" id="Seg_766" s="T98">e-t-e</ta>
            <ta e="T100" id="Seg_767" s="T99">onton</ta>
            <ta e="T101" id="Seg_768" s="T100">onton</ta>
            <ta e="T102" id="Seg_769" s="T101">onno</ta>
            <ta e="T103" id="Seg_770" s="T102">onton</ta>
            <ta e="T104" id="Seg_771" s="T103">bihigi</ta>
            <ta e="T105" id="Seg_772" s="T104">Rada-nɨ</ta>
            <ta e="T106" id="Seg_773" s="T105">kim</ta>
            <ta e="T107" id="Seg_774" s="T106">bihigi</ta>
            <ta e="T108" id="Seg_775" s="T107">dʼi͡e-leː-bip-pit</ta>
            <ta e="T109" id="Seg_776" s="T108">onton</ta>
            <ta e="T110" id="Seg_777" s="T109">dʼi͡e-leː-bip-pit</ta>
            <ta e="T111" id="Seg_778" s="T110">onton</ta>
            <ta e="T112" id="Seg_779" s="T111">kim-neː</ta>
            <ta e="T113" id="Seg_780" s="T112">onton</ta>
            <ta e="T114" id="Seg_781" s="T113">Spirja</ta>
            <ta e="T115" id="Seg_782" s="T114">kim</ta>
            <ta e="T118" id="Seg_783" s="T117">e</ta>
            <ta e="T119" id="Seg_784" s="T118">bar-tɨ-bɨt</ta>
            <ta e="T120" id="Seg_785" s="T119">onno</ta>
            <ta e="T121" id="Seg_786" s="T120">össü͡ö</ta>
            <ta e="T122" id="Seg_787" s="T121">katatsa-l-ɨː</ta>
            <ta e="T123" id="Seg_788" s="T122">tüs-püp-püt</ta>
            <ta e="T124" id="Seg_789" s="T123">onton</ta>
            <ta e="T125" id="Seg_790" s="T124">dʼi͡e-leː-bip-pit</ta>
            <ta e="T126" id="Seg_791" s="T125">ele-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_792" s="T0">e</ta>
            <ta e="T2" id="Seg_793" s="T1">bihigi</ta>
            <ta e="T3" id="Seg_794" s="T2">kim</ta>
            <ta e="T4" id="Seg_795" s="T3">gorka-GA</ta>
            <ta e="T5" id="Seg_796" s="T4">katatsa-LAː-A</ta>
            <ta e="T6" id="Seg_797" s="T5">bar-Ar</ta>
            <ta e="T7" id="Seg_798" s="T6">e-TI-BIt</ta>
            <ta e="T8" id="Seg_799" s="T7">Ponomarjovka-GA</ta>
            <ta e="T9" id="Seg_800" s="T8">onton</ta>
            <ta e="T10" id="Seg_801" s="T9">bihigi</ta>
            <ta e="T11" id="Seg_802" s="T10">hɨrga-LAːK</ta>
            <ta e="T12" id="Seg_803" s="T11">e-TI-BIt</ta>
            <ta e="T13" id="Seg_804" s="T12">Rada</ta>
            <ta e="T14" id="Seg_805" s="T13">min</ta>
            <ta e="T15" id="Seg_806" s="T14">Spire</ta>
            <ta e="T16" id="Seg_807" s="T15">onton</ta>
            <ta e="T17" id="Seg_808" s="T16">bihigi</ta>
            <ta e="T18" id="Seg_809" s="T17">Spire-nI</ta>
            <ta e="T19" id="Seg_810" s="T18">kɨtta</ta>
            <ta e="T20" id="Seg_811" s="T19">kalɨj-BIT</ta>
            <ta e="T21" id="Seg_812" s="T20">e-TI-BIt</ta>
            <ta e="T22" id="Seg_813" s="T21">onton</ta>
            <ta e="T23" id="Seg_814" s="T22">kalɨj-A</ta>
            <ta e="T24" id="Seg_815" s="T23">tur-AːktAː-A-BIt</ta>
            <ta e="T25" id="Seg_816" s="T24">di͡e-An</ta>
            <ta e="T26" id="Seg_817" s="T25">onton</ta>
            <ta e="T27" id="Seg_818" s="T26">kim</ta>
            <ta e="T28" id="Seg_819" s="T27">e</ta>
            <ta e="T29" id="Seg_820" s="T28">kalɨj-A</ta>
            <ta e="T30" id="Seg_821" s="T29">tur-AːktAː-A-BIt</ta>
            <ta e="T31" id="Seg_822" s="T30">onton</ta>
            <ta e="T32" id="Seg_823" s="T31">hɨrga-BIt</ta>
            <ta e="T35" id="Seg_824" s="T34">onton</ta>
            <ta e="T36" id="Seg_825" s="T35">e</ta>
            <ta e="T37" id="Seg_826" s="T36">tu͡ok</ta>
            <ta e="T38" id="Seg_827" s="T37">ere</ta>
            <ta e="T39" id="Seg_828" s="T38">e</ta>
            <ta e="T40" id="Seg_829" s="T39">kim</ta>
            <ta e="T41" id="Seg_830" s="T40">mas</ta>
            <ta e="T42" id="Seg_831" s="T41">baːr</ta>
            <ta e="T43" id="Seg_832" s="T42">e-TI-tA</ta>
            <ta e="T44" id="Seg_833" s="T43">onno</ta>
            <ta e="T45" id="Seg_834" s="T44">onton</ta>
            <ta e="T48" id="Seg_835" s="T47">kim</ta>
            <ta e="T49" id="Seg_836" s="T48">onton</ta>
            <ta e="T50" id="Seg_837" s="T49">hɨrga-BIt</ta>
            <ta e="T51" id="Seg_838" s="T50">kim-LAː-BIT-tA</ta>
            <ta e="T52" id="Seg_839" s="T51">onno</ta>
            <ta e="T53" id="Seg_840" s="T52">kam</ta>
            <ta e="T54" id="Seg_841" s="T53">zastrjal-LAː-BIT-BIt</ta>
            <ta e="T55" id="Seg_842" s="T54">onton</ta>
            <ta e="T56" id="Seg_843" s="T55">bihigi</ta>
            <ta e="T57" id="Seg_844" s="T56">tüs-An</ta>
            <ta e="T60" id="Seg_845" s="T59">köt-An</ta>
            <ta e="T61" id="Seg_846" s="T60">bar-BIT-BIt</ta>
            <ta e="T62" id="Seg_847" s="T61">onton</ta>
            <ta e="T63" id="Seg_848" s="T62">hɨrga-BIt</ta>
            <ta e="T64" id="Seg_849" s="T63">kim</ta>
            <ta e="T65" id="Seg_850" s="T64">aldʼan-BIT</ta>
            <ta e="T66" id="Seg_851" s="T65">e-TI-tA</ta>
            <ta e="T67" id="Seg_852" s="T66">hɨrga-BIt</ta>
            <ta e="T68" id="Seg_853" s="T67">kim</ta>
            <ta e="T69" id="Seg_854" s="T68">hɨrga-BIt</ta>
            <ta e="T70" id="Seg_855" s="T69">kim</ta>
            <ta e="T71" id="Seg_856" s="T70">kiːl</ta>
            <ta e="T72" id="Seg_857" s="T71">e</ta>
            <ta e="T73" id="Seg_858" s="T72">mannɨk</ta>
            <ta e="T74" id="Seg_859" s="T73">tokuruj-BIT</ta>
            <ta e="T75" id="Seg_860" s="T74">e-TI-tA</ta>
            <ta e="T76" id="Seg_861" s="T75">hɨrga-BIt</ta>
            <ta e="T77" id="Seg_862" s="T76">onton</ta>
            <ta e="T78" id="Seg_863" s="T77">onton</ta>
            <ta e="T79" id="Seg_864" s="T78">Spire</ta>
            <ta e="T80" id="Seg_865" s="T79">tu͡ok</ta>
            <ta e="T81" id="Seg_866" s="T80">ere</ta>
            <ta e="T82" id="Seg_867" s="T81">bu͡ol-A</ta>
            <ta e="T83" id="Seg_868" s="T82">hɨt-Ar</ta>
            <ta e="T84" id="Seg_869" s="T83">e-TI-tA</ta>
            <ta e="T85" id="Seg_870" s="T84">onno</ta>
            <ta e="T86" id="Seg_871" s="T85">onton</ta>
            <ta e="T87" id="Seg_872" s="T86">min</ta>
            <ta e="T88" id="Seg_873" s="T87">ɨtaː-A</ta>
            <ta e="T89" id="Seg_874" s="T88">hɨs-BIT-I-m</ta>
            <ta e="T90" id="Seg_875" s="T89">Spire</ta>
            <ta e="T91" id="Seg_876" s="T90">onno</ta>
            <ta e="T92" id="Seg_877" s="T91">tu͡ok</ta>
            <ta e="T93" id="Seg_878" s="T92">ere</ta>
            <ta e="T96" id="Seg_879" s="T95">kim</ta>
            <ta e="T97" id="Seg_880" s="T96">tɨːn-tA</ta>
            <ta e="T98" id="Seg_881" s="T97">kaːjɨstalɨn-Ar</ta>
            <ta e="T99" id="Seg_882" s="T98">e-TI-tA</ta>
            <ta e="T100" id="Seg_883" s="T99">onton</ta>
            <ta e="T101" id="Seg_884" s="T100">onton</ta>
            <ta e="T102" id="Seg_885" s="T101">onno</ta>
            <ta e="T103" id="Seg_886" s="T102">onton</ta>
            <ta e="T104" id="Seg_887" s="T103">bihigi</ta>
            <ta e="T105" id="Seg_888" s="T104">Rada-nI</ta>
            <ta e="T106" id="Seg_889" s="T105">kim</ta>
            <ta e="T107" id="Seg_890" s="T106">bihigi</ta>
            <ta e="T108" id="Seg_891" s="T107">dʼi͡e-LAː-BIT-BIt</ta>
            <ta e="T109" id="Seg_892" s="T108">onton</ta>
            <ta e="T110" id="Seg_893" s="T109">dʼi͡e-LAː-BIT-BIt</ta>
            <ta e="T111" id="Seg_894" s="T110">onton</ta>
            <ta e="T112" id="Seg_895" s="T111">kim-LAː</ta>
            <ta e="T113" id="Seg_896" s="T112">onton</ta>
            <ta e="T114" id="Seg_897" s="T113">Spire</ta>
            <ta e="T115" id="Seg_898" s="T114">kim</ta>
            <ta e="T118" id="Seg_899" s="T117">e</ta>
            <ta e="T119" id="Seg_900" s="T118">bar-TI-BIt</ta>
            <ta e="T120" id="Seg_901" s="T119">onno</ta>
            <ta e="T121" id="Seg_902" s="T120">össü͡ö</ta>
            <ta e="T122" id="Seg_903" s="T121">katatsa-LAː-A</ta>
            <ta e="T123" id="Seg_904" s="T122">tüs-BIT-BIt</ta>
            <ta e="T124" id="Seg_905" s="T123">onton</ta>
            <ta e="T125" id="Seg_906" s="T124">dʼi͡e-LAː-BIT-BIt</ta>
            <ta e="T126" id="Seg_907" s="T125">ele-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_908" s="T0">eh</ta>
            <ta e="T2" id="Seg_909" s="T1">1PL.[NOM]</ta>
            <ta e="T3" id="Seg_910" s="T2">who.[NOM]</ta>
            <ta e="T4" id="Seg_911" s="T3">hill-DAT/LOC</ta>
            <ta e="T5" id="Seg_912" s="T4">ride-VBZ-CVB.SIM</ta>
            <ta e="T6" id="Seg_913" s="T5">go-PTCP.PRS</ta>
            <ta e="T7" id="Seg_914" s="T6">be-PST1-1PL</ta>
            <ta e="T8" id="Seg_915" s="T7">Ponomaryovka-DAT/LOC</ta>
            <ta e="T9" id="Seg_916" s="T8">then</ta>
            <ta e="T10" id="Seg_917" s="T9">1PL.[NOM]</ta>
            <ta e="T11" id="Seg_918" s="T10">sledge-PROPR</ta>
            <ta e="T12" id="Seg_919" s="T11">be-PST1-1PL</ta>
            <ta e="T13" id="Seg_920" s="T12">Rada.[NOM]</ta>
            <ta e="T14" id="Seg_921" s="T13">1SG.[NOM]</ta>
            <ta e="T15" id="Seg_922" s="T14">Spiridon.[NOM]</ta>
            <ta e="T16" id="Seg_923" s="T15">then</ta>
            <ta e="T17" id="Seg_924" s="T16">1PL.[NOM]</ta>
            <ta e="T18" id="Seg_925" s="T17">Spiridon-ACC</ta>
            <ta e="T19" id="Seg_926" s="T18">with</ta>
            <ta e="T20" id="Seg_927" s="T19">glide-PTCP.PST</ta>
            <ta e="T21" id="Seg_928" s="T20">be-PST1-1PL</ta>
            <ta e="T22" id="Seg_929" s="T21">then</ta>
            <ta e="T23" id="Seg_930" s="T22">glide-CVB.SIM</ta>
            <ta e="T24" id="Seg_931" s="T23">stand-EMOT-PRS-1PL</ta>
            <ta e="T25" id="Seg_932" s="T24">say-CVB.SEQ</ta>
            <ta e="T26" id="Seg_933" s="T25">then</ta>
            <ta e="T27" id="Seg_934" s="T26">who.[NOM]</ta>
            <ta e="T28" id="Seg_935" s="T27">eh</ta>
            <ta e="T29" id="Seg_936" s="T28">glide-CVB.SIM</ta>
            <ta e="T30" id="Seg_937" s="T29">stand-EMOT-PRS-1PL</ta>
            <ta e="T31" id="Seg_938" s="T30">then</ta>
            <ta e="T32" id="Seg_939" s="T31">sledge-1PL.[NOM]</ta>
            <ta e="T35" id="Seg_940" s="T34">then</ta>
            <ta e="T36" id="Seg_941" s="T35">eh</ta>
            <ta e="T37" id="Seg_942" s="T36">what.[NOM]</ta>
            <ta e="T38" id="Seg_943" s="T37">INDEF</ta>
            <ta e="T39" id="Seg_944" s="T38">eh</ta>
            <ta e="T40" id="Seg_945" s="T39">who.[NOM]</ta>
            <ta e="T41" id="Seg_946" s="T40">wood.[NOM]</ta>
            <ta e="T42" id="Seg_947" s="T41">there.is</ta>
            <ta e="T43" id="Seg_948" s="T42">be-PST1-3SG</ta>
            <ta e="T44" id="Seg_949" s="T43">there</ta>
            <ta e="T45" id="Seg_950" s="T44">then</ta>
            <ta e="T48" id="Seg_951" s="T47">who.[NOM]</ta>
            <ta e="T49" id="Seg_952" s="T48">then</ta>
            <ta e="T50" id="Seg_953" s="T49">sledge-1PL.[NOM]</ta>
            <ta e="T51" id="Seg_954" s="T50">who-VBZ-PST2-3SG</ta>
            <ta e="T52" id="Seg_955" s="T51">there</ta>
            <ta e="T53" id="Seg_956" s="T52">strongly</ta>
            <ta e="T54" id="Seg_957" s="T53">get.stuck-VBZ-PST2-1PL</ta>
            <ta e="T55" id="Seg_958" s="T54">then</ta>
            <ta e="T56" id="Seg_959" s="T55">1PL.[NOM]</ta>
            <ta e="T57" id="Seg_960" s="T56">fall-CVB.SEQ</ta>
            <ta e="T60" id="Seg_961" s="T59">fly-CVB.SEQ</ta>
            <ta e="T61" id="Seg_962" s="T60">go-PST2-1PL</ta>
            <ta e="T62" id="Seg_963" s="T61">then</ta>
            <ta e="T63" id="Seg_964" s="T62">sledge-1PL.[NOM]</ta>
            <ta e="T64" id="Seg_965" s="T63">who.[NOM]</ta>
            <ta e="T65" id="Seg_966" s="T64">break-PTCP.PST</ta>
            <ta e="T66" id="Seg_967" s="T65">be-PST1-3SG</ta>
            <ta e="T67" id="Seg_968" s="T66">sledge-1PL.[NOM]</ta>
            <ta e="T68" id="Seg_969" s="T67">who.[NOM]</ta>
            <ta e="T69" id="Seg_970" s="T68">sledge-1PL.[NOM]</ta>
            <ta e="T70" id="Seg_971" s="T69">who.[NOM]</ta>
            <ta e="T71" id="Seg_972" s="T70">sleigh.runner.[NOM]</ta>
            <ta e="T72" id="Seg_973" s="T71">eh</ta>
            <ta e="T73" id="Seg_974" s="T72">such</ta>
            <ta e="T74" id="Seg_975" s="T73">bend-PTCP.PST</ta>
            <ta e="T75" id="Seg_976" s="T74">be-PST1-3SG</ta>
            <ta e="T76" id="Seg_977" s="T75">sledge-1PL.[NOM]</ta>
            <ta e="T77" id="Seg_978" s="T76">then</ta>
            <ta e="T78" id="Seg_979" s="T77">then</ta>
            <ta e="T79" id="Seg_980" s="T78">Spiridon.[NOM]</ta>
            <ta e="T80" id="Seg_981" s="T79">what.[NOM]</ta>
            <ta e="T81" id="Seg_982" s="T80">INDEF</ta>
            <ta e="T82" id="Seg_983" s="T81">be-CVB.SIM</ta>
            <ta e="T83" id="Seg_984" s="T82">lie-PTCP.PRS</ta>
            <ta e="T84" id="Seg_985" s="T83">be-PST1-3SG</ta>
            <ta e="T85" id="Seg_986" s="T84">there</ta>
            <ta e="T86" id="Seg_987" s="T85">then</ta>
            <ta e="T87" id="Seg_988" s="T86">1SG.[NOM]</ta>
            <ta e="T88" id="Seg_989" s="T87">cry-CVB.SIM</ta>
            <ta e="T89" id="Seg_990" s="T88">almost.do-PST2-EP-1SG</ta>
            <ta e="T90" id="Seg_991" s="T89">Spiridon.[NOM]</ta>
            <ta e="T91" id="Seg_992" s="T90">there</ta>
            <ta e="T92" id="Seg_993" s="T91">what.[NOM]</ta>
            <ta e="T93" id="Seg_994" s="T92">INDEF</ta>
            <ta e="T96" id="Seg_995" s="T95">who.[NOM]</ta>
            <ta e="T97" id="Seg_996" s="T96">breath-3SG.[NOM]</ta>
            <ta e="T98" id="Seg_997" s="T97">wheeze-PTCP.PRS</ta>
            <ta e="T99" id="Seg_998" s="T98">be-PST1-3SG</ta>
            <ta e="T100" id="Seg_999" s="T99">then</ta>
            <ta e="T101" id="Seg_1000" s="T100">then</ta>
            <ta e="T102" id="Seg_1001" s="T101">there</ta>
            <ta e="T103" id="Seg_1002" s="T102">then</ta>
            <ta e="T104" id="Seg_1003" s="T103">1PL.[NOM]</ta>
            <ta e="T105" id="Seg_1004" s="T104">Rada-ACC</ta>
            <ta e="T106" id="Seg_1005" s="T105">who.[NOM]</ta>
            <ta e="T107" id="Seg_1006" s="T106">1PL.[NOM]</ta>
            <ta e="T108" id="Seg_1007" s="T107">house-VBZ-PST2-1PL</ta>
            <ta e="T109" id="Seg_1008" s="T108">then</ta>
            <ta e="T110" id="Seg_1009" s="T109">house-VBZ-PST2-1PL</ta>
            <ta e="T111" id="Seg_1010" s="T110">then</ta>
            <ta e="T112" id="Seg_1011" s="T111">who-VBZ</ta>
            <ta e="T113" id="Seg_1012" s="T112">then</ta>
            <ta e="T114" id="Seg_1013" s="T113">Spiridon.[NOM]</ta>
            <ta e="T115" id="Seg_1014" s="T114">who.[NOM]</ta>
            <ta e="T118" id="Seg_1015" s="T117">eh</ta>
            <ta e="T119" id="Seg_1016" s="T118">go-PST1-1PL</ta>
            <ta e="T120" id="Seg_1017" s="T119">there</ta>
            <ta e="T121" id="Seg_1018" s="T120">still</ta>
            <ta e="T122" id="Seg_1019" s="T121">ride-VBZ-CVB.SIM</ta>
            <ta e="T123" id="Seg_1020" s="T122">fall-PST2-1PL</ta>
            <ta e="T124" id="Seg_1021" s="T123">then</ta>
            <ta e="T125" id="Seg_1022" s="T124">house-VBZ-PST2-1PL</ta>
            <ta e="T126" id="Seg_1023" s="T125">last-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_1024" s="T0">äh</ta>
            <ta e="T2" id="Seg_1025" s="T1">1PL.[NOM]</ta>
            <ta e="T3" id="Seg_1026" s="T2">wer.[NOM]</ta>
            <ta e="T4" id="Seg_1027" s="T3">Hügel-DAT/LOC</ta>
            <ta e="T5" id="Seg_1028" s="T4">fahren-VBZ-CVB.SIM</ta>
            <ta e="T6" id="Seg_1029" s="T5">gehen-PTCP.PRS</ta>
            <ta e="T7" id="Seg_1030" s="T6">sein-PST1-1PL</ta>
            <ta e="T8" id="Seg_1031" s="T7">Ponomarjovka-DAT/LOC</ta>
            <ta e="T9" id="Seg_1032" s="T8">dann</ta>
            <ta e="T10" id="Seg_1033" s="T9">1PL.[NOM]</ta>
            <ta e="T11" id="Seg_1034" s="T10">Schlitten-PROPR</ta>
            <ta e="T12" id="Seg_1035" s="T11">sein-PST1-1PL</ta>
            <ta e="T13" id="Seg_1036" s="T12">Rada.[NOM]</ta>
            <ta e="T14" id="Seg_1037" s="T13">1SG.[NOM]</ta>
            <ta e="T15" id="Seg_1038" s="T14">Spiridon.[NOM]</ta>
            <ta e="T16" id="Seg_1039" s="T15">dann</ta>
            <ta e="T17" id="Seg_1040" s="T16">1PL.[NOM]</ta>
            <ta e="T18" id="Seg_1041" s="T17">Spiridon-ACC</ta>
            <ta e="T19" id="Seg_1042" s="T18">mit</ta>
            <ta e="T20" id="Seg_1043" s="T19">gleiten-PTCP.PST</ta>
            <ta e="T21" id="Seg_1044" s="T20">sein-PST1-1PL</ta>
            <ta e="T22" id="Seg_1045" s="T21">dann</ta>
            <ta e="T23" id="Seg_1046" s="T22">gleiten-CVB.SIM</ta>
            <ta e="T24" id="Seg_1047" s="T23">stehen-EMOT-PRS-1PL</ta>
            <ta e="T25" id="Seg_1048" s="T24">sagen-CVB.SEQ</ta>
            <ta e="T26" id="Seg_1049" s="T25">dann</ta>
            <ta e="T27" id="Seg_1050" s="T26">wer.[NOM]</ta>
            <ta e="T28" id="Seg_1051" s="T27">äh</ta>
            <ta e="T29" id="Seg_1052" s="T28">gleiten-CVB.SIM</ta>
            <ta e="T30" id="Seg_1053" s="T29">stehen-EMOT-PRS-1PL</ta>
            <ta e="T31" id="Seg_1054" s="T30">dann</ta>
            <ta e="T32" id="Seg_1055" s="T31">Schlitten-1PL.[NOM]</ta>
            <ta e="T35" id="Seg_1056" s="T34">dann</ta>
            <ta e="T36" id="Seg_1057" s="T35">äh</ta>
            <ta e="T37" id="Seg_1058" s="T36">was.[NOM]</ta>
            <ta e="T38" id="Seg_1059" s="T37">INDEF</ta>
            <ta e="T39" id="Seg_1060" s="T38">äh</ta>
            <ta e="T40" id="Seg_1061" s="T39">wer.[NOM]</ta>
            <ta e="T41" id="Seg_1062" s="T40">Holz.[NOM]</ta>
            <ta e="T42" id="Seg_1063" s="T41">es.gibt</ta>
            <ta e="T43" id="Seg_1064" s="T42">sein-PST1-3SG</ta>
            <ta e="T44" id="Seg_1065" s="T43">dort</ta>
            <ta e="T45" id="Seg_1066" s="T44">dann</ta>
            <ta e="T48" id="Seg_1067" s="T47">wer.[NOM]</ta>
            <ta e="T49" id="Seg_1068" s="T48">dann</ta>
            <ta e="T50" id="Seg_1069" s="T49">Schlitten-1PL.[NOM]</ta>
            <ta e="T51" id="Seg_1070" s="T50">wer-VBZ-PST2-3SG</ta>
            <ta e="T52" id="Seg_1071" s="T51">dort</ta>
            <ta e="T53" id="Seg_1072" s="T52">heftig</ta>
            <ta e="T54" id="Seg_1073" s="T53">feststecken-VBZ-PST2-1PL</ta>
            <ta e="T55" id="Seg_1074" s="T54">dann</ta>
            <ta e="T56" id="Seg_1075" s="T55">1PL.[NOM]</ta>
            <ta e="T57" id="Seg_1076" s="T56">fallen-CVB.SEQ</ta>
            <ta e="T60" id="Seg_1077" s="T59">fliegen-CVB.SEQ</ta>
            <ta e="T61" id="Seg_1078" s="T60">gehen-PST2-1PL</ta>
            <ta e="T62" id="Seg_1079" s="T61">dann</ta>
            <ta e="T63" id="Seg_1080" s="T62">Schlitten-1PL.[NOM]</ta>
            <ta e="T64" id="Seg_1081" s="T63">wer.[NOM]</ta>
            <ta e="T65" id="Seg_1082" s="T64">brechen-PTCP.PST</ta>
            <ta e="T66" id="Seg_1083" s="T65">sein-PST1-3SG</ta>
            <ta e="T67" id="Seg_1084" s="T66">Schlitten-1PL.[NOM]</ta>
            <ta e="T68" id="Seg_1085" s="T67">wer.[NOM]</ta>
            <ta e="T69" id="Seg_1086" s="T68">Schlitten-1PL.[NOM]</ta>
            <ta e="T70" id="Seg_1087" s="T69">wer.[NOM]</ta>
            <ta e="T71" id="Seg_1088" s="T70">Schlittenkufe.[NOM]</ta>
            <ta e="T72" id="Seg_1089" s="T71">äh</ta>
            <ta e="T73" id="Seg_1090" s="T72">solch</ta>
            <ta e="T74" id="Seg_1091" s="T73">biegen-PTCP.PST</ta>
            <ta e="T75" id="Seg_1092" s="T74">sein-PST1-3SG</ta>
            <ta e="T76" id="Seg_1093" s="T75">Schlitten-1PL.[NOM]</ta>
            <ta e="T77" id="Seg_1094" s="T76">dann</ta>
            <ta e="T78" id="Seg_1095" s="T77">dann</ta>
            <ta e="T79" id="Seg_1096" s="T78">Spiridon.[NOM]</ta>
            <ta e="T80" id="Seg_1097" s="T79">was.[NOM]</ta>
            <ta e="T81" id="Seg_1098" s="T80">INDEF</ta>
            <ta e="T82" id="Seg_1099" s="T81">sein-CVB.SIM</ta>
            <ta e="T83" id="Seg_1100" s="T82">liegen-PTCP.PRS</ta>
            <ta e="T84" id="Seg_1101" s="T83">sein-PST1-3SG</ta>
            <ta e="T85" id="Seg_1102" s="T84">dort</ta>
            <ta e="T86" id="Seg_1103" s="T85">dann</ta>
            <ta e="T87" id="Seg_1104" s="T86">1SG.[NOM]</ta>
            <ta e="T88" id="Seg_1105" s="T87">weinen-CVB.SIM</ta>
            <ta e="T89" id="Seg_1106" s="T88">fast.tun-PST2-EP-1SG</ta>
            <ta e="T90" id="Seg_1107" s="T89">Spiridon.[NOM]</ta>
            <ta e="T91" id="Seg_1108" s="T90">dort</ta>
            <ta e="T92" id="Seg_1109" s="T91">was.[NOM]</ta>
            <ta e="T93" id="Seg_1110" s="T92">INDEF</ta>
            <ta e="T96" id="Seg_1111" s="T95">wer.[NOM]</ta>
            <ta e="T97" id="Seg_1112" s="T96">Atem-3SG.[NOM]</ta>
            <ta e="T98" id="Seg_1113" s="T97">schnaufen-PTCP.PRS</ta>
            <ta e="T99" id="Seg_1114" s="T98">sein-PST1-3SG</ta>
            <ta e="T100" id="Seg_1115" s="T99">dann</ta>
            <ta e="T101" id="Seg_1116" s="T100">dann</ta>
            <ta e="T102" id="Seg_1117" s="T101">dort</ta>
            <ta e="T103" id="Seg_1118" s="T102">dann</ta>
            <ta e="T104" id="Seg_1119" s="T103">1PL.[NOM]</ta>
            <ta e="T105" id="Seg_1120" s="T104">Rada-ACC</ta>
            <ta e="T106" id="Seg_1121" s="T105">wer.[NOM]</ta>
            <ta e="T107" id="Seg_1122" s="T106">1PL.[NOM]</ta>
            <ta e="T108" id="Seg_1123" s="T107">Haus-VBZ-PST2-1PL</ta>
            <ta e="T109" id="Seg_1124" s="T108">dann</ta>
            <ta e="T110" id="Seg_1125" s="T109">Haus-VBZ-PST2-1PL</ta>
            <ta e="T111" id="Seg_1126" s="T110">dann</ta>
            <ta e="T112" id="Seg_1127" s="T111">wer-VBZ</ta>
            <ta e="T113" id="Seg_1128" s="T112">dann</ta>
            <ta e="T114" id="Seg_1129" s="T113">Spiridon.[NOM]</ta>
            <ta e="T115" id="Seg_1130" s="T114">wer.[NOM]</ta>
            <ta e="T118" id="Seg_1131" s="T117">äh</ta>
            <ta e="T119" id="Seg_1132" s="T118">gehen-PST1-1PL</ta>
            <ta e="T120" id="Seg_1133" s="T119">dort</ta>
            <ta e="T121" id="Seg_1134" s="T120">noch</ta>
            <ta e="T122" id="Seg_1135" s="T121">fahren-VBZ-CVB.SIM</ta>
            <ta e="T123" id="Seg_1136" s="T122">fallen-PST2-1PL</ta>
            <ta e="T124" id="Seg_1137" s="T123">dann</ta>
            <ta e="T125" id="Seg_1138" s="T124">Haus-VBZ-PST2-1PL</ta>
            <ta e="T126" id="Seg_1139" s="T125">letzter-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1140" s="T0">э</ta>
            <ta e="T2" id="Seg_1141" s="T1">1PL.[NOM]</ta>
            <ta e="T3" id="Seg_1142" s="T2">кто.[NOM]</ta>
            <ta e="T4" id="Seg_1143" s="T3">горка-DAT/LOC</ta>
            <ta e="T5" id="Seg_1144" s="T4">кататься-VBZ-CVB.SIM</ta>
            <ta e="T6" id="Seg_1145" s="T5">идти-PTCP.PRS</ta>
            <ta e="T7" id="Seg_1146" s="T6">быть-PST1-1PL</ta>
            <ta e="T8" id="Seg_1147" s="T7">Пономарёвка-DAT/LOC</ta>
            <ta e="T9" id="Seg_1148" s="T8">потом</ta>
            <ta e="T10" id="Seg_1149" s="T9">1PL.[NOM]</ta>
            <ta e="T11" id="Seg_1150" s="T10">сани-PROPR</ta>
            <ta e="T12" id="Seg_1151" s="T11">быть-PST1-1PL</ta>
            <ta e="T13" id="Seg_1152" s="T12">Рада.[NOM]</ta>
            <ta e="T14" id="Seg_1153" s="T13">1SG.[NOM]</ta>
            <ta e="T15" id="Seg_1154" s="T14">Спиридон.[NOM]</ta>
            <ta e="T16" id="Seg_1155" s="T15">потом</ta>
            <ta e="T17" id="Seg_1156" s="T16">1PL.[NOM]</ta>
            <ta e="T18" id="Seg_1157" s="T17">Спиридон-ACC</ta>
            <ta e="T19" id="Seg_1158" s="T18">с</ta>
            <ta e="T20" id="Seg_1159" s="T19">скользить-PTCP.PST</ta>
            <ta e="T21" id="Seg_1160" s="T20">быть-PST1-1PL</ta>
            <ta e="T22" id="Seg_1161" s="T21">потом</ta>
            <ta e="T23" id="Seg_1162" s="T22">скользить-CVB.SIM</ta>
            <ta e="T24" id="Seg_1163" s="T23">стоять-EMOT-PRS-1PL</ta>
            <ta e="T25" id="Seg_1164" s="T24">говорить-CVB.SEQ</ta>
            <ta e="T26" id="Seg_1165" s="T25">потом</ta>
            <ta e="T27" id="Seg_1166" s="T26">кто.[NOM]</ta>
            <ta e="T28" id="Seg_1167" s="T27">э</ta>
            <ta e="T29" id="Seg_1168" s="T28">скользить-CVB.SIM</ta>
            <ta e="T30" id="Seg_1169" s="T29">стоять-EMOT-PRS-1PL</ta>
            <ta e="T31" id="Seg_1170" s="T30">потом</ta>
            <ta e="T32" id="Seg_1171" s="T31">сани-1PL.[NOM]</ta>
            <ta e="T35" id="Seg_1172" s="T34">потом</ta>
            <ta e="T36" id="Seg_1173" s="T35">э</ta>
            <ta e="T37" id="Seg_1174" s="T36">что.[NOM]</ta>
            <ta e="T38" id="Seg_1175" s="T37">INDEF</ta>
            <ta e="T39" id="Seg_1176" s="T38">э</ta>
            <ta e="T40" id="Seg_1177" s="T39">кто.[NOM]</ta>
            <ta e="T41" id="Seg_1178" s="T40">дерево.[NOM]</ta>
            <ta e="T42" id="Seg_1179" s="T41">есть</ta>
            <ta e="T43" id="Seg_1180" s="T42">быть-PST1-3SG</ta>
            <ta e="T44" id="Seg_1181" s="T43">там</ta>
            <ta e="T45" id="Seg_1182" s="T44">потом</ta>
            <ta e="T48" id="Seg_1183" s="T47">кто.[NOM]</ta>
            <ta e="T49" id="Seg_1184" s="T48">потом</ta>
            <ta e="T50" id="Seg_1185" s="T49">сани-1PL.[NOM]</ta>
            <ta e="T51" id="Seg_1186" s="T50">кто-VBZ-PST2-3SG</ta>
            <ta e="T52" id="Seg_1187" s="T51">там</ta>
            <ta e="T53" id="Seg_1188" s="T52">крепко</ta>
            <ta e="T54" id="Seg_1189" s="T53">застрять-VBZ-PST2-1PL</ta>
            <ta e="T55" id="Seg_1190" s="T54">потом</ta>
            <ta e="T56" id="Seg_1191" s="T55">1PL.[NOM]</ta>
            <ta e="T57" id="Seg_1192" s="T56">падать-CVB.SEQ</ta>
            <ta e="T60" id="Seg_1193" s="T59">летать-CVB.SEQ</ta>
            <ta e="T61" id="Seg_1194" s="T60">идти-PST2-1PL</ta>
            <ta e="T62" id="Seg_1195" s="T61">потом</ta>
            <ta e="T63" id="Seg_1196" s="T62">сани-1PL.[NOM]</ta>
            <ta e="T64" id="Seg_1197" s="T63">кто.[NOM]</ta>
            <ta e="T65" id="Seg_1198" s="T64">ломаться-PTCP.PST</ta>
            <ta e="T66" id="Seg_1199" s="T65">быть-PST1-3SG</ta>
            <ta e="T67" id="Seg_1200" s="T66">сани-1PL.[NOM]</ta>
            <ta e="T68" id="Seg_1201" s="T67">кто.[NOM]</ta>
            <ta e="T69" id="Seg_1202" s="T68">сани-1PL.[NOM]</ta>
            <ta e="T70" id="Seg_1203" s="T69">кто.[NOM]</ta>
            <ta e="T71" id="Seg_1204" s="T70">полозье.[NOM]</ta>
            <ta e="T72" id="Seg_1205" s="T71">э</ta>
            <ta e="T73" id="Seg_1206" s="T72">такой</ta>
            <ta e="T74" id="Seg_1207" s="T73">гнуться-PTCP.PST</ta>
            <ta e="T75" id="Seg_1208" s="T74">быть-PST1-3SG</ta>
            <ta e="T76" id="Seg_1209" s="T75">сани-1PL.[NOM]</ta>
            <ta e="T77" id="Seg_1210" s="T76">потом</ta>
            <ta e="T78" id="Seg_1211" s="T77">потом</ta>
            <ta e="T79" id="Seg_1212" s="T78">Спиридон.[NOM]</ta>
            <ta e="T80" id="Seg_1213" s="T79">что.[NOM]</ta>
            <ta e="T81" id="Seg_1214" s="T80">INDEF</ta>
            <ta e="T82" id="Seg_1215" s="T81">быть-CVB.SIM</ta>
            <ta e="T83" id="Seg_1216" s="T82">лежать-PTCP.PRS</ta>
            <ta e="T84" id="Seg_1217" s="T83">быть-PST1-3SG</ta>
            <ta e="T85" id="Seg_1218" s="T84">там</ta>
            <ta e="T86" id="Seg_1219" s="T85">потом</ta>
            <ta e="T87" id="Seg_1220" s="T86">1SG.[NOM]</ta>
            <ta e="T88" id="Seg_1221" s="T87">плакать-CVB.SIM</ta>
            <ta e="T89" id="Seg_1222" s="T88">почти.делать-PST2-EP-1SG</ta>
            <ta e="T90" id="Seg_1223" s="T89">Спиридон.[NOM]</ta>
            <ta e="T91" id="Seg_1224" s="T90">там</ta>
            <ta e="T92" id="Seg_1225" s="T91">что.[NOM]</ta>
            <ta e="T93" id="Seg_1226" s="T92">INDEF</ta>
            <ta e="T96" id="Seg_1227" s="T95">кто.[NOM]</ta>
            <ta e="T97" id="Seg_1228" s="T96">дыхание-3SG.[NOM]</ta>
            <ta e="T98" id="Seg_1229" s="T97">задыхаться-PTCP.PRS</ta>
            <ta e="T99" id="Seg_1230" s="T98">быть-PST1-3SG</ta>
            <ta e="T100" id="Seg_1231" s="T99">потом</ta>
            <ta e="T101" id="Seg_1232" s="T100">потом</ta>
            <ta e="T102" id="Seg_1233" s="T101">там</ta>
            <ta e="T103" id="Seg_1234" s="T102">потом</ta>
            <ta e="T104" id="Seg_1235" s="T103">1PL.[NOM]</ta>
            <ta e="T105" id="Seg_1236" s="T104">Рада-ACC</ta>
            <ta e="T106" id="Seg_1237" s="T105">кто.[NOM]</ta>
            <ta e="T107" id="Seg_1238" s="T106">1PL.[NOM]</ta>
            <ta e="T108" id="Seg_1239" s="T107">дом-VBZ-PST2-1PL</ta>
            <ta e="T109" id="Seg_1240" s="T108">потом</ta>
            <ta e="T110" id="Seg_1241" s="T109">дом-VBZ-PST2-1PL</ta>
            <ta e="T111" id="Seg_1242" s="T110">потом</ta>
            <ta e="T112" id="Seg_1243" s="T111">кто-VBZ</ta>
            <ta e="T113" id="Seg_1244" s="T112">потом</ta>
            <ta e="T114" id="Seg_1245" s="T113">Спиридон.[NOM]</ta>
            <ta e="T115" id="Seg_1246" s="T114">кто.[NOM]</ta>
            <ta e="T118" id="Seg_1247" s="T117">э</ta>
            <ta e="T119" id="Seg_1248" s="T118">идти-PST1-1PL</ta>
            <ta e="T120" id="Seg_1249" s="T119">там</ta>
            <ta e="T121" id="Seg_1250" s="T120">еще</ta>
            <ta e="T122" id="Seg_1251" s="T121">кататься-VBZ-CVB.SIM</ta>
            <ta e="T123" id="Seg_1252" s="T122">падать-PST2-1PL</ta>
            <ta e="T124" id="Seg_1253" s="T123">потом</ta>
            <ta e="T125" id="Seg_1254" s="T124">дом-VBZ-PST2-1PL</ta>
            <ta e="T126" id="Seg_1255" s="T125">последний-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1256" s="T0">interj</ta>
            <ta e="T2" id="Seg_1257" s="T1">pers-pro:case</ta>
            <ta e="T3" id="Seg_1258" s="T2">que-pro:case</ta>
            <ta e="T4" id="Seg_1259" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_1260" s="T4">v-v&gt;v-v:cvb</ta>
            <ta e="T6" id="Seg_1261" s="T5">v-v:ptcp</ta>
            <ta e="T7" id="Seg_1262" s="T6">v-v:tense-v:poss.pn</ta>
            <ta e="T8" id="Seg_1263" s="T7">propr-n:case</ta>
            <ta e="T9" id="Seg_1264" s="T8">adv</ta>
            <ta e="T10" id="Seg_1265" s="T9">pers-pro:case</ta>
            <ta e="T11" id="Seg_1266" s="T10">n-n&gt;adj</ta>
            <ta e="T12" id="Seg_1267" s="T11">v-v:tense-v:poss.pn</ta>
            <ta e="T13" id="Seg_1268" s="T12">propr-n:case</ta>
            <ta e="T14" id="Seg_1269" s="T13">pers-pro:case</ta>
            <ta e="T15" id="Seg_1270" s="T14">propr-n:case</ta>
            <ta e="T16" id="Seg_1271" s="T15">adv</ta>
            <ta e="T17" id="Seg_1272" s="T16">pers-pro:case</ta>
            <ta e="T18" id="Seg_1273" s="T17">propr-n:case</ta>
            <ta e="T19" id="Seg_1274" s="T18">post</ta>
            <ta e="T20" id="Seg_1275" s="T19">v-v:ptcp</ta>
            <ta e="T21" id="Seg_1276" s="T20">v-v:tense-v:poss.pn</ta>
            <ta e="T22" id="Seg_1277" s="T21">adv</ta>
            <ta e="T23" id="Seg_1278" s="T22">v-v:cvb</ta>
            <ta e="T24" id="Seg_1279" s="T23">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T25" id="Seg_1280" s="T24">v-v:cvb</ta>
            <ta e="T26" id="Seg_1281" s="T25">adv</ta>
            <ta e="T27" id="Seg_1282" s="T26">que-pro:case</ta>
            <ta e="T28" id="Seg_1283" s="T27">interj</ta>
            <ta e="T29" id="Seg_1284" s="T28">v-v:cvb</ta>
            <ta e="T30" id="Seg_1285" s="T29">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T31" id="Seg_1286" s="T30">adv</ta>
            <ta e="T32" id="Seg_1287" s="T31">n-n:(poss)-n:case</ta>
            <ta e="T35" id="Seg_1288" s="T34">adv</ta>
            <ta e="T36" id="Seg_1289" s="T35">interj</ta>
            <ta e="T37" id="Seg_1290" s="T36">que-pro:case</ta>
            <ta e="T38" id="Seg_1291" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_1292" s="T38">interj</ta>
            <ta e="T40" id="Seg_1293" s="T39">que-pro:case</ta>
            <ta e="T41" id="Seg_1294" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_1295" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_1296" s="T42">v-v:tense-v:poss.pn</ta>
            <ta e="T44" id="Seg_1297" s="T43">adv</ta>
            <ta e="T45" id="Seg_1298" s="T44">adv</ta>
            <ta e="T48" id="Seg_1299" s="T47">que-pro:case</ta>
            <ta e="T49" id="Seg_1300" s="T48">adv</ta>
            <ta e="T50" id="Seg_1301" s="T49">n-n:(poss)-n:case</ta>
            <ta e="T51" id="Seg_1302" s="T50">que-que&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T52" id="Seg_1303" s="T51">adv</ta>
            <ta e="T53" id="Seg_1304" s="T52">adv</ta>
            <ta e="T54" id="Seg_1305" s="T53">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T55" id="Seg_1306" s="T54">adv</ta>
            <ta e="T56" id="Seg_1307" s="T55">pers-pro:case</ta>
            <ta e="T57" id="Seg_1308" s="T56">v-v:cvb</ta>
            <ta e="T60" id="Seg_1309" s="T59">v-v:cvb</ta>
            <ta e="T61" id="Seg_1310" s="T60">v-v:tense-v:pred.pn</ta>
            <ta e="T62" id="Seg_1311" s="T61">adv</ta>
            <ta e="T63" id="Seg_1312" s="T62">n-n:(poss)-n:case</ta>
            <ta e="T64" id="Seg_1313" s="T63">que-pro:case</ta>
            <ta e="T65" id="Seg_1314" s="T64">v-v:ptcp</ta>
            <ta e="T66" id="Seg_1315" s="T65">v-v:tense-v:poss.pn</ta>
            <ta e="T67" id="Seg_1316" s="T66">n-n:(poss)-n:case</ta>
            <ta e="T68" id="Seg_1317" s="T67">que-pro:case</ta>
            <ta e="T69" id="Seg_1318" s="T68">n-n:(poss)-n:case</ta>
            <ta e="T70" id="Seg_1319" s="T69">que-pro:case</ta>
            <ta e="T71" id="Seg_1320" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_1321" s="T71">interj</ta>
            <ta e="T73" id="Seg_1322" s="T72">dempro</ta>
            <ta e="T74" id="Seg_1323" s="T73">v-v:ptcp</ta>
            <ta e="T75" id="Seg_1324" s="T74">v-v:tense-v:poss.pn</ta>
            <ta e="T76" id="Seg_1325" s="T75">n-n:(poss)-n:case</ta>
            <ta e="T77" id="Seg_1326" s="T76">adv</ta>
            <ta e="T78" id="Seg_1327" s="T77">adv</ta>
            <ta e="T79" id="Seg_1328" s="T78">propr-n:case</ta>
            <ta e="T80" id="Seg_1329" s="T79">que-pro:case</ta>
            <ta e="T81" id="Seg_1330" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_1331" s="T81">v-v:cvb</ta>
            <ta e="T83" id="Seg_1332" s="T82">v-v:ptcp</ta>
            <ta e="T84" id="Seg_1333" s="T83">v-v:tense-v:poss.pn</ta>
            <ta e="T85" id="Seg_1334" s="T84">adv</ta>
            <ta e="T86" id="Seg_1335" s="T85">adv</ta>
            <ta e="T87" id="Seg_1336" s="T86">pers-pro:case</ta>
            <ta e="T88" id="Seg_1337" s="T87">v-v:cvb</ta>
            <ta e="T89" id="Seg_1338" s="T88">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T90" id="Seg_1339" s="T89">propr-n:case</ta>
            <ta e="T91" id="Seg_1340" s="T90">adv</ta>
            <ta e="T92" id="Seg_1341" s="T91">que-pro:case</ta>
            <ta e="T93" id="Seg_1342" s="T92">ptcl</ta>
            <ta e="T96" id="Seg_1343" s="T95">que-pro:case</ta>
            <ta e="T97" id="Seg_1344" s="T96">n-n:(poss)-n:case</ta>
            <ta e="T98" id="Seg_1345" s="T97">v-v:ptcp</ta>
            <ta e="T99" id="Seg_1346" s="T98">v-v:tense-v:poss.pn</ta>
            <ta e="T100" id="Seg_1347" s="T99">adv</ta>
            <ta e="T101" id="Seg_1348" s="T100">adv</ta>
            <ta e="T102" id="Seg_1349" s="T101">adv</ta>
            <ta e="T103" id="Seg_1350" s="T102">adv</ta>
            <ta e="T104" id="Seg_1351" s="T103">pers-pro:case</ta>
            <ta e="T105" id="Seg_1352" s="T104">propr-n:case</ta>
            <ta e="T106" id="Seg_1353" s="T105">que-pro:case</ta>
            <ta e="T107" id="Seg_1354" s="T106">pers-pro:case</ta>
            <ta e="T108" id="Seg_1355" s="T107">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T109" id="Seg_1356" s="T108">adv</ta>
            <ta e="T110" id="Seg_1357" s="T109">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T111" id="Seg_1358" s="T110">adv</ta>
            <ta e="T112" id="Seg_1359" s="T111">que-que&gt;v</ta>
            <ta e="T113" id="Seg_1360" s="T112">adv</ta>
            <ta e="T114" id="Seg_1361" s="T113">propr-n:case</ta>
            <ta e="T115" id="Seg_1362" s="T114">que-pro:case</ta>
            <ta e="T118" id="Seg_1363" s="T117">interj</ta>
            <ta e="T119" id="Seg_1364" s="T118">v-v:tense-v:poss.pn</ta>
            <ta e="T120" id="Seg_1365" s="T119">adv</ta>
            <ta e="T121" id="Seg_1366" s="T120">adv</ta>
            <ta e="T122" id="Seg_1367" s="T121">v-v&gt;v-v:cvb</ta>
            <ta e="T123" id="Seg_1368" s="T122">v-v:tense-v:poss.pn</ta>
            <ta e="T124" id="Seg_1369" s="T123">adv</ta>
            <ta e="T125" id="Seg_1370" s="T124">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T126" id="Seg_1371" s="T125">adj-n:(poss)-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_1372" s="T0">interj</ta>
            <ta e="T2" id="Seg_1373" s="T1">pers</ta>
            <ta e="T3" id="Seg_1374" s="T2">que</ta>
            <ta e="T4" id="Seg_1375" s="T3">n</ta>
            <ta e="T5" id="Seg_1376" s="T4">v</ta>
            <ta e="T6" id="Seg_1377" s="T5">aux</ta>
            <ta e="T7" id="Seg_1378" s="T6">aux</ta>
            <ta e="T8" id="Seg_1379" s="T7">propr</ta>
            <ta e="T9" id="Seg_1380" s="T8">adv</ta>
            <ta e="T10" id="Seg_1381" s="T9">pers</ta>
            <ta e="T11" id="Seg_1382" s="T10">adj</ta>
            <ta e="T12" id="Seg_1383" s="T11">cop</ta>
            <ta e="T13" id="Seg_1384" s="T12">propr</ta>
            <ta e="T14" id="Seg_1385" s="T13">pers</ta>
            <ta e="T15" id="Seg_1386" s="T14">propr</ta>
            <ta e="T16" id="Seg_1387" s="T15">adv</ta>
            <ta e="T17" id="Seg_1388" s="T16">pers</ta>
            <ta e="T18" id="Seg_1389" s="T17">propr</ta>
            <ta e="T19" id="Seg_1390" s="T18">post</ta>
            <ta e="T20" id="Seg_1391" s="T19">v</ta>
            <ta e="T21" id="Seg_1392" s="T20">aux</ta>
            <ta e="T22" id="Seg_1393" s="T21">adv</ta>
            <ta e="T23" id="Seg_1394" s="T22">v</ta>
            <ta e="T24" id="Seg_1395" s="T23">aux</ta>
            <ta e="T25" id="Seg_1396" s="T24">v</ta>
            <ta e="T26" id="Seg_1397" s="T25">adv</ta>
            <ta e="T27" id="Seg_1398" s="T26">que</ta>
            <ta e="T28" id="Seg_1399" s="T27">interj</ta>
            <ta e="T29" id="Seg_1400" s="T28">v</ta>
            <ta e="T30" id="Seg_1401" s="T29">aux</ta>
            <ta e="T31" id="Seg_1402" s="T30">adv</ta>
            <ta e="T32" id="Seg_1403" s="T31">n</ta>
            <ta e="T35" id="Seg_1404" s="T34">adv</ta>
            <ta e="T36" id="Seg_1405" s="T35">interj</ta>
            <ta e="T37" id="Seg_1406" s="T36">que</ta>
            <ta e="T38" id="Seg_1407" s="T37">ptcl</ta>
            <ta e="T39" id="Seg_1408" s="T38">interj</ta>
            <ta e="T40" id="Seg_1409" s="T39">que</ta>
            <ta e="T41" id="Seg_1410" s="T40">n</ta>
            <ta e="T42" id="Seg_1411" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_1412" s="T42">cop</ta>
            <ta e="T44" id="Seg_1413" s="T43">adv</ta>
            <ta e="T45" id="Seg_1414" s="T44">adv</ta>
            <ta e="T48" id="Seg_1415" s="T47">que</ta>
            <ta e="T49" id="Seg_1416" s="T48">adv</ta>
            <ta e="T50" id="Seg_1417" s="T49">n</ta>
            <ta e="T51" id="Seg_1418" s="T50">v</ta>
            <ta e="T52" id="Seg_1419" s="T51">adv</ta>
            <ta e="T53" id="Seg_1420" s="T52">adv</ta>
            <ta e="T54" id="Seg_1421" s="T53">v</ta>
            <ta e="T55" id="Seg_1422" s="T54">adv</ta>
            <ta e="T56" id="Seg_1423" s="T55">pers</ta>
            <ta e="T57" id="Seg_1424" s="T56">v</ta>
            <ta e="T60" id="Seg_1425" s="T59">v</ta>
            <ta e="T61" id="Seg_1426" s="T60">aux</ta>
            <ta e="T62" id="Seg_1427" s="T61">adv</ta>
            <ta e="T63" id="Seg_1428" s="T62">n</ta>
            <ta e="T64" id="Seg_1429" s="T63">que</ta>
            <ta e="T65" id="Seg_1430" s="T64">v</ta>
            <ta e="T66" id="Seg_1431" s="T65">aux</ta>
            <ta e="T67" id="Seg_1432" s="T66">n</ta>
            <ta e="T68" id="Seg_1433" s="T67">que</ta>
            <ta e="T69" id="Seg_1434" s="T68">n</ta>
            <ta e="T70" id="Seg_1435" s="T69">que</ta>
            <ta e="T71" id="Seg_1436" s="T70">n</ta>
            <ta e="T72" id="Seg_1437" s="T71">interj</ta>
            <ta e="T73" id="Seg_1438" s="T72">dempro</ta>
            <ta e="T74" id="Seg_1439" s="T73">v</ta>
            <ta e="T75" id="Seg_1440" s="T74">aux</ta>
            <ta e="T76" id="Seg_1441" s="T75">n</ta>
            <ta e="T77" id="Seg_1442" s="T76">adv</ta>
            <ta e="T78" id="Seg_1443" s="T77">adv</ta>
            <ta e="T79" id="Seg_1444" s="T78">propr</ta>
            <ta e="T80" id="Seg_1445" s="T79">que</ta>
            <ta e="T81" id="Seg_1446" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_1447" s="T81">cop</ta>
            <ta e="T83" id="Seg_1448" s="T82">v</ta>
            <ta e="T84" id="Seg_1449" s="T83">aux</ta>
            <ta e="T85" id="Seg_1450" s="T84">adv</ta>
            <ta e="T86" id="Seg_1451" s="T85">adv</ta>
            <ta e="T87" id="Seg_1452" s="T86">pers</ta>
            <ta e="T88" id="Seg_1453" s="T87">v</ta>
            <ta e="T89" id="Seg_1454" s="T88">v</ta>
            <ta e="T90" id="Seg_1455" s="T89">propr</ta>
            <ta e="T91" id="Seg_1456" s="T90">adv</ta>
            <ta e="T92" id="Seg_1457" s="T91">que</ta>
            <ta e="T93" id="Seg_1458" s="T92">ptcl</ta>
            <ta e="T96" id="Seg_1459" s="T95">que</ta>
            <ta e="T97" id="Seg_1460" s="T96">n</ta>
            <ta e="T98" id="Seg_1461" s="T97">v</ta>
            <ta e="T99" id="Seg_1462" s="T98">aux</ta>
            <ta e="T100" id="Seg_1463" s="T99">adv</ta>
            <ta e="T101" id="Seg_1464" s="T100">adv</ta>
            <ta e="T102" id="Seg_1465" s="T101">adv</ta>
            <ta e="T103" id="Seg_1466" s="T102">adv</ta>
            <ta e="T104" id="Seg_1467" s="T103">pers</ta>
            <ta e="T105" id="Seg_1468" s="T104">propr</ta>
            <ta e="T106" id="Seg_1469" s="T105">que</ta>
            <ta e="T107" id="Seg_1470" s="T106">pers</ta>
            <ta e="T108" id="Seg_1471" s="T107">v</ta>
            <ta e="T109" id="Seg_1472" s="T108">adv</ta>
            <ta e="T110" id="Seg_1473" s="T109">v</ta>
            <ta e="T111" id="Seg_1474" s="T110">adv</ta>
            <ta e="T112" id="Seg_1475" s="T111">v</ta>
            <ta e="T113" id="Seg_1476" s="T112">adv</ta>
            <ta e="T114" id="Seg_1477" s="T113">propr</ta>
            <ta e="T115" id="Seg_1478" s="T114">que</ta>
            <ta e="T118" id="Seg_1479" s="T117">interj</ta>
            <ta e="T119" id="Seg_1480" s="T118">v</ta>
            <ta e="T120" id="Seg_1481" s="T119">adv</ta>
            <ta e="T121" id="Seg_1482" s="T120">adv</ta>
            <ta e="T122" id="Seg_1483" s="T121">v</ta>
            <ta e="T123" id="Seg_1484" s="T122">aux</ta>
            <ta e="T124" id="Seg_1485" s="T123">adv</ta>
            <ta e="T125" id="Seg_1486" s="T124">v</ta>
            <ta e="T126" id="Seg_1487" s="T125">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_1488" s="T3">RUS:core</ta>
            <ta e="T5" id="Seg_1489" s="T4">RUS:cult</ta>
            <ta e="T8" id="Seg_1490" s="T7">RUS:cult</ta>
            <ta e="T13" id="Seg_1491" s="T12">RUS:cult</ta>
            <ta e="T15" id="Seg_1492" s="T14">RUS:cult</ta>
            <ta e="T18" id="Seg_1493" s="T17">RUS:cult</ta>
            <ta e="T54" id="Seg_1494" s="T53">RUS:core</ta>
            <ta e="T79" id="Seg_1495" s="T78">RUS:cult</ta>
            <ta e="T90" id="Seg_1496" s="T89">RUS:cult</ta>
            <ta e="T105" id="Seg_1497" s="T104">RUS:cult</ta>
            <ta e="T114" id="Seg_1498" s="T113">RUS:cult</ta>
            <ta e="T121" id="Seg_1499" s="T120">RUS:mod</ta>
            <ta e="T122" id="Seg_1500" s="T121">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T15" id="Seg_1501" s="T0">Eh, we were on the hill sledding, and we had a sledge, Rada, me and Spire.</ta>
            <ta e="T21" id="Seg_1502" s="T15">Then I sledded with Spire.</ta>
            <ta e="T47" id="Seg_1503" s="T21">We sledded then, whatchamacallit, eh, we sledded and then our sledge, eh, some, eh, whatchamacallit, there was a piece of wood.</ta>
            <ta e="T61" id="Seg_1504" s="T47">Then our sledge whatchamacallit, well, we got stuck and then we fell, flew.</ta>
            <ta e="T76" id="Seg_1505" s="T61">Then our sledge broke, our sledge, the whatchamacallit, the sleigh runner, eh, it bent so, our sledge.</ta>
            <ta e="T85" id="Seg_1506" s="T76">Then then something was with Spire, he laid there.</ta>
            <ta e="T99" id="Seg_1507" s="T85">Then I almost cried, Spire there something, he (wheezed?).</ta>
            <ta e="T110" id="Seg_1508" s="T99">Then, then, then we [with] Rada went home, then we went home.</ta>
            <ta e="T126" id="Seg_1509" s="T110">Then, then Spire, eh, we went there, we sledded once more, then we went home, that's it.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T15" id="Seg_1510" s="T0">Äh, wir waren auf dem Hügel zum Rodeln, und wir hatten einen Schlitten, Rada, ich und Spire.</ta>
            <ta e="T21" id="Seg_1511" s="T15">Dann bin ich mit Spire gerodelt.</ta>
            <ta e="T47" id="Seg_1512" s="T21">Wir rodelten dann, Dings, äh, wir rodelten und dann unser Schlitten, äh, irgendein, äh, Dings, da war ein Stück Holz.</ta>
            <ta e="T61" id="Seg_1513" s="T47">Dann machte unser Schlitten so, wir blieben hängen und dann fielen, flogen wir.</ta>
            <ta e="T76" id="Seg_1514" s="T61">Dann zerbrach unser Schlitten, unser Schlitten, das Dings, die Schlittenkufe, äh, bog sich so, unser Schlitten.</ta>
            <ta e="T85" id="Seg_1515" s="T76">Dann, dann war mit Spire etwas, er lag da.</ta>
            <ta e="T99" id="Seg_1516" s="T85">Dann weinte ich fast, Spire dort irgendwas, er (schnaufte?).</ta>
            <ta e="T110" id="Seg_1517" s="T99">Dann, dann, dann wir [mir] Rada gingen nach Hause, dann gingen wir nach Hause.</ta>
            <ta e="T126" id="Seg_1518" s="T110">Dann, dann Spire, äh, wir gingen da, wir rodelten noch mal, dann gingen wir nach Hause, das ist alles.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T15" id="Seg_1519" s="T0">Ээ, мы катались на санках с горки в Пономарёвке, у нас санки были, Рада, я, Спиря.</ta>
            <ta e="T21" id="Seg_1520" s="T15">Потом мы со Спирей скатились.</ta>
            <ta e="T47" id="Seg_1521" s="T21">Мы скатитлсь, потом это, ээ, мы скатитлись и потом наши санки, ээ, что-то, это, там было бревно.</ta>
            <ta e="T61" id="Seg_1522" s="T47">Потом наши санки это, ну, мы застряли, а оптом мы упали, полетели.</ta>
            <ta e="T76" id="Seg_1523" s="T61">Потом наши санки сломались, наши санки, это, полозья, ээ, они так погнулись, наши санки.</ta>
            <ta e="T85" id="Seg_1524" s="T76">Потом со Спирей что-то случилось, он там лежал.</ta>
            <ta e="T99" id="Seg_1525" s="T85">Потом я чуть не заплакала, Спиря там это, (задыхался?).</ta>
            <ta e="T110" id="Seg_1526" s="T99">Потом, потом, потом мы с Радой, мы домой пошли, потом мы домой пошли.</ta>
            <ta e="T126" id="Seg_1527" s="T110">Потом, это, потом Спиря, это, ээ, мы пошли там ещё прокатиться, потом мы пошли домой, всё.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T15" id="Seg_1528" s="T0">Мы на горке кататься шли в Пономарёвке ещё мы с санками были Рада я Спиря.</ta>
            <ta e="T21" id="Seg_1529" s="T15">Потом мы со Спирей скатились.</ta>
            <ta e="T47" id="Seg_1530" s="T21">Катились потом эта ещё санки что то эта бревно было там потом эта.</ta>
            <ta e="T61" id="Seg_1531" s="T47">Эта санки сделалось там вообще застряли потом мы упали улетели.</ta>
            <ta e="T76" id="Seg_1532" s="T61">Потом санки сломались было санки эта санки эта полозья вот так согнулись было санки.</ta>
            <ta e="T85" id="Seg_1533" s="T76">Потом потом Спиря что то делал лежал там.</ta>
            <ta e="T99" id="Seg_1534" s="T85">Потом я чуть не заплакал, Спиря там что то эта задыхался было.</ta>
            <ta e="T110" id="Seg_1535" s="T99">Потом потом там потом мы с Радой мы домой пошли потом домой пошли.</ta>
            <ta e="T126" id="Seg_1536" s="T110">Потом эта потом Спиря эта э пошли там ещё прокатиться чуть потом пошли домой, всё.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
