<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID04577BBA-BBD8-4383-0D11-CDB71CE9A720">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>AnIM_2009_Accident_nar</transcription-name>
         <referenced-file url="AnIM_2009_Accident_nar.wav" />
         <referenced-file url="AnIM_2009_Accident_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\AnIM_2009_Accident_nar\AnIM_2009_Accident_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">705</ud-information>
            <ud-information attribute-name="# HIAT:w">619</ud-information>
            <ud-information attribute-name="# e">589</ud-information>
            <ud-information attribute-name="# HIAT:u">61</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AnIM">
            <abbreviation>AnIM</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="9.54" type="appl" />
         <tli id="T1" time="10.184" type="appl" />
         <tli id="T2" time="10.829" type="appl" />
         <tli id="T3" time="11.473" type="appl" />
         <tli id="T4" time="12.117" type="appl" />
         <tli id="T5" time="12.761" type="appl" />
         <tli id="T6" time="13.406" type="appl" />
         <tli id="T7" time="14.05" type="appl" />
         <tli id="T8" time="14.472" type="appl" />
         <tli id="T9" time="14.865" type="appl" />
         <tli id="T10" time="15.29799868666249" />
         <tli id="T11" time="15.65" type="appl" />
         <tli id="T12" time="16.184" type="appl" />
         <tli id="T13" time="16.668" type="appl" />
         <tli id="T14" time="17.152" type="appl" />
         <tli id="T15" time="17.636" type="appl" />
         <tli id="T16" time="18.12" type="appl" />
         <tli id="T17" time="18.518" type="appl" />
         <tli id="T18" time="18.847" type="appl" />
         <tli id="T19" time="19.175" type="appl" />
         <tli id="T20" time="19.503" type="appl" />
         <tli id="T21" time="19.832" type="appl" />
         <tli id="T22" time="20.032907897810528" />
         <tli id="T23" time="20.62" type="appl" />
         <tli id="T24" time="21.04" type="appl" />
         <tli id="T25" time="21.46" type="appl" />
         <tli id="T26" time="21.88" type="appl" />
         <tli id="T27" time="22.3" type="appl" />
         <tli id="T28" time="22.72" type="appl" />
         <tli id="T29" time="23.14" type="appl" />
         <tli id="T30" time="23.56" type="appl" />
         <tli id="T31" time="23.98" type="appl" />
         <tli id="T32" time="24.4" type="appl" />
         <tli id="T33" time="24.82" type="appl" />
         <tli id="T34" time="25.606658344106375" />
         <tli id="T35" time="26.072" type="appl" />
         <tli id="T36" time="26.534" type="appl" />
         <tli id="T37" time="26.997" type="appl" />
         <tli id="T38" time="27.459" type="appl" />
         <tli id="T39" time="27.921" type="appl" />
         <tli id="T40" time="28.383" type="appl" />
         <tli id="T41" time="28.846" type="appl" />
         <tli id="T42" time="29.308" type="appl" />
         <tli id="T43" time="30.939342946335664" />
         <tli id="T607" time="34.44117147316783" type="intp" />
         <tli id="T44" time="37.943" type="appl" />
         <tli id="T45" time="38.436" type="appl" />
         <tli id="T46" time="38.929" type="appl" />
         <tli id="T47" time="39.423" type="appl" />
         <tli id="T48" time="39.916" type="appl" />
         <tli id="T49" time="40.409" type="appl" />
         <tli id="T50" time="40.902" type="appl" />
         <tli id="T51" time="41.395" type="appl" />
         <tli id="T52" time="41.888" type="appl" />
         <tli id="T53" time="42.382" type="appl" />
         <tli id="T54" time="42.875" type="appl" />
         <tli id="T55" time="43.368" type="appl" />
         <tli id="T56" time="43.861" type="appl" />
         <tli id="T57" time="44.354" type="appl" />
         <tli id="T58" time="44.847" type="appl" />
         <tli id="T59" time="45.341" type="appl" />
         <tli id="T60" time="45.834" type="appl" />
         <tli id="T61" time="46.327" type="appl" />
         <tli id="T62" time="46.97999513222302" />
         <tli id="T63" time="47.482" type="appl" />
         <tli id="T64" time="47.993" type="appl" />
         <tli id="T65" time="48.505" type="appl" />
         <tli id="T66" time="49.017" type="appl" />
         <tli id="T67" time="49.528" type="appl" />
         <tli id="T68" time="50.04" type="appl" />
         <tli id="T69" time="50.552" type="appl" />
         <tli id="T70" time="51.063" type="appl" />
         <tli id="T71" time="51.575" type="appl" />
         <tli id="T72" time="52.087" type="appl" />
         <tli id="T73" time="52.598" type="appl" />
         <tli id="T74" time="53.2966611164497" />
         <tli id="T75" time="53.878" type="appl" />
         <tli id="T76" time="54.425" type="appl" />
         <tli id="T77" time="54.973" type="appl" />
         <tli id="T78" time="55.521" type="appl" />
         <tli id="T79" time="56.068" type="appl" />
         <tli id="T80" time="56.616" type="appl" />
         <tli id="T81" time="57.164" type="appl" />
         <tli id="T82" time="57.711" type="appl" />
         <tli id="T83" time="58.259" type="appl" />
         <tli id="T84" time="58.806" type="appl" />
         <tli id="T85" time="59.354" type="appl" />
         <tli id="T86" time="59.902" type="appl" />
         <tli id="T87" time="60.449" type="appl" />
         <tli id="T88" time="60.997" type="appl" />
         <tli id="T89" time="61.545" type="appl" />
         <tli id="T90" time="62.092" type="appl" />
         <tli id="T91" time="62.63333132472551" />
         <tli id="T92" time="63.195" type="appl" />
         <tli id="T93" time="63.68" type="appl" />
         <tli id="T94" time="64.165" type="appl" />
         <tli id="T95" time="64.65" type="appl" />
         <tli id="T96" time="65.135" type="appl" />
         <tli id="T97" time="65.62" type="appl" />
         <tli id="T98" time="66.105" type="appl" />
         <tli id="T99" time="66.6566638506339" />
         <tli id="T100" time="67.114" type="appl" />
         <tli id="T101" time="67.579" type="appl" />
         <tli id="T102" time="68.043" type="appl" />
         <tli id="T103" time="68.507" type="appl" />
         <tli id="T104" time="68.971" type="appl" />
         <tli id="T105" time="69.436" type="appl" />
         <tli id="T106" time="69.9" type="appl" />
         <tli id="T107" time="70.338" type="appl" />
         <tli id="T108" time="70.726" type="appl" />
         <tli id="T109" time="71.114" type="appl" />
         <tli id="T110" time="71.502" type="appl" />
         <tli id="T111" time="72.14513453248105" />
         <tli id="T112" time="72.755" type="appl" />
         <tli id="T113" time="73.16" type="appl" />
         <tli id="T114" time="73.565" type="appl" />
         <tli id="T115" time="73.97" type="appl" />
         <tli id="T116" time="74.375" type="appl" />
         <tli id="T117" time="74.78" type="appl" />
         <tli id="T118" time="75.185" type="appl" />
         <tli id="T119" time="76.51837499203634" />
         <tli id="T120" time="77.015" type="appl" />
         <tli id="T121" time="77.339" type="appl" />
         <tli id="T122" time="77.664" type="appl" />
         <tli id="T123" time="77.988" type="appl" />
         <tli id="T124" time="78.313" type="appl" />
         <tli id="T125" time="78.638" type="appl" />
         <tli id="T126" time="78.962" type="appl" />
         <tli id="T127" time="79.287" type="appl" />
         <tli id="T128" time="79.612" type="appl" />
         <tli id="T129" time="79.936" type="appl" />
         <tli id="T130" time="80.261" type="appl" />
         <tli id="T131" time="80.585" type="appl" />
         <tli id="T132" time="81.03666445454353" />
         <tli id="T608" time="81.28483222727176" type="intp" />
         <tli id="T133" time="81.533" type="appl" />
         <tli id="T134" time="82.036" type="appl" />
         <tli id="T135" time="82.54" type="appl" />
         <tli id="T136" time="83.043" type="appl" />
         <tli id="T137" time="83.546" type="appl" />
         <tli id="T138" time="84.049" type="appl" />
         <tli id="T139" time="84.552" type="appl" />
         <tli id="T140" time="85.056" type="appl" />
         <tli id="T141" time="85.559" type="appl" />
         <tli id="T142" time="86.062" type="appl" />
         <tli id="T143" time="86.565" type="appl" />
         <tli id="T144" time="87.068" type="appl" />
         <tli id="T145" time="87.572" type="appl" />
         <tli id="T146" time="88.075" type="appl" />
         <tli id="T147" time="88.578" type="appl" />
         <tli id="T148" time="89.081" type="appl" />
         <tli id="T149" time="89.584" type="appl" />
         <tli id="T150" time="90.088" type="appl" />
         <tli id="T151" time="90.591" type="appl" />
         <tli id="T152" time="91.094" type="appl" />
         <tli id="T153" time="91.597" type="appl" />
         <tli id="T154" time="92.1" type="appl" />
         <tli id="T155" time="92.604" type="appl" />
         <tli id="T156" time="93.107" type="appl" />
         <tli id="T157" time="93.70999557354159" />
         <tli id="T158" time="94.084" type="appl" />
         <tli id="T159" time="94.478" type="appl" />
         <tli id="T160" time="94.872" type="appl" />
         <tli id="T161" time="95.266" type="appl" />
         <tli id="T162" time="95.66" type="appl" />
         <tli id="T163" time="96.054" type="appl" />
         <tli id="T164" time="96.448" type="appl" />
         <tli id="T165" time="96.842" type="appl" />
         <tli id="T166" time="97.236" type="appl" />
         <tli id="T167" time="97.63" type="appl" />
         <tli id="T168" time="98.024" type="appl" />
         <tli id="T169" time="98.418" type="appl" />
         <tli id="T170" time="98.812" type="appl" />
         <tli id="T171" time="99.206" type="appl" />
         <tli id="T172" time="99.67999899790821" />
         <tli id="T173" time="100.091" type="appl" />
         <tli id="T174" time="100.472" type="appl" />
         <tli id="T175" time="100.853" type="appl" />
         <tli id="T176" time="101.234" type="appl" />
         <tli id="T177" time="101.615" type="appl" />
         <tli id="T178" time="101.996" type="appl" />
         <tli id="T179" time="102.377" type="appl" />
         <tli id="T180" time="102.758" type="appl" />
         <tli id="T181" time="103.139" type="appl" />
         <tli id="T182" time="103.62666648527257" />
         <tli id="T183" time="104.047" type="appl" />
         <tli id="T184" time="104.325" type="appl" />
         <tli id="T185" time="104.602" type="appl" />
         <tli id="T186" time="104.879" type="appl" />
         <tli id="T187" time="105.156" type="appl" />
         <tli id="T188" time="105.434" type="appl" />
         <tli id="T189" time="105.711" type="appl" />
         <tli id="T190" time="105.988" type="appl" />
         <tli id="T191" time="106.265" type="appl" />
         <tli id="T192" time="106.543" type="appl" />
         <tli id="T193" time="106.95999413344943" />
         <tli id="T194" time="107.553" type="appl" />
         <tli id="T195" time="108.226" type="appl" />
         <tli id="T196" time="108.899" type="appl" />
         <tli id="T197" time="109.572" type="appl" />
         <tli id="T198" time="110.245" type="appl" />
         <tli id="T199" time="110.918" type="appl" />
         <tli id="T200" time="111.592" type="appl" />
         <tli id="T201" time="112.265" type="appl" />
         <tli id="T202" time="112.938" type="appl" />
         <tli id="T203" time="113.611" type="appl" />
         <tli id="T204" time="114.284" type="appl" />
         <tli id="T205" time="114.957" type="appl" />
         <tli id="T206" time="115.67000447291299" />
         <tli id="T207" time="116.039" type="appl" />
         <tli id="T208" time="116.417" type="appl" />
         <tli id="T209" time="116.796" type="appl" />
         <tli id="T210" time="117.175" type="appl" />
         <tli id="T211" time="117.554" type="appl" />
         <tli id="T212" time="117.932" type="appl" />
         <tli id="T213" time="118.30433654887553" />
         <tli id="T214" time="118.91666729490964" />
         <tli id="T215" time="119.375" type="appl" />
         <tli id="T216" time="119.881" type="appl" />
         <tli id="T217" time="120.386" type="appl" />
         <tli id="T218" time="120.891" type="appl" />
         <tli id="T219" time="121.397" type="appl" />
         <tli id="T220" time="121.902" type="appl" />
         <tli id="T221" time="122.407" type="appl" />
         <tli id="T222" time="122.913" type="appl" />
         <tli id="T223" time="123.418" type="appl" />
         <tli id="T224" time="123.923" type="appl" />
         <tli id="T225" time="124.429" type="appl" />
         <tli id="T226" time="124.934" type="appl" />
         <tli id="T227" time="125.439" type="appl" />
         <tli id="T228" time="125.945" type="appl" />
         <tli id="T229" time="128.40393977362015" />
         <tli id="T230" time="128.726" type="appl" />
         <tli id="T231" time="129.133" type="appl" />
         <tli id="T232" time="129.539" type="appl" />
         <tli id="T233" time="129.946" type="appl" />
         <tli id="T234" time="130.352" type="appl" />
         <tli id="T235" time="130.759" type="appl" />
         <tli id="T236" time="131.203" type="appl" />
         <tli id="T237" time="131.545" type="appl" />
         <tli id="T238" time="131.888" type="appl" />
         <tli id="T239" time="132.23" type="appl" />
         <tli id="T240" time="132.572" type="appl" />
         <tli id="T241" time="132.915" type="appl" />
         <tli id="T242" time="133.258" type="appl" />
         <tli id="T243" time="133.77333095308882" />
         <tli id="T244" time="134.114" type="appl" />
         <tli id="T245" time="134.438" type="appl" />
         <tli id="T246" time="134.761" type="appl" />
         <tli id="T247" time="135.085" type="appl" />
         <tli id="T248" time="135.409" type="appl" />
         <tli id="T249" time="135.732" type="appl" />
         <tli id="T250" time="136.056" type="appl" />
         <tli id="T251" time="136.34666172035932" />
         <tli id="T609" time="136.56733086017965" type="intp" />
         <tli id="T252" time="136.788" type="appl" />
         <tli id="T253" time="137.165" type="appl" />
         <tli id="T254" time="137.542" type="appl" />
         <tli id="T255" time="137.92" type="appl" />
         <tli id="T256" time="138.332" type="appl" />
         <tli id="T257" time="138.695" type="appl" />
         <tli id="T258" time="139.058" type="appl" />
         <tli id="T259" time="139.43332794017712" />
         <tli id="T260" time="139.936" type="appl" />
         <tli id="T261" time="140.412" type="appl" />
         <tli id="T262" time="140.888" type="appl" />
         <tli id="T263" time="141.364" type="appl" />
         <tli id="T264" time="141.8599951554503" />
         <tli id="T265" time="142.309" type="appl" />
         <tli id="T266" time="142.748" type="appl" />
         <tli id="T267" time="143.187" type="appl" />
         <tli id="T268" time="143.626" type="appl" />
         <tli id="T269" time="144.064" type="appl" />
         <tli id="T270" time="144.503" type="appl" />
         <tli id="T271" time="144.942" type="appl" />
         <tli id="T272" time="145.381" type="appl" />
         <tli id="T273" time="145.82" type="appl" />
         <tli id="T274" time="146.185" type="appl" />
         <tli id="T275" time="146.491" type="appl" />
         <tli id="T276" time="146.796" type="appl" />
         <tli id="T277" time="147.102" type="appl" />
         <tli id="T278" time="147.407" type="appl" />
         <tli id="T279" time="147.713" type="appl" />
         <tli id="T280" time="148.018" type="appl" />
         <tli id="T281" time="148.324" type="appl" />
         <tli id="T282" time="148.629" type="appl" />
         <tli id="T283" time="148.935" type="appl" />
         <tli id="T284" time="151.12345728301725" />
         <tli id="T285" time="151.587" type="appl" />
         <tli id="T286" time="151.963" type="appl" />
         <tli id="T287" time="152.34" type="appl" />
         <tli id="T288" time="152.717" type="appl" />
         <tli id="T289" time="153.093" type="appl" />
         <tli id="T290" time="154.27005712586805" />
         <tli id="T291" time="154.622" type="appl" />
         <tli id="T292" time="154.993" type="appl" />
         <tli id="T293" time="155.365" type="appl" />
         <tli id="T294" time="155.737" type="appl" />
         <tli id="T295" time="156.108" type="appl" />
         <tli id="T296" time="156.48" type="appl" />
         <tli id="T297" time="156.852" type="appl" />
         <tli id="T298" time="157.223" type="appl" />
         <tli id="T299" time="157.595" type="appl" />
         <tli id="T300" time="157.967" type="appl" />
         <tli id="T301" time="158.338" type="appl" />
         <tli id="T302" time="158.77666714890952" />
         <tli id="T303" time="159.214" type="appl" />
         <tli id="T304" time="159.668" type="appl" />
         <tli id="T305" time="160.122" type="appl" />
         <tli id="T306" time="160.576" type="appl" />
         <tli id="T307" time="161.1033364878209" />
         <tli id="T308" time="161.663" type="appl" />
         <tli id="T309" time="162.146" type="appl" />
         <tli id="T310" time="162.629" type="appl" />
         <tli id="T311" time="163.112" type="appl" />
         <tli id="T312" time="163.595" type="appl" />
         <tli id="T313" time="164.078" type="appl" />
         <tli id="T314" time="164.561" type="appl" />
         <tli id="T315" time="165.044" type="appl" />
         <tli id="T316" time="165.527" type="appl" />
         <tli id="T317" time="166.01" type="appl" />
         <tli id="T318" time="166.39" type="appl" />
         <tli id="T319" time="166.71" type="appl" />
         <tli id="T320" time="167.03" type="appl" />
         <tli id="T321" time="167.35" type="appl" />
         <tli id="T322" time="167.67" type="appl" />
         <tli id="T323" time="167.99" type="appl" />
         <tli id="T324" time="168.31" type="appl" />
         <tli id="T325" time="168.63" type="appl" />
         <tli id="T326" time="168.95" type="appl" />
         <tli id="T327" time="169.27" type="appl" />
         <tli id="T328" time="169.74333529327447" />
         <tli id="T329" time="170.353" type="appl" />
         <tli id="T330" time="170.896" type="appl" />
         <tli id="T331" time="171.439" type="appl" />
         <tli id="T332" time="171.981" type="appl" />
         <tli id="T333" time="172.524" type="appl" />
         <tli id="T334" time="173.067" type="appl" />
         <tli id="T335" time="173.61" type="appl" />
         <tli id="T336" time="174.038" type="appl" />
         <tli id="T337" time="174.426" type="appl" />
         <tli id="T338" time="174.814" type="appl" />
         <tli id="T339" time="175.202" type="appl" />
         <tli id="T340" time="175.7033259094481" />
         <tli id="T341" time="176.087" type="appl" />
         <tli id="T342" time="176.494" type="appl" />
         <tli id="T343" time="176.901" type="appl" />
         <tli id="T344" time="177.308" type="appl" />
         <tli id="T345" time="177.714" type="appl" />
         <tli id="T346" time="178.121" type="appl" />
         <tli id="T347" time="178.528" type="appl" />
         <tli id="T348" time="178.935" type="appl" />
         <tli id="T349" time="179.342" type="appl" />
         <tli id="T350" time="179.749" type="appl" />
         <tli id="T351" time="180.156" type="appl" />
         <tli id="T352" time="180.562" type="appl" />
         <tli id="T353" time="180.969" type="appl" />
         <tli id="T354" time="181.376" type="appl" />
         <tli id="T355" time="181.783" type="appl" />
         <tli id="T356" time="182.34332812599547" />
         <tli id="T357" time="182.824" type="appl" />
         <tli id="T358" time="183.288" type="appl" />
         <tli id="T359" time="183.752" type="appl" />
         <tli id="T360" time="184.216" type="appl" />
         <tli id="T361" time="184.8466603798127" />
         <tli id="T362" time="185.193" type="appl" />
         <tli id="T363" time="185.557" type="appl" />
         <tli id="T364" time="185.92" type="appl" />
         <tli id="T365" time="186.283" type="appl" />
         <tli id="T366" time="186.647" type="appl" />
         <tli id="T367" time="187.01" type="appl" />
         <tli id="T368" time="187.373" type="appl" />
         <tli id="T369" time="187.737" type="appl" />
         <tli id="T370" time="188.1" type="appl" />
         <tli id="T371" time="188.463" type="appl" />
         <tli id="T372" time="188.827" type="appl" />
         <tli id="T373" time="189.19" type="appl" />
         <tli id="T374" time="189.553" type="appl" />
         <tli id="T375" time="189.917" type="appl" />
         <tli id="T376" time="190.28" type="appl" />
         <tli id="T377" time="190.643" type="appl" />
         <tli id="T378" time="191.007" type="appl" />
         <tli id="T379" time="191.37" type="appl" />
         <tli id="T380" time="191.733" type="appl" />
         <tli id="T381" time="192.097" type="appl" />
         <tli id="T382" time="192.59999832763492" />
         <tli id="T610" time="192.68149916381748" type="intp" />
         <tli id="T383" time="192.763" type="appl" />
         <tli id="T384" time="193.066" type="appl" />
         <tli id="T385" time="193.369" type="appl" />
         <tli id="T386" time="193.671" type="appl" />
         <tli id="T387" time="193.974" type="appl" />
         <tli id="T388" time="194.277" type="appl" />
         <tli id="T389" time="194.58" type="appl" />
         <tli id="T390" time="195.072" type="appl" />
         <tli id="T391" time="195.513" type="appl" />
         <tli id="T392" time="195.955" type="appl" />
         <tli id="T393" time="196.397" type="appl" />
         <tli id="T394" time="196.838" type="appl" />
         <tli id="T395" time="197.28" type="appl" />
         <tli id="T396" time="197.666" type="appl" />
         <tli id="T397" time="197.993" type="appl" />
         <tli id="T398" time="198.319" type="appl" />
         <tli id="T399" time="198.646" type="appl" />
         <tli id="T400" time="198.972" type="appl" />
         <tli id="T401" time="199.299" type="appl" />
         <tli id="T402" time="199.625" type="appl" />
         <tli id="T403" time="199.951" type="appl" />
         <tli id="T404" time="200.278" type="appl" />
         <tli id="T405" time="200.604" type="appl" />
         <tli id="T406" time="200.931" type="appl" />
         <tli id="T407" time="201.257" type="appl" />
         <tli id="T408" time="201.584" type="appl" />
         <tli id="T409" time="202.2099895543545" />
         <tli id="T410" time="202.546" type="appl" />
         <tli id="T411" time="203.052" type="appl" />
         <tli id="T412" time="203.559" type="appl" />
         <tli id="T413" time="204.065" type="appl" />
         <tli id="T414" time="204.571" type="appl" />
         <tli id="T415" time="205.078" type="appl" />
         <tli id="T416" time="205.584" type="appl" />
         <tli id="T417" time="206.09" type="appl" />
         <tli id="T418" time="206.456" type="appl" />
         <tli id="T419" time="206.772" type="appl" />
         <tli id="T420" time="207.088" type="appl" />
         <tli id="T421" time="207.404" type="appl" />
         <tli id="T422" time="207.72" type="appl" />
         <tli id="T423" time="208.036" type="appl" />
         <tli id="T424" time="208.352" type="appl" />
         <tli id="T425" time="208.668" type="appl" />
         <tli id="T426" time="208.984" type="appl" />
         <tli id="T427" time="209.36666048599463" />
         <tli id="T611" time="209.5693302429973" type="intp" />
         <tli id="T428" time="209.772" type="appl" />
         <tli id="T429" time="210.164" type="appl" />
         <tli id="T430" time="210.555" type="appl" />
         <tli id="T431" time="210.947" type="appl" />
         <tli id="T432" time="211.339" type="appl" />
         <tli id="T433" time="211.731" type="appl" />
         <tli id="T434" time="212.123" type="appl" />
         <tli id="T435" time="212.515" type="appl" />
         <tli id="T436" time="212.906" type="appl" />
         <tli id="T437" time="213.298" type="appl" />
         <tli id="T438" time="213.8233366736393" />
         <tli id="T439" time="214.21" type="appl" />
         <tli id="T440" time="214.68" type="appl" />
         <tli id="T441" time="215.15" type="appl" />
         <tli id="T442" time="215.62" type="appl" />
         <tli id="T443" time="216.09" type="appl" />
         <tli id="T444" time="216.56" type="appl" />
         <tli id="T445" time="217.03" type="appl" />
         <tli id="T446" time="217.5" type="appl" />
         <tli id="T447" time="217.97" type="appl" />
         <tli id="T448" time="218.44" type="appl" />
         <tli id="T449" time="218.97667775382786" />
         <tli id="T450" time="219.397" type="appl" />
         <tli id="T451" time="219.813" type="appl" />
         <tli id="T452" time="220.23" type="appl" />
         <tli id="T453" time="220.647" type="appl" />
         <tli id="T454" time="221.063" type="appl" />
         <tli id="T455" time="221.91528722206886" />
         <tli id="T456" time="222.488" type="appl" />
         <tli id="T457" time="222.967" type="appl" />
         <tli id="T458" time="223.445" type="appl" />
         <tli id="T459" time="223.923" type="appl" />
         <tli id="T460" time="224.402" type="appl" />
         <tli id="T461" time="224.88" type="appl" />
         <tli id="T462" time="225.358" type="appl" />
         <tli id="T463" time="225.837" type="appl" />
         <tli id="T464" time="226.315" type="appl" />
         <tli id="T465" time="226.793" type="appl" />
         <tli id="T466" time="227.272" type="appl" />
         <tli id="T467" time="227.75667358618784" />
         <tli id="T468" time="228.204" type="appl" />
         <tli id="T469" time="228.598" type="appl" />
         <tli id="T470" time="228.992" type="appl" />
         <tli id="T471" time="229.386" type="appl" />
         <tli id="T472" time="229.78" type="appl" />
         <tli id="T473" time="230.174" type="appl" />
         <tli id="T474" time="230.568" type="appl" />
         <tli id="T475" time="230.962" type="appl" />
         <tli id="T476" time="231.356" type="appl" />
         <tli id="T477" time="231.83666506508953" />
         <tli id="T478" time="232.273" type="appl" />
         <tli id="T479" time="232.676" type="appl" />
         <tli id="T480" time="233.079" type="appl" />
         <tli id="T481" time="233.481" type="appl" />
         <tli id="T482" time="233.884" type="appl" />
         <tli id="T483" time="234.287" type="appl" />
         <tli id="T484" time="234.69" type="appl" />
         <tli id="T485" time="235.132" type="appl" />
         <tli id="T486" time="235.544" type="appl" />
         <tli id="T487" time="235.956" type="appl" />
         <tli id="T488" time="236.368" type="appl" />
         <tli id="T489" time="236.6799997079997" />
         <tli id="T490" time="237.148" type="appl" />
         <tli id="T491" time="237.487" type="appl" />
         <tli id="T492" time="237.825" type="appl" />
         <tli id="T493" time="238.164" type="appl" />
         <tli id="T494" time="238.502" type="appl" />
         <tli id="T495" time="238.841" type="appl" />
         <tli id="T496" time="239.179" type="appl" />
         <tli id="T497" time="239.517" type="appl" />
         <tli id="T498" time="239.856" type="appl" />
         <tli id="T499" time="240.194" type="appl" />
         <tli id="T500" time="240.533" type="appl" />
         <tli id="T501" time="240.871" type="appl" />
         <tli id="T502" time="241.209" type="appl" />
         <tli id="T503" time="241.548" type="appl" />
         <tli id="T504" time="241.886" type="appl" />
         <tli id="T505" time="242.225" type="appl" />
         <tli id="T506" time="242.563" type="appl" />
         <tli id="T507" time="242.902" type="appl" />
         <tli id="T508" time="243.44665288071525" />
         <tli id="T509" time="243.876" type="appl" />
         <tli id="T510" time="244.301" type="appl" />
         <tli id="T511" time="244.727" type="appl" />
         <tli id="T512" time="245.152" type="appl" />
         <tli id="T513" time="245.578" type="appl" />
         <tli id="T514" time="246.003" type="appl" />
         <tli id="T515" time="246.429" type="appl" />
         <tli id="T516" time="246.854" type="appl" />
         <tli id="T517" time="247.28" type="appl" />
         <tli id="T518" time="247.705" type="appl" />
         <tli id="T519" time="248.131" type="appl" />
         <tli id="T520" time="248.556" type="appl" />
         <tli id="T521" time="248.982" type="appl" />
         <tli id="T522" time="249.407" type="appl" />
         <tli id="T523" time="249.833" type="appl" />
         <tli id="T524" time="250.258" type="appl" />
         <tli id="T525" time="250.684" type="appl" />
         <tli id="T526" time="251.11" type="appl" />
         <tli id="T527" time="251.535" type="appl" />
         <tli id="T528" time="251.961" type="appl" />
         <tli id="T529" time="252.386" type="appl" />
         <tli id="T530" time="252.812" type="appl" />
         <tli id="T531" time="253.237" type="appl" />
         <tli id="T532" time="253.663" type="appl" />
         <tli id="T533" time="254.088" type="appl" />
         <tli id="T534" time="254.514" type="appl" />
         <tli id="T535" time="254.939" type="appl" />
         <tli id="T536" time="255.365" type="appl" />
         <tli id="T537" time="255.79" type="appl" />
         <tli id="T538" time="256.216" type="appl" />
         <tli id="T539" time="256.641" type="appl" />
         <tli id="T540" time="257.067" type="appl" />
         <tli id="T541" time="257.492" type="appl" />
         <tli id="T542" time="257.918" type="appl" />
         <tli id="T543" time="258.343" type="appl" />
         <tli id="T544" time="258.769" type="appl" />
         <tli id="T545" time="259.377" type="appl" />
         <tli id="T546" time="259.863" type="appl" />
         <tli id="T547" time="260.42999012508227" />
         <tli id="T548" time="260.888" type="appl" />
         <tli id="T549" time="261.306" type="appl" />
         <tli id="T550" time="261.724" type="appl" />
         <tli id="T551" time="262.141" type="appl" />
         <tli id="T552" time="262.559" type="appl" />
         <tli id="T553" time="262.977" type="appl" />
         <tli id="T554" time="263.395" type="appl" />
         <tli id="T555" time="263.813" type="appl" />
         <tli id="T556" time="264.231" type="appl" />
         <tli id="T557" time="264.649" type="appl" />
         <tli id="T558" time="265.066" type="appl" />
         <tli id="T559" time="265.484" type="appl" />
         <tli id="T560" time="265.902" type="appl" />
         <tli id="T561" time="266.32" type="appl" />
         <tli id="T562" time="266.76" type="appl" />
         <tli id="T563" time="267.17" type="appl" />
         <tli id="T564" time="267.58" type="appl" />
         <tli id="T565" time="267.99" type="appl" />
         <tli id="T566" time="268.4" type="appl" />
         <tli id="T567" time="268.81" type="appl" />
         <tli id="T568" time="269.22" type="appl" />
         <tli id="T569" time="269.63" type="appl" />
         <tli id="T570" time="270.04" type="appl" />
         <tli id="T571" time="270.5300100076451" />
         <tli id="T572" time="271.076" type="appl" />
         <tli id="T573" time="271.532" type="appl" />
         <tli id="T574" time="271.988" type="appl" />
         <tli id="T575" time="272.444" type="appl" />
         <tli id="T576" time="272.9" type="appl" />
         <tli id="T577" time="273.393" type="appl" />
         <tli id="T578" time="273.847" type="appl" />
         <tli id="T579" time="274.3" type="appl" />
         <tli id="T580" time="274.753" type="appl" />
         <tli id="T581" time="275.207" type="appl" />
         <tli id="T582" time="275.66" type="appl" />
         <tli id="T583" time="276.113" type="appl" />
         <tli id="T584" time="276.567" type="appl" />
         <tli id="T585" time="277.02" type="appl" />
         <tli id="T586" time="277.473" type="appl" />
         <tli id="T587" time="277.927" type="appl" />
         <tli id="T588" time="278.38" type="appl" />
         <tli id="T614" time="279.4425" type="intp" />
         <tli id="T589" time="280.505" type="appl" />
         <tli id="T590" time="283.0073231540275" />
         <tli id="T591" time="283.065" type="appl" />
         <tli id="T592" time="283.5" type="appl" />
         <tli id="T593" time="283.935" type="appl" />
         <tli id="T594" time="284.37" type="appl" />
         <tli id="T595" time="284.805" type="appl" />
         <tli id="T596" time="285.24" type="appl" />
         <tli id="T597" time="285.675" type="appl" />
         <tli id="T598" time="286.11" type="appl" />
         <tli id="T599" time="286.545" type="appl" />
         <tli id="T600" time="286.98" type="appl" />
         <tli id="T601" time="287.415" type="appl" />
         <tli id="T602" time="288.7805338826478" />
         <tli id="T612" time="288.9203559217652" type="intp" />
         <tli id="T603" time="289.0601779608826" type="intp" />
         <tli id="T604" time="289.2" type="appl" />
         <tli id="T605" time="289.83" type="appl" />
         <tli id="T606" time="290.46" type="appl" />
         <tli id="T613" time="313.92" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AnIM"
                      type="t">
         <timeline-fork end="T47" start="T43">
            <tli id="T43.tx.1" />
            <tli id="T43.tx.2" />
            <tli id="T43.tx.3" />
            <tli id="T43.tx.4" />
            <tli id="T43.tx.5" />
            <tli id="T43.tx.6" />
            <tli id="T43.tx.7" />
            <tli id="T43.tx.8" />
            <tli id="T43.tx.9" />
            <tli id="T43.tx.10" />
         </timeline-fork>
         <timeline-fork end="T164" start="T160">
            <tli id="T160.tx.1" />
            <tli id="T160.tx.2" />
         </timeline-fork>
         <timeline-fork end="T366" start="T364">
            <tli id="T364.tx.1" />
         </timeline-fork>
         <timeline-fork end="T380" start="T378">
            <tli id="T378.tx.1" />
         </timeline-fork>
         <timeline-fork end="T385" start="T382">
            <tli id="T382.tx.1" />
            <tli id="T382.tx.2" />
            <tli id="T382.tx.3" />
         </timeline-fork>
         <timeline-fork end="T428" start="T427">
            <tli id="T427.tx.1" />
         </timeline-fork>
         <timeline-fork end="T465" start="T463">
            <tli id="T463.tx.1" />
         </timeline-fork>
         <timeline-fork end="T614" start="T588">
            <tli id="T588.tx.1" />
            <tli id="T588.tx.2" />
            <tli id="T588.tx.3" />
            <tli id="T588.tx.4" />
            <tli id="T588.tx.5" />
            <tli id="T588.tx.6" />
         </timeline-fork>
         <timeline-fork end="T597" start="T592">
            <tli id="T592.tx.1" />
            <tli id="T592.tx.2" />
            <tli id="T592.tx.3" />
            <tli id="T592.tx.4" />
            <tli id="T592.tx.5" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T606" id="Seg_0" n="sc" s="T0">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Ogo</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">erdekpinen</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">bu͡ollagɨnan</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">bu͡o</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">ogolorum</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">kɨra</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">etilere</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_26" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">Biːr</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">ogom</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">dʼɨllaːk</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">ete</ts>
                  <nts id="Seg_38" n="HIAT:ip">.</nts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_41" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">Dʼɨltan</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">orduktaːk</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">god</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">i</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">sem</ts>
                  <nts id="Seg_56" n="HIAT:ip">.</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_59" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_61" n="HIAT:w" s="T16">Biːr</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_64" n="HIAT:w" s="T17">ogom</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">bu͡ollagɨna</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">agɨs</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_73" n="HIAT:w" s="T20">ɨjdaːk</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">ete</ts>
                  <nts id="Seg_77" n="HIAT:ip">.</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_80" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">Oččogo</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">bu͡ollagɨnan</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_88" n="HIAT:w" s="T24">bu͡o</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_91" n="HIAT:w" s="T25">teːtem</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_94" n="HIAT:w" s="T26">mas</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_97" n="HIAT:w" s="T27">egele</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_100" n="HIAT:w" s="T28">barammɨn</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_103" n="HIAT:w" s="T29">teːtem</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_106" n="HIAT:w" s="T30">gɨtta</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_109" n="HIAT:w" s="T31">barsɨbɨt</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_112" n="HIAT:w" s="T32">etim</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_115" n="HIAT:w" s="T33">hetiːleːkpin</ts>
                  <nts id="Seg_116" n="HIAT:ip">.</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_119" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_121" n="HIAT:w" s="T34">Hetiːleːppin</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_124" n="HIAT:w" s="T35">hetiːber</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_127" n="HIAT:w" s="T36">bu͡ollagɨna</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_130" n="HIAT:w" s="T37">baltɨm</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_133" n="HIAT:w" s="T38">oloror</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_136" n="HIAT:w" s="T39">ete</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_139" n="HIAT:w" s="T40">bi͡es</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_142" n="HIAT:w" s="T41">dʼɨllaːk</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_145" n="HIAT:w" s="T42">baltɨm</ts>
                  <nts id="Seg_146" n="HIAT:ip">.</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_149" n="HIAT:u" s="T43">
                  <ts e="T43.tx.1" id="Seg_151" n="HIAT:w" s="T43">Ješʼo</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43.tx.2" id="Seg_154" n="HIAT:w" s="T43.tx.1">ja</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43.tx.3" id="Seg_157" n="HIAT:w" s="T43.tx.2">vam</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43.tx.4" id="Seg_160" n="HIAT:w" s="T43.tx.3">ne</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43.tx.5" id="Seg_163" n="HIAT:w" s="T43.tx.4">rasskazala</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43.tx.6" id="Seg_166" n="HIAT:w" s="T43.tx.5">že</ts>
                  <nts id="Seg_167" n="HIAT:ip">,</nts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43.tx.7" id="Seg_170" n="HIAT:w" s="T43.tx.6">u</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43.tx.8" id="Seg_173" n="HIAT:w" s="T43.tx.7">nas</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43.tx.9" id="Seg_176" n="HIAT:w" s="T43.tx.8">oleni</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43.tx.10" id="Seg_179" n="HIAT:w" s="T43.tx.9">že</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_182" n="HIAT:w" s="T43.tx.10">raznɨe</ts>
                  <nts id="Seg_183" n="HIAT:ip">,</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_186" n="HIAT:w" s="T47">oččogo</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_189" n="HIAT:w" s="T48">že</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_192" n="HIAT:w" s="T49">dʼaktar</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_195" n="HIAT:w" s="T50">bu͡ollagɨna</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_198" n="HIAT:w" s="T51">bu͡o</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_201" n="HIAT:w" s="T52">bugdi</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_204" n="HIAT:w" s="T53">tabanɨ</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_207" n="HIAT:w" s="T54">ne</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_210" n="HIAT:w" s="T55">dolžna</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_213" n="HIAT:w" s="T56">ona</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_216" n="HIAT:w" s="T57">kimni͡egin</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_219" n="HIAT:w" s="T58">ke</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_222" n="HIAT:w" s="T59">kölüjü͡ögün</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_225" n="HIAT:w" s="T60">ke</ts>
                  <nts id="Seg_226" n="HIAT:ip">.</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_229" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_231" n="HIAT:w" s="T62">I</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_234" n="HIAT:w" s="T63">oččogo</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_237" n="HIAT:w" s="T64">bu͡ollagɨna</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_240" n="HIAT:w" s="T65">bu͡o</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_243" n="HIAT:w" s="T66">tɨnnastɨbɨt</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_246" n="HIAT:w" s="T67">taba</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_249" n="HIAT:w" s="T68">potom</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_252" n="HIAT:w" s="T69">ja</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_255" n="HIAT:w" s="T70">rasskazhu</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_258" n="HIAT:w" s="T71">da</ts>
                  <nts id="Seg_259" n="HIAT:ip">,</nts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_262" n="HIAT:w" s="T72">tɨnnastɨbɨt</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_265" n="HIAT:w" s="T73">taba</ts>
                  <nts id="Seg_266" n="HIAT:ip">.</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_269" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_271" n="HIAT:w" s="T74">Eto</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_274" n="HIAT:w" s="T75">tɨnnastɨbɨt</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_277" n="HIAT:w" s="T76">taba</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_280" n="HIAT:w" s="T77">bu͡ollagɨnan</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_283" n="HIAT:w" s="T78">bu͡o</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_286" n="HIAT:w" s="T79">eh</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_289" n="HIAT:w" s="T80">tɨnnastɨbɨt</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_292" n="HIAT:w" s="T81">taba</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_295" n="HIAT:w" s="T82">ɨ͡aldʼarɨgar</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_298" n="HIAT:w" s="T83">bu͡o</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_301" n="HIAT:w" s="T84">erim</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_304" n="HIAT:w" s="T85">inʼete</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_307" n="HIAT:w" s="T86">tɨnnastaːččɨ</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_310" n="HIAT:w" s="T87">ete</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_313" n="HIAT:w" s="T88">menʼiːtiger</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_316" n="HIAT:w" s="T89">tabanɨ</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_319" n="HIAT:w" s="T90">ke</ts>
                  <nts id="Seg_320" n="HIAT:ip">.</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_323" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_325" n="HIAT:w" s="T91">Ol</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_328" n="HIAT:w" s="T92">tabanɨ</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_331" n="HIAT:w" s="T93">bu͡olla</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_334" n="HIAT:w" s="T94">dʼaktar</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_337" n="HIAT:w" s="T95">kölüjü͡ögün</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_340" n="HIAT:w" s="T96">nelzja</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_343" n="HIAT:w" s="T97">ete</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_346" n="HIAT:w" s="T98">bu͡o</ts>
                  <nts id="Seg_347" n="HIAT:ip">.</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_350" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_352" n="HIAT:w" s="T99">Oččogo</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_355" n="HIAT:w" s="T100">min</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_358" n="HIAT:w" s="T101">bilbekkebin</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_361" n="HIAT:w" s="T102">bu</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_364" n="HIAT:w" s="T103">tabanɨ</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_367" n="HIAT:w" s="T104">kostuːrdaːmmɨppɨn</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_370" n="HIAT:w" s="T105">bu͡o</ts>
                  <nts id="Seg_371" n="HIAT:ip">.</nts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_374" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_376" n="HIAT:w" s="T106">Ontuŋ</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_379" n="HIAT:w" s="T107">čeːlke</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_382" n="HIAT:w" s="T108">taba</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_385" n="HIAT:w" s="T109">buruna</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_388" n="HIAT:w" s="T110">taba</ts>
                  <nts id="Seg_389" n="HIAT:ip">.</nts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_392" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_394" n="HIAT:w" s="T111">Inʼe</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_397" n="HIAT:w" s="T112">bu͡ollagɨna</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_400" n="HIAT:w" s="T113">baraːrɨbɨt</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_403" n="HIAT:w" s="T114">bu͡ollagɨna</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_406" n="HIAT:w" s="T115">ol</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_409" n="HIAT:w" s="T116">taba</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_412" n="HIAT:w" s="T117">bu͡o</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_415" n="HIAT:w" s="T118">ɨtɨrdar</ts>
                  <nts id="Seg_416" n="HIAT:ip">.</nts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_419" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_421" n="HIAT:w" s="T119">Üste</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_424" n="HIAT:w" s="T120">raz</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_427" n="HIAT:w" s="T121">ɨtɨrdar</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_430" n="HIAT:w" s="T122">bu͡o</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_433" n="HIAT:w" s="T123">oččogo</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_436" n="HIAT:w" s="T124">min</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_439" n="HIAT:w" s="T125">diːbin</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_442" n="HIAT:w" s="T126">bu͡o</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_445" n="HIAT:w" s="T127">araː</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_448" n="HIAT:w" s="T128">tu͡okka</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_451" n="HIAT:w" s="T129">ɨtɨrdar</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_454" n="HIAT:w" s="T130">ke</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_457" n="HIAT:w" s="T131">di͡ebin</ts>
                  <nts id="Seg_458" n="HIAT:ip">.</nts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_461" n="HIAT:u" s="T132">
                  <ts e="T608" id="Seg_463" n="HIAT:w" s="T132">A</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_466" n="HIAT:w" s="T608">do</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_469" n="HIAT:w" s="T133">etogo</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_471" n="HIAT:ip">(</nts>
                  <ts e="T135" id="Seg_473" n="HIAT:w" s="T134">iti</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_476" n="HIAT:w" s="T135">innitinen</ts>
                  <nts id="Seg_477" n="HIAT:ip">)</nts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_480" n="HIAT:w" s="T136">kaːmnaːrɨbɨt</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_483" n="HIAT:w" s="T137">maːmam</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_486" n="HIAT:w" s="T138">diːr</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_489" n="HIAT:w" s="T139">bu͡o</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_492" n="HIAT:w" s="T140">u͡opput</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_495" n="HIAT:w" s="T141">haŋarar</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_498" n="HIAT:w" s="T142">diːr</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_501" n="HIAT:w" s="T143">u͡opput</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_504" n="HIAT:w" s="T144">haŋarar</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_507" n="HIAT:w" s="T145">eto</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_510" n="HIAT:w" s="T146">značit</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_513" n="HIAT:w" s="T147">bu͡ollagɨna</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_516" n="HIAT:w" s="T148">tu͡ok</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_519" n="HIAT:w" s="T149">ere</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_522" n="HIAT:w" s="T150">kuhagan</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_525" n="HIAT:w" s="T151">bu͡olaːččɨ</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_528" n="HIAT:w" s="T152">u͡ot</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_531" n="HIAT:w" s="T153">haŋardagɨna</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_534" n="HIAT:w" s="T154">ke</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_537" n="HIAT:w" s="T155">haŋardagɨna</ts>
                  <nts id="Seg_538" n="HIAT:ip">.</nts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_541" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_543" n="HIAT:w" s="T157">Oččogo</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_546" n="HIAT:w" s="T158">bu͡ollagɨna</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_549" n="HIAT:w" s="T159">nu</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_551" n="HIAT:ip">(</nts>
                  <ts e="T160.tx.1" id="Seg_553" n="HIAT:w" s="T160">ja</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160.tx.2" id="Seg_556" n="HIAT:w" s="T160.tx.1">ne</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_559" n="HIAT:w" s="T160.tx.2">s-</ts>
                  <nts id="Seg_560" n="HIAT:ip">)</nts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_563" n="HIAT:w" s="T164">min</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_566" n="HIAT:w" s="T165">bu͡ollagɨna</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_569" n="HIAT:w" s="T166">olus</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_572" n="HIAT:w" s="T167">daːgɨnɨ</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_575" n="HIAT:w" s="T168">onu</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_578" n="HIAT:w" s="T169">bu͡ollagɨnan</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_581" n="HIAT:w" s="T170">itegejbekkebin</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_584" n="HIAT:w" s="T171">bu͡ollagɨna</ts>
                  <nts id="Seg_585" n="HIAT:ip">.</nts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_588" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_590" n="HIAT:w" s="T172">De</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_593" n="HIAT:w" s="T173">taba</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_596" n="HIAT:w" s="T174">tutunnubut</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_599" n="HIAT:w" s="T175">da</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_602" n="HIAT:w" s="T176">baran</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_605" n="HIAT:w" s="T177">kaːllɨbɨt</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_608" n="HIAT:w" s="T178">bu͡o</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_611" n="HIAT:w" s="T179">ol</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_614" n="HIAT:w" s="T180">bu</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_617" n="HIAT:w" s="T181">tabanɨ</ts>
                  <nts id="Seg_618" n="HIAT:ip">.</nts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_621" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_623" n="HIAT:w" s="T182">Kappɨppɨt</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_626" n="HIAT:w" s="T183">innʼe</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_629" n="HIAT:w" s="T184">baran</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_632" n="HIAT:w" s="T185">kaːlbɨppɨt</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_635" n="HIAT:w" s="T186">bu͡o</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_638" n="HIAT:w" s="T187">teːtebin</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_641" n="HIAT:w" s="T188">gɨtta</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_644" n="HIAT:w" s="T189">barsan</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_647" n="HIAT:w" s="T190">kaːlbɨppɨt</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_650" n="HIAT:w" s="T191">barsan</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_652" n="HIAT:ip">(</nts>
                  <ts e="T193" id="Seg_654" n="HIAT:w" s="T192">kaːl-</ts>
                  <nts id="Seg_655" n="HIAT:ip">)</nts>
                  <nts id="Seg_656" n="HIAT:ip">.</nts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_659" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_661" n="HIAT:w" s="T193">Huːrka</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_664" n="HIAT:w" s="T194">kelen</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_667" n="HIAT:w" s="T195">baran</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_670" n="HIAT:w" s="T196">bu͡ollagɨnan</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_673" n="HIAT:w" s="T197">teːtem</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_676" n="HIAT:w" s="T198">hɨrgabɨtɨn</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_679" n="HIAT:w" s="T199">hetiːbitin</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_682" n="HIAT:w" s="T200">bu͡ollagɨnan</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_685" n="HIAT:w" s="T201">ti͡ennibit</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_688" n="HIAT:w" s="T202">ti͡enen</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_691" n="HIAT:w" s="T203">ötüleːnen</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_694" n="HIAT:w" s="T204">kanʼan</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_697" n="HIAT:w" s="T205">bu͡o</ts>
                  <nts id="Seg_698" n="HIAT:ip">.</nts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_701" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_703" n="HIAT:w" s="T206">Minigin</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_706" n="HIAT:w" s="T207">de</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_709" n="HIAT:w" s="T208">kamnaːtta</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_712" n="HIAT:w" s="T209">bu͡o</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_715" n="HIAT:w" s="T210">hɨrgalaːk</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_718" n="HIAT:w" s="T211">tababɨn</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_721" n="HIAT:w" s="T212">hi͡eten</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_724" n="HIAT:w" s="T213">baran</ts>
                  <nts id="Seg_725" n="HIAT:ip">.</nts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_728" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_730" n="HIAT:w" s="T214">Hɨrgalaːk</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_733" n="HIAT:w" s="T215">tababɨn</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_736" n="HIAT:w" s="T216">hi͡eten</ts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_739" n="HIAT:w" s="T217">barammɨn</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_742" n="HIAT:w" s="T218">bu͡o</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_745" n="HIAT:w" s="T219">hɨːha</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_748" n="HIAT:w" s="T220">olorobun</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_751" n="HIAT:w" s="T221">bu͡o</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_754" n="HIAT:w" s="T222">hɨrgabar</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_757" n="HIAT:w" s="T223">bu͡o</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_760" n="HIAT:w" s="T224">balɨktarɨ</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_763" n="HIAT:w" s="T225">ti͡ejbippin</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_766" n="HIAT:w" s="T226">alta</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_769" n="HIAT:w" s="T227">kul</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_772" n="HIAT:w" s="T228">balɨk</ts>
                  <nts id="Seg_773" n="HIAT:ip">.</nts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T235" id="Seg_776" n="HIAT:u" s="T229">
                  <ts e="T230" id="Seg_778" n="HIAT:w" s="T229">Alta</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_781" n="HIAT:w" s="T230">kul</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_784" n="HIAT:w" s="T231">balɨk</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_787" n="HIAT:w" s="T232">ti͡ejen</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_790" n="HIAT:w" s="T233">baran</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_793" n="HIAT:w" s="T234">bu͡ollana</ts>
                  <nts id="Seg_794" n="HIAT:ip">.</nts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_797" n="HIAT:u" s="T235">
                  <ts e="T236" id="Seg_799" n="HIAT:w" s="T235">Hɨrgalar</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_802" n="HIAT:w" s="T236">ɨstanabin</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_805" n="HIAT:w" s="T237">diːbin</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_808" n="HIAT:w" s="T238">bu͡o</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_811" n="HIAT:w" s="T239">hɨrgabɨn</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_814" n="HIAT:w" s="T240">hɨːha</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_817" n="HIAT:w" s="T241">olorobun</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_820" n="HIAT:w" s="T242">bu͡o</ts>
                  <nts id="Seg_821" n="HIAT:ip">.</nts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_824" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_826" n="HIAT:w" s="T243">Hu͡ok</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_829" n="HIAT:w" s="T244">bu</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_832" n="HIAT:w" s="T245">kihi</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_835" n="HIAT:w" s="T246">bu͡o</ts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_838" n="HIAT:w" s="T247">ješo</ts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_841" n="HIAT:w" s="T248">da</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_844" n="HIAT:w" s="T249">ɨstanabɨn</ts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_847" n="HIAT:w" s="T250">bu͡o</ts>
                  <nts id="Seg_848" n="HIAT:ip">.</nts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T255" id="Seg_851" n="HIAT:u" s="T251">
                  <ts e="T609" id="Seg_853" n="HIAT:w" s="T251">Ješo</ts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_856" n="HIAT:w" s="T609">raz</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_859" n="HIAT:w" s="T252">ɨstanabɨn</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_862" n="HIAT:w" s="T253">bu͡o</ts>
                  <nts id="Seg_863" n="HIAT:ip">.</nts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T259" id="Seg_866" n="HIAT:u" s="T255">
                  <ts e="T256" id="Seg_868" n="HIAT:w" s="T255">Emi͡e</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_871" n="HIAT:w" s="T256">hɨːha</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_874" n="HIAT:w" s="T257">olorobun</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_877" n="HIAT:w" s="T258">bu͡o</ts>
                  <nts id="Seg_878" n="HIAT:ip">.</nts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T264" id="Seg_881" n="HIAT:u" s="T259">
                  <ts e="T260" id="Seg_883" n="HIAT:w" s="T259">Hɨrgalaːk</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_886" n="HIAT:w" s="T260">tababɨn</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_889" n="HIAT:w" s="T261">ɨhɨktan</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_892" n="HIAT:w" s="T262">keːstim</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_895" n="HIAT:w" s="T263">bu͡o</ts>
                  <nts id="Seg_896" n="HIAT:ip">.</nts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T273" id="Seg_899" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_901" n="HIAT:w" s="T264">Innʼe</ts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_904" n="HIAT:w" s="T265">hɨrgalaːk</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_907" n="HIAT:w" s="T266">tabam</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_910" n="HIAT:w" s="T267">bu͡ollagɨna</ts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_913" n="HIAT:w" s="T268">hetiːm</ts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_916" n="HIAT:w" s="T269">tabalara</ts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_919" n="HIAT:w" s="T270">ürdübünen</ts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_922" n="HIAT:w" s="T271">barallar</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_925" n="HIAT:w" s="T272">bu͡o</ts>
                  <nts id="Seg_926" n="HIAT:ip">.</nts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T284" id="Seg_929" n="HIAT:u" s="T273">
                  <ts e="T274" id="Seg_931" n="HIAT:w" s="T273">I</ts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_934" n="HIAT:w" s="T274">bejem</ts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_937" n="HIAT:w" s="T275">bu͡ollagɨnan</ts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_940" n="HIAT:w" s="T276">bu͡o</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_943" n="HIAT:w" s="T277">hetiː</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_946" n="HIAT:w" s="T278">ɨlagɨn</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_949" n="HIAT:w" s="T279">aːjɨnan</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_952" n="HIAT:w" s="T280">üste</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_955" n="HIAT:w" s="T281">raz</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_958" n="HIAT:w" s="T282">ergijdim</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_961" n="HIAT:w" s="T283">bu͡o</ts>
                  <nts id="Seg_962" n="HIAT:ip">.</nts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T290" id="Seg_965" n="HIAT:u" s="T284">
                  <ts e="T285" id="Seg_967" n="HIAT:w" s="T284">Araː</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_970" n="HIAT:w" s="T285">innʼe</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_973" n="HIAT:w" s="T286">bu͡o</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_976" n="HIAT:w" s="T287">ittenne</ts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_979" n="HIAT:w" s="T288">tühen</ts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_981" n="HIAT:ip">(</nts>
                  <ts e="T290" id="Seg_983" n="HIAT:w" s="T289">kaːl-</ts>
                  <nts id="Seg_984" n="HIAT:ip">)</nts>
                  <nts id="Seg_985" n="HIAT:ip">.</nts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T302" id="Seg_988" n="HIAT:u" s="T290">
                  <ts e="T291" id="Seg_990" n="HIAT:w" s="T290">Ittenne</ts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_993" n="HIAT:w" s="T291">tühen</ts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_996" n="HIAT:w" s="T292">baran</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_999" n="HIAT:w" s="T293">bu͡o</ts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1002" n="HIAT:w" s="T294">hɨtabɨn</ts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1005" n="HIAT:w" s="T295">bu͡o</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1008" n="HIAT:w" s="T296">teːtem</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1011" n="HIAT:w" s="T297">hokuj</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1014" n="HIAT:w" s="T298">kette</ts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1017" n="HIAT:w" s="T299">turar</ts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1020" n="HIAT:w" s="T300">ete</ts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1023" n="HIAT:w" s="T301">bu͡o</ts>
                  <nts id="Seg_1024" n="HIAT:ip">.</nts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T307" id="Seg_1027" n="HIAT:u" s="T302">
                  <ts e="T303" id="Seg_1029" n="HIAT:w" s="T302">Hokuj</ts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1032" n="HIAT:w" s="T303">keten</ts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1035" n="HIAT:w" s="T304">baran</ts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1038" n="HIAT:w" s="T305">bukaːn</ts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1041" n="HIAT:w" s="T306">körbüte</ts>
                  <nts id="Seg_1042" n="HIAT:ip">.</nts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T317" id="Seg_1045" n="HIAT:u" s="T307">
                  <ts e="T308" id="Seg_1047" n="HIAT:w" s="T307">Körör</ts>
                  <nts id="Seg_1048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1050" n="HIAT:w" s="T308">hɨrgalaːk</ts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1053" n="HIAT:w" s="T309">taba</ts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1056" n="HIAT:w" s="T310">bara</ts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1059" n="HIAT:w" s="T311">turar</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1062" n="HIAT:w" s="T312">kihite</ts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1065" n="HIAT:w" s="T313">bu͡o</ts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1068" n="HIAT:w" s="T314">munna</ts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1071" n="HIAT:w" s="T315">hɨtar</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1074" n="HIAT:w" s="T316">hu͡olga</ts>
                  <nts id="Seg_1075" n="HIAT:ip">.</nts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T328" id="Seg_1078" n="HIAT:u" s="T317">
                  <ts e="T318" id="Seg_1080" n="HIAT:w" s="T317">De</ts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1083" n="HIAT:w" s="T318">hüːren</ts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1086" n="HIAT:w" s="T319">kelle</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1089" n="HIAT:w" s="T320">bu͡o</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1092" n="HIAT:w" s="T321">innʼe</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1095" n="HIAT:w" s="T322">di͡ete</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1098" n="HIAT:w" s="T323">kaja</ts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1101" n="HIAT:w" s="T324">en</ts>
                  <nts id="Seg_1102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1104" n="HIAT:w" s="T325">tɨːnnaːkkɨn</ts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1107" n="HIAT:w" s="T326">du͡o</ts>
                  <nts id="Seg_1108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1110" n="HIAT:w" s="T327">di͡ete</ts>
                  <nts id="Seg_1111" n="HIAT:ip">.</nts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T335" id="Seg_1114" n="HIAT:u" s="T328">
                  <ts e="T329" id="Seg_1116" n="HIAT:w" s="T328">Heee</ts>
                  <nts id="Seg_1117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1119" n="HIAT:w" s="T329">tɨːnnaːppɨn</ts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1122" n="HIAT:w" s="T330">tɨːnnaːppɨn</ts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1125" n="HIAT:w" s="T331">atagɨm</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1128" n="HIAT:w" s="T332">ere</ts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1131" n="HIAT:w" s="T333">kɨ͡ajan</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1134" n="HIAT:w" s="T334">kaːmnaːbat</ts>
                  <nts id="Seg_1135" n="HIAT:ip">.</nts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T340" id="Seg_1138" n="HIAT:u" s="T335">
                  <ts e="T336" id="Seg_1140" n="HIAT:w" s="T335">Eee</ts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1143" n="HIAT:w" s="T336">de</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1146" n="HIAT:w" s="T337">beːbe</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1149" n="HIAT:w" s="T338">min</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1152" n="HIAT:w" s="T339">keli͡em</ts>
                  <nts id="Seg_1153" n="HIAT:ip">.</nts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T356" id="Seg_1156" n="HIAT:u" s="T340">
                  <ts e="T341" id="Seg_1158" n="HIAT:w" s="T340">De</ts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1161" n="HIAT:w" s="T341">kelen</ts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1164" n="HIAT:w" s="T342">baran</ts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1167" n="HIAT:w" s="T343">hɨrgalaːk</ts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1170" n="HIAT:w" s="T344">tabatɨgar</ts>
                  <nts id="Seg_1171" n="HIAT:ip">,</nts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1174" n="HIAT:w" s="T345">kajdak</ts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1177" n="HIAT:w" s="T346">ere</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1180" n="HIAT:w" s="T347">miːnen</ts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1183" n="HIAT:w" s="T348">baran</ts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1186" n="HIAT:w" s="T349">bu͡o</ts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1189" n="HIAT:w" s="T350">min</ts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1192" n="HIAT:w" s="T351">hɨrgalaːk</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1195" n="HIAT:w" s="T352">tababɨn</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1198" n="HIAT:w" s="T353">hite</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1201" n="HIAT:w" s="T354">barabɨt</ts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1204" n="HIAT:w" s="T355">bu͡o</ts>
                  <nts id="Seg_1205" n="HIAT:ip">.</nts>
                  <nts id="Seg_1206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T361" id="Seg_1208" n="HIAT:u" s="T356">
                  <ts e="T357" id="Seg_1210" n="HIAT:w" s="T356">Onno</ts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1213" n="HIAT:w" s="T357">bu͡o</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1216" n="HIAT:w" s="T358">kü͡ölü</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1219" n="HIAT:w" s="T359">ɨttaːrɨbɨt</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1222" n="HIAT:w" s="T360">eni͡ege</ts>
                  <nts id="Seg_1223" n="HIAT:ip">.</nts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T382" id="Seg_1226" n="HIAT:u" s="T361">
                  <ts e="T362" id="Seg_1228" n="HIAT:w" s="T361">Eni͡e</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1231" n="HIAT:w" s="T362">üstün</ts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1234" n="HIAT:w" s="T363">bu͡ollagɨna</ts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364.tx.1" id="Seg_1237" n="HIAT:w" s="T364">kak</ts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1240" n="HIAT:w" s="T364.tx.1">raz</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1243" n="HIAT:w" s="T366">baltɨm</ts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1246" n="HIAT:w" s="T367">baːr</ts>
                  <nts id="Seg_1247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1249" n="HIAT:w" s="T368">ete</ts>
                  <nts id="Seg_1250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1252" n="HIAT:w" s="T369">ol</ts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1255" n="HIAT:w" s="T370">muŋnaːk</ts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1258" n="HIAT:w" s="T371">hɨrgattan</ts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1261" n="HIAT:w" s="T372">tühen</ts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1264" n="HIAT:w" s="T373">baran</ts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1267" n="HIAT:w" s="T374">nʼuŋuːnu</ts>
                  <nts id="Seg_1268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1270" n="HIAT:w" s="T375">kaban</ts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1273" n="HIAT:w" s="T376">ɨlla</ts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1276" n="HIAT:w" s="T377">innʼe</ts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378.tx.1" id="Seg_1279" n="HIAT:w" s="T378">kak</ts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1282" n="HIAT:w" s="T378.tx.1">raz</ts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1285" n="HIAT:w" s="T380">tabalar</ts>
                  <nts id="Seg_1286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1288" n="HIAT:w" s="T381">toktoːtular</ts>
                  <nts id="Seg_1289" n="HIAT:ip">.</nts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T389" id="Seg_1292" n="HIAT:u" s="T382">
                  <ts e="T382.tx.1" id="Seg_1294" n="HIAT:w" s="T382">I</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382.tx.2" id="Seg_1297" n="HIAT:w" s="T382.tx.1">v</ts>
                  <nts id="Seg_1298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382.tx.3" id="Seg_1300" n="HIAT:w" s="T382.tx.2">eto</ts>
                  <nts id="Seg_1301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1303" n="HIAT:w" s="T382.tx.3">vremja</ts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1306" n="HIAT:w" s="T385">bihigi</ts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1309" n="HIAT:w" s="T386">kelebit</ts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1312" n="HIAT:w" s="T387">bu͡o</ts>
                  <nts id="Seg_1313" n="HIAT:ip">.</nts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1316" n="HIAT:u" s="T389">
                  <ts e="T390" id="Seg_1318" n="HIAT:w" s="T389">Teːtem</ts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1321" n="HIAT:w" s="T390">hɨrgalaːk</ts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1324" n="HIAT:w" s="T391">tababɨn</ts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1327" n="HIAT:w" s="T392">baːjan</ts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1330" n="HIAT:w" s="T393">keːste</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1333" n="HIAT:w" s="T394">mini͡enin</ts>
                  <nts id="Seg_1334" n="HIAT:ip">.</nts>
                  <nts id="Seg_1335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T409" id="Seg_1337" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_1339" n="HIAT:w" s="T395">Innʼe</ts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1342" n="HIAT:w" s="T396">gɨnan</ts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1345" n="HIAT:w" s="T397">baran</ts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1348" n="HIAT:w" s="T398">bejete</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1351" n="HIAT:w" s="T399">bu͡o</ts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1354" n="HIAT:w" s="T400">min</ts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1357" n="HIAT:w" s="T401">hɨrgalaːk</ts>
                  <nts id="Seg_1358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1360" n="HIAT:w" s="T402">tabalar</ts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1363" n="HIAT:w" s="T403">miːnen</ts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1366" n="HIAT:w" s="T404">baran</ts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1369" n="HIAT:w" s="T405">ol</ts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1372" n="HIAT:w" s="T406">di͡ebitiger</ts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1375" n="HIAT:w" s="T407">bardɨbɨt</ts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1378" n="HIAT:w" s="T408">bu͡o</ts>
                  <nts id="Seg_1379" n="HIAT:ip">.</nts>
                  <nts id="Seg_1380" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T417" id="Seg_1382" n="HIAT:u" s="T409">
                  <ts e="T410" id="Seg_1384" n="HIAT:w" s="T409">Di͡ebitiger</ts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1387" n="HIAT:w" s="T410">baran</ts>
                  <nts id="Seg_1388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1390" n="HIAT:w" s="T411">baran</ts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1393" n="HIAT:w" s="T412">bu͡ollagɨnan</ts>
                  <nts id="Seg_1394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1396" n="HIAT:w" s="T413">kimi͡eke</ts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1399" n="HIAT:w" s="T414">aːmmar</ts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1402" n="HIAT:w" s="T415">toktotor</ts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1405" n="HIAT:w" s="T416">bu͡o</ts>
                  <nts id="Seg_1406" n="HIAT:ip">.</nts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T427" id="Seg_1409" n="HIAT:u" s="T417">
                  <ts e="T418" id="Seg_1411" n="HIAT:w" s="T417">Bu͡ologum</ts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1414" n="HIAT:w" s="T418">aːnɨgar</ts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1417" n="HIAT:w" s="T419">ke</ts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1420" n="HIAT:w" s="T420">innʼe</ts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1423" n="HIAT:w" s="T421">bu͡o</ts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1426" n="HIAT:w" s="T422">emeːksitter</ts>
                  <nts id="Seg_1427" n="HIAT:ip">,</nts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1430" n="HIAT:w" s="T423">ikki</ts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1433" n="HIAT:w" s="T424">emeːksin</ts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1436" n="HIAT:w" s="T425">baːr</ts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1439" n="HIAT:w" s="T426">ete</ts>
                  <nts id="Seg_1440" n="HIAT:ip">.</nts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T438" id="Seg_1443" n="HIAT:u" s="T427">
                  <ts e="T427.tx.1" id="Seg_1445" n="HIAT:w" s="T427">Dve</ts>
                  <nts id="Seg_1446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1448" n="HIAT:w" s="T427.tx.1">babki</ts>
                  <nts id="Seg_1449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1451" n="HIAT:w" s="T428">olor</ts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1454" n="HIAT:w" s="T429">kelliler</ts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1456" n="HIAT:ip">"</nts>
                  <ts e="T431" id="Seg_1458" n="HIAT:w" s="T430">kaja</ts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1461" n="HIAT:w" s="T431">bu͡o</ts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1464" n="HIAT:w" s="T432">tu͡ok</ts>
                  <nts id="Seg_1465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1467" n="HIAT:w" s="T433">bu͡ollugut</ts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1470" n="HIAT:w" s="T434">kajdak</ts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1473" n="HIAT:w" s="T435">kajdak</ts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1476" n="HIAT:w" s="T436">bu͡ollugut</ts>
                  <nts id="Seg_1477" n="HIAT:ip">"</nts>
                  <nts id="Seg_1478" n="HIAT:ip">.</nts>
                  <nts id="Seg_1479" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T449" id="Seg_1481" n="HIAT:u" s="T438">
                  <ts e="T439" id="Seg_1483" n="HIAT:w" s="T438">Araː</ts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1486" n="HIAT:w" s="T439">bu</ts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1489" n="HIAT:w" s="T440">mini͡eke</ts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1492" n="HIAT:w" s="T441">kata</ts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1495" n="HIAT:w" s="T442">bu</ts>
                  <nts id="Seg_1496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1498" n="HIAT:w" s="T443">dʼaktar</ts>
                  <nts id="Seg_1499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1501" n="HIAT:w" s="T444">ölörünne</ts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1504" n="HIAT:w" s="T445">munu</ts>
                  <nts id="Seg_1505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1507" n="HIAT:w" s="T446">naːda</ts>
                  <nts id="Seg_1508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1510" n="HIAT:w" s="T447">du͡oktuːrda</ts>
                  <nts id="Seg_1511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1513" n="HIAT:w" s="T448">ɨgɨrɨnɨ͡akka</ts>
                  <nts id="Seg_1514" n="HIAT:ip">.</nts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T455" id="Seg_1517" n="HIAT:u" s="T449">
                  <ts e="T450" id="Seg_1519" n="HIAT:w" s="T449">Munu</ts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1522" n="HIAT:w" s="T450">teːtem</ts>
                  <nts id="Seg_1523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1525" n="HIAT:w" s="T451">maːmam</ts>
                  <nts id="Seg_1526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1528" n="HIAT:w" s="T452">diːr</ts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1530" n="HIAT:ip">"</nts>
                  <ts e="T454" id="Seg_1532" n="HIAT:w" s="T453">barɨma</ts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1535" n="HIAT:w" s="T454">barɨma</ts>
                  <nts id="Seg_1536" n="HIAT:ip">"</nts>
                  <nts id="Seg_1537" n="HIAT:ip">.</nts>
                  <nts id="Seg_1538" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T467" id="Seg_1540" n="HIAT:u" s="T455">
                  <ts e="T456" id="Seg_1542" n="HIAT:w" s="T455">Du͡oktuːru</ts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1545" n="HIAT:w" s="T456">tugun</ts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1548" n="HIAT:w" s="T457">diːr</ts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1551" n="HIAT:w" s="T458">anʼiː</ts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1554" n="HIAT:w" s="T459">dagɨnɨ</ts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1557" n="HIAT:w" s="T460">üčügej</ts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1560" n="HIAT:w" s="T461">köksüte</ts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1563" n="HIAT:w" s="T462">ki͡eŋ</ts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463.tx.1" id="Seg_1566" n="HIAT:w" s="T463">vrode</ts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1569" n="HIAT:w" s="T463.tx.1">bɨ</ts>
                  <nts id="Seg_1570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1572" n="HIAT:w" s="T465">üčügej</ts>
                  <nts id="Seg_1573" n="HIAT:ip">,</nts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1576" n="HIAT:w" s="T466">beːbe</ts>
                  <nts id="Seg_1577" n="HIAT:ip">.</nts>
                  <nts id="Seg_1578" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T477" id="Seg_1580" n="HIAT:u" s="T467">
                  <ts e="T468" id="Seg_1582" n="HIAT:w" s="T467">Oččogo</ts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1585" n="HIAT:w" s="T468">minigin</ts>
                  <nts id="Seg_1586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1588" n="HIAT:w" s="T469">ikki</ts>
                  <nts id="Seg_1589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1591" n="HIAT:w" s="T470">di͡egitten</ts>
                  <nts id="Seg_1592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1594" n="HIAT:w" s="T471">kötögön</ts>
                  <nts id="Seg_1595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1597" n="HIAT:w" s="T472">baran</ts>
                  <nts id="Seg_1598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1600" n="HIAT:w" s="T473">di͡e</ts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1603" n="HIAT:w" s="T474">bolokko</ts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1606" n="HIAT:w" s="T475">kiːllerdiler</ts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1609" n="HIAT:w" s="T476">bu͡o</ts>
                  <nts id="Seg_1610" n="HIAT:ip">.</nts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T484" id="Seg_1613" n="HIAT:u" s="T477">
                  <ts e="T478" id="Seg_1615" n="HIAT:w" s="T477">Eee</ts>
                  <nts id="Seg_1616" n="HIAT:ip">,</nts>
                  <nts id="Seg_1617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1619" n="HIAT:w" s="T478">de</ts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1622" n="HIAT:w" s="T479">ol</ts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1625" n="HIAT:w" s="T480">hɨgɨnnʼaktanan</ts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1628" n="HIAT:w" s="T481">kanʼan</ts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1631" n="HIAT:w" s="T482">baran</ts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1634" n="HIAT:w" s="T483">de</ts>
                  <nts id="Seg_1635" n="HIAT:ip">.</nts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T489" id="Seg_1638" n="HIAT:u" s="T484">
                  <ts e="T485" id="Seg_1640" n="HIAT:w" s="T484">Tu͡ok</ts>
                  <nts id="Seg_1641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1643" n="HIAT:w" s="T485">du͡oktuːrun</ts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1646" n="HIAT:w" s="T486">ittetin</ts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1649" n="HIAT:w" s="T487">beːbe</ts>
                  <nts id="Seg_1650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1652" n="HIAT:w" s="T488">üčügej</ts>
                  <nts id="Seg_1653" n="HIAT:ip">.</nts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T508" id="Seg_1656" n="HIAT:u" s="T489">
                  <ts e="T490" id="Seg_1658" n="HIAT:w" s="T489">Možet</ts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1661" n="HIAT:w" s="T490">i</ts>
                  <nts id="Seg_1662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1664" n="HIAT:w" s="T491">meːne</ts>
                  <nts id="Seg_1665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1667" n="HIAT:w" s="T492">itte</ts>
                  <nts id="Seg_1668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1670" n="HIAT:w" s="T493">eni</ts>
                  <nts id="Seg_1671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1673" n="HIAT:w" s="T494">tu͡oga</ts>
                  <nts id="Seg_1674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1676" n="HIAT:w" s="T495">da</ts>
                  <nts id="Seg_1677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1679" n="HIAT:w" s="T496">vrode</ts>
                  <nts id="Seg_1680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1682" n="HIAT:w" s="T497">tostubatak</ts>
                  <nts id="Seg_1683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1685" n="HIAT:w" s="T498">kaːn</ts>
                  <nts id="Seg_1686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1688" n="HIAT:w" s="T499">da</ts>
                  <nts id="Seg_1689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1691" n="HIAT:w" s="T500">hu͡ok</ts>
                  <nts id="Seg_1692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1694" n="HIAT:w" s="T501">tu͡ok</ts>
                  <nts id="Seg_1695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1697" n="HIAT:w" s="T502">da</ts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1700" n="HIAT:w" s="T503">hu͡ok</ts>
                  <nts id="Seg_1701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1703" n="HIAT:w" s="T504">üčügej</ts>
                  <nts id="Seg_1704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1706" n="HIAT:w" s="T505">bu͡olu͡o</ts>
                  <nts id="Seg_1707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1709" n="HIAT:w" s="T506">oččogo</ts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1712" n="HIAT:w" s="T507">heee</ts>
                  <nts id="Seg_1713" n="HIAT:ip">.</nts>
                  <nts id="Seg_1714" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T544" id="Seg_1716" n="HIAT:u" s="T508">
                  <ts e="T509" id="Seg_1718" n="HIAT:w" s="T508">De</ts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1721" n="HIAT:w" s="T509">vot</ts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1724" n="HIAT:w" s="T510">onton</ts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1727" n="HIAT:w" s="T511">ol</ts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1730" n="HIAT:w" s="T512">de</ts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1733" n="HIAT:w" s="T513">onton</ts>
                  <nts id="Seg_1734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1736" n="HIAT:w" s="T514">onton</ts>
                  <nts id="Seg_1737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1739" n="HIAT:w" s="T515">kojutaːn</ts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1742" n="HIAT:w" s="T516">kojut</ts>
                  <nts id="Seg_1743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1745" n="HIAT:w" s="T517">bagajɨ</ts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1748" n="HIAT:w" s="T518">de</ts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1751" n="HIAT:w" s="T519">ɨj</ts>
                  <nts id="Seg_1752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1754" n="HIAT:w" s="T520">aːsta</ts>
                  <nts id="Seg_1755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1757" n="HIAT:w" s="T521">bu͡o</ts>
                  <nts id="Seg_1758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1760" n="HIAT:w" s="T522">ɨj</ts>
                  <nts id="Seg_1761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1763" n="HIAT:w" s="T523">aːhan</ts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1766" n="HIAT:w" s="T524">baran</ts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1769" n="HIAT:w" s="T525">honon</ts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1772" n="HIAT:w" s="T526">du͡oktuːr</ts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1775" n="HIAT:w" s="T527">da</ts>
                  <nts id="Seg_1776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1778" n="HIAT:w" s="T528">hu͡ok</ts>
                  <nts id="Seg_1779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1781" n="HIAT:w" s="T529">tu͡ok</ts>
                  <nts id="Seg_1782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1784" n="HIAT:w" s="T530">da</ts>
                  <nts id="Seg_1785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1787" n="HIAT:w" s="T531">hu͡ok</ts>
                  <nts id="Seg_1788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1790" n="HIAT:w" s="T532">ɨj</ts>
                  <nts id="Seg_1791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1793" n="HIAT:w" s="T533">aːhan</ts>
                  <nts id="Seg_1794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1796" n="HIAT:w" s="T534">baran</ts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1799" n="HIAT:w" s="T535">de</ts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1802" n="HIAT:w" s="T536">ol</ts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1805" n="HIAT:w" s="T537">haŋa</ts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1808" n="HIAT:w" s="T538">dʼɨlga</ts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1811" n="HIAT:w" s="T539">kiːreːri</ts>
                  <nts id="Seg_1812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1814" n="HIAT:w" s="T540">de</ts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1817" n="HIAT:w" s="T541">du͡oktuːrga</ts>
                  <nts id="Seg_1818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1820" n="HIAT:w" s="T542">kiːrdibit</ts>
                  <nts id="Seg_1821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_1823" n="HIAT:w" s="T543">bu͡o</ts>
                  <nts id="Seg_1824" n="HIAT:ip">.</nts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T547" id="Seg_1827" n="HIAT:u" s="T544">
                  <ts e="T545" id="Seg_1829" n="HIAT:w" s="T544">Onno</ts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1832" n="HIAT:w" s="T545">kiːremmit</ts>
                  <nts id="Seg_1833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_1835" n="HIAT:w" s="T546">ol</ts>
                  <nts id="Seg_1836" n="HIAT:ip">.</nts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T561" id="Seg_1839" n="HIAT:u" s="T547">
                  <ts e="T548" id="Seg_1841" n="HIAT:w" s="T547">Kaja</ts>
                  <nts id="Seg_1842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1844" n="HIAT:w" s="T548">du͡oktuːrum</ts>
                  <nts id="Seg_1845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_1847" n="HIAT:w" s="T549">diːr</ts>
                  <nts id="Seg_1848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_1850" n="HIAT:w" s="T550">bu͡o</ts>
                  <nts id="Seg_1851" n="HIAT:ip">,</nts>
                  <nts id="Seg_1852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_1854" n="HIAT:w" s="T551">kaja</ts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_1857" n="HIAT:w" s="T552">eni͡ene</ts>
                  <nts id="Seg_1858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_1860" n="HIAT:w" s="T553">öttügüŋ</ts>
                  <nts id="Seg_1861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_1863" n="HIAT:w" s="T554">öttügüŋ</ts>
                  <nts id="Seg_1864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_1866" n="HIAT:w" s="T555">tostubut</ts>
                  <nts id="Seg_1867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_1869" n="HIAT:w" s="T556">ebit</ts>
                  <nts id="Seg_1870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_1872" n="HIAT:w" s="T557">enigin</ts>
                  <nts id="Seg_1873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_1875" n="HIAT:w" s="T558">naːda</ts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_1878" n="HIAT:w" s="T559">Nosku͡oga</ts>
                  <nts id="Seg_1879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_1881" n="HIAT:w" s="T560">ɨːtɨ͡akka</ts>
                  <nts id="Seg_1882" n="HIAT:ip">.</nts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T571" id="Seg_1885" n="HIAT:u" s="T561">
                  <ts e="T562" id="Seg_1887" n="HIAT:w" s="T561">Vot</ts>
                  <nts id="Seg_1888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_1890" n="HIAT:w" s="T562">onton</ts>
                  <nts id="Seg_1891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_1893" n="HIAT:w" s="T563">Nosku͡oga</ts>
                  <nts id="Seg_1894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_1896" n="HIAT:w" s="T564">ɨːppɨttara</ts>
                  <nts id="Seg_1897" n="HIAT:ip">,</nts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_1900" n="HIAT:w" s="T565">Nosku͡o</ts>
                  <nts id="Seg_1901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_1903" n="HIAT:w" s="T566">min</ts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_1906" n="HIAT:w" s="T567">hɨttɨm</ts>
                  <nts id="Seg_1907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_1909" n="HIAT:w" s="T568">bu͡o</ts>
                  <nts id="Seg_1910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_1912" n="HIAT:w" s="T569">onno</ts>
                  <nts id="Seg_1913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_1915" n="HIAT:w" s="T570">de</ts>
                  <nts id="Seg_1916" n="HIAT:ip">.</nts>
                  <nts id="Seg_1917" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T576" id="Seg_1919" n="HIAT:u" s="T571">
                  <ts e="T572" id="Seg_1921" n="HIAT:w" s="T571">Ikki</ts>
                  <nts id="Seg_1922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_1924" n="HIAT:w" s="T572">ɨjɨ</ts>
                  <nts id="Seg_1925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_1927" n="HIAT:w" s="T573">vɨčiškaga</ts>
                  <nts id="Seg_1928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_1930" n="HIAT:w" s="T574">hɨttɨm</ts>
                  <nts id="Seg_1931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_1933" n="HIAT:w" s="T575">bu͡o</ts>
                  <nts id="Seg_1934" n="HIAT:ip">.</nts>
                  <nts id="Seg_1935" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T588" id="Seg_1937" n="HIAT:u" s="T576">
                  <ts e="T577" id="Seg_1939" n="HIAT:w" s="T576">Onton</ts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_1942" n="HIAT:w" s="T577">bu͡o</ts>
                  <nts id="Seg_1943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_1945" n="HIAT:w" s="T578">aŋar</ts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_1948" n="HIAT:w" s="T579">ɨjdarɨn</ts>
                  <nts id="Seg_1949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_1951" n="HIAT:w" s="T580">bu͡o</ts>
                  <nts id="Seg_1952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_1954" n="HIAT:w" s="T581">kör</ts>
                  <nts id="Seg_1955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_1957" n="HIAT:w" s="T582">agɨs</ts>
                  <nts id="Seg_1958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_1960" n="HIAT:w" s="T583">ɨjɨ</ts>
                  <nts id="Seg_1961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_1963" n="HIAT:w" s="T584">meldʼi</ts>
                  <nts id="Seg_1964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_1966" n="HIAT:w" s="T585">hɨttɨm</ts>
                  <nts id="Seg_1967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_1969" n="HIAT:w" s="T586">bu͡o</ts>
                  <nts id="Seg_1970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_1972" n="HIAT:w" s="T587">Nosku͡oŋa</ts>
                  <nts id="Seg_1973" n="HIAT:ip">.</nts>
                  <nts id="Seg_1974" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T590" id="Seg_1976" n="HIAT:u" s="T588">
                  <ts e="T588.tx.1" id="Seg_1978" n="HIAT:w" s="T588">Eto</ts>
                  <nts id="Seg_1979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588.tx.2" id="Seg_1981" n="HIAT:w" s="T588.tx.1">v</ts>
                  <nts id="Seg_1982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588.tx.3" id="Seg_1984" n="HIAT:w" s="T588.tx.2">ijun'e</ts>
                  <nts id="Seg_1985" n="HIAT:ip">,</nts>
                  <nts id="Seg_1986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588.tx.4" id="Seg_1988" n="HIAT:w" s="T588.tx.3">v</ts>
                  <nts id="Seg_1989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588.tx.5" id="Seg_1991" n="HIAT:w" s="T588.tx.4">ijun'e</ts>
                  <nts id="Seg_1992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588.tx.6" id="Seg_1994" n="HIAT:w" s="T588.tx.5">domoj</ts>
                  <nts id="Seg_1995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_1997" n="HIAT:w" s="T588.tx.6">priexala</ts>
                  <nts id="Seg_1998" n="HIAT:ip">,</nts>
                  <nts id="Seg_1999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2001" n="HIAT:w" s="T614">di͡eber</ts>
                  <nts id="Seg_2002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2004" n="HIAT:w" s="T589">kelbitim</ts>
                  <nts id="Seg_2005" n="HIAT:ip">.</nts>
                  <nts id="Seg_2006" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T602" id="Seg_2008" n="HIAT:u" s="T590">
                  <ts e="T591" id="Seg_2010" n="HIAT:w" s="T590">I</ts>
                  <nts id="Seg_2011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2013" n="HIAT:w" s="T591">atagɨm</ts>
                  <nts id="Seg_2014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2015" n="HIAT:ip">(</nts>
                  <ts e="T592.tx.1" id="Seg_2017" n="HIAT:w" s="T592">kak</ts>
                  <nts id="Seg_2018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592.tx.2" id="Seg_2020" n="HIAT:w" s="T592.tx.1">kak</ts>
                  <nts id="Seg_2021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592.tx.3" id="Seg_2023" n="HIAT:w" s="T592.tx.2">kak</ts>
                  <nts id="Seg_2024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592.tx.4" id="Seg_2026" n="HIAT:w" s="T592.tx.3">=</ts>
                  <nts id="Seg_2027" n="HIAT:ip">)</nts>
                  <nts id="Seg_2028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592.tx.5" id="Seg_2030" n="HIAT:w" s="T592.tx.4">kak</ts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2033" n="HIAT:w" s="T592.tx.5">perelomannyj</ts>
                  <nts id="Seg_2034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2036" n="HIAT:w" s="T597">ete</ts>
                  <nts id="Seg_2037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2039" n="HIAT:w" s="T598">ol</ts>
                  <nts id="Seg_2040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2042" n="HIAT:w" s="T599">kördük</ts>
                  <nts id="Seg_2043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2045" n="HIAT:w" s="T600">honon</ts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2048" n="HIAT:w" s="T601">osput</ts>
                  <nts id="Seg_2049" n="HIAT:ip">.</nts>
                  <nts id="Seg_2050" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T606" id="Seg_2052" n="HIAT:u" s="T602">
                  <ts e="T612" id="Seg_2054" n="HIAT:w" s="T602">Vot</ts>
                  <nts id="Seg_2055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2057" n="HIAT:w" s="T612">takaja</ts>
                  <nts id="Seg_2058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2060" n="HIAT:w" s="T603">ja</ts>
                  <nts id="Seg_2061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2063" n="HIAT:w" s="T604">pri͡ehala</ts>
                  <nts id="Seg_2064" n="HIAT:ip">.</nts>
                  <nts id="Seg_2065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T606" id="Seg_2066" n="sc" s="T0">
               <ts e="T1" id="Seg_2068" n="e" s="T0">Ogo </ts>
               <ts e="T2" id="Seg_2070" n="e" s="T1">erdekpinen </ts>
               <ts e="T3" id="Seg_2072" n="e" s="T2">bu͡ollagɨnan </ts>
               <ts e="T4" id="Seg_2074" n="e" s="T3">bu͡o </ts>
               <ts e="T5" id="Seg_2076" n="e" s="T4">ogolorum </ts>
               <ts e="T6" id="Seg_2078" n="e" s="T5">kɨra </ts>
               <ts e="T7" id="Seg_2080" n="e" s="T6">etilere. </ts>
               <ts e="T8" id="Seg_2082" n="e" s="T7">Biːr </ts>
               <ts e="T9" id="Seg_2084" n="e" s="T8">ogom </ts>
               <ts e="T10" id="Seg_2086" n="e" s="T9">dʼɨllaːk </ts>
               <ts e="T11" id="Seg_2088" n="e" s="T10">ete. </ts>
               <ts e="T12" id="Seg_2090" n="e" s="T11">Dʼɨltan </ts>
               <ts e="T13" id="Seg_2092" n="e" s="T12">orduktaːk </ts>
               <ts e="T14" id="Seg_2094" n="e" s="T13">god </ts>
               <ts e="T15" id="Seg_2096" n="e" s="T14">i </ts>
               <ts e="T16" id="Seg_2098" n="e" s="T15">sem. </ts>
               <ts e="T17" id="Seg_2100" n="e" s="T16">Biːr </ts>
               <ts e="T18" id="Seg_2102" n="e" s="T17">ogom </ts>
               <ts e="T19" id="Seg_2104" n="e" s="T18">bu͡ollagɨna </ts>
               <ts e="T20" id="Seg_2106" n="e" s="T19">agɨs </ts>
               <ts e="T21" id="Seg_2108" n="e" s="T20">ɨjdaːk </ts>
               <ts e="T22" id="Seg_2110" n="e" s="T21">ete. </ts>
               <ts e="T23" id="Seg_2112" n="e" s="T22">Oččogo </ts>
               <ts e="T24" id="Seg_2114" n="e" s="T23">bu͡ollagɨnan </ts>
               <ts e="T25" id="Seg_2116" n="e" s="T24">bu͡o </ts>
               <ts e="T26" id="Seg_2118" n="e" s="T25">teːtem </ts>
               <ts e="T27" id="Seg_2120" n="e" s="T26">mas </ts>
               <ts e="T28" id="Seg_2122" n="e" s="T27">egele </ts>
               <ts e="T29" id="Seg_2124" n="e" s="T28">barammɨn </ts>
               <ts e="T30" id="Seg_2126" n="e" s="T29">teːtem </ts>
               <ts e="T31" id="Seg_2128" n="e" s="T30">gɨtta </ts>
               <ts e="T32" id="Seg_2130" n="e" s="T31">barsɨbɨt </ts>
               <ts e="T33" id="Seg_2132" n="e" s="T32">etim </ts>
               <ts e="T34" id="Seg_2134" n="e" s="T33">hetiːleːkpin. </ts>
               <ts e="T35" id="Seg_2136" n="e" s="T34">Hetiːleːppin </ts>
               <ts e="T36" id="Seg_2138" n="e" s="T35">hetiːber </ts>
               <ts e="T37" id="Seg_2140" n="e" s="T36">bu͡ollagɨna </ts>
               <ts e="T38" id="Seg_2142" n="e" s="T37">baltɨm </ts>
               <ts e="T39" id="Seg_2144" n="e" s="T38">oloror </ts>
               <ts e="T40" id="Seg_2146" n="e" s="T39">ete </ts>
               <ts e="T41" id="Seg_2148" n="e" s="T40">bi͡es </ts>
               <ts e="T42" id="Seg_2150" n="e" s="T41">dʼɨllaːk </ts>
               <ts e="T43" id="Seg_2152" n="e" s="T42">baltɨm. </ts>
               <ts e="T47" id="Seg_2154" n="e" s="T43">Ješʼo ja vam ne rasskazala že, u nas oleni že raznɨe, </ts>
               <ts e="T48" id="Seg_2156" n="e" s="T47">oččogo </ts>
               <ts e="T49" id="Seg_2158" n="e" s="T48">že </ts>
               <ts e="T50" id="Seg_2160" n="e" s="T49">dʼaktar </ts>
               <ts e="T51" id="Seg_2162" n="e" s="T50">bu͡ollagɨna </ts>
               <ts e="T52" id="Seg_2164" n="e" s="T51">bu͡o </ts>
               <ts e="T53" id="Seg_2166" n="e" s="T52">bugdi </ts>
               <ts e="T54" id="Seg_2168" n="e" s="T53">tabanɨ </ts>
               <ts e="T55" id="Seg_2170" n="e" s="T54">ne </ts>
               <ts e="T56" id="Seg_2172" n="e" s="T55">dolžna </ts>
               <ts e="T57" id="Seg_2174" n="e" s="T56">ona </ts>
               <ts e="T58" id="Seg_2176" n="e" s="T57">kimni͡egin </ts>
               <ts e="T59" id="Seg_2178" n="e" s="T58">ke </ts>
               <ts e="T60" id="Seg_2180" n="e" s="T59">kölüjü͡ögün </ts>
               <ts e="T62" id="Seg_2182" n="e" s="T60">ke. </ts>
               <ts e="T63" id="Seg_2184" n="e" s="T62">I </ts>
               <ts e="T64" id="Seg_2186" n="e" s="T63">oččogo </ts>
               <ts e="T65" id="Seg_2188" n="e" s="T64">bu͡ollagɨna </ts>
               <ts e="T66" id="Seg_2190" n="e" s="T65">bu͡o </ts>
               <ts e="T67" id="Seg_2192" n="e" s="T66">tɨnnastɨbɨt </ts>
               <ts e="T68" id="Seg_2194" n="e" s="T67">taba </ts>
               <ts e="T69" id="Seg_2196" n="e" s="T68">potom </ts>
               <ts e="T70" id="Seg_2198" n="e" s="T69">ja </ts>
               <ts e="T71" id="Seg_2200" n="e" s="T70">rasskazhu </ts>
               <ts e="T72" id="Seg_2202" n="e" s="T71">da, </ts>
               <ts e="T73" id="Seg_2204" n="e" s="T72">tɨnnastɨbɨt </ts>
               <ts e="T74" id="Seg_2206" n="e" s="T73">taba. </ts>
               <ts e="T75" id="Seg_2208" n="e" s="T74">Eto </ts>
               <ts e="T76" id="Seg_2210" n="e" s="T75">tɨnnastɨbɨt </ts>
               <ts e="T77" id="Seg_2212" n="e" s="T76">taba </ts>
               <ts e="T78" id="Seg_2214" n="e" s="T77">bu͡ollagɨnan </ts>
               <ts e="T79" id="Seg_2216" n="e" s="T78">bu͡o </ts>
               <ts e="T80" id="Seg_2218" n="e" s="T79">eh </ts>
               <ts e="T81" id="Seg_2220" n="e" s="T80">tɨnnastɨbɨt </ts>
               <ts e="T82" id="Seg_2222" n="e" s="T81">taba </ts>
               <ts e="T83" id="Seg_2224" n="e" s="T82">ɨ͡aldʼarɨgar </ts>
               <ts e="T84" id="Seg_2226" n="e" s="T83">bu͡o </ts>
               <ts e="T85" id="Seg_2228" n="e" s="T84">erim </ts>
               <ts e="T86" id="Seg_2230" n="e" s="T85">inʼete </ts>
               <ts e="T87" id="Seg_2232" n="e" s="T86">tɨnnastaːččɨ </ts>
               <ts e="T88" id="Seg_2234" n="e" s="T87">ete </ts>
               <ts e="T89" id="Seg_2236" n="e" s="T88">menʼiːtiger </ts>
               <ts e="T90" id="Seg_2238" n="e" s="T89">tabanɨ </ts>
               <ts e="T91" id="Seg_2240" n="e" s="T90">ke. </ts>
               <ts e="T92" id="Seg_2242" n="e" s="T91">Ol </ts>
               <ts e="T93" id="Seg_2244" n="e" s="T92">tabanɨ </ts>
               <ts e="T94" id="Seg_2246" n="e" s="T93">bu͡olla </ts>
               <ts e="T95" id="Seg_2248" n="e" s="T94">dʼaktar </ts>
               <ts e="T96" id="Seg_2250" n="e" s="T95">kölüjü͡ögün </ts>
               <ts e="T97" id="Seg_2252" n="e" s="T96">nelzja </ts>
               <ts e="T98" id="Seg_2254" n="e" s="T97">ete </ts>
               <ts e="T99" id="Seg_2256" n="e" s="T98">bu͡o. </ts>
               <ts e="T100" id="Seg_2258" n="e" s="T99">Oččogo </ts>
               <ts e="T101" id="Seg_2260" n="e" s="T100">min </ts>
               <ts e="T102" id="Seg_2262" n="e" s="T101">bilbekkebin </ts>
               <ts e="T103" id="Seg_2264" n="e" s="T102">bu </ts>
               <ts e="T104" id="Seg_2266" n="e" s="T103">tabanɨ </ts>
               <ts e="T105" id="Seg_2268" n="e" s="T104">kostuːrdaːmmɨppɨn </ts>
               <ts e="T106" id="Seg_2270" n="e" s="T105">bu͡o. </ts>
               <ts e="T107" id="Seg_2272" n="e" s="T106">Ontuŋ </ts>
               <ts e="T108" id="Seg_2274" n="e" s="T107">čeːlke </ts>
               <ts e="T109" id="Seg_2276" n="e" s="T108">taba </ts>
               <ts e="T110" id="Seg_2278" n="e" s="T109">buruna </ts>
               <ts e="T111" id="Seg_2280" n="e" s="T110">taba. </ts>
               <ts e="T112" id="Seg_2282" n="e" s="T111">Inʼe </ts>
               <ts e="T113" id="Seg_2284" n="e" s="T112">bu͡ollagɨna </ts>
               <ts e="T114" id="Seg_2286" n="e" s="T113">baraːrɨbɨt </ts>
               <ts e="T115" id="Seg_2288" n="e" s="T114">bu͡ollagɨna </ts>
               <ts e="T116" id="Seg_2290" n="e" s="T115">ol </ts>
               <ts e="T117" id="Seg_2292" n="e" s="T116">taba </ts>
               <ts e="T118" id="Seg_2294" n="e" s="T117">bu͡o </ts>
               <ts e="T119" id="Seg_2296" n="e" s="T118">ɨtɨrdar. </ts>
               <ts e="T120" id="Seg_2298" n="e" s="T119">Üste </ts>
               <ts e="T121" id="Seg_2300" n="e" s="T120">raz </ts>
               <ts e="T122" id="Seg_2302" n="e" s="T121">ɨtɨrdar </ts>
               <ts e="T123" id="Seg_2304" n="e" s="T122">bu͡o </ts>
               <ts e="T124" id="Seg_2306" n="e" s="T123">oččogo </ts>
               <ts e="T125" id="Seg_2308" n="e" s="T124">min </ts>
               <ts e="T126" id="Seg_2310" n="e" s="T125">diːbin </ts>
               <ts e="T127" id="Seg_2312" n="e" s="T126">bu͡o </ts>
               <ts e="T128" id="Seg_2314" n="e" s="T127">araː </ts>
               <ts e="T129" id="Seg_2316" n="e" s="T128">tu͡okka </ts>
               <ts e="T130" id="Seg_2318" n="e" s="T129">ɨtɨrdar </ts>
               <ts e="T131" id="Seg_2320" n="e" s="T130">ke </ts>
               <ts e="T132" id="Seg_2322" n="e" s="T131">di͡ebin. </ts>
               <ts e="T608" id="Seg_2324" n="e" s="T132">A </ts>
               <ts e="T133" id="Seg_2326" n="e" s="T608">do </ts>
               <ts e="T134" id="Seg_2328" n="e" s="T133">etogo </ts>
               <ts e="T135" id="Seg_2330" n="e" s="T134">(iti </ts>
               <ts e="T136" id="Seg_2332" n="e" s="T135">innitinen) </ts>
               <ts e="T137" id="Seg_2334" n="e" s="T136">kaːmnaːrɨbɨt </ts>
               <ts e="T138" id="Seg_2336" n="e" s="T137">maːmam </ts>
               <ts e="T139" id="Seg_2338" n="e" s="T138">diːr </ts>
               <ts e="T140" id="Seg_2340" n="e" s="T139">bu͡o </ts>
               <ts e="T141" id="Seg_2342" n="e" s="T140">u͡opput </ts>
               <ts e="T142" id="Seg_2344" n="e" s="T141">haŋarar </ts>
               <ts e="T143" id="Seg_2346" n="e" s="T142">diːr </ts>
               <ts e="T144" id="Seg_2348" n="e" s="T143">u͡opput </ts>
               <ts e="T145" id="Seg_2350" n="e" s="T144">haŋarar </ts>
               <ts e="T146" id="Seg_2352" n="e" s="T145">eto </ts>
               <ts e="T147" id="Seg_2354" n="e" s="T146">značit </ts>
               <ts e="T148" id="Seg_2356" n="e" s="T147">bu͡ollagɨna </ts>
               <ts e="T149" id="Seg_2358" n="e" s="T148">tu͡ok </ts>
               <ts e="T150" id="Seg_2360" n="e" s="T149">ere </ts>
               <ts e="T151" id="Seg_2362" n="e" s="T150">kuhagan </ts>
               <ts e="T152" id="Seg_2364" n="e" s="T151">bu͡olaːččɨ </ts>
               <ts e="T153" id="Seg_2366" n="e" s="T152">u͡ot </ts>
               <ts e="T154" id="Seg_2368" n="e" s="T153">haŋardagɨna </ts>
               <ts e="T155" id="Seg_2370" n="e" s="T154">ke </ts>
               <ts e="T157" id="Seg_2372" n="e" s="T155">haŋardagɨna. </ts>
               <ts e="T158" id="Seg_2374" n="e" s="T157">Oččogo </ts>
               <ts e="T159" id="Seg_2376" n="e" s="T158">bu͡ollagɨna </ts>
               <ts e="T160" id="Seg_2378" n="e" s="T159">nu </ts>
               <ts e="T164" id="Seg_2380" n="e" s="T160">(ja ne s-) </ts>
               <ts e="T165" id="Seg_2382" n="e" s="T164">min </ts>
               <ts e="T166" id="Seg_2384" n="e" s="T165">bu͡ollagɨna </ts>
               <ts e="T167" id="Seg_2386" n="e" s="T166">olus </ts>
               <ts e="T168" id="Seg_2388" n="e" s="T167">daːgɨnɨ </ts>
               <ts e="T169" id="Seg_2390" n="e" s="T168">onu </ts>
               <ts e="T170" id="Seg_2392" n="e" s="T169">bu͡ollagɨnan </ts>
               <ts e="T171" id="Seg_2394" n="e" s="T170">itegejbekkebin </ts>
               <ts e="T172" id="Seg_2396" n="e" s="T171">bu͡ollagɨna. </ts>
               <ts e="T173" id="Seg_2398" n="e" s="T172">De </ts>
               <ts e="T174" id="Seg_2400" n="e" s="T173">taba </ts>
               <ts e="T175" id="Seg_2402" n="e" s="T174">tutunnubut </ts>
               <ts e="T176" id="Seg_2404" n="e" s="T175">da </ts>
               <ts e="T177" id="Seg_2406" n="e" s="T176">baran </ts>
               <ts e="T178" id="Seg_2408" n="e" s="T177">kaːllɨbɨt </ts>
               <ts e="T179" id="Seg_2410" n="e" s="T178">bu͡o </ts>
               <ts e="T180" id="Seg_2412" n="e" s="T179">ol </ts>
               <ts e="T181" id="Seg_2414" n="e" s="T180">bu </ts>
               <ts e="T182" id="Seg_2416" n="e" s="T181">tabanɨ. </ts>
               <ts e="T183" id="Seg_2418" n="e" s="T182">Kappɨppɨt </ts>
               <ts e="T184" id="Seg_2420" n="e" s="T183">innʼe </ts>
               <ts e="T185" id="Seg_2422" n="e" s="T184">baran </ts>
               <ts e="T186" id="Seg_2424" n="e" s="T185">kaːlbɨppɨt </ts>
               <ts e="T187" id="Seg_2426" n="e" s="T186">bu͡o </ts>
               <ts e="T188" id="Seg_2428" n="e" s="T187">teːtebin </ts>
               <ts e="T189" id="Seg_2430" n="e" s="T188">gɨtta </ts>
               <ts e="T190" id="Seg_2432" n="e" s="T189">barsan </ts>
               <ts e="T191" id="Seg_2434" n="e" s="T190">kaːlbɨppɨt </ts>
               <ts e="T192" id="Seg_2436" n="e" s="T191">barsan </ts>
               <ts e="T193" id="Seg_2438" n="e" s="T192">(kaːl-). </ts>
               <ts e="T194" id="Seg_2440" n="e" s="T193">Huːrka </ts>
               <ts e="T195" id="Seg_2442" n="e" s="T194">kelen </ts>
               <ts e="T196" id="Seg_2444" n="e" s="T195">baran </ts>
               <ts e="T197" id="Seg_2446" n="e" s="T196">bu͡ollagɨnan </ts>
               <ts e="T198" id="Seg_2448" n="e" s="T197">teːtem </ts>
               <ts e="T199" id="Seg_2450" n="e" s="T198">hɨrgabɨtɨn </ts>
               <ts e="T200" id="Seg_2452" n="e" s="T199">hetiːbitin </ts>
               <ts e="T201" id="Seg_2454" n="e" s="T200">bu͡ollagɨnan </ts>
               <ts e="T202" id="Seg_2456" n="e" s="T201">ti͡ennibit </ts>
               <ts e="T203" id="Seg_2458" n="e" s="T202">ti͡enen </ts>
               <ts e="T204" id="Seg_2460" n="e" s="T203">ötüleːnen </ts>
               <ts e="T205" id="Seg_2462" n="e" s="T204">kanʼan </ts>
               <ts e="T206" id="Seg_2464" n="e" s="T205">bu͡o. </ts>
               <ts e="T207" id="Seg_2466" n="e" s="T206">Minigin </ts>
               <ts e="T208" id="Seg_2468" n="e" s="T207">de </ts>
               <ts e="T209" id="Seg_2470" n="e" s="T208">kamnaːtta </ts>
               <ts e="T210" id="Seg_2472" n="e" s="T209">bu͡o </ts>
               <ts e="T211" id="Seg_2474" n="e" s="T210">hɨrgalaːk </ts>
               <ts e="T212" id="Seg_2476" n="e" s="T211">tababɨn </ts>
               <ts e="T213" id="Seg_2478" n="e" s="T212">hi͡eten </ts>
               <ts e="T214" id="Seg_2480" n="e" s="T213">baran. </ts>
               <ts e="T215" id="Seg_2482" n="e" s="T214">Hɨrgalaːk </ts>
               <ts e="T216" id="Seg_2484" n="e" s="T215">tababɨn </ts>
               <ts e="T217" id="Seg_2486" n="e" s="T216">hi͡eten </ts>
               <ts e="T218" id="Seg_2488" n="e" s="T217">barammɨn </ts>
               <ts e="T219" id="Seg_2490" n="e" s="T218">bu͡o </ts>
               <ts e="T220" id="Seg_2492" n="e" s="T219">hɨːha </ts>
               <ts e="T221" id="Seg_2494" n="e" s="T220">olorobun </ts>
               <ts e="T222" id="Seg_2496" n="e" s="T221">bu͡o </ts>
               <ts e="T223" id="Seg_2498" n="e" s="T222">hɨrgabar </ts>
               <ts e="T224" id="Seg_2500" n="e" s="T223">bu͡o </ts>
               <ts e="T225" id="Seg_2502" n="e" s="T224">balɨktarɨ </ts>
               <ts e="T226" id="Seg_2504" n="e" s="T225">ti͡ejbippin </ts>
               <ts e="T227" id="Seg_2506" n="e" s="T226">alta </ts>
               <ts e="T228" id="Seg_2508" n="e" s="T227">kul </ts>
               <ts e="T229" id="Seg_2510" n="e" s="T228">balɨk. </ts>
               <ts e="T230" id="Seg_2512" n="e" s="T229">Alta </ts>
               <ts e="T231" id="Seg_2514" n="e" s="T230">kul </ts>
               <ts e="T232" id="Seg_2516" n="e" s="T231">balɨk </ts>
               <ts e="T233" id="Seg_2518" n="e" s="T232">ti͡ejen </ts>
               <ts e="T234" id="Seg_2520" n="e" s="T233">baran </ts>
               <ts e="T235" id="Seg_2522" n="e" s="T234">bu͡ollana. </ts>
               <ts e="T236" id="Seg_2524" n="e" s="T235">Hɨrgalar </ts>
               <ts e="T237" id="Seg_2526" n="e" s="T236">ɨstanabin </ts>
               <ts e="T238" id="Seg_2528" n="e" s="T237">diːbin </ts>
               <ts e="T239" id="Seg_2530" n="e" s="T238">bu͡o </ts>
               <ts e="T240" id="Seg_2532" n="e" s="T239">hɨrgabɨn </ts>
               <ts e="T241" id="Seg_2534" n="e" s="T240">hɨːha </ts>
               <ts e="T242" id="Seg_2536" n="e" s="T241">olorobun </ts>
               <ts e="T243" id="Seg_2538" n="e" s="T242">bu͡o. </ts>
               <ts e="T244" id="Seg_2540" n="e" s="T243">Hu͡ok </ts>
               <ts e="T245" id="Seg_2542" n="e" s="T244">bu </ts>
               <ts e="T246" id="Seg_2544" n="e" s="T245">kihi </ts>
               <ts e="T247" id="Seg_2546" n="e" s="T246">bu͡o </ts>
               <ts e="T248" id="Seg_2548" n="e" s="T247">ješo </ts>
               <ts e="T249" id="Seg_2550" n="e" s="T248">da </ts>
               <ts e="T250" id="Seg_2552" n="e" s="T249">ɨstanabɨn </ts>
               <ts e="T251" id="Seg_2554" n="e" s="T250">bu͡o. </ts>
               <ts e="T609" id="Seg_2556" n="e" s="T251">Ješo </ts>
               <ts e="T252" id="Seg_2558" n="e" s="T609">raz </ts>
               <ts e="T253" id="Seg_2560" n="e" s="T252">ɨstanabɨn </ts>
               <ts e="T255" id="Seg_2562" n="e" s="T253">bu͡o. </ts>
               <ts e="T256" id="Seg_2564" n="e" s="T255">Emi͡e </ts>
               <ts e="T257" id="Seg_2566" n="e" s="T256">hɨːha </ts>
               <ts e="T258" id="Seg_2568" n="e" s="T257">olorobun </ts>
               <ts e="T259" id="Seg_2570" n="e" s="T258">bu͡o. </ts>
               <ts e="T260" id="Seg_2572" n="e" s="T259">Hɨrgalaːk </ts>
               <ts e="T261" id="Seg_2574" n="e" s="T260">tababɨn </ts>
               <ts e="T262" id="Seg_2576" n="e" s="T261">ɨhɨktan </ts>
               <ts e="T263" id="Seg_2578" n="e" s="T262">keːstim </ts>
               <ts e="T264" id="Seg_2580" n="e" s="T263">bu͡o. </ts>
               <ts e="T265" id="Seg_2582" n="e" s="T264">Innʼe </ts>
               <ts e="T266" id="Seg_2584" n="e" s="T265">hɨrgalaːk </ts>
               <ts e="T267" id="Seg_2586" n="e" s="T266">tabam </ts>
               <ts e="T268" id="Seg_2588" n="e" s="T267">bu͡ollagɨna </ts>
               <ts e="T269" id="Seg_2590" n="e" s="T268">hetiːm </ts>
               <ts e="T270" id="Seg_2592" n="e" s="T269">tabalara </ts>
               <ts e="T271" id="Seg_2594" n="e" s="T270">ürdübünen </ts>
               <ts e="T272" id="Seg_2596" n="e" s="T271">barallar </ts>
               <ts e="T273" id="Seg_2598" n="e" s="T272">bu͡o. </ts>
               <ts e="T274" id="Seg_2600" n="e" s="T273">I </ts>
               <ts e="T275" id="Seg_2602" n="e" s="T274">bejem </ts>
               <ts e="T276" id="Seg_2604" n="e" s="T275">bu͡ollagɨnan </ts>
               <ts e="T277" id="Seg_2606" n="e" s="T276">bu͡o </ts>
               <ts e="T278" id="Seg_2608" n="e" s="T277">hetiː </ts>
               <ts e="T279" id="Seg_2610" n="e" s="T278">ɨlagɨn </ts>
               <ts e="T280" id="Seg_2612" n="e" s="T279">aːjɨnan </ts>
               <ts e="T281" id="Seg_2614" n="e" s="T280">üste </ts>
               <ts e="T282" id="Seg_2616" n="e" s="T281">raz </ts>
               <ts e="T283" id="Seg_2618" n="e" s="T282">ergijdim </ts>
               <ts e="T284" id="Seg_2620" n="e" s="T283">bu͡o. </ts>
               <ts e="T285" id="Seg_2622" n="e" s="T284">Araː </ts>
               <ts e="T286" id="Seg_2624" n="e" s="T285">innʼe </ts>
               <ts e="T287" id="Seg_2626" n="e" s="T286">bu͡o </ts>
               <ts e="T288" id="Seg_2628" n="e" s="T287">ittenne </ts>
               <ts e="T289" id="Seg_2630" n="e" s="T288">tühen </ts>
               <ts e="T290" id="Seg_2632" n="e" s="T289">(kaːl-). </ts>
               <ts e="T291" id="Seg_2634" n="e" s="T290">Ittenne </ts>
               <ts e="T292" id="Seg_2636" n="e" s="T291">tühen </ts>
               <ts e="T293" id="Seg_2638" n="e" s="T292">baran </ts>
               <ts e="T294" id="Seg_2640" n="e" s="T293">bu͡o </ts>
               <ts e="T295" id="Seg_2642" n="e" s="T294">hɨtabɨn </ts>
               <ts e="T296" id="Seg_2644" n="e" s="T295">bu͡o </ts>
               <ts e="T297" id="Seg_2646" n="e" s="T296">teːtem </ts>
               <ts e="T298" id="Seg_2648" n="e" s="T297">hokuj </ts>
               <ts e="T299" id="Seg_2650" n="e" s="T298">kette </ts>
               <ts e="T300" id="Seg_2652" n="e" s="T299">turar </ts>
               <ts e="T301" id="Seg_2654" n="e" s="T300">ete </ts>
               <ts e="T302" id="Seg_2656" n="e" s="T301">bu͡o. </ts>
               <ts e="T303" id="Seg_2658" n="e" s="T302">Hokuj </ts>
               <ts e="T304" id="Seg_2660" n="e" s="T303">keten </ts>
               <ts e="T305" id="Seg_2662" n="e" s="T304">baran </ts>
               <ts e="T306" id="Seg_2664" n="e" s="T305">bukaːn </ts>
               <ts e="T307" id="Seg_2666" n="e" s="T306">körbüte. </ts>
               <ts e="T308" id="Seg_2668" n="e" s="T307">Körör </ts>
               <ts e="T309" id="Seg_2670" n="e" s="T308">hɨrgalaːk </ts>
               <ts e="T310" id="Seg_2672" n="e" s="T309">taba </ts>
               <ts e="T311" id="Seg_2674" n="e" s="T310">bara </ts>
               <ts e="T312" id="Seg_2676" n="e" s="T311">turar </ts>
               <ts e="T313" id="Seg_2678" n="e" s="T312">kihite </ts>
               <ts e="T314" id="Seg_2680" n="e" s="T313">bu͡o </ts>
               <ts e="T315" id="Seg_2682" n="e" s="T314">munna </ts>
               <ts e="T316" id="Seg_2684" n="e" s="T315">hɨtar </ts>
               <ts e="T317" id="Seg_2686" n="e" s="T316">hu͡olga. </ts>
               <ts e="T318" id="Seg_2688" n="e" s="T317">De </ts>
               <ts e="T319" id="Seg_2690" n="e" s="T318">hüːren </ts>
               <ts e="T320" id="Seg_2692" n="e" s="T319">kelle </ts>
               <ts e="T321" id="Seg_2694" n="e" s="T320">bu͡o </ts>
               <ts e="T322" id="Seg_2696" n="e" s="T321">innʼe </ts>
               <ts e="T323" id="Seg_2698" n="e" s="T322">di͡ete </ts>
               <ts e="T324" id="Seg_2700" n="e" s="T323">kaja </ts>
               <ts e="T325" id="Seg_2702" n="e" s="T324">en </ts>
               <ts e="T326" id="Seg_2704" n="e" s="T325">tɨːnnaːkkɨn </ts>
               <ts e="T327" id="Seg_2706" n="e" s="T326">du͡o </ts>
               <ts e="T328" id="Seg_2708" n="e" s="T327">di͡ete. </ts>
               <ts e="T329" id="Seg_2710" n="e" s="T328">Heee </ts>
               <ts e="T330" id="Seg_2712" n="e" s="T329">tɨːnnaːppɨn </ts>
               <ts e="T331" id="Seg_2714" n="e" s="T330">tɨːnnaːppɨn </ts>
               <ts e="T332" id="Seg_2716" n="e" s="T331">atagɨm </ts>
               <ts e="T333" id="Seg_2718" n="e" s="T332">ere </ts>
               <ts e="T334" id="Seg_2720" n="e" s="T333">kɨ͡ajan </ts>
               <ts e="T335" id="Seg_2722" n="e" s="T334">kaːmnaːbat. </ts>
               <ts e="T336" id="Seg_2724" n="e" s="T335">Eee </ts>
               <ts e="T337" id="Seg_2726" n="e" s="T336">de </ts>
               <ts e="T338" id="Seg_2728" n="e" s="T337">beːbe </ts>
               <ts e="T339" id="Seg_2730" n="e" s="T338">min </ts>
               <ts e="T340" id="Seg_2732" n="e" s="T339">keli͡em. </ts>
               <ts e="T341" id="Seg_2734" n="e" s="T340">De </ts>
               <ts e="T342" id="Seg_2736" n="e" s="T341">kelen </ts>
               <ts e="T343" id="Seg_2738" n="e" s="T342">baran </ts>
               <ts e="T344" id="Seg_2740" n="e" s="T343">hɨrgalaːk </ts>
               <ts e="T345" id="Seg_2742" n="e" s="T344">tabatɨgar, </ts>
               <ts e="T346" id="Seg_2744" n="e" s="T345">kajdak </ts>
               <ts e="T347" id="Seg_2746" n="e" s="T346">ere </ts>
               <ts e="T348" id="Seg_2748" n="e" s="T347">miːnen </ts>
               <ts e="T349" id="Seg_2750" n="e" s="T348">baran </ts>
               <ts e="T350" id="Seg_2752" n="e" s="T349">bu͡o </ts>
               <ts e="T351" id="Seg_2754" n="e" s="T350">min </ts>
               <ts e="T352" id="Seg_2756" n="e" s="T351">hɨrgalaːk </ts>
               <ts e="T353" id="Seg_2758" n="e" s="T352">tababɨn </ts>
               <ts e="T354" id="Seg_2760" n="e" s="T353">hite </ts>
               <ts e="T355" id="Seg_2762" n="e" s="T354">barabɨt </ts>
               <ts e="T356" id="Seg_2764" n="e" s="T355">bu͡o. </ts>
               <ts e="T357" id="Seg_2766" n="e" s="T356">Onno </ts>
               <ts e="T358" id="Seg_2768" n="e" s="T357">bu͡o </ts>
               <ts e="T359" id="Seg_2770" n="e" s="T358">kü͡ölü </ts>
               <ts e="T360" id="Seg_2772" n="e" s="T359">ɨttaːrɨbɨt </ts>
               <ts e="T361" id="Seg_2774" n="e" s="T360">eni͡ege. </ts>
               <ts e="T362" id="Seg_2776" n="e" s="T361">Eni͡e </ts>
               <ts e="T363" id="Seg_2778" n="e" s="T362">üstün </ts>
               <ts e="T364" id="Seg_2780" n="e" s="T363">bu͡ollagɨna </ts>
               <ts e="T366" id="Seg_2782" n="e" s="T364">kak raz </ts>
               <ts e="T367" id="Seg_2784" n="e" s="T366">baltɨm </ts>
               <ts e="T368" id="Seg_2786" n="e" s="T367">baːr </ts>
               <ts e="T369" id="Seg_2788" n="e" s="T368">ete </ts>
               <ts e="T370" id="Seg_2790" n="e" s="T369">ol </ts>
               <ts e="T371" id="Seg_2792" n="e" s="T370">muŋnaːk </ts>
               <ts e="T372" id="Seg_2794" n="e" s="T371">hɨrgattan </ts>
               <ts e="T373" id="Seg_2796" n="e" s="T372">tühen </ts>
               <ts e="T374" id="Seg_2798" n="e" s="T373">baran </ts>
               <ts e="T375" id="Seg_2800" n="e" s="T374">nʼuŋuːnu </ts>
               <ts e="T376" id="Seg_2802" n="e" s="T375">kaban </ts>
               <ts e="T377" id="Seg_2804" n="e" s="T376">ɨlla </ts>
               <ts e="T378" id="Seg_2806" n="e" s="T377">innʼe </ts>
               <ts e="T380" id="Seg_2808" n="e" s="T378">kak raz </ts>
               <ts e="T381" id="Seg_2810" n="e" s="T380">tabalar </ts>
               <ts e="T382" id="Seg_2812" n="e" s="T381">toktoːtular. </ts>
               <ts e="T385" id="Seg_2814" n="e" s="T382">I v eto vremja </ts>
               <ts e="T386" id="Seg_2816" n="e" s="T385">bihigi </ts>
               <ts e="T387" id="Seg_2818" n="e" s="T386">kelebit </ts>
               <ts e="T389" id="Seg_2820" n="e" s="T387">bu͡o. </ts>
               <ts e="T390" id="Seg_2822" n="e" s="T389">Teːtem </ts>
               <ts e="T391" id="Seg_2824" n="e" s="T390">hɨrgalaːk </ts>
               <ts e="T392" id="Seg_2826" n="e" s="T391">tababɨn </ts>
               <ts e="T393" id="Seg_2828" n="e" s="T392">baːjan </ts>
               <ts e="T394" id="Seg_2830" n="e" s="T393">keːste </ts>
               <ts e="T395" id="Seg_2832" n="e" s="T394">mini͡enin. </ts>
               <ts e="T396" id="Seg_2834" n="e" s="T395">Innʼe </ts>
               <ts e="T397" id="Seg_2836" n="e" s="T396">gɨnan </ts>
               <ts e="T398" id="Seg_2838" n="e" s="T397">baran </ts>
               <ts e="T399" id="Seg_2840" n="e" s="T398">bejete </ts>
               <ts e="T400" id="Seg_2842" n="e" s="T399">bu͡o </ts>
               <ts e="T401" id="Seg_2844" n="e" s="T400">min </ts>
               <ts e="T402" id="Seg_2846" n="e" s="T401">hɨrgalaːk </ts>
               <ts e="T403" id="Seg_2848" n="e" s="T402">tabalar </ts>
               <ts e="T404" id="Seg_2850" n="e" s="T403">miːnen </ts>
               <ts e="T405" id="Seg_2852" n="e" s="T404">baran </ts>
               <ts e="T406" id="Seg_2854" n="e" s="T405">ol </ts>
               <ts e="T407" id="Seg_2856" n="e" s="T406">di͡ebitiger </ts>
               <ts e="T408" id="Seg_2858" n="e" s="T407">bardɨbɨt </ts>
               <ts e="T409" id="Seg_2860" n="e" s="T408">bu͡o. </ts>
               <ts e="T410" id="Seg_2862" n="e" s="T409">Di͡ebitiger </ts>
               <ts e="T411" id="Seg_2864" n="e" s="T410">baran </ts>
               <ts e="T412" id="Seg_2866" n="e" s="T411">baran </ts>
               <ts e="T413" id="Seg_2868" n="e" s="T412">bu͡ollagɨnan </ts>
               <ts e="T414" id="Seg_2870" n="e" s="T413">kimi͡eke </ts>
               <ts e="T415" id="Seg_2872" n="e" s="T414">aːmmar </ts>
               <ts e="T416" id="Seg_2874" n="e" s="T415">toktotor </ts>
               <ts e="T417" id="Seg_2876" n="e" s="T416">bu͡o. </ts>
               <ts e="T418" id="Seg_2878" n="e" s="T417">Bu͡ologum </ts>
               <ts e="T419" id="Seg_2880" n="e" s="T418">aːnɨgar </ts>
               <ts e="T420" id="Seg_2882" n="e" s="T419">ke </ts>
               <ts e="T421" id="Seg_2884" n="e" s="T420">innʼe </ts>
               <ts e="T422" id="Seg_2886" n="e" s="T421">bu͡o </ts>
               <ts e="T423" id="Seg_2888" n="e" s="T422">emeːksitter, </ts>
               <ts e="T424" id="Seg_2890" n="e" s="T423">ikki </ts>
               <ts e="T425" id="Seg_2892" n="e" s="T424">emeːksin </ts>
               <ts e="T426" id="Seg_2894" n="e" s="T425">baːr </ts>
               <ts e="T427" id="Seg_2896" n="e" s="T426">ete. </ts>
               <ts e="T428" id="Seg_2898" n="e" s="T427">Dve babki </ts>
               <ts e="T429" id="Seg_2900" n="e" s="T428">olor </ts>
               <ts e="T430" id="Seg_2902" n="e" s="T429">kelliler </ts>
               <ts e="T431" id="Seg_2904" n="e" s="T430">"kaja </ts>
               <ts e="T432" id="Seg_2906" n="e" s="T431">bu͡o </ts>
               <ts e="T433" id="Seg_2908" n="e" s="T432">tu͡ok </ts>
               <ts e="T434" id="Seg_2910" n="e" s="T433">bu͡ollugut </ts>
               <ts e="T435" id="Seg_2912" n="e" s="T434">kajdak </ts>
               <ts e="T436" id="Seg_2914" n="e" s="T435">kajdak </ts>
               <ts e="T438" id="Seg_2916" n="e" s="T436">bu͡ollugut". </ts>
               <ts e="T439" id="Seg_2918" n="e" s="T438">Araː </ts>
               <ts e="T440" id="Seg_2920" n="e" s="T439">bu </ts>
               <ts e="T441" id="Seg_2922" n="e" s="T440">mini͡eke </ts>
               <ts e="T442" id="Seg_2924" n="e" s="T441">kata </ts>
               <ts e="T443" id="Seg_2926" n="e" s="T442">bu </ts>
               <ts e="T444" id="Seg_2928" n="e" s="T443">dʼaktar </ts>
               <ts e="T445" id="Seg_2930" n="e" s="T444">ölörünne </ts>
               <ts e="T446" id="Seg_2932" n="e" s="T445">munu </ts>
               <ts e="T447" id="Seg_2934" n="e" s="T446">naːda </ts>
               <ts e="T448" id="Seg_2936" n="e" s="T447">du͡oktuːrda </ts>
               <ts e="T449" id="Seg_2938" n="e" s="T448">ɨgɨrɨnɨ͡akka. </ts>
               <ts e="T450" id="Seg_2940" n="e" s="T449">Munu </ts>
               <ts e="T451" id="Seg_2942" n="e" s="T450">teːtem </ts>
               <ts e="T452" id="Seg_2944" n="e" s="T451">maːmam </ts>
               <ts e="T453" id="Seg_2946" n="e" s="T452">diːr </ts>
               <ts e="T454" id="Seg_2948" n="e" s="T453">"barɨma </ts>
               <ts e="T455" id="Seg_2950" n="e" s="T454">barɨma". </ts>
               <ts e="T456" id="Seg_2952" n="e" s="T455">Du͡oktuːru </ts>
               <ts e="T457" id="Seg_2954" n="e" s="T456">tugun </ts>
               <ts e="T458" id="Seg_2956" n="e" s="T457">diːr </ts>
               <ts e="T459" id="Seg_2958" n="e" s="T458">anʼiː </ts>
               <ts e="T460" id="Seg_2960" n="e" s="T459">dagɨnɨ </ts>
               <ts e="T461" id="Seg_2962" n="e" s="T460">üčügej </ts>
               <ts e="T462" id="Seg_2964" n="e" s="T461">köksüte </ts>
               <ts e="T463" id="Seg_2966" n="e" s="T462">ki͡eŋ </ts>
               <ts e="T465" id="Seg_2968" n="e" s="T463">vrode bɨ </ts>
               <ts e="T466" id="Seg_2970" n="e" s="T465">üčügej, </ts>
               <ts e="T467" id="Seg_2972" n="e" s="T466">beːbe. </ts>
               <ts e="T468" id="Seg_2974" n="e" s="T467">Oččogo </ts>
               <ts e="T469" id="Seg_2976" n="e" s="T468">minigin </ts>
               <ts e="T470" id="Seg_2978" n="e" s="T469">ikki </ts>
               <ts e="T471" id="Seg_2980" n="e" s="T470">di͡egitten </ts>
               <ts e="T472" id="Seg_2982" n="e" s="T471">kötögön </ts>
               <ts e="T473" id="Seg_2984" n="e" s="T472">baran </ts>
               <ts e="T474" id="Seg_2986" n="e" s="T473">di͡e </ts>
               <ts e="T475" id="Seg_2988" n="e" s="T474">bolokko </ts>
               <ts e="T476" id="Seg_2990" n="e" s="T475">kiːllerdiler </ts>
               <ts e="T477" id="Seg_2992" n="e" s="T476">bu͡o. </ts>
               <ts e="T478" id="Seg_2994" n="e" s="T477">Eee, </ts>
               <ts e="T479" id="Seg_2996" n="e" s="T478">de </ts>
               <ts e="T480" id="Seg_2998" n="e" s="T479">ol </ts>
               <ts e="T481" id="Seg_3000" n="e" s="T480">hɨgɨnnʼaktanan </ts>
               <ts e="T482" id="Seg_3002" n="e" s="T481">kanʼan </ts>
               <ts e="T483" id="Seg_3004" n="e" s="T482">baran </ts>
               <ts e="T484" id="Seg_3006" n="e" s="T483">de. </ts>
               <ts e="T485" id="Seg_3008" n="e" s="T484">Tu͡ok </ts>
               <ts e="T486" id="Seg_3010" n="e" s="T485">du͡oktuːrun </ts>
               <ts e="T487" id="Seg_3012" n="e" s="T486">ittetin </ts>
               <ts e="T488" id="Seg_3014" n="e" s="T487">beːbe </ts>
               <ts e="T489" id="Seg_3016" n="e" s="T488">üčügej. </ts>
               <ts e="T490" id="Seg_3018" n="e" s="T489">Možet </ts>
               <ts e="T491" id="Seg_3020" n="e" s="T490">i </ts>
               <ts e="T492" id="Seg_3022" n="e" s="T491">meːne </ts>
               <ts e="T493" id="Seg_3024" n="e" s="T492">itte </ts>
               <ts e="T494" id="Seg_3026" n="e" s="T493">eni </ts>
               <ts e="T495" id="Seg_3028" n="e" s="T494">tu͡oga </ts>
               <ts e="T496" id="Seg_3030" n="e" s="T495">da </ts>
               <ts e="T497" id="Seg_3032" n="e" s="T496">vrode </ts>
               <ts e="T498" id="Seg_3034" n="e" s="T497">tostubatak </ts>
               <ts e="T499" id="Seg_3036" n="e" s="T498">kaːn </ts>
               <ts e="T500" id="Seg_3038" n="e" s="T499">da </ts>
               <ts e="T501" id="Seg_3040" n="e" s="T500">hu͡ok </ts>
               <ts e="T502" id="Seg_3042" n="e" s="T501">tu͡ok </ts>
               <ts e="T503" id="Seg_3044" n="e" s="T502">da </ts>
               <ts e="T504" id="Seg_3046" n="e" s="T503">hu͡ok </ts>
               <ts e="T505" id="Seg_3048" n="e" s="T504">üčügej </ts>
               <ts e="T506" id="Seg_3050" n="e" s="T505">bu͡olu͡o </ts>
               <ts e="T507" id="Seg_3052" n="e" s="T506">oččogo </ts>
               <ts e="T508" id="Seg_3054" n="e" s="T507">heee. </ts>
               <ts e="T509" id="Seg_3056" n="e" s="T508">De </ts>
               <ts e="T510" id="Seg_3058" n="e" s="T509">vot </ts>
               <ts e="T511" id="Seg_3060" n="e" s="T510">onton </ts>
               <ts e="T512" id="Seg_3062" n="e" s="T511">ol </ts>
               <ts e="T513" id="Seg_3064" n="e" s="T512">de </ts>
               <ts e="T514" id="Seg_3066" n="e" s="T513">onton </ts>
               <ts e="T515" id="Seg_3068" n="e" s="T514">onton </ts>
               <ts e="T516" id="Seg_3070" n="e" s="T515">kojutaːn </ts>
               <ts e="T517" id="Seg_3072" n="e" s="T516">kojut </ts>
               <ts e="T518" id="Seg_3074" n="e" s="T517">bagajɨ </ts>
               <ts e="T519" id="Seg_3076" n="e" s="T518">de </ts>
               <ts e="T520" id="Seg_3078" n="e" s="T519">ɨj </ts>
               <ts e="T521" id="Seg_3080" n="e" s="T520">aːsta </ts>
               <ts e="T522" id="Seg_3082" n="e" s="T521">bu͡o </ts>
               <ts e="T523" id="Seg_3084" n="e" s="T522">ɨj </ts>
               <ts e="T524" id="Seg_3086" n="e" s="T523">aːhan </ts>
               <ts e="T525" id="Seg_3088" n="e" s="T524">baran </ts>
               <ts e="T526" id="Seg_3090" n="e" s="T525">honon </ts>
               <ts e="T527" id="Seg_3092" n="e" s="T526">du͡oktuːr </ts>
               <ts e="T528" id="Seg_3094" n="e" s="T527">da </ts>
               <ts e="T529" id="Seg_3096" n="e" s="T528">hu͡ok </ts>
               <ts e="T530" id="Seg_3098" n="e" s="T529">tu͡ok </ts>
               <ts e="T531" id="Seg_3100" n="e" s="T530">da </ts>
               <ts e="T532" id="Seg_3102" n="e" s="T531">hu͡ok </ts>
               <ts e="T533" id="Seg_3104" n="e" s="T532">ɨj </ts>
               <ts e="T534" id="Seg_3106" n="e" s="T533">aːhan </ts>
               <ts e="T535" id="Seg_3108" n="e" s="T534">baran </ts>
               <ts e="T536" id="Seg_3110" n="e" s="T535">de </ts>
               <ts e="T537" id="Seg_3112" n="e" s="T536">ol </ts>
               <ts e="T538" id="Seg_3114" n="e" s="T537">haŋa </ts>
               <ts e="T539" id="Seg_3116" n="e" s="T538">dʼɨlga </ts>
               <ts e="T540" id="Seg_3118" n="e" s="T539">kiːreːri </ts>
               <ts e="T541" id="Seg_3120" n="e" s="T540">de </ts>
               <ts e="T542" id="Seg_3122" n="e" s="T541">du͡oktuːrga </ts>
               <ts e="T543" id="Seg_3124" n="e" s="T542">kiːrdibit </ts>
               <ts e="T544" id="Seg_3126" n="e" s="T543">bu͡o. </ts>
               <ts e="T545" id="Seg_3128" n="e" s="T544">Onno </ts>
               <ts e="T546" id="Seg_3130" n="e" s="T545">kiːremmit </ts>
               <ts e="T547" id="Seg_3132" n="e" s="T546">ol. </ts>
               <ts e="T548" id="Seg_3134" n="e" s="T547">Kaja </ts>
               <ts e="T549" id="Seg_3136" n="e" s="T548">du͡oktuːrum </ts>
               <ts e="T550" id="Seg_3138" n="e" s="T549">diːr </ts>
               <ts e="T551" id="Seg_3140" n="e" s="T550">bu͡o, </ts>
               <ts e="T552" id="Seg_3142" n="e" s="T551">kaja </ts>
               <ts e="T553" id="Seg_3144" n="e" s="T552">eni͡ene </ts>
               <ts e="T554" id="Seg_3146" n="e" s="T553">öttügüŋ </ts>
               <ts e="T555" id="Seg_3148" n="e" s="T554">öttügüŋ </ts>
               <ts e="T556" id="Seg_3150" n="e" s="T555">tostubut </ts>
               <ts e="T557" id="Seg_3152" n="e" s="T556">ebit </ts>
               <ts e="T558" id="Seg_3154" n="e" s="T557">enigin </ts>
               <ts e="T559" id="Seg_3156" n="e" s="T558">naːda </ts>
               <ts e="T560" id="Seg_3158" n="e" s="T559">Nosku͡oga </ts>
               <ts e="T561" id="Seg_3160" n="e" s="T560">ɨːtɨ͡akka. </ts>
               <ts e="T562" id="Seg_3162" n="e" s="T561">Vot </ts>
               <ts e="T563" id="Seg_3164" n="e" s="T562">onton </ts>
               <ts e="T564" id="Seg_3166" n="e" s="T563">Nosku͡oga </ts>
               <ts e="T565" id="Seg_3168" n="e" s="T564">ɨːppɨttara, </ts>
               <ts e="T566" id="Seg_3170" n="e" s="T565">Nosku͡o </ts>
               <ts e="T567" id="Seg_3172" n="e" s="T566">min </ts>
               <ts e="T568" id="Seg_3174" n="e" s="T567">hɨttɨm </ts>
               <ts e="T569" id="Seg_3176" n="e" s="T568">bu͡o </ts>
               <ts e="T570" id="Seg_3178" n="e" s="T569">onno </ts>
               <ts e="T571" id="Seg_3180" n="e" s="T570">de. </ts>
               <ts e="T572" id="Seg_3182" n="e" s="T571">Ikki </ts>
               <ts e="T573" id="Seg_3184" n="e" s="T572">ɨjɨ </ts>
               <ts e="T574" id="Seg_3186" n="e" s="T573">vɨčiškaga </ts>
               <ts e="T575" id="Seg_3188" n="e" s="T574">hɨttɨm </ts>
               <ts e="T576" id="Seg_3190" n="e" s="T575">bu͡o. </ts>
               <ts e="T577" id="Seg_3192" n="e" s="T576">Onton </ts>
               <ts e="T578" id="Seg_3194" n="e" s="T577">bu͡o </ts>
               <ts e="T579" id="Seg_3196" n="e" s="T578">aŋar </ts>
               <ts e="T580" id="Seg_3198" n="e" s="T579">ɨjdarɨn </ts>
               <ts e="T581" id="Seg_3200" n="e" s="T580">bu͡o </ts>
               <ts e="T582" id="Seg_3202" n="e" s="T581">kör </ts>
               <ts e="T583" id="Seg_3204" n="e" s="T582">agɨs </ts>
               <ts e="T584" id="Seg_3206" n="e" s="T583">ɨjɨ </ts>
               <ts e="T585" id="Seg_3208" n="e" s="T584">meldʼi </ts>
               <ts e="T586" id="Seg_3210" n="e" s="T585">hɨttɨm </ts>
               <ts e="T587" id="Seg_3212" n="e" s="T586">bu͡o </ts>
               <ts e="T588" id="Seg_3214" n="e" s="T587">Nosku͡oŋa. </ts>
               <ts e="T614" id="Seg_3216" n="e" s="T588">Eto v ijun'e, v ijun'e domoj priexala, </ts>
               <ts e="T589" id="Seg_3218" n="e" s="T614">di͡eber </ts>
               <ts e="T590" id="Seg_3220" n="e" s="T589">kelbitim. </ts>
               <ts e="T591" id="Seg_3222" n="e" s="T590">I </ts>
               <ts e="T592" id="Seg_3224" n="e" s="T591">atagɨm </ts>
               <ts e="T597" id="Seg_3226" n="e" s="T592">(kak kak kak =) kak perelomannyj </ts>
               <ts e="T598" id="Seg_3228" n="e" s="T597">ete </ts>
               <ts e="T599" id="Seg_3230" n="e" s="T598">ol </ts>
               <ts e="T600" id="Seg_3232" n="e" s="T599">kördük </ts>
               <ts e="T601" id="Seg_3234" n="e" s="T600">honon </ts>
               <ts e="T602" id="Seg_3236" n="e" s="T601">osput. </ts>
               <ts e="T612" id="Seg_3238" n="e" s="T602">Vot </ts>
               <ts e="T603" id="Seg_3240" n="e" s="T612">takaja </ts>
               <ts e="T604" id="Seg_3242" n="e" s="T603">ja </ts>
               <ts e="T606" id="Seg_3244" n="e" s="T604">pri͡ehala. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_3245" s="T0">AnIM_2009_Accident_nar.001 (001)</ta>
            <ta e="T11" id="Seg_3246" s="T7">AnIM_2009_Accident_nar.002 (002)</ta>
            <ta e="T16" id="Seg_3247" s="T11">AnIM_2009_Accident_nar.003 (003)</ta>
            <ta e="T22" id="Seg_3248" s="T16">AnIM_2009_Accident_nar.004 (004)</ta>
            <ta e="T34" id="Seg_3249" s="T22">AnIM_2009_Accident_nar.005 (005)</ta>
            <ta e="T43" id="Seg_3250" s="T34">AnIM_2009_Accident_nar.006 (006)</ta>
            <ta e="T62" id="Seg_3251" s="T43">AnIM_2009_Accident_nar.007 (007)</ta>
            <ta e="T74" id="Seg_3252" s="T62">AnIM_2009_Accident_nar.008 (008)</ta>
            <ta e="T91" id="Seg_3253" s="T74">AnIM_2009_Accident_nar.009 (009)</ta>
            <ta e="T99" id="Seg_3254" s="T91">AnIM_2009_Accident_nar.010 (010)</ta>
            <ta e="T106" id="Seg_3255" s="T99">AnIM_2009_Accident_nar.011 (011)</ta>
            <ta e="T111" id="Seg_3256" s="T106">AnIM_2009_Accident_nar.012 (012)</ta>
            <ta e="T119" id="Seg_3257" s="T111">AnIM_2009_Accident_nar.013 (013)</ta>
            <ta e="T132" id="Seg_3258" s="T119">AnIM_2009_Accident_nar.014 (014)</ta>
            <ta e="T157" id="Seg_3259" s="T132">AnIM_2009_Accident_nar.015 (015)</ta>
            <ta e="T172" id="Seg_3260" s="T157">AnIM_2009_Accident_nar.016 (016)</ta>
            <ta e="T182" id="Seg_3261" s="T172">AnIM_2009_Accident_nar.017 (017)</ta>
            <ta e="T193" id="Seg_3262" s="T182">AnIM_2009_Accident_nar.018 (018)</ta>
            <ta e="T206" id="Seg_3263" s="T193">AnIM_2009_Accident_nar.019 (019)</ta>
            <ta e="T214" id="Seg_3264" s="T206">AnIM_2009_Accident_nar.020 (020)</ta>
            <ta e="T229" id="Seg_3265" s="T214">AnIM_2009_Accident_nar.021 (021)</ta>
            <ta e="T235" id="Seg_3266" s="T229">AnIM_2009_Accident_nar.022 (022)</ta>
            <ta e="T243" id="Seg_3267" s="T235">AnIM_2009_Accident_nar.023 (023)</ta>
            <ta e="T251" id="Seg_3268" s="T243">AnIM_2009_Accident_nar.024 (024)</ta>
            <ta e="T255" id="Seg_3269" s="T251">AnIM_2009_Accident_nar.025 (025)</ta>
            <ta e="T259" id="Seg_3270" s="T255">AnIM_2009_Accident_nar.026 (026)</ta>
            <ta e="T264" id="Seg_3271" s="T259">AnIM_2009_Accident_nar.027 (027)</ta>
            <ta e="T273" id="Seg_3272" s="T264">AnIM_2009_Accident_nar.028 (028)</ta>
            <ta e="T284" id="Seg_3273" s="T273">AnIM_2009_Accident_nar.029 (029)</ta>
            <ta e="T290" id="Seg_3274" s="T284">AnIM_2009_Accident_nar.030 (030)</ta>
            <ta e="T302" id="Seg_3275" s="T290">AnIM_2009_Accident_nar.031 (031)</ta>
            <ta e="T307" id="Seg_3276" s="T302">AnIM_2009_Accident_nar.032 (032)</ta>
            <ta e="T317" id="Seg_3277" s="T307">AnIM_2009_Accident_nar.033 (033)</ta>
            <ta e="T328" id="Seg_3278" s="T317">AnIM_2009_Accident_nar.034 (034)</ta>
            <ta e="T335" id="Seg_3279" s="T328">AnIM_2009_Accident_nar.035 (035)</ta>
            <ta e="T340" id="Seg_3280" s="T335">AnIM_2009_Accident_nar.036 (036)</ta>
            <ta e="T356" id="Seg_3281" s="T340">AnIM_2009_Accident_nar.037 (037)</ta>
            <ta e="T361" id="Seg_3282" s="T356">AnIM_2009_Accident_nar.038 (038)</ta>
            <ta e="T382" id="Seg_3283" s="T361">AnIM_2009_Accident_nar.039 (039)</ta>
            <ta e="T389" id="Seg_3284" s="T382">AnIM_2009_Accident_nar.040 (040)</ta>
            <ta e="T395" id="Seg_3285" s="T389">AnIM_2009_Accident_nar.041 (041)</ta>
            <ta e="T409" id="Seg_3286" s="T395">AnIM_2009_Accident_nar.042 (042)</ta>
            <ta e="T417" id="Seg_3287" s="T409">AnIM_2009_Accident_nar.043 (043)</ta>
            <ta e="T427" id="Seg_3288" s="T417">AnIM_2009_Accident_nar.044 (044)</ta>
            <ta e="T438" id="Seg_3289" s="T427">AnIM_2009_Accident_nar.045 (045)</ta>
            <ta e="T449" id="Seg_3290" s="T438">AnIM_2009_Accident_nar.046 (046)</ta>
            <ta e="T455" id="Seg_3291" s="T449">AnIM_2009_Accident_nar.047 (047)</ta>
            <ta e="T467" id="Seg_3292" s="T455">AnIM_2009_Accident_nar.048 (048)</ta>
            <ta e="T477" id="Seg_3293" s="T467">AnIM_2009_Accident_nar.049 (049)</ta>
            <ta e="T484" id="Seg_3294" s="T477">AnIM_2009_Accident_nar.050 (050)</ta>
            <ta e="T489" id="Seg_3295" s="T484">AnIM_2009_Accident_nar.051 (051)</ta>
            <ta e="T508" id="Seg_3296" s="T489">AnIM_2009_Accident_nar.052 (052)</ta>
            <ta e="T544" id="Seg_3297" s="T508">AnIM_2009_Accident_nar.053 (053)</ta>
            <ta e="T547" id="Seg_3298" s="T544">AnIM_2009_Accident_nar.054 (054)</ta>
            <ta e="T561" id="Seg_3299" s="T547">AnIM_2009_Accident_nar.055 (055)</ta>
            <ta e="T571" id="Seg_3300" s="T561">AnIM_2009_Accident_nar.056 (056)</ta>
            <ta e="T576" id="Seg_3301" s="T571">AnIM_2009_Accident_nar.057 (057)</ta>
            <ta e="T588" id="Seg_3302" s="T576">AnIM_2009_Accident_nar.058 (058)</ta>
            <ta e="T590" id="Seg_3303" s="T588">AnIM_2009_Accident_nar.059 (059)</ta>
            <ta e="T602" id="Seg_3304" s="T590">AnIM_2009_Accident_nar.060 (060)</ta>
            <ta e="T606" id="Seg_3305" s="T602">AnIM_2009_Accident_nar.061 (061)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T157" id="Seg_3306" s="T132">kaːmnaː-AːrI-BIt mama.R-(I)m di͡e-Ar bu͡o u͡ot-BIt saŋar-Ar di͡e-Ar u͡ot-BIt saŋar-Ar eto.R značit.R bu͡ollagɨna tu͡ok ere kuhagan bu͡ol-AːččI u͡ot saŋar-TAk-InA ke saŋar-TAk-InA</ta>
            <ta e="T172" id="Seg_3307" s="T157">oččogo bu͡ollagɨna nu.R ja net.R si͡e min bu͡ollagɨna olus daːgɨnɨ ol-(n)I bu͡ol-TAk-InA itigej-BAt-GA bu͡ollagɨna</ta>
            <ta e="T182" id="Seg_3308" s="T172">d'e taba da bar-An kaːl-TI-BIt bu͡o ol bu taba-(n)I</ta>
            <ta e="T193" id="Seg_3309" s="T182">kap-BIt-BIt inn’e bar-An kaːl-BIt-BIt bu͡o teːte-BIn kɨtta bar-(I)s--An kaːl-BIt-BIt bar-(I)s--An kaːl</ta>
            <ta e="T206" id="Seg_3310" s="T193">hurt-GA kel-An bar-An bu͡ol-TAk-InA teːte-(I)m hɨrga-BItIn hetiː-BItIn bu͡ol-TAk-InA ti͡ej-(I)n-TI-BIt ti͡ej-(I)n-An ötü-LAː-(I)n-An kann’aː-An bu͡o</ta>
            <ta e="T214" id="Seg_3311" s="T206">minigin d'e kaːmnaː-(A)ttA bu͡o hɨrga-LAːk taba-BIn hi͡et-An bar-An</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_3312" s="T0">Ogo erdekpinen bu͡ollagɨnan bu͡o ogolorum kɨra etilere. </ta>
            <ta e="T11" id="Seg_3313" s="T7">Biːr ogom dʼɨllaːk ete. </ta>
            <ta e="T16" id="Seg_3314" s="T11">Dʼɨltan orduktaːk god i sem. </ta>
            <ta e="T22" id="Seg_3315" s="T16">Biːr ogom bu͡ollagɨna agɨs ɨjdaːk ete. </ta>
            <ta e="T34" id="Seg_3316" s="T22">Oččogo bu͡ollagɨnan bu͡o teːtem mas egele barammɨn teːtem gɨtta barsɨbɨt etim hetiːleːkpin. </ta>
            <ta e="T43" id="Seg_3317" s="T34">Hetiːleːppin hetiːber bu͡ollagɨna baltɨm oloror ete bi͡es dʼɨllaːk baltɨm. </ta>
            <ta e="T62" id="Seg_3318" s="T43">Ješʼo ja vam ne rasskazala že, u nas oleni že raznɨe, oččogo že dʼaktar bu͡ollagɨna bu͡o bugdi tabanɨ ne dolžna ona kimni͡egin ke kölüjü͡ögün ke. </ta>
            <ta e="T74" id="Seg_3319" s="T62">I oččogo bu͡ollagɨna bu͡o tɨnnastɨbɨt taba potom ja rasskazhu da, tɨnnastɨbɨt taba. </ta>
            <ta e="T91" id="Seg_3320" s="T74">Eto tɨnnastɨbɨt taba bu͡ollagɨnan bu͡o eh tɨnnastɨbɨt taba ɨ͡aldʼarɨgar bu͡o erim inʼete tɨnnastaːččɨ ete menʼiːtiger tabanɨ ke. </ta>
            <ta e="T99" id="Seg_3321" s="T91">Ol tabanɨ bu͡olla dʼaktar kölüjü͡ögün nelzja ete bu͡o. </ta>
            <ta e="T106" id="Seg_3322" s="T99">Oččogo min bilbekkebin bu tabanɨ kostuːrdaːmmɨppɨn bu͡o. </ta>
            <ta e="T111" id="Seg_3323" s="T106">Ontuŋ čeːlke taba buruna taba. </ta>
            <ta e="T119" id="Seg_3324" s="T111">Inʼe bu͡ollagɨna baraːrɨbɨt bu͡ollagɨna ol taba bu͡o ɨtɨrdar. </ta>
            <ta e="T132" id="Seg_3325" s="T119">Üste raz ɨtɨrdar bu͡o oččogo min diːbin bu͡o araː tu͡okka ɨtɨrdar ke di͡ebin. </ta>
            <ta e="T157" id="Seg_3326" s="T132">A do etogo (iti innitinen) kaːmnaːrɨbɨt maːmam diːr bu͡o u͡opput haŋarar diːr u͡opput haŋarar eto značit bu͡ollagɨna tu͡ok ere kuhagan bu͡olaːččɨ u͡ot haŋardagɨna ke haŋardagɨna. </ta>
            <ta e="T172" id="Seg_3327" s="T157">Oččogo bu͡ollagɨna nu ja ne (s-) min bu͡ollagɨna olus daːgɨnɨ onu bu͡ollagɨnan itegejbekkebin bu͡ollagɨna. </ta>
            <ta e="T182" id="Seg_3328" s="T172">De taba tutunnubut da baran kaːllɨbɨt bu͡o ol bu tabanɨ. </ta>
            <ta e="T193" id="Seg_3329" s="T182">Kappɨppɨt innʼe baran kaːlbɨppɨt bu͡o teːtebin gɨtta barsan kaːlbɨppɨt barsan (kaːl-). </ta>
            <ta e="T206" id="Seg_3330" s="T193">Huːrka kelen baran bu͡ollagɨnan teːtem hɨrgabɨtɨn hetiːbitin bu͡ollagɨnan ti͡ennibit ti͡enen ötüleːnen kanʼan bu͡o. </ta>
            <ta e="T214" id="Seg_3331" s="T206">Minigin de kamnaːtta bu͡o hɨrgalaːk tababɨn hi͡eten baran. </ta>
            <ta e="T229" id="Seg_3332" s="T214">Hɨrgalaːk tababɨn hi͡eten barammɨn bu͡o hɨːha olorobun bu͡o hɨrgabar bu͡o balɨktarɨ ti͡ejbippin alta kul balɨk. </ta>
            <ta e="T235" id="Seg_3333" s="T229">Alta kul balɨk ti͡ejen baran bu͡ollana. </ta>
            <ta e="T243" id="Seg_3334" s="T235">Hɨrgalar ɨstanabin diːbin bu͡o hɨrgabɨn hɨːha olorobun bu͡o. </ta>
            <ta e="T251" id="Seg_3335" s="T243">Hu͡ok bu kihi bu͡o ješo da ɨstanabɨn bu͡o. </ta>
            <ta e="T255" id="Seg_3336" s="T251">Ješo raz ɨstanabɨn bu͡o. </ta>
            <ta e="T259" id="Seg_3337" s="T255">Emi͡e hɨːha olorobun bu͡o. </ta>
            <ta e="T264" id="Seg_3338" s="T259">Hɨrgalaːk tababɨn ɨhɨktan keːstim bu͡o. </ta>
            <ta e="T273" id="Seg_3339" s="T264">Innʼe hɨrgalaːk tabam bu͡ollagɨna hetiːm tabalara ürdübünen barallar bu͡o. </ta>
            <ta e="T284" id="Seg_3340" s="T273">I bejem bu͡ollagɨnan bu͡o hetiː ɨlagɨn aːjɨnan üste raz ergijdim bu͡o. </ta>
            <ta e="T290" id="Seg_3341" s="T284">Araː innʼe bu͡o ittenne tühen (kaːl-). </ta>
            <ta e="T302" id="Seg_3342" s="T290">Ittenne tühen baran bu͡o hɨtabɨn bu͡o teːtem hokuj kette turar ete bu͡o. </ta>
            <ta e="T307" id="Seg_3343" s="T302">Hokuj keten baran bukaːn körbüte. </ta>
            <ta e="T317" id="Seg_3344" s="T307">Körör hɨrgalaːk taba bara turar kihite bu͡o munna hɨtar hu͡olga. </ta>
            <ta e="T328" id="Seg_3345" s="T317">De hüːren kelle bu͡o innʼe di͡ete: “kaja en tɨːnnaːkkɨn du͡o?” di͡ete. </ta>
            <ta e="T335" id="Seg_3346" s="T328">“Heee tɨːnnaːppɨn tɨːnnaːppɨn atagɨm ere kɨ͡ajan kaːmnaːbat.” </ta>
            <ta e="T340" id="Seg_3347" s="T335">“Eee de beːbe min keli͡em.” </ta>
            <ta e="T356" id="Seg_3348" s="T340">De kelen baran hɨrgalaːk tabatɨgar, kajdak ere miːnen baran bu͡o min hɨrgalaːk tababɨn hite barabɨt bu͡o. </ta>
            <ta e="T361" id="Seg_3349" s="T356">Onno bu͡o kü͡ölü ɨttaːrɨbɨt eni͡ege. </ta>
            <ta e="T382" id="Seg_3350" s="T361">Eni͡e üstün bu͡ollagɨna kak raz baltɨm baːr ete ol muŋnaːk hɨrgattan tühen baran nʼuŋuːnu kaban ɨlla innʼe kak raz tabalar toktoːtular. </ta>
            <ta e="T389" id="Seg_3351" s="T382">I v eto vremja bihigi kelebit bu͡o. </ta>
            <ta e="T395" id="Seg_3352" s="T389">Teːtem hɨrgalaːk tababɨn baːjan keːste mini͡enin. </ta>
            <ta e="T409" id="Seg_3353" s="T395">Innʼe gɨnan baran bejete bu͡o min hɨrgalaːk tabalar miːnen baran ol di͡ebitiger bardɨbɨt bu͡o. </ta>
            <ta e="T417" id="Seg_3354" s="T409">Di͡ebitiger baran baran bu͡ollagɨnan kimi͡eke aːmmar toktotor bu͡o. </ta>
            <ta e="T427" id="Seg_3355" s="T417">Bu͡ologum aːnɨgar ke innʼe bu͡o emeːksitter, ikki emeːksin baːr ete. </ta>
            <ta e="T438" id="Seg_3356" s="T427">Dve babki olor kelliler" kaja bu͡o tu͡ok bu͡ollugut kajdak kajdak bu͡ollugut". </ta>
            <ta e="T449" id="Seg_3357" s="T438">Araː bu mini͡eke kata bu dʼaktar ölörünne munu naːda du͡oktuːrda ɨgɨrɨnɨ͡akka. </ta>
            <ta e="T455" id="Seg_3358" s="T449">Munu teːtem maːmam diːr "barɨma barɨma". </ta>
            <ta e="T467" id="Seg_3359" s="T455">Du͡oktuːru tugun diːr anʼiː dagɨnɨ üčügej köksüte ki͡eŋ vrode bɨ üčügej, beːbe. </ta>
            <ta e="T477" id="Seg_3360" s="T467">Oččogo minigin ikki di͡egitten kötögön baran di͡e bolokko kiːllerdiler bu͡o. </ta>
            <ta e="T484" id="Seg_3361" s="T477">Eee, de ol hɨgɨnnʼaktanan kanʼan baran de. </ta>
            <ta e="T489" id="Seg_3362" s="T484">Tu͡ok du͡oktuːrun ittetin beːbe üčügej. </ta>
            <ta e="T508" id="Seg_3363" s="T489">Možet i meːne itte eni tu͡oga da vrode tostubatak kaːn da hu͡ok tu͡ok da hu͡ok üčügej bu͡olu͡o oččogo heee. </ta>
            <ta e="T544" id="Seg_3364" s="T508">De vot onton ol de onton onton kojutaːn kojut bagajɨ de ɨj aːsta bu͡o ɨj aːhan baran honon du͡oktuːr da hu͡ok tu͡ok da hu͡ok ɨj aːhan baran de ol haŋa dʼɨlga kiːreːri de du͡oktuːrga kiːrdibit bu͡o. </ta>
            <ta e="T547" id="Seg_3365" s="T544">Onno kiːremmit ol. </ta>
            <ta e="T561" id="Seg_3366" s="T547">Kaja du͡oktuːrum diːr bu͡o, kaja eni͡ene öttügüŋ öttügüŋ tostubut ebit enigin naːda Nosku͡oga ɨːtɨ͡akka. </ta>
            <ta e="T571" id="Seg_3367" s="T561">Vot onton Nosku͡oga ɨːppɨttara, Nosku͡o min hɨttɨm bu͡o onno de. </ta>
            <ta e="T576" id="Seg_3368" s="T571">Ikki ɨjɨ vɨčiškaga hɨttɨm bu͡o. </ta>
            <ta e="T588" id="Seg_3369" s="T576">Onton bu͡o aŋar ɨjdarɨn bu͡o kör agɨs ɨjɨ meldʼi hɨttɨm bu͡o Nosku͡oŋa. </ta>
            <ta e="T590" id="Seg_3370" s="T588">Eto v ijun'e, v ijun'e domoj priexala, di͡eber kelbitim. </ta>
            <ta e="T602" id="Seg_3371" s="T590">I atagɨm kak kak kak kak perelomannyj ete ol kördük honon osput. </ta>
            <ta e="T606" id="Seg_3372" s="T602">Vot takaja ja pri͡ehala. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_3373" s="T0">ogo</ta>
            <ta e="T2" id="Seg_3374" s="T1">er-dek-pinen</ta>
            <ta e="T3" id="Seg_3375" s="T2">bu͡ollagɨnan</ta>
            <ta e="T4" id="Seg_3376" s="T3">bu͡o</ta>
            <ta e="T5" id="Seg_3377" s="T4">ogo-lor-u-m</ta>
            <ta e="T6" id="Seg_3378" s="T5">kɨra</ta>
            <ta e="T7" id="Seg_3379" s="T6">e-ti-lere</ta>
            <ta e="T8" id="Seg_3380" s="T7">biːr</ta>
            <ta e="T9" id="Seg_3381" s="T8">ogo-m</ta>
            <ta e="T10" id="Seg_3382" s="T9">dʼɨl-laːk</ta>
            <ta e="T11" id="Seg_3383" s="T10">e-t-e</ta>
            <ta e="T12" id="Seg_3384" s="T11">dʼɨl-tan</ta>
            <ta e="T13" id="Seg_3385" s="T12">orduk-taːk</ta>
            <ta e="T17" id="Seg_3386" s="T16">biːr</ta>
            <ta e="T18" id="Seg_3387" s="T17">ogo-m</ta>
            <ta e="T19" id="Seg_3388" s="T18">bu͡ollagɨna</ta>
            <ta e="T20" id="Seg_3389" s="T19">agɨs</ta>
            <ta e="T21" id="Seg_3390" s="T20">ɨj-daːk</ta>
            <ta e="T22" id="Seg_3391" s="T21">e-t-e</ta>
            <ta e="T23" id="Seg_3392" s="T22">oččogo</ta>
            <ta e="T24" id="Seg_3393" s="T23">bu͡ollagɨnan</ta>
            <ta e="T25" id="Seg_3394" s="T24">bu͡o</ta>
            <ta e="T26" id="Seg_3395" s="T25">teːte-m</ta>
            <ta e="T27" id="Seg_3396" s="T26">mas</ta>
            <ta e="T28" id="Seg_3397" s="T27">egel-e</ta>
            <ta e="T29" id="Seg_3398" s="T28">bar-am-mɨn</ta>
            <ta e="T30" id="Seg_3399" s="T29">teːte-m</ta>
            <ta e="T31" id="Seg_3400" s="T30">gɨtta</ta>
            <ta e="T32" id="Seg_3401" s="T31">bars-ɨ-bɨt</ta>
            <ta e="T33" id="Seg_3402" s="T32">e-ti-m</ta>
            <ta e="T34" id="Seg_3403" s="T33">hetiː-leːk-pin</ta>
            <ta e="T35" id="Seg_3404" s="T34">hetiː-leːk-pin</ta>
            <ta e="T36" id="Seg_3405" s="T35">hetiː-be-r</ta>
            <ta e="T37" id="Seg_3406" s="T36">bu͡ollagɨna</ta>
            <ta e="T38" id="Seg_3407" s="T37">balt-ɨ-m</ta>
            <ta e="T39" id="Seg_3408" s="T38">olor-or</ta>
            <ta e="T40" id="Seg_3409" s="T39">e-t-e</ta>
            <ta e="T41" id="Seg_3410" s="T40">bi͡es</ta>
            <ta e="T42" id="Seg_3411" s="T41">dʼɨl-laːk</ta>
            <ta e="T43" id="Seg_3412" s="T42">balt-I-m</ta>
            <ta e="T50" id="Seg_3413" s="T49">dʼaktar</ta>
            <ta e="T51" id="Seg_3414" s="T50">bu͡ollagɨna</ta>
            <ta e="T52" id="Seg_3415" s="T51">bu͡o</ta>
            <ta e="T53" id="Seg_3416" s="T52">bugdi</ta>
            <ta e="T54" id="Seg_3417" s="T53">taba-nɨ</ta>
            <ta e="T58" id="Seg_3418" s="T57">kim-n-i͡eg-i-n</ta>
            <ta e="T59" id="Seg_3419" s="T58">ke</ta>
            <ta e="T60" id="Seg_3420" s="T59">kölüj-ü͡ög-ü-n</ta>
            <ta e="T62" id="Seg_3421" s="T60">ke</ta>
            <ta e="T63" id="Seg_3422" s="T62">i</ta>
            <ta e="T64" id="Seg_3423" s="T63">oččogo</ta>
            <ta e="T65" id="Seg_3424" s="T64">bu͡ollagɨna</ta>
            <ta e="T66" id="Seg_3425" s="T65">bu͡o</ta>
            <ta e="T67" id="Seg_3426" s="T66">tɨnnast-ɨ-bɨt</ta>
            <ta e="T68" id="Seg_3427" s="T67">taba</ta>
            <ta e="T73" id="Seg_3428" s="T72">tɨnnast-ɨ-bɨt</ta>
            <ta e="T74" id="Seg_3429" s="T73">taba</ta>
            <ta e="T75" id="Seg_3430" s="T74">eto</ta>
            <ta e="T76" id="Seg_3431" s="T75">tɨnnast-ɨ-bɨt</ta>
            <ta e="T77" id="Seg_3432" s="T76">taba</ta>
            <ta e="T78" id="Seg_3433" s="T77">bu͡ollagɨnan</ta>
            <ta e="T79" id="Seg_3434" s="T78">bu͡o</ta>
            <ta e="T80" id="Seg_3435" s="T79">eh</ta>
            <ta e="T81" id="Seg_3436" s="T80">tɨnnast-ɨ-bɨt</ta>
            <ta e="T82" id="Seg_3437" s="T81">taba</ta>
            <ta e="T83" id="Seg_3438" s="T82">ɨ͡aldʼ-ar-ɨ-gar</ta>
            <ta e="T84" id="Seg_3439" s="T83">bu͡o</ta>
            <ta e="T85" id="Seg_3440" s="T84">er-i-m</ta>
            <ta e="T86" id="Seg_3441" s="T85">inʼe-te</ta>
            <ta e="T87" id="Seg_3442" s="T86">tɨnnast-aːččɨ</ta>
            <ta e="T88" id="Seg_3443" s="T87">e-t-e</ta>
            <ta e="T89" id="Seg_3444" s="T88">mejiː-ti-ger</ta>
            <ta e="T90" id="Seg_3445" s="T89">taba-nɨ</ta>
            <ta e="T91" id="Seg_3446" s="T90">ke</ta>
            <ta e="T92" id="Seg_3447" s="T91">ol</ta>
            <ta e="T93" id="Seg_3448" s="T92">taba-nɨ</ta>
            <ta e="T94" id="Seg_3449" s="T93">bu͡olla</ta>
            <ta e="T95" id="Seg_3450" s="T94">dʼaktar</ta>
            <ta e="T96" id="Seg_3451" s="T95">kölüj-ü͡ög-ü-n</ta>
            <ta e="T97" id="Seg_3452" s="T96">nelzja</ta>
            <ta e="T98" id="Seg_3453" s="T97">e-t-e</ta>
            <ta e="T99" id="Seg_3454" s="T98">bu͡o</ta>
            <ta e="T100" id="Seg_3455" s="T99">oččogo</ta>
            <ta e="T101" id="Seg_3456" s="T100">min</ta>
            <ta e="T102" id="Seg_3457" s="T101">bil-bekke-bin</ta>
            <ta e="T103" id="Seg_3458" s="T102">bu</ta>
            <ta e="T104" id="Seg_3459" s="T103">taba-nɨ</ta>
            <ta e="T105" id="Seg_3460" s="T104">kostuːr-daː-m-mɨp-pɨn</ta>
            <ta e="T106" id="Seg_3461" s="T105">bu͡o</ta>
            <ta e="T107" id="Seg_3462" s="T106">on-tu-ŋ</ta>
            <ta e="T108" id="Seg_3463" s="T107">čeːlkeː</ta>
            <ta e="T109" id="Seg_3464" s="T108">taba</ta>
            <ta e="T110" id="Seg_3465" s="T109">buruna</ta>
            <ta e="T111" id="Seg_3466" s="T110">taba</ta>
            <ta e="T112" id="Seg_3467" s="T111">innʼe</ta>
            <ta e="T113" id="Seg_3468" s="T112">bu͡ollagɨna</ta>
            <ta e="T114" id="Seg_3469" s="T113">bar-aːrɨ-bɨt</ta>
            <ta e="T115" id="Seg_3470" s="T114">bu͡ollagɨna</ta>
            <ta e="T116" id="Seg_3471" s="T115">ol</ta>
            <ta e="T117" id="Seg_3472" s="T116">taba</ta>
            <ta e="T118" id="Seg_3473" s="T117">bu͡o</ta>
            <ta e="T119" id="Seg_3474" s="T118">ɨtɨr-d-ar</ta>
            <ta e="T120" id="Seg_3475" s="T119">üs-te</ta>
            <ta e="T121" id="Seg_3476" s="T120">raz</ta>
            <ta e="T122" id="Seg_3477" s="T121">ɨtɨr-d-ar</ta>
            <ta e="T123" id="Seg_3478" s="T122">bu͡o</ta>
            <ta e="T124" id="Seg_3479" s="T123">oččogo</ta>
            <ta e="T125" id="Seg_3480" s="T124">min</ta>
            <ta e="T126" id="Seg_3481" s="T125">d-iː-bin</ta>
            <ta e="T127" id="Seg_3482" s="T126">bu͡o</ta>
            <ta e="T128" id="Seg_3483" s="T127">araː</ta>
            <ta e="T129" id="Seg_3484" s="T128">tu͡ok-ka</ta>
            <ta e="T130" id="Seg_3485" s="T129">ɨtɨr-d-ar</ta>
            <ta e="T131" id="Seg_3486" s="T130">ke</ta>
            <ta e="T132" id="Seg_3487" s="T131">d-iː-bin</ta>
            <ta e="T135" id="Seg_3488" s="T134">iti</ta>
            <ta e="T136" id="Seg_3489" s="T135">inni-ti-nen</ta>
            <ta e="T137" id="Seg_3490" s="T136">kaːmn-aːrɨ-bɨt</ta>
            <ta e="T138" id="Seg_3491" s="T137">maːma-m</ta>
            <ta e="T139" id="Seg_3492" s="T138">diː-r</ta>
            <ta e="T140" id="Seg_3493" s="T139">bu͡o</ta>
            <ta e="T141" id="Seg_3494" s="T140">u͡op-put</ta>
            <ta e="T142" id="Seg_3495" s="T141">haŋar-ar</ta>
            <ta e="T143" id="Seg_3496" s="T142">diː-r</ta>
            <ta e="T144" id="Seg_3497" s="T143">u͡op-put</ta>
            <ta e="T145" id="Seg_3498" s="T144">haŋar-ar</ta>
            <ta e="T148" id="Seg_3499" s="T147">bu͡ollagɨna</ta>
            <ta e="T149" id="Seg_3500" s="T148">tu͡ok</ta>
            <ta e="T150" id="Seg_3501" s="T149">ere</ta>
            <ta e="T151" id="Seg_3502" s="T150">kuhagan</ta>
            <ta e="T152" id="Seg_3503" s="T151">bu͡ol-aːččɨ</ta>
            <ta e="T153" id="Seg_3504" s="T152">u͡ot</ta>
            <ta e="T154" id="Seg_3505" s="T153">haŋar-dag-ɨna</ta>
            <ta e="T155" id="Seg_3506" s="T154">ke</ta>
            <ta e="T157" id="Seg_3507" s="T155">haŋar-dag-ɨna</ta>
            <ta e="T158" id="Seg_3508" s="T157">oččogo</ta>
            <ta e="T159" id="Seg_3509" s="T158">bu͡ollagɨna</ta>
            <ta e="T160" id="Seg_3510" s="T159">nu</ta>
            <ta e="T165" id="Seg_3511" s="T164">min</ta>
            <ta e="T166" id="Seg_3512" s="T165">bu͡ollagɨna</ta>
            <ta e="T167" id="Seg_3513" s="T166">olus</ta>
            <ta e="T168" id="Seg_3514" s="T167">daːgɨnɨ</ta>
            <ta e="T169" id="Seg_3515" s="T168">o-nu</ta>
            <ta e="T170" id="Seg_3516" s="T169">bu͡ollagɨnan</ta>
            <ta e="T171" id="Seg_3517" s="T170">itegej-bekke-bin</ta>
            <ta e="T172" id="Seg_3518" s="T171">bu͡ollagɨna</ta>
            <ta e="T173" id="Seg_3519" s="T172">de</ta>
            <ta e="T174" id="Seg_3520" s="T173">taba</ta>
            <ta e="T175" id="Seg_3521" s="T174">tutun-nu-but</ta>
            <ta e="T176" id="Seg_3522" s="T175">da</ta>
            <ta e="T177" id="Seg_3523" s="T176">bar-an</ta>
            <ta e="T178" id="Seg_3524" s="T177">kaːl-lɨ-bɨt</ta>
            <ta e="T179" id="Seg_3525" s="T178">bu͡o</ta>
            <ta e="T180" id="Seg_3526" s="T179">ol</ta>
            <ta e="T181" id="Seg_3527" s="T180">bu</ta>
            <ta e="T182" id="Seg_3528" s="T181">taba-nɨ</ta>
            <ta e="T183" id="Seg_3529" s="T182">kap-pɨp-pɨt</ta>
            <ta e="T184" id="Seg_3530" s="T183">innʼe</ta>
            <ta e="T185" id="Seg_3531" s="T184">bar-an</ta>
            <ta e="T186" id="Seg_3532" s="T185">kaːl-bɨp-pɨt</ta>
            <ta e="T187" id="Seg_3533" s="T186">bu͡o</ta>
            <ta e="T188" id="Seg_3534" s="T187">teːte-bi-n</ta>
            <ta e="T189" id="Seg_3535" s="T188">gɨtta</ta>
            <ta e="T190" id="Seg_3536" s="T189">bars-an</ta>
            <ta e="T191" id="Seg_3537" s="T190">kaːl-bɨp-pɨt</ta>
            <ta e="T192" id="Seg_3538" s="T191">bars-an</ta>
            <ta e="T193" id="Seg_3539" s="T192">kaːl</ta>
            <ta e="T194" id="Seg_3540" s="T193">huːr-ka</ta>
            <ta e="T195" id="Seg_3541" s="T194">kel-en</ta>
            <ta e="T196" id="Seg_3542" s="T195">bar-an</ta>
            <ta e="T197" id="Seg_3543" s="T196">bu͡ollagɨnan</ta>
            <ta e="T198" id="Seg_3544" s="T197">teːte-m</ta>
            <ta e="T199" id="Seg_3545" s="T198">hɨrga-bɨtɨ-n</ta>
            <ta e="T200" id="Seg_3546" s="T199">hetiː-biti-n</ta>
            <ta e="T201" id="Seg_3547" s="T200">bu͡ollagɨnan</ta>
            <ta e="T202" id="Seg_3548" s="T201">ti͡e-n-ni-bit</ta>
            <ta e="T203" id="Seg_3549" s="T202">ti͡e-n-en</ta>
            <ta e="T204" id="Seg_3550" s="T203">ötüː-leː-n-en</ta>
            <ta e="T205" id="Seg_3551" s="T204">kanʼ-an</ta>
            <ta e="T206" id="Seg_3552" s="T205">bu͡o</ta>
            <ta e="T207" id="Seg_3553" s="T206">minigi-n</ta>
            <ta e="T208" id="Seg_3554" s="T207">de</ta>
            <ta e="T209" id="Seg_3555" s="T208">kamnaː-t-t-a</ta>
            <ta e="T210" id="Seg_3556" s="T209">bu͡o</ta>
            <ta e="T211" id="Seg_3557" s="T210">hɨrga-laːk</ta>
            <ta e="T212" id="Seg_3558" s="T211">taba-bɨ-n</ta>
            <ta e="T213" id="Seg_3559" s="T212">hi͡et-en</ta>
            <ta e="T214" id="Seg_3560" s="T213">bar-an</ta>
            <ta e="T215" id="Seg_3561" s="T214">hɨrga-laːk</ta>
            <ta e="T216" id="Seg_3562" s="T215">taba-bɨ-n</ta>
            <ta e="T217" id="Seg_3563" s="T216">hi͡et-en</ta>
            <ta e="T218" id="Seg_3564" s="T217">bar-am-mɨn</ta>
            <ta e="T219" id="Seg_3565" s="T218">bu͡o</ta>
            <ta e="T220" id="Seg_3566" s="T219">hɨːha</ta>
            <ta e="T221" id="Seg_3567" s="T220">olor-o-bun</ta>
            <ta e="T222" id="Seg_3568" s="T221">bu͡o</ta>
            <ta e="T223" id="Seg_3569" s="T222">hɨrga-ba-r</ta>
            <ta e="T224" id="Seg_3570" s="T223">bu͡o</ta>
            <ta e="T225" id="Seg_3571" s="T224">balɨk-tar-ɨ</ta>
            <ta e="T226" id="Seg_3572" s="T225">ti͡ej-bip-pin</ta>
            <ta e="T227" id="Seg_3573" s="T226">alta</ta>
            <ta e="T228" id="Seg_3574" s="T227">küːl</ta>
            <ta e="T229" id="Seg_3575" s="T228">balɨk</ta>
            <ta e="T230" id="Seg_3576" s="T229">alta</ta>
            <ta e="T231" id="Seg_3577" s="T230">küːl</ta>
            <ta e="T232" id="Seg_3578" s="T231">balɨk</ta>
            <ta e="T233" id="Seg_3579" s="T232">ti͡ej-en</ta>
            <ta e="T234" id="Seg_3580" s="T233">bar-an</ta>
            <ta e="T235" id="Seg_3581" s="T234">bu͡ollagɨna</ta>
            <ta e="T236" id="Seg_3582" s="T235">hɨrga-lar</ta>
            <ta e="T237" id="Seg_3583" s="T236">ɨstan-a-bin</ta>
            <ta e="T238" id="Seg_3584" s="T237">d-iː-bin</ta>
            <ta e="T239" id="Seg_3585" s="T238">bu͡o</ta>
            <ta e="T240" id="Seg_3586" s="T239">hɨrga-bɨ-n</ta>
            <ta e="T241" id="Seg_3587" s="T240">hɨːha</ta>
            <ta e="T242" id="Seg_3588" s="T241">olor-o-bun</ta>
            <ta e="T243" id="Seg_3589" s="T242">bu͡o</ta>
            <ta e="T244" id="Seg_3590" s="T243">hu͡ok</ta>
            <ta e="T245" id="Seg_3591" s="T244">bu</ta>
            <ta e="T246" id="Seg_3592" s="T245">kihi</ta>
            <ta e="T247" id="Seg_3593" s="T246">bu͡o</ta>
            <ta e="T248" id="Seg_3594" s="T247">ješo</ta>
            <ta e="T249" id="Seg_3595" s="T248">da</ta>
            <ta e="T250" id="Seg_3596" s="T249">ɨstan-a-bɨn</ta>
            <ta e="T251" id="Seg_3597" s="T250">bu͡o</ta>
            <ta e="T253" id="Seg_3598" s="T252">ɨstan-a-bɨn</ta>
            <ta e="T255" id="Seg_3599" s="T253">bu͡o</ta>
            <ta e="T256" id="Seg_3600" s="T255">emi͡e</ta>
            <ta e="T257" id="Seg_3601" s="T256">hɨːha</ta>
            <ta e="T258" id="Seg_3602" s="T257">olor-o-bun</ta>
            <ta e="T259" id="Seg_3603" s="T258">bu͡o</ta>
            <ta e="T260" id="Seg_3604" s="T259">hɨrga-laːk</ta>
            <ta e="T261" id="Seg_3605" s="T260">taba-bɨ-n</ta>
            <ta e="T262" id="Seg_3606" s="T261">ɨhɨk-t-an</ta>
            <ta e="T263" id="Seg_3607" s="T262">keːs-ti-m</ta>
            <ta e="T264" id="Seg_3608" s="T263">bu͡o</ta>
            <ta e="T265" id="Seg_3609" s="T264">innʼe</ta>
            <ta e="T266" id="Seg_3610" s="T265">hɨrga-laːk</ta>
            <ta e="T267" id="Seg_3611" s="T266">taba-m</ta>
            <ta e="T268" id="Seg_3612" s="T267">bu͡ollagɨna</ta>
            <ta e="T269" id="Seg_3613" s="T268">hetiː-m</ta>
            <ta e="T270" id="Seg_3614" s="T269">taba-lar-a</ta>
            <ta e="T271" id="Seg_3615" s="T270">ürdü-bü-nen</ta>
            <ta e="T272" id="Seg_3616" s="T271">bar-al-lar</ta>
            <ta e="T273" id="Seg_3617" s="T272">bu͡o</ta>
            <ta e="T274" id="Seg_3618" s="T273">i</ta>
            <ta e="T275" id="Seg_3619" s="T274">beje-m</ta>
            <ta e="T276" id="Seg_3620" s="T275">bu͡ollagɨnan</ta>
            <ta e="T277" id="Seg_3621" s="T276">bu͡o</ta>
            <ta e="T278" id="Seg_3622" s="T277">hetiː</ta>
            <ta e="T279" id="Seg_3623" s="T278">ɨlag-ɨ-n</ta>
            <ta e="T280" id="Seg_3624" s="T279">aːjɨ-nan</ta>
            <ta e="T281" id="Seg_3625" s="T280">üs-te</ta>
            <ta e="T282" id="Seg_3626" s="T281">raz</ta>
            <ta e="T283" id="Seg_3627" s="T282">ergij-di-m</ta>
            <ta e="T284" id="Seg_3628" s="T283">bu͡o</ta>
            <ta e="T285" id="Seg_3629" s="T284">araː</ta>
            <ta e="T286" id="Seg_3630" s="T285">innʼe</ta>
            <ta e="T287" id="Seg_3631" s="T286">bu͡o</ta>
            <ta e="T288" id="Seg_3632" s="T287">ittenne</ta>
            <ta e="T289" id="Seg_3633" s="T288">tüh-en</ta>
            <ta e="T290" id="Seg_3634" s="T289">kaːl</ta>
            <ta e="T291" id="Seg_3635" s="T290">ittenne</ta>
            <ta e="T292" id="Seg_3636" s="T291">tüh-en</ta>
            <ta e="T293" id="Seg_3637" s="T292">bar-an</ta>
            <ta e="T294" id="Seg_3638" s="T293">bu͡o</ta>
            <ta e="T295" id="Seg_3639" s="T294">hɨt-a-bɨn</ta>
            <ta e="T296" id="Seg_3640" s="T295">bu͡o</ta>
            <ta e="T297" id="Seg_3641" s="T296">teːte-m</ta>
            <ta e="T298" id="Seg_3642" s="T297">hokuj</ta>
            <ta e="T299" id="Seg_3643" s="T298">ket-t-e</ta>
            <ta e="T300" id="Seg_3644" s="T299">tur-ar</ta>
            <ta e="T301" id="Seg_3645" s="T300">e-t-e</ta>
            <ta e="T302" id="Seg_3646" s="T301">bu͡o</ta>
            <ta e="T303" id="Seg_3647" s="T302">hokuj</ta>
            <ta e="T304" id="Seg_3648" s="T303">ket-en</ta>
            <ta e="T305" id="Seg_3649" s="T304">bar-an</ta>
            <ta e="T306" id="Seg_3650" s="T305">bu-kaːn</ta>
            <ta e="T307" id="Seg_3651" s="T306">kör-büt-e</ta>
            <ta e="T308" id="Seg_3652" s="T307">kör-ör</ta>
            <ta e="T309" id="Seg_3653" s="T308">hɨrga-laːk</ta>
            <ta e="T310" id="Seg_3654" s="T309">taba</ta>
            <ta e="T311" id="Seg_3655" s="T310">bar-a</ta>
            <ta e="T312" id="Seg_3656" s="T311">tur-ar</ta>
            <ta e="T313" id="Seg_3657" s="T312">kihi-te</ta>
            <ta e="T314" id="Seg_3658" s="T313">bu͡o</ta>
            <ta e="T315" id="Seg_3659" s="T314">munna</ta>
            <ta e="T316" id="Seg_3660" s="T315">hɨt-ar</ta>
            <ta e="T317" id="Seg_3661" s="T316">hu͡ol-ga</ta>
            <ta e="T318" id="Seg_3662" s="T317">de</ta>
            <ta e="T319" id="Seg_3663" s="T318">hüːr-en</ta>
            <ta e="T320" id="Seg_3664" s="T319">kel-l-e</ta>
            <ta e="T321" id="Seg_3665" s="T320">bu͡o</ta>
            <ta e="T322" id="Seg_3666" s="T321">innʼe</ta>
            <ta e="T323" id="Seg_3667" s="T322">di͡e-t-e</ta>
            <ta e="T324" id="Seg_3668" s="T323">kaja</ta>
            <ta e="T325" id="Seg_3669" s="T324">en</ta>
            <ta e="T326" id="Seg_3670" s="T325">tɨːn-naːk-kɨn</ta>
            <ta e="T327" id="Seg_3671" s="T326">du͡o</ta>
            <ta e="T328" id="Seg_3672" s="T327">di͡e-t-e</ta>
            <ta e="T329" id="Seg_3673" s="T328">heː</ta>
            <ta e="T330" id="Seg_3674" s="T329">tɨːn-naːp-pɨn</ta>
            <ta e="T331" id="Seg_3675" s="T330">tɨːn-naːp-pɨn</ta>
            <ta e="T332" id="Seg_3676" s="T331">atag-ɨ-m</ta>
            <ta e="T333" id="Seg_3677" s="T332">ere</ta>
            <ta e="T334" id="Seg_3678" s="T333">kɨ͡aj-an</ta>
            <ta e="T335" id="Seg_3679" s="T334">kamnaː-bat</ta>
            <ta e="T336" id="Seg_3680" s="T335">eː</ta>
            <ta e="T337" id="Seg_3681" s="T336">de</ta>
            <ta e="T338" id="Seg_3682" s="T337">beːbe</ta>
            <ta e="T339" id="Seg_3683" s="T338">min</ta>
            <ta e="T340" id="Seg_3684" s="T339">kel-i͡e-m</ta>
            <ta e="T341" id="Seg_3685" s="T340">de</ta>
            <ta e="T342" id="Seg_3686" s="T341">kel-en</ta>
            <ta e="T343" id="Seg_3687" s="T342">bar-an</ta>
            <ta e="T344" id="Seg_3688" s="T343">hɨrga-laːk</ta>
            <ta e="T345" id="Seg_3689" s="T344">taba-tɨ-gar</ta>
            <ta e="T346" id="Seg_3690" s="T345">kajdak</ta>
            <ta e="T347" id="Seg_3691" s="T346">ere</ta>
            <ta e="T348" id="Seg_3692" s="T347">miːn-en</ta>
            <ta e="T349" id="Seg_3693" s="T348">bar-an</ta>
            <ta e="T350" id="Seg_3694" s="T349">bu͡o</ta>
            <ta e="T351" id="Seg_3695" s="T350">min</ta>
            <ta e="T352" id="Seg_3696" s="T351">hɨrga-laːk</ta>
            <ta e="T353" id="Seg_3697" s="T352">taba-bɨ-n</ta>
            <ta e="T354" id="Seg_3698" s="T353">hit-e</ta>
            <ta e="T355" id="Seg_3699" s="T354">bar-a-bɨt</ta>
            <ta e="T356" id="Seg_3700" s="T355">bu͡o</ta>
            <ta e="T357" id="Seg_3701" s="T356">onno</ta>
            <ta e="T358" id="Seg_3702" s="T357">bu͡o</ta>
            <ta e="T359" id="Seg_3703" s="T358">kü͡öl-ü</ta>
            <ta e="T360" id="Seg_3704" s="T359">ɨtt-aːrɨ-bɨt</ta>
            <ta e="T361" id="Seg_3705" s="T360">eni͡e-ge</ta>
            <ta e="T362" id="Seg_3706" s="T361">eni͡e</ta>
            <ta e="T363" id="Seg_3707" s="T362">üstün</ta>
            <ta e="T364" id="Seg_3708" s="T363">bu͡ollagɨna</ta>
            <ta e="T367" id="Seg_3709" s="T366">balt-ɨ-m</ta>
            <ta e="T368" id="Seg_3710" s="T367">baːr</ta>
            <ta e="T369" id="Seg_3711" s="T368">e-t-e</ta>
            <ta e="T370" id="Seg_3712" s="T369">ol</ta>
            <ta e="T371" id="Seg_3713" s="T370">muŋ-naːk</ta>
            <ta e="T372" id="Seg_3714" s="T371">hɨrga-ttan</ta>
            <ta e="T373" id="Seg_3715" s="T372">tüh-en</ta>
            <ta e="T374" id="Seg_3716" s="T373">baran</ta>
            <ta e="T375" id="Seg_3717" s="T374">nʼuŋuː-nu</ta>
            <ta e="T376" id="Seg_3718" s="T375">kab-an</ta>
            <ta e="T377" id="Seg_3719" s="T376">ɨl-l-a</ta>
            <ta e="T378" id="Seg_3720" s="T377">innʼe</ta>
            <ta e="T381" id="Seg_3721" s="T380">taba-lar</ta>
            <ta e="T382" id="Seg_3722" s="T381">toktoː-tu-lar</ta>
            <ta e="T386" id="Seg_3723" s="T385">bihigi</ta>
            <ta e="T387" id="Seg_3724" s="T386">kel-e-bit</ta>
            <ta e="T389" id="Seg_3725" s="T387">bu͡o</ta>
            <ta e="T390" id="Seg_3726" s="T389">teːte-m</ta>
            <ta e="T391" id="Seg_3727" s="T390">hɨrga-laːk</ta>
            <ta e="T392" id="Seg_3728" s="T391">taba-bɨ-n</ta>
            <ta e="T393" id="Seg_3729" s="T392">baːj-an</ta>
            <ta e="T394" id="Seg_3730" s="T393">keːs-t-e</ta>
            <ta e="T395" id="Seg_3731" s="T394">mini͡en-i-n</ta>
            <ta e="T396" id="Seg_3732" s="T395">innʼe</ta>
            <ta e="T397" id="Seg_3733" s="T396">gɨn-an</ta>
            <ta e="T398" id="Seg_3734" s="T397">bar-an</ta>
            <ta e="T399" id="Seg_3735" s="T398">beje-te</ta>
            <ta e="T400" id="Seg_3736" s="T399">bu͡o</ta>
            <ta e="T401" id="Seg_3737" s="T400">min</ta>
            <ta e="T402" id="Seg_3738" s="T401">hɨrga-laːk</ta>
            <ta e="T403" id="Seg_3739" s="T402">taba-lar</ta>
            <ta e="T404" id="Seg_3740" s="T403">miːn-en</ta>
            <ta e="T405" id="Seg_3741" s="T404">bar-an</ta>
            <ta e="T406" id="Seg_3742" s="T405">ol</ta>
            <ta e="T407" id="Seg_3743" s="T406">dʼi͡e-biti-ger</ta>
            <ta e="T408" id="Seg_3744" s="T407">bar-dɨ-bɨt</ta>
            <ta e="T409" id="Seg_3745" s="T408">bu͡o</ta>
            <ta e="T410" id="Seg_3746" s="T409">dʼi͡e-biti-ger</ta>
            <ta e="T411" id="Seg_3747" s="T410">bar-an</ta>
            <ta e="T412" id="Seg_3748" s="T411">bar-an</ta>
            <ta e="T413" id="Seg_3749" s="T412">bu͡ollagɨnan</ta>
            <ta e="T414" id="Seg_3750" s="T413">kimi͡e-ke</ta>
            <ta e="T415" id="Seg_3751" s="T414">aːm-ma-r</ta>
            <ta e="T416" id="Seg_3752" s="T415">tokto-t-or</ta>
            <ta e="T417" id="Seg_3753" s="T416">bu͡o</ta>
            <ta e="T418" id="Seg_3754" s="T417">bu͡olog-u-m</ta>
            <ta e="T419" id="Seg_3755" s="T418">aːn-ɨ-gar</ta>
            <ta e="T420" id="Seg_3756" s="T419">ke</ta>
            <ta e="T421" id="Seg_3757" s="T420">innʼe</ta>
            <ta e="T422" id="Seg_3758" s="T421">bu͡o</ta>
            <ta e="T423" id="Seg_3759" s="T422">emeːksit-ter</ta>
            <ta e="T424" id="Seg_3760" s="T423">ikki</ta>
            <ta e="T425" id="Seg_3761" s="T424">emeːksin</ta>
            <ta e="T426" id="Seg_3762" s="T425">baːr</ta>
            <ta e="T427" id="Seg_3763" s="T426">e-t-e</ta>
            <ta e="T429" id="Seg_3764" s="T428">o-lor</ta>
            <ta e="T430" id="Seg_3765" s="T429">kel-li-ler</ta>
            <ta e="T431" id="Seg_3766" s="T430">kaja</ta>
            <ta e="T432" id="Seg_3767" s="T431">bu͡o</ta>
            <ta e="T433" id="Seg_3768" s="T432">tu͡ok</ta>
            <ta e="T434" id="Seg_3769" s="T433">bu͡ol-lu-gut</ta>
            <ta e="T435" id="Seg_3770" s="T434">kajdak</ta>
            <ta e="T436" id="Seg_3771" s="T435">kajdak</ta>
            <ta e="T438" id="Seg_3772" s="T436">bu͡ol-lu-gut</ta>
            <ta e="T439" id="Seg_3773" s="T438">araː</ta>
            <ta e="T440" id="Seg_3774" s="T439">bu</ta>
            <ta e="T441" id="Seg_3775" s="T440">mini͡e-ke</ta>
            <ta e="T442" id="Seg_3776" s="T441">kata</ta>
            <ta e="T443" id="Seg_3777" s="T442">bu</ta>
            <ta e="T444" id="Seg_3778" s="T443">dʼaktar</ta>
            <ta e="T445" id="Seg_3779" s="T444">ölör-ü-n-n-e</ta>
            <ta e="T446" id="Seg_3780" s="T445">mu-nu</ta>
            <ta e="T447" id="Seg_3781" s="T446">naːda</ta>
            <ta e="T448" id="Seg_3782" s="T447">du͡oktuːr-da</ta>
            <ta e="T449" id="Seg_3783" s="T448">ɨgɨr-ɨ-n-ɨ͡ak-ka</ta>
            <ta e="T450" id="Seg_3784" s="T449">mu-nu</ta>
            <ta e="T451" id="Seg_3785" s="T450">teːte-m</ta>
            <ta e="T452" id="Seg_3786" s="T451">maːma-m</ta>
            <ta e="T453" id="Seg_3787" s="T452">diː-r</ta>
            <ta e="T454" id="Seg_3788" s="T453">bar-ɨ-ma</ta>
            <ta e="T455" id="Seg_3789" s="T454">bar-ɨ-ma</ta>
            <ta e="T456" id="Seg_3790" s="T455">du͡oktuːr-u</ta>
            <ta e="T457" id="Seg_3791" s="T456">tug-u-n</ta>
            <ta e="T458" id="Seg_3792" s="T457">diː-r</ta>
            <ta e="T459" id="Seg_3793" s="T458">anʼiː</ta>
            <ta e="T460" id="Seg_3794" s="T459">dagɨnɨ</ta>
            <ta e="T461" id="Seg_3795" s="T460">üčügej</ta>
            <ta e="T462" id="Seg_3796" s="T461">köksü-te</ta>
            <ta e="T463" id="Seg_3797" s="T462">ki͡eŋ</ta>
            <ta e="T466" id="Seg_3798" s="T465">üčügej</ta>
            <ta e="T467" id="Seg_3799" s="T466">beːbe</ta>
            <ta e="T468" id="Seg_3800" s="T467">oččogo</ta>
            <ta e="T469" id="Seg_3801" s="T468">minigi-n</ta>
            <ta e="T470" id="Seg_3802" s="T469">ikki</ta>
            <ta e="T471" id="Seg_3803" s="T470">di͡egi-tten</ta>
            <ta e="T472" id="Seg_3804" s="T471">kötög-ön</ta>
            <ta e="T473" id="Seg_3805" s="T472">bar-an</ta>
            <ta e="T474" id="Seg_3806" s="T473">dʼi͡e</ta>
            <ta e="T475" id="Seg_3807" s="T474">bolok-ko</ta>
            <ta e="T476" id="Seg_3808" s="T475">kiːl-ler-di-ler</ta>
            <ta e="T477" id="Seg_3809" s="T476">bu͡o</ta>
            <ta e="T478" id="Seg_3810" s="T477">eː</ta>
            <ta e="T479" id="Seg_3811" s="T478">de</ta>
            <ta e="T480" id="Seg_3812" s="T479">ol</ta>
            <ta e="T481" id="Seg_3813" s="T480">hɨgɨnnʼak-tan-an</ta>
            <ta e="T482" id="Seg_3814" s="T481">kanʼ-an</ta>
            <ta e="T483" id="Seg_3815" s="T482">bar-an</ta>
            <ta e="T484" id="Seg_3816" s="T483">de</ta>
            <ta e="T485" id="Seg_3817" s="T484">tu͡ok</ta>
            <ta e="T486" id="Seg_3818" s="T485">du͡oktuːr-u-n</ta>
            <ta e="T487" id="Seg_3819" s="T486">itte-ti-n</ta>
            <ta e="T488" id="Seg_3820" s="T487">beːbe</ta>
            <ta e="T489" id="Seg_3821" s="T488">üčügej</ta>
            <ta e="T490" id="Seg_3822" s="T489">možet</ta>
            <ta e="T491" id="Seg_3823" s="T490">i</ta>
            <ta e="T492" id="Seg_3824" s="T491">meːne</ta>
            <ta e="T493" id="Seg_3825" s="T492">itte</ta>
            <ta e="T494" id="Seg_3826" s="T493">eni</ta>
            <ta e="T495" id="Seg_3827" s="T494">tu͡og-a</ta>
            <ta e="T496" id="Seg_3828" s="T495">da</ta>
            <ta e="T497" id="Seg_3829" s="T496">vrode</ta>
            <ta e="T498" id="Seg_3830" s="T497">tost-u-batak</ta>
            <ta e="T499" id="Seg_3831" s="T498">kaːn</ta>
            <ta e="T500" id="Seg_3832" s="T499">da</ta>
            <ta e="T501" id="Seg_3833" s="T500">hu͡ok</ta>
            <ta e="T502" id="Seg_3834" s="T501">tu͡ok</ta>
            <ta e="T503" id="Seg_3835" s="T502">da</ta>
            <ta e="T504" id="Seg_3836" s="T503">hu͡ok</ta>
            <ta e="T505" id="Seg_3837" s="T504">üčügej</ta>
            <ta e="T506" id="Seg_3838" s="T505">bu͡ol-u͡o</ta>
            <ta e="T507" id="Seg_3839" s="T506">oččogo</ta>
            <ta e="T508" id="Seg_3840" s="T507">heː</ta>
            <ta e="T509" id="Seg_3841" s="T508">de</ta>
            <ta e="T510" id="Seg_3842" s="T509">vot</ta>
            <ta e="T511" id="Seg_3843" s="T510">on-ton</ta>
            <ta e="T512" id="Seg_3844" s="T511">ol</ta>
            <ta e="T513" id="Seg_3845" s="T512">de</ta>
            <ta e="T514" id="Seg_3846" s="T513">on-ton</ta>
            <ta e="T515" id="Seg_3847" s="T514">on-ton</ta>
            <ta e="T516" id="Seg_3848" s="T515">kojutaːn</ta>
            <ta e="T517" id="Seg_3849" s="T516">kojut</ta>
            <ta e="T518" id="Seg_3850" s="T517">bagajɨ</ta>
            <ta e="T519" id="Seg_3851" s="T518">de</ta>
            <ta e="T520" id="Seg_3852" s="T519">ɨj</ta>
            <ta e="T521" id="Seg_3853" s="T520">aːs-t-a</ta>
            <ta e="T522" id="Seg_3854" s="T521">bu͡o</ta>
            <ta e="T523" id="Seg_3855" s="T522">ɨj</ta>
            <ta e="T524" id="Seg_3856" s="T523">aːh-an</ta>
            <ta e="T525" id="Seg_3857" s="T524">bar-an</ta>
            <ta e="T526" id="Seg_3858" s="T525">ho-non</ta>
            <ta e="T527" id="Seg_3859" s="T526">du͡oktuːr</ta>
            <ta e="T528" id="Seg_3860" s="T527">da</ta>
            <ta e="T529" id="Seg_3861" s="T528">hu͡ok</ta>
            <ta e="T530" id="Seg_3862" s="T529">tu͡ok</ta>
            <ta e="T531" id="Seg_3863" s="T530">da</ta>
            <ta e="T532" id="Seg_3864" s="T531">hu͡ok</ta>
            <ta e="T533" id="Seg_3865" s="T532">ɨj</ta>
            <ta e="T534" id="Seg_3866" s="T533">aːh-an</ta>
            <ta e="T535" id="Seg_3867" s="T534">bar-an</ta>
            <ta e="T536" id="Seg_3868" s="T535">de</ta>
            <ta e="T537" id="Seg_3869" s="T536">ol</ta>
            <ta e="T538" id="Seg_3870" s="T537">haŋa</ta>
            <ta e="T539" id="Seg_3871" s="T538">dʼɨl-ga</ta>
            <ta e="T540" id="Seg_3872" s="T539">kiːr-eːri</ta>
            <ta e="T541" id="Seg_3873" s="T540">de</ta>
            <ta e="T542" id="Seg_3874" s="T541">du͡oktuːr-ga</ta>
            <ta e="T543" id="Seg_3875" s="T542">kiːr-di-bit</ta>
            <ta e="T544" id="Seg_3876" s="T543">bu͡o</ta>
            <ta e="T545" id="Seg_3877" s="T544">onno</ta>
            <ta e="T546" id="Seg_3878" s="T545">kiːr-em-mit</ta>
            <ta e="T547" id="Seg_3879" s="T546">ol</ta>
            <ta e="T548" id="Seg_3880" s="T547">kaja</ta>
            <ta e="T549" id="Seg_3881" s="T548">du͡oktuːr-u-m</ta>
            <ta e="T550" id="Seg_3882" s="T549">diː-r</ta>
            <ta e="T551" id="Seg_3883" s="T550">bu͡o</ta>
            <ta e="T552" id="Seg_3884" s="T551">kaja</ta>
            <ta e="T553" id="Seg_3885" s="T552">eni͡ene</ta>
            <ta e="T554" id="Seg_3886" s="T553">öttüg-ü-ŋ</ta>
            <ta e="T555" id="Seg_3887" s="T554">öttüg-ü-ŋ</ta>
            <ta e="T556" id="Seg_3888" s="T555">tost-u-but</ta>
            <ta e="T557" id="Seg_3889" s="T556">e-bit</ta>
            <ta e="T558" id="Seg_3890" s="T557">enigi-n</ta>
            <ta e="T559" id="Seg_3891" s="T558">naːda</ta>
            <ta e="T560" id="Seg_3892" s="T559">Nosku͡o-ga</ta>
            <ta e="T561" id="Seg_3893" s="T560">ɨːt-ɨ͡ak-ka</ta>
            <ta e="T562" id="Seg_3894" s="T561">vot</ta>
            <ta e="T563" id="Seg_3895" s="T562">onton</ta>
            <ta e="T564" id="Seg_3896" s="T563">Nosku͡o-ga</ta>
            <ta e="T565" id="Seg_3897" s="T564">ɨːp-pɨt-tara</ta>
            <ta e="T566" id="Seg_3898" s="T565">Nosku͡o</ta>
            <ta e="T567" id="Seg_3899" s="T566">min</ta>
            <ta e="T568" id="Seg_3900" s="T567">hɨt-tɨ-m</ta>
            <ta e="T569" id="Seg_3901" s="T568">bu͡o</ta>
            <ta e="T570" id="Seg_3902" s="T569">onno</ta>
            <ta e="T571" id="Seg_3903" s="T570">de</ta>
            <ta e="T572" id="Seg_3904" s="T571">ikki</ta>
            <ta e="T573" id="Seg_3905" s="T572">ɨj-ɨ</ta>
            <ta e="T574" id="Seg_3906" s="T573">vɨčiška-ga</ta>
            <ta e="T575" id="Seg_3907" s="T574">hɨt-tɨ-m</ta>
            <ta e="T576" id="Seg_3908" s="T575">bu͡o</ta>
            <ta e="T577" id="Seg_3909" s="T576">on-ton</ta>
            <ta e="T578" id="Seg_3910" s="T577">bu͡o</ta>
            <ta e="T579" id="Seg_3911" s="T578">aŋar</ta>
            <ta e="T580" id="Seg_3912" s="T579">ɨj-dar-ɨ-n</ta>
            <ta e="T581" id="Seg_3913" s="T580">bu͡o</ta>
            <ta e="T582" id="Seg_3914" s="T581">kör</ta>
            <ta e="T583" id="Seg_3915" s="T582">agɨs</ta>
            <ta e="T584" id="Seg_3916" s="T583">ɨj-ɨ</ta>
            <ta e="T585" id="Seg_3917" s="T584">meldʼi</ta>
            <ta e="T586" id="Seg_3918" s="T585">hɨt-tɨ-m</ta>
            <ta e="T587" id="Seg_3919" s="T586">bu͡o</ta>
            <ta e="T588" id="Seg_3920" s="T587">Nosku͡o-ŋa</ta>
            <ta e="T589" id="Seg_3921" s="T614">dʼi͡e-be-r</ta>
            <ta e="T590" id="Seg_3922" s="T589">kel-bit-i-m</ta>
            <ta e="T591" id="Seg_3923" s="T590">i</ta>
            <ta e="T592" id="Seg_3924" s="T591">atag-ɨ-m</ta>
            <ta e="T598" id="Seg_3925" s="T597">e-t-e</ta>
            <ta e="T599" id="Seg_3926" s="T598">ol</ta>
            <ta e="T600" id="Seg_3927" s="T599">kördük</ta>
            <ta e="T601" id="Seg_3928" s="T600">h-onon</ta>
            <ta e="T602" id="Seg_3929" s="T601">os-put</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_3930" s="T0">ogo</ta>
            <ta e="T2" id="Seg_3931" s="T1">er-TAK-BInA</ta>
            <ta e="T3" id="Seg_3932" s="T2">bu͡ollagɨna</ta>
            <ta e="T4" id="Seg_3933" s="T3">bu͡o</ta>
            <ta e="T5" id="Seg_3934" s="T4">ogo-LAr-I-m</ta>
            <ta e="T6" id="Seg_3935" s="T5">kɨra</ta>
            <ta e="T7" id="Seg_3936" s="T6">e-TI-LArA</ta>
            <ta e="T8" id="Seg_3937" s="T7">biːr</ta>
            <ta e="T9" id="Seg_3938" s="T8">ogo-m</ta>
            <ta e="T10" id="Seg_3939" s="T9">dʼɨl-LAːK</ta>
            <ta e="T11" id="Seg_3940" s="T10">e-TI-tA</ta>
            <ta e="T12" id="Seg_3941" s="T11">dʼɨl-ttAn</ta>
            <ta e="T13" id="Seg_3942" s="T12">orduk-LAːK</ta>
            <ta e="T17" id="Seg_3943" s="T16">biːr</ta>
            <ta e="T18" id="Seg_3944" s="T17">ogo-m</ta>
            <ta e="T19" id="Seg_3945" s="T18">bu͡ollagɨna</ta>
            <ta e="T20" id="Seg_3946" s="T19">agɨs</ta>
            <ta e="T21" id="Seg_3947" s="T20">ɨj-LAːK</ta>
            <ta e="T22" id="Seg_3948" s="T21">e-TI-tA</ta>
            <ta e="T23" id="Seg_3949" s="T22">oččogo</ta>
            <ta e="T24" id="Seg_3950" s="T23">bu͡ollagɨna</ta>
            <ta e="T25" id="Seg_3951" s="T24">bu͡o</ta>
            <ta e="T26" id="Seg_3952" s="T25">teːte-m</ta>
            <ta e="T27" id="Seg_3953" s="T26">mas</ta>
            <ta e="T28" id="Seg_3954" s="T27">egel-A</ta>
            <ta e="T29" id="Seg_3955" s="T28">bar-An-BIn</ta>
            <ta e="T30" id="Seg_3956" s="T29">teːte-m</ta>
            <ta e="T31" id="Seg_3957" s="T30">kɨtta</ta>
            <ta e="T32" id="Seg_3958" s="T31">barɨs-I-BIT</ta>
            <ta e="T33" id="Seg_3959" s="T32">e-TI-m</ta>
            <ta e="T34" id="Seg_3960" s="T33">hetiː-LAːK-BIn</ta>
            <ta e="T35" id="Seg_3961" s="T34">hetiː-LAːK-BIn</ta>
            <ta e="T36" id="Seg_3962" s="T35">hetiː-BA-r</ta>
            <ta e="T37" id="Seg_3963" s="T36">bu͡ollagɨna</ta>
            <ta e="T38" id="Seg_3964" s="T37">balɨs-I-m</ta>
            <ta e="T39" id="Seg_3965" s="T38">olor-Ar</ta>
            <ta e="T40" id="Seg_3966" s="T39">e-TI-tA</ta>
            <ta e="T41" id="Seg_3967" s="T40">bi͡es</ta>
            <ta e="T42" id="Seg_3968" s="T41">dʼɨl-LAːK</ta>
            <ta e="T43" id="Seg_3969" s="T42">balɨs-I-m</ta>
            <ta e="T50" id="Seg_3970" s="T49">dʼaktar</ta>
            <ta e="T51" id="Seg_3971" s="T50">bu͡ollagɨna</ta>
            <ta e="T52" id="Seg_3972" s="T51">bu͡o</ta>
            <ta e="T53" id="Seg_3973" s="T52">bugdi</ta>
            <ta e="T54" id="Seg_3974" s="T53">taba-nI</ta>
            <ta e="T58" id="Seg_3975" s="T57">kim-LAː-IAk-tI-n</ta>
            <ta e="T59" id="Seg_3976" s="T58">ke</ta>
            <ta e="T60" id="Seg_3977" s="T59">kölüj-IAk-tI-n</ta>
            <ta e="T62" id="Seg_3978" s="T60">ke</ta>
            <ta e="T63" id="Seg_3979" s="T62">i</ta>
            <ta e="T64" id="Seg_3980" s="T63">oččogo</ta>
            <ta e="T65" id="Seg_3981" s="T64">bu͡ollagɨna</ta>
            <ta e="T66" id="Seg_3982" s="T65">bu͡o</ta>
            <ta e="T67" id="Seg_3983" s="T66">tɨnnast-I-BIT</ta>
            <ta e="T68" id="Seg_3984" s="T67">taba</ta>
            <ta e="T73" id="Seg_3985" s="T72">tɨnnast-I-BIT</ta>
            <ta e="T74" id="Seg_3986" s="T73">taba</ta>
            <ta e="T75" id="Seg_3987" s="T74">eto</ta>
            <ta e="T76" id="Seg_3988" s="T75">tɨnnast-I-BIT</ta>
            <ta e="T77" id="Seg_3989" s="T76">taba</ta>
            <ta e="T78" id="Seg_3990" s="T77">bu͡ollagɨna</ta>
            <ta e="T79" id="Seg_3991" s="T78">bu͡o</ta>
            <ta e="T80" id="Seg_3992" s="T79">eː</ta>
            <ta e="T81" id="Seg_3993" s="T80">tɨnnast-I-BIT</ta>
            <ta e="T82" id="Seg_3994" s="T81">taba</ta>
            <ta e="T83" id="Seg_3995" s="T82">ɨ͡arɨj-Ar-tI-GAr</ta>
            <ta e="T84" id="Seg_3996" s="T83">bu͡o</ta>
            <ta e="T85" id="Seg_3997" s="T84">er-I-m</ta>
            <ta e="T86" id="Seg_3998" s="T85">inʼe-tA</ta>
            <ta e="T87" id="Seg_3999" s="T86">tɨnnast-AːččI</ta>
            <ta e="T88" id="Seg_4000" s="T87">e-TI-tA</ta>
            <ta e="T89" id="Seg_4001" s="T88">menʼiː-tI-GAr</ta>
            <ta e="T90" id="Seg_4002" s="T89">taba-nI</ta>
            <ta e="T91" id="Seg_4003" s="T90">ka</ta>
            <ta e="T92" id="Seg_4004" s="T91">ol</ta>
            <ta e="T93" id="Seg_4005" s="T92">taba-nI</ta>
            <ta e="T94" id="Seg_4006" s="T93">bu͡olla</ta>
            <ta e="T95" id="Seg_4007" s="T94">dʼaktar</ta>
            <ta e="T96" id="Seg_4008" s="T95">kölüj-IAK-tI-n</ta>
            <ta e="T97" id="Seg_4009" s="T96">nelzja</ta>
            <ta e="T98" id="Seg_4010" s="T97">e-TI-tA</ta>
            <ta e="T99" id="Seg_4011" s="T98">bu͡o</ta>
            <ta e="T100" id="Seg_4012" s="T99">oččogo</ta>
            <ta e="T101" id="Seg_4013" s="T100">min</ta>
            <ta e="T102" id="Seg_4014" s="T101">bil-BAkkA-BIn</ta>
            <ta e="T103" id="Seg_4015" s="T102">bu</ta>
            <ta e="T104" id="Seg_4016" s="T103">taba-nI</ta>
            <ta e="T105" id="Seg_4017" s="T104">kostuːr-LAː-n-BIT-BIn</ta>
            <ta e="T106" id="Seg_4018" s="T105">bu͡o</ta>
            <ta e="T107" id="Seg_4019" s="T106">ol-tI-ŋ</ta>
            <ta e="T108" id="Seg_4020" s="T107">čeːlkeː</ta>
            <ta e="T109" id="Seg_4021" s="T108">taba</ta>
            <ta e="T110" id="Seg_4022" s="T109">buruna</ta>
            <ta e="T111" id="Seg_4023" s="T110">taba</ta>
            <ta e="T112" id="Seg_4024" s="T111">innʼe</ta>
            <ta e="T113" id="Seg_4025" s="T112">bu͡ollagɨna</ta>
            <ta e="T114" id="Seg_4026" s="T113">bar-AːrI-BIT</ta>
            <ta e="T115" id="Seg_4027" s="T114">bu͡ollagɨna</ta>
            <ta e="T116" id="Seg_4028" s="T115">ol</ta>
            <ta e="T117" id="Seg_4029" s="T116">taba</ta>
            <ta e="T118" id="Seg_4030" s="T117">bu͡o</ta>
            <ta e="T119" id="Seg_4031" s="T118">ɨtɨr-t-Ar</ta>
            <ta e="T120" id="Seg_4032" s="T119">üs-TA</ta>
            <ta e="T121" id="Seg_4033" s="T120">raz</ta>
            <ta e="T122" id="Seg_4034" s="T121">ɨtɨr-t-Ar</ta>
            <ta e="T123" id="Seg_4035" s="T122">bu͡o</ta>
            <ta e="T124" id="Seg_4036" s="T123">oččogo</ta>
            <ta e="T125" id="Seg_4037" s="T124">min</ta>
            <ta e="T126" id="Seg_4038" s="T125">di͡e-A-BIn</ta>
            <ta e="T127" id="Seg_4039" s="T126">bu͡o</ta>
            <ta e="T128" id="Seg_4040" s="T127">araː</ta>
            <ta e="T129" id="Seg_4041" s="T128">tu͡ok-GA</ta>
            <ta e="T130" id="Seg_4042" s="T129">ɨtɨr-t-Ar</ta>
            <ta e="T131" id="Seg_4043" s="T130">ka</ta>
            <ta e="T132" id="Seg_4044" s="T131">di͡e-A-BIn</ta>
            <ta e="T135" id="Seg_4045" s="T134">iti</ta>
            <ta e="T136" id="Seg_4046" s="T135">ilin-tI-nAn</ta>
            <ta e="T137" id="Seg_4047" s="T136">kaːmnaː-AːrI-BIt</ta>
            <ta e="T138" id="Seg_4048" s="T137">maːma-m</ta>
            <ta e="T139" id="Seg_4049" s="T138">di͡e-Ar</ta>
            <ta e="T140" id="Seg_4050" s="T139">bu͡o</ta>
            <ta e="T141" id="Seg_4051" s="T140">u͡ot-BIt</ta>
            <ta e="T142" id="Seg_4052" s="T141">haŋar-Ar</ta>
            <ta e="T143" id="Seg_4053" s="T142">di͡e-Ar</ta>
            <ta e="T144" id="Seg_4054" s="T143">u͡ot-BIt</ta>
            <ta e="T145" id="Seg_4055" s="T144">haŋar-Ar</ta>
            <ta e="T148" id="Seg_4056" s="T147">bu͡ollagɨna</ta>
            <ta e="T149" id="Seg_4057" s="T148">tu͡ok</ta>
            <ta e="T150" id="Seg_4058" s="T149">ere</ta>
            <ta e="T151" id="Seg_4059" s="T150">kuhagan</ta>
            <ta e="T152" id="Seg_4060" s="T151">bu͡ol-AːččI</ta>
            <ta e="T153" id="Seg_4061" s="T152">u͡ot</ta>
            <ta e="T154" id="Seg_4062" s="T153">haŋar-TAK-InA</ta>
            <ta e="T155" id="Seg_4063" s="T154">ke</ta>
            <ta e="T157" id="Seg_4064" s="T155">haŋar-TAK-InA</ta>
            <ta e="T158" id="Seg_4065" s="T157">oččogo</ta>
            <ta e="T159" id="Seg_4066" s="T158">bu͡ollagɨna</ta>
            <ta e="T160" id="Seg_4067" s="T159">nu</ta>
            <ta e="T165" id="Seg_4068" s="T164">min</ta>
            <ta e="T166" id="Seg_4069" s="T165">bu͡ollagɨna</ta>
            <ta e="T167" id="Seg_4070" s="T166">olus</ta>
            <ta e="T168" id="Seg_4071" s="T167">daːganɨ</ta>
            <ta e="T169" id="Seg_4072" s="T168">ol-nI</ta>
            <ta e="T170" id="Seg_4073" s="T169">bu͡ollagɨna</ta>
            <ta e="T171" id="Seg_4074" s="T170">itegej-BAkkA-BIn</ta>
            <ta e="T172" id="Seg_4075" s="T171">bu͡ollagɨna</ta>
            <ta e="T173" id="Seg_4076" s="T172">dʼe</ta>
            <ta e="T174" id="Seg_4077" s="T173">taba</ta>
            <ta e="T175" id="Seg_4078" s="T174">tutun-TI-BIt</ta>
            <ta e="T176" id="Seg_4079" s="T175">da</ta>
            <ta e="T177" id="Seg_4080" s="T176">bar-An</ta>
            <ta e="T178" id="Seg_4081" s="T177">kaːl-TI-BIt</ta>
            <ta e="T179" id="Seg_4082" s="T178">bu͡o</ta>
            <ta e="T180" id="Seg_4083" s="T179">ol</ta>
            <ta e="T181" id="Seg_4084" s="T180">bu</ta>
            <ta e="T182" id="Seg_4085" s="T181">taba-nI</ta>
            <ta e="T183" id="Seg_4086" s="T182">kap-BIT-BIt</ta>
            <ta e="T184" id="Seg_4087" s="T183">innʼe</ta>
            <ta e="T185" id="Seg_4088" s="T184">bar-An</ta>
            <ta e="T186" id="Seg_4089" s="T185">kaːl-BIT-BIt</ta>
            <ta e="T187" id="Seg_4090" s="T186">bu͡o</ta>
            <ta e="T188" id="Seg_4091" s="T187">teːte-BI-n</ta>
            <ta e="T189" id="Seg_4092" s="T188">kɨtta</ta>
            <ta e="T190" id="Seg_4093" s="T189">barɨs-An</ta>
            <ta e="T191" id="Seg_4094" s="T190">kaːl-BIT-BIt</ta>
            <ta e="T192" id="Seg_4095" s="T191">barɨs-An</ta>
            <ta e="T193" id="Seg_4096" s="T192">kaːl</ta>
            <ta e="T194" id="Seg_4097" s="T193">huːrt-GA</ta>
            <ta e="T195" id="Seg_4098" s="T194">kel-An</ta>
            <ta e="T196" id="Seg_4099" s="T195">bar-An</ta>
            <ta e="T197" id="Seg_4100" s="T196">bu͡ollagɨna</ta>
            <ta e="T198" id="Seg_4101" s="T197">teːte-m</ta>
            <ta e="T199" id="Seg_4102" s="T198">hɨrga-BItI-n</ta>
            <ta e="T200" id="Seg_4103" s="T199">hetiː-BItI-n</ta>
            <ta e="T201" id="Seg_4104" s="T200">bu͡ollagɨna</ta>
            <ta e="T202" id="Seg_4105" s="T201">ti͡ej-n-TI-BIt</ta>
            <ta e="T203" id="Seg_4106" s="T202">ti͡ej-n-An</ta>
            <ta e="T204" id="Seg_4107" s="T203">ötüː-LAː-n-An</ta>
            <ta e="T205" id="Seg_4108" s="T204">kanʼaː-An</ta>
            <ta e="T206" id="Seg_4109" s="T205">bu͡o</ta>
            <ta e="T207" id="Seg_4110" s="T206">min-n</ta>
            <ta e="T208" id="Seg_4111" s="T207">dʼe</ta>
            <ta e="T209" id="Seg_4112" s="T208">kamnaː-t-TI-tA</ta>
            <ta e="T210" id="Seg_4113" s="T209">bu͡o</ta>
            <ta e="T211" id="Seg_4114" s="T210">hɨrga-LAːK</ta>
            <ta e="T212" id="Seg_4115" s="T211">taba-BI-n</ta>
            <ta e="T213" id="Seg_4116" s="T212">hi͡et-An</ta>
            <ta e="T214" id="Seg_4117" s="T213">bar-An</ta>
            <ta e="T215" id="Seg_4118" s="T214">hɨrga-LAːK</ta>
            <ta e="T216" id="Seg_4119" s="T215">taba-BI-n</ta>
            <ta e="T217" id="Seg_4120" s="T216">hi͡et-An</ta>
            <ta e="T218" id="Seg_4121" s="T217">bar-An-BIn</ta>
            <ta e="T219" id="Seg_4122" s="T218">bu͡o</ta>
            <ta e="T220" id="Seg_4123" s="T219">hɨːha</ta>
            <ta e="T221" id="Seg_4124" s="T220">olor-A-BIn</ta>
            <ta e="T222" id="Seg_4125" s="T221">bu͡o</ta>
            <ta e="T223" id="Seg_4126" s="T222">hɨrga-BA-r</ta>
            <ta e="T224" id="Seg_4127" s="T223">bu͡o</ta>
            <ta e="T225" id="Seg_4128" s="T224">balɨk-LAr-nI</ta>
            <ta e="T226" id="Seg_4129" s="T225">ti͡ej-BIT-BIn</ta>
            <ta e="T227" id="Seg_4130" s="T226">alta</ta>
            <ta e="T228" id="Seg_4131" s="T227">küːl</ta>
            <ta e="T229" id="Seg_4132" s="T228">balɨk</ta>
            <ta e="T230" id="Seg_4133" s="T229">alta</ta>
            <ta e="T231" id="Seg_4134" s="T230">küːl</ta>
            <ta e="T232" id="Seg_4135" s="T231">balɨk</ta>
            <ta e="T233" id="Seg_4136" s="T232">ti͡ej-An</ta>
            <ta e="T234" id="Seg_4137" s="T233">bar-An</ta>
            <ta e="T235" id="Seg_4138" s="T234">bu͡ollagɨna</ta>
            <ta e="T236" id="Seg_4139" s="T235">hɨrga-LAr</ta>
            <ta e="T237" id="Seg_4140" s="T236">ɨstan-A-BIn</ta>
            <ta e="T238" id="Seg_4141" s="T237">di͡e-A-BIn</ta>
            <ta e="T239" id="Seg_4142" s="T238">bu͡o</ta>
            <ta e="T240" id="Seg_4143" s="T239">hɨrga-BI-n</ta>
            <ta e="T241" id="Seg_4144" s="T240">hɨːha</ta>
            <ta e="T242" id="Seg_4145" s="T241">olor-A-BIn</ta>
            <ta e="T243" id="Seg_4146" s="T242">bu͡o</ta>
            <ta e="T244" id="Seg_4147" s="T243">hu͡ok</ta>
            <ta e="T245" id="Seg_4148" s="T244">bu</ta>
            <ta e="T246" id="Seg_4149" s="T245">kihi</ta>
            <ta e="T247" id="Seg_4150" s="T246">bu͡o</ta>
            <ta e="T248" id="Seg_4151" s="T247">össü͡ö</ta>
            <ta e="T249" id="Seg_4152" s="T248">da</ta>
            <ta e="T250" id="Seg_4153" s="T249">ɨstan-A-BIn</ta>
            <ta e="T251" id="Seg_4154" s="T250">bu͡o</ta>
            <ta e="T253" id="Seg_4155" s="T252">ɨstan-A-BIn</ta>
            <ta e="T255" id="Seg_4156" s="T253">bu͡o</ta>
            <ta e="T256" id="Seg_4157" s="T255">emi͡e</ta>
            <ta e="T257" id="Seg_4158" s="T256">hɨːha</ta>
            <ta e="T258" id="Seg_4159" s="T257">olor-A-BIn</ta>
            <ta e="T259" id="Seg_4160" s="T258">bu͡o</ta>
            <ta e="T260" id="Seg_4161" s="T259">hɨrga-LAːK</ta>
            <ta e="T261" id="Seg_4162" s="T260">taba-BI-n</ta>
            <ta e="T262" id="Seg_4163" s="T261">ɨhɨk-t-An</ta>
            <ta e="T263" id="Seg_4164" s="T262">keːs-TI-m</ta>
            <ta e="T264" id="Seg_4165" s="T263">bu͡o</ta>
            <ta e="T265" id="Seg_4166" s="T264">innʼe</ta>
            <ta e="T266" id="Seg_4167" s="T265">hɨrga-LAːK</ta>
            <ta e="T267" id="Seg_4168" s="T266">taba-m</ta>
            <ta e="T268" id="Seg_4169" s="T267">bu͡ollagɨna</ta>
            <ta e="T269" id="Seg_4170" s="T268">hetiː-m</ta>
            <ta e="T270" id="Seg_4171" s="T269">taba-LAr-tA</ta>
            <ta e="T271" id="Seg_4172" s="T270">ürüt-BI-nAn</ta>
            <ta e="T272" id="Seg_4173" s="T271">bar-Ar-LAr</ta>
            <ta e="T273" id="Seg_4174" s="T272">bu͡o</ta>
            <ta e="T274" id="Seg_4175" s="T273">i</ta>
            <ta e="T275" id="Seg_4176" s="T274">beje-m</ta>
            <ta e="T276" id="Seg_4177" s="T275">bu͡ollagɨna</ta>
            <ta e="T277" id="Seg_4178" s="T276">bu͡o</ta>
            <ta e="T278" id="Seg_4179" s="T277">hetiː</ta>
            <ta e="T279" id="Seg_4180" s="T278">ɨlak-tI-n</ta>
            <ta e="T280" id="Seg_4181" s="T279">aːjɨ-nAn</ta>
            <ta e="T281" id="Seg_4182" s="T280">üs-TA</ta>
            <ta e="T282" id="Seg_4183" s="T281">raz</ta>
            <ta e="T283" id="Seg_4184" s="T282">ergij-TI-m</ta>
            <ta e="T284" id="Seg_4185" s="T283">bu͡o</ta>
            <ta e="T285" id="Seg_4186" s="T284">araː</ta>
            <ta e="T286" id="Seg_4187" s="T285">innʼe</ta>
            <ta e="T287" id="Seg_4188" s="T286">bu͡o</ta>
            <ta e="T288" id="Seg_4189" s="T287">ittenne</ta>
            <ta e="T289" id="Seg_4190" s="T288">tüs-An</ta>
            <ta e="T290" id="Seg_4191" s="T289">kaːl</ta>
            <ta e="T291" id="Seg_4192" s="T290">ittenne</ta>
            <ta e="T292" id="Seg_4193" s="T291">tüs-An</ta>
            <ta e="T293" id="Seg_4194" s="T292">bar-An</ta>
            <ta e="T294" id="Seg_4195" s="T293">bu͡o</ta>
            <ta e="T295" id="Seg_4196" s="T294">hɨt-A-BIn</ta>
            <ta e="T296" id="Seg_4197" s="T295">bu͡o</ta>
            <ta e="T297" id="Seg_4198" s="T296">teːte-m</ta>
            <ta e="T298" id="Seg_4199" s="T297">hokuj</ta>
            <ta e="T299" id="Seg_4200" s="T298">ket-TI-tA</ta>
            <ta e="T300" id="Seg_4201" s="T299">tur-Ar</ta>
            <ta e="T301" id="Seg_4202" s="T300">e-TI-tA</ta>
            <ta e="T302" id="Seg_4203" s="T301">bu͡o</ta>
            <ta e="T303" id="Seg_4204" s="T302">hokuj</ta>
            <ta e="T304" id="Seg_4205" s="T303">ket-An</ta>
            <ta e="T305" id="Seg_4206" s="T304">bar-An</ta>
            <ta e="T306" id="Seg_4207" s="T305">bu-kAːN</ta>
            <ta e="T307" id="Seg_4208" s="T306">kör-BIT-tA</ta>
            <ta e="T308" id="Seg_4209" s="T307">kör-Ar</ta>
            <ta e="T309" id="Seg_4210" s="T308">hɨrga-LAːK</ta>
            <ta e="T310" id="Seg_4211" s="T309">taba</ta>
            <ta e="T311" id="Seg_4212" s="T310">bar-A</ta>
            <ta e="T312" id="Seg_4213" s="T311">tur-Ar</ta>
            <ta e="T313" id="Seg_4214" s="T312">kihi-tA</ta>
            <ta e="T314" id="Seg_4215" s="T313">bu͡o</ta>
            <ta e="T315" id="Seg_4216" s="T314">manna</ta>
            <ta e="T316" id="Seg_4217" s="T315">hɨt-Ar</ta>
            <ta e="T317" id="Seg_4218" s="T316">hu͡ol-GA</ta>
            <ta e="T318" id="Seg_4219" s="T317">dʼe</ta>
            <ta e="T319" id="Seg_4220" s="T318">hüːr-An</ta>
            <ta e="T320" id="Seg_4221" s="T319">kel-TI-tA</ta>
            <ta e="T321" id="Seg_4222" s="T320">bu͡o</ta>
            <ta e="T322" id="Seg_4223" s="T321">innʼe</ta>
            <ta e="T323" id="Seg_4224" s="T322">di͡e-TI-tA</ta>
            <ta e="T324" id="Seg_4225" s="T323">kaja</ta>
            <ta e="T325" id="Seg_4226" s="T324">en</ta>
            <ta e="T326" id="Seg_4227" s="T325">tɨːn-LAːK-GIn</ta>
            <ta e="T327" id="Seg_4228" s="T326">du͡o</ta>
            <ta e="T328" id="Seg_4229" s="T327">di͡e-TI-tA</ta>
            <ta e="T329" id="Seg_4230" s="T328">eː</ta>
            <ta e="T330" id="Seg_4231" s="T329">tɨːn-LAːK-BIn</ta>
            <ta e="T331" id="Seg_4232" s="T330">tɨːn-LAːK-BIn</ta>
            <ta e="T332" id="Seg_4233" s="T331">atak-I-m</ta>
            <ta e="T333" id="Seg_4234" s="T332">ere</ta>
            <ta e="T334" id="Seg_4235" s="T333">kɨ͡aj-An</ta>
            <ta e="T335" id="Seg_4236" s="T334">kamnaː-BAT</ta>
            <ta e="T336" id="Seg_4237" s="T335">eː</ta>
            <ta e="T337" id="Seg_4238" s="T336">dʼe</ta>
            <ta e="T338" id="Seg_4239" s="T337">beːbe</ta>
            <ta e="T339" id="Seg_4240" s="T338">min</ta>
            <ta e="T340" id="Seg_4241" s="T339">kel-IAK-m</ta>
            <ta e="T341" id="Seg_4242" s="T340">dʼe</ta>
            <ta e="T342" id="Seg_4243" s="T341">kel-An</ta>
            <ta e="T343" id="Seg_4244" s="T342">bar-An</ta>
            <ta e="T344" id="Seg_4245" s="T343">hɨrga-LAːK</ta>
            <ta e="T345" id="Seg_4246" s="T344">taba-tI-GAr</ta>
            <ta e="T346" id="Seg_4247" s="T345">kajdak</ta>
            <ta e="T347" id="Seg_4248" s="T346">ere</ta>
            <ta e="T348" id="Seg_4249" s="T347">miːn-An</ta>
            <ta e="T349" id="Seg_4250" s="T348">bar-An</ta>
            <ta e="T350" id="Seg_4251" s="T349">bu͡o</ta>
            <ta e="T351" id="Seg_4252" s="T350">min</ta>
            <ta e="T352" id="Seg_4253" s="T351">hɨrga-LAːK</ta>
            <ta e="T353" id="Seg_4254" s="T352">taba-BI-n</ta>
            <ta e="T354" id="Seg_4255" s="T353">hit-A</ta>
            <ta e="T355" id="Seg_4256" s="T354">bar-A-BIt</ta>
            <ta e="T356" id="Seg_4257" s="T355">bu͡o</ta>
            <ta e="T357" id="Seg_4258" s="T356">onno</ta>
            <ta e="T358" id="Seg_4259" s="T357">bu͡o</ta>
            <ta e="T359" id="Seg_4260" s="T358">kü͡öl-nI</ta>
            <ta e="T360" id="Seg_4261" s="T359">ɨtɨn-AːrI-BIT</ta>
            <ta e="T361" id="Seg_4262" s="T360">eni͡e-GA</ta>
            <ta e="T362" id="Seg_4263" s="T361">eni͡e</ta>
            <ta e="T363" id="Seg_4264" s="T362">üstün</ta>
            <ta e="T364" id="Seg_4265" s="T363">bu͡ollagɨna</ta>
            <ta e="T367" id="Seg_4266" s="T366">balɨs-tI-m</ta>
            <ta e="T368" id="Seg_4267" s="T367">baːr</ta>
            <ta e="T369" id="Seg_4268" s="T368">e-TI-tA</ta>
            <ta e="T370" id="Seg_4269" s="T369">ol</ta>
            <ta e="T371" id="Seg_4270" s="T370">muŋ-LAːK</ta>
            <ta e="T372" id="Seg_4271" s="T371">hɨrga-ttAn</ta>
            <ta e="T373" id="Seg_4272" s="T372">tüs-An</ta>
            <ta e="T374" id="Seg_4273" s="T373">baran</ta>
            <ta e="T375" id="Seg_4274" s="T374">nʼu͡oguː-nI</ta>
            <ta e="T376" id="Seg_4275" s="T375">kap-An</ta>
            <ta e="T377" id="Seg_4276" s="T376">ɨl-TI-tA</ta>
            <ta e="T378" id="Seg_4277" s="T377">innʼe</ta>
            <ta e="T381" id="Seg_4278" s="T380">taba-LAr</ta>
            <ta e="T382" id="Seg_4279" s="T381">toktoː-TI-LAr</ta>
            <ta e="T386" id="Seg_4280" s="T385">bihigi</ta>
            <ta e="T387" id="Seg_4281" s="T386">kel-A-BIt</ta>
            <ta e="T389" id="Seg_4282" s="T387">bu͡o</ta>
            <ta e="T390" id="Seg_4283" s="T389">teːte-m</ta>
            <ta e="T391" id="Seg_4284" s="T390">hɨrga-LAːK</ta>
            <ta e="T392" id="Seg_4285" s="T391">taba-BI-n</ta>
            <ta e="T393" id="Seg_4286" s="T392">baːj-An</ta>
            <ta e="T394" id="Seg_4287" s="T393">keːs-TI-tA</ta>
            <ta e="T395" id="Seg_4288" s="T394">mini͡ene-tI-n</ta>
            <ta e="T396" id="Seg_4289" s="T395">innʼe</ta>
            <ta e="T397" id="Seg_4290" s="T396">gɨn-An</ta>
            <ta e="T398" id="Seg_4291" s="T397">bar-An</ta>
            <ta e="T399" id="Seg_4292" s="T398">beje-tA</ta>
            <ta e="T400" id="Seg_4293" s="T399">bu͡o</ta>
            <ta e="T401" id="Seg_4294" s="T400">min</ta>
            <ta e="T402" id="Seg_4295" s="T401">hɨrga-LAːK</ta>
            <ta e="T403" id="Seg_4296" s="T402">taba-LAr</ta>
            <ta e="T404" id="Seg_4297" s="T403">miːn-An</ta>
            <ta e="T405" id="Seg_4298" s="T404">bar-An</ta>
            <ta e="T406" id="Seg_4299" s="T405">ol</ta>
            <ta e="T407" id="Seg_4300" s="T406">dʼi͡e-BItI-GAr</ta>
            <ta e="T408" id="Seg_4301" s="T407">bar-TI-BIt</ta>
            <ta e="T409" id="Seg_4302" s="T408">bu͡o</ta>
            <ta e="T410" id="Seg_4303" s="T409">dʼi͡e-BItI-GAr</ta>
            <ta e="T411" id="Seg_4304" s="T410">bar-An</ta>
            <ta e="T412" id="Seg_4305" s="T411">bar-An</ta>
            <ta e="T413" id="Seg_4306" s="T412">bu͡ollagɨna</ta>
            <ta e="T414" id="Seg_4307" s="T413">kim-GA</ta>
            <ta e="T415" id="Seg_4308" s="T414">aːn-BA-r</ta>
            <ta e="T416" id="Seg_4309" s="T415">toktoː-t-Ar</ta>
            <ta e="T417" id="Seg_4310" s="T416">bu͡o</ta>
            <ta e="T418" id="Seg_4311" s="T417">balok-I-m</ta>
            <ta e="T419" id="Seg_4312" s="T418">aːn-tI-GAr</ta>
            <ta e="T420" id="Seg_4313" s="T419">ka</ta>
            <ta e="T421" id="Seg_4314" s="T420">innʼe</ta>
            <ta e="T422" id="Seg_4315" s="T421">bu͡o</ta>
            <ta e="T423" id="Seg_4316" s="T422">emeːksin-LAr</ta>
            <ta e="T424" id="Seg_4317" s="T423">ikki</ta>
            <ta e="T425" id="Seg_4318" s="T424">emeːksin</ta>
            <ta e="T426" id="Seg_4319" s="T425">baːr</ta>
            <ta e="T427" id="Seg_4320" s="T426">e-TI-tA</ta>
            <ta e="T429" id="Seg_4321" s="T428">ol-LAr</ta>
            <ta e="T430" id="Seg_4322" s="T429">kel-TI-LAr</ta>
            <ta e="T431" id="Seg_4323" s="T430">kaja</ta>
            <ta e="T432" id="Seg_4324" s="T431">bu͡o</ta>
            <ta e="T433" id="Seg_4325" s="T432">tu͡ok</ta>
            <ta e="T434" id="Seg_4326" s="T433">bu͡ol-TI-GIt</ta>
            <ta e="T435" id="Seg_4327" s="T434">kajdak</ta>
            <ta e="T436" id="Seg_4328" s="T435">kajdak</ta>
            <ta e="T438" id="Seg_4329" s="T436">bu͡ol-TI-GIt</ta>
            <ta e="T439" id="Seg_4330" s="T438">araː</ta>
            <ta e="T440" id="Seg_4331" s="T439">bu</ta>
            <ta e="T441" id="Seg_4332" s="T440">min-GA</ta>
            <ta e="T442" id="Seg_4333" s="T441">kata</ta>
            <ta e="T443" id="Seg_4334" s="T442">bu</ta>
            <ta e="T444" id="Seg_4335" s="T443">dʼaktar</ta>
            <ta e="T445" id="Seg_4336" s="T444">ölör-I-n-TI-tA</ta>
            <ta e="T446" id="Seg_4337" s="T445">bu-nI</ta>
            <ta e="T447" id="Seg_4338" s="T446">naːda</ta>
            <ta e="T448" id="Seg_4339" s="T447">du͡oktuːr-TA</ta>
            <ta e="T449" id="Seg_4340" s="T448">ɨgɨr-I-n-IAK-GA</ta>
            <ta e="T450" id="Seg_4341" s="T449">bu-nI</ta>
            <ta e="T451" id="Seg_4342" s="T450">teːte-m</ta>
            <ta e="T452" id="Seg_4343" s="T451">maːma-m</ta>
            <ta e="T453" id="Seg_4344" s="T452">di͡e-Ar</ta>
            <ta e="T454" id="Seg_4345" s="T453">bar-I-BA</ta>
            <ta e="T455" id="Seg_4346" s="T454">bar-I-m</ta>
            <ta e="T456" id="Seg_4347" s="T455">du͡oktuːr-nI</ta>
            <ta e="T457" id="Seg_4348" s="T456">tu͡ok-I-n</ta>
            <ta e="T458" id="Seg_4349" s="T457">di͡e-Ar</ta>
            <ta e="T459" id="Seg_4350" s="T458">anʼɨː</ta>
            <ta e="T460" id="Seg_4351" s="T459">daːganɨ</ta>
            <ta e="T461" id="Seg_4352" s="T460">üčügej</ta>
            <ta e="T462" id="Seg_4353" s="T461">köksü-tA</ta>
            <ta e="T463" id="Seg_4354" s="T462">ki͡eŋ</ta>
            <ta e="T466" id="Seg_4355" s="T465">üčügej</ta>
            <ta e="T467" id="Seg_4356" s="T466">beːbe</ta>
            <ta e="T468" id="Seg_4357" s="T467">oččogo</ta>
            <ta e="T469" id="Seg_4358" s="T468">min-n</ta>
            <ta e="T470" id="Seg_4359" s="T469">ikki</ta>
            <ta e="T471" id="Seg_4360" s="T470">di͡egi-ttAn</ta>
            <ta e="T472" id="Seg_4361" s="T471">kötök-An</ta>
            <ta e="T473" id="Seg_4362" s="T472">bar-An</ta>
            <ta e="T474" id="Seg_4363" s="T473">dʼi͡e</ta>
            <ta e="T475" id="Seg_4364" s="T474">balok-GA</ta>
            <ta e="T476" id="Seg_4365" s="T475">kiːr-TAr-TI-LAr</ta>
            <ta e="T477" id="Seg_4366" s="T476">bu͡o</ta>
            <ta e="T478" id="Seg_4367" s="T477">eː</ta>
            <ta e="T479" id="Seg_4368" s="T478">dʼe</ta>
            <ta e="T480" id="Seg_4369" s="T479">ol</ta>
            <ta e="T481" id="Seg_4370" s="T480">hɨgɨnʼak-LAN-An</ta>
            <ta e="T482" id="Seg_4371" s="T481">kanʼaː-An</ta>
            <ta e="T483" id="Seg_4372" s="T482">bar-An</ta>
            <ta e="T484" id="Seg_4373" s="T483">dʼe</ta>
            <ta e="T485" id="Seg_4374" s="T484">tu͡ok</ta>
            <ta e="T486" id="Seg_4375" s="T485">du͡oktuːr-tI-n</ta>
            <ta e="T487" id="Seg_4376" s="T486">itte-tI-n</ta>
            <ta e="T488" id="Seg_4377" s="T487">beːbe</ta>
            <ta e="T489" id="Seg_4378" s="T488">üčügej</ta>
            <ta e="T490" id="Seg_4379" s="T489">mozet</ta>
            <ta e="T491" id="Seg_4380" s="T490">i</ta>
            <ta e="T492" id="Seg_4381" s="T491">meːne</ta>
            <ta e="T493" id="Seg_4382" s="T492">itte</ta>
            <ta e="T494" id="Seg_4383" s="T493">eni</ta>
            <ta e="T495" id="Seg_4384" s="T494">tu͡ok-tA</ta>
            <ta e="T496" id="Seg_4385" s="T495">da</ta>
            <ta e="T497" id="Seg_4386" s="T496">vrodʼe</ta>
            <ta e="T498" id="Seg_4387" s="T497">tohut-I-BAtAK</ta>
            <ta e="T499" id="Seg_4388" s="T498">kaːn</ta>
            <ta e="T500" id="Seg_4389" s="T499">da</ta>
            <ta e="T501" id="Seg_4390" s="T500">hu͡ok</ta>
            <ta e="T502" id="Seg_4391" s="T501">tu͡ok</ta>
            <ta e="T503" id="Seg_4392" s="T502">da</ta>
            <ta e="T504" id="Seg_4393" s="T503">hu͡ok</ta>
            <ta e="T505" id="Seg_4394" s="T504">üčügej</ta>
            <ta e="T506" id="Seg_4395" s="T505">bu͡ol-IAK.[tA]</ta>
            <ta e="T507" id="Seg_4396" s="T506">oččogo</ta>
            <ta e="T508" id="Seg_4397" s="T507">eː</ta>
            <ta e="T509" id="Seg_4398" s="T508">dʼe</ta>
            <ta e="T510" id="Seg_4399" s="T509">vot</ta>
            <ta e="T511" id="Seg_4400" s="T510">ol-ttAn</ta>
            <ta e="T512" id="Seg_4401" s="T511">ol</ta>
            <ta e="T513" id="Seg_4402" s="T512">dʼe</ta>
            <ta e="T514" id="Seg_4403" s="T513">ol-ttAn</ta>
            <ta e="T515" id="Seg_4404" s="T514">ol-ttAn</ta>
            <ta e="T516" id="Seg_4405" s="T515">kojutaːn</ta>
            <ta e="T517" id="Seg_4406" s="T516">kojut</ta>
            <ta e="T518" id="Seg_4407" s="T517">bagajɨ</ta>
            <ta e="T519" id="Seg_4408" s="T518">dʼe</ta>
            <ta e="T520" id="Seg_4409" s="T519">ɨj</ta>
            <ta e="T521" id="Seg_4410" s="T520">aːs-TI-tA</ta>
            <ta e="T522" id="Seg_4411" s="T521">bu͡o</ta>
            <ta e="T523" id="Seg_4412" s="T522">ɨj</ta>
            <ta e="T524" id="Seg_4413" s="T523">aːs-An</ta>
            <ta e="T525" id="Seg_4414" s="T524">bar-An</ta>
            <ta e="T526" id="Seg_4415" s="T525">hol-nAn</ta>
            <ta e="T527" id="Seg_4416" s="T526">du͡oktuːr</ta>
            <ta e="T528" id="Seg_4417" s="T527">da</ta>
            <ta e="T529" id="Seg_4418" s="T528">hu͡ok</ta>
            <ta e="T530" id="Seg_4419" s="T529">tu͡ok</ta>
            <ta e="T531" id="Seg_4420" s="T530">da</ta>
            <ta e="T532" id="Seg_4421" s="T531">hu͡ok</ta>
            <ta e="T533" id="Seg_4422" s="T532">ɨj</ta>
            <ta e="T534" id="Seg_4423" s="T533">aːs-An</ta>
            <ta e="T535" id="Seg_4424" s="T534">bar-An</ta>
            <ta e="T536" id="Seg_4425" s="T535">dʼe</ta>
            <ta e="T537" id="Seg_4426" s="T536">ol</ta>
            <ta e="T538" id="Seg_4427" s="T537">haŋa</ta>
            <ta e="T539" id="Seg_4428" s="T538">dʼɨl-GA</ta>
            <ta e="T540" id="Seg_4429" s="T539">kiːr-AːrI</ta>
            <ta e="T541" id="Seg_4430" s="T540">dʼe</ta>
            <ta e="T542" id="Seg_4431" s="T541">du͡oktuːr-GA</ta>
            <ta e="T543" id="Seg_4432" s="T542">kiːr-TI-BIt</ta>
            <ta e="T544" id="Seg_4433" s="T543">bu͡o</ta>
            <ta e="T545" id="Seg_4434" s="T544">onno</ta>
            <ta e="T546" id="Seg_4435" s="T545">kiːr-An-BIt</ta>
            <ta e="T547" id="Seg_4436" s="T546">ol</ta>
            <ta e="T548" id="Seg_4437" s="T547">kaja</ta>
            <ta e="T549" id="Seg_4438" s="T548">du͡oktuːr-I-m</ta>
            <ta e="T550" id="Seg_4439" s="T549">di͡e-Ar</ta>
            <ta e="T551" id="Seg_4440" s="T550">bu͡o</ta>
            <ta e="T552" id="Seg_4441" s="T551">kaja</ta>
            <ta e="T553" id="Seg_4442" s="T552">eni͡ene</ta>
            <ta e="T554" id="Seg_4443" s="T553">öttük-I-ŋ</ta>
            <ta e="T555" id="Seg_4444" s="T554">öttük-I-ŋ</ta>
            <ta e="T556" id="Seg_4445" s="T555">tohut-I-BIT</ta>
            <ta e="T557" id="Seg_4446" s="T556">e-BIT</ta>
            <ta e="T558" id="Seg_4447" s="T557">en-n</ta>
            <ta e="T559" id="Seg_4448" s="T558">naːda</ta>
            <ta e="T560" id="Seg_4449" s="T559">Nosku͡o-GA</ta>
            <ta e="T561" id="Seg_4450" s="T560">ɨːt-IAK-GA</ta>
            <ta e="T562" id="Seg_4451" s="T561">vot</ta>
            <ta e="T563" id="Seg_4452" s="T562">onton</ta>
            <ta e="T564" id="Seg_4453" s="T563">Nosku͡o-GA</ta>
            <ta e="T565" id="Seg_4454" s="T564">ɨːt-BIT-LArA</ta>
            <ta e="T566" id="Seg_4455" s="T565">Nosku͡o</ta>
            <ta e="T567" id="Seg_4456" s="T566">min</ta>
            <ta e="T568" id="Seg_4457" s="T567">hɨt-TI-m</ta>
            <ta e="T569" id="Seg_4458" s="T568">bu͡o</ta>
            <ta e="T570" id="Seg_4459" s="T569">onno</ta>
            <ta e="T571" id="Seg_4460" s="T570">dʼe</ta>
            <ta e="T572" id="Seg_4461" s="T571">ikki</ta>
            <ta e="T573" id="Seg_4462" s="T572">ɨj-nI</ta>
            <ta e="T574" id="Seg_4463" s="T573">vɨčiška-GA</ta>
            <ta e="T575" id="Seg_4464" s="T574">hɨt-TI-m</ta>
            <ta e="T576" id="Seg_4465" s="T575">bu͡o</ta>
            <ta e="T577" id="Seg_4466" s="T576">ol-ttAn</ta>
            <ta e="T578" id="Seg_4467" s="T577">bu͡o</ta>
            <ta e="T579" id="Seg_4468" s="T578">aŋar</ta>
            <ta e="T580" id="Seg_4469" s="T579">ɨj-LAr-tI-n</ta>
            <ta e="T581" id="Seg_4470" s="T580">bu͡o</ta>
            <ta e="T582" id="Seg_4471" s="T581">kör</ta>
            <ta e="T583" id="Seg_4472" s="T582">agɨs</ta>
            <ta e="T584" id="Seg_4473" s="T583">ɨj-nI</ta>
            <ta e="T585" id="Seg_4474" s="T584">meldʼi</ta>
            <ta e="T586" id="Seg_4475" s="T585">hɨt-TI-m</ta>
            <ta e="T587" id="Seg_4476" s="T586">bu͡o</ta>
            <ta e="T588" id="Seg_4477" s="T587">Nosku͡o-GA</ta>
            <ta e="T589" id="Seg_4478" s="T614">dʼi͡e-BA-r</ta>
            <ta e="T590" id="Seg_4479" s="T589">kel-BIT-I-m</ta>
            <ta e="T591" id="Seg_4480" s="T590">i</ta>
            <ta e="T592" id="Seg_4481" s="T591">atak-I-m</ta>
            <ta e="T598" id="Seg_4482" s="T597">e-TI-tA</ta>
            <ta e="T599" id="Seg_4483" s="T598">ol</ta>
            <ta e="T600" id="Seg_4484" s="T599">kördük</ta>
            <ta e="T601" id="Seg_4485" s="T600">h-onon</ta>
            <ta e="T602" id="Seg_4486" s="T601">os-BIT</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_4487" s="T0">child.[NOM]</ta>
            <ta e="T2" id="Seg_4488" s="T1">be-TEMP-1SG</ta>
            <ta e="T3" id="Seg_4489" s="T2">though</ta>
            <ta e="T4" id="Seg_4490" s="T3">EMPH</ta>
            <ta e="T5" id="Seg_4491" s="T4">child-PL-EP-1SG.[NOM]</ta>
            <ta e="T6" id="Seg_4492" s="T5">small.[NOM]</ta>
            <ta e="T7" id="Seg_4493" s="T6">be-PST1-3PL</ta>
            <ta e="T8" id="Seg_4494" s="T7">one</ta>
            <ta e="T9" id="Seg_4495" s="T8">child-1SG.[NOM]</ta>
            <ta e="T10" id="Seg_4496" s="T9">year-PROPR.[NOM]</ta>
            <ta e="T11" id="Seg_4497" s="T10">be-PST1-3SG</ta>
            <ta e="T12" id="Seg_4498" s="T11">year-ABL</ta>
            <ta e="T13" id="Seg_4499" s="T12">rest-PROPR</ta>
            <ta e="T17" id="Seg_4500" s="T16">one</ta>
            <ta e="T18" id="Seg_4501" s="T17">child-1SG.[NOM]</ta>
            <ta e="T19" id="Seg_4502" s="T18">though</ta>
            <ta e="T20" id="Seg_4503" s="T19">eight</ta>
            <ta e="T21" id="Seg_4504" s="T20">month-PROPR.[NOM]</ta>
            <ta e="T22" id="Seg_4505" s="T21">be-PST1-3SG</ta>
            <ta e="T23" id="Seg_4506" s="T22">then</ta>
            <ta e="T24" id="Seg_4507" s="T23">though</ta>
            <ta e="T25" id="Seg_4508" s="T24">EMPH</ta>
            <ta e="T26" id="Seg_4509" s="T25">father-1SG.[NOM]</ta>
            <ta e="T27" id="Seg_4510" s="T26">wood.[NOM]</ta>
            <ta e="T28" id="Seg_4511" s="T27">bring-CVB.SIM</ta>
            <ta e="T29" id="Seg_4512" s="T28">go-CVB.SEQ-1SG</ta>
            <ta e="T30" id="Seg_4513" s="T29">father-1SG.[NOM]</ta>
            <ta e="T31" id="Seg_4514" s="T30">with</ta>
            <ta e="T32" id="Seg_4515" s="T31">come.along-EP-PTCP.PST</ta>
            <ta e="T33" id="Seg_4516" s="T32">be-PST1-1SG</ta>
            <ta e="T34" id="Seg_4517" s="T33">sledge-PROPR-1SG</ta>
            <ta e="T35" id="Seg_4518" s="T34">sledge-PROPR-1SG</ta>
            <ta e="T36" id="Seg_4519" s="T35">sledge-1SG-DAT/LOC</ta>
            <ta e="T37" id="Seg_4520" s="T36">though</ta>
            <ta e="T38" id="Seg_4521" s="T37">younger.brother-EP-1SG.[NOM]</ta>
            <ta e="T39" id="Seg_4522" s="T38">sit-PTCP.PRS</ta>
            <ta e="T40" id="Seg_4523" s="T39">be-PST1-3SG</ta>
            <ta e="T41" id="Seg_4524" s="T40">five</ta>
            <ta e="T42" id="Seg_4525" s="T41">year-PROPR</ta>
            <ta e="T43" id="Seg_4526" s="T42">younger.brother-EP-1SG.[NOM]</ta>
            <ta e="T50" id="Seg_4527" s="T49">woman.[NOM]</ta>
            <ta e="T51" id="Seg_4528" s="T50">though</ta>
            <ta e="T52" id="Seg_4529" s="T51">EMPH</ta>
            <ta e="T53" id="Seg_4530" s="T52">spotted</ta>
            <ta e="T54" id="Seg_4531" s="T53">reindeer-ACC</ta>
            <ta e="T58" id="Seg_4532" s="T57">who-VBZ-PTCP.FUT-3SG-ACC</ta>
            <ta e="T59" id="Seg_4533" s="T58">well</ta>
            <ta e="T60" id="Seg_4534" s="T59">harness-PTCP.FUT-3SG-ACC</ta>
            <ta e="T62" id="Seg_4535" s="T60">well</ta>
            <ta e="T63" id="Seg_4536" s="T62">and</ta>
            <ta e="T64" id="Seg_4537" s="T63">then</ta>
            <ta e="T65" id="Seg_4538" s="T64">though</ta>
            <ta e="T66" id="Seg_4539" s="T65">EMPH</ta>
            <ta e="T67" id="Seg_4540" s="T66">cure.with.reindeer-EP-PTCP.PST</ta>
            <ta e="T68" id="Seg_4541" s="T67">reindeer.[NOM]</ta>
            <ta e="T73" id="Seg_4542" s="T72">cure.with.reindeer-EP-PTCP.PST</ta>
            <ta e="T74" id="Seg_4543" s="T73">reindeer.[NOM]</ta>
            <ta e="T75" id="Seg_4544" s="T74">this</ta>
            <ta e="T76" id="Seg_4545" s="T75">cure.with.reindeer-EP-PTCP.PST</ta>
            <ta e="T77" id="Seg_4546" s="T76">reindeer.[NOM]</ta>
            <ta e="T78" id="Seg_4547" s="T77">though</ta>
            <ta e="T79" id="Seg_4548" s="T78">EMPH</ta>
            <ta e="T80" id="Seg_4549" s="T79">eh</ta>
            <ta e="T81" id="Seg_4550" s="T80">cure.with.reindeer-EP-PTCP.PST</ta>
            <ta e="T82" id="Seg_4551" s="T81">reindeer.[NOM]</ta>
            <ta e="T83" id="Seg_4552" s="T82">be.sick-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T84" id="Seg_4553" s="T83">EMPH</ta>
            <ta e="T85" id="Seg_4554" s="T84">man-EP-1SG.[NOM]</ta>
            <ta e="T86" id="Seg_4555" s="T85">mother-3SG.[NOM]</ta>
            <ta e="T87" id="Seg_4556" s="T86">cure.with.reindeer-PTCP.HAB</ta>
            <ta e="T88" id="Seg_4557" s="T87">be-PST1-3SG</ta>
            <ta e="T89" id="Seg_4558" s="T88">head-3SG-DAT/LOC</ta>
            <ta e="T90" id="Seg_4559" s="T89">reindeer-ACC</ta>
            <ta e="T91" id="Seg_4560" s="T90">well</ta>
            <ta e="T92" id="Seg_4561" s="T91">that</ta>
            <ta e="T93" id="Seg_4562" s="T92">reindeer-ACC</ta>
            <ta e="T94" id="Seg_4563" s="T93">MOD</ta>
            <ta e="T95" id="Seg_4564" s="T94">woman.[NOM]</ta>
            <ta e="T96" id="Seg_4565" s="T95">harness-PTCP.FUT-3SG-ACC</ta>
            <ta e="T97" id="Seg_4566" s="T96">must.not</ta>
            <ta e="T98" id="Seg_4567" s="T97">be-PST1-3SG</ta>
            <ta e="T99" id="Seg_4568" s="T98">EMPH</ta>
            <ta e="T100" id="Seg_4569" s="T99">then</ta>
            <ta e="T101" id="Seg_4570" s="T100">1SG.[NOM]</ta>
            <ta e="T102" id="Seg_4571" s="T101">know-NEG.CVB.SIM-1SG</ta>
            <ta e="T103" id="Seg_4572" s="T102">this</ta>
            <ta e="T104" id="Seg_4573" s="T103">reindeer-ACC</ta>
            <ta e="T105" id="Seg_4574" s="T104">left.reindeer-VBZ-REFL-PST2-1SG</ta>
            <ta e="T106" id="Seg_4575" s="T105">EMPH</ta>
            <ta e="T107" id="Seg_4576" s="T106">that-3SG-2SG.[NOM]</ta>
            <ta e="T108" id="Seg_4577" s="T107">white</ta>
            <ta e="T109" id="Seg_4578" s="T108">reindeer.[NOM]</ta>
            <ta e="T110" id="Seg_4579" s="T109">off.white</ta>
            <ta e="T111" id="Seg_4580" s="T110">reindeer.[NOM]</ta>
            <ta e="T112" id="Seg_4581" s="T111">so</ta>
            <ta e="T113" id="Seg_4582" s="T112">though</ta>
            <ta e="T114" id="Seg_4583" s="T113">go-CVB.PURP-1PL</ta>
            <ta e="T115" id="Seg_4584" s="T114">though</ta>
            <ta e="T116" id="Seg_4585" s="T115">that</ta>
            <ta e="T117" id="Seg_4586" s="T116">reindeer.[NOM]</ta>
            <ta e="T118" id="Seg_4587" s="T117">EMPH</ta>
            <ta e="T119" id="Seg_4588" s="T118">bite-CAUS-PRS.[3SG]</ta>
            <ta e="T120" id="Seg_4589" s="T119">three-MLTP</ta>
            <ta e="T121" id="Seg_4590" s="T120">time.[NOM]</ta>
            <ta e="T122" id="Seg_4591" s="T121">bite-CAUS-PRS.[3SG]</ta>
            <ta e="T123" id="Seg_4592" s="T122">EMPH</ta>
            <ta e="T124" id="Seg_4593" s="T123">then</ta>
            <ta e="T125" id="Seg_4594" s="T124">1SG.[NOM]</ta>
            <ta e="T126" id="Seg_4595" s="T125">say-PRS-1SG</ta>
            <ta e="T127" id="Seg_4596" s="T126">EMPH</ta>
            <ta e="T128" id="Seg_4597" s="T127">oh.dear</ta>
            <ta e="T129" id="Seg_4598" s="T128">what-DAT/LOC</ta>
            <ta e="T130" id="Seg_4599" s="T129">bite-CAUS-PRS.[3SG]</ta>
            <ta e="T131" id="Seg_4600" s="T130">well</ta>
            <ta e="T132" id="Seg_4601" s="T131">say-PRS-1SG</ta>
            <ta e="T135" id="Seg_4602" s="T134">that.[NOM]</ta>
            <ta e="T136" id="Seg_4603" s="T135">front-3SG-INSTR</ta>
            <ta e="T137" id="Seg_4604" s="T136">hit.the.road-CVB.PURP-1PL</ta>
            <ta e="T138" id="Seg_4605" s="T137">mum-1SG.[NOM]</ta>
            <ta e="T139" id="Seg_4606" s="T138">say-PRS.[3SG]</ta>
            <ta e="T140" id="Seg_4607" s="T139">EMPH</ta>
            <ta e="T141" id="Seg_4608" s="T140">fire-1PL.[NOM]</ta>
            <ta e="T142" id="Seg_4609" s="T141">speak-PRS.[3SG]</ta>
            <ta e="T143" id="Seg_4610" s="T142">say-PRS.[3SG]</ta>
            <ta e="T144" id="Seg_4611" s="T143">fire-1PL.[NOM]</ta>
            <ta e="T145" id="Seg_4612" s="T144">speak-PRS.[3SG]</ta>
            <ta e="T148" id="Seg_4613" s="T147">though</ta>
            <ta e="T149" id="Seg_4614" s="T148">what.[NOM]</ta>
            <ta e="T150" id="Seg_4615" s="T149">INDEF</ta>
            <ta e="T151" id="Seg_4616" s="T150">bad.[NOM]</ta>
            <ta e="T152" id="Seg_4617" s="T151">be-HAB.[3SG]</ta>
            <ta e="T153" id="Seg_4618" s="T152">fire.[NOM]</ta>
            <ta e="T154" id="Seg_4619" s="T153">speak-TEMP-3SG</ta>
            <ta e="T155" id="Seg_4620" s="T154">well</ta>
            <ta e="T157" id="Seg_4621" s="T155">speak-TEMP-3SG</ta>
            <ta e="T158" id="Seg_4622" s="T157">then</ta>
            <ta e="T159" id="Seg_4623" s="T158">though</ta>
            <ta e="T160" id="Seg_4624" s="T159">so</ta>
            <ta e="T165" id="Seg_4625" s="T164">1SG.[NOM]</ta>
            <ta e="T166" id="Seg_4626" s="T165">though</ta>
            <ta e="T167" id="Seg_4627" s="T166">very</ta>
            <ta e="T168" id="Seg_4628" s="T167">EMPH</ta>
            <ta e="T169" id="Seg_4629" s="T168">that-ACC</ta>
            <ta e="T170" id="Seg_4630" s="T169">though</ta>
            <ta e="T171" id="Seg_4631" s="T170">believe-NEG.CVB.SIM-1SG</ta>
            <ta e="T172" id="Seg_4632" s="T171">though</ta>
            <ta e="T173" id="Seg_4633" s="T172">well</ta>
            <ta e="T174" id="Seg_4634" s="T173">reindeer.[NOM]</ta>
            <ta e="T175" id="Seg_4635" s="T174">catch-PST1-1PL</ta>
            <ta e="T176" id="Seg_4636" s="T175">and</ta>
            <ta e="T177" id="Seg_4637" s="T176">go-CVB.SEQ</ta>
            <ta e="T178" id="Seg_4638" s="T177">stay-PST1-1PL</ta>
            <ta e="T179" id="Seg_4639" s="T178">EMPH</ta>
            <ta e="T180" id="Seg_4640" s="T179">that</ta>
            <ta e="T181" id="Seg_4641" s="T180">this</ta>
            <ta e="T182" id="Seg_4642" s="T181">reindeer-ACC</ta>
            <ta e="T183" id="Seg_4643" s="T182">catch-PST2-1PL</ta>
            <ta e="T184" id="Seg_4644" s="T183">so</ta>
            <ta e="T185" id="Seg_4645" s="T184">go-CVB.SEQ</ta>
            <ta e="T186" id="Seg_4646" s="T185">stay-PST2-1PL</ta>
            <ta e="T187" id="Seg_4647" s="T186">EMPH</ta>
            <ta e="T188" id="Seg_4648" s="T187">father-1SG-ACC</ta>
            <ta e="T189" id="Seg_4649" s="T188">with</ta>
            <ta e="T190" id="Seg_4650" s="T189">come.along-CVB.SEQ</ta>
            <ta e="T191" id="Seg_4651" s="T190">stay-PST2-1PL</ta>
            <ta e="T192" id="Seg_4652" s="T191">come.along-CVB.SEQ</ta>
            <ta e="T193" id="Seg_4653" s="T192">stay</ta>
            <ta e="T194" id="Seg_4654" s="T193">temporary.settlement-DAT/LOC</ta>
            <ta e="T195" id="Seg_4655" s="T194">come-CVB.SEQ</ta>
            <ta e="T196" id="Seg_4656" s="T195">go-CVB.SEQ</ta>
            <ta e="T197" id="Seg_4657" s="T196">though</ta>
            <ta e="T198" id="Seg_4658" s="T197">father-1SG.[NOM]</ta>
            <ta e="T199" id="Seg_4659" s="T198">sledge-1PL-ACC</ta>
            <ta e="T200" id="Seg_4660" s="T199">sledge-1PL-ACC</ta>
            <ta e="T201" id="Seg_4661" s="T200">though</ta>
            <ta e="T202" id="Seg_4662" s="T201">load-REFL-PST1-1PL</ta>
            <ta e="T203" id="Seg_4663" s="T202">load-REFL-CVB.SEQ</ta>
            <ta e="T204" id="Seg_4664" s="T203">string.for.luggage-VBZ-REFL-CVB.SEQ</ta>
            <ta e="T205" id="Seg_4665" s="T204">and.so.on-CVB.SEQ</ta>
            <ta e="T206" id="Seg_4666" s="T205">EMPH</ta>
            <ta e="T207" id="Seg_4667" s="T206">1SG-ACC</ta>
            <ta e="T208" id="Seg_4668" s="T207">well</ta>
            <ta e="T209" id="Seg_4669" s="T208">hit.the.road-CAUS-PST1-3SG</ta>
            <ta e="T210" id="Seg_4670" s="T209">EMPH</ta>
            <ta e="T211" id="Seg_4671" s="T210">sledge-PROPR</ta>
            <ta e="T212" id="Seg_4672" s="T211">reindeer-1SG-ACC</ta>
            <ta e="T213" id="Seg_4673" s="T212">lead-CVB.SEQ</ta>
            <ta e="T214" id="Seg_4674" s="T213">go-CVB.SEQ</ta>
            <ta e="T215" id="Seg_4675" s="T214">sledge-PROPR</ta>
            <ta e="T216" id="Seg_4676" s="T215">reindeer-1SG-ACC</ta>
            <ta e="T217" id="Seg_4677" s="T216">lead-CVB.SEQ</ta>
            <ta e="T218" id="Seg_4678" s="T217">go-CVB.SEQ-1SG</ta>
            <ta e="T219" id="Seg_4679" s="T218">EMPH</ta>
            <ta e="T220" id="Seg_4680" s="T219">mistake</ta>
            <ta e="T221" id="Seg_4681" s="T220">sit.down-PRS-1SG</ta>
            <ta e="T222" id="Seg_4682" s="T221">EMPH</ta>
            <ta e="T223" id="Seg_4683" s="T222">sledge-1SG-DAT/LOC</ta>
            <ta e="T224" id="Seg_4684" s="T223">EMPH</ta>
            <ta e="T225" id="Seg_4685" s="T224">fish-PL-ACC</ta>
            <ta e="T226" id="Seg_4686" s="T225">load-PST2-1SG</ta>
            <ta e="T227" id="Seg_4687" s="T226">six</ta>
            <ta e="T228" id="Seg_4688" s="T227">sack.[NOM]</ta>
            <ta e="T229" id="Seg_4689" s="T228">fish.[NOM]</ta>
            <ta e="T230" id="Seg_4690" s="T229">six</ta>
            <ta e="T231" id="Seg_4691" s="T230">sack.[NOM]</ta>
            <ta e="T232" id="Seg_4692" s="T231">fish.[NOM]</ta>
            <ta e="T233" id="Seg_4693" s="T232">load-CVB.SEQ</ta>
            <ta e="T234" id="Seg_4694" s="T233">go-CVB.SEQ</ta>
            <ta e="T235" id="Seg_4695" s="T234">though</ta>
            <ta e="T236" id="Seg_4696" s="T235">sledge-PL.[NOM]</ta>
            <ta e="T237" id="Seg_4697" s="T236">jump-PRS-1SG</ta>
            <ta e="T238" id="Seg_4698" s="T237">say-PRS-1SG</ta>
            <ta e="T239" id="Seg_4699" s="T238">EMPH</ta>
            <ta e="T240" id="Seg_4700" s="T239">sledge-1SG-ACC</ta>
            <ta e="T241" id="Seg_4701" s="T240">mistake</ta>
            <ta e="T242" id="Seg_4702" s="T241">sit.down-PRS-1SG</ta>
            <ta e="T243" id="Seg_4703" s="T242">EMPH</ta>
            <ta e="T244" id="Seg_4704" s="T243">no</ta>
            <ta e="T245" id="Seg_4705" s="T244">this</ta>
            <ta e="T246" id="Seg_4706" s="T245">human.being.[NOM]</ta>
            <ta e="T247" id="Seg_4707" s="T246">EMPH</ta>
            <ta e="T248" id="Seg_4708" s="T247">still</ta>
            <ta e="T249" id="Seg_4709" s="T248">and</ta>
            <ta e="T250" id="Seg_4710" s="T249">jump-PRS-1SG</ta>
            <ta e="T251" id="Seg_4711" s="T250">EMPH</ta>
            <ta e="T253" id="Seg_4712" s="T252">jump-PRS-1SG</ta>
            <ta e="T255" id="Seg_4713" s="T253">EMPH</ta>
            <ta e="T256" id="Seg_4714" s="T255">again</ta>
            <ta e="T257" id="Seg_4715" s="T256">mistake</ta>
            <ta e="T258" id="Seg_4716" s="T257">sit.down-PRS-1SG</ta>
            <ta e="T259" id="Seg_4717" s="T258">EMPH</ta>
            <ta e="T260" id="Seg_4718" s="T259">sledge-PROPR</ta>
            <ta e="T261" id="Seg_4719" s="T260">reindeer-1SG-ACC</ta>
            <ta e="T262" id="Seg_4720" s="T261">fall-CAUS-CVB.SEQ</ta>
            <ta e="T263" id="Seg_4721" s="T262">let-PST1-1SG</ta>
            <ta e="T264" id="Seg_4722" s="T263">EMPH</ta>
            <ta e="T265" id="Seg_4723" s="T264">so</ta>
            <ta e="T266" id="Seg_4724" s="T265">sledge-PROPR</ta>
            <ta e="T267" id="Seg_4725" s="T266">reindeer-1SG.[NOM]</ta>
            <ta e="T268" id="Seg_4726" s="T267">though</ta>
            <ta e="T269" id="Seg_4727" s="T268">sledge-1SG.[NOM]</ta>
            <ta e="T270" id="Seg_4728" s="T269">reindeer-PL-3SG.[NOM]</ta>
            <ta e="T271" id="Seg_4729" s="T270">upper.part-1SG-INSTR</ta>
            <ta e="T272" id="Seg_4730" s="T271">go-PRS-3PL</ta>
            <ta e="T273" id="Seg_4731" s="T272">EMPH</ta>
            <ta e="T274" id="Seg_4732" s="T273">and</ta>
            <ta e="T275" id="Seg_4733" s="T274">self-1SG.[NOM]</ta>
            <ta e="T276" id="Seg_4734" s="T275">though</ta>
            <ta e="T277" id="Seg_4735" s="T276">EMPH</ta>
            <ta e="T278" id="Seg_4736" s="T277">sledge.[NOM]</ta>
            <ta e="T279" id="Seg_4737" s="T278">supporting.pole.in.sleigh-3SG-ACC</ta>
            <ta e="T280" id="Seg_4738" s="T279">every-INSTR</ta>
            <ta e="T281" id="Seg_4739" s="T280">three-MLTP</ta>
            <ta e="T282" id="Seg_4740" s="T281">time.[NOM]</ta>
            <ta e="T283" id="Seg_4741" s="T282">turn-PST1-1SG</ta>
            <ta e="T284" id="Seg_4742" s="T283">EMPH</ta>
            <ta e="T285" id="Seg_4743" s="T284">oh.dear</ta>
            <ta e="T286" id="Seg_4744" s="T285">so</ta>
            <ta e="T287" id="Seg_4745" s="T286">EMPH</ta>
            <ta e="T288" id="Seg_4746" s="T287">on.the.back</ta>
            <ta e="T289" id="Seg_4747" s="T288">fall-CVB.SEQ</ta>
            <ta e="T290" id="Seg_4748" s="T289">stay</ta>
            <ta e="T291" id="Seg_4749" s="T290">on.the.back</ta>
            <ta e="T292" id="Seg_4750" s="T291">fall-CVB.SEQ</ta>
            <ta e="T293" id="Seg_4751" s="T292">go-CVB.SEQ</ta>
            <ta e="T294" id="Seg_4752" s="T293">EMPH</ta>
            <ta e="T295" id="Seg_4753" s="T294">lie-PRS-1SG</ta>
            <ta e="T296" id="Seg_4754" s="T295">EMPH</ta>
            <ta e="T297" id="Seg_4755" s="T296">father-1SG.[NOM]</ta>
            <ta e="T298" id="Seg_4756" s="T297">long.coat.[NOM]</ta>
            <ta e="T299" id="Seg_4757" s="T298">dress-PST1-3SG</ta>
            <ta e="T300" id="Seg_4758" s="T299">stand-PTCP.PRS</ta>
            <ta e="T301" id="Seg_4759" s="T300">be-PST1-3SG</ta>
            <ta e="T302" id="Seg_4760" s="T301">EMPH</ta>
            <ta e="T303" id="Seg_4761" s="T302">long.coat.[NOM]</ta>
            <ta e="T304" id="Seg_4762" s="T303">dress-CVB.SEQ</ta>
            <ta e="T305" id="Seg_4763" s="T304">go-CVB.SEQ</ta>
            <ta e="T306" id="Seg_4764" s="T305">this-INTNS.[NOM]</ta>
            <ta e="T307" id="Seg_4765" s="T306">see-PST2-3SG</ta>
            <ta e="T308" id="Seg_4766" s="T307">see-PRS.[3SG]</ta>
            <ta e="T309" id="Seg_4767" s="T308">sledge-PROPR</ta>
            <ta e="T310" id="Seg_4768" s="T309">reindeer.[NOM]</ta>
            <ta e="T311" id="Seg_4769" s="T310">go-CVB.SIM</ta>
            <ta e="T312" id="Seg_4770" s="T311">stand-PRS.[3SG]</ta>
            <ta e="T313" id="Seg_4771" s="T312">human.being-3SG.[NOM]</ta>
            <ta e="T314" id="Seg_4772" s="T313">EMPH</ta>
            <ta e="T315" id="Seg_4773" s="T314">here</ta>
            <ta e="T316" id="Seg_4774" s="T315">lie-PRS.[3SG]</ta>
            <ta e="T317" id="Seg_4775" s="T316">way-DAT/LOC</ta>
            <ta e="T318" id="Seg_4776" s="T317">well</ta>
            <ta e="T319" id="Seg_4777" s="T318">run-CVB.SEQ</ta>
            <ta e="T320" id="Seg_4778" s="T319">come-PST1-3SG</ta>
            <ta e="T321" id="Seg_4779" s="T320">EMPH</ta>
            <ta e="T322" id="Seg_4780" s="T321">so</ta>
            <ta e="T323" id="Seg_4781" s="T322">say-PST1-3SG</ta>
            <ta e="T324" id="Seg_4782" s="T323">what.kind.of</ta>
            <ta e="T325" id="Seg_4783" s="T324">2SG.[NOM]</ta>
            <ta e="T326" id="Seg_4784" s="T325">breath-PROPR-2SG</ta>
            <ta e="T327" id="Seg_4785" s="T326">Q</ta>
            <ta e="T328" id="Seg_4786" s="T327">say-PST1-3SG</ta>
            <ta e="T329" id="Seg_4787" s="T328">AFFIRM</ta>
            <ta e="T330" id="Seg_4788" s="T329">breath-PROPR-1SG</ta>
            <ta e="T331" id="Seg_4789" s="T330">breath-PROPR-1SG</ta>
            <ta e="T332" id="Seg_4790" s="T331">leg-EP-1SG.[NOM]</ta>
            <ta e="T333" id="Seg_4791" s="T332">just</ta>
            <ta e="T334" id="Seg_4792" s="T333">can-CVB.SEQ</ta>
            <ta e="T335" id="Seg_4793" s="T334">move-NEG.[3SG]</ta>
            <ta e="T336" id="Seg_4794" s="T335">AFFIRM</ta>
            <ta e="T337" id="Seg_4795" s="T336">well</ta>
            <ta e="T338" id="Seg_4796" s="T337">wait</ta>
            <ta e="T339" id="Seg_4797" s="T338">1SG.[NOM]</ta>
            <ta e="T340" id="Seg_4798" s="T339">come-FUT-1SG</ta>
            <ta e="T341" id="Seg_4799" s="T340">well</ta>
            <ta e="T342" id="Seg_4800" s="T341">come-CVB.SEQ</ta>
            <ta e="T343" id="Seg_4801" s="T342">go-CVB.SEQ</ta>
            <ta e="T344" id="Seg_4802" s="T343">sledge-PROPR</ta>
            <ta e="T345" id="Seg_4803" s="T344">reindeer-3SG-DAT/LOC</ta>
            <ta e="T346" id="Seg_4804" s="T345">how</ta>
            <ta e="T347" id="Seg_4805" s="T346">INDEF</ta>
            <ta e="T348" id="Seg_4806" s="T347">mount-CVB.SEQ</ta>
            <ta e="T349" id="Seg_4807" s="T348">go-CVB.SEQ</ta>
            <ta e="T350" id="Seg_4808" s="T349">EMPH</ta>
            <ta e="T351" id="Seg_4809" s="T350">1SG.[NOM]</ta>
            <ta e="T352" id="Seg_4810" s="T351">sledge-PROPR</ta>
            <ta e="T353" id="Seg_4811" s="T352">reindeer-1SG-ACC</ta>
            <ta e="T354" id="Seg_4812" s="T353">chase-CVB.SIM</ta>
            <ta e="T355" id="Seg_4813" s="T354">go-PRS-1PL</ta>
            <ta e="T356" id="Seg_4814" s="T355">EMPH</ta>
            <ta e="T357" id="Seg_4815" s="T356">there</ta>
            <ta e="T358" id="Seg_4816" s="T357">EMPH</ta>
            <ta e="T359" id="Seg_4817" s="T358">lake-ACC</ta>
            <ta e="T360" id="Seg_4818" s="T359">climb-CVB.PURP-1PL</ta>
            <ta e="T361" id="Seg_4819" s="T360">slope-DAT/LOC</ta>
            <ta e="T362" id="Seg_4820" s="T361">slope.[NOM]</ta>
            <ta e="T363" id="Seg_4821" s="T362">along</ta>
            <ta e="T364" id="Seg_4822" s="T363">though</ta>
            <ta e="T367" id="Seg_4823" s="T366">younger.brother-EP-1SG.[NOM]</ta>
            <ta e="T368" id="Seg_4824" s="T367">there.is</ta>
            <ta e="T369" id="Seg_4825" s="T368">be-PST1-3SG</ta>
            <ta e="T370" id="Seg_4826" s="T369">that</ta>
            <ta e="T371" id="Seg_4827" s="T370">misery-PROPR</ta>
            <ta e="T372" id="Seg_4828" s="T371">sledge-ABL</ta>
            <ta e="T373" id="Seg_4829" s="T372">fall-CVB.SEQ</ta>
            <ta e="T374" id="Seg_4830" s="T373">after</ta>
            <ta e="T375" id="Seg_4831" s="T374">rein-ACC</ta>
            <ta e="T376" id="Seg_4832" s="T375">catch-CVB.SEQ</ta>
            <ta e="T377" id="Seg_4833" s="T376">take-PST1-3SG</ta>
            <ta e="T378" id="Seg_4834" s="T377">so</ta>
            <ta e="T381" id="Seg_4835" s="T380">reindeer-PL.[NOM]</ta>
            <ta e="T382" id="Seg_4836" s="T381">stop-PST1-3PL</ta>
            <ta e="T386" id="Seg_4837" s="T385">1PL.[NOM]</ta>
            <ta e="T387" id="Seg_4838" s="T386">come-PRS-1PL</ta>
            <ta e="T389" id="Seg_4839" s="T387">EMPH</ta>
            <ta e="T390" id="Seg_4840" s="T389">father-1SG.[NOM]</ta>
            <ta e="T391" id="Seg_4841" s="T390">sledge-PROPR</ta>
            <ta e="T392" id="Seg_4842" s="T391">reindeer-1SG-ACC</ta>
            <ta e="T393" id="Seg_4843" s="T392">tie-CVB.SEQ</ta>
            <ta e="T394" id="Seg_4844" s="T393">throw-PST1-3SG</ta>
            <ta e="T395" id="Seg_4845" s="T394">my-3SG-ACC</ta>
            <ta e="T396" id="Seg_4846" s="T395">so</ta>
            <ta e="T397" id="Seg_4847" s="T396">make-CVB.SEQ</ta>
            <ta e="T398" id="Seg_4848" s="T397">go-CVB.SEQ</ta>
            <ta e="T399" id="Seg_4849" s="T398">self-3SG.[NOM]</ta>
            <ta e="T400" id="Seg_4850" s="T399">EMPH</ta>
            <ta e="T401" id="Seg_4851" s="T400">1SG.[NOM]</ta>
            <ta e="T402" id="Seg_4852" s="T401">sledge-PROPR</ta>
            <ta e="T403" id="Seg_4853" s="T402">reindeer-PL.[NOM]</ta>
            <ta e="T404" id="Seg_4854" s="T403">mount-CVB.SEQ</ta>
            <ta e="T405" id="Seg_4855" s="T404">go-CVB.SEQ</ta>
            <ta e="T406" id="Seg_4856" s="T405">that</ta>
            <ta e="T407" id="Seg_4857" s="T406">house-1PL-DAT/LOC</ta>
            <ta e="T408" id="Seg_4858" s="T407">go-PST1-1PL</ta>
            <ta e="T409" id="Seg_4859" s="T408">EMPH</ta>
            <ta e="T410" id="Seg_4860" s="T409">house-1PL-DAT/LOC</ta>
            <ta e="T411" id="Seg_4861" s="T410">go-CVB.SEQ</ta>
            <ta e="T412" id="Seg_4862" s="T411">go-CVB.SEQ</ta>
            <ta e="T413" id="Seg_4863" s="T412">though</ta>
            <ta e="T414" id="Seg_4864" s="T413">who-DAT/LOC</ta>
            <ta e="T415" id="Seg_4865" s="T414">door-1SG-DAT/LOC</ta>
            <ta e="T416" id="Seg_4866" s="T415">stop-CAUS-PRS.[3SG]</ta>
            <ta e="T417" id="Seg_4867" s="T416">EMPH</ta>
            <ta e="T418" id="Seg_4868" s="T417">balok-EP-1SG.[NOM]</ta>
            <ta e="T419" id="Seg_4869" s="T418">door-3SG-DAT/LOC</ta>
            <ta e="T420" id="Seg_4870" s="T419">well</ta>
            <ta e="T421" id="Seg_4871" s="T420">so</ta>
            <ta e="T422" id="Seg_4872" s="T421">EMPH</ta>
            <ta e="T423" id="Seg_4873" s="T422">old.woman-PL.[NOM]</ta>
            <ta e="T424" id="Seg_4874" s="T423">two</ta>
            <ta e="T425" id="Seg_4875" s="T424">old.woman.[NOM]</ta>
            <ta e="T426" id="Seg_4876" s="T425">there.is</ta>
            <ta e="T427" id="Seg_4877" s="T426">be-PST1-3SG</ta>
            <ta e="T429" id="Seg_4878" s="T428">that-PL.[NOM]</ta>
            <ta e="T430" id="Seg_4879" s="T429">come-PST1-3PL</ta>
            <ta e="T431" id="Seg_4880" s="T430">well</ta>
            <ta e="T432" id="Seg_4881" s="T431">EMPH</ta>
            <ta e="T433" id="Seg_4882" s="T432">what.[NOM]</ta>
            <ta e="T434" id="Seg_4883" s="T433">be-PST1-2PL</ta>
            <ta e="T435" id="Seg_4884" s="T434">how</ta>
            <ta e="T436" id="Seg_4885" s="T435">how</ta>
            <ta e="T438" id="Seg_4886" s="T436">be-PST1-2PL</ta>
            <ta e="T439" id="Seg_4887" s="T438">oh.dear</ta>
            <ta e="T440" id="Seg_4888" s="T439">this</ta>
            <ta e="T441" id="Seg_4889" s="T440">1SG-DAT/LOC</ta>
            <ta e="T442" id="Seg_4890" s="T441">just</ta>
            <ta e="T443" id="Seg_4891" s="T442">this</ta>
            <ta e="T444" id="Seg_4892" s="T443">woman.[NOM]</ta>
            <ta e="T445" id="Seg_4893" s="T444">injure-EP-REFL-PST1-3SG</ta>
            <ta e="T446" id="Seg_4894" s="T445">this-ACC</ta>
            <ta e="T447" id="Seg_4895" s="T446">need.to</ta>
            <ta e="T448" id="Seg_4896" s="T447">doctor-PART</ta>
            <ta e="T449" id="Seg_4897" s="T448">call-EP-REFL-PTCP.FUT-DAT/LOC</ta>
            <ta e="T450" id="Seg_4898" s="T449">this-ACC</ta>
            <ta e="T451" id="Seg_4899" s="T450">father-1SG.[NOM]</ta>
            <ta e="T452" id="Seg_4900" s="T451">mum-1SG.[NOM]</ta>
            <ta e="T453" id="Seg_4901" s="T452">say-PRS.[3SG]</ta>
            <ta e="T454" id="Seg_4902" s="T453">go-EP-NEG.[IMP.2SG]</ta>
            <ta e="T455" id="Seg_4903" s="T454">go-EP-NEG.[IMP.2SG]</ta>
            <ta e="T456" id="Seg_4904" s="T455">doctor-ACC</ta>
            <ta e="T457" id="Seg_4905" s="T456">what-EP-ACC</ta>
            <ta e="T458" id="Seg_4906" s="T457">say-PRS.[3SG]</ta>
            <ta e="T459" id="Seg_4907" s="T458">sin.[NOM]</ta>
            <ta e="T460" id="Seg_4908" s="T459">EMPH</ta>
            <ta e="T461" id="Seg_4909" s="T460">good.[NOM]</ta>
            <ta e="T462" id="Seg_4910" s="T461">lungs-3SG.[NOM]</ta>
            <ta e="T463" id="Seg_4911" s="T462">broad</ta>
            <ta e="T466" id="Seg_4912" s="T465">good.[NOM]</ta>
            <ta e="T467" id="Seg_4913" s="T466">wait</ta>
            <ta e="T468" id="Seg_4914" s="T467">then</ta>
            <ta e="T469" id="Seg_4915" s="T468">1SG-ACC</ta>
            <ta e="T470" id="Seg_4916" s="T469">two</ta>
            <ta e="T471" id="Seg_4917" s="T470">side-ABL</ta>
            <ta e="T472" id="Seg_4918" s="T471">raise-CVB.SEQ</ta>
            <ta e="T473" id="Seg_4919" s="T472">go-CVB.SEQ</ta>
            <ta e="T474" id="Seg_4920" s="T473">house.[NOM]</ta>
            <ta e="T475" id="Seg_4921" s="T474">balok-DAT/LOC</ta>
            <ta e="T476" id="Seg_4922" s="T475">come.back-CAUS-PST1-3PL</ta>
            <ta e="T477" id="Seg_4923" s="T476">EMPH</ta>
            <ta e="T478" id="Seg_4924" s="T477">AFFIRM</ta>
            <ta e="T479" id="Seg_4925" s="T478">well</ta>
            <ta e="T480" id="Seg_4926" s="T479">that</ta>
            <ta e="T481" id="Seg_4927" s="T480">naked-VBZ-CVB.SEQ</ta>
            <ta e="T482" id="Seg_4928" s="T481">and.so.on-CVB.SEQ</ta>
            <ta e="T483" id="Seg_4929" s="T482">go-CVB.SEQ</ta>
            <ta e="T484" id="Seg_4930" s="T483">well</ta>
            <ta e="T485" id="Seg_4931" s="T484">what.[NOM]</ta>
            <ta e="T486" id="Seg_4932" s="T485">doctor-3SG-ACC</ta>
            <ta e="T487" id="Seg_4933" s="T486">EMPH-3SG-ACC</ta>
            <ta e="T488" id="Seg_4934" s="T487">wait</ta>
            <ta e="T489" id="Seg_4935" s="T488">good.[NOM]</ta>
            <ta e="T490" id="Seg_4936" s="T489">maybe</ta>
            <ta e="T491" id="Seg_4937" s="T490">and</ta>
            <ta e="T492" id="Seg_4938" s="T491">simply</ta>
            <ta e="T493" id="Seg_4939" s="T492">EMPH</ta>
            <ta e="T494" id="Seg_4940" s="T493">apparently</ta>
            <ta e="T495" id="Seg_4941" s="T494">what-3SG.[NOM]</ta>
            <ta e="T496" id="Seg_4942" s="T495">NEG</ta>
            <ta e="T497" id="Seg_4943" s="T496">such.as</ta>
            <ta e="T498" id="Seg_4944" s="T497">break-EP-PST2.NEG.[3SG]</ta>
            <ta e="T499" id="Seg_4945" s="T498">blood.[NOM]</ta>
            <ta e="T500" id="Seg_4946" s="T499">NEG</ta>
            <ta e="T501" id="Seg_4947" s="T500">NEG.EX</ta>
            <ta e="T502" id="Seg_4948" s="T501">what.[NOM]</ta>
            <ta e="T503" id="Seg_4949" s="T502">NEG</ta>
            <ta e="T504" id="Seg_4950" s="T503">NEG.EX</ta>
            <ta e="T505" id="Seg_4951" s="T504">good.[NOM]</ta>
            <ta e="T506" id="Seg_4952" s="T505">be-FUT.[3SG]</ta>
            <ta e="T507" id="Seg_4953" s="T506">then</ta>
            <ta e="T508" id="Seg_4954" s="T507">AFFIRM</ta>
            <ta e="T509" id="Seg_4955" s="T508">well</ta>
            <ta e="T510" id="Seg_4956" s="T509">well</ta>
            <ta e="T511" id="Seg_4957" s="T510">that-ABL</ta>
            <ta e="T512" id="Seg_4958" s="T511">that</ta>
            <ta e="T513" id="Seg_4959" s="T512">well</ta>
            <ta e="T514" id="Seg_4960" s="T513">that-ABL</ta>
            <ta e="T515" id="Seg_4961" s="T514">that-ABL</ta>
            <ta e="T516" id="Seg_4962" s="T515">little.later</ta>
            <ta e="T517" id="Seg_4963" s="T516">later</ta>
            <ta e="T518" id="Seg_4964" s="T517">very</ta>
            <ta e="T519" id="Seg_4965" s="T518">well</ta>
            <ta e="T520" id="Seg_4966" s="T519">month.[NOM]</ta>
            <ta e="T521" id="Seg_4967" s="T520">pass.by-PST1-3SG</ta>
            <ta e="T522" id="Seg_4968" s="T521">EMPH</ta>
            <ta e="T523" id="Seg_4969" s="T522">month.[NOM]</ta>
            <ta e="T524" id="Seg_4970" s="T523">pass.by-CVB.SEQ</ta>
            <ta e="T525" id="Seg_4971" s="T524">go-CVB.SEQ</ta>
            <ta e="T526" id="Seg_4972" s="T525">that.EMPH-INSTR</ta>
            <ta e="T527" id="Seg_4973" s="T526">doctor</ta>
            <ta e="T528" id="Seg_4974" s="T527">NEG</ta>
            <ta e="T529" id="Seg_4975" s="T528">NEG.EX</ta>
            <ta e="T530" id="Seg_4976" s="T529">what.[NOM]</ta>
            <ta e="T531" id="Seg_4977" s="T530">NEG</ta>
            <ta e="T532" id="Seg_4978" s="T531">NEG.EX</ta>
            <ta e="T533" id="Seg_4979" s="T532">month.[NOM]</ta>
            <ta e="T534" id="Seg_4980" s="T533">pass.by-CVB.SEQ</ta>
            <ta e="T535" id="Seg_4981" s="T534">go-CVB.SEQ</ta>
            <ta e="T536" id="Seg_4982" s="T535">well</ta>
            <ta e="T537" id="Seg_4983" s="T536">that</ta>
            <ta e="T538" id="Seg_4984" s="T537">new</ta>
            <ta e="T539" id="Seg_4985" s="T538">year-DAT/LOC</ta>
            <ta e="T540" id="Seg_4986" s="T539">go.in-CVB.PURP</ta>
            <ta e="T541" id="Seg_4987" s="T540">well</ta>
            <ta e="T542" id="Seg_4988" s="T541">doctor-DAT/LOC</ta>
            <ta e="T543" id="Seg_4989" s="T542">go.in-PST1-1PL</ta>
            <ta e="T544" id="Seg_4990" s="T543">EMPH</ta>
            <ta e="T545" id="Seg_4991" s="T544">there</ta>
            <ta e="T546" id="Seg_4992" s="T545">go.in-CVB.SEQ-1PL</ta>
            <ta e="T547" id="Seg_4993" s="T546">that</ta>
            <ta e="T548" id="Seg_4994" s="T547">well</ta>
            <ta e="T549" id="Seg_4995" s="T548">doctor-EP-1SG.[NOM]</ta>
            <ta e="T550" id="Seg_4996" s="T549">say-PRS.[3SG]</ta>
            <ta e="T551" id="Seg_4997" s="T550">EMPH</ta>
            <ta e="T552" id="Seg_4998" s="T551">well</ta>
            <ta e="T553" id="Seg_4999" s="T552">your</ta>
            <ta e="T554" id="Seg_5000" s="T553">hip-EP-2SG.[NOM]</ta>
            <ta e="T555" id="Seg_5001" s="T554">hip-EP-2SG.[NOM]</ta>
            <ta e="T556" id="Seg_5002" s="T555">break-EP-PTCP.PST</ta>
            <ta e="T557" id="Seg_5003" s="T556">be-PST2.[3SG]</ta>
            <ta e="T558" id="Seg_5004" s="T557">2SG-ACC</ta>
            <ta e="T559" id="Seg_5005" s="T558">need.to</ta>
            <ta e="T560" id="Seg_5006" s="T559">Khatanga-DAT/LOC</ta>
            <ta e="T561" id="Seg_5007" s="T560">send-PTCP.FUT-DAT/LOC</ta>
            <ta e="T562" id="Seg_5008" s="T561">well</ta>
            <ta e="T563" id="Seg_5009" s="T562">then</ta>
            <ta e="T564" id="Seg_5010" s="T563">Khatanga-DAT/LOC</ta>
            <ta e="T565" id="Seg_5011" s="T564">send-PST2-3PL</ta>
            <ta e="T566" id="Seg_5012" s="T565">Khatanga.[NOM]</ta>
            <ta e="T567" id="Seg_5013" s="T566">1SG.[NOM]</ta>
            <ta e="T568" id="Seg_5014" s="T567">lie-PST1-1SG</ta>
            <ta e="T569" id="Seg_5015" s="T568">EMPH</ta>
            <ta e="T570" id="Seg_5016" s="T569">there</ta>
            <ta e="T571" id="Seg_5017" s="T570">well</ta>
            <ta e="T572" id="Seg_5018" s="T571">two</ta>
            <ta e="T573" id="Seg_5019" s="T572">month-ACC</ta>
            <ta e="T574" id="Seg_5020" s="T573">traction-DAT/LOC</ta>
            <ta e="T575" id="Seg_5021" s="T574">lie-PST1-1SG</ta>
            <ta e="T576" id="Seg_5022" s="T575">EMPH</ta>
            <ta e="T577" id="Seg_5023" s="T576">that-ABL</ta>
            <ta e="T578" id="Seg_5024" s="T577">EMPH</ta>
            <ta e="T579" id="Seg_5025" s="T578">other.of.two</ta>
            <ta e="T580" id="Seg_5026" s="T579">month-PL-3SG-ACC</ta>
            <ta e="T581" id="Seg_5027" s="T580">EMPH</ta>
            <ta e="T582" id="Seg_5028" s="T581">see.[IMP.2SG]</ta>
            <ta e="T583" id="Seg_5029" s="T582">eight</ta>
            <ta e="T584" id="Seg_5030" s="T583">month-ACC</ta>
            <ta e="T585" id="Seg_5031" s="T584">whole</ta>
            <ta e="T586" id="Seg_5032" s="T585">lie-PST1-1SG</ta>
            <ta e="T587" id="Seg_5033" s="T586">EMPH</ta>
            <ta e="T588" id="Seg_5034" s="T587">Khatanga-DAT/LOC</ta>
            <ta e="T589" id="Seg_5035" s="T614">house-1SG-DAT/LOC</ta>
            <ta e="T590" id="Seg_5036" s="T589">come-PST2-EP-1SG</ta>
            <ta e="T591" id="Seg_5037" s="T590">and</ta>
            <ta e="T592" id="Seg_5038" s="T591">leg-EP-1SG.[NOM]</ta>
            <ta e="T598" id="Seg_5039" s="T597">be-PST1-3SG</ta>
            <ta e="T599" id="Seg_5040" s="T598">that.[NOM]</ta>
            <ta e="T600" id="Seg_5041" s="T599">similar</ta>
            <ta e="T601" id="Seg_5042" s="T600">EMPH-then</ta>
            <ta e="T602" id="Seg_5043" s="T601">get.better-PST2.[3SG]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_5044" s="T0">Kind.[NOM]</ta>
            <ta e="T2" id="Seg_5045" s="T1">sein-TEMP-1SG</ta>
            <ta e="T3" id="Seg_5046" s="T2">aber</ta>
            <ta e="T4" id="Seg_5047" s="T3">EMPH</ta>
            <ta e="T5" id="Seg_5048" s="T4">Kind-PL-EP-1SG.[NOM]</ta>
            <ta e="T6" id="Seg_5049" s="T5">klein.[NOM]</ta>
            <ta e="T7" id="Seg_5050" s="T6">sein-PST1-3PL</ta>
            <ta e="T8" id="Seg_5051" s="T7">eins</ta>
            <ta e="T9" id="Seg_5052" s="T8">Kind-1SG.[NOM]</ta>
            <ta e="T10" id="Seg_5053" s="T9">Jahr-PROPR.[NOM]</ta>
            <ta e="T11" id="Seg_5054" s="T10">sein-PST1-3SG</ta>
            <ta e="T12" id="Seg_5055" s="T11">Jahr-ABL</ta>
            <ta e="T13" id="Seg_5056" s="T12">Rest-PROPR</ta>
            <ta e="T17" id="Seg_5057" s="T16">eins</ta>
            <ta e="T18" id="Seg_5058" s="T17">Kind-1SG.[NOM]</ta>
            <ta e="T19" id="Seg_5059" s="T18">aber</ta>
            <ta e="T20" id="Seg_5060" s="T19">acht</ta>
            <ta e="T21" id="Seg_5061" s="T20">Monat-PROPR.[NOM]</ta>
            <ta e="T22" id="Seg_5062" s="T21">sein-PST1-3SG</ta>
            <ta e="T23" id="Seg_5063" s="T22">dann</ta>
            <ta e="T24" id="Seg_5064" s="T23">aber</ta>
            <ta e="T25" id="Seg_5065" s="T24">EMPH</ta>
            <ta e="T26" id="Seg_5066" s="T25">Vater-1SG.[NOM]</ta>
            <ta e="T27" id="Seg_5067" s="T26">Holz.[NOM]</ta>
            <ta e="T28" id="Seg_5068" s="T27">bringen-CVB.SIM</ta>
            <ta e="T29" id="Seg_5069" s="T28">gehen-CVB.SEQ-1SG</ta>
            <ta e="T30" id="Seg_5070" s="T29">Vater-1SG.[NOM]</ta>
            <ta e="T31" id="Seg_5071" s="T30">mit</ta>
            <ta e="T32" id="Seg_5072" s="T31">mitkommen-EP-PTCP.PST</ta>
            <ta e="T33" id="Seg_5073" s="T32">sein-PST1-1SG</ta>
            <ta e="T34" id="Seg_5074" s="T33">Schlitten-PROPR-1SG</ta>
            <ta e="T35" id="Seg_5075" s="T34">Schlitten-PROPR-1SG</ta>
            <ta e="T36" id="Seg_5076" s="T35">Schlitten-1SG-DAT/LOC</ta>
            <ta e="T37" id="Seg_5077" s="T36">aber</ta>
            <ta e="T38" id="Seg_5078" s="T37">jüngerer.Bruder-EP-1SG.[NOM]</ta>
            <ta e="T39" id="Seg_5079" s="T38">sitzen-PTCP.PRS</ta>
            <ta e="T40" id="Seg_5080" s="T39">sein-PST1-3SG</ta>
            <ta e="T41" id="Seg_5081" s="T40">fünf</ta>
            <ta e="T42" id="Seg_5082" s="T41">Jahr-PROPR</ta>
            <ta e="T43" id="Seg_5083" s="T42">jüngerer.Bruder-EP-1SG.[NOM]</ta>
            <ta e="T50" id="Seg_5084" s="T49">Frau.[NOM]</ta>
            <ta e="T51" id="Seg_5085" s="T50">aber</ta>
            <ta e="T52" id="Seg_5086" s="T51">EMPH</ta>
            <ta e="T53" id="Seg_5087" s="T52">scheckig</ta>
            <ta e="T54" id="Seg_5088" s="T53">Rentier-ACC</ta>
            <ta e="T58" id="Seg_5089" s="T57">wer-VBZ-PTCP.FUT-3SG-ACC</ta>
            <ta e="T59" id="Seg_5090" s="T58">nun</ta>
            <ta e="T60" id="Seg_5091" s="T59">einspannen-PTCP.FUT-3SG-ACC</ta>
            <ta e="T62" id="Seg_5092" s="T60">nun</ta>
            <ta e="T63" id="Seg_5093" s="T62">und</ta>
            <ta e="T64" id="Seg_5094" s="T63">dann</ta>
            <ta e="T65" id="Seg_5095" s="T64">aber</ta>
            <ta e="T66" id="Seg_5096" s="T65">EMPH</ta>
            <ta e="T67" id="Seg_5097" s="T66">heilen.mit.Rentier-EP-PTCP.PST</ta>
            <ta e="T68" id="Seg_5098" s="T67">Rentier.[NOM]</ta>
            <ta e="T73" id="Seg_5099" s="T72">heilen.mit.Rentier-EP-PTCP.PST</ta>
            <ta e="T74" id="Seg_5100" s="T73">Rentier.[NOM]</ta>
            <ta e="T75" id="Seg_5101" s="T74">dies</ta>
            <ta e="T76" id="Seg_5102" s="T75">heilen.mit.Rentier-EP-PTCP.PST</ta>
            <ta e="T77" id="Seg_5103" s="T76">Rentier.[NOM]</ta>
            <ta e="T78" id="Seg_5104" s="T77">aber</ta>
            <ta e="T79" id="Seg_5105" s="T78">EMPH</ta>
            <ta e="T80" id="Seg_5106" s="T79">äh</ta>
            <ta e="T81" id="Seg_5107" s="T80">heilen.mit.Rentier-EP-PTCP.PST</ta>
            <ta e="T82" id="Seg_5108" s="T81">Rentier.[NOM]</ta>
            <ta e="T83" id="Seg_5109" s="T82">krank.sein-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T84" id="Seg_5110" s="T83">EMPH</ta>
            <ta e="T85" id="Seg_5111" s="T84">Mann-EP-1SG.[NOM]</ta>
            <ta e="T86" id="Seg_5112" s="T85">Mutter-3SG.[NOM]</ta>
            <ta e="T87" id="Seg_5113" s="T86">heilen.mit.Rentier-PTCP.HAB</ta>
            <ta e="T88" id="Seg_5114" s="T87">sein-PST1-3SG</ta>
            <ta e="T89" id="Seg_5115" s="T88">Kopf-3SG-DAT/LOC</ta>
            <ta e="T90" id="Seg_5116" s="T89">Rentier-ACC</ta>
            <ta e="T91" id="Seg_5117" s="T90">nun</ta>
            <ta e="T92" id="Seg_5118" s="T91">jenes</ta>
            <ta e="T93" id="Seg_5119" s="T92">Rentier-ACC</ta>
            <ta e="T94" id="Seg_5120" s="T93">MOD</ta>
            <ta e="T95" id="Seg_5121" s="T94">Frau.[NOM]</ta>
            <ta e="T96" id="Seg_5122" s="T95">einspannen-PTCP.FUT-3SG-ACC</ta>
            <ta e="T97" id="Seg_5123" s="T96">darf.nicht</ta>
            <ta e="T98" id="Seg_5124" s="T97">sein-PST1-3SG</ta>
            <ta e="T99" id="Seg_5125" s="T98">EMPH</ta>
            <ta e="T100" id="Seg_5126" s="T99">dann</ta>
            <ta e="T101" id="Seg_5127" s="T100">1SG.[NOM]</ta>
            <ta e="T102" id="Seg_5128" s="T101">wissen-NEG.CVB.SIM-1SG</ta>
            <ta e="T103" id="Seg_5129" s="T102">dieses</ta>
            <ta e="T104" id="Seg_5130" s="T103">Rentier-ACC</ta>
            <ta e="T105" id="Seg_5131" s="T104">linkes.Rentier-VBZ-REFL-PST2-1SG</ta>
            <ta e="T106" id="Seg_5132" s="T105">EMPH</ta>
            <ta e="T107" id="Seg_5133" s="T106">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T108" id="Seg_5134" s="T107">weiß</ta>
            <ta e="T109" id="Seg_5135" s="T108">Rentier.[NOM]</ta>
            <ta e="T110" id="Seg_5136" s="T109">grauweiß</ta>
            <ta e="T111" id="Seg_5137" s="T110">Rentier.[NOM]</ta>
            <ta e="T112" id="Seg_5138" s="T111">so</ta>
            <ta e="T113" id="Seg_5139" s="T112">aber</ta>
            <ta e="T114" id="Seg_5140" s="T113">gehen-CVB.PURP-1PL</ta>
            <ta e="T115" id="Seg_5141" s="T114">aber</ta>
            <ta e="T116" id="Seg_5142" s="T115">jenes</ta>
            <ta e="T117" id="Seg_5143" s="T116">Rentier.[NOM]</ta>
            <ta e="T118" id="Seg_5144" s="T117">EMPH</ta>
            <ta e="T119" id="Seg_5145" s="T118">beissen-CAUS-PRS.[3SG]</ta>
            <ta e="T120" id="Seg_5146" s="T119">drei-MLTP</ta>
            <ta e="T121" id="Seg_5147" s="T120">Mal.[NOM]</ta>
            <ta e="T122" id="Seg_5148" s="T121">beissen-CAUS-PRS.[3SG]</ta>
            <ta e="T123" id="Seg_5149" s="T122">EMPH</ta>
            <ta e="T124" id="Seg_5150" s="T123">dann</ta>
            <ta e="T125" id="Seg_5151" s="T124">1SG.[NOM]</ta>
            <ta e="T126" id="Seg_5152" s="T125">sagen-PRS-1SG</ta>
            <ta e="T127" id="Seg_5153" s="T126">EMPH</ta>
            <ta e="T128" id="Seg_5154" s="T127">oh.nein</ta>
            <ta e="T129" id="Seg_5155" s="T128">was-DAT/LOC</ta>
            <ta e="T130" id="Seg_5156" s="T129">beissen-CAUS-PRS.[3SG]</ta>
            <ta e="T131" id="Seg_5157" s="T130">nun</ta>
            <ta e="T132" id="Seg_5158" s="T131">sagen-PRS-1SG</ta>
            <ta e="T135" id="Seg_5159" s="T134">dieses.[NOM]</ta>
            <ta e="T136" id="Seg_5160" s="T135">Vorderteil-3SG-INSTR</ta>
            <ta e="T137" id="Seg_5161" s="T136">sich.auf.den.Weg.machen-CVB.PURP-1PL</ta>
            <ta e="T138" id="Seg_5162" s="T137">Mama-1SG.[NOM]</ta>
            <ta e="T139" id="Seg_5163" s="T138">sagen-PRS.[3SG]</ta>
            <ta e="T140" id="Seg_5164" s="T139">EMPH</ta>
            <ta e="T141" id="Seg_5165" s="T140">Feuer-1PL.[NOM]</ta>
            <ta e="T142" id="Seg_5166" s="T141">sprechen-PRS.[3SG]</ta>
            <ta e="T143" id="Seg_5167" s="T142">sagen-PRS.[3SG]</ta>
            <ta e="T144" id="Seg_5168" s="T143">Feuer-1PL.[NOM]</ta>
            <ta e="T145" id="Seg_5169" s="T144">sprechen-PRS.[3SG]</ta>
            <ta e="T148" id="Seg_5170" s="T147">aber</ta>
            <ta e="T149" id="Seg_5171" s="T148">was.[NOM]</ta>
            <ta e="T150" id="Seg_5172" s="T149">INDEF</ta>
            <ta e="T151" id="Seg_5173" s="T150">schlecht.[NOM]</ta>
            <ta e="T152" id="Seg_5174" s="T151">sein-HAB.[3SG]</ta>
            <ta e="T153" id="Seg_5175" s="T152">Feuer.[NOM]</ta>
            <ta e="T154" id="Seg_5176" s="T153">sprechen-TEMP-3SG</ta>
            <ta e="T155" id="Seg_5177" s="T154">nun</ta>
            <ta e="T157" id="Seg_5178" s="T155">sprechen-TEMP-3SG</ta>
            <ta e="T158" id="Seg_5179" s="T157">dann</ta>
            <ta e="T159" id="Seg_5180" s="T158">aber</ta>
            <ta e="T160" id="Seg_5181" s="T159">also</ta>
            <ta e="T165" id="Seg_5182" s="T164">1SG.[NOM]</ta>
            <ta e="T166" id="Seg_5183" s="T165">aber</ta>
            <ta e="T167" id="Seg_5184" s="T166">sehr</ta>
            <ta e="T168" id="Seg_5185" s="T167">EMPH</ta>
            <ta e="T169" id="Seg_5186" s="T168">jenes-ACC</ta>
            <ta e="T170" id="Seg_5187" s="T169">aber</ta>
            <ta e="T171" id="Seg_5188" s="T170">glauben-NEG.CVB.SIM-1SG</ta>
            <ta e="T172" id="Seg_5189" s="T171">aber</ta>
            <ta e="T173" id="Seg_5190" s="T172">doch</ta>
            <ta e="T174" id="Seg_5191" s="T173">Rentier.[NOM]</ta>
            <ta e="T175" id="Seg_5192" s="T174">fangen-PST1-1PL</ta>
            <ta e="T176" id="Seg_5193" s="T175">und</ta>
            <ta e="T177" id="Seg_5194" s="T176">gehen-CVB.SEQ</ta>
            <ta e="T178" id="Seg_5195" s="T177">bleiben-PST1-1PL</ta>
            <ta e="T179" id="Seg_5196" s="T178">EMPH</ta>
            <ta e="T180" id="Seg_5197" s="T179">jenes</ta>
            <ta e="T181" id="Seg_5198" s="T180">dieses</ta>
            <ta e="T182" id="Seg_5199" s="T181">Rentier-ACC</ta>
            <ta e="T183" id="Seg_5200" s="T182">fangen-PST2-1PL</ta>
            <ta e="T184" id="Seg_5201" s="T183">so</ta>
            <ta e="T185" id="Seg_5202" s="T184">gehen-CVB.SEQ</ta>
            <ta e="T186" id="Seg_5203" s="T185">bleiben-PST2-1PL</ta>
            <ta e="T187" id="Seg_5204" s="T186">EMPH</ta>
            <ta e="T188" id="Seg_5205" s="T187">Vater-1SG-ACC</ta>
            <ta e="T189" id="Seg_5206" s="T188">mit</ta>
            <ta e="T190" id="Seg_5207" s="T189">mitkommen-CVB.SEQ</ta>
            <ta e="T191" id="Seg_5208" s="T190">bleiben-PST2-1PL</ta>
            <ta e="T192" id="Seg_5209" s="T191">mitkommen-CVB.SEQ</ta>
            <ta e="T193" id="Seg_5210" s="T192">bleiben</ta>
            <ta e="T194" id="Seg_5211" s="T193">vorübergehende.Siedlung-DAT/LOC</ta>
            <ta e="T195" id="Seg_5212" s="T194">kommen-CVB.SEQ</ta>
            <ta e="T196" id="Seg_5213" s="T195">gehen-CVB.SEQ</ta>
            <ta e="T197" id="Seg_5214" s="T196">aber</ta>
            <ta e="T198" id="Seg_5215" s="T197">Vater-1SG.[NOM]</ta>
            <ta e="T199" id="Seg_5216" s="T198">Schlitten-1PL-ACC</ta>
            <ta e="T200" id="Seg_5217" s="T199">Schlitten-1PL-ACC</ta>
            <ta e="T201" id="Seg_5218" s="T200">aber</ta>
            <ta e="T202" id="Seg_5219" s="T201">einladen-REFL-PST1-1PL</ta>
            <ta e="T203" id="Seg_5220" s="T202">einladen-REFL-CVB.SEQ</ta>
            <ta e="T204" id="Seg_5221" s="T203">Gepäckschnur-VBZ-REFL-CVB.SEQ</ta>
            <ta e="T205" id="Seg_5222" s="T204">und.so.weiter-CVB.SEQ</ta>
            <ta e="T206" id="Seg_5223" s="T205">EMPH</ta>
            <ta e="T207" id="Seg_5224" s="T206">1SG-ACC</ta>
            <ta e="T208" id="Seg_5225" s="T207">doch</ta>
            <ta e="T209" id="Seg_5226" s="T208">sich.auf.den.Weg.machen-CAUS-PST1-3SG</ta>
            <ta e="T210" id="Seg_5227" s="T209">EMPH</ta>
            <ta e="T211" id="Seg_5228" s="T210">Schlitten-PROPR</ta>
            <ta e="T212" id="Seg_5229" s="T211">Rentier-1SG-ACC</ta>
            <ta e="T213" id="Seg_5230" s="T212">führen-CVB.SEQ</ta>
            <ta e="T214" id="Seg_5231" s="T213">gehen-CVB.SEQ</ta>
            <ta e="T215" id="Seg_5232" s="T214">Schlitten-PROPR</ta>
            <ta e="T216" id="Seg_5233" s="T215">Rentier-1SG-ACC</ta>
            <ta e="T217" id="Seg_5234" s="T216">führen-CVB.SEQ</ta>
            <ta e="T218" id="Seg_5235" s="T217">gehen-CVB.SEQ-1SG</ta>
            <ta e="T219" id="Seg_5236" s="T218">EMPH</ta>
            <ta e="T220" id="Seg_5237" s="T219">Fehler</ta>
            <ta e="T221" id="Seg_5238" s="T220">sich.setzen-PRS-1SG</ta>
            <ta e="T222" id="Seg_5239" s="T221">EMPH</ta>
            <ta e="T223" id="Seg_5240" s="T222">Schlitten-1SG-DAT/LOC</ta>
            <ta e="T224" id="Seg_5241" s="T223">EMPH</ta>
            <ta e="T225" id="Seg_5242" s="T224">Fisch-PL-ACC</ta>
            <ta e="T226" id="Seg_5243" s="T225">einladen-PST2-1SG</ta>
            <ta e="T227" id="Seg_5244" s="T226">sechs</ta>
            <ta e="T228" id="Seg_5245" s="T227">Sack.[NOM]</ta>
            <ta e="T229" id="Seg_5246" s="T228">Fisch.[NOM]</ta>
            <ta e="T230" id="Seg_5247" s="T229">sechs</ta>
            <ta e="T231" id="Seg_5248" s="T230">Sack.[NOM]</ta>
            <ta e="T232" id="Seg_5249" s="T231">Fisch.[NOM]</ta>
            <ta e="T233" id="Seg_5250" s="T232">einladen-CVB.SEQ</ta>
            <ta e="T234" id="Seg_5251" s="T233">gehen-CVB.SEQ</ta>
            <ta e="T235" id="Seg_5252" s="T234">aber</ta>
            <ta e="T236" id="Seg_5253" s="T235">Schlitten-PL.[NOM]</ta>
            <ta e="T237" id="Seg_5254" s="T236">springen-PRS-1SG</ta>
            <ta e="T238" id="Seg_5255" s="T237">sagen-PRS-1SG</ta>
            <ta e="T239" id="Seg_5256" s="T238">EMPH</ta>
            <ta e="T240" id="Seg_5257" s="T239">Schlitten-1SG-ACC</ta>
            <ta e="T241" id="Seg_5258" s="T240">Fehler</ta>
            <ta e="T242" id="Seg_5259" s="T241">sich.setzen-PRS-1SG</ta>
            <ta e="T243" id="Seg_5260" s="T242">EMPH</ta>
            <ta e="T244" id="Seg_5261" s="T243">nein</ta>
            <ta e="T245" id="Seg_5262" s="T244">dieses</ta>
            <ta e="T246" id="Seg_5263" s="T245">Mensch.[NOM]</ta>
            <ta e="T247" id="Seg_5264" s="T246">EMPH</ta>
            <ta e="T248" id="Seg_5265" s="T247">noch</ta>
            <ta e="T249" id="Seg_5266" s="T248">und</ta>
            <ta e="T250" id="Seg_5267" s="T249">springen-PRS-1SG</ta>
            <ta e="T251" id="Seg_5268" s="T250">EMPH</ta>
            <ta e="T253" id="Seg_5269" s="T252">springen-PRS-1SG</ta>
            <ta e="T255" id="Seg_5270" s="T253">EMPH</ta>
            <ta e="T256" id="Seg_5271" s="T255">wieder</ta>
            <ta e="T257" id="Seg_5272" s="T256">Fehler</ta>
            <ta e="T258" id="Seg_5273" s="T257">sich.setzen-PRS-1SG</ta>
            <ta e="T259" id="Seg_5274" s="T258">EMPH</ta>
            <ta e="T260" id="Seg_5275" s="T259">Schlitten-PROPR</ta>
            <ta e="T261" id="Seg_5276" s="T260">Rentier-1SG-ACC</ta>
            <ta e="T262" id="Seg_5277" s="T261">fallen-CAUS-CVB.SEQ</ta>
            <ta e="T263" id="Seg_5278" s="T262">lassen-PST1-1SG</ta>
            <ta e="T264" id="Seg_5279" s="T263">EMPH</ta>
            <ta e="T265" id="Seg_5280" s="T264">so</ta>
            <ta e="T266" id="Seg_5281" s="T265">Schlitten-PROPR</ta>
            <ta e="T267" id="Seg_5282" s="T266">Rentier-1SG.[NOM]</ta>
            <ta e="T268" id="Seg_5283" s="T267">aber</ta>
            <ta e="T269" id="Seg_5284" s="T268">Schlitten-1SG.[NOM]</ta>
            <ta e="T270" id="Seg_5285" s="T269">Rentier-PL-3SG.[NOM]</ta>
            <ta e="T271" id="Seg_5286" s="T270">oberer.Teil-1SG-INSTR</ta>
            <ta e="T272" id="Seg_5287" s="T271">gehen-PRS-3PL</ta>
            <ta e="T273" id="Seg_5288" s="T272">EMPH</ta>
            <ta e="T274" id="Seg_5289" s="T273">und</ta>
            <ta e="T275" id="Seg_5290" s="T274">selbst-1SG.[NOM]</ta>
            <ta e="T276" id="Seg_5291" s="T275">aber</ta>
            <ta e="T277" id="Seg_5292" s="T276">EMPH</ta>
            <ta e="T278" id="Seg_5293" s="T277">Schlitten.[NOM]</ta>
            <ta e="T279" id="Seg_5294" s="T278">Stützbalken.im.Schlitten-3SG-ACC</ta>
            <ta e="T280" id="Seg_5295" s="T279">jeder-INSTR</ta>
            <ta e="T281" id="Seg_5296" s="T280">drei-MLTP</ta>
            <ta e="T282" id="Seg_5297" s="T281">Mal.[NOM]</ta>
            <ta e="T283" id="Seg_5298" s="T282">sich.drehen-PST1-1SG</ta>
            <ta e="T284" id="Seg_5299" s="T283">EMPH</ta>
            <ta e="T285" id="Seg_5300" s="T284">oh.nein</ta>
            <ta e="T286" id="Seg_5301" s="T285">so</ta>
            <ta e="T287" id="Seg_5302" s="T286">EMPH</ta>
            <ta e="T288" id="Seg_5303" s="T287">auf.dem.Rücken</ta>
            <ta e="T289" id="Seg_5304" s="T288">fallen-CVB.SEQ</ta>
            <ta e="T290" id="Seg_5305" s="T289">bleiben</ta>
            <ta e="T291" id="Seg_5306" s="T290">auf.dem.Rücken</ta>
            <ta e="T292" id="Seg_5307" s="T291">fallen-CVB.SEQ</ta>
            <ta e="T293" id="Seg_5308" s="T292">gehen-CVB.SEQ</ta>
            <ta e="T294" id="Seg_5309" s="T293">EMPH</ta>
            <ta e="T295" id="Seg_5310" s="T294">liegen-PRS-1SG</ta>
            <ta e="T296" id="Seg_5311" s="T295">EMPH</ta>
            <ta e="T297" id="Seg_5312" s="T296">Vater-1SG.[NOM]</ta>
            <ta e="T298" id="Seg_5313" s="T297">langer.Mantel.[NOM]</ta>
            <ta e="T299" id="Seg_5314" s="T298">anziehen-PST1-3SG</ta>
            <ta e="T300" id="Seg_5315" s="T299">stehen-PTCP.PRS</ta>
            <ta e="T301" id="Seg_5316" s="T300">sein-PST1-3SG</ta>
            <ta e="T302" id="Seg_5317" s="T301">EMPH</ta>
            <ta e="T303" id="Seg_5318" s="T302">langer.Mantel.[NOM]</ta>
            <ta e="T304" id="Seg_5319" s="T303">anziehen-CVB.SEQ</ta>
            <ta e="T305" id="Seg_5320" s="T304">gehen-CVB.SEQ</ta>
            <ta e="T306" id="Seg_5321" s="T305">dieses-INTNS.[NOM]</ta>
            <ta e="T307" id="Seg_5322" s="T306">sehen-PST2-3SG</ta>
            <ta e="T308" id="Seg_5323" s="T307">sehen-PRS.[3SG]</ta>
            <ta e="T309" id="Seg_5324" s="T308">Schlitten-PROPR</ta>
            <ta e="T310" id="Seg_5325" s="T309">Rentier.[NOM]</ta>
            <ta e="T311" id="Seg_5326" s="T310">gehen-CVB.SIM</ta>
            <ta e="T312" id="Seg_5327" s="T311">stehen-PRS.[3SG]</ta>
            <ta e="T313" id="Seg_5328" s="T312">Mensch-3SG.[NOM]</ta>
            <ta e="T314" id="Seg_5329" s="T313">EMPH</ta>
            <ta e="T315" id="Seg_5330" s="T314">hier</ta>
            <ta e="T316" id="Seg_5331" s="T315">liegen-PRS.[3SG]</ta>
            <ta e="T317" id="Seg_5332" s="T316">Weg-DAT/LOC</ta>
            <ta e="T318" id="Seg_5333" s="T317">doch</ta>
            <ta e="T319" id="Seg_5334" s="T318">laufen-CVB.SEQ</ta>
            <ta e="T320" id="Seg_5335" s="T319">kommen-PST1-3SG</ta>
            <ta e="T321" id="Seg_5336" s="T320">EMPH</ta>
            <ta e="T322" id="Seg_5337" s="T321">so</ta>
            <ta e="T323" id="Seg_5338" s="T322">sagen-PST1-3SG</ta>
            <ta e="T324" id="Seg_5339" s="T323">was.für.ein</ta>
            <ta e="T325" id="Seg_5340" s="T324">2SG.[NOM]</ta>
            <ta e="T326" id="Seg_5341" s="T325">Atem-PROPR-2SG</ta>
            <ta e="T327" id="Seg_5342" s="T326">Q</ta>
            <ta e="T328" id="Seg_5343" s="T327">sagen-PST1-3SG</ta>
            <ta e="T329" id="Seg_5344" s="T328">AFFIRM</ta>
            <ta e="T330" id="Seg_5345" s="T329">Atem-PROPR-1SG</ta>
            <ta e="T331" id="Seg_5346" s="T330">Atem-PROPR-1SG</ta>
            <ta e="T332" id="Seg_5347" s="T331">Bein-EP-1SG.[NOM]</ta>
            <ta e="T333" id="Seg_5348" s="T332">nur</ta>
            <ta e="T334" id="Seg_5349" s="T333">können-CVB.SEQ</ta>
            <ta e="T335" id="Seg_5350" s="T334">sich.bewegen-NEG.[3SG]</ta>
            <ta e="T336" id="Seg_5351" s="T335">AFFIRM</ta>
            <ta e="T337" id="Seg_5352" s="T336">doch</ta>
            <ta e="T338" id="Seg_5353" s="T337">warte</ta>
            <ta e="T339" id="Seg_5354" s="T338">1SG.[NOM]</ta>
            <ta e="T340" id="Seg_5355" s="T339">kommen-FUT-1SG</ta>
            <ta e="T341" id="Seg_5356" s="T340">doch</ta>
            <ta e="T342" id="Seg_5357" s="T341">kommen-CVB.SEQ</ta>
            <ta e="T343" id="Seg_5358" s="T342">gehen-CVB.SEQ</ta>
            <ta e="T344" id="Seg_5359" s="T343">Schlitten-PROPR</ta>
            <ta e="T345" id="Seg_5360" s="T344">Rentier-3SG-DAT/LOC</ta>
            <ta e="T346" id="Seg_5361" s="T345">wie</ta>
            <ta e="T347" id="Seg_5362" s="T346">INDEF</ta>
            <ta e="T348" id="Seg_5363" s="T347">besteigen-CVB.SEQ</ta>
            <ta e="T349" id="Seg_5364" s="T348">gehen-CVB.SEQ</ta>
            <ta e="T350" id="Seg_5365" s="T349">EMPH</ta>
            <ta e="T351" id="Seg_5366" s="T350">1SG.[NOM]</ta>
            <ta e="T352" id="Seg_5367" s="T351">Schlitten-PROPR</ta>
            <ta e="T353" id="Seg_5368" s="T352">Rentier-1SG-ACC</ta>
            <ta e="T354" id="Seg_5369" s="T353">verfolgen-CVB.SIM</ta>
            <ta e="T355" id="Seg_5370" s="T354">gehen-PRS-1PL</ta>
            <ta e="T356" id="Seg_5371" s="T355">EMPH</ta>
            <ta e="T357" id="Seg_5372" s="T356">dort</ta>
            <ta e="T358" id="Seg_5373" s="T357">EMPH</ta>
            <ta e="T359" id="Seg_5374" s="T358">See-ACC</ta>
            <ta e="T360" id="Seg_5375" s="T359">klettern-CVB.PURP-1PL</ta>
            <ta e="T361" id="Seg_5376" s="T360">Hang-DAT/LOC</ta>
            <ta e="T362" id="Seg_5377" s="T361">Hang.[NOM]</ta>
            <ta e="T363" id="Seg_5378" s="T362">entlang</ta>
            <ta e="T364" id="Seg_5379" s="T363">aber</ta>
            <ta e="T367" id="Seg_5380" s="T366">jüngerer.Bruder-EP-1SG.[NOM]</ta>
            <ta e="T368" id="Seg_5381" s="T367">es.gibt</ta>
            <ta e="T369" id="Seg_5382" s="T368">sein-PST1-3SG</ta>
            <ta e="T370" id="Seg_5383" s="T369">jenes</ta>
            <ta e="T371" id="Seg_5384" s="T370">Unglück-PROPR</ta>
            <ta e="T372" id="Seg_5385" s="T371">Schlitten-ABL</ta>
            <ta e="T373" id="Seg_5386" s="T372">fallen-CVB.SEQ</ta>
            <ta e="T374" id="Seg_5387" s="T373">nachdem</ta>
            <ta e="T375" id="Seg_5388" s="T374">Zügel-ACC</ta>
            <ta e="T376" id="Seg_5389" s="T375">fangen-CVB.SEQ</ta>
            <ta e="T377" id="Seg_5390" s="T376">nehmen-PST1-3SG</ta>
            <ta e="T378" id="Seg_5391" s="T377">so</ta>
            <ta e="T381" id="Seg_5392" s="T380">Rentier-PL.[NOM]</ta>
            <ta e="T382" id="Seg_5393" s="T381">halten-PST1-3PL</ta>
            <ta e="T386" id="Seg_5394" s="T385">1PL.[NOM]</ta>
            <ta e="T387" id="Seg_5395" s="T386">kommen-PRS-1PL</ta>
            <ta e="T389" id="Seg_5396" s="T387">EMPH</ta>
            <ta e="T390" id="Seg_5397" s="T389">Vater-1SG.[NOM]</ta>
            <ta e="T391" id="Seg_5398" s="T390">Schlitten-PROPR</ta>
            <ta e="T392" id="Seg_5399" s="T391">Rentier-1SG-ACC</ta>
            <ta e="T393" id="Seg_5400" s="T392">binden-CVB.SEQ</ta>
            <ta e="T394" id="Seg_5401" s="T393">werfen-PST1-3SG</ta>
            <ta e="T395" id="Seg_5402" s="T394">mein-3SG-ACC</ta>
            <ta e="T396" id="Seg_5403" s="T395">so</ta>
            <ta e="T397" id="Seg_5404" s="T396">machen-CVB.SEQ</ta>
            <ta e="T398" id="Seg_5405" s="T397">gehen-CVB.SEQ</ta>
            <ta e="T399" id="Seg_5406" s="T398">selbst-3SG.[NOM]</ta>
            <ta e="T400" id="Seg_5407" s="T399">EMPH</ta>
            <ta e="T401" id="Seg_5408" s="T400">1SG.[NOM]</ta>
            <ta e="T402" id="Seg_5409" s="T401">Schlitten-PROPR</ta>
            <ta e="T403" id="Seg_5410" s="T402">Rentier-PL.[NOM]</ta>
            <ta e="T404" id="Seg_5411" s="T403">besteigen-CVB.SEQ</ta>
            <ta e="T405" id="Seg_5412" s="T404">gehen-CVB.SEQ</ta>
            <ta e="T406" id="Seg_5413" s="T405">jenes</ta>
            <ta e="T407" id="Seg_5414" s="T406">Haus-1PL-DAT/LOC</ta>
            <ta e="T408" id="Seg_5415" s="T407">gehen-PST1-1PL</ta>
            <ta e="T409" id="Seg_5416" s="T408">EMPH</ta>
            <ta e="T410" id="Seg_5417" s="T409">Haus-1PL-DAT/LOC</ta>
            <ta e="T411" id="Seg_5418" s="T410">gehen-CVB.SEQ</ta>
            <ta e="T412" id="Seg_5419" s="T411">gehen-CVB.SEQ</ta>
            <ta e="T413" id="Seg_5420" s="T412">aber</ta>
            <ta e="T414" id="Seg_5421" s="T413">wer-DAT/LOC</ta>
            <ta e="T415" id="Seg_5422" s="T414">Tür-1SG-DAT/LOC</ta>
            <ta e="T416" id="Seg_5423" s="T415">halten-CAUS-PRS.[3SG]</ta>
            <ta e="T417" id="Seg_5424" s="T416">EMPH</ta>
            <ta e="T418" id="Seg_5425" s="T417">Balok-EP-1SG.[NOM]</ta>
            <ta e="T419" id="Seg_5426" s="T418">Tür-3SG-DAT/LOC</ta>
            <ta e="T420" id="Seg_5427" s="T419">nun</ta>
            <ta e="T421" id="Seg_5428" s="T420">so</ta>
            <ta e="T422" id="Seg_5429" s="T421">EMPH</ta>
            <ta e="T423" id="Seg_5430" s="T422">Alte-PL.[NOM]</ta>
            <ta e="T424" id="Seg_5431" s="T423">zwei</ta>
            <ta e="T425" id="Seg_5432" s="T424">Alte.[NOM]</ta>
            <ta e="T426" id="Seg_5433" s="T425">es.gibt</ta>
            <ta e="T427" id="Seg_5434" s="T426">sein-PST1-3SG</ta>
            <ta e="T429" id="Seg_5435" s="T428">jenes-PL.[NOM]</ta>
            <ta e="T430" id="Seg_5436" s="T429">kommen-PST1-3PL</ta>
            <ta e="T431" id="Seg_5437" s="T430">na</ta>
            <ta e="T432" id="Seg_5438" s="T431">EMPH</ta>
            <ta e="T433" id="Seg_5439" s="T432">was.[NOM]</ta>
            <ta e="T434" id="Seg_5440" s="T433">sein-PST1-2PL</ta>
            <ta e="T435" id="Seg_5441" s="T434">wie</ta>
            <ta e="T436" id="Seg_5442" s="T435">wie</ta>
            <ta e="T438" id="Seg_5443" s="T436">sein-PST1-2PL</ta>
            <ta e="T439" id="Seg_5444" s="T438">oh.nein</ta>
            <ta e="T440" id="Seg_5445" s="T439">dieses</ta>
            <ta e="T441" id="Seg_5446" s="T440">1SG-DAT/LOC</ta>
            <ta e="T442" id="Seg_5447" s="T441">gerade</ta>
            <ta e="T443" id="Seg_5448" s="T442">dieses</ta>
            <ta e="T444" id="Seg_5449" s="T443">Frau.[NOM]</ta>
            <ta e="T445" id="Seg_5450" s="T444">verletzen-EP-REFL-PST1-3SG</ta>
            <ta e="T446" id="Seg_5451" s="T445">dieses-ACC</ta>
            <ta e="T447" id="Seg_5452" s="T446">man.muss</ta>
            <ta e="T448" id="Seg_5453" s="T447">Doktor-PART</ta>
            <ta e="T449" id="Seg_5454" s="T448">rufen-EP-REFL-PTCP.FUT-DAT/LOC</ta>
            <ta e="T450" id="Seg_5455" s="T449">dieses-ACC</ta>
            <ta e="T451" id="Seg_5456" s="T450">Vater-1SG.[NOM]</ta>
            <ta e="T452" id="Seg_5457" s="T451">Mama-1SG.[NOM]</ta>
            <ta e="T453" id="Seg_5458" s="T452">sagen-PRS.[3SG]</ta>
            <ta e="T454" id="Seg_5459" s="T453">gehen-EP-NEG.[IMP.2SG]</ta>
            <ta e="T455" id="Seg_5460" s="T454">gehen-EP-NEG.[IMP.2SG]</ta>
            <ta e="T456" id="Seg_5461" s="T455">Doktor-ACC</ta>
            <ta e="T457" id="Seg_5462" s="T456">was-EP-ACC</ta>
            <ta e="T458" id="Seg_5463" s="T457">sagen-PRS.[3SG]</ta>
            <ta e="T459" id="Seg_5464" s="T458">Sünde.[NOM]</ta>
            <ta e="T460" id="Seg_5465" s="T459">EMPH</ta>
            <ta e="T461" id="Seg_5466" s="T460">gut.[NOM]</ta>
            <ta e="T462" id="Seg_5467" s="T461">Lungen-3SG.[NOM]</ta>
            <ta e="T463" id="Seg_5468" s="T462">breit</ta>
            <ta e="T466" id="Seg_5469" s="T465">gut.[NOM]</ta>
            <ta e="T467" id="Seg_5470" s="T466">warte</ta>
            <ta e="T468" id="Seg_5471" s="T467">dann</ta>
            <ta e="T469" id="Seg_5472" s="T468">1SG-ACC</ta>
            <ta e="T470" id="Seg_5473" s="T469">zwei</ta>
            <ta e="T471" id="Seg_5474" s="T470">Seite-ABL</ta>
            <ta e="T472" id="Seg_5475" s="T471">heben-CVB.SEQ</ta>
            <ta e="T473" id="Seg_5476" s="T472">gehen-CVB.SEQ</ta>
            <ta e="T474" id="Seg_5477" s="T473">Haus.[NOM]</ta>
            <ta e="T475" id="Seg_5478" s="T474">Balok-DAT/LOC</ta>
            <ta e="T476" id="Seg_5479" s="T475">zurückkommen-CAUS-PST1-3PL</ta>
            <ta e="T477" id="Seg_5480" s="T476">EMPH</ta>
            <ta e="T478" id="Seg_5481" s="T477">AFFIRM</ta>
            <ta e="T479" id="Seg_5482" s="T478">doch</ta>
            <ta e="T480" id="Seg_5483" s="T479">jenes</ta>
            <ta e="T481" id="Seg_5484" s="T480">nackt-VBZ-CVB.SEQ</ta>
            <ta e="T482" id="Seg_5485" s="T481">und.so.weiter-CVB.SEQ</ta>
            <ta e="T483" id="Seg_5486" s="T482">gehen-CVB.SEQ</ta>
            <ta e="T484" id="Seg_5487" s="T483">doch</ta>
            <ta e="T485" id="Seg_5488" s="T484">was.[NOM]</ta>
            <ta e="T486" id="Seg_5489" s="T485">Doktor-3SG-ACC</ta>
            <ta e="T487" id="Seg_5490" s="T486">EMPH-3SG-ACC</ta>
            <ta e="T488" id="Seg_5491" s="T487">warte</ta>
            <ta e="T489" id="Seg_5492" s="T488">gut.[NOM]</ta>
            <ta e="T490" id="Seg_5493" s="T489">vielleicht</ta>
            <ta e="T491" id="Seg_5494" s="T490">und</ta>
            <ta e="T492" id="Seg_5495" s="T491">einfach</ta>
            <ta e="T493" id="Seg_5496" s="T492">EMPH</ta>
            <ta e="T494" id="Seg_5497" s="T493">offenbar</ta>
            <ta e="T495" id="Seg_5498" s="T494">was-3SG.[NOM]</ta>
            <ta e="T496" id="Seg_5499" s="T495">NEG</ta>
            <ta e="T497" id="Seg_5500" s="T496">so.wie</ta>
            <ta e="T498" id="Seg_5501" s="T497">brechen-EP-PST2.NEG.[3SG]</ta>
            <ta e="T499" id="Seg_5502" s="T498">Blut.[NOM]</ta>
            <ta e="T500" id="Seg_5503" s="T499">NEG</ta>
            <ta e="T501" id="Seg_5504" s="T500">NEG.EX</ta>
            <ta e="T502" id="Seg_5505" s="T501">was.[NOM]</ta>
            <ta e="T503" id="Seg_5506" s="T502">NEG</ta>
            <ta e="T504" id="Seg_5507" s="T503">NEG.EX</ta>
            <ta e="T505" id="Seg_5508" s="T504">gut.[NOM]</ta>
            <ta e="T506" id="Seg_5509" s="T505">sein-FUT.[3SG]</ta>
            <ta e="T507" id="Seg_5510" s="T506">dann</ta>
            <ta e="T508" id="Seg_5511" s="T507">AFFIRM</ta>
            <ta e="T509" id="Seg_5512" s="T508">doch</ta>
            <ta e="T510" id="Seg_5513" s="T509">so</ta>
            <ta e="T511" id="Seg_5514" s="T510">jenes-ABL</ta>
            <ta e="T512" id="Seg_5515" s="T511">jenes</ta>
            <ta e="T513" id="Seg_5516" s="T512">doch</ta>
            <ta e="T514" id="Seg_5517" s="T513">jenes-ABL</ta>
            <ta e="T515" id="Seg_5518" s="T514">jenes-ABL</ta>
            <ta e="T516" id="Seg_5519" s="T515">etwas.später</ta>
            <ta e="T517" id="Seg_5520" s="T516">später</ta>
            <ta e="T518" id="Seg_5521" s="T517">sehr</ta>
            <ta e="T519" id="Seg_5522" s="T518">doch</ta>
            <ta e="T520" id="Seg_5523" s="T519">Monat.[NOM]</ta>
            <ta e="T521" id="Seg_5524" s="T520">vorbeigehen-PST1-3SG</ta>
            <ta e="T522" id="Seg_5525" s="T521">EMPH</ta>
            <ta e="T523" id="Seg_5526" s="T522">Monat.[NOM]</ta>
            <ta e="T524" id="Seg_5527" s="T523">vorbeigehen-CVB.SEQ</ta>
            <ta e="T525" id="Seg_5528" s="T524">gehen-CVB.SEQ</ta>
            <ta e="T526" id="Seg_5529" s="T525">jenes.EMPH-INSTR</ta>
            <ta e="T527" id="Seg_5530" s="T526">Doktor</ta>
            <ta e="T528" id="Seg_5531" s="T527">NEG</ta>
            <ta e="T529" id="Seg_5532" s="T528">NEG.EX</ta>
            <ta e="T530" id="Seg_5533" s="T529">was.[NOM]</ta>
            <ta e="T531" id="Seg_5534" s="T530">NEG</ta>
            <ta e="T532" id="Seg_5535" s="T531">NEG.EX</ta>
            <ta e="T533" id="Seg_5536" s="T532">Monat.[NOM]</ta>
            <ta e="T534" id="Seg_5537" s="T533">vorbeigehen-CVB.SEQ</ta>
            <ta e="T535" id="Seg_5538" s="T534">gehen-CVB.SEQ</ta>
            <ta e="T536" id="Seg_5539" s="T535">doch</ta>
            <ta e="T537" id="Seg_5540" s="T536">jenes</ta>
            <ta e="T538" id="Seg_5541" s="T537">neu</ta>
            <ta e="T539" id="Seg_5542" s="T538">Jahr-DAT/LOC</ta>
            <ta e="T540" id="Seg_5543" s="T539">hineingehen-CVB.PURP</ta>
            <ta e="T541" id="Seg_5544" s="T540">doch</ta>
            <ta e="T542" id="Seg_5545" s="T541">Doktor-DAT/LOC</ta>
            <ta e="T543" id="Seg_5546" s="T542">hineingehen-PST1-1PL</ta>
            <ta e="T544" id="Seg_5547" s="T543">EMPH</ta>
            <ta e="T545" id="Seg_5548" s="T544">dort</ta>
            <ta e="T546" id="Seg_5549" s="T545">hineingehen-CVB.SEQ-1PL</ta>
            <ta e="T547" id="Seg_5550" s="T546">jenes</ta>
            <ta e="T548" id="Seg_5551" s="T547">na</ta>
            <ta e="T549" id="Seg_5552" s="T548">Doktor-EP-1SG.[NOM]</ta>
            <ta e="T550" id="Seg_5553" s="T549">sagen-PRS.[3SG]</ta>
            <ta e="T551" id="Seg_5554" s="T550">EMPH</ta>
            <ta e="T552" id="Seg_5555" s="T551">na</ta>
            <ta e="T553" id="Seg_5556" s="T552">dein</ta>
            <ta e="T554" id="Seg_5557" s="T553">Hüfte-EP-2SG.[NOM]</ta>
            <ta e="T555" id="Seg_5558" s="T554">Hüfte-EP-2SG.[NOM]</ta>
            <ta e="T556" id="Seg_5559" s="T555">brechen-EP-PTCP.PST</ta>
            <ta e="T557" id="Seg_5560" s="T556">sein-PST2.[3SG]</ta>
            <ta e="T558" id="Seg_5561" s="T557">2SG-ACC</ta>
            <ta e="T559" id="Seg_5562" s="T558">man.muss</ta>
            <ta e="T560" id="Seg_5563" s="T559">Chatanga-DAT/LOC</ta>
            <ta e="T561" id="Seg_5564" s="T560">schicken-PTCP.FUT-DAT/LOC</ta>
            <ta e="T562" id="Seg_5565" s="T561">so</ta>
            <ta e="T563" id="Seg_5566" s="T562">dann</ta>
            <ta e="T564" id="Seg_5567" s="T563">Chatanga-DAT/LOC</ta>
            <ta e="T565" id="Seg_5568" s="T564">schicken-PST2-3PL</ta>
            <ta e="T566" id="Seg_5569" s="T565">Chatanga.[NOM]</ta>
            <ta e="T567" id="Seg_5570" s="T566">1SG.[NOM]</ta>
            <ta e="T568" id="Seg_5571" s="T567">liegen-PST1-1SG</ta>
            <ta e="T569" id="Seg_5572" s="T568">EMPH</ta>
            <ta e="T570" id="Seg_5573" s="T569">dort</ta>
            <ta e="T571" id="Seg_5574" s="T570">doch</ta>
            <ta e="T572" id="Seg_5575" s="T571">zwei</ta>
            <ta e="T573" id="Seg_5576" s="T572">Monat-ACC</ta>
            <ta e="T574" id="Seg_5577" s="T573">Streckverband-DAT/LOC</ta>
            <ta e="T575" id="Seg_5578" s="T574">liegen-PST1-1SG</ta>
            <ta e="T576" id="Seg_5579" s="T575">EMPH</ta>
            <ta e="T577" id="Seg_5580" s="T576">jenes-ABL</ta>
            <ta e="T578" id="Seg_5581" s="T577">EMPH</ta>
            <ta e="T579" id="Seg_5582" s="T578">anderer.von.zwei</ta>
            <ta e="T580" id="Seg_5583" s="T579">Monat-PL-3SG-ACC</ta>
            <ta e="T581" id="Seg_5584" s="T580">EMPH</ta>
            <ta e="T582" id="Seg_5585" s="T581">sehen.[IMP.2SG]</ta>
            <ta e="T583" id="Seg_5586" s="T582">acht</ta>
            <ta e="T584" id="Seg_5587" s="T583">Monat-ACC</ta>
            <ta e="T585" id="Seg_5588" s="T584">ganz</ta>
            <ta e="T586" id="Seg_5589" s="T585">liegen-PST1-1SG</ta>
            <ta e="T587" id="Seg_5590" s="T586">EMPH</ta>
            <ta e="T588" id="Seg_5591" s="T587">Chatanga-DAT/LOC</ta>
            <ta e="T589" id="Seg_5592" s="T614">Haus-1SG-DAT/LOC</ta>
            <ta e="T590" id="Seg_5593" s="T589">kommen-PST2-EP-1SG</ta>
            <ta e="T591" id="Seg_5594" s="T590">und</ta>
            <ta e="T592" id="Seg_5595" s="T591">Bein-EP-1SG.[NOM]</ta>
            <ta e="T598" id="Seg_5596" s="T597">sein-PST1-3SG</ta>
            <ta e="T599" id="Seg_5597" s="T598">jenes.[NOM]</ta>
            <ta e="T600" id="Seg_5598" s="T599">ähnlich</ta>
            <ta e="T601" id="Seg_5599" s="T600">EMPH-dann</ta>
            <ta e="T602" id="Seg_5600" s="T601">gesund.werden-PST2.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_5601" s="T0">ребенок.[NOM]</ta>
            <ta e="T2" id="Seg_5602" s="T1">быть-TEMP-1SG</ta>
            <ta e="T3" id="Seg_5603" s="T2">однако</ta>
            <ta e="T4" id="Seg_5604" s="T3">EMPH</ta>
            <ta e="T5" id="Seg_5605" s="T4">ребенок-PL-EP-1SG.[NOM]</ta>
            <ta e="T6" id="Seg_5606" s="T5">маленький.[NOM]</ta>
            <ta e="T7" id="Seg_5607" s="T6">быть-PST1-3PL</ta>
            <ta e="T8" id="Seg_5608" s="T7">один</ta>
            <ta e="T9" id="Seg_5609" s="T8">ребенок-1SG.[NOM]</ta>
            <ta e="T10" id="Seg_5610" s="T9">год-PROPR.[NOM]</ta>
            <ta e="T11" id="Seg_5611" s="T10">быть-PST1-3SG</ta>
            <ta e="T12" id="Seg_5612" s="T11">год-ABL</ta>
            <ta e="T13" id="Seg_5613" s="T12">остаток-PROPR</ta>
            <ta e="T17" id="Seg_5614" s="T16">один</ta>
            <ta e="T18" id="Seg_5615" s="T17">ребенок-1SG.[NOM]</ta>
            <ta e="T19" id="Seg_5616" s="T18">однако</ta>
            <ta e="T20" id="Seg_5617" s="T19">восемь</ta>
            <ta e="T21" id="Seg_5618" s="T20">месяц-PROPR.[NOM]</ta>
            <ta e="T22" id="Seg_5619" s="T21">быть-PST1-3SG</ta>
            <ta e="T23" id="Seg_5620" s="T22">тогда</ta>
            <ta e="T24" id="Seg_5621" s="T23">однако</ta>
            <ta e="T25" id="Seg_5622" s="T24">EMPH</ta>
            <ta e="T26" id="Seg_5623" s="T25">отец-1SG.[NOM]</ta>
            <ta e="T27" id="Seg_5624" s="T26">дерево.[NOM]</ta>
            <ta e="T28" id="Seg_5625" s="T27">принести-CVB.SIM</ta>
            <ta e="T29" id="Seg_5626" s="T28">идти-CVB.SEQ-1SG</ta>
            <ta e="T30" id="Seg_5627" s="T29">отец-1SG.[NOM]</ta>
            <ta e="T31" id="Seg_5628" s="T30">с</ta>
            <ta e="T32" id="Seg_5629" s="T31">сопровождать-EP-PTCP.PST</ta>
            <ta e="T33" id="Seg_5630" s="T32">быть-PST1-1SG</ta>
            <ta e="T34" id="Seg_5631" s="T33">нарта-PROPR-1SG</ta>
            <ta e="T35" id="Seg_5632" s="T34">нарта-PROPR-1SG</ta>
            <ta e="T36" id="Seg_5633" s="T35">нарта-1SG-DAT/LOC</ta>
            <ta e="T37" id="Seg_5634" s="T36">однако</ta>
            <ta e="T38" id="Seg_5635" s="T37">младший.брат-EP-1SG.[NOM]</ta>
            <ta e="T39" id="Seg_5636" s="T38">сидеть-PTCP.PRS</ta>
            <ta e="T40" id="Seg_5637" s="T39">быть-PST1-3SG</ta>
            <ta e="T41" id="Seg_5638" s="T40">пять</ta>
            <ta e="T42" id="Seg_5639" s="T41">год-PROPR</ta>
            <ta e="T43" id="Seg_5640" s="T42">младший.брат-EP-1SG.[NOM]</ta>
            <ta e="T50" id="Seg_5641" s="T49">жена.[NOM]</ta>
            <ta e="T51" id="Seg_5642" s="T50">однако</ta>
            <ta e="T52" id="Seg_5643" s="T51">EMPH</ta>
            <ta e="T53" id="Seg_5644" s="T52">пятнистый</ta>
            <ta e="T54" id="Seg_5645" s="T53">олень-ACC</ta>
            <ta e="T58" id="Seg_5646" s="T57">кто-VBZ-PTCP.FUT-3SG-ACC</ta>
            <ta e="T59" id="Seg_5647" s="T58">вот</ta>
            <ta e="T60" id="Seg_5648" s="T59">запрячь-PTCP.FUT-3SG-ACC</ta>
            <ta e="T62" id="Seg_5649" s="T60">вот</ta>
            <ta e="T63" id="Seg_5650" s="T62">и</ta>
            <ta e="T64" id="Seg_5651" s="T63">тогда</ta>
            <ta e="T65" id="Seg_5652" s="T64">однако</ta>
            <ta e="T66" id="Seg_5653" s="T65">EMPH</ta>
            <ta e="T67" id="Seg_5654" s="T66">лечить.оленем-EP-PTCP.PST</ta>
            <ta e="T68" id="Seg_5655" s="T67">олень.[NOM]</ta>
            <ta e="T73" id="Seg_5656" s="T72">лечить.оленем-EP-PTCP.PST</ta>
            <ta e="T74" id="Seg_5657" s="T73">олень.[NOM]</ta>
            <ta e="T75" id="Seg_5658" s="T74">это</ta>
            <ta e="T76" id="Seg_5659" s="T75">лечить.оленем-EP-PTCP.PST</ta>
            <ta e="T77" id="Seg_5660" s="T76">олень.[NOM]</ta>
            <ta e="T78" id="Seg_5661" s="T77">однако</ta>
            <ta e="T79" id="Seg_5662" s="T78">EMPH</ta>
            <ta e="T80" id="Seg_5663" s="T79">ээ</ta>
            <ta e="T81" id="Seg_5664" s="T80">лечить.оленем-EP-PTCP.PST</ta>
            <ta e="T82" id="Seg_5665" s="T81">олень.[NOM]</ta>
            <ta e="T83" id="Seg_5666" s="T82">быть.больным-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T84" id="Seg_5667" s="T83">EMPH</ta>
            <ta e="T85" id="Seg_5668" s="T84">мужчина-EP-1SG.[NOM]</ta>
            <ta e="T86" id="Seg_5669" s="T85">мать-3SG.[NOM]</ta>
            <ta e="T87" id="Seg_5670" s="T86">лечить.оленем-PTCP.HAB</ta>
            <ta e="T88" id="Seg_5671" s="T87">быть-PST1-3SG</ta>
            <ta e="T89" id="Seg_5672" s="T88">голова-3SG-DAT/LOC</ta>
            <ta e="T90" id="Seg_5673" s="T89">олень-ACC</ta>
            <ta e="T91" id="Seg_5674" s="T90">вот</ta>
            <ta e="T92" id="Seg_5675" s="T91">тот</ta>
            <ta e="T93" id="Seg_5676" s="T92">олень-ACC</ta>
            <ta e="T94" id="Seg_5677" s="T93">MOD</ta>
            <ta e="T95" id="Seg_5678" s="T94">жена.[NOM]</ta>
            <ta e="T96" id="Seg_5679" s="T95">запрячь-PTCP.FUT-3SG-ACC</ta>
            <ta e="T97" id="Seg_5680" s="T96">нельзя</ta>
            <ta e="T98" id="Seg_5681" s="T97">быть-PST1-3SG</ta>
            <ta e="T99" id="Seg_5682" s="T98">EMPH</ta>
            <ta e="T100" id="Seg_5683" s="T99">тогда</ta>
            <ta e="T101" id="Seg_5684" s="T100">1SG.[NOM]</ta>
            <ta e="T102" id="Seg_5685" s="T101">знать-NEG.CVB.SIM-1SG</ta>
            <ta e="T103" id="Seg_5686" s="T102">этот</ta>
            <ta e="T104" id="Seg_5687" s="T103">олень-ACC</ta>
            <ta e="T105" id="Seg_5688" s="T104">левый.олень-VBZ-REFL-PST2-1SG</ta>
            <ta e="T106" id="Seg_5689" s="T105">EMPH</ta>
            <ta e="T107" id="Seg_5690" s="T106">тот-3SG-2SG.[NOM]</ta>
            <ta e="T108" id="Seg_5691" s="T107">белый</ta>
            <ta e="T109" id="Seg_5692" s="T108">олень.[NOM]</ta>
            <ta e="T110" id="Seg_5693" s="T109">серобелый</ta>
            <ta e="T111" id="Seg_5694" s="T110">олень.[NOM]</ta>
            <ta e="T112" id="Seg_5695" s="T111">так</ta>
            <ta e="T113" id="Seg_5696" s="T112">однако</ta>
            <ta e="T114" id="Seg_5697" s="T113">идти-CVB.PURP-1PL</ta>
            <ta e="T115" id="Seg_5698" s="T114">однако</ta>
            <ta e="T116" id="Seg_5699" s="T115">тот</ta>
            <ta e="T117" id="Seg_5700" s="T116">олень.[NOM]</ta>
            <ta e="T118" id="Seg_5701" s="T117">EMPH</ta>
            <ta e="T119" id="Seg_5702" s="T118">кусать-CAUS-PRS.[3SG]</ta>
            <ta e="T120" id="Seg_5703" s="T119">три-MLTP</ta>
            <ta e="T121" id="Seg_5704" s="T120">раз.[NOM]</ta>
            <ta e="T122" id="Seg_5705" s="T121">кусать-CAUS-PRS.[3SG]</ta>
            <ta e="T123" id="Seg_5706" s="T122">EMPH</ta>
            <ta e="T124" id="Seg_5707" s="T123">тогда</ta>
            <ta e="T125" id="Seg_5708" s="T124">1SG.[NOM]</ta>
            <ta e="T126" id="Seg_5709" s="T125">говорить-PRS-1SG</ta>
            <ta e="T127" id="Seg_5710" s="T126">EMPH</ta>
            <ta e="T128" id="Seg_5711" s="T127">вот.беда</ta>
            <ta e="T129" id="Seg_5712" s="T128">что-DAT/LOC</ta>
            <ta e="T130" id="Seg_5713" s="T129">кусать-CAUS-PRS.[3SG]</ta>
            <ta e="T131" id="Seg_5714" s="T130">вот</ta>
            <ta e="T132" id="Seg_5715" s="T131">говорить-PRS-1SG</ta>
            <ta e="T135" id="Seg_5716" s="T134">тот.[NOM]</ta>
            <ta e="T136" id="Seg_5717" s="T135">передняя.часть-3SG-INSTR</ta>
            <ta e="T137" id="Seg_5718" s="T136">отправляться-CVB.PURP-1PL</ta>
            <ta e="T138" id="Seg_5719" s="T137">мама-1SG.[NOM]</ta>
            <ta e="T139" id="Seg_5720" s="T138">говорить-PRS.[3SG]</ta>
            <ta e="T140" id="Seg_5721" s="T139">EMPH</ta>
            <ta e="T141" id="Seg_5722" s="T140">огонь-1PL.[NOM]</ta>
            <ta e="T142" id="Seg_5723" s="T141">говорить-PRS.[3SG]</ta>
            <ta e="T143" id="Seg_5724" s="T142">говорить-PRS.[3SG]</ta>
            <ta e="T144" id="Seg_5725" s="T143">огонь-1PL.[NOM]</ta>
            <ta e="T145" id="Seg_5726" s="T144">говорить-PRS.[3SG]</ta>
            <ta e="T148" id="Seg_5727" s="T147">однако</ta>
            <ta e="T149" id="Seg_5728" s="T148">что.[NOM]</ta>
            <ta e="T150" id="Seg_5729" s="T149">INDEF</ta>
            <ta e="T151" id="Seg_5730" s="T150">плохой.[NOM]</ta>
            <ta e="T152" id="Seg_5731" s="T151">быть-HAB.[3SG]</ta>
            <ta e="T153" id="Seg_5732" s="T152">огонь.[NOM]</ta>
            <ta e="T154" id="Seg_5733" s="T153">говорить-TEMP-3SG</ta>
            <ta e="T155" id="Seg_5734" s="T154">вот</ta>
            <ta e="T157" id="Seg_5735" s="T155">говорить-TEMP-3SG</ta>
            <ta e="T158" id="Seg_5736" s="T157">тогда</ta>
            <ta e="T159" id="Seg_5737" s="T158">однако</ta>
            <ta e="T160" id="Seg_5738" s="T159">ну</ta>
            <ta e="T165" id="Seg_5739" s="T164">1SG.[NOM]</ta>
            <ta e="T166" id="Seg_5740" s="T165">однако</ta>
            <ta e="T167" id="Seg_5741" s="T166">очень</ta>
            <ta e="T168" id="Seg_5742" s="T167">EMPH</ta>
            <ta e="T169" id="Seg_5743" s="T168">тот-ACC</ta>
            <ta e="T170" id="Seg_5744" s="T169">однако</ta>
            <ta e="T171" id="Seg_5745" s="T170">верить-NEG.CVB.SIM-1SG</ta>
            <ta e="T172" id="Seg_5746" s="T171">однако</ta>
            <ta e="T173" id="Seg_5747" s="T172">вот</ta>
            <ta e="T174" id="Seg_5748" s="T173">олень.[NOM]</ta>
            <ta e="T175" id="Seg_5749" s="T174">поймать -PST1-1PL</ta>
            <ta e="T176" id="Seg_5750" s="T175">и</ta>
            <ta e="T177" id="Seg_5751" s="T176">идти-CVB.SEQ</ta>
            <ta e="T178" id="Seg_5752" s="T177">оставаться-PST1-1PL</ta>
            <ta e="T179" id="Seg_5753" s="T178">EMPH</ta>
            <ta e="T180" id="Seg_5754" s="T179">тот</ta>
            <ta e="T181" id="Seg_5755" s="T180">этот</ta>
            <ta e="T182" id="Seg_5756" s="T181">олень-ACC</ta>
            <ta e="T183" id="Seg_5757" s="T182">поймать-PST2-1PL</ta>
            <ta e="T184" id="Seg_5758" s="T183">так</ta>
            <ta e="T185" id="Seg_5759" s="T184">идти-CVB.SEQ</ta>
            <ta e="T186" id="Seg_5760" s="T185">оставаться-PST2-1PL</ta>
            <ta e="T187" id="Seg_5761" s="T186">EMPH</ta>
            <ta e="T188" id="Seg_5762" s="T187">отец-1SG-ACC</ta>
            <ta e="T189" id="Seg_5763" s="T188">с</ta>
            <ta e="T190" id="Seg_5764" s="T189">сопровождать-CVB.SEQ</ta>
            <ta e="T191" id="Seg_5765" s="T190">оставаться-PST2-1PL</ta>
            <ta e="T192" id="Seg_5766" s="T191">сопровождать-CVB.SEQ</ta>
            <ta e="T193" id="Seg_5767" s="T192">оставаться</ta>
            <ta e="T194" id="Seg_5768" s="T193">стоянка-DAT/LOC</ta>
            <ta e="T195" id="Seg_5769" s="T194">приходить-CVB.SEQ</ta>
            <ta e="T196" id="Seg_5770" s="T195">идти-CVB.SEQ</ta>
            <ta e="T197" id="Seg_5771" s="T196">однако</ta>
            <ta e="T198" id="Seg_5772" s="T197">отец-1SG.[NOM]</ta>
            <ta e="T199" id="Seg_5773" s="T198">сани-1PL-ACC</ta>
            <ta e="T200" id="Seg_5774" s="T199">нарта-1PL-ACC</ta>
            <ta e="T201" id="Seg_5775" s="T200">однако</ta>
            <ta e="T202" id="Seg_5776" s="T201">грузить-REFL-PST1-1PL</ta>
            <ta e="T203" id="Seg_5777" s="T202">грузить-REFL-CVB.SEQ</ta>
            <ta e="T204" id="Seg_5778" s="T203">веревка.для.воза-VBZ-REFL-CVB.SEQ</ta>
            <ta e="T205" id="Seg_5779" s="T204">и.так.далее-CVB.SEQ</ta>
            <ta e="T206" id="Seg_5780" s="T205">EMPH</ta>
            <ta e="T207" id="Seg_5781" s="T206">1SG-ACC</ta>
            <ta e="T208" id="Seg_5782" s="T207">вот</ta>
            <ta e="T209" id="Seg_5783" s="T208">отправляться-CAUS-PST1-3SG</ta>
            <ta e="T210" id="Seg_5784" s="T209">EMPH</ta>
            <ta e="T211" id="Seg_5785" s="T210">сани-PROPR</ta>
            <ta e="T212" id="Seg_5786" s="T211">олень-1SG-ACC</ta>
            <ta e="T213" id="Seg_5787" s="T212">водить-CVB.SEQ</ta>
            <ta e="T214" id="Seg_5788" s="T213">идти-CVB.SEQ</ta>
            <ta e="T215" id="Seg_5789" s="T214">сани-PROPR</ta>
            <ta e="T216" id="Seg_5790" s="T215">олень-1SG-ACC</ta>
            <ta e="T217" id="Seg_5791" s="T216">водить-CVB.SEQ</ta>
            <ta e="T218" id="Seg_5792" s="T217">идти-CVB.SEQ-1SG</ta>
            <ta e="T219" id="Seg_5793" s="T218">EMPH</ta>
            <ta e="T220" id="Seg_5794" s="T219">ошибка</ta>
            <ta e="T221" id="Seg_5795" s="T220">сесть-PRS-1SG</ta>
            <ta e="T222" id="Seg_5796" s="T221">EMPH</ta>
            <ta e="T223" id="Seg_5797" s="T222">сани-1SG-DAT/LOC</ta>
            <ta e="T224" id="Seg_5798" s="T223">EMPH</ta>
            <ta e="T225" id="Seg_5799" s="T224">рыба-PL-ACC</ta>
            <ta e="T226" id="Seg_5800" s="T225">грузить-PST2-1SG</ta>
            <ta e="T227" id="Seg_5801" s="T226">шесть</ta>
            <ta e="T228" id="Seg_5802" s="T227">мешок.[NOM]</ta>
            <ta e="T229" id="Seg_5803" s="T228">рыба.[NOM]</ta>
            <ta e="T230" id="Seg_5804" s="T229">шесть</ta>
            <ta e="T231" id="Seg_5805" s="T230">мешок.[NOM]</ta>
            <ta e="T232" id="Seg_5806" s="T231">рыба.[NOM]</ta>
            <ta e="T233" id="Seg_5807" s="T232">грузить-CVB.SEQ</ta>
            <ta e="T234" id="Seg_5808" s="T233">идти-CVB.SEQ</ta>
            <ta e="T235" id="Seg_5809" s="T234">однако</ta>
            <ta e="T236" id="Seg_5810" s="T235">сани-PL.[NOM]</ta>
            <ta e="T237" id="Seg_5811" s="T236">прыгать-PRS-1SG</ta>
            <ta e="T238" id="Seg_5812" s="T237">говорить-PRS-1SG</ta>
            <ta e="T239" id="Seg_5813" s="T238">EMPH</ta>
            <ta e="T240" id="Seg_5814" s="T239">сани-1SG-ACC</ta>
            <ta e="T241" id="Seg_5815" s="T240">ошибка</ta>
            <ta e="T242" id="Seg_5816" s="T241">сесть-PRS-1SG</ta>
            <ta e="T243" id="Seg_5817" s="T242">EMPH</ta>
            <ta e="T244" id="Seg_5818" s="T243">нет</ta>
            <ta e="T245" id="Seg_5819" s="T244">этот</ta>
            <ta e="T246" id="Seg_5820" s="T245">человек.[NOM]</ta>
            <ta e="T247" id="Seg_5821" s="T246">EMPH</ta>
            <ta e="T248" id="Seg_5822" s="T247">еще</ta>
            <ta e="T249" id="Seg_5823" s="T248">да</ta>
            <ta e="T250" id="Seg_5824" s="T249">прыгать-PRS-1SG</ta>
            <ta e="T251" id="Seg_5825" s="T250">EMPH</ta>
            <ta e="T253" id="Seg_5826" s="T252">прыгать-PRS-1SG</ta>
            <ta e="T255" id="Seg_5827" s="T253">EMPH</ta>
            <ta e="T256" id="Seg_5828" s="T255">опять</ta>
            <ta e="T257" id="Seg_5829" s="T256">ошибка</ta>
            <ta e="T258" id="Seg_5830" s="T257">сесть-PRS-1SG</ta>
            <ta e="T259" id="Seg_5831" s="T258">EMPH</ta>
            <ta e="T260" id="Seg_5832" s="T259">сани-PROPR</ta>
            <ta e="T261" id="Seg_5833" s="T260">олень-1SG-ACC</ta>
            <ta e="T262" id="Seg_5834" s="T261">падать-CAUS-CVB.SEQ</ta>
            <ta e="T263" id="Seg_5835" s="T262">оставлять-PST1-1SG</ta>
            <ta e="T264" id="Seg_5836" s="T263">EMPH</ta>
            <ta e="T265" id="Seg_5837" s="T264">так</ta>
            <ta e="T266" id="Seg_5838" s="T265">сани-PROPR</ta>
            <ta e="T267" id="Seg_5839" s="T266">олень-1SG.[NOM]</ta>
            <ta e="T268" id="Seg_5840" s="T267">однако</ta>
            <ta e="T269" id="Seg_5841" s="T268">нарта-1SG.[NOM]</ta>
            <ta e="T270" id="Seg_5842" s="T269">олень-PL-3SG.[NOM]</ta>
            <ta e="T271" id="Seg_5843" s="T270">верхняя.часть-1SG-INSTR</ta>
            <ta e="T272" id="Seg_5844" s="T271">идти-PRS-3PL</ta>
            <ta e="T273" id="Seg_5845" s="T272">EMPH</ta>
            <ta e="T274" id="Seg_5846" s="T273">и</ta>
            <ta e="T275" id="Seg_5847" s="T274">сам-1SG.[NOM]</ta>
            <ta e="T276" id="Seg_5848" s="T275">однако</ta>
            <ta e="T277" id="Seg_5849" s="T276">EMPH</ta>
            <ta e="T278" id="Seg_5850" s="T277">нарта.[NOM]</ta>
            <ta e="T279" id="Seg_5851" s="T278">опорная.балка.в.санках-3SG-ACC</ta>
            <ta e="T280" id="Seg_5852" s="T279">каждый-INSTR</ta>
            <ta e="T281" id="Seg_5853" s="T280">три-MLTP</ta>
            <ta e="T282" id="Seg_5854" s="T281">раз.[NOM]</ta>
            <ta e="T283" id="Seg_5855" s="T282">вертеться-PST1-1SG</ta>
            <ta e="T284" id="Seg_5856" s="T283">EMPH</ta>
            <ta e="T285" id="Seg_5857" s="T284">вот.беда</ta>
            <ta e="T286" id="Seg_5858" s="T285">так</ta>
            <ta e="T287" id="Seg_5859" s="T286">EMPH</ta>
            <ta e="T288" id="Seg_5860" s="T287">навзничь</ta>
            <ta e="T289" id="Seg_5861" s="T288">падать-CVB.SEQ</ta>
            <ta e="T290" id="Seg_5862" s="T289">оставаться</ta>
            <ta e="T291" id="Seg_5863" s="T290">навзничь</ta>
            <ta e="T292" id="Seg_5864" s="T291">падать-CVB.SEQ</ta>
            <ta e="T293" id="Seg_5865" s="T292">идти-CVB.SEQ</ta>
            <ta e="T294" id="Seg_5866" s="T293">EMPH</ta>
            <ta e="T295" id="Seg_5867" s="T294">лежать-PRS-1SG</ta>
            <ta e="T296" id="Seg_5868" s="T295">EMPH</ta>
            <ta e="T297" id="Seg_5869" s="T296">отец-1SG.[NOM]</ta>
            <ta e="T298" id="Seg_5870" s="T297">сокуй.[NOM]</ta>
            <ta e="T299" id="Seg_5871" s="T298">одеть-PST1-3SG</ta>
            <ta e="T300" id="Seg_5872" s="T299">стоять-PTCP.PRS</ta>
            <ta e="T301" id="Seg_5873" s="T300">быть-PST1-3SG</ta>
            <ta e="T302" id="Seg_5874" s="T301">EMPH</ta>
            <ta e="T303" id="Seg_5875" s="T302">сокуй.[NOM]</ta>
            <ta e="T304" id="Seg_5876" s="T303">одеть-CVB.SEQ</ta>
            <ta e="T305" id="Seg_5877" s="T304">идти-CVB.SEQ</ta>
            <ta e="T306" id="Seg_5878" s="T305">этот-INTNS.[NOM]</ta>
            <ta e="T307" id="Seg_5879" s="T306">видеть-PST2-3SG</ta>
            <ta e="T308" id="Seg_5880" s="T307">видеть-PRS.[3SG]</ta>
            <ta e="T309" id="Seg_5881" s="T308">сани-PROPR</ta>
            <ta e="T310" id="Seg_5882" s="T309">олень.[NOM]</ta>
            <ta e="T311" id="Seg_5883" s="T310">идти-CVB.SIM</ta>
            <ta e="T312" id="Seg_5884" s="T311">стоять-PRS.[3SG]</ta>
            <ta e="T313" id="Seg_5885" s="T312">человек-3SG.[NOM]</ta>
            <ta e="T314" id="Seg_5886" s="T313">EMPH</ta>
            <ta e="T315" id="Seg_5887" s="T314">здесь</ta>
            <ta e="T316" id="Seg_5888" s="T315">лежать-PRS.[3SG]</ta>
            <ta e="T317" id="Seg_5889" s="T316">дорога-DAT/LOC</ta>
            <ta e="T318" id="Seg_5890" s="T317">вот</ta>
            <ta e="T319" id="Seg_5891" s="T318">бегать-CVB.SEQ</ta>
            <ta e="T320" id="Seg_5892" s="T319">приходить-PST1-3SG</ta>
            <ta e="T321" id="Seg_5893" s="T320">EMPH</ta>
            <ta e="T322" id="Seg_5894" s="T321">так</ta>
            <ta e="T323" id="Seg_5895" s="T322">говорить-PST1-3SG</ta>
            <ta e="T324" id="Seg_5896" s="T323">какой</ta>
            <ta e="T325" id="Seg_5897" s="T324">2SG.[NOM]</ta>
            <ta e="T326" id="Seg_5898" s="T325">дыхание-PROPR-2SG</ta>
            <ta e="T327" id="Seg_5899" s="T326">Q</ta>
            <ta e="T328" id="Seg_5900" s="T327">говорить-PST1-3SG</ta>
            <ta e="T329" id="Seg_5901" s="T328">AFFIRM</ta>
            <ta e="T330" id="Seg_5902" s="T329">дыхание-PROPR-1SG</ta>
            <ta e="T331" id="Seg_5903" s="T330">дыхание-PROPR-1SG</ta>
            <ta e="T332" id="Seg_5904" s="T331">нога-EP-1SG.[NOM]</ta>
            <ta e="T333" id="Seg_5905" s="T332">только</ta>
            <ta e="T334" id="Seg_5906" s="T333">мочь-CVB.SEQ</ta>
            <ta e="T335" id="Seg_5907" s="T334">двигаться-NEG.[3SG]</ta>
            <ta e="T336" id="Seg_5908" s="T335">AFFIRM</ta>
            <ta e="T337" id="Seg_5909" s="T336">вот</ta>
            <ta e="T338" id="Seg_5910" s="T337">подожди</ta>
            <ta e="T339" id="Seg_5911" s="T338">1SG.[NOM]</ta>
            <ta e="T340" id="Seg_5912" s="T339">приходить-FUT-1SG</ta>
            <ta e="T341" id="Seg_5913" s="T340">вот</ta>
            <ta e="T342" id="Seg_5914" s="T341">приходить-CVB.SEQ</ta>
            <ta e="T343" id="Seg_5915" s="T342">идти-CVB.SEQ</ta>
            <ta e="T344" id="Seg_5916" s="T343">сани-PROPR</ta>
            <ta e="T345" id="Seg_5917" s="T344">олень-3SG-DAT/LOC</ta>
            <ta e="T346" id="Seg_5918" s="T345">как</ta>
            <ta e="T347" id="Seg_5919" s="T346">INDEF</ta>
            <ta e="T348" id="Seg_5920" s="T347">садиться-CVB.SEQ</ta>
            <ta e="T349" id="Seg_5921" s="T348">идти-CVB.SEQ</ta>
            <ta e="T350" id="Seg_5922" s="T349">EMPH</ta>
            <ta e="T351" id="Seg_5923" s="T350">1SG.[NOM]</ta>
            <ta e="T352" id="Seg_5924" s="T351">сани-PROPR</ta>
            <ta e="T353" id="Seg_5925" s="T352">олень-1SG-ACC</ta>
            <ta e="T354" id="Seg_5926" s="T353">догонять-CVB.SIM</ta>
            <ta e="T355" id="Seg_5927" s="T354">идти-PRS-1PL</ta>
            <ta e="T356" id="Seg_5928" s="T355">EMPH</ta>
            <ta e="T357" id="Seg_5929" s="T356">там</ta>
            <ta e="T358" id="Seg_5930" s="T357">EMPH</ta>
            <ta e="T359" id="Seg_5931" s="T358">озеро-ACC</ta>
            <ta e="T360" id="Seg_5932" s="T359">лезть-CVB.PURP-1PL</ta>
            <ta e="T361" id="Seg_5933" s="T360">склон-DAT/LOC</ta>
            <ta e="T362" id="Seg_5934" s="T361">склон.[NOM]</ta>
            <ta e="T363" id="Seg_5935" s="T362">вдоль</ta>
            <ta e="T364" id="Seg_5936" s="T363">однако</ta>
            <ta e="T367" id="Seg_5937" s="T366">младший.брат-EP-1SG.[NOM]</ta>
            <ta e="T368" id="Seg_5938" s="T367">есть</ta>
            <ta e="T369" id="Seg_5939" s="T368">быть-PST1-3SG</ta>
            <ta e="T370" id="Seg_5940" s="T369">тот</ta>
            <ta e="T371" id="Seg_5941" s="T370">беда-PROPR</ta>
            <ta e="T372" id="Seg_5942" s="T371">сани-ABL</ta>
            <ta e="T373" id="Seg_5943" s="T372">падать-CVB.SEQ</ta>
            <ta e="T374" id="Seg_5944" s="T373">после</ta>
            <ta e="T375" id="Seg_5945" s="T374">повод-ACC</ta>
            <ta e="T376" id="Seg_5946" s="T375">поймать-CVB.SEQ</ta>
            <ta e="T377" id="Seg_5947" s="T376">взять-PST1-3SG</ta>
            <ta e="T378" id="Seg_5948" s="T377">так</ta>
            <ta e="T381" id="Seg_5949" s="T380">олень-PL.[NOM]</ta>
            <ta e="T382" id="Seg_5950" s="T381">останавливаться-PST1-3PL</ta>
            <ta e="T386" id="Seg_5951" s="T385">1PL.[NOM]</ta>
            <ta e="T387" id="Seg_5952" s="T386">приходить-PRS-1PL</ta>
            <ta e="T389" id="Seg_5953" s="T387">EMPH</ta>
            <ta e="T390" id="Seg_5954" s="T389">отец-1SG.[NOM]</ta>
            <ta e="T391" id="Seg_5955" s="T390">сани-PROPR</ta>
            <ta e="T392" id="Seg_5956" s="T391">олень-1SG-ACC</ta>
            <ta e="T393" id="Seg_5957" s="T392">связывать-CVB.SEQ</ta>
            <ta e="T394" id="Seg_5958" s="T393">бросать-PST1-3SG</ta>
            <ta e="T395" id="Seg_5959" s="T394">мой-3SG-ACC</ta>
            <ta e="T396" id="Seg_5960" s="T395">так</ta>
            <ta e="T397" id="Seg_5961" s="T396">делать-CVB.SEQ</ta>
            <ta e="T398" id="Seg_5962" s="T397">идти-CVB.SEQ</ta>
            <ta e="T399" id="Seg_5963" s="T398">сам-3SG.[NOM]</ta>
            <ta e="T400" id="Seg_5964" s="T399">EMPH</ta>
            <ta e="T401" id="Seg_5965" s="T400">1SG.[NOM]</ta>
            <ta e="T402" id="Seg_5966" s="T401">сани-PROPR</ta>
            <ta e="T403" id="Seg_5967" s="T402">олень-PL.[NOM]</ta>
            <ta e="T404" id="Seg_5968" s="T403">садиться-CVB.SEQ</ta>
            <ta e="T405" id="Seg_5969" s="T404">идти-CVB.SEQ</ta>
            <ta e="T406" id="Seg_5970" s="T405">тот</ta>
            <ta e="T407" id="Seg_5971" s="T406">дом-1PL-DAT/LOC</ta>
            <ta e="T408" id="Seg_5972" s="T407">идти-PST1-1PL</ta>
            <ta e="T409" id="Seg_5973" s="T408">EMPH</ta>
            <ta e="T410" id="Seg_5974" s="T409">дом-1PL-DAT/LOC</ta>
            <ta e="T411" id="Seg_5975" s="T410">идти-CVB.SEQ</ta>
            <ta e="T412" id="Seg_5976" s="T411">идти-CVB.SEQ</ta>
            <ta e="T413" id="Seg_5977" s="T412">однако</ta>
            <ta e="T414" id="Seg_5978" s="T413">кто-DAT/LOC</ta>
            <ta e="T415" id="Seg_5979" s="T414">дверь-1SG-DAT/LOC</ta>
            <ta e="T416" id="Seg_5980" s="T415">останавливаться-CAUS-PRS.[3SG]</ta>
            <ta e="T417" id="Seg_5981" s="T416">EMPH</ta>
            <ta e="T418" id="Seg_5982" s="T417">балок-EP-1SG.[NOM]</ta>
            <ta e="T419" id="Seg_5983" s="T418">дверь-3SG-DAT/LOC</ta>
            <ta e="T420" id="Seg_5984" s="T419">вот</ta>
            <ta e="T421" id="Seg_5985" s="T420">так</ta>
            <ta e="T422" id="Seg_5986" s="T421">EMPH</ta>
            <ta e="T423" id="Seg_5987" s="T422">старуха-PL.[NOM]</ta>
            <ta e="T424" id="Seg_5988" s="T423">два</ta>
            <ta e="T425" id="Seg_5989" s="T424">старуха.[NOM]</ta>
            <ta e="T426" id="Seg_5990" s="T425">есть</ta>
            <ta e="T427" id="Seg_5991" s="T426">быть-PST1-3SG</ta>
            <ta e="T429" id="Seg_5992" s="T428">тот-PL.[NOM]</ta>
            <ta e="T430" id="Seg_5993" s="T429">приходить-PST1-3PL</ta>
            <ta e="T431" id="Seg_5994" s="T430">эй</ta>
            <ta e="T432" id="Seg_5995" s="T431">EMPH</ta>
            <ta e="T433" id="Seg_5996" s="T432">что.[NOM]</ta>
            <ta e="T434" id="Seg_5997" s="T433">быть-PST1-2PL</ta>
            <ta e="T435" id="Seg_5998" s="T434">как</ta>
            <ta e="T436" id="Seg_5999" s="T435">как</ta>
            <ta e="T438" id="Seg_6000" s="T436">быть-PST1-2PL</ta>
            <ta e="T439" id="Seg_6001" s="T438">вот.беда</ta>
            <ta e="T440" id="Seg_6002" s="T439">этот</ta>
            <ta e="T441" id="Seg_6003" s="T440">1SG-DAT/LOC</ta>
            <ta e="T442" id="Seg_6004" s="T441">как.раз</ta>
            <ta e="T443" id="Seg_6005" s="T442">этот</ta>
            <ta e="T444" id="Seg_6006" s="T443">жена.[NOM]</ta>
            <ta e="T445" id="Seg_6007" s="T444">повредить-EP-REFL-PST1-3SG</ta>
            <ta e="T446" id="Seg_6008" s="T445">этот-ACC</ta>
            <ta e="T447" id="Seg_6009" s="T446">надо</ta>
            <ta e="T448" id="Seg_6010" s="T447">доктор-PART</ta>
            <ta e="T449" id="Seg_6011" s="T448">звать-EP-REFL-PTCP.FUT-DAT/LOC</ta>
            <ta e="T450" id="Seg_6012" s="T449">этот-ACC</ta>
            <ta e="T451" id="Seg_6013" s="T450">отец-1SG.[NOM]</ta>
            <ta e="T452" id="Seg_6014" s="T451">мама-1SG.[NOM]</ta>
            <ta e="T453" id="Seg_6015" s="T452">говорить-PRS.[3SG]</ta>
            <ta e="T454" id="Seg_6016" s="T453">идти-EP-NEG.[IMP.2SG]</ta>
            <ta e="T455" id="Seg_6017" s="T454">идти-EP-NEG.[IMP.2SG]</ta>
            <ta e="T456" id="Seg_6018" s="T455">доктор-ACC</ta>
            <ta e="T457" id="Seg_6019" s="T456">что-EP-ACC</ta>
            <ta e="T458" id="Seg_6020" s="T457">говорить-PRS.[3SG]</ta>
            <ta e="T459" id="Seg_6021" s="T458">грех.[NOM]</ta>
            <ta e="T460" id="Seg_6022" s="T459">EMPH</ta>
            <ta e="T461" id="Seg_6023" s="T460">хороший.[NOM]</ta>
            <ta e="T462" id="Seg_6024" s="T461">легкие-3SG.[NOM]</ta>
            <ta e="T463" id="Seg_6025" s="T462">широкий</ta>
            <ta e="T466" id="Seg_6026" s="T465">хороший.[NOM]</ta>
            <ta e="T467" id="Seg_6027" s="T466">подожди</ta>
            <ta e="T468" id="Seg_6028" s="T467">тогда</ta>
            <ta e="T469" id="Seg_6029" s="T468">1SG-ACC</ta>
            <ta e="T470" id="Seg_6030" s="T469">два</ta>
            <ta e="T471" id="Seg_6031" s="T470">сторона-ABL</ta>
            <ta e="T472" id="Seg_6032" s="T471">поднимать-CVB.SEQ</ta>
            <ta e="T473" id="Seg_6033" s="T472">идти-CVB.SEQ</ta>
            <ta e="T474" id="Seg_6034" s="T473">дом.[NOM]</ta>
            <ta e="T475" id="Seg_6035" s="T474">балок-DAT/LOC</ta>
            <ta e="T476" id="Seg_6036" s="T475">возвращаться-CAUS-PST1-3PL</ta>
            <ta e="T477" id="Seg_6037" s="T476">EMPH</ta>
            <ta e="T478" id="Seg_6038" s="T477">AFFIRM</ta>
            <ta e="T479" id="Seg_6039" s="T478">вот</ta>
            <ta e="T480" id="Seg_6040" s="T479">тот</ta>
            <ta e="T481" id="Seg_6041" s="T480">голый-VBZ-CVB.SEQ</ta>
            <ta e="T482" id="Seg_6042" s="T481">и.так.далее-CVB.SEQ</ta>
            <ta e="T483" id="Seg_6043" s="T482">идти-CVB.SEQ</ta>
            <ta e="T484" id="Seg_6044" s="T483">вот</ta>
            <ta e="T485" id="Seg_6045" s="T484">что.[NOM]</ta>
            <ta e="T486" id="Seg_6046" s="T485">доктор-3SG-ACC</ta>
            <ta e="T487" id="Seg_6047" s="T486">EMPH-3SG-ACC</ta>
            <ta e="T488" id="Seg_6048" s="T487">подожди</ta>
            <ta e="T489" id="Seg_6049" s="T488">хороший.[NOM]</ta>
            <ta e="T490" id="Seg_6050" s="T489">может</ta>
            <ta e="T491" id="Seg_6051" s="T490">и</ta>
            <ta e="T492" id="Seg_6052" s="T491">просто</ta>
            <ta e="T493" id="Seg_6053" s="T492">EMPH</ta>
            <ta e="T494" id="Seg_6054" s="T493">очевидно</ta>
            <ta e="T495" id="Seg_6055" s="T494">что-3SG.[NOM]</ta>
            <ta e="T496" id="Seg_6056" s="T495">NEG</ta>
            <ta e="T497" id="Seg_6057" s="T496">вроде</ta>
            <ta e="T498" id="Seg_6058" s="T497">ломать-EP-PST2.NEG.[3SG]</ta>
            <ta e="T499" id="Seg_6059" s="T498">кровь.[NOM]</ta>
            <ta e="T500" id="Seg_6060" s="T499">NEG</ta>
            <ta e="T501" id="Seg_6061" s="T500">NEG.EX</ta>
            <ta e="T502" id="Seg_6062" s="T501">что.[NOM]</ta>
            <ta e="T503" id="Seg_6063" s="T502">NEG</ta>
            <ta e="T504" id="Seg_6064" s="T503">NEG.EX</ta>
            <ta e="T505" id="Seg_6065" s="T504">хороший.[NOM]</ta>
            <ta e="T506" id="Seg_6066" s="T505">быть-FUT.[3SG]</ta>
            <ta e="T507" id="Seg_6067" s="T506">тогда</ta>
            <ta e="T508" id="Seg_6068" s="T507">AFFIRM</ta>
            <ta e="T509" id="Seg_6069" s="T508">вот</ta>
            <ta e="T510" id="Seg_6070" s="T509">вот</ta>
            <ta e="T511" id="Seg_6071" s="T510">тот-ABL</ta>
            <ta e="T512" id="Seg_6072" s="T511">тот</ta>
            <ta e="T513" id="Seg_6073" s="T512">вот</ta>
            <ta e="T514" id="Seg_6074" s="T513">тот-ABL</ta>
            <ta e="T515" id="Seg_6075" s="T514">тот-ABL</ta>
            <ta e="T516" id="Seg_6076" s="T515">попозже</ta>
            <ta e="T517" id="Seg_6077" s="T516">позже</ta>
            <ta e="T518" id="Seg_6078" s="T517">очень</ta>
            <ta e="T519" id="Seg_6079" s="T518">вот</ta>
            <ta e="T520" id="Seg_6080" s="T519">месяц.[NOM]</ta>
            <ta e="T521" id="Seg_6081" s="T520">проехать-PST1-3SG</ta>
            <ta e="T522" id="Seg_6082" s="T521">EMPH</ta>
            <ta e="T523" id="Seg_6083" s="T522">месяц.[NOM]</ta>
            <ta e="T524" id="Seg_6084" s="T523">проехать-CVB.SEQ</ta>
            <ta e="T525" id="Seg_6085" s="T524">идти-CVB.SEQ</ta>
            <ta e="T526" id="Seg_6086" s="T525">тот.EMPH-INSTR</ta>
            <ta e="T527" id="Seg_6087" s="T526">доктор</ta>
            <ta e="T528" id="Seg_6088" s="T527">NEG</ta>
            <ta e="T529" id="Seg_6089" s="T528">NEG.EX</ta>
            <ta e="T530" id="Seg_6090" s="T529">что.[NOM]</ta>
            <ta e="T531" id="Seg_6091" s="T530">NEG</ta>
            <ta e="T532" id="Seg_6092" s="T531">NEG.EX</ta>
            <ta e="T533" id="Seg_6093" s="T532">месяц.[NOM]</ta>
            <ta e="T534" id="Seg_6094" s="T533">проехать-CVB.SEQ</ta>
            <ta e="T535" id="Seg_6095" s="T534">идти-CVB.SEQ</ta>
            <ta e="T536" id="Seg_6096" s="T535">вот</ta>
            <ta e="T537" id="Seg_6097" s="T536">тот</ta>
            <ta e="T538" id="Seg_6098" s="T537">новый</ta>
            <ta e="T539" id="Seg_6099" s="T538">год-DAT/LOC</ta>
            <ta e="T540" id="Seg_6100" s="T539">входить-CVB.PURP</ta>
            <ta e="T541" id="Seg_6101" s="T540">вот</ta>
            <ta e="T542" id="Seg_6102" s="T541">доктор-DAT/LOC</ta>
            <ta e="T543" id="Seg_6103" s="T542">входить-PST1-1PL</ta>
            <ta e="T544" id="Seg_6104" s="T543">EMPH</ta>
            <ta e="T545" id="Seg_6105" s="T544">там</ta>
            <ta e="T546" id="Seg_6106" s="T545">входить-CVB.SEQ-1PL</ta>
            <ta e="T547" id="Seg_6107" s="T546">тот</ta>
            <ta e="T548" id="Seg_6108" s="T547">эй</ta>
            <ta e="T549" id="Seg_6109" s="T548">доктор-EP-1SG.[NOM]</ta>
            <ta e="T550" id="Seg_6110" s="T549">говорить-PRS.[3SG]</ta>
            <ta e="T551" id="Seg_6111" s="T550">EMPH</ta>
            <ta e="T552" id="Seg_6112" s="T551">эй</ta>
            <ta e="T553" id="Seg_6113" s="T552">твой</ta>
            <ta e="T554" id="Seg_6114" s="T553">бедро-EP-2SG.[NOM]</ta>
            <ta e="T555" id="Seg_6115" s="T554">бедро-EP-2SG.[NOM]</ta>
            <ta e="T556" id="Seg_6116" s="T555">ломать-EP-PTCP.PST</ta>
            <ta e="T557" id="Seg_6117" s="T556">быть-PST2.[3SG]</ta>
            <ta e="T558" id="Seg_6118" s="T557">2SG-ACC</ta>
            <ta e="T559" id="Seg_6119" s="T558">надо</ta>
            <ta e="T560" id="Seg_6120" s="T559">Хатанга-DAT/LOC</ta>
            <ta e="T561" id="Seg_6121" s="T560">послать-PTCP.FUT-DAT/LOC</ta>
            <ta e="T562" id="Seg_6122" s="T561">вот</ta>
            <ta e="T563" id="Seg_6123" s="T562">потом</ta>
            <ta e="T564" id="Seg_6124" s="T563">Хатанга-DAT/LOC</ta>
            <ta e="T565" id="Seg_6125" s="T564">послать-PST2-3PL</ta>
            <ta e="T566" id="Seg_6126" s="T565">Хатанга.[NOM]</ta>
            <ta e="T567" id="Seg_6127" s="T566">1SG.[NOM]</ta>
            <ta e="T568" id="Seg_6128" s="T567">лежать-PST1-1SG</ta>
            <ta e="T569" id="Seg_6129" s="T568">EMPH</ta>
            <ta e="T570" id="Seg_6130" s="T569">там</ta>
            <ta e="T571" id="Seg_6131" s="T570">вот</ta>
            <ta e="T572" id="Seg_6132" s="T571">два</ta>
            <ta e="T573" id="Seg_6133" s="T572">месяц-ACC</ta>
            <ta e="T574" id="Seg_6134" s="T573">вытяжка-DAT/LOC</ta>
            <ta e="T575" id="Seg_6135" s="T574">лежать-PST1-1SG</ta>
            <ta e="T576" id="Seg_6136" s="T575">EMPH</ta>
            <ta e="T577" id="Seg_6137" s="T576">тот-ABL</ta>
            <ta e="T578" id="Seg_6138" s="T577">EMPH</ta>
            <ta e="T579" id="Seg_6139" s="T578">другой.из.двух</ta>
            <ta e="T580" id="Seg_6140" s="T579">месяц-PL-3SG-ACC</ta>
            <ta e="T581" id="Seg_6141" s="T580">EMPH</ta>
            <ta e="T582" id="Seg_6142" s="T581">видеть.[IMP.2SG]</ta>
            <ta e="T583" id="Seg_6143" s="T582">восемь</ta>
            <ta e="T584" id="Seg_6144" s="T583">месяц-ACC</ta>
            <ta e="T585" id="Seg_6145" s="T584">целый</ta>
            <ta e="T586" id="Seg_6146" s="T585">лежать-PST1-1SG</ta>
            <ta e="T587" id="Seg_6147" s="T586">EMPH</ta>
            <ta e="T588" id="Seg_6148" s="T587">Хатанга-DAT/LOC</ta>
            <ta e="T589" id="Seg_6149" s="T614">дом-1SG-DAT/LOC</ta>
            <ta e="T590" id="Seg_6150" s="T589">приходить-PST2-EP-1SG</ta>
            <ta e="T591" id="Seg_6151" s="T590">и</ta>
            <ta e="T592" id="Seg_6152" s="T591">нога-EP-1SG.[NOM]</ta>
            <ta e="T598" id="Seg_6153" s="T597">быть-PST1-3SG</ta>
            <ta e="T599" id="Seg_6154" s="T598">тот.[NOM]</ta>
            <ta e="T600" id="Seg_6155" s="T599">подобно</ta>
            <ta e="T601" id="Seg_6156" s="T600">EMPH-вот</ta>
            <ta e="T602" id="Seg_6157" s="T601">поправляться-PST2.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_6158" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_6159" s="T1">v-v:mood-v:temp.pn</ta>
            <ta e="T3" id="Seg_6160" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_6161" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_6162" s="T4">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T6" id="Seg_6163" s="T5">adj.[n:case]</ta>
            <ta e="T7" id="Seg_6164" s="T6">v-v:tense-v:poss.pn</ta>
            <ta e="T8" id="Seg_6165" s="T7">cardnum</ta>
            <ta e="T9" id="Seg_6166" s="T8">n-n:(poss)-n:case</ta>
            <ta e="T10" id="Seg_6167" s="T9">n-n&gt;adj.[n:case]</ta>
            <ta e="T11" id="Seg_6168" s="T10">v-v:tense-v:poss.pn</ta>
            <ta e="T12" id="Seg_6169" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_6170" s="T12">n-n&gt;adj</ta>
            <ta e="T17" id="Seg_6171" s="T16">cardnum</ta>
            <ta e="T18" id="Seg_6172" s="T17">n-n:(poss)-n:case</ta>
            <ta e="T19" id="Seg_6173" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_6174" s="T19">cardnum</ta>
            <ta e="T21" id="Seg_6175" s="T20">n-n&gt;adj.[n:case]</ta>
            <ta e="T22" id="Seg_6176" s="T21">v-v:tense-v:poss.pn</ta>
            <ta e="T23" id="Seg_6177" s="T22">adv</ta>
            <ta e="T24" id="Seg_6178" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_6179" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_6180" s="T25">n-n:(poss)-n:case</ta>
            <ta e="T27" id="Seg_6181" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_6182" s="T27">v-v:cvb</ta>
            <ta e="T29" id="Seg_6183" s="T28">v-v:cvb-v:pred.pn</ta>
            <ta e="T30" id="Seg_6184" s="T29">n-n:(poss)-n:case</ta>
            <ta e="T31" id="Seg_6185" s="T30">post</ta>
            <ta e="T32" id="Seg_6186" s="T31">v-v:(ins)-v:ptcp</ta>
            <ta e="T33" id="Seg_6187" s="T32">v-v:tense-v:poss.pn</ta>
            <ta e="T34" id="Seg_6188" s="T33">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T35" id="Seg_6189" s="T34">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T36" id="Seg_6190" s="T35">n-n:poss-n:case</ta>
            <ta e="T37" id="Seg_6191" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_6192" s="T37">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T39" id="Seg_6193" s="T38">v-v:ptcp</ta>
            <ta e="T40" id="Seg_6194" s="T39">v-v:tense-v:poss.pn</ta>
            <ta e="T41" id="Seg_6195" s="T40">cardnum</ta>
            <ta e="T42" id="Seg_6196" s="T41">n-n&gt;adj</ta>
            <ta e="T43" id="Seg_6197" s="T42">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T50" id="Seg_6198" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_6199" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_6200" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_6201" s="T52">adj</ta>
            <ta e="T54" id="Seg_6202" s="T53">n-n:case</ta>
            <ta e="T58" id="Seg_6203" s="T57">que-que&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T59" id="Seg_6204" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_6205" s="T59">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T62" id="Seg_6206" s="T60">ptcl</ta>
            <ta e="T63" id="Seg_6207" s="T62">conj</ta>
            <ta e="T64" id="Seg_6208" s="T63">adv</ta>
            <ta e="T65" id="Seg_6209" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_6210" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_6211" s="T66">v-v:(ins)-v:ptcp</ta>
            <ta e="T68" id="Seg_6212" s="T67">n-n:case</ta>
            <ta e="T73" id="Seg_6213" s="T72">v-v:(ins)-v:ptcp</ta>
            <ta e="T74" id="Seg_6214" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_6215" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_6216" s="T75">v-v:(ins)-v:ptcp</ta>
            <ta e="T77" id="Seg_6217" s="T76">n-n:case</ta>
            <ta e="T78" id="Seg_6218" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_6219" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_6220" s="T79">interj</ta>
            <ta e="T81" id="Seg_6221" s="T80">v-v:(ins)-v:ptcp</ta>
            <ta e="T82" id="Seg_6222" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_6223" s="T82">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T84" id="Seg_6224" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_6225" s="T84">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T86" id="Seg_6226" s="T85">n-n:(poss)-n:case</ta>
            <ta e="T87" id="Seg_6227" s="T86">v-v:ptcp</ta>
            <ta e="T88" id="Seg_6228" s="T87">v-v:tense-v:poss.pn</ta>
            <ta e="T89" id="Seg_6229" s="T88">n-n:poss-n:case</ta>
            <ta e="T90" id="Seg_6230" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_6231" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_6232" s="T91">dempro</ta>
            <ta e="T93" id="Seg_6233" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_6234" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_6235" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_6236" s="T95">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T97" id="Seg_6237" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_6238" s="T97">v-v:tense-v:poss.pn</ta>
            <ta e="T99" id="Seg_6239" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_6240" s="T99">adv</ta>
            <ta e="T101" id="Seg_6241" s="T100">pers-pro:case</ta>
            <ta e="T102" id="Seg_6242" s="T101">v-v:cvb-v:pred.pn</ta>
            <ta e="T103" id="Seg_6243" s="T102">dempro</ta>
            <ta e="T104" id="Seg_6244" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_6245" s="T104">n-n&gt;v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T106" id="Seg_6246" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_6247" s="T106">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T108" id="Seg_6248" s="T107">adj</ta>
            <ta e="T109" id="Seg_6249" s="T108">n-n:case</ta>
            <ta e="T110" id="Seg_6250" s="T109">adj</ta>
            <ta e="T111" id="Seg_6251" s="T110">n-n:case</ta>
            <ta e="T112" id="Seg_6252" s="T111">adv</ta>
            <ta e="T113" id="Seg_6253" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_6254" s="T113">v-v:cvb-v:pred.pn</ta>
            <ta e="T115" id="Seg_6255" s="T114">ptcl</ta>
            <ta e="T116" id="Seg_6256" s="T115">dempro</ta>
            <ta e="T117" id="Seg_6257" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_6258" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_6259" s="T118">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T120" id="Seg_6260" s="T119">cardnum-cardnum&gt;adv</ta>
            <ta e="T121" id="Seg_6261" s="T120">n-n:case</ta>
            <ta e="T122" id="Seg_6262" s="T121">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T123" id="Seg_6263" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_6264" s="T123">adv</ta>
            <ta e="T125" id="Seg_6265" s="T124">pers-pro:case</ta>
            <ta e="T126" id="Seg_6266" s="T125">v-v:tense-v:pred.pn</ta>
            <ta e="T127" id="Seg_6267" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_6268" s="T127">interj</ta>
            <ta e="T129" id="Seg_6269" s="T128">que-pro:case</ta>
            <ta e="T130" id="Seg_6270" s="T129">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T131" id="Seg_6271" s="T130">ptcl</ta>
            <ta e="T132" id="Seg_6272" s="T131">v-v:tense-v:pred.pn</ta>
            <ta e="T135" id="Seg_6273" s="T134">dempro-pro:case</ta>
            <ta e="T136" id="Seg_6274" s="T135">n-n:poss-n:case</ta>
            <ta e="T137" id="Seg_6275" s="T136">v-v:cvb-v:pred.pn</ta>
            <ta e="T138" id="Seg_6276" s="T137">n-n:(poss)-n:case</ta>
            <ta e="T139" id="Seg_6277" s="T138">v-v:tense-v:pred.pn</ta>
            <ta e="T140" id="Seg_6278" s="T139">ptcl</ta>
            <ta e="T141" id="Seg_6279" s="T140">n-n:(poss)-n:case</ta>
            <ta e="T142" id="Seg_6280" s="T141">v-v:tense-v:pred.pn</ta>
            <ta e="T143" id="Seg_6281" s="T142">v-v:tense-v:pred.pn</ta>
            <ta e="T144" id="Seg_6282" s="T143">n-n:(poss)-n:case</ta>
            <ta e="T145" id="Seg_6283" s="T144">v-v:tense-v:pred.pn</ta>
            <ta e="T148" id="Seg_6284" s="T147">ptcl</ta>
            <ta e="T149" id="Seg_6285" s="T148">que-pro:case</ta>
            <ta e="T150" id="Seg_6286" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_6287" s="T150">adj-n:case</ta>
            <ta e="T152" id="Seg_6288" s="T151">v-v:mood-v:pred.pn</ta>
            <ta e="T153" id="Seg_6289" s="T152">n-n:case</ta>
            <ta e="T154" id="Seg_6290" s="T153">v-v:mood-v:temp.pn</ta>
            <ta e="T155" id="Seg_6291" s="T154">ptcl</ta>
            <ta e="T157" id="Seg_6292" s="T155">v-v:mood-v:temp.pn</ta>
            <ta e="T158" id="Seg_6293" s="T157">adv</ta>
            <ta e="T159" id="Seg_6294" s="T158">ptcl</ta>
            <ta e="T160" id="Seg_6295" s="T159">ptcl</ta>
            <ta e="T165" id="Seg_6296" s="T164">pers-pro:case</ta>
            <ta e="T166" id="Seg_6297" s="T165">ptcl</ta>
            <ta e="T167" id="Seg_6298" s="T166">adv</ta>
            <ta e="T168" id="Seg_6299" s="T167">ptcl</ta>
            <ta e="T169" id="Seg_6300" s="T168">dempro-pro:case</ta>
            <ta e="T170" id="Seg_6301" s="T169">ptcl</ta>
            <ta e="T171" id="Seg_6302" s="T170">v-v:cvb-v:pred.pn</ta>
            <ta e="T172" id="Seg_6303" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_6304" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_6305" s="T173">n-n:case</ta>
            <ta e="T175" id="Seg_6306" s="T174">v-v:tense-v:pred.pn</ta>
            <ta e="T176" id="Seg_6307" s="T175">conj</ta>
            <ta e="T177" id="Seg_6308" s="T176">v-v:cvb</ta>
            <ta e="T178" id="Seg_6309" s="T177">v-v:tense-v:poss.pn</ta>
            <ta e="T179" id="Seg_6310" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_6311" s="T179">dempro</ta>
            <ta e="T181" id="Seg_6312" s="T180">dempro</ta>
            <ta e="T182" id="Seg_6313" s="T181">n-n:case</ta>
            <ta e="T183" id="Seg_6314" s="T182">v-v:tense-v:pred.pn</ta>
            <ta e="T184" id="Seg_6315" s="T183">adv</ta>
            <ta e="T185" id="Seg_6316" s="T184">v-v:cvb</ta>
            <ta e="T186" id="Seg_6317" s="T185">v-v:tense-v:pred.pn</ta>
            <ta e="T187" id="Seg_6318" s="T186">ptcl</ta>
            <ta e="T188" id="Seg_6319" s="T187">n-n:poss-n:case</ta>
            <ta e="T189" id="Seg_6320" s="T188">post</ta>
            <ta e="T190" id="Seg_6321" s="T189">v-v:cvb</ta>
            <ta e="T191" id="Seg_6322" s="T190">v-v:tense-v:pred.pn</ta>
            <ta e="T192" id="Seg_6323" s="T191">v-v:cvb</ta>
            <ta e="T193" id="Seg_6324" s="T192">v</ta>
            <ta e="T194" id="Seg_6325" s="T193">n-n:case</ta>
            <ta e="T195" id="Seg_6326" s="T194">v-v:cvb</ta>
            <ta e="T196" id="Seg_6327" s="T195">v-v:cvb</ta>
            <ta e="T197" id="Seg_6328" s="T196">ptcl</ta>
            <ta e="T198" id="Seg_6329" s="T197">n-n:(poss)-n:case</ta>
            <ta e="T199" id="Seg_6330" s="T198">n-n:poss-n:case</ta>
            <ta e="T200" id="Seg_6331" s="T199">n-n:poss-n:case</ta>
            <ta e="T201" id="Seg_6332" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_6333" s="T201">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T203" id="Seg_6334" s="T202">v-v&gt;v-v:cvb</ta>
            <ta e="T204" id="Seg_6335" s="T203">n-n&gt;v-v&gt;v-v:cvb</ta>
            <ta e="T205" id="Seg_6336" s="T204">v-v:cvb</ta>
            <ta e="T206" id="Seg_6337" s="T205">ptcl</ta>
            <ta e="T207" id="Seg_6338" s="T206">pers-pro:case</ta>
            <ta e="T208" id="Seg_6339" s="T207">ptcl</ta>
            <ta e="T209" id="Seg_6340" s="T208">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T210" id="Seg_6341" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_6342" s="T210">n-n&gt;adj</ta>
            <ta e="T212" id="Seg_6343" s="T211">n-n:poss-n:case</ta>
            <ta e="T213" id="Seg_6344" s="T212">v-v:cvb</ta>
            <ta e="T214" id="Seg_6345" s="T213">v-v:cvb</ta>
            <ta e="T215" id="Seg_6346" s="T214">n-n&gt;adj</ta>
            <ta e="T216" id="Seg_6347" s="T215">n-n:poss-n:case</ta>
            <ta e="T217" id="Seg_6348" s="T216">v-v:cvb</ta>
            <ta e="T218" id="Seg_6349" s="T217">v-v:cvb-v:pred.pn</ta>
            <ta e="T219" id="Seg_6350" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_6351" s="T219">n</ta>
            <ta e="T221" id="Seg_6352" s="T220">v-v:tense-v:pred.pn</ta>
            <ta e="T222" id="Seg_6353" s="T221">ptcl</ta>
            <ta e="T223" id="Seg_6354" s="T222">n-n:poss-n:case</ta>
            <ta e="T224" id="Seg_6355" s="T223">ptcl</ta>
            <ta e="T225" id="Seg_6356" s="T224">n-n:(num)-n:case</ta>
            <ta e="T226" id="Seg_6357" s="T225">v-v:tense-v:pred.pn</ta>
            <ta e="T227" id="Seg_6358" s="T226">cardnum</ta>
            <ta e="T228" id="Seg_6359" s="T227">n-n:case</ta>
            <ta e="T229" id="Seg_6360" s="T228">n-n:case</ta>
            <ta e="T230" id="Seg_6361" s="T229">cardnum</ta>
            <ta e="T231" id="Seg_6362" s="T230">n.[n:case]</ta>
            <ta e="T232" id="Seg_6363" s="T231">n-n:case</ta>
            <ta e="T233" id="Seg_6364" s="T232">v-v:cvb</ta>
            <ta e="T234" id="Seg_6365" s="T233">v-v:cvb</ta>
            <ta e="T235" id="Seg_6366" s="T234">ptcl</ta>
            <ta e="T236" id="Seg_6367" s="T235">n-n:(num)-n:case</ta>
            <ta e="T237" id="Seg_6368" s="T236">v-v:tense-v:pred.pn</ta>
            <ta e="T238" id="Seg_6369" s="T237">v-v:tense-v:pred.pn</ta>
            <ta e="T239" id="Seg_6370" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_6371" s="T239">n-n:poss-n:case</ta>
            <ta e="T241" id="Seg_6372" s="T240">n</ta>
            <ta e="T242" id="Seg_6373" s="T241">v-v:tense-v:pred.pn</ta>
            <ta e="T243" id="Seg_6374" s="T242">ptcl</ta>
            <ta e="T244" id="Seg_6375" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_6376" s="T244">dempro</ta>
            <ta e="T246" id="Seg_6377" s="T245">n-n:case</ta>
            <ta e="T247" id="Seg_6378" s="T246">ptcl</ta>
            <ta e="T248" id="Seg_6379" s="T247">adv</ta>
            <ta e="T249" id="Seg_6380" s="T248">conj</ta>
            <ta e="T250" id="Seg_6381" s="T249">v-v:tense-v:pred.pn</ta>
            <ta e="T251" id="Seg_6382" s="T250">ptcl</ta>
            <ta e="T253" id="Seg_6383" s="T252">v-v:tense-v:pred.pn</ta>
            <ta e="T255" id="Seg_6384" s="T253">ptcl</ta>
            <ta e="T256" id="Seg_6385" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_6386" s="T256">n</ta>
            <ta e="T258" id="Seg_6387" s="T257">v-v:tense-v:pred.pn</ta>
            <ta e="T259" id="Seg_6388" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_6389" s="T259">n-n&gt;adj</ta>
            <ta e="T261" id="Seg_6390" s="T260">n-n:poss-n:case</ta>
            <ta e="T262" id="Seg_6391" s="T261">v-v&gt;v-v:cvb</ta>
            <ta e="T263" id="Seg_6392" s="T262">v-v:tense-v:poss.pn</ta>
            <ta e="T264" id="Seg_6393" s="T263">ptcl</ta>
            <ta e="T265" id="Seg_6394" s="T264">adv</ta>
            <ta e="T266" id="Seg_6395" s="T265">n-n&gt;adj</ta>
            <ta e="T267" id="Seg_6396" s="T266">n-n:(poss)-n:case</ta>
            <ta e="T268" id="Seg_6397" s="T267">ptcl</ta>
            <ta e="T269" id="Seg_6398" s="T268">n-n:(poss)-n:case</ta>
            <ta e="T270" id="Seg_6399" s="T269">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T271" id="Seg_6400" s="T270">n-n:poss-n:case</ta>
            <ta e="T272" id="Seg_6401" s="T271">v-v:tense-v:pred.pn</ta>
            <ta e="T273" id="Seg_6402" s="T272">ptcl</ta>
            <ta e="T274" id="Seg_6403" s="T273">conj</ta>
            <ta e="T275" id="Seg_6404" s="T274">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T276" id="Seg_6405" s="T275">ptcl</ta>
            <ta e="T277" id="Seg_6406" s="T276">ptcl</ta>
            <ta e="T278" id="Seg_6407" s="T277">n-n:case</ta>
            <ta e="T279" id="Seg_6408" s="T278">n-n:poss-n:case</ta>
            <ta e="T280" id="Seg_6409" s="T279">adj-n:case</ta>
            <ta e="T281" id="Seg_6410" s="T280">cardnum-cardnum&gt;adv</ta>
            <ta e="T282" id="Seg_6411" s="T281">n-n:case</ta>
            <ta e="T283" id="Seg_6412" s="T282">v-v:tense-v:poss.pn</ta>
            <ta e="T284" id="Seg_6413" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_6414" s="T284">interj</ta>
            <ta e="T286" id="Seg_6415" s="T285">adv</ta>
            <ta e="T287" id="Seg_6416" s="T286">ptcl</ta>
            <ta e="T288" id="Seg_6417" s="T287">adv</ta>
            <ta e="T289" id="Seg_6418" s="T288">v-v:cvb</ta>
            <ta e="T290" id="Seg_6419" s="T289">v</ta>
            <ta e="T291" id="Seg_6420" s="T290">adv</ta>
            <ta e="T292" id="Seg_6421" s="T291">v-v:cvb</ta>
            <ta e="T293" id="Seg_6422" s="T292">v-v:cvb</ta>
            <ta e="T294" id="Seg_6423" s="T293">ptcl</ta>
            <ta e="T295" id="Seg_6424" s="T294">v-v:tense-v:pred.pn</ta>
            <ta e="T296" id="Seg_6425" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_6426" s="T296">n-n:(poss)-n:case</ta>
            <ta e="T298" id="Seg_6427" s="T297">n-n:case</ta>
            <ta e="T299" id="Seg_6428" s="T298">v-v:tense-v:poss.pn</ta>
            <ta e="T300" id="Seg_6429" s="T299">v-v:ptcp</ta>
            <ta e="T301" id="Seg_6430" s="T300">v-v:tense-v:poss.pn</ta>
            <ta e="T302" id="Seg_6431" s="T301">ptcl</ta>
            <ta e="T303" id="Seg_6432" s="T302">n-n:case</ta>
            <ta e="T304" id="Seg_6433" s="T303">v-v:cvb</ta>
            <ta e="T305" id="Seg_6434" s="T304">v-v:cvb</ta>
            <ta e="T306" id="Seg_6435" s="T305">dempro-pro&gt;pro.[pro:case]</ta>
            <ta e="T307" id="Seg_6436" s="T306">v-v:tense-v:poss.pn</ta>
            <ta e="T308" id="Seg_6437" s="T307">v-v:tense-v:pred.pn</ta>
            <ta e="T309" id="Seg_6438" s="T308">n-n&gt;adj</ta>
            <ta e="T310" id="Seg_6439" s="T309">n-n:case</ta>
            <ta e="T311" id="Seg_6440" s="T310">v-v:cvb</ta>
            <ta e="T312" id="Seg_6441" s="T311">v-v:tense-v:pred.pn</ta>
            <ta e="T313" id="Seg_6442" s="T312">n-n:(poss)-n:case</ta>
            <ta e="T314" id="Seg_6443" s="T313">ptcl</ta>
            <ta e="T315" id="Seg_6444" s="T314">adv</ta>
            <ta e="T316" id="Seg_6445" s="T315">v-v:tense-v:pred.pn</ta>
            <ta e="T317" id="Seg_6446" s="T316">n-n:case</ta>
            <ta e="T318" id="Seg_6447" s="T317">ptcl</ta>
            <ta e="T319" id="Seg_6448" s="T318">v-v:cvb</ta>
            <ta e="T320" id="Seg_6449" s="T319">v-v:tense-v:poss.pn</ta>
            <ta e="T321" id="Seg_6450" s="T320">ptcl</ta>
            <ta e="T322" id="Seg_6451" s="T321">adv</ta>
            <ta e="T323" id="Seg_6452" s="T322">v-v:tense-v:poss.pn</ta>
            <ta e="T324" id="Seg_6453" s="T323">que</ta>
            <ta e="T325" id="Seg_6454" s="T324">pers-pro:case</ta>
            <ta e="T326" id="Seg_6455" s="T325">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T327" id="Seg_6456" s="T326">ptcl</ta>
            <ta e="T328" id="Seg_6457" s="T327">v-v:tense-v:poss.pn</ta>
            <ta e="T329" id="Seg_6458" s="T328">ptcl</ta>
            <ta e="T330" id="Seg_6459" s="T329">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T331" id="Seg_6460" s="T330">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T332" id="Seg_6461" s="T331">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T333" id="Seg_6462" s="T332">ptcl</ta>
            <ta e="T334" id="Seg_6463" s="T333">v-v:cvb</ta>
            <ta e="T335" id="Seg_6464" s="T334">v-v:(neg)-v:pred.pn</ta>
            <ta e="T336" id="Seg_6465" s="T335">ptcl</ta>
            <ta e="T337" id="Seg_6466" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_6467" s="T337">interj</ta>
            <ta e="T339" id="Seg_6468" s="T338">pers-pro:case</ta>
            <ta e="T340" id="Seg_6469" s="T339">v-v:tense-v:poss.pn</ta>
            <ta e="T341" id="Seg_6470" s="T340">ptcl</ta>
            <ta e="T342" id="Seg_6471" s="T341">v-v:cvb</ta>
            <ta e="T343" id="Seg_6472" s="T342">v-v:cvb</ta>
            <ta e="T344" id="Seg_6473" s="T343">n-n&gt;adj</ta>
            <ta e="T345" id="Seg_6474" s="T344">n-n:poss-n:case</ta>
            <ta e="T346" id="Seg_6475" s="T345">que</ta>
            <ta e="T347" id="Seg_6476" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_6477" s="T347">v-v:cvb</ta>
            <ta e="T349" id="Seg_6478" s="T348">v-v:cvb</ta>
            <ta e="T350" id="Seg_6479" s="T349">ptcl</ta>
            <ta e="T351" id="Seg_6480" s="T350">pers-pro:case</ta>
            <ta e="T352" id="Seg_6481" s="T351">n-n&gt;adj</ta>
            <ta e="T353" id="Seg_6482" s="T352">n-n:poss-n:case</ta>
            <ta e="T354" id="Seg_6483" s="T353">v-v:cvb</ta>
            <ta e="T355" id="Seg_6484" s="T354">v-v:tense-v:pred.pn</ta>
            <ta e="T356" id="Seg_6485" s="T355">ptcl</ta>
            <ta e="T357" id="Seg_6486" s="T356">adv</ta>
            <ta e="T358" id="Seg_6487" s="T357">ptcl</ta>
            <ta e="T359" id="Seg_6488" s="T358">n-n:case</ta>
            <ta e="T360" id="Seg_6489" s="T359">v-v:cvb-v:pred.pn</ta>
            <ta e="T361" id="Seg_6490" s="T360">n-n:case</ta>
            <ta e="T362" id="Seg_6491" s="T361">n.[n:case]</ta>
            <ta e="T363" id="Seg_6492" s="T362">post</ta>
            <ta e="T364" id="Seg_6493" s="T363">ptcl</ta>
            <ta e="T367" id="Seg_6494" s="T366">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T368" id="Seg_6495" s="T367">ptcl</ta>
            <ta e="T369" id="Seg_6496" s="T368">v-v:tense-v:poss.pn</ta>
            <ta e="T370" id="Seg_6497" s="T369">dempro</ta>
            <ta e="T371" id="Seg_6498" s="T370">n-n&gt;adj</ta>
            <ta e="T372" id="Seg_6499" s="T371">n-n:case</ta>
            <ta e="T373" id="Seg_6500" s="T372">v-v:cvb</ta>
            <ta e="T374" id="Seg_6501" s="T373">post</ta>
            <ta e="T375" id="Seg_6502" s="T374">n-n:case</ta>
            <ta e="T376" id="Seg_6503" s="T375">v-v:cvb</ta>
            <ta e="T377" id="Seg_6504" s="T376">v-v:tense-v:poss.pn</ta>
            <ta e="T378" id="Seg_6505" s="T377">adv</ta>
            <ta e="T381" id="Seg_6506" s="T380">n-n:(num)-n:case</ta>
            <ta e="T382" id="Seg_6507" s="T381">v-v:tense-v:pred.pn</ta>
            <ta e="T386" id="Seg_6508" s="T385">pers-pro:case</ta>
            <ta e="T387" id="Seg_6509" s="T386">v-v:tense-v:pred.pn</ta>
            <ta e="T389" id="Seg_6510" s="T387">ptcl</ta>
            <ta e="T390" id="Seg_6511" s="T389">n-n:(poss)-n:case</ta>
            <ta e="T391" id="Seg_6512" s="T390">n-n&gt;adj</ta>
            <ta e="T392" id="Seg_6513" s="T391">n-n:poss-n:case</ta>
            <ta e="T393" id="Seg_6514" s="T392">v-v:cvb</ta>
            <ta e="T394" id="Seg_6515" s="T393">v-v:tense-v:poss.pn</ta>
            <ta e="T395" id="Seg_6516" s="T394">posspr-n:poss-n:case</ta>
            <ta e="T396" id="Seg_6517" s="T395">adv</ta>
            <ta e="T397" id="Seg_6518" s="T396">v-v:cvb</ta>
            <ta e="T398" id="Seg_6519" s="T397">v-v:cvb</ta>
            <ta e="T399" id="Seg_6520" s="T398">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T400" id="Seg_6521" s="T399">ptcl</ta>
            <ta e="T401" id="Seg_6522" s="T400">pers-pro:case</ta>
            <ta e="T402" id="Seg_6523" s="T401">n-n&gt;adj</ta>
            <ta e="T403" id="Seg_6524" s="T402">n-n:(num)-n:case</ta>
            <ta e="T404" id="Seg_6525" s="T403">v-v:cvb</ta>
            <ta e="T405" id="Seg_6526" s="T404">v-v:cvb</ta>
            <ta e="T406" id="Seg_6527" s="T405">dempro</ta>
            <ta e="T407" id="Seg_6528" s="T406">n-n:poss-n:case</ta>
            <ta e="T408" id="Seg_6529" s="T407">v-v:tense-v:poss.pn</ta>
            <ta e="T409" id="Seg_6530" s="T408">ptcl</ta>
            <ta e="T410" id="Seg_6531" s="T409">n-n:poss-n:case</ta>
            <ta e="T411" id="Seg_6532" s="T410">v-v:cvb</ta>
            <ta e="T412" id="Seg_6533" s="T411">v-v:cvb</ta>
            <ta e="T413" id="Seg_6534" s="T412">ptcl</ta>
            <ta e="T414" id="Seg_6535" s="T413">que-pro:case</ta>
            <ta e="T415" id="Seg_6536" s="T414">n-n:poss-n:case</ta>
            <ta e="T416" id="Seg_6537" s="T415">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T417" id="Seg_6538" s="T416">ptcl</ta>
            <ta e="T418" id="Seg_6539" s="T417">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T419" id="Seg_6540" s="T418">n-n:poss-n:case</ta>
            <ta e="T420" id="Seg_6541" s="T419">ptcl</ta>
            <ta e="T421" id="Seg_6542" s="T420">adv</ta>
            <ta e="T422" id="Seg_6543" s="T421">ptcl</ta>
            <ta e="T423" id="Seg_6544" s="T422">n-n:(num)-n:case</ta>
            <ta e="T424" id="Seg_6545" s="T423">cardnum</ta>
            <ta e="T425" id="Seg_6546" s="T424">n-n:case</ta>
            <ta e="T426" id="Seg_6547" s="T425">ptcl</ta>
            <ta e="T427" id="Seg_6548" s="T426">v-v:tense-v:poss.pn</ta>
            <ta e="T429" id="Seg_6549" s="T428">dempro-pro:(num)-pro:case</ta>
            <ta e="T430" id="Seg_6550" s="T429">v-v:tense-v:pred.pn</ta>
            <ta e="T431" id="Seg_6551" s="T430">interj</ta>
            <ta e="T432" id="Seg_6552" s="T431">ptcl</ta>
            <ta e="T433" id="Seg_6553" s="T432">que-pro:case</ta>
            <ta e="T434" id="Seg_6554" s="T433">v-v:tense-v:pred.pn</ta>
            <ta e="T435" id="Seg_6555" s="T434">que</ta>
            <ta e="T436" id="Seg_6556" s="T435">que</ta>
            <ta e="T438" id="Seg_6557" s="T436">v-v:tense-v:pred.pn</ta>
            <ta e="T439" id="Seg_6558" s="T438">interj</ta>
            <ta e="T440" id="Seg_6559" s="T439">dempro</ta>
            <ta e="T441" id="Seg_6560" s="T440">pers-pro:case</ta>
            <ta e="T442" id="Seg_6561" s="T441">adv</ta>
            <ta e="T443" id="Seg_6562" s="T442">dempro</ta>
            <ta e="T444" id="Seg_6563" s="T443">n-n:case</ta>
            <ta e="T445" id="Seg_6564" s="T444">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T446" id="Seg_6565" s="T445">dempro-pro:case</ta>
            <ta e="T447" id="Seg_6566" s="T446">ptcl</ta>
            <ta e="T448" id="Seg_6567" s="T447">n-n:case</ta>
            <ta e="T449" id="Seg_6568" s="T448">v-v:(ins)-v&gt;v-v:ptcp-v:(case)</ta>
            <ta e="T450" id="Seg_6569" s="T449">dempro-pro:case</ta>
            <ta e="T451" id="Seg_6570" s="T450">n-n:(poss)-n:case</ta>
            <ta e="T452" id="Seg_6571" s="T451">n-n:(poss)-n:case</ta>
            <ta e="T453" id="Seg_6572" s="T452">v-v:tense-v:pred.pn</ta>
            <ta e="T454" id="Seg_6573" s="T453">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T455" id="Seg_6574" s="T454">v-n:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T456" id="Seg_6575" s="T455">n-n:case</ta>
            <ta e="T457" id="Seg_6576" s="T456">que-pro:(ins)-pro:case</ta>
            <ta e="T458" id="Seg_6577" s="T457">v-v:tense-v:pred.pn</ta>
            <ta e="T459" id="Seg_6578" s="T458">n-n:case</ta>
            <ta e="T460" id="Seg_6579" s="T459">ptcl</ta>
            <ta e="T461" id="Seg_6580" s="T460">adj-n:case</ta>
            <ta e="T462" id="Seg_6581" s="T461">n-n:(poss)-n:case</ta>
            <ta e="T463" id="Seg_6582" s="T462">adj</ta>
            <ta e="T466" id="Seg_6583" s="T465">adj-n:case</ta>
            <ta e="T467" id="Seg_6584" s="T466">interj</ta>
            <ta e="T468" id="Seg_6585" s="T467">adv</ta>
            <ta e="T469" id="Seg_6586" s="T468">pers-pro:case</ta>
            <ta e="T470" id="Seg_6587" s="T469">cardnum</ta>
            <ta e="T471" id="Seg_6588" s="T470">n-n:case</ta>
            <ta e="T472" id="Seg_6589" s="T471">v-v:cvb</ta>
            <ta e="T473" id="Seg_6590" s="T472">v-v:cvb</ta>
            <ta e="T474" id="Seg_6591" s="T473">n.[n:case]</ta>
            <ta e="T475" id="Seg_6592" s="T474">n-n:case</ta>
            <ta e="T476" id="Seg_6593" s="T475">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T477" id="Seg_6594" s="T476">ptcl</ta>
            <ta e="T478" id="Seg_6595" s="T477">ptcl</ta>
            <ta e="T479" id="Seg_6596" s="T478">ptcl</ta>
            <ta e="T480" id="Seg_6597" s="T479">dempro</ta>
            <ta e="T481" id="Seg_6598" s="T480">adj-adj&gt;v-v:cvb</ta>
            <ta e="T482" id="Seg_6599" s="T481">v-v:cvb</ta>
            <ta e="T483" id="Seg_6600" s="T482">v-v:cvb</ta>
            <ta e="T484" id="Seg_6601" s="T483">ptcl</ta>
            <ta e="T485" id="Seg_6602" s="T484">que-pro:case</ta>
            <ta e="T486" id="Seg_6603" s="T485">n-n:poss-n:case</ta>
            <ta e="T487" id="Seg_6604" s="T486">ptcl-n:poss-n:case</ta>
            <ta e="T488" id="Seg_6605" s="T487">interj</ta>
            <ta e="T489" id="Seg_6606" s="T488">adj-n:case</ta>
            <ta e="T490" id="Seg_6607" s="T489">ptcl</ta>
            <ta e="T491" id="Seg_6608" s="T490">conj</ta>
            <ta e="T492" id="Seg_6609" s="T491">adv</ta>
            <ta e="T493" id="Seg_6610" s="T492">ptcl</ta>
            <ta e="T494" id="Seg_6611" s="T493">ptcl</ta>
            <ta e="T495" id="Seg_6612" s="T494">que-pro:(poss)-pro:case</ta>
            <ta e="T496" id="Seg_6613" s="T495">ptcl</ta>
            <ta e="T497" id="Seg_6614" s="T496">ptcl</ta>
            <ta e="T498" id="Seg_6615" s="T497">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T499" id="Seg_6616" s="T498">n-n:case</ta>
            <ta e="T500" id="Seg_6617" s="T499">ptcl</ta>
            <ta e="T501" id="Seg_6618" s="T500">ptcl</ta>
            <ta e="T502" id="Seg_6619" s="T501">que-pro:case</ta>
            <ta e="T503" id="Seg_6620" s="T502">ptcl</ta>
            <ta e="T504" id="Seg_6621" s="T503">ptcl</ta>
            <ta e="T505" id="Seg_6622" s="T504">adj-n:case</ta>
            <ta e="T506" id="Seg_6623" s="T505">v-v:tense-v:poss.pn</ta>
            <ta e="T507" id="Seg_6624" s="T506">adv</ta>
            <ta e="T508" id="Seg_6625" s="T507">ptcl</ta>
            <ta e="T509" id="Seg_6626" s="T508">ptcl</ta>
            <ta e="T510" id="Seg_6627" s="T509">ptcl</ta>
            <ta e="T511" id="Seg_6628" s="T510">dempro-pro:case</ta>
            <ta e="T512" id="Seg_6629" s="T511">dempro</ta>
            <ta e="T513" id="Seg_6630" s="T512">ptcl</ta>
            <ta e="T514" id="Seg_6631" s="T513">dempro-pro:case</ta>
            <ta e="T515" id="Seg_6632" s="T514">dempro-pro:case</ta>
            <ta e="T516" id="Seg_6633" s="T515">adv</ta>
            <ta e="T517" id="Seg_6634" s="T516">adv</ta>
            <ta e="T518" id="Seg_6635" s="T517">ptcl</ta>
            <ta e="T519" id="Seg_6636" s="T518">ptcl</ta>
            <ta e="T520" id="Seg_6637" s="T519">n-n:case</ta>
            <ta e="T521" id="Seg_6638" s="T520">v-v:tense-v:poss.pn</ta>
            <ta e="T522" id="Seg_6639" s="T521">ptcl</ta>
            <ta e="T523" id="Seg_6640" s="T522">n-n:case</ta>
            <ta e="T524" id="Seg_6641" s="T523">v-v:cvb</ta>
            <ta e="T525" id="Seg_6642" s="T524">v-v:cvb</ta>
            <ta e="T526" id="Seg_6643" s="T525">dempro-pro:case</ta>
            <ta e="T527" id="Seg_6644" s="T526">n</ta>
            <ta e="T528" id="Seg_6645" s="T527">ptcl</ta>
            <ta e="T529" id="Seg_6646" s="T528">ptcl</ta>
            <ta e="T530" id="Seg_6647" s="T529">que-pro:case</ta>
            <ta e="T531" id="Seg_6648" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_6649" s="T531">ptcl</ta>
            <ta e="T533" id="Seg_6650" s="T532">n-n:case</ta>
            <ta e="T534" id="Seg_6651" s="T533">v-v:cvb</ta>
            <ta e="T535" id="Seg_6652" s="T534">v-v:cvb</ta>
            <ta e="T536" id="Seg_6653" s="T535">ptcl</ta>
            <ta e="T537" id="Seg_6654" s="T536">dempro</ta>
            <ta e="T538" id="Seg_6655" s="T537">adj</ta>
            <ta e="T539" id="Seg_6656" s="T538">n-n:case</ta>
            <ta e="T540" id="Seg_6657" s="T539">v-v:cvb</ta>
            <ta e="T541" id="Seg_6658" s="T540">ptcl</ta>
            <ta e="T542" id="Seg_6659" s="T541">n-n:case</ta>
            <ta e="T543" id="Seg_6660" s="T542">v-v:tense-v:pred.pn</ta>
            <ta e="T544" id="Seg_6661" s="T543">ptcl</ta>
            <ta e="T545" id="Seg_6662" s="T544">adv</ta>
            <ta e="T546" id="Seg_6663" s="T545">v-v:cvb-v:pred.pn</ta>
            <ta e="T547" id="Seg_6664" s="T546">dempro</ta>
            <ta e="T548" id="Seg_6665" s="T547">interj</ta>
            <ta e="T549" id="Seg_6666" s="T548">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T550" id="Seg_6667" s="T549">v-v:tense-v:pred.pn</ta>
            <ta e="T551" id="Seg_6668" s="T550">ptcl</ta>
            <ta e="T552" id="Seg_6669" s="T551">interj</ta>
            <ta e="T553" id="Seg_6670" s="T552">posspr</ta>
            <ta e="T554" id="Seg_6671" s="T553">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T555" id="Seg_6672" s="T554">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T556" id="Seg_6673" s="T555">v-v:(ins)-v:ptcp</ta>
            <ta e="T557" id="Seg_6674" s="T556">v-v:tense-v:pred.pn</ta>
            <ta e="T558" id="Seg_6675" s="T557">pers-pro:case</ta>
            <ta e="T559" id="Seg_6676" s="T558">ptcl</ta>
            <ta e="T560" id="Seg_6677" s="T559">propr-n:case</ta>
            <ta e="T561" id="Seg_6678" s="T560">v-v:ptcp-v:(case)</ta>
            <ta e="T562" id="Seg_6679" s="T561">ptcl</ta>
            <ta e="T563" id="Seg_6680" s="T562">adv</ta>
            <ta e="T564" id="Seg_6681" s="T563">propr-n:case</ta>
            <ta e="T565" id="Seg_6682" s="T564">v-v:tense-v:poss.pn</ta>
            <ta e="T566" id="Seg_6683" s="T565">propr.[n:case]</ta>
            <ta e="T567" id="Seg_6684" s="T566">pers-pro:case</ta>
            <ta e="T568" id="Seg_6685" s="T567">v-v:tense-v:poss.pn</ta>
            <ta e="T569" id="Seg_6686" s="T568">ptcl</ta>
            <ta e="T570" id="Seg_6687" s="T569">adv</ta>
            <ta e="T571" id="Seg_6688" s="T570">ptcl</ta>
            <ta e="T572" id="Seg_6689" s="T571">cardnum</ta>
            <ta e="T573" id="Seg_6690" s="T572">n-n:case</ta>
            <ta e="T574" id="Seg_6691" s="T573">n-n:case</ta>
            <ta e="T575" id="Seg_6692" s="T574">v-v:tense-v:poss.pn</ta>
            <ta e="T576" id="Seg_6693" s="T575">ptcl</ta>
            <ta e="T577" id="Seg_6694" s="T576">dempro-pro:case</ta>
            <ta e="T578" id="Seg_6695" s="T577">ptcl</ta>
            <ta e="T579" id="Seg_6696" s="T578">adj</ta>
            <ta e="T580" id="Seg_6697" s="T579">n-n:(num)-n:poss-n:case</ta>
            <ta e="T581" id="Seg_6698" s="T580">ptcl</ta>
            <ta e="T582" id="Seg_6699" s="T581">v-v:mood.pn</ta>
            <ta e="T583" id="Seg_6700" s="T582">cardnum</ta>
            <ta e="T584" id="Seg_6701" s="T583">n-n:case</ta>
            <ta e="T585" id="Seg_6702" s="T584">post</ta>
            <ta e="T586" id="Seg_6703" s="T585">v-v:tense-n:(poss)</ta>
            <ta e="T587" id="Seg_6704" s="T586">ptcl</ta>
            <ta e="T588" id="Seg_6705" s="T587">propr-n:case</ta>
            <ta e="T589" id="Seg_6706" s="T614">n-n:poss-n:case</ta>
            <ta e="T590" id="Seg_6707" s="T589">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T591" id="Seg_6708" s="T590">conj</ta>
            <ta e="T592" id="Seg_6709" s="T591">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T598" id="Seg_6710" s="T597">v-v:tense-v:poss.pn</ta>
            <ta e="T599" id="Seg_6711" s="T598">dempro.[pro:case]</ta>
            <ta e="T600" id="Seg_6712" s="T599">post</ta>
            <ta e="T601" id="Seg_6713" s="T600">adv&gt;adv-adv</ta>
            <ta e="T602" id="Seg_6714" s="T601">v-v:tense-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_6715" s="T0">n</ta>
            <ta e="T2" id="Seg_6716" s="T1">cop</ta>
            <ta e="T3" id="Seg_6717" s="T2">ptcl</ta>
            <ta e="T4" id="Seg_6718" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_6719" s="T4">n</ta>
            <ta e="T6" id="Seg_6720" s="T5">adj</ta>
            <ta e="T7" id="Seg_6721" s="T6">cop</ta>
            <ta e="T8" id="Seg_6722" s="T7">cardnum</ta>
            <ta e="T9" id="Seg_6723" s="T8">n</ta>
            <ta e="T10" id="Seg_6724" s="T9">adj</ta>
            <ta e="T11" id="Seg_6725" s="T10">cop</ta>
            <ta e="T12" id="Seg_6726" s="T11">n</ta>
            <ta e="T13" id="Seg_6727" s="T12">adj</ta>
            <ta e="T17" id="Seg_6728" s="T16">cardnum</ta>
            <ta e="T18" id="Seg_6729" s="T17">n</ta>
            <ta e="T19" id="Seg_6730" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_6731" s="T19">cardnum</ta>
            <ta e="T21" id="Seg_6732" s="T20">adj</ta>
            <ta e="T22" id="Seg_6733" s="T21">cop</ta>
            <ta e="T23" id="Seg_6734" s="T22">adv</ta>
            <ta e="T24" id="Seg_6735" s="T23">ptcl</ta>
            <ta e="T25" id="Seg_6736" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_6737" s="T25">n</ta>
            <ta e="T27" id="Seg_6738" s="T26">n</ta>
            <ta e="T28" id="Seg_6739" s="T27">v</ta>
            <ta e="T29" id="Seg_6740" s="T28">aux</ta>
            <ta e="T30" id="Seg_6741" s="T29">n</ta>
            <ta e="T31" id="Seg_6742" s="T30">post</ta>
            <ta e="T32" id="Seg_6743" s="T31">v</ta>
            <ta e="T33" id="Seg_6744" s="T32">aux</ta>
            <ta e="T34" id="Seg_6745" s="T33">adj</ta>
            <ta e="T35" id="Seg_6746" s="T34">adj</ta>
            <ta e="T36" id="Seg_6747" s="T35">n</ta>
            <ta e="T37" id="Seg_6748" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_6749" s="T37">n</ta>
            <ta e="T39" id="Seg_6750" s="T38">v</ta>
            <ta e="T40" id="Seg_6751" s="T39">aux</ta>
            <ta e="T41" id="Seg_6752" s="T40">cardnum</ta>
            <ta e="T42" id="Seg_6753" s="T41">adj</ta>
            <ta e="T43" id="Seg_6754" s="T42">n</ta>
            <ta e="T50" id="Seg_6755" s="T49">n</ta>
            <ta e="T51" id="Seg_6756" s="T50">ptcl</ta>
            <ta e="T52" id="Seg_6757" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_6758" s="T52">adj</ta>
            <ta e="T54" id="Seg_6759" s="T53">n</ta>
            <ta e="T58" id="Seg_6760" s="T57">v</ta>
            <ta e="T59" id="Seg_6761" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_6762" s="T59">v</ta>
            <ta e="T62" id="Seg_6763" s="T60">ptcl</ta>
            <ta e="T63" id="Seg_6764" s="T62">conj</ta>
            <ta e="T64" id="Seg_6765" s="T63">adv</ta>
            <ta e="T65" id="Seg_6766" s="T64">ptcl</ta>
            <ta e="T66" id="Seg_6767" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_6768" s="T66">v</ta>
            <ta e="T68" id="Seg_6769" s="T67">n</ta>
            <ta e="T73" id="Seg_6770" s="T72">v</ta>
            <ta e="T74" id="Seg_6771" s="T73">n</ta>
            <ta e="T75" id="Seg_6772" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_6773" s="T75">v</ta>
            <ta e="T77" id="Seg_6774" s="T76">n</ta>
            <ta e="T78" id="Seg_6775" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_6776" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_6777" s="T79">interj</ta>
            <ta e="T81" id="Seg_6778" s="T80">v</ta>
            <ta e="T82" id="Seg_6779" s="T81">n</ta>
            <ta e="T83" id="Seg_6780" s="T82">v</ta>
            <ta e="T84" id="Seg_6781" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_6782" s="T84">n</ta>
            <ta e="T86" id="Seg_6783" s="T85">n</ta>
            <ta e="T87" id="Seg_6784" s="T86">v</ta>
            <ta e="T88" id="Seg_6785" s="T87">aux</ta>
            <ta e="T89" id="Seg_6786" s="T88">n</ta>
            <ta e="T90" id="Seg_6787" s="T89">n</ta>
            <ta e="T91" id="Seg_6788" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_6789" s="T91">dempro</ta>
            <ta e="T93" id="Seg_6790" s="T92">n</ta>
            <ta e="T94" id="Seg_6791" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_6792" s="T94">n</ta>
            <ta e="T96" id="Seg_6793" s="T95">v</ta>
            <ta e="T97" id="Seg_6794" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_6795" s="T97">aux</ta>
            <ta e="T99" id="Seg_6796" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_6797" s="T99">adv</ta>
            <ta e="T101" id="Seg_6798" s="T100">pers</ta>
            <ta e="T102" id="Seg_6799" s="T101">v</ta>
            <ta e="T103" id="Seg_6800" s="T102">dempro</ta>
            <ta e="T104" id="Seg_6801" s="T103">n</ta>
            <ta e="T105" id="Seg_6802" s="T104">v</ta>
            <ta e="T106" id="Seg_6803" s="T105">ptcl</ta>
            <ta e="T107" id="Seg_6804" s="T106">dempro</ta>
            <ta e="T108" id="Seg_6805" s="T107">adj</ta>
            <ta e="T109" id="Seg_6806" s="T108">n</ta>
            <ta e="T110" id="Seg_6807" s="T109">adj</ta>
            <ta e="T111" id="Seg_6808" s="T110">n</ta>
            <ta e="T112" id="Seg_6809" s="T111">n</ta>
            <ta e="T113" id="Seg_6810" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_6811" s="T113">v</ta>
            <ta e="T115" id="Seg_6812" s="T114">ptcl</ta>
            <ta e="T116" id="Seg_6813" s="T115">dempro</ta>
            <ta e="T117" id="Seg_6814" s="T116">n</ta>
            <ta e="T118" id="Seg_6815" s="T117">ptcl</ta>
            <ta e="T119" id="Seg_6816" s="T118">v</ta>
            <ta e="T120" id="Seg_6817" s="T119">adv</ta>
            <ta e="T121" id="Seg_6818" s="T120">n</ta>
            <ta e="T122" id="Seg_6819" s="T121">v</ta>
            <ta e="T123" id="Seg_6820" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_6821" s="T123">adv</ta>
            <ta e="T125" id="Seg_6822" s="T124">pers</ta>
            <ta e="T126" id="Seg_6823" s="T125">v</ta>
            <ta e="T127" id="Seg_6824" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_6825" s="T127">interj</ta>
            <ta e="T129" id="Seg_6826" s="T128">que</ta>
            <ta e="T130" id="Seg_6827" s="T129">v</ta>
            <ta e="T131" id="Seg_6828" s="T130">ptcl</ta>
            <ta e="T132" id="Seg_6829" s="T131">v</ta>
            <ta e="T135" id="Seg_6830" s="T134">dempro</ta>
            <ta e="T136" id="Seg_6831" s="T135">n</ta>
            <ta e="T137" id="Seg_6832" s="T136">v</ta>
            <ta e="T138" id="Seg_6833" s="T137">n</ta>
            <ta e="T139" id="Seg_6834" s="T138">v</ta>
            <ta e="T140" id="Seg_6835" s="T139">ptcl</ta>
            <ta e="T141" id="Seg_6836" s="T140">n</ta>
            <ta e="T142" id="Seg_6837" s="T141">v</ta>
            <ta e="T143" id="Seg_6838" s="T142">v</ta>
            <ta e="T144" id="Seg_6839" s="T143">n</ta>
            <ta e="T145" id="Seg_6840" s="T144">v</ta>
            <ta e="T148" id="Seg_6841" s="T147">ptcl</ta>
            <ta e="T149" id="Seg_6842" s="T148">que</ta>
            <ta e="T150" id="Seg_6843" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_6844" s="T150">n</ta>
            <ta e="T152" id="Seg_6845" s="T151">cop</ta>
            <ta e="T153" id="Seg_6846" s="T152">n</ta>
            <ta e="T154" id="Seg_6847" s="T153">v</ta>
            <ta e="T155" id="Seg_6848" s="T154">ptcl</ta>
            <ta e="T157" id="Seg_6849" s="T155">v</ta>
            <ta e="T158" id="Seg_6850" s="T157">adv</ta>
            <ta e="T159" id="Seg_6851" s="T158">ptcl</ta>
            <ta e="T160" id="Seg_6852" s="T159">ptcl</ta>
            <ta e="T165" id="Seg_6853" s="T164">pers</ta>
            <ta e="T166" id="Seg_6854" s="T165">ptcl</ta>
            <ta e="T167" id="Seg_6855" s="T166">adv</ta>
            <ta e="T168" id="Seg_6856" s="T167">ptcl</ta>
            <ta e="T169" id="Seg_6857" s="T168">dempro</ta>
            <ta e="T170" id="Seg_6858" s="T169">ptcl</ta>
            <ta e="T171" id="Seg_6859" s="T170">v</ta>
            <ta e="T172" id="Seg_6860" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_6861" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_6862" s="T173">n</ta>
            <ta e="T175" id="Seg_6863" s="T174">v</ta>
            <ta e="T176" id="Seg_6864" s="T175">conj</ta>
            <ta e="T177" id="Seg_6865" s="T176">v</ta>
            <ta e="T178" id="Seg_6866" s="T177">aux</ta>
            <ta e="T179" id="Seg_6867" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_6868" s="T179">dempro</ta>
            <ta e="T181" id="Seg_6869" s="T180">dempro</ta>
            <ta e="T182" id="Seg_6870" s="T181">n</ta>
            <ta e="T183" id="Seg_6871" s="T182">v</ta>
            <ta e="T184" id="Seg_6872" s="T183">adv</ta>
            <ta e="T185" id="Seg_6873" s="T184">v</ta>
            <ta e="T186" id="Seg_6874" s="T185">aux</ta>
            <ta e="T187" id="Seg_6875" s="T186">ptcl</ta>
            <ta e="T188" id="Seg_6876" s="T187">n</ta>
            <ta e="T189" id="Seg_6877" s="T188">post</ta>
            <ta e="T190" id="Seg_6878" s="T189">v</ta>
            <ta e="T191" id="Seg_6879" s="T190">aux</ta>
            <ta e="T192" id="Seg_6880" s="T191">v</ta>
            <ta e="T193" id="Seg_6881" s="T192">v</ta>
            <ta e="T194" id="Seg_6882" s="T193">n</ta>
            <ta e="T195" id="Seg_6883" s="T194">v</ta>
            <ta e="T196" id="Seg_6884" s="T195">aux</ta>
            <ta e="T197" id="Seg_6885" s="T196">ptcl</ta>
            <ta e="T198" id="Seg_6886" s="T197">n</ta>
            <ta e="T199" id="Seg_6887" s="T198">n</ta>
            <ta e="T200" id="Seg_6888" s="T199">n</ta>
            <ta e="T201" id="Seg_6889" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_6890" s="T201">v</ta>
            <ta e="T203" id="Seg_6891" s="T202">v</ta>
            <ta e="T204" id="Seg_6892" s="T203">v</ta>
            <ta e="T205" id="Seg_6893" s="T204">v</ta>
            <ta e="T206" id="Seg_6894" s="T205">ptcl</ta>
            <ta e="T207" id="Seg_6895" s="T206">pers</ta>
            <ta e="T208" id="Seg_6896" s="T207">ptcl</ta>
            <ta e="T209" id="Seg_6897" s="T208">v</ta>
            <ta e="T210" id="Seg_6898" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_6899" s="T210">adj</ta>
            <ta e="T212" id="Seg_6900" s="T211">n</ta>
            <ta e="T213" id="Seg_6901" s="T212">v</ta>
            <ta e="T214" id="Seg_6902" s="T213">aux</ta>
            <ta e="T215" id="Seg_6903" s="T214">adj</ta>
            <ta e="T216" id="Seg_6904" s="T215">n</ta>
            <ta e="T217" id="Seg_6905" s="T216">v</ta>
            <ta e="T218" id="Seg_6906" s="T217">aux</ta>
            <ta e="T219" id="Seg_6907" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_6908" s="T219">adv</ta>
            <ta e="T221" id="Seg_6909" s="T220">v</ta>
            <ta e="T222" id="Seg_6910" s="T221">ptcl</ta>
            <ta e="T223" id="Seg_6911" s="T222">n</ta>
            <ta e="T224" id="Seg_6912" s="T223">ptcl</ta>
            <ta e="T225" id="Seg_6913" s="T224">n</ta>
            <ta e="T226" id="Seg_6914" s="T225">v</ta>
            <ta e="T227" id="Seg_6915" s="T226">cardnum</ta>
            <ta e="T228" id="Seg_6916" s="T227">n</ta>
            <ta e="T229" id="Seg_6917" s="T228">n</ta>
            <ta e="T230" id="Seg_6918" s="T229">cardnum</ta>
            <ta e="T231" id="Seg_6919" s="T230">n</ta>
            <ta e="T232" id="Seg_6920" s="T231">n</ta>
            <ta e="T233" id="Seg_6921" s="T232">v</ta>
            <ta e="T234" id="Seg_6922" s="T233">aux</ta>
            <ta e="T235" id="Seg_6923" s="T234">ptcl</ta>
            <ta e="T236" id="Seg_6924" s="T235">n</ta>
            <ta e="T237" id="Seg_6925" s="T236">v</ta>
            <ta e="T238" id="Seg_6926" s="T237">v</ta>
            <ta e="T239" id="Seg_6927" s="T238">ptcl</ta>
            <ta e="T240" id="Seg_6928" s="T239">n</ta>
            <ta e="T241" id="Seg_6929" s="T240">adv</ta>
            <ta e="T242" id="Seg_6930" s="T241">v</ta>
            <ta e="T243" id="Seg_6931" s="T242">ptcl</ta>
            <ta e="T244" id="Seg_6932" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_6933" s="T244">dempro</ta>
            <ta e="T246" id="Seg_6934" s="T245">n</ta>
            <ta e="T247" id="Seg_6935" s="T246">ptcl</ta>
            <ta e="T248" id="Seg_6936" s="T247">adv</ta>
            <ta e="T249" id="Seg_6937" s="T248">conj</ta>
            <ta e="T250" id="Seg_6938" s="T249">v</ta>
            <ta e="T251" id="Seg_6939" s="T250">ptcl</ta>
            <ta e="T253" id="Seg_6940" s="T252">v</ta>
            <ta e="T255" id="Seg_6941" s="T253">ptcl</ta>
            <ta e="T256" id="Seg_6942" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_6943" s="T256">ad</ta>
            <ta e="T258" id="Seg_6944" s="T257">v</ta>
            <ta e="T259" id="Seg_6945" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_6946" s="T259">adj</ta>
            <ta e="T261" id="Seg_6947" s="T260">n</ta>
            <ta e="T262" id="Seg_6948" s="T261">v</ta>
            <ta e="T263" id="Seg_6949" s="T262">aux</ta>
            <ta e="T264" id="Seg_6950" s="T263">ptcl</ta>
            <ta e="T265" id="Seg_6951" s="T264">adv</ta>
            <ta e="T266" id="Seg_6952" s="T265">adj</ta>
            <ta e="T267" id="Seg_6953" s="T266">n</ta>
            <ta e="T268" id="Seg_6954" s="T267">ptcl</ta>
            <ta e="T269" id="Seg_6955" s="T268">n</ta>
            <ta e="T270" id="Seg_6956" s="T269">n</ta>
            <ta e="T271" id="Seg_6957" s="T270">n</ta>
            <ta e="T272" id="Seg_6958" s="T271">v</ta>
            <ta e="T273" id="Seg_6959" s="T272">ptcl</ta>
            <ta e="T274" id="Seg_6960" s="T273">conj</ta>
            <ta e="T275" id="Seg_6961" s="T274">emphpro</ta>
            <ta e="T276" id="Seg_6962" s="T275">ptcl</ta>
            <ta e="T277" id="Seg_6963" s="T276">ptcl</ta>
            <ta e="T278" id="Seg_6964" s="T277">n</ta>
            <ta e="T279" id="Seg_6965" s="T278">n</ta>
            <ta e="T280" id="Seg_6966" s="T279">adj</ta>
            <ta e="T281" id="Seg_6967" s="T280">adv</ta>
            <ta e="T282" id="Seg_6968" s="T281">n</ta>
            <ta e="T283" id="Seg_6969" s="T282">v</ta>
            <ta e="T284" id="Seg_6970" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_6971" s="T284">interj</ta>
            <ta e="T286" id="Seg_6972" s="T285">adv</ta>
            <ta e="T287" id="Seg_6973" s="T286">ptcl</ta>
            <ta e="T288" id="Seg_6974" s="T287">adv</ta>
            <ta e="T289" id="Seg_6975" s="T288">v</ta>
            <ta e="T290" id="Seg_6976" s="T289">v</ta>
            <ta e="T291" id="Seg_6977" s="T290">adv</ta>
            <ta e="T292" id="Seg_6978" s="T291">v</ta>
            <ta e="T293" id="Seg_6979" s="T292">aux</ta>
            <ta e="T294" id="Seg_6980" s="T293">ptcl</ta>
            <ta e="T295" id="Seg_6981" s="T294">v</ta>
            <ta e="T296" id="Seg_6982" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_6983" s="T296">n</ta>
            <ta e="T298" id="Seg_6984" s="T297">n</ta>
            <ta e="T299" id="Seg_6985" s="T298">v</ta>
            <ta e="T300" id="Seg_6986" s="T299">v</ta>
            <ta e="T301" id="Seg_6987" s="T300">aux</ta>
            <ta e="T302" id="Seg_6988" s="T301">ptcl</ta>
            <ta e="T303" id="Seg_6989" s="T302">n</ta>
            <ta e="T304" id="Seg_6990" s="T303">v</ta>
            <ta e="T305" id="Seg_6991" s="T304">aux</ta>
            <ta e="T306" id="Seg_6992" s="T305">dempro</ta>
            <ta e="T307" id="Seg_6993" s="T306">v</ta>
            <ta e="T308" id="Seg_6994" s="T307">v</ta>
            <ta e="T309" id="Seg_6995" s="T308">adj</ta>
            <ta e="T310" id="Seg_6996" s="T309">n</ta>
            <ta e="T311" id="Seg_6997" s="T310">v</ta>
            <ta e="T312" id="Seg_6998" s="T311">aux</ta>
            <ta e="T313" id="Seg_6999" s="T312">n</ta>
            <ta e="T314" id="Seg_7000" s="T313">ptcl</ta>
            <ta e="T315" id="Seg_7001" s="T314">adv</ta>
            <ta e="T316" id="Seg_7002" s="T315">v</ta>
            <ta e="T317" id="Seg_7003" s="T316">n</ta>
            <ta e="T318" id="Seg_7004" s="T317">ptcl</ta>
            <ta e="T319" id="Seg_7005" s="T318">v</ta>
            <ta e="T320" id="Seg_7006" s="T319">v</ta>
            <ta e="T321" id="Seg_7007" s="T320">ptcl</ta>
            <ta e="T322" id="Seg_7008" s="T321">adv</ta>
            <ta e="T323" id="Seg_7009" s="T322">v</ta>
            <ta e="T324" id="Seg_7010" s="T323">que</ta>
            <ta e="T325" id="Seg_7011" s="T324">pers</ta>
            <ta e="T326" id="Seg_7012" s="T325">adj</ta>
            <ta e="T327" id="Seg_7013" s="T326">ptcl</ta>
            <ta e="T328" id="Seg_7014" s="T327">v</ta>
            <ta e="T329" id="Seg_7015" s="T328">ptcl</ta>
            <ta e="T330" id="Seg_7016" s="T329">n</ta>
            <ta e="T331" id="Seg_7017" s="T330">n</ta>
            <ta e="T332" id="Seg_7018" s="T331">n</ta>
            <ta e="T333" id="Seg_7019" s="T332">ptcl</ta>
            <ta e="T334" id="Seg_7020" s="T333">v</ta>
            <ta e="T335" id="Seg_7021" s="T334">v</ta>
            <ta e="T336" id="Seg_7022" s="T335">ptcl</ta>
            <ta e="T337" id="Seg_7023" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_7024" s="T337">interj</ta>
            <ta e="T339" id="Seg_7025" s="T338">pers</ta>
            <ta e="T340" id="Seg_7026" s="T339">v</ta>
            <ta e="T341" id="Seg_7027" s="T340">ptcl</ta>
            <ta e="T342" id="Seg_7028" s="T341">v</ta>
            <ta e="T343" id="Seg_7029" s="T342">aux</ta>
            <ta e="T344" id="Seg_7030" s="T343">adj</ta>
            <ta e="T345" id="Seg_7031" s="T344">n</ta>
            <ta e="T346" id="Seg_7032" s="T345">que</ta>
            <ta e="T347" id="Seg_7033" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_7034" s="T347">v</ta>
            <ta e="T349" id="Seg_7035" s="T348">aux</ta>
            <ta e="T350" id="Seg_7036" s="T349">ptcl</ta>
            <ta e="T351" id="Seg_7037" s="T350">pers</ta>
            <ta e="T352" id="Seg_7038" s="T351">adj</ta>
            <ta e="T353" id="Seg_7039" s="T352">n</ta>
            <ta e="T354" id="Seg_7040" s="T353">v</ta>
            <ta e="T355" id="Seg_7041" s="T354">aux</ta>
            <ta e="T356" id="Seg_7042" s="T355">ptcl</ta>
            <ta e="T357" id="Seg_7043" s="T356">adv</ta>
            <ta e="T358" id="Seg_7044" s="T357">ptcl</ta>
            <ta e="T359" id="Seg_7045" s="T358">n</ta>
            <ta e="T360" id="Seg_7046" s="T359">v</ta>
            <ta e="T361" id="Seg_7047" s="T360">n</ta>
            <ta e="T362" id="Seg_7048" s="T361">n</ta>
            <ta e="T363" id="Seg_7049" s="T362">post</ta>
            <ta e="T364" id="Seg_7050" s="T363">ptcl</ta>
            <ta e="T367" id="Seg_7051" s="T366">n</ta>
            <ta e="T368" id="Seg_7052" s="T367">ptcl</ta>
            <ta e="T369" id="Seg_7053" s="T368">cop</ta>
            <ta e="T370" id="Seg_7054" s="T369">dempro</ta>
            <ta e="T371" id="Seg_7055" s="T370">adj</ta>
            <ta e="T372" id="Seg_7056" s="T371">n</ta>
            <ta e="T373" id="Seg_7057" s="T372">v</ta>
            <ta e="T374" id="Seg_7058" s="T373">post</ta>
            <ta e="T375" id="Seg_7059" s="T374">n</ta>
            <ta e="T376" id="Seg_7060" s="T375">v</ta>
            <ta e="T377" id="Seg_7061" s="T376">v</ta>
            <ta e="T378" id="Seg_7062" s="T377">adv</ta>
            <ta e="T381" id="Seg_7063" s="T380">n</ta>
            <ta e="T382" id="Seg_7064" s="T381">v</ta>
            <ta e="T386" id="Seg_7065" s="T385">pers</ta>
            <ta e="T387" id="Seg_7066" s="T386">v</ta>
            <ta e="T389" id="Seg_7067" s="T387">ptcl</ta>
            <ta e="T390" id="Seg_7068" s="T389">n</ta>
            <ta e="T391" id="Seg_7069" s="T390">adj</ta>
            <ta e="T392" id="Seg_7070" s="T391">n</ta>
            <ta e="T393" id="Seg_7071" s="T392">v</ta>
            <ta e="T394" id="Seg_7072" s="T393">aux</ta>
            <ta e="T395" id="Seg_7073" s="T394">posspr</ta>
            <ta e="T396" id="Seg_7074" s="T395">adv</ta>
            <ta e="T397" id="Seg_7075" s="T396">v</ta>
            <ta e="T398" id="Seg_7076" s="T397">aux</ta>
            <ta e="T399" id="Seg_7077" s="T398">emphpro</ta>
            <ta e="T400" id="Seg_7078" s="T399">ptcl</ta>
            <ta e="T401" id="Seg_7079" s="T400">pers</ta>
            <ta e="T402" id="Seg_7080" s="T401">adj</ta>
            <ta e="T403" id="Seg_7081" s="T402">n</ta>
            <ta e="T404" id="Seg_7082" s="T403">v</ta>
            <ta e="T405" id="Seg_7083" s="T404">aux</ta>
            <ta e="T406" id="Seg_7084" s="T405">dempro</ta>
            <ta e="T407" id="Seg_7085" s="T406">n</ta>
            <ta e="T408" id="Seg_7086" s="T407">v</ta>
            <ta e="T409" id="Seg_7087" s="T408">ptcl</ta>
            <ta e="T410" id="Seg_7088" s="T409">n</ta>
            <ta e="T411" id="Seg_7089" s="T410">v</ta>
            <ta e="T412" id="Seg_7090" s="T411">aux</ta>
            <ta e="T413" id="Seg_7091" s="T412">ptcl</ta>
            <ta e="T414" id="Seg_7092" s="T413">que</ta>
            <ta e="T415" id="Seg_7093" s="T414">n</ta>
            <ta e="T416" id="Seg_7094" s="T415">v</ta>
            <ta e="T417" id="Seg_7095" s="T416">ptcl</ta>
            <ta e="T418" id="Seg_7096" s="T417">n</ta>
            <ta e="T419" id="Seg_7097" s="T418">n</ta>
            <ta e="T420" id="Seg_7098" s="T419">ptcl</ta>
            <ta e="T421" id="Seg_7099" s="T420">adv</ta>
            <ta e="T422" id="Seg_7100" s="T421">ptcl</ta>
            <ta e="T423" id="Seg_7101" s="T422">n</ta>
            <ta e="T424" id="Seg_7102" s="T423">cardnum</ta>
            <ta e="T425" id="Seg_7103" s="T424">n</ta>
            <ta e="T426" id="Seg_7104" s="T425">ptcl</ta>
            <ta e="T427" id="Seg_7105" s="T426">cop</ta>
            <ta e="T429" id="Seg_7106" s="T428">dempro</ta>
            <ta e="T430" id="Seg_7107" s="T429">v</ta>
            <ta e="T431" id="Seg_7108" s="T430">interj</ta>
            <ta e="T432" id="Seg_7109" s="T431">ptcl</ta>
            <ta e="T433" id="Seg_7110" s="T432">que</ta>
            <ta e="T434" id="Seg_7111" s="T433">cop</ta>
            <ta e="T435" id="Seg_7112" s="T434">que</ta>
            <ta e="T436" id="Seg_7113" s="T435">que</ta>
            <ta e="T438" id="Seg_7114" s="T436">cop</ta>
            <ta e="T439" id="Seg_7115" s="T438">interj</ta>
            <ta e="T440" id="Seg_7116" s="T439">dempro</ta>
            <ta e="T441" id="Seg_7117" s="T440">pers</ta>
            <ta e="T442" id="Seg_7118" s="T441">adv</ta>
            <ta e="T443" id="Seg_7119" s="T442">dempro</ta>
            <ta e="T444" id="Seg_7120" s="T443">n</ta>
            <ta e="T445" id="Seg_7121" s="T444">v</ta>
            <ta e="T446" id="Seg_7122" s="T445">dempro</ta>
            <ta e="T447" id="Seg_7123" s="T446">ptcl</ta>
            <ta e="T448" id="Seg_7124" s="T447">n</ta>
            <ta e="T449" id="Seg_7125" s="T448">v</ta>
            <ta e="T450" id="Seg_7126" s="T449">dempro</ta>
            <ta e="T451" id="Seg_7127" s="T450">n</ta>
            <ta e="T452" id="Seg_7128" s="T451">n</ta>
            <ta e="T453" id="Seg_7129" s="T452">v</ta>
            <ta e="T454" id="Seg_7130" s="T453">v</ta>
            <ta e="T455" id="Seg_7131" s="T454">v</ta>
            <ta e="T456" id="Seg_7132" s="T455">n</ta>
            <ta e="T457" id="Seg_7133" s="T456">que</ta>
            <ta e="T458" id="Seg_7134" s="T457">v</ta>
            <ta e="T459" id="Seg_7135" s="T458">n</ta>
            <ta e="T460" id="Seg_7136" s="T459">ptcl</ta>
            <ta e="T461" id="Seg_7137" s="T460">adj</ta>
            <ta e="T462" id="Seg_7138" s="T461">n</ta>
            <ta e="T463" id="Seg_7139" s="T462">adj</ta>
            <ta e="T466" id="Seg_7140" s="T465">adj</ta>
            <ta e="T467" id="Seg_7141" s="T466">interj</ta>
            <ta e="T468" id="Seg_7142" s="T467">adv</ta>
            <ta e="T469" id="Seg_7143" s="T468">pers</ta>
            <ta e="T470" id="Seg_7144" s="T469">cardnum</ta>
            <ta e="T471" id="Seg_7145" s="T470">n</ta>
            <ta e="T472" id="Seg_7146" s="T471">v</ta>
            <ta e="T473" id="Seg_7147" s="T472">aux</ta>
            <ta e="T474" id="Seg_7148" s="T473">n</ta>
            <ta e="T475" id="Seg_7149" s="T474">n</ta>
            <ta e="T476" id="Seg_7150" s="T475">v</ta>
            <ta e="T477" id="Seg_7151" s="T476">ptcl</ta>
            <ta e="T478" id="Seg_7152" s="T477">ptcl</ta>
            <ta e="T479" id="Seg_7153" s="T478">ptcl</ta>
            <ta e="T480" id="Seg_7154" s="T479">dempro</ta>
            <ta e="T481" id="Seg_7155" s="T480">v</ta>
            <ta e="T482" id="Seg_7156" s="T481">v</ta>
            <ta e="T483" id="Seg_7157" s="T482">aux</ta>
            <ta e="T484" id="Seg_7158" s="T483">ptcl</ta>
            <ta e="T485" id="Seg_7159" s="T484">que</ta>
            <ta e="T486" id="Seg_7160" s="T485">n</ta>
            <ta e="T487" id="Seg_7161" s="T486">ptcl</ta>
            <ta e="T488" id="Seg_7162" s="T487">interj</ta>
            <ta e="T489" id="Seg_7163" s="T488">adj</ta>
            <ta e="T490" id="Seg_7164" s="T489">ptcl</ta>
            <ta e="T491" id="Seg_7165" s="T490">conj</ta>
            <ta e="T492" id="Seg_7166" s="T491">adv</ta>
            <ta e="T493" id="Seg_7167" s="T492">ptcl</ta>
            <ta e="T494" id="Seg_7168" s="T493">ptcl</ta>
            <ta e="T495" id="Seg_7169" s="T494">que</ta>
            <ta e="T496" id="Seg_7170" s="T495">ptcl</ta>
            <ta e="T497" id="Seg_7171" s="T496">ptcl</ta>
            <ta e="T498" id="Seg_7172" s="T497">v</ta>
            <ta e="T499" id="Seg_7173" s="T498">n</ta>
            <ta e="T500" id="Seg_7174" s="T499">ptcl</ta>
            <ta e="T501" id="Seg_7175" s="T500">ptcl</ta>
            <ta e="T502" id="Seg_7176" s="T501">que</ta>
            <ta e="T503" id="Seg_7177" s="T502">ptcl</ta>
            <ta e="T504" id="Seg_7178" s="T503">ptcl</ta>
            <ta e="T505" id="Seg_7179" s="T504">adj</ta>
            <ta e="T506" id="Seg_7180" s="T505">cop</ta>
            <ta e="T507" id="Seg_7181" s="T506">adv</ta>
            <ta e="T508" id="Seg_7182" s="T507">ptcl</ta>
            <ta e="T509" id="Seg_7183" s="T508">ptcl</ta>
            <ta e="T510" id="Seg_7184" s="T509">ptcl</ta>
            <ta e="T511" id="Seg_7185" s="T510">dempro</ta>
            <ta e="T512" id="Seg_7186" s="T511">dempro</ta>
            <ta e="T513" id="Seg_7187" s="T512">ptcl</ta>
            <ta e="T514" id="Seg_7188" s="T513">dempro</ta>
            <ta e="T515" id="Seg_7189" s="T514">dempro</ta>
            <ta e="T516" id="Seg_7190" s="T515">adv</ta>
            <ta e="T517" id="Seg_7191" s="T516">adv</ta>
            <ta e="T518" id="Seg_7192" s="T517">ptcl</ta>
            <ta e="T519" id="Seg_7193" s="T518">ptcl</ta>
            <ta e="T520" id="Seg_7194" s="T519">n</ta>
            <ta e="T521" id="Seg_7195" s="T520">v</ta>
            <ta e="T522" id="Seg_7196" s="T521">ptcl</ta>
            <ta e="T523" id="Seg_7197" s="T522">n</ta>
            <ta e="T524" id="Seg_7198" s="T523">v</ta>
            <ta e="T525" id="Seg_7199" s="T524">aux</ta>
            <ta e="T526" id="Seg_7200" s="T525">dempro</ta>
            <ta e="T527" id="Seg_7201" s="T526">n</ta>
            <ta e="T528" id="Seg_7202" s="T527">ptcl</ta>
            <ta e="T529" id="Seg_7203" s="T528">ptcl</ta>
            <ta e="T530" id="Seg_7204" s="T529">que</ta>
            <ta e="T531" id="Seg_7205" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_7206" s="T531">ptcl</ta>
            <ta e="T533" id="Seg_7207" s="T532">n</ta>
            <ta e="T534" id="Seg_7208" s="T533">v</ta>
            <ta e="T535" id="Seg_7209" s="T534">aux</ta>
            <ta e="T536" id="Seg_7210" s="T535">ptcl</ta>
            <ta e="T537" id="Seg_7211" s="T536">dempro</ta>
            <ta e="T538" id="Seg_7212" s="T537">adj</ta>
            <ta e="T539" id="Seg_7213" s="T538">n</ta>
            <ta e="T540" id="Seg_7214" s="T539">v</ta>
            <ta e="T541" id="Seg_7215" s="T540">ptcl</ta>
            <ta e="T542" id="Seg_7216" s="T541">n</ta>
            <ta e="T543" id="Seg_7217" s="T542">v</ta>
            <ta e="T544" id="Seg_7218" s="T543">ptcl</ta>
            <ta e="T545" id="Seg_7219" s="T544">adv</ta>
            <ta e="T546" id="Seg_7220" s="T545">v</ta>
            <ta e="T547" id="Seg_7221" s="T546">dempro</ta>
            <ta e="T548" id="Seg_7222" s="T547">interj</ta>
            <ta e="T549" id="Seg_7223" s="T548">n</ta>
            <ta e="T550" id="Seg_7224" s="T549">v</ta>
            <ta e="T551" id="Seg_7225" s="T550">ptcl</ta>
            <ta e="T552" id="Seg_7226" s="T551">que</ta>
            <ta e="T553" id="Seg_7227" s="T552">posspr</ta>
            <ta e="T554" id="Seg_7228" s="T553">n</ta>
            <ta e="T555" id="Seg_7229" s="T554">n</ta>
            <ta e="T556" id="Seg_7230" s="T555">v</ta>
            <ta e="T557" id="Seg_7231" s="T556">aux</ta>
            <ta e="T558" id="Seg_7232" s="T557">pers</ta>
            <ta e="T559" id="Seg_7233" s="T558">ptcl</ta>
            <ta e="T560" id="Seg_7234" s="T559">propr</ta>
            <ta e="T561" id="Seg_7235" s="T560">v</ta>
            <ta e="T562" id="Seg_7236" s="T561">ptcl</ta>
            <ta e="T563" id="Seg_7237" s="T562">adv</ta>
            <ta e="T564" id="Seg_7238" s="T563">propr</ta>
            <ta e="T565" id="Seg_7239" s="T564">v</ta>
            <ta e="T566" id="Seg_7240" s="T565">propr</ta>
            <ta e="T567" id="Seg_7241" s="T566">pers</ta>
            <ta e="T568" id="Seg_7242" s="T567">v</ta>
            <ta e="T569" id="Seg_7243" s="T568">ptcl</ta>
            <ta e="T570" id="Seg_7244" s="T569">adv</ta>
            <ta e="T571" id="Seg_7245" s="T570">ptcl</ta>
            <ta e="T572" id="Seg_7246" s="T571">cardnum</ta>
            <ta e="T573" id="Seg_7247" s="T572">n</ta>
            <ta e="T574" id="Seg_7248" s="T573">n</ta>
            <ta e="T575" id="Seg_7249" s="T574">v</ta>
            <ta e="T576" id="Seg_7250" s="T575">ptcl</ta>
            <ta e="T577" id="Seg_7251" s="T576">dempro</ta>
            <ta e="T578" id="Seg_7252" s="T577">ptcl</ta>
            <ta e="T579" id="Seg_7253" s="T578">adj</ta>
            <ta e="T580" id="Seg_7254" s="T579">n</ta>
            <ta e="T581" id="Seg_7255" s="T580">ptcl</ta>
            <ta e="T582" id="Seg_7256" s="T581">v</ta>
            <ta e="T583" id="Seg_7257" s="T582">cardnum</ta>
            <ta e="T584" id="Seg_7258" s="T583">n</ta>
            <ta e="T585" id="Seg_7259" s="T584">post</ta>
            <ta e="T586" id="Seg_7260" s="T585">v</ta>
            <ta e="T587" id="Seg_7261" s="T586">ptcl</ta>
            <ta e="T588" id="Seg_7262" s="T587">propr</ta>
            <ta e="T589" id="Seg_7263" s="T614">n</ta>
            <ta e="T590" id="Seg_7264" s="T589">v</ta>
            <ta e="T591" id="Seg_7265" s="T590">conj</ta>
            <ta e="T592" id="Seg_7266" s="T591">n</ta>
            <ta e="T598" id="Seg_7267" s="T597">cop</ta>
            <ta e="T599" id="Seg_7268" s="T598">dempro</ta>
            <ta e="T600" id="Seg_7269" s="T599">post</ta>
            <ta e="T601" id="Seg_7270" s="T600">adv</ta>
            <ta e="T602" id="Seg_7271" s="T601">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T63" id="Seg_7272" s="T62">RUS:gram</ta>
            <ta e="T75" id="Seg_7273" s="T74">RUS:disc</ta>
            <ta e="T97" id="Seg_7274" s="T96">RUS:gram</ta>
            <ta e="T105" id="Seg_7275" s="T104">EV:cult</ta>
            <ta e="T108" id="Seg_7276" s="T107">EV:core</ta>
            <ta e="T121" id="Seg_7277" s="T120">RUS:core</ta>
            <ta e="T138" id="Seg_7278" s="T137">RUS:cult</ta>
            <ta e="T160" id="Seg_7279" s="T159">RUS:disc</ta>
            <ta e="T248" id="Seg_7280" s="T247">RUS:mod</ta>
            <ta e="T249" id="Seg_7281" s="T248">RUS:gram</ta>
            <ta e="T274" id="Seg_7282" s="T273">RUS:gram</ta>
            <ta e="T282" id="Seg_7283" s="T281">RUS:core</ta>
            <ta e="T298" id="Seg_7284" s="T297">RUS:cult</ta>
            <ta e="T303" id="Seg_7285" s="T302">RUS:cult</ta>
            <ta e="T306" id="Seg_7286" s="T305">EV:gram (DIM)</ta>
            <ta e="T366" id="Seg_7287" s="T364">RUS:disc</ta>
            <ta e="T380" id="Seg_7288" s="T378">RUS:disc</ta>
            <ta e="T418" id="Seg_7289" s="T417">RUS:cult</ta>
            <ta e="T447" id="Seg_7290" s="T446">RUS:mod</ta>
            <ta e="T448" id="Seg_7291" s="T447">RUS:cult</ta>
            <ta e="T452" id="Seg_7292" s="T451">RUS:cult</ta>
            <ta e="T456" id="Seg_7293" s="T455">RUS:cult</ta>
            <ta e="T465" id="Seg_7294" s="T463">RUS:mod</ta>
            <ta e="T475" id="Seg_7295" s="T474">RUS:cult</ta>
            <ta e="T486" id="Seg_7296" s="T485">RUS:cult</ta>
            <ta e="T490" id="Seg_7297" s="T489">RUS:mod</ta>
            <ta e="T491" id="Seg_7298" s="T490">RUS:gram</ta>
            <ta e="T497" id="Seg_7299" s="T496">RUS:mod</ta>
            <ta e="T510" id="Seg_7300" s="T509">RUS:disc</ta>
            <ta e="T527" id="Seg_7301" s="T526">RUS:cult</ta>
            <ta e="T542" id="Seg_7302" s="T541">RUS:cult</ta>
            <ta e="T549" id="Seg_7303" s="T548">RUS:cult</ta>
            <ta e="T559" id="Seg_7304" s="T558">RUS:mod</ta>
            <ta e="T562" id="Seg_7305" s="T561">RUS:disc</ta>
            <ta e="T591" id="Seg_7306" s="T590">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS">
            <ta e="T16" id="Seg_7307" s="T13">RUS:int.ins</ta>
            <ta e="T47" id="Seg_7308" s="T43">RUS:int.ins</ta>
            <ta e="T57" id="Seg_7309" s="T54">RUS:int.alt</ta>
            <ta e="T72" id="Seg_7310" s="T68">RUS:int.ins</ta>
            <ta e="T134" id="Seg_7311" s="T132">RUS:int.ins</ta>
            <ta e="T147" id="Seg_7312" s="T145">RUS:int.alt</ta>
            <ta e="T164" id="Seg_7313" s="T159">RUS:int.alt</ta>
            <ta e="T252" id="Seg_7314" s="T251">RUS:int.ins</ta>
            <ta e="T385" id="Seg_7315" s="T382">RUS:int.ins</ta>
            <ta e="T428" id="Seg_7316" s="T427">RUS:int.ins</ta>
            <ta e="T614" id="Seg_7317" s="T588">RUS:int.ins</ta>
            <ta e="T597" id="Seg_7318" s="T592">RUS:int.ins</ta>
            <ta e="T606" id="Seg_7319" s="T602">RUS:ext</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_7320" s="T0">When I was a child… my children were small.</ta>
            <ta e="T11" id="Seg_7321" s="T7">One child was one year old.</ta>
            <ta e="T16" id="Seg_7322" s="T11">One year and something, a year and seven months.</ta>
            <ta e="T22" id="Seg_7323" s="T16">And one child was eight months old. </ta>
            <ta e="T34" id="Seg_7324" s="T22">Then I went to collect wood with my father, I went with with my father with the sleigh. </ta>
            <ta e="T43" id="Seg_7325" s="T34">I had a sleigh… on my sleigh my little brother was sitting, my brother of five years old.</ta>
            <ta e="T62" id="Seg_7326" s="T43">I havenʼt told you yet, we have various reindeer, and a woman is not allowed to harness a spotted reindeer.</ta>
            <ta e="T74" id="Seg_7327" s="T62">And then there is the reindeer with which we cure illnesses - I'll tell you later ok? - the curing reindeer.</ta>
            <ta e="T91" id="Seg_7328" s="T74">This curing reindeer, ehm curing reindeer, when the mother of my husband was ill, she let the reindeer cure on her head.</ta>
            <ta e="T99" id="Seg_7329" s="T91">A woman should not harness this reindeer.</ta>
            <ta e="T106" id="Seg_7330" s="T99">Then I, not knowing this reindeer, harnessed it.</ta>
            <ta e="T111" id="Seg_7331" s="T106">It is a white reindeer, an off-white reindeer.</ta>
            <ta e="T119" id="Seg_7332" s="T111">So when we were travelling this reindeer sneezed.</ta>
            <ta e="T132" id="Seg_7333" s="T119">It sneezed three times, and then I say, “Oh dear! Why is it sneezing?” I say.</ta>
            <ta e="T157" id="Seg_7334" s="T132">And before that, when we hit the road, my mum says that the fire speaks, that means that something bad will happen, when the fire speaks.</ta>
            <ta e="T172" id="Seg_7335" s="T157">Then I didn't really believe that.</ta>
            <ta e="T182" id="Seg_7336" s="T172">We caught the reindeer and took off.</ta>
            <ta e="T193" id="Seg_7337" s="T182">We caught them and left, we went together with my father.</ta>
            <ta e="T206" id="Seg_7338" s="T193">When we arrived at the camp my father loaded the sleigh, he loaded it tied it and so on.</ta>
            <ta e="T214" id="Seg_7339" s="T206">He made me hit the road, leading my harnessed reindeer. </ta>
            <ta e="T229" id="Seg_7340" s="T214">After I lead the harnessed reindeer I sat past the sleigh, on the sleigh [we] had loaded six bags of fish.</ta>
            <ta e="T235" id="Seg_7341" s="T229">[We] had loaded six bags of fish.</ta>
            <ta e="T243" id="Seg_7342" s="T235">I jumped on the sleigh, I say, again I sat past the sleigh.</ta>
            <ta e="T251" id="Seg_7343" s="T243">No, this human (?), I jumped once again.</ta>
            <ta e="T255" id="Seg_7344" s="T251">I jumped once again.</ta>
            <ta e="T259" id="Seg_7345" s="T255">Again I sat past it.</ta>
            <ta e="T264" id="Seg_7346" s="T259">I let my harnessed reindeer go.</ta>
            <ta e="T273" id="Seg_7347" s="T264">So my harnessed reindeer, my sleigh reindeer went over me.</ta>
            <ta e="T284" id="Seg_7348" s="T273">And I myself, every supporting pole in the sleigh, I turned around three times.</ta>
            <ta e="T290" id="Seg_7349" s="T284">Oh dear, I fell on my back.</ta>
            <ta e="T302" id="Seg_7350" s="T290">After I fell on my back I lie there, my father was putting on his poncho.</ta>
            <ta e="T307" id="Seg_7351" s="T302">After he had put on his poncho he looked.</ta>
            <ta e="T317" id="Seg_7352" s="T307">He saw that the reindeer sleigh went off and a person lies here on the road.</ta>
            <ta e="T328" id="Seg_7353" s="T317">He came running and said: “Oh! Are you alive?” he said.</ta>
            <ta e="T335" id="Seg_7354" s="T328">“Yes, I am alive, I am alive, just my leg can't move.”</ta>
            <ta e="T340" id="Seg_7355" s="T335">“Yeah, wait, I am coming!”</ta>
            <ta e="T356" id="Seg_7356" s="T340">After he had come, and after we had somehow mounted his reindeer sleigh, we went chasing my reindeer sleigh.</ta>
            <ta e="T361" id="Seg_7357" s="T356">Then we climbed up the slope of the lake.</ta>
            <ta e="T382" id="Seg_7358" s="T361">Along the slope was just my younger brother, the poor one got off the sleigh and caught the lead, and so he stopped the reindeer.</ta>
            <ta e="T389" id="Seg_7359" s="T382">And at that time we arrived.</ta>
            <ta e="T395" id="Seg_7360" s="T389">My father tied my reindeer sleigh.</ta>
            <ta e="T409" id="Seg_7361" s="T395">After doing that and after he sat himself onto my reindeer sleigh we went home.</ta>
            <ta e="T417" id="Seg_7362" s="T409">After coming home he stopped at my door.</ta>
            <ta e="T427" id="Seg_7363" s="T417">At the door of the balok were old women, there were two old women.</ta>
            <ta e="T438" id="Seg_7364" s="T427">Two old women, they came: “Well, what happened to you, what happened to you?”</ta>
            <ta e="T449" id="Seg_7365" s="T438">“Oh dear, this woman just got injured, it's necessary to call a doctor.”</ta>
            <ta e="T455" id="Seg_7366" s="T449">Then my father, my mother says: “Don't go, don't go.”</ta>
            <ta e="T467" id="Seg_7367" s="T455">“A doctor what for?” (s)he says, “Thank God everything is ok as it were, the lungs(?), wait a second.”</ta>
            <ta e="T477" id="Seg_7368" s="T467">Then they lifted me from two sides and brought me back to the balok.</ta>
            <ta e="T484" id="Seg_7369" s="T477">Then they undressed me and so on.</ta>
            <ta e="T489" id="Seg_7370" s="T484">“Why a doctor, wait, everything is ok.</ta>
            <ta e="T508" id="Seg_7371" s="T489">Maybe just nothing seems to be broken, there is no blood, there is nothing, it will be ok, yeah.”</ta>
            <ta e="T544" id="Seg_7372" s="T508">Then after a month had passed, there was no doctor, there was nothing, after a month we went [to the village] for New Year, and we went to the doctor. </ta>
            <ta e="T547" id="Seg_7373" s="T544">We went.</ta>
            <ta e="T561" id="Seg_7374" s="T547">And the doctor says: “Oh your hip is broken, it's necessary to send you to Khatanga.”</ta>
            <ta e="T571" id="Seg_7375" s="T561">Then they sent me to Khatanga and I lay there.</ta>
            <ta e="T576" id="Seg_7376" s="T571">I lay two months in traction.</ta>
            <ta e="T588" id="Seg_7377" s="T576">And then the other months, a whole eight months I lay in Khatanga.</ta>
            <ta e="T590" id="Seg_7378" s="T588">In June, in June I came home, I came home.</ta>
            <ta e="T602" id="Seg_7379" s="T590">And my leg got better, as if it was broken.</ta>
            <ta e="T606" id="Seg_7380" s="T602">This is how I came back.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_7381" s="T0">Als ich ein Kind war… meine Kinder waren klein.</ta>
            <ta e="T11" id="Seg_7382" s="T7">Eins meiner Kinder war ein Jahr alt.</ta>
            <ta e="T16" id="Seg_7383" s="T11">Ein Jahr und ein Bisschen, ein Jahr und sieben Monate.</ta>
            <ta e="T22" id="Seg_7384" s="T16">Und ein Kind war acht Monate alt.</ta>
            <ta e="T34" id="Seg_7385" s="T22">Dann ging ich meinem Vater Holz sammeln, ich fuhr mit meinem Vater mit dem Schlitten.</ta>
            <ta e="T43" id="Seg_7386" s="T34">Ich hatte einen Schlitten… auf meinem Schlitten saß mein kleiner Bruder, mein fünfjähriger Bruder.</ta>
            <ta e="T62" id="Seg_7387" s="T43">Ich habe Ihnen noch nicht erzählt, wir haben unterschiedliche Rentiere, und eine Frau darf kein scheckiges Rentier anspannen.</ta>
            <ta e="T74" id="Seg_7388" s="T62">Und dann gibt es dann Rentier, mit dem man heilt - ich erzähle später, ja - das Heilrentier. </ta>
            <ta e="T91" id="Seg_7389" s="T74">Dieses Heilrentiert, ähm, das Heilrentier, als die Mutter meines Mannes krank war, ließ sie das Rentier ihren Kopf heilen.</ta>
            <ta e="T99" id="Seg_7390" s="T91">Eine Frau darf jenes Rentier nicht anspannen.</ta>
            <ta e="T106" id="Seg_7391" s="T99">Dann, ohne das Rentier zu kennen, spannte ich es an. </ta>
            <ta e="T111" id="Seg_7392" s="T106">Es ist ein weißes Rentier, ein grauweißes Rentier.</ta>
            <ta e="T119" id="Seg_7393" s="T111">Und während wir fuhren, nieste dieses Rentier.</ta>
            <ta e="T132" id="Seg_7394" s="T119">Es nieste drei mal, und dann sage ich: "Oh nein, warum niest es?", sage ich.</ta>
            <ta e="T157" id="Seg_7395" s="T132">Und davor, als wir losfuhren, sagt[e] meine Mutter, dass das Feuer spricht, das heißt, dass etwas Schlechtes passieren wird, wenn das Feuer spricht.</ta>
            <ta e="T172" id="Seg_7396" s="T157">Damals habe ich das nicht wirklich geglaubt.</ta>
            <ta e="T182" id="Seg_7397" s="T172">Wir fingen die Rentiere und fuhren los.</ta>
            <ta e="T193" id="Seg_7398" s="T182">Wir fingen sie und fuhren los, ich fuhr zusammen mit meinem Vater.</ta>
            <ta e="T206" id="Seg_7399" s="T193">Als wir am Lagerplatz ankamen, belud mein Vater den Schlitten, er belud ihn, band es fest und so weiter.</ta>
            <ta e="T214" id="Seg_7400" s="T206">Er schickte mich auf den Weg und führte meine angespannten Rentiere.</ta>
            <ta e="T229" id="Seg_7401" s="T214">Nachdem ich die angespannten Rentiere geführt hatte, verfehlte ich [den Schlitten], auf dem Schlitten hatten [wir] sechs Säcke Fisch geladen.</ta>
            <ta e="T235" id="Seg_7402" s="T229">[Wir] hatten sechs Säcke Fisck geladen.</ta>
            <ta e="T243" id="Seg_7403" s="T235">Ich sprang auf den Schlitten, sage ich, ich verfehle den Schlitten.</ta>
            <ta e="T251" id="Seg_7404" s="T243">Nein, dieser Mensch (?), ich sprang wieder.</ta>
            <ta e="T255" id="Seg_7405" s="T251">Ich sprang noch mal.</ta>
            <ta e="T259" id="Seg_7406" s="T255">Wieder verfehlte ich [ihn].</ta>
            <ta e="T264" id="Seg_7407" s="T259">Ich ließ meine angespannten Rentiere los.</ta>
            <ta e="T273" id="Seg_7408" s="T264">Und so liefen meine angespannten Rentieren, meine Schlittenrentiere über mich hinüber. </ta>
            <ta e="T284" id="Seg_7409" s="T273">Und ich selbst, mit jedem Stützbalken des Schlittens, ich drehte mich drei mal.</ta>
            <ta e="T290" id="Seg_7410" s="T284">Oh nein, ich fiel auf den Rücken.</ta>
            <ta e="T302" id="Seg_7411" s="T290">Nachdem ich auf den Rücken fiel, liege ich da, mein Vater zog seinen Mantel an.</ta>
            <ta e="T307" id="Seg_7412" s="T302">Als er sich den Mantel angezogen hatte, schaute er.</ta>
            <ta e="T317" id="Seg_7413" s="T307">Er sieht dass der Rentierschlitten wegfährt und dass ein Mensch auf dem Weg liegt.</ta>
            <ta e="T328" id="Seg_7414" s="T317">Er kam angerannt und sagte: "Oh, lebst du?", sagte er.</ta>
            <ta e="T335" id="Seg_7415" s="T328">"Ja, ich lebe, ich lebe, nur mein Bein bewegt sich nicht."</ta>
            <ta e="T340" id="Seg_7416" s="T335">"Ja, warte, ich komme!"</ta>
            <ta e="T356" id="Seg_7417" s="T340">Nachdem er gekommen war und nachdem wir irgendwie auf seinen Rentierschlitten gestiegen waren, verfolgten wir meinen Rentierschlitten.</ta>
            <ta e="T361" id="Seg_7418" s="T356">Dann kletterten wir auf einen Abhang am See.</ta>
            <ta e="T382" id="Seg_7419" s="T361">Auf dem Abhang war mein jüngerer Bruder, der Arme war vom Schlitten gefallen (/gesprungen?) und hatte die Zügel gegriffen, so stoppte er die Rentiere.</ta>
            <ta e="T389" id="Seg_7420" s="T382">Und du der Zeit kamen wir an.</ta>
            <ta e="T395" id="Seg_7421" s="T389">Mein Vater band meinen Rentierschlitten fest.</ta>
            <ta e="T409" id="Seg_7422" s="T395">Danach und nachdem er sich selbst auf meinen Rentierschlitten gesetzt hatte, fuhren wir nach Hause.</ta>
            <ta e="T417" id="Seg_7423" s="T409">Nachdem [wir] nach Hause kamen, hielt er an meiner Tür an.</ta>
            <ta e="T427" id="Seg_7424" s="T417">An der Tür meines Baloks waren alte Frauen, da waren zwei alte Frauen.</ta>
            <ta e="T438" id="Seg_7425" s="T427">Zwei alte Frauen, sie kamen: "Nun, was ist mit euch passiert, was ist mit euch los?"</ta>
            <ta e="T449" id="Seg_7426" s="T438">"Oh nein, diese Frau ist verletzt, man muss einen Arzt rufen."</ta>
            <ta e="T455" id="Seg_7427" s="T449">Dann sagen mein Vater und meine Mutter: "Geh nicht, geh nicht."</ta>
            <ta e="T467" id="Seg_7428" s="T455">"Wozu ein Doktor?", sagt er/sie, "Gott sei Dank ist es gut, die Lungen(?), warte".</ta>
            <ta e="T477" id="Seg_7429" s="T467">Dann hoben sie mich von Seiten hoch und brachten mich zurück in den Balok.</ta>
            <ta e="T484" id="Seg_7430" s="T477">Sie zogen mich aus und so.</ta>
            <ta e="T489" id="Seg_7431" s="T484">"Wofür ein Arzt, warte, alles ist gut.</ta>
            <ta e="T508" id="Seg_7432" s="T489">Vielleicht ist gar nichts gebrochen, da ist kein Blut, da ist nichts, es wird gut, ja."</ta>
            <ta e="T544" id="Seg_7433" s="T508">Dann nachdem ein Monat vergangen war, da war kein Arzt, da war nichts, als wir nach einem Monat zu Neujahr [ins Dorf] fuhren, gingen wir zum Arzt.</ta>
            <ta e="T547" id="Seg_7434" s="T544">Wir fuhren hin.</ta>
            <ta e="T561" id="Seg_7435" s="T547">Und der Arzt sagt: "Oh, deine Hüfte ist gebrochen, man muss dich nach Chatanga schicken."</ta>
            <ta e="T571" id="Seg_7436" s="T561">Dann wurde ich nach Chatanga geschickt und ich lag da.</ta>
            <ta e="T576" id="Seg_7437" s="T571">Ich lag zwei Monate im Streckverband.</ta>
            <ta e="T588" id="Seg_7438" s="T576">Und dann die anderen Monate, ganze acht Monate lag ich in Chatanga.</ta>
            <ta e="T590" id="Seg_7439" s="T588">Im Juni, im Juni kam ich nach Hause, ich kam nach Hause.</ta>
            <ta e="T602" id="Seg_7440" s="T590">Und mein Bein heilte so, wie es gebrochen war.</ta>
            <ta e="T606" id="Seg_7441" s="T602">Und so kam ich zurück.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T7" id="Seg_7442" s="T0">Когда я была ребёнком… мои дети были маленькими.</ta>
            <ta e="T11" id="Seg_7443" s="T7">Одному ребёнку был год.</ta>
            <ta e="T16" id="Seg_7444" s="T11">Год с небольшим, год и семь.</ta>
            <ta e="T22" id="Seg_7445" s="T16">А одному ребёнку было восемь месяцев.</ta>
            <ta e="T34" id="Seg_7446" s="T22">Тогда я отправилась с отцом за дровами, вместе с отцом на нарте поехала.</ta>
            <ta e="T43" id="Seg_7447" s="T34">У меня нарта… на моей нарте сидел мой братик, пятилетний братик.</ta>
            <ta e="T62" id="Seg_7448" s="T43">Ещё я вам не рассказала же, у нас олени же разные, женщина не должна пятнистого оленя это, запрягать.</ta>
            <ta e="T74" id="Seg_7449" s="T62">И потом бывает ещё олень, которым лечат, — потом я расскажу, да? — целебный олень.</ta>
            <ta e="T91" id="Seg_7450" s="T74">Этот целебный олень, э-э, целебный олень, вот мать моего мужа когда болела, то оленю давала свою голову лечить.</ta>
            <ta e="T99" id="Seg_7451" s="T91">Этого оленя женщине запрягать нельзя.</ta>
            <ta e="T106" id="Seg_7452" s="T99">А я не знала этого оленя и запрягла его.</ta>
            <ta e="T111" id="Seg_7453" s="T106">Это белый олень, серо-белый.</ta>
            <ta e="T119" id="Seg_7454" s="T111">И когда мы поехали, этот олень чихнул.</ta>
            <ta e="T132" id="Seg_7455" s="T119">Три раза чихнул, и я говорю: «Ой, беда! Почему он чихает?» — говорю.</ta>
            <ta e="T157" id="Seg_7456" s="T132">А до этого, когда мы отправлялись, мама говорит, что огонь разговаривал, это значит, что что-то плохое случится, когда огонь разговаривает.</ta>
            <ta e="T172" id="Seg_7457" s="T157">Ну тогда я не… я этому не поверила.</ta>
            <ta e="T182" id="Seg_7458" s="T172">Мы поймали оленей и поехали.</ta>
            <ta e="T193" id="Seg_7459" s="T182">Мы поймали их и поехали, мы вместе с отцом поехали.</ta>
            <ta e="T206" id="Seg_7460" s="T193">Когда на стоянку приехали, отец нагрузил сани, нагрузил и привязал и так далее.</ta>
            <ta e="T214" id="Seg_7461" s="T206">Меня отправил, водя мои запряжённые олени. </ta>
            <ta e="T229" id="Seg_7462" s="T214">Когда вывели моего запряжённого оленя, я села мимо саней, на сани тогда нагрузили шесть мешков рыбы.</ta>
            <ta e="T235" id="Seg_7463" s="T229">Шесть мешков рыбы погрузили.</ta>
            <ta e="T243" id="Seg_7464" s="T235">Я прыгнула на сани, я говорю, села мимо саней.</ta>
            <ta e="T251" id="Seg_7465" s="T243">Нет, этот человек (?), я ещё раз прыгнула.</ta>
            <ta e="T255" id="Seg_7466" s="T251">Ещё раз прыгнула.</ta>
            <ta e="T259" id="Seg_7467" s="T255">Снова мимо села.</ta>
            <ta e="T264" id="Seg_7468" s="T259">Своего запряжённого оленя отпустила.</ta>
            <ta e="T273" id="Seg_7469" s="T264">И моя упряжка, запряжённые олени через меня пробежали. </ta>
            <ta e="T284" id="Seg_7470" s="T273">И я сама, с каждой подпоркой в нартах, три раза перевернулась.</ta>
            <ta e="T290" id="Seg_7471" s="T284">Ой, упала на спину.</ta>
            <ta e="T302" id="Seg_7472" s="T290">Упала на спину и лежу, мой отец сокуй надевал.</ta>
            <ta e="T307" id="Seg_7473" s="T302">Надев сокуй, он посмотрел.</ta>
            <ta e="T317" id="Seg_7474" s="T307">Видит, упряжка едет, а тут на дороге человек лежит.</ta>
            <ta e="T328" id="Seg_7475" s="T317">Он прибежал и говорит: «Эй, ты жива?» — говорит.</ta>
            <ta e="T335" id="Seg_7476" s="T328">«Ага, жива я, жива, только нога не двигается».</ta>
            <ta e="T340" id="Seg_7477" s="T335">«Ладно, погоди, иду!»</ta>
            <ta e="T356" id="Seg_7478" s="T340">Ну, он пришёл, кое-как на свои сани меня усадил, мою упряжку догонять отправились.</ta>
            <ta e="T361" id="Seg_7479" s="T356">Там на берег озера взобрались.</ta>
            <ta e="T382" id="Seg_7480" s="T361">На склоне был как раз мой братик, бедняга с саней упал (/спрыгнул?) и повод поймал, и так как раз оленей остановил.</ta>
            <ta e="T389" id="Seg_7481" s="T382">И в это время мы подъехали.</ta>
            <ta e="T395" id="Seg_7482" s="T389">Отец мою упряжку привязал.</ta>
            <ta e="T409" id="Seg_7483" s="T395">Сделал это, сам в мою упряжку сел, и мы поехали домой.</ta>
            <ta e="T417" id="Seg_7484" s="T409">Приехав домой, у моей двери остановился.</ta>
            <ta e="T427" id="Seg_7485" s="T417">У двери балка старухи, две старухи были.</ta>
            <ta e="T438" id="Seg_7486" s="T427">Две бабки эти пришли: «Эй, что у вас, что случилось?»</ta>
            <ta e="T449" id="Seg_7487" s="T438">«Ой, эта девушка сейчас пострадала, к ней надо доктора вызвать».</ta>
            <ta e="T455" id="Seg_7488" s="T449">Тут отец и мать говорят: «Не ходи, не ходи».</ta>
            <ta e="T467" id="Seg_7489" s="T455">«Зачем доктора?» говорит он(а), «Слава Богу, всё, вроде бы, хорошо, подожди».</ta>
            <ta e="T477" id="Seg_7490" s="T467">Потом меня с двух сторон подняли и обратно в балок внесли.</ta>
            <ta e="T484" id="Seg_7491" s="T477">Потом раздели меня и так далее.</ta>
            <ta e="T489" id="Seg_7492" s="T484">«Зачем доктора, подожди, всё в порядке.</ta>
            <ta e="T508" id="Seg_7493" s="T489">Может и просто так, кажется, ничего не сломано, крови нет, ничего нет, всё будет хорошо, да».</ta>
            <ta e="T544" id="Seg_7494" s="T508">Вот потом прошёл месяц, когда прошёл месяц, доктора не было, ничего не было, прошёл месяц, и на Новый год мы поехали [в посёлок] и пошли к доктору. </ta>
            <ta e="T547" id="Seg_7495" s="T544">Мы пришли.</ta>
            <ta e="T561" id="Seg_7496" s="T547">А доктор говорит: «О, у тебя бедро сломано, надо тебя отправить в Хатангу».</ta>
            <ta e="T571" id="Seg_7497" s="T561">Вот, потом послали в Хатангу, в Хатанге я вот там лежала.</ta>
            <ta e="T576" id="Seg_7498" s="T571">Два месяца на вытяжке пролежала.</ta>
            <ta e="T588" id="Seg_7499" s="T576">А потом ещё [несколько] месяцев, смотри, восемь месяцев целых я пролежала в Хатанге.</ta>
            <ta e="T590" id="Seg_7500" s="T588">Это в июне, в июне домой приехала, вернулась домой.</ta>
            <ta e="T602" id="Seg_7501" s="T590">И нога у меня срослась как переломанная была. </ta>
            <ta e="T606" id="Seg_7502" s="T602">Вот такая я приехала.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr" />
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T607" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T608" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T609" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T610" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T611" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T614" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T612" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T613" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
