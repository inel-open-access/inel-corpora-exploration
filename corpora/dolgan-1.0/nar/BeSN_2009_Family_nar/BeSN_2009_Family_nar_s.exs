<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID69030928-50F7-706D-CDAF-14F4CC81380A">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>BeSN_2009_Family_nar</transcription-name>
         <referenced-file url="BeSN_2009_Family_nar.wav" />
         <referenced-file url="BeSN_2009_Family_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\BeSN_2009_Family_nar\BeSN_2009_Family_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">881</ud-information>
            <ud-information attribute-name="# HIAT:w">759</ud-information>
            <ud-information attribute-name="# e">759</ud-information>
            <ud-information attribute-name="# HIAT:u">80</ud-information>
            <ud-information attribute-name="# sc">152</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="unknown">
            <abbreviation>BeSN</abbreviation>
            <sex value="f" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="29.77" type="appl" />
         <tli id="T2" time="30.686666666666667" type="appl" />
         <tli id="T3" time="31.603333333333335" type="appl" />
         <tli id="T4" time="32.52" type="appl" />
         <tli id="T5" time="33.43666666666667" type="appl" />
         <tli id="T6" time="34.35333333333334" type="appl" />
         <tli id="T7" time="35.27" type="appl" />
         <tli id="T8" time="35.79333333333334" />
         <tli id="T9" time="36.58" type="appl" />
         <tli id="T10" time="37.49" type="appl" />
         <tli id="T11" time="38.400000000000006" type="appl" />
         <tli id="T12" time="39.31" type="appl" />
         <tli id="T13" time="40.63" type="appl" />
         <tli id="T14" time="41.70777777777778" type="appl" />
         <tli id="T15" time="42.785555555555554" type="appl" />
         <tli id="T16" time="43.86333333333334" type="appl" />
         <tli id="T17" time="44.94111111111111" type="appl" />
         <tli id="T18" time="46.01888888888889" type="appl" />
         <tli id="T19" time="47.096666666666664" type="appl" />
         <tli id="T20" time="48.17444444444445" type="appl" />
         <tli id="T21" time="49.25222222222222" type="appl" />
         <tli id="T22" time="50.19666666666667" />
         <tli id="T23" time="50.203333333333326" />
         <tli id="T24" time="50.965" type="appl" />
         <tli id="T25" time="51.58" type="appl" />
         <tli id="T26" time="52.195" type="appl" />
         <tli id="T27" time="52.81" type="appl" />
         <tli id="T28" time="53.425" type="appl" />
         <tli id="T29" time="54.04" type="appl" />
         <tli id="T30" time="54.655" type="appl" />
         <tli id="T31" time="55.27" type="appl" />
         <tli id="T32" time="55.885" type="appl" />
         <tli id="T33" time="56.5" type="appl" />
         <tli id="T34" time="56.95333333333333" />
         <tli id="T35" time="57.589999999999996" type="appl" />
         <tli id="T36" time="58.39" type="appl" />
         <tli id="T37" time="59.19" type="appl" />
         <tli id="T38" time="59.99" type="appl" />
         <tli id="T39" time="60.79" type="appl" />
         <tli id="T40" time="61.59" type="appl" />
         <tli id="T41" time="62.223333333333336" />
         <tli id="T42" time="62.373333333333335" />
         <tli id="T43" time="63.06" type="appl" />
         <tli id="T44" time="63.7" type="appl" />
         <tli id="T45" time="64.34" type="appl" />
         <tli id="T46" time="64.98" type="appl" />
         <tli id="T47" time="65.62" type="appl" />
         <tli id="T48" time="66.08" />
         <tli id="T49" time="66.34666666666668" />
         <tli id="T50" time="67.2525" type="appl" />
         <tli id="T51" time="67.925" type="appl" />
         <tli id="T52" time="68.5975" type="appl" />
         <tli id="T53" time="69.27" type="appl" />
         <tli id="T54" time="69.27333333333334" />
         <tli id="T55" time="70.12166666666667" type="appl" />
         <tli id="T56" time="70.76333333333334" type="appl" />
         <tli id="T57" time="71.405" type="appl" />
         <tli id="T58" time="72.04666666666667" type="appl" />
         <tli id="T59" time="72.68833333333333" type="appl" />
         <tli id="T60" time="73.64666666666666" />
         <tli id="T61" time="74.19" type="appl" />
         <tli id="T62" time="74.75571428571428" type="appl" />
         <tli id="T63" time="75.32142857142857" type="appl" />
         <tli id="T64" time="75.88714285714286" type="appl" />
         <tli id="T65" time="76.45285714285714" type="appl" />
         <tli id="T66" time="77.01857142857142" type="appl" />
         <tli id="T67" time="77.58428571428571" type="appl" />
         <tli id="T68" time="78.15" type="appl" />
         <tli id="T69" time="78.71571428571428" type="appl" />
         <tli id="T70" time="79.28142857142856" type="appl" />
         <tli id="T71" time="79.84714285714286" type="appl" />
         <tli id="T72" time="80.41285714285715" type="appl" />
         <tli id="T73" time="80.97857142857143" type="appl" />
         <tli id="T74" time="81.5442857142857" type="appl" />
         <tli id="T75" time="82.11" type="appl" />
         <tli id="T76" time="83.0" />
         <tli id="T77" time="83.58990909090909" type="appl" />
         <tli id="T78" time="84.25981818181819" type="appl" />
         <tli id="T79" time="84.92972727272728" type="appl" />
         <tli id="T80" time="85.59963636363636" type="appl" />
         <tli id="T81" time="86.26954545454545" type="appl" />
         <tli id="T82" time="86.93945454545455" type="appl" />
         <tli id="T83" time="87.60936363636364" type="appl" />
         <tli id="T84" time="88.27927272727273" type="appl" />
         <tli id="T85" time="88.94918181818181" type="appl" />
         <tli id="T86" time="89.61909090909091" type="appl" />
         <tli id="T87" time="90.289" type="appl" />
         <tli id="T88" time="91.3" type="appl" />
         <tli id="T89" time="92.02071428571428" type="appl" />
         <tli id="T90" time="92.74142857142857" type="appl" />
         <tli id="T91" time="93.46214285714285" type="appl" />
         <tli id="T92" time="94.18285714285715" type="appl" />
         <tli id="T93" time="94.90357142857142" type="appl" />
         <tli id="T94" time="95.62428571428572" type="appl" />
         <tli id="T95" time="96.345" type="appl" />
         <tli id="T96" time="97.06571428571428" type="appl" />
         <tli id="T97" time="97.78642857142857" type="appl" />
         <tli id="T98" time="98.50714285714285" type="appl" />
         <tli id="T99" time="99.22785714285715" type="appl" />
         <tli id="T100" time="99.94857142857143" type="appl" />
         <tli id="T101" time="100.66928571428572" type="appl" />
         <tli id="T102" time="101.51" />
         <tli id="T103" time="102.82666666666667" />
         <tli id="T104" time="103.54052631578948" type="appl" />
         <tli id="T105" time="104.16105263157895" type="appl" />
         <tli id="T106" time="104.78157894736842" type="appl" />
         <tli id="T107" time="105.40210526315789" type="appl" />
         <tli id="T108" time="106.02263157894737" type="appl" />
         <tli id="T109" time="106.64315789473685" type="appl" />
         <tli id="T110" time="107.26368421052632" type="appl" />
         <tli id="T111" time="107.88421052631578" type="appl" />
         <tli id="T112" time="108.50473684210526" type="appl" />
         <tli id="T113" time="109.12526315789474" type="appl" />
         <tli id="T114" time="109.74578947368421" type="appl" />
         <tli id="T115" time="110.36631578947367" type="appl" />
         <tli id="T116" time="110.98684210526315" type="appl" />
         <tli id="T117" time="111.60736842105263" type="appl" />
         <tli id="T118" time="112.2278947368421" type="appl" />
         <tli id="T119" time="112.84842105263158" type="appl" />
         <tli id="T120" time="113.46894736842104" type="appl" />
         <tli id="T121" time="114.08947368421052" type="appl" />
         <tli id="T122" time="114.83" />
         <tli id="T123" time="115.83333333333334" />
         <tli id="T124" time="116.5125" type="appl" />
         <tli id="T125" time="117.185" type="appl" />
         <tli id="T126" time="117.8575" type="appl" />
         <tli id="T127" time="118.53" type="appl" />
         <tli id="T128" time="119.2025" type="appl" />
         <tli id="T129" time="119.875" type="appl" />
         <tli id="T130" time="120.5475" type="appl" />
         <tli id="T131" time="121.22" type="appl" />
         <tli id="T132" time="124.30666666666667" />
         <tli id="T133" time="124.6525" type="appl" />
         <tli id="T134" time="125.215" type="appl" />
         <tli id="T135" time="125.7775" type="appl" />
         <tli id="T136" time="126.34" type="appl" />
         <tli id="T137" time="126.9025" type="appl" />
         <tli id="T138" time="127.465" type="appl" />
         <tli id="T139" time="128.0275" type="appl" />
         <tli id="T140" time="128.34333333333333" />
         <tli id="T141" time="128.69" type="appl" />
         <tli id="T142" time="129.36833333333334" type="appl" />
         <tli id="T143" time="130.04666666666665" type="appl" />
         <tli id="T144" time="130.725" type="appl" />
         <tli id="T145" time="131.40333333333334" type="appl" />
         <tli id="T146" time="132.08166666666665" type="appl" />
         <tli id="T147" time="132.62666666666667" />
         <tli id="T148" time="134.14666666666665" />
         <tli id="T149" time="134.15958333333333" type="intp" />
         <tli id="T150" time="134.17249999999999" type="appl" />
         <tli id="T151" time="134.70875" type="appl" />
         <tli id="T152" time="135.245" type="appl" />
         <tli id="T153" time="135.78125" type="appl" />
         <tli id="T154" time="136.3175" type="appl" />
         <tli id="T155" time="136.85375" type="appl" />
         <tli id="T156" time="137.39" type="appl" />
         <tli id="T157" time="137.92625" type="appl" />
         <tli id="T158" time="138.4625" type="appl" />
         <tli id="T159" time="138.99875" type="appl" />
         <tli id="T160" time="139.535" type="appl" />
         <tli id="T161" time="140.07125" type="appl" />
         <tli id="T162" time="140.60750000000002" type="appl" />
         <tli id="T163" time="141.14375" type="appl" />
         <tli id="T164" time="141.78666666666666" />
         <tli id="T165" time="141.79" />
         <tli id="T166" time="142.50333333333333" type="appl" />
         <tli id="T167" time="143.15666666666667" type="appl" />
         <tli id="T168" time="143.81" type="appl" />
         <tli id="T169" time="144.46333333333334" type="appl" />
         <tli id="T170" time="145.11666666666667" type="appl" />
         <tli id="T171" time="145.77" type="appl" />
         <tli id="T172" time="146.32666666666665" />
         <tli id="T173" time="146.90800000000002" type="appl" />
         <tli id="T174" time="147.45600000000002" type="appl" />
         <tli id="T175" time="148.00400000000002" type="appl" />
         <tli id="T176" time="148.55200000000002" type="appl" />
         <tli id="T177" time="149.10000000000002" type="appl" />
         <tli id="T178" time="149.648" type="appl" />
         <tli id="T179" time="150.196" type="appl" />
         <tli id="T180" time="150.744" type="appl" />
         <tli id="T181" time="151.292" type="appl" />
         <tli id="T182" time="151.67333333333335" />
         <tli id="T183" time="152.20333333333335" />
         <tli id="T184" time="152.75647058823532" type="appl" />
         <tli id="T185" time="153.4029411764706" type="appl" />
         <tli id="T186" time="154.0494117647059" type="appl" />
         <tli id="T187" time="154.6958823529412" type="appl" />
         <tli id="T188" time="155.34235294117647" type="appl" />
         <tli id="T189" time="155.98882352941177" type="appl" />
         <tli id="T190" time="156.63529411764708" type="appl" />
         <tli id="T191" time="157.28176470588235" type="appl" />
         <tli id="T192" time="157.92823529411766" type="appl" />
         <tli id="T193" time="158.57470588235293" type="appl" />
         <tli id="T194" time="159.22117647058823" type="appl" />
         <tli id="T195" time="159.86764705882354" type="appl" />
         <tli id="T196" time="160.5141176470588" type="appl" />
         <tli id="T197" time="161.1605882352941" type="appl" />
         <tli id="T198" time="161.80705882352942" type="appl" />
         <tli id="T199" time="162.4535294117647" type="appl" />
         <tli id="T200" time="163.22" />
         <tli id="T201" time="165.82666666666665" />
         <tli id="T202" time="166.1646153846154" type="appl" />
         <tli id="T203" time="166.87923076923076" type="appl" />
         <tli id="T204" time="167.59384615384616" type="appl" />
         <tli id="T205" time="168.30846153846153" type="appl" />
         <tli id="T206" time="169.02307692307693" type="appl" />
         <tli id="T207" time="169.7376923076923" type="appl" />
         <tli id="T208" time="170.4523076923077" type="appl" />
         <tli id="T209" time="171.16692307692307" type="appl" />
         <tli id="T210" time="171.88153846153847" type="appl" />
         <tli id="T211" time="172.59615384615384" type="appl" />
         <tli id="T212" time="173.31076923076924" type="appl" />
         <tli id="T213" time="174.0253846153846" type="appl" />
         <tli id="T214" time="174.64666666666668" />
         <tli id="T215" time="175.07333333333335" />
         <tli id="T216" time="175.8525" type="appl" />
         <tli id="T217" time="176.665" type="appl" />
         <tli id="T218" time="177.4775" type="appl" />
         <tli id="T219" time="178.29" type="appl" />
         <tli id="T220" time="179.1025" type="appl" />
         <tli id="T221" time="179.915" type="appl" />
         <tli id="T222" time="180.7275" type="appl" />
         <tli id="T223" time="181.54" type="appl" />
         <tli id="T224" time="182.72" type="appl" />
         <tli id="T225" time="183.322" type="appl" />
         <tli id="T226" time="183.924" type="appl" />
         <tli id="T227" time="184.526" type="appl" />
         <tli id="T228" time="185.12800000000001" type="appl" />
         <tli id="T229" time="185.73000000000002" type="appl" />
         <tli id="T230" time="186.332" type="appl" />
         <tli id="T231" time="186.934" type="appl" />
         <tli id="T232" time="187.536" type="appl" />
         <tli id="T233" time="188.138" type="appl" />
         <tli id="T234" time="188.74" type="appl" />
         <tli id="T235" time="188.77" type="appl" />
         <tli id="T236" time="189.32384615384618" type="appl" />
         <tli id="T237" time="189.8776923076923" type="appl" />
         <tli id="T238" time="190.43153846153848" type="appl" />
         <tli id="T239" time="190.98538461538462" type="appl" />
         <tli id="T240" time="191.53923076923078" type="appl" />
         <tli id="T241" time="192.09307692307692" type="appl" />
         <tli id="T242" time="192.6469230769231" type="appl" />
         <tli id="T243" time="193.20076923076923" type="appl" />
         <tli id="T244" time="193.7546153846154" type="appl" />
         <tli id="T245" time="194.30846153846153" type="appl" />
         <tli id="T246" time="194.8623076923077" type="appl" />
         <tli id="T247" time="195.41615384615383" type="appl" />
         <tli id="T248" time="195.62" />
         <tli id="T249" time="197.61" type="appl" />
         <tli id="T250" time="198.21666666666667" type="appl" />
         <tli id="T251" time="198.82333333333335" type="appl" />
         <tli id="T252" time="199.43" type="appl" />
         <tli id="T253" time="200.03666666666666" type="appl" />
         <tli id="T254" time="200.64333333333335" type="appl" />
         <tli id="T255" time="201.25" type="appl" />
         <tli id="T256" time="201.85666666666665" type="appl" />
         <tli id="T257" time="202.46333333333334" type="appl" />
         <tli id="T258" time="203.07" type="appl" />
         <tli id="T259" time="203.67666666666665" type="appl" />
         <tli id="T260" time="204.28333333333333" type="appl" />
         <tli id="T261" time="204.33333333333334" />
         <tli id="T262" time="207.25333333333333" />
         <tli id="T263" time="208.2975" type="appl" />
         <tli id="T264" time="209.195" type="appl" />
         <tli id="T265" time="210.0925" type="appl" />
         <tli id="T266" time="210.99" type="appl" />
         <tli id="T267" time="211.8875" type="appl" />
         <tli id="T268" time="212.785" type="appl" />
         <tli id="T269" time="213.6825" type="appl" />
         <tli id="T270" time="214.57999999999998" type="appl" />
         <tli id="T271" time="215.4775" type="appl" />
         <tli id="T272" time="216.375" type="appl" />
         <tli id="T273" time="217.27249999999998" type="appl" />
         <tli id="T274" time="218.17" type="appl" />
         <tli id="T275" time="219.16" type="appl" />
         <tli id="T276" time="220.212" type="appl" />
         <tli id="T277" time="221.26399999999998" type="appl" />
         <tli id="T278" time="222.316" type="appl" />
         <tli id="T279" time="223.368" type="appl" />
         <tli id="T280" time="224.45333333333335" />
         <tli id="T281" time="224.79" />
         <tli id="T282" time="225.57625000000002" type="appl" />
         <tli id="T283" time="226.4025" type="appl" />
         <tli id="T284" time="227.22875" type="appl" />
         <tli id="T285" time="228.055" type="appl" />
         <tli id="T286" time="228.88125000000002" type="appl" />
         <tli id="T287" time="229.7075" type="appl" />
         <tli id="T288" time="230.53375" type="appl" />
         <tli id="T289" time="231.57333333333335" />
         <tli id="T290" time="232.25333333333333" />
         <tli id="T291" time="233.08" type="appl" />
         <tli id="T292" time="234.51" type="appl" />
         <tli id="T293" time="235.94" type="appl" />
         <tli id="T294" time="237.37" type="appl" />
         <tli id="T295" time="238.79999999999998" type="appl" />
         <tli id="T296" time="239.92" />
         <tli id="T297" time="241.9066666666667" />
         <tli id="T298" time="242.39444444444445" type="appl" />
         <tli id="T299" time="242.9488888888889" type="appl" />
         <tli id="T300" time="243.50333333333333" type="appl" />
         <tli id="T301" time="244.05777777777777" type="appl" />
         <tli id="T302" time="244.61222222222224" type="appl" />
         <tli id="T303" time="245.16666666666669" type="appl" />
         <tli id="T304" time="245.72111111111113" type="appl" />
         <tli id="T305" time="246.27555555555557" type="appl" />
         <tli id="T306" time="246.83" type="appl" />
         <tli id="T307" time="247.26" type="appl" />
         <tli id="T308" time="248.142" type="appl" />
         <tli id="T309" time="249.024" type="appl" />
         <tli id="T310" time="249.90599999999998" type="appl" />
         <tli id="T311" time="250.78799999999998" type="appl" />
         <tli id="T312" time="251.67" type="appl" />
         <tli id="T313" time="252.57" type="appl" />
         <tli id="T314" time="253.52375" type="appl" />
         <tli id="T315" time="254.4775" type="appl" />
         <tli id="T316" time="255.43124999999998" type="appl" />
         <tli id="T317" time="256.385" type="appl" />
         <tli id="T318" time="257.33875" type="appl" />
         <tli id="T319" time="258.2925" type="appl" />
         <tli id="T320" time="259.24625" type="appl" />
         <tli id="T321" time="260.16" />
         <tli id="T322" time="260.1656770833333" />
         <tli id="T323" time="260.9403125" type="appl" />
         <tli id="T324" time="261.601625" type="appl" />
         <tli id="T325" time="262.2629375" type="appl" />
         <tli id="T326" time="262.92425000000003" type="appl" />
         <tli id="T327" time="263.5855625" type="appl" />
         <tli id="T328" time="264.246875" type="appl" />
         <tli id="T329" time="264.9081875" type="appl" />
         <tli id="T330" time="265.5695" type="appl" />
         <tli id="T331" time="266.2308125" type="appl" />
         <tli id="T332" time="266.892125" type="appl" />
         <tli id="T333" time="267.55343750000003" type="appl" />
         <tli id="T334" time="268.21475" type="appl" />
         <tli id="T335" time="268.8760625" type="appl" />
         <tli id="T336" time="269.537375" type="appl" />
         <tli id="T337" time="270.1986875" type="appl" />
         <tli id="T338" time="270.81333333333333" />
         <tli id="T339" time="272.30333333333334" />
         <tli id="T340" time="273.47" type="appl" />
         <tli id="T341" time="274.47" type="appl" />
         <tli id="T342" time="275.47" type="appl" />
         <tli id="T343" time="276.47" type="appl" />
         <tli id="T344" time="277.47" type="appl" />
         <tli id="T345" time="278.47" type="appl" />
         <tli id="T346" time="279.47" type="appl" />
         <tli id="T347" time="279.4733333333333" />
         <tli id="T348" time="280.9766666666667" />
         <tli id="T349" time="281.68416666666667" type="appl" />
         <tli id="T350" time="282.2783333333333" type="appl" />
         <tli id="T351" time="282.8725" type="appl" />
         <tli id="T352" time="283.46666666666664" type="appl" />
         <tli id="T353" time="284.06083333333333" type="appl" />
         <tli id="T354" time="284.655" type="appl" />
         <tli id="T355" time="285.24916666666667" type="appl" />
         <tli id="T356" time="285.84333333333336" type="appl" />
         <tli id="T357" time="286.4375" type="appl" />
         <tli id="T358" time="287.0316666666667" type="appl" />
         <tli id="T359" time="287.62583333333333" type="appl" />
         <tli id="T360" time="287.7666666666667" />
         <tli id="T361" time="288.25" />
         <tli id="T362" time="288.9271428571429" type="appl" />
         <tli id="T363" time="289.50428571428574" type="appl" />
         <tli id="T364" time="290.0814285714286" type="appl" />
         <tli id="T365" time="290.6585714285714" type="appl" />
         <tli id="T366" time="291.23571428571427" type="appl" />
         <tli id="T367" time="291.8128571428571" type="appl" />
         <tli id="T368" time="292.39" type="appl" />
         <tli id="T369" time="292.74" type="appl" />
         <tli id="T370" time="293.43125" type="appl" />
         <tli id="T371" time="294.1225" type="appl" />
         <tli id="T372" time="294.81375" type="appl" />
         <tli id="T373" time="295.505" type="appl" />
         <tli id="T374" time="296.19624999999996" type="appl" />
         <tli id="T375" time="296.8875" type="appl" />
         <tli id="T376" time="297.57875" type="appl" />
         <tli id="T377" time="298.27" type="appl" />
         <tli id="T378" time="298.2766666666667" />
         <tli id="T379" time="298.8441666666667" type="appl" />
         <tli id="T380" time="299.39833333333337" type="appl" />
         <tli id="T381" time="299.9525" type="appl" />
         <tli id="T382" time="300.50666666666666" type="appl" />
         <tli id="T383" time="301.06083333333333" type="appl" />
         <tli id="T384" time="301.615" type="appl" />
         <tli id="T385" time="302.1691666666667" type="appl" />
         <tli id="T386" time="302.72333333333336" type="appl" />
         <tli id="T387" time="303.27750000000003" type="appl" />
         <tli id="T388" time="303.83166666666665" type="appl" />
         <tli id="T389" time="304.3858333333333" type="appl" />
         <tli id="T390" time="304.6933333333333" />
         <tli id="T391" time="305.54" />
         <tli id="T392" time="306.02666666666664" type="appl" />
         <tli id="T393" time="306.7133333333333" type="appl" />
         <tli id="T394" time="307.4" type="appl" />
         <tli id="T395" time="308.08666666666664" type="appl" />
         <tli id="T396" time="308.7733333333333" type="appl" />
         <tli id="T397" time="309.46" type="appl" />
         <tli id="T398" time="310.14666666666665" type="appl" />
         <tli id="T399" time="310.8333333333333" type="appl" />
         <tli id="T400" time="311.52" type="appl" />
         <tli id="T401" time="312.20666666666665" type="appl" />
         <tli id="T402" time="312.8933333333333" type="appl" />
         <tli id="T403" time="313.65333333333336" />
         <tli id="T404" time="313.66" type="appl" />
         <tli id="T405" time="314.315" type="appl" />
         <tli id="T406" time="314.97" type="appl" />
         <tli id="T407" time="315.625" type="appl" />
         <tli id="T408" time="316.28000000000003" type="appl" />
         <tli id="T409" time="316.935" type="appl" />
         <tli id="T410" time="317.59" type="appl" />
         <tli id="T411" time="318.245" type="appl" />
         <tli id="T412" time="318.9" type="appl" />
         <tli id="T413" time="319.555" type="appl" />
         <tli id="T414" time="320.13" />
         <tli id="T415" time="320.5299999999999" />
         <tli id="T416" time="321.25857142857143" type="appl" />
         <tli id="T417" time="321.9871428571428" type="appl" />
         <tli id="T418" time="322.7157142857143" type="appl" />
         <tli id="T419" time="323.4442857142857" type="appl" />
         <tli id="T420" time="324.17285714285714" type="appl" />
         <tli id="T421" time="324.90142857142854" type="appl" />
         <tli id="T422" time="325.49" />
         <tli id="T423" time="325.72" type="appl" />
         <tli id="T424" time="326.25800000000004" type="appl" />
         <tli id="T425" time="326.79600000000005" type="appl" />
         <tli id="T426" time="327.334" type="appl" />
         <tli id="T427" time="327.872" type="appl" />
         <tli id="T428" time="328.4366666666666" />
         <tli id="T429" time="328.44" type="appl" />
         <tli id="T430" time="328.90666666666664" type="appl" />
         <tli id="T431" time="329.37333333333333" type="appl" />
         <tli id="T432" time="329.84" type="appl" />
         <tli id="T433" time="330.3066666666667" type="appl" />
         <tli id="T434" time="330.7733333333333" type="appl" />
         <tli id="T435" time="331.24" type="appl" />
         <tli id="T436" time="331.70666666666665" type="appl" />
         <tli id="T437" time="332.17333333333335" type="appl" />
         <tli id="T438" time="332.2666666666667" />
         <tli id="T439" time="332.91333333333336" />
         <tli id="T440" time="333.2876470588235" type="appl" />
         <tli id="T441" time="333.735294117647" type="appl" />
         <tli id="T442" time="334.1829411764706" type="appl" />
         <tli id="T443" time="334.6305882352941" type="appl" />
         <tli id="T444" time="335.07823529411763" type="appl" />
         <tli id="T445" time="335.52588235294115" type="appl" />
         <tli id="T446" time="335.9735294117647" type="appl" />
         <tli id="T447" time="336.4211764705882" type="appl" />
         <tli id="T448" time="336.86882352941177" type="appl" />
         <tli id="T449" time="337.3164705882353" type="appl" />
         <tli id="T450" time="337.7641176470588" type="appl" />
         <tli id="T451" time="338.21176470588233" type="appl" />
         <tli id="T452" time="338.65941176470585" type="appl" />
         <tli id="T453" time="339.10705882352937" type="appl" />
         <tli id="T454" time="339.55470588235295" type="appl" />
         <tli id="T455" time="340.00235294117647" type="appl" />
         <tli id="T456" time="340.69" />
         <tli id="T457" time="341.08" />
         <tli id="T458" time="341.6511111111111" type="appl" />
         <tli id="T459" time="342.1622222222222" type="appl" />
         <tli id="T460" time="342.67333333333335" type="appl" />
         <tli id="T461" time="343.18444444444447" type="appl" />
         <tli id="T462" time="343.69555555555553" type="appl" />
         <tli id="T463" time="344.20666666666665" type="appl" />
         <tli id="T464" time="344.71777777777777" type="appl" />
         <tli id="T465" time="345.2288888888889" type="appl" />
         <tli id="T466" time="345.38" />
         <tli id="T467" time="346.7066666666667" />
         <tli id="T468" time="347.44" type="appl" />
         <tli id="T469" time="348.0" type="appl" />
         <tli id="T470" time="348.56" type="appl" />
         <tli id="T471" time="349.12" type="appl" />
         <tli id="T472" time="349.68" type="appl" />
         <tli id="T473" time="350.24" type="appl" />
         <tli id="T474" time="350.8" type="appl" />
         <tli id="T475" time="351.36" type="appl" />
         <tli id="T476" time="351.92" type="appl" />
         <tli id="T477" time="352.48" type="appl" />
         <tli id="T478" time="353.04" type="appl" />
         <tli id="T479" time="353.6" type="appl" />
         <tli id="T480" time="354.16" type="appl" />
         <tli id="T481" time="354.16666666666663" />
         <tli id="T482" time="355.16333333333336" />
         <tli id="T483" time="357.18" type="appl" />
         <tli id="T484" time="359.17" type="appl" />
         <tli id="T485" time="362.38" type="appl" />
         <tli id="T486" time="363.0094117647059" type="appl" />
         <tli id="T487" time="363.63882352941175" type="appl" />
         <tli id="T488" time="364.26823529411763" type="appl" />
         <tli id="T489" time="364.8976470588235" type="appl" />
         <tli id="T490" time="365.5270588235294" type="appl" />
         <tli id="T491" time="366.15647058823527" type="appl" />
         <tli id="T492" time="366.78588235294114" type="appl" />
         <tli id="T493" time="367.415294117647" type="appl" />
         <tli id="T494" time="368.04470588235296" type="appl" />
         <tli id="T495" time="368.67411764705884" type="appl" />
         <tli id="T496" time="369.3035294117647" type="appl" />
         <tli id="T497" time="369.9329411764706" type="appl" />
         <tli id="T498" time="370.56235294117647" type="appl" />
         <tli id="T499" time="371.19176470588235" type="appl" />
         <tli id="T500" time="371.8211764705882" type="appl" />
         <tli id="T501" time="372.4505882352941" type="appl" />
         <tli id="T502" time="373.08" type="appl" />
         <tli id="T503" time="376.75666666666666" />
         <tli id="T504" time="377.33733333333333" type="appl" />
         <tli id="T505" time="377.9046666666666" type="appl" />
         <tli id="T506" time="378.472" type="appl" />
         <tli id="T507" time="379.03933333333333" type="appl" />
         <tli id="T508" time="379.6066666666666" type="appl" />
         <tli id="T509" time="380.174" type="appl" />
         <tli id="T510" time="380.74133333333333" type="appl" />
         <tli id="T511" time="381.3086666666666" type="appl" />
         <tli id="T512" time="381.876" type="appl" />
         <tli id="T513" time="382.4433333333333" type="appl" />
         <tli id="T514" time="383.0106666666666" type="appl" />
         <tli id="T515" time="383.578" type="appl" />
         <tli id="T516" time="384.1453333333333" type="appl" />
         <tli id="T517" time="384.7126666666666" type="appl" />
         <tli id="T518" time="385.41333333333336" />
         <tli id="T519" time="385.42" type="appl" />
         <tli id="T520" time="386.15444444444444" type="appl" />
         <tli id="T521" time="386.8888888888889" type="appl" />
         <tli id="T522" time="387.62333333333333" type="appl" />
         <tli id="T523" time="388.35777777777776" type="appl" />
         <tli id="T524" time="389.09222222222223" type="appl" />
         <tli id="T525" time="389.82666666666665" type="appl" />
         <tli id="T526" time="390.5611111111111" type="appl" />
         <tli id="T527" time="391.29555555555555" type="appl" />
         <tli id="T528" time="391.87" />
         <tli id="T529" time="392.5466666666667" />
         <tli id="T530" time="392.98333333333335" type="appl" />
         <tli id="T531" time="393.6666666666667" type="appl" />
         <tli id="T532" time="394.35" type="appl" />
         <tli id="T533" time="395.03333333333336" type="appl" />
         <tli id="T534" time="395.7166666666667" type="appl" />
         <tli id="T535" time="396.4" type="appl" />
         <tli id="T536" time="397.0833333333333" type="appl" />
         <tli id="T537" time="397.76666666666665" type="appl" />
         <tli id="T538" time="398.45" type="appl" />
         <tli id="T539" time="399.1333333333333" type="appl" />
         <tli id="T540" time="399.81666666666666" type="appl" />
         <tli id="T541" time="400.5" type="appl" />
         <tli id="T542" time="403.4966666666666" />
         <tli id="T543" time="403.8666666666667" />
         <tli id="T544" time="404.64714285714285" type="appl" />
         <tli id="T545" time="405.4742857142857" type="appl" />
         <tli id="T546" time="406.3014285714286" type="appl" />
         <tli id="T547" time="407.12857142857143" type="appl" />
         <tli id="T548" time="407.9557142857143" type="appl" />
         <tli id="T549" time="408.78285714285715" type="appl" />
         <tli id="T550" time="409.61" type="appl" />
         <tli id="T551" time="409.9266666666666" />
         <tli id="T552" time="410.34749999999997" type="appl" />
         <tli id="T553" time="410.91499999999996" type="appl" />
         <tli id="T554" time="411.4825" type="appl" />
         <tli id="T555" time="411.79" />
         <tli id="T556" time="413.3566666666667" />
         <tli id="T557" time="414.0675" type="appl" />
         <tli id="T558" time="414.685" type="appl" />
         <tli id="T559" time="415.3025" type="appl" />
         <tli id="T560" time="415.91999999999996" type="appl" />
         <tli id="T561" time="416.53749999999997" type="appl" />
         <tli id="T562" time="417.155" type="appl" />
         <tli id="T563" time="417.7725" type="appl" />
         <tli id="T564" time="418.45" />
         <tli id="T565" time="418.54" />
         <tli id="T566" time="419.084" type="appl" />
         <tli id="T567" time="419.688" type="appl" />
         <tli id="T568" time="420.29200000000003" type="appl" />
         <tli id="T569" time="420.896" type="appl" />
         <tli id="T570" time="421.4533333333334" />
         <tli id="T571" time="422.28666666666663" />
         <tli id="T572" time="422.45" type="appl" />
         <tli id="T573" time="423.21" type="appl" />
         <tli id="T574" time="423.97" type="appl" />
         <tli id="T575" time="424.73" type="appl" />
         <tli id="T576" time="425.49" type="appl" />
         <tli id="T577" time="426.25" type="appl" />
         <tli id="T578" time="427.01" type="appl" />
         <tli id="T579" time="427.77" type="appl" />
         <tli id="T580" time="428.53" type="appl" />
         <tli id="T581" time="429.29" type="appl" />
         <tli id="T582" time="430.05" type="appl" />
         <tli id="T583" time="430.81" type="appl" />
         <tli id="T584" time="431.6479166666667" type="appl" />
         <tli id="T585" time="432.48583333333335" type="appl" />
         <tli id="T586" time="433.32375" type="appl" />
         <tli id="T587" time="434.1616666666667" type="appl" />
         <tli id="T588" time="434.99958333333336" type="appl" />
         <tli id="T589" time="435.8375" type="appl" />
         <tli id="T590" time="436.67541666666665" type="appl" />
         <tli id="T591" time="437.5133333333333" type="appl" />
         <tli id="T592" time="438.35125" type="appl" />
         <tli id="T593" time="439.18916666666667" type="appl" />
         <tli id="T594" time="440.02708333333334" type="appl" />
         <tli id="T595" time="440.865" type="appl" />
         <tli id="T596" time="441.7029166666667" type="appl" />
         <tli id="T597" time="442.54083333333335" type="appl" />
         <tli id="T598" time="443.37875" type="appl" />
         <tli id="T599" time="444.2166666666667" type="appl" />
         <tli id="T600" time="445.05458333333337" type="appl" />
         <tli id="T601" time="445.89250000000004" type="appl" />
         <tli id="T602" time="446.73041666666666" type="appl" />
         <tli id="T603" time="447.5683333333333" type="appl" />
         <tli id="T604" time="448.40625" type="appl" />
         <tli id="T605" time="449.2441666666667" type="appl" />
         <tli id="T606" time="450.08208333333334" type="appl" />
         <tli id="T607" time="450.92" type="appl" />
         <tli id="T608" time="454.54" type="appl" />
         <tli id="T609" time="455.0025" type="appl" />
         <tli id="T610" time="455.46500000000003" type="appl" />
         <tli id="T611" time="455.9275" type="appl" />
         <tli id="T612" time="456.2366666666666" />
         <tli id="T613" time="456.3233333333333" />
         <tli id="T614" time="457.41499999999996" type="appl" />
         <tli id="T615" time="458.38" type="appl" />
         <tli id="T616" time="459.345" type="appl" />
         <tli id="T617" time="460.31" type="appl" />
         <tli id="T618" time="461.275" type="appl" />
         <tli id="T619" time="462.24" type="appl" />
         <tli id="T620" time="463.51" type="appl" />
         <tli id="T621" time="464.08375" type="appl" />
         <tli id="T622" time="464.6575" type="appl" />
         <tli id="T623" time="465.23125" type="appl" />
         <tli id="T624" time="465.805" type="appl" />
         <tli id="T625" time="466.37875" type="appl" />
         <tli id="T626" time="466.9525" type="appl" />
         <tli id="T627" time="467.52625" type="appl" />
         <tli id="T628" time="468.1" type="appl" />
         <tli id="T629" time="468.88" type="appl" />
         <tli id="T630" time="469.75666666666666" type="appl" />
         <tli id="T631" time="470.6333333333333" type="appl" />
         <tli id="T632" time="471.51" type="appl" />
         <tli id="T633" time="472.38666666666666" type="appl" />
         <tli id="T634" time="473.2633333333333" type="appl" />
         <tli id="T635" time="474.14" type="appl" />
         <tli id="T636" time="475.01666666666665" type="appl" />
         <tli id="T637" time="475.8933333333333" type="appl" />
         <tli id="T638" time="476.77" type="appl" />
         <tli id="T639" time="477.64" type="appl" />
         <tli id="T640" time="478.35555555555555" type="appl" />
         <tli id="T641" time="479.0711111111111" type="appl" />
         <tli id="T642" time="479.78666666666663" type="appl" />
         <tli id="T643" time="480.5022222222222" type="appl" />
         <tli id="T644" time="481.21777777777777" type="appl" />
         <tli id="T645" time="481.93333333333334" type="appl" />
         <tli id="T646" time="482.64888888888885" type="appl" />
         <tli id="T647" time="483.3644444444444" type="appl" />
         <tli id="T648" time="484.08" type="appl" />
         <tli id="T649" time="484.32" />
         <tli id="T650" time="485.35076923076923" type="appl" />
         <tli id="T651" time="486.26153846153846" type="appl" />
         <tli id="T652" time="487.1723076923077" type="appl" />
         <tli id="T653" time="488.08307692307693" type="appl" />
         <tli id="T654" time="488.99384615384616" type="appl" />
         <tli id="T655" time="489.9046153846154" type="appl" />
         <tli id="T656" time="490.8153846153846" type="appl" />
         <tli id="T657" time="491.7261538461538" type="appl" />
         <tli id="T658" time="492.63692307692304" type="appl" />
         <tli id="T659" time="493.5476923076923" type="appl" />
         <tli id="T660" time="494.4584615384615" type="appl" />
         <tli id="T661" time="495.36923076923074" type="appl" />
         <tli id="T662" time="495.5466666666666" />
         <tli id="T663" time="497.64666666666665" />
         <tli id="T664" time="498.6571428571429" type="appl" />
         <tli id="T665" time="499.5942857142857" type="appl" />
         <tli id="T666" time="500.5314285714286" type="appl" />
         <tli id="T667" time="501.4685714285714" type="appl" />
         <tli id="T668" time="502.4057142857143" type="appl" />
         <tli id="T669" time="503.3428571428571" type="appl" />
         <tli id="T670" time="504.28" type="appl" />
         <tli id="T671" time="504.3" />
         <tli id="T672" time="505.2157142857143" type="appl" />
         <tli id="T673" time="505.93142857142857" type="appl" />
         <tli id="T674" time="506.64714285714285" type="appl" />
         <tli id="T675" time="507.36285714285714" type="appl" />
         <tli id="T676" time="508.0785714285714" type="appl" />
         <tli id="T677" time="508.7942857142857" type="appl" />
         <tli id="T678" time="509.43" />
         <tli id="T679" time="509.87666666666667" />
         <tli id="T680" time="509.96999999999997" type="appl" />
         <tli id="T681" time="510.37" type="appl" />
         <tli id="T682" time="510.77" type="appl" />
         <tli id="T683" time="511.17" type="appl" />
         <tli id="T684" time="511.57" type="appl" />
         <tli id="T685" time="511.97" type="appl" />
         <tli id="T686" time="512.37" type="appl" />
         <tli id="T687" time="512.77" type="appl" />
         <tli id="T688" time="513.17" type="appl" />
         <tli id="T689" time="513.57" type="appl" />
         <tli id="T690" time="513.97" type="appl" />
         <tli id="T691" time="514.31" />
         <tli id="T692" time="514.66234375" />
         <tli id="T693" time="515.692875" type="appl" />
         <tli id="T694" time="516.71675" type="appl" />
         <tli id="T695" time="517.740625" type="appl" />
         <tli id="T696" time="518.7645" type="appl" />
         <tli id="T697" time="519.788375" type="appl" />
         <tli id="T698" time="520.81225" type="appl" />
         <tli id="T699" time="521.836125" type="appl" />
         <tli id="T700" time="521.8666666666667" />
         <tli id="T701" time="523.0566666666667" />
         <tli id="T702" time="523.85" type="appl" />
         <tli id="T703" time="524.59" type="appl" />
         <tli id="T704" time="525.33" type="appl" />
         <tli id="T705" time="526.0699999999999" type="appl" />
         <tli id="T706" time="526.81" type="appl" />
         <tli id="T707" time="527.55" type="appl" />
         <tli id="T708" time="528.29" type="appl" />
         <tli id="T709" time="528.9033333333333" />
         <tli id="T710" time="529.655" type="appl" />
         <tli id="T711" time="530.5" type="appl" />
         <tli id="T712" time="531.345" type="appl" />
         <tli id="T713" time="531.42" />
         <tli id="T714" time="534.1333333333333" />
         <tli id="T715" time="535.103888888889" type="appl" />
         <tli id="T716" time="535.8577777777778" type="appl" />
         <tli id="T717" time="536.6116666666667" type="appl" />
         <tli id="T718" time="537.3655555555556" type="appl" />
         <tli id="T719" time="538.1194444444444" type="appl" />
         <tli id="T720" time="538.8733333333333" type="appl" />
         <tli id="T721" time="539.6272222222223" type="appl" />
         <tli id="T722" time="540.3811111111111" type="appl" />
         <tli id="T723" time="541.135" type="appl" />
         <tli id="T724" time="541.8888888888889" type="appl" />
         <tli id="T725" time="542.6427777777777" type="appl" />
         <tli id="T726" time="543.3966666666666" type="appl" />
         <tli id="T727" time="544.1505555555556" type="appl" />
         <tli id="T728" time="544.9044444444444" type="appl" />
         <tli id="T729" time="545.6583333333333" type="appl" />
         <tli id="T730" time="546.4122222222222" type="appl" />
         <tli id="T731" time="547.166111111111" type="appl" />
         <tli id="T732" time="547.92" type="appl" />
         <tli id="T835" time="549.032" type="intp" />
         <tli id="T733" time="550.7" type="appl" />
         <tli id="T734" time="551.4647368421053" type="appl" />
         <tli id="T735" time="552.2294736842106" type="appl" />
         <tli id="T736" time="552.9942105263158" type="appl" />
         <tli id="T737" time="553.7589473684211" type="appl" />
         <tli id="T738" time="554.5236842105263" type="appl" />
         <tli id="T739" time="555.2884210526316" type="appl" />
         <tli id="T740" time="556.0531578947368" type="appl" />
         <tli id="T741" time="556.8178947368422" type="appl" />
         <tli id="T742" time="557.5826315789474" type="appl" />
         <tli id="T743" time="558.3473684210527" type="appl" />
         <tli id="T744" time="559.1121052631579" type="appl" />
         <tli id="T745" time="559.8768421052632" type="appl" />
         <tli id="T746" time="560.6415789473684" type="appl" />
         <tli id="T747" time="561.4063157894738" type="appl" />
         <tli id="T748" time="562.171052631579" type="appl" />
         <tli id="T749" time="562.9357894736843" type="appl" />
         <tli id="T750" time="563.7005263157895" type="appl" />
         <tli id="T751" time="563.92" />
         <tli id="T752" time="564.14" />
         <tli id="T753" time="565.3633333333333" />
         <tli id="T754" time="566.3386666666667" type="appl" />
         <tli id="T755" time="567.2873333333333" type="appl" />
         <tli id="T756" time="568.236" type="appl" />
         <tli id="T757" time="569.1846666666667" type="appl" />
         <tli id="T758" time="570.1333333333333" type="appl" />
         <tli id="T759" time="571.082" type="appl" />
         <tli id="T760" time="572.0306666666667" type="appl" />
         <tli id="T761" time="572.9793333333333" type="appl" />
         <tli id="T762" time="573.928" type="appl" />
         <tli id="T763" time="574.8766666666667" type="appl" />
         <tli id="T764" time="575.8253333333333" type="appl" />
         <tli id="T765" time="576.774" type="appl" />
         <tli id="T766" time="577.7226666666667" type="appl" />
         <tli id="T767" time="578.6713333333333" type="appl" />
         <tli id="T768" time="579.62" type="appl" />
         <tli id="T769" time="581.4133333333334" />
         <tli id="T770" time="581.5" />
         <tli id="T771" time="581.5699999999999" type="appl" />
         <tli id="T772" time="582.35" type="appl" />
         <tli id="T773" time="583.13" type="appl" />
         <tli id="T774" time="583.91" type="appl" />
         <tli id="T775" time="584.6899999999999" type="appl" />
         <tli id="T776" time="585.47" type="appl" />
         <tli id="T777" time="586.25" type="appl" />
         <tli id="T778" time="587.03" type="appl" />
         <tli id="T779" time="587.81" type="appl" />
         <tli id="T780" time="588.5899999999999" type="appl" />
         <tli id="T781" time="589.37" type="appl" />
         <tli id="T782" time="590.15" type="appl" />
         <tli id="T783" time="590.93" type="appl" />
         <tli id="T784" time="591.7099999999999" type="appl" />
         <tli id="T785" time="592.49" type="appl" />
         <tli id="T786" time="593.27" type="appl" />
         <tli id="T787" time="593.8566666666667" />
         <tli id="T788" time="593.86" />
         <tli id="T789" time="594.5133333333333" type="appl" />
         <tli id="T790" time="594.9266666666667" type="appl" />
         <tli id="T791" time="595.34" type="appl" />
         <tli id="T792" time="595.7533333333333" type="appl" />
         <tli id="T793" time="596.1666666666667" type="appl" />
         <tli id="T794" time="596.58" type="appl" />
         <tli id="T795" time="597.6433333333333" />
         <tli id="T796" time="598.1341666666666" type="appl" />
         <tli id="T797" time="598.4983333333333" type="appl" />
         <tli id="T798" time="598.8625" type="appl" />
         <tli id="T799" time="599.2266666666667" type="appl" />
         <tli id="T800" time="599.5908333333333" type="appl" />
         <tli id="T801" time="599.9549999999999" type="appl" />
         <tli id="T802" time="600.3191666666667" type="appl" />
         <tli id="T803" time="600.6833333333333" type="appl" />
         <tli id="T804" time="601.0475" type="appl" />
         <tli id="T805" time="601.4116666666666" type="appl" />
         <tli id="T806" time="601.7758333333334" type="appl" />
         <tli id="T807" time="602.02" />
         <tli id="T808" time="602.6" type="appl" />
         <tli id="T809" time="603.048" type="appl" />
         <tli id="T810" time="603.496" type="appl" />
         <tli id="T811" time="603.9440000000001" type="appl" />
         <tli id="T812" time="604.392" type="appl" />
         <tli id="T813" time="604.74" />
         <tli id="T814" time="604.84" />
         <tli id="T815" time="605.6391666666667" type="appl" />
         <tli id="T816" time="606.3183333333334" type="appl" />
         <tli id="T817" time="606.9975000000001" type="appl" />
         <tli id="T818" time="607.6766666666667" type="appl" />
         <tli id="T819" time="608.3558333333334" type="appl" />
         <tli id="T820" time="609.0350000000001" type="appl" />
         <tli id="T821" time="609.7141666666666" type="appl" />
         <tli id="T822" time="610.3933333333333" type="appl" />
         <tli id="T823" time="611.0725" type="appl" />
         <tli id="T824" time="611.7516666666667" type="appl" />
         <tli id="T825" time="612.4308333333333" type="appl" />
         <tli id="T826" time="613.0233333333334" />
         <tli id="T827" time="613.4166666666667" />
         <tli id="T828" time="614.19" type="appl" />
         <tli id="T829" time="615.05" type="appl" />
         <tli id="T830" time="615.44" />
         <tli id="T831" time="617.38" type="appl" />
         <tli id="T832" time="618.0966666666667" type="appl" />
         <tli id="T833" time="618.8133333333333" type="appl" />
         <tli id="T834" time="619.53" type="appl" />
         <tli id="T0" time="621.72" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="unknown"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T7" id="Seg_0" n="sc" s="T1">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Min</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">aːtɨm</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">Iste</ts>
                  <nts id="Seg_11" n="HIAT:ip">,</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_14" n="HIAT:w" s="T4">hakalɨː</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_17" n="HIAT:w" s="T5">Iste</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_20" n="HIAT:w" s="T6">di͡eččiler</ts>
                  <nts id="Seg_21" n="HIAT:ip">.</nts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T12" id="Seg_23" n="sc" s="T8">
               <ts e="T12" id="Seg_25" n="HIAT:u" s="T8">
                  <ts e="T9" id="Seg_27" n="HIAT:w" s="T8">Töröːbütüm</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_30" n="HIAT:w" s="T9">Popigaj</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_33" n="HIAT:w" s="T10">di͡en</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_36" n="HIAT:w" s="T11">hirge</ts>
                  <nts id="Seg_37" n="HIAT:ip">.</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T22" id="Seg_39" n="sc" s="T13">
               <ts e="T22" id="Seg_41" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_43" n="HIAT:w" s="T13">Min</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_46" n="HIAT:w" s="T14">kergetterim</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_49" n="HIAT:w" s="T15">teːtem</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_52" n="HIAT:w" s="T16">kim</ts>
                  <nts id="Seg_53" n="HIAT:ip">,</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_56" n="HIAT:w" s="T17">Nʼukulaj</ts>
                  <nts id="Seg_57" n="HIAT:ip">,</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_60" n="HIAT:w" s="T18">maːmam</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_63" n="HIAT:w" s="T19">eto</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_66" n="HIAT:w" s="T20">Taiska</ts>
                  <nts id="Seg_67" n="HIAT:ip">,</nts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_70" n="HIAT:w" s="T21">Taiska</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T33" id="Seg_73" n="sc" s="T23">
               <ts e="T33" id="Seg_75" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_77" n="HIAT:w" s="T23">Ontuŋ</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_80" n="HIAT:w" s="T24">maːmam</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_83" n="HIAT:w" s="T25">erde</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_86" n="HIAT:w" s="T26">bagajɨ</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_89" n="HIAT:w" s="T27">ölbüte</ts>
                  <nts id="Seg_90" n="HIAT:ip">,</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_93" n="HIAT:w" s="T28">onton</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_96" n="HIAT:w" s="T29">min</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_99" n="HIAT:w" s="T30">teːtebin</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_102" n="HIAT:w" s="T31">gɨtta</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_105" n="HIAT:w" s="T32">kaːlbɨtɨm</ts>
                  <nts id="Seg_106" n="HIAT:ip">.</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T41" id="Seg_108" n="sc" s="T34">
               <ts e="T41" id="Seg_110" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_112" n="HIAT:w" s="T34">Teːtem</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_115" n="HIAT:w" s="T35">du͡o</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_118" n="HIAT:w" s="T36">kimŋe</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_121" n="HIAT:w" s="T37">pastuxga</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_124" n="HIAT:w" s="T38">brigadiːrga</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_127" n="HIAT:w" s="T39">üleliːr</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_130" n="HIAT:w" s="T40">ete</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T48" id="Seg_133" n="sc" s="T42">
               <ts e="T48" id="Seg_135" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_137" n="HIAT:w" s="T42">Onton</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_140" n="HIAT:w" s="T43">bɨraːtɨm</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_143" n="HIAT:w" s="T44">tože</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_146" n="HIAT:w" s="T45">teːtebin</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_149" n="HIAT:w" s="T46">gɨtta</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_152" n="HIAT:w" s="T47">hɨldʼaːččɨ</ts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T53" id="Seg_155" n="sc" s="T49">
               <ts e="T53" id="Seg_157" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_159" n="HIAT:w" s="T49">Tundraga</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_162" n="HIAT:w" s="T50">bihigi</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_165" n="HIAT:w" s="T51">köhö</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_168" n="HIAT:w" s="T52">hɨldʼaːččɨbɨt</ts>
                  <nts id="Seg_169" n="HIAT:ip">.</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T60" id="Seg_171" n="sc" s="T54">
               <ts e="T60" id="Seg_173" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_175" n="HIAT:w" s="T54">Školatan</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_178" n="HIAT:w" s="T55">kanikulbar</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_181" n="HIAT:w" s="T56">kellekpine</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_184" n="HIAT:w" s="T57">min</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_187" n="HIAT:w" s="T58">teːteber</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_190" n="HIAT:w" s="T59">bu͡olaːččɨbɨn</ts>
                  <nts id="Seg_191" n="HIAT:ip">.</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T75" id="Seg_193" n="sc" s="T61">
               <ts e="T75" id="Seg_195" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_197" n="HIAT:w" s="T61">No</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_200" n="HIAT:w" s="T62">ulaːppɨtɨm</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_203" n="HIAT:w" s="T63">kenne</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_206" n="HIAT:w" s="T64">min</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_209" n="HIAT:w" s="T65">teːteber</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_212" n="HIAT:w" s="T66">bu͡olbutum</ts>
                  <nts id="Seg_213" n="HIAT:ip">,</nts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_216" n="HIAT:w" s="T67">kogda</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_219" n="HIAT:w" s="T68">min</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_222" n="HIAT:w" s="T69">oččuguj</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_225" n="HIAT:w" s="T70">erdekpine</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_228" n="HIAT:w" s="T71">kihilerge</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_231" n="HIAT:w" s="T72">iːtillibitim</ts>
                  <nts id="Seg_232" n="HIAT:ip">,</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_235" n="HIAT:w" s="T73">atɨn</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_238" n="HIAT:w" s="T74">kihilerge</ts>
                  <nts id="Seg_239" n="HIAT:ip">.</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T87" id="Seg_241" n="sc" s="T76">
               <ts e="T87" id="Seg_243" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_245" n="HIAT:w" s="T76">Iːtillibitim</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_248" n="HIAT:w" s="T77">onton</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_251" n="HIAT:w" s="T78">ulaːtan</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_254" n="HIAT:w" s="T79">barammɨn</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_257" n="HIAT:w" s="T80">ere</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_260" n="HIAT:w" s="T81">teːteber</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_263" n="HIAT:w" s="T82">bu͡olbutum</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_266" n="HIAT:w" s="T83">baraːččɨbɨn</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_269" n="HIAT:w" s="T84">teːteber</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_272" n="HIAT:w" s="T85">kanikulga</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_275" n="HIAT:w" s="T86">kellekpine</ts>
                  <nts id="Seg_276" n="HIAT:ip">.</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T102" id="Seg_278" n="sc" s="T88">
               <ts e="T102" id="Seg_280" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_282" n="HIAT:w" s="T88">De</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_285" n="HIAT:w" s="T89">ol</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_288" n="HIAT:w" s="T90">köhö</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_291" n="HIAT:w" s="T91">hɨldʼaːččɨbɨt</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_294" n="HIAT:w" s="T92">kimten</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_297" n="HIAT:w" s="T93">mi͡esteten</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_300" n="HIAT:w" s="T94">mi͡estege</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_303" n="HIAT:w" s="T95">di͡eri</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_306" n="HIAT:w" s="T96">na</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_309" n="HIAT:w" s="T97">nartax</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_312" n="HIAT:w" s="T98">na</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_315" n="HIAT:w" s="T99">etix</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_318" n="HIAT:w" s="T100">köhö</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_321" n="HIAT:w" s="T101">hɨldʼaːččɨbɨt</ts>
                  <nts id="Seg_322" n="HIAT:ip">.</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T122" id="Seg_324" n="sc" s="T103">
               <ts e="T122" id="Seg_326" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_328" n="HIAT:w" s="T103">Čumŋa</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_331" n="HIAT:w" s="T104">dʼi͡eleːk</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_334" n="HIAT:w" s="T105">bu͡olaːččɨbɨt</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_337" n="HIAT:w" s="T106">tam</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_340" n="HIAT:w" s="T107">sneg</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_343" n="HIAT:w" s="T108">eh</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_346" n="HIAT:w" s="T109">kaːrɨ</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_349" n="HIAT:w" s="T110">kimniːbit</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_352" n="HIAT:w" s="T111">ubirajdɨːbɨt</ts>
                  <nts id="Seg_353" n="HIAT:ip">,</nts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_356" n="HIAT:w" s="T112">potom</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_359" n="HIAT:w" s="T113">mɨ</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_362" n="HIAT:w" s="T114">stavim</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_365" n="HIAT:w" s="T115">eti</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_368" n="HIAT:w" s="T116">palki</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_371" n="HIAT:w" s="T117">taki͡e</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_374" n="HIAT:w" s="T118">dlinnɨe</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_377" n="HIAT:w" s="T119">čum</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_380" n="HIAT:w" s="T120">čum</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_383" n="HIAT:w" s="T121">oŋorollor</ts>
                  <nts id="Seg_384" n="HIAT:ip">.</nts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T131" id="Seg_386" n="sc" s="T123">
               <ts e="T131" id="Seg_388" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_390" n="HIAT:w" s="T123">I</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_393" n="HIAT:w" s="T124">eto</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_396" n="HIAT:w" s="T125">i</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_399" n="HIAT:w" s="T126">eto</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_402" n="HIAT:w" s="T127">živem</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_405" n="HIAT:w" s="T128">tam</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_408" n="HIAT:w" s="T129">v</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_411" n="HIAT:w" s="T130">čume</ts>
                  <nts id="Seg_412" n="HIAT:ip">.</nts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T140" id="Seg_414" n="sc" s="T132">
               <ts e="T140" id="Seg_416" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_418" n="HIAT:w" s="T132">Heː</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_421" n="HIAT:w" s="T133">eto</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_424" n="HIAT:w" s="T134">ja</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_427" n="HIAT:w" s="T135">po</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_430" n="HIAT:w" s="T136">russki</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_433" n="HIAT:w" s="T137">uraha</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_436" n="HIAT:w" s="T138">dʼi͡ege</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_439" n="HIAT:w" s="T139">aha</ts>
                  <nts id="Seg_440" n="HIAT:ip">.</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T147" id="Seg_442" n="sc" s="T141">
               <ts e="T147" id="Seg_444" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_446" n="HIAT:w" s="T141">Urahalarɨ</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_449" n="HIAT:w" s="T142">delajut</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_452" n="HIAT:w" s="T143">kimten</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_455" n="HIAT:w" s="T144">taki͡e</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_458" n="HIAT:w" s="T145">dlinnɨe</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_461" n="HIAT:w" s="T146">taki͡e</ts>
                  <nts id="Seg_462" n="HIAT:ip">.</nts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T164" id="Seg_464" n="sc" s="T148">
               <ts e="T164" id="Seg_466" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_468" n="HIAT:w" s="T148">Ol</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_471" n="HIAT:w" s="T149">dʼi͡eŋ</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_474" n="HIAT:w" s="T150">inogda</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_477" n="HIAT:w" s="T151">ki͡eŋ</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_480" n="HIAT:w" s="T152">bagajɨ</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_483" n="HIAT:w" s="T153">oŋorollor</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_486" n="HIAT:w" s="T154">ügüs</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_489" n="HIAT:w" s="T155">kihi</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_492" n="HIAT:w" s="T156">ol</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_495" n="HIAT:w" s="T157">gɨnan</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_498" n="HIAT:w" s="T158">ügüspüt</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_501" n="HIAT:w" s="T159">mnogo</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_504" n="HIAT:w" s="T160">že</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_507" n="HIAT:w" s="T161">nam</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_510" n="HIAT:w" s="T162">ügüspüt</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_513" n="HIAT:w" s="T163">ved</ts>
                  <nts id="Seg_514" n="HIAT:ip">.</nts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T171" id="Seg_516" n="sc" s="T165">
               <ts e="T171" id="Seg_518" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_520" n="HIAT:w" s="T165">Širokij</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_523" n="HIAT:w" s="T166">kim</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_526" n="HIAT:w" s="T167">ki͡eŋ</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_529" n="HIAT:w" s="T168">dʼi͡e</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_532" n="HIAT:w" s="T169">ki͡eŋ</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_535" n="HIAT:w" s="T170">dʼuka</ts>
                  <nts id="Seg_536" n="HIAT:ip">.</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T182" id="Seg_538" n="sc" s="T172">
               <ts e="T182" id="Seg_540" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_542" n="HIAT:w" s="T172">Potom</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_545" n="HIAT:w" s="T173">pečkabɨt</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_548" n="HIAT:w" s="T174">ohok</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_551" n="HIAT:w" s="T175">timir</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_554" n="HIAT:w" s="T176">ohok</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_557" n="HIAT:w" s="T177">pečka</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_560" n="HIAT:w" s="T178">srazu</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_563" n="HIAT:w" s="T179">zatopljaem</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_566" n="HIAT:w" s="T180">i</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_569" n="HIAT:w" s="T181">teplo</ts>
                  <nts id="Seg_570" n="HIAT:ip">.</nts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T200" id="Seg_572" n="sc" s="T183">
               <ts e="T200" id="Seg_574" n="HIAT:u" s="T183">
                  <ts e="T184" id="Seg_576" n="HIAT:w" s="T183">I</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_579" n="HIAT:w" s="T184">eto</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_582" n="HIAT:w" s="T185">vniz</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_585" n="HIAT:w" s="T186">eto</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_588" n="HIAT:w" s="T187">kimin</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_591" n="HIAT:w" s="T188">annɨtɨn</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_594" n="HIAT:w" s="T189">snegom</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_597" n="HIAT:w" s="T190">kimniːller</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_599" n="HIAT:ip">(</nts>
                  <ts e="T192" id="Seg_601" n="HIAT:w" s="T191">battat-</ts>
                  <nts id="Seg_602" n="HIAT:ip">)</nts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_605" n="HIAT:w" s="T192">battɨk</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_608" n="HIAT:w" s="T193">oŋorollor</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_611" n="HIAT:w" s="T194">battatallar</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_614" n="HIAT:w" s="T195">i</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_617" n="HIAT:w" s="T196">srazu</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_620" n="HIAT:w" s="T197">štobɨ</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_623" n="HIAT:w" s="T198">kimneːmi͡egin</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_626" n="HIAT:w" s="T199">argɨjɨmɨ͡agɨn</ts>
                  <nts id="Seg_627" n="HIAT:ip">.</nts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T214" id="Seg_629" n="sc" s="T201">
               <ts e="T214" id="Seg_631" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_633" n="HIAT:w" s="T201">Daže</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_636" n="HIAT:w" s="T202">oččuguj</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_639" n="HIAT:w" s="T203">ogoloːk</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_642" n="HIAT:w" s="T204">ezdittaːččɨbɨt</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_645" n="HIAT:w" s="T205">tam</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_648" n="HIAT:w" s="T206">ljulka</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_651" n="HIAT:w" s="T207">tam</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_654" n="HIAT:w" s="T208">u</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_657" n="HIAT:w" s="T209">nix</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_660" n="HIAT:w" s="T210">kim</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_663" n="HIAT:w" s="T211">haka</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_666" n="HIAT:w" s="T212">ljulkaga</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_669" n="HIAT:w" s="T213">tuda</ts>
                  <nts id="Seg_670" n="HIAT:ip">.</nts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T223" id="Seg_672" n="sc" s="T215">
               <ts e="T223" id="Seg_674" n="HIAT:u" s="T215">
                  <ts e="T216" id="Seg_676" n="HIAT:w" s="T215">Jejo</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_679" n="HIAT:w" s="T216">položat</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_682" n="HIAT:w" s="T217">eti</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_685" n="HIAT:w" s="T218">rebjonka</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_688" n="HIAT:w" s="T219">i</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_691" n="HIAT:w" s="T220">ezdjat</ts>
                  <nts id="Seg_692" n="HIAT:ip">,</nts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_695" n="HIAT:w" s="T221">bara</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_698" n="HIAT:w" s="T222">hɨldʼabɨt</ts>
                  <nts id="Seg_699" n="HIAT:ip">.</nts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T234" id="Seg_701" n="sc" s="T224">
               <ts e="T234" id="Seg_703" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_705" n="HIAT:w" s="T224">Onton</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_708" n="HIAT:w" s="T225">tugu</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_711" n="HIAT:w" s="T226">kepsi͡emij</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_714" n="HIAT:w" s="T227">teːtem</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_717" n="HIAT:w" s="T228">du</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_720" n="HIAT:w" s="T229">Kamennɨj</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_723" n="HIAT:w" s="T230">kimŋe</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_726" n="HIAT:w" s="T231">storonaga</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_729" n="HIAT:w" s="T232">hɨldʼɨbɨta</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_732" n="HIAT:w" s="T233">Kamennɨj</ts>
                  <nts id="Seg_733" n="HIAT:ip">.</nts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T248" id="Seg_735" n="sc" s="T235">
               <ts e="T248" id="Seg_737" n="HIAT:u" s="T235">
                  <ts e="T236" id="Seg_739" n="HIAT:w" s="T235">Tam</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_742" n="HIAT:w" s="T236">netu</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_745" n="HIAT:w" s="T237">kim</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_748" n="HIAT:w" s="T238">kaːs</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_751" n="HIAT:w" s="T239">da</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_754" n="HIAT:w" s="T240">hu͡ok</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_757" n="HIAT:w" s="T241">utok</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_760" n="HIAT:w" s="T242">netu</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_763" n="HIAT:w" s="T243">tam</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_766" n="HIAT:w" s="T244">etix</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_769" n="HIAT:w" s="T245">tolko</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_772" n="HIAT:w" s="T246">odni</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_775" n="HIAT:w" s="T247">oleni</ts>
                  <nts id="Seg_776" n="HIAT:ip">.</nts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T261" id="Seg_778" n="sc" s="T249">
               <ts e="T261" id="Seg_780" n="HIAT:u" s="T249">
                  <ts e="T250" id="Seg_782" n="HIAT:w" s="T249">Tam</ts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_785" n="HIAT:w" s="T250">kamennɨe</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_788" n="HIAT:w" s="T251">mɨšɨ</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_791" n="HIAT:w" s="T252">redko</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_794" n="HIAT:w" s="T253">kim</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_797" n="HIAT:w" s="T254">kutujaktar</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_800" n="HIAT:w" s="T255">kamen</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_803" n="HIAT:w" s="T256">kamen</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_806" n="HIAT:w" s="T257">pod</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_809" n="HIAT:w" s="T258">kamjem</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_812" n="HIAT:w" s="T259">inogda</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_815" n="HIAT:w" s="T260">bɨvaet</ts>
                  <nts id="Seg_816" n="HIAT:ip">.</nts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T274" id="Seg_818" n="sc" s="T262">
               <ts e="T274" id="Seg_820" n="HIAT:u" s="T262">
                  <ts e="T263" id="Seg_822" n="HIAT:w" s="T262">Ješo</ts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_825" n="HIAT:w" s="T263">tuːguj</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_828" n="HIAT:w" s="T264">kim</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_831" n="HIAT:w" s="T265">min</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_834" n="HIAT:w" s="T266">bejem</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_837" n="HIAT:w" s="T267">ü͡öremmitim</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_840" n="HIAT:w" s="T268">hette</ts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_843" n="HIAT:w" s="T269">kɨlaːska</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_846" n="HIAT:w" s="T270">di͡eri</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_849" n="HIAT:w" s="T271">agsɨs</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_852" n="HIAT:w" s="T272">kɨlaːstan</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_855" n="HIAT:w" s="T273">büppütüm</ts>
                  <nts id="Seg_856" n="HIAT:ip">.</nts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T280" id="Seg_858" n="sc" s="T275">
               <ts e="T280" id="Seg_860" n="HIAT:u" s="T275">
                  <ts e="T276" id="Seg_862" n="HIAT:w" s="T275">Kimi</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_865" n="HIAT:w" s="T276">internaːkka</ts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_868" n="HIAT:w" s="T277">onton</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_871" n="HIAT:w" s="T278">internaːppɨt</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_874" n="HIAT:w" s="T279">bu͡olaːččɨbɨt</ts>
                  <nts id="Seg_875" n="HIAT:ip">.</nts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T289" id="Seg_877" n="sc" s="T281">
               <ts e="T289" id="Seg_879" n="HIAT:u" s="T281">
                  <ts e="T282" id="Seg_881" n="HIAT:w" s="T281">Ikki</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_884" n="HIAT:w" s="T282">dʼɨlɨ</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_887" n="HIAT:w" s="T283">Norilskaga</ts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_890" n="HIAT:w" s="T284">ü͡öremmippit</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_893" n="HIAT:w" s="T285">internaːkka</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_895" n="HIAT:ip">(</nts>
                  <ts e="T287" id="Seg_897" n="HIAT:w" s="T286">ž-</ts>
                  <nts id="Seg_898" n="HIAT:ip">)</nts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_901" n="HIAT:w" s="T287">kim</ts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_904" n="HIAT:w" s="T288">olorbupput</ts>
                  <nts id="Seg_905" n="HIAT:ip">.</nts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T296" id="Seg_907" n="sc" s="T290">
               <ts e="T296" id="Seg_909" n="HIAT:u" s="T290">
                  <ts e="T291" id="Seg_911" n="HIAT:w" s="T290">Lagerga</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_914" n="HIAT:w" s="T291">hɨldʼɨbɨtɨm</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_917" n="HIAT:w" s="T292">kimŋe</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_920" n="HIAT:w" s="T293">Tajožnajga</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_923" n="HIAT:w" s="T294">hɨldʼɨbɨtɨm</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_926" n="HIAT:w" s="T295">Kurejkaga</ts>
                  <nts id="Seg_927" n="HIAT:ip">.</nts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T306" id="Seg_929" n="sc" s="T297">
               <ts e="T306" id="Seg_931" n="HIAT:u" s="T297">
                  <ts e="T298" id="Seg_933" n="HIAT:w" s="T297">Potom</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_936" n="HIAT:w" s="T298">ulaːtan</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_939" n="HIAT:w" s="T299">baran</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_942" n="HIAT:w" s="T300">kimŋe</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_944" n="HIAT:ip">(</nts>
                  <ts e="T302" id="Seg_946" n="HIAT:w" s="T301">ü͡ör-</ts>
                  <nts id="Seg_947" n="HIAT:ip">)</nts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_950" n="HIAT:w" s="T302">eː</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_953" n="HIAT:w" s="T303">ol</ts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_956" n="HIAT:w" s="T304">ü͡örene</ts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_959" n="HIAT:w" s="T305">barbɨtɨm</ts>
                  <nts id="Seg_960" n="HIAT:ip">.</nts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T312" id="Seg_962" n="sc" s="T307">
               <ts e="T312" id="Seg_964" n="HIAT:u" s="T307">
                  <ts e="T308" id="Seg_966" n="HIAT:w" s="T307">Na</ts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_969" n="HIAT:w" s="T308">eto</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_972" n="HIAT:w" s="T309">na</ts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_975" n="HIAT:w" s="T310">kursɨ</ts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_978" n="HIAT:w" s="T311">kimŋe</ts>
                  <nts id="Seg_979" n="HIAT:ip">.</nts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T321" id="Seg_981" n="sc" s="T313">
               <ts e="T321" id="Seg_983" n="HIAT:u" s="T313">
                  <ts e="T314" id="Seg_985" n="HIAT:w" s="T313">Atɨːlɨːr</ts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_988" n="HIAT:w" s="T314">kihi</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_991" n="HIAT:w" s="T315">ke</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_994" n="HIAT:w" s="T316">kimŋe</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_997" n="HIAT:w" s="T317">prodaveska</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1000" n="HIAT:w" s="T318">barbɨtɨm</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1003" n="HIAT:w" s="T319">ü͡örene</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1006" n="HIAT:w" s="T320">Dudinkaga</ts>
                  <nts id="Seg_1007" n="HIAT:ip">.</nts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T338" id="Seg_1009" n="sc" s="T322">
               <ts e="T338" id="Seg_1011" n="HIAT:u" s="T322">
                  <ts e="T323" id="Seg_1013" n="HIAT:w" s="T322">Alta</ts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1016" n="HIAT:w" s="T323">ɨjɨ</ts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1019" n="HIAT:w" s="T324">ü͡öremmitim</ts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1022" n="HIAT:w" s="T325">onton</ts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1025" n="HIAT:w" s="T326">kelbitim</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1028" n="HIAT:w" s="T327">i</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1031" n="HIAT:w" s="T328">Nosku͡oga</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1033" n="HIAT:ip">(</nts>
                  <ts e="T330" id="Seg_1035" n="HIAT:w" s="T329">nemno-</ts>
                  <nts id="Seg_1036" n="HIAT:ip">)</nts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1039" n="HIAT:w" s="T330">kimneːbitim</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1042" n="HIAT:w" s="T331">praktikanɨ</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1045" n="HIAT:w" s="T332">proxodili</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1048" n="HIAT:w" s="T333">i</ts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1051" n="HIAT:w" s="T334">tam</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1054" n="HIAT:w" s="T335">kimŋe</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1057" n="HIAT:w" s="T336">bufekka</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1060" n="HIAT:w" s="T337">üleleːččibin</ts>
                  <nts id="Seg_1061" n="HIAT:ip">.</nts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T347" id="Seg_1063" n="sc" s="T339">
               <ts e="T347" id="Seg_1065" n="HIAT:u" s="T339">
                  <ts e="T340" id="Seg_1067" n="HIAT:w" s="T339">Stolovojga</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1070" n="HIAT:w" s="T340">bufet</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1073" n="HIAT:w" s="T341">ete</ts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1076" n="HIAT:w" s="T342">Nosku͡oga</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1079" n="HIAT:w" s="T343">onno</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1082" n="HIAT:w" s="T344">etot</ts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1085" n="HIAT:w" s="T345">bufekka</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1088" n="HIAT:w" s="T346">üleleːččibin</ts>
                  <nts id="Seg_1089" n="HIAT:ip">.</nts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T360" id="Seg_1091" n="sc" s="T348">
               <ts e="T360" id="Seg_1093" n="HIAT:u" s="T348">
                  <ts e="T349" id="Seg_1095" n="HIAT:w" s="T348">Onton</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1098" n="HIAT:w" s="T349">v</ts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1101" n="HIAT:w" s="T350">etom</ts>
                  <nts id="Seg_1102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1104" n="HIAT:w" s="T351">Nosku͡oga</ts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1107" n="HIAT:w" s="T352">biːr</ts>
                  <nts id="Seg_1108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1110" n="HIAT:w" s="T353">kihini</ts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1113" n="HIAT:w" s="T354">munna</ts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1116" n="HIAT:w" s="T355">Ki͡etaːtan</ts>
                  <nts id="Seg_1117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1119" n="HIAT:w" s="T356">biːr</ts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1122" n="HIAT:w" s="T357">u͡olu</ts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1125" n="HIAT:w" s="T358">gɨtta</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1128" n="HIAT:w" s="T359">dogordosputum</ts>
                  <nts id="Seg_1129" n="HIAT:ip">.</nts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T368" id="Seg_1131" n="sc" s="T361">
               <ts e="T368" id="Seg_1133" n="HIAT:u" s="T361">
                  <ts e="T362" id="Seg_1135" n="HIAT:w" s="T361">I</ts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1138" n="HIAT:w" s="T362">mɨ</ts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1141" n="HIAT:w" s="T363">tam</ts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1144" n="HIAT:w" s="T364">poženilis</ts>
                  <nts id="Seg_1145" n="HIAT:ip">,</nts>
                  <nts id="Seg_1146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1148" n="HIAT:w" s="T365">poznakomilis</ts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1151" n="HIAT:w" s="T366">i</ts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1154" n="HIAT:w" s="T367">poženilis</ts>
                  <nts id="Seg_1155" n="HIAT:ip">.</nts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T377" id="Seg_1157" n="sc" s="T369">
               <ts e="T377" id="Seg_1159" n="HIAT:u" s="T369">
                  <ts e="T370" id="Seg_1161" n="HIAT:w" s="T369">Onton</ts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1164" n="HIAT:w" s="T370">ɨlsan</ts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1167" n="HIAT:w" s="T371">barammɨt</ts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1170" n="HIAT:w" s="T372">to</ts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1173" n="HIAT:w" s="T373">kimneːbippit</ts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1176" n="HIAT:w" s="T374">Nosku͡oga</ts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1179" n="HIAT:w" s="T375">oloro</ts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1182" n="HIAT:w" s="T376">tüspütüm</ts>
                  <nts id="Seg_1183" n="HIAT:ip">.</nts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T390" id="Seg_1185" n="sc" s="T378">
               <ts e="T390" id="Seg_1187" n="HIAT:u" s="T378">
                  <ts e="T379" id="Seg_1189" n="HIAT:w" s="T378">Min</ts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1192" n="HIAT:w" s="T379">saːdikka</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1195" n="HIAT:w" s="T380">üleliːr</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1198" n="HIAT:w" s="T381">etim</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1201" n="HIAT:w" s="T382">njanjaga</ts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1204" n="HIAT:w" s="T383">a</ts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1207" n="HIAT:w" s="T384">giniŋ</ts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1210" n="HIAT:w" s="T385">du͡o</ts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1213" n="HIAT:w" s="T386">počtaga</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1216" n="HIAT:w" s="T387">montjorom</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1219" n="HIAT:w" s="T388">üleliːr</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1222" n="HIAT:w" s="T389">ete</ts>
                  <nts id="Seg_1223" n="HIAT:ip">.</nts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T403" id="Seg_1225" n="sc" s="T391">
               <ts e="T403" id="Seg_1227" n="HIAT:u" s="T391">
                  <ts e="T392" id="Seg_1229" n="HIAT:w" s="T391">Onton</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1232" n="HIAT:w" s="T392">u</ts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1235" n="HIAT:w" s="T393">nas</ts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1238" n="HIAT:w" s="T394">rodilsja</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1241" n="HIAT:w" s="T395">vot</ts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1244" n="HIAT:w" s="T396">kim</ts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1247" n="HIAT:w" s="T397">kɨːspɨt</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1250" n="HIAT:w" s="T398">kɨːhɨm</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1253" n="HIAT:w" s="T399">Riːta</ts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1256" n="HIAT:w" s="T400">aːttaːk</ts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1259" n="HIAT:w" s="T401">kɨːhɨm</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1262" n="HIAT:w" s="T402">u͡olum</ts>
                  <nts id="Seg_1263" n="HIAT:ip">.</nts>
                  <nts id="Seg_1264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T414" id="Seg_1265" n="sc" s="T404">
               <ts e="T414" id="Seg_1267" n="HIAT:u" s="T404">
                  <ts e="T405" id="Seg_1269" n="HIAT:w" s="T404">Onton</ts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1272" n="HIAT:w" s="T405">on</ts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1275" n="HIAT:w" s="T406">što</ts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1278" n="HIAT:w" s="T407">to</ts>
                  <nts id="Seg_1279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1281" n="HIAT:w" s="T408">zaxotel</ts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1283" n="HIAT:ip">(</nts>
                  <ts e="T410" id="Seg_1285" n="HIAT:w" s="T409">bɨt-</ts>
                  <nts id="Seg_1286" n="HIAT:ip">)</nts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1289" n="HIAT:w" s="T410">hiriger</ts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1292" n="HIAT:w" s="T411">keli͡egin</ts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1295" n="HIAT:w" s="T412">rodinatɨgar</ts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1298" n="HIAT:w" s="T413">keli͡egin</ts>
                  <nts id="Seg_1299" n="HIAT:ip">.</nts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T422" id="Seg_1301" n="sc" s="T415">
               <ts e="T422" id="Seg_1303" n="HIAT:u" s="T415">
                  <ts e="T416" id="Seg_1305" n="HIAT:w" s="T415">I</ts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1308" n="HIAT:w" s="T416">on</ts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1311" n="HIAT:w" s="T417">menja</ts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1314" n="HIAT:w" s="T418">snačala</ts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1317" n="HIAT:w" s="T419">menja</ts>
                  <nts id="Seg_1318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1320" n="HIAT:w" s="T420">otpravil</ts>
                  <nts id="Seg_1321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1323" n="HIAT:w" s="T421">ɨːppɨta</ts>
                  <nts id="Seg_1324" n="HIAT:ip">.</nts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T428" id="Seg_1326" n="sc" s="T423">
               <ts e="T428" id="Seg_1328" n="HIAT:u" s="T423">
                  <ts e="T424" id="Seg_1330" n="HIAT:w" s="T423">Ogobun</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1333" n="HIAT:w" s="T424">gɨtta</ts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1336" n="HIAT:w" s="T425">ke</ts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1339" n="HIAT:w" s="T426">kɨːspɨn</ts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1342" n="HIAT:w" s="T427">gɨtta</ts>
                  <nts id="Seg_1343" n="HIAT:ip">.</nts>
                  <nts id="Seg_1344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T438" id="Seg_1345" n="sc" s="T429">
               <ts e="T438" id="Seg_1347" n="HIAT:u" s="T429">
                  <ts e="T430" id="Seg_1349" n="HIAT:w" s="T429">I</ts>
                  <nts id="Seg_1350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1352" n="HIAT:w" s="T430">potom</ts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1355" n="HIAT:w" s="T431">tolko</ts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1358" n="HIAT:w" s="T432">gini</ts>
                  <nts id="Seg_1359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1361" n="HIAT:w" s="T433">kennige</ts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1364" n="HIAT:w" s="T434">min</ts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1367" n="HIAT:w" s="T435">kennibitten</ts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1370" n="HIAT:w" s="T436">gini</ts>
                  <nts id="Seg_1371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1373" n="HIAT:w" s="T437">kelbite</ts>
                  <nts id="Seg_1374" n="HIAT:ip">.</nts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T456" id="Seg_1376" n="sc" s="T439">
               <ts e="T456" id="Seg_1378" n="HIAT:u" s="T439">
                  <ts e="T440" id="Seg_1380" n="HIAT:w" s="T439">I</ts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1383" n="HIAT:w" s="T440">vot</ts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1386" n="HIAT:w" s="T441">s</ts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1389" n="HIAT:w" s="T442">tex</ts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1392" n="HIAT:w" s="T443">por</ts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1395" n="HIAT:w" s="T444">kas</ts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1398" n="HIAT:w" s="T445">dʼɨl</ts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1401" n="HIAT:w" s="T446">bu͡olla</ts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1404" n="HIAT:w" s="T447">ke</ts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1407" n="HIAT:w" s="T448">šest</ts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1410" n="HIAT:w" s="T449">šestdesjat</ts>
                  <nts id="Seg_1411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1413" n="HIAT:w" s="T450">pervɨj</ts>
                  <nts id="Seg_1414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1416" n="HIAT:w" s="T451">gottan</ts>
                  <nts id="Seg_1417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1419" n="HIAT:w" s="T452">vot</ts>
                  <nts id="Seg_1420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1422" n="HIAT:w" s="T453">ja</ts>
                  <nts id="Seg_1423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1425" n="HIAT:w" s="T454">živu</ts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1428" n="HIAT:w" s="T455">zdes</ts>
                  <nts id="Seg_1429" n="HIAT:ip">.</nts>
                  <nts id="Seg_1430" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T466" id="Seg_1431" n="sc" s="T457">
               <ts e="T466" id="Seg_1433" n="HIAT:u" s="T457">
                  <ts e="T458" id="Seg_1435" n="HIAT:w" s="T457">I</ts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1438" n="HIAT:w" s="T458">on</ts>
                  <nts id="Seg_1439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1441" n="HIAT:w" s="T459">umer</ts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1444" n="HIAT:w" s="T460">gde</ts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1447" n="HIAT:w" s="T461">to</ts>
                  <nts id="Seg_1448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1450" n="HIAT:w" s="T462">semdesjat</ts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1453" n="HIAT:w" s="T463">devjatɨj</ts>
                  <nts id="Seg_1454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1456" n="HIAT:w" s="T464">gokka</ts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1459" n="HIAT:w" s="T465">ölbüte</ts>
                  <nts id="Seg_1460" n="HIAT:ip">.</nts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T481" id="Seg_1462" n="sc" s="T467">
               <ts e="T481" id="Seg_1464" n="HIAT:u" s="T467">
                  <ts e="T468" id="Seg_1466" n="HIAT:w" s="T467">I</ts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1469" n="HIAT:w" s="T468">s</ts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1472" n="HIAT:w" s="T469">tex</ts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1475" n="HIAT:w" s="T470">por</ts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1478" n="HIAT:w" s="T471">zdes</ts>
                  <nts id="Seg_1479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1481" n="HIAT:w" s="T472">min</ts>
                  <nts id="Seg_1482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1484" n="HIAT:w" s="T473">ogolommutum</ts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1487" n="HIAT:w" s="T474">u͡olbun</ts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1490" n="HIAT:w" s="T475">kotorɨj</ts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1493" n="HIAT:w" s="T476">služil</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1496" n="HIAT:w" s="T477">eto</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1499" n="HIAT:w" s="T478">učilsja</ts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1502" n="HIAT:w" s="T479">v</ts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1505" n="HIAT:w" s="T480">Irkutske</ts>
                  <nts id="Seg_1506" n="HIAT:ip">.</nts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T484" id="Seg_1508" n="sc" s="T482">
               <ts e="T484" id="Seg_1510" n="HIAT:u" s="T482">
                  <ts e="T483" id="Seg_1512" n="HIAT:w" s="T482">Služil</ts>
                  <nts id="Seg_1513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1515" n="HIAT:w" s="T483">Usurijskajga</ts>
                  <nts id="Seg_1516" n="HIAT:ip">.</nts>
                  <nts id="Seg_1517" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T502" id="Seg_1518" n="sc" s="T485">
               <ts e="T502" id="Seg_1520" n="HIAT:u" s="T485">
                  <ts e="T486" id="Seg_1522" n="HIAT:w" s="T485">Nu</ts>
                  <nts id="Seg_1523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1525" n="HIAT:w" s="T486">tak</ts>
                  <nts id="Seg_1526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1528" n="HIAT:w" s="T487">i</ts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1531" n="HIAT:w" s="T488">s</ts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1534" n="HIAT:w" s="T489">tex</ts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1537" n="HIAT:w" s="T490">por</ts>
                  <nts id="Seg_1538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1540" n="HIAT:w" s="T491">živu</ts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1543" n="HIAT:w" s="T492">olorobun</ts>
                  <nts id="Seg_1544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1546" n="HIAT:w" s="T493">saːdikka</ts>
                  <nts id="Seg_1547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1549" n="HIAT:w" s="T494">üleleːbitim</ts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1552" n="HIAT:w" s="T495">do</ts>
                  <nts id="Seg_1553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1555" n="HIAT:w" s="T496">pensia</ts>
                  <nts id="Seg_1556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1558" n="HIAT:w" s="T497">uže</ts>
                  <nts id="Seg_1559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1561" n="HIAT:w" s="T498">saːdikka</ts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1564" n="HIAT:w" s="T499">hubu</ts>
                  <nts id="Seg_1565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1567" n="HIAT:w" s="T500">že</ts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1570" n="HIAT:w" s="T501">saːdikka</ts>
                  <nts id="Seg_1571" n="HIAT:ip">.</nts>
                  <nts id="Seg_1572" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T518" id="Seg_1573" n="sc" s="T503">
               <ts e="T518" id="Seg_1575" n="HIAT:u" s="T503">
                  <ts e="T504" id="Seg_1577" n="HIAT:w" s="T503">Onton</ts>
                  <nts id="Seg_1578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1580" n="HIAT:w" s="T504">tugu</ts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1583" n="HIAT:w" s="T505">kepsi͡emij</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1586" n="HIAT:w" s="T506">onton</ts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1589" n="HIAT:w" s="T507">erim</ts>
                  <nts id="Seg_1590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1592" n="HIAT:w" s="T508">du͡o</ts>
                  <nts id="Seg_1593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1595" n="HIAT:w" s="T509">ol</ts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1598" n="HIAT:w" s="T510">erim</ts>
                  <nts id="Seg_1599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1601" n="HIAT:w" s="T511">rɨbalkaga</ts>
                  <nts id="Seg_1602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1604" n="HIAT:w" s="T512">bu͡olaːččɨ</ts>
                  <nts id="Seg_1605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1607" n="HIAT:w" s="T513">itinne</ts>
                  <nts id="Seg_1608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1609" n="HIAT:ip">(</nts>
                  <ts e="T515" id="Seg_1611" n="HIAT:w" s="T514">to-</ts>
                  <nts id="Seg_1612" n="HIAT:ip">)</nts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1615" n="HIAT:w" s="T515">točkaga</ts>
                  <nts id="Seg_1616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1618" n="HIAT:w" s="T516">oloroːččubut</ts>
                  <nts id="Seg_1619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1621" n="HIAT:w" s="T517">čugas</ts>
                  <nts id="Seg_1622" n="HIAT:ip">.</nts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T528" id="Seg_1624" n="sc" s="T519">
               <ts e="T528" id="Seg_1626" n="HIAT:u" s="T519">
                  <ts e="T520" id="Seg_1628" n="HIAT:w" s="T519">Kogda</ts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1631" n="HIAT:w" s="T520">u</ts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1634" n="HIAT:w" s="T521">menja</ts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1637" n="HIAT:w" s="T522">vɨxodnɨe</ts>
                  <nts id="Seg_1638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1640" n="HIAT:w" s="T523">vɨxodnojdarbar</ts>
                  <nts id="Seg_1641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1643" n="HIAT:w" s="T524">gini͡eke</ts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1646" n="HIAT:w" s="T525">baraːččɨbɨn</ts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1649" n="HIAT:w" s="T526">motorunan</ts>
                  <nts id="Seg_1650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1652" n="HIAT:w" s="T527">ezditaːččɨbɨt</ts>
                  <nts id="Seg_1653" n="HIAT:ip">.</nts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T542" id="Seg_1655" n="sc" s="T529">
               <ts e="T541" id="Seg_1657" n="HIAT:u" s="T529">
                  <ts e="T530" id="Seg_1659" n="HIAT:w" s="T529">I</ts>
                  <nts id="Seg_1660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1662" n="HIAT:w" s="T530">tam</ts>
                  <nts id="Seg_1663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1665" n="HIAT:w" s="T531">ja</ts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1668" n="HIAT:w" s="T532">kim</ts>
                  <nts id="Seg_1669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1671" n="HIAT:w" s="T533">kömölöhöːččübün</ts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1674" n="HIAT:w" s="T534">rɨbu</ts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1677" n="HIAT:w" s="T535">razdel</ts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1680" n="HIAT:w" s="T536">kimni</ts>
                  <nts id="Seg_1681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1683" n="HIAT:w" s="T537">vɨdelɨval</ts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1686" n="HIAT:w" s="T538">eto</ts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1689" n="HIAT:w" s="T539">razdelɨvajdɨː</ts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1692" n="HIAT:w" s="T540">kanʼɨː</ts>
                  <nts id="Seg_1693" n="HIAT:ip">.</nts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T542" id="Seg_1696" n="HIAT:u" s="T541">
                  <ts e="T542" id="Seg_1698" n="HIAT:w" s="T541">Kanʼiː</ts>
                  <nts id="Seg_1699" n="HIAT:ip">.</nts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T550" id="Seg_1701" n="sc" s="T543">
               <ts e="T550" id="Seg_1703" n="HIAT:u" s="T543">
                  <ts e="T544" id="Seg_1705" n="HIAT:w" s="T543">Bi͡ereːččibit</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1708" n="HIAT:w" s="T544">že</ts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1711" n="HIAT:w" s="T545">eto</ts>
                  <nts id="Seg_1712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_1714" n="HIAT:w" s="T546">kimŋe</ts>
                  <nts id="Seg_1715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1717" n="HIAT:w" s="T547">rɨbzavokka</ts>
                  <nts id="Seg_1718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1720" n="HIAT:w" s="T548">bi͡ereːččibit</ts>
                  <nts id="Seg_1721" n="HIAT:ip">,</nts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_1724" n="HIAT:w" s="T549">sdavajdaːččɨbɨt</ts>
                  <nts id="Seg_1725" n="HIAT:ip">.</nts>
                  <nts id="Seg_1726" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T555" id="Seg_1727" n="sc" s="T551">
               <ts e="T555" id="Seg_1729" n="HIAT:u" s="T551">
                  <ts e="T552" id="Seg_1731" n="HIAT:w" s="T551">Oni</ts>
                  <nts id="Seg_1732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_1734" n="HIAT:w" s="T552">lovili</ts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_1737" n="HIAT:w" s="T553">mnogo</ts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_1740" n="HIAT:w" s="T554">rɨbɨ</ts>
                  <nts id="Seg_1741" n="HIAT:ip">.</nts>
                  <nts id="Seg_1742" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T564" id="Seg_1743" n="sc" s="T556">
               <ts e="T564" id="Seg_1745" n="HIAT:u" s="T556">
                  <ts e="T557" id="Seg_1747" n="HIAT:w" s="T556">Utrom</ts>
                  <nts id="Seg_1748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_1750" n="HIAT:w" s="T557">rano</ts>
                  <nts id="Seg_1751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_1753" n="HIAT:w" s="T558">baraːččɨlar</ts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_1756" n="HIAT:w" s="T559">potom</ts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_1759" n="HIAT:w" s="T560">seti</ts>
                  <nts id="Seg_1760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_1762" n="HIAT:w" s="T561">proverjat</ts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_1765" n="HIAT:w" s="T562">pozdno</ts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_1768" n="HIAT:w" s="T563">pri͡ezzhajut</ts>
                  <nts id="Seg_1769" n="HIAT:ip">.</nts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T570" id="Seg_1771" n="sc" s="T565">
               <ts e="T570" id="Seg_1773" n="HIAT:u" s="T565">
                  <ts e="T566" id="Seg_1775" n="HIAT:w" s="T565">Počti</ts>
                  <nts id="Seg_1776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_1778" n="HIAT:w" s="T566">vot</ts>
                  <nts id="Seg_1779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1780" n="HIAT:ip">(</nts>
                  <ts e="T568" id="Seg_1782" n="HIAT:w" s="T567">sid-</ts>
                  <nts id="Seg_1783" n="HIAT:ip">)</nts>
                  <nts id="Seg_1784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_1786" n="HIAT:w" s="T568">oloroːččubut</ts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_1789" n="HIAT:w" s="T569">tüːnneri</ts>
                  <nts id="Seg_1790" n="HIAT:ip">.</nts>
                  <nts id="Seg_1791" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T607" id="Seg_1792" n="sc" s="T571">
               <ts e="T583" id="Seg_1794" n="HIAT:u" s="T571">
                  <ts e="T572" id="Seg_1796" n="HIAT:w" s="T571">Kumar</ts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_1799" n="HIAT:w" s="T572">letom</ts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_1802" n="HIAT:w" s="T573">komarka</ts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_1805" n="HIAT:w" s="T574">hi͡ete</ts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_1808" n="HIAT:w" s="T575">hi͡ete</ts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_1811" n="HIAT:w" s="T576">ol</ts>
                  <nts id="Seg_1812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_1814" n="HIAT:w" s="T577">ogolorbut</ts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_1817" n="HIAT:w" s="T578">ke</ts>
                  <nts id="Seg_1818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_1820" n="HIAT:w" s="T579">u͡olattarbut</ts>
                  <nts id="Seg_1821" n="HIAT:ip">,</nts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_1824" n="HIAT:w" s="T580">sɨn</ts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_1827" n="HIAT:w" s="T581">moj</ts>
                  <nts id="Seg_1828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_1830" n="HIAT:w" s="T582">u͡olum</ts>
                  <nts id="Seg_1831" n="HIAT:ip">.</nts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T607" id="Seg_1834" n="HIAT:u" s="T583">
                  <ts e="T584" id="Seg_1836" n="HIAT:w" s="T583">Onton</ts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_1839" n="HIAT:w" s="T584">tam</ts>
                  <nts id="Seg_1840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_1842" n="HIAT:w" s="T585">ješo</ts>
                  <nts id="Seg_1843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_1845" n="HIAT:w" s="T586">dogottoro</ts>
                  <nts id="Seg_1846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_1848" n="HIAT:w" s="T587">erim</ts>
                  <nts id="Seg_1849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_1851" n="HIAT:w" s="T588">sestratɨn</ts>
                  <nts id="Seg_1852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_1854" n="HIAT:w" s="T589">u͡ola</ts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_1857" n="HIAT:w" s="T590">ol</ts>
                  <nts id="Seg_1858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1859" n="HIAT:ip">(</nts>
                  <ts e="T592" id="Seg_1861" n="HIAT:w" s="T591">ka-</ts>
                  <nts id="Seg_1862" n="HIAT:ip">)</nts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_1865" n="HIAT:w" s="T592">u͡otu</ts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_1868" n="HIAT:w" s="T593">oŋoroːččular</ts>
                  <nts id="Seg_1869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_1871" n="HIAT:w" s="T594">buru͡o</ts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_1874" n="HIAT:w" s="T595">ke</ts>
                  <nts id="Seg_1875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_1877" n="HIAT:w" s="T596">iti</ts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_1880" n="HIAT:w" s="T597">štobɨ</ts>
                  <nts id="Seg_1881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_1883" n="HIAT:w" s="T598">komarɨ</ts>
                  <nts id="Seg_1884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_1886" n="HIAT:w" s="T599">kimneːmi͡ekterin</ts>
                  <nts id="Seg_1887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_1889" n="HIAT:w" s="T600">čugahaːmɨ͡aktarɨn</ts>
                  <nts id="Seg_1890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_1892" n="HIAT:w" s="T601">bihi͡eke</ts>
                  <nts id="Seg_1893" n="HIAT:ip">,</nts>
                  <nts id="Seg_1894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_1896" n="HIAT:w" s="T602">kogda</ts>
                  <nts id="Seg_1897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_1899" n="HIAT:w" s="T603">eto</ts>
                  <nts id="Seg_1900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_1902" n="HIAT:w" s="T604">mɨ</ts>
                  <nts id="Seg_1903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_1905" n="HIAT:w" s="T605">rɨbu</ts>
                  <nts id="Seg_1906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_1908" n="HIAT:w" s="T606">razdelajdɨːrbɨtɨgar</ts>
                  <nts id="Seg_1909" n="HIAT:ip">.</nts>
                  <nts id="Seg_1910" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T612" id="Seg_1911" n="sc" s="T608">
               <ts e="T612" id="Seg_1913" n="HIAT:u" s="T608">
                  <ts e="T609" id="Seg_1915" n="HIAT:w" s="T608">Tak</ts>
                  <nts id="Seg_1916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_1918" n="HIAT:w" s="T609">i</ts>
                  <nts id="Seg_1919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_1921" n="HIAT:w" s="T610">mɨ</ts>
                  <nts id="Seg_1922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_1924" n="HIAT:w" s="T611">žili</ts>
                  <nts id="Seg_1925" n="HIAT:ip">.</nts>
                  <nts id="Seg_1926" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T619" id="Seg_1927" n="sc" s="T613">
               <ts e="T619" id="Seg_1929" n="HIAT:u" s="T613">
                  <ts e="T614" id="Seg_1931" n="HIAT:w" s="T613">Potom</ts>
                  <nts id="Seg_1932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_1934" n="HIAT:w" s="T614">eto</ts>
                  <nts id="Seg_1935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_1937" n="HIAT:w" s="T615">što</ts>
                  <nts id="Seg_1938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_1940" n="HIAT:w" s="T616">ješo</ts>
                  <nts id="Seg_1941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_1943" n="HIAT:w" s="T617">tugu</ts>
                  <nts id="Seg_1944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_1946" n="HIAT:w" s="T618">kepsi͡emij</ts>
                  <nts id="Seg_1947" n="HIAT:ip">?</nts>
                  <nts id="Seg_1948" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T628" id="Seg_1949" n="sc" s="T620">
               <ts e="T628" id="Seg_1951" n="HIAT:u" s="T620">
                  <ts e="T621" id="Seg_1953" n="HIAT:w" s="T620">Kim</ts>
                  <nts id="Seg_1954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_1956" n="HIAT:w" s="T621">ol</ts>
                  <nts id="Seg_1957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_1959" n="HIAT:w" s="T622">saːdikka</ts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_1962" n="HIAT:w" s="T623">onton</ts>
                  <nts id="Seg_1963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_1965" n="HIAT:w" s="T624">iti</ts>
                  <nts id="Seg_1966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_1968" n="HIAT:w" s="T625">pensiaga</ts>
                  <nts id="Seg_1969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_1971" n="HIAT:w" s="T626">bardɨm</ts>
                  <nts id="Seg_1972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_1974" n="HIAT:w" s="T627">bu͡o</ts>
                  <nts id="Seg_1975" n="HIAT:ip">.</nts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T638" id="Seg_1977" n="sc" s="T629">
               <ts e="T638" id="Seg_1979" n="HIAT:u" s="T629">
                  <ts e="T630" id="Seg_1981" n="HIAT:w" s="T629">Tridtsat</ts>
                  <nts id="Seg_1982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_1984" n="HIAT:w" s="T630">otut</ts>
                  <nts id="Seg_1985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_1987" n="HIAT:w" s="T631">agɨs</ts>
                  <nts id="Seg_1988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_1990" n="HIAT:w" s="T632">dʼɨlɨ</ts>
                  <nts id="Seg_1991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_1993" n="HIAT:w" s="T633">otut</ts>
                  <nts id="Seg_1994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_1996" n="HIAT:w" s="T634">alta</ts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_1999" n="HIAT:w" s="T635">dʼɨlɨ</ts>
                  <nts id="Seg_2000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2002" n="HIAT:w" s="T636">üleleːbitim</ts>
                  <nts id="Seg_2003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2005" n="HIAT:w" s="T637">saːdikka</ts>
                  <nts id="Seg_2006" n="HIAT:ip">.</nts>
                  <nts id="Seg_2007" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T648" id="Seg_2008" n="sc" s="T639">
               <ts e="T648" id="Seg_2010" n="HIAT:u" s="T639">
                  <ts e="T640" id="Seg_2012" n="HIAT:w" s="T639">Maŋnajɨ</ts>
                  <nts id="Seg_2013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2015" n="HIAT:w" s="T640">šku͡ola</ts>
                  <nts id="Seg_2016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2018" n="HIAT:w" s="T641">šku͡ola</ts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2021" n="HIAT:w" s="T642">di͡et</ts>
                  <nts id="Seg_2022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2024" n="HIAT:w" s="T643">üleleːbitim</ts>
                  <nts id="Seg_2025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2027" n="HIAT:w" s="T644">povarga</ts>
                  <nts id="Seg_2028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2030" n="HIAT:w" s="T645">povarom</ts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2033" n="HIAT:w" s="T646">üleleːbitim</ts>
                  <nts id="Seg_2034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2036" n="HIAT:w" s="T647">povarga</ts>
                  <nts id="Seg_2037" n="HIAT:ip">.</nts>
                  <nts id="Seg_2038" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T662" id="Seg_2039" n="sc" s="T649">
               <ts e="T662" id="Seg_2041" n="HIAT:u" s="T649">
                  <ts e="T650" id="Seg_2043" n="HIAT:w" s="T649">Školaga</ts>
                  <nts id="Seg_2044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2046" n="HIAT:w" s="T650">onton</ts>
                  <nts id="Seg_2047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2049" n="HIAT:w" s="T651">saːdikkaga</ts>
                  <nts id="Seg_2050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2052" n="HIAT:w" s="T652">ɨlbɨttara</ts>
                  <nts id="Seg_2053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2055" n="HIAT:w" s="T653">pereveli</ts>
                  <nts id="Seg_2056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2058" n="HIAT:w" s="T654">eh</ts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2061" n="HIAT:w" s="T655">kimneːbittere</ts>
                  <nts id="Seg_2062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2064" n="HIAT:w" s="T656">egelbittere</ts>
                  <nts id="Seg_2065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2067" n="HIAT:w" s="T657">kim</ts>
                  <nts id="Seg_2068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2070" n="HIAT:w" s="T658">onton</ts>
                  <nts id="Seg_2071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2073" n="HIAT:w" s="T659">šku͡olattan</ts>
                  <nts id="Seg_2074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2076" n="HIAT:w" s="T660">saːdikka</ts>
                  <nts id="Seg_2077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2079" n="HIAT:w" s="T661">üleleːbitim</ts>
                  <nts id="Seg_2080" n="HIAT:ip">.</nts>
                  <nts id="Seg_2081" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T670" id="Seg_2082" n="sc" s="T663">
               <ts e="T670" id="Seg_2084" n="HIAT:u" s="T663">
                  <ts e="T664" id="Seg_2086" n="HIAT:w" s="T663">A</ts>
                  <nts id="Seg_2087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2089" n="HIAT:w" s="T664">sejčas</ts>
                  <nts id="Seg_2090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2092" n="HIAT:w" s="T665">pensiaga</ts>
                  <nts id="Seg_2093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2095" n="HIAT:w" s="T666">olorobun</ts>
                  <nts id="Seg_2096" n="HIAT:ip">,</nts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2099" n="HIAT:w" s="T667">vospitɨvaju</ts>
                  <nts id="Seg_2100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2102" n="HIAT:w" s="T668">vnuk</ts>
                  <nts id="Seg_2103" n="HIAT:ip">,</nts>
                  <nts id="Seg_2104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2106" n="HIAT:w" s="T669">vnuktarbɨn</ts>
                  <nts id="Seg_2107" n="HIAT:ip">.</nts>
                  <nts id="Seg_2108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T678" id="Seg_2109" n="sc" s="T671">
               <ts e="T678" id="Seg_2111" n="HIAT:u" s="T671">
                  <ts e="T672" id="Seg_2113" n="HIAT:w" s="T671">Pravnučkajbun</ts>
                  <nts id="Seg_2114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2116" n="HIAT:w" s="T672">sižu</ts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2119" n="HIAT:w" s="T673">tože</ts>
                  <nts id="Seg_2120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2122" n="HIAT:w" s="T674">hožu</ts>
                  <nts id="Seg_2123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2125" n="HIAT:w" s="T675">üleliːbin</ts>
                  <nts id="Seg_2126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2128" n="HIAT:w" s="T676">üleliː</ts>
                  <nts id="Seg_2129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2131" n="HIAT:w" s="T677">hɨldʼabɨn</ts>
                  <nts id="Seg_2132" n="HIAT:ip">.</nts>
                  <nts id="Seg_2133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T691" id="Seg_2134" n="sc" s="T679">
               <ts e="T691" id="Seg_2136" n="HIAT:u" s="T679">
                  <ts e="T680" id="Seg_2138" n="HIAT:w" s="T679">A</ts>
                  <nts id="Seg_2139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2141" n="HIAT:w" s="T680">nevestka</ts>
                  <nts id="Seg_2142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2144" n="HIAT:w" s="T681">na</ts>
                  <nts id="Seg_2145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2147" n="HIAT:w" s="T682">rabotu</ts>
                  <nts id="Seg_2148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2150" n="HIAT:w" s="T683">naːda</ts>
                  <nts id="Seg_2151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2153" n="HIAT:w" s="T684">idti</ts>
                  <nts id="Seg_2154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2156" n="HIAT:w" s="T685">a</ts>
                  <nts id="Seg_2157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2159" n="HIAT:w" s="T686">min</ts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2162" n="HIAT:w" s="T687">ol</ts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2165" n="HIAT:w" s="T688">ogonɨ</ts>
                  <nts id="Seg_2166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2168" n="HIAT:w" s="T689">gɨtta</ts>
                  <nts id="Seg_2169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2171" n="HIAT:w" s="T690">olorobun</ts>
                  <nts id="Seg_2172" n="HIAT:ip">.</nts>
                  <nts id="Seg_2173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T700" id="Seg_2174" n="sc" s="T692">
               <ts e="T700" id="Seg_2176" n="HIAT:u" s="T692">
                  <ts e="T693" id="Seg_2178" n="HIAT:w" s="T692">Ješo</ts>
                  <nts id="Seg_2179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2181" n="HIAT:w" s="T693">malenki͡e</ts>
                  <nts id="Seg_2182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2184" n="HIAT:w" s="T694">hüːrbeleːger</ts>
                  <nts id="Seg_2185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2187" n="HIAT:w" s="T695">dʼɨllaːnɨ͡aga</ts>
                  <nts id="Seg_2188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2190" n="HIAT:w" s="T696">markka</ts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2193" n="HIAT:w" s="T697">ol</ts>
                  <nts id="Seg_2194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2195" n="HIAT:ip">(</nts>
                  <ts e="T699" id="Seg_2197" n="HIAT:w" s="T698">vnu-</ts>
                  <nts id="Seg_2198" n="HIAT:ip">)</nts>
                  <nts id="Seg_2199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2201" n="HIAT:w" s="T699">vnučkam</ts>
                  <nts id="Seg_2202" n="HIAT:ip">.</nts>
                  <nts id="Seg_2203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T708" id="Seg_2204" n="sc" s="T701">
               <ts e="T708" id="Seg_2206" n="HIAT:u" s="T701">
                  <ts e="T702" id="Seg_2208" n="HIAT:w" s="T701">Sejčas</ts>
                  <nts id="Seg_2209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2211" n="HIAT:w" s="T702">što</ts>
                  <nts id="Seg_2212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2214" n="HIAT:w" s="T703">to</ts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2217" n="HIAT:w" s="T704">kimneːbit</ts>
                  <nts id="Seg_2218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2220" n="HIAT:w" s="T705">nemnožko</ts>
                  <nts id="Seg_2221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2223" n="HIAT:w" s="T706">ɨ͡aldʼabɨt</ts>
                  <nts id="Seg_2224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2226" n="HIAT:w" s="T707">stomatittammɨt</ts>
                  <nts id="Seg_2227" n="HIAT:ip">.</nts>
                  <nts id="Seg_2228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T713" id="Seg_2229" n="sc" s="T709">
               <ts e="T713" id="Seg_2231" n="HIAT:u" s="T709">
                  <ts e="T710" id="Seg_2233" n="HIAT:w" s="T709">Maːmata</ts>
                  <nts id="Seg_2234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2236" n="HIAT:w" s="T710">oloror</ts>
                  <nts id="Seg_2237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2239" n="HIAT:w" s="T711">na</ts>
                  <nts id="Seg_2240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2242" n="HIAT:w" s="T712">bolničnom</ts>
                  <nts id="Seg_2243" n="HIAT:ip">.</nts>
                  <nts id="Seg_2244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T752" id="Seg_2245" n="sc" s="T714">
               <ts e="T732" id="Seg_2247" n="HIAT:u" s="T714">
                  <ts e="T715" id="Seg_2249" n="HIAT:w" s="T714">Tak</ts>
                  <nts id="Seg_2250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2252" n="HIAT:w" s="T715">kim</ts>
                  <nts id="Seg_2253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2255" n="HIAT:w" s="T716">bu</ts>
                  <nts id="Seg_2256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2258" n="HIAT:w" s="T717">ulakan</ts>
                  <nts id="Seg_2259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2261" n="HIAT:w" s="T718">vnugum</ts>
                  <nts id="Seg_2262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2264" n="HIAT:w" s="T719">u͡olun</ts>
                  <nts id="Seg_2265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2267" n="HIAT:w" s="T720">gi͡ene</ts>
                  <nts id="Seg_2268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2270" n="HIAT:w" s="T721">ulakan</ts>
                  <nts id="Seg_2271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2273" n="HIAT:w" s="T722">kotorɨj</ts>
                  <nts id="Seg_2274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2276" n="HIAT:w" s="T723">eto</ts>
                  <nts id="Seg_2277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2279" n="HIAT:w" s="T724">staršij</ts>
                  <nts id="Seg_2280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2282" n="HIAT:w" s="T725">vnugum</ts>
                  <nts id="Seg_2283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2285" n="HIAT:w" s="T726">ulakan</ts>
                  <nts id="Seg_2286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2288" n="HIAT:w" s="T727">anɨ</ts>
                  <nts id="Seg_2289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2291" n="HIAT:w" s="T728">služittɨːr</ts>
                  <nts id="Seg_2292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2294" n="HIAT:w" s="T729">kimŋe</ts>
                  <nts id="Seg_2295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2297" n="HIAT:w" s="T730">armiaga</ts>
                  <nts id="Seg_2298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2300" n="HIAT:w" s="T731">služit</ts>
                  <nts id="Seg_2301" n="HIAT:ip">.</nts>
                  <nts id="Seg_2302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T733" id="Seg_2304" n="HIAT:u" s="T732">
                  <ts e="T835" id="Seg_2306" n="HIAT:w" s="T732">Aŋardar</ts>
                  <nts id="Seg_2307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2309" n="HIAT:w" s="T835">ü͡öreneller</ts>
                  <nts id="Seg_2310" n="HIAT:ip">.</nts>
                  <nts id="Seg_2311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T752" id="Seg_2313" n="HIAT:u" s="T733">
                  <ts e="T734" id="Seg_2315" n="HIAT:w" s="T733">A</ts>
                  <nts id="Seg_2316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2318" n="HIAT:w" s="T734">kɨːha</ts>
                  <nts id="Seg_2319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2321" n="HIAT:w" s="T735">kɨːhɨm</ts>
                  <nts id="Seg_2322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2324" n="HIAT:w" s="T736">vnučkam</ts>
                  <nts id="Seg_2325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2327" n="HIAT:w" s="T737">ulakan</ts>
                  <nts id="Seg_2328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2330" n="HIAT:w" s="T738">eto</ts>
                  <nts id="Seg_2331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2333" n="HIAT:w" s="T739">u͡olum</ts>
                  <nts id="Seg_2334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2335" n="HIAT:ip">(</nts>
                  <ts e="T741" id="Seg_2337" n="HIAT:w" s="T740">vnučk-</ts>
                  <nts id="Seg_2338" n="HIAT:ip">)</nts>
                  <nts id="Seg_2339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2341" n="HIAT:w" s="T741">kɨːha</ts>
                  <nts id="Seg_2342" n="HIAT:ip">,</nts>
                  <nts id="Seg_2343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2345" n="HIAT:w" s="T742">vnučkam</ts>
                  <nts id="Seg_2346" n="HIAT:ip">,</nts>
                  <nts id="Seg_2347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2349" n="HIAT:w" s="T743">Katja</ts>
                  <nts id="Seg_2350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2352" n="HIAT:w" s="T744">aːta</ts>
                  <nts id="Seg_2353" n="HIAT:ip">,</nts>
                  <nts id="Seg_2354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2356" n="HIAT:w" s="T745">iti</ts>
                  <nts id="Seg_2357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2359" n="HIAT:w" s="T746">Norilske</ts>
                  <nts id="Seg_2360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2362" n="HIAT:w" s="T747">kimŋe</ts>
                  <nts id="Seg_2363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2365" n="HIAT:w" s="T748">ü͡örener</ts>
                  <nts id="Seg_2366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2368" n="HIAT:w" s="T749">tože</ts>
                  <nts id="Seg_2369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2371" n="HIAT:w" s="T750">maːmatɨn</ts>
                  <nts id="Seg_2372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_2374" n="HIAT:w" s="T751">kördük</ts>
                  <nts id="Seg_2375" n="HIAT:ip">.</nts>
                  <nts id="Seg_2376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T768" id="Seg_2377" n="sc" s="T753">
               <ts e="T768" id="Seg_2379" n="HIAT:u" s="T753">
                  <ts e="T754" id="Seg_2381" n="HIAT:w" s="T753">Eto</ts>
                  <nts id="Seg_2382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_2384" n="HIAT:w" s="T754">Norilskaj</ts>
                  <nts id="Seg_2385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2387" n="HIAT:w" s="T755">kimŋe</ts>
                  <nts id="Seg_2388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2390" n="HIAT:w" s="T756">tʼexnʼikumŋa</ts>
                  <nts id="Seg_2391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_2393" n="HIAT:w" s="T757">ü͡örener</ts>
                  <nts id="Seg_2394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_2396" n="HIAT:w" s="T758">maːmatɨn</ts>
                  <nts id="Seg_2397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_2399" n="HIAT:w" s="T759">kördük</ts>
                  <nts id="Seg_2400" n="HIAT:ip">,</nts>
                  <nts id="Seg_2401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_2403" n="HIAT:w" s="T760">nu</ts>
                  <nts id="Seg_2404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_2406" n="HIAT:w" s="T761">na</ts>
                  <nts id="Seg_2407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_2409" n="HIAT:w" s="T762">šitjo</ts>
                  <nts id="Seg_2410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_2412" n="HIAT:w" s="T763">tam</ts>
                  <nts id="Seg_2413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2415" n="HIAT:w" s="T764">na</ts>
                  <nts id="Seg_2416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_2418" n="HIAT:w" s="T765">oformleni͡e</ts>
                  <nts id="Seg_2419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_2421" n="HIAT:w" s="T766">iti</ts>
                  <nts id="Seg_2422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_2424" n="HIAT:w" s="T767">kimŋe</ts>
                  <nts id="Seg_2425" n="HIAT:ip">.</nts>
                  <nts id="Seg_2426" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T787" id="Seg_2427" n="sc" s="T769">
               <ts e="T787" id="Seg_2429" n="HIAT:u" s="T769">
                  <ts e="T770" id="Seg_2431" n="HIAT:w" s="T769">Rodila</ts>
                  <nts id="Seg_2432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_2434" n="HIAT:w" s="T770">rebjonka</ts>
                  <nts id="Seg_2435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_2437" n="HIAT:w" s="T771">keːspite</ts>
                  <nts id="Seg_2438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_2440" n="HIAT:w" s="T772">bihi͡eke</ts>
                  <nts id="Seg_2441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_2443" n="HIAT:w" s="T773">i</ts>
                  <nts id="Seg_2444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_2446" n="HIAT:w" s="T774">pošla</ts>
                  <nts id="Seg_2447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2449" n="HIAT:w" s="T775">poehala</ts>
                  <nts id="Seg_2450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_2452" n="HIAT:w" s="T776">töttörü</ts>
                  <nts id="Seg_2453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_2455" n="HIAT:w" s="T777">büte</ts>
                  <nts id="Seg_2456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_2458" n="HIAT:w" s="T778">štob</ts>
                  <nts id="Seg_2459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_2461" n="HIAT:w" s="T779">dʼɨlɨn</ts>
                  <nts id="Seg_2462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_2464" n="HIAT:w" s="T780">hüterimeːri</ts>
                  <nts id="Seg_2465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_2467" n="HIAT:w" s="T781">ke</ts>
                  <nts id="Seg_2468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_2470" n="HIAT:w" s="T782">štobɨ</ts>
                  <nts id="Seg_2471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_2473" n="HIAT:w" s="T783">god</ts>
                  <nts id="Seg_2474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_2476" n="HIAT:w" s="T784">ne</ts>
                  <nts id="Seg_2477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_2479" n="HIAT:w" s="T785">propal</ts>
                  <nts id="Seg_2480" n="HIAT:ip">,</nts>
                  <nts id="Seg_2481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_2483" n="HIAT:w" s="T786">zakančivat</ts>
                  <nts id="Seg_2484" n="HIAT:ip">.</nts>
                  <nts id="Seg_2485" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T794" id="Seg_2486" n="sc" s="T788">
               <ts e="T794" id="Seg_2488" n="HIAT:u" s="T788">
                  <ts e="T789" id="Seg_2490" n="HIAT:w" s="T788">Ješo</ts>
                  <nts id="Seg_2491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_2493" n="HIAT:w" s="T789">dva</ts>
                  <nts id="Seg_2494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_2496" n="HIAT:w" s="T790">ikki</ts>
                  <nts id="Seg_2497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_2499" n="HIAT:w" s="T791">dʼɨlɨ</ts>
                  <nts id="Seg_2500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_2502" n="HIAT:w" s="T792">naːda</ts>
                  <nts id="Seg_2503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_2505" n="HIAT:w" s="T793">ü͡öreni͡ekke</ts>
                  <nts id="Seg_2506" n="HIAT:ip">.</nts>
                  <nts id="Seg_2507" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T807" id="Seg_2508" n="sc" s="T795">
               <ts e="T807" id="Seg_2510" n="HIAT:u" s="T795">
                  <ts e="T796" id="Seg_2512" n="HIAT:w" s="T795">Tri</ts>
                  <nts id="Seg_2513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_2515" n="HIAT:w" s="T796">goda</ts>
                  <nts id="Seg_2516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_2518" n="HIAT:w" s="T797">s</ts>
                  <nts id="Seg_2519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_2521" n="HIAT:w" s="T798">čem</ts>
                  <nts id="Seg_2522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_2524" n="HIAT:w" s="T799">to</ts>
                  <nts id="Seg_2525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_2527" n="HIAT:w" s="T800">üs</ts>
                  <nts id="Seg_2528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_2530" n="HIAT:w" s="T801">dʼɨl</ts>
                  <nts id="Seg_2531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_2533" n="HIAT:w" s="T802">s</ts>
                  <nts id="Seg_2534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_2536" n="HIAT:w" s="T803">čem</ts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_2539" n="HIAT:w" s="T804">to</ts>
                  <nts id="Seg_2540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_2542" n="HIAT:w" s="T805">üle</ts>
                  <nts id="Seg_2543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_2545" n="HIAT:w" s="T806">ü͡öreneller</ts>
                  <nts id="Seg_2546" n="HIAT:ip">.</nts>
                  <nts id="Seg_2547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T813" id="Seg_2548" n="sc" s="T808">
               <ts e="T813" id="Seg_2550" n="HIAT:u" s="T808">
                  <ts e="T809" id="Seg_2552" n="HIAT:w" s="T808">Vot</ts>
                  <nts id="Seg_2553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_2555" n="HIAT:w" s="T809">tak</ts>
                  <nts id="Seg_2556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_2558" n="HIAT:w" s="T810">vot</ts>
                  <nts id="Seg_2559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_2561" n="HIAT:w" s="T811">taki͡e</ts>
                  <nts id="Seg_2562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_2564" n="HIAT:w" s="T812">dela</ts>
                  <nts id="Seg_2565" n="HIAT:ip">.</nts>
                  <nts id="Seg_2566" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T826" id="Seg_2567" n="sc" s="T814">
               <ts e="T826" id="Seg_2569" n="HIAT:u" s="T814">
                  <ts e="T815" id="Seg_2571" n="HIAT:w" s="T814">Žhivu</ts>
                  <nts id="Seg_2572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_2574" n="HIAT:w" s="T815">odna</ts>
                  <nts id="Seg_2575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_2577" n="HIAT:w" s="T816">biːrges</ts>
                  <nts id="Seg_2578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_2580" n="HIAT:w" s="T817">vnukbɨn</ts>
                  <nts id="Seg_2581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2582" n="HIAT:ip">(</nts>
                  <ts e="T819" id="Seg_2584" n="HIAT:w" s="T818">gɨt-</ts>
                  <nts id="Seg_2585" n="HIAT:ip">)</nts>
                  <nts id="Seg_2586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_2588" n="HIAT:w" s="T819">kɨːhɨm</ts>
                  <nts id="Seg_2589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_2591" n="HIAT:w" s="T820">u͡olun</ts>
                  <nts id="Seg_2592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_2594" n="HIAT:w" s="T821">gɨtta</ts>
                  <nts id="Seg_2595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_2597" n="HIAT:w" s="T822">vnukbɨn</ts>
                  <nts id="Seg_2598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_2600" n="HIAT:w" s="T823">gɨtta</ts>
                  <nts id="Seg_2601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_2603" n="HIAT:w" s="T824">baːrbɨn</ts>
                  <nts id="Seg_2604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_2606" n="HIAT:w" s="T825">di͡eber</ts>
                  <nts id="Seg_2607" n="HIAT:ip">.</nts>
                  <nts id="Seg_2608" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T830" id="Seg_2609" n="sc" s="T827">
               <ts e="T830" id="Seg_2611" n="HIAT:u" s="T827">
                  <ts e="T828" id="Seg_2613" n="HIAT:w" s="T827">Hettis</ts>
                  <nts id="Seg_2614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_2616" n="HIAT:w" s="T828">kɨlaːska</ts>
                  <nts id="Seg_2617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_2619" n="HIAT:w" s="T829">ü͡örener</ts>
                  <nts id="Seg_2620" n="HIAT:ip">.</nts>
                  <nts id="Seg_2621" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T834" id="Seg_2622" n="sc" s="T831">
               <ts e="T834" id="Seg_2624" n="HIAT:u" s="T831">
                  <ts e="T832" id="Seg_2626" n="HIAT:w" s="T831">Ješo</ts>
                  <nts id="Seg_2627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_2629" n="HIAT:w" s="T832">što</ts>
                  <nts id="Seg_2630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_2632" n="HIAT:w" s="T833">govorit</ts>
                  <nts id="Seg_2633" n="HIAT:ip">.</nts>
                  <nts id="Seg_2634" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T7" id="Seg_2635" n="sc" s="T1">
               <ts e="T2" id="Seg_2637" n="e" s="T1">Min </ts>
               <ts e="T3" id="Seg_2639" n="e" s="T2">aːtɨm </ts>
               <ts e="T4" id="Seg_2641" n="e" s="T3">Iste, </ts>
               <ts e="T5" id="Seg_2643" n="e" s="T4">hakalɨː </ts>
               <ts e="T6" id="Seg_2645" n="e" s="T5">Iste </ts>
               <ts e="T7" id="Seg_2647" n="e" s="T6">di͡eččiler. </ts>
            </ts>
            <ts e="T12" id="Seg_2648" n="sc" s="T8">
               <ts e="T9" id="Seg_2650" n="e" s="T8">Töröːbütüm </ts>
               <ts e="T10" id="Seg_2652" n="e" s="T9">Popigaj </ts>
               <ts e="T11" id="Seg_2654" n="e" s="T10">di͡en </ts>
               <ts e="T12" id="Seg_2656" n="e" s="T11">hirge. </ts>
            </ts>
            <ts e="T22" id="Seg_2657" n="sc" s="T13">
               <ts e="T14" id="Seg_2659" n="e" s="T13">Min </ts>
               <ts e="T15" id="Seg_2661" n="e" s="T14">kergetterim </ts>
               <ts e="T16" id="Seg_2663" n="e" s="T15">teːtem </ts>
               <ts e="T17" id="Seg_2665" n="e" s="T16">kim, </ts>
               <ts e="T18" id="Seg_2667" n="e" s="T17">Nʼukulaj, </ts>
               <ts e="T19" id="Seg_2669" n="e" s="T18">maːmam </ts>
               <ts e="T20" id="Seg_2671" n="e" s="T19">eto </ts>
               <ts e="T21" id="Seg_2673" n="e" s="T20">Taiska, </ts>
               <ts e="T22" id="Seg_2675" n="e" s="T21">Taiska. </ts>
            </ts>
            <ts e="T33" id="Seg_2676" n="sc" s="T23">
               <ts e="T24" id="Seg_2678" n="e" s="T23">Ontuŋ </ts>
               <ts e="T25" id="Seg_2680" n="e" s="T24">maːmam </ts>
               <ts e="T26" id="Seg_2682" n="e" s="T25">erde </ts>
               <ts e="T27" id="Seg_2684" n="e" s="T26">bagajɨ </ts>
               <ts e="T28" id="Seg_2686" n="e" s="T27">ölbüte, </ts>
               <ts e="T29" id="Seg_2688" n="e" s="T28">onton </ts>
               <ts e="T30" id="Seg_2690" n="e" s="T29">min </ts>
               <ts e="T31" id="Seg_2692" n="e" s="T30">teːtebin </ts>
               <ts e="T32" id="Seg_2694" n="e" s="T31">gɨtta </ts>
               <ts e="T33" id="Seg_2696" n="e" s="T32">kaːlbɨtɨm. </ts>
            </ts>
            <ts e="T41" id="Seg_2697" n="sc" s="T34">
               <ts e="T35" id="Seg_2699" n="e" s="T34">Teːtem </ts>
               <ts e="T36" id="Seg_2701" n="e" s="T35">du͡o </ts>
               <ts e="T37" id="Seg_2703" n="e" s="T36">kimŋe </ts>
               <ts e="T38" id="Seg_2705" n="e" s="T37">pastuxga </ts>
               <ts e="T39" id="Seg_2707" n="e" s="T38">brigadiːrga </ts>
               <ts e="T40" id="Seg_2709" n="e" s="T39">üleliːr </ts>
               <ts e="T41" id="Seg_2711" n="e" s="T40">ete. </ts>
            </ts>
            <ts e="T48" id="Seg_2712" n="sc" s="T42">
               <ts e="T43" id="Seg_2714" n="e" s="T42">Onton </ts>
               <ts e="T44" id="Seg_2716" n="e" s="T43">bɨraːtɨm </ts>
               <ts e="T45" id="Seg_2718" n="e" s="T44">tože </ts>
               <ts e="T46" id="Seg_2720" n="e" s="T45">teːtebin </ts>
               <ts e="T47" id="Seg_2722" n="e" s="T46">gɨtta </ts>
               <ts e="T48" id="Seg_2724" n="e" s="T47">hɨldʼaːččɨ. </ts>
            </ts>
            <ts e="T53" id="Seg_2725" n="sc" s="T49">
               <ts e="T50" id="Seg_2727" n="e" s="T49">Tundraga </ts>
               <ts e="T51" id="Seg_2729" n="e" s="T50">bihigi </ts>
               <ts e="T52" id="Seg_2731" n="e" s="T51">köhö </ts>
               <ts e="T53" id="Seg_2733" n="e" s="T52">hɨldʼaːččɨbɨt. </ts>
            </ts>
            <ts e="T60" id="Seg_2734" n="sc" s="T54">
               <ts e="T55" id="Seg_2736" n="e" s="T54">Školatan </ts>
               <ts e="T56" id="Seg_2738" n="e" s="T55">kanikulbar </ts>
               <ts e="T57" id="Seg_2740" n="e" s="T56">kellekpine </ts>
               <ts e="T58" id="Seg_2742" n="e" s="T57">min </ts>
               <ts e="T59" id="Seg_2744" n="e" s="T58">teːteber </ts>
               <ts e="T60" id="Seg_2746" n="e" s="T59">bu͡olaːččɨbɨn. </ts>
            </ts>
            <ts e="T75" id="Seg_2747" n="sc" s="T61">
               <ts e="T62" id="Seg_2749" n="e" s="T61">No </ts>
               <ts e="T63" id="Seg_2751" n="e" s="T62">ulaːppɨtɨm </ts>
               <ts e="T64" id="Seg_2753" n="e" s="T63">kenne </ts>
               <ts e="T65" id="Seg_2755" n="e" s="T64">min </ts>
               <ts e="T66" id="Seg_2757" n="e" s="T65">teːteber </ts>
               <ts e="T67" id="Seg_2759" n="e" s="T66">bu͡olbutum, </ts>
               <ts e="T68" id="Seg_2761" n="e" s="T67">kogda </ts>
               <ts e="T69" id="Seg_2763" n="e" s="T68">min </ts>
               <ts e="T70" id="Seg_2765" n="e" s="T69">oččuguj </ts>
               <ts e="T71" id="Seg_2767" n="e" s="T70">erdekpine </ts>
               <ts e="T72" id="Seg_2769" n="e" s="T71">kihilerge </ts>
               <ts e="T73" id="Seg_2771" n="e" s="T72">iːtillibitim, </ts>
               <ts e="T74" id="Seg_2773" n="e" s="T73">atɨn </ts>
               <ts e="T75" id="Seg_2775" n="e" s="T74">kihilerge. </ts>
            </ts>
            <ts e="T87" id="Seg_2776" n="sc" s="T76">
               <ts e="T77" id="Seg_2778" n="e" s="T76">Iːtillibitim </ts>
               <ts e="T78" id="Seg_2780" n="e" s="T77">onton </ts>
               <ts e="T79" id="Seg_2782" n="e" s="T78">ulaːtan </ts>
               <ts e="T80" id="Seg_2784" n="e" s="T79">barammɨn </ts>
               <ts e="T81" id="Seg_2786" n="e" s="T80">ere </ts>
               <ts e="T82" id="Seg_2788" n="e" s="T81">teːteber </ts>
               <ts e="T83" id="Seg_2790" n="e" s="T82">bu͡olbutum </ts>
               <ts e="T84" id="Seg_2792" n="e" s="T83">baraːččɨbɨn </ts>
               <ts e="T85" id="Seg_2794" n="e" s="T84">teːteber </ts>
               <ts e="T86" id="Seg_2796" n="e" s="T85">kanikulga </ts>
               <ts e="T87" id="Seg_2798" n="e" s="T86">kellekpine. </ts>
            </ts>
            <ts e="T102" id="Seg_2799" n="sc" s="T88">
               <ts e="T89" id="Seg_2801" n="e" s="T88">De </ts>
               <ts e="T90" id="Seg_2803" n="e" s="T89">ol </ts>
               <ts e="T91" id="Seg_2805" n="e" s="T90">köhö </ts>
               <ts e="T92" id="Seg_2807" n="e" s="T91">hɨldʼaːččɨbɨt </ts>
               <ts e="T93" id="Seg_2809" n="e" s="T92">kimten </ts>
               <ts e="T94" id="Seg_2811" n="e" s="T93">mi͡esteten </ts>
               <ts e="T95" id="Seg_2813" n="e" s="T94">mi͡estege </ts>
               <ts e="T96" id="Seg_2815" n="e" s="T95">di͡eri </ts>
               <ts e="T97" id="Seg_2817" n="e" s="T96">na </ts>
               <ts e="T98" id="Seg_2819" n="e" s="T97">nartax </ts>
               <ts e="T99" id="Seg_2821" n="e" s="T98">na </ts>
               <ts e="T100" id="Seg_2823" n="e" s="T99">etix </ts>
               <ts e="T101" id="Seg_2825" n="e" s="T100">köhö </ts>
               <ts e="T102" id="Seg_2827" n="e" s="T101">hɨldʼaːččɨbɨt. </ts>
            </ts>
            <ts e="T122" id="Seg_2828" n="sc" s="T103">
               <ts e="T104" id="Seg_2830" n="e" s="T103">Čumŋa </ts>
               <ts e="T105" id="Seg_2832" n="e" s="T104">dʼi͡eleːk </ts>
               <ts e="T106" id="Seg_2834" n="e" s="T105">bu͡olaːččɨbɨt </ts>
               <ts e="T107" id="Seg_2836" n="e" s="T106">tam </ts>
               <ts e="T108" id="Seg_2838" n="e" s="T107">sneg </ts>
               <ts e="T109" id="Seg_2840" n="e" s="T108">eh </ts>
               <ts e="T110" id="Seg_2842" n="e" s="T109">kaːrɨ </ts>
               <ts e="T111" id="Seg_2844" n="e" s="T110">kimniːbit </ts>
               <ts e="T112" id="Seg_2846" n="e" s="T111">ubirajdɨːbɨt, </ts>
               <ts e="T113" id="Seg_2848" n="e" s="T112">potom </ts>
               <ts e="T114" id="Seg_2850" n="e" s="T113">mɨ </ts>
               <ts e="T115" id="Seg_2852" n="e" s="T114">stavim </ts>
               <ts e="T116" id="Seg_2854" n="e" s="T115">eti </ts>
               <ts e="T117" id="Seg_2856" n="e" s="T116">palki </ts>
               <ts e="T118" id="Seg_2858" n="e" s="T117">taki͡e </ts>
               <ts e="T119" id="Seg_2860" n="e" s="T118">dlinnɨe </ts>
               <ts e="T120" id="Seg_2862" n="e" s="T119">čum </ts>
               <ts e="T121" id="Seg_2864" n="e" s="T120">čum </ts>
               <ts e="T122" id="Seg_2866" n="e" s="T121">oŋorollor. </ts>
            </ts>
            <ts e="T131" id="Seg_2867" n="sc" s="T123">
               <ts e="T124" id="Seg_2869" n="e" s="T123">I </ts>
               <ts e="T125" id="Seg_2871" n="e" s="T124">eto </ts>
               <ts e="T126" id="Seg_2873" n="e" s="T125">i </ts>
               <ts e="T127" id="Seg_2875" n="e" s="T126">eto </ts>
               <ts e="T128" id="Seg_2877" n="e" s="T127">živem </ts>
               <ts e="T129" id="Seg_2879" n="e" s="T128">tam </ts>
               <ts e="T130" id="Seg_2881" n="e" s="T129">v </ts>
               <ts e="T131" id="Seg_2883" n="e" s="T130">čume. </ts>
            </ts>
            <ts e="T140" id="Seg_2884" n="sc" s="T132">
               <ts e="T133" id="Seg_2886" n="e" s="T132">Heː </ts>
               <ts e="T134" id="Seg_2888" n="e" s="T133">eto </ts>
               <ts e="T135" id="Seg_2890" n="e" s="T134">ja </ts>
               <ts e="T136" id="Seg_2892" n="e" s="T135">po </ts>
               <ts e="T137" id="Seg_2894" n="e" s="T136">russki </ts>
               <ts e="T138" id="Seg_2896" n="e" s="T137">uraha </ts>
               <ts e="T139" id="Seg_2898" n="e" s="T138">dʼi͡ege </ts>
               <ts e="T140" id="Seg_2900" n="e" s="T139">aha. </ts>
            </ts>
            <ts e="T147" id="Seg_2901" n="sc" s="T141">
               <ts e="T142" id="Seg_2903" n="e" s="T141">Urahalarɨ </ts>
               <ts e="T143" id="Seg_2905" n="e" s="T142">delajut </ts>
               <ts e="T144" id="Seg_2907" n="e" s="T143">kimten </ts>
               <ts e="T145" id="Seg_2909" n="e" s="T144">taki͡e </ts>
               <ts e="T146" id="Seg_2911" n="e" s="T145">dlinnɨe </ts>
               <ts e="T147" id="Seg_2913" n="e" s="T146">taki͡e. </ts>
            </ts>
            <ts e="T164" id="Seg_2914" n="sc" s="T148">
               <ts e="T149" id="Seg_2916" n="e" s="T148">Ol </ts>
               <ts e="T150" id="Seg_2918" n="e" s="T149">dʼi͡eŋ </ts>
               <ts e="T151" id="Seg_2920" n="e" s="T150">inogda </ts>
               <ts e="T152" id="Seg_2922" n="e" s="T151">ki͡eŋ </ts>
               <ts e="T153" id="Seg_2924" n="e" s="T152">bagajɨ </ts>
               <ts e="T154" id="Seg_2926" n="e" s="T153">oŋorollor </ts>
               <ts e="T155" id="Seg_2928" n="e" s="T154">ügüs </ts>
               <ts e="T156" id="Seg_2930" n="e" s="T155">kihi </ts>
               <ts e="T157" id="Seg_2932" n="e" s="T156">ol </ts>
               <ts e="T158" id="Seg_2934" n="e" s="T157">gɨnan </ts>
               <ts e="T159" id="Seg_2936" n="e" s="T158">ügüspüt </ts>
               <ts e="T160" id="Seg_2938" n="e" s="T159">mnogo </ts>
               <ts e="T161" id="Seg_2940" n="e" s="T160">že </ts>
               <ts e="T162" id="Seg_2942" n="e" s="T161">nam </ts>
               <ts e="T163" id="Seg_2944" n="e" s="T162">ügüspüt </ts>
               <ts e="T164" id="Seg_2946" n="e" s="T163">ved. </ts>
            </ts>
            <ts e="T171" id="Seg_2947" n="sc" s="T165">
               <ts e="T166" id="Seg_2949" n="e" s="T165">Širokij </ts>
               <ts e="T167" id="Seg_2951" n="e" s="T166">kim </ts>
               <ts e="T168" id="Seg_2953" n="e" s="T167">ki͡eŋ </ts>
               <ts e="T169" id="Seg_2955" n="e" s="T168">dʼi͡e </ts>
               <ts e="T170" id="Seg_2957" n="e" s="T169">ki͡eŋ </ts>
               <ts e="T171" id="Seg_2959" n="e" s="T170">dʼuka. </ts>
            </ts>
            <ts e="T182" id="Seg_2960" n="sc" s="T172">
               <ts e="T173" id="Seg_2962" n="e" s="T172">Potom </ts>
               <ts e="T174" id="Seg_2964" n="e" s="T173">pečkabɨt </ts>
               <ts e="T175" id="Seg_2966" n="e" s="T174">ohok </ts>
               <ts e="T176" id="Seg_2968" n="e" s="T175">timir </ts>
               <ts e="T177" id="Seg_2970" n="e" s="T176">ohok </ts>
               <ts e="T178" id="Seg_2972" n="e" s="T177">pečka </ts>
               <ts e="T179" id="Seg_2974" n="e" s="T178">srazu </ts>
               <ts e="T180" id="Seg_2976" n="e" s="T179">zatopljaem </ts>
               <ts e="T181" id="Seg_2978" n="e" s="T180">i </ts>
               <ts e="T182" id="Seg_2980" n="e" s="T181">teplo. </ts>
            </ts>
            <ts e="T200" id="Seg_2981" n="sc" s="T183">
               <ts e="T184" id="Seg_2983" n="e" s="T183">I </ts>
               <ts e="T185" id="Seg_2985" n="e" s="T184">eto </ts>
               <ts e="T186" id="Seg_2987" n="e" s="T185">vniz </ts>
               <ts e="T187" id="Seg_2989" n="e" s="T186">eto </ts>
               <ts e="T188" id="Seg_2991" n="e" s="T187">kimin </ts>
               <ts e="T189" id="Seg_2993" n="e" s="T188">annɨtɨn </ts>
               <ts e="T190" id="Seg_2995" n="e" s="T189">snegom </ts>
               <ts e="T191" id="Seg_2997" n="e" s="T190">kimniːller </ts>
               <ts e="T192" id="Seg_2999" n="e" s="T191">(battat-) </ts>
               <ts e="T193" id="Seg_3001" n="e" s="T192">battɨk </ts>
               <ts e="T194" id="Seg_3003" n="e" s="T193">oŋorollor </ts>
               <ts e="T195" id="Seg_3005" n="e" s="T194">battatallar </ts>
               <ts e="T196" id="Seg_3007" n="e" s="T195">i </ts>
               <ts e="T197" id="Seg_3009" n="e" s="T196">srazu </ts>
               <ts e="T198" id="Seg_3011" n="e" s="T197">štobɨ </ts>
               <ts e="T199" id="Seg_3013" n="e" s="T198">kimneːmi͡egin </ts>
               <ts e="T200" id="Seg_3015" n="e" s="T199">argɨjɨmɨ͡agɨn. </ts>
            </ts>
            <ts e="T214" id="Seg_3016" n="sc" s="T201">
               <ts e="T202" id="Seg_3018" n="e" s="T201">Daže </ts>
               <ts e="T203" id="Seg_3020" n="e" s="T202">oččuguj </ts>
               <ts e="T204" id="Seg_3022" n="e" s="T203">ogoloːk </ts>
               <ts e="T205" id="Seg_3024" n="e" s="T204">ezdittaːččɨbɨt </ts>
               <ts e="T206" id="Seg_3026" n="e" s="T205">tam </ts>
               <ts e="T207" id="Seg_3028" n="e" s="T206">ljulka </ts>
               <ts e="T208" id="Seg_3030" n="e" s="T207">tam </ts>
               <ts e="T209" id="Seg_3032" n="e" s="T208">u </ts>
               <ts e="T210" id="Seg_3034" n="e" s="T209">nix </ts>
               <ts e="T211" id="Seg_3036" n="e" s="T210">kim </ts>
               <ts e="T212" id="Seg_3038" n="e" s="T211">haka </ts>
               <ts e="T213" id="Seg_3040" n="e" s="T212">ljulkaga </ts>
               <ts e="T214" id="Seg_3042" n="e" s="T213">tuda. </ts>
            </ts>
            <ts e="T223" id="Seg_3043" n="sc" s="T215">
               <ts e="T216" id="Seg_3045" n="e" s="T215">Jejo </ts>
               <ts e="T217" id="Seg_3047" n="e" s="T216">položat </ts>
               <ts e="T218" id="Seg_3049" n="e" s="T217">eti </ts>
               <ts e="T219" id="Seg_3051" n="e" s="T218">rebjonka </ts>
               <ts e="T220" id="Seg_3053" n="e" s="T219">i </ts>
               <ts e="T221" id="Seg_3055" n="e" s="T220">ezdjat, </ts>
               <ts e="T222" id="Seg_3057" n="e" s="T221">bara </ts>
               <ts e="T223" id="Seg_3059" n="e" s="T222">hɨldʼabɨt. </ts>
            </ts>
            <ts e="T234" id="Seg_3060" n="sc" s="T224">
               <ts e="T225" id="Seg_3062" n="e" s="T224">Onton </ts>
               <ts e="T226" id="Seg_3064" n="e" s="T225">tugu </ts>
               <ts e="T227" id="Seg_3066" n="e" s="T226">kepsi͡emij </ts>
               <ts e="T228" id="Seg_3068" n="e" s="T227">teːtem </ts>
               <ts e="T229" id="Seg_3070" n="e" s="T228">du </ts>
               <ts e="T230" id="Seg_3072" n="e" s="T229">Kamennɨj </ts>
               <ts e="T231" id="Seg_3074" n="e" s="T230">kimŋe </ts>
               <ts e="T232" id="Seg_3076" n="e" s="T231">storonaga </ts>
               <ts e="T233" id="Seg_3078" n="e" s="T232">hɨldʼɨbɨta </ts>
               <ts e="T234" id="Seg_3080" n="e" s="T233">Kamennɨj. </ts>
            </ts>
            <ts e="T248" id="Seg_3081" n="sc" s="T235">
               <ts e="T236" id="Seg_3083" n="e" s="T235">Tam </ts>
               <ts e="T237" id="Seg_3085" n="e" s="T236">netu </ts>
               <ts e="T238" id="Seg_3087" n="e" s="T237">kim </ts>
               <ts e="T239" id="Seg_3089" n="e" s="T238">kaːs </ts>
               <ts e="T240" id="Seg_3091" n="e" s="T239">da </ts>
               <ts e="T241" id="Seg_3093" n="e" s="T240">hu͡ok </ts>
               <ts e="T242" id="Seg_3095" n="e" s="T241">utok </ts>
               <ts e="T243" id="Seg_3097" n="e" s="T242">netu </ts>
               <ts e="T244" id="Seg_3099" n="e" s="T243">tam </ts>
               <ts e="T245" id="Seg_3101" n="e" s="T244">etix </ts>
               <ts e="T246" id="Seg_3103" n="e" s="T245">tolko </ts>
               <ts e="T247" id="Seg_3105" n="e" s="T246">odni </ts>
               <ts e="T248" id="Seg_3107" n="e" s="T247">oleni. </ts>
            </ts>
            <ts e="T261" id="Seg_3108" n="sc" s="T249">
               <ts e="T250" id="Seg_3110" n="e" s="T249">Tam </ts>
               <ts e="T251" id="Seg_3112" n="e" s="T250">kamennɨe </ts>
               <ts e="T252" id="Seg_3114" n="e" s="T251">mɨšɨ </ts>
               <ts e="T253" id="Seg_3116" n="e" s="T252">redko </ts>
               <ts e="T254" id="Seg_3118" n="e" s="T253">kim </ts>
               <ts e="T255" id="Seg_3120" n="e" s="T254">kutujaktar </ts>
               <ts e="T256" id="Seg_3122" n="e" s="T255">kamen </ts>
               <ts e="T257" id="Seg_3124" n="e" s="T256">kamen </ts>
               <ts e="T258" id="Seg_3126" n="e" s="T257">pod </ts>
               <ts e="T259" id="Seg_3128" n="e" s="T258">kamjem </ts>
               <ts e="T260" id="Seg_3130" n="e" s="T259">inogda </ts>
               <ts e="T261" id="Seg_3132" n="e" s="T260">bɨvaet. </ts>
            </ts>
            <ts e="T274" id="Seg_3133" n="sc" s="T262">
               <ts e="T263" id="Seg_3135" n="e" s="T262">Ješo </ts>
               <ts e="T264" id="Seg_3137" n="e" s="T263">tuːguj </ts>
               <ts e="T265" id="Seg_3139" n="e" s="T264">kim </ts>
               <ts e="T266" id="Seg_3141" n="e" s="T265">min </ts>
               <ts e="T267" id="Seg_3143" n="e" s="T266">bejem </ts>
               <ts e="T268" id="Seg_3145" n="e" s="T267">ü͡öremmitim </ts>
               <ts e="T269" id="Seg_3147" n="e" s="T268">hette </ts>
               <ts e="T270" id="Seg_3149" n="e" s="T269">kɨlaːska </ts>
               <ts e="T271" id="Seg_3151" n="e" s="T270">di͡eri </ts>
               <ts e="T272" id="Seg_3153" n="e" s="T271">agsɨs </ts>
               <ts e="T273" id="Seg_3155" n="e" s="T272">kɨlaːstan </ts>
               <ts e="T274" id="Seg_3157" n="e" s="T273">büppütüm. </ts>
            </ts>
            <ts e="T280" id="Seg_3158" n="sc" s="T275">
               <ts e="T276" id="Seg_3160" n="e" s="T275">Kimi </ts>
               <ts e="T277" id="Seg_3162" n="e" s="T276">internaːkka </ts>
               <ts e="T278" id="Seg_3164" n="e" s="T277">onton </ts>
               <ts e="T279" id="Seg_3166" n="e" s="T278">internaːppɨt </ts>
               <ts e="T280" id="Seg_3168" n="e" s="T279">bu͡olaːččɨbɨt. </ts>
            </ts>
            <ts e="T289" id="Seg_3169" n="sc" s="T281">
               <ts e="T282" id="Seg_3171" n="e" s="T281">Ikki </ts>
               <ts e="T283" id="Seg_3173" n="e" s="T282">dʼɨlɨ </ts>
               <ts e="T284" id="Seg_3175" n="e" s="T283">Norilskaga </ts>
               <ts e="T285" id="Seg_3177" n="e" s="T284">ü͡öremmippit </ts>
               <ts e="T286" id="Seg_3179" n="e" s="T285">internaːkka </ts>
               <ts e="T287" id="Seg_3181" n="e" s="T286">(ž-) </ts>
               <ts e="T288" id="Seg_3183" n="e" s="T287">kim </ts>
               <ts e="T289" id="Seg_3185" n="e" s="T288">olorbupput. </ts>
            </ts>
            <ts e="T296" id="Seg_3186" n="sc" s="T290">
               <ts e="T291" id="Seg_3188" n="e" s="T290">Lagerga </ts>
               <ts e="T292" id="Seg_3190" n="e" s="T291">hɨldʼɨbɨtɨm </ts>
               <ts e="T293" id="Seg_3192" n="e" s="T292">kimŋe </ts>
               <ts e="T294" id="Seg_3194" n="e" s="T293">Tajožnajga </ts>
               <ts e="T295" id="Seg_3196" n="e" s="T294">hɨldʼɨbɨtɨm </ts>
               <ts e="T296" id="Seg_3198" n="e" s="T295">Kurejkaga. </ts>
            </ts>
            <ts e="T306" id="Seg_3199" n="sc" s="T297">
               <ts e="T298" id="Seg_3201" n="e" s="T297">Potom </ts>
               <ts e="T299" id="Seg_3203" n="e" s="T298">ulaːtan </ts>
               <ts e="T300" id="Seg_3205" n="e" s="T299">baran </ts>
               <ts e="T301" id="Seg_3207" n="e" s="T300">kimŋe </ts>
               <ts e="T302" id="Seg_3209" n="e" s="T301">(ü͡ör-) </ts>
               <ts e="T303" id="Seg_3211" n="e" s="T302">eː </ts>
               <ts e="T304" id="Seg_3213" n="e" s="T303">ol </ts>
               <ts e="T305" id="Seg_3215" n="e" s="T304">ü͡örene </ts>
               <ts e="T306" id="Seg_3217" n="e" s="T305">barbɨtɨm. </ts>
            </ts>
            <ts e="T312" id="Seg_3218" n="sc" s="T307">
               <ts e="T308" id="Seg_3220" n="e" s="T307">Na </ts>
               <ts e="T309" id="Seg_3222" n="e" s="T308">eto </ts>
               <ts e="T310" id="Seg_3224" n="e" s="T309">na </ts>
               <ts e="T311" id="Seg_3226" n="e" s="T310">kursɨ </ts>
               <ts e="T312" id="Seg_3228" n="e" s="T311">kimŋe. </ts>
            </ts>
            <ts e="T321" id="Seg_3229" n="sc" s="T313">
               <ts e="T314" id="Seg_3231" n="e" s="T313">Atɨːlɨːr </ts>
               <ts e="T315" id="Seg_3233" n="e" s="T314">kihi </ts>
               <ts e="T316" id="Seg_3235" n="e" s="T315">ke </ts>
               <ts e="T317" id="Seg_3237" n="e" s="T316">kimŋe </ts>
               <ts e="T318" id="Seg_3239" n="e" s="T317">prodaveska </ts>
               <ts e="T319" id="Seg_3241" n="e" s="T318">barbɨtɨm </ts>
               <ts e="T320" id="Seg_3243" n="e" s="T319">ü͡örene </ts>
               <ts e="T321" id="Seg_3245" n="e" s="T320">Dudinkaga. </ts>
            </ts>
            <ts e="T338" id="Seg_3246" n="sc" s="T322">
               <ts e="T323" id="Seg_3248" n="e" s="T322">Alta </ts>
               <ts e="T324" id="Seg_3250" n="e" s="T323">ɨjɨ </ts>
               <ts e="T325" id="Seg_3252" n="e" s="T324">ü͡öremmitim </ts>
               <ts e="T326" id="Seg_3254" n="e" s="T325">onton </ts>
               <ts e="T327" id="Seg_3256" n="e" s="T326">kelbitim </ts>
               <ts e="T328" id="Seg_3258" n="e" s="T327">i </ts>
               <ts e="T329" id="Seg_3260" n="e" s="T328">Nosku͡oga </ts>
               <ts e="T330" id="Seg_3262" n="e" s="T329">(nemno-) </ts>
               <ts e="T331" id="Seg_3264" n="e" s="T330">kimneːbitim </ts>
               <ts e="T332" id="Seg_3266" n="e" s="T331">praktikanɨ </ts>
               <ts e="T333" id="Seg_3268" n="e" s="T332">proxodili </ts>
               <ts e="T334" id="Seg_3270" n="e" s="T333">i </ts>
               <ts e="T335" id="Seg_3272" n="e" s="T334">tam </ts>
               <ts e="T336" id="Seg_3274" n="e" s="T335">kimŋe </ts>
               <ts e="T337" id="Seg_3276" n="e" s="T336">bufekka </ts>
               <ts e="T338" id="Seg_3278" n="e" s="T337">üleleːččibin. </ts>
            </ts>
            <ts e="T347" id="Seg_3279" n="sc" s="T339">
               <ts e="T340" id="Seg_3281" n="e" s="T339">Stolovojga </ts>
               <ts e="T341" id="Seg_3283" n="e" s="T340">bufet </ts>
               <ts e="T342" id="Seg_3285" n="e" s="T341">ete </ts>
               <ts e="T343" id="Seg_3287" n="e" s="T342">Nosku͡oga </ts>
               <ts e="T344" id="Seg_3289" n="e" s="T343">onno </ts>
               <ts e="T345" id="Seg_3291" n="e" s="T344">etot </ts>
               <ts e="T346" id="Seg_3293" n="e" s="T345">bufekka </ts>
               <ts e="T347" id="Seg_3295" n="e" s="T346">üleleːččibin. </ts>
            </ts>
            <ts e="T360" id="Seg_3296" n="sc" s="T348">
               <ts e="T349" id="Seg_3298" n="e" s="T348">Onton </ts>
               <ts e="T350" id="Seg_3300" n="e" s="T349">v </ts>
               <ts e="T351" id="Seg_3302" n="e" s="T350">etom </ts>
               <ts e="T352" id="Seg_3304" n="e" s="T351">Nosku͡oga </ts>
               <ts e="T353" id="Seg_3306" n="e" s="T352">biːr </ts>
               <ts e="T354" id="Seg_3308" n="e" s="T353">kihini </ts>
               <ts e="T355" id="Seg_3310" n="e" s="T354">munna </ts>
               <ts e="T356" id="Seg_3312" n="e" s="T355">Ki͡etaːtan </ts>
               <ts e="T357" id="Seg_3314" n="e" s="T356">biːr </ts>
               <ts e="T358" id="Seg_3316" n="e" s="T357">u͡olu </ts>
               <ts e="T359" id="Seg_3318" n="e" s="T358">gɨtta </ts>
               <ts e="T360" id="Seg_3320" n="e" s="T359">dogordosputum. </ts>
            </ts>
            <ts e="T368" id="Seg_3321" n="sc" s="T361">
               <ts e="T362" id="Seg_3323" n="e" s="T361">I </ts>
               <ts e="T363" id="Seg_3325" n="e" s="T362">mɨ </ts>
               <ts e="T364" id="Seg_3327" n="e" s="T363">tam </ts>
               <ts e="T365" id="Seg_3329" n="e" s="T364">poženilis, </ts>
               <ts e="T366" id="Seg_3331" n="e" s="T365">poznakomilis </ts>
               <ts e="T367" id="Seg_3333" n="e" s="T366">i </ts>
               <ts e="T368" id="Seg_3335" n="e" s="T367">poženilis. </ts>
            </ts>
            <ts e="T377" id="Seg_3336" n="sc" s="T369">
               <ts e="T370" id="Seg_3338" n="e" s="T369">Onton </ts>
               <ts e="T371" id="Seg_3340" n="e" s="T370">ɨlsan </ts>
               <ts e="T372" id="Seg_3342" n="e" s="T371">barammɨt </ts>
               <ts e="T373" id="Seg_3344" n="e" s="T372">to </ts>
               <ts e="T374" id="Seg_3346" n="e" s="T373">kimneːbippit </ts>
               <ts e="T375" id="Seg_3348" n="e" s="T374">Nosku͡oga </ts>
               <ts e="T376" id="Seg_3350" n="e" s="T375">oloro </ts>
               <ts e="T377" id="Seg_3352" n="e" s="T376">tüspütüm. </ts>
            </ts>
            <ts e="T390" id="Seg_3353" n="sc" s="T378">
               <ts e="T379" id="Seg_3355" n="e" s="T378">Min </ts>
               <ts e="T380" id="Seg_3357" n="e" s="T379">saːdikka </ts>
               <ts e="T381" id="Seg_3359" n="e" s="T380">üleliːr </ts>
               <ts e="T382" id="Seg_3361" n="e" s="T381">etim </ts>
               <ts e="T383" id="Seg_3363" n="e" s="T382">njanjaga </ts>
               <ts e="T384" id="Seg_3365" n="e" s="T383">a </ts>
               <ts e="T385" id="Seg_3367" n="e" s="T384">giniŋ </ts>
               <ts e="T386" id="Seg_3369" n="e" s="T385">du͡o </ts>
               <ts e="T387" id="Seg_3371" n="e" s="T386">počtaga </ts>
               <ts e="T388" id="Seg_3373" n="e" s="T387">montjorom </ts>
               <ts e="T389" id="Seg_3375" n="e" s="T388">üleliːr </ts>
               <ts e="T390" id="Seg_3377" n="e" s="T389">ete. </ts>
            </ts>
            <ts e="T403" id="Seg_3378" n="sc" s="T391">
               <ts e="T392" id="Seg_3380" n="e" s="T391">Onton </ts>
               <ts e="T393" id="Seg_3382" n="e" s="T392">u </ts>
               <ts e="T394" id="Seg_3384" n="e" s="T393">nas </ts>
               <ts e="T395" id="Seg_3386" n="e" s="T394">rodilsja </ts>
               <ts e="T396" id="Seg_3388" n="e" s="T395">vot </ts>
               <ts e="T397" id="Seg_3390" n="e" s="T396">kim </ts>
               <ts e="T398" id="Seg_3392" n="e" s="T397">kɨːspɨt </ts>
               <ts e="T399" id="Seg_3394" n="e" s="T398">kɨːhɨm </ts>
               <ts e="T400" id="Seg_3396" n="e" s="T399">Riːta </ts>
               <ts e="T401" id="Seg_3398" n="e" s="T400">aːttaːk </ts>
               <ts e="T402" id="Seg_3400" n="e" s="T401">kɨːhɨm </ts>
               <ts e="T403" id="Seg_3402" n="e" s="T402">u͡olum. </ts>
            </ts>
            <ts e="T414" id="Seg_3403" n="sc" s="T404">
               <ts e="T405" id="Seg_3405" n="e" s="T404">Onton </ts>
               <ts e="T406" id="Seg_3407" n="e" s="T405">on </ts>
               <ts e="T407" id="Seg_3409" n="e" s="T406">što </ts>
               <ts e="T408" id="Seg_3411" n="e" s="T407">to </ts>
               <ts e="T409" id="Seg_3413" n="e" s="T408">zaxotel </ts>
               <ts e="T410" id="Seg_3415" n="e" s="T409">(bɨt-) </ts>
               <ts e="T411" id="Seg_3417" n="e" s="T410">hiriger </ts>
               <ts e="T412" id="Seg_3419" n="e" s="T411">keli͡egin </ts>
               <ts e="T413" id="Seg_3421" n="e" s="T412">rodinatɨgar </ts>
               <ts e="T414" id="Seg_3423" n="e" s="T413">keli͡egin. </ts>
            </ts>
            <ts e="T422" id="Seg_3424" n="sc" s="T415">
               <ts e="T416" id="Seg_3426" n="e" s="T415">I </ts>
               <ts e="T417" id="Seg_3428" n="e" s="T416">on </ts>
               <ts e="T418" id="Seg_3430" n="e" s="T417">menja </ts>
               <ts e="T419" id="Seg_3432" n="e" s="T418">snačala </ts>
               <ts e="T420" id="Seg_3434" n="e" s="T419">menja </ts>
               <ts e="T421" id="Seg_3436" n="e" s="T420">otpravil </ts>
               <ts e="T422" id="Seg_3438" n="e" s="T421">ɨːppɨta. </ts>
            </ts>
            <ts e="T428" id="Seg_3439" n="sc" s="T423">
               <ts e="T424" id="Seg_3441" n="e" s="T423">Ogobun </ts>
               <ts e="T425" id="Seg_3443" n="e" s="T424">gɨtta </ts>
               <ts e="T426" id="Seg_3445" n="e" s="T425">ke </ts>
               <ts e="T427" id="Seg_3447" n="e" s="T426">kɨːspɨn </ts>
               <ts e="T428" id="Seg_3449" n="e" s="T427">gɨtta. </ts>
            </ts>
            <ts e="T438" id="Seg_3450" n="sc" s="T429">
               <ts e="T430" id="Seg_3452" n="e" s="T429">I </ts>
               <ts e="T431" id="Seg_3454" n="e" s="T430">potom </ts>
               <ts e="T432" id="Seg_3456" n="e" s="T431">tolko </ts>
               <ts e="T433" id="Seg_3458" n="e" s="T432">gini </ts>
               <ts e="T434" id="Seg_3460" n="e" s="T433">kennige </ts>
               <ts e="T435" id="Seg_3462" n="e" s="T434">min </ts>
               <ts e="T436" id="Seg_3464" n="e" s="T435">kennibitten </ts>
               <ts e="T437" id="Seg_3466" n="e" s="T436">gini </ts>
               <ts e="T438" id="Seg_3468" n="e" s="T437">kelbite. </ts>
            </ts>
            <ts e="T456" id="Seg_3469" n="sc" s="T439">
               <ts e="T440" id="Seg_3471" n="e" s="T439">I </ts>
               <ts e="T441" id="Seg_3473" n="e" s="T440">vot </ts>
               <ts e="T442" id="Seg_3475" n="e" s="T441">s </ts>
               <ts e="T443" id="Seg_3477" n="e" s="T442">tex </ts>
               <ts e="T444" id="Seg_3479" n="e" s="T443">por </ts>
               <ts e="T445" id="Seg_3481" n="e" s="T444">kas </ts>
               <ts e="T446" id="Seg_3483" n="e" s="T445">dʼɨl </ts>
               <ts e="T447" id="Seg_3485" n="e" s="T446">bu͡olla </ts>
               <ts e="T448" id="Seg_3487" n="e" s="T447">ke </ts>
               <ts e="T449" id="Seg_3489" n="e" s="T448">šest </ts>
               <ts e="T450" id="Seg_3491" n="e" s="T449">šestdesjat </ts>
               <ts e="T451" id="Seg_3493" n="e" s="T450">pervɨj </ts>
               <ts e="T452" id="Seg_3495" n="e" s="T451">gottan </ts>
               <ts e="T453" id="Seg_3497" n="e" s="T452">vot </ts>
               <ts e="T454" id="Seg_3499" n="e" s="T453">ja </ts>
               <ts e="T455" id="Seg_3501" n="e" s="T454">živu </ts>
               <ts e="T456" id="Seg_3503" n="e" s="T455">zdes. </ts>
            </ts>
            <ts e="T466" id="Seg_3504" n="sc" s="T457">
               <ts e="T458" id="Seg_3506" n="e" s="T457">I </ts>
               <ts e="T459" id="Seg_3508" n="e" s="T458">on </ts>
               <ts e="T460" id="Seg_3510" n="e" s="T459">umer </ts>
               <ts e="T461" id="Seg_3512" n="e" s="T460">gde </ts>
               <ts e="T462" id="Seg_3514" n="e" s="T461">to </ts>
               <ts e="T463" id="Seg_3516" n="e" s="T462">semdesjat </ts>
               <ts e="T464" id="Seg_3518" n="e" s="T463">devjatɨj </ts>
               <ts e="T465" id="Seg_3520" n="e" s="T464">gokka </ts>
               <ts e="T466" id="Seg_3522" n="e" s="T465">ölbüte. </ts>
            </ts>
            <ts e="T481" id="Seg_3523" n="sc" s="T467">
               <ts e="T468" id="Seg_3525" n="e" s="T467">I </ts>
               <ts e="T469" id="Seg_3527" n="e" s="T468">s </ts>
               <ts e="T470" id="Seg_3529" n="e" s="T469">tex </ts>
               <ts e="T471" id="Seg_3531" n="e" s="T470">por </ts>
               <ts e="T472" id="Seg_3533" n="e" s="T471">zdes </ts>
               <ts e="T473" id="Seg_3535" n="e" s="T472">min </ts>
               <ts e="T474" id="Seg_3537" n="e" s="T473">ogolommutum </ts>
               <ts e="T475" id="Seg_3539" n="e" s="T474">u͡olbun </ts>
               <ts e="T476" id="Seg_3541" n="e" s="T475">kotorɨj </ts>
               <ts e="T477" id="Seg_3543" n="e" s="T476">služil </ts>
               <ts e="T478" id="Seg_3545" n="e" s="T477">eto </ts>
               <ts e="T479" id="Seg_3547" n="e" s="T478">učilsja </ts>
               <ts e="T480" id="Seg_3549" n="e" s="T479">v </ts>
               <ts e="T481" id="Seg_3551" n="e" s="T480">Irkutske. </ts>
            </ts>
            <ts e="T484" id="Seg_3552" n="sc" s="T482">
               <ts e="T483" id="Seg_3554" n="e" s="T482">Služil </ts>
               <ts e="T484" id="Seg_3556" n="e" s="T483">Usurijskajga. </ts>
            </ts>
            <ts e="T502" id="Seg_3557" n="sc" s="T485">
               <ts e="T486" id="Seg_3559" n="e" s="T485">Nu </ts>
               <ts e="T487" id="Seg_3561" n="e" s="T486">tak </ts>
               <ts e="T488" id="Seg_3563" n="e" s="T487">i </ts>
               <ts e="T489" id="Seg_3565" n="e" s="T488">s </ts>
               <ts e="T490" id="Seg_3567" n="e" s="T489">tex </ts>
               <ts e="T491" id="Seg_3569" n="e" s="T490">por </ts>
               <ts e="T492" id="Seg_3571" n="e" s="T491">živu </ts>
               <ts e="T493" id="Seg_3573" n="e" s="T492">olorobun </ts>
               <ts e="T494" id="Seg_3575" n="e" s="T493">saːdikka </ts>
               <ts e="T495" id="Seg_3577" n="e" s="T494">üleleːbitim </ts>
               <ts e="T496" id="Seg_3579" n="e" s="T495">do </ts>
               <ts e="T497" id="Seg_3581" n="e" s="T496">pensia </ts>
               <ts e="T498" id="Seg_3583" n="e" s="T497">uže </ts>
               <ts e="T499" id="Seg_3585" n="e" s="T498">saːdikka </ts>
               <ts e="T500" id="Seg_3587" n="e" s="T499">hubu </ts>
               <ts e="T501" id="Seg_3589" n="e" s="T500">že </ts>
               <ts e="T502" id="Seg_3591" n="e" s="T501">saːdikka. </ts>
            </ts>
            <ts e="T518" id="Seg_3592" n="sc" s="T503">
               <ts e="T504" id="Seg_3594" n="e" s="T503">Onton </ts>
               <ts e="T505" id="Seg_3596" n="e" s="T504">tugu </ts>
               <ts e="T506" id="Seg_3598" n="e" s="T505">kepsi͡emij </ts>
               <ts e="T507" id="Seg_3600" n="e" s="T506">onton </ts>
               <ts e="T508" id="Seg_3602" n="e" s="T507">erim </ts>
               <ts e="T509" id="Seg_3604" n="e" s="T508">du͡o </ts>
               <ts e="T510" id="Seg_3606" n="e" s="T509">ol </ts>
               <ts e="T511" id="Seg_3608" n="e" s="T510">erim </ts>
               <ts e="T512" id="Seg_3610" n="e" s="T511">rɨbalkaga </ts>
               <ts e="T513" id="Seg_3612" n="e" s="T512">bu͡olaːččɨ </ts>
               <ts e="T514" id="Seg_3614" n="e" s="T513">itinne </ts>
               <ts e="T515" id="Seg_3616" n="e" s="T514">(to-) </ts>
               <ts e="T516" id="Seg_3618" n="e" s="T515">točkaga </ts>
               <ts e="T517" id="Seg_3620" n="e" s="T516">oloroːččubut </ts>
               <ts e="T518" id="Seg_3622" n="e" s="T517">čugas. </ts>
            </ts>
            <ts e="T528" id="Seg_3623" n="sc" s="T519">
               <ts e="T520" id="Seg_3625" n="e" s="T519">Kogda </ts>
               <ts e="T521" id="Seg_3627" n="e" s="T520">u </ts>
               <ts e="T522" id="Seg_3629" n="e" s="T521">menja </ts>
               <ts e="T523" id="Seg_3631" n="e" s="T522">vɨxodnɨe </ts>
               <ts e="T524" id="Seg_3633" n="e" s="T523">vɨxodnojdarbar </ts>
               <ts e="T525" id="Seg_3635" n="e" s="T524">gini͡eke </ts>
               <ts e="T526" id="Seg_3637" n="e" s="T525">baraːččɨbɨn </ts>
               <ts e="T527" id="Seg_3639" n="e" s="T526">motorunan </ts>
               <ts e="T528" id="Seg_3641" n="e" s="T527">ezditaːččɨbɨt. </ts>
            </ts>
            <ts e="T542" id="Seg_3642" n="sc" s="T529">
               <ts e="T530" id="Seg_3644" n="e" s="T529">I </ts>
               <ts e="T531" id="Seg_3646" n="e" s="T530">tam </ts>
               <ts e="T532" id="Seg_3648" n="e" s="T531">ja </ts>
               <ts e="T533" id="Seg_3650" n="e" s="T532">kim </ts>
               <ts e="T534" id="Seg_3652" n="e" s="T533">kömölöhöːččübün </ts>
               <ts e="T535" id="Seg_3654" n="e" s="T534">rɨbu </ts>
               <ts e="T536" id="Seg_3656" n="e" s="T535">razdel </ts>
               <ts e="T537" id="Seg_3658" n="e" s="T536">kimni </ts>
               <ts e="T538" id="Seg_3660" n="e" s="T537">vɨdelɨval </ts>
               <ts e="T539" id="Seg_3662" n="e" s="T538">eto </ts>
               <ts e="T540" id="Seg_3664" n="e" s="T539">razdelɨvajdɨː </ts>
               <ts e="T541" id="Seg_3666" n="e" s="T540">kanʼɨː. </ts>
               <ts e="T542" id="Seg_3668" n="e" s="T541">Kanʼiː. </ts>
            </ts>
            <ts e="T550" id="Seg_3669" n="sc" s="T543">
               <ts e="T544" id="Seg_3671" n="e" s="T543">Bi͡ereːččibit </ts>
               <ts e="T545" id="Seg_3673" n="e" s="T544">že </ts>
               <ts e="T546" id="Seg_3675" n="e" s="T545">eto </ts>
               <ts e="T547" id="Seg_3677" n="e" s="T546">kimŋe </ts>
               <ts e="T548" id="Seg_3679" n="e" s="T547">rɨbzavokka </ts>
               <ts e="T549" id="Seg_3681" n="e" s="T548">bi͡ereːččibit, </ts>
               <ts e="T550" id="Seg_3683" n="e" s="T549">sdavajdaːččɨbɨt. </ts>
            </ts>
            <ts e="T555" id="Seg_3684" n="sc" s="T551">
               <ts e="T552" id="Seg_3686" n="e" s="T551">Oni </ts>
               <ts e="T553" id="Seg_3688" n="e" s="T552">lovili </ts>
               <ts e="T554" id="Seg_3690" n="e" s="T553">mnogo </ts>
               <ts e="T555" id="Seg_3692" n="e" s="T554">rɨbɨ. </ts>
            </ts>
            <ts e="T564" id="Seg_3693" n="sc" s="T556">
               <ts e="T557" id="Seg_3695" n="e" s="T556">Utrom </ts>
               <ts e="T558" id="Seg_3697" n="e" s="T557">rano </ts>
               <ts e="T559" id="Seg_3699" n="e" s="T558">baraːččɨlar </ts>
               <ts e="T560" id="Seg_3701" n="e" s="T559">potom </ts>
               <ts e="T561" id="Seg_3703" n="e" s="T560">seti </ts>
               <ts e="T562" id="Seg_3705" n="e" s="T561">proverjat </ts>
               <ts e="T563" id="Seg_3707" n="e" s="T562">pozdno </ts>
               <ts e="T564" id="Seg_3709" n="e" s="T563">pri͡ezzhajut. </ts>
            </ts>
            <ts e="T570" id="Seg_3710" n="sc" s="T565">
               <ts e="T566" id="Seg_3712" n="e" s="T565">Počti </ts>
               <ts e="T567" id="Seg_3714" n="e" s="T566">vot </ts>
               <ts e="T568" id="Seg_3716" n="e" s="T567">(sid-) </ts>
               <ts e="T569" id="Seg_3718" n="e" s="T568">oloroːččubut </ts>
               <ts e="T570" id="Seg_3720" n="e" s="T569">tüːnneri. </ts>
            </ts>
            <ts e="T607" id="Seg_3721" n="sc" s="T571">
               <ts e="T572" id="Seg_3723" n="e" s="T571">Kumar </ts>
               <ts e="T573" id="Seg_3725" n="e" s="T572">letom </ts>
               <ts e="T574" id="Seg_3727" n="e" s="T573">komarka </ts>
               <ts e="T575" id="Seg_3729" n="e" s="T574">hi͡ete </ts>
               <ts e="T576" id="Seg_3731" n="e" s="T575">hi͡ete </ts>
               <ts e="T577" id="Seg_3733" n="e" s="T576">ol </ts>
               <ts e="T578" id="Seg_3735" n="e" s="T577">ogolorbut </ts>
               <ts e="T579" id="Seg_3737" n="e" s="T578">ke </ts>
               <ts e="T580" id="Seg_3739" n="e" s="T579">u͡olattarbut, </ts>
               <ts e="T581" id="Seg_3741" n="e" s="T580">sɨn </ts>
               <ts e="T582" id="Seg_3743" n="e" s="T581">moj </ts>
               <ts e="T583" id="Seg_3745" n="e" s="T582">u͡olum. </ts>
               <ts e="T584" id="Seg_3747" n="e" s="T583">Onton </ts>
               <ts e="T585" id="Seg_3749" n="e" s="T584">tam </ts>
               <ts e="T586" id="Seg_3751" n="e" s="T585">ješo </ts>
               <ts e="T587" id="Seg_3753" n="e" s="T586">dogottoro </ts>
               <ts e="T588" id="Seg_3755" n="e" s="T587">erim </ts>
               <ts e="T589" id="Seg_3757" n="e" s="T588">sestratɨn </ts>
               <ts e="T590" id="Seg_3759" n="e" s="T589">u͡ola </ts>
               <ts e="T591" id="Seg_3761" n="e" s="T590">ol </ts>
               <ts e="T592" id="Seg_3763" n="e" s="T591">(ka-) </ts>
               <ts e="T593" id="Seg_3765" n="e" s="T592">u͡otu </ts>
               <ts e="T594" id="Seg_3767" n="e" s="T593">oŋoroːččular </ts>
               <ts e="T595" id="Seg_3769" n="e" s="T594">buru͡o </ts>
               <ts e="T596" id="Seg_3771" n="e" s="T595">ke </ts>
               <ts e="T597" id="Seg_3773" n="e" s="T596">iti </ts>
               <ts e="T598" id="Seg_3775" n="e" s="T597">štobɨ </ts>
               <ts e="T599" id="Seg_3777" n="e" s="T598">komarɨ </ts>
               <ts e="T600" id="Seg_3779" n="e" s="T599">kimneːmi͡ekterin </ts>
               <ts e="T601" id="Seg_3781" n="e" s="T600">čugahaːmɨ͡aktarɨn </ts>
               <ts e="T602" id="Seg_3783" n="e" s="T601">bihi͡eke, </ts>
               <ts e="T603" id="Seg_3785" n="e" s="T602">kogda </ts>
               <ts e="T604" id="Seg_3787" n="e" s="T603">eto </ts>
               <ts e="T605" id="Seg_3789" n="e" s="T604">mɨ </ts>
               <ts e="T606" id="Seg_3791" n="e" s="T605">rɨbu </ts>
               <ts e="T607" id="Seg_3793" n="e" s="T606">razdelajdɨːrbɨtɨgar. </ts>
            </ts>
            <ts e="T612" id="Seg_3794" n="sc" s="T608">
               <ts e="T609" id="Seg_3796" n="e" s="T608">Tak </ts>
               <ts e="T610" id="Seg_3798" n="e" s="T609">i </ts>
               <ts e="T611" id="Seg_3800" n="e" s="T610">mɨ </ts>
               <ts e="T612" id="Seg_3802" n="e" s="T611">žili. </ts>
            </ts>
            <ts e="T619" id="Seg_3803" n="sc" s="T613">
               <ts e="T614" id="Seg_3805" n="e" s="T613">Potom </ts>
               <ts e="T615" id="Seg_3807" n="e" s="T614">eto </ts>
               <ts e="T616" id="Seg_3809" n="e" s="T615">što </ts>
               <ts e="T617" id="Seg_3811" n="e" s="T616">ješo </ts>
               <ts e="T618" id="Seg_3813" n="e" s="T617">tugu </ts>
               <ts e="T619" id="Seg_3815" n="e" s="T618">kepsi͡emij? </ts>
            </ts>
            <ts e="T628" id="Seg_3816" n="sc" s="T620">
               <ts e="T621" id="Seg_3818" n="e" s="T620">Kim </ts>
               <ts e="T622" id="Seg_3820" n="e" s="T621">ol </ts>
               <ts e="T623" id="Seg_3822" n="e" s="T622">saːdikka </ts>
               <ts e="T624" id="Seg_3824" n="e" s="T623">onton </ts>
               <ts e="T625" id="Seg_3826" n="e" s="T624">iti </ts>
               <ts e="T626" id="Seg_3828" n="e" s="T625">pensiaga </ts>
               <ts e="T627" id="Seg_3830" n="e" s="T626">bardɨm </ts>
               <ts e="T628" id="Seg_3832" n="e" s="T627">bu͡o. </ts>
            </ts>
            <ts e="T638" id="Seg_3833" n="sc" s="T629">
               <ts e="T630" id="Seg_3835" n="e" s="T629">Tridtsat </ts>
               <ts e="T631" id="Seg_3837" n="e" s="T630">otut </ts>
               <ts e="T632" id="Seg_3839" n="e" s="T631">agɨs </ts>
               <ts e="T633" id="Seg_3841" n="e" s="T632">dʼɨlɨ </ts>
               <ts e="T634" id="Seg_3843" n="e" s="T633">otut </ts>
               <ts e="T635" id="Seg_3845" n="e" s="T634">alta </ts>
               <ts e="T636" id="Seg_3847" n="e" s="T635">dʼɨlɨ </ts>
               <ts e="T637" id="Seg_3849" n="e" s="T636">üleleːbitim </ts>
               <ts e="T638" id="Seg_3851" n="e" s="T637">saːdikka. </ts>
            </ts>
            <ts e="T648" id="Seg_3852" n="sc" s="T639">
               <ts e="T640" id="Seg_3854" n="e" s="T639">Maŋnajɨ </ts>
               <ts e="T641" id="Seg_3856" n="e" s="T640">šku͡ola </ts>
               <ts e="T642" id="Seg_3858" n="e" s="T641">šku͡ola </ts>
               <ts e="T643" id="Seg_3860" n="e" s="T642">di͡et </ts>
               <ts e="T644" id="Seg_3862" n="e" s="T643">üleleːbitim </ts>
               <ts e="T645" id="Seg_3864" n="e" s="T644">povarga </ts>
               <ts e="T646" id="Seg_3866" n="e" s="T645">povarom </ts>
               <ts e="T647" id="Seg_3868" n="e" s="T646">üleleːbitim </ts>
               <ts e="T648" id="Seg_3870" n="e" s="T647">povarga. </ts>
            </ts>
            <ts e="T662" id="Seg_3871" n="sc" s="T649">
               <ts e="T650" id="Seg_3873" n="e" s="T649">Školaga </ts>
               <ts e="T651" id="Seg_3875" n="e" s="T650">onton </ts>
               <ts e="T652" id="Seg_3877" n="e" s="T651">saːdikkaga </ts>
               <ts e="T653" id="Seg_3879" n="e" s="T652">ɨlbɨttara </ts>
               <ts e="T654" id="Seg_3881" n="e" s="T653">pereveli </ts>
               <ts e="T655" id="Seg_3883" n="e" s="T654">eh </ts>
               <ts e="T656" id="Seg_3885" n="e" s="T655">kimneːbittere </ts>
               <ts e="T657" id="Seg_3887" n="e" s="T656">egelbittere </ts>
               <ts e="T658" id="Seg_3889" n="e" s="T657">kim </ts>
               <ts e="T659" id="Seg_3891" n="e" s="T658">onton </ts>
               <ts e="T660" id="Seg_3893" n="e" s="T659">šku͡olattan </ts>
               <ts e="T661" id="Seg_3895" n="e" s="T660">saːdikka </ts>
               <ts e="T662" id="Seg_3897" n="e" s="T661">üleleːbitim. </ts>
            </ts>
            <ts e="T670" id="Seg_3898" n="sc" s="T663">
               <ts e="T664" id="Seg_3900" n="e" s="T663">A </ts>
               <ts e="T665" id="Seg_3902" n="e" s="T664">sejčas </ts>
               <ts e="T666" id="Seg_3904" n="e" s="T665">pensiaga </ts>
               <ts e="T667" id="Seg_3906" n="e" s="T666">olorobun, </ts>
               <ts e="T668" id="Seg_3908" n="e" s="T667">vospitɨvaju </ts>
               <ts e="T669" id="Seg_3910" n="e" s="T668">vnuk, </ts>
               <ts e="T670" id="Seg_3912" n="e" s="T669">vnuktarbɨn. </ts>
            </ts>
            <ts e="T678" id="Seg_3913" n="sc" s="T671">
               <ts e="T672" id="Seg_3915" n="e" s="T671">Pravnučkajbun </ts>
               <ts e="T673" id="Seg_3917" n="e" s="T672">sižu </ts>
               <ts e="T674" id="Seg_3919" n="e" s="T673">tože </ts>
               <ts e="T675" id="Seg_3921" n="e" s="T674">hožu </ts>
               <ts e="T676" id="Seg_3923" n="e" s="T675">üleliːbin </ts>
               <ts e="T677" id="Seg_3925" n="e" s="T676">üleliː </ts>
               <ts e="T678" id="Seg_3927" n="e" s="T677">hɨldʼabɨn. </ts>
            </ts>
            <ts e="T691" id="Seg_3928" n="sc" s="T679">
               <ts e="T680" id="Seg_3930" n="e" s="T679">A </ts>
               <ts e="T681" id="Seg_3932" n="e" s="T680">nevestka </ts>
               <ts e="T682" id="Seg_3934" n="e" s="T681">na </ts>
               <ts e="T683" id="Seg_3936" n="e" s="T682">rabotu </ts>
               <ts e="T684" id="Seg_3938" n="e" s="T683">naːda </ts>
               <ts e="T685" id="Seg_3940" n="e" s="T684">idti </ts>
               <ts e="T686" id="Seg_3942" n="e" s="T685">a </ts>
               <ts e="T687" id="Seg_3944" n="e" s="T686">min </ts>
               <ts e="T688" id="Seg_3946" n="e" s="T687">ol </ts>
               <ts e="T689" id="Seg_3948" n="e" s="T688">ogonɨ </ts>
               <ts e="T690" id="Seg_3950" n="e" s="T689">gɨtta </ts>
               <ts e="T691" id="Seg_3952" n="e" s="T690">olorobun. </ts>
            </ts>
            <ts e="T700" id="Seg_3953" n="sc" s="T692">
               <ts e="T693" id="Seg_3955" n="e" s="T692">Ješo </ts>
               <ts e="T694" id="Seg_3957" n="e" s="T693">malenki͡e </ts>
               <ts e="T695" id="Seg_3959" n="e" s="T694">hüːrbeleːger </ts>
               <ts e="T696" id="Seg_3961" n="e" s="T695">dʼɨllaːnɨ͡aga </ts>
               <ts e="T697" id="Seg_3963" n="e" s="T696">markka </ts>
               <ts e="T698" id="Seg_3965" n="e" s="T697">ol </ts>
               <ts e="T699" id="Seg_3967" n="e" s="T698">(vnu-) </ts>
               <ts e="T700" id="Seg_3969" n="e" s="T699">vnučkam. </ts>
            </ts>
            <ts e="T708" id="Seg_3970" n="sc" s="T701">
               <ts e="T702" id="Seg_3972" n="e" s="T701">Sejčas </ts>
               <ts e="T703" id="Seg_3974" n="e" s="T702">što </ts>
               <ts e="T704" id="Seg_3976" n="e" s="T703">to </ts>
               <ts e="T705" id="Seg_3978" n="e" s="T704">kimneːbit </ts>
               <ts e="T706" id="Seg_3980" n="e" s="T705">nemnožko </ts>
               <ts e="T707" id="Seg_3982" n="e" s="T706">ɨ͡aldʼabɨt </ts>
               <ts e="T708" id="Seg_3984" n="e" s="T707">stomatittammɨt. </ts>
            </ts>
            <ts e="T713" id="Seg_3985" n="sc" s="T709">
               <ts e="T710" id="Seg_3987" n="e" s="T709">Maːmata </ts>
               <ts e="T711" id="Seg_3989" n="e" s="T710">oloror </ts>
               <ts e="T712" id="Seg_3991" n="e" s="T711">na </ts>
               <ts e="T713" id="Seg_3993" n="e" s="T712">bolničnom. </ts>
            </ts>
            <ts e="T752" id="Seg_3994" n="sc" s="T714">
               <ts e="T715" id="Seg_3996" n="e" s="T714">Tak </ts>
               <ts e="T716" id="Seg_3998" n="e" s="T715">kim </ts>
               <ts e="T717" id="Seg_4000" n="e" s="T716">bu </ts>
               <ts e="T718" id="Seg_4002" n="e" s="T717">ulakan </ts>
               <ts e="T719" id="Seg_4004" n="e" s="T718">vnugum </ts>
               <ts e="T720" id="Seg_4006" n="e" s="T719">u͡olun </ts>
               <ts e="T721" id="Seg_4008" n="e" s="T720">gi͡ene </ts>
               <ts e="T722" id="Seg_4010" n="e" s="T721">ulakan </ts>
               <ts e="T723" id="Seg_4012" n="e" s="T722">kotorɨj </ts>
               <ts e="T724" id="Seg_4014" n="e" s="T723">eto </ts>
               <ts e="T725" id="Seg_4016" n="e" s="T724">staršij </ts>
               <ts e="T726" id="Seg_4018" n="e" s="T725">vnugum </ts>
               <ts e="T727" id="Seg_4020" n="e" s="T726">ulakan </ts>
               <ts e="T728" id="Seg_4022" n="e" s="T727">anɨ </ts>
               <ts e="T729" id="Seg_4024" n="e" s="T728">služittɨːr </ts>
               <ts e="T730" id="Seg_4026" n="e" s="T729">kimŋe </ts>
               <ts e="T731" id="Seg_4028" n="e" s="T730">armiaga </ts>
               <ts e="T732" id="Seg_4030" n="e" s="T731">služit. </ts>
               <ts e="T835" id="Seg_4032" n="e" s="T732">Aŋardar </ts>
               <ts e="T733" id="Seg_4034" n="e" s="T835">ü͡öreneller. </ts>
               <ts e="T734" id="Seg_4036" n="e" s="T733">A </ts>
               <ts e="T735" id="Seg_4038" n="e" s="T734">kɨːha </ts>
               <ts e="T736" id="Seg_4040" n="e" s="T735">kɨːhɨm </ts>
               <ts e="T737" id="Seg_4042" n="e" s="T736">vnučkam </ts>
               <ts e="T738" id="Seg_4044" n="e" s="T737">ulakan </ts>
               <ts e="T739" id="Seg_4046" n="e" s="T738">eto </ts>
               <ts e="T740" id="Seg_4048" n="e" s="T739">u͡olum </ts>
               <ts e="T741" id="Seg_4050" n="e" s="T740">(vnučk-) </ts>
               <ts e="T742" id="Seg_4052" n="e" s="T741">kɨːha, </ts>
               <ts e="T743" id="Seg_4054" n="e" s="T742">vnučkam, </ts>
               <ts e="T744" id="Seg_4056" n="e" s="T743">Katja </ts>
               <ts e="T745" id="Seg_4058" n="e" s="T744">aːta, </ts>
               <ts e="T746" id="Seg_4060" n="e" s="T745">iti </ts>
               <ts e="T747" id="Seg_4062" n="e" s="T746">Norilske </ts>
               <ts e="T748" id="Seg_4064" n="e" s="T747">kimŋe </ts>
               <ts e="T749" id="Seg_4066" n="e" s="T748">ü͡örener </ts>
               <ts e="T750" id="Seg_4068" n="e" s="T749">tože </ts>
               <ts e="T751" id="Seg_4070" n="e" s="T750">maːmatɨn </ts>
               <ts e="T752" id="Seg_4072" n="e" s="T751">kördük. </ts>
            </ts>
            <ts e="T768" id="Seg_4073" n="sc" s="T753">
               <ts e="T754" id="Seg_4075" n="e" s="T753">Eto </ts>
               <ts e="T755" id="Seg_4077" n="e" s="T754">Norilskaj </ts>
               <ts e="T756" id="Seg_4079" n="e" s="T755">kimŋe </ts>
               <ts e="T757" id="Seg_4081" n="e" s="T756">tʼexnʼikumŋa </ts>
               <ts e="T758" id="Seg_4083" n="e" s="T757">ü͡örener </ts>
               <ts e="T759" id="Seg_4085" n="e" s="T758">maːmatɨn </ts>
               <ts e="T760" id="Seg_4087" n="e" s="T759">kördük, </ts>
               <ts e="T761" id="Seg_4089" n="e" s="T760">nu </ts>
               <ts e="T762" id="Seg_4091" n="e" s="T761">na </ts>
               <ts e="T763" id="Seg_4093" n="e" s="T762">šitjo </ts>
               <ts e="T764" id="Seg_4095" n="e" s="T763">tam </ts>
               <ts e="T765" id="Seg_4097" n="e" s="T764">na </ts>
               <ts e="T766" id="Seg_4099" n="e" s="T765">oformleni͡e </ts>
               <ts e="T767" id="Seg_4101" n="e" s="T766">iti </ts>
               <ts e="T768" id="Seg_4103" n="e" s="T767">kimŋe. </ts>
            </ts>
            <ts e="T787" id="Seg_4104" n="sc" s="T769">
               <ts e="T770" id="Seg_4106" n="e" s="T769">Rodila </ts>
               <ts e="T771" id="Seg_4108" n="e" s="T770">rebjonka </ts>
               <ts e="T772" id="Seg_4110" n="e" s="T771">keːspite </ts>
               <ts e="T773" id="Seg_4112" n="e" s="T772">bihi͡eke </ts>
               <ts e="T774" id="Seg_4114" n="e" s="T773">i </ts>
               <ts e="T775" id="Seg_4116" n="e" s="T774">pošla </ts>
               <ts e="T776" id="Seg_4118" n="e" s="T775">poehala </ts>
               <ts e="T777" id="Seg_4120" n="e" s="T776">töttörü </ts>
               <ts e="T778" id="Seg_4122" n="e" s="T777">büte </ts>
               <ts e="T779" id="Seg_4124" n="e" s="T778">štob </ts>
               <ts e="T780" id="Seg_4126" n="e" s="T779">dʼɨlɨn </ts>
               <ts e="T781" id="Seg_4128" n="e" s="T780">hüterimeːri </ts>
               <ts e="T782" id="Seg_4130" n="e" s="T781">ke </ts>
               <ts e="T783" id="Seg_4132" n="e" s="T782">štobɨ </ts>
               <ts e="T784" id="Seg_4134" n="e" s="T783">god </ts>
               <ts e="T785" id="Seg_4136" n="e" s="T784">ne </ts>
               <ts e="T786" id="Seg_4138" n="e" s="T785">propal, </ts>
               <ts e="T787" id="Seg_4140" n="e" s="T786">zakančivat. </ts>
            </ts>
            <ts e="T794" id="Seg_4141" n="sc" s="T788">
               <ts e="T789" id="Seg_4143" n="e" s="T788">Ješo </ts>
               <ts e="T790" id="Seg_4145" n="e" s="T789">dva </ts>
               <ts e="T791" id="Seg_4147" n="e" s="T790">ikki </ts>
               <ts e="T792" id="Seg_4149" n="e" s="T791">dʼɨlɨ </ts>
               <ts e="T793" id="Seg_4151" n="e" s="T792">naːda </ts>
               <ts e="T794" id="Seg_4153" n="e" s="T793">ü͡öreni͡ekke. </ts>
            </ts>
            <ts e="T807" id="Seg_4154" n="sc" s="T795">
               <ts e="T796" id="Seg_4156" n="e" s="T795">Tri </ts>
               <ts e="T797" id="Seg_4158" n="e" s="T796">goda </ts>
               <ts e="T798" id="Seg_4160" n="e" s="T797">s </ts>
               <ts e="T799" id="Seg_4162" n="e" s="T798">čem </ts>
               <ts e="T800" id="Seg_4164" n="e" s="T799">to </ts>
               <ts e="T801" id="Seg_4166" n="e" s="T800">üs </ts>
               <ts e="T802" id="Seg_4168" n="e" s="T801">dʼɨl </ts>
               <ts e="T803" id="Seg_4170" n="e" s="T802">s </ts>
               <ts e="T804" id="Seg_4172" n="e" s="T803">čem </ts>
               <ts e="T805" id="Seg_4174" n="e" s="T804">to </ts>
               <ts e="T806" id="Seg_4176" n="e" s="T805">üle </ts>
               <ts e="T807" id="Seg_4178" n="e" s="T806">ü͡öreneller. </ts>
            </ts>
            <ts e="T813" id="Seg_4179" n="sc" s="T808">
               <ts e="T809" id="Seg_4181" n="e" s="T808">Vot </ts>
               <ts e="T810" id="Seg_4183" n="e" s="T809">tak </ts>
               <ts e="T811" id="Seg_4185" n="e" s="T810">vot </ts>
               <ts e="T812" id="Seg_4187" n="e" s="T811">taki͡e </ts>
               <ts e="T813" id="Seg_4189" n="e" s="T812">dela. </ts>
            </ts>
            <ts e="T826" id="Seg_4190" n="sc" s="T814">
               <ts e="T815" id="Seg_4192" n="e" s="T814">Žhivu </ts>
               <ts e="T816" id="Seg_4194" n="e" s="T815">odna </ts>
               <ts e="T817" id="Seg_4196" n="e" s="T816">biːrges </ts>
               <ts e="T818" id="Seg_4198" n="e" s="T817">vnukbɨn </ts>
               <ts e="T819" id="Seg_4200" n="e" s="T818">(gɨt-) </ts>
               <ts e="T820" id="Seg_4202" n="e" s="T819">kɨːhɨm </ts>
               <ts e="T821" id="Seg_4204" n="e" s="T820">u͡olun </ts>
               <ts e="T822" id="Seg_4206" n="e" s="T821">gɨtta </ts>
               <ts e="T823" id="Seg_4208" n="e" s="T822">vnukbɨn </ts>
               <ts e="T824" id="Seg_4210" n="e" s="T823">gɨtta </ts>
               <ts e="T825" id="Seg_4212" n="e" s="T824">baːrbɨn </ts>
               <ts e="T826" id="Seg_4214" n="e" s="T825">di͡eber. </ts>
            </ts>
            <ts e="T830" id="Seg_4215" n="sc" s="T827">
               <ts e="T828" id="Seg_4217" n="e" s="T827">Hettis </ts>
               <ts e="T829" id="Seg_4219" n="e" s="T828">kɨlaːska </ts>
               <ts e="T830" id="Seg_4221" n="e" s="T829">ü͡örener. </ts>
            </ts>
            <ts e="T834" id="Seg_4222" n="sc" s="T831">
               <ts e="T832" id="Seg_4224" n="e" s="T831">Ješo </ts>
               <ts e="T833" id="Seg_4226" n="e" s="T832">što </ts>
               <ts e="T834" id="Seg_4228" n="e" s="T833">govorit. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T7" id="Seg_4229" s="T1">BeSN_2009_Family_nar.001 (001)</ta>
            <ta e="T12" id="Seg_4230" s="T8">BeSN_2009_Family_nar.002 (002)</ta>
            <ta e="T22" id="Seg_4231" s="T13">BeSN_2009_Family_nar.003 (003)</ta>
            <ta e="T33" id="Seg_4232" s="T23">BeSN_2009_Family_nar.004 (004)</ta>
            <ta e="T41" id="Seg_4233" s="T34">BeSN_2009_Family_nar.005 (005)</ta>
            <ta e="T48" id="Seg_4234" s="T42">BeSN_2009_Family_nar.006 (006)</ta>
            <ta e="T53" id="Seg_4235" s="T49">BeSN_2009_Family_nar.007 (007)</ta>
            <ta e="T60" id="Seg_4236" s="T54">BeSN_2009_Family_nar.008 (008)</ta>
            <ta e="T75" id="Seg_4237" s="T61">BeSN_2009_Family_nar.009 (009)</ta>
            <ta e="T87" id="Seg_4238" s="T76">BeSN_2009_Family_nar.010 (010)</ta>
            <ta e="T102" id="Seg_4239" s="T88">BeSN_2009_Family_nar.011 (011)</ta>
            <ta e="T122" id="Seg_4240" s="T103">BeSN_2009_Family_nar.012 (012)</ta>
            <ta e="T131" id="Seg_4241" s="T123">BeSN_2009_Family_nar.013 (013)</ta>
            <ta e="T140" id="Seg_4242" s="T132">BeSN_2009_Family_nar.014 (014)</ta>
            <ta e="T147" id="Seg_4243" s="T141">BeSN_2009_Family_nar.015 (015)</ta>
            <ta e="T164" id="Seg_4244" s="T148">BeSN_2009_Family_nar.016 (016)</ta>
            <ta e="T171" id="Seg_4245" s="T165">BeSN_2009_Family_nar.017 (017)</ta>
            <ta e="T182" id="Seg_4246" s="T172">BeSN_2009_Family_nar.018 (018)</ta>
            <ta e="T200" id="Seg_4247" s="T183">BeSN_2009_Family_nar.019 (019)</ta>
            <ta e="T214" id="Seg_4248" s="T201">BeSN_2009_Family_nar.020 (020)</ta>
            <ta e="T223" id="Seg_4249" s="T215">BeSN_2009_Family_nar.021 (021)</ta>
            <ta e="T234" id="Seg_4250" s="T224">BeSN_2009_Family_nar.022 (022)</ta>
            <ta e="T248" id="Seg_4251" s="T235">BeSN_2009_Family_nar.023 (023)</ta>
            <ta e="T261" id="Seg_4252" s="T249">BeSN_2009_Family_nar.024 (024)</ta>
            <ta e="T274" id="Seg_4253" s="T262">BeSN_2009_Family_nar.025 (025)</ta>
            <ta e="T280" id="Seg_4254" s="T275">BeSN_2009_Family_nar.026 (026)</ta>
            <ta e="T289" id="Seg_4255" s="T281">BeSN_2009_Family_nar.027 (027)</ta>
            <ta e="T296" id="Seg_4256" s="T290">BeSN_2009_Family_nar.028 (028)</ta>
            <ta e="T306" id="Seg_4257" s="T297">BeSN_2009_Family_nar.029 (029)</ta>
            <ta e="T312" id="Seg_4258" s="T307">BeSN_2009_Family_nar.030 (030)</ta>
            <ta e="T321" id="Seg_4259" s="T313">BeSN_2009_Family_nar.031 (031)</ta>
            <ta e="T338" id="Seg_4260" s="T322">BeSN_2009_Family_nar.032 (032)</ta>
            <ta e="T347" id="Seg_4261" s="T339">BeSN_2009_Family_nar.033 (033)</ta>
            <ta e="T360" id="Seg_4262" s="T348">BeSN_2009_Family_nar.034 (034)</ta>
            <ta e="T368" id="Seg_4263" s="T361">BeSN_2009_Family_nar.035 (035)</ta>
            <ta e="T377" id="Seg_4264" s="T369">BeSN_2009_Family_nar.036 (036)</ta>
            <ta e="T390" id="Seg_4265" s="T378">BeSN_2009_Family_nar.037 (037)</ta>
            <ta e="T403" id="Seg_4266" s="T391">BeSN_2009_Family_nar.038 (038)</ta>
            <ta e="T414" id="Seg_4267" s="T404">BeSN_2009_Family_nar.039 (039)</ta>
            <ta e="T422" id="Seg_4268" s="T415">BeSN_2009_Family_nar.040 (040)</ta>
            <ta e="T428" id="Seg_4269" s="T423">BeSN_2009_Family_nar.041 (041)</ta>
            <ta e="T438" id="Seg_4270" s="T429">BeSN_2009_Family_nar.042 (042)</ta>
            <ta e="T456" id="Seg_4271" s="T439">BeSN_2009_Family_nar.043 (043)</ta>
            <ta e="T466" id="Seg_4272" s="T457">BeSN_2009_Family_nar.044 (044)</ta>
            <ta e="T481" id="Seg_4273" s="T467">BeSN_2009_Family_nar.045 (045)</ta>
            <ta e="T484" id="Seg_4274" s="T482">BeSN_2009_Family_nar.046 (046)</ta>
            <ta e="T502" id="Seg_4275" s="T485">BeSN_2009_Family_nar.047 (047)</ta>
            <ta e="T518" id="Seg_4276" s="T503">BeSN_2009_Family_nar.048 (048)</ta>
            <ta e="T528" id="Seg_4277" s="T519">BeSN_2009_Family_nar.049 (049)</ta>
            <ta e="T541" id="Seg_4278" s="T529">BeSN_2009_Family_nar.050 (050)</ta>
            <ta e="T542" id="Seg_4279" s="T541">BeSN_2009_Family_nar.051 (051)</ta>
            <ta e="T550" id="Seg_4280" s="T543">BeSN_2009_Family_nar.052 (052)</ta>
            <ta e="T555" id="Seg_4281" s="T551">BeSN_2009_Family_nar.053 (053)</ta>
            <ta e="T564" id="Seg_4282" s="T556">BeSN_2009_Family_nar.054 (054)</ta>
            <ta e="T570" id="Seg_4283" s="T565">BeSN_2009_Family_nar.055 (055)</ta>
            <ta e="T583" id="Seg_4284" s="T571">BeSN_2009_Family_nar.056 (056)</ta>
            <ta e="T607" id="Seg_4285" s="T583">BeSN_2009_Family_nar.057 (057)</ta>
            <ta e="T612" id="Seg_4286" s="T608">BeSN_2009_Family_nar.058 (058)</ta>
            <ta e="T619" id="Seg_4287" s="T613">BeSN_2009_Family_nar.059 (059)</ta>
            <ta e="T628" id="Seg_4288" s="T620">BeSN_2009_Family_nar.060 (060)</ta>
            <ta e="T638" id="Seg_4289" s="T629">BeSN_2009_Family_nar.061 (061)</ta>
            <ta e="T648" id="Seg_4290" s="T639">BeSN_2009_Family_nar.062 (062)</ta>
            <ta e="T662" id="Seg_4291" s="T649">BeSN_2009_Family_nar.063 (063)</ta>
            <ta e="T670" id="Seg_4292" s="T663">BeSN_2009_Family_nar.064 (064)</ta>
            <ta e="T678" id="Seg_4293" s="T671">BeSN_2009_Family_nar.065 (065)</ta>
            <ta e="T691" id="Seg_4294" s="T679">BeSN_2009_Family_nar.066 (066)</ta>
            <ta e="T700" id="Seg_4295" s="T692">BeSN_2009_Family_nar.067 (067)</ta>
            <ta e="T708" id="Seg_4296" s="T701">BeSN_2009_Family_nar.068 (068)</ta>
            <ta e="T713" id="Seg_4297" s="T709">BeSN_2009_Family_nar.069 (069)</ta>
            <ta e="T732" id="Seg_4298" s="T714">BeSN_2009_Family_nar.070 (070)</ta>
            <ta e="T733" id="Seg_4299" s="T732">BeSN_2009_Family_nar.071 (071)</ta>
            <ta e="T752" id="Seg_4300" s="T733">BeSN_2009_Family_nar.072 (071)</ta>
            <ta e="T768" id="Seg_4301" s="T753">BeSN_2009_Family_nar.073 (072)</ta>
            <ta e="T787" id="Seg_4302" s="T769">BeSN_2009_Family_nar.074 (073)</ta>
            <ta e="T794" id="Seg_4303" s="T788">BeSN_2009_Family_nar.075 (074)</ta>
            <ta e="T807" id="Seg_4304" s="T795">BeSN_2009_Family_nar.076 (075)</ta>
            <ta e="T813" id="Seg_4305" s="T808">BeSN_2009_Family_nar.077 (076)</ta>
            <ta e="T826" id="Seg_4306" s="T814">BeSN_2009_Family_nar.078 (077)</ta>
            <ta e="T830" id="Seg_4307" s="T827">BeSN_2009_Family_nar.079 (078)</ta>
            <ta e="T834" id="Seg_4308" s="T831">BeSN_2009_Family_nar.080 (079)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T7" id="Seg_4309" s="T1">min aːt-(I)m Iste Saka-LIː Iste di͡e-AːččI-LAr</ta>
            <ta e="T12" id="Seg_4310" s="T8">töröː-BIt-(I)m Popigaj di͡e-An sir-GA</ta>
            <ta e="T22" id="Seg_4311" s="T13">min kergen-LAr-(I)m teːte-(I)m kim N’ukulaj mama.R-(I)m eto.R Taisa Taisa</ta>
            <ta e="T33" id="Seg_4312" s="T23">ol-TI-(I)ŋ mama.R-(I)m erde bagajɨ öl-BIt-(t)A onton min teːte-BIn kɨtta kaːl-BIt-(I)m</ta>
            <ta e="T41" id="Seg_4313" s="T34">teːte-(I)m du͡o kim-GA pastux.R-GA brigadir.R-GA üleleː-Ar e-TA</ta>
            <ta e="T48" id="Seg_4314" s="T42">onton bɨraːt-(I)m tože.R teːte-BIn kɨtta hɨrɨt-AːččI</ta>
            <ta e="T53" id="Seg_4315" s="T49">tundra.R-GA bihigi kös-A hɨrɨt-AːččI-BIt</ta>
            <ta e="T60" id="Seg_4316" s="T54">škola-(t)tAn kanikul-BAr kel-TAk-BInA min teːte-BAr bu͡ol-AːččI-BIn</ta>
            <ta e="T75" id="Seg_4317" s="T61">no.R ulaːt-BIt-(I)m kennine min teːte-BAr bu͡ol-BIt-(I)m kogda.R min oččuguj er-TAk-BInA kihi-LAr-GA iːt-IlIn-I-BIt-(I)m atɨn kihi-LAr-GA</ta>
            <ta e="T87" id="Seg_4318" s="T76">iːt-IlIn-I-BIt-(I)m onton ulaːt-An bar-An-BIn ere teːte-BAr bu͡ol-BIt-(I)m bar-AːččI-BIn teːte-BAr kanikul-GA kel-TAk-BInA</ta>
            <ta e="T102" id="Seg_4319" s="T88">de ol kös-A hɨrɨt-AːččI-BIt kim-(t)tAn mesto.R-(t)tAn mesto.R-GA di͡eri na *nartax na * kös-A hɨrɨt-AːččI-BIt</ta>
            <ta e="T122" id="Seg_4320" s="T103">čum-GA d’i͡e-LAːk bu͡ol-AːččI-BIt tam.R sneg.R eh kaːr-(n)I kim-LAː-A-BIt ubiraj-LAː-A-BIt potom.R mɨ.R stavim.R eti.R palka.R taki͡e.R dlinnɨe.R čum čum oŋor-Ar-LAr</ta>
            <ta e="T140" id="Seg_4321" s="T132">heee eto.R ja po.R *r-(I)s-(I)s-GI uraha d’i͡e-GA aha</ta>
            <ta e="T147" id="Seg_4322" s="T141">uraha-LAr-(n)I kim-(t)tAn taki͡e.R dlinnɨe.R taki͡e.R</ta>
            <ta e="T164" id="Seg_4323" s="T148">ol d’i͡e-(I)ŋ inogda.R ki͡eŋ bagajɨ oŋor-Ar-LAr ügüs kihi ol gɨn-An ügüs-BIt mnogo.R že.R ügüs-BIt</ta>
            <ta e="T171" id="Seg_4324" s="T165">* kim ki͡eŋ d’i͡e ki͡eŋ d’uka</ta>
            <ta e="T182" id="Seg_4325" s="T172">potom.R pečka.R-BIt ohok timir ohok pečka.R srazu.R</ta>
            <ta e="T200" id="Seg_4326" s="T183">i.R eto.R *vniz eto.R kim-(t)In annɨ-(t)In kim-LAː-Ar-LAr battɨk oŋor-Ar-LAr battat-Ar-LAr i.R srazu.R štobɨ.R kim-LAː-(I)m-IAgIŋ argɨj-(I)m-IAgIŋ</ta>
            <ta e="T214" id="Seg_4327" s="T201">daže.R oččuguj ogo-LAːk ezdit.R-LAː-AːččI-BIt tam.R ljulka.R tam.R u.R * kim Saka ljulka.R-GA tuda.R</ta>
            <ta e="T223" id="Seg_4328" s="T215">jejo.R *polož-A-T eti.R *reb-(n)Ij-An-GA i.R *ez-T-(n)Ij-A-T bar-A hɨrɨt-A-BIt</ta>
            <ta e="T234" id="Seg_4329" s="T224">onton tugu kepseː-IAm-(n)Ij teːte-(I)m du͡o Kamennɨj kim-GA storona.R-GA hɨrɨt-I-BIt-(t)A Kamennɨj</ta>
            <ta e="T248" id="Seg_4330" s="T235">tam.R net.R-I kim kaːs da su͡ok *</ta>
            <ta e="T274" id="Seg_4331" s="T262">ješo tu͡ok-(n)I-(n)Ij kim min beje-(I)m ü͡ören-BIt-(I)m sette kɨlaːs-GA di͡eri agɨs-(I)s kɨlaːs-(t)tAn büt-BIt-(I)m</ta>
            <ta e="T280" id="Seg_4332" s="T275">kim-(n)I internaːt-GA onton internaːt-BIt bu͡ol-AːččI-BIt</ta>
            <ta e="T289" id="Seg_4333" s="T281">ikki d’ɨl-(n)I Norilsk-A-GA ü͡ören-BIt-BIt internaːt-GA *ž kim olor-BIt-BIt</ta>
            <ta e="T296" id="Seg_4334" s="T290">lager.R-GA hɨrɨt-I-BIt-(I)m kim-GA Tajožnaj-GA hɨrɨt-I-BIt-(I)m Kurejka-GA</ta>
            <ta e="T306" id="Seg_4335" s="T297">potom.R ulaːt-An bar-An kim-GA eee ol ü͡ören-A bar-BIt-(I)m</ta>
            <ta e="T312" id="Seg_4336" s="T307">na eto.R na kurs.R-(n)I kim-GA</ta>
            <ta e="T321" id="Seg_4337" s="T313">atɨːlaː-Ar kihi ke kim-GA prodavets.R-GA bar-BIt-(I)m ü͡ören-A Dudinka-GA</ta>
            <ta e="T338" id="Seg_4338" s="T322">alta ɨj-(n)I ü͡ören-BIt-(I)m onton kel-BIt-(I)m i.R Nosku͡o-GA kim-LAː-BIt-(I)m *prak-TI-GA-TI *prox-o-TI-LI i.R tam.R kim-GA bufet.R-GA üleleː-AːččI-BIn</ta>
            <ta e="T347" id="Seg_4339" s="T339">stolovaja.R-GA bufet e-TA Nosku͡o-GA onno etot.R bufet.R-GA üleleː-AːččI-BIn</ta>
            <ta e="T360" id="Seg_4340" s="T348">onton v.R Nosku͡o-GA biːr kihi-(n)I manna Ki͡etaː-(t)tAn biːr u͡ol-(n)I kɨtta dogor-LAː-(I)s-BIt-(I)m</ta>
            <ta e="T377" id="Seg_4341" s="T369">onton ɨl-(I)s-An bar-An-BIt to kim-LAː-BIt-BIt Nosku͡o-GA olor-A tüs-BIt-(I)m</ta>
            <ta e="T390" id="Seg_4342" s="T378">min saːdik-GA üleleː-Ar e-TI-(I)m njanja.R-GA a.R gini-(I)ŋ du͡o počta-GA montjor.INST.R üleleː-Ar e-TA</ta>
            <ta e="T403" id="Seg_4343" s="T391">onton u.R nas.GEN.R vot.R kim kɨːs-BIt kɨːs-(I)m Riːta aːt-LAːk kɨːs-(I)m u͡ol-(I)m</ta>
            <ta e="T414" id="Seg_4344" s="T404">onton on.R što to *zax-o-TAr bɨt.R sir-(t)IgAr kel-IAk-(t)In rodina.R-(t)IgAr kel-IAk-(t)In</ta>
            <ta e="T422" id="Seg_4345" s="T415">i.R on.R menja.R menja.R *otprav-I-LAː ɨːt-BIt-(t)A</ta>
            <ta e="T428" id="Seg_4346" s="T423">ogo-BIn kɨtta ke kɨːs-BIn kɨtta</ta>
            <ta e="T438" id="Seg_4347" s="T429">i.R potom.R tolko.R gini kenni-GA min kenni-BIttAn gini kel-BIt-(t)A</ta>
            <ta e="T456" id="Seg_4348" s="T439">i.R vot.R si͡e *tex *-BAr kas d’ɨl bu͡ol-TA ke</ta>
            <ta e="T481" id="Seg_4349" s="T467">min ogolon-BIt-(I)m u͡ol-BIn</ta>
            <ta e="T502" id="Seg_4350" s="T485">nu.R tak.R i.R si͡e *tex *-BAr * olor-A-BIn saːdik-GA üle-LAː-BIt-(I)m do pensia.R uže.R saːdik-GA hubu že.R saːdik-GA</ta>
            <ta e="T518" id="Seg_4351" s="T503">onton tu͡ok kepseː-IAm-(n)Ij onton er-(I)m da ol er-(I)m rɨbalka.R-GA bu͡ol-AːččI iti-NA to točka.R-GA olor-AːččI-BIt čugas</ta>
            <ta e="T528" id="Seg_4352" s="T519">kogda.R u.R menja.R vɨxodnɨe.PL.R vɨxodnoj.R-LAr-BAr gini͡eke bar-AːččI-BIn motor-(I)nAn ezdit.R-AːččI-BIt</ta>
            <ta e="T541" id="Seg_4353" s="T529">i.R tam.R ja kim kömölös-AːččI-BIn * eto.R razdelɨvaj-LAː-A kann’aː-A</ta>
            <ta e="T550" id="Seg_4354" s="T543">bi͡er-AːččI-BIt že.R eto.R kim-GA rɨbzavod-GA bi͡er-AːččI-BIt sdavat.R-LAː-AːččI-BIt</ta>
            <ta e="T570" id="Seg_4355" s="T565">olor-AːččI-BIt tüːn-LAr-(n)I</ta>
            <ta e="T583" id="Seg_4356" s="T571">komar.R letom.R komar.R-GA si͡e-TA si͡e-TA ol ogo-LAr-BIt ke u͡olattar-BIt u͡ol-(I)m</ta>
            <ta e="T607" id="Seg_4357" s="T583">onton tam.R ješo dogor-LArA er-(I)m sestra.R-(t)In u͡ol-(t)A ol ke u͡ot-(n)I oŋor-AːččI-LAr buru͡o ke iti štobɨ.R komar.R-(n)I kim-LAː-(I)m-IAk-LArIN hugahaː-(I)m-IAk-LArIN bihi͡eke kogda.R eto.R mɨ.R rɨba.R razdelɨvat.R-LAː-Ar-BItIgAr</ta>
            <ta e="T612" id="Seg_4358" s="T608">tak.R i.R mɨ.R žil.R-I</ta>
            <ta e="T619" id="Seg_4359" s="T613">potom.R eto.R što ješo tugu kepseː-IAm-(n)Ij</ta>
            <ta e="T628" id="Seg_4360" s="T620">kim ol saːdik-GA onton iti pensia.R-GA bar-TI-(I)m bu͡o</ta>
            <ta e="T638" id="Seg_4361" s="T629">otut agɨs d’ɨl-(n)I otut alta d’ɨl-(n)I üleleː-BIt-(I)m saːdik-GA</ta>
            <ta e="T648" id="Seg_4362" s="T639">maŋnaj-(n)I škola škola di͡ek üleleː-BIt-(I)m povar.R-GA povarom.R üleleː-BIt-(I)m povar.R-GA</ta>
            <ta e="T662" id="Seg_4363" s="T649">škola-GA onton saːdik-GA-GA ɨl-BIt-LArA *perev-A-LI eh kim-LAː-BIt-LArA egel-BIt-LArA kim onton škola-(t)tAn saːdik-GA üle-LAː-BIt-(I)m</ta>
            <ta e="T670" id="Seg_4364" s="T663">a.R sejčas.R pensia.R-GA olor-A-BIn * vnuk vnuk-LAr-BIn</ta>
            <ta e="T678" id="Seg_4365" s="T671">tože.R *hož-(n)I üleleː-A-BIn üleleː-A hɨrɨt-A-BIn</ta>
            <ta e="T691" id="Seg_4366" s="T679">a.R min ol ogo-(n)I kɨtta olor-A-BIn</ta>
            <ta e="T700" id="Seg_4367" s="T692">ješo *-(I)m-(I)ALAː-An-GI-A süːrbe-LAːk-Ar d’ɨl-LAː-(I)n-IAgA mart.R-GA ol vnuk.R</ta>
            <ta e="T708" id="Seg_4368" s="T701">sejčas.R što to kim-LAː-BIt nemnožko.R ɨ͡arɨt-A-BIt</ta>
            <ta e="T713" id="Seg_4369" s="T709">mama.R-(t)A olor-Ar na *</ta>
            <ta e="T732" id="Seg_4370" s="T714">tak.R kim bu ulakan vnuk.R-(I)m u͡ol-(t)In gi͡en-(t)A ulakan ulakan anɨ služit.R-LAː-Ar kim-GA armia.R-GA služit.R</ta>
            <ta e="T752" id="Seg_4371" s="T733">a.R kɨːs-(t)A kɨːs-(I)m vnuk.R-(I)m ulakan eto.R u͡ol-(I)m * kɨːs-(t)A vnuk.R-(I)m Katja.R aːt-(t)A iti Norilske kim-GA ü͡ören-Ar tože.R mama.R-(t)In kördük</ta>
            <ta e="T768" id="Seg_4372" s="T753">eto.R Norilsk kim-GA texnikum.R-GA ü͡ören-Ar mama.R-(t)In kördük nu.R na iti kim-GA</ta>
            <ta e="T787" id="Seg_4373" s="T769">keːs-BIt-(t)A bihi͡eke i.R *poš-LAː töttörü büt-A štobɨ.R d’ɨl-(t)In hüt-(I)Ar-ImAːrI ke štobɨ.R god.R net.R *pr-o-T-Ar *</ta>
            <ta e="T794" id="Seg_4374" s="T788">ješo dva.R ikki d’ɨl-(n)I naːda ü͡ören-IAk-GA</ta>
            <ta e="T807" id="Seg_4375" s="T795">üs d’ɨl si͡e üle ü͡ören-Ar-LAr</ta>
            <ta e="T813" id="Seg_4376" s="T808">vot.R tak.R vot.R taki͡e.R</ta>
            <ta e="T826" id="Seg_4377" s="T814">biːrges vnuk-BIn *-GIt kɨːs-(I)m u͡ol-(t)In kɨtta vnuk-BIn kɨtta baːr-BIn d’i͡e-BAr</ta>
            <ta e="T830" id="Seg_4378" s="T827">sette-(I)s kɨlaːs-GA ü͡ören-Ar</ta>
            <ta e="T834" id="Seg_4379" s="T831">ješo što govorit.R</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T7" id="Seg_4380" s="T1">Min aːtɨm Iste, hakalɨː Iste di͡eččiler. </ta>
            <ta e="T12" id="Seg_4381" s="T8">Töröːbütüm Popigaj di͡en hirge. </ta>
            <ta e="T22" id="Seg_4382" s="T13">Min kergetterim teːtem kim, Nʼukulaj, maːmam eto Taiska, Taiska. </ta>
            <ta e="T33" id="Seg_4383" s="T23">Ontuŋ maːmam erde bagajɨ ölbüte, onton min teːtebin gɨtta kaːlbɨtɨm. </ta>
            <ta e="T41" id="Seg_4384" s="T34">Teːtem du͡o kimŋe pastuxga brigadiːrga üleliːr ete. </ta>
            <ta e="T48" id="Seg_4385" s="T42">Onton bɨraːtɨm tože teːtebin gɨtta hɨldʼaːččɨ. </ta>
            <ta e="T53" id="Seg_4386" s="T49">Tundraga bihigi köhö hɨldʼaːččɨbɨt. </ta>
            <ta e="T60" id="Seg_4387" s="T54">Školatan kanikulbar kellekpine min teːteber bu͡olaːččɨbɨn. </ta>
            <ta e="T75" id="Seg_4388" s="T61">No ulaːppɨtɨm kenne min teːteber bu͡olbutum, kogda min oččuguj erdekpine kihilerge iːtillibitim, atɨn kihilerge. </ta>
            <ta e="T87" id="Seg_4389" s="T76">Iːtillibitim onton ulaːtan barammɨn ere teːteber bu͡olbutum baraːččɨbɨn teːteber kanikulga kellekpine. </ta>
            <ta e="T102" id="Seg_4390" s="T88">De ol köhö hɨldʼaːččɨbɨt kimten mi͡esteten mi͡estege di͡eri na nartax na etix köhö hɨldʼaːččɨbɨt. </ta>
            <ta e="T122" id="Seg_4391" s="T103">Čumŋa dʼi͡eleːk bu͡olaːččɨbɨt tam sneg eh kaːrɨ kimniːbit ubirajdɨːbɨt, potom mɨ stavim eti palki taki͡e dlinnɨe čum čum oŋorollor. </ta>
            <ta e="T131" id="Seg_4392" s="T123">I eto i eto živem tam v čume. </ta>
            <ta e="T140" id="Seg_4393" s="T132">Heː eto ja po russki uraha dʼi͡ege aha. </ta>
            <ta e="T147" id="Seg_4394" s="T141">Urahalarɨ delajut kimten taki͡e dlinnɨe taki͡e. </ta>
            <ta e="T164" id="Seg_4395" s="T148">Ol dʼi͡eŋ inogda ki͡eŋ bagajɨ oŋorollor ügüs kihi ol gɨnan ügüspüt mnogo že nam ügüspüt ved. </ta>
            <ta e="T171" id="Seg_4396" s="T165">Širokij kim ki͡eŋ dʼi͡e ki͡eŋ dʼuka. </ta>
            <ta e="T182" id="Seg_4397" s="T172">Potom pečkabɨt ohok timir ohok pečka srazu zatopljaem i teplo. </ta>
            <ta e="T200" id="Seg_4398" s="T183">I eto vniz eto kimin annɨtɨn snegom kimniːller (battat-) battɨk oŋorollor battatallar i srazu štobɨ kimneːmi͡egin argɨjɨmɨ͡agɨn. </ta>
            <ta e="T214" id="Seg_4399" s="T201">Daže oččuguj ogoloːk ezdittaːččɨbɨt tam ljulka tam u nix kim haka ljulkaga tuda. </ta>
            <ta e="T223" id="Seg_4400" s="T215">Jejo položat eti rebjonka i ezdjat, bara hɨldʼabɨt. </ta>
            <ta e="T234" id="Seg_4401" s="T224">Onton tugu kepsi͡emij teːtem du Kamennɨj kimŋe storonaga hɨldʼɨbɨta Kamennɨj. </ta>
            <ta e="T248" id="Seg_4402" s="T235">Tam netu kim kaːs da hu͡ok utok netu tam etix tolko odni oleni. </ta>
            <ta e="T261" id="Seg_4403" s="T249">Tam kamennɨe mɨšɨ redko kim kutujaktar kamen kamen pod kamjem inogda bɨvaet. </ta>
            <ta e="T274" id="Seg_4404" s="T262">Ješo tuːguj kim min bejem ü͡öremmitim hette kɨlaːska di͡eri agsɨs kɨlaːstan büppütüm. </ta>
            <ta e="T280" id="Seg_4405" s="T275">Kimi internaːkka onton internaːppɨt bu͡olaːččɨbɨt. </ta>
            <ta e="T289" id="Seg_4406" s="T281">Ikki dʼɨlɨ Norilskaga ü͡öremmippit internaːkka (ž-) kim olorbupput. </ta>
            <ta e="T296" id="Seg_4407" s="T290">Lagerga hɨldʼɨbɨtɨm kimŋe Tajožnajga hɨldʼɨbɨtɨm Kurejkaga. </ta>
            <ta e="T306" id="Seg_4408" s="T297">Potom ulaːtan baran kimŋe (ü͡ör-) eː ol ü͡örene barbɨtɨm. </ta>
            <ta e="T312" id="Seg_4409" s="T307">Na eto na kursɨ kimŋe. </ta>
            <ta e="T321" id="Seg_4410" s="T313">Atɨːlɨːr kihi ke kimŋe prodaveska barbɨtɨm ü͡örene Dudinkaga. </ta>
            <ta e="T338" id="Seg_4411" s="T322">Alta ɨjɨ ü͡öremmitim onton kelbitim i Nosku͡oga (nemno-) kimneːbitim praktikanɨ proxodili i tam kimŋe bufekka üleleːččibin. </ta>
            <ta e="T347" id="Seg_4412" s="T339">Stolovojga bufet ete Nosku͡oga onno etot bufekka üleleːččibin. </ta>
            <ta e="T360" id="Seg_4413" s="T348">Onton v etom Nosku͡oga biːr kihini munna Ki͡etaːtan biːr u͡olu gɨtta dogordosputum. </ta>
            <ta e="T368" id="Seg_4414" s="T361">I mɨ tam poženilis, poznakomilis i poženilis. </ta>
            <ta e="T377" id="Seg_4415" s="T369">Onton ɨlsan barammɨt to kimneːbippit Nosku͡oga oloro tüspütüm. </ta>
            <ta e="T390" id="Seg_4416" s="T378">Min saːdikka üleliːr etim njanjaga a giniŋ du͡o počtaga montjorom üleliːr ete. </ta>
            <ta e="T403" id="Seg_4417" s="T391">Onton u nas rodilsja vot kim kɨːspɨt kɨːhɨm Riːta aːttaːk kɨːhɨm u͡olum. </ta>
            <ta e="T414" id="Seg_4418" s="T404">Onton on što to zaxotel (bɨt-) hiriger keli͡egin rodinatɨgar keli͡egin. </ta>
            <ta e="T422" id="Seg_4419" s="T415">I on menja snačala menja otpravil ɨːppɨta. </ta>
            <ta e="T428" id="Seg_4420" s="T423">Ogobun gɨtta ke kɨːspɨn gɨtta. </ta>
            <ta e="T438" id="Seg_4421" s="T429">I potom tolko gini kennige min kennibitten gini kelbite. </ta>
            <ta e="T456" id="Seg_4422" s="T439">I vot s tex por kas dʼɨl bu͡olla ke šest šestdesjat pervɨj gottan vot ja živu zdes. </ta>
            <ta e="T466" id="Seg_4423" s="T457">I on umer gde to semdesjat devjatɨj gokka ölbüte. </ta>
            <ta e="T481" id="Seg_4424" s="T467">I s tex por zdes min ogolommutum u͡olbun kotorɨj služil eto učilsja v Irkutske. </ta>
            <ta e="T484" id="Seg_4425" s="T482">Služil Usurijskajga. </ta>
            <ta e="T502" id="Seg_4426" s="T485">Nu tak i s tex por živu olorobun saːdikka üleleːbitim do pensia uže saːdikka hubu že saːdikka. </ta>
            <ta e="T518" id="Seg_4427" s="T503">Onton tugu kepsi͡emij onton erim du͡o ol erim rɨbalkaga bu͡olaːččɨ itinne (to-) točkaga oloroːččubut čugas. </ta>
            <ta e="T528" id="Seg_4428" s="T519">Kogda u menja vɨxodnɨe vɨxodnojdarbar gini͡eke baraːččɨbɨn motorunan ezditaːččɨbɨt. </ta>
            <ta e="T541" id="Seg_4429" s="T529">I tam ja kim kömölöhöːččübün rɨbu razdel kimni vɨdelɨval eto razdelɨvajdɨː kanʼɨː. </ta>
            <ta e="T542" id="Seg_4430" s="T541">Kanʼiː. </ta>
            <ta e="T550" id="Seg_4431" s="T543">Bi͡ereːččibit že eto kimŋe rɨbzavokka bi͡ereːččibit, sdavajdaːččɨbɨt. </ta>
            <ta e="T555" id="Seg_4432" s="T551">Oni lovili mnogo rɨbɨ. </ta>
            <ta e="T564" id="Seg_4433" s="T556">Utrom rano baraːččɨlar potom seti proverjat pozdno pri͡ezzhajut. </ta>
            <ta e="T570" id="Seg_4434" s="T565">Počti vot (sid-) oloroːččubut tüːnneri. </ta>
            <ta e="T583" id="Seg_4435" s="T571">Kumar letom komarka hi͡ete hi͡ete ol ogolorbut ke u͡olattarbut, sɨn moj u͡olum. </ta>
            <ta e="T607" id="Seg_4436" s="T583">Onton tam ješo dogottoro erim sestratɨn u͡ola ol (ka-) u͡otu oŋoroːččular buru͡o ke iti štobɨ komarɨ kimneːmi͡ekterin čugahaːmɨ͡aktarɨn bihi͡eke, kogda eto mɨ rɨbu razdelajdɨːrbɨtɨgar. </ta>
            <ta e="T612" id="Seg_4437" s="T608">Tak i mɨ žili. </ta>
            <ta e="T619" id="Seg_4438" s="T613">Potom eto što ješo tugu kepsi͡emij? </ta>
            <ta e="T628" id="Seg_4439" s="T620">Kim ol saːdikka onton iti pensiaga bardɨm bu͡o. </ta>
            <ta e="T638" id="Seg_4440" s="T629">Tridtsat otut agɨs dʼɨlɨ otut alta dʼɨlɨ üleleːbitim saːdikka. </ta>
            <ta e="T648" id="Seg_4441" s="T639">Maŋnajɨ šku͡ola šku͡ola di͡et üleleːbitim povarga povarom üleleːbitim povarga. </ta>
            <ta e="T662" id="Seg_4442" s="T649">Školaga onton saːdikkaga ɨlbɨttara pereveli eh kimneːbittere egelbittere kim onton šku͡olattan saːdikka üleleːbitim. </ta>
            <ta e="T670" id="Seg_4443" s="T663">A sejčas pensiaga olorobun, vospitɨvaju vnuk, vnuktarbɨn. </ta>
            <ta e="T678" id="Seg_4444" s="T671">Pravnučkajbun sižu tože hožu üleliːbin üleliː hɨldʼabɨn. </ta>
            <ta e="T691" id="Seg_4445" s="T679">A nevestka na rabotu naːda idti a min ol ogonɨ gɨtta olorobun. </ta>
            <ta e="T700" id="Seg_4446" s="T692">Ješo malenki͡e hüːrbeleːger dʼɨllaːnɨ͡aga markka ol (vnu-) vnučkam. </ta>
            <ta e="T708" id="Seg_4447" s="T701">Sejčas što to kimneːbit nemnožko ɨ͡aldʼabɨt stomatittammɨt. </ta>
            <ta e="T713" id="Seg_4448" s="T709">Maːmata oloror na bolničnom. </ta>
            <ta e="T732" id="Seg_4449" s="T714">Tak kim bu ulakan vnugum u͡olun gi͡ene ulakan kotorɨj eto staršij vnugum ulakan anɨ služittɨːr kimŋe armiaga služit. </ta>
            <ta e="T733" id="Seg_4450" s="T732">Aŋardar ü͡öreneller.</ta>
            <ta e="T752" id="Seg_4451" s="T733">A kɨːha kɨːhɨm vnučkam ulakan eto u͡olum (vnučk-) kɨːha, vnučkam, Katja aːta, iti Norilske kimŋe ü͡örener tože maːmatɨn kördük. ŋaŋ</ta>
            <ta e="T768" id="Seg_4452" s="T753">Eto Norilskaj kimŋe tʼexnʼikumŋa ü͡örener maːmatɨn kördük, nu na šitjo tam na oformleni͡e iti kimŋe. </ta>
            <ta e="T787" id="Seg_4453" s="T769">Rodila rebjonka keːspite bihi͡eke i pošla poehala töttörü büte štob dʼɨlɨn hüterimeːri ke štobɨ god ne propal, zakančivat. </ta>
            <ta e="T794" id="Seg_4454" s="T788">Ješo dva ikki dʼɨlɨ naːda ü͡öreni͡ekke. </ta>
            <ta e="T807" id="Seg_4455" s="T795">Tri goda s čem to üs dʼɨl s čem to üle ü͡öreneller. </ta>
            <ta e="T813" id="Seg_4456" s="T808">Vot tak vot taki͡e dela. </ta>
            <ta e="T826" id="Seg_4457" s="T814">Žhivu odna biːrges vnukbɨn (gɨt-) kɨːhɨm u͡olun gɨtta vnukbɨn gɨtta baːrbɨn di͡eber. </ta>
            <ta e="T830" id="Seg_4458" s="T827">Hettis kɨlaːska ü͡örener. </ta>
            <ta e="T834" id="Seg_4459" s="T831">Ješo što govorit. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_4460" s="T1">min</ta>
            <ta e="T3" id="Seg_4461" s="T2">aːt-ɨ-m</ta>
            <ta e="T4" id="Seg_4462" s="T3">Iste</ta>
            <ta e="T5" id="Seg_4463" s="T4">haka-lɨː</ta>
            <ta e="T6" id="Seg_4464" s="T5">Iste</ta>
            <ta e="T7" id="Seg_4465" s="T6">di͡e-čči-ler</ta>
            <ta e="T9" id="Seg_4466" s="T8">töröː-büt-ü-m</ta>
            <ta e="T10" id="Seg_4467" s="T9">Popigaj</ta>
            <ta e="T11" id="Seg_4468" s="T10">di͡e-n</ta>
            <ta e="T12" id="Seg_4469" s="T11">hir-ge</ta>
            <ta e="T14" id="Seg_4470" s="T13">min</ta>
            <ta e="T15" id="Seg_4471" s="T14">kerget-ter-i-m</ta>
            <ta e="T16" id="Seg_4472" s="T15">teːte-m</ta>
            <ta e="T17" id="Seg_4473" s="T16">kim</ta>
            <ta e="T18" id="Seg_4474" s="T17">Nʼukulaj</ta>
            <ta e="T19" id="Seg_4475" s="T18">maːma-m</ta>
            <ta e="T20" id="Seg_4476" s="T19">eto</ta>
            <ta e="T21" id="Seg_4477" s="T20">Taiska</ta>
            <ta e="T22" id="Seg_4478" s="T21">Taiska</ta>
            <ta e="T24" id="Seg_4479" s="T23">on-tu-ŋ</ta>
            <ta e="T25" id="Seg_4480" s="T24">maːma-m</ta>
            <ta e="T26" id="Seg_4481" s="T25">erde</ta>
            <ta e="T27" id="Seg_4482" s="T26">bagajɨ</ta>
            <ta e="T28" id="Seg_4483" s="T27">öl-büt-e</ta>
            <ta e="T29" id="Seg_4484" s="T28">on-ton</ta>
            <ta e="T30" id="Seg_4485" s="T29">min</ta>
            <ta e="T31" id="Seg_4486" s="T30">teːte-bi-n</ta>
            <ta e="T32" id="Seg_4487" s="T31">gɨtta</ta>
            <ta e="T33" id="Seg_4488" s="T32">kaːl-bɨt-ɨ-m</ta>
            <ta e="T35" id="Seg_4489" s="T34">teːte-m</ta>
            <ta e="T36" id="Seg_4490" s="T35">du͡o</ta>
            <ta e="T37" id="Seg_4491" s="T36">kim-ŋe</ta>
            <ta e="T38" id="Seg_4492" s="T37">pastux-ga</ta>
            <ta e="T39" id="Seg_4493" s="T38">brigadiːr-ga</ta>
            <ta e="T40" id="Seg_4494" s="T39">üleliː-r</ta>
            <ta e="T41" id="Seg_4495" s="T40">e-t-e</ta>
            <ta e="T43" id="Seg_4496" s="T42">on-ton</ta>
            <ta e="T44" id="Seg_4497" s="T43">bɨraːt-ɨ-m</ta>
            <ta e="T45" id="Seg_4498" s="T44">tože</ta>
            <ta e="T46" id="Seg_4499" s="T45">teːte-bi-n</ta>
            <ta e="T47" id="Seg_4500" s="T46">gɨtta</ta>
            <ta e="T48" id="Seg_4501" s="T47">hɨldʼ-aːččɨ</ta>
            <ta e="T50" id="Seg_4502" s="T49">tundra-ga</ta>
            <ta e="T51" id="Seg_4503" s="T50">bihigi</ta>
            <ta e="T52" id="Seg_4504" s="T51">köh-ö</ta>
            <ta e="T53" id="Seg_4505" s="T52">hɨldʼ-aːččɨ-bɨt</ta>
            <ta e="T55" id="Seg_4506" s="T54">škola-tan</ta>
            <ta e="T56" id="Seg_4507" s="T55">kanikul-ba-r</ta>
            <ta e="T57" id="Seg_4508" s="T56">kel-lek-pine</ta>
            <ta e="T58" id="Seg_4509" s="T57">min</ta>
            <ta e="T59" id="Seg_4510" s="T58">teːte-be-r</ta>
            <ta e="T60" id="Seg_4511" s="T59">bu͡ol-aːččɨ-bɨn</ta>
            <ta e="T62" id="Seg_4512" s="T61">no</ta>
            <ta e="T63" id="Seg_4513" s="T62">ulaːp-pɨt-ɨ-m</ta>
            <ta e="T64" id="Seg_4514" s="T63">kenne</ta>
            <ta e="T65" id="Seg_4515" s="T64">min</ta>
            <ta e="T66" id="Seg_4516" s="T65">teːte-be-r</ta>
            <ta e="T67" id="Seg_4517" s="T66">bu͡ol-but-u-m</ta>
            <ta e="T68" id="Seg_4518" s="T67">kogda</ta>
            <ta e="T69" id="Seg_4519" s="T68">min</ta>
            <ta e="T70" id="Seg_4520" s="T69">oččuguj</ta>
            <ta e="T71" id="Seg_4521" s="T70">er-dek-pine</ta>
            <ta e="T72" id="Seg_4522" s="T71">kihi-ler-ge</ta>
            <ta e="T73" id="Seg_4523" s="T72">iːt-i-ll-i-bit-i-m</ta>
            <ta e="T74" id="Seg_4524" s="T73">atɨn</ta>
            <ta e="T75" id="Seg_4525" s="T74">kihi-ler-ge</ta>
            <ta e="T77" id="Seg_4526" s="T76">iːt-i-ll-i-bit-i-m</ta>
            <ta e="T78" id="Seg_4527" s="T77">on-ton</ta>
            <ta e="T79" id="Seg_4528" s="T78">ulaːt-an</ta>
            <ta e="T80" id="Seg_4529" s="T79">bar-am-mɨn</ta>
            <ta e="T81" id="Seg_4530" s="T80">ere</ta>
            <ta e="T82" id="Seg_4531" s="T81">teːte-be-r</ta>
            <ta e="T83" id="Seg_4532" s="T82">bu͡ol-but-u-m</ta>
            <ta e="T84" id="Seg_4533" s="T83">bar-aːččɨ-bɨn</ta>
            <ta e="T85" id="Seg_4534" s="T84">teːte-be-r</ta>
            <ta e="T86" id="Seg_4535" s="T85">kanikul-ga</ta>
            <ta e="T87" id="Seg_4536" s="T86">kel-lek-pine</ta>
            <ta e="T89" id="Seg_4537" s="T88">de</ta>
            <ta e="T90" id="Seg_4538" s="T89">ol</ta>
            <ta e="T91" id="Seg_4539" s="T90">köh-ö</ta>
            <ta e="T92" id="Seg_4540" s="T91">hɨldʼ-aːččɨ-bɨt</ta>
            <ta e="T93" id="Seg_4541" s="T92">kim-ten</ta>
            <ta e="T94" id="Seg_4542" s="T93">mi͡este-ten</ta>
            <ta e="T95" id="Seg_4543" s="T94">mi͡este-ge</ta>
            <ta e="T96" id="Seg_4544" s="T95">di͡eri</ta>
            <ta e="T101" id="Seg_4545" s="T100">köh-ö</ta>
            <ta e="T102" id="Seg_4546" s="T101">hɨldʼ-aːččɨ-bɨt</ta>
            <ta e="T104" id="Seg_4547" s="T103">čum-ŋa</ta>
            <ta e="T105" id="Seg_4548" s="T104">dʼi͡e-leːk</ta>
            <ta e="T106" id="Seg_4549" s="T105">bu͡ol-aːččɨ-bɨt</ta>
            <ta e="T107" id="Seg_4550" s="T106">tam</ta>
            <ta e="T109" id="Seg_4551" s="T108">eh</ta>
            <ta e="T110" id="Seg_4552" s="T109">kaːr-ɨ</ta>
            <ta e="T111" id="Seg_4553" s="T110">kim-n-iː-bit</ta>
            <ta e="T112" id="Seg_4554" s="T111">ubiraj-d-ɨː-bɨt</ta>
            <ta e="T120" id="Seg_4555" s="T119">čum</ta>
            <ta e="T121" id="Seg_4556" s="T120">čum</ta>
            <ta e="T122" id="Seg_4557" s="T121">oŋor-ol-lor</ta>
            <ta e="T133" id="Seg_4558" s="T132">heː</ta>
            <ta e="T138" id="Seg_4559" s="T137">uraha</ta>
            <ta e="T139" id="Seg_4560" s="T138">dʼi͡e-ge</ta>
            <ta e="T140" id="Seg_4561" s="T139">aha</ta>
            <ta e="T142" id="Seg_4562" s="T141">uraha-lar-ɨ</ta>
            <ta e="T144" id="Seg_4563" s="T143">kim-ten</ta>
            <ta e="T149" id="Seg_4564" s="T148">ol</ta>
            <ta e="T150" id="Seg_4565" s="T149">dʼi͡e-ŋ</ta>
            <ta e="T151" id="Seg_4566" s="T150">inogda</ta>
            <ta e="T152" id="Seg_4567" s="T151">ki͡eŋ</ta>
            <ta e="T153" id="Seg_4568" s="T152">bagajɨ</ta>
            <ta e="T154" id="Seg_4569" s="T153">oŋor-ol-lor</ta>
            <ta e="T155" id="Seg_4570" s="T154">ügüs</ta>
            <ta e="T156" id="Seg_4571" s="T155">kihi</ta>
            <ta e="T157" id="Seg_4572" s="T156">ol</ta>
            <ta e="T158" id="Seg_4573" s="T157">gɨn-an</ta>
            <ta e="T159" id="Seg_4574" s="T158">ügüs-püt</ta>
            <ta e="T163" id="Seg_4575" s="T162">ügüs-püt</ta>
            <ta e="T164" id="Seg_4576" s="T163">ved</ta>
            <ta e="T167" id="Seg_4577" s="T166">kim</ta>
            <ta e="T168" id="Seg_4578" s="T167">ki͡eŋ</ta>
            <ta e="T169" id="Seg_4579" s="T168">dʼi͡e</ta>
            <ta e="T170" id="Seg_4580" s="T169">ki͡eŋ</ta>
            <ta e="T171" id="Seg_4581" s="T170">dʼuka</ta>
            <ta e="T173" id="Seg_4582" s="T172">potom</ta>
            <ta e="T174" id="Seg_4583" s="T173">pečka-bɨt</ta>
            <ta e="T175" id="Seg_4584" s="T174">ohok</ta>
            <ta e="T176" id="Seg_4585" s="T175">timir</ta>
            <ta e="T177" id="Seg_4586" s="T176">ohok</ta>
            <ta e="T188" id="Seg_4587" s="T187">kim-i-n</ta>
            <ta e="T189" id="Seg_4588" s="T188">annɨ-tɨ-n</ta>
            <ta e="T191" id="Seg_4589" s="T190">kim-niː-l-ler</ta>
            <ta e="T193" id="Seg_4590" s="T192">bat-tɨk</ta>
            <ta e="T194" id="Seg_4591" s="T193">oŋor-ol-lor</ta>
            <ta e="T195" id="Seg_4592" s="T194">batta-t-al-lar</ta>
            <ta e="T199" id="Seg_4593" s="T198">kim-neː-m-i͡eg-i-n</ta>
            <ta e="T200" id="Seg_4594" s="T199">argɨj-ɨ-m-ɨ͡ag-ɨ-n</ta>
            <ta e="T202" id="Seg_4595" s="T201">daže</ta>
            <ta e="T203" id="Seg_4596" s="T202">oččuguj</ta>
            <ta e="T204" id="Seg_4597" s="T203">ogo-loːk</ta>
            <ta e="T205" id="Seg_4598" s="T204">ezdit-taː-ččɨ-bɨt</ta>
            <ta e="T211" id="Seg_4599" s="T210">kim</ta>
            <ta e="T212" id="Seg_4600" s="T211">haka</ta>
            <ta e="T213" id="Seg_4601" s="T212">ljulka-ga</ta>
            <ta e="T222" id="Seg_4602" s="T221">bar-a</ta>
            <ta e="T223" id="Seg_4603" s="T222">hɨldʼ-a-bɨt</ta>
            <ta e="T225" id="Seg_4604" s="T224">on-ton</ta>
            <ta e="T226" id="Seg_4605" s="T225">tug-u</ta>
            <ta e="T227" id="Seg_4606" s="T226">keps-i͡e-m=ij</ta>
            <ta e="T228" id="Seg_4607" s="T227">teːte-m</ta>
            <ta e="T229" id="Seg_4608" s="T228">du</ta>
            <ta e="T230" id="Seg_4609" s="T229">Kamennɨj</ta>
            <ta e="T231" id="Seg_4610" s="T230">kim-ŋe</ta>
            <ta e="T232" id="Seg_4611" s="T231">storona-ga</ta>
            <ta e="T233" id="Seg_4612" s="T232">hɨldʼ-ɨ-bɨt-a</ta>
            <ta e="T234" id="Seg_4613" s="T233">Kamennɨj</ta>
            <ta e="T238" id="Seg_4614" s="T237">kim</ta>
            <ta e="T239" id="Seg_4615" s="T238">kaːs</ta>
            <ta e="T240" id="Seg_4616" s="T239">da</ta>
            <ta e="T241" id="Seg_4617" s="T240">hu͡ok</ta>
            <ta e="T254" id="Seg_4618" s="T253">kim</ta>
            <ta e="T255" id="Seg_4619" s="T254">kutujak-tar</ta>
            <ta e="T263" id="Seg_4620" s="T262">ješo</ta>
            <ta e="T264" id="Seg_4621" s="T263">tuːg-u=j</ta>
            <ta e="T265" id="Seg_4622" s="T264">kim</ta>
            <ta e="T266" id="Seg_4623" s="T265">min</ta>
            <ta e="T267" id="Seg_4624" s="T266">beje-m</ta>
            <ta e="T268" id="Seg_4625" s="T267">ü͡örem-mit-i-m</ta>
            <ta e="T269" id="Seg_4626" s="T268">hette</ta>
            <ta e="T270" id="Seg_4627" s="T269">kɨlaːs-ka</ta>
            <ta e="T271" id="Seg_4628" s="T270">di͡eri</ta>
            <ta e="T272" id="Seg_4629" s="T271">ags-ɨs</ta>
            <ta e="T273" id="Seg_4630" s="T272">kɨlaːs-tan</ta>
            <ta e="T274" id="Seg_4631" s="T273">büp-püt-ü-m</ta>
            <ta e="T276" id="Seg_4632" s="T275">kim-i</ta>
            <ta e="T277" id="Seg_4633" s="T276">internaːk-ka</ta>
            <ta e="T278" id="Seg_4634" s="T277">on-ton</ta>
            <ta e="T279" id="Seg_4635" s="T278">internaːp-pɨt</ta>
            <ta e="T280" id="Seg_4636" s="T279">bu͡ol-aːččɨ-bɨt</ta>
            <ta e="T282" id="Seg_4637" s="T281">ikki</ta>
            <ta e="T283" id="Seg_4638" s="T282">dʼɨl-ɨ</ta>
            <ta e="T284" id="Seg_4639" s="T283">Norilska-ga</ta>
            <ta e="T285" id="Seg_4640" s="T284">ü͡örem-mip-pit</ta>
            <ta e="T286" id="Seg_4641" s="T285">internaːk-ka</ta>
            <ta e="T288" id="Seg_4642" s="T287">kim</ta>
            <ta e="T289" id="Seg_4643" s="T288">olor-bup-put</ta>
            <ta e="T291" id="Seg_4644" s="T290">lager-ga</ta>
            <ta e="T292" id="Seg_4645" s="T291">hɨldʼ-ɨ-bɨt-ɨ-m</ta>
            <ta e="T293" id="Seg_4646" s="T292">kim-ŋe</ta>
            <ta e="T294" id="Seg_4647" s="T293">Tajožnaj-ga</ta>
            <ta e="T295" id="Seg_4648" s="T294">hɨldʼ-ɨ-bɨt-ɨ-m</ta>
            <ta e="T296" id="Seg_4649" s="T295">Kurejka-ga</ta>
            <ta e="T298" id="Seg_4650" s="T297">potom</ta>
            <ta e="T299" id="Seg_4651" s="T298">ulaːt-an</ta>
            <ta e="T300" id="Seg_4652" s="T299">baran</ta>
            <ta e="T301" id="Seg_4653" s="T300">kim-ŋe</ta>
            <ta e="T303" id="Seg_4654" s="T302">eː</ta>
            <ta e="T304" id="Seg_4655" s="T303">ol</ta>
            <ta e="T305" id="Seg_4656" s="T304">ü͡ören-e</ta>
            <ta e="T306" id="Seg_4657" s="T305">bar-bɨt-ɨ-m</ta>
            <ta e="T312" id="Seg_4658" s="T311">kim-ŋe</ta>
            <ta e="T314" id="Seg_4659" s="T313">atɨːlɨː-r</ta>
            <ta e="T315" id="Seg_4660" s="T314">kihi</ta>
            <ta e="T316" id="Seg_4661" s="T315">ke</ta>
            <ta e="T317" id="Seg_4662" s="T316">kim-ŋe</ta>
            <ta e="T318" id="Seg_4663" s="T317">prodaves-ka</ta>
            <ta e="T319" id="Seg_4664" s="T318">bar-bɨt-ɨ-m</ta>
            <ta e="T320" id="Seg_4665" s="T319">ü͡ören-e</ta>
            <ta e="T321" id="Seg_4666" s="T320">Dudʼinka-ga</ta>
            <ta e="T323" id="Seg_4667" s="T322">alta</ta>
            <ta e="T324" id="Seg_4668" s="T323">ɨj-ɨ</ta>
            <ta e="T325" id="Seg_4669" s="T324">ü͡örem-mit-i-m</ta>
            <ta e="T326" id="Seg_4670" s="T325">on-ton</ta>
            <ta e="T327" id="Seg_4671" s="T326">kel-bit-i-m</ta>
            <ta e="T328" id="Seg_4672" s="T327">i</ta>
            <ta e="T329" id="Seg_4673" s="T328">Nosku͡o-ga</ta>
            <ta e="T331" id="Seg_4674" s="T330">kim-neː-bit-i-m</ta>
            <ta e="T332" id="Seg_4675" s="T331">praktika-nɨ</ta>
            <ta e="T336" id="Seg_4676" s="T335">kim-ŋe</ta>
            <ta e="T337" id="Seg_4677" s="T336">bufek-ka</ta>
            <ta e="T338" id="Seg_4678" s="T337">üleleː-čči-bin</ta>
            <ta e="T340" id="Seg_4679" s="T339">stolovoj-ga</ta>
            <ta e="T341" id="Seg_4680" s="T340">bufet</ta>
            <ta e="T342" id="Seg_4681" s="T341">e-t-e</ta>
            <ta e="T343" id="Seg_4682" s="T342">Nosku͡o-ga</ta>
            <ta e="T344" id="Seg_4683" s="T343">onno</ta>
            <ta e="T346" id="Seg_4684" s="T345">bufek-ka</ta>
            <ta e="T347" id="Seg_4685" s="T346">üleleː-čči-bin</ta>
            <ta e="T349" id="Seg_4686" s="T348">on-ton</ta>
            <ta e="T352" id="Seg_4687" s="T351">Nosku͡o-ga</ta>
            <ta e="T353" id="Seg_4688" s="T352">biːr</ta>
            <ta e="T354" id="Seg_4689" s="T353">kihi-ni</ta>
            <ta e="T355" id="Seg_4690" s="T354">munna</ta>
            <ta e="T356" id="Seg_4691" s="T355">Ki͡etaː-tan</ta>
            <ta e="T357" id="Seg_4692" s="T356">biːr</ta>
            <ta e="T358" id="Seg_4693" s="T357">u͡ol-u</ta>
            <ta e="T359" id="Seg_4694" s="T358">gɨtta</ta>
            <ta e="T360" id="Seg_4695" s="T359">dogor-do-s-put-u-m</ta>
            <ta e="T370" id="Seg_4696" s="T369">on-ton</ta>
            <ta e="T371" id="Seg_4697" s="T370">ɨl-s-an</ta>
            <ta e="T372" id="Seg_4698" s="T371">bar-am-mɨt</ta>
            <ta e="T373" id="Seg_4699" s="T372">to</ta>
            <ta e="T374" id="Seg_4700" s="T373">kim-neː-bip-pit</ta>
            <ta e="T375" id="Seg_4701" s="T374">Nosku͡o-ga</ta>
            <ta e="T376" id="Seg_4702" s="T375">olor-o</ta>
            <ta e="T377" id="Seg_4703" s="T376">tüs-püt-ü-m</ta>
            <ta e="T379" id="Seg_4704" s="T378">min</ta>
            <ta e="T380" id="Seg_4705" s="T379">saːdik-ka</ta>
            <ta e="T381" id="Seg_4706" s="T380">üleliː-r</ta>
            <ta e="T382" id="Seg_4707" s="T381">e-ti-m</ta>
            <ta e="T383" id="Seg_4708" s="T382">njanja-ga</ta>
            <ta e="T384" id="Seg_4709" s="T383">a</ta>
            <ta e="T385" id="Seg_4710" s="T384">gini-ŋ</ta>
            <ta e="T386" id="Seg_4711" s="T385">du͡o</ta>
            <ta e="T387" id="Seg_4712" s="T386">počta-ga</ta>
            <ta e="T389" id="Seg_4713" s="T388">üleliː-r</ta>
            <ta e="T390" id="Seg_4714" s="T389">e-t-e</ta>
            <ta e="T392" id="Seg_4715" s="T391">on-ton</ta>
            <ta e="T397" id="Seg_4716" s="T396">kim</ta>
            <ta e="T398" id="Seg_4717" s="T397">kɨːs-pɨt</ta>
            <ta e="T399" id="Seg_4718" s="T398">kɨːh-ɨ-m</ta>
            <ta e="T400" id="Seg_4719" s="T399">Riːta</ta>
            <ta e="T401" id="Seg_4720" s="T400">aːt-taːk</ta>
            <ta e="T402" id="Seg_4721" s="T401">kɨːh-ɨ-m</ta>
            <ta e="T403" id="Seg_4722" s="T402">u͡ol-u-m</ta>
            <ta e="T405" id="Seg_4723" s="T404">on-ton</ta>
            <ta e="T411" id="Seg_4724" s="T410">hir-i-ger</ta>
            <ta e="T412" id="Seg_4725" s="T411">kel-i͡eg-i-n</ta>
            <ta e="T413" id="Seg_4726" s="T412">rodina-tɨ-gar</ta>
            <ta e="T414" id="Seg_4727" s="T413">kel-i͡eg-i-n</ta>
            <ta e="T422" id="Seg_4728" s="T421">ɨːp-pɨt-a</ta>
            <ta e="T424" id="Seg_4729" s="T423">ogo-bu-n</ta>
            <ta e="T425" id="Seg_4730" s="T424">gɨtta</ta>
            <ta e="T426" id="Seg_4731" s="T425">ke</ta>
            <ta e="T427" id="Seg_4732" s="T426">kɨːs-pɨ-n</ta>
            <ta e="T428" id="Seg_4733" s="T427">gɨtta</ta>
            <ta e="T433" id="Seg_4734" s="T432">gini</ta>
            <ta e="T434" id="Seg_4735" s="T433">kenni-ge</ta>
            <ta e="T435" id="Seg_4736" s="T434">min</ta>
            <ta e="T436" id="Seg_4737" s="T435">kenn-i-bi-tten</ta>
            <ta e="T437" id="Seg_4738" s="T436">gini</ta>
            <ta e="T438" id="Seg_4739" s="T437">kel-bit-e</ta>
            <ta e="T445" id="Seg_4740" s="T444">kas</ta>
            <ta e="T446" id="Seg_4741" s="T445">dʼɨl</ta>
            <ta e="T447" id="Seg_4742" s="T446">bu͡olla</ta>
            <ta e="T448" id="Seg_4743" s="T447">ke</ta>
            <ta e="T452" id="Seg_4744" s="T451">got-tan</ta>
            <ta e="T465" id="Seg_4745" s="T464">gok-ka</ta>
            <ta e="T466" id="Seg_4746" s="T465">öl-büt-e</ta>
            <ta e="T473" id="Seg_4747" s="T472">min</ta>
            <ta e="T474" id="Seg_4748" s="T473">ogo-lom-mut-u-m</ta>
            <ta e="T475" id="Seg_4749" s="T474">u͡ol-bu-n</ta>
            <ta e="T484" id="Seg_4750" s="T483">Usurijskaj-ga</ta>
            <ta e="T493" id="Seg_4751" s="T492">olor-o-bun</ta>
            <ta e="T494" id="Seg_4752" s="T493">saːdik-ka</ta>
            <ta e="T495" id="Seg_4753" s="T494">üleleː-bit-i-m</ta>
            <ta e="T499" id="Seg_4754" s="T498">saːdik-ka</ta>
            <ta e="T500" id="Seg_4755" s="T499">hubu</ta>
            <ta e="T501" id="Seg_4756" s="T500">že</ta>
            <ta e="T502" id="Seg_4757" s="T501">saːdik-ka</ta>
            <ta e="T504" id="Seg_4758" s="T503">on-ton</ta>
            <ta e="T505" id="Seg_4759" s="T504">tug-u</ta>
            <ta e="T506" id="Seg_4760" s="T505">keps-i͡e-m=ij</ta>
            <ta e="T507" id="Seg_4761" s="T506">on-ton</ta>
            <ta e="T508" id="Seg_4762" s="T507">er-i-m</ta>
            <ta e="T509" id="Seg_4763" s="T508">du͡o</ta>
            <ta e="T510" id="Seg_4764" s="T509">ol</ta>
            <ta e="T511" id="Seg_4765" s="T510">er-i-m</ta>
            <ta e="T512" id="Seg_4766" s="T511">rɨbalka-ga</ta>
            <ta e="T513" id="Seg_4767" s="T512">bu͡ol-aːččɨ</ta>
            <ta e="T514" id="Seg_4768" s="T513">itinne</ta>
            <ta e="T516" id="Seg_4769" s="T515">točka-ga</ta>
            <ta e="T517" id="Seg_4770" s="T516">olor-oːčču-but</ta>
            <ta e="T518" id="Seg_4771" s="T517">čugas</ta>
            <ta e="T524" id="Seg_4772" s="T523">vɨxodnoj-dar-ba-r</ta>
            <ta e="T525" id="Seg_4773" s="T524">gini͡e-ke</ta>
            <ta e="T526" id="Seg_4774" s="T525">bar-aːččɨ-bɨn</ta>
            <ta e="T527" id="Seg_4775" s="T526">motor-u-nan</ta>
            <ta e="T528" id="Seg_4776" s="T527">ezdi-taː-ččɨ-bɨt</ta>
            <ta e="T533" id="Seg_4777" s="T532">kim</ta>
            <ta e="T534" id="Seg_4778" s="T533">kömölöh-öːččü-bün</ta>
            <ta e="T537" id="Seg_4779" s="T536">kim-ni</ta>
            <ta e="T540" id="Seg_4780" s="T539">razdelɨvaj-d-ɨː</ta>
            <ta e="T541" id="Seg_4781" s="T540">kanʼ-ɨː</ta>
            <ta e="T542" id="Seg_4782" s="T541">kanʼ-iː</ta>
            <ta e="T544" id="Seg_4783" s="T543">bi͡er-eːčči-bit</ta>
            <ta e="T545" id="Seg_4784" s="T544">že</ta>
            <ta e="T546" id="Seg_4785" s="T545">eto</ta>
            <ta e="T547" id="Seg_4786" s="T546">kim-ŋe</ta>
            <ta e="T548" id="Seg_4787" s="T547">rɨbzavok-ka</ta>
            <ta e="T549" id="Seg_4788" s="T548">bi͡er-eːčči-bit</ta>
            <ta e="T550" id="Seg_4789" s="T549">sdavaj-daː-ččɨ-bɨt</ta>
            <ta e="T559" id="Seg_4790" s="T558">bar-aːččɨ-lar</ta>
            <ta e="T569" id="Seg_4791" s="T568">olor-oːčču-but</ta>
            <ta e="T570" id="Seg_4792" s="T569">tüːn-ner-i</ta>
            <ta e="T575" id="Seg_4793" s="T574">hi͡e-t-e</ta>
            <ta e="T576" id="Seg_4794" s="T575">hi͡e-t-e</ta>
            <ta e="T577" id="Seg_4795" s="T576">ol</ta>
            <ta e="T578" id="Seg_4796" s="T577">ogo-lor-but</ta>
            <ta e="T579" id="Seg_4797" s="T578">ke</ta>
            <ta e="T580" id="Seg_4798" s="T579">u͡olat-tar-but</ta>
            <ta e="T583" id="Seg_4799" s="T582">u͡ol-u-m</ta>
            <ta e="T584" id="Seg_4800" s="T583">on-ton</ta>
            <ta e="T585" id="Seg_4801" s="T584">tam</ta>
            <ta e="T586" id="Seg_4802" s="T585">ješo</ta>
            <ta e="T587" id="Seg_4803" s="T586">dogot-tor-o</ta>
            <ta e="T588" id="Seg_4804" s="T587">er-i-m</ta>
            <ta e="T589" id="Seg_4805" s="T588">sestra-tɨ-n</ta>
            <ta e="T590" id="Seg_4806" s="T589">u͡ol-a</ta>
            <ta e="T591" id="Seg_4807" s="T590">ol</ta>
            <ta e="T593" id="Seg_4808" s="T592">u͡ot-u</ta>
            <ta e="T594" id="Seg_4809" s="T593">oŋor-oːčču-lar</ta>
            <ta e="T595" id="Seg_4810" s="T594">buru͡o</ta>
            <ta e="T596" id="Seg_4811" s="T595">ke</ta>
            <ta e="T597" id="Seg_4812" s="T596">iti</ta>
            <ta e="T600" id="Seg_4813" s="T599">kim-neː-m-i͡ek-teri-n</ta>
            <ta e="T601" id="Seg_4814" s="T600">čugahaː-m-ɨ͡ak-tarɨ-n</ta>
            <ta e="T602" id="Seg_4815" s="T601">bihi͡e-ke</ta>
            <ta e="T607" id="Seg_4816" s="T606">razdelaj-dɨː-r-bɨtɨ-gar</ta>
            <ta e="T618" id="Seg_4817" s="T617">tug-u</ta>
            <ta e="T619" id="Seg_4818" s="T618">keps-i͡e-m=ij</ta>
            <ta e="T621" id="Seg_4819" s="T620">kim</ta>
            <ta e="T622" id="Seg_4820" s="T621">ol</ta>
            <ta e="T623" id="Seg_4821" s="T622">saːdik-ka</ta>
            <ta e="T624" id="Seg_4822" s="T623">on-ton</ta>
            <ta e="T625" id="Seg_4823" s="T624">iti</ta>
            <ta e="T626" id="Seg_4824" s="T625">pensia-ga</ta>
            <ta e="T627" id="Seg_4825" s="T626">bar-dɨ-m</ta>
            <ta e="T628" id="Seg_4826" s="T627">bu͡o</ta>
            <ta e="T631" id="Seg_4827" s="T630">otut</ta>
            <ta e="T632" id="Seg_4828" s="T631">agɨs</ta>
            <ta e="T633" id="Seg_4829" s="T632">dʼɨl-ɨ</ta>
            <ta e="T634" id="Seg_4830" s="T633">otut</ta>
            <ta e="T635" id="Seg_4831" s="T634">alta</ta>
            <ta e="T636" id="Seg_4832" s="T635">dʼɨl-ɨ</ta>
            <ta e="T637" id="Seg_4833" s="T636">üleleː-bit-i-m</ta>
            <ta e="T638" id="Seg_4834" s="T637">saːdik-ka</ta>
            <ta e="T640" id="Seg_4835" s="T639">maŋnaj-ɨ</ta>
            <ta e="T641" id="Seg_4836" s="T640">šku͡ola</ta>
            <ta e="T642" id="Seg_4837" s="T641">šku͡ola</ta>
            <ta e="T643" id="Seg_4838" s="T642">di͡et</ta>
            <ta e="T644" id="Seg_4839" s="T643">üleleː-bit-i-m</ta>
            <ta e="T645" id="Seg_4840" s="T644">povar-ga</ta>
            <ta e="T647" id="Seg_4841" s="T646">üleleː-bit-i-m</ta>
            <ta e="T648" id="Seg_4842" s="T647">povar-ga</ta>
            <ta e="T650" id="Seg_4843" s="T649">škola-ga</ta>
            <ta e="T651" id="Seg_4844" s="T650">onton</ta>
            <ta e="T652" id="Seg_4845" s="T651">saːdikka-ga</ta>
            <ta e="T653" id="Seg_4846" s="T652">ɨl-bɨt-tara</ta>
            <ta e="T655" id="Seg_4847" s="T654">eh</ta>
            <ta e="T656" id="Seg_4848" s="T655">kim-neː-bit-tere</ta>
            <ta e="T657" id="Seg_4849" s="T656">egel-bit-tere</ta>
            <ta e="T658" id="Seg_4850" s="T657">kim</ta>
            <ta e="T659" id="Seg_4851" s="T658">onton</ta>
            <ta e="T660" id="Seg_4852" s="T659">šku͡ola-ttan</ta>
            <ta e="T661" id="Seg_4853" s="T660">saːdik-ka</ta>
            <ta e="T662" id="Seg_4854" s="T661">üleleː-bit-i-m</ta>
            <ta e="T666" id="Seg_4855" s="T665">pensia-ga</ta>
            <ta e="T667" id="Seg_4856" s="T666">olor-o-bun</ta>
            <ta e="T670" id="Seg_4857" s="T669">vnuk-tar-bɨ-n</ta>
            <ta e="T672" id="Seg_4858" s="T671">pravnučkaj-bu-n</ta>
            <ta e="T676" id="Seg_4859" s="T675">ülel-iː-bin</ta>
            <ta e="T677" id="Seg_4860" s="T676">ülel-iː</ta>
            <ta e="T678" id="Seg_4861" s="T677">hɨldʼ-a-bɨn</ta>
            <ta e="T686" id="Seg_4862" s="T685">a</ta>
            <ta e="T687" id="Seg_4863" s="T686">min</ta>
            <ta e="T688" id="Seg_4864" s="T687">ol</ta>
            <ta e="T689" id="Seg_4865" s="T688">ogo-nɨ</ta>
            <ta e="T690" id="Seg_4866" s="T689">gɨtta</ta>
            <ta e="T691" id="Seg_4867" s="T690">olor-o-bun</ta>
            <ta e="T695" id="Seg_4868" s="T694">hüːrbe-leːger</ta>
            <ta e="T696" id="Seg_4869" s="T695">dʼɨl-laː-n-ɨ͡ag-a</ta>
            <ta e="T697" id="Seg_4870" s="T696">mark-ka</ta>
            <ta e="T705" id="Seg_4871" s="T704">kim-neː-bit</ta>
            <ta e="T706" id="Seg_4872" s="T705">nemnožko</ta>
            <ta e="T707" id="Seg_4873" s="T706">ɨ͡aldʼ-a-bɨt</ta>
            <ta e="T708" id="Seg_4874" s="T707">stomatit-tam-mɨt</ta>
            <ta e="T710" id="Seg_4875" s="T709">maːma-ta</ta>
            <ta e="T711" id="Seg_4876" s="T710">olor-or</ta>
            <ta e="T715" id="Seg_4877" s="T714">tak</ta>
            <ta e="T716" id="Seg_4878" s="T715">kim</ta>
            <ta e="T717" id="Seg_4879" s="T716">bu</ta>
            <ta e="T718" id="Seg_4880" s="T717">ulakan</ta>
            <ta e="T719" id="Seg_4881" s="T718">vnug-u-m</ta>
            <ta e="T720" id="Seg_4882" s="T719">u͡ol-u-n</ta>
            <ta e="T721" id="Seg_4883" s="T720">gi͡en-e</ta>
            <ta e="T722" id="Seg_4884" s="T721">ulakan</ta>
            <ta e="T726" id="Seg_4885" s="T725">vnug-u-m</ta>
            <ta e="T727" id="Seg_4886" s="T726">ulakan</ta>
            <ta e="T728" id="Seg_4887" s="T727">anɨ</ta>
            <ta e="T729" id="Seg_4888" s="T728">služit-tɨː-r</ta>
            <ta e="T730" id="Seg_4889" s="T729">kim-ŋe</ta>
            <ta e="T731" id="Seg_4890" s="T730">armia-ga</ta>
            <ta e="T835" id="Seg_4891" s="T732">aŋar-dar</ta>
            <ta e="T733" id="Seg_4892" s="T835">ü͡ören-el-ler</ta>
            <ta e="T734" id="Seg_4893" s="T733">a</ta>
            <ta e="T735" id="Seg_4894" s="T734">kɨːh-a</ta>
            <ta e="T736" id="Seg_4895" s="T735">kɨːh-ɨ-m</ta>
            <ta e="T737" id="Seg_4896" s="T736">vnučka-m</ta>
            <ta e="T738" id="Seg_4897" s="T737">ulakan</ta>
            <ta e="T739" id="Seg_4898" s="T738">eto</ta>
            <ta e="T740" id="Seg_4899" s="T739">u͡ol-u-m</ta>
            <ta e="T742" id="Seg_4900" s="T741">kɨːh-a</ta>
            <ta e="T743" id="Seg_4901" s="T742">vnučka-m</ta>
            <ta e="T744" id="Seg_4902" s="T743">Katja</ta>
            <ta e="T745" id="Seg_4903" s="T744">aːt-a</ta>
            <ta e="T746" id="Seg_4904" s="T745">iti</ta>
            <ta e="T748" id="Seg_4905" s="T747">kim-ŋe</ta>
            <ta e="T749" id="Seg_4906" s="T748">ü͡ören-er</ta>
            <ta e="T750" id="Seg_4907" s="T749">tože</ta>
            <ta e="T751" id="Seg_4908" s="T750">maːma-tɨ-n</ta>
            <ta e="T752" id="Seg_4909" s="T751">kördük</ta>
            <ta e="T754" id="Seg_4910" s="T753">eto</ta>
            <ta e="T755" id="Seg_4911" s="T754">Norilskaj</ta>
            <ta e="T756" id="Seg_4912" s="T755">kim-ŋe</ta>
            <ta e="T757" id="Seg_4913" s="T756">tʼexnʼikum-ŋa</ta>
            <ta e="T758" id="Seg_4914" s="T757">ü͡ören-er</ta>
            <ta e="T759" id="Seg_4915" s="T758">maːma-tɨ-n</ta>
            <ta e="T760" id="Seg_4916" s="T759">kördük</ta>
            <ta e="T767" id="Seg_4917" s="T766">iti</ta>
            <ta e="T768" id="Seg_4918" s="T767">kim-ŋe</ta>
            <ta e="T772" id="Seg_4919" s="T771">keːs-pit-e</ta>
            <ta e="T773" id="Seg_4920" s="T772">bihi͡e-ke</ta>
            <ta e="T777" id="Seg_4921" s="T776">töttörü</ta>
            <ta e="T778" id="Seg_4922" s="T777">büt-e</ta>
            <ta e="T779" id="Seg_4923" s="T778">štob</ta>
            <ta e="T780" id="Seg_4924" s="T779">dʼɨl-ɨ-n</ta>
            <ta e="T781" id="Seg_4925" s="T780">hüter-i-m-eːri</ta>
            <ta e="T782" id="Seg_4926" s="T781">ke</ta>
            <ta e="T789" id="Seg_4927" s="T788">ješo</ta>
            <ta e="T791" id="Seg_4928" s="T790">ikki</ta>
            <ta e="T792" id="Seg_4929" s="T791">dʼɨl-ɨ</ta>
            <ta e="T793" id="Seg_4930" s="T792">naːda</ta>
            <ta e="T794" id="Seg_4931" s="T793">ü͡ören-i͡ek-ke</ta>
            <ta e="T801" id="Seg_4932" s="T800">üs</ta>
            <ta e="T802" id="Seg_4933" s="T801">dʼɨl</ta>
            <ta e="T806" id="Seg_4934" s="T805">üle</ta>
            <ta e="T807" id="Seg_4935" s="T806">ü͡ören-el-ler</ta>
            <ta e="T817" id="Seg_4936" s="T816">biːrges</ta>
            <ta e="T818" id="Seg_4937" s="T817">vnuk-bɨ-n</ta>
            <ta e="T820" id="Seg_4938" s="T819">kɨːh-ɨ-m</ta>
            <ta e="T821" id="Seg_4939" s="T820">u͡ol-u-n</ta>
            <ta e="T822" id="Seg_4940" s="T821">gɨtta</ta>
            <ta e="T823" id="Seg_4941" s="T822">vnuk-bɨ-n</ta>
            <ta e="T824" id="Seg_4942" s="T823">gɨtta</ta>
            <ta e="T825" id="Seg_4943" s="T824">baːr-bɨn</ta>
            <ta e="T826" id="Seg_4944" s="T825">dʼi͡e-be-r</ta>
            <ta e="T828" id="Seg_4945" s="T827">hett-is</ta>
            <ta e="T829" id="Seg_4946" s="T828">kɨlaːs-ka</ta>
            <ta e="T830" id="Seg_4947" s="T829">ü͡ören-er</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_4948" s="T1">min</ta>
            <ta e="T3" id="Seg_4949" s="T2">aːt-I-m</ta>
            <ta e="T4" id="Seg_4950" s="T3">Iste</ta>
            <ta e="T5" id="Seg_4951" s="T4">haka-LIː</ta>
            <ta e="T6" id="Seg_4952" s="T5">Iste</ta>
            <ta e="T7" id="Seg_4953" s="T6">di͡e-AːččI-LAr</ta>
            <ta e="T9" id="Seg_4954" s="T8">töröː-BIT-I-m</ta>
            <ta e="T10" id="Seg_4955" s="T9">Popigaj</ta>
            <ta e="T11" id="Seg_4956" s="T10">di͡e-An</ta>
            <ta e="T12" id="Seg_4957" s="T11">hir-GA</ta>
            <ta e="T14" id="Seg_4958" s="T13">min</ta>
            <ta e="T15" id="Seg_4959" s="T14">kergen-LAr-I-m</ta>
            <ta e="T16" id="Seg_4960" s="T15">teːte-m</ta>
            <ta e="T17" id="Seg_4961" s="T16">kim</ta>
            <ta e="T18" id="Seg_4962" s="T17">Nʼikalaj</ta>
            <ta e="T19" id="Seg_4963" s="T18">maːma-m</ta>
            <ta e="T20" id="Seg_4964" s="T19">eto</ta>
            <ta e="T21" id="Seg_4965" s="T20">Taiska</ta>
            <ta e="T22" id="Seg_4966" s="T21">Taiska</ta>
            <ta e="T24" id="Seg_4967" s="T23">ol-tI-ŋ</ta>
            <ta e="T25" id="Seg_4968" s="T24">maːma-m</ta>
            <ta e="T26" id="Seg_4969" s="T25">erde</ta>
            <ta e="T27" id="Seg_4970" s="T26">bagajɨ</ta>
            <ta e="T28" id="Seg_4971" s="T27">öl-BIT-tA</ta>
            <ta e="T29" id="Seg_4972" s="T28">ol-ttAn</ta>
            <ta e="T30" id="Seg_4973" s="T29">min</ta>
            <ta e="T31" id="Seg_4974" s="T30">teːte-BI-n</ta>
            <ta e="T32" id="Seg_4975" s="T31">kɨtta</ta>
            <ta e="T33" id="Seg_4976" s="T32">kaːl-BIT-I-m</ta>
            <ta e="T35" id="Seg_4977" s="T34">teːte-m</ta>
            <ta e="T36" id="Seg_4978" s="T35">du͡o</ta>
            <ta e="T37" id="Seg_4979" s="T36">kim-GA</ta>
            <ta e="T38" id="Seg_4980" s="T37">pastux-GA</ta>
            <ta e="T39" id="Seg_4981" s="T38">brʼigadʼiːr-GA</ta>
            <ta e="T40" id="Seg_4982" s="T39">üleleː-Ar</ta>
            <ta e="T41" id="Seg_4983" s="T40">e-TI-tA</ta>
            <ta e="T43" id="Seg_4984" s="T42">ol-ttAn</ta>
            <ta e="T44" id="Seg_4985" s="T43">bɨraːt-I-m</ta>
            <ta e="T45" id="Seg_4986" s="T44">tože</ta>
            <ta e="T46" id="Seg_4987" s="T45">teːte-BI-n</ta>
            <ta e="T47" id="Seg_4988" s="T46">kɨtta</ta>
            <ta e="T48" id="Seg_4989" s="T47">hɨrɨt-AːččI</ta>
            <ta e="T50" id="Seg_4990" s="T49">tundra-GA</ta>
            <ta e="T51" id="Seg_4991" s="T50">bihigi</ta>
            <ta e="T52" id="Seg_4992" s="T51">kös-A</ta>
            <ta e="T53" id="Seg_4993" s="T52">hɨrɨt-AːččI-BIt</ta>
            <ta e="T55" id="Seg_4994" s="T54">usku͡ola-ttAn</ta>
            <ta e="T56" id="Seg_4995" s="T55">kanikul-BA-r</ta>
            <ta e="T57" id="Seg_4996" s="T56">kel-TAK-BInA</ta>
            <ta e="T58" id="Seg_4997" s="T57">min</ta>
            <ta e="T59" id="Seg_4998" s="T58">teːte-BA-r</ta>
            <ta e="T60" id="Seg_4999" s="T59">bu͡ol-AːččI-BIn</ta>
            <ta e="T62" id="Seg_5000" s="T61">no</ta>
            <ta e="T63" id="Seg_5001" s="T62">ulaːt-BIT-I-m</ta>
            <ta e="T64" id="Seg_5002" s="T63">genne</ta>
            <ta e="T65" id="Seg_5003" s="T64">min</ta>
            <ta e="T66" id="Seg_5004" s="T65">teːte-BA-r</ta>
            <ta e="T67" id="Seg_5005" s="T66">bu͡ol-BIT-I-m</ta>
            <ta e="T68" id="Seg_5006" s="T67">kagda</ta>
            <ta e="T69" id="Seg_5007" s="T68">min</ta>
            <ta e="T70" id="Seg_5008" s="T69">küččügüj</ta>
            <ta e="T71" id="Seg_5009" s="T70">er-TAK-BInA</ta>
            <ta e="T72" id="Seg_5010" s="T71">kihi-LAr-GA</ta>
            <ta e="T73" id="Seg_5011" s="T72">iːt-I-LIN-I-BIT-I-m</ta>
            <ta e="T74" id="Seg_5012" s="T73">atɨn</ta>
            <ta e="T75" id="Seg_5013" s="T74">kihi-LAr-GA</ta>
            <ta e="T77" id="Seg_5014" s="T76">iːt-I-LIN-I-BIT-I-m</ta>
            <ta e="T78" id="Seg_5015" s="T77">ol-ttAn</ta>
            <ta e="T79" id="Seg_5016" s="T78">ulaːt-An</ta>
            <ta e="T80" id="Seg_5017" s="T79">bar-An-BIn</ta>
            <ta e="T81" id="Seg_5018" s="T80">ere</ta>
            <ta e="T82" id="Seg_5019" s="T81">teːte-BA-r</ta>
            <ta e="T83" id="Seg_5020" s="T82">bu͡ol-BIT-I-m</ta>
            <ta e="T84" id="Seg_5021" s="T83">bar-AːččI-BIn</ta>
            <ta e="T85" id="Seg_5022" s="T84">teːte-BA-r</ta>
            <ta e="T86" id="Seg_5023" s="T85">kanikul-GA</ta>
            <ta e="T87" id="Seg_5024" s="T86">kel-TAK-BInA</ta>
            <ta e="T89" id="Seg_5025" s="T88">dʼe</ta>
            <ta e="T90" id="Seg_5026" s="T89">ol</ta>
            <ta e="T91" id="Seg_5027" s="T90">kös-A</ta>
            <ta e="T92" id="Seg_5028" s="T91">hɨrɨt-AːččI-BIt</ta>
            <ta e="T93" id="Seg_5029" s="T92">kim-ttAn</ta>
            <ta e="T94" id="Seg_5030" s="T93">mi͡este-ttAn</ta>
            <ta e="T95" id="Seg_5031" s="T94">mi͡este-GA</ta>
            <ta e="T96" id="Seg_5032" s="T95">di͡eri</ta>
            <ta e="T101" id="Seg_5033" s="T100">kös-A</ta>
            <ta e="T102" id="Seg_5034" s="T101">hɨrɨt-AːččI-BIt</ta>
            <ta e="T104" id="Seg_5035" s="T103">čum-GA</ta>
            <ta e="T105" id="Seg_5036" s="T104">dʼi͡e-LAːK</ta>
            <ta e="T106" id="Seg_5037" s="T105">bu͡ol-AːččI-BIt</ta>
            <ta e="T107" id="Seg_5038" s="T106">tam</ta>
            <ta e="T109" id="Seg_5039" s="T108">eː</ta>
            <ta e="T110" id="Seg_5040" s="T109">kaːr-nI</ta>
            <ta e="T111" id="Seg_5041" s="T110">kim-LAː-A-BIt</ta>
            <ta e="T112" id="Seg_5042" s="T111">ubiraj-LAː-A-BIt</ta>
            <ta e="T120" id="Seg_5043" s="T119">čum</ta>
            <ta e="T121" id="Seg_5044" s="T120">čum</ta>
            <ta e="T122" id="Seg_5045" s="T121">oŋor-Ar-LAr</ta>
            <ta e="T133" id="Seg_5046" s="T132">eː</ta>
            <ta e="T138" id="Seg_5047" s="T137">uraha</ta>
            <ta e="T139" id="Seg_5048" s="T138">dʼi͡e-GA</ta>
            <ta e="T140" id="Seg_5049" s="T139">aga</ta>
            <ta e="T142" id="Seg_5050" s="T141">uraha-LAr-nI</ta>
            <ta e="T144" id="Seg_5051" s="T143">kim-ttAn</ta>
            <ta e="T149" id="Seg_5052" s="T148">ol</ta>
            <ta e="T150" id="Seg_5053" s="T149">dʼi͡e-ŋ</ta>
            <ta e="T151" id="Seg_5054" s="T150">inogda</ta>
            <ta e="T152" id="Seg_5055" s="T151">ki͡eŋ</ta>
            <ta e="T153" id="Seg_5056" s="T152">bagajɨ</ta>
            <ta e="T154" id="Seg_5057" s="T153">oŋor-Ar-LAr</ta>
            <ta e="T155" id="Seg_5058" s="T154">ügüs</ta>
            <ta e="T156" id="Seg_5059" s="T155">kihi</ta>
            <ta e="T157" id="Seg_5060" s="T156">ol</ta>
            <ta e="T158" id="Seg_5061" s="T157">gɨn-An</ta>
            <ta e="T159" id="Seg_5062" s="T158">ügüs-BIt</ta>
            <ta e="T163" id="Seg_5063" s="T162">ügüs-BIt</ta>
            <ta e="T164" id="Seg_5064" s="T163">ved</ta>
            <ta e="T167" id="Seg_5065" s="T166">kim</ta>
            <ta e="T168" id="Seg_5066" s="T167">ki͡eŋ</ta>
            <ta e="T169" id="Seg_5067" s="T168">dʼi͡e</ta>
            <ta e="T170" id="Seg_5068" s="T169">ki͡eŋ</ta>
            <ta e="T171" id="Seg_5069" s="T170">dʼuka</ta>
            <ta e="T173" id="Seg_5070" s="T172">patom</ta>
            <ta e="T174" id="Seg_5071" s="T173">pečka-BIt</ta>
            <ta e="T175" id="Seg_5072" s="T174">ačaːk</ta>
            <ta e="T176" id="Seg_5073" s="T175">timir</ta>
            <ta e="T177" id="Seg_5074" s="T176">ačaːk</ta>
            <ta e="T188" id="Seg_5075" s="T187">kim-I-n</ta>
            <ta e="T189" id="Seg_5076" s="T188">alɨn-tI-n</ta>
            <ta e="T191" id="Seg_5077" s="T190">kim-LAː-Ar-LAr</ta>
            <ta e="T193" id="Seg_5078" s="T192">bat-LIk</ta>
            <ta e="T194" id="Seg_5079" s="T193">oŋor-Ar-LAr</ta>
            <ta e="T195" id="Seg_5080" s="T194">battaː-t-Ar-LAr</ta>
            <ta e="T199" id="Seg_5081" s="T198">kim-LAː-m-IAK-tI-n</ta>
            <ta e="T200" id="Seg_5082" s="T199">argɨj-I-m-IAK-tI-n</ta>
            <ta e="T202" id="Seg_5083" s="T201">daːse</ta>
            <ta e="T203" id="Seg_5084" s="T202">küččügüj</ta>
            <ta e="T204" id="Seg_5085" s="T203">ogo-LAːK</ta>
            <ta e="T205" id="Seg_5086" s="T204">ezdit-LAː-AːččI-BIt</ta>
            <ta e="T211" id="Seg_5087" s="T210">kim</ta>
            <ta e="T212" id="Seg_5088" s="T211">haka</ta>
            <ta e="T213" id="Seg_5089" s="T212">ljulka-GA</ta>
            <ta e="T222" id="Seg_5090" s="T221">bar-A</ta>
            <ta e="T223" id="Seg_5091" s="T222">hɨrɨt-A-BIt</ta>
            <ta e="T225" id="Seg_5092" s="T224">ol-ttAn</ta>
            <ta e="T226" id="Seg_5093" s="T225">tu͡ok-nI</ta>
            <ta e="T227" id="Seg_5094" s="T226">kepseː-IAK-m=Ij</ta>
            <ta e="T228" id="Seg_5095" s="T227">teːte-m</ta>
            <ta e="T229" id="Seg_5096" s="T228">du͡o</ta>
            <ta e="T230" id="Seg_5097" s="T229">Kamennɨj</ta>
            <ta e="T231" id="Seg_5098" s="T230">kim-GA</ta>
            <ta e="T232" id="Seg_5099" s="T231">storona-GA</ta>
            <ta e="T233" id="Seg_5100" s="T232">hɨrɨt-I-BIT-tA</ta>
            <ta e="T234" id="Seg_5101" s="T233">Kamennɨj</ta>
            <ta e="T238" id="Seg_5102" s="T237">kim</ta>
            <ta e="T239" id="Seg_5103" s="T238">kaːs</ta>
            <ta e="T240" id="Seg_5104" s="T239">da</ta>
            <ta e="T241" id="Seg_5105" s="T240">hu͡ok</ta>
            <ta e="T254" id="Seg_5106" s="T253">kim</ta>
            <ta e="T255" id="Seg_5107" s="T254">kutujak-LAr</ta>
            <ta e="T263" id="Seg_5108" s="T262">össü͡ö</ta>
            <ta e="T264" id="Seg_5109" s="T263">tu͡ok-nI=Ij</ta>
            <ta e="T265" id="Seg_5110" s="T264">kim</ta>
            <ta e="T266" id="Seg_5111" s="T265">min</ta>
            <ta e="T267" id="Seg_5112" s="T266">beje-m</ta>
            <ta e="T268" id="Seg_5113" s="T267">ü͡ören-BIT-I-m</ta>
            <ta e="T269" id="Seg_5114" s="T268">hette</ta>
            <ta e="T270" id="Seg_5115" s="T269">kɨlaːs-GA</ta>
            <ta e="T271" id="Seg_5116" s="T270">di͡eri</ta>
            <ta e="T272" id="Seg_5117" s="T271">agɨs-Is</ta>
            <ta e="T273" id="Seg_5118" s="T272">kɨlaːs-ttAn</ta>
            <ta e="T274" id="Seg_5119" s="T273">büt-BIT-I-m</ta>
            <ta e="T276" id="Seg_5120" s="T275">kim-nI</ta>
            <ta e="T277" id="Seg_5121" s="T276">internat-GA</ta>
            <ta e="T278" id="Seg_5122" s="T277">ol-ttAn</ta>
            <ta e="T279" id="Seg_5123" s="T278">internat-BIt</ta>
            <ta e="T280" id="Seg_5124" s="T279">bu͡ol-AːččI-BIt</ta>
            <ta e="T282" id="Seg_5125" s="T281">ikki</ta>
            <ta e="T283" id="Seg_5126" s="T282">dʼɨl-nI</ta>
            <ta e="T284" id="Seg_5127" s="T283">Norilskaj-GA</ta>
            <ta e="T285" id="Seg_5128" s="T284">ü͡ören-BIT-BIt</ta>
            <ta e="T286" id="Seg_5129" s="T285">internat-GA</ta>
            <ta e="T288" id="Seg_5130" s="T287">kim</ta>
            <ta e="T289" id="Seg_5131" s="T288">olor-BIT-BIt</ta>
            <ta e="T291" id="Seg_5132" s="T290">lager-GA</ta>
            <ta e="T292" id="Seg_5133" s="T291">hɨrɨt-I-BIT-I-m</ta>
            <ta e="T293" id="Seg_5134" s="T292">kim-GA</ta>
            <ta e="T294" id="Seg_5135" s="T293">Tajožnaj-GA</ta>
            <ta e="T295" id="Seg_5136" s="T294">hɨrɨt-I-BIT-I-m</ta>
            <ta e="T296" id="Seg_5137" s="T295">Kurejka-GA</ta>
            <ta e="T298" id="Seg_5138" s="T297">patom</ta>
            <ta e="T299" id="Seg_5139" s="T298">ulaːt-An</ta>
            <ta e="T300" id="Seg_5140" s="T299">baran</ta>
            <ta e="T301" id="Seg_5141" s="T300">kim-GA</ta>
            <ta e="T303" id="Seg_5142" s="T302">eː</ta>
            <ta e="T304" id="Seg_5143" s="T303">ol</ta>
            <ta e="T305" id="Seg_5144" s="T304">ü͡ören-A</ta>
            <ta e="T306" id="Seg_5145" s="T305">bar-BIT-I-m</ta>
            <ta e="T312" id="Seg_5146" s="T311">kim-GA</ta>
            <ta e="T314" id="Seg_5147" s="T313">atɨːlaː-Ar</ta>
            <ta e="T315" id="Seg_5148" s="T314">kihi</ta>
            <ta e="T316" id="Seg_5149" s="T315">ka</ta>
            <ta e="T317" id="Seg_5150" s="T316">kim-GA</ta>
            <ta e="T318" id="Seg_5151" s="T317">prodaves-GA</ta>
            <ta e="T319" id="Seg_5152" s="T318">bar-BIT-I-m</ta>
            <ta e="T320" id="Seg_5153" s="T319">ü͡ören-A</ta>
            <ta e="T321" id="Seg_5154" s="T320">Dudʼinka-GA</ta>
            <ta e="T323" id="Seg_5155" s="T322">alta</ta>
            <ta e="T324" id="Seg_5156" s="T323">ɨj-nI</ta>
            <ta e="T325" id="Seg_5157" s="T324">ü͡ören-BIT-I-m</ta>
            <ta e="T326" id="Seg_5158" s="T325">ol-ttAn</ta>
            <ta e="T327" id="Seg_5159" s="T326">kel-BIT-I-m</ta>
            <ta e="T328" id="Seg_5160" s="T327">i</ta>
            <ta e="T329" id="Seg_5161" s="T328">Nosku͡o-GA</ta>
            <ta e="T331" id="Seg_5162" s="T330">kim-LAː-BIT-I-m</ta>
            <ta e="T332" id="Seg_5163" s="T331">praktika-nI</ta>
            <ta e="T336" id="Seg_5164" s="T335">kim-GA</ta>
            <ta e="T337" id="Seg_5165" s="T336">bufet-GA</ta>
            <ta e="T338" id="Seg_5166" s="T337">üleleː-AːččI-BIn</ta>
            <ta e="T340" id="Seg_5167" s="T339">stolovoj-GA</ta>
            <ta e="T341" id="Seg_5168" s="T340">bufet</ta>
            <ta e="T342" id="Seg_5169" s="T341">e-TI-tA</ta>
            <ta e="T343" id="Seg_5170" s="T342">Nosku͡o-GA</ta>
            <ta e="T344" id="Seg_5171" s="T343">onno</ta>
            <ta e="T346" id="Seg_5172" s="T345">bufet-GA</ta>
            <ta e="T347" id="Seg_5173" s="T346">üleleː-AːččI-BIn</ta>
            <ta e="T349" id="Seg_5174" s="T348">ol-ttAn</ta>
            <ta e="T352" id="Seg_5175" s="T351">Nosku͡o-GA</ta>
            <ta e="T353" id="Seg_5176" s="T352">biːr</ta>
            <ta e="T354" id="Seg_5177" s="T353">kihi-nI</ta>
            <ta e="T355" id="Seg_5178" s="T354">manna</ta>
            <ta e="T356" id="Seg_5179" s="T355">Ki͡etaː-ttAn</ta>
            <ta e="T357" id="Seg_5180" s="T356">biːr</ta>
            <ta e="T358" id="Seg_5181" s="T357">u͡ol-nI</ta>
            <ta e="T359" id="Seg_5182" s="T358">kɨtta</ta>
            <ta e="T360" id="Seg_5183" s="T359">dogor-LAː-s-BIT-I-m</ta>
            <ta e="T370" id="Seg_5184" s="T369">ol-ttAn</ta>
            <ta e="T371" id="Seg_5185" s="T370">ɨl-s-An</ta>
            <ta e="T372" id="Seg_5186" s="T371">bar-An-BIt</ta>
            <ta e="T373" id="Seg_5187" s="T372">to</ta>
            <ta e="T374" id="Seg_5188" s="T373">kim-LAː-BIT-BIt</ta>
            <ta e="T375" id="Seg_5189" s="T374">Nosku͡o-GA</ta>
            <ta e="T376" id="Seg_5190" s="T375">olor-A</ta>
            <ta e="T377" id="Seg_5191" s="T376">tüs-BIT-I-m</ta>
            <ta e="T379" id="Seg_5192" s="T378">min</ta>
            <ta e="T380" id="Seg_5193" s="T379">saːdik-GA</ta>
            <ta e="T381" id="Seg_5194" s="T380">üleleː-Ar</ta>
            <ta e="T382" id="Seg_5195" s="T381">e-TI-m</ta>
            <ta e="T383" id="Seg_5196" s="T382">njanja-GA</ta>
            <ta e="T384" id="Seg_5197" s="T383">a</ta>
            <ta e="T385" id="Seg_5198" s="T384">gini-ŋ</ta>
            <ta e="T386" id="Seg_5199" s="T385">du͡o</ta>
            <ta e="T387" id="Seg_5200" s="T386">počta-GA</ta>
            <ta e="T389" id="Seg_5201" s="T388">üleleː-Ar</ta>
            <ta e="T390" id="Seg_5202" s="T389">e-TI-tA</ta>
            <ta e="T392" id="Seg_5203" s="T391">ol-ttAn</ta>
            <ta e="T397" id="Seg_5204" s="T396">kim</ta>
            <ta e="T398" id="Seg_5205" s="T397">kɨːs-BIt</ta>
            <ta e="T399" id="Seg_5206" s="T398">kɨːs-I-m</ta>
            <ta e="T400" id="Seg_5207" s="T399">Rita</ta>
            <ta e="T401" id="Seg_5208" s="T400">aːt-LAːK</ta>
            <ta e="T402" id="Seg_5209" s="T401">kɨːs-I-m</ta>
            <ta e="T403" id="Seg_5210" s="T402">u͡ol-I-m</ta>
            <ta e="T405" id="Seg_5211" s="T404">ol-ttAn</ta>
            <ta e="T411" id="Seg_5212" s="T410">hir-tI-GAr</ta>
            <ta e="T412" id="Seg_5213" s="T411">kel-IAK-tI-n</ta>
            <ta e="T413" id="Seg_5214" s="T412">rodina-tI-GAr</ta>
            <ta e="T414" id="Seg_5215" s="T413">kel-IAK-tI-n</ta>
            <ta e="T422" id="Seg_5216" s="T421">ɨːt-BIT-tA</ta>
            <ta e="T424" id="Seg_5217" s="T423">ogo-BI-n</ta>
            <ta e="T425" id="Seg_5218" s="T424">kɨtta</ta>
            <ta e="T426" id="Seg_5219" s="T425">ka</ta>
            <ta e="T427" id="Seg_5220" s="T426">kɨːs-BI-n</ta>
            <ta e="T428" id="Seg_5221" s="T427">kɨtta</ta>
            <ta e="T433" id="Seg_5222" s="T432">gini</ta>
            <ta e="T434" id="Seg_5223" s="T433">kelin-GA</ta>
            <ta e="T435" id="Seg_5224" s="T434">min</ta>
            <ta e="T436" id="Seg_5225" s="T435">kelin-I-BI-ttAn</ta>
            <ta e="T437" id="Seg_5226" s="T436">gini</ta>
            <ta e="T438" id="Seg_5227" s="T437">kel-BIT-tA</ta>
            <ta e="T445" id="Seg_5228" s="T444">kas</ta>
            <ta e="T446" id="Seg_5229" s="T445">dʼɨl</ta>
            <ta e="T447" id="Seg_5230" s="T446">bu͡olla</ta>
            <ta e="T448" id="Seg_5231" s="T447">ka</ta>
            <ta e="T452" id="Seg_5232" s="T451">god-ttAn</ta>
            <ta e="T465" id="Seg_5233" s="T464">god-GA</ta>
            <ta e="T466" id="Seg_5234" s="T465">öl-BIT-tA</ta>
            <ta e="T473" id="Seg_5235" s="T472">min</ta>
            <ta e="T474" id="Seg_5236" s="T473">ogo-LAN-BIT-I-m</ta>
            <ta e="T475" id="Seg_5237" s="T474">u͡ol-BI-n</ta>
            <ta e="T484" id="Seg_5238" s="T483">Usurijskaj-GA</ta>
            <ta e="T493" id="Seg_5239" s="T492">olor-A-BIn</ta>
            <ta e="T494" id="Seg_5240" s="T493">saːdik-GA</ta>
            <ta e="T495" id="Seg_5241" s="T494">üleleː-BIT-I-m</ta>
            <ta e="T499" id="Seg_5242" s="T498">saːdik-GA</ta>
            <ta e="T500" id="Seg_5243" s="T499">hubu</ta>
            <ta e="T501" id="Seg_5244" s="T500">že</ta>
            <ta e="T502" id="Seg_5245" s="T501">saːdik-GA</ta>
            <ta e="T504" id="Seg_5246" s="T503">ol-ttAn</ta>
            <ta e="T505" id="Seg_5247" s="T504">tu͡ok-nI</ta>
            <ta e="T506" id="Seg_5248" s="T505">kepseː-IAK-m=Ij</ta>
            <ta e="T507" id="Seg_5249" s="T506">ol-ttAn</ta>
            <ta e="T508" id="Seg_5250" s="T507">er-I-m</ta>
            <ta e="T509" id="Seg_5251" s="T508">du͡o</ta>
            <ta e="T510" id="Seg_5252" s="T509">ol</ta>
            <ta e="T511" id="Seg_5253" s="T510">er-I-m</ta>
            <ta e="T512" id="Seg_5254" s="T511">rɨbalka-GA</ta>
            <ta e="T513" id="Seg_5255" s="T512">bu͡ol-AːččI</ta>
            <ta e="T514" id="Seg_5256" s="T513">itinne</ta>
            <ta e="T516" id="Seg_5257" s="T515">točka-GA</ta>
            <ta e="T517" id="Seg_5258" s="T516">olor-AːččI-BIt</ta>
            <ta e="T518" id="Seg_5259" s="T517">hugas</ta>
            <ta e="T524" id="Seg_5260" s="T523">vɨxodnoj-LAr-BA-r</ta>
            <ta e="T525" id="Seg_5261" s="T524">gini-GA</ta>
            <ta e="T526" id="Seg_5262" s="T525">bar-AːččI-BIn</ta>
            <ta e="T527" id="Seg_5263" s="T526">motor-I-nAn</ta>
            <ta e="T528" id="Seg_5264" s="T527">ezdit-LAː-AːččI-BIt</ta>
            <ta e="T533" id="Seg_5265" s="T532">kim</ta>
            <ta e="T534" id="Seg_5266" s="T533">kömölös-AːččI-BIn</ta>
            <ta e="T537" id="Seg_5267" s="T536">kim-nI</ta>
            <ta e="T540" id="Seg_5268" s="T539">razdelɨvaj-LAː-A</ta>
            <ta e="T541" id="Seg_5269" s="T540">kanʼaː-A</ta>
            <ta e="T542" id="Seg_5270" s="T541">kanʼaː-A</ta>
            <ta e="T544" id="Seg_5271" s="T543">bi͡er-AːččI-BIt</ta>
            <ta e="T545" id="Seg_5272" s="T544">že</ta>
            <ta e="T546" id="Seg_5273" s="T545">eto</ta>
            <ta e="T547" id="Seg_5274" s="T546">kim-GA</ta>
            <ta e="T548" id="Seg_5275" s="T547">rɨbzavod-GA</ta>
            <ta e="T549" id="Seg_5276" s="T548">bi͡er-AːččI-BIt</ta>
            <ta e="T550" id="Seg_5277" s="T549">sdavaj-LAː-AːččI-BIt</ta>
            <ta e="T559" id="Seg_5278" s="T558">bar-AːččI-LAr</ta>
            <ta e="T569" id="Seg_5279" s="T568">olor-AːččI-BIt</ta>
            <ta e="T570" id="Seg_5280" s="T569">tüːn-LAr-nI</ta>
            <ta e="T575" id="Seg_5281" s="T574">hi͡e-TI-tA</ta>
            <ta e="T576" id="Seg_5282" s="T575">hi͡e-TI-tA</ta>
            <ta e="T577" id="Seg_5283" s="T576">ol</ta>
            <ta e="T578" id="Seg_5284" s="T577">ogo-LAr-BIt</ta>
            <ta e="T579" id="Seg_5285" s="T578">ka</ta>
            <ta e="T580" id="Seg_5286" s="T579">u͡ol-LAr-BIt</ta>
            <ta e="T583" id="Seg_5287" s="T582">u͡ol-I-m</ta>
            <ta e="T584" id="Seg_5288" s="T583">ol-ttAn</ta>
            <ta e="T585" id="Seg_5289" s="T584">tam</ta>
            <ta e="T586" id="Seg_5290" s="T585">össü͡ö</ta>
            <ta e="T587" id="Seg_5291" s="T586">dogor-LAr-tA</ta>
            <ta e="T588" id="Seg_5292" s="T587">er-I-m</ta>
            <ta e="T589" id="Seg_5293" s="T588">sestra-tI-n</ta>
            <ta e="T590" id="Seg_5294" s="T589">u͡ol-tA</ta>
            <ta e="T591" id="Seg_5295" s="T590">ol</ta>
            <ta e="T593" id="Seg_5296" s="T592">u͡ot-nI</ta>
            <ta e="T594" id="Seg_5297" s="T593">oŋor-AːččI-LAr</ta>
            <ta e="T595" id="Seg_5298" s="T594">buru͡o</ta>
            <ta e="T596" id="Seg_5299" s="T595">ka</ta>
            <ta e="T597" id="Seg_5300" s="T596">iti</ta>
            <ta e="T600" id="Seg_5301" s="T599">kim-LAː-m-IAK-LArI-n</ta>
            <ta e="T601" id="Seg_5302" s="T600">čugahaː-m-IAK-LArI-n</ta>
            <ta e="T602" id="Seg_5303" s="T601">bihigi-GA</ta>
            <ta e="T607" id="Seg_5304" s="T606">razdelaj-LAː-Ar-BItI-GAr</ta>
            <ta e="T618" id="Seg_5305" s="T617">tu͡ok-nI</ta>
            <ta e="T619" id="Seg_5306" s="T618">kepseː-IAK-m=Ij</ta>
            <ta e="T621" id="Seg_5307" s="T620">kim</ta>
            <ta e="T622" id="Seg_5308" s="T621">ol</ta>
            <ta e="T623" id="Seg_5309" s="T622">saːdik-GA</ta>
            <ta e="T624" id="Seg_5310" s="T623">ol-ttAn</ta>
            <ta e="T625" id="Seg_5311" s="T624">iti</ta>
            <ta e="T626" id="Seg_5312" s="T625">pʼensija-GA</ta>
            <ta e="T627" id="Seg_5313" s="T626">bar-TI-m</ta>
            <ta e="T628" id="Seg_5314" s="T627">bu͡o</ta>
            <ta e="T631" id="Seg_5315" s="T630">otut</ta>
            <ta e="T632" id="Seg_5316" s="T631">agɨs</ta>
            <ta e="T633" id="Seg_5317" s="T632">dʼɨl-nI</ta>
            <ta e="T634" id="Seg_5318" s="T633">otut</ta>
            <ta e="T635" id="Seg_5319" s="T634">alta</ta>
            <ta e="T636" id="Seg_5320" s="T635">dʼɨl-nI</ta>
            <ta e="T637" id="Seg_5321" s="T636">üleleː-BIT-I-m</ta>
            <ta e="T638" id="Seg_5322" s="T637">saːdik-GA</ta>
            <ta e="T640" id="Seg_5323" s="T639">maŋnaj-nI</ta>
            <ta e="T641" id="Seg_5324" s="T640">usku͡ola</ta>
            <ta e="T642" id="Seg_5325" s="T641">usku͡ola</ta>
            <ta e="T643" id="Seg_5326" s="T642">dek</ta>
            <ta e="T644" id="Seg_5327" s="T643">üleleː-BIT-I-m</ta>
            <ta e="T645" id="Seg_5328" s="T644">poːvar-GA</ta>
            <ta e="T647" id="Seg_5329" s="T646">üleleː-BIT-I-m</ta>
            <ta e="T648" id="Seg_5330" s="T647">poːvar-GA</ta>
            <ta e="T650" id="Seg_5331" s="T649">usku͡ola-GA</ta>
            <ta e="T651" id="Seg_5332" s="T650">onton</ta>
            <ta e="T652" id="Seg_5333" s="T651">saːdik-GA</ta>
            <ta e="T653" id="Seg_5334" s="T652">ɨl-BIT-LArA</ta>
            <ta e="T655" id="Seg_5335" s="T654">eː</ta>
            <ta e="T656" id="Seg_5336" s="T655">kim-LAː-BIT-LArA</ta>
            <ta e="T657" id="Seg_5337" s="T656">egel-BIT-LArA</ta>
            <ta e="T658" id="Seg_5338" s="T657">kim</ta>
            <ta e="T659" id="Seg_5339" s="T658">onton</ta>
            <ta e="T660" id="Seg_5340" s="T659">usku͡ola-ttAn</ta>
            <ta e="T661" id="Seg_5341" s="T660">saːdik-GA</ta>
            <ta e="T662" id="Seg_5342" s="T661">üleleː-BIT-I-m</ta>
            <ta e="T666" id="Seg_5343" s="T665">pʼensija-GA</ta>
            <ta e="T667" id="Seg_5344" s="T666">olor-A-BIn</ta>
            <ta e="T670" id="Seg_5345" s="T669">vnuk-LAr-BI-n</ta>
            <ta e="T672" id="Seg_5346" s="T671">pravnučkaj-BI-n</ta>
            <ta e="T676" id="Seg_5347" s="T675">üleleː-A-BIn</ta>
            <ta e="T677" id="Seg_5348" s="T676">üleleː-A</ta>
            <ta e="T678" id="Seg_5349" s="T677">hɨrɨt-A-BIn</ta>
            <ta e="T686" id="Seg_5350" s="T685">a</ta>
            <ta e="T687" id="Seg_5351" s="T686">min</ta>
            <ta e="T688" id="Seg_5352" s="T687">ol</ta>
            <ta e="T689" id="Seg_5353" s="T688">ogo-nI</ta>
            <ta e="T690" id="Seg_5354" s="T689">kɨtta</ta>
            <ta e="T691" id="Seg_5355" s="T690">olor-A-BIn</ta>
            <ta e="T695" id="Seg_5356" s="T694">hüːrbe-TAːgAr</ta>
            <ta e="T696" id="Seg_5357" s="T695">dʼɨl-LAː-n-IAK-tA</ta>
            <ta e="T697" id="Seg_5358" s="T696">mart-GA</ta>
            <ta e="T705" id="Seg_5359" s="T704">kim-LAː-BIT</ta>
            <ta e="T706" id="Seg_5360" s="T705">nemnožko</ta>
            <ta e="T707" id="Seg_5361" s="T706">ɨ͡arɨj-A-BIt</ta>
            <ta e="T708" id="Seg_5362" s="T707">stomatit-LAN-BIT</ta>
            <ta e="T710" id="Seg_5363" s="T709">maːma-tA</ta>
            <ta e="T711" id="Seg_5364" s="T710">olor-Ar</ta>
            <ta e="T715" id="Seg_5365" s="T714">taːk</ta>
            <ta e="T716" id="Seg_5366" s="T715">kim</ta>
            <ta e="T717" id="Seg_5367" s="T716">bu</ta>
            <ta e="T718" id="Seg_5368" s="T717">ulakan</ta>
            <ta e="T719" id="Seg_5369" s="T718">vnuk-I-m</ta>
            <ta e="T720" id="Seg_5370" s="T719">u͡ol-tI-n</ta>
            <ta e="T721" id="Seg_5371" s="T720">gi͡en-tA</ta>
            <ta e="T722" id="Seg_5372" s="T721">ulakan</ta>
            <ta e="T726" id="Seg_5373" s="T725">vnuk-I-m</ta>
            <ta e="T727" id="Seg_5374" s="T726">ulakan</ta>
            <ta e="T728" id="Seg_5375" s="T727">anɨ</ta>
            <ta e="T729" id="Seg_5376" s="T728">služit-LAː-Ar</ta>
            <ta e="T730" id="Seg_5377" s="T729">kim-GA</ta>
            <ta e="T731" id="Seg_5378" s="T730">armʼija-GA</ta>
            <ta e="T835" id="Seg_5379" s="T732">aŋar-LAr</ta>
            <ta e="T733" id="Seg_5380" s="T835">ü͡ören-Ar-LAr</ta>
            <ta e="T734" id="Seg_5381" s="T733">a</ta>
            <ta e="T735" id="Seg_5382" s="T734">kɨːs-tA</ta>
            <ta e="T736" id="Seg_5383" s="T735">kɨːs-I-m</ta>
            <ta e="T737" id="Seg_5384" s="T736">vnučka-m</ta>
            <ta e="T738" id="Seg_5385" s="T737">ulakan</ta>
            <ta e="T739" id="Seg_5386" s="T738">eto</ta>
            <ta e="T740" id="Seg_5387" s="T739">u͡ol-I-m</ta>
            <ta e="T742" id="Seg_5388" s="T741">kɨːs-tA</ta>
            <ta e="T743" id="Seg_5389" s="T742">vnučka-m</ta>
            <ta e="T744" id="Seg_5390" s="T743">Kaːtʼa</ta>
            <ta e="T745" id="Seg_5391" s="T744">aːt-tA</ta>
            <ta e="T746" id="Seg_5392" s="T745">iti</ta>
            <ta e="T748" id="Seg_5393" s="T747">kim-GA</ta>
            <ta e="T749" id="Seg_5394" s="T748">ü͡ören-Ar</ta>
            <ta e="T750" id="Seg_5395" s="T749">tože</ta>
            <ta e="T751" id="Seg_5396" s="T750">maːma-tI-n</ta>
            <ta e="T752" id="Seg_5397" s="T751">kördük</ta>
            <ta e="T754" id="Seg_5398" s="T753">eto</ta>
            <ta e="T755" id="Seg_5399" s="T754">Norilskaj</ta>
            <ta e="T756" id="Seg_5400" s="T755">kim-GA</ta>
            <ta e="T757" id="Seg_5401" s="T756">tʼexnʼikum-GA</ta>
            <ta e="T758" id="Seg_5402" s="T757">ü͡ören-Ar</ta>
            <ta e="T759" id="Seg_5403" s="T758">maːma-tI-n</ta>
            <ta e="T760" id="Seg_5404" s="T759">kördük</ta>
            <ta e="T767" id="Seg_5405" s="T766">iti</ta>
            <ta e="T768" id="Seg_5406" s="T767">kim-GA</ta>
            <ta e="T772" id="Seg_5407" s="T771">keːs-BIT-tA</ta>
            <ta e="T773" id="Seg_5408" s="T772">bihigi-GA</ta>
            <ta e="T777" id="Seg_5409" s="T776">töttörü</ta>
            <ta e="T778" id="Seg_5410" s="T777">büt-A</ta>
            <ta e="T779" id="Seg_5411" s="T778">štobɨ</ta>
            <ta e="T780" id="Seg_5412" s="T779">dʼɨl-tI-n</ta>
            <ta e="T781" id="Seg_5413" s="T780">hüter-I-m-AːrI</ta>
            <ta e="T782" id="Seg_5414" s="T781">ka</ta>
            <ta e="T789" id="Seg_5415" s="T788">össü͡ö</ta>
            <ta e="T791" id="Seg_5416" s="T790">ikki</ta>
            <ta e="T792" id="Seg_5417" s="T791">dʼɨl-nI</ta>
            <ta e="T793" id="Seg_5418" s="T792">naːda</ta>
            <ta e="T794" id="Seg_5419" s="T793">ü͡ören-IAK-GA</ta>
            <ta e="T801" id="Seg_5420" s="T800">üs</ta>
            <ta e="T802" id="Seg_5421" s="T801">dʼɨl</ta>
            <ta e="T806" id="Seg_5422" s="T805">üle</ta>
            <ta e="T807" id="Seg_5423" s="T806">ü͡ören-Ar-LAr</ta>
            <ta e="T817" id="Seg_5424" s="T816">biːrges</ta>
            <ta e="T818" id="Seg_5425" s="T817">vnuk-BI-n</ta>
            <ta e="T820" id="Seg_5426" s="T819">kɨːs-I-m</ta>
            <ta e="T821" id="Seg_5427" s="T820">u͡ol-tI-n</ta>
            <ta e="T822" id="Seg_5428" s="T821">kɨtta</ta>
            <ta e="T823" id="Seg_5429" s="T822">vnuk-BI-n</ta>
            <ta e="T824" id="Seg_5430" s="T823">kɨtta</ta>
            <ta e="T825" id="Seg_5431" s="T824">baːr-BIn</ta>
            <ta e="T826" id="Seg_5432" s="T825">dʼi͡e-BA-r</ta>
            <ta e="T828" id="Seg_5433" s="T827">hette-Is</ta>
            <ta e="T829" id="Seg_5434" s="T828">kɨlaːs-GA</ta>
            <ta e="T830" id="Seg_5435" s="T829">ü͡ören-Ar</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_5436" s="T1">1SG.[NOM]</ta>
            <ta e="T3" id="Seg_5437" s="T2">name-EP-1SG.[NOM]</ta>
            <ta e="T4" id="Seg_5438" s="T3">Stepanida</ta>
            <ta e="T5" id="Seg_5439" s="T4">Dolgan-SIM</ta>
            <ta e="T6" id="Seg_5440" s="T5">Stepanida</ta>
            <ta e="T7" id="Seg_5441" s="T6">say-HAB-3PL</ta>
            <ta e="T9" id="Seg_5442" s="T8">be.born-PST2-EP-1SG</ta>
            <ta e="T10" id="Seg_5443" s="T9">Popigaj</ta>
            <ta e="T11" id="Seg_5444" s="T10">say-CVB.SEQ</ta>
            <ta e="T12" id="Seg_5445" s="T11">place-DAT/LOC</ta>
            <ta e="T14" id="Seg_5446" s="T13">1SG.[NOM]</ta>
            <ta e="T15" id="Seg_5447" s="T14">parents-PL-EP-1SG.[NOM]</ta>
            <ta e="T16" id="Seg_5448" s="T15">father-1SG.[NOM]</ta>
            <ta e="T17" id="Seg_5449" s="T16">who.[NOM]</ta>
            <ta e="T18" id="Seg_5450" s="T17">Nikolay.[NOM]</ta>
            <ta e="T19" id="Seg_5451" s="T18">mum-1SG.[NOM]</ta>
            <ta e="T20" id="Seg_5452" s="T19">this</ta>
            <ta e="T21" id="Seg_5453" s="T20">Taiska.[NOM]</ta>
            <ta e="T22" id="Seg_5454" s="T21">Taiska.[NOM]</ta>
            <ta e="T24" id="Seg_5455" s="T23">that-3SG-2SG.[NOM]</ta>
            <ta e="T25" id="Seg_5456" s="T24">mum-1SG.[NOM]</ta>
            <ta e="T26" id="Seg_5457" s="T25">early</ta>
            <ta e="T27" id="Seg_5458" s="T26">very</ta>
            <ta e="T28" id="Seg_5459" s="T27">die-PST2-3SG</ta>
            <ta e="T29" id="Seg_5460" s="T28">that-ABL</ta>
            <ta e="T30" id="Seg_5461" s="T29">1SG.[NOM]</ta>
            <ta e="T31" id="Seg_5462" s="T30">father-1SG-ACC</ta>
            <ta e="T32" id="Seg_5463" s="T31">with</ta>
            <ta e="T33" id="Seg_5464" s="T32">stay-PST2-EP-1SG</ta>
            <ta e="T35" id="Seg_5465" s="T34">father-1SG.[NOM]</ta>
            <ta e="T36" id="Seg_5466" s="T35">MOD</ta>
            <ta e="T37" id="Seg_5467" s="T36">who-DAT/LOC</ta>
            <ta e="T38" id="Seg_5468" s="T37">shepherd-DAT/LOC</ta>
            <ta e="T39" id="Seg_5469" s="T38">brigade.leader-DAT/LOC</ta>
            <ta e="T40" id="Seg_5470" s="T39">work-PTCP.PRS</ta>
            <ta e="T41" id="Seg_5471" s="T40">be-PST1-3SG</ta>
            <ta e="T43" id="Seg_5472" s="T42">that-ABL</ta>
            <ta e="T44" id="Seg_5473" s="T43">brother-EP-1SG.[NOM]</ta>
            <ta e="T45" id="Seg_5474" s="T44">also</ta>
            <ta e="T46" id="Seg_5475" s="T45">father-1SG-ACC</ta>
            <ta e="T47" id="Seg_5476" s="T46">with</ta>
            <ta e="T48" id="Seg_5477" s="T47">drive-HAB.[3SG]</ta>
            <ta e="T50" id="Seg_5478" s="T49">tundra-DAT/LOC</ta>
            <ta e="T51" id="Seg_5479" s="T50">1PL.[NOM]</ta>
            <ta e="T52" id="Seg_5480" s="T51">nomadize-CVB.SIM</ta>
            <ta e="T53" id="Seg_5481" s="T52">go-HAB-1PL</ta>
            <ta e="T55" id="Seg_5482" s="T54">school-ABL</ta>
            <ta e="T56" id="Seg_5483" s="T55">holiday-1SG-DAT/LOC</ta>
            <ta e="T57" id="Seg_5484" s="T56">come-TEMP-1SG</ta>
            <ta e="T58" id="Seg_5485" s="T57">1SG.[NOM]</ta>
            <ta e="T59" id="Seg_5486" s="T58">father-1SG-DAT/LOC</ta>
            <ta e="T60" id="Seg_5487" s="T59">be-HAB-1SG</ta>
            <ta e="T62" id="Seg_5488" s="T61">but</ta>
            <ta e="T63" id="Seg_5489" s="T62">grow-PTCP.PST-EP-1SG.[NOM]</ta>
            <ta e="T64" id="Seg_5490" s="T63">after</ta>
            <ta e="T65" id="Seg_5491" s="T64">1SG.[NOM]</ta>
            <ta e="T66" id="Seg_5492" s="T65">father-1SG-DAT/LOC</ta>
            <ta e="T67" id="Seg_5493" s="T66">be-PST2-EP-1SG</ta>
            <ta e="T68" id="Seg_5494" s="T67">when</ta>
            <ta e="T69" id="Seg_5495" s="T68">1SG.[NOM]</ta>
            <ta e="T70" id="Seg_5496" s="T69">small.[NOM]</ta>
            <ta e="T71" id="Seg_5497" s="T70">be-TEMP-1SG</ta>
            <ta e="T72" id="Seg_5498" s="T71">human.being-PL-DAT/LOC</ta>
            <ta e="T73" id="Seg_5499" s="T72">bring.up-EP-PASS/REFL-EP-PST2-EP-1SG</ta>
            <ta e="T74" id="Seg_5500" s="T73">different</ta>
            <ta e="T75" id="Seg_5501" s="T74">human.being-PL-DAT/LOC</ta>
            <ta e="T77" id="Seg_5502" s="T76">bring.up-EP-PASS/REFL-EP-PST2-EP-1SG</ta>
            <ta e="T78" id="Seg_5503" s="T77">that-ABL</ta>
            <ta e="T79" id="Seg_5504" s="T78">grow-CVB.SEQ</ta>
            <ta e="T80" id="Seg_5505" s="T79">go-CVB.SEQ-1SG</ta>
            <ta e="T81" id="Seg_5506" s="T80">EMPH</ta>
            <ta e="T82" id="Seg_5507" s="T81">father-1SG-DAT/LOC</ta>
            <ta e="T83" id="Seg_5508" s="T82">be-PST2-EP-1SG</ta>
            <ta e="T84" id="Seg_5509" s="T83">go-HAB-1SG</ta>
            <ta e="T85" id="Seg_5510" s="T84">father-1SG-DAT/LOC</ta>
            <ta e="T86" id="Seg_5511" s="T85">holiday-DAT/LOC</ta>
            <ta e="T87" id="Seg_5512" s="T86">come-TEMP-1SG</ta>
            <ta e="T89" id="Seg_5513" s="T88">well</ta>
            <ta e="T90" id="Seg_5514" s="T89">that</ta>
            <ta e="T91" id="Seg_5515" s="T90">nomadize-CVB.SIM</ta>
            <ta e="T92" id="Seg_5516" s="T91">go-HAB-1PL</ta>
            <ta e="T93" id="Seg_5517" s="T92">who-ABL</ta>
            <ta e="T94" id="Seg_5518" s="T93">place-ABL</ta>
            <ta e="T95" id="Seg_5519" s="T94">place-DAT/LOC</ta>
            <ta e="T96" id="Seg_5520" s="T95">until</ta>
            <ta e="T101" id="Seg_5521" s="T100">nomadize-CVB.SIM</ta>
            <ta e="T102" id="Seg_5522" s="T101">go-HAB-1PL</ta>
            <ta e="T104" id="Seg_5523" s="T103">tent-DAT/LOC</ta>
            <ta e="T105" id="Seg_5524" s="T104">house-PROPR.[NOM]</ta>
            <ta e="T106" id="Seg_5525" s="T105">be-HAB-1PL</ta>
            <ta e="T107" id="Seg_5526" s="T106">there</ta>
            <ta e="T109" id="Seg_5527" s="T108">eh</ta>
            <ta e="T110" id="Seg_5528" s="T109">snow-ACC</ta>
            <ta e="T111" id="Seg_5529" s="T110">who-VBZ-PRS-1PL</ta>
            <ta e="T112" id="Seg_5530" s="T111">clean.up-VBZ-PRS-1PL</ta>
            <ta e="T120" id="Seg_5531" s="T119">tent.[NOM]</ta>
            <ta e="T121" id="Seg_5532" s="T120">tent.[NOM]</ta>
            <ta e="T122" id="Seg_5533" s="T121">make-PRS-3PL</ta>
            <ta e="T133" id="Seg_5534" s="T132">EMPH</ta>
            <ta e="T138" id="Seg_5535" s="T137">pole.[NOM]</ta>
            <ta e="T139" id="Seg_5536" s="T138">tent-DAT/LOC</ta>
            <ta e="T140" id="Seg_5537" s="T139">mhm</ta>
            <ta e="T142" id="Seg_5538" s="T141">pole.tent-PL-ACC</ta>
            <ta e="T144" id="Seg_5539" s="T143">who-ABL</ta>
            <ta e="T149" id="Seg_5540" s="T148">that</ta>
            <ta e="T150" id="Seg_5541" s="T149">house-2SG.[NOM]</ta>
            <ta e="T151" id="Seg_5542" s="T150">sometimes</ta>
            <ta e="T152" id="Seg_5543" s="T151">broad</ta>
            <ta e="T153" id="Seg_5544" s="T152">very</ta>
            <ta e="T154" id="Seg_5545" s="T153">make-PRS-3PL</ta>
            <ta e="T155" id="Seg_5546" s="T154">many</ta>
            <ta e="T156" id="Seg_5547" s="T155">human.being.[NOM]</ta>
            <ta e="T157" id="Seg_5548" s="T156">that</ta>
            <ta e="T158" id="Seg_5549" s="T157">make-CVB.SEQ</ta>
            <ta e="T159" id="Seg_5550" s="T158">many-1PL</ta>
            <ta e="T163" id="Seg_5551" s="T162">many-1PL</ta>
            <ta e="T164" id="Seg_5552" s="T163">though</ta>
            <ta e="T167" id="Seg_5553" s="T166">who.[NOM]</ta>
            <ta e="T168" id="Seg_5554" s="T167">broad</ta>
            <ta e="T169" id="Seg_5555" s="T168">house.[NOM]</ta>
            <ta e="T170" id="Seg_5556" s="T169">broad</ta>
            <ta e="T171" id="Seg_5557" s="T170">tent.[NOM]</ta>
            <ta e="T173" id="Seg_5558" s="T172">then</ta>
            <ta e="T174" id="Seg_5559" s="T173">oven-1PL.[NOM]</ta>
            <ta e="T175" id="Seg_5560" s="T174">stove.[NOM]</ta>
            <ta e="T176" id="Seg_5561" s="T175">iron</ta>
            <ta e="T177" id="Seg_5562" s="T176">stove.[NOM]</ta>
            <ta e="T188" id="Seg_5563" s="T187">who-3SG-ACC</ta>
            <ta e="T189" id="Seg_5564" s="T188">lower.part-3SG-ACC</ta>
            <ta e="T191" id="Seg_5565" s="T190">who-VBZ-PRS-3PL</ta>
            <ta e="T193" id="Seg_5566" s="T192">fit-NMNZ</ta>
            <ta e="T194" id="Seg_5567" s="T193">make-PRS-3PL</ta>
            <ta e="T195" id="Seg_5568" s="T194">weigh-CAUS-PRS-3PL</ta>
            <ta e="T199" id="Seg_5569" s="T198">who-VBZ-NEG-PTCP.FUT-3SG-ACC</ta>
            <ta e="T200" id="Seg_5570" s="T199">blow-EP-NEG-PTCP.FUT-3SG-ACC</ta>
            <ta e="T202" id="Seg_5571" s="T201">even</ta>
            <ta e="T203" id="Seg_5572" s="T202">small</ta>
            <ta e="T204" id="Seg_5573" s="T203">child-PROPR.[NOM]</ta>
            <ta e="T205" id="Seg_5574" s="T204">ride-VBZ-HAB-1PL</ta>
            <ta e="T211" id="Seg_5575" s="T210">who.[NOM]</ta>
            <ta e="T212" id="Seg_5576" s="T211">Dolgan</ta>
            <ta e="T213" id="Seg_5577" s="T212">cradle-DAT/LOC</ta>
            <ta e="T222" id="Seg_5578" s="T221">go-CVB.SIM</ta>
            <ta e="T223" id="Seg_5579" s="T222">go-PRS-1PL</ta>
            <ta e="T225" id="Seg_5580" s="T224">that-ABL</ta>
            <ta e="T226" id="Seg_5581" s="T225">what-ACC</ta>
            <ta e="T227" id="Seg_5582" s="T226">tell-FUT-1SG=Q</ta>
            <ta e="T228" id="Seg_5583" s="T227">father-1SG.[NOM]</ta>
            <ta e="T229" id="Seg_5584" s="T228">Q</ta>
            <ta e="T230" id="Seg_5585" s="T229">Kamen.[NOM]</ta>
            <ta e="T231" id="Seg_5586" s="T230">who-DAT/LOC</ta>
            <ta e="T232" id="Seg_5587" s="T231">side-DAT/LOC</ta>
            <ta e="T233" id="Seg_5588" s="T232">go-EP-PST2-3SG</ta>
            <ta e="T234" id="Seg_5589" s="T233">Kamen</ta>
            <ta e="T238" id="Seg_5590" s="T237">who.[NOM]</ta>
            <ta e="T239" id="Seg_5591" s="T238">goose.[NOM]</ta>
            <ta e="T240" id="Seg_5592" s="T239">NEG</ta>
            <ta e="T241" id="Seg_5593" s="T240">NEG.EX</ta>
            <ta e="T254" id="Seg_5594" s="T253">who.[NOM]</ta>
            <ta e="T255" id="Seg_5595" s="T254">mouse-PL.[NOM]</ta>
            <ta e="T263" id="Seg_5596" s="T262">still</ta>
            <ta e="T264" id="Seg_5597" s="T263">what-ACC=Q</ta>
            <ta e="T265" id="Seg_5598" s="T264">who.[NOM]</ta>
            <ta e="T266" id="Seg_5599" s="T265">1SG.[NOM]</ta>
            <ta e="T267" id="Seg_5600" s="T266">self-1SG.[NOM]</ta>
            <ta e="T268" id="Seg_5601" s="T267">learn-PST2-EP-1SG</ta>
            <ta e="T269" id="Seg_5602" s="T268">seven</ta>
            <ta e="T270" id="Seg_5603" s="T269">class-DAT/LOC</ta>
            <ta e="T271" id="Seg_5604" s="T270">until</ta>
            <ta e="T272" id="Seg_5605" s="T271">eight-ORD</ta>
            <ta e="T273" id="Seg_5606" s="T272">class-ABL</ta>
            <ta e="T274" id="Seg_5607" s="T273">stop-PST2-EP-1SG</ta>
            <ta e="T276" id="Seg_5608" s="T275">who-ACC</ta>
            <ta e="T277" id="Seg_5609" s="T276">boarding.school-DAT/LOC</ta>
            <ta e="T278" id="Seg_5610" s="T277">that-ABL</ta>
            <ta e="T279" id="Seg_5611" s="T278">boarding.school-1PL.[NOM]</ta>
            <ta e="T280" id="Seg_5612" s="T279">be-HAB-1PL</ta>
            <ta e="T282" id="Seg_5613" s="T281">two</ta>
            <ta e="T283" id="Seg_5614" s="T282">year-ACC</ta>
            <ta e="T284" id="Seg_5615" s="T283">Norilsk-DAT/LOC</ta>
            <ta e="T285" id="Seg_5616" s="T284">learn-PST2-1PL</ta>
            <ta e="T286" id="Seg_5617" s="T285">boarding.school-DAT/LOC</ta>
            <ta e="T288" id="Seg_5618" s="T287">who.[NOM]</ta>
            <ta e="T289" id="Seg_5619" s="T288">live-PST2-1PL</ta>
            <ta e="T291" id="Seg_5620" s="T290">camp-DAT/LOC</ta>
            <ta e="T292" id="Seg_5621" s="T291">go-EP-PST2-EP-1SG</ta>
            <ta e="T293" id="Seg_5622" s="T292">who-DAT/LOC</ta>
            <ta e="T294" id="Seg_5623" s="T293">Tayozhniy-DAT/LOC</ta>
            <ta e="T295" id="Seg_5624" s="T294">go-EP-PST2-EP-1SG</ta>
            <ta e="T296" id="Seg_5625" s="T295">Kureyka-DAT/LOC</ta>
            <ta e="T298" id="Seg_5626" s="T297">then</ta>
            <ta e="T299" id="Seg_5627" s="T298">grow-CVB.SEQ</ta>
            <ta e="T300" id="Seg_5628" s="T299">after</ta>
            <ta e="T301" id="Seg_5629" s="T300">who-DAT/LOC</ta>
            <ta e="T303" id="Seg_5630" s="T302">eh</ta>
            <ta e="T304" id="Seg_5631" s="T303">that</ta>
            <ta e="T305" id="Seg_5632" s="T304">learn-CVB.SIM</ta>
            <ta e="T306" id="Seg_5633" s="T305">go-PST2-EP-1SG</ta>
            <ta e="T312" id="Seg_5634" s="T311">who-DAT/LOC</ta>
            <ta e="T314" id="Seg_5635" s="T313">sell-PTCP.PRS</ta>
            <ta e="T315" id="Seg_5636" s="T314">human.being.[NOM]</ta>
            <ta e="T316" id="Seg_5637" s="T315">well</ta>
            <ta e="T317" id="Seg_5638" s="T316">who-DAT/LOC</ta>
            <ta e="T318" id="Seg_5639" s="T317">salesman-DAT/LOC</ta>
            <ta e="T319" id="Seg_5640" s="T318">go-PST2-EP-1SG</ta>
            <ta e="T320" id="Seg_5641" s="T319">learn-CVB.SIM</ta>
            <ta e="T321" id="Seg_5642" s="T320">Dudinka-DAT/LOC</ta>
            <ta e="T323" id="Seg_5643" s="T322">six</ta>
            <ta e="T324" id="Seg_5644" s="T323">month-ACC</ta>
            <ta e="T325" id="Seg_5645" s="T324">learn-PST2-EP-1SG</ta>
            <ta e="T326" id="Seg_5646" s="T325">that-ABL</ta>
            <ta e="T327" id="Seg_5647" s="T326">come-PST2-EP-1SG</ta>
            <ta e="T328" id="Seg_5648" s="T327">and</ta>
            <ta e="T329" id="Seg_5649" s="T328">Khatanga-DAT/LOC</ta>
            <ta e="T331" id="Seg_5650" s="T330">who-VBZ-PST2-EP-1SG</ta>
            <ta e="T332" id="Seg_5651" s="T331">placement-ACC</ta>
            <ta e="T336" id="Seg_5652" s="T335">who-DAT/LOC</ta>
            <ta e="T337" id="Seg_5653" s="T336">buffet-DAT/LOC</ta>
            <ta e="T338" id="Seg_5654" s="T337">work-HAB-1SG</ta>
            <ta e="T340" id="Seg_5655" s="T339">cantine-DAT/LOC</ta>
            <ta e="T341" id="Seg_5656" s="T340">buffet.[NOM]</ta>
            <ta e="T342" id="Seg_5657" s="T341">be-PST1-3SG</ta>
            <ta e="T343" id="Seg_5658" s="T342">Khatanga-DAT/LOC</ta>
            <ta e="T344" id="Seg_5659" s="T343">there</ta>
            <ta e="T346" id="Seg_5660" s="T345">buffet-DAT/LOC</ta>
            <ta e="T347" id="Seg_5661" s="T346">work-HAB-1SG</ta>
            <ta e="T349" id="Seg_5662" s="T348">that-ABL</ta>
            <ta e="T352" id="Seg_5663" s="T351">Khatanga-DAT/LOC</ta>
            <ta e="T353" id="Seg_5664" s="T352">one</ta>
            <ta e="T354" id="Seg_5665" s="T353">human.being-ACC</ta>
            <ta e="T355" id="Seg_5666" s="T354">here</ta>
            <ta e="T356" id="Seg_5667" s="T355">Kheta-ABL</ta>
            <ta e="T357" id="Seg_5668" s="T356">one</ta>
            <ta e="T358" id="Seg_5669" s="T357">boy-ACC</ta>
            <ta e="T359" id="Seg_5670" s="T358">with</ta>
            <ta e="T360" id="Seg_5671" s="T359">friend-VBZ-RECP/COLL-PST2-EP-1SG</ta>
            <ta e="T370" id="Seg_5672" s="T369">that-ABL</ta>
            <ta e="T371" id="Seg_5673" s="T370">take-RECP/COLL-CVB.SEQ</ta>
            <ta e="T372" id="Seg_5674" s="T371">go-CVB.SEQ-1PL</ta>
            <ta e="T373" id="Seg_5675" s="T372">then</ta>
            <ta e="T374" id="Seg_5676" s="T373">who-VBZ-PST2-1PL</ta>
            <ta e="T375" id="Seg_5677" s="T374">Khatanga-DAT/LOC</ta>
            <ta e="T376" id="Seg_5678" s="T375">live-CVB.SIM</ta>
            <ta e="T377" id="Seg_5679" s="T376">fall-PST2-EP-1SG</ta>
            <ta e="T379" id="Seg_5680" s="T378">1SG.[NOM]</ta>
            <ta e="T380" id="Seg_5681" s="T379">kindergarten-DAT/LOC</ta>
            <ta e="T381" id="Seg_5682" s="T380">work-PTCP.PRS</ta>
            <ta e="T382" id="Seg_5683" s="T381">be-PST1-1SG</ta>
            <ta e="T383" id="Seg_5684" s="T382">nanny-DAT/LOC</ta>
            <ta e="T384" id="Seg_5685" s="T383">and</ta>
            <ta e="T385" id="Seg_5686" s="T384">3SG-2SG.[NOM]</ta>
            <ta e="T386" id="Seg_5687" s="T385">MOD</ta>
            <ta e="T387" id="Seg_5688" s="T386">post.office-DAT/LOC</ta>
            <ta e="T389" id="Seg_5689" s="T388">work-PTCP.PRS</ta>
            <ta e="T390" id="Seg_5690" s="T389">be-PST1-3SG</ta>
            <ta e="T392" id="Seg_5691" s="T391">that-ABL</ta>
            <ta e="T397" id="Seg_5692" s="T396">who.[NOM]</ta>
            <ta e="T398" id="Seg_5693" s="T397">daughter-1PL.[NOM]</ta>
            <ta e="T399" id="Seg_5694" s="T398">daughter-EP-1SG.[NOM]</ta>
            <ta e="T400" id="Seg_5695" s="T399">Rita</ta>
            <ta e="T401" id="Seg_5696" s="T400">name-PROPR</ta>
            <ta e="T402" id="Seg_5697" s="T401">daughter-EP-1SG.[NOM]</ta>
            <ta e="T403" id="Seg_5698" s="T402">boy-EP-1SG.[NOM]</ta>
            <ta e="T405" id="Seg_5699" s="T404">that-ABL</ta>
            <ta e="T411" id="Seg_5700" s="T410">place-3SG-DAT/LOC</ta>
            <ta e="T412" id="Seg_5701" s="T411">come-PTCP.FUT-3SG-ACC</ta>
            <ta e="T413" id="Seg_5702" s="T412">motherland-3SG-DAT/LOC</ta>
            <ta e="T414" id="Seg_5703" s="T413">come-PTCP.FUT-3SG-ACC</ta>
            <ta e="T422" id="Seg_5704" s="T421">send-PST2-3SG</ta>
            <ta e="T424" id="Seg_5705" s="T423">child-1SG-ACC</ta>
            <ta e="T425" id="Seg_5706" s="T424">with</ta>
            <ta e="T426" id="Seg_5707" s="T425">well</ta>
            <ta e="T427" id="Seg_5708" s="T426">daughter-1SG-ACC</ta>
            <ta e="T428" id="Seg_5709" s="T427">with</ta>
            <ta e="T433" id="Seg_5710" s="T432">3SG.[NOM]</ta>
            <ta e="T434" id="Seg_5711" s="T433">back-DAT/LOC</ta>
            <ta e="T435" id="Seg_5712" s="T434">1SG.[NOM]</ta>
            <ta e="T436" id="Seg_5713" s="T435">back-EP-1SG-ABL</ta>
            <ta e="T437" id="Seg_5714" s="T436">3SG.[NOM]</ta>
            <ta e="T438" id="Seg_5715" s="T437">come-PST2-3SG</ta>
            <ta e="T445" id="Seg_5716" s="T444">how.much</ta>
            <ta e="T446" id="Seg_5717" s="T445">year.[NOM]</ta>
            <ta e="T447" id="Seg_5718" s="T446">MOD</ta>
            <ta e="T448" id="Seg_5719" s="T447">well</ta>
            <ta e="T452" id="Seg_5720" s="T451">year-ABL</ta>
            <ta e="T465" id="Seg_5721" s="T464">year-DAT/LOC</ta>
            <ta e="T466" id="Seg_5722" s="T465">die-PST2-3SG</ta>
            <ta e="T473" id="Seg_5723" s="T472">1SG.[NOM]</ta>
            <ta e="T474" id="Seg_5724" s="T473">child-VBZ-PST2-EP-1SG</ta>
            <ta e="T475" id="Seg_5725" s="T474">son-1SG-ACC</ta>
            <ta e="T484" id="Seg_5726" s="T483">Usuriysk-DAT/LOC</ta>
            <ta e="T493" id="Seg_5727" s="T492">live-PRS-1SG</ta>
            <ta e="T494" id="Seg_5728" s="T493">kindergarten-DAT/LOC</ta>
            <ta e="T495" id="Seg_5729" s="T494">work-PST2-EP-1SG</ta>
            <ta e="T499" id="Seg_5730" s="T498">kindergarten-DAT/LOC</ta>
            <ta e="T500" id="Seg_5731" s="T499">this.EMPH</ta>
            <ta e="T501" id="Seg_5732" s="T500">EMPH</ta>
            <ta e="T502" id="Seg_5733" s="T501">kindergarten-DAT/LOC</ta>
            <ta e="T504" id="Seg_5734" s="T503">that-ABL</ta>
            <ta e="T505" id="Seg_5735" s="T504">what-ACC</ta>
            <ta e="T506" id="Seg_5736" s="T505">tell-FUT-1SG=Q</ta>
            <ta e="T507" id="Seg_5737" s="T506">that-ABL</ta>
            <ta e="T508" id="Seg_5738" s="T507">man-EP-1SG.[NOM]</ta>
            <ta e="T509" id="Seg_5739" s="T508">Q</ta>
            <ta e="T510" id="Seg_5740" s="T509">that</ta>
            <ta e="T511" id="Seg_5741" s="T510">man-EP-1SG.[NOM]</ta>
            <ta e="T512" id="Seg_5742" s="T511">fishing-DAT/LOC</ta>
            <ta e="T513" id="Seg_5743" s="T512">be-HAB.[3SG]</ta>
            <ta e="T514" id="Seg_5744" s="T513">there</ta>
            <ta e="T516" id="Seg_5745" s="T515">hunting.spot-DAT/LOC</ta>
            <ta e="T517" id="Seg_5746" s="T516">live-HAB-1PL</ta>
            <ta e="T518" id="Seg_5747" s="T517">close.[NOM]</ta>
            <ta e="T524" id="Seg_5748" s="T523">weekend-PL-1SG-DAT/LOC</ta>
            <ta e="T525" id="Seg_5749" s="T524">3SG-DAT/LOC</ta>
            <ta e="T526" id="Seg_5750" s="T525">go-HAB-1SG</ta>
            <ta e="T527" id="Seg_5751" s="T526">motor.boat-EP-INSTR</ta>
            <ta e="T528" id="Seg_5752" s="T527">ride-VBZ-HAB-1PL</ta>
            <ta e="T533" id="Seg_5753" s="T532">who.[NOM]</ta>
            <ta e="T534" id="Seg_5754" s="T533">help-HAB-1SG</ta>
            <ta e="T537" id="Seg_5755" s="T536">who-ACC</ta>
            <ta e="T540" id="Seg_5756" s="T539">fillet-VBZ-CVB.SIM</ta>
            <ta e="T541" id="Seg_5757" s="T540">and.so.on-CVB.SIM</ta>
            <ta e="T542" id="Seg_5758" s="T541">and.so.on-CVB.SIM</ta>
            <ta e="T544" id="Seg_5759" s="T543">give-HAB-1PL</ta>
            <ta e="T545" id="Seg_5760" s="T544">EMPH</ta>
            <ta e="T546" id="Seg_5761" s="T545">this</ta>
            <ta e="T547" id="Seg_5762" s="T546">who-DAT/LOC</ta>
            <ta e="T548" id="Seg_5763" s="T547">fish.industry-DAT/LOC</ta>
            <ta e="T549" id="Seg_5764" s="T548">give-HAB-1PL</ta>
            <ta e="T550" id="Seg_5765" s="T549">give-VBZ-HAB-1PL</ta>
            <ta e="T559" id="Seg_5766" s="T558">go-HAB-3PL</ta>
            <ta e="T569" id="Seg_5767" s="T568">live-HAB-1PL</ta>
            <ta e="T570" id="Seg_5768" s="T569">night-PL-ACC</ta>
            <ta e="T575" id="Seg_5769" s="T574">eat-PST1-3SG</ta>
            <ta e="T576" id="Seg_5770" s="T575">eat-PST1-3SG</ta>
            <ta e="T577" id="Seg_5771" s="T576">that</ta>
            <ta e="T578" id="Seg_5772" s="T577">child-PL-1PL.[NOM]</ta>
            <ta e="T579" id="Seg_5773" s="T578">well</ta>
            <ta e="T580" id="Seg_5774" s="T579">boy-PL-1PL.[NOM]</ta>
            <ta e="T583" id="Seg_5775" s="T582">boy-EP-1SG.[NOM]</ta>
            <ta e="T584" id="Seg_5776" s="T583">that-ABL</ta>
            <ta e="T585" id="Seg_5777" s="T584">there</ta>
            <ta e="T586" id="Seg_5778" s="T585">still</ta>
            <ta e="T587" id="Seg_5779" s="T586">friend-PL-3SG.[NOM]</ta>
            <ta e="T588" id="Seg_5780" s="T587">man-EP-1SG.[NOM]</ta>
            <ta e="T589" id="Seg_5781" s="T588">sister-3SG-GEN</ta>
            <ta e="T590" id="Seg_5782" s="T589">son-3SG.[NOM]</ta>
            <ta e="T591" id="Seg_5783" s="T590">that</ta>
            <ta e="T593" id="Seg_5784" s="T592">fire-ACC</ta>
            <ta e="T594" id="Seg_5785" s="T593">make-HAB-3PL</ta>
            <ta e="T595" id="Seg_5786" s="T594">smoke.[NOM]</ta>
            <ta e="T596" id="Seg_5787" s="T595">well</ta>
            <ta e="T597" id="Seg_5788" s="T596">that.[NOM]</ta>
            <ta e="T600" id="Seg_5789" s="T599">who-VBZ-NEG-PTCP.FUT-3PL-ACC</ta>
            <ta e="T601" id="Seg_5790" s="T600">come.closer-NEG-PTCP.FUT-3PL-ACC</ta>
            <ta e="T602" id="Seg_5791" s="T601">1PL-DAT/LOC</ta>
            <ta e="T607" id="Seg_5792" s="T606">fillet-VBZ-PTCP.PRS-1PL-DAT/LOC</ta>
            <ta e="T618" id="Seg_5793" s="T617">what-ACC</ta>
            <ta e="T619" id="Seg_5794" s="T618">tell-FUT-1SG=Q</ta>
            <ta e="T621" id="Seg_5795" s="T620">who.[NOM]</ta>
            <ta e="T622" id="Seg_5796" s="T621">that</ta>
            <ta e="T623" id="Seg_5797" s="T622">kindergarten-DAT/LOC</ta>
            <ta e="T624" id="Seg_5798" s="T623">that-ABL</ta>
            <ta e="T625" id="Seg_5799" s="T624">that.[NOM]</ta>
            <ta e="T626" id="Seg_5800" s="T625">retirement-DAT/LOC</ta>
            <ta e="T627" id="Seg_5801" s="T626">go-PST1-1SG</ta>
            <ta e="T628" id="Seg_5802" s="T627">EMPH</ta>
            <ta e="T631" id="Seg_5803" s="T630">thirty</ta>
            <ta e="T632" id="Seg_5804" s="T631">eight</ta>
            <ta e="T633" id="Seg_5805" s="T632">year-ACC</ta>
            <ta e="T634" id="Seg_5806" s="T633">thirty</ta>
            <ta e="T635" id="Seg_5807" s="T634">six</ta>
            <ta e="T636" id="Seg_5808" s="T635">year-ACC</ta>
            <ta e="T637" id="Seg_5809" s="T636">work-PST2-EP-1SG</ta>
            <ta e="T638" id="Seg_5810" s="T637">kindergarten-DAT/LOC</ta>
            <ta e="T640" id="Seg_5811" s="T639">at.first-ACC</ta>
            <ta e="T641" id="Seg_5812" s="T640">school.[NOM]</ta>
            <ta e="T642" id="Seg_5813" s="T641">school.[NOM]</ta>
            <ta e="T643" id="Seg_5814" s="T642">to</ta>
            <ta e="T644" id="Seg_5815" s="T643">work-PST2-EP-1SG</ta>
            <ta e="T645" id="Seg_5816" s="T644">cook-DAT/LOC</ta>
            <ta e="T647" id="Seg_5817" s="T646">work-PST2-EP-1SG</ta>
            <ta e="T648" id="Seg_5818" s="T647">cook-DAT/LOC</ta>
            <ta e="T650" id="Seg_5819" s="T649">school-DAT/LOC</ta>
            <ta e="T651" id="Seg_5820" s="T650">then</ta>
            <ta e="T652" id="Seg_5821" s="T651">kindergarten-DAT/LOC</ta>
            <ta e="T653" id="Seg_5822" s="T652">take-PST2-3PL</ta>
            <ta e="T655" id="Seg_5823" s="T654">eh</ta>
            <ta e="T656" id="Seg_5824" s="T655">who-VBZ-PST2-3PL</ta>
            <ta e="T657" id="Seg_5825" s="T656">bring-PST2-3PL</ta>
            <ta e="T658" id="Seg_5826" s="T657">who.[NOM]</ta>
            <ta e="T659" id="Seg_5827" s="T658">then</ta>
            <ta e="T660" id="Seg_5828" s="T659">school-ABL</ta>
            <ta e="T661" id="Seg_5829" s="T660">kindergarten-DAT/LOC</ta>
            <ta e="T662" id="Seg_5830" s="T661">work-PST2-EP-1SG</ta>
            <ta e="T666" id="Seg_5831" s="T665">retirement-DAT/LOC</ta>
            <ta e="T667" id="Seg_5832" s="T666">live-PRS-1SG</ta>
            <ta e="T670" id="Seg_5833" s="T669">grandchild-PL-1SG-ACC</ta>
            <ta e="T672" id="Seg_5834" s="T671">great.granchild-1SG-ACC</ta>
            <ta e="T676" id="Seg_5835" s="T675">work-PRS-1SG</ta>
            <ta e="T677" id="Seg_5836" s="T676">work-CVB.SIM</ta>
            <ta e="T678" id="Seg_5837" s="T677">go-PRS-1SG</ta>
            <ta e="T686" id="Seg_5838" s="T685">and</ta>
            <ta e="T687" id="Seg_5839" s="T686">1SG.[NOM]</ta>
            <ta e="T688" id="Seg_5840" s="T687">that</ta>
            <ta e="T689" id="Seg_5841" s="T688">child-ACC</ta>
            <ta e="T690" id="Seg_5842" s="T689">with</ta>
            <ta e="T691" id="Seg_5843" s="T690">sit-PRS-1SG</ta>
            <ta e="T695" id="Seg_5844" s="T694">twenty-COMP</ta>
            <ta e="T696" id="Seg_5845" s="T695">year-VBZ-REFL-FUT-3SG</ta>
            <ta e="T697" id="Seg_5846" s="T696">March-DAT/LOC</ta>
            <ta e="T705" id="Seg_5847" s="T704">who-VBZ-PST2.[3SG]</ta>
            <ta e="T706" id="Seg_5848" s="T705">a.little.bit</ta>
            <ta e="T707" id="Seg_5849" s="T706">be.sick-EP-PST2.[3SG]</ta>
            <ta e="T708" id="Seg_5850" s="T707">stomatitis-VBZ-PST2.[3SG]</ta>
            <ta e="T710" id="Seg_5851" s="T709">mum-3SG.[NOM]</ta>
            <ta e="T711" id="Seg_5852" s="T710">sit-PRS.[3SG]</ta>
            <ta e="T715" id="Seg_5853" s="T714">so</ta>
            <ta e="T716" id="Seg_5854" s="T715">who.[NOM]</ta>
            <ta e="T717" id="Seg_5855" s="T716">this</ta>
            <ta e="T718" id="Seg_5856" s="T717">big</ta>
            <ta e="T719" id="Seg_5857" s="T718">grandchild-EP-1SG.[NOM]</ta>
            <ta e="T720" id="Seg_5858" s="T719">son-3SG-GEN</ta>
            <ta e="T721" id="Seg_5859" s="T720">own-3SG</ta>
            <ta e="T722" id="Seg_5860" s="T721">big</ta>
            <ta e="T726" id="Seg_5861" s="T725">grandchild-EP-1SG.[NOM]</ta>
            <ta e="T727" id="Seg_5862" s="T726">big</ta>
            <ta e="T728" id="Seg_5863" s="T727">now</ta>
            <ta e="T729" id="Seg_5864" s="T728">serve-VBZ-PRS.[3SG]</ta>
            <ta e="T730" id="Seg_5865" s="T729">who-DAT/LOC</ta>
            <ta e="T731" id="Seg_5866" s="T730">army-DAT/LOC</ta>
            <ta e="T835" id="Seg_5867" s="T732">other.of.two-PL.[NOM]</ta>
            <ta e="T733" id="Seg_5868" s="T835">learn-PRS-3PL</ta>
            <ta e="T734" id="Seg_5869" s="T733">and</ta>
            <ta e="T735" id="Seg_5870" s="T734">daughter-3SG.[NOM]</ta>
            <ta e="T736" id="Seg_5871" s="T735">daughter-EP-1SG.[NOM]</ta>
            <ta e="T737" id="Seg_5872" s="T736">granddaughter-1SG.[NOM]</ta>
            <ta e="T738" id="Seg_5873" s="T737">big</ta>
            <ta e="T739" id="Seg_5874" s="T738">this</ta>
            <ta e="T740" id="Seg_5875" s="T739">boy-EP-1SG.[NOM]</ta>
            <ta e="T742" id="Seg_5876" s="T741">daughter-3SG.[NOM]</ta>
            <ta e="T743" id="Seg_5877" s="T742">granddaughter-1SG.[NOM]</ta>
            <ta e="T744" id="Seg_5878" s="T743">Katya.[NOM]</ta>
            <ta e="T745" id="Seg_5879" s="T744">name-3SG.[NOM]</ta>
            <ta e="T746" id="Seg_5880" s="T745">that.[NOM]</ta>
            <ta e="T748" id="Seg_5881" s="T747">who-DAT/LOC</ta>
            <ta e="T749" id="Seg_5882" s="T748">learn-PRS.[3SG]</ta>
            <ta e="T750" id="Seg_5883" s="T749">also</ta>
            <ta e="T751" id="Seg_5884" s="T750">mum-3SG-ACC</ta>
            <ta e="T752" id="Seg_5885" s="T751">similar</ta>
            <ta e="T754" id="Seg_5886" s="T753">this</ta>
            <ta e="T755" id="Seg_5887" s="T754">Norilsk</ta>
            <ta e="T756" id="Seg_5888" s="T755">who-DAT/LOC</ta>
            <ta e="T757" id="Seg_5889" s="T756">technical.college-DAT/LOC</ta>
            <ta e="T758" id="Seg_5890" s="T757">learn-PRS.[3SG]</ta>
            <ta e="T759" id="Seg_5891" s="T758">mum-3SG-ACC</ta>
            <ta e="T760" id="Seg_5892" s="T759">similar</ta>
            <ta e="T767" id="Seg_5893" s="T766">that.[NOM]</ta>
            <ta e="T768" id="Seg_5894" s="T767">who-DAT/LOC</ta>
            <ta e="T772" id="Seg_5895" s="T771">let-PST2-3SG</ta>
            <ta e="T773" id="Seg_5896" s="T772">1PL-DAT/LOC</ta>
            <ta e="T777" id="Seg_5897" s="T776">back</ta>
            <ta e="T778" id="Seg_5898" s="T777">stop-CVB.SIM</ta>
            <ta e="T779" id="Seg_5899" s="T778">in.order</ta>
            <ta e="T780" id="Seg_5900" s="T779">year-3SG-ACC</ta>
            <ta e="T781" id="Seg_5901" s="T780">lose-EP-NEG-CVB.PURP</ta>
            <ta e="T782" id="Seg_5902" s="T781">well</ta>
            <ta e="T789" id="Seg_5903" s="T788">still</ta>
            <ta e="T791" id="Seg_5904" s="T790">two</ta>
            <ta e="T792" id="Seg_5905" s="T791">year-ACC</ta>
            <ta e="T793" id="Seg_5906" s="T792">need.to</ta>
            <ta e="T794" id="Seg_5907" s="T793">learn-PTCP.FUT-DAT/LOC</ta>
            <ta e="T801" id="Seg_5908" s="T800">three</ta>
            <ta e="T802" id="Seg_5909" s="T801">year.[NOM]</ta>
            <ta e="T806" id="Seg_5910" s="T805">work.[NOM]</ta>
            <ta e="T807" id="Seg_5911" s="T806">learn-PRS-3PL</ta>
            <ta e="T817" id="Seg_5912" s="T816">one.out.of.two</ta>
            <ta e="T818" id="Seg_5913" s="T817">grandchild-1SG-ACC</ta>
            <ta e="T820" id="Seg_5914" s="T819">daughter-EP-1SG.[NOM]</ta>
            <ta e="T821" id="Seg_5915" s="T820">son-3SG-ACC</ta>
            <ta e="T822" id="Seg_5916" s="T821">with</ta>
            <ta e="T823" id="Seg_5917" s="T822">grandchild-1SG-ACC</ta>
            <ta e="T824" id="Seg_5918" s="T823">with</ta>
            <ta e="T825" id="Seg_5919" s="T824">there.is-1SG</ta>
            <ta e="T826" id="Seg_5920" s="T825">house-1SG-DAT/LOC</ta>
            <ta e="T828" id="Seg_5921" s="T827">seven-ORD</ta>
            <ta e="T829" id="Seg_5922" s="T828">class-DAT/LOC</ta>
            <ta e="T830" id="Seg_5923" s="T829">learn-PRS.[3SG]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T2" id="Seg_5924" s="T1">1SG.[NOM]</ta>
            <ta e="T3" id="Seg_5925" s="T2">Name-EP-1SG.[NOM]</ta>
            <ta e="T4" id="Seg_5926" s="T3">Stepanida</ta>
            <ta e="T5" id="Seg_5927" s="T4">dolganisch-SIM</ta>
            <ta e="T6" id="Seg_5928" s="T5">Stepanida</ta>
            <ta e="T7" id="Seg_5929" s="T6">sagen-HAB-3PL</ta>
            <ta e="T9" id="Seg_5930" s="T8">geboren.werden-PST2-EP-1SG</ta>
            <ta e="T10" id="Seg_5931" s="T9">Popigaj</ta>
            <ta e="T11" id="Seg_5932" s="T10">sagen-CVB.SEQ</ta>
            <ta e="T12" id="Seg_5933" s="T11">Ort-DAT/LOC</ta>
            <ta e="T14" id="Seg_5934" s="T13">1SG.[NOM]</ta>
            <ta e="T15" id="Seg_5935" s="T14">Eltern-PL-EP-1SG.[NOM]</ta>
            <ta e="T16" id="Seg_5936" s="T15">Vater-1SG.[NOM]</ta>
            <ta e="T17" id="Seg_5937" s="T16">wer.[NOM]</ta>
            <ta e="T18" id="Seg_5938" s="T17">Nikolaj.[NOM]</ta>
            <ta e="T19" id="Seg_5939" s="T18">Mama-1SG.[NOM]</ta>
            <ta e="T20" id="Seg_5940" s="T19">dies</ta>
            <ta e="T21" id="Seg_5941" s="T20">Taiska.[NOM]</ta>
            <ta e="T22" id="Seg_5942" s="T21">Taiska.[NOM]</ta>
            <ta e="T24" id="Seg_5943" s="T23">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T25" id="Seg_5944" s="T24">Mama-1SG.[NOM]</ta>
            <ta e="T26" id="Seg_5945" s="T25">früh</ta>
            <ta e="T27" id="Seg_5946" s="T26">sehr</ta>
            <ta e="T28" id="Seg_5947" s="T27">sterben-PST2-3SG</ta>
            <ta e="T29" id="Seg_5948" s="T28">jenes-ABL</ta>
            <ta e="T30" id="Seg_5949" s="T29">1SG.[NOM]</ta>
            <ta e="T31" id="Seg_5950" s="T30">Vater-1SG-ACC</ta>
            <ta e="T32" id="Seg_5951" s="T31">mit</ta>
            <ta e="T33" id="Seg_5952" s="T32">bleiben-PST2-EP-1SG</ta>
            <ta e="T35" id="Seg_5953" s="T34">Vater-1SG.[NOM]</ta>
            <ta e="T36" id="Seg_5954" s="T35">MOD</ta>
            <ta e="T37" id="Seg_5955" s="T36">wer-DAT/LOC</ta>
            <ta e="T38" id="Seg_5956" s="T37">Hirte-DAT/LOC</ta>
            <ta e="T39" id="Seg_5957" s="T38">Brigadeleiter-DAT/LOC</ta>
            <ta e="T40" id="Seg_5958" s="T39">arbeiten-PTCP.PRS</ta>
            <ta e="T41" id="Seg_5959" s="T40">sein-PST1-3SG</ta>
            <ta e="T43" id="Seg_5960" s="T42">jenes-ABL</ta>
            <ta e="T44" id="Seg_5961" s="T43">Bruder-EP-1SG.[NOM]</ta>
            <ta e="T45" id="Seg_5962" s="T44">auch</ta>
            <ta e="T46" id="Seg_5963" s="T45">Vater-1SG-ACC</ta>
            <ta e="T47" id="Seg_5964" s="T46">mit</ta>
            <ta e="T48" id="Seg_5965" s="T47">fahren-HAB.[3SG]</ta>
            <ta e="T50" id="Seg_5966" s="T49">Tundra-DAT/LOC</ta>
            <ta e="T51" id="Seg_5967" s="T50">1PL.[NOM]</ta>
            <ta e="T52" id="Seg_5968" s="T51">nomadisieren-CVB.SIM</ta>
            <ta e="T53" id="Seg_5969" s="T52">gehen-HAB-1PL</ta>
            <ta e="T55" id="Seg_5970" s="T54">Schule-ABL</ta>
            <ta e="T56" id="Seg_5971" s="T55">Urlaub-1SG-DAT/LOC</ta>
            <ta e="T57" id="Seg_5972" s="T56">kommen-TEMP-1SG</ta>
            <ta e="T58" id="Seg_5973" s="T57">1SG.[NOM]</ta>
            <ta e="T59" id="Seg_5974" s="T58">Vater-1SG-DAT/LOC</ta>
            <ta e="T60" id="Seg_5975" s="T59">sein-HAB-1SG</ta>
            <ta e="T62" id="Seg_5976" s="T61">aber</ta>
            <ta e="T63" id="Seg_5977" s="T62">wachsen-PTCP.PST-EP-1SG.[NOM]</ta>
            <ta e="T64" id="Seg_5978" s="T63">nachdem</ta>
            <ta e="T65" id="Seg_5979" s="T64">1SG.[NOM]</ta>
            <ta e="T66" id="Seg_5980" s="T65">Vater-1SG-DAT/LOC</ta>
            <ta e="T67" id="Seg_5981" s="T66">sein-PST2-EP-1SG</ta>
            <ta e="T68" id="Seg_5982" s="T67">wenn</ta>
            <ta e="T69" id="Seg_5983" s="T68">1SG.[NOM]</ta>
            <ta e="T70" id="Seg_5984" s="T69">klein.[NOM]</ta>
            <ta e="T71" id="Seg_5985" s="T70">sein-TEMP-1SG</ta>
            <ta e="T72" id="Seg_5986" s="T71">Mensch-PL-DAT/LOC</ta>
            <ta e="T73" id="Seg_5987" s="T72">aufziehen-EP-PASS/REFL-EP-PST2-EP-1SG</ta>
            <ta e="T74" id="Seg_5988" s="T73">anders</ta>
            <ta e="T75" id="Seg_5989" s="T74">Mensch-PL-DAT/LOC</ta>
            <ta e="T77" id="Seg_5990" s="T76">aufziehen-EP-PASS/REFL-EP-PST2-EP-1SG</ta>
            <ta e="T78" id="Seg_5991" s="T77">jenes-ABL</ta>
            <ta e="T79" id="Seg_5992" s="T78">wachsen-CVB.SEQ</ta>
            <ta e="T80" id="Seg_5993" s="T79">gehen-CVB.SEQ-1SG</ta>
            <ta e="T81" id="Seg_5994" s="T80">EMPH</ta>
            <ta e="T82" id="Seg_5995" s="T81">Vater-1SG-DAT/LOC</ta>
            <ta e="T83" id="Seg_5996" s="T82">sein-PST2-EP-1SG</ta>
            <ta e="T84" id="Seg_5997" s="T83">gehen-HAB-1SG</ta>
            <ta e="T85" id="Seg_5998" s="T84">Vater-1SG-DAT/LOC</ta>
            <ta e="T86" id="Seg_5999" s="T85">Urlaub-DAT/LOC</ta>
            <ta e="T87" id="Seg_6000" s="T86">kommen-TEMP-1SG</ta>
            <ta e="T89" id="Seg_6001" s="T88">doch</ta>
            <ta e="T90" id="Seg_6002" s="T89">jenes</ta>
            <ta e="T91" id="Seg_6003" s="T90">nomadisieren-CVB.SIM</ta>
            <ta e="T92" id="Seg_6004" s="T91">gehen-HAB-1PL</ta>
            <ta e="T93" id="Seg_6005" s="T92">wer-ABL</ta>
            <ta e="T94" id="Seg_6006" s="T93">Ort-ABL</ta>
            <ta e="T95" id="Seg_6007" s="T94">Ort-DAT/LOC</ta>
            <ta e="T96" id="Seg_6008" s="T95">bis.zu</ta>
            <ta e="T101" id="Seg_6009" s="T100">nomadisieren-CVB.SIM</ta>
            <ta e="T102" id="Seg_6010" s="T101">gehen-HAB-1PL</ta>
            <ta e="T104" id="Seg_6011" s="T103">Zelt-DAT/LOC</ta>
            <ta e="T105" id="Seg_6012" s="T104">Haus-PROPR.[NOM]</ta>
            <ta e="T106" id="Seg_6013" s="T105">sein-HAB-1PL</ta>
            <ta e="T107" id="Seg_6014" s="T106">dort</ta>
            <ta e="T109" id="Seg_6015" s="T108">äh</ta>
            <ta e="T110" id="Seg_6016" s="T109">Schnee-ACC</ta>
            <ta e="T111" id="Seg_6017" s="T110">wer-VBZ-PRS-1PL</ta>
            <ta e="T112" id="Seg_6018" s="T111">aufraeumen-VBZ-PRS-1PL</ta>
            <ta e="T120" id="Seg_6019" s="T119">Zelt.[NOM]</ta>
            <ta e="T121" id="Seg_6020" s="T120">Zelt.[NOM]</ta>
            <ta e="T122" id="Seg_6021" s="T121">machen-PRS-3PL</ta>
            <ta e="T133" id="Seg_6022" s="T132">EMPH</ta>
            <ta e="T138" id="Seg_6023" s="T137">Stange.[NOM]</ta>
            <ta e="T139" id="Seg_6024" s="T138">Zelt-DAT/LOC</ta>
            <ta e="T140" id="Seg_6025" s="T139">aha</ta>
            <ta e="T142" id="Seg_6026" s="T141">Stangenzelt-PL-ACC</ta>
            <ta e="T144" id="Seg_6027" s="T143">wer-ABL</ta>
            <ta e="T149" id="Seg_6028" s="T148">jenes</ta>
            <ta e="T150" id="Seg_6029" s="T149">Haus-2SG.[NOM]</ta>
            <ta e="T151" id="Seg_6030" s="T150">manchmal</ta>
            <ta e="T152" id="Seg_6031" s="T151">breit</ta>
            <ta e="T153" id="Seg_6032" s="T152">sehr</ta>
            <ta e="T154" id="Seg_6033" s="T153">machen-PRS-3PL</ta>
            <ta e="T155" id="Seg_6034" s="T154">viel</ta>
            <ta e="T156" id="Seg_6035" s="T155">Mensch.[NOM]</ta>
            <ta e="T157" id="Seg_6036" s="T156">jenes</ta>
            <ta e="T158" id="Seg_6037" s="T157">machen-CVB.SEQ</ta>
            <ta e="T159" id="Seg_6038" s="T158">viel-1PL</ta>
            <ta e="T163" id="Seg_6039" s="T162">viel-1PL</ta>
            <ta e="T164" id="Seg_6040" s="T163">doch</ta>
            <ta e="T167" id="Seg_6041" s="T166">wer.[NOM]</ta>
            <ta e="T168" id="Seg_6042" s="T167">breit</ta>
            <ta e="T169" id="Seg_6043" s="T168">Haus.[NOM]</ta>
            <ta e="T170" id="Seg_6044" s="T169">breit</ta>
            <ta e="T171" id="Seg_6045" s="T170">Zelt.[NOM]</ta>
            <ta e="T173" id="Seg_6046" s="T172">dann</ta>
            <ta e="T174" id="Seg_6047" s="T173">Ofen-1PL.[NOM]</ta>
            <ta e="T175" id="Seg_6048" s="T174">Herd.[NOM]</ta>
            <ta e="T176" id="Seg_6049" s="T175">eisern</ta>
            <ta e="T177" id="Seg_6050" s="T176">Herd.[NOM]</ta>
            <ta e="T188" id="Seg_6051" s="T187">wer-3SG-ACC</ta>
            <ta e="T189" id="Seg_6052" s="T188">Unterteil-3SG-ACC</ta>
            <ta e="T191" id="Seg_6053" s="T190">wer-VBZ-PRS-3PL</ta>
            <ta e="T193" id="Seg_6054" s="T192">passen-NMNZ</ta>
            <ta e="T194" id="Seg_6055" s="T193">machen-PRS-3PL</ta>
            <ta e="T195" id="Seg_6056" s="T194">lasten-CAUS-PRS-3PL</ta>
            <ta e="T199" id="Seg_6057" s="T198">wer-VBZ-NEG-PTCP.FUT-3SG-ACC</ta>
            <ta e="T200" id="Seg_6058" s="T199">wehen-EP-NEG-PTCP.FUT-3SG-ACC</ta>
            <ta e="T202" id="Seg_6059" s="T201">sogar</ta>
            <ta e="T203" id="Seg_6060" s="T202">klein</ta>
            <ta e="T204" id="Seg_6061" s="T203">Kind-PROPR.[NOM]</ta>
            <ta e="T205" id="Seg_6062" s="T204">fahren-VBZ-HAB-1PL</ta>
            <ta e="T211" id="Seg_6063" s="T210">wer.[NOM]</ta>
            <ta e="T212" id="Seg_6064" s="T211">dolganisch</ta>
            <ta e="T213" id="Seg_6065" s="T212">Wiege-DAT/LOC</ta>
            <ta e="T222" id="Seg_6066" s="T221">gehen-CVB.SIM</ta>
            <ta e="T223" id="Seg_6067" s="T222">gehen-PRS-1PL</ta>
            <ta e="T225" id="Seg_6068" s="T224">jenes-ABL</ta>
            <ta e="T226" id="Seg_6069" s="T225">was-ACC</ta>
            <ta e="T227" id="Seg_6070" s="T226">erzählen-FUT-1SG=Q</ta>
            <ta e="T228" id="Seg_6071" s="T227">Vater-1SG.[NOM]</ta>
            <ta e="T229" id="Seg_6072" s="T228">Q</ta>
            <ta e="T230" id="Seg_6073" s="T229">Kamen.[NOM]</ta>
            <ta e="T231" id="Seg_6074" s="T230">wer-DAT/LOC</ta>
            <ta e="T232" id="Seg_6075" s="T231">Seite-DAT/LOC</ta>
            <ta e="T233" id="Seg_6076" s="T232">gehen-EP-PST2-3SG</ta>
            <ta e="T234" id="Seg_6077" s="T233">Kamen</ta>
            <ta e="T238" id="Seg_6078" s="T237">wer.[NOM]</ta>
            <ta e="T239" id="Seg_6079" s="T238">Gans.[NOM]</ta>
            <ta e="T240" id="Seg_6080" s="T239">NEG</ta>
            <ta e="T241" id="Seg_6081" s="T240">NEG.EX</ta>
            <ta e="T254" id="Seg_6082" s="T253">wer.[NOM]</ta>
            <ta e="T255" id="Seg_6083" s="T254">Maus-PL.[NOM]</ta>
            <ta e="T263" id="Seg_6084" s="T262">noch</ta>
            <ta e="T264" id="Seg_6085" s="T263">was-ACC=Q</ta>
            <ta e="T265" id="Seg_6086" s="T264">wer.[NOM]</ta>
            <ta e="T266" id="Seg_6087" s="T265">1SG.[NOM]</ta>
            <ta e="T267" id="Seg_6088" s="T266">selbst-1SG.[NOM]</ta>
            <ta e="T268" id="Seg_6089" s="T267">lernen-PST2-EP-1SG</ta>
            <ta e="T269" id="Seg_6090" s="T268">sieben</ta>
            <ta e="T270" id="Seg_6091" s="T269">Klasse-DAT/LOC</ta>
            <ta e="T271" id="Seg_6092" s="T270">bis.zu</ta>
            <ta e="T272" id="Seg_6093" s="T271">acht-ORD</ta>
            <ta e="T273" id="Seg_6094" s="T272">Klasse-ABL</ta>
            <ta e="T274" id="Seg_6095" s="T273">aufhören-PST2-EP-1SG</ta>
            <ta e="T276" id="Seg_6096" s="T275">wer-ACC</ta>
            <ta e="T277" id="Seg_6097" s="T276">Internat-DAT/LOC</ta>
            <ta e="T278" id="Seg_6098" s="T277">jenes-ABL</ta>
            <ta e="T279" id="Seg_6099" s="T278">Internat-1PL.[NOM]</ta>
            <ta e="T280" id="Seg_6100" s="T279">sein-HAB-1PL</ta>
            <ta e="T282" id="Seg_6101" s="T281">zwei</ta>
            <ta e="T283" id="Seg_6102" s="T282">Jahr-ACC</ta>
            <ta e="T284" id="Seg_6103" s="T283">Norilsk-DAT/LOC</ta>
            <ta e="T285" id="Seg_6104" s="T284">lernen-PST2-1PL</ta>
            <ta e="T286" id="Seg_6105" s="T285">Internat-DAT/LOC</ta>
            <ta e="T288" id="Seg_6106" s="T287">wer.[NOM]</ta>
            <ta e="T289" id="Seg_6107" s="T288">leben-PST2-1PL</ta>
            <ta e="T291" id="Seg_6108" s="T290">Lager-DAT/LOC</ta>
            <ta e="T292" id="Seg_6109" s="T291">gehen-EP-PST2-EP-1SG</ta>
            <ta e="T293" id="Seg_6110" s="T292">wer-DAT/LOC</ta>
            <ta e="T294" id="Seg_6111" s="T293">Tajozhnij-DAT/LOC</ta>
            <ta e="T295" id="Seg_6112" s="T294">gehen-EP-PST2-EP-1SG</ta>
            <ta e="T296" id="Seg_6113" s="T295">Kurejka-DAT/LOC</ta>
            <ta e="T298" id="Seg_6114" s="T297">dann</ta>
            <ta e="T299" id="Seg_6115" s="T298">wachsen-CVB.SEQ</ta>
            <ta e="T300" id="Seg_6116" s="T299">nachdem</ta>
            <ta e="T301" id="Seg_6117" s="T300">wer-DAT/LOC</ta>
            <ta e="T303" id="Seg_6118" s="T302">äh</ta>
            <ta e="T304" id="Seg_6119" s="T303">jenes</ta>
            <ta e="T305" id="Seg_6120" s="T304">lernen-CVB.SIM</ta>
            <ta e="T306" id="Seg_6121" s="T305">gehen-PST2-EP-1SG</ta>
            <ta e="T312" id="Seg_6122" s="T311">wer-DAT/LOC</ta>
            <ta e="T314" id="Seg_6123" s="T313">verkaufen-PTCP.PRS</ta>
            <ta e="T315" id="Seg_6124" s="T314">Mensch.[NOM]</ta>
            <ta e="T316" id="Seg_6125" s="T315">nun</ta>
            <ta e="T317" id="Seg_6126" s="T316">wer-DAT/LOC</ta>
            <ta e="T318" id="Seg_6127" s="T317">Verkäufer-DAT/LOC</ta>
            <ta e="T319" id="Seg_6128" s="T318">gehen-PST2-EP-1SG</ta>
            <ta e="T320" id="Seg_6129" s="T319">lernen-CVB.SIM</ta>
            <ta e="T321" id="Seg_6130" s="T320">Dudinka-DAT/LOC</ta>
            <ta e="T323" id="Seg_6131" s="T322">sechs</ta>
            <ta e="T324" id="Seg_6132" s="T323">Monat-ACC</ta>
            <ta e="T325" id="Seg_6133" s="T324">lernen-PST2-EP-1SG</ta>
            <ta e="T326" id="Seg_6134" s="T325">jenes-ABL</ta>
            <ta e="T327" id="Seg_6135" s="T326">kommen-PST2-EP-1SG</ta>
            <ta e="T328" id="Seg_6136" s="T327">und</ta>
            <ta e="T329" id="Seg_6137" s="T328">Chatanga-DAT/LOC</ta>
            <ta e="T331" id="Seg_6138" s="T330">wer-VBZ-PST2-EP-1SG</ta>
            <ta e="T332" id="Seg_6139" s="T331">Praktikum-ACC</ta>
            <ta e="T336" id="Seg_6140" s="T335">wer-DAT/LOC</ta>
            <ta e="T337" id="Seg_6141" s="T336">Buffet-DAT/LOC</ta>
            <ta e="T338" id="Seg_6142" s="T337">arbeiten-HAB-1SG</ta>
            <ta e="T340" id="Seg_6143" s="T339">Kantine-DAT/LOC</ta>
            <ta e="T341" id="Seg_6144" s="T340">Buffet.[NOM]</ta>
            <ta e="T342" id="Seg_6145" s="T341">sein-PST1-3SG</ta>
            <ta e="T343" id="Seg_6146" s="T342">Chatanga-DAT/LOC</ta>
            <ta e="T344" id="Seg_6147" s="T343">dort</ta>
            <ta e="T346" id="Seg_6148" s="T345">Buffet-DAT/LOC</ta>
            <ta e="T347" id="Seg_6149" s="T346">arbeiten-HAB-1SG</ta>
            <ta e="T349" id="Seg_6150" s="T348">jenes-ABL</ta>
            <ta e="T352" id="Seg_6151" s="T351">Chatanga-DAT/LOC</ta>
            <ta e="T353" id="Seg_6152" s="T352">eins</ta>
            <ta e="T354" id="Seg_6153" s="T353">Mensch-ACC</ta>
            <ta e="T355" id="Seg_6154" s="T354">hier</ta>
            <ta e="T356" id="Seg_6155" s="T355">Cheta-ABL</ta>
            <ta e="T357" id="Seg_6156" s="T356">eins</ta>
            <ta e="T358" id="Seg_6157" s="T357">Junge-ACC</ta>
            <ta e="T359" id="Seg_6158" s="T358">mit</ta>
            <ta e="T360" id="Seg_6159" s="T359">Freund-VBZ-RECP/COLL-PST2-EP-1SG</ta>
            <ta e="T370" id="Seg_6160" s="T369">jenes-ABL</ta>
            <ta e="T371" id="Seg_6161" s="T370">nehmen-RECP/COLL-CVB.SEQ</ta>
            <ta e="T372" id="Seg_6162" s="T371">gehen-CVB.SEQ-1PL</ta>
            <ta e="T373" id="Seg_6163" s="T372">dann</ta>
            <ta e="T374" id="Seg_6164" s="T373">wer-VBZ-PST2-1PL</ta>
            <ta e="T375" id="Seg_6165" s="T374">Chatanga-DAT/LOC</ta>
            <ta e="T376" id="Seg_6166" s="T375">leben-CVB.SIM</ta>
            <ta e="T377" id="Seg_6167" s="T376">fallen-PST2-EP-1SG</ta>
            <ta e="T379" id="Seg_6168" s="T378">1SG.[NOM]</ta>
            <ta e="T380" id="Seg_6169" s="T379">Kindergarten-DAT/LOC</ta>
            <ta e="T381" id="Seg_6170" s="T380">arbeiten-PTCP.PRS</ta>
            <ta e="T382" id="Seg_6171" s="T381">sein-PST1-1SG</ta>
            <ta e="T383" id="Seg_6172" s="T382">Kindermädchen-DAT/LOC</ta>
            <ta e="T384" id="Seg_6173" s="T383">und</ta>
            <ta e="T385" id="Seg_6174" s="T384">3SG-2SG.[NOM]</ta>
            <ta e="T386" id="Seg_6175" s="T385">MOD</ta>
            <ta e="T387" id="Seg_6176" s="T386">Post-DAT/LOC</ta>
            <ta e="T389" id="Seg_6177" s="T388">arbeiten-PTCP.PRS</ta>
            <ta e="T390" id="Seg_6178" s="T389">sein-PST1-3SG</ta>
            <ta e="T392" id="Seg_6179" s="T391">jenes-ABL</ta>
            <ta e="T397" id="Seg_6180" s="T396">wer.[NOM]</ta>
            <ta e="T398" id="Seg_6181" s="T397">Tochter-1PL.[NOM]</ta>
            <ta e="T399" id="Seg_6182" s="T398">Tochter-EP-1SG.[NOM]</ta>
            <ta e="T400" id="Seg_6183" s="T399">Rita</ta>
            <ta e="T401" id="Seg_6184" s="T400">Name-PROPR</ta>
            <ta e="T402" id="Seg_6185" s="T401">Tochter-EP-1SG.[NOM]</ta>
            <ta e="T403" id="Seg_6186" s="T402">Junge-EP-1SG.[NOM]</ta>
            <ta e="T405" id="Seg_6187" s="T404">jenes-ABL</ta>
            <ta e="T411" id="Seg_6188" s="T410">Ort-3SG-DAT/LOC</ta>
            <ta e="T412" id="Seg_6189" s="T411">kommen-PTCP.FUT-3SG-ACC</ta>
            <ta e="T413" id="Seg_6190" s="T412">Vaterland-3SG-DAT/LOC</ta>
            <ta e="T414" id="Seg_6191" s="T413">kommen-PTCP.FUT-3SG-ACC</ta>
            <ta e="T422" id="Seg_6192" s="T421">schicken-PST2-3SG</ta>
            <ta e="T424" id="Seg_6193" s="T423">Kind-1SG-ACC</ta>
            <ta e="T425" id="Seg_6194" s="T424">mit</ta>
            <ta e="T426" id="Seg_6195" s="T425">nun</ta>
            <ta e="T427" id="Seg_6196" s="T426">Tochter-1SG-ACC</ta>
            <ta e="T428" id="Seg_6197" s="T427">mit</ta>
            <ta e="T433" id="Seg_6198" s="T432">3SG.[NOM]</ta>
            <ta e="T434" id="Seg_6199" s="T433">Hinterteil-DAT/LOC</ta>
            <ta e="T435" id="Seg_6200" s="T434">1SG.[NOM]</ta>
            <ta e="T436" id="Seg_6201" s="T435">Hinterteil-EP-1SG-ABL</ta>
            <ta e="T437" id="Seg_6202" s="T436">3SG.[NOM]</ta>
            <ta e="T438" id="Seg_6203" s="T437">kommen-PST2-3SG</ta>
            <ta e="T445" id="Seg_6204" s="T444">wie.viel</ta>
            <ta e="T446" id="Seg_6205" s="T445">Jahr.[NOM]</ta>
            <ta e="T447" id="Seg_6206" s="T446">MOD</ta>
            <ta e="T448" id="Seg_6207" s="T447">nun</ta>
            <ta e="T452" id="Seg_6208" s="T451">Jahr-ABL</ta>
            <ta e="T465" id="Seg_6209" s="T464">Jahr-DAT/LOC</ta>
            <ta e="T466" id="Seg_6210" s="T465">sterben-PST2-3SG</ta>
            <ta e="T473" id="Seg_6211" s="T472">1SG.[NOM]</ta>
            <ta e="T474" id="Seg_6212" s="T473">Kind-VBZ-PST2-EP-1SG</ta>
            <ta e="T475" id="Seg_6213" s="T474">Sohn-1SG-ACC</ta>
            <ta e="T484" id="Seg_6214" s="T483">Usurijsk-DAT/LOC</ta>
            <ta e="T493" id="Seg_6215" s="T492">leben-PRS-1SG</ta>
            <ta e="T494" id="Seg_6216" s="T493">Kindergarten-DAT/LOC</ta>
            <ta e="T495" id="Seg_6217" s="T494">arbeiten-PST2-EP-1SG</ta>
            <ta e="T499" id="Seg_6218" s="T498">Kindergarten-DAT/LOC</ta>
            <ta e="T500" id="Seg_6219" s="T499">dieses.EMPH</ta>
            <ta e="T501" id="Seg_6220" s="T500">EMPH</ta>
            <ta e="T502" id="Seg_6221" s="T501">Kindergarten-DAT/LOC</ta>
            <ta e="T504" id="Seg_6222" s="T503">jenes-ABL</ta>
            <ta e="T505" id="Seg_6223" s="T504">was-ACC</ta>
            <ta e="T506" id="Seg_6224" s="T505">erzählen-FUT-1SG=Q</ta>
            <ta e="T507" id="Seg_6225" s="T506">jenes-ABL</ta>
            <ta e="T508" id="Seg_6226" s="T507">Mann-EP-1SG.[NOM]</ta>
            <ta e="T509" id="Seg_6227" s="T508">Q</ta>
            <ta e="T510" id="Seg_6228" s="T509">jenes</ta>
            <ta e="T511" id="Seg_6229" s="T510">Mann-EP-1SG.[NOM]</ta>
            <ta e="T512" id="Seg_6230" s="T511">Fischen-DAT/LOC</ta>
            <ta e="T513" id="Seg_6231" s="T512">sein-HAB.[3SG]</ta>
            <ta e="T514" id="Seg_6232" s="T513">dort</ta>
            <ta e="T516" id="Seg_6233" s="T515">Jagdstelle-DAT/LOC</ta>
            <ta e="T517" id="Seg_6234" s="T516">leben-HAB-1PL</ta>
            <ta e="T518" id="Seg_6235" s="T517">nah.[NOM]</ta>
            <ta e="T524" id="Seg_6236" s="T523">Wochenende-PL-1SG-DAT/LOC</ta>
            <ta e="T525" id="Seg_6237" s="T524">3SG-DAT/LOC</ta>
            <ta e="T526" id="Seg_6238" s="T525">gehen-HAB-1SG</ta>
            <ta e="T527" id="Seg_6239" s="T526">Motorboot-EP-INSTR</ta>
            <ta e="T528" id="Seg_6240" s="T527">fahren-VBZ-HAB-1PL</ta>
            <ta e="T533" id="Seg_6241" s="T532">wer.[NOM]</ta>
            <ta e="T534" id="Seg_6242" s="T533">helfen-HAB-1SG</ta>
            <ta e="T537" id="Seg_6243" s="T536">wer-ACC</ta>
            <ta e="T540" id="Seg_6244" s="T539">zerteilen-VBZ-CVB.SIM</ta>
            <ta e="T541" id="Seg_6245" s="T540">und.so.weiter-CVB.SIM</ta>
            <ta e="T542" id="Seg_6246" s="T541">und.so.weiter-CVB.SIM</ta>
            <ta e="T544" id="Seg_6247" s="T543">geben-HAB-1PL</ta>
            <ta e="T545" id="Seg_6248" s="T544">EMPH</ta>
            <ta e="T546" id="Seg_6249" s="T545">dies</ta>
            <ta e="T547" id="Seg_6250" s="T546">wer-DAT/LOC</ta>
            <ta e="T548" id="Seg_6251" s="T547">Fischfabrik-DAT/LOC</ta>
            <ta e="T549" id="Seg_6252" s="T548">geben-HAB-1PL</ta>
            <ta e="T550" id="Seg_6253" s="T549">abgeben-VBZ-HAB-1PL</ta>
            <ta e="T559" id="Seg_6254" s="T558">gehen-HAB-3PL</ta>
            <ta e="T569" id="Seg_6255" s="T568">leben-HAB-1PL</ta>
            <ta e="T570" id="Seg_6256" s="T569">Nacht-PL-ACC</ta>
            <ta e="T575" id="Seg_6257" s="T574">essen-PST1-3SG</ta>
            <ta e="T576" id="Seg_6258" s="T575">essen-PST1-3SG</ta>
            <ta e="T577" id="Seg_6259" s="T576">jenes</ta>
            <ta e="T578" id="Seg_6260" s="T577">Kind-PL-1PL.[NOM]</ta>
            <ta e="T579" id="Seg_6261" s="T578">nun</ta>
            <ta e="T580" id="Seg_6262" s="T579">Junge-PL-1PL.[NOM]</ta>
            <ta e="T583" id="Seg_6263" s="T582">Junge-EP-1SG.[NOM]</ta>
            <ta e="T584" id="Seg_6264" s="T583">jenes-ABL</ta>
            <ta e="T585" id="Seg_6265" s="T584">dort</ta>
            <ta e="T586" id="Seg_6266" s="T585">noch</ta>
            <ta e="T587" id="Seg_6267" s="T586">Freund-PL-3SG.[NOM]</ta>
            <ta e="T588" id="Seg_6268" s="T587">Mann-EP-1SG.[NOM]</ta>
            <ta e="T589" id="Seg_6269" s="T588">Schwester-3SG-GEN</ta>
            <ta e="T590" id="Seg_6270" s="T589">Sohn-3SG.[NOM]</ta>
            <ta e="T591" id="Seg_6271" s="T590">jenes</ta>
            <ta e="T593" id="Seg_6272" s="T592">Feuer-ACC</ta>
            <ta e="T594" id="Seg_6273" s="T593">machen-HAB-3PL</ta>
            <ta e="T595" id="Seg_6274" s="T594">Rauch.[NOM]</ta>
            <ta e="T596" id="Seg_6275" s="T595">nun</ta>
            <ta e="T597" id="Seg_6276" s="T596">dieses.[NOM]</ta>
            <ta e="T600" id="Seg_6277" s="T599">wer-VBZ-NEG-PTCP.FUT-3PL-ACC</ta>
            <ta e="T601" id="Seg_6278" s="T600">sich.nähern-NEG-PTCP.FUT-3PL-ACC</ta>
            <ta e="T602" id="Seg_6279" s="T601">1PL-DAT/LOC</ta>
            <ta e="T607" id="Seg_6280" s="T606">zerteilen-VBZ-PTCP.PRS-1PL-DAT/LOC</ta>
            <ta e="T618" id="Seg_6281" s="T617">was-ACC</ta>
            <ta e="T619" id="Seg_6282" s="T618">erzählen-FUT-1SG=Q</ta>
            <ta e="T621" id="Seg_6283" s="T620">wer.[NOM]</ta>
            <ta e="T622" id="Seg_6284" s="T621">jenes</ta>
            <ta e="T623" id="Seg_6285" s="T622">Kindergarten-DAT/LOC</ta>
            <ta e="T624" id="Seg_6286" s="T623">jenes-ABL</ta>
            <ta e="T625" id="Seg_6287" s="T624">dieses.[NOM]</ta>
            <ta e="T626" id="Seg_6288" s="T625">Rente-DAT/LOC</ta>
            <ta e="T627" id="Seg_6289" s="T626">gehen-PST1-1SG</ta>
            <ta e="T628" id="Seg_6290" s="T627">EMPH</ta>
            <ta e="T631" id="Seg_6291" s="T630">dreißig</ta>
            <ta e="T632" id="Seg_6292" s="T631">acht</ta>
            <ta e="T633" id="Seg_6293" s="T632">Jahr-ACC</ta>
            <ta e="T634" id="Seg_6294" s="T633">dreißig</ta>
            <ta e="T635" id="Seg_6295" s="T634">sechs</ta>
            <ta e="T636" id="Seg_6296" s="T635">Jahr-ACC</ta>
            <ta e="T637" id="Seg_6297" s="T636">arbeiten-PST2-EP-1SG</ta>
            <ta e="T638" id="Seg_6298" s="T637">Kindergarten-DAT/LOC</ta>
            <ta e="T640" id="Seg_6299" s="T639">zuerst-ACC</ta>
            <ta e="T641" id="Seg_6300" s="T640">Schule.[NOM]</ta>
            <ta e="T642" id="Seg_6301" s="T641">Schule.[NOM]</ta>
            <ta e="T643" id="Seg_6302" s="T642">zu</ta>
            <ta e="T644" id="Seg_6303" s="T643">arbeiten-PST2-EP-1SG</ta>
            <ta e="T645" id="Seg_6304" s="T644">Koch-DAT/LOC</ta>
            <ta e="T647" id="Seg_6305" s="T646">arbeiten-PST2-EP-1SG</ta>
            <ta e="T648" id="Seg_6306" s="T647">Koch-DAT/LOC</ta>
            <ta e="T650" id="Seg_6307" s="T649">Schule-DAT/LOC</ta>
            <ta e="T651" id="Seg_6308" s="T650">dann</ta>
            <ta e="T652" id="Seg_6309" s="T651">Kindergarten-DAT/LOC</ta>
            <ta e="T653" id="Seg_6310" s="T652">nehmen-PST2-3PL</ta>
            <ta e="T655" id="Seg_6311" s="T654">äh</ta>
            <ta e="T656" id="Seg_6312" s="T655">wer-VBZ-PST2-3PL</ta>
            <ta e="T657" id="Seg_6313" s="T656">bringen-PST2-3PL</ta>
            <ta e="T658" id="Seg_6314" s="T657">wer.[NOM]</ta>
            <ta e="T659" id="Seg_6315" s="T658">dann</ta>
            <ta e="T660" id="Seg_6316" s="T659">Schule-ABL</ta>
            <ta e="T661" id="Seg_6317" s="T660">Kindergarten-DAT/LOC</ta>
            <ta e="T662" id="Seg_6318" s="T661">arbeiten-PST2-EP-1SG</ta>
            <ta e="T666" id="Seg_6319" s="T665">Rente-DAT/LOC</ta>
            <ta e="T667" id="Seg_6320" s="T666">leben-PRS-1SG</ta>
            <ta e="T670" id="Seg_6321" s="T669">Enkel-PL-1SG-ACC</ta>
            <ta e="T672" id="Seg_6322" s="T671">Urenkel-1SG-ACC</ta>
            <ta e="T676" id="Seg_6323" s="T675">arbeiten-PRS-1SG</ta>
            <ta e="T677" id="Seg_6324" s="T676">arbeiten-CVB.SIM</ta>
            <ta e="T678" id="Seg_6325" s="T677">gehen-PRS-1SG</ta>
            <ta e="T686" id="Seg_6326" s="T685">und</ta>
            <ta e="T687" id="Seg_6327" s="T686">1SG.[NOM]</ta>
            <ta e="T688" id="Seg_6328" s="T687">jenes</ta>
            <ta e="T689" id="Seg_6329" s="T688">Kind-ACC</ta>
            <ta e="T690" id="Seg_6330" s="T689">mit</ta>
            <ta e="T691" id="Seg_6331" s="T690">sitzen-PRS-1SG</ta>
            <ta e="T695" id="Seg_6332" s="T694">zwanzig-COMP</ta>
            <ta e="T696" id="Seg_6333" s="T695">Jahr-VBZ-REFL-FUT-3SG</ta>
            <ta e="T697" id="Seg_6334" s="T696">März-DAT/LOC</ta>
            <ta e="T705" id="Seg_6335" s="T704">wer-VBZ-PST2.[3SG]</ta>
            <ta e="T706" id="Seg_6336" s="T705">ein.kleines.Bisschen</ta>
            <ta e="T707" id="Seg_6337" s="T706">krank.sein-EP-PST2.[3SG]</ta>
            <ta e="T708" id="Seg_6338" s="T707">Stomatitis-VBZ-PST2.[3SG]</ta>
            <ta e="T710" id="Seg_6339" s="T709">Mama-3SG.[NOM]</ta>
            <ta e="T711" id="Seg_6340" s="T710">sitzen-PRS.[3SG]</ta>
            <ta e="T715" id="Seg_6341" s="T714">so</ta>
            <ta e="T716" id="Seg_6342" s="T715">wer.[NOM]</ta>
            <ta e="T717" id="Seg_6343" s="T716">dieses</ta>
            <ta e="T718" id="Seg_6344" s="T717">groß</ta>
            <ta e="T719" id="Seg_6345" s="T718">Enkel-EP-1SG.[NOM]</ta>
            <ta e="T720" id="Seg_6346" s="T719">Sohn-3SG-GEN</ta>
            <ta e="T721" id="Seg_6347" s="T720">eigen-3SG</ta>
            <ta e="T722" id="Seg_6348" s="T721">groß</ta>
            <ta e="T726" id="Seg_6349" s="T725">Enkel-EP-1SG.[NOM]</ta>
            <ta e="T727" id="Seg_6350" s="T726">groß</ta>
            <ta e="T728" id="Seg_6351" s="T727">jetzt</ta>
            <ta e="T729" id="Seg_6352" s="T728">dienen-VBZ-PRS.[3SG]</ta>
            <ta e="T730" id="Seg_6353" s="T729">wer-DAT/LOC</ta>
            <ta e="T731" id="Seg_6354" s="T730">Armee-DAT/LOC</ta>
            <ta e="T835" id="Seg_6355" s="T732">anderer.von.zwei-PL.[NOM]</ta>
            <ta e="T733" id="Seg_6356" s="T835">lernen-PRS-3PL</ta>
            <ta e="T734" id="Seg_6357" s="T733">und</ta>
            <ta e="T735" id="Seg_6358" s="T734">Tochter-3SG.[NOM]</ta>
            <ta e="T736" id="Seg_6359" s="T735">Tochter-EP-1SG.[NOM]</ta>
            <ta e="T737" id="Seg_6360" s="T736">Enkelin-1SG.[NOM]</ta>
            <ta e="T738" id="Seg_6361" s="T737">groß</ta>
            <ta e="T739" id="Seg_6362" s="T738">dies</ta>
            <ta e="T740" id="Seg_6363" s="T739">Junge-EP-1SG.[NOM]</ta>
            <ta e="T742" id="Seg_6364" s="T741">Tochter-3SG.[NOM]</ta>
            <ta e="T743" id="Seg_6365" s="T742">Enkelin-1SG.[NOM]</ta>
            <ta e="T744" id="Seg_6366" s="T743">Katja.[NOM]</ta>
            <ta e="T745" id="Seg_6367" s="T744">Name-3SG.[NOM]</ta>
            <ta e="T746" id="Seg_6368" s="T745">dieses.[NOM]</ta>
            <ta e="T748" id="Seg_6369" s="T747">wer-DAT/LOC</ta>
            <ta e="T749" id="Seg_6370" s="T748">lernen-PRS.[3SG]</ta>
            <ta e="T750" id="Seg_6371" s="T749">auch</ta>
            <ta e="T751" id="Seg_6372" s="T750">Mama-3SG-ACC</ta>
            <ta e="T752" id="Seg_6373" s="T751">ähnlich</ta>
            <ta e="T754" id="Seg_6374" s="T753">dies</ta>
            <ta e="T755" id="Seg_6375" s="T754">Norilsker</ta>
            <ta e="T756" id="Seg_6376" s="T755">wer-DAT/LOC</ta>
            <ta e="T757" id="Seg_6377" s="T756">Fach(hoch)schule-DAT/LOC</ta>
            <ta e="T758" id="Seg_6378" s="T757">lernen-PRS.[3SG]</ta>
            <ta e="T759" id="Seg_6379" s="T758">Mama-3SG-ACC</ta>
            <ta e="T760" id="Seg_6380" s="T759">ähnlich</ta>
            <ta e="T767" id="Seg_6381" s="T766">dieses.[NOM]</ta>
            <ta e="T768" id="Seg_6382" s="T767">wer-DAT/LOC</ta>
            <ta e="T772" id="Seg_6383" s="T771">lassen-PST2-3SG</ta>
            <ta e="T773" id="Seg_6384" s="T772">1PL-DAT/LOC</ta>
            <ta e="T777" id="Seg_6385" s="T776">zurück</ta>
            <ta e="T778" id="Seg_6386" s="T777">aufhören-CVB.SIM</ta>
            <ta e="T779" id="Seg_6387" s="T778">damit</ta>
            <ta e="T780" id="Seg_6388" s="T779">Jahr-3SG-ACC</ta>
            <ta e="T781" id="Seg_6389" s="T780">verlieren-EP-NEG-CVB.PURP</ta>
            <ta e="T782" id="Seg_6390" s="T781">nun</ta>
            <ta e="T789" id="Seg_6391" s="T788">noch</ta>
            <ta e="T791" id="Seg_6392" s="T790">zwei</ta>
            <ta e="T792" id="Seg_6393" s="T791">Jahr-ACC</ta>
            <ta e="T793" id="Seg_6394" s="T792">man.muss</ta>
            <ta e="T794" id="Seg_6395" s="T793">lernen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T801" id="Seg_6396" s="T800">drei</ta>
            <ta e="T802" id="Seg_6397" s="T801">Jahr.[NOM]</ta>
            <ta e="T806" id="Seg_6398" s="T805">Arbeit.[NOM]</ta>
            <ta e="T807" id="Seg_6399" s="T806">lernen-PRS-3PL</ta>
            <ta e="T817" id="Seg_6400" s="T816">einer.von.zwei</ta>
            <ta e="T818" id="Seg_6401" s="T817">Enkel-1SG-ACC</ta>
            <ta e="T820" id="Seg_6402" s="T819">Tochter-EP-1SG.[NOM]</ta>
            <ta e="T821" id="Seg_6403" s="T820">Sohn-3SG-ACC</ta>
            <ta e="T822" id="Seg_6404" s="T821">mit</ta>
            <ta e="T823" id="Seg_6405" s="T822">Enkel-1SG-ACC</ta>
            <ta e="T824" id="Seg_6406" s="T823">mit</ta>
            <ta e="T825" id="Seg_6407" s="T824">es.gibt-1SG</ta>
            <ta e="T826" id="Seg_6408" s="T825">Haus-1SG-DAT/LOC</ta>
            <ta e="T828" id="Seg_6409" s="T827">sieben-ORD</ta>
            <ta e="T829" id="Seg_6410" s="T828">Klasse-DAT/LOC</ta>
            <ta e="T830" id="Seg_6411" s="T829">lernen-PRS.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_6412" s="T1">1SG.[NOM]</ta>
            <ta e="T3" id="Seg_6413" s="T2">имя-EP-1SG.[NOM]</ta>
            <ta e="T4" id="Seg_6414" s="T3">Степанида</ta>
            <ta e="T5" id="Seg_6415" s="T4">долганский-SIM</ta>
            <ta e="T6" id="Seg_6416" s="T5">Степанида</ta>
            <ta e="T7" id="Seg_6417" s="T6">говорить-HAB-3PL</ta>
            <ta e="T9" id="Seg_6418" s="T8">родиться-PST2-EP-1SG</ta>
            <ta e="T10" id="Seg_6419" s="T9">Попигай</ta>
            <ta e="T11" id="Seg_6420" s="T10">говорить-CVB.SEQ</ta>
            <ta e="T12" id="Seg_6421" s="T11">место-DAT/LOC</ta>
            <ta e="T14" id="Seg_6422" s="T13">1SG.[NOM]</ta>
            <ta e="T15" id="Seg_6423" s="T14">родители-PL-EP-1SG.[NOM]</ta>
            <ta e="T16" id="Seg_6424" s="T15">отец-1SG.[NOM]</ta>
            <ta e="T17" id="Seg_6425" s="T16">кто.[NOM]</ta>
            <ta e="T18" id="Seg_6426" s="T17">Николай.[NOM]</ta>
            <ta e="T19" id="Seg_6427" s="T18">мама-1SG.[NOM]</ta>
            <ta e="T20" id="Seg_6428" s="T19">это</ta>
            <ta e="T21" id="Seg_6429" s="T20">Таиска.[NOM]</ta>
            <ta e="T22" id="Seg_6430" s="T21">Таиска.[NOM]</ta>
            <ta e="T24" id="Seg_6431" s="T23">тот-3SG-2SG.[NOM]</ta>
            <ta e="T25" id="Seg_6432" s="T24">мама-1SG.[NOM]</ta>
            <ta e="T26" id="Seg_6433" s="T25">рано</ta>
            <ta e="T27" id="Seg_6434" s="T26">очень</ta>
            <ta e="T28" id="Seg_6435" s="T27">умирать-PST2-3SG</ta>
            <ta e="T29" id="Seg_6436" s="T28">тот-ABL</ta>
            <ta e="T30" id="Seg_6437" s="T29">1SG.[NOM]</ta>
            <ta e="T31" id="Seg_6438" s="T30">отец-1SG-ACC</ta>
            <ta e="T32" id="Seg_6439" s="T31">с</ta>
            <ta e="T33" id="Seg_6440" s="T32">оставаться-PST2-EP-1SG</ta>
            <ta e="T35" id="Seg_6441" s="T34">отец-1SG.[NOM]</ta>
            <ta e="T36" id="Seg_6442" s="T35">MOD</ta>
            <ta e="T37" id="Seg_6443" s="T36">кто-DAT/LOC</ta>
            <ta e="T38" id="Seg_6444" s="T37">пастух-DAT/LOC</ta>
            <ta e="T39" id="Seg_6445" s="T38">бригадир-DAT/LOC</ta>
            <ta e="T40" id="Seg_6446" s="T39">работать-PTCP.PRS</ta>
            <ta e="T41" id="Seg_6447" s="T40">быть-PST1-3SG</ta>
            <ta e="T43" id="Seg_6448" s="T42">тот-ABL</ta>
            <ta e="T44" id="Seg_6449" s="T43">брат-EP-1SG.[NOM]</ta>
            <ta e="T45" id="Seg_6450" s="T44">тоже</ta>
            <ta e="T46" id="Seg_6451" s="T45">отец-1SG-ACC</ta>
            <ta e="T47" id="Seg_6452" s="T46">с</ta>
            <ta e="T48" id="Seg_6453" s="T47">ехать-HAB.[3SG]</ta>
            <ta e="T50" id="Seg_6454" s="T49">тундра-DAT/LOC</ta>
            <ta e="T51" id="Seg_6455" s="T50">1PL.[NOM]</ta>
            <ta e="T52" id="Seg_6456" s="T51">кочевать-CVB.SIM</ta>
            <ta e="T53" id="Seg_6457" s="T52">идти-HAB-1PL</ta>
            <ta e="T55" id="Seg_6458" s="T54">школа-ABL</ta>
            <ta e="T56" id="Seg_6459" s="T55">каникулы-1SG-DAT/LOC</ta>
            <ta e="T57" id="Seg_6460" s="T56">приходить-TEMP-1SG</ta>
            <ta e="T58" id="Seg_6461" s="T57">1SG.[NOM]</ta>
            <ta e="T59" id="Seg_6462" s="T58">отец-1SG-DAT/LOC</ta>
            <ta e="T60" id="Seg_6463" s="T59">быть-HAB-1SG</ta>
            <ta e="T62" id="Seg_6464" s="T61">но</ta>
            <ta e="T63" id="Seg_6465" s="T62">расти-PTCP.PST-EP-1SG.[NOM]</ta>
            <ta e="T64" id="Seg_6466" s="T63">после.того</ta>
            <ta e="T65" id="Seg_6467" s="T64">1SG.[NOM]</ta>
            <ta e="T66" id="Seg_6468" s="T65">отец-1SG-DAT/LOC</ta>
            <ta e="T67" id="Seg_6469" s="T66">быть-PST2-EP-1SG</ta>
            <ta e="T68" id="Seg_6470" s="T67">когда</ta>
            <ta e="T69" id="Seg_6471" s="T68">1SG.[NOM]</ta>
            <ta e="T70" id="Seg_6472" s="T69">маленький.[NOM]</ta>
            <ta e="T71" id="Seg_6473" s="T70">быть-TEMP-1SG</ta>
            <ta e="T72" id="Seg_6474" s="T71">человек-PL-DAT/LOC</ta>
            <ta e="T73" id="Seg_6475" s="T72">воспитывать-EP-PASS/REFL-EP-PST2-EP-1SG</ta>
            <ta e="T74" id="Seg_6476" s="T73">другой</ta>
            <ta e="T75" id="Seg_6477" s="T74">человек-PL-DAT/LOC</ta>
            <ta e="T77" id="Seg_6478" s="T76">воспитывать-EP-PASS/REFL-EP-PST2-EP-1SG</ta>
            <ta e="T78" id="Seg_6479" s="T77">тот-ABL</ta>
            <ta e="T79" id="Seg_6480" s="T78">расти-CVB.SEQ</ta>
            <ta e="T80" id="Seg_6481" s="T79">идти-CVB.SEQ-1SG</ta>
            <ta e="T81" id="Seg_6482" s="T80">EMPH</ta>
            <ta e="T82" id="Seg_6483" s="T81">отец-1SG-DAT/LOC</ta>
            <ta e="T83" id="Seg_6484" s="T82">быть-PST2-EP-1SG</ta>
            <ta e="T84" id="Seg_6485" s="T83">идти-HAB-1SG</ta>
            <ta e="T85" id="Seg_6486" s="T84">отец-1SG-DAT/LOC</ta>
            <ta e="T86" id="Seg_6487" s="T85">каникулы-DAT/LOC</ta>
            <ta e="T87" id="Seg_6488" s="T86">приходить-TEMP-1SG</ta>
            <ta e="T89" id="Seg_6489" s="T88">вот</ta>
            <ta e="T90" id="Seg_6490" s="T89">тот</ta>
            <ta e="T91" id="Seg_6491" s="T90">кочевать-CVB.SIM</ta>
            <ta e="T92" id="Seg_6492" s="T91">идти-HAB-1PL</ta>
            <ta e="T93" id="Seg_6493" s="T92">кто-ABL</ta>
            <ta e="T94" id="Seg_6494" s="T93">место-ABL</ta>
            <ta e="T95" id="Seg_6495" s="T94">место-DAT/LOC</ta>
            <ta e="T96" id="Seg_6496" s="T95">пока</ta>
            <ta e="T101" id="Seg_6497" s="T100">кочевать-CVB.SIM</ta>
            <ta e="T102" id="Seg_6498" s="T101">идти-HAB-1PL</ta>
            <ta e="T104" id="Seg_6499" s="T103">чум-DAT/LOC</ta>
            <ta e="T105" id="Seg_6500" s="T104">дом-PROPR.[NOM]</ta>
            <ta e="T106" id="Seg_6501" s="T105">быть-HAB-1PL</ta>
            <ta e="T107" id="Seg_6502" s="T106">там</ta>
            <ta e="T109" id="Seg_6503" s="T108">ээ</ta>
            <ta e="T110" id="Seg_6504" s="T109">снег-ACC</ta>
            <ta e="T111" id="Seg_6505" s="T110">кто-VBZ-PRS-1PL</ta>
            <ta e="T112" id="Seg_6506" s="T111">убирать-VBZ-PRS-1PL</ta>
            <ta e="T120" id="Seg_6507" s="T119">чум.[NOM]</ta>
            <ta e="T121" id="Seg_6508" s="T120">чум.[NOM]</ta>
            <ta e="T122" id="Seg_6509" s="T121">делать-PRS-3PL</ta>
            <ta e="T133" id="Seg_6510" s="T132">EMPH</ta>
            <ta e="T138" id="Seg_6511" s="T137">шест.[NOM]</ta>
            <ta e="T139" id="Seg_6512" s="T138">чум-DAT/LOC</ta>
            <ta e="T140" id="Seg_6513" s="T139">ага</ta>
            <ta e="T142" id="Seg_6514" s="T141">чум-PL-ACC</ta>
            <ta e="T144" id="Seg_6515" s="T143">кто-ABL</ta>
            <ta e="T149" id="Seg_6516" s="T148">тот</ta>
            <ta e="T150" id="Seg_6517" s="T149">дом-2SG.[NOM]</ta>
            <ta e="T151" id="Seg_6518" s="T150">иногда</ta>
            <ta e="T152" id="Seg_6519" s="T151">широкий</ta>
            <ta e="T153" id="Seg_6520" s="T152">очень</ta>
            <ta e="T154" id="Seg_6521" s="T153">делать-PRS-3PL</ta>
            <ta e="T155" id="Seg_6522" s="T154">много</ta>
            <ta e="T156" id="Seg_6523" s="T155">человек.[NOM]</ta>
            <ta e="T157" id="Seg_6524" s="T156">тот</ta>
            <ta e="T158" id="Seg_6525" s="T157">делать-CVB.SEQ</ta>
            <ta e="T159" id="Seg_6526" s="T158">много-1PL</ta>
            <ta e="T163" id="Seg_6527" s="T162">много-1PL</ta>
            <ta e="T164" id="Seg_6528" s="T163">ведь</ta>
            <ta e="T167" id="Seg_6529" s="T166">кто.[NOM]</ta>
            <ta e="T168" id="Seg_6530" s="T167">широкий</ta>
            <ta e="T169" id="Seg_6531" s="T168">дом.[NOM]</ta>
            <ta e="T170" id="Seg_6532" s="T169">широкий</ta>
            <ta e="T171" id="Seg_6533" s="T170">чум.[NOM]</ta>
            <ta e="T173" id="Seg_6534" s="T172">потом</ta>
            <ta e="T174" id="Seg_6535" s="T173">печка-1PL.[NOM]</ta>
            <ta e="T175" id="Seg_6536" s="T174">очаг.[NOM]</ta>
            <ta e="T176" id="Seg_6537" s="T175">железный</ta>
            <ta e="T177" id="Seg_6538" s="T176">очаг.[NOM]</ta>
            <ta e="T188" id="Seg_6539" s="T187">кто-3SG-ACC</ta>
            <ta e="T189" id="Seg_6540" s="T188">нижняя.часть-3SG-ACC</ta>
            <ta e="T191" id="Seg_6541" s="T190">кто-VBZ-PRS-3PL</ta>
            <ta e="T193" id="Seg_6542" s="T192">подходить-NMNZ</ta>
            <ta e="T194" id="Seg_6543" s="T193">делать-PRS-3PL</ta>
            <ta e="T195" id="Seg_6544" s="T194">давить-CAUS-PRS-3PL</ta>
            <ta e="T199" id="Seg_6545" s="T198">кто-VBZ-NEG-PTCP.FUT-3SG-ACC</ta>
            <ta e="T200" id="Seg_6546" s="T199">дуть-EP-NEG-PTCP.FUT-3SG-ACC</ta>
            <ta e="T202" id="Seg_6547" s="T201">даже</ta>
            <ta e="T203" id="Seg_6548" s="T202">маленький</ta>
            <ta e="T204" id="Seg_6549" s="T203">ребенок-PROPR.[NOM]</ta>
            <ta e="T205" id="Seg_6550" s="T204">ездить-VBZ-HAB-1PL</ta>
            <ta e="T211" id="Seg_6551" s="T210">кто.[NOM]</ta>
            <ta e="T212" id="Seg_6552" s="T211">долганский</ta>
            <ta e="T213" id="Seg_6553" s="T212">люлька-DAT/LOC</ta>
            <ta e="T222" id="Seg_6554" s="T221">идти-CVB.SIM</ta>
            <ta e="T223" id="Seg_6555" s="T222">идти-PRS-1PL</ta>
            <ta e="T225" id="Seg_6556" s="T224">тот-ABL</ta>
            <ta e="T226" id="Seg_6557" s="T225">что-ACC</ta>
            <ta e="T227" id="Seg_6558" s="T226">рассказывать-FUT-1SG=Q</ta>
            <ta e="T228" id="Seg_6559" s="T227">отец-1SG.[NOM]</ta>
            <ta e="T229" id="Seg_6560" s="T228">Q</ta>
            <ta e="T230" id="Seg_6561" s="T229">Камень.[NOM]</ta>
            <ta e="T231" id="Seg_6562" s="T230">кто-DAT/LOC</ta>
            <ta e="T232" id="Seg_6563" s="T231">сторона-DAT/LOC</ta>
            <ta e="T233" id="Seg_6564" s="T232">идти-EP-PST2-3SG</ta>
            <ta e="T234" id="Seg_6565" s="T233">Камень</ta>
            <ta e="T238" id="Seg_6566" s="T237">кто.[NOM]</ta>
            <ta e="T239" id="Seg_6567" s="T238">гусь.[NOM]</ta>
            <ta e="T240" id="Seg_6568" s="T239">NEG</ta>
            <ta e="T241" id="Seg_6569" s="T240">NEG.EX</ta>
            <ta e="T254" id="Seg_6570" s="T253">кто.[NOM]</ta>
            <ta e="T255" id="Seg_6571" s="T254">мышь-PL.[NOM]</ta>
            <ta e="T263" id="Seg_6572" s="T262">еще</ta>
            <ta e="T264" id="Seg_6573" s="T263">что-ACC=Q</ta>
            <ta e="T265" id="Seg_6574" s="T264">кто.[NOM]</ta>
            <ta e="T266" id="Seg_6575" s="T265">1SG.[NOM]</ta>
            <ta e="T267" id="Seg_6576" s="T266">сам-1SG.[NOM]</ta>
            <ta e="T268" id="Seg_6577" s="T267">учиться-PST2-EP-1SG</ta>
            <ta e="T269" id="Seg_6578" s="T268">семь</ta>
            <ta e="T270" id="Seg_6579" s="T269">класс-DAT/LOC</ta>
            <ta e="T271" id="Seg_6580" s="T270">пока</ta>
            <ta e="T272" id="Seg_6581" s="T271">восемь-ORD</ta>
            <ta e="T273" id="Seg_6582" s="T272">класс-ABL</ta>
            <ta e="T274" id="Seg_6583" s="T273">кончать-PST2-EP-1SG</ta>
            <ta e="T276" id="Seg_6584" s="T275">кто-ACC</ta>
            <ta e="T277" id="Seg_6585" s="T276">интернат-DAT/LOC</ta>
            <ta e="T278" id="Seg_6586" s="T277">тот-ABL</ta>
            <ta e="T279" id="Seg_6587" s="T278">интернат-1PL.[NOM]</ta>
            <ta e="T280" id="Seg_6588" s="T279">быть-HAB-1PL</ta>
            <ta e="T282" id="Seg_6589" s="T281">два</ta>
            <ta e="T283" id="Seg_6590" s="T282">год-ACC</ta>
            <ta e="T284" id="Seg_6591" s="T283">Норильск-DAT/LOC</ta>
            <ta e="T285" id="Seg_6592" s="T284">учиться-PST2-1PL</ta>
            <ta e="T286" id="Seg_6593" s="T285">интернат-DAT/LOC</ta>
            <ta e="T288" id="Seg_6594" s="T287">кто.[NOM]</ta>
            <ta e="T289" id="Seg_6595" s="T288">жить-PST2-1PL</ta>
            <ta e="T291" id="Seg_6596" s="T290">лагерь-DAT/LOC</ta>
            <ta e="T292" id="Seg_6597" s="T291">идти-EP-PST2-EP-1SG</ta>
            <ta e="T293" id="Seg_6598" s="T292">кто-DAT/LOC</ta>
            <ta e="T294" id="Seg_6599" s="T293">Таёжный-DAT/LOC</ta>
            <ta e="T295" id="Seg_6600" s="T294">идти-EP-PST2-EP-1SG</ta>
            <ta e="T296" id="Seg_6601" s="T295">Курейка-DAT/LOC</ta>
            <ta e="T298" id="Seg_6602" s="T297">потом</ta>
            <ta e="T299" id="Seg_6603" s="T298">расти-CVB.SEQ</ta>
            <ta e="T300" id="Seg_6604" s="T299">после</ta>
            <ta e="T301" id="Seg_6605" s="T300">кто-DAT/LOC</ta>
            <ta e="T303" id="Seg_6606" s="T302">ээ</ta>
            <ta e="T304" id="Seg_6607" s="T303">тот</ta>
            <ta e="T305" id="Seg_6608" s="T304">учиться-CVB.SIM</ta>
            <ta e="T306" id="Seg_6609" s="T305">идти-PST2-EP-1SG</ta>
            <ta e="T312" id="Seg_6610" s="T311">кто-DAT/LOC</ta>
            <ta e="T314" id="Seg_6611" s="T313">продавать-PTCP.PRS</ta>
            <ta e="T315" id="Seg_6612" s="T314">человек.[NOM]</ta>
            <ta e="T316" id="Seg_6613" s="T315">вот</ta>
            <ta e="T317" id="Seg_6614" s="T316">кто-DAT/LOC</ta>
            <ta e="T318" id="Seg_6615" s="T317">продавец-DAT/LOC</ta>
            <ta e="T319" id="Seg_6616" s="T318">идти-PST2-EP-1SG</ta>
            <ta e="T320" id="Seg_6617" s="T319">учиться-CVB.SIM</ta>
            <ta e="T321" id="Seg_6618" s="T320">Дудинка-DAT/LOC</ta>
            <ta e="T323" id="Seg_6619" s="T322">шесть</ta>
            <ta e="T324" id="Seg_6620" s="T323">месяц-ACC</ta>
            <ta e="T325" id="Seg_6621" s="T324">учиться-PST2-EP-1SG</ta>
            <ta e="T326" id="Seg_6622" s="T325">тот-ABL</ta>
            <ta e="T327" id="Seg_6623" s="T326">приходить-PST2-EP-1SG</ta>
            <ta e="T328" id="Seg_6624" s="T327">и</ta>
            <ta e="T329" id="Seg_6625" s="T328">Хатанга-DAT/LOC</ta>
            <ta e="T331" id="Seg_6626" s="T330">кто-VBZ-PST2-EP-1SG</ta>
            <ta e="T332" id="Seg_6627" s="T331">практика-ACC</ta>
            <ta e="T336" id="Seg_6628" s="T335">кто-DAT/LOC</ta>
            <ta e="T337" id="Seg_6629" s="T336">буфет-DAT/LOC</ta>
            <ta e="T338" id="Seg_6630" s="T337">работать-HAB-1SG</ta>
            <ta e="T340" id="Seg_6631" s="T339">столовая-DAT/LOC</ta>
            <ta e="T341" id="Seg_6632" s="T340">буфет.[NOM]</ta>
            <ta e="T342" id="Seg_6633" s="T341">быть-PST1-3SG</ta>
            <ta e="T343" id="Seg_6634" s="T342">Хатанга-DAT/LOC</ta>
            <ta e="T344" id="Seg_6635" s="T343">там</ta>
            <ta e="T346" id="Seg_6636" s="T345">буфет-DAT/LOC</ta>
            <ta e="T347" id="Seg_6637" s="T346">работать-HAB-1SG</ta>
            <ta e="T349" id="Seg_6638" s="T348">тот-ABL</ta>
            <ta e="T352" id="Seg_6639" s="T351">Хатанга-DAT/LOC</ta>
            <ta e="T353" id="Seg_6640" s="T352">один</ta>
            <ta e="T354" id="Seg_6641" s="T353">человек-ACC</ta>
            <ta e="T355" id="Seg_6642" s="T354">здесь</ta>
            <ta e="T356" id="Seg_6643" s="T355">Хета-ABL</ta>
            <ta e="T357" id="Seg_6644" s="T356">один</ta>
            <ta e="T358" id="Seg_6645" s="T357">мальчик-ACC</ta>
            <ta e="T359" id="Seg_6646" s="T358">с</ta>
            <ta e="T360" id="Seg_6647" s="T359">друг-VBZ-RECP/COLL-PST2-EP-1SG</ta>
            <ta e="T370" id="Seg_6648" s="T369">тот-ABL</ta>
            <ta e="T371" id="Seg_6649" s="T370">взять-RECP/COLL-CVB.SEQ</ta>
            <ta e="T372" id="Seg_6650" s="T371">идти-CVB.SEQ-1PL</ta>
            <ta e="T373" id="Seg_6651" s="T372">то</ta>
            <ta e="T374" id="Seg_6652" s="T373">кто-VBZ-PST2-1PL</ta>
            <ta e="T375" id="Seg_6653" s="T374">Хатанга-DAT/LOC</ta>
            <ta e="T376" id="Seg_6654" s="T375">жить-CVB.SIM</ta>
            <ta e="T377" id="Seg_6655" s="T376">падать-PST2-EP-1SG</ta>
            <ta e="T379" id="Seg_6656" s="T378">1SG.[NOM]</ta>
            <ta e="T380" id="Seg_6657" s="T379">садик-DAT/LOC</ta>
            <ta e="T381" id="Seg_6658" s="T380">работать-PTCP.PRS</ta>
            <ta e="T382" id="Seg_6659" s="T381">быть-PST1-1SG</ta>
            <ta e="T383" id="Seg_6660" s="T382">няня-DAT/LOC</ta>
            <ta e="T384" id="Seg_6661" s="T383">а</ta>
            <ta e="T385" id="Seg_6662" s="T384">3SG-2SG.[NOM]</ta>
            <ta e="T386" id="Seg_6663" s="T385">MOD</ta>
            <ta e="T387" id="Seg_6664" s="T386">почта-DAT/LOC</ta>
            <ta e="T389" id="Seg_6665" s="T388">работать-PTCP.PRS</ta>
            <ta e="T390" id="Seg_6666" s="T389">быть-PST1-3SG</ta>
            <ta e="T392" id="Seg_6667" s="T391">тот-ABL</ta>
            <ta e="T397" id="Seg_6668" s="T396">кто.[NOM]</ta>
            <ta e="T398" id="Seg_6669" s="T397">дочь-1PL.[NOM]</ta>
            <ta e="T399" id="Seg_6670" s="T398">дочь-EP-1SG.[NOM]</ta>
            <ta e="T400" id="Seg_6671" s="T399">Рита</ta>
            <ta e="T401" id="Seg_6672" s="T400">имя-PROPR</ta>
            <ta e="T402" id="Seg_6673" s="T401">дочь-EP-1SG.[NOM]</ta>
            <ta e="T403" id="Seg_6674" s="T402">мальчик-EP-1SG.[NOM]</ta>
            <ta e="T405" id="Seg_6675" s="T404">тот-ABL</ta>
            <ta e="T411" id="Seg_6676" s="T410">место-3SG-DAT/LOC</ta>
            <ta e="T412" id="Seg_6677" s="T411">приходить-PTCP.FUT-3SG-ACC</ta>
            <ta e="T413" id="Seg_6678" s="T412">родина-3SG-DAT/LOC</ta>
            <ta e="T414" id="Seg_6679" s="T413">приходить-PTCP.FUT-3SG-ACC</ta>
            <ta e="T422" id="Seg_6680" s="T421">послать-PST2-3SG</ta>
            <ta e="T424" id="Seg_6681" s="T423">ребенок-1SG-ACC</ta>
            <ta e="T425" id="Seg_6682" s="T424">с</ta>
            <ta e="T426" id="Seg_6683" s="T425">вот</ta>
            <ta e="T427" id="Seg_6684" s="T426">дочь-1SG-ACC</ta>
            <ta e="T428" id="Seg_6685" s="T427">с</ta>
            <ta e="T433" id="Seg_6686" s="T432">3SG.[NOM]</ta>
            <ta e="T434" id="Seg_6687" s="T433">задняя.часть-DAT/LOC</ta>
            <ta e="T435" id="Seg_6688" s="T434">1SG.[NOM]</ta>
            <ta e="T436" id="Seg_6689" s="T435">задняя.часть-EP-1SG-ABL</ta>
            <ta e="T437" id="Seg_6690" s="T436">3SG.[NOM]</ta>
            <ta e="T438" id="Seg_6691" s="T437">приходить-PST2-3SG</ta>
            <ta e="T445" id="Seg_6692" s="T444">сколько</ta>
            <ta e="T446" id="Seg_6693" s="T445">год.[NOM]</ta>
            <ta e="T447" id="Seg_6694" s="T446">MOD</ta>
            <ta e="T448" id="Seg_6695" s="T447">вот</ta>
            <ta e="T452" id="Seg_6696" s="T451">год-ABL</ta>
            <ta e="T465" id="Seg_6697" s="T464">год-DAT/LOC</ta>
            <ta e="T466" id="Seg_6698" s="T465">умирать-PST2-3SG</ta>
            <ta e="T473" id="Seg_6699" s="T472">1SG.[NOM]</ta>
            <ta e="T474" id="Seg_6700" s="T473">ребенок-VBZ-PST2-EP-1SG</ta>
            <ta e="T475" id="Seg_6701" s="T474">сын-1SG-ACC</ta>
            <ta e="T484" id="Seg_6702" s="T483">Усурийск-DAT/LOC</ta>
            <ta e="T493" id="Seg_6703" s="T492">жить-PRS-1SG</ta>
            <ta e="T494" id="Seg_6704" s="T493">садик-DAT/LOC</ta>
            <ta e="T495" id="Seg_6705" s="T494">работать-PST2-EP-1SG</ta>
            <ta e="T499" id="Seg_6706" s="T498">садик-DAT/LOC</ta>
            <ta e="T500" id="Seg_6707" s="T499">этот.EMPH</ta>
            <ta e="T501" id="Seg_6708" s="T500">EMPH</ta>
            <ta e="T502" id="Seg_6709" s="T501">садик-DAT/LOC</ta>
            <ta e="T504" id="Seg_6710" s="T503">тот-ABL</ta>
            <ta e="T505" id="Seg_6711" s="T504">что-ACC</ta>
            <ta e="T506" id="Seg_6712" s="T505">рассказывать-FUT-1SG=Q</ta>
            <ta e="T507" id="Seg_6713" s="T506">тот-ABL</ta>
            <ta e="T508" id="Seg_6714" s="T507">мужчина-EP-1SG.[NOM]</ta>
            <ta e="T509" id="Seg_6715" s="T508">Q</ta>
            <ta e="T510" id="Seg_6716" s="T509">тот</ta>
            <ta e="T511" id="Seg_6717" s="T510">мужчина-EP-1SG.[NOM]</ta>
            <ta e="T512" id="Seg_6718" s="T511">рыбалка-DAT/LOC</ta>
            <ta e="T513" id="Seg_6719" s="T512">быть-HAB.[3SG]</ta>
            <ta e="T514" id="Seg_6720" s="T513">там</ta>
            <ta e="T516" id="Seg_6721" s="T515">точка-DAT/LOC</ta>
            <ta e="T517" id="Seg_6722" s="T516">жить-HAB-1PL</ta>
            <ta e="T518" id="Seg_6723" s="T517">близкий.[NOM]</ta>
            <ta e="T524" id="Seg_6724" s="T523">выходной-PL-1SG-DAT/LOC</ta>
            <ta e="T525" id="Seg_6725" s="T524">3SG-DAT/LOC</ta>
            <ta e="T526" id="Seg_6726" s="T525">идти-HAB-1SG</ta>
            <ta e="T527" id="Seg_6727" s="T526">моторная.лодка-EP-INSTR</ta>
            <ta e="T528" id="Seg_6728" s="T527">ездить-VBZ-HAB-1PL</ta>
            <ta e="T533" id="Seg_6729" s="T532">кто.[NOM]</ta>
            <ta e="T534" id="Seg_6730" s="T533">помогать-HAB-1SG</ta>
            <ta e="T537" id="Seg_6731" s="T536">кто-ACC</ta>
            <ta e="T540" id="Seg_6732" s="T539">разделывать-VBZ-CVB.SIM</ta>
            <ta e="T541" id="Seg_6733" s="T540">и.так.далее-CVB.SIM</ta>
            <ta e="T542" id="Seg_6734" s="T541">и.так.далее-CVB.SIM</ta>
            <ta e="T544" id="Seg_6735" s="T543">давать-HAB-1PL</ta>
            <ta e="T545" id="Seg_6736" s="T544">EMPH</ta>
            <ta e="T546" id="Seg_6737" s="T545">это</ta>
            <ta e="T547" id="Seg_6738" s="T546">кто-DAT/LOC</ta>
            <ta e="T548" id="Seg_6739" s="T547">рыбзавод-DAT/LOC</ta>
            <ta e="T549" id="Seg_6740" s="T548">давать-HAB-1PL</ta>
            <ta e="T550" id="Seg_6741" s="T549">сдавать-VBZ-HAB-1PL</ta>
            <ta e="T559" id="Seg_6742" s="T558">идти-HAB-3PL</ta>
            <ta e="T569" id="Seg_6743" s="T568">жить-HAB-1PL</ta>
            <ta e="T570" id="Seg_6744" s="T569">ночь-PL-ACC</ta>
            <ta e="T575" id="Seg_6745" s="T574">есть-PST1-3SG</ta>
            <ta e="T576" id="Seg_6746" s="T575">есть-PST1-3SG</ta>
            <ta e="T577" id="Seg_6747" s="T576">тот</ta>
            <ta e="T578" id="Seg_6748" s="T577">ребенок-PL-1PL.[NOM]</ta>
            <ta e="T579" id="Seg_6749" s="T578">вот</ta>
            <ta e="T580" id="Seg_6750" s="T579">мальчик-PL-1PL.[NOM]</ta>
            <ta e="T583" id="Seg_6751" s="T582">мальчик-EP-1SG.[NOM]</ta>
            <ta e="T584" id="Seg_6752" s="T583">тот-ABL</ta>
            <ta e="T585" id="Seg_6753" s="T584">там</ta>
            <ta e="T586" id="Seg_6754" s="T585">еще</ta>
            <ta e="T587" id="Seg_6755" s="T586">друг-PL-3SG.[NOM]</ta>
            <ta e="T588" id="Seg_6756" s="T587">мужчина-EP-1SG.[NOM]</ta>
            <ta e="T589" id="Seg_6757" s="T588">сестра-3SG-GEN</ta>
            <ta e="T590" id="Seg_6758" s="T589">сын-3SG.[NOM]</ta>
            <ta e="T591" id="Seg_6759" s="T590">тот</ta>
            <ta e="T593" id="Seg_6760" s="T592">огонь-ACC</ta>
            <ta e="T594" id="Seg_6761" s="T593">делать-HAB-3PL</ta>
            <ta e="T595" id="Seg_6762" s="T594">дым.[NOM]</ta>
            <ta e="T596" id="Seg_6763" s="T595">вот</ta>
            <ta e="T597" id="Seg_6764" s="T596">тот.[NOM]</ta>
            <ta e="T600" id="Seg_6765" s="T599">кто-VBZ-NEG-PTCP.FUT-3PL-ACC</ta>
            <ta e="T601" id="Seg_6766" s="T600">приближаться-NEG-PTCP.FUT-3PL-ACC</ta>
            <ta e="T602" id="Seg_6767" s="T601">1PL-DAT/LOC</ta>
            <ta e="T607" id="Seg_6768" s="T606">разделать-VBZ-PTCP.PRS-1PL-DAT/LOC</ta>
            <ta e="T618" id="Seg_6769" s="T617">что-ACC</ta>
            <ta e="T619" id="Seg_6770" s="T618">рассказывать-FUT-1SG=Q</ta>
            <ta e="T621" id="Seg_6771" s="T620">кто.[NOM]</ta>
            <ta e="T622" id="Seg_6772" s="T621">тот</ta>
            <ta e="T623" id="Seg_6773" s="T622">садик-DAT/LOC</ta>
            <ta e="T624" id="Seg_6774" s="T623">тот-ABL</ta>
            <ta e="T625" id="Seg_6775" s="T624">тот.[NOM]</ta>
            <ta e="T626" id="Seg_6776" s="T625">пенсия-DAT/LOC</ta>
            <ta e="T627" id="Seg_6777" s="T626">идти-PST1-1SG</ta>
            <ta e="T628" id="Seg_6778" s="T627">EMPH</ta>
            <ta e="T631" id="Seg_6779" s="T630">тридцать</ta>
            <ta e="T632" id="Seg_6780" s="T631">восемь</ta>
            <ta e="T633" id="Seg_6781" s="T632">год-ACC</ta>
            <ta e="T634" id="Seg_6782" s="T633">тридцать</ta>
            <ta e="T635" id="Seg_6783" s="T634">шесть</ta>
            <ta e="T636" id="Seg_6784" s="T635">год-ACC</ta>
            <ta e="T637" id="Seg_6785" s="T636">работать-PST2-EP-1SG</ta>
            <ta e="T638" id="Seg_6786" s="T637">садик-DAT/LOC</ta>
            <ta e="T640" id="Seg_6787" s="T639">сначала-ACC</ta>
            <ta e="T641" id="Seg_6788" s="T640">школа.[NOM]</ta>
            <ta e="T642" id="Seg_6789" s="T641">школа.[NOM]</ta>
            <ta e="T643" id="Seg_6790" s="T642">к</ta>
            <ta e="T644" id="Seg_6791" s="T643">работать-PST2-EP-1SG</ta>
            <ta e="T645" id="Seg_6792" s="T644">повар-DAT/LOC</ta>
            <ta e="T647" id="Seg_6793" s="T646">работать-PST2-EP-1SG</ta>
            <ta e="T648" id="Seg_6794" s="T647">повар-DAT/LOC</ta>
            <ta e="T650" id="Seg_6795" s="T649">школа-DAT/LOC</ta>
            <ta e="T651" id="Seg_6796" s="T650">потом</ta>
            <ta e="T652" id="Seg_6797" s="T651">садик-DAT/LOC</ta>
            <ta e="T653" id="Seg_6798" s="T652">взять-PST2-3PL</ta>
            <ta e="T655" id="Seg_6799" s="T654">ээ</ta>
            <ta e="T656" id="Seg_6800" s="T655">кто-VBZ-PST2-3PL</ta>
            <ta e="T657" id="Seg_6801" s="T656">принести-PST2-3PL</ta>
            <ta e="T658" id="Seg_6802" s="T657">кто.[NOM]</ta>
            <ta e="T659" id="Seg_6803" s="T658">потом</ta>
            <ta e="T660" id="Seg_6804" s="T659">школа-ABL</ta>
            <ta e="T661" id="Seg_6805" s="T660">садик-DAT/LOC</ta>
            <ta e="T662" id="Seg_6806" s="T661">работать-PST2-EP-1SG</ta>
            <ta e="T666" id="Seg_6807" s="T665">пенсия-DAT/LOC</ta>
            <ta e="T667" id="Seg_6808" s="T666">жить-PRS-1SG</ta>
            <ta e="T670" id="Seg_6809" s="T669">внук-PL-1SG-ACC</ta>
            <ta e="T672" id="Seg_6810" s="T671">правнучка-1SG-ACC</ta>
            <ta e="T676" id="Seg_6811" s="T675">работать-PRS-1SG</ta>
            <ta e="T677" id="Seg_6812" s="T676">работать-CVB.SIM</ta>
            <ta e="T678" id="Seg_6813" s="T677">идти-PRS-1SG</ta>
            <ta e="T686" id="Seg_6814" s="T685">а</ta>
            <ta e="T687" id="Seg_6815" s="T686">1SG.[NOM]</ta>
            <ta e="T688" id="Seg_6816" s="T687">тот</ta>
            <ta e="T689" id="Seg_6817" s="T688">ребенок-ACC</ta>
            <ta e="T690" id="Seg_6818" s="T689">с</ta>
            <ta e="T691" id="Seg_6819" s="T690">сидеть-PRS-1SG</ta>
            <ta e="T695" id="Seg_6820" s="T694">двадцать-COMP</ta>
            <ta e="T696" id="Seg_6821" s="T695">год-VBZ-REFL-FUT-3SG</ta>
            <ta e="T697" id="Seg_6822" s="T696">март-DAT/LOC</ta>
            <ta e="T705" id="Seg_6823" s="T704">кто-VBZ-PST2.[3SG]</ta>
            <ta e="T706" id="Seg_6824" s="T705">немножко</ta>
            <ta e="T707" id="Seg_6825" s="T706">быть.больным-EP-PST2.[3SG]</ta>
            <ta e="T708" id="Seg_6826" s="T707">стоматит-VBZ-PST2.[3SG]</ta>
            <ta e="T710" id="Seg_6827" s="T709">мама-3SG.[NOM]</ta>
            <ta e="T711" id="Seg_6828" s="T710">сидеть-PRS.[3SG]</ta>
            <ta e="T715" id="Seg_6829" s="T714">так</ta>
            <ta e="T716" id="Seg_6830" s="T715">кто.[NOM]</ta>
            <ta e="T717" id="Seg_6831" s="T716">этот</ta>
            <ta e="T718" id="Seg_6832" s="T717">большой</ta>
            <ta e="T719" id="Seg_6833" s="T718">внук-EP-1SG.[NOM]</ta>
            <ta e="T720" id="Seg_6834" s="T719">сын-3SG-GEN</ta>
            <ta e="T721" id="Seg_6835" s="T720">собственный-3SG</ta>
            <ta e="T722" id="Seg_6836" s="T721">большой</ta>
            <ta e="T726" id="Seg_6837" s="T725">внук-EP-1SG.[NOM]</ta>
            <ta e="T727" id="Seg_6838" s="T726">большой</ta>
            <ta e="T728" id="Seg_6839" s="T727">теперь</ta>
            <ta e="T729" id="Seg_6840" s="T728">служить-VBZ-PRS.[3SG]</ta>
            <ta e="T730" id="Seg_6841" s="T729">кто-DAT/LOC</ta>
            <ta e="T731" id="Seg_6842" s="T730">армия-DAT/LOC</ta>
            <ta e="T835" id="Seg_6843" s="T732">другой.из.двух-PL.[NOM]</ta>
            <ta e="T733" id="Seg_6844" s="T835">учиться-PRS-3PL</ta>
            <ta e="T734" id="Seg_6845" s="T733">а</ta>
            <ta e="T735" id="Seg_6846" s="T734">дочь-3SG.[NOM]</ta>
            <ta e="T736" id="Seg_6847" s="T735">дочь-EP-1SG.[NOM]</ta>
            <ta e="T737" id="Seg_6848" s="T736">внучка-1SG.[NOM]</ta>
            <ta e="T738" id="Seg_6849" s="T737">большой</ta>
            <ta e="T739" id="Seg_6850" s="T738">это</ta>
            <ta e="T740" id="Seg_6851" s="T739">мальчик-EP-1SG.[NOM]</ta>
            <ta e="T742" id="Seg_6852" s="T741">дочь-3SG.[NOM]</ta>
            <ta e="T743" id="Seg_6853" s="T742">внучка-1SG.[NOM]</ta>
            <ta e="T744" id="Seg_6854" s="T743">Катя.[NOM]</ta>
            <ta e="T745" id="Seg_6855" s="T744">имя-3SG.[NOM]</ta>
            <ta e="T746" id="Seg_6856" s="T745">тот.[NOM]</ta>
            <ta e="T748" id="Seg_6857" s="T747">кто-DAT/LOC</ta>
            <ta e="T749" id="Seg_6858" s="T748">учиться-PRS.[3SG]</ta>
            <ta e="T750" id="Seg_6859" s="T749">тоже</ta>
            <ta e="T751" id="Seg_6860" s="T750">мама-3SG-ACC</ta>
            <ta e="T752" id="Seg_6861" s="T751">подобно</ta>
            <ta e="T754" id="Seg_6862" s="T753">это</ta>
            <ta e="T755" id="Seg_6863" s="T754">норильский</ta>
            <ta e="T756" id="Seg_6864" s="T755">кто-DAT/LOC</ta>
            <ta e="T757" id="Seg_6865" s="T756">техникум-DAT/LOC</ta>
            <ta e="T758" id="Seg_6866" s="T757">учиться-PRS.[3SG]</ta>
            <ta e="T759" id="Seg_6867" s="T758">мама-3SG-ACC</ta>
            <ta e="T760" id="Seg_6868" s="T759">подобно</ta>
            <ta e="T767" id="Seg_6869" s="T766">тот.[NOM]</ta>
            <ta e="T768" id="Seg_6870" s="T767">кто-DAT/LOC</ta>
            <ta e="T772" id="Seg_6871" s="T771">оставлять-PST2-3SG</ta>
            <ta e="T773" id="Seg_6872" s="T772">1PL-DAT/LOC</ta>
            <ta e="T777" id="Seg_6873" s="T776">назад</ta>
            <ta e="T778" id="Seg_6874" s="T777">кончать-CVB.SIM</ta>
            <ta e="T779" id="Seg_6875" s="T778">чтобы</ta>
            <ta e="T780" id="Seg_6876" s="T779">год-3SG-ACC</ta>
            <ta e="T781" id="Seg_6877" s="T780">потерять-EP-NEG-CVB.PURP</ta>
            <ta e="T782" id="Seg_6878" s="T781">вот</ta>
            <ta e="T789" id="Seg_6879" s="T788">еще</ta>
            <ta e="T791" id="Seg_6880" s="T790">два</ta>
            <ta e="T792" id="Seg_6881" s="T791">год-ACC</ta>
            <ta e="T793" id="Seg_6882" s="T792">надо</ta>
            <ta e="T794" id="Seg_6883" s="T793">учиться-PTCP.FUT-DAT/LOC</ta>
            <ta e="T801" id="Seg_6884" s="T800">три</ta>
            <ta e="T802" id="Seg_6885" s="T801">год.[NOM]</ta>
            <ta e="T806" id="Seg_6886" s="T805">работа.[NOM]</ta>
            <ta e="T807" id="Seg_6887" s="T806">учиться-PRS-3PL</ta>
            <ta e="T817" id="Seg_6888" s="T816">один.из.двух</ta>
            <ta e="T818" id="Seg_6889" s="T817">внук-1SG-ACC</ta>
            <ta e="T820" id="Seg_6890" s="T819">дочь-EP-1SG.[NOM]</ta>
            <ta e="T821" id="Seg_6891" s="T820">сын-3SG-ACC</ta>
            <ta e="T822" id="Seg_6892" s="T821">с</ta>
            <ta e="T823" id="Seg_6893" s="T822">внук-1SG-ACC</ta>
            <ta e="T824" id="Seg_6894" s="T823">с</ta>
            <ta e="T825" id="Seg_6895" s="T824">есть-1SG</ta>
            <ta e="T826" id="Seg_6896" s="T825">дом-1SG-DAT/LOC</ta>
            <ta e="T828" id="Seg_6897" s="T827">семь-ORD</ta>
            <ta e="T829" id="Seg_6898" s="T828">класс-DAT/LOC</ta>
            <ta e="T830" id="Seg_6899" s="T829">учиться-PRS.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_6900" s="T1">pers.[pro:case]</ta>
            <ta e="T3" id="Seg_6901" s="T2">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T4" id="Seg_6902" s="T3">propr</ta>
            <ta e="T5" id="Seg_6903" s="T4">adj-adj&gt;adv</ta>
            <ta e="T6" id="Seg_6904" s="T5">propr</ta>
            <ta e="T7" id="Seg_6905" s="T6">v-v:mood-v:pred.pn</ta>
            <ta e="T9" id="Seg_6906" s="T8">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T10" id="Seg_6907" s="T9">propr</ta>
            <ta e="T11" id="Seg_6908" s="T10">v-v:cvb</ta>
            <ta e="T12" id="Seg_6909" s="T11">n-n:case</ta>
            <ta e="T14" id="Seg_6910" s="T13">pers.[pro:case]</ta>
            <ta e="T15" id="Seg_6911" s="T14">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T16" id="Seg_6912" s="T15">n-n:(poss).[n:case]</ta>
            <ta e="T17" id="Seg_6913" s="T16">que.[pro:case]</ta>
            <ta e="T18" id="Seg_6914" s="T17">propr.[n:case]</ta>
            <ta e="T19" id="Seg_6915" s="T18">n-n:(poss).[n:case]</ta>
            <ta e="T20" id="Seg_6916" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_6917" s="T20">propr.[n:case]</ta>
            <ta e="T22" id="Seg_6918" s="T21">propr.[n:case]</ta>
            <ta e="T24" id="Seg_6919" s="T23">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T25" id="Seg_6920" s="T24">n-n:(poss).[n:case]</ta>
            <ta e="T26" id="Seg_6921" s="T25">adv</ta>
            <ta e="T27" id="Seg_6922" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_6923" s="T27">v-v:tense-v:poss.pn</ta>
            <ta e="T29" id="Seg_6924" s="T28">dempro-pro:case</ta>
            <ta e="T30" id="Seg_6925" s="T29">pers.[pro:case]</ta>
            <ta e="T31" id="Seg_6926" s="T30">n-n:poss-n:case</ta>
            <ta e="T32" id="Seg_6927" s="T31">post</ta>
            <ta e="T33" id="Seg_6928" s="T32">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T35" id="Seg_6929" s="T34">n-n:(poss).[n:case]</ta>
            <ta e="T36" id="Seg_6930" s="T35">ptcl</ta>
            <ta e="T37" id="Seg_6931" s="T36">que-pro:case</ta>
            <ta e="T38" id="Seg_6932" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_6933" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_6934" s="T39">v-v:ptcp</ta>
            <ta e="T41" id="Seg_6935" s="T40">v-v:tense-v:poss.pn</ta>
            <ta e="T43" id="Seg_6936" s="T42">dempro-pro:case</ta>
            <ta e="T44" id="Seg_6937" s="T43">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T45" id="Seg_6938" s="T44">adv</ta>
            <ta e="T46" id="Seg_6939" s="T45">n-n:poss-n:case</ta>
            <ta e="T47" id="Seg_6940" s="T46">post</ta>
            <ta e="T48" id="Seg_6941" s="T47">v-v:mood.[v:pred.pn]</ta>
            <ta e="T50" id="Seg_6942" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_6943" s="T50">pers.[pro:case]</ta>
            <ta e="T52" id="Seg_6944" s="T51">v-v:cvb</ta>
            <ta e="T53" id="Seg_6945" s="T52">v-v:mood-v:pred.pn</ta>
            <ta e="T55" id="Seg_6946" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_6947" s="T55">n-n:poss-n:case</ta>
            <ta e="T57" id="Seg_6948" s="T56">v-v:mood-v:temp.pn</ta>
            <ta e="T58" id="Seg_6949" s="T57">pers.[pro:case]</ta>
            <ta e="T59" id="Seg_6950" s="T58">n-n:poss-n:case</ta>
            <ta e="T60" id="Seg_6951" s="T59">v-v:mood-v:pred.pn</ta>
            <ta e="T62" id="Seg_6952" s="T61">conj</ta>
            <ta e="T63" id="Seg_6953" s="T62">v-v:ptcp-v:(ins)-v:(poss).[v:(case)]</ta>
            <ta e="T64" id="Seg_6954" s="T63">post</ta>
            <ta e="T65" id="Seg_6955" s="T64">pers.[pro:case]</ta>
            <ta e="T66" id="Seg_6956" s="T65">n-n:poss-n:case</ta>
            <ta e="T67" id="Seg_6957" s="T66">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T68" id="Seg_6958" s="T67">conj</ta>
            <ta e="T69" id="Seg_6959" s="T68">pers.[pro:case]</ta>
            <ta e="T70" id="Seg_6960" s="T69">adj.[n:case]</ta>
            <ta e="T71" id="Seg_6961" s="T70">v-v:mood-v:temp.pn</ta>
            <ta e="T72" id="Seg_6962" s="T71">n-n:(num)-n:case</ta>
            <ta e="T73" id="Seg_6963" s="T72">v-v:(ins)-v&gt;v-v:(ins)-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T74" id="Seg_6964" s="T73">adj</ta>
            <ta e="T75" id="Seg_6965" s="T74">n-n:(num)-n:case</ta>
            <ta e="T77" id="Seg_6966" s="T76">v-v:(ins)-v&gt;v-v:(ins)-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T78" id="Seg_6967" s="T77">dempro-pro:case</ta>
            <ta e="T79" id="Seg_6968" s="T78">v-v:cvb</ta>
            <ta e="T80" id="Seg_6969" s="T79">v-v:cvb-v:pred.pn</ta>
            <ta e="T81" id="Seg_6970" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_6971" s="T81">n-n:poss-n:case</ta>
            <ta e="T83" id="Seg_6972" s="T82">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T84" id="Seg_6973" s="T83">v-v:mood-v:pred.pn</ta>
            <ta e="T85" id="Seg_6974" s="T84">n-n:poss-n:case</ta>
            <ta e="T86" id="Seg_6975" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_6976" s="T86">v-v:mood-v:temp.pn</ta>
            <ta e="T89" id="Seg_6977" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_6978" s="T89">dempro</ta>
            <ta e="T91" id="Seg_6979" s="T90">v-v:cvb</ta>
            <ta e="T92" id="Seg_6980" s="T91">v-v:mood-v:pred.pn</ta>
            <ta e="T93" id="Seg_6981" s="T92">que-pro:case</ta>
            <ta e="T94" id="Seg_6982" s="T93">n-n:case</ta>
            <ta e="T95" id="Seg_6983" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_6984" s="T95">post</ta>
            <ta e="T101" id="Seg_6985" s="T100">v-v:cvb</ta>
            <ta e="T102" id="Seg_6986" s="T101">v-v:mood-v:pred.pn</ta>
            <ta e="T104" id="Seg_6987" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_6988" s="T104">n-n&gt;adj.[n:case]</ta>
            <ta e="T106" id="Seg_6989" s="T105">v-v:mood-v:pred.pn</ta>
            <ta e="T107" id="Seg_6990" s="T106">ptcl</ta>
            <ta e="T109" id="Seg_6991" s="T108">interj</ta>
            <ta e="T110" id="Seg_6992" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_6993" s="T110">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T112" id="Seg_6994" s="T111">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T120" id="Seg_6995" s="T119">n.[n:case]</ta>
            <ta e="T121" id="Seg_6996" s="T120">n.[n:case]</ta>
            <ta e="T122" id="Seg_6997" s="T121">v-v:tense-v:pred.pn</ta>
            <ta e="T133" id="Seg_6998" s="T132">ptcl</ta>
            <ta e="T138" id="Seg_6999" s="T137">n.[n:case]</ta>
            <ta e="T139" id="Seg_7000" s="T138">n-n:case</ta>
            <ta e="T140" id="Seg_7001" s="T139">interj</ta>
            <ta e="T142" id="Seg_7002" s="T141">n-n:(num)-n:case</ta>
            <ta e="T144" id="Seg_7003" s="T143">que-pro:case</ta>
            <ta e="T149" id="Seg_7004" s="T148">dempro</ta>
            <ta e="T150" id="Seg_7005" s="T149">n-n:(poss).[n:case]</ta>
            <ta e="T151" id="Seg_7006" s="T150">adv</ta>
            <ta e="T152" id="Seg_7007" s="T151">adj</ta>
            <ta e="T153" id="Seg_7008" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_7009" s="T153">v-v:tense-v:pred.pn</ta>
            <ta e="T155" id="Seg_7010" s="T154">quant</ta>
            <ta e="T156" id="Seg_7011" s="T155">n.[n:case]</ta>
            <ta e="T157" id="Seg_7012" s="T156">dempro</ta>
            <ta e="T158" id="Seg_7013" s="T157">v-v:cvb</ta>
            <ta e="T159" id="Seg_7014" s="T158">quant-n:(pred.pn)</ta>
            <ta e="T163" id="Seg_7015" s="T162">quant-n:(pred.pn)</ta>
            <ta e="T164" id="Seg_7016" s="T163">ptcl</ta>
            <ta e="T167" id="Seg_7017" s="T166">que.[pro:case]</ta>
            <ta e="T168" id="Seg_7018" s="T167">adj</ta>
            <ta e="T169" id="Seg_7019" s="T168">n.[n:case]</ta>
            <ta e="T170" id="Seg_7020" s="T169">adj</ta>
            <ta e="T171" id="Seg_7021" s="T170">n.[n:case]</ta>
            <ta e="T173" id="Seg_7022" s="T172">adv</ta>
            <ta e="T174" id="Seg_7023" s="T173">n-n:(poss).[n:case]</ta>
            <ta e="T175" id="Seg_7024" s="T174">n.[n:case]</ta>
            <ta e="T176" id="Seg_7025" s="T175">adj</ta>
            <ta e="T177" id="Seg_7026" s="T176">n.[n:case]</ta>
            <ta e="T188" id="Seg_7027" s="T187">que-pro:(poss)-pro:case</ta>
            <ta e="T189" id="Seg_7028" s="T188">n-n:poss-n:case</ta>
            <ta e="T191" id="Seg_7029" s="T190">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T193" id="Seg_7030" s="T192">v-v&gt;n</ta>
            <ta e="T194" id="Seg_7031" s="T193">v-v:tense-v:pred.pn</ta>
            <ta e="T195" id="Seg_7032" s="T194">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T199" id="Seg_7033" s="T198">que-que&gt;v-v:(neg)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T200" id="Seg_7034" s="T199">v-v:(ins)-v:(neg)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T202" id="Seg_7035" s="T201">ptcl</ta>
            <ta e="T203" id="Seg_7036" s="T202">adj</ta>
            <ta e="T204" id="Seg_7037" s="T203">n-n&gt;adj.[n:case]</ta>
            <ta e="T205" id="Seg_7038" s="T204">v-v&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T211" id="Seg_7039" s="T210">que.[pro:case]</ta>
            <ta e="T212" id="Seg_7040" s="T211">adj</ta>
            <ta e="T213" id="Seg_7041" s="T212">n-n:case</ta>
            <ta e="T222" id="Seg_7042" s="T221">v-v:cvb</ta>
            <ta e="T223" id="Seg_7043" s="T222">v-v:tense-v:pred.pn</ta>
            <ta e="T225" id="Seg_7044" s="T224">dempro-pro:case</ta>
            <ta e="T226" id="Seg_7045" s="T225">que-pro:case</ta>
            <ta e="T227" id="Seg_7046" s="T226">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T228" id="Seg_7047" s="T227">n-n:(poss).[n:case]</ta>
            <ta e="T229" id="Seg_7048" s="T228">ptcl</ta>
            <ta e="T230" id="Seg_7049" s="T229">propr.[n:case]</ta>
            <ta e="T231" id="Seg_7050" s="T230">que-pro:case</ta>
            <ta e="T232" id="Seg_7051" s="T231">n-n:case</ta>
            <ta e="T233" id="Seg_7052" s="T232">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T234" id="Seg_7053" s="T233">propr</ta>
            <ta e="T238" id="Seg_7054" s="T237">que.[pro:case]</ta>
            <ta e="T239" id="Seg_7055" s="T238">n.[n:case]</ta>
            <ta e="T240" id="Seg_7056" s="T239">ptcl</ta>
            <ta e="T241" id="Seg_7057" s="T240">ptcl</ta>
            <ta e="T254" id="Seg_7058" s="T253">que.[pro:case]</ta>
            <ta e="T255" id="Seg_7059" s="T254">n-n:(num).[n:case]</ta>
            <ta e="T263" id="Seg_7060" s="T262">adv</ta>
            <ta e="T264" id="Seg_7061" s="T263">que-pro:case=ptcl</ta>
            <ta e="T265" id="Seg_7062" s="T264">que.[pro:case]</ta>
            <ta e="T266" id="Seg_7063" s="T265">pers.[pro:case]</ta>
            <ta e="T267" id="Seg_7064" s="T266">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T268" id="Seg_7065" s="T267">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T269" id="Seg_7066" s="T268">cardnum</ta>
            <ta e="T270" id="Seg_7067" s="T269">n-n:case</ta>
            <ta e="T271" id="Seg_7068" s="T270">post</ta>
            <ta e="T272" id="Seg_7069" s="T271">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T273" id="Seg_7070" s="T272">n-n:case</ta>
            <ta e="T274" id="Seg_7071" s="T273">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T276" id="Seg_7072" s="T275">que-pro:case</ta>
            <ta e="T277" id="Seg_7073" s="T276">n-n:case</ta>
            <ta e="T278" id="Seg_7074" s="T277">dempro-pro:case</ta>
            <ta e="T279" id="Seg_7075" s="T278">n-n:(poss).[n:case]</ta>
            <ta e="T280" id="Seg_7076" s="T279">v-v:mood-v:pred.pn</ta>
            <ta e="T282" id="Seg_7077" s="T281">cardnum</ta>
            <ta e="T283" id="Seg_7078" s="T282">n-n:case</ta>
            <ta e="T284" id="Seg_7079" s="T283">propr-n:case</ta>
            <ta e="T285" id="Seg_7080" s="T284">v-v:tense-v:pred.pn</ta>
            <ta e="T286" id="Seg_7081" s="T285">n-n:case</ta>
            <ta e="T288" id="Seg_7082" s="T287">que.[pro:case]</ta>
            <ta e="T289" id="Seg_7083" s="T288">v-v:tense-v:pred.pn</ta>
            <ta e="T291" id="Seg_7084" s="T290">n-n:case</ta>
            <ta e="T292" id="Seg_7085" s="T291">v-v:(ins)-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T293" id="Seg_7086" s="T292">que-pro:case</ta>
            <ta e="T294" id="Seg_7087" s="T293">propr-n:case</ta>
            <ta e="T295" id="Seg_7088" s="T294">v-v:(ins)-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T296" id="Seg_7089" s="T295">propr-n:case</ta>
            <ta e="T298" id="Seg_7090" s="T297">adv</ta>
            <ta e="T299" id="Seg_7091" s="T298">v-v:cvb</ta>
            <ta e="T300" id="Seg_7092" s="T299">post</ta>
            <ta e="T301" id="Seg_7093" s="T300">que-pro:case</ta>
            <ta e="T303" id="Seg_7094" s="T302">interj</ta>
            <ta e="T304" id="Seg_7095" s="T303">dempro</ta>
            <ta e="T305" id="Seg_7096" s="T304">v-v:cvb</ta>
            <ta e="T306" id="Seg_7097" s="T305">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T312" id="Seg_7098" s="T311">que-pro:case</ta>
            <ta e="T314" id="Seg_7099" s="T313">v-v:ptcp</ta>
            <ta e="T315" id="Seg_7100" s="T314">n.[n:case]</ta>
            <ta e="T316" id="Seg_7101" s="T315">ptcl</ta>
            <ta e="T317" id="Seg_7102" s="T316">que-pro:case</ta>
            <ta e="T318" id="Seg_7103" s="T317">n-n:case</ta>
            <ta e="T319" id="Seg_7104" s="T318">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T320" id="Seg_7105" s="T319">v-v:cvb</ta>
            <ta e="T321" id="Seg_7106" s="T320">propr-n:case</ta>
            <ta e="T323" id="Seg_7107" s="T322">cardnum</ta>
            <ta e="T324" id="Seg_7108" s="T323">n-n:case</ta>
            <ta e="T325" id="Seg_7109" s="T324">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T326" id="Seg_7110" s="T325">dempro-pro:case</ta>
            <ta e="T327" id="Seg_7111" s="T326">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T328" id="Seg_7112" s="T327">conj</ta>
            <ta e="T329" id="Seg_7113" s="T328">propr-n:case</ta>
            <ta e="T331" id="Seg_7114" s="T330">que-que&gt;v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T332" id="Seg_7115" s="T331">n-n:case</ta>
            <ta e="T336" id="Seg_7116" s="T335">que-pro:case</ta>
            <ta e="T337" id="Seg_7117" s="T336">n-n:case</ta>
            <ta e="T338" id="Seg_7118" s="T337">v-v:mood-v:pred.pn</ta>
            <ta e="T340" id="Seg_7119" s="T339">n-n:case</ta>
            <ta e="T341" id="Seg_7120" s="T340">n.[n:case]</ta>
            <ta e="T342" id="Seg_7121" s="T341">v-v:tense-v:poss.pn</ta>
            <ta e="T343" id="Seg_7122" s="T342">propr-n:case</ta>
            <ta e="T344" id="Seg_7123" s="T343">adv</ta>
            <ta e="T346" id="Seg_7124" s="T345">n-n:case</ta>
            <ta e="T347" id="Seg_7125" s="T346">v-v:mood-v:pred.pn</ta>
            <ta e="T349" id="Seg_7126" s="T348">dempro-pro:case</ta>
            <ta e="T352" id="Seg_7127" s="T351">propr-n:case</ta>
            <ta e="T353" id="Seg_7128" s="T352">cardnum</ta>
            <ta e="T354" id="Seg_7129" s="T353">n-n:case</ta>
            <ta e="T355" id="Seg_7130" s="T354">adv</ta>
            <ta e="T356" id="Seg_7131" s="T355">propr-n:case</ta>
            <ta e="T357" id="Seg_7132" s="T356">cardnum</ta>
            <ta e="T358" id="Seg_7133" s="T357">n-n:case</ta>
            <ta e="T359" id="Seg_7134" s="T358">post</ta>
            <ta e="T360" id="Seg_7135" s="T359">n-n&gt;v-v&gt;v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T370" id="Seg_7136" s="T369">dempro-pro:case</ta>
            <ta e="T371" id="Seg_7137" s="T370">v-v&gt;v-v:cvb</ta>
            <ta e="T372" id="Seg_7138" s="T371">v-v:cvb-v:pred.pn</ta>
            <ta e="T373" id="Seg_7139" s="T372">conj</ta>
            <ta e="T374" id="Seg_7140" s="T373">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T375" id="Seg_7141" s="T374">propr-n:case</ta>
            <ta e="T376" id="Seg_7142" s="T375">v-v:cvb</ta>
            <ta e="T377" id="Seg_7143" s="T376">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T379" id="Seg_7144" s="T378">pers.[pro:case]</ta>
            <ta e="T380" id="Seg_7145" s="T379">n-n:case</ta>
            <ta e="T381" id="Seg_7146" s="T380">v-v:ptcp</ta>
            <ta e="T382" id="Seg_7147" s="T381">v-v:tense-v:poss.pn</ta>
            <ta e="T383" id="Seg_7148" s="T382">n-n:case</ta>
            <ta e="T384" id="Seg_7149" s="T383">conj</ta>
            <ta e="T385" id="Seg_7150" s="T384">pers-pro:(poss).[pro:case]</ta>
            <ta e="T386" id="Seg_7151" s="T385">ptcl</ta>
            <ta e="T387" id="Seg_7152" s="T386">n-n:case</ta>
            <ta e="T389" id="Seg_7153" s="T388">v-v:ptcp</ta>
            <ta e="T390" id="Seg_7154" s="T389">v-v:tense-v:poss.pn</ta>
            <ta e="T392" id="Seg_7155" s="T391">dempro-pro:case</ta>
            <ta e="T397" id="Seg_7156" s="T396">que.[pro:case]</ta>
            <ta e="T398" id="Seg_7157" s="T397">n-n:(poss).[n:case]</ta>
            <ta e="T399" id="Seg_7158" s="T398">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T400" id="Seg_7159" s="T399">propr</ta>
            <ta e="T401" id="Seg_7160" s="T400">n-n&gt;adj</ta>
            <ta e="T402" id="Seg_7161" s="T401">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T403" id="Seg_7162" s="T402">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T405" id="Seg_7163" s="T404">dempro-pro:case</ta>
            <ta e="T411" id="Seg_7164" s="T410">n-n:poss-n:case</ta>
            <ta e="T412" id="Seg_7165" s="T411">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T413" id="Seg_7166" s="T412">n-n:poss-n:case</ta>
            <ta e="T414" id="Seg_7167" s="T413">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T422" id="Seg_7168" s="T421">v-v:tense-v:poss.pn</ta>
            <ta e="T424" id="Seg_7169" s="T423">n-n:poss-n:case</ta>
            <ta e="T425" id="Seg_7170" s="T424">post</ta>
            <ta e="T426" id="Seg_7171" s="T425">ptcl</ta>
            <ta e="T427" id="Seg_7172" s="T426">n-n:poss-n:case</ta>
            <ta e="T428" id="Seg_7173" s="T427">post</ta>
            <ta e="T433" id="Seg_7174" s="T432">pers.[pro:case]</ta>
            <ta e="T434" id="Seg_7175" s="T433">n-n:case</ta>
            <ta e="T435" id="Seg_7176" s="T434">pers.[pro:case]</ta>
            <ta e="T436" id="Seg_7177" s="T435">n-n:(ins)-n:poss-n:case</ta>
            <ta e="T437" id="Seg_7178" s="T436">pers.[pro:case]</ta>
            <ta e="T438" id="Seg_7179" s="T437">v-v:tense-v:poss.pn</ta>
            <ta e="T445" id="Seg_7180" s="T444">que</ta>
            <ta e="T446" id="Seg_7181" s="T445">n.[n:case]</ta>
            <ta e="T447" id="Seg_7182" s="T446">ptcl</ta>
            <ta e="T448" id="Seg_7183" s="T447">ptcl</ta>
            <ta e="T452" id="Seg_7184" s="T451">n-n:case</ta>
            <ta e="T465" id="Seg_7185" s="T464">n-n:case</ta>
            <ta e="T466" id="Seg_7186" s="T465">v-v:tense-v:poss.pn</ta>
            <ta e="T473" id="Seg_7187" s="T472">pers.[pro:case]</ta>
            <ta e="T474" id="Seg_7188" s="T473">n-n&gt;v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T475" id="Seg_7189" s="T474">n-n:poss-n:case</ta>
            <ta e="T484" id="Seg_7190" s="T483">propr-n:case</ta>
            <ta e="T493" id="Seg_7191" s="T492">v-v:tense-v:pred.pn</ta>
            <ta e="T494" id="Seg_7192" s="T493">n-n:case</ta>
            <ta e="T495" id="Seg_7193" s="T494">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T499" id="Seg_7194" s="T498">n-n:case</ta>
            <ta e="T500" id="Seg_7195" s="T499">dempro</ta>
            <ta e="T501" id="Seg_7196" s="T500">ptcl</ta>
            <ta e="T502" id="Seg_7197" s="T501">n-n:case</ta>
            <ta e="T504" id="Seg_7198" s="T503">dempro-pro:case</ta>
            <ta e="T505" id="Seg_7199" s="T504">que-pro:case</ta>
            <ta e="T506" id="Seg_7200" s="T505">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T507" id="Seg_7201" s="T506">dempro-pro:case</ta>
            <ta e="T508" id="Seg_7202" s="T507">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T509" id="Seg_7203" s="T508">ptcl</ta>
            <ta e="T510" id="Seg_7204" s="T509">dempro</ta>
            <ta e="T511" id="Seg_7205" s="T510">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T512" id="Seg_7206" s="T511">n-n:case</ta>
            <ta e="T513" id="Seg_7207" s="T512">v-v:mood.[v:pred.pn]</ta>
            <ta e="T514" id="Seg_7208" s="T513">adv</ta>
            <ta e="T516" id="Seg_7209" s="T515">n-n:case</ta>
            <ta e="T517" id="Seg_7210" s="T516">v-v:mood-v:pred.pn</ta>
            <ta e="T518" id="Seg_7211" s="T517">adj.[n:case]</ta>
            <ta e="T524" id="Seg_7212" s="T523">n-n:(num)-n:poss-n:case</ta>
            <ta e="T525" id="Seg_7213" s="T524">pers-pro:case</ta>
            <ta e="T526" id="Seg_7214" s="T525">v-v:mood-v:pred.pn</ta>
            <ta e="T527" id="Seg_7215" s="T526">n-n:(ins)-n:case</ta>
            <ta e="T528" id="Seg_7216" s="T527">v-v&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T533" id="Seg_7217" s="T532">que.[pro:case]</ta>
            <ta e="T534" id="Seg_7218" s="T533">v-v:mood-v:pred.pn</ta>
            <ta e="T537" id="Seg_7219" s="T536">que-pro:case</ta>
            <ta e="T540" id="Seg_7220" s="T539">v-v&gt;v-v:cvb</ta>
            <ta e="T541" id="Seg_7221" s="T540">v-v:cvb</ta>
            <ta e="T542" id="Seg_7222" s="T541">v-v:cvb</ta>
            <ta e="T544" id="Seg_7223" s="T543">v-v:mood-v:pred.pn</ta>
            <ta e="T545" id="Seg_7224" s="T544">ptcl</ta>
            <ta e="T546" id="Seg_7225" s="T545">ptcl</ta>
            <ta e="T547" id="Seg_7226" s="T546">que-pro:case</ta>
            <ta e="T548" id="Seg_7227" s="T547">n-n:case</ta>
            <ta e="T549" id="Seg_7228" s="T548">v-v:mood-v:pred.pn</ta>
            <ta e="T550" id="Seg_7229" s="T549">v-v&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T559" id="Seg_7230" s="T558">v-v:mood-v:pred.pn</ta>
            <ta e="T569" id="Seg_7231" s="T568">v-v:mood-v:pred.pn</ta>
            <ta e="T570" id="Seg_7232" s="T569">n-n:(num)-n:case</ta>
            <ta e="T575" id="Seg_7233" s="T574">v-v:tense-v:poss.pn</ta>
            <ta e="T576" id="Seg_7234" s="T575">v-v:tense-v:poss.pn</ta>
            <ta e="T577" id="Seg_7235" s="T576">dempro</ta>
            <ta e="T578" id="Seg_7236" s="T577">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T579" id="Seg_7237" s="T578">ptcl</ta>
            <ta e="T580" id="Seg_7238" s="T579">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T583" id="Seg_7239" s="T582">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T584" id="Seg_7240" s="T583">dempro-pro:case</ta>
            <ta e="T585" id="Seg_7241" s="T584">ptcl</ta>
            <ta e="T586" id="Seg_7242" s="T585">adv</ta>
            <ta e="T587" id="Seg_7243" s="T586">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T588" id="Seg_7244" s="T587">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T589" id="Seg_7245" s="T588">n-n:poss-n:case</ta>
            <ta e="T590" id="Seg_7246" s="T589">n-n:(poss).[n:case]</ta>
            <ta e="T591" id="Seg_7247" s="T590">dempro</ta>
            <ta e="T593" id="Seg_7248" s="T592">n-n:case</ta>
            <ta e="T594" id="Seg_7249" s="T593">v-v:mood-v:pred.pn</ta>
            <ta e="T595" id="Seg_7250" s="T594">n.[n:case]</ta>
            <ta e="T596" id="Seg_7251" s="T595">ptcl</ta>
            <ta e="T597" id="Seg_7252" s="T596">dempro.[pro:case]</ta>
            <ta e="T600" id="Seg_7253" s="T599">que-que&gt;v-v:(neg)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T601" id="Seg_7254" s="T600">v-v:(neg)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T602" id="Seg_7255" s="T601">pers-pro:case</ta>
            <ta e="T607" id="Seg_7256" s="T606">v-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T618" id="Seg_7257" s="T617">que-pro:case</ta>
            <ta e="T619" id="Seg_7258" s="T618">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T621" id="Seg_7259" s="T620">que.[pro:case]</ta>
            <ta e="T622" id="Seg_7260" s="T621">dempro</ta>
            <ta e="T623" id="Seg_7261" s="T622">n-n:case</ta>
            <ta e="T624" id="Seg_7262" s="T623">dempro-pro:case</ta>
            <ta e="T625" id="Seg_7263" s="T624">dempro.[pro:case]</ta>
            <ta e="T626" id="Seg_7264" s="T625">n-n:case</ta>
            <ta e="T627" id="Seg_7265" s="T626">v-v:tense-v:poss.pn</ta>
            <ta e="T628" id="Seg_7266" s="T627">ptcl</ta>
            <ta e="T631" id="Seg_7267" s="T630">cardnum</ta>
            <ta e="T632" id="Seg_7268" s="T631">cardnum</ta>
            <ta e="T633" id="Seg_7269" s="T632">n-n:case</ta>
            <ta e="T634" id="Seg_7270" s="T633">cardnum</ta>
            <ta e="T635" id="Seg_7271" s="T634">cardnum</ta>
            <ta e="T636" id="Seg_7272" s="T635">n-n:case</ta>
            <ta e="T637" id="Seg_7273" s="T636">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T638" id="Seg_7274" s="T637">n-n:case</ta>
            <ta e="T640" id="Seg_7275" s="T639">adv-n:case</ta>
            <ta e="T641" id="Seg_7276" s="T640">n.[n:case]</ta>
            <ta e="T642" id="Seg_7277" s="T641">n.[n:case]</ta>
            <ta e="T643" id="Seg_7278" s="T642">post</ta>
            <ta e="T644" id="Seg_7279" s="T643">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T645" id="Seg_7280" s="T644">n-n:case</ta>
            <ta e="T647" id="Seg_7281" s="T646">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T648" id="Seg_7282" s="T647">n-n:case</ta>
            <ta e="T650" id="Seg_7283" s="T649">n-n:case</ta>
            <ta e="T651" id="Seg_7284" s="T650">adv</ta>
            <ta e="T652" id="Seg_7285" s="T651">n-n:case</ta>
            <ta e="T653" id="Seg_7286" s="T652">v-v:tense-v:poss.pn</ta>
            <ta e="T655" id="Seg_7287" s="T654">interj</ta>
            <ta e="T656" id="Seg_7288" s="T655">que-que&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T657" id="Seg_7289" s="T656">v-v:tense-v:poss.pn</ta>
            <ta e="T658" id="Seg_7290" s="T657">que.[pro:case]</ta>
            <ta e="T659" id="Seg_7291" s="T658">adv</ta>
            <ta e="T660" id="Seg_7292" s="T659">n-n:case</ta>
            <ta e="T661" id="Seg_7293" s="T660">n-n:case</ta>
            <ta e="T662" id="Seg_7294" s="T661">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T666" id="Seg_7295" s="T665">n-n:case</ta>
            <ta e="T667" id="Seg_7296" s="T666">v-v:tense-v:pred.pn</ta>
            <ta e="T670" id="Seg_7297" s="T669">n-n:(num)-n:poss-n:case</ta>
            <ta e="T672" id="Seg_7298" s="T671">n-n:poss-n:case</ta>
            <ta e="T676" id="Seg_7299" s="T675">v-v:tense-v:pred.pn</ta>
            <ta e="T677" id="Seg_7300" s="T676">v-v:cvb</ta>
            <ta e="T678" id="Seg_7301" s="T677">v-v:tense-v:pred.pn</ta>
            <ta e="T686" id="Seg_7302" s="T685">conj</ta>
            <ta e="T687" id="Seg_7303" s="T686">pers.[pro:case]</ta>
            <ta e="T688" id="Seg_7304" s="T687">dempro</ta>
            <ta e="T689" id="Seg_7305" s="T688">n-n:case</ta>
            <ta e="T690" id="Seg_7306" s="T689">post</ta>
            <ta e="T691" id="Seg_7307" s="T690">v-v:tense-v:pred.pn</ta>
            <ta e="T695" id="Seg_7308" s="T694">cardnum-n:case</ta>
            <ta e="T696" id="Seg_7309" s="T695">n-n&gt;v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T697" id="Seg_7310" s="T696">n-n:case</ta>
            <ta e="T705" id="Seg_7311" s="T704">que-que&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T706" id="Seg_7312" s="T705">adv</ta>
            <ta e="T707" id="Seg_7313" s="T706">v-v:(ins)-v:tense.[v:pred.pn]</ta>
            <ta e="T708" id="Seg_7314" s="T707">n-n&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T710" id="Seg_7315" s="T709">n-n:(poss).[n:case]</ta>
            <ta e="T711" id="Seg_7316" s="T710">v-v:tense.[v:pred.pn]</ta>
            <ta e="T715" id="Seg_7317" s="T714">ptcl</ta>
            <ta e="T716" id="Seg_7318" s="T715">que.[pro:case]</ta>
            <ta e="T717" id="Seg_7319" s="T716">dempro</ta>
            <ta e="T718" id="Seg_7320" s="T717">adj</ta>
            <ta e="T719" id="Seg_7321" s="T718">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T720" id="Seg_7322" s="T719">n-n:poss-n:case</ta>
            <ta e="T721" id="Seg_7323" s="T720">adj-n:(poss)</ta>
            <ta e="T722" id="Seg_7324" s="T721">adj</ta>
            <ta e="T726" id="Seg_7325" s="T725">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T727" id="Seg_7326" s="T726">adj</ta>
            <ta e="T728" id="Seg_7327" s="T727">adv</ta>
            <ta e="T729" id="Seg_7328" s="T728">v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T730" id="Seg_7329" s="T729">que-pro:case</ta>
            <ta e="T731" id="Seg_7330" s="T730">n-n:case</ta>
            <ta e="T835" id="Seg_7331" s="T732">adj-n:num.[n:case]</ta>
            <ta e="T733" id="Seg_7332" s="T835">v-v:tense-v:pred.pn</ta>
            <ta e="T734" id="Seg_7333" s="T733">conj</ta>
            <ta e="T735" id="Seg_7334" s="T734">n-n:(poss).[n:case]</ta>
            <ta e="T736" id="Seg_7335" s="T735">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T737" id="Seg_7336" s="T736">n-n:(poss).[n:case]</ta>
            <ta e="T738" id="Seg_7337" s="T737">adj</ta>
            <ta e="T739" id="Seg_7338" s="T738">ptcl</ta>
            <ta e="T740" id="Seg_7339" s="T739">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T742" id="Seg_7340" s="T741">n-n:(poss).[n:case]</ta>
            <ta e="T743" id="Seg_7341" s="T742">n-n:(poss).[n:case]</ta>
            <ta e="T744" id="Seg_7342" s="T743">propr.[n:case]</ta>
            <ta e="T745" id="Seg_7343" s="T744">n-n:(poss).[n:case]</ta>
            <ta e="T746" id="Seg_7344" s="T745">dempro.[pro:case]</ta>
            <ta e="T748" id="Seg_7345" s="T747">que-pro:case</ta>
            <ta e="T749" id="Seg_7346" s="T748">v-v:tense.[v:pred.pn]</ta>
            <ta e="T750" id="Seg_7347" s="T749">adv</ta>
            <ta e="T751" id="Seg_7348" s="T750">n-n:poss-n:case</ta>
            <ta e="T752" id="Seg_7349" s="T751">post</ta>
            <ta e="T754" id="Seg_7350" s="T753">ptcl</ta>
            <ta e="T755" id="Seg_7351" s="T754">adj</ta>
            <ta e="T756" id="Seg_7352" s="T755">que-pro:case</ta>
            <ta e="T757" id="Seg_7353" s="T756">n-n:case</ta>
            <ta e="T758" id="Seg_7354" s="T757">v-v:tense.[v:pred.pn]</ta>
            <ta e="T759" id="Seg_7355" s="T758">n-n:poss-n:case</ta>
            <ta e="T760" id="Seg_7356" s="T759">post</ta>
            <ta e="T767" id="Seg_7357" s="T766">dempro.[pro:case]</ta>
            <ta e="T768" id="Seg_7358" s="T767">que-pro:case</ta>
            <ta e="T772" id="Seg_7359" s="T771">v-v:tense-v:poss.pn</ta>
            <ta e="T773" id="Seg_7360" s="T772">pers-pro:case</ta>
            <ta e="T777" id="Seg_7361" s="T776">adv</ta>
            <ta e="T778" id="Seg_7362" s="T777">v-v:cvb</ta>
            <ta e="T779" id="Seg_7363" s="T778">conj</ta>
            <ta e="T780" id="Seg_7364" s="T779">n-n:poss-n:case</ta>
            <ta e="T781" id="Seg_7365" s="T780">v-v:(ins)-v:(neg)-v:cvb</ta>
            <ta e="T782" id="Seg_7366" s="T781">ptcl</ta>
            <ta e="T789" id="Seg_7367" s="T788">adv</ta>
            <ta e="T791" id="Seg_7368" s="T790">cardnum</ta>
            <ta e="T792" id="Seg_7369" s="T791">n-n:case</ta>
            <ta e="T793" id="Seg_7370" s="T792">ptcl</ta>
            <ta e="T794" id="Seg_7371" s="T793">v-v:ptcp-v:(case)</ta>
            <ta e="T801" id="Seg_7372" s="T800">cardnum</ta>
            <ta e="T802" id="Seg_7373" s="T801">n.[n:case]</ta>
            <ta e="T806" id="Seg_7374" s="T805">n.[n:case]</ta>
            <ta e="T807" id="Seg_7375" s="T806">v-v:tense-v:pred.pn</ta>
            <ta e="T817" id="Seg_7376" s="T816">adj</ta>
            <ta e="T818" id="Seg_7377" s="T817">n-n:poss-n:case</ta>
            <ta e="T820" id="Seg_7378" s="T819">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T821" id="Seg_7379" s="T820">n-n:poss-n:case</ta>
            <ta e="T822" id="Seg_7380" s="T821">post</ta>
            <ta e="T823" id="Seg_7381" s="T822">n-n:poss-n:case</ta>
            <ta e="T824" id="Seg_7382" s="T823">post</ta>
            <ta e="T825" id="Seg_7383" s="T824">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T826" id="Seg_7384" s="T825">n-n:poss-n:case</ta>
            <ta e="T828" id="Seg_7385" s="T827">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T829" id="Seg_7386" s="T828">n-n:case</ta>
            <ta e="T830" id="Seg_7387" s="T829">v-v:tense.[v:pred.pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_7388" s="T1">pers</ta>
            <ta e="T3" id="Seg_7389" s="T2">n</ta>
            <ta e="T4" id="Seg_7390" s="T3">propr</ta>
            <ta e="T5" id="Seg_7391" s="T4">adv</ta>
            <ta e="T6" id="Seg_7392" s="T5">propr</ta>
            <ta e="T7" id="Seg_7393" s="T6">v</ta>
            <ta e="T9" id="Seg_7394" s="T8">v</ta>
            <ta e="T10" id="Seg_7395" s="T9">propr</ta>
            <ta e="T11" id="Seg_7396" s="T10">v</ta>
            <ta e="T12" id="Seg_7397" s="T11">n</ta>
            <ta e="T14" id="Seg_7398" s="T13">pers</ta>
            <ta e="T15" id="Seg_7399" s="T14">n</ta>
            <ta e="T16" id="Seg_7400" s="T15">n</ta>
            <ta e="T17" id="Seg_7401" s="T16">que</ta>
            <ta e="T18" id="Seg_7402" s="T17">propr</ta>
            <ta e="T19" id="Seg_7403" s="T18">n</ta>
            <ta e="T20" id="Seg_7404" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_7405" s="T20">propr</ta>
            <ta e="T22" id="Seg_7406" s="T21">propr</ta>
            <ta e="T24" id="Seg_7407" s="T23">dempro</ta>
            <ta e="T25" id="Seg_7408" s="T24">n</ta>
            <ta e="T26" id="Seg_7409" s="T25">adv</ta>
            <ta e="T27" id="Seg_7410" s="T26">ptcl</ta>
            <ta e="T28" id="Seg_7411" s="T27">v</ta>
            <ta e="T29" id="Seg_7412" s="T28">dempro</ta>
            <ta e="T30" id="Seg_7413" s="T29">pers</ta>
            <ta e="T31" id="Seg_7414" s="T30">n</ta>
            <ta e="T32" id="Seg_7415" s="T31">post</ta>
            <ta e="T33" id="Seg_7416" s="T32">v</ta>
            <ta e="T35" id="Seg_7417" s="T34">n</ta>
            <ta e="T36" id="Seg_7418" s="T35">ptcl</ta>
            <ta e="T37" id="Seg_7419" s="T36">que</ta>
            <ta e="T38" id="Seg_7420" s="T37">n</ta>
            <ta e="T39" id="Seg_7421" s="T38">n</ta>
            <ta e="T40" id="Seg_7422" s="T39">v</ta>
            <ta e="T41" id="Seg_7423" s="T40">aux</ta>
            <ta e="T43" id="Seg_7424" s="T42">dempro</ta>
            <ta e="T44" id="Seg_7425" s="T43">n</ta>
            <ta e="T45" id="Seg_7426" s="T44">adv</ta>
            <ta e="T46" id="Seg_7427" s="T45">n</ta>
            <ta e="T47" id="Seg_7428" s="T46">post</ta>
            <ta e="T48" id="Seg_7429" s="T47">v</ta>
            <ta e="T50" id="Seg_7430" s="T49">n</ta>
            <ta e="T51" id="Seg_7431" s="T50">pers</ta>
            <ta e="T52" id="Seg_7432" s="T51">v</ta>
            <ta e="T53" id="Seg_7433" s="T52">v</ta>
            <ta e="T55" id="Seg_7434" s="T54">n</ta>
            <ta e="T56" id="Seg_7435" s="T55">n</ta>
            <ta e="T57" id="Seg_7436" s="T56">v</ta>
            <ta e="T58" id="Seg_7437" s="T57">pers</ta>
            <ta e="T59" id="Seg_7438" s="T58">n</ta>
            <ta e="T60" id="Seg_7439" s="T59">cop</ta>
            <ta e="T62" id="Seg_7440" s="T61">conj</ta>
            <ta e="T63" id="Seg_7441" s="T62">v</ta>
            <ta e="T64" id="Seg_7442" s="T63">post</ta>
            <ta e="T65" id="Seg_7443" s="T64">pers</ta>
            <ta e="T66" id="Seg_7444" s="T65">n</ta>
            <ta e="T67" id="Seg_7445" s="T66">cop</ta>
            <ta e="T68" id="Seg_7446" s="T67">conj</ta>
            <ta e="T69" id="Seg_7447" s="T68">pers</ta>
            <ta e="T70" id="Seg_7448" s="T69">adj</ta>
            <ta e="T71" id="Seg_7449" s="T70">cop</ta>
            <ta e="T72" id="Seg_7450" s="T71">n</ta>
            <ta e="T73" id="Seg_7451" s="T72">v</ta>
            <ta e="T74" id="Seg_7452" s="T73">adj</ta>
            <ta e="T75" id="Seg_7453" s="T74">n</ta>
            <ta e="T77" id="Seg_7454" s="T76">v</ta>
            <ta e="T78" id="Seg_7455" s="T77">dempro</ta>
            <ta e="T79" id="Seg_7456" s="T78">v</ta>
            <ta e="T80" id="Seg_7457" s="T79">aux</ta>
            <ta e="T81" id="Seg_7458" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_7459" s="T81">n</ta>
            <ta e="T83" id="Seg_7460" s="T82">cop</ta>
            <ta e="T84" id="Seg_7461" s="T83">v</ta>
            <ta e="T85" id="Seg_7462" s="T84">n</ta>
            <ta e="T86" id="Seg_7463" s="T85">n</ta>
            <ta e="T87" id="Seg_7464" s="T86">v</ta>
            <ta e="T89" id="Seg_7465" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_7466" s="T89">dempro</ta>
            <ta e="T91" id="Seg_7467" s="T90">v</ta>
            <ta e="T92" id="Seg_7468" s="T91">v</ta>
            <ta e="T93" id="Seg_7469" s="T92">que</ta>
            <ta e="T94" id="Seg_7470" s="T93">n</ta>
            <ta e="T95" id="Seg_7471" s="T94">n</ta>
            <ta e="T96" id="Seg_7472" s="T95">post</ta>
            <ta e="T101" id="Seg_7473" s="T100">v</ta>
            <ta e="T102" id="Seg_7474" s="T101">v</ta>
            <ta e="T104" id="Seg_7475" s="T103">n</ta>
            <ta e="T105" id="Seg_7476" s="T104">adj</ta>
            <ta e="T106" id="Seg_7477" s="T105">v</ta>
            <ta e="T107" id="Seg_7478" s="T106">ptcl</ta>
            <ta e="T109" id="Seg_7479" s="T108">interj</ta>
            <ta e="T110" id="Seg_7480" s="T109">n</ta>
            <ta e="T111" id="Seg_7481" s="T110">v</ta>
            <ta e="T112" id="Seg_7482" s="T111">v</ta>
            <ta e="T120" id="Seg_7483" s="T119">n</ta>
            <ta e="T121" id="Seg_7484" s="T120">n</ta>
            <ta e="T122" id="Seg_7485" s="T121">v</ta>
            <ta e="T133" id="Seg_7486" s="T132">ptcl</ta>
            <ta e="T138" id="Seg_7487" s="T137">n</ta>
            <ta e="T139" id="Seg_7488" s="T138">n</ta>
            <ta e="T140" id="Seg_7489" s="T139">interj</ta>
            <ta e="T142" id="Seg_7490" s="T141">n</ta>
            <ta e="T144" id="Seg_7491" s="T143">que</ta>
            <ta e="T149" id="Seg_7492" s="T148">dempro</ta>
            <ta e="T150" id="Seg_7493" s="T149">n</ta>
            <ta e="T151" id="Seg_7494" s="T150">adv</ta>
            <ta e="T152" id="Seg_7495" s="T151">adj</ta>
            <ta e="T153" id="Seg_7496" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_7497" s="T153">v</ta>
            <ta e="T155" id="Seg_7498" s="T154">quant</ta>
            <ta e="T156" id="Seg_7499" s="T155">n</ta>
            <ta e="T157" id="Seg_7500" s="T156">dempro</ta>
            <ta e="T158" id="Seg_7501" s="T157">v</ta>
            <ta e="T159" id="Seg_7502" s="T158">quant</ta>
            <ta e="T163" id="Seg_7503" s="T162">quant</ta>
            <ta e="T164" id="Seg_7504" s="T163">ptcl</ta>
            <ta e="T167" id="Seg_7505" s="T166">que</ta>
            <ta e="T168" id="Seg_7506" s="T167">adj</ta>
            <ta e="T169" id="Seg_7507" s="T168">n</ta>
            <ta e="T170" id="Seg_7508" s="T169">adj</ta>
            <ta e="T171" id="Seg_7509" s="T170">n</ta>
            <ta e="T173" id="Seg_7510" s="T172">adv</ta>
            <ta e="T174" id="Seg_7511" s="T173">n</ta>
            <ta e="T175" id="Seg_7512" s="T174">n</ta>
            <ta e="T176" id="Seg_7513" s="T175">adj</ta>
            <ta e="T177" id="Seg_7514" s="T176">n</ta>
            <ta e="T188" id="Seg_7515" s="T187">que</ta>
            <ta e="T189" id="Seg_7516" s="T188">n</ta>
            <ta e="T191" id="Seg_7517" s="T190">v</ta>
            <ta e="T193" id="Seg_7518" s="T192">n</ta>
            <ta e="T194" id="Seg_7519" s="T193">v</ta>
            <ta e="T195" id="Seg_7520" s="T194">v</ta>
            <ta e="T199" id="Seg_7521" s="T198">v</ta>
            <ta e="T200" id="Seg_7522" s="T199">v</ta>
            <ta e="T202" id="Seg_7523" s="T201">ptcl</ta>
            <ta e="T203" id="Seg_7524" s="T202">adj</ta>
            <ta e="T204" id="Seg_7525" s="T203">adj</ta>
            <ta e="T205" id="Seg_7526" s="T204">v</ta>
            <ta e="T211" id="Seg_7527" s="T210">que</ta>
            <ta e="T212" id="Seg_7528" s="T211">adj</ta>
            <ta e="T213" id="Seg_7529" s="T212">n</ta>
            <ta e="T222" id="Seg_7530" s="T221">v</ta>
            <ta e="T223" id="Seg_7531" s="T222">v</ta>
            <ta e="T225" id="Seg_7532" s="T224">dempro</ta>
            <ta e="T226" id="Seg_7533" s="T225">que</ta>
            <ta e="T227" id="Seg_7534" s="T226">v</ta>
            <ta e="T228" id="Seg_7535" s="T227">n</ta>
            <ta e="T229" id="Seg_7536" s="T228">ptcl</ta>
            <ta e="T230" id="Seg_7537" s="T229">propr</ta>
            <ta e="T231" id="Seg_7538" s="T230">que</ta>
            <ta e="T232" id="Seg_7539" s="T231">n</ta>
            <ta e="T233" id="Seg_7540" s="T232">v</ta>
            <ta e="T234" id="Seg_7541" s="T233">propr</ta>
            <ta e="T238" id="Seg_7542" s="T237">que</ta>
            <ta e="T239" id="Seg_7543" s="T238">n</ta>
            <ta e="T240" id="Seg_7544" s="T239">ptcl</ta>
            <ta e="T241" id="Seg_7545" s="T240">ptcl</ta>
            <ta e="T254" id="Seg_7546" s="T253">que</ta>
            <ta e="T255" id="Seg_7547" s="T254">n</ta>
            <ta e="T263" id="Seg_7548" s="T262">adv</ta>
            <ta e="T264" id="Seg_7549" s="T263">que</ta>
            <ta e="T265" id="Seg_7550" s="T264">que</ta>
            <ta e="T266" id="Seg_7551" s="T265">pers</ta>
            <ta e="T267" id="Seg_7552" s="T266">emphpro</ta>
            <ta e="T268" id="Seg_7553" s="T267">v</ta>
            <ta e="T269" id="Seg_7554" s="T268">cardnum</ta>
            <ta e="T270" id="Seg_7555" s="T269">n</ta>
            <ta e="T271" id="Seg_7556" s="T270">post</ta>
            <ta e="T272" id="Seg_7557" s="T271">ordnum</ta>
            <ta e="T273" id="Seg_7558" s="T272">n</ta>
            <ta e="T274" id="Seg_7559" s="T273">v</ta>
            <ta e="T276" id="Seg_7560" s="T275">que</ta>
            <ta e="T277" id="Seg_7561" s="T276">n</ta>
            <ta e="T278" id="Seg_7562" s="T277">dempro</ta>
            <ta e="T279" id="Seg_7563" s="T278">n</ta>
            <ta e="T280" id="Seg_7564" s="T279">cop</ta>
            <ta e="T282" id="Seg_7565" s="T281">cardnum</ta>
            <ta e="T283" id="Seg_7566" s="T282">n</ta>
            <ta e="T284" id="Seg_7567" s="T283">propr</ta>
            <ta e="T285" id="Seg_7568" s="T284">v</ta>
            <ta e="T286" id="Seg_7569" s="T285">n</ta>
            <ta e="T288" id="Seg_7570" s="T287">que</ta>
            <ta e="T289" id="Seg_7571" s="T288">v</ta>
            <ta e="T291" id="Seg_7572" s="T290">n</ta>
            <ta e="T292" id="Seg_7573" s="T291">v</ta>
            <ta e="T293" id="Seg_7574" s="T292">que</ta>
            <ta e="T294" id="Seg_7575" s="T293">propr</ta>
            <ta e="T295" id="Seg_7576" s="T294">v</ta>
            <ta e="T296" id="Seg_7577" s="T295">propr</ta>
            <ta e="T298" id="Seg_7578" s="T297">adv</ta>
            <ta e="T299" id="Seg_7579" s="T298">v</ta>
            <ta e="T300" id="Seg_7580" s="T299">post</ta>
            <ta e="T301" id="Seg_7581" s="T300">que</ta>
            <ta e="T303" id="Seg_7582" s="T302">interj</ta>
            <ta e="T304" id="Seg_7583" s="T303">dempro</ta>
            <ta e="T305" id="Seg_7584" s="T304">v</ta>
            <ta e="T306" id="Seg_7585" s="T305">v</ta>
            <ta e="T312" id="Seg_7586" s="T311">que</ta>
            <ta e="T314" id="Seg_7587" s="T313">v</ta>
            <ta e="T315" id="Seg_7588" s="T314">n</ta>
            <ta e="T316" id="Seg_7589" s="T315">ptcl</ta>
            <ta e="T317" id="Seg_7590" s="T316">que</ta>
            <ta e="T318" id="Seg_7591" s="T317">n</ta>
            <ta e="T319" id="Seg_7592" s="T318">v</ta>
            <ta e="T320" id="Seg_7593" s="T319">v</ta>
            <ta e="T321" id="Seg_7594" s="T320">propr</ta>
            <ta e="T323" id="Seg_7595" s="T322">cardnum</ta>
            <ta e="T324" id="Seg_7596" s="T323">n</ta>
            <ta e="T325" id="Seg_7597" s="T324">v</ta>
            <ta e="T326" id="Seg_7598" s="T325">dempro</ta>
            <ta e="T327" id="Seg_7599" s="T326">v</ta>
            <ta e="T328" id="Seg_7600" s="T327">conj</ta>
            <ta e="T329" id="Seg_7601" s="T328">propr</ta>
            <ta e="T331" id="Seg_7602" s="T330">v</ta>
            <ta e="T332" id="Seg_7603" s="T331">n</ta>
            <ta e="T336" id="Seg_7604" s="T335">que</ta>
            <ta e="T337" id="Seg_7605" s="T336">n</ta>
            <ta e="T338" id="Seg_7606" s="T337">v</ta>
            <ta e="T340" id="Seg_7607" s="T339">n</ta>
            <ta e="T341" id="Seg_7608" s="T340">n</ta>
            <ta e="T342" id="Seg_7609" s="T341">cop</ta>
            <ta e="T343" id="Seg_7610" s="T342">propr</ta>
            <ta e="T344" id="Seg_7611" s="T343">adv</ta>
            <ta e="T346" id="Seg_7612" s="T345">n</ta>
            <ta e="T347" id="Seg_7613" s="T346">v</ta>
            <ta e="T349" id="Seg_7614" s="T348">dempro</ta>
            <ta e="T352" id="Seg_7615" s="T351">propr</ta>
            <ta e="T353" id="Seg_7616" s="T352">cardnum</ta>
            <ta e="T354" id="Seg_7617" s="T353">n</ta>
            <ta e="T355" id="Seg_7618" s="T354">adv</ta>
            <ta e="T356" id="Seg_7619" s="T355">propr</ta>
            <ta e="T357" id="Seg_7620" s="T356">cardnum</ta>
            <ta e="T358" id="Seg_7621" s="T357">n</ta>
            <ta e="T359" id="Seg_7622" s="T358">post</ta>
            <ta e="T360" id="Seg_7623" s="T359">v</ta>
            <ta e="T370" id="Seg_7624" s="T369">dempro</ta>
            <ta e="T371" id="Seg_7625" s="T370">v</ta>
            <ta e="T372" id="Seg_7626" s="T371">aux</ta>
            <ta e="T373" id="Seg_7627" s="T372">conj</ta>
            <ta e="T374" id="Seg_7628" s="T373">v</ta>
            <ta e="T375" id="Seg_7629" s="T374">propr</ta>
            <ta e="T376" id="Seg_7630" s="T375">v</ta>
            <ta e="T377" id="Seg_7631" s="T376">aux</ta>
            <ta e="T379" id="Seg_7632" s="T378">pers</ta>
            <ta e="T380" id="Seg_7633" s="T379">n</ta>
            <ta e="T381" id="Seg_7634" s="T380">v</ta>
            <ta e="T382" id="Seg_7635" s="T381">aux</ta>
            <ta e="T383" id="Seg_7636" s="T382">n</ta>
            <ta e="T384" id="Seg_7637" s="T383">conj</ta>
            <ta e="T385" id="Seg_7638" s="T384">pers</ta>
            <ta e="T386" id="Seg_7639" s="T385">ptcl</ta>
            <ta e="T387" id="Seg_7640" s="T386">n</ta>
            <ta e="T389" id="Seg_7641" s="T388">v</ta>
            <ta e="T390" id="Seg_7642" s="T389">aux</ta>
            <ta e="T392" id="Seg_7643" s="T391">dempro</ta>
            <ta e="T397" id="Seg_7644" s="T396">que</ta>
            <ta e="T398" id="Seg_7645" s="T397">n</ta>
            <ta e="T399" id="Seg_7646" s="T398">n</ta>
            <ta e="T400" id="Seg_7647" s="T399">propr</ta>
            <ta e="T401" id="Seg_7648" s="T400">adj</ta>
            <ta e="T402" id="Seg_7649" s="T401">n</ta>
            <ta e="T403" id="Seg_7650" s="T402">n</ta>
            <ta e="T405" id="Seg_7651" s="T404">dempro</ta>
            <ta e="T411" id="Seg_7652" s="T410">n</ta>
            <ta e="T412" id="Seg_7653" s="T411">v</ta>
            <ta e="T413" id="Seg_7654" s="T412">n</ta>
            <ta e="T414" id="Seg_7655" s="T413">v</ta>
            <ta e="T422" id="Seg_7656" s="T421">v</ta>
            <ta e="T424" id="Seg_7657" s="T423">n</ta>
            <ta e="T425" id="Seg_7658" s="T424">post</ta>
            <ta e="T426" id="Seg_7659" s="T425">ptcl</ta>
            <ta e="T427" id="Seg_7660" s="T426">n</ta>
            <ta e="T428" id="Seg_7661" s="T427">post</ta>
            <ta e="T433" id="Seg_7662" s="T432">pers</ta>
            <ta e="T434" id="Seg_7663" s="T433">n</ta>
            <ta e="T435" id="Seg_7664" s="T434">pers</ta>
            <ta e="T436" id="Seg_7665" s="T435">n</ta>
            <ta e="T437" id="Seg_7666" s="T436">pers</ta>
            <ta e="T438" id="Seg_7667" s="T437">v</ta>
            <ta e="T445" id="Seg_7668" s="T444">que</ta>
            <ta e="T446" id="Seg_7669" s="T445">n</ta>
            <ta e="T447" id="Seg_7670" s="T446">ptcl</ta>
            <ta e="T448" id="Seg_7671" s="T447">ptcl</ta>
            <ta e="T452" id="Seg_7672" s="T451">n</ta>
            <ta e="T465" id="Seg_7673" s="T464">n</ta>
            <ta e="T466" id="Seg_7674" s="T465">v</ta>
            <ta e="T473" id="Seg_7675" s="T472">pers</ta>
            <ta e="T474" id="Seg_7676" s="T473">v</ta>
            <ta e="T475" id="Seg_7677" s="T474">n</ta>
            <ta e="T484" id="Seg_7678" s="T483">propr</ta>
            <ta e="T493" id="Seg_7679" s="T492">v</ta>
            <ta e="T494" id="Seg_7680" s="T493">n</ta>
            <ta e="T495" id="Seg_7681" s="T494">v</ta>
            <ta e="T499" id="Seg_7682" s="T498">n</ta>
            <ta e="T500" id="Seg_7683" s="T499">dempro</ta>
            <ta e="T501" id="Seg_7684" s="T500">ptcl</ta>
            <ta e="T502" id="Seg_7685" s="T501">n</ta>
            <ta e="T504" id="Seg_7686" s="T503">dempro</ta>
            <ta e="T505" id="Seg_7687" s="T504">que</ta>
            <ta e="T506" id="Seg_7688" s="T505">v</ta>
            <ta e="T507" id="Seg_7689" s="T506">dempro</ta>
            <ta e="T508" id="Seg_7690" s="T507">n</ta>
            <ta e="T509" id="Seg_7691" s="T508">ptcl</ta>
            <ta e="T510" id="Seg_7692" s="T509">dempro</ta>
            <ta e="T511" id="Seg_7693" s="T510">n</ta>
            <ta e="T512" id="Seg_7694" s="T511">n</ta>
            <ta e="T513" id="Seg_7695" s="T512">cop</ta>
            <ta e="T514" id="Seg_7696" s="T513">adv</ta>
            <ta e="T516" id="Seg_7697" s="T515">n</ta>
            <ta e="T517" id="Seg_7698" s="T516">v</ta>
            <ta e="T518" id="Seg_7699" s="T517">adj</ta>
            <ta e="T524" id="Seg_7700" s="T523">n</ta>
            <ta e="T525" id="Seg_7701" s="T524">pers</ta>
            <ta e="T526" id="Seg_7702" s="T525">v</ta>
            <ta e="T527" id="Seg_7703" s="T526">n</ta>
            <ta e="T528" id="Seg_7704" s="T527">v</ta>
            <ta e="T533" id="Seg_7705" s="T532">que</ta>
            <ta e="T534" id="Seg_7706" s="T533">v</ta>
            <ta e="T537" id="Seg_7707" s="T536">que</ta>
            <ta e="T540" id="Seg_7708" s="T539">v</ta>
            <ta e="T541" id="Seg_7709" s="T540">v</ta>
            <ta e="T542" id="Seg_7710" s="T541">v</ta>
            <ta e="T544" id="Seg_7711" s="T543">v</ta>
            <ta e="T545" id="Seg_7712" s="T544">ptcl</ta>
            <ta e="T546" id="Seg_7713" s="T545">ptcl</ta>
            <ta e="T547" id="Seg_7714" s="T546">que</ta>
            <ta e="T548" id="Seg_7715" s="T547">n</ta>
            <ta e="T549" id="Seg_7716" s="T548">v</ta>
            <ta e="T550" id="Seg_7717" s="T549">v</ta>
            <ta e="T559" id="Seg_7718" s="T558">v</ta>
            <ta e="T569" id="Seg_7719" s="T568">v</ta>
            <ta e="T570" id="Seg_7720" s="T569">n</ta>
            <ta e="T575" id="Seg_7721" s="T574">v</ta>
            <ta e="T576" id="Seg_7722" s="T575">v</ta>
            <ta e="T577" id="Seg_7723" s="T576">dempro</ta>
            <ta e="T578" id="Seg_7724" s="T577">n</ta>
            <ta e="T579" id="Seg_7725" s="T578">ptcl</ta>
            <ta e="T580" id="Seg_7726" s="T579">n</ta>
            <ta e="T583" id="Seg_7727" s="T582">n</ta>
            <ta e="T584" id="Seg_7728" s="T583">dempro</ta>
            <ta e="T585" id="Seg_7729" s="T584">ptcl</ta>
            <ta e="T586" id="Seg_7730" s="T585">adv</ta>
            <ta e="T587" id="Seg_7731" s="T586">n</ta>
            <ta e="T588" id="Seg_7732" s="T587">n</ta>
            <ta e="T589" id="Seg_7733" s="T588">n</ta>
            <ta e="T590" id="Seg_7734" s="T589">n</ta>
            <ta e="T591" id="Seg_7735" s="T590">dempro</ta>
            <ta e="T593" id="Seg_7736" s="T592">n</ta>
            <ta e="T594" id="Seg_7737" s="T593">v</ta>
            <ta e="T595" id="Seg_7738" s="T594">n</ta>
            <ta e="T596" id="Seg_7739" s="T595">ptcl</ta>
            <ta e="T597" id="Seg_7740" s="T596">dempro</ta>
            <ta e="T600" id="Seg_7741" s="T599">v</ta>
            <ta e="T601" id="Seg_7742" s="T600">v</ta>
            <ta e="T602" id="Seg_7743" s="T601">pers</ta>
            <ta e="T607" id="Seg_7744" s="T606">v</ta>
            <ta e="T618" id="Seg_7745" s="T617">que</ta>
            <ta e="T619" id="Seg_7746" s="T618">v</ta>
            <ta e="T621" id="Seg_7747" s="T620">que</ta>
            <ta e="T622" id="Seg_7748" s="T621">dempro</ta>
            <ta e="T623" id="Seg_7749" s="T622">n</ta>
            <ta e="T624" id="Seg_7750" s="T623">dempro</ta>
            <ta e="T625" id="Seg_7751" s="T624">dempro</ta>
            <ta e="T626" id="Seg_7752" s="T625">n</ta>
            <ta e="T627" id="Seg_7753" s="T626">v</ta>
            <ta e="T628" id="Seg_7754" s="T627">ptcl</ta>
            <ta e="T631" id="Seg_7755" s="T630">cardnum</ta>
            <ta e="T632" id="Seg_7756" s="T631">cardnum</ta>
            <ta e="T633" id="Seg_7757" s="T632">n</ta>
            <ta e="T634" id="Seg_7758" s="T633">cardnum</ta>
            <ta e="T635" id="Seg_7759" s="T634">cardnum</ta>
            <ta e="T636" id="Seg_7760" s="T635">n</ta>
            <ta e="T637" id="Seg_7761" s="T636">v</ta>
            <ta e="T638" id="Seg_7762" s="T637">n</ta>
            <ta e="T640" id="Seg_7763" s="T639">adv</ta>
            <ta e="T641" id="Seg_7764" s="T640">n</ta>
            <ta e="T642" id="Seg_7765" s="T641">n</ta>
            <ta e="T643" id="Seg_7766" s="T642">post</ta>
            <ta e="T644" id="Seg_7767" s="T643">v</ta>
            <ta e="T645" id="Seg_7768" s="T644">n</ta>
            <ta e="T647" id="Seg_7769" s="T646">v</ta>
            <ta e="T648" id="Seg_7770" s="T647">n</ta>
            <ta e="T650" id="Seg_7771" s="T649">n</ta>
            <ta e="T651" id="Seg_7772" s="T650">adv</ta>
            <ta e="T652" id="Seg_7773" s="T651">n</ta>
            <ta e="T653" id="Seg_7774" s="T652">v</ta>
            <ta e="T655" id="Seg_7775" s="T654">interj</ta>
            <ta e="T656" id="Seg_7776" s="T655">v</ta>
            <ta e="T657" id="Seg_7777" s="T656">v</ta>
            <ta e="T658" id="Seg_7778" s="T657">que</ta>
            <ta e="T659" id="Seg_7779" s="T658">adv</ta>
            <ta e="T660" id="Seg_7780" s="T659">n</ta>
            <ta e="T661" id="Seg_7781" s="T660">n</ta>
            <ta e="T662" id="Seg_7782" s="T661">v</ta>
            <ta e="T666" id="Seg_7783" s="T665">n</ta>
            <ta e="T667" id="Seg_7784" s="T666">v</ta>
            <ta e="T670" id="Seg_7785" s="T669">n</ta>
            <ta e="T672" id="Seg_7786" s="T671">n</ta>
            <ta e="T676" id="Seg_7787" s="T675">v</ta>
            <ta e="T677" id="Seg_7788" s="T676">v</ta>
            <ta e="T678" id="Seg_7789" s="T677">v</ta>
            <ta e="T686" id="Seg_7790" s="T685">conj</ta>
            <ta e="T687" id="Seg_7791" s="T686">pers</ta>
            <ta e="T688" id="Seg_7792" s="T687">dempro</ta>
            <ta e="T689" id="Seg_7793" s="T688">n</ta>
            <ta e="T690" id="Seg_7794" s="T689">post</ta>
            <ta e="T691" id="Seg_7795" s="T690">v</ta>
            <ta e="T695" id="Seg_7796" s="T694">cardnum</ta>
            <ta e="T696" id="Seg_7797" s="T695">v</ta>
            <ta e="T697" id="Seg_7798" s="T696">n</ta>
            <ta e="T705" id="Seg_7799" s="T704">v</ta>
            <ta e="T706" id="Seg_7800" s="T705">adv</ta>
            <ta e="T707" id="Seg_7801" s="T706">v</ta>
            <ta e="T708" id="Seg_7802" s="T707">v</ta>
            <ta e="T710" id="Seg_7803" s="T709">n</ta>
            <ta e="T711" id="Seg_7804" s="T710">v</ta>
            <ta e="T715" id="Seg_7805" s="T714">ptcl</ta>
            <ta e="T716" id="Seg_7806" s="T715">que</ta>
            <ta e="T717" id="Seg_7807" s="T716">dempro</ta>
            <ta e="T718" id="Seg_7808" s="T717">adj</ta>
            <ta e="T719" id="Seg_7809" s="T718">n</ta>
            <ta e="T720" id="Seg_7810" s="T719">n</ta>
            <ta e="T721" id="Seg_7811" s="T720">adj</ta>
            <ta e="T722" id="Seg_7812" s="T721">adj</ta>
            <ta e="T726" id="Seg_7813" s="T725">n</ta>
            <ta e="T727" id="Seg_7814" s="T726">adj</ta>
            <ta e="T728" id="Seg_7815" s="T727">adv</ta>
            <ta e="T729" id="Seg_7816" s="T728">v</ta>
            <ta e="T730" id="Seg_7817" s="T729">que</ta>
            <ta e="T731" id="Seg_7818" s="T730">n</ta>
            <ta e="T835" id="Seg_7819" s="T732">n</ta>
            <ta e="T733" id="Seg_7820" s="T835">v</ta>
            <ta e="T734" id="Seg_7821" s="T733">conj</ta>
            <ta e="T735" id="Seg_7822" s="T734">n</ta>
            <ta e="T736" id="Seg_7823" s="T735">n</ta>
            <ta e="T737" id="Seg_7824" s="T736">n</ta>
            <ta e="T738" id="Seg_7825" s="T737">adj</ta>
            <ta e="T739" id="Seg_7826" s="T738">ptcl</ta>
            <ta e="T740" id="Seg_7827" s="T739">n</ta>
            <ta e="T742" id="Seg_7828" s="T741">n</ta>
            <ta e="T743" id="Seg_7829" s="T742">n</ta>
            <ta e="T744" id="Seg_7830" s="T743">propr</ta>
            <ta e="T745" id="Seg_7831" s="T744">n</ta>
            <ta e="T746" id="Seg_7832" s="T745">dempro</ta>
            <ta e="T748" id="Seg_7833" s="T747">que</ta>
            <ta e="T749" id="Seg_7834" s="T748">v</ta>
            <ta e="T750" id="Seg_7835" s="T749">adv</ta>
            <ta e="T751" id="Seg_7836" s="T750">n</ta>
            <ta e="T752" id="Seg_7837" s="T751">post</ta>
            <ta e="T754" id="Seg_7838" s="T753">ptcl</ta>
            <ta e="T755" id="Seg_7839" s="T754">adj</ta>
            <ta e="T756" id="Seg_7840" s="T755">que</ta>
            <ta e="T757" id="Seg_7841" s="T756">n</ta>
            <ta e="T758" id="Seg_7842" s="T757">v</ta>
            <ta e="T759" id="Seg_7843" s="T758">n</ta>
            <ta e="T760" id="Seg_7844" s="T759">post</ta>
            <ta e="T767" id="Seg_7845" s="T766">dempro</ta>
            <ta e="T768" id="Seg_7846" s="T767">que</ta>
            <ta e="T772" id="Seg_7847" s="T771">v</ta>
            <ta e="T773" id="Seg_7848" s="T772">pers</ta>
            <ta e="T777" id="Seg_7849" s="T776">adv</ta>
            <ta e="T778" id="Seg_7850" s="T777">v</ta>
            <ta e="T779" id="Seg_7851" s="T778">conj</ta>
            <ta e="T780" id="Seg_7852" s="T779">n</ta>
            <ta e="T781" id="Seg_7853" s="T780">v</ta>
            <ta e="T782" id="Seg_7854" s="T781">ptcl</ta>
            <ta e="T789" id="Seg_7855" s="T788">adv</ta>
            <ta e="T791" id="Seg_7856" s="T790">cardnum</ta>
            <ta e="T792" id="Seg_7857" s="T791">n</ta>
            <ta e="T793" id="Seg_7858" s="T792">ptcl</ta>
            <ta e="T794" id="Seg_7859" s="T793">v</ta>
            <ta e="T801" id="Seg_7860" s="T800">cardnum</ta>
            <ta e="T802" id="Seg_7861" s="T801">n</ta>
            <ta e="T806" id="Seg_7862" s="T805">n</ta>
            <ta e="T807" id="Seg_7863" s="T806">v</ta>
            <ta e="T817" id="Seg_7864" s="T816">adj</ta>
            <ta e="T818" id="Seg_7865" s="T817">n</ta>
            <ta e="T820" id="Seg_7866" s="T819">n</ta>
            <ta e="T821" id="Seg_7867" s="T820">n</ta>
            <ta e="T822" id="Seg_7868" s="T821">post</ta>
            <ta e="T823" id="Seg_7869" s="T822">n</ta>
            <ta e="T824" id="Seg_7870" s="T823">post</ta>
            <ta e="T825" id="Seg_7871" s="T824">ptcl</ta>
            <ta e="T826" id="Seg_7872" s="T825">n</ta>
            <ta e="T828" id="Seg_7873" s="T827">ordnum</ta>
            <ta e="T829" id="Seg_7874" s="T828">n</ta>
            <ta e="T830" id="Seg_7875" s="T829">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_7876" s="T3">RUS:cult</ta>
            <ta e="T6" id="Seg_7877" s="T5">RUS:cult</ta>
            <ta e="T10" id="Seg_7878" s="T9">NGAN:cult</ta>
            <ta e="T18" id="Seg_7879" s="T17">RUS:cult</ta>
            <ta e="T19" id="Seg_7880" s="T18">RUS:cult</ta>
            <ta e="T20" id="Seg_7881" s="T19">RUS:disc</ta>
            <ta e="T25" id="Seg_7882" s="T24">RUS:cult</ta>
            <ta e="T38" id="Seg_7883" s="T37">RUS:cult</ta>
            <ta e="T39" id="Seg_7884" s="T38">RUS:cult</ta>
            <ta e="T44" id="Seg_7885" s="T43">RUS:core</ta>
            <ta e="T45" id="Seg_7886" s="T44">RUS:mod</ta>
            <ta e="T50" id="Seg_7887" s="T49">RUS:core</ta>
            <ta e="T55" id="Seg_7888" s="T54">RUS:cult</ta>
            <ta e="T56" id="Seg_7889" s="T55">RUS:cult</ta>
            <ta e="T62" id="Seg_7890" s="T61">RUS:gram</ta>
            <ta e="T68" id="Seg_7891" s="T67">RUS:gram</ta>
            <ta e="T86" id="Seg_7892" s="T85">RUS:cult</ta>
            <ta e="T94" id="Seg_7893" s="T93">RUS:core</ta>
            <ta e="T95" id="Seg_7894" s="T94">RUS:core</ta>
            <ta e="T107" id="Seg_7895" s="T106">RUS:disc</ta>
            <ta e="T112" id="Seg_7896" s="T111">RUS:cult</ta>
            <ta e="T151" id="Seg_7897" s="T150">RUS:core</ta>
            <ta e="T164" id="Seg_7898" s="T163">RUS:mod</ta>
            <ta e="T171" id="Seg_7899" s="T170">EV:cult</ta>
            <ta e="T173" id="Seg_7900" s="T172">RUS:mod</ta>
            <ta e="T174" id="Seg_7901" s="T173">RUS:core</ta>
            <ta e="T175" id="Seg_7902" s="T174">RUS:cult</ta>
            <ta e="T177" id="Seg_7903" s="T176">RUS:cult</ta>
            <ta e="T202" id="Seg_7904" s="T201">RUS:mod</ta>
            <ta e="T205" id="Seg_7905" s="T204">RUS:core</ta>
            <ta e="T213" id="Seg_7906" s="T212">RUS:cult</ta>
            <ta e="T230" id="Seg_7907" s="T229">RUS:cult</ta>
            <ta e="T232" id="Seg_7908" s="T231">RUS:core</ta>
            <ta e="T234" id="Seg_7909" s="T233">RUS:cult</ta>
            <ta e="T263" id="Seg_7910" s="T262">RUS:mod</ta>
            <ta e="T270" id="Seg_7911" s="T269">RUS:cult</ta>
            <ta e="T273" id="Seg_7912" s="T272">RUS:cult</ta>
            <ta e="T284" id="Seg_7913" s="T283">RUS:cult</ta>
            <ta e="T291" id="Seg_7914" s="T290">RUS:cult</ta>
            <ta e="T294" id="Seg_7915" s="T293">RUS:cult</ta>
            <ta e="T296" id="Seg_7916" s="T295">RUS:cult</ta>
            <ta e="T298" id="Seg_7917" s="T297">RUS:mod</ta>
            <ta e="T318" id="Seg_7918" s="T317">RUS:cult</ta>
            <ta e="T321" id="Seg_7919" s="T320">RUS:cult</ta>
            <ta e="T328" id="Seg_7920" s="T327">RUS:gram</ta>
            <ta e="T332" id="Seg_7921" s="T331">RUS:cult</ta>
            <ta e="T337" id="Seg_7922" s="T336">RUS:cult</ta>
            <ta e="T340" id="Seg_7923" s="T339">RUS:cult</ta>
            <ta e="T341" id="Seg_7924" s="T340">RUS:cult</ta>
            <ta e="T346" id="Seg_7925" s="T345">RUS:cult</ta>
            <ta e="T360" id="Seg_7926" s="T359">EV:core</ta>
            <ta e="T373" id="Seg_7927" s="T372">RUS:gram</ta>
            <ta e="T380" id="Seg_7928" s="T379">RUS:cult</ta>
            <ta e="T383" id="Seg_7929" s="T382">RUS:cult</ta>
            <ta e="T384" id="Seg_7930" s="T383">RUS:gram</ta>
            <ta e="T387" id="Seg_7931" s="T386">RUS:cult</ta>
            <ta e="T400" id="Seg_7932" s="T399">RUS:cult</ta>
            <ta e="T413" id="Seg_7933" s="T412">RUS:cult</ta>
            <ta e="T452" id="Seg_7934" s="T451">RUS:core</ta>
            <ta e="T465" id="Seg_7935" s="T464">RUS:core</ta>
            <ta e="T484" id="Seg_7936" s="T483">RUS:cult</ta>
            <ta e="T494" id="Seg_7937" s="T493">RUS:cult</ta>
            <ta e="T499" id="Seg_7938" s="T498">RUS:cult</ta>
            <ta e="T501" id="Seg_7939" s="T500">RUS:mod</ta>
            <ta e="T502" id="Seg_7940" s="T501">RUS:cult</ta>
            <ta e="T512" id="Seg_7941" s="T511">RUS:cult</ta>
            <ta e="T516" id="Seg_7942" s="T515">RUS:cult</ta>
            <ta e="T524" id="Seg_7943" s="T523">RUS:cult</ta>
            <ta e="T527" id="Seg_7944" s="T526">RUS:cult</ta>
            <ta e="T528" id="Seg_7945" s="T527">RUS:core</ta>
            <ta e="T540" id="Seg_7946" s="T539">RUS:cult</ta>
            <ta e="T545" id="Seg_7947" s="T544">RUS:mod</ta>
            <ta e="T546" id="Seg_7948" s="T545">RUS:disc</ta>
            <ta e="T548" id="Seg_7949" s="T547">RUS:cult</ta>
            <ta e="T550" id="Seg_7950" s="T549">RUS:core</ta>
            <ta e="T585" id="Seg_7951" s="T584">RUS:disc</ta>
            <ta e="T586" id="Seg_7952" s="T585">RUS:mod</ta>
            <ta e="T587" id="Seg_7953" s="T586">EV:core</ta>
            <ta e="T589" id="Seg_7954" s="T588">RUS:core</ta>
            <ta e="T607" id="Seg_7955" s="T606">RUS:cult</ta>
            <ta e="T623" id="Seg_7956" s="T622">RUS:cult</ta>
            <ta e="T626" id="Seg_7957" s="T625">RUS:cult</ta>
            <ta e="T638" id="Seg_7958" s="T637">RUS:cult</ta>
            <ta e="T641" id="Seg_7959" s="T640">RUS:cult</ta>
            <ta e="T642" id="Seg_7960" s="T641">RUS:cult</ta>
            <ta e="T645" id="Seg_7961" s="T644">RUS:cult</ta>
            <ta e="T648" id="Seg_7962" s="T647">RUS:cult</ta>
            <ta e="T650" id="Seg_7963" s="T649">RUS:cult</ta>
            <ta e="T652" id="Seg_7964" s="T651">RUS:cult</ta>
            <ta e="T660" id="Seg_7965" s="T659">RUS:cult</ta>
            <ta e="T661" id="Seg_7966" s="T660">RUS:cult</ta>
            <ta e="T666" id="Seg_7967" s="T665">RUS:cult</ta>
            <ta e="T670" id="Seg_7968" s="T669">RUS:cult</ta>
            <ta e="T672" id="Seg_7969" s="T671">RUS:cult</ta>
            <ta e="T686" id="Seg_7970" s="T685">RUS:gram</ta>
            <ta e="T697" id="Seg_7971" s="T696">RUS:cult</ta>
            <ta e="T706" id="Seg_7972" s="T705">RUS:mod</ta>
            <ta e="T708" id="Seg_7973" s="T707">RUS:cult</ta>
            <ta e="T710" id="Seg_7974" s="T709">RUS:cult</ta>
            <ta e="T715" id="Seg_7975" s="T714">RUS:disc</ta>
            <ta e="T719" id="Seg_7976" s="T718">RUS:cult</ta>
            <ta e="T726" id="Seg_7977" s="T725">RUS:cult</ta>
            <ta e="T729" id="Seg_7978" s="T728">RUS:cult</ta>
            <ta e="T731" id="Seg_7979" s="T730">RUS:cult</ta>
            <ta e="T734" id="Seg_7980" s="T733">RUS:gram</ta>
            <ta e="T737" id="Seg_7981" s="T736">RUS:cult</ta>
            <ta e="T739" id="Seg_7982" s="T738">RUS:disc</ta>
            <ta e="T743" id="Seg_7983" s="T742">RUS:cult</ta>
            <ta e="T744" id="Seg_7984" s="T743">RUS:cult</ta>
            <ta e="T750" id="Seg_7985" s="T749">RUS:mod</ta>
            <ta e="T751" id="Seg_7986" s="T750">RUS:cult</ta>
            <ta e="T754" id="Seg_7987" s="T753">RUS:disc</ta>
            <ta e="T755" id="Seg_7988" s="T754">RUS:cult</ta>
            <ta e="T757" id="Seg_7989" s="T756">RUS:cult</ta>
            <ta e="T759" id="Seg_7990" s="T758">RUS:cult</ta>
            <ta e="T779" id="Seg_7991" s="T778">RUS:gram</ta>
            <ta e="T789" id="Seg_7992" s="T788">RUS:mod</ta>
            <ta e="T793" id="Seg_7993" s="T792">RUS:mod</ta>
            <ta e="T818" id="Seg_7994" s="T817">RUS:cult</ta>
            <ta e="T823" id="Seg_7995" s="T822">RUS:cult</ta>
            <ta e="T829" id="Seg_7996" s="T828">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T7" id="Seg_7997" s="T1">My name is Iste (Stepanida), in Dolgan they say Iste.</ta>
            <ta e="T12" id="Seg_7998" s="T8">I was born in Popigaj.</ta>
            <ta e="T22" id="Seg_7999" s="T13">My parents, my father is Nikolaj, my mother is Taiska.</ta>
            <ta e="T33" id="Seg_8000" s="T23">Then my mother died very early, and I stayed with my father.</ta>
            <ta e="T41" id="Seg_8001" s="T34">My father worked as a brigade leader.</ta>
            <ta e="T48" id="Seg_8002" s="T42">Then my brother also travelled with my father.</ta>
            <ta e="T53" id="Seg_8003" s="T49">We travelled in the tundra.</ta>
            <ta e="T60" id="Seg_8004" s="T54">When I came from school for my holiday I was at my father.</ta>
            <ta e="T75" id="Seg_8005" s="T61">But after I grew up I was at my father, when I was little I was raised by people, by other people.</ta>
            <ta e="T87" id="Seg_8006" s="T76">I was raised and then when I was grown up I was at my father, I came to my father in my holidays.</ta>
            <ta e="T102" id="Seg_8007" s="T88">We migrated from the one place to the other place on a sleigh, on them we migrated.</ta>
            <ta e="T122" id="Seg_8008" s="T103">We lived in a tent, there we whatchammacallit cleaned up snow, then we put up long poles, we make a tent.</ta>
            <ta e="T131" id="Seg_8009" s="T123">And we live there in the tent.</ta>
            <ta e="T140" id="Seg_8010" s="T132">Hmmm that I said in Russian, in a pole tent.</ta>
            <ta e="T147" id="Seg_8011" s="T141">The poles they make of whatever, such long ones.</ta>
            <ta e="T164" id="Seg_8012" s="T148">That house they sometimes make very broad, many people, we are many people.</ta>
            <ta e="T171" id="Seg_8013" s="T165">Broad, well, broad house, a broad tent.</ta>
            <ta e="T182" id="Seg_8014" s="T172">Then our oven, our stove, our metal stove, we immediately light the oven and it's warm.</ta>
            <ta e="T200" id="Seg_8015" s="T183">The bottom they cover with snow and immediately so the wind doesn't blow through.</ta>
            <ta e="T214" id="Seg_8016" s="T201">Even with small children we travelled, there they had a cradle, in a Dolgan cradle there.</ta>
            <ta e="T223" id="Seg_8017" s="T215">They put the child in and we travel.</ta>
            <ta e="T234" id="Seg_8018" s="T224">Then what do I say? My father travelled in the direction of Kamen'.</ta>
            <ta e="T248" id="Seg_8019" s="T235">There is no goose, no ducks, there are only reindeer.</ta>
            <ta e="T261" id="Seg_8020" s="T249">There are stone mice, rarely mice appear under the stone.</ta>
            <ta e="T274" id="Seg_8021" s="T262">What else? I myself studied until the seventh grade, I stopped from the eighth grade.</ta>
            <ta e="T280" id="Seg_8022" s="T275">After that I was (we were) in our boarding school.</ta>
            <ta e="T289" id="Seg_8023" s="T281">Two years we studied in Norilsk, we lived in the boarding school.</ta>
            <ta e="T296" id="Seg_8024" s="T290">We went to a holiday camp in where… to Tajozhnaj I went, to Kurejka.</ta>
            <ta e="T306" id="Seg_8025" s="T297">Then when I grew up I went to study.</ta>
            <ta e="T312" id="Seg_8026" s="T307">At courses.</ta>
            <ta e="T321" id="Seg_8027" s="T313">A selling person, I went to study for salesman in Dudinka.</ta>
            <ta e="T338" id="Seg_8028" s="T322">I studied six months, then I came to Khatanga, we did a placement and then I worked at the buffet.</ta>
            <ta e="T347" id="Seg_8029" s="T339">In the cantine there was a buffet in Khatanga and there in that buffet I used to work.</ta>
            <ta e="T360" id="Seg_8030" s="T348">And then in Khatanga I became friends with one person here from Kheta, with one boy.</ta>
            <ta e="T368" id="Seg_8031" s="T361">And we got married there, we got to know each other and got married.</ta>
            <ta e="T377" id="Seg_8032" s="T369">Then after we got married we whatever, I lived in Khatanga.</ta>
            <ta e="T390" id="Seg_8033" s="T378">I worked in the kindergarten as a nanny, and he worked as a technician.</ta>
            <ta e="T403" id="Seg_8034" s="T391">And then our daughter was born, my daughter named Rita, my daughter, my son.</ta>
            <ta e="T414" id="Seg_8035" s="T404">Then he somehow wanted to go to his place of birth.</ta>
            <ta e="T422" id="Seg_8036" s="T415">And first he sent me.</ta>
            <ta e="T428" id="Seg_8037" s="T423">With my child, with my daughter.</ta>
            <ta e="T438" id="Seg_8038" s="T429">And then he, after me he came.</ta>
            <ta e="T456" id="Seg_8039" s="T439">And from then onwards, how many years it was, from sixty one I live here.</ta>
            <ta e="T466" id="Seg_8040" s="T457">He died some time in the year seventy nine he died.</ta>
            <ta e="T481" id="Seg_8041" s="T467">And from that time I gave birth to my son, who served in the army, studied in Irkutsk.</ta>
            <ta e="T484" id="Seg_8042" s="T482">He served in Ussurijsk.</ta>
            <ta e="T502" id="Seg_8043" s="T485">From then onwards I live, I worked in the kindergarten, till retirement, in this kindergarten.</ta>
            <ta e="T518" id="Seg_8044" s="T503">Then what shall I say? Then my husband has gone fishing, we live at a hunting spot nearby.</ta>
            <ta e="T528" id="Seg_8045" s="T519">When I have weekend, in my weekends I go to him by motor boat, we travel.</ta>
            <ta e="T541" id="Seg_8046" s="T529">And there I help I cleaned the fish and so on.</ta>
            <ta e="T542" id="Seg_8047" s="T541">And so on.</ta>
            <ta e="T550" id="Seg_8048" s="T543">We gave it to the fish industry, he gave it.</ta>
            <ta e="T555" id="Seg_8049" s="T551">They caught a lot of fish.</ta>
            <ta e="T564" id="Seg_8050" s="T556">Early in the morning they go, then they check the nets, and return late.</ta>
            <ta e="T570" id="Seg_8051" s="T565">We almost live in the nights.</ta>
            <ta e="T583" id="Seg_8052" s="T571">In summer the mosquitoes bite, those kids of us, those boys of us, my son.</ta>
            <ta e="T607" id="Seg_8053" s="T583">And then there is also friends, the son of my husbands sister, they made fire, smoke, for the mosquitoes not to come closer to us, when we filleted the fish.</ta>
            <ta e="T612" id="Seg_8054" s="T608">That's how we lived.</ta>
            <ta e="T619" id="Seg_8055" s="T613">Then what else is there, what do I tell?</ta>
            <ta e="T628" id="Seg_8056" s="T620">Well in that kindergarten, and the I retired.</ta>
            <ta e="T638" id="Seg_8057" s="T629">Thirty eight years, thirty eight years I worked in the kindergarten.</ta>
            <ta e="T648" id="Seg_8058" s="T639">First at the school I worked as a cook, I worked as a cook.</ta>
            <ta e="T662" id="Seg_8059" s="T649">At the school then they tranferred me to the kindergarten, they took me, then from the school I worked in the kindergarten.</ta>
            <ta e="T670" id="Seg_8060" s="T663">And now I am retired, I raise my grandchild, I raise my grandchildren.</ta>
            <ta e="T678" id="Seg_8061" s="T671">My great grandchild I also sit, I go and work.</ta>
            <ta e="T691" id="Seg_8062" s="T679">My daughter in law also has to work and I sit with the child.</ta>
            <ta e="T700" id="Seg_8063" s="T692">It is still small it will turn one the twentieth of March</ta>
            <ta e="T708" id="Seg_8064" s="T701">Now he is (we are) a little ill, suffer of stomatitis.</ta>
            <ta e="T713" id="Seg_8065" s="T709">Mother is ill.</ta>
            <ta e="T732" id="Seg_8066" s="T714">Well my big grandchild of my son, the big one who, ehm, the oldest grandchild now serves in the army.</ta>
            <ta e="T733" id="Seg_8067" s="T732">The others are learning.</ta>
            <ta e="T752" id="Seg_8068" s="T733">And the girl, my granddaughter, the big son… the girl, granddaughter her name is Katja, she studies in Norilsk, like her mother.</ta>
            <ta e="T768" id="Seg_8069" s="T753">She studies at the technical college in Norilsk, like her mother, for sewing and so on.</ta>
            <ta e="T787" id="Seg_8070" s="T769">She gave birth to a child and left it with us, and she went back to finish, in order not to miss a year.</ta>
            <ta e="T794" id="Seg_8071" s="T788">Still two years she has to study.</ta>
            <ta e="T807" id="Seg_8072" s="T795">Three years and something, three years and something they study to work.</ta>
            <ta e="T813" id="Seg_8073" s="T808">Well that's how it is.</ta>
            <ta e="T826" id="Seg_8074" s="T814">I live alone, I live with one of my grandchildren, with the son of my daughter, I am home with my grandchild.</ta>
            <ta e="T830" id="Seg_8075" s="T827">He studies in the seventh grade.</ta>
            <ta e="T834" id="Seg_8076" s="T831">What else is there to say?</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T7" id="Seg_8077" s="T1">Mein Name ist Iste (Stepanida), auf Dolganisch sagt man Iste.</ta>
            <ta e="T12" id="Seg_8078" s="T8">Ich wurde in Popigaj geboren.</ta>
            <ta e="T22" id="Seg_8079" s="T13">Meine Eltern, mein Vater ist Nikolaj, meine Mutter ist Taiska.</ta>
            <ta e="T33" id="Seg_8080" s="T23">Dann starb meine Mutter sehr früh und ich blieb mit meinem Vater zurück.</ta>
            <ta e="T41" id="Seg_8081" s="T34">Mein Vater arbeitete als Brigadeleiter.</ta>
            <ta e="T48" id="Seg_8082" s="T42">Dann mein Bruder arbeitete auch mit meinem Vater.</ta>
            <ta e="T53" id="Seg_8083" s="T49">Wir nomadisierten in der Tundra.</ta>
            <ta e="T60" id="Seg_8084" s="T54">Wenn ich in meinen Ferien aus der Schule kam, war ich bei meinem Vater.</ta>
            <ta e="T75" id="Seg_8085" s="T61">Aber nachdem ich groß geworden war, war ich bei meinem Vater, als ich klein war, wurde ich von Leuten aufgezogen, von anderen Leuten.</ta>
            <ta e="T87" id="Seg_8086" s="T76">Ich wurde aufgezogen und dann, als ich groß geworden war, war ich bei meinem Vater, ich kam in den Ferien zu meinem Vater.</ta>
            <ta e="T102" id="Seg_8087" s="T88">We zogen von einem Ort zum anderen auf einem Schlitten, auf diesen zogen wir [umher].</ta>
            <ta e="T122" id="Seg_8088" s="T103">Wir lebten in einem Zelt, dort dingsten, räumten wir den Schnee weg, dann stellen wir lange Stangen auf, wir machen ein Zelt.</ta>
            <ta e="T131" id="Seg_8089" s="T123">Und wir leben dort in dem Zelt.</ta>
            <ta e="T140" id="Seg_8090" s="T132">Hm, das habe ich auf Russisch gesagt, in einem Stangenzelt.</ta>
            <ta e="T147" id="Seg_8091" s="T141">Die Stangen sind aus irgendwas gemacht, so lange.</ta>
            <ta e="T164" id="Seg_8092" s="T148">Das Haus wird manchmal sehr geräumig gemacht, viele Leute, wir sind viele Leute.</ta>
            <ta e="T171" id="Seg_8093" s="T165">Breit, nun, ein geräumiges Haus, ein geräumiges Zelt.</ta>
            <ta e="T182" id="Seg_8094" s="T172">Dann unser Ofen, unser Herd, unser Metallofen, wir heizen den Ofen sofort und es ist warm.</ta>
            <ta e="T200" id="Seg_8095" s="T183">Den Boden bedeckt man mit Schnee und sofort, damit es nicht dingst, nicht zieht.</ta>
            <ta e="T214" id="Seg_8096" s="T201">Sogar mit kleinen Kindern sind wir gefahren, dort hatten sie eine Wiege, in einer dolganischen Wiege dort.</ta>
            <ta e="T223" id="Seg_8097" s="T215">Man legt das Kind hinein und wir fahren.</ta>
            <ta e="T234" id="Seg_8098" s="T224">Was erzähle ich dann? Mein Vater fuhr in Richtung Kamen'.</ta>
            <ta e="T248" id="Seg_8099" s="T235">Dort gibt es keine Gänse, keine Enten, dort sind nur Rentiere.</ta>
            <ta e="T261" id="Seg_8100" s="T249">Dort gibt es Steinmäuse, selten gibt es unter den Steinen Mäuse.</ta>
            <ta e="T274" id="Seg_8101" s="T262">Was noch? Ich selbst lernte bis zum siebten Klasse, ich hörte in der achten Klasse auf.</ta>
            <ta e="T280" id="Seg_8102" s="T275">Danach waren wir in unserem Internat.</ta>
            <ta e="T289" id="Seg_8103" s="T281">Zwei Jahre lernten wir in Norilsk, wir lebten im Internat.</ta>
            <ta e="T296" id="Seg_8104" s="T290">Wir fuhren in ein Ferienlager in Dings, ich fuhr nach Tajozhnij, nach Kurejka.</ta>
            <ta e="T306" id="Seg_8105" s="T297">Dann, als ich erwachsen war, ging ich studieren.</ta>
            <ta e="T312" id="Seg_8106" s="T307">In Kursen.</ta>
            <ta e="T321" id="Seg_8107" s="T313">Eine Verkäuferin, ich ging nach Dudinka, um auf Verkäuferin zu lernen.</ta>
            <ta e="T338" id="Seg_8108" s="T322">Ich lernte sechs Monate, dann kam ich nach Chatanga, wir machten ein Praktikum und dann arbeite ich am Buffet.</ta>
            <ta e="T347" id="Seg_8109" s="T339">In der Kantine gab es ein Buffet in Chatanga und dort am Buffet arbeitete ich immer.</ta>
            <ta e="T360" id="Seg_8110" s="T348">Und dann in Chatanga freundete ich mich einer Person hier aus Cheta an, mit einem Jungen.</ta>
            <ta e="T368" id="Seg_8111" s="T361">Und wir heirateten dort, wir lernten uns kennen und heirateten.</ta>
            <ta e="T377" id="Seg_8112" s="T369">Dann nachdem wir geheiratet hatten, dingsten wir, ich lebte in Chatanga.</ta>
            <ta e="T390" id="Seg_8113" s="T378">Ich arbeitete im Kindergarten als Kindermädchen und er arbeitete als Techniker.</ta>
            <ta e="T403" id="Seg_8114" s="T391">Und dann wurde unsere Tochter geboren, meine Tochter namens Rita, meine Tochter, mein Sohn.</ta>
            <ta e="T414" id="Seg_8115" s="T404">Dann wollte er irgendwie an seinen Geburtsort gehen.</ta>
            <ta e="T422" id="Seg_8116" s="T415">Und zuerst schickte er mich.</ta>
            <ta e="T428" id="Seg_8117" s="T423">Mit meinem Kind, mit meiner Tochter.</ta>
            <ta e="T438" id="Seg_8118" s="T429">Und dann erst er, nach mir kam er.</ta>
            <ta e="T456" id="Seg_8119" s="T439">Und seit damals, wie lange war es, seit Einundsechzig lebe ich hier.</ta>
            <ta e="T466" id="Seg_8120" s="T457">Er starb irgendwann, im Jahr neunundsiebzig starb er.</ta>
            <ta e="T481" id="Seg_8121" s="T467">Und seitdem habe ich meinen Sohn geboren, der in der Armee gedient hat, in Irkutsk studiert hat.</ta>
            <ta e="T484" id="Seg_8122" s="T482">Er diente in Ussurijsk.</ta>
            <ta e="T502" id="Seg_8123" s="T485">Nun, und seitdem lebe ich, ich habe im Kindergarten gearbeitet, bis zur Rente, in diesem Kindergarten.</ta>
            <ta e="T518" id="Seg_8124" s="T503">Was soll ich dann erzählen? Dann ging mein Mann fischen, wir leben an einer Jagdstelle in der Nähe.</ta>
            <ta e="T528" id="Seg_8125" s="T519">Wenn ich frei habe, an meinen Wochenende fahre ich mit dem Motorboot zu ihm, wir fahren.</ta>
            <ta e="T541" id="Seg_8126" s="T529">Und dort helfe ich den Fisch zu reinigen und so weiter.</ta>
            <ta e="T542" id="Seg_8127" s="T541">Und so weiter.</ta>
            <ta e="T550" id="Seg_8128" s="T543">Wir gaben ihn in die Fischfabrik, er gab ihn.</ta>
            <ta e="T555" id="Seg_8129" s="T551">Sie fingen viel Fisch.</ta>
            <ta e="T564" id="Seg_8130" s="T556">Früh am Morgen gehen sie, dann prüfen sie die Netze und kommen spät zurück.</ta>
            <ta e="T570" id="Seg_8131" s="T565">Wir leben fast in der Nacht.</ta>
            <ta e="T583" id="Seg_8132" s="T571">Im Sommer stechen die Mücken, unsere Kinder, unsere Jungs, meinen Sohn.</ta>
            <ta e="T607" id="Seg_8133" s="T583">Und dann sind dort auch Freunde, der Sohn meines Schwagers, sie machten Feuer, Rauch, damit die Mücken nicht näher zu uns kommen, wenn wir den Fisch filetieren. </ta>
            <ta e="T612" id="Seg_8134" s="T608">Und so lebten wir.</ta>
            <ta e="T619" id="Seg_8135" s="T613">Dann was noch, was erzähle ich?</ta>
            <ta e="T628" id="Seg_8136" s="T620">Nun, in dem Kindergarten, und dann ging ich in Rente.</ta>
            <ta e="T638" id="Seg_8137" s="T629">38 Jahre, 38 Jahre arbeitete ich im Kindergarten.</ta>
            <ta e="T648" id="Seg_8138" s="T639">Zuerst arbeitete ich als Köchin in der Schule, ich arbeitete als Köchin.</ta>
            <ta e="T662" id="Seg_8139" s="T649">In der Schule versetzten sie man dann in den Kindergarten, sie nahme mich, dann nach der Schule arbeitete ich im Kindergarten.</ta>
            <ta e="T670" id="Seg_8140" s="T663">Und jetzt bin ich in Rente, ich ziehe meinen Enkel groß, meine Enkel.</ta>
            <ta e="T678" id="Seg_8141" s="T671">Meinen Urenkel sitze(?) ich auch, ich gehe arbeiten.</ta>
            <ta e="T691" id="Seg_8142" s="T679">Meine Schwiegertochter muss auch arbeiten und ich sitze mit dem Kind da.</ta>
            <ta e="T700" id="Seg_8143" s="T692">Es ist noch klein, am 20. März wird es ein Jahr alt.</ta>
            <ta e="T708" id="Seg_8144" s="T701">Jetzt dingst er, wir sind [=er ist] ein bisschen krank, er hat Stomatitis.</ta>
            <ta e="T713" id="Seg_8145" s="T709">Mama ist krank.</ta>
            <ta e="T732" id="Seg_8146" s="T714">Nun, mein großer Enkel, von meinem Sohn, der große, äh, der älteste Enkel dient jetzt in der Armee.</ta>
            <ta e="T733" id="Seg_8147" s="T732">Die anderen lernen.</ta>
            <ta e="T752" id="Seg_8148" s="T733">Und das Mädchen, meine Enkelin, der große Sohn… das Mädchen, die Enkelin, ihr Name ist Katja, sie lernt in Norilsk wie ihre Mutter.</ta>
            <ta e="T768" id="Seg_8149" s="T753">Sie lernt an der Fachhochschule in Norilsk, wie ihr Mutter, Nähen und so weiter.</ta>
            <ta e="T787" id="Seg_8150" s="T769">Sie hat ein Kind geboren und es bei uns gelassen, und sie ging zurück, um es zu beenden, um nicht ein Jahr zu verlieren.</ta>
            <ta e="T794" id="Seg_8151" s="T788">Zwei Jahre muss sie noch lernen.</ta>
            <ta e="T807" id="Seg_8152" s="T795">Drei Jahre und ein bisschen, drei Jahre und ein bisschen lernen sie die Arbeit.</ta>
            <ta e="T813" id="Seg_8153" s="T808">Nun, so ist es.</ta>
            <ta e="T826" id="Seg_8154" s="T814">Ich lebe alleine, ich lebe mit einem meiner Enkel, mit dem Sohn meiner Tochter, ich bin zuhause mit meinem Enkel.</ta>
            <ta e="T830" id="Seg_8155" s="T827">Er geht in die siebte Klasse.</ta>
            <ta e="T834" id="Seg_8156" s="T831">Was ist noch zu sagen?</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T7" id="Seg_8157" s="T1">меня зовут Степанида, по долгански говорят Исте</ta>
            <ta e="T12" id="Seg_8158" s="T8">родилась в Попигае</ta>
            <ta e="T22" id="Seg_8159" s="T13">мои родители, отец эмм Николай, мама Таиска</ta>
            <ta e="T33" id="Seg_8160" s="T23">потом мама умерла, а я осталась с отцом</ta>
            <ta e="T41" id="Seg_8161" s="T34">мой отец пастух, бригадиром работал</ta>
            <ta e="T48" id="Seg_8162" s="T42">потом брат тоже с отцом ездил</ta>
            <ta e="T53" id="Seg_8163" s="T49">по тундре мы аргишили</ta>
            <ta e="T60" id="Seg_8164" s="T54">когда из школы на каникулах я была у отца</ta>
            <ta e="T75" id="Seg_8165" s="T61">но когда выросла, я была у отца, когда была маленькой меня воспитывали другие люди</ta>
            <ta e="T87" id="Seg_8166" s="T76">я воспитывалась, потом когда выросла у отца была, к отцу на каникулы ездила</ta>
            <ta e="T102" id="Seg_8167" s="T88">вот так мы аргишили с места на место на нартах, на этих мы аргишили</ta>
            <ta e="T122" id="Seg_8168" s="T103">жили в чум (чум был наш дом) там снег убираем, потом ставим палки такие длинные, чум делаем</ta>
            <ta e="T147" id="Seg_8169" s="T141">палки делают из чего</ta>
            <ta e="T164" id="Seg_8170" s="T148">этот дом иногда широким делают, много людей потому/ к тому же много нас</ta>
            <ta e="T171" id="Seg_8171" s="T165">широкий чум</ta>
            <ta e="T182" id="Seg_8172" s="T172">потом печка железная, печку сразу затопляем и тепло</ta>
            <ta e="T200" id="Seg_8173" s="T183">а внизу груз делают, покрывают снегом, чтобы не дуло</ta>
            <ta e="T214" id="Seg_8174" s="T201">даже с маленькими детьми аргишили там, люлька у них долганская люлька, туда</ta>
            <ta e="T223" id="Seg_8175" s="T215">ездим</ta>
            <ta e="T248" id="Seg_8176" s="T235">там нету гусей уток нету, одни олени там</ta>
            <ta e="T261" id="Seg_8177" s="T249">там каменные мыши, редко эмм, мыши под камнем, иногда бывает</ta>
            <ta e="T274" id="Seg_8178" s="T262">ещё что эмм, я сама училась до седьмого класса, с восьмого класса закончила</ta>
            <ta e="T280" id="Seg_8179" s="T275">в интернате, потом в интернате была</ta>
            <ta e="T289" id="Seg_8180" s="T281">два года в Норильске училась, в интернате жили</ta>
            <ta e="T296" id="Seg_8181" s="T290">ездила в лагерь, где это Таёжна ездила, Курейка (санаторная школа)</ta>
            <ta e="T306" id="Seg_8182" s="T297">потом, когда я выросла, поехала учиться</ta>
            <ta e="T312" id="Seg_8183" s="T307">на это, на курсы</ta>
            <ta e="T321" id="Seg_8184" s="T313">человек, который продаёт (вспоминает, это) на продавца пошла учиться в Дудинке</ta>
            <ta e="T338" id="Seg_8185" s="T322">шесть месяцев проучилась и приехала в Хатангу, проходить практику и там в этом буфете и работала</ta>
            <ta e="T347" id="Seg_8186" s="T339">в столовой был буфет в Хатанге, там в буфете я работала</ta>
            <ta e="T360" id="Seg_8187" s="T348">потом в Хатанге с одним человеком с Хеты познакомилась</ta>
            <ta e="T368" id="Seg_8188" s="T361">и мы там поженились, познакомились и поженились</ta>
            <ta e="T377" id="Seg_8189" s="T369">потом после того, как поженились в Хатанге пожили</ta>
            <ta e="T390" id="Seg_8190" s="T378">я работала в садике няней, а он на почте, монтёром работал</ta>
            <ta e="T403" id="Seg_8191" s="T391">потом родилась дочка, Рита зовут, дочка мальчик</ta>
            <ta e="T414" id="Seg_8192" s="T404">он захотел на свою землю вернуться</ta>
            <ta e="T422" id="Seg_8193" s="T415">и он сначала меня отправил</ta>
            <ta e="T428" id="Seg_8194" s="T423">с ребёнком с дочкой</ta>
            <ta e="T438" id="Seg_8195" s="T429">и потом он попозже за мной приехал</ta>
            <ta e="T456" id="Seg_8196" s="T439">с тех пор столько лет прошло-то</ta>
            <ta e="T466" id="Seg_8197" s="T457">и он умер в семьдесят девятом году</ta>
            <ta e="T481" id="Seg_8198" s="T467">и здесь я родила сына</ta>
            <ta e="T484" id="Seg_8199" s="T482">служил в Уссурийске</ta>
            <ta e="T502" id="Seg_8200" s="T485">ну и с тех пор живу…живу, работала в садике до пенсии уже, в садике, в этом же садике</ta>
            <ta e="T518" id="Seg_8201" s="T503">потом ещё что расскажу, потом мой муж был на рыболовной точке, мы жили близко</ta>
            <ta e="T528" id="Seg_8202" s="T519">когда у меня выходные, в выходные я ездила к нему, на моторке ездили</ta>
            <ta e="T541" id="Seg_8203" s="T529">и там я ..эмм.. помогла, рыбу разделала…эмм..эмм…выделывала разделывала</ta>
            <ta e="T542" id="Seg_8204" s="T541">и прочее</ta>
            <ta e="T550" id="Seg_8205" s="T543">он давал же это ему рыбзаводу, сдавал</ta>
            <ta e="T555" id="Seg_8206" s="T551">они ловили много рыбы</ta>
            <ta e="T564" id="Seg_8207" s="T556">утром рано уехали, потом сети проверять, поздно приезжают</ta>
            <ta e="T570" id="Seg_8208" s="T565">подчти вот сидим ночами</ta>
            <ta e="T583" id="Seg_8209" s="T571">комары кусаются, эти дети парни, мой сын сын</ta>
            <ta e="T607" id="Seg_8210" s="T583">и друзья, мужа сестры сын, они делали костёр, дым делали, это делали для того чтобы комары не приближались к нам, когда мы разделывали рыбу</ta>
            <ta e="T612" id="Seg_8211" s="T608">так и мы жили</ta>
            <ta e="T619" id="Seg_8212" s="T613">потом это что ещё, что ещё рассказать?</ta>
            <ta e="T628" id="Seg_8213" s="T620">эмм в этом садике, из садика это вышла на пенсию</ta>
            <ta e="T638" id="Seg_8214" s="T629">придцать восемь лет, тридцать шесть лет работала в садике</ta>
            <ta e="T648" id="Seg_8215" s="T639">сначала в школе работала поваром, поваром работала, поваром</ta>
            <ta e="T670" id="Seg_8216" s="T663">а сейчас я сижу на пенсии, воспитываю внуков</ta>
            <ta e="T691" id="Seg_8217" s="T679">а невестке надо на работу идти, а я с этим ребёнком сижу</ta>
            <ta e="T700" id="Seg_8218" s="T692">ещё маленькая, двадцатого марта исполнится год, моей внучке</ta>
            <ta e="T713" id="Seg_8219" s="T709">его мама (бабушка) сидит на больничном</ta>
            <ta e="T768" id="Seg_8220" s="T753">в норильском техникуме учится, как её мама на шитьё там на оформлении на этом</ta>
            <ta e="T787" id="Seg_8221" s="T769">родила и оставила нам ребёнка и пошла, поехала обратно закончить, чтобы год не потерять, чтобы год не пропал</ta>
            <ta e="T794" id="Seg_8222" s="T788">ещё два года надо учиться</ta>
            <ta e="T807" id="Seg_8223" s="T795">три года с чем-то работе учатся</ta>
            <ta e="T813" id="Seg_8224" s="T808">вот так, вот такие дела</ta>
            <ta e="T826" id="Seg_8225" s="T814">живу одна, живу с другим внуком, с сыном дочки, с внуком я живу дома</ta>
            <ta e="T830" id="Seg_8226" s="T827">учится в седьмом классе</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T835" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
