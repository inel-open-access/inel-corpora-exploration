<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:string="https://inel.corpora.uni-hamburg.de/xmlns/string"
    exclude-result-prefixes="xs string"
    version="2.0">
    
    <xsl:output method="xml" media-type="text/xml" encoding="UTF-8"/>
    
    <!-- Global parameters -->
    <xsl:param name="base-directory" select="'file:/E:/emnlp2020/corpora/'" as="xs:string"/>
    <xsl:param name="corpus-directory" select="'selkup-0.1', 'dolgan-1.0', 'kamas-1.0'" as="xs:string+"/>
    <xsl:param name="file-pattern" select="'*.exb'" as="xs:string"/>
    <xsl:param name="utterance-tier-category" select="'fe'" as="xs:string"/>
    
    <!-- Templates -->
    <xsl:template match="/">
        <xsl:for-each select="$corpus-directory">
            <xsl:result-document href="{.}-utterances.xml">
                <corpus name="{.}" dir="{concat($base-directory, .)}">
                    <xsl:for-each select="collection(concat($base-directory, ., '/', '?select=', $file-pattern, ';recurse=yes'))">
                        <xsl:apply-templates/>
                    </xsl:for-each>
                </corpus>            
            </xsl:result-document>            
        </xsl:for-each>
    </xsl:template>
    
    
    <!-- for TEI files -->
    <xsl:template match="*:spanGrp[@type=$utterance-tier-category]/*:span">
        <u file-ref="{base-uri()}" tier-category="{$utterance-tier-category}" speaker="{../../@who}" start="{@from}" end="{@to}">
            <xsl:value-of select="replace(text(), '(\([^\(]*\)|\[|\])', '')"/>
        </u>
    </xsl:template>
    
    <!-- for EXB files -->
    <xsl:template match="*:tier[@category=$utterance-tier-category]/*:event">
        <u file-ref="{base-uri()}" tier-category="{$utterance-tier-category}" speaker="{../@speaker}" start="{@start}" end="{@end}">
            <xsl:value-of select="replace(text(), '(\([^\(]*\)|\[|\])', '')"/>
        </u>
    </xsl:template>
        
    <xsl:template match="*">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="@*|text()"/>
    
</xsl:stylesheet>