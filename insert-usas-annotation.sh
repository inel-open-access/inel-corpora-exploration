#!/bin/bash
java -jar software/saxon9he.jar usas-annotation/insert-usas-annotations.xsl -xsl:usas-annotation/insert-usas-annotations.xsl 

for file in $(find corpora/ -name '*.usas')
do
  mv $file $(echo "$file" | sed -r 's|.usas|.exb|g')
done